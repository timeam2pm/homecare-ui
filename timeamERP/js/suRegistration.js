var parentRef = null;
var operation = "";
var selItem = null;
var ADD = "add";
var UPDATE = "edit";
var VIEW = "view";
var DELETE = "delete";
var patientId = "";
var communicationsArrayData = [], relationshipArrayData = [];
var towArr = [{Key:'0',Value:'Sun'},{Key:'1',Value:'Mon'},{Key:'2',Value:'Tue'},{Key:'3',Value:'Wed'},{Key:'4',Value:'Thu'},{Key:'5',Value:'Fri'},{Key:'6',Value:'Sat'}];

$(document).ready(function(){
   
});


$(window).load(function(){
	parentRef = parent.frames['iframe'].window;
	loading = false;
	
	onLoaded();
	if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
	init();
    buttonEvents();
}

function init(){
    getAjaxObject(ipAddress + "/master/Gender/list/", "GET", getGenderValueList, onError);
    getAjaxObject(ipAddress + "/master/marital_status/list?is-active=1", "GET", getMaritalList, onError);
    getAjaxObject(ipAddress + "/master/religion/list?is-active=1", "GET", getReligionList, onError);
    getAjaxObject(ipAddress + "/master/Race/list/", "GET", getRaceValueList, onError);
    getAjaxObject(ipAddress + "/master/Ethnicity/list/", "GET", getEthnicityValueList, onError);
    getAjaxObject(ipAddress + "/master/Prefix/list/", "GET", getPrefixList, onError);
    getAjaxObject(ipAddress + "/master/Language/list?is-active=1", "GET", getLanguageValueList, onError);
    getAjaxObject(ipAddress + "/master/SMS/list/", "GET", getSMSValueList, onError);
    getAjaxObject(ipAddress + '/homecare/communication-types/?fields=id,value&id=:bt:201,202', "GET", getcommunications, onError);
    getAjaxObject(ipAddress + '/master/relation/list/', "GET", getRelations, onError);
}

function getMaritalList(dataObj) {
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDropDownForSelection(dArray, "cmbMarital", onChangeMaritalStatus, ["desc", "value"], 0, "");
    }
}


function getGenderValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDropDownForSelection(dArray, "cmbStaffGender", onGenderChange, ["desc", "idk"], 0, "");
    }
}

function getReligionList(dataObj) {
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDropDownForSelection(dArray, "cmbReligion", onChangeReligion, ["desc", "value"], 0, "");
    }
}

function getRaceValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDropDownForSelection(dArray, "cmbRace", onRaceChange, ["desc", "idk"], 0, "");
    }
}

function getEthnicityValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDropDownForSelection(dArray, "cmbEthicity", onEthnicityChange, ["desc", "idk"], 0, "");
    }
}

function getPrefixList(dataObj) {
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDropDownForSelection(dArray, "cmbPrefix", onPrefixChange, ["value", "idk"], 0, "");
    }
}

function getLanguageValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    

    if (dArray && dArray.length > 0) {
        setDropDownForSelection(dArray, "txtL1", onLanChange1, ["desc", "idk"], 0, "");
        setDropDownForSelection(dArray, "txtL2", onLanChange2, ["desc", "idk"], 0, "");
        setDropDownForSelection(dArray, "txtL3", onLanChange3, ["desc", "idk"], 0, "");
        setDropDownForSelection(dArray, "txtL4", onLanChange4, ["desc", "idk"], 0, "");
        setDropDownForSelection(dArray, "txtL5", onLanChange5, ["desc", "idk"], 0, "");
        setDropDownForSelection(dArray, "txtL6", onLanChange6, ["desc", "idk"], 0, "");
    }
}


function getSMSValueList(dataObj) {
    debugger;
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDropDownForSelection(dArray, "cmbSMS", onSMSChange, ["desc", "idk"], 0, "");
        setDropDownForSelection(dArray, "cmbSMSSecondary", onSMSSecondary, ["desc", "idk"], 0, "");
        setDropDownForSelection(dArray, "cmbContactSMSPrimary", onContactSMSPrimary, ["desc", "idk"], 0, "");
        setDropDownForSelection(dArray, "cmbContactSMSSecondary", onContactSMSSecondary, ["desc", "idk"], 0, "");
        
    }
}


function getcommunications(resp) {
    var dataArray = [];
    communicationsArrayData = [];
    if(resp && resp.response && resp.response.communicationTypes){
        if($.isArray(resp.response.communicationTypes)){
            dataArray = resp.response.communicationTypes;
        }else{
            dataArray.push(resp.response.communicationTypes);
        }
    }
    if(dataArray.length) {
        for (var i = 0; i < dataArray.length; i++){
            if (dataArray[i].value) {
             dataArray[i].desc = dataArray[i].value;
            }
            if (dataArray[i].id) {
             dataArray[i].idk = dataArray[i].id;
            }
         }
        var obj = {}
        obj.idk = 0;
        obj.desc = "";
        dataArray.unshift(obj);

        communicationsArrayData = dataArray;

        setDropDownForSelection(communicationsArrayData, "txtCommunicationType", onCommunicationTypeChange, ["desc", "idk"], 0, "");
    }
}

function getRelations(resp) {
    var dataArray = [];
    if(resp && resp.response && resp.response.codeTable){
        if($.isArray(resp.response.codeTable)){
            dataArray = resp.response.codeTable;
        }else{
            dataArray.push(resp.response.codeTable);
        }
    }
    if(dataArray.length) {

        for (var i = 0; i < dataArray.length; i++){
            if (dataArray[i].value) {
             dataArray[i].desc = dataArray[i].value;
            }
            if (dataArray[i].id) {
             dataArray[i].idk = dataArray[i].id;
            }
         }
        var obj = {}
        obj.idk = 0;
        obj.desc = "";
        dataArray.unshift(obj);


        relationshipArrayData = dataArray;

        setDropDownForSelection(relationshipArrayData, "txtRelationship", onRelationshipChange, ["desc", "idk"], 0, "");
    }
}

function onGenderChange(){

}

function onChangeMaritalStatus(){

}

function onChangeReligion(){

}

function onRaceChange(){

}

function onEthnicityChange(){

}

function onPrefixChange(){

}

function onLanChange1(){

}

function onLanChange2(){
    
}

function onLanChange3(){
    
}

function onLanChange4(){
    
}

function onLanChange5(){
    
}

function onLanChange6(){
    
}

function onSMSChange(){

}

function onSMSSecondary(){

}


function onError(){

}

function onCommunicationTypeChange(){

}

function onRelationshipChange(){

}

function onContactSMSPrimary(){

}

function onContactSMSSecondary(){

}

function buttonEvents() {
}

function setDropDownForSelection(data, container, changeMethod, namesArr, selIndex, optionLabelValue) {
    var selList = [];
    if ($.isArray(data) == false) {
        var obj = {};
        obj[namesArr[0]] = data[namesArr[0]];
        obj[namesArr[1]] = data[namesArr[1]];
        selList.push(obj);

    } else {
        selList = data;
    }

   if(selList !== null && selList.length){
       for(var c = 0 ; c < selList.length ; c++){
           $('#'+container).append('<option value="'+selList[c].idk+'">'+selList[c].desc+'</option>');
       }
   }
  
   
}


function getTableListArray(dataObj) {
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.codeTable) {
        if ($.isArray(dataObj.response.codeTable)) {
            dataArray = dataObj.response.codeTable;
        } else {
            dataArray.push(dataObj.response.codeTable);
        }
    }
    var tempDataArry = [];
    var obj = {};
    obj.desc = "";
    obj.zip = "";
    obj.value = "";
    obj.idk = "";
    tempDataArry.push(obj);
    for (var i = 0; i < dataArray.length; i++) {
        if (dataArray[i].isActive == 1) {
            var obj = dataArray[i];
            obj.idk = dataArray[i].id;
            obj.status = dataArray[i].Status;
            tempDataArry.push(obj);
        }
    }
    return tempDataArry;
}
﻿$('body').click(function(evt){
    //debugger;
    //     if(!$(evt.target).is('.inputBlock, .form-input, .form-label')) {
    //     if ( $('.form-group .form-input').val() == "") {
    //         $('.filled').removeClass('filled');
    //         $('.form-group').removeClass('focused');  
    //     } 
    //     else if(!$('select').val() == ''){
    //        $('.filled').removeClass('filled');
    //         $('.form-group').removeClass('focused');  
    //     }
    // }
});

$(function () {
   
      $('.form-elements-content-inner .form-group .form-input, .profileDetailsFormElements .form-group .form-input').focus(function(){
          $(this).parents('.form-group').addClass('focused');
     });

     $('.form-elements-content-inner .form-group .form-input').blur(function(){
         
         var inputValue = $(this).val();
         if ( inputValue == "") {
             $(this).removeClass('filled');
             $(this).parents('.form-group').removeClass('focused');  
         } 
         else if(!$('select').val() == ''){
            $(this).removeClass('filled');
            $(this).parents('.form-group').removeClass('focused');
         }
         else {
            $(this).prev().focus();
            $(this).parents('.form-group').addClass('focused');
             $(this).addClass('filled');
         }

     });

     // Profile details form element function of list side
     $('.profileDetailsFormElements .form-group .form-input').blur(function(){
         
        var inputValue = $(this).val();
        if ( inputValue == "") {
            $(this).removeClass('filled');
            $(this).parents('.form-group').removeClass('focused');  
        } 
        else if(!$('select').val() == ''){
           $(this).removeClass('filled');
           $(this).parents('.form-group').removeClass('focused');
        }
        else {
           $(this).parents('.form-group').addClass('focused');
            $(this).addClass('filled');
        }

    });

    $('.profileDetailsFormElements .form-group .form-input').blur(function(){
        var inputValue = $(this).val();
        if(inputValue !== undefined && inputValue !== null && inputValue !== ''){
            $(this).prev().html(inputValue);
        }
        
        $(this).removeClass('filled');
        $(this).parents('.form-group').removeClass('focused');  
        console.log(inputValue);
    });
    // Profile details form element function of list side ends
	
});

function fncInputAni(getCurrent){
    $('.clickedOnFormelement').removeClass('clickedOnFormelement');  
    var current = $(getCurrent).addClass('clickedOnFormelement');

    var inputValue = $('.clickedOnFormelement ').next().val();
    var selectValue = $('.clickedOnFormelement').next().find('select').val();


    if ('.clickedOnFormelement') {
        $(this).removeClass('filled');
        $(this).parents('.form-group').removeClass('focused');  
    } 
    else if(!$('select').val() == ''){
       $(this).removeClass('filled');
       $(this).parents('.form-group').removeClass('focused');
    }

    $(getCurrent).parents('.form-group').addClass('focused');
    $(getCurrent).next('.form-input').focus();

    
}


function fncTabs(id, getCurrent) {
    var getDisplay = $('#' + id).css('display');

    if (getDisplay == 'block') {
       // $('#' + id).slideUp();
        //$(getCurrent).removeClass("linkTab-active")
    }
    else {
        $('.tabContentDiv').hide();
        $('.linkTab-active').removeClass('linkTab-active')
        $('#' + id).slideDown();
        $(getCurrent).addClass("linkTab-active");
        $('.tabInnerBlock').hide();
        $('.tabContentRightInner .tabInnerBlock:first-child').show();
        $('.tabSublink-active').removeClass('tabSublink-active');
        $('.tabSublinks li:first-child a').addClass("tabSublink-active");
    }
}

function fncSubTabs(id, getCurrent) {
    var getDisplay = $('#' + id).css('display');
    if (getDisplay == 'block') {
        //$('#' + id).slideUp();
        //$(getCurrent).removeClass("tabSublink-active")
    }
    else {
        $('.tabInnerBlock').hide();
        $('.tabSublink-active').removeClass('tabSublink-active')
        $('#' + id).show();
        $(getCurrent).addClass("tabSublink-active")
    }
}

function fncContactAdd(){
    $('#tabRightDiv').hide();
    $('#contactAddForm').show();
}



function openPopup(id) {
    $('#' + id).show();
}
function closePopup(id) {
    $('#' + id).hide();
}

function fncPhotoassets(){
    $('.photoAssets').show();
}

function fncAddTable(id, showId){
    $('#'+id).hide();
    $('#'+showId).show();
}
function fncAddTableCancel(id, showId){
    $('#'+id).show();
    $('#'+showId).hide();
}
var angularUIgridWrapper;
var angularUIgridWrapperroom;
var angularUIgridWrapper_resisdentterms;
var angularUIgridWrapper_hospitals;
var angularUIgridWrapper_funding;
var angularUIgridWrapper_remittance;
var angularUIgridWrapper_purchase;
var angularUIgridWrapper_resisdent;
var angularUIgridWrapper_bed;
var angularUIgridWrapper_bedAllocation;
var angularUIgridWrapper_remittanceList;
var angularUIgridWrapper_purchaseorder;
var angularUIgridWrapper_dols;
var parentRef = null;
var normUIgrid;
var tradeNameUIgrid;
var windowLoad = false;
var selectedItems;
var activeListFlag = true;
var fetchTradeDetails = false;
var operation = "GET";
var typeArr = [{ Key: '', Value: 'Residential Care' }, { Key: '', Value: 'Home Care' }];
var statusArr = [{ Key: 'Active', Value: 'Active' }, { Key: 'InActive', Value: 'InActive' }];
var careHome, bedRooms, fundings, fncBeds, atid, nameglobal, resisdent_terms, resisdents, remittances, hospitals, remittance_status, puchaseOrder_status;
var bedAllocationResidentId, beds, purchaseOrderResidentId, remittanceResidentId, dolsResidentId;


var iframeCouncil = $("[name='iframe']").contents();
function bindEvents() {
    sessionStorage.setItem("nameglobal", "");
    $("#btnMaster").css("display","");
    var parentWindow = window.parent.document;
    $('.sidebar-nav-link').on('click', function () {
        $('.sidebar-info-content').show();
        $(this).closest('li').addClass('activeList').siblings().removeClass('activeList');
        $('.medicationForm-form')[0].reset();
        $('.hasError').hide();
        $('.alert').remove();
        $('#carehome-save').show();
        $('#carehome-reset').show();
        $('#carehome-update').hide();
        $('#carehome-cancel').hide();
        $('#navBar').show();


        // $('.hasError-rxNorm').remove();
        // $('.sidebar-info-content').show();
        // $('[data-attr-content]').hide();
        // $('.contentWrapper').hide();
        // $('.sidebar-nav li').removeClass('activeList');
        // $('#navBar').show();
        // $('#hTitle').hide();
        // $('[data-medicate-content="1"]').hide();
        // $('[data-medicate-content="2"]').show();


        $('.tableListbtn-wrapper').find('.carehome-btn-link').attr('disabled', 'disabled');
        $('.medicationForm-form').find('option').each(function () {
            if ($(this).attr('data-medicate-status') == 1) {
                $(this).attr('selected', 'selected');
            }
            else {
                $(this).removeAttr('selected');
            }
        });
        $('.hasError-rxNorm').remove();
        // $('[data-attr-content]').hide();
        // $('.contentWrapper').hide();
        $(parentWindow).find('[data-link-name]').removeClass('activeList');
        if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name').toLowerCase() == "dashboard") {
            $('[data-attr-content="dashboard"]').show();
            $('#carehomeList-dashboard').show();
            $(parentWindow).find('[data-link-name="dashboard"]').addClass('activeList');
			/*if($('#carehome-list-search').closest('li').hasClass('active-tabList')) {
			 $('#carehome-list-search').trigger('click');
			 }*/
			/*if(windowLoad && $('.tab-list.active-tabList').attr('data-medicate-list') == "2") {
			 //rxNormDataOnLoad();
			 }*/
        }
        else if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name').toLowerCase() == "carehomemaster") {
            $('[data-attr-content="CareHomeMaster"]').show();
            $('#carehomeList-CareHomeMaster').show();
            $(parentWindow).find('[data-link-name="CareHomeMaster"]').closest('li').addClass('activeList');
            $('#hTitle').css("display","none");
            sessionStorage.setItem("nameglobal", "carehomemaster");
        }
        else if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name').toLowerCase() == "residents") {
            $('[data-attr-content="residents"]').show();
            $(parentWindow).find('[data-link-name="residents"]').addClass('activeList');
            $('#carehomeList-residents').show();
			/*if($('#carehome-list-search').closest('li').hasClass('active-tabList')) {
			 $('#carehome-list-search').trigger('click');
			 $('#carehomeList-residents').show();
			 }*/
			/*if(windowLoad && $('.tab-list.active-tabList').attr('data-medicate-list') == "2") {
			 //rxNormDataOnLoad();
			 }*/
        }
        else if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name').toLowerCase() == "bedallocation") {
            // $('[data-attr-content="beds"]').show();
            $(parentWindow).find('[data-link-name="BedAllocation"]').addClass('activeList');

        }
        // else if($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name').toLowerCase() == "remittance") {
        // 	$('[data-attr-content="remittance"]').show();
        // 	$(parentWindow).find('[data-link-name="remittance"]').addClass('activeList');
        // 	$('#carehomeList-remittance').show();
        // 	/*if($('#carehome-list-search').closest('li').hasClass('active-tabList')) {
        // 		$('#carehome-list-search').trigger('click');
        // 		$('#carehomeList-remittance').show();
        // 	}*/
        // 	/*if(windowLoad && $('.tab-list.active-tabList').attr('data-medicate-list') == "2") {
        // 		//rxNormDataOnLoad();
        // 	}*/
        // }
        else if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name').toLowerCase() == "po") {
            $('[data-attr-content="po"]').show();
            $(parentWindow).find('[data-link-name="po"]').addClass('activeList');
            $('#carehomeList-po').show();
			/*if($('#carehome-list-search').closest('li').hasClass('active-tabList')) {
			 $('#carehome-list-search').trigger('click');
			 $('#carehomeList-po').show();
			 }*/
			/*if(windowLoad && $('.tab-list.active-tabList').attr('data-medicate-list') == "2") {
			 //rxNormDataOnLoad();
			 }*/
        }
        else if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name').toLowerCase() == "dols") {
            $('[data-attr-content="dols"]').show();
            $(parentWindow).find('[data-link-name="dols"]').addClass('activeList');
            $('#carehomeList-dols').show();
        }
        else if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name').toLowerCase() == "reports") {
            $('[data-attr-content="reports"]').show();
            $(parentWindow).find('[data-link-name="reports"]').addClass('activeList');
            $('#carehomeList-reports').show();
        }

    });

    $('#carehome-add').on('click', function (e) {
        e.preventDefault();
        $('[data-medicate-content]').hide();
        $('[data-medicate-content="1"]').show();
        reset();
        fncgetroom();
        onSelectCareHomeName();
        operation = "POST";
    });
    $('#carehome-edit').on('click', function (e) {
        e.preventDefault();
        // fncgetroom();
        onClickEdit();
        operation = "PUT";
    });

    $("#btnBrowse").off("click", onClickBrowse);
    $("#btnBrowse").on("click", onClickBrowse);

    $("#btnBrowse").off("mouseover", onClickBrowseOver);
    $("#btnBrowse").on("mouseover", onClickBrowseOver);

    $("#btnBrowse").off("mouseout", onClickBrowseOut);
    $("#btnBrowse").on("mouseout", onClickBrowseOut);

    $("#fileElem").off("change", onSelectionFiles);
    $("#fileElem").on("change", onSelectionFiles);

    $("#btnResidentZipSearch").off("click");
    $("#btnResidentZipSearch").on("click", onClickZipSearch);

    $("#btnResidentSearch").off("click");
    $("#btnResidentSearch").on("click", onClickResidentSearch);

    $("#btnDolsResidentSearch").off("click");
    $("#btnDolsResidentSearch").on("click", onClickResidentSearch);

    $("#btnRemitanceResidentSearch").off("click");
    $("#btnRemitanceResidentSearch").on("click", onClickResidentSearch);

    $("#btnPurchaseOrderResidentSearch").off("click");
    $("#btnPurchaseOrderResidentSearch").on("click", onClickResidentSearch);

    $("#btnCareHomeZipSearch").off("click");
    $("#btnCareHomeZipSearch").on("click", onClickZipSearch);



    var nameglobal = sessionStorage.getItem("nameglobal");

    $('#addCareHome').on('click', function (e) {
        e.preventDefault();
        var iframeCouncil = $("[name='iframe']").contents();
        iframeCouncil.find('[data-attr-content]').hide();
        iframeCouncil.find('.contentWrapper').hide();
        iframeCouncil.find('#carehomeList-CareHomeMaster').show();
        iframeCouncil.find('[data-attr-content="CareHomeMaster"]').show();
        iframeCouncil.find('#navBar').show();
        iframeCouncil.find('.sidebar-nav li').removeClass('activeList');
        iframeCouncil.find('[data-medicate-content="1"]').hide();
        iframeCouncil.find('[data-medicate-content="2"]').show();
        collapseMasterMenu();
        sessionStorage.setItem("nameglobal", "carehomemaster");
    });

    $('#addHospital').on('click', function (e) {
        e.preventDefault();

        var iframeCouncil = $("[name='iframe']").contents();
        iframeCouncil.find('[data-attr-content]').hide();
        iframeCouncil.find('.contentWrapper').hide();
        iframeCouncil.find('[data-attr-content="RoomMaster"]').show();
        iframeCouncil.find('#carehomeList-hospitals').show();
        iframeCouncil.find('#lblName').html("Hospital Name");
        iframeCouncil.find('#hTitle').show();
        iframeCouncil.find('#hTitle').html("Masters :: Hospitals");
        $(".mainNav-link").removeClass('activeList');
        sessionStorage.setItem("nameglobal", "hospitals");
        iframeCouncil.find('#navBar').show();
        iframeCouncil.find('[data-medicate-content="1"]').hide();
        iframeCouncil.find('[data-medicate-content="2"]').show();
        collapseMasterMenu()
        reset();
    });


    $("#facilityFilter-btn").off("click");
    $("#facilityFilter-btn").on("click", onClickFilterBtn);

    $("#DOLareHomeFilter-btn").off("click");
    $("#DOLareHomeFilter-btn").on("click", onClickFilterBtn);

    $("#BACareHomeFilter-btn").off("click");
    $("#BACareHomeFilter-btn").on("click", onClickFilterBtn);

    $("#RCareHomeFilter-btn").off("click");
    $("#RCareHomeFilter-btn").on("click", onClickFilterBtn);

    $("#POareHomeFilter-btn").off("click");
    $("#POareHomeFilter-btn").on("click", onClickFilterBtn);

    $("#ResisLareHomeFilter-btn").off("click");
    $("#ResisLareHomeFilter-btn").on("click", onClickFilterBtn);

    $("#BedCareHomeFilter-btn").off("click");
    $("#BedCareHomeFilter-btn").on("click", onClickFilterBtn);

    $("#RTCareHomeFilter-btn").off("click");
    $("#RTCareHomeFilter-btn").on("click", onClickFilterBtn);

    $("#HosCareHomeFilter-btn").off("click");
    $("#HosCareHomeFilter-btn").on("click", onClickFilterBtn);

    $("#FundCareHomeFilter-btn").off("click");
    $("#FundCareHomeFilter-btn").on("click", onClickFilterBtn);

    $("#RSHomeFilter-btn").off("click");
    $("#RSHomeFilter-btn").on("click", onClickFilterBtn);

    $("#POSHomeFilter-btn").off("click");
    $("#POSHomeFilter-btn").on("click", onClickFilterBtn);

    $('#addRooms').on('click', function (e) {
        e.preventDefault();
        var iframeCouncil = $("[name='iframe']").contents();
        iframeCouncil.find('[data-attr-content]').hide();
        iframeCouncil.find('.contentWrapper').hide();
        iframeCouncil.find('[data-attr-content="RoomMaster"]').show();
        iframeCouncil.find('#carehomeList-RoomMaster').show();
        iframeCouncil.find('#lblName').html("Room Name");
        iframeCouncil.find('#hTitle').show();
        iframeCouncil.find('#hTitle').html("Masters :: Rooms");
        $(".mainNav-link").removeClass('activeList');
        sessionStorage.setItem("nameglobal", "roommaster");
        reset();
        iframeCouncil.find('#navBar').show();
        iframeCouncil.find('[data-medicate-content="1"]').hide();
        iframeCouncil.find('[data-medicate-content="2"]').show();
        //buildRoomListGrid([]);
        collapseMasterMenu();
    });

    $('#addBed').on('click', function (e) {
        // e.preventDefault();
        collapseMasterMenu();
        var iframeCouncil = $("[name='iframe']").contents();
        iframeCouncil.find('[data-attr-content]').hide();
        // iframeCouncil.find('.contentWrapper').hide();
        iframeCouncil.find('[data-attr-content="BedMaster"]').show();
        iframeCouncil.find('#carehomeList-bed').show();
        iframeCouncil.find('#lblName').html("Bed Name");
        iframeCouncil.find('#hTitle').show();
        iframeCouncil.find('#hTitle').html("Masters :: Beds");
        $(".mainNav-link").removeClass('activeList');
        sessionStorage.setItem("nameglobal", "beds");
        iframeCouncil.find('#navBar').show();
       // buildRoomListGrid([]);

        iframeCouncil.find('[data-medicate-content="1"]').hide();
        iframeCouncil.find('[data-medicate-content="2"]').show();
        // $('[data-medicate-content]').hide();
        reset();
    });

    $('#addRT').on('click', function (e) {
        e.preventDefault();
        var iframeCouncil = $("[name='iframe']").contents();
        iframeCouncil.find('[data-attr-content]').hide();
        iframeCouncil.find('.contentWrapper').hide();
        iframeCouncil.find('[data-attr-content="RoomMaster"]').show();
        iframeCouncil.find('#carehomeList-ResidentTerms').show();
        iframeCouncil.find('#lblName').html("Resisdent Term Name");
        iframeCouncil.find('#hTitle').show();
        iframeCouncil.find('#hTitle').html("Masters :: Resisdent Terms");
        $(".mainNav-link").removeClass('activeList');
        sessionStorage.setItem("nameglobal", "residentterms");
        reset();
        iframeCouncil.find('#navBar').show();
        iframeCouncil.find('[data-medicate-content="1"]').hide();
        iframeCouncil.find('[data-medicate-content="2"]').show();
        collapseMasterMenu();
    });

    $('#addFundig').on('click', function (e) {
        e.preventDefault();
        var iframeCouncil = $("[name='iframe']").contents();
        iframeCouncil.find('[data-attr-content]').hide();
        iframeCouncil.find('.contentWrapper').hide();
        iframeCouncil.find('[data-attr-content="RoomMaster"]').show();
        iframeCouncil.find('#carehomeList-fundings').show();
        iframeCouncil.find('#lblName').html("Funding Name");
        iframeCouncil.find('#hTitle').show();
        iframeCouncil.find('#hTitle').html("Masters :: Funding");
        $(".mainNav-link").removeClass('activeList');
        sessionStorage.setItem("nameglobal", "fundings");
        reset();
        iframeCouncil.find('#navBar').show();
        iframeCouncil.find('[data-medicate-content="1"]').hide();
        iframeCouncil.find('[data-medicate-content="2"]').show();
        collapseMasterMenu();
    });

    $('#addRStatus').on('click', function (e) {
        e.preventDefault();
        var iframeCouncil = $("[name='iframe']").contents();
        iframeCouncil.find('.contentWrapper').hide();
        iframeCouncil.find('[data-attr-content]').hide();
        iframeCouncil.find('[data-attr-content="RoomMaster"]').show();
        iframeCouncil.find('#carehomeList-RemittanceList').show();
        iframeCouncil.find('#lblName').html("Remittance Status Name");
        iframeCouncil.find('#hTitle').show();
        iframeCouncil.find('#hTitle').html("Masters :: Remittance Status");
        $(".mainNav-link").removeClass('activeList');
        sessionStorage.setItem("nameglobal", "remittance_status");
        reset();
        iframeCouncil.find('#navBar').show();
        iframeCouncil.find('[data-medicate-content="1"]').hide();
        iframeCouncil.find('[data-medicate-content="2"]').show();
        collapseMasterMenu();
    });

    $('#addPOStatus').on('click', function (e) {
        e.preventDefault();
        var iframeCouncil = $("[name='iframe']").contents();
        iframeCouncil.find('.contentWrapper').hide();
        iframeCouncil.find('[data-attr-content]').hide();
        iframeCouncil.find('[data-attr-content="RoomMaster"]').show();
        iframeCouncil.find('#carehomeList-Purchase').show();
        iframeCouncil.find('#lblName').html("PO Status Name");
        iframeCouncil.find('#hTitle').show();
        iframeCouncil.find('#hTitle').html("Masters :: Purchase Order Status");
        $(".mainNav-link").removeClass('activeList');
        sessionStorage.setItem("nameglobal", "purchase_order_status");
        reset();
        iframeCouncil.find('#navBar').show();
        iframeCouncil.find('[data-medicate-content="1"]').hide();
        iframeCouncil.find('[data-medicate-content="2"]').show();
        collapseMasterMenu();
    });

    // $('#addCommunication').on('click', function (e) {
    //     e.preventDefault();
    //     collapseMasterMenu();
    //     $("#ptframe").attr('src', "../../html/reports/reportDesigner.html");
    // });


    $('#addResisdents').on('click', function (e) {
        e.preventDefault();
        var iframeCouncil = $("[name='iframe']").contents();
        iframeCouncil.find('[data-attr-content]').hide();
        iframeCouncil.find('.contentWrapper').hide();
        iframeCouncil.find('[data-attr-content="residents-master"]').show();
        iframeCouncil.find('#carehomeList-Resisdent').show();
        iframeCouncil.find('.mainNav-list li').removeClass('activeList');
        iframeCouncil.find('#hTitle').show();
        sessionStorage.setItem("nameglobal", "resisdentsmaster");
        reset();
        iframeCouncil.find('#navBar').show();
        iframeCouncil.find('[data-medicate-content="1"]').hide();
        iframeCouncil.find('[data-medicate-content="2"]').show();
        collapseMasterMenu();
    });

    $('#addBedAllocation').on('click', function (e) {
        e.preventDefault();
        var iframeCouncil = $("[name='iframe']").contents();
        iframeCouncil.find('.contentWrapper').hide();
        iframeCouncil.find('[data-attr-content]').hide();
        iframeCouncil.find('[data-attr-content="BedAllocation"]').show();
        iframeCouncil.find('#carehomeList-BedAllocaion').show();
        iframeCouncil.find('.sidebar-nav li').removeClass('activeList');
        sessionStorage.setItem("nameglobal", "bed_allocation");
        iframeCouncil.find('#hTitle').show();
        reset();
        iframeCouncil.find('#navBar').show();
        iframeCouncil.find('[data-medicate-content="1"]').hide();
        iframeCouncil.find('[data-medicate-content="2"]').show();
        collapseMasterMenu();
    });

    $('#addPO').on('click', function (e) {
        e.preventDefault();

        var iframeCouncil = $("[name='iframe']").contents();
        iframeCouncil.find('.contentWrapper').hide();
        iframeCouncil.find('[data-attr-content]').hide();
        iframeCouncil.find('[data-attr-content="purchase_Orders"]').show();
        iframeCouncil.find('#carehomeList-CareHomePurchaseOrder').show();
        iframeCouncil.find('#hTitle').show();
        sessionStorage.setItem("nameglobal", "purchase_order");
        reset();
        iframeCouncil.find('#navBar').show();
        iframeCouncil.find('[data-medicate-content="1"]').hide();
        iframeCouncil.find('[data-medicate-content="2"]').show();
        collapseMasterMenu();
    });

    $('#addRemittance').on('click', function (e) {
        e.preventDefault();
        var iframeCouncil = $("[name='iframe']").contents();
        iframeCouncil.find('.contentWrapper').hide();
        iframeCouncil.find('[data-attr-content]').hide();
        iframeCouncil.find('[data-attr-content="remittances"]').show();
        iframeCouncil.find('#carehomeList-CareHomeRemittance').show();
        iframeCouncil.find('#hTitle').show();
        sessionStorage.setItem("nameglobal", "remittances");
        reset();
        iframeCouncil.find('#navBar').show();
        iframeCouncil.find('[data-medicate-content="1"]').hide();
        iframeCouncil.find('[data-medicate-content="2"]').show();
        collapseMasterMenu();
    });

    $('#addDOL').on('click', function (e) {
        e.preventDefault();
        var iframeCouncil = $("[name='iframe']").contents();
        iframeCouncil.find('.contentWrapper').hide();
        iframeCouncil.find('[data-attr-content]').hide();
        iframeCouncil.find('[data-attr-content="Dols"]').show();
        iframeCouncil.find('#carehomeList-CareHomeDols').show();
        iframeCouncil.find('#hTitle').show();
        sessionStorage.setItem("nameglobal", "dols");
        reset();
        iframeCouncil.find('#navBar').show();
        iframeCouncil.find('[data-medicate-content="1"]').hide();
        iframeCouncil.find('[data-medicate-content="2"]').show();
        collapseMasterMenu();
    });

    $('#carehome-backToList').on('click', function (e) {
        e.preventDefault();
        $('[data-medicate-content]').hide();
        $('[data-medicate-content="2"]').show();
    });
    $('#carehome-save').on('click', function (e) {
        e.preventDefault();
        fncgetdetails();
        searchOnLoad('active');
        reset();
        ///saveCareHome();
    });
    $('#carehome-delete').on('click', function (e) {
        e.preventDefault();
        deleteCareHome();
        searchOnLoad('active');
    });
    $('#carehome-reset').on('click', function (e) {
        e.preventDefault();
        $('[data-medicate-content]').hide();
        $('[data-medicate-content="1"]').show();

        reset();

    });

    $(".btnActive").on("click", function (e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('active');
        onClickActive();
    });
    $(".btnInActive").on("click", function (e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('inactive');
        onClickInActive();
    });
    $('.mainNav-link').on('click', function () {
        collapseMasterMenu();
        $(this).addClass('activeList').closest('li').siblings().find(".mainNav-link").removeClass('activeList');
        $('.hasError-rxNorm').remove();
        var iframeCouncil = $("[name='iframe']").contents();
        iframeCouncil.find('.sidebar-info-content').show();
        iframeCouncil.find('[data-attr-content]').hide();
        iframeCouncil.find('.contentWrapper').hide();
        iframeCouncil.find('.sidebar-nav li').removeClass('activeList');
        iframeCouncil.find('#navBar').show();
        iframeCouncil.find('#hTitle').hide();
        iframeCouncil.find('[data-medicate-content="1"]').hide();
        iframeCouncil.find('[data-medicate-content="2"]').show();

        if ($('.mainNav-list').find('.mainNav-link.activeList').attr('data-link-name')) {

            if ($('.mainNav-list').find('.mainNav-link.activeList').attr('data-link-name').toLowerCase() == "dashboard") {
                //iframeCouncil.find('.sidebar-info-content').hide();
                iframeCouncil.find('[data-attr-content="dashboard"]').show();
                iframeCouncil.find('#carehomeList-dashboard').show();
                iframeCouncil.find('[data-link-name="dashboard"]').closest('li').addClass('activeList');
            }
            else if ($('.mainNav-list').find('.mainNav-link.activeList').attr('data-link-name').toLowerCase() == "care") {
                iframeCouncil.find('[data-attr-content="CareHomeMaster"]').show();
                iframeCouncil.find('#carehomeList-CareHomeMaster').show();
                iframeCouncil.find('[data-link-name="CareHomeMaster"]').closest('li').addClass('activeList');
                /*var dataOptions = {
                 pagination: false,
                 changeCallBack: onChange
                 }
                 angularUIgridWrapper = new AngularUIGridWrapper("dgridActivityList", dataOptions);
                 angularUIgridWrapper.init();
                 buildDeviceListGrid([]);*/
            }
            else if ($('.mainNav-list').find('.mainNav-link.activeList').attr('data-link-name').toLowerCase() == "residents") {
                iframeCouncil.find('[data-attr-content="residents"]').show();
                iframeCouncil.find('#carehomeList-residents').show();
                iframeCouncil.find('[data-link-name="residents"]').closest('li').addClass('activeList');
            }
            else if ($('.mainNav-list').find('.mainNav-link.activeList').attr('data-link-name').toLowerCase() == "beds") {
                iframeCouncil.find('[data-attr-content="beds"]').show();
                iframeCouncil.find('#carehomeList-beds').show();
                iframeCouncil.find('[data-link-name="beds"]').closest('li').addClass('activeList');
            }
            // else if($('.mainNav-list').find('.mainNav-link.activeList').attr('data-link-name').toLowerCase() == "remittance") {
            // 	iframeCouncil.find('[data-attr-content="remittance"]').show();
            // 	iframeCouncil.find('#carehomeList-remittance').show();
            // 	iframeCouncil.find('[data-link-name="remittance"]').closest('li').addClass('activeList');
            // }
            else if ($('.mainNav-list').find('.mainNav-link.activeList').attr('data-link-name').toLowerCase() == "po") {
                iframeCouncil.find('[data-attr-content="po"]').show();
                iframeCouncil.find('#carehomeList-po').show();
                iframeCouncil.find('[data-link-name="po"]').closest('li').addClass('activeList');
            }
            // else if($('.mainNav-list').find('.mainNav-link.activeList').attr('data-link-name').toLowerCase() == "dols") {
            // 	iframeCouncil.find('[data-attr-content="dols"]').show();
            // 	iframeCouncil.find('#carehomeList-dols').show();
            // 	iframeCouncil.find('[data-link-name="dols"]').closest('li').addClass('activeList');
            // }
            else if ($('.mainNav-list').find('.mainNav-link.activeList').attr('data-link-name').toLowerCase() == "reports") {
                iframeCouncil.find('[data-attr-content="reports"]').show();
                iframeCouncil.find('#carehomeList-reports').show();
                iframeCouncil.find('[data-link-name="reports"]').closest('li').addClass('activeList');
            }
            else if ($('.mainNav-list').find('.mainNav-link.activeList').attr('data-link-name').toLowerCase() == "reports") {
                iframeCouncil.find('[data-attr-content="reports"]').show();
                iframeCouncil.find('#carehomeList-reports').show();
                iframeCouncil.find('[data-link-name="reports"]').closest('li').addClass('activeList');
            }
            else if ($('.mainNav-list').find('.mainNav-link.activeList').attr('data-link-name').toLowerCase() == "carehomemaster") {
                iframeCouncil.find('[data-attr-content="CareHomeMaster"]').show();
                iframeCouncil.find('#carehomeList-CareHomeMaster').show();
                iframeCouncil.find('[data-link-name="CareHomeMaster"]').closest('li').addClass('activeList');
                iframeCouncil.find('#hTitle').css("display","none");
                sessionStorage.setItem("nameglobal", "carehomemaster");
            }
            else if ($('.mainNav-list').find('.mainNav-link.activeList').attr('data-link-name').toLowerCase() == "roomsmaster") {
                iframeCouncil.find('[data-attr-content="RoomMaster"]').show();
                iframeCouncil.find('#carehomeList-RoomMaster').show();
                sessionStorage.setItem("nameglobal", "roommaster");
                reset();
            }
            else if ($('.mainNav-list').find('.mainNav-link.activeList').attr('data-link-name').toLowerCase() == "residentterms") {
                iframeCouncil.find('[data-attr-content="RoomMaster"]').show();
                iframeCouncil.find('#carehomeList-ResidentTerms').show();
                sessionStorage.setItem("nameglobal", "residentterms");
                reset();
            }
            else if ($('.mainNav-list').find('.mainNav-link.activeList').attr('data-link-name').toLowerCase() == "fundings") {
                iframeCouncil.find('[data-attr-content="RoomMaster"]').show();
                iframeCouncil.find('#carehomeList-fundings').show();
                sessionStorage.setItem("nameglobal", "fundings");
                reset();
            }
            else if ($('.mainNav-list').find('.mainNav-link.activeList').attr('data-link-name').toLowerCase() == "remittance_status") {
                iframeCouncil.find('[data-attr-content="RoomMaster"]').show();
                iframeCouncil.find('#carehomeList-RemittanceList').show();
                sessionStorage.setItem("nameglobal", "remittance_status");
                reset();
            }
            else if ($('.mainNav-list').find('.mainNav-link.activeList').attr('data-link-name').toLowerCase() == "purchase_order_status") {
                iframeCouncil.find('[data-attr-content="RoomMaster"]').show();
                iframeCouncil.find('#carehomeList-Purchase').show();
                sessionStorage.setItem("nameglobal", "purchase_order_status");
                reset();
            }
            else if ($('.mainNav-list').find('.mainNav-link.activeList').attr('data-link-name').toLowerCase() == "hospitals") {
                iframeCouncil.find('[data-attr-content="RoomMaster"]').show();
                iframeCouncil.find('#carehomeList-hospitals').show();
                sessionStorage.setItem("nameglobal", "hospitals");
                reset();
            }
            else if ($('.mainNav-list').find('.mainNav-link.activeList').attr('data-link-name').toLowerCase() == "resisdentsmaster") {
                iframeCouncil.find('[data-attr-content="residents-master"]').show();
                iframeCouncil.find('#carehomeList-Resisdent').show();
                iframeCouncil.find('#hTitle').css("display","none");
                iframeCouncil.find('[data-link-name="residents"]').closest('li').addClass('activeList');
                sessionStorage.setItem("nameglobal", "resisdentsmaster");
                reset();
            }
            else if ($('.mainNav-list').find('.mainNav-link.activeList').attr('data-link-name').toLowerCase() == "bedlist") {
                iframeCouncil.find('[data-attr-content="BedMaster"]').show();
                iframeCouncil.find('#carehomeList-bed').show();
                sessionStorage.setItem("nameglobal", "beds");
                reset();
            }

            else if ($('.mainNav-list').find('.mainNav-link.activeList').attr('data-link-name').toLowerCase() == "bed_allocation") {
                iframeCouncil.find('[data-attr-content="BedAllocation"]').show();
                iframeCouncil.find('#carehomeList-BedAllocaion').show();
                iframeCouncil.find('#hTitle').css("display","none");
                sessionStorage.setItem("nameglobal", "bed_allocation");
                iframeCouncil.find('[data-link-name="BedAllocation"]').closest('li').addClass('activeList');
                reset();
            }

            else if ($('.mainNav-list').find('.mainNav-link.activeList').attr('data-link-name').toLowerCase() == "remittance") {
                iframeCouncil.find('[data-attr-content="remittances"]').show();
                iframeCouncil.find('#carehomeList-CareHomeRemittance').show();
                iframeCouncil.find('#hTitle').css("display","none");
                sessionStorage.setItem("nameglobal", "remittances");
                iframeCouncil.find('[data-link-name="remittance"]').closest('li').addClass('activeList');
                reset();
            }

            else if ($('.mainNav-list').find('.mainNav-link.activeList').attr('data-link-name').toLowerCase() == "purchase_order") {
                iframeCouncil.find('[data-attr-content="purchase_Orders"]').show();
                iframeCouncil.find('#carehomeList-CareHomePurchaseOrder').show();
                iframeCouncil.find('#hTitle').css("display","none");
                iframeCouncil.find('[data-link-name="po"]').closest('li').addClass('activeList');
                sessionStorage.setItem("nameglobal", "purchase_order");
                reset();
            }
            else if ($('.mainNav-list').find('.mainNav-link.activeList').attr('data-link-name').toLowerCase() == "dols") {
                iframeCouncil.find('[data-attr-content="Dols"]').show();
                iframeCouncil.find('#carehomeList-CareHomeDols').show();
                iframeCouncil.find('#hTitle').css("display","none");
                iframeCouncil.find('[data-link-name="dols"]').closest('li').addClass('activeList');
                sessionStorage.setItem("nameglobal", "dols");
                reset();
            }
        }
    });

    $('#masterMenu').on('click', function (e) {
        e.preventDefault();
        var iframeCouncil = $("[name='iframe']").contents();
        iframeCouncil.find('#navBar').hide();
        iframeCouncil.find('[data-medicate-content="1"]').hide();
        iframeCouncil.find('[data-medicate-content="2"]').show();
        reset();
    });
}

    function resetDates() {
        $('[name="residents-birthdate"]').kendoDatePicker();
        $('[name="residents-termfrom"]').kendoDatePicker();
        $('[name="residents-termto"]').kendoDatePicker();
        $('[name="residents-dateoccupied"]').kendoDatePicker();
        $('[name="beds-birthdate"]').kendoDatePicker();
        $('[name="beds-termfrom"]').kendoDatePicker();
        $('[name="beds-termto"]').kendoDatePicker();
        $('[name="beds-dateoccupied"]').kendoDatePicker();
        $('[name="beds-datevacant"]').kendoDatePicker();
        $('[name="remittance-fromdate"]').kendoDatePicker();
        $('[name="remittance-todate"]').kendoDatePicker();
        $('[name="po-residentdob"]').kendoDatePicker();
        $('[name="po-termfrom"]').kendoDatePicker();
        $('[name="po-termto"]').kendoDatePicker();
        $('[name="dols-dategenerate"]').kendoDatePicker();
        $('[name="dols-dateexpire"]').kendoDatePicker();
        $('[name="dols-dateexpire"]').kendoDatePicker();
        // $("#resisdentBirthDate").kendoDatePicker();

        // var cntry = sessionStorage.countryName;
        // if (cntry.indexOf("India") >= 0) {
        //     $("#resisdentBirthDate").datepicker({
        //         changeMonth: true,
        //         changeYear: true,
        //         dateFormat: 'dd/mm/yy',
        //         yearRange: "-200:+200",
        //         maxDate: '0'
        //     });
        // } else if (cntry.indexOf("United Kingdom") >= 0) {
        //     $("#resisdentBirthDate").datepicker({
        //         changeMonth: true,
        //         changeYear: true,
        //         dateFormat: 'dd/mm/yy',
        //         yearRange: "-200:+200",
        //         maxDate: '0'
        //     });
        // } else {
        //     $("#resisdentBirthDate").datepicker({
        //         changeMonth: true,
        //         changeYear: true,
        //         dateFormat: 'mm/dd/yy',
        //         yearRange: "-200:+200",
        //         maxDate: '0'
        //     });
        // }

    }

    function init() {
        bindEvents();
        //addMedicationData();
        $("#cmbType").kendoComboBox();
        $("#cmbcarehome").kendoComboBox();
        // $("#cmbBedRate").kendoComboBox();
        $("#resisdentCareHomeId").kendoComboBox();
        $("#cmbStatus").kendoComboBox();
        $("#residents-status").kendoComboBox();
        $("#cmbGender").kendoComboBox();
        $("#resisdentGender").kendoComboBox();
        $("#cmbrtCareHome").kendoComboBox();
        $("#roomBirthDate").kendoDatePicker();
        $("#cmbGroup").kendoComboBox();
        $("#cmbBedStatus").kendoComboBox();
        $("#cmbRemittanceStatus").kendoComboBox();
        $("#cmbPoStatus").kendoComboBox();
        $("#cmbDolsStatus").kendoComboBox();
        $("#bedCareHome").kendoComboBox();
        $("#cmbBedListStatus").kendoComboBox();
        $("#cmbBedRoom").kendoComboBox();
        $("#cmbFundingId").kendoComboBox();
        $("#cmbBedId").kendoComboBox();
        $("#cmbCareHomeId").kendoComboBox();
        $("#cmdResisdentTermId").kendoComboBox();
        $("#cmbRemittanceStatusId").kendoComboBox();
        $("#cmbRemittanceCareHomeId").kendoComboBox();

        $("#cmbRemittancesStatus").kendoComboBox();
        $("#cmbPurchaseOrderStatusId").kendoComboBox();
        $("#cmbPurchaseCareHomeId").kendoComboBox();
        $("#cmbPurchaseOrderStatus").kendoComboBox();
        $("#cmbDolsCareHomeId").kendoComboBox();
        // $("#cmbDolsResidentId").kendoComboBox();
        // $("#txtDateGenerated").kendoDatePicker();
        // $("#txtDateExpired").kendoDatePicker();
        $("#cmbDOLStatus").kendoComboBox();
        $("#cmbHospitalId").kendoComboBox();
        $("#cmbCareHomeStatus").kendoComboBox();
        $("#cmbCareHomeName").kendoComboBox();
        $("#cmbRoomId").kendoComboBox();
        $("#cmdPurchaseResisdentTermId").kendoComboBox();
        // $("#cmdDOLResisdentDOB").kendoDatePicker();
        $("#cmdDOLResisdentGender").kendoComboBox();
        $("#cmbResisdentsCareHomeName").kendoComboBox();
        $("#cmbBACareHomeName").kendoComboBox();
        $("#cmbRCareHomeName").kendoComboBox();
        $("#cmbPOCareHomeName").kendoComboBox();
        $("#cmbDOLCareHomeName").kendoComboBox();
        $("#cmbHOSCareHomeName").kendoComboBox();
        $("#cmbBedsCareHomeName").kendoComboBox();
        $("#cmbRTCareHomeName").kendoComboBox();
        $("#cmbFundingCareHomeName").kendoComboBox();
        $("#cmbRSeHomeName").kendoComboBox();
        $("#cmbPOSeHomeName").kendoComboBox();
        $("#cmbSMS").kendoComboBox();
        allowNumerics("bedRate");
        allowNumerics("txtTopupRate");
        allowNumerics("txtRemittanceBandingRate");


        var cntry = sessionStorage.countryName;
        if (cntry.indexOf("India") >= 0) {
            $("#resisdentBirthDate").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd/mm/yy',
                yearRange: "-200:+200",
                maxDate: '0'
            });

            $("#txtFromDate").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd/mm/yy',
                yearRange: "-200:+200",
                maxDate: '0'
            });

            $("#txtToDate").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd/mm/yy',
                yearRange: "-200:+200"
            });
            $("#txtOccupiedDate").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd/mm/yy',
                yearRange: "-200:+200"
            });
            $("#txtVacantDate").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd/mm/yy',
                yearRange: "-200:+200"
            });
            $("#cmdResisdentTermFrom").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd/mm/yy',
                yearRange: "-200:+200",
                maxDate: '0'
            });
            $("#cmdResisdentTermTo").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd/mm/yy',
                yearRange: "-200:+200"
            });
            $("#txtRemittanceFromDate").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd/mm/yy',
                yearRange: "-200:+200",
                maxDate: '0'
            });
            $("#txtRemittanceToDate").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd/mm/yy',
                yearRange: "-200:+200"
            });
            $("#txtDateGenerated").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd/mm/yy',
                yearRange: "-200:+200"
            });
            $("#txtDateExpired").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd/mm/yy',
                yearRange: "-200:+200"
            });
        } else if (cntry.indexOf("United Kingdom") >= 0) {
            $("#resisdentBirthDate").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd/mm/yy',
                yearRange: "-200:+200",
                maxDate: '0'
            });

            $("#txtFromDate").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd/mm/yy',
                yearRange: "-200:+200",
                maxDate: '0'
            });
            $("#txtToDate").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd/mm/yy',
                yearRange: "-200:+200",
                maxDate: '0'
            });
            $("#txtOccupiedDate").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd/mm/yy',
                yearRange: "-200:+200",
                maxDate: '0'
            });
            $("#txtVacantDate").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd/mm/yy',
                yearRange: "-200:+200",
                maxDate: '0'
            });
            $("#cmdResisdentTermFrom").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd/mm/yy',
                yearRange: "-200:+200",
                maxDate: '0'
            });
            $("#cmdResisdentTermTo").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd/mm/yy',
                yearRange: "-200:+200"
            });
            $("#txtRemittanceFromDate").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd/mm/yy',
                yearRange: "-200:+200",
                maxDate: '0'
            });
            $("#txtRemittanceToDate").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd/mm/yy',
                yearRange: "-200:+200"
            });
            $("#txtDateGenerated").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd/mm/yy',
                yearRange: "-200:+200"
            });
            $("#txtDateExpired").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd/mm/yy',
                yearRange: "-200:+200"
            });
        } else {
            $("#resisdentBirthDate").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'mm/dd/yy',
                yearRange: "-200:+200",
                maxDate: '0'
            });

            $("#txtFromDate").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'mm/dd/yy',
                yearRange: "-200:+200",
                maxDate: '0'
            });
            $("#txtToDate").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'mm/dd/yy',
                yearRange: "-200:+200"
            });
            $("#txtOccupiedDate").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'mm/dd/yy',
                yearRange: "-200:+200"
            });
            $("#txtVacantDate").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'mm/dd/yy',
                yearRange: "-200:+200"
            });
            $("#cmdResisdentTermFrom").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'mm/dd/yy',
                yearRange: "-200:+200",
                maxDate: '0'
            });
            $("#cmdResisdentTermTo").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'mm/dd/yy',
                yearRange: "-200:+200"
            });
            $("#txtRemittanceFromDate").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'mm/dd/yy',
                yearRange: "-200:+200",
                maxDate: '0'
            });
            $("#txtRemittanceToDate").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'mm/dd/yy',
                yearRange: "-200:+200"
            });
            $("#txtDateGenerated").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'mm/dd/yy',
                yearRange: "-200:+200"
            });
            $("#txtDateExpired").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'mm/dd/yy',
                yearRange: "-200:+200"
            });
        }
        resetDates();
        $('.contentWrapper').hide();
    }

    function onTypeChange() {
        onComboChange("cmbType");
    }

	/*function addMedicationData() {
	 $('.sidebar-nav-list:first-child').find('.sidebar-nav-link').trigger('click');
	 }*/
    var dataOptions;
    var roomdataOptions;

    $(document).ready(function () {
        parentRef = parent.frames['iframe'].window;
        init();
        dataOptions = {
            pagination: false,
            changeCallBack: onChange
        }

        roomdataOptions = {
            pagination: false,
            changeCallBack: onRoomGridChange
        }

        var resisdentdataOptions = {
            pagination: false,
            changeCallBack: onResisdentGridChange
        }

        var fundingdataOptions = {
            pagination: false,
            changeCallBack: onFundingGridChange
        }

        var remittancedataOptions = {
            pagination: false,
            changeCallBack: onRemittanceGridChange
        }

        var purchansedataOptions = {
            pagination: false,
            changeCallBack: onPurchaseGridChange
        }

        var hospitaldataOptions = {
            pagination: false,
            changeCallBack: onHospitalGridChange
        }

        var resisdentListdataOptions = {
            pagination: false,
            changeCallBack: onResisdentListGridChange
        }


        var bedListdataOptions = {
            pagination: false,
            changeCallBack: onBedListGridChange
        }
        var bedAllocationdataOptions = {
            pagination: false,
            changeCallBack: onBedAllocationGridChange
        }
        var remittanceListdataOptions = {
            pagination: false,
            changeCallBack: onRemittanceListGridChange
        }
        var purchaseOrderdataOptions = {
            pagination: false,
            changeCallBack: onPurchaseOrdeGridChange
        }
        var dolsdataOptions = {
            pagination: false,
            changeCallBack: onDOLSGridChange
        }

        angularUIgridWrapperroom = new AngularUIGridWrapper("dgridRoomList", roomdataOptions);
        angularUIgridWrapperroom.init();
        buildRoomListGrid([]);
        //fncgetrooms();
        //getTableValueList('active');

        angularUIgridWrapper = new AngularUIGridWrapper("dgridCareHomeList", dataOptions);
        angularUIgridWrapper.init();
        buildDeviceListGrid([]);
        fncgetCareHome();

        angularUIgridWrapper_resisdentterms = new AngularUIGridWrapper("dgridResidentList", resisdentdataOptions);
        angularUIgridWrapper_resisdentterms.init();
        buildResisdentListGrid([]);
        //fncgetrt();

        angularUIgridWrapper_hospitals = new AngularUIGridWrapper("dgridHospitalList", hospitaldataOptions);
        angularUIgridWrapper_hospitals.init();
        buildHospitalListGrid([]);
        //fncgethospitals();

        angularUIgridWrapper_funding = new AngularUIGridWrapper("dgridFundList", fundingdataOptions);
        angularUIgridWrapper_funding.init();
        buildFundingListGrid([]);
        // fncgetfunding();

        angularUIgridWrapper_remittance = new AngularUIGridWrapper("dgridRemittanceList", remittancedataOptions);
        angularUIgridWrapper_remittance.init();
        buildRemittanceStatusListGrid([]);
        // fncgetRemittanceStatus();

        angularUIgridWrapper_purchase = new AngularUIGridWrapper("dgridPurchaseList", purchansedataOptions);
        angularUIgridWrapper_purchase.init();
        buildPurchaseListGrid([]);
        // fncgetPurchase();

        angularUIgridWrapper_resisdent = new AngularUIGridWrapper("dgridResisdent", resisdentListdataOptions);
        angularUIgridWrapper_resisdent.init();
        buildResisdentGrid([]);
        // fncGetResisdentList();

        angularUIgridWrapper_bed = new AngularUIGridWrapper("dgridBed", bedListdataOptions);
        angularUIgridWrapper_bed.init();
        buildBedListGrid([]);
        fncgetBeds();

        angularUIgridWrapper_bedAllocation = new AngularUIGridWrapper("dgridBedAllocationList", bedAllocationdataOptions);
        angularUIgridWrapper_bedAllocation.init();
        buildBedAllocationListGrid([]);
        // fncgetBedAllocations();

        angularUIgridWrapper_remittanceList = new AngularUIGridWrapper("dgridRemittance", remittanceListdataOptions);
        angularUIgridWrapper_remittanceList.init();
        buildRemittanceListGrid([]);
        // fncgetRemittance();

        angularUIgridWrapper_purchaseorder = new AngularUIGridWrapper("dgridPurchaseOrder", purchaseOrderdataOptions);
        angularUIgridWrapper_purchaseorder.init();
        buildPuchaseOrderListGrid([]);
        // fncgetPurchaseOrder();

        angularUIgridWrapper_dols = new AngularUIGridWrapper("dgridDols", dolsdataOptions);
        angularUIgridWrapper_dols.init();
        buildDolsListGrid([]);
        // fncgetDols();

    });
    $(window).load(function () {
        windowLoad = true;
        // buildRoomListGrid([]);
        fncgetroom();
        // fncGetCareHomeDetails();
    });
    function onChange() {
        setTimeout(function () {
            var selectedItems = angularUIgridWrapper.getSelectedRows();
            console.log(selectedItems);
            if (selectedItems && selectedItems.length > 0) {
                var obj = selectedItems[0];
                atid = obj.idk;
                $("#carehome-edit").prop("disabled", false);
                $("#carehome-delete").prop("disabled", false);
            } else {
                $("#carehome-edit").prop("disabled", true);
                $("#carehome-delete").prop("disabled", true);
            }
        });
    }

    function onRoomGridChange() {
        setTimeout(function () {
            var selectedItems = angularUIgridWrapperroom.getSelectedRows();
            console.log(selectedItems);
            if (selectedItems && selectedItems.length > 0) {
                var obj = selectedItems[0];
                atid = obj.idk;
                $("#carehome-edit").prop("disabled", false);
                $("#carehome-delete").prop("disabled", false);
            } else {
                $("#carehome-edit").prop("disabled", true);
                $("#carehome-delete").prop("disabled", true);
            }
        });
    }

    function onResisdentGridChange() {
        setTimeout(function () {
            var selectedItems = angularUIgridWrapper_resisdentterms.getSelectedRows();
            console.log(selectedItems);
            if (selectedItems && selectedItems.length > 0) {
                var obj = selectedItems[0];
                atid = obj.idk;
                $("#carehome-edit").prop("disabled", false);
                $("#carehome-delete").prop("disabled", false);
            } else {
                $("#carehome-edit").prop("disabled", true);
                $("#carehome-delete").prop("disabled", true);
            }
        });
    }

    function onFundingGridChange() {
        setTimeout(function () {
            var selectedItems = angularUIgridWrapper_funding.getSelectedRows();
            console.log(selectedItems);
            if (selectedItems && selectedItems.length > 0) {
                var obj = selectedItems[0];
                atid = obj.idk;
                $("#carehome-edit").prop("disabled", false);
                $("#carehome-delete").prop("disabled", false);
            } else {
                $("#carehome-edit").prop("disabled", true);
                $("#carehome-delete").prop("disabled", true);
            }
        });
    }

    function onRemittanceGridChange() {
        setTimeout(function () {
            var selectedItems = angularUIgridWrapper_remittance.getSelectedRows();
            console.log(selectedItems);
            if (selectedItems && selectedItems.length > 0) {
                var obj = selectedItems[0];
                atid = obj.idk;
                $("#carehome-edit").prop("disabled", false);
                $("#carehome-delete").prop("disabled", false);
            } else {
                $("#carehome-edit").prop("disabled", true);
                $("#carehome-delete").prop("disabled", true);
            }
        });
    }

    function onPurchaseGridChange() {
        setTimeout(function () {
            var selectedItems = angularUIgridWrapper_purchase.getSelectedRows();
            console.log(selectedItems);
            if (selectedItems && selectedItems.length > 0) {
                var obj = selectedItems[0];
                atid = obj.idk;
                $("#carehome-edit").prop("disabled", false);
                $("#carehome-delete").prop("disabled", false);
            } else {
                $("#carehome-edit").prop("disabled", true);
                $("#carehome-delete").prop("disabled", true);
            }
        });
    }

    function onHospitalGridChange() {
        setTimeout(function () {
            var selectedItems = angularUIgridWrapper_hospitals.getSelectedRows();
            console.log(selectedItems);
            if (selectedItems && selectedItems.length > 0) {
                var obj = selectedItems[0];
                atid = obj.idk;
                $("#carehome-edit").prop("disabled", false);
                $("#carehome-delete").prop("disabled", false);
            } else {
                $("#carehome-edit").prop("disabled", true);
                $("#carehome-delete").prop("disabled", true);
            }
        });
    }

    function onResisdentListGridChange() {
        setTimeout(function () {
            var selectedItems = angularUIgridWrapper_resisdent.getSelectedRows();
            console.log(selectedItems);
            if (selectedItems && selectedItems.length > 0) {
                var obj = selectedItems[0];
                atid = obj.idk;
                $("#carehome-edit").prop("disabled", false);
                $("#carehome-delete").prop("disabled", false);
            } else {
                $("#carehome-edit").prop("disabled", true);
                $("#carehome-delete").prop("disabled", true);
            }
        });
    }

    function onBedListGridChange() {
        setTimeout(function () {
            var selectedItems = angularUIgridWrapper_bed.getSelectedRows();
            console.log(selectedItems);
            if (selectedItems && selectedItems.length > 0) {
                var obj = selectedItems[0];
                atid = obj.idk;
                $("#carehome-edit").prop("disabled", false);
                $("#carehome-delete").prop("disabled", false);
            } else {
                $("#carehome-edit").prop("disabled", true);
                $("#carehome-delete").prop("disabled", true);
            }
        });
    }

    function onBedAllocationGridChange() {
        setTimeout(function () {
            var selectedItems = angularUIgridWrapper_bedAllocation.getSelectedRows();
            console.log(selectedItems);
            if (selectedItems && selectedItems.length > 0) {
                var obj = selectedItems[0];
                atid = obj.idk;
                $("#carehome-edit").prop("disabled", false);
                $("#carehome-delete").prop("disabled", false);
            } else {
                $("#carehome-edit").prop("disabled", true);
                $("#carehome-delete").prop("disabled", true);
            }
        });
    }

    function onRemittanceListGridChange() {
        setTimeout(function () {
            var selectedItems = angularUIgridWrapper_remittanceList.getSelectedRows();
            console.log(selectedItems);
            if (selectedItems && selectedItems.length > 0) {
                var obj = selectedItems[0];
                atid = obj.idk;
                $("#carehome-edit").prop("disabled", false);
                $("#carehome-delete").prop("disabled", false);
            } else {
                $("#carehome-edit").prop("disabled", true);
                $("#carehome-delete").prop("disabled", true);
            }
        });
    }

    function onPurchaseOrdeGridChange() {
        setTimeout(function () {
            var selectedItems = angularUIgridWrapper_purchaseorder.getSelectedRows();
            console.log(selectedItems);
            if (selectedItems && selectedItems.length > 0) {
                var obj = selectedItems[0];
                atid = obj.idk;
                $("#carehome-edit").prop("disabled", false);
                $("#carehome-delete").prop("disabled", false);
            } else {
                $("#carehome-edit").prop("disabled", true);
                $("#carehome-delete").prop("disabled", true);
            }
        });
    }

    function onDOLSGridChange() {
        setTimeout(function () {
            var selectedItems = angularUIgridWrapper_dols.getSelectedRows();
            console.log(selectedItems);
            if (selectedItems && selectedItems.length > 0) {
                var obj = selectedItems[0];
                atid = obj.idk;
                $("#carehome-edit").prop("disabled", false);
                $("#carehome-delete").prop("disabled", false);
            } else {
                $("#carehome-edit").prop("disabled", true);
                $("#carehome-delete").prop("disabled", true);
            }
        });
    }

    function adjustHeight() {
        var defHeight = 180;//+window.top.getAvailPageHeight();
        if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
            defHeight = 80;
        }
        var cmpHeight = window.innerHeight - defHeight;
        cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;

        if (angularUIgridWrapper)
            angularUIgridWrapper.adjustGridHeight(cmpHeight);
    }

    function buildDeviceListGrid(dataSource) {
        var gridColumns = [];
        var otoptions = {};
        otoptions.noUnselect = false;
        gridColumns.push({
            "title": "Abbreviation",
            "field": "abbreviation"
        });
        gridColumns.push({
            "title": "Name",
            "field": "name",
        });
        angularUIgridWrapper.creategrid(dataSource, gridColumns, otoptions);
        adjustHeight();
        //onIndividualRowClick();
    }

    function buildRoomListGrid(dataSource) {
        var gridColumns = [];
        var otoptions = {};
        otoptions.noUnselect = false;
        gridColumns.push({
            "title": "Room ID",
            "field": "idk",
        });
        gridColumns.push({
            "title": "Room Name",
            "field": "name",
        });
        angularUIgridWrapperroom.creategrid(dataSource, gridColumns, otoptions);
        // adjustHeight();
    }



    function buildBedListGrid(dataSource) {
        var gridColumns = [];
        var otoptions = {};
        otoptions.noUnselect = false;
        gridColumns.push({
            "title": "ID",
            "field": "idk",
        });
        gridColumns.push({
            "title": "Name",
            "field": "name",
        });
        gridColumns.push({
            "title": "Rate",
            "field": "rate",
        });
        angularUIgridWrapper_bed.creategrid(dataSource, gridColumns, otoptions);
        adjustHeight();
    }

    function buildBedAllocationListGrid(dataSource) {
        var gridColumns = [];
        var otoptions = {};
        otoptions.noUnselect = false;
        gridColumns.push({
            "title": "ID",
            "field": "idk",
        });
        gridColumns.push({
            "title": "Care Home Name",
            "field": "careHomeName",
        });

        gridColumns.push({
            "title": "Bed Name",
            "field": "bedName",
        });

        // gridColumns.push({
        //     "title": "Bed Rate",
        //     "field": "bedRate",
        // });
        angularUIgridWrapper_bedAllocation.creategrid(dataSource, gridColumns, otoptions);
        // adjustHeight();
    }


    function buildRemittanceListGrid(dataSource) {
        var gridColumns = [];
        var otoptions = {};
        otoptions.noUnselect = false;
        gridColumns.push({
            "title": "ID",
            "field": "idk",
        });
        gridColumns.push({
            "title": "Care Home Name",
            "field": "careHomeName",
        });

        gridColumns.push({
            "title": "Top UpRate",
            "field": "topUpRate",
        });

        gridColumns.push({
            "title": "Banding Rate",
            "field": "bandingRate",
        });
        angularUIgridWrapper_remittanceList.creategrid(dataSource, gridColumns, otoptions);

    }

    function buildPuchaseOrderListGrid(dataSource) {
        var gridColumns = [];
        var otoptions = {};
        otoptions.noUnselect = false;
        gridColumns.push({
            "title": "ID",
            "field": "idk",
        });

        gridColumns.push({
            "title": "Purchase Order Number",
            "field": "purchaseOrderNumber",
        });

        angularUIgridWrapper_purchaseorder.creategrid(dataSource, gridColumns, otoptions);
    }

    function buildDolsListGrid(dataSource) {
        var gridColumns = [];
        var otoptions = {};
        otoptions.noUnselect = false;
        gridColumns.push({
            "title": "ID",
            "field": "idk",
        });

        gridColumns.push({
            "title": "Care Home Name",
            "field": "careHomeName",
        });

        gridColumns.push({
            "title": "DOL Name",
            "field": "name",
        });

        gridColumns.push({
            "title": "Safeguard",
            "field": "safeguard",
        });

        angularUIgridWrapper_dols.creategrid(dataSource, gridColumns, otoptions);
    }

    function buildResisdentGrid(dataSource) {
        var gridColumns = [];
        var otoptions = {};
        otoptions.noUnselect = false;
        gridColumns.push({
            "title": "Id",
            "field": "idk",
        });
        gridColumns.push({
            "title": "First Name",
            "field": "firstName",
        });
        gridColumns.push({
            "title": "Middle Name",
            "field": "middleName",
        });
        gridColumns.push({
            "title": "Last Name",
            "field": "lastName",
        });
        gridColumns.push({
            "title": "Gender",
            "field": "gender",
        });
        angularUIgridWrapper_resisdent.creategrid(dataSource, gridColumns, otoptions);
        // adjustHeight();
    }


    function buildResisdentListGrid(dataSource) {
        var gridColumns = [];
        var otoptions = {};
        otoptions.noUnselect = false;
        gridColumns.push({
            "title": "ID",
            "field": "idk",
        });
        gridColumns.push({
            "title": "Name",
            "field": "name",
        });
        // gridColumns.push({
        //     "title": "Gender",
        //     "field": "gender",
        // });
        angularUIgridWrapper_resisdentterms.creategrid(dataSource, gridColumns, otoptions);
        adjustHeight();
    }

    function buildHospitalListGrid(dataSource) {
        var gridColumns = [];
        var otoptions = {};
        otoptions.noUnselect = false;
        gridColumns.push({
            "title": "ID",
            "field": "idk",
        });
        gridColumns.push({
            "title": "Name",
            "field": "name",
        });
        angularUIgridWrapper_hospitals.creategrid(dataSource, gridColumns, otoptions);
        adjustHeight();
    }

    function buildFundingListGrid(dataSource) {
        var gridColumns = [];
        var otoptions = {};
        otoptions.noUnselect = false;
        gridColumns.push({
            "title": "ID",
            "field": "idk",
        });
        gridColumns.push({
            "title": "Name",
            "field": "name",
        });
        angularUIgridWrapper_funding.creategrid(dataSource, gridColumns, otoptions);
        adjustHeight();
    }

    function buildRemittanceStatusListGrid(dataSource) {
        var gridColumns = [];
        var otoptions = {};
        otoptions.noUnselect = false;
        gridColumns.push({
            "title": "ID",
            "field": "idk",
        });
        gridColumns.push({
            "title": "Name",
            "field": "name",
        });
        angularUIgridWrapper_remittance.creategrid(dataSource, gridColumns, otoptions);
        adjustHeight();
    }

    function buildPurchaseListGrid(dataSource) {
        var gridColumns = [];
        var otoptions = {};
        otoptions.noUnselect = false;
        gridColumns.push({
            "title": "ID",
            "field": "idk",
        });
        gridColumns.push({
            "title": "Name",
            "field": "name",
        });
        angularUIgridWrapper_purchase.creategrid(dataSource, gridColumns, otoptions);
        // adjustHeight();
    }

    function getCareHomeList(dataObj) {
        nameglobal = sessionStorage.getItem("nameglobal");
        console.log(dataObj);
        var tempCompType = [];
        careHome = [];
        if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
            if (dataObj.response.careHomes) {
                if ($.isArray(dataObj.response.careHomes)) {
                    tempCompType = dataObj.response.careHomes;
                } else {
                    tempCompType.push(dataObj.response.careHomes);
                }
            }
        }

        for (var i = 0; i < tempCompType.length; i++) {
            tempCompType[i].idk = tempCompType[i].id;
        }

        for (var i = 0; i < tempCompType.length; i++) {
            var obj = {};
            obj.Key = tempCompType[i].name;
            obj.Value = tempCompType[i].id;
            careHome.push(obj);
        }

        var dataArray = careHome;

        var objD = {};
        objD.Key = "";
        objD.Value = "";
        dataArray.unshift(objD);

        if(nameglobal != "resisdentsmaster"){
            setDataForSelection(dataArray, "cmbCareHomeName", onCareHomeChange, ["Key", "Value"], 0, "");
        }

        if (nameglobal != "carehomemaster") {
            setDataForSelection(careHome, "cmbCareHome", onCareHomeChange, ["Key", "Value"], 0, "");
            setDataForSelection(careHome, "cmbrtCareHome", onCareHomeChange, ["Key", "Value"], 0, "");
            setDataForSelection(careHome, "resisdentCareHomeId", onCareHomeChange, ["Key", "Value"], 0, "");
            setDataForSelection(careHome, "bedCareHome", onCareHomeChange, ["Key", "Value"], 0, "");
            setDataForSelection(careHome, "bedCareHome", onCareHomeChange, ["Key", "Value"], 0, "");
            setDataForSelection(careHome, "cmbCareHomeId", onCareHomeChange, ["Key", "Value"], 0, "");
            setDataForSelection(careHome, "cmbRemittanceCareHomeId", onCareHomeChange, ["Key", "Value"], 0, "");
            setDataForSelection(careHome, "cmbPurchaseCareHomeId", onCareHomeChange, ["Key", "Value"], 0, "");
            setDataForSelection(careHome, "cmbDolsCareHomeId", onCareHomeChange, ["Key", "Value"], 0, "");
            setDataForSelection(dataArray, "cmbResisdentsCareHomeName", onCareHomeChange, ["Key", "Value"], 0, "");
            setDataForSelection(dataArray, "cmbBACareHomeName", onCareHomeChange, ["Key", "Value"], 0, "");
            setDataForSelection(dataArray, "cmbRCareHomeName", onCareHomeChange, ["Key", "Value"], 0, "");
            setDataForSelection(dataArray, "cmbPOCareHomeName", onCareHomeChange, ["Key", "Value"], 0, "");
            setDataForSelection(dataArray, "cmbDOLCareHomeName", onCareHomeChange, ["Key", "Value"], 0, "");
            setDataForSelection(dataArray, "cmbHOSCareHomeName", onCareHomeChange, ["Key", "Value"], 0, "");
            setDataForSelection(dataArray, "cmbBedsCareHomeName", onCareHomeChange, ["Key", "Value"], 0, "");
            setDataForSelection(dataArray, "cmbRTCareHomeName", onCareHomeChange, ["Key", "Value"], 0, "");
            setDataForSelection(dataArray, "cmbFundingCareHomeName", onCareHomeChange, ["Key", "Value"], 0, "");
            setDataForSelection(dataArray, "cmbRSeHomeName", onCareHomeChange, ["Key", "Value"], 0, "");
            setDataForSelection(dataArray, "cmbPOSeHomeName", onCareHomeChange, ["Key", "Value"], 0, "");
            fncGetGender();
        }
        setDataForSelection(statusArr, "cmbStatus", onStatusChange, ["Value", "Key"], 0, "");
        setDataForSelection(statusArr, "cmbBedListStatus", onStatusChange, ["Value", "Key"], 0, "");
        setDataForSelection(statusArr, "residents-status", onStatusChange, ["Value", "Key"], 0, "");
        setDataForSelection(statusArr, "cmbBedAllocStatus", onStatusChange, ["Key", "Value"], 0, "");
        setDataForSelection(statusArr, "cmbRemittancesStatus", onStatusChange, ["Key", "Value"], 0, "");
        setDataForSelection(statusArr, "cmbPurchaseOrderStatus", onStatusChange, ["Key", "Value"], 0, "");
        setDataForSelection(statusArr, "cmbDOLStatus", onStatusChange, ["Key", "Value"], 0, "");
        setDataForSelection(statusArr, "cmbCareHomeStatus", onStatusChange, ["Key", "Value"], 0, "");

        // getTableValueList('active');

        buildDeviceListGrid(tempCompType);
    }

    function onCareHomeChange() {
        var cmbCareHome = $("#cmbCareHome").data("kendoComboBox");
        if (cmbCareHome && cmbCareHome.selectedIndex < 0) {
            cmbCareHome.select(0);
        }
    }

    function onFundingChange() {
        var cmbFundingId = $("#cmbFundingId").data("kendoComboBox");
        if (cmbFundingId && cmbFundingId.selectedIndex < 0) {
            cmbFundingId.select(0);
        }
    }

    function onBedChange() {
        var cmbBedId = $("#cmbBedId").data("kendoComboBox");
        if (cmbBedId && cmbBedId.selectedIndex < 0) {
            cmbBedId.select(0);
        }
    }

    function onBedRoomChange() {
        var cmbBedRoom = $("#cmbBedRoom").data("kendoComboBox");
        if (cmbBedRoom && cmbBedRoom.selectedIndex < 0) {
            cmbBedRoom.select(0);
        }
    }

    function onResisdentTermChange() {
        var cmdResisdentTermId = $("#cmdResisdentTermId").data("kendoComboBox");
        if (cmdResisdentTermId && cmdResisdentTermId.selectedIndex < 0) {
            cmdResisdentTermId.select(0);
        }
    }

    function onError(errorObj) {
        customAlert.error("error", errorObj.response.status.message);
        console.log(errorObj);
    }

    function saveCareHome() {
        IsFlag = 0;
    	var isActive = 0;
        var cmbCareHomeStatus = $("#cmbCareHomeStatus").data("kendoComboBox");
        if (cmbCareHomeStatus) {
            if (cmbCareHomeStatus.selectedIndex == 0) {
                isActive = 1;
            }
        }
        var dataUrl = ipAddress + "/carehome/care-homes/";
        var method = operation;
        var reqObj = {};
        reqObj.isDeleted = 0;
        reqObj.createdBy = parseInt(sessionStorage.userId);
        reqObj.name = $('#careHomeName').val();
        reqObj.abbreviation = $('#careHomeAbbreviation').val();
        reqObj.isActive = isActive;
        reqObj.website = $('input[name="care-website"]').val();
        reqObj.notes = $('input[name="care-notes"]').val();
        reqObj.managerLastName = $('input[name="careManagement-lastname"]').val();
        reqObj.managerFirstName = $('input[name="careManagement-firstname"]').val();
        reqObj.adminFirstName = $('input[name="careAdmin-firstname"]').val();
        reqObj.adminLastName = $('input[name="careAdmin-lastname"]').val();

        if (operation.toLowerCase() == "put") {
            reqObj.id = $('#careHomeid').val();
        }

        if ($('#careHomeName').val() != "" || $('#careHomeAbbreviation').val()) {
            createAjaxObject(dataUrl, reqObj, method, fncGetCarehomedetails, onError);
        }
        else {
            customAlert.error("error", "Please Fill All required fields.");
        }

    }

    function deleteCareHome() {
        nameglobal = sessionStorage.getItem("nameglobal");
        var method = "DELETE";
        var reqObj = {};
        var dataUrl, selectedItems;

        if (nameglobal == "carehomemaster") {
            dataUrl = ipAddress + "/carehome/care-homes/";
            selectedItems = angularUIgridWrapper.getSelectedRows();
        }
        else if (nameglobal == "roommaster") {
            dataUrl = ipAddress + "/carehome/rooms/";
            selectedItems = angularUIgridWrapperroom.getSelectedRows();
        }
        else if (nameglobal == "fundings") {
            dataUrl = ipAddress + "/carehome/fundings/";
            selectedItems = angularUIgridWrapper_funding.getSelectedRows();
        }
        else if (nameglobal == "residentterms") {
            dataUrl = ipAddress + "/carehome/resident-terms/";
            selectedItems = angularUIgridWrapper_resisdentterms.getSelectedRows();
        }
        else if (nameglobal == "remittance_status") {
            dataUrl = ipAddress + "/carehome/remittance-status/";
            selectedItems = angularUIgridWrapper_remittance.getSelectedRows();
        }
        else if (nameglobal == "purchase_order_status") {
            dataUrl = ipAddress + "/carehome/purchase-order-status/";
            selectedItems = angularUIgridWrapper_purchase.getSelectedRows();
        }
        else if (nameglobal == "hospitals") {
            dataUrl = ipAddress + "/carehome/hospitals/";
            selectedItems = angularUIgridWrapper_hospitals.getSelectedRows();
        }
        else if (nameglobal == "resisdentsmaster") {
            dataUrl = ipAddress + "/carehome/residents/";
            selectedItems = angularUIgridWrapper_resisdent.getSelectedRows();
        }
        else if (nameglobal == "beds") {
            dataUrl = ipAddress + "/carehome/beds/";
            selectedItems = angularUIgridWrapper_bed.getSelectedRows();
        }
        else if (nameglobal == "bed_allocation") {
            dataUrl = ipAddress + "/carehome/bed-allocations/";
            selectedItems = angularUIgridWrapper_bedAllocation.getSelectedRows();
        }
        else if (nameglobal == "purchase_order") {
            dataUrl = ipAddress + "/carehome/purchase-orders/";
            selectedItems = angularUIgridWrapper_purchaseorder.getSelectedRows();
        }
        else if (nameglobal == "remittances") {
            dataUrl = ipAddress + "/carehome/remittances/";
            selectedItems = angularUIgridWrapper_remittanceList.getSelectedRows();
        }
        else if (nameglobal == "dols") {
            dataUrl = ipAddress + "/carehome/dols/";
            selectedItems = angularUIgridWrapper_dols.getSelectedRows();
        }


        if (selectedItems && selectedItems.length > 0) {
            var obj = selectedItems[0];
            reqObj.id = obj.idk;
            reqObj.isDeleted = 1;
            reqObj.isActive = 0;
        }

        createAjaxObject(dataUrl, reqObj, method, onDelete, onError);
    }


function onDelete(resp) {
    if (resp.response.status.code == 1) {
        nameglobal = sessionStorage.getItem("nameglobal");
        if (nameglobal == "carehomemaster") {
            customAlert.info("info", "Care Home deleted successfully");
        }
        else if (nameglobal == "roommaster") {
            customAlert.info("info", "Room deleted successfully");
        }
        else if (nameglobal == "fundings") {
            customAlert.info("info", "Funding deleted successfully");
        }
        else if (nameglobal == "residentterms") {
            customAlert.info("info", "Resisdent Term deleted successfully");
        }
        else if (nameglobal == "remittance_status") {
            customAlert.info("info", "Remittance Status deleted successfully");
        }
        else if (nameglobal == "purchase_order_status") {
            customAlert.info("info", "Purchase Order Status deleted successfully");
        }
        else if (nameglobal == "hospitals") {
            customAlert.info("info", "Hospital deleted successfully");
        }
        else if (nameglobal == "resisdentsmaster") {
            customAlert.info("info", "Resident deleted successfully");
        }
        else if (nameglobal == "beds") {
            customAlert.info("info", "Bed deleted successfully");
        }
        else if (nameglobal == "bed_allocation") {
            customAlert.info("info", "Bed Allocation deleted successfully");
        }
        else if(nameglobal == "purchase_order"){
            customAlert.info("info", "Purchase Order deleted successfully");
        }
        else if (nameglobal == "remittances") {
            customAlert.info("info", "Remittances deleted successfully");
        }
        else if (nameglobal == "dols") {
            customAlert.info("info", "dol deleted successfully");
        }
    }

    // $('[data-medicate-content]').show();
    // $('[data-medicate-content="1"]').hide();
    // fncgetCareHome();
}

    function onCreate(resp) {
        reset();
        if (resp.response.status.code == 1) {
            nameglobal = sessionStorage.getItem("nameglobal");
            if (nameglobal == "carehomemaster") {
                if (operation.toLowerCase() == "put") {
                    customAlert.info("info", "Care Home updated successfully");
                }
                else{
                    customAlert.info("info", "Care Home saved successfully");
                }
            }
            else if (nameglobal == "roommaster") {
                if (operation.toLowerCase() == "put") {
                    customAlert.info("info", "Room updated successfully");
                }
                else{
                    customAlert.info("info", "Room saved successfully");
                }
            }
            else if (nameglobal == "fundings") {
                if (operation.toLowerCase() == "put") {
                    customAlert.info("info", "Funding updated successfully");
                }
                else{
                    customAlert.info("info", "Funding saved successfully");
                }
            }
            else if (nameglobal == "residentterms") {
                if (operation.toLowerCase() == "put") {
                    customAlert.info("info", "Resisdent Term updated successfully");
                }
                else{
                    customAlert.info("info", "Resisdent Term saved successfully");
                }
            }
            else if (nameglobal == "remittance_status") {
                if (operation.toLowerCase() == "put") {
                    customAlert.info("info", "Remittance Status updated successfully");
                }
                else{
                    customAlert.info("info", "Remittance Status saved successfully");
                }
            }
            else if (nameglobal == "purchase_order_status") {
                if (operation.toLowerCase() == "put") {
                    customAlert.info("info", "Purchase Order updated successfully");
                }
                else{
                    customAlert.info("info", "Purchase Order Status saved successfully");
                }
            }
            else if (nameglobal == "hospitals") {
                if (operation.toLowerCase() == "put") {
                    customAlert.info("info", "Hospital updated successfully");
                }
                else{
                    customAlert.info("info", "Hospital saved successfully");
                }
            }
            else if (nameglobal == "resisdentsmaster") {
                if (operation.toLowerCase() == "put") {
                    customAlert.info("info", "Resident updated successfully");
                }
                else{
                    customAlert.info("info", "Resident saved successfully");
                }
            }
            else if (nameglobal == "beds") {
                if (operation.toLowerCase() == "put") {
                    customAlert.info("info", "Bed updated successfully");
                }
                else{
                    customAlert.info("info", "Bed saved successfully");
                }
            }
            else if (nameglobal == "bed_allocation") {
                if (operation.toLowerCase() == "put") {
                    customAlert.info("info", "Bed Allocation updated successfully");
                }
                else{
                    customAlert.info("info", "Bed Allocation saved successfully");
                }
            }
            else if (nameglobal == "purchase_order") {
                if (operation.toLowerCase() == "put") {
                    customAlert.info("info", "Purchase Order updated successfully");
                }
                else{
                    customAlert.info("info", "Purchase Order saved successfully");
                }
            }
            else if (nameglobal == "remittances") {
                if (operation.toLowerCase() == "put") {
                    customAlert.info("info", "Remittance updated successfully");
                }
                else{
                    customAlert.info("info", "Remittance saved successfully");
                }
            }
            else if (nameglobal == "remittances") {
                if (operation.toLowerCase() == "put") {
                    customAlert.info("info", "Remittance updated successfully");
                }
                else{
                    customAlert.info("info", "Remittance saved successfully");
                }
            }
            else if (nameglobal == "dols") {
                if (operation.toLowerCase() == "put") {
                    customAlert.info("info", "DOL updated successfully");
                }
                else {
                    customAlert.info("info", "DOL saved successfully");
                }
            }
            $('[data-medicate-content]').show();
            $('[data-medicate-content="1"]').hide();
        }
        else {
            customAlert.info("info", resp.response.status.message);
        }

    }

    var IsFlag = 0;

    function onClickEdit() {
        IsFlag = 1;
        var selectedItems;
        nameglobal = sessionStorage.getItem("nameglobal");
        var url;
        if (nameglobal == "carehomemaster") {
            var selectedItems = angularUIgridWrapper.getSelectedRows();

            if (selectedItems && selectedItems.length > 0) {
                var obj = selectedItems[0];
                atid = obj.idk;
                var cmbCareHomeStatus = $("#cmbCareHomeStatus").data("kendoComboBox");
                if (cmbCareHomeStatus) {
                    if (obj.isActive == 1) {
                        cmbCareHomeStatus.select(0);
                    } else {
                        cmbCareHomeStatus.select(1);
                    }
                }
                $('#careHomeid').val(atid);
                $('#careHomeName').val(obj.name);
                $('#careHomeAbbreviation').val(obj.abbreviation);
                $('input[name="care-website"]').val(obj.website);
                $('input[name="care-notes"]').val(obj.notes);
                $('input[name="careManagement-lastname"]').val(obj.managerLastName);
                $('input[name="careManagement-firstname"]').val(obj.managerFirstName);
                $('input[name="careAdmin-firstname"]').val(obj.adminFirstName);
                $('input[name="careAdmin-lastname"]').val(obj.adminLastName);
                fncGetCarehomedetails();
                $('[data-medicate-content]').hide();
                $('[data-medicate-content="1"]').show();
            }
        }
        else if (nameglobal == "resisdentsmaster") {
            var selectedItems = angularUIgridWrapper_resisdent.getSelectedRows();
            if (selectedItems && selectedItems.length > 0) {
                var obj = selectedItems[0];
                atid = obj.idk;
                $('#residentID').val(atid);
                var resisdentCareHomeId = $("#resisdentCareHomeId").data("kendoComboBox");
                if (resisdentCareHomeId) {
                    var aid = getActivityNameById(obj.careHomeId);
                    resisdentCareHomeId.select(aid);
                }

                var cmbGender = $("#cmbGender").data("kendoComboBox");
                getComboListIndex("cmbGender", "desc", obj.gender);

                if (obj.dateOfBirth) {
                    var dt = new Date(obj.dateOfBirth);
                    if (dt) {
                        var strDT = "";
                        var cntry = sessionStorage.countryName;
                        if (cntry.indexOf("India") >= 0) {
                            strDT = kendo.toString(dt, "dd/MM/yyyy");
                        } else if (cntry.indexOf("United Kingdom") >= 0) {
                            strDT = kendo.toString(dt, "dd/MM/yyyy");
                        } else {
                            strDT = kendo.toString(dt, "MM/dd/yyyy");
                        }

                        $("#resisdentBirthDate").datepicker();
                        $("#resisdentBirthDate").datepicker("setDate", strDT);
                    }
                }

                $('#residents-fname').val(obj.firstName);
                $('#residents-lname').val(obj.lastName);
                $('#residents-mname').val(obj.middleName);

                var residents_status = $("#residents-status").data("kendoComboBox");
                if (residents_status) {
                    if (obj.isActive == 1) {
                        residents_status.select(0);
                    } else {
                        residents_status.select(1);
                    }
                }
                getResisdentCommunictiondetails(obj);
                $('[data-medicate-content]').hide();
                $('[data-medicate-content="1"]').show();
            }
        }
        else if (nameglobal == "beds") {
            selectedItems = angularUIgridWrapper_bed.getSelectedRows();
            if (selectedItems && selectedItems.length > 0) {
                var obj = selectedItems[0];
                var cmbBedListStatus = $("#cmbBedListStatus").data("kendoComboBox");
                if (cmbBedListStatus) {
                    if (obj.isActive == 1) {
                        cmbBedListStatus.select(0);
                    } else {
                        cmbBedListStatus.select(1);
                    }
                }
                atid = obj.idk;
                $('#bedid').val(atid);
                $('#bedName').val(obj.name);
                $('#bedRate').val(obj.rate);
                var bedCareHome = $("#bedCareHome").data("kendoComboBox");
                if (bedCareHome) {
                    var aid = getActivityNameById(obj.careHomeId);
                    bedCareHome.select(aid);
                }

                var cmbBedRoom = $("#cmbBedRoom").data("kendoComboBox");
                if (cmbBedRoom) {
                    var aid = getBedRoomById(obj.roomId);
                    cmbBedRoom.select(aid);
                }

                $('[data-medicate-content]').hide();
                $('[data-medicate-content="1"]').show();
            }
        }

        else if (nameglobal == "bed_allocation") {
            selectedItems = angularUIgridWrapper_bedAllocation.getSelectedRows();
            if (selectedItems && selectedItems.length > 0) {
                var obj = selectedItems[0];
                var cmbBedAllocStatus = $("#cmbBedAllocStatus").data("kendoComboBox");
                if (cmbBedAllocStatus) {
                    if (obj.isActive == 1) {
                        cmbBedAllocStatus.select(0);
                    } else {
                        cmbBedAllocStatus.select(1);
                    }
                }
                atid = obj.idk;
                $('#txtBedAllocationId').val(atid);
                // $('#bedName').val(obj.name);
                // $('#bedRate').val(obj.rate);

                var cmbFundingId = $("#cmbFundingId").data("kendoComboBox");
                if (cmbFundingId) {
                    var aid = getfundingById(obj.fundingId);
                    cmbFundingId.select(aid);
                }

                var cmdResisdentTermId = $("#cmdResisdentTermId").data("kendoComboBox");
                if (cmdResisdentTermId) {
                    var aid = getResidentTermsById(obj.residentTermId);
                    cmdResisdentTermId.select(aid);
                }

                if (obj.fromDate != "" && obj.fromDate != null) {
                    $("#txtFromDate").datepicker();
                    $("#txtFromDate").datepicker("setDate", GetDateEdit(obj.fromDate));
                }

                if (obj.toDate != "" && obj.toDate != null) {
                    $("#txtToDate").datepicker();
                    $("#txtToDate").datepicker("setDate", GetDateEdit(obj.toDate));
                }
                if (obj.occupiedDate != "" && obj.occupiedDate != null) {
                    $("#txtOccupiedDate").datepicker();
                    $("#txtOccupiedDate").datepicker("setDate", GetDateEdit(obj.occupiedDate));
                }
                if (obj.vacantDate != "" && obj.vacantDate != null) {
                    $("#txtVacantDate").datepicker();
                    $("#txtVacantDate").datepicker("setDate", GetDateEdit(obj.vacantDate));
                }


                var cmbCareHomeId = $("#cmbCareHomeId").data("kendoComboBox");
                if (cmbCareHomeId) {
                    var aid = getActivityNameById(obj.careHomeId);
                    cmbCareHomeId.select(aid);
                }

                var cmbRoomId = $("#cmbRoomId").data("kendoComboBox");
                if (cmbRoomId) {
                    var aid = getRoomByBedId(obj.bedId);
                    cmbRoomId.select(aid);
                }

                var cmbBedId = $("#cmbBedId").data("kendoComboBox");
                if (cmbBedId) {
                    var aid = getBedById(obj.bedId);
                    cmbBedId.select(aid);
                }
                resisdentId = obj.residentId;

                var URL;
                URL = ipAddress+"/carehome/residents/?id="+obj.residentId;
                getAjaxObject(URL,"GET", onResidentListData, onError);

                // onChangeRoom();

                $('[data-medicate-content]').hide();
                $('[data-medicate-content="1"]').show();
            }
        }
        else if (nameglobal == "purchase_order") {
            selectedItems = angularUIgridWrapper_purchaseorder.getSelectedRows();
            if (selectedItems && selectedItems.length > 0) {
                var obj = selectedItems[0];
                var cmbPurchaseOrderStatus = $("#cmbPurchaseOrderStatus").data("kendoComboBox");
                if (cmbPurchaseOrderStatus) {
                    if (obj.isActive == 1) {
                        cmbPurchaseOrderStatus.select(0);
                    } else {
                        cmbPurchaseOrderStatus.select(1);
                    }
                }
                atid = obj.idk;
                $('#txtPurchaseOrderID').val(atid);
                $('#txtPurchaseOrderNumber').val(obj.purchaseOrderNumber);

                var cmbPurchaseOrderStatusId = $("#cmbPurchaseOrderStatusId").data("kendoComboBox");
                if (cmbPurchaseOrderStatusId) {
                    var aid = getPOStatusById(obj.purchaseOrderStatusId);
                    cmbPurchaseOrderStatusId.select(aid);
                }


                var cmbHospitalId = $("#cmbHospitalId").data("kendoComboBox");
                if (cmbHospitalId) {
                    var aid = getHospitalById(obj.hospitalId);
                    cmbHospitalId.select(aid);
                }

                var cmdPurchaseResisdentTermId = $("#cmdPurchaseResisdentTermId").data("kendoComboBox");
                if (cmdPurchaseResisdentTermId) {
                    var aid = getResidentTermsById(obj.residentTermId);
                    cmdPurchaseResisdentTermId.select(aid);
                }

                if (obj.residentdateOfBirth != "" && obj.residentdateOfBirth != null) {
                    $("#txtPurchaseResisdentDOB").datepicker();
                    $("#txtPurchaseResisdentDOB").datepicker("setDate", GetDateEdit(obj.residentdateOfBirth));
                }

                if (obj.residentgender != "" && obj.residentgender != null) {
                    $("#txtPurchaseResisdentGender").datepicker();
                    $("#txtPurchaseResisdentGender").datepicker("setDate", GetDateEdit(obj.residentgender));
                }


                var cmbPurchaseCareHomeId = $("#cmbPurchaseCareHomeId").data("kendoComboBox");
                if (cmbPurchaseCareHomeId) {
                    var aid = getActivityNameById(obj.careHomeId);
                    cmbPurchaseCareHomeId.select(aid);
                }

                purchaseOrderResidentId = obj.residentId;

                var URL;
                URL = ipAddress+"/carehome/residents/?id="+purchaseOrderResidentId;
                getAjaxObject(URL,"GET", onResidentListData, onError);

                $('[data-medicate-content]').hide();
                $('[data-medicate-content="1"]').show();
            }
        }
        else if (nameglobal == "remittances") {
            selectedItems = angularUIgridWrapper_remittanceList.getSelectedRows();
            if (selectedItems && selectedItems.length > 0) {
                var obj = selectedItems[0];
                var cmbRemittancesStatus = $("#cmbRemittancesStatus").data("kendoComboBox");
                if (cmbRemittancesStatus) {
                    if (obj.isActive == 1) {
                        cmbRemittancesStatus.select(0);
                    } else {
                        cmbRemittancesStatus.select(1);
                    }
                }
                atid = obj.idk;
                $('#txtRemittanceID').val(atid);
                var cmbRemittanceStatusId = $("#cmbRemittanceStatusId").data("kendoComboBox");
                if (cmbRemittanceStatusId) {
                    var aid = getRemittanceStatusById(obj.remittanceStatusId);
                    cmbRemittanceStatusId.select(aid);
                }

                if (obj.fromDate != "" && obj.fromDate != null) {
                    $("#txtRemittanceFromDate").datepicker();
                    $("#txtRemittanceFromDate").datepicker("setDate", GetDateEdit(obj.fromDate));
                }

                if (obj.toDate != "" && obj.toDatetoDate != null) {
                    $("#txtRemittanceToDate").datepicker();
                    $("#txtRemittanceToDate").datepicker("setDate", GetDateEdit(obj.toDate));
                }

                $('#txtTopupRate').val(obj.topUpRate);
                $('#txtRemittanceBandingRate').val(obj.bandingRate);

                var cmbRemittanceCareHomeId = $("#cmbRemittanceCareHomeId").data("kendoComboBox");
                if (cmbRemittanceCareHomeId) {
                    var aid = getActivityNameById(obj.careHomeId);
                    cmbRemittanceCareHomeId.select(aid);
                }

                remittanceResidentId = obj.residentId;

                var URL;
                URL = ipAddress+"/carehome/residents/?id="+remittanceResidentId;
                getAjaxObject(URL,"GET", onResidentListData, onError);

                $('[data-medicate-content]').hide();
                $('[data-medicate-content="1"]').show();
            }
        }
        else if (nameglobal == "dols") {
            selectedItems = angularUIgridWrapper_dols.getSelectedRows();
            if (selectedItems && selectedItems.length > 0) {
                var obj = selectedItems[0];
                var cmbDOLStatus = $("#cmbDOLStatus").data("kendoComboBox");
                if (cmbDOLStatus) {
                    if (obj.isActive == 1) {
                        cmbDOLStatus.select(0);
                    } else {
                        cmbDOLStatus.select(1);
                    }
                }
                atid = obj.idk;
                $('#txtDolsID').val(atid);
                $('#txtDolName').val(obj.name);
                $('#txtSafeGuard').val(obj.safeguard);
                if (obj.dateGenerated != "" && obj.dateGenerated != null) {
                    $("#txtDateGenerated").datepicker();
                    $("#txtDateGenerated").datepicker("setDate", GetDateEdit(obj.dateGenerated));
                }

                if (obj.dateExpired != "" && obj.dateExpired != null) {
                    $("#txtDateExpired").datepicker();
                    $("#txtDateExpired").datepicker("setDate", GetDateEdit(obj.dateExpired));
                }

                var cmbDolsCareHomeId = $("#cmbDolsCareHomeId").data("kendoComboBox");
                if (cmbDolsCareHomeId) {
                    var aid = getActivityNameById(obj.careHomeId);
                    cmbDolsCareHomeId.select(aid);
                }

                dolsResidentId = obj.residentId;

                var URL;
                URL = ipAddress+"/carehome/residents/?id="+dolsResidentId;
                getAjaxObject(URL,"GET", onResidentListData, onError);

                $('[data-medicate-content]').hide();
                $('[data-medicate-content="1"]').show();
            }
        }

        else {
            if (nameglobal == "residentterms") {
                selectedItems = angularUIgridWrapper_resisdentterms.getSelectedRows();
            }
            else if (nameglobal == "remittance_status") {
                selectedItems = angularUIgridWrapper_remittance.getSelectedRows();
            }
            else if (nameglobal == "purchase_order_status") {
                selectedItems = angularUIgridWrapper_purchase.getSelectedRows();
            }
            else if (nameglobal == "hospitals") {
                selectedItems = angularUIgridWrapper_hospitals.getSelectedRows();
            }
            else if (nameglobal == "roommaster") {
                selectedItems = angularUIgridWrapperroom.getSelectedRows();
            }
            else if (nameglobal == "fundings") {
                selectedItems = angularUIgridWrapper_funding.getSelectedRows();
            }

            if (selectedItems && selectedItems.length > 0) {
                var obj = selectedItems[0];
                var cmbStatus = $("#cmbStatus").data("kendoComboBox");
                if (cmbStatus) {
                    if (obj.isActive == 1) {
                        cmbStatus.select(0);
                    } else {
                        cmbStatus.select(1);
                    }
                }
                atid = obj.idk;
                $('#roomid').val(atid);
                $('#roomName').val(obj.name);
                var cmbcarehome = $("#cmbCareHome").data("kendoComboBox");
                if (cmbcarehome) {
                    var aid = getActivityNameById(obj.careHomeId);
                    cmbcarehome.select(aid);
                }

                $('[data-medicate-content]').hide();
                $('[data-medicate-content="1"]').show();
            }
        }
    }

    function GetDateEdit(objDate){
        var dt = new Date(objDate);
        var strDT = "";
        if (dt) {
            var cntry = sessionStorage.countryName;
            if (cntry.indexOf("India") >= 0) {
                strDT = kendo.toString(dt, "dd/MM/yyyy");
            } else if (cntry.indexOf("United Kingdom") >= 0) {
                strDT = kendo.toString(dt, "dd/MM/yyyy");
            } else {
                strDT = kendo.toString(dt, "MM/dd/yyyy");
            }
        }
        return strDT;
    }

    function GetResidentDate(objDate){
        var dt = new Date(objDate);
        var strDT = "";
        if (dt) {
            var cntry = sessionStorage.countryName;
            if (cntry.indexOf("India") >= 0) {
                strDT = kendo.toString(dt, "dd-MM-yyyy");
            } else if (cntry.indexOf("United Kingdom") >= 0) {
                strDT = kendo.toString(dt, "dd-MM-yyyy");
            } else {
                strDT = kendo.toString(dt, "MM-dd-yyyy");
            }
        }
        return strDT;
    }

    function fncgetCareHome() {
        getAjaxObject(ipAddress + "/carehome/care-homes/?is-active=1&is-deleted=0", "GET", getCareHomeList, onError);
    }


    function searchOnLoad(status) {
        nameglobal = sessionStorage.getItem("nameglobal");
        var url;
        if (nameglobal == "roommaster") {
            // if (status == "active") {
            //     dataUrl = ipAddress + "/carehome/rooms/?is-active=1&is-deleted=0";
            // }
            // else if (status == "inactive") {
            //     dataUrl = ipAddress + "/carehome/rooms/?is-active=0&is-deleted=1";
            // }
            getTableValueList(status);
            // buildRoomListGrid();
            // getAjaxObject(dataUrl, "GET", getRoomList, onError);

        }
        else if (nameglobal == "residentterms") {
            if (status == "active") {
                dataUrl = ipAddress + "/carehome/resident-terms/?is-active=1&is-deleted=0";
            }
            else if (status == "inactive") {
                dataUrl = ipAddress + "/carehome/resident-terms/?is-active=0&is-deleted=1";
            }
            buildResisdentListGrid();
            getAjaxObject(dataUrl, "GET", getRoomList, onError);
        }
        else if (nameglobal == "fundings") {
            if (status == "active") {
                dataUrl = ipAddress + "/carehome/fundings/?is-active=1&is-deleted=0";
            }
            else if (status == "inactive") {
                dataUrl = ipAddress + "/carehome/fundings/?is-active=0&is-deleted=1";
            }
            buildFundingListGrid();
            getAjaxObject(dataUrl, "GET", getRoomList, onError);
        }
        else if (nameglobal == "remittance_status") {
            if (status == "active") {
                dataUrl = ipAddress + "/carehome/remittance-status/?is-active=1&is-deleted=0";
            }
            else if (status == "inactive") {
                dataUrl = ipAddress + "/carehome/remittance-status/?is-active=0&is-deleted=1";
            }
            buildRemittanceStatusListGrid();
            getAjaxObject(dataUrl, "GET", getRoomList, onError);
        }
        else if (nameglobal == "purchase_order_status") {
            if (status == "active") {
                dataUrl = ipAddress + "/carehome/purchase-order-status/?is-active=1&is-deleted=0";
            }
            else if (status == "inactive") {
                dataUrl = ipAddress + "/carehome/purchase-order-status/?is-active=0&is-deleted=1";
            }
            buildPurchaseListGrid();
            getAjaxObject(dataUrl, "GET", getRoomList, onError);
        }
        else if (nameglobal == "hospitals") {
            if (status == "active") {
                dataUrl = ipAddress + "/carehome/hospitals/?is-active=1&is-deleted=0";
            }
            else if (status == "inactive") {
                dataUrl = ipAddress + "/carehome/hospitals/?is-active=0&is-deleted=1";
            }
            buildHospitalListGrid([]);
            getAjaxObject(dataUrl, "GET", getRoomList, onError);
        }
        else if (nameglobal == "carehomemaster") {
            buildDeviceListGrid([]);
            if (status == "active") {
                dataUrl = ipAddress + "/carehome/care-homes/?is-active=1&is-deleted=0";
            }
            else if (status == "inactive") {
                dataUrl = ipAddress + "/carehome/care-homes/?is-deleted=1";
            }
            getAjaxObject(dataUrl, "GET", getCareHomeList, onError);
        }
        else if (nameglobal == "resisdentsmaster") {
            buildResisdentGrid([]);
            if (status == "active") {
                dataUrl = ipAddress + "/carehome/residents/?is-active=1&is-deleted=0";
            }
            else if (status == "inactive") {
                dataUrl = ipAddress + "/carehome/residents/?is-active=0&is-deleted=1";
            }
            getAjaxObject(dataUrl, "GET", getResisdentList, onError);
        }
        else if (nameglobal == "beds") {
            buildBedListGrid([]);
            if (status == "active") {
                dataUrl = ipAddress + "/carehome/beds/?is-active=1&is-deleted=0";
            }
            else if (status == "inactive") {
                dataUrl = ipAddress + "/carehome/beds/?is-active=0&is-deleted=1";
            }
            getAjaxObject(dataUrl, "GET", getBedList, onError);
        }
        else if (nameglobal == "bed_allocation") {
            buildBedAllocationListGrid([]);
            if (status == "active") {
                dataUrl = ipAddress + "/carehome/bed-allocations/?is-active=1&is-deleted=0&fields=*,careHome.*,funding.*,residentTerm.*,resident.*,bed.*";
            }
            else if (status == "inactive") {
                dataUrl = ipAddress + "/carehome/bed-allocations/?is-active=0&is-deleted=1&fields=*,careHome.*,funding.*,residentTerm.*,resident.*,bed.*";
            }
            getAjaxObject(dataUrl, "GET", getBedAllocationList, onError);
        }
        else if (nameglobal == "purchase_order") {
            buildPuchaseOrderListGrid([]);
            if (status == "active") {
                dataUrl = ipAddress + "/carehome/purchase-orders/?is-active=1&is-deleted=0&fields=*,hospital.*,purchaseOrderStatus.*,resident.*,careHome.*";
            }
            else if (status == "inactive") {
                dataUrl = ipAddress + "/carehome/purchase-orders/?is-active=0&is-deleted=1&fields=*,hospital.*,purchaseOrderStatus.*,resident.*,careHome.*";
            }
            getAjaxObject(dataUrl, "GET", getPurchaseOrderList, onError);
        }
        else if (nameglobal == "remittances") {
            buildRemittanceListGrid([]);
            if (status == "active") {
                dataUrl = ipAddress + "/carehome/remittances/?is-active=1&is-deleted=0&fields=*,resident.*,remittanceStatus.*,careHome.*";
            }
            else if (status == "inactive") {
                dataUrl = ipAddress + "/carehome/remittances/?is-active=0&is-deleted=1&fields=*,resident.*,remittanceStatus.*,careHome.*";
            }
            getAjaxObject(dataUrl, "GET", getRemittanceList, onError);
        }
        else if (nameglobal == "dols") {
            buildDolsListGrid([]);
            if (status == "active") {
                dataUrl = ipAddress + "/carehome/dols/?is-active=1&is-deleted=0&fields=*,resident.*,careHome.*";
            }
            else if (status == "inactive") {
                dataUrl = ipAddress + "/carehome/dols/?is-active=0&is-deleted=1&fields=*,resident.*,careHome.*";
            }
            getAjaxObject(dataUrl, "GET", getDolsList, onError);
        }

    }



    function onClickActive() {
        $(".btnInActive").removeClass("selectButtonBarClass");
        $(".btnActive").addClass("selectButtonBarClass");
    }

    function onClickInActive() {
        $(".btnActive").removeClass("selectButtonBarClass");
        $(".btnInActive").addClass("selectButtonBarClass");
    }

    function fncgetroom() {
        nameglobal = sessionStorage.getItem("nameglobal");
        var url;
        $('.sidebar-info-panel').width("90%");
        if (nameglobal == "roommaster") {
            url = ipAddress + "/carehome/rooms/?is-active=1&is-deleted=0";
            getAjaxObject(url, "GET", getRoomList, onError);
        }
        else if (nameglobal == "residentterms") {
            url = ipAddress + "/carehome/resident-terms/?is-active=1&is-deleted=0";
            getAjaxObject(url, "GET", getRoomList, onError);
        }
        else if (nameglobal == "fundings") {
            url = ipAddress + "/carehome/fundings/?is-active=1&is-deleted=0";
            getAjaxObject(url, "GET", getRoomList, onError);
        }
        else if (nameglobal == "remittance_status") {
            url = ipAddress + "/carehome/remittance-status/?is-active=1&is-deleted=0";
            getAjaxObject(url, "GET", getRoomList, onError);
        }
        else if (nameglobal == "purchase_order_status") {
            url = ipAddress + "/carehome/purchase-order-status/?is-active=1&is-deleted=0";
            getAjaxObject(url, "GET", getRoomList, onError);
        }
        else if (nameglobal == "hospitals") {
            url = ipAddress + "/carehome/hospitals/?is-active=1&is-deleted=0";
            getAjaxObject(url, "GET", getRoomList, onError);
        }
        else if (nameglobal == "carehomemaster") {
            // $('.sidebar-info-panel').addClass("width","100% !important");
            $('.sidebar-info-panel').width("100%");
            fncgetCareHome();
        }
        else if (nameglobal == "resisdentsmaster") {
            $('.sidebar-info-panel').width("100%");
            // fncgetCareHome();
            fncGetGender();
            getAjaxObject(ipAddress + "/master/SMS/list/", "GET", getSMSValueList, onError);
        }
        else if (nameglobal == "beds") {
            fncgetrooms();
        }
        else if (nameglobal == "bed_allocation") {
            fncgetrooms();
            fncgetBeds();
            fncgetrt();
            fncgetfunding();
        }
        else if (nameglobal == "purchase_order") {
            fncgetPurchase();
            fncgetrt();
            fncgethospitals();
        }
        else if (nameglobal == "remittances") {
            fncgetRemittanceStatus();
            // getResisdentList();
        }
    }

    function fncgetrt() {
        var url = ipAddress + "/carehome/resident-terms/?is-active=1&is-deleted=0";
        getAjaxObject(url, "GET", getRTList, onError);
    }

    function fncgetrooms() {
        url = ipAddress + "/carehome/rooms/?is-active=1&is-deleted=0";
        getAjaxObject(url, "GET", getRoomsList, onError);
    }


    function fncGetResisdentList() {
        url = ipAddress + "/carehome/residents/?is-active=1&is-deleted=0";
        getAjaxObject(url, "GET", getResisdentList, onError);
    }


    function fncgethospitals() {
        url = ipAddress + "/carehome/hospitals/?is-active=1&is-deleted=0";
        getAjaxObject(url, "GET", getHospitalsList, onError);
    }

    function fncgetfunding() {
        url = ipAddress + "/carehome/fundings/?is-active=1&is-deleted=0";
        getAjaxObject(url, "GET", getfundingsList, onError);
    }

    function fncgetRemittanceStatus() {
        url = ipAddress + "/carehome/remittance-status/?is-active=1&is-deleted=0";
        getAjaxObject(url, "GET", getRemittanceStatusList, onError);
    }

    function fncgetPurchase() {
        url = ipAddress + "/carehome/purchase-order-status/?is-active=1&is-deleted=0";
        getAjaxObject(url, "GET", getPurchaseList, onError);
    }

    function fncgetBeds() {
        url = ipAddress + "/carehome/beds/?is-active=1&is-deleted=0";
        getAjaxObject(url, "GET", getBedList, onError);
    }

    function fncgetBedAllocations() {
        url = ipAddress + "/carehome/bed-allocations/?is-active=1&is-deleted=0&fields=*,careHome.*,funding.*,residentTerm.*,resident.*,bed.*";
        getAjaxObject(url, "GET", getBedAllocationList, onError);
    }

    function fncgetRemittance() {
        url = ipAddress + "/carehome/remittances/?is-active=1&is-deleted=0&fields=*,resident.*,remittanceStatus.*,careHome.*";
        getAjaxObject(url, "GET", getRemittanceList, onError);
    }

    function fncgetPurchaseOrder() {
        url = ipAddress + "/carehome/purchase-orders/?is-active=1&is-deleted=0&fields=*,hospital.*,purchaseOrderStatus.*,resident.*,careHome.*";
        getAjaxObject(url, "GET", getPurchaseOrderList, onError);
    }

    function fncgetDols() {
        url = ipAddress + "/carehome/dols/?is-active=1&is-deleted=0&fields=*,resident.*,careHome.*";
        getAjaxObject(url, "GET", getDolsList, onError);
    }


    function getDolsList(dataObj) {
        var tempCompType = [];

        if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
            if (dataObj.response.dols) {
                if ($.isArray(dataObj.response.dols)) {
                    tempCompType = dataObj.response.dols;
                } else {
                    tempCompType.push(dataObj.response.dols);
                }
            }
        }

        for (var i = 0; i < tempCompType.length; i++) {
            tempCompType[i].idk = tempCompType[i].id;
            tempCompType[i].careHomeName = getRoomNameById(tempCompType[i].careHomeId);
        }

        buildDolsListGrid(tempCompType);
    }

    function getPurchaseOrderList(dataObj) {
        var tempCompType = [];

        if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
            if (dataObj.response.purchaseOrders) {
                if ($.isArray(dataObj.response.purchaseOrders)) {
                    tempCompType = dataObj.response.purchaseOrders;
                } else {
                    tempCompType.push(dataObj.response.purchaseOrders);
                }
            }
        }

        for (var i = 0; i < tempCompType.length; i++) {
            tempCompType[i].idk = tempCompType[i].id;
        }

        buildPuchaseOrderListGrid(tempCompType);
    }

    function getRemittanceList(dataObj) {
        var tempCompType = [];

        if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
            if (dataObj.response.remittances) {
                if ($.isArray(dataObj.response.remittances)) {
                    tempCompType = dataObj.response.remittances;
                } else {
                    tempCompType.push(dataObj.response.remittances);
                }
            }
        }

        for (var i = 0; i < tempCompType.length; i++) {
            tempCompType[i].idk = tempCompType[i].id;
            tempCompType[i].careHomeName = getRoomNameById(tempCompType[i].careHomeId);
        }

        buildRemittanceListGrid(tempCompType);
    }

    function getBedAllocationList(dataObj) {
        var tempCompType = [];

        if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
            if (dataObj.response.bedAllocations) {
                if ($.isArray(dataObj.response.bedAllocations)) {
                    tempCompType = dataObj.response.bedAllocations;
                } else {
                    tempCompType.push(dataObj.response.bedAllocations);
                }
            }
        }

        for (var i = 0; i < tempCompType.length; i++) {
            tempCompType[i].idk = tempCompType[i].id;
            tempCompType[i].careHomeName = getRoomNameById(tempCompType[i].careHomeId);
            tempCompType[i].bedName = getBedNameById(tempCompType[i].bedId);
        }

        buildBedAllocationListGrid(tempCompType);
    }

    function getRoomNameById(aId) {
        for (var i = 0; i < careHome.length; i++) {
            var item = careHome[i];
            if (item && item.Value == aId) {
                return item.Key;
            }
        }
        return "";
    }

    function getBedNameById(aId) {
        for (var i = 0; i < fncBeds.length; i++) {
            var item = fncBeds[i];
            if (item && item.Value == aId) {
                return item.Key;
            }
        }
        return "";
    }

    function getBedList(dataObj) {
        var tempCompType = [];
        fncBeds = [];
        beds = [];
        nameglobal = sessionStorage.getItem("nameglobal");
        if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
            if (dataObj.response.beds) {
                if ($.isArray(dataObj.response.beds)) {
                    tempCompType = dataObj.response.beds;
                    beds = dataObj.response.beds;
                } else {
                    tempCompType.push(dataObj.response.beds);
                    beds.push(dataObj.response.beds);
                }
            }
        }

        for (var i = 0; i < tempCompType.length; i++) {
            tempCompType[i].idk = tempCompType[i].id;
        }

        for (var i = 0; i < tempCompType.length; i++) {
            var obj = {};
            obj.Key = tempCompType[i].name;
            obj.Value = tempCompType[i].id;
            fncBeds.push(obj);
        }
        if(nameglobal == "bed_allocation"){
            onChangeRoom();
        }
        else if(nameglobal == "beds"){
            buildBedListGrid(tempCompType);
        }

        // setDataForSelection(fncBeds, "cmbBedId", onBedChange, ["Key", "Value"], 0, "");
    }


    function getPurchaseList(dataObj) {
        var tempCompType = [];
        puchaseOrder_status = [];
        if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
            if (dataObj.response.purchaseOrderStatus) {
                if ($.isArray(dataObj.response.purchaseOrderStatus)) {
                    tempCompType = dataObj.response.purchaseOrderStatus;
                } else {
                    tempCompType.push(dataObj.response.purchaseOrderStatus);
                }
            }
        }

        for (var i = 0; i < tempCompType.length; i++) {
            tempCompType[i].idk = tempCompType[i].id;
        }

        for (var i = 0; i < tempCompType.length; i++) {
            var obj = {};
            obj.Key = tempCompType[i].name;
            obj.Value = tempCompType[i].id;
            puchaseOrder_status.push(obj);
        }

        if(nameglobal == "purchase_order"){
            setDataForSelection(puchaseOrder_status, "cmbPurchaseOrderStatusId", onFundingChange, ["Key", "Value"], 0, "");
        }
        else{
            buildPurchaseListGrid(tempCompType);
        }
    }


    function getRemittanceStatusList(dataObj) {
        var tempCompType = [];
        remittance_status = [];

        if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
            if (dataObj.response.remittanceStatus) {
                if ($.isArray(dataObj.response.remittanceStatus)) {
                    tempCompType = dataObj.response.remittanceStatus;
                } else {
                    tempCompType.push(dataObj.response.remittanceStatus);
                }
            }
        }

        for (var i = 0; i < tempCompType.length; i++) {
            tempCompType[i].idk = tempCompType[i].id;
        }

        for (var i = 0; i < tempCompType.length; i++) {
            var obj = {};
            obj.Key = tempCompType[i].name;
            obj.Value = tempCompType[i].id;
            remittance_status.push(obj);
        }

        if(nameglobal == "remittances"){
            setDataForSelection(remittance_status, "cmbRemittanceStatusId", onFundingChange, ["Key", "Value"], 0, "");
        }
        else{
            buildRemittanceStatusListGrid(tempCompType);
        }
    }


    function getfundingsList(dataObj) {
        var tempCompType = [];
        fundings = [];
        nameglobal = sessionStorage.getItem("nameglobal");
        if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
            if (dataObj.response.fundings) {
                if ($.isArray(dataObj.response.fundings)) {
                    tempCompType = dataObj.response.fundings;
                } else {
                    tempCompType.push(dataObj.response.fundings);
                }
            }
        }

        for (var i = 0; i < tempCompType.length; i++) {
            tempCompType[i].idk = tempCompType[i].id;
        }

        for (var i = 0; i < tempCompType.length; i++) {
            var obj = {};
            obj.Key = tempCompType[i].name;
            obj.Value = tempCompType[i].id;
            fundings.push(obj);
        }

        if(nameglobal == "bed_allocation"){
            setDataForSelection(fundings, "cmbFundingId", onFundingChange, ["Key", "Value"], 0, "");
        }

        if(nameglobal == "fundings"){
            buildFundingListGrid(tempCompType);
        }
    }


    function getResisdentList(dataObj) {
        var tempCompType = [];
        resisdents = [];
        if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
            if (dataObj.response.residents) {
                if ($.isArray(dataObj.response.residents)) {
                    tempCompType = dataObj.response.residents;
                } else {
                    tempCompType.push(dataObj.response.residents);
                }
            }
        }

        for (var i = 0; i < tempCompType.length; i++) {
            tempCompType[i].idk = tempCompType[i].id;
        }

        for (var i = 0; i < tempCompType.length; i++) {
            var obj = {};
            obj.Key = tempCompType[i].firstName + " " + tempCompType[i].middleName + " " + tempCompType[i].lastName;
            obj.Value = tempCompType[i].id;
            resisdents.push(obj);
        }

        // if(nameglobal == "remittances"){
        //     setDataForSelection(resisdents, "cmdRemittanceResisdentId", onFundingChange, ["Key", "Value"], 0, "");
        //
        // }else{
            setDataForSelection(resisdents, "cmdResisdentId", onFundingChange, ["Key", "Value"], 0, "");
            // setDataForSelection(resisdents, "cmbPurchaseOrderResisdentId", onFundingChange, ["Key", "Value"], 0, "");
            setDataForSelection(resisdents, "cmbDolsResidentId", onFundingChange, ["Key", "Value"], 0, "");
            buildResisdentGrid(tempCompType);
        // }
    }

    function getHospitalsList(dataObj) {
        var tempCompType = [];
        hospitals = [];

        if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
            if (dataObj.response.hospitals) {
                if ($.isArray(dataObj.response.hospitals)) {
                    tempCompType = dataObj.response.hospitals;
                } else {
                    tempCompType.push(dataObj.response.hospitals);
                }
            }
        }

        for (var i = 0; i < tempCompType.length; i++) {
            tempCompType[i].idk = tempCompType[i].id;
        }


        for (var i = 0; i < tempCompType.length; i++) {
            var obj = {};
            obj.Key = tempCompType[i].name;
            obj.Value = tempCompType[i].id;
            hospitals.push(obj);
        }

        if(nameglobal == "purchase_order"){
            setDataForSelection(hospitals, "cmbHospitalId", onResisdentTermChange, ["Key", "Value"], 0, "");
        }else{
            buildHospitalListGrid(tempCompType);
        }
    }

    function getRTList(dataObj) {
        var tempCompType = [];
        resisdent_terms = [];

        if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
            if (dataObj.response.residentTerms) {
                if ($.isArray(dataObj.response.residentTerms)) {
                    tempCompType = dataObj.response.residentTerms;
                } else {
                    tempCompType.push(dataObj.response.residentTerms);
                }
            }
        }

        for (var i = 0; i < tempCompType.length; i++) {
            tempCompType[i].idk = tempCompType[i].id;
        }

        for (var i = 0; i < tempCompType.length; i++) {
            var obj = {};
            obj.Key = tempCompType[i].name;
            obj.Value = tempCompType[i].id;
            resisdent_terms.push(obj);
        }
        if (nameglobal != "carehomemaster") {
            setDataForSelection(resisdent_terms, "cmdResisdentTermId", onResisdentTermChange, ["Key", "Value"], 0, "");
            setDataForSelection(resisdent_terms, "cmdPurchaseResisdentTermId", onResisdentTermChange, ["Key", "Value"], 0, "");
        }
        buildResisdentListGrid(tempCompType);
    }

    function getRoomsList(dataObj) {
        var tempCompType = [];
        bedRooms = [];
        nameglobal = sessionStorage.getItem("nameglobal");

        if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
            if (dataObj.response.rooms) {
                if ($.isArray(dataObj.response.rooms)) {
                    tempCompType = dataObj.response.rooms;
                } else {
                    tempCompType.push(dataObj.response.rooms);
                }
            }
        }

        for (var i = 0; i < tempCompType.length; i++) {
            tempCompType[i].idk = tempCompType[i].id;
        }


        for (var i = 0; i < tempCompType.length; i++) {
            var obj = {};
            obj.Key = tempCompType[i].name;
            obj.Value = tempCompType[i].id;
            bedRooms.push(obj);
        }

        if (nameglobal != "carehomemaster") {
            setDataForSelection(bedRooms, "cmbBedRoom", onBedRoomChange, ["Key", "Value"], 0, "");
            setDataForSelection(bedRooms, "cmbRoomId", function() {
                onChangeRoom();
            }, ["Key", "Value"], 0, "");
        }

        if (nameglobal == "roommaster") {
            buildRoomListGrid(tempCompType);
        }
    }

    function onChangeRoom(){
        var cmbRoomId = $("#cmbRoomId").data("kendoComboBox");
        var roomId = Number(cmbRoomId.value());
        var roombeds = [];

        for (var i = 0; i < beds.length; i++) {
            if(beds[i].roomId == roomId){
                var obj = {};
                obj.Key = beds[i].name;
                obj.Value = beds[i].id;
                roombeds.push(obj)
            }
        }

        setDataForSelection(roombeds, "cmbBedId", onBedChange, ["Key", "Value"], 0, "");
    }

    function getRoomList(dataObj) {
        var tempCompType = [];
        //buildRoomListGrid([]);
        nameglobal = sessionStorage.getItem("nameglobal");
        if (nameglobal == "carehomemaster") {

        }
        else if (nameglobal == "roommaster") {
            if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
                if (dataObj.response.rooms) {
                    if ($.isArray(dataObj.response.rooms)) {
                        tempCompType = dataObj.response.rooms;
                    } else {
                        tempCompType.push(dataObj.response.rooms);
                    }
                }
            }

            for (var i = 0; i < tempCompType.length; i++) {
                tempCompType[i].idk = tempCompType[i].id;
            }

            buildRoomListGrid(tempCompType);
        }
        else if (nameglobal == "residentterms") {
            if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
                if (dataObj.response.residentTerms) {
                    if ($.isArray(dataObj.response.residentTerms)) {
                        tempCompType = dataObj.response.residentTerms;
                    } else {
                        tempCompType.push(dataObj.response.resident - terms);
                    }
                }
            }

            for (var i = 0; i < tempCompType.length; i++) {
                tempCompType[i].idk = tempCompType[i].id;
            }

            buildResisdentListGrid(tempCompType);
        }
        else if (nameglobal == "fundings") {
            fundings = []
            if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
                if (dataObj.response.fundings) {
                    if ($.isArray(dataObj.response.fundings)) {
                        tempCompType = dataObj.response.fundings;
                    } else {
                        tempCompType.push(dataObj.response.fundings);
                    }
                }
            }

            for (var i = 0; i < tempCompType.length; i++) {
                tempCompType[i].idk = tempCompType[i].id;
            }

            buildFundingListGrid(tempCompType);


        }
        else if (nameglobal == "remittance_status") {
            if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
                if (dataObj.response.remittanceStatus) {
                    if ($.isArray(dataObj.response.remittanceStatus)) {
                        tempCompType = dataObj.response.remittanceStatus;
                    } else {
                        tempCompType.push(dataObj.response.remittanceStatus);
                    }
                }
            }

            for (var i = 0; i < tempCompType.length; i++) {
                tempCompType[i].idk = tempCompType[i].id;
            }

            buildRemittanceStatusListGrid(tempCompType);
        }
        else if (nameglobal == "purchase_order_status") {
            if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
                if (dataObj.response.purchaseOrderStatus) {
                    if ($.isArray(dataObj.response.purchaseOrderStatus)) {
                        tempCompType = dataObj.response.purchaseOrderStatus;
                    } else {
                        tempCompType.push(dataObj.response.purchaseOrderStatus);
                    }
                }
            }

            for (var i = 0; i < tempCompType.length; i++) {
                tempCompType[i].idk = tempCompType[i].id;
            }

            buildPurchaseListGrid(tempCompType);
        }
        else if (nameglobal == "hospitals") {
            if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
                if (dataObj.response.hospitals) {
                    if ($.isArray(dataObj.response.hospitals)) {
                        tempCompType = dataObj.response.hospitals;
                    } else {
                        tempCompType.push(dataObj.response.hospitals);
                    }
                }
            }

            for (var i = 0; i < tempCompType.length; i++) {
                tempCompType[i].idk = tempCompType[i].id;
            }

            buildHospitalListGrid(tempCompType);
        }


    }

    function fncgetdetails() {
        nameglobal = sessionStorage.getItem("nameglobal");
        if (nameglobal == "carehomemaster") {
            saveCareHome();
        }
        else if (nameglobal == "roommaster") {
            saveRoom();
        }
        else if (nameglobal == "residentterms") {
            saveResisdentTerms();
        }
        else if (nameglobal == "fundings") {
            saveRoom();
        }
        else if (nameglobal == "remittance_status") {
            saveRoom();
        }
        else if (nameglobal == "purchase_order_status") {
            saveRoom();
        }
        else if (nameglobal == "hospitals") {
            saveRoom();

        }
        else if (nameglobal == "resisdentsmaster") {
            saveResisdent();
        }
        else if (nameglobal == "beds") {
            saveBed();
        }
        else if (nameglobal == "bed_allocation") {
            saveBedAllocation();
        }
        else if (nameglobal == "remittances") {
            saveRemittanceList();
        }
        else if (nameglobal == "purchase_order") {
            savePurchaseOrder();
        }
        else if (nameglobal == "dols") {
            saveDols();
        }
        searchOnLoad('active');
    }

function saveDols() {
    var cmbDolsCareHomeId = $("#cmbDolsCareHomeId").data("kendoComboBox");
    var cmbDOLStatus = $("#cmbDOLStatus").data("kendoComboBox");

    var isActive = 0;
    if (cmbDOLStatus) {
        if (cmbDOLStatus.selectedIndex == 0) {
            isActive = 1;
        }
    }

    var dataUrl = ipAddress + "/carehome/dols/";
    var method = operation;
    var reqObj = {};
    reqObj.isDeleted = 0;
    reqObj.createdBy = parseInt(sessionStorage.userId);
    reqObj.dateGenerated = GetDateTime("txtDateGenerated");
    reqObj.dateExpired = GetDateTime("txtDateExpired");;
    reqObj.careHomeId = Number(cmbDolsCareHomeId.value());
    reqObj.name = $('#txtDolName').val();
    reqObj.residentId = dolsResidentId;
    reqObj.safeguard = $('#txtSafeGuard').val();
    reqObj.isActive = 1;
    if (operation.toLowerCase() == "put") {
        reqObj.id = $('#txtDolsID').val();
    }
    createAjaxObject(dataUrl, reqObj, method, onCreate, onError);
    //fncgetroom();
}


function saveRemittance() {
    var cmbRemittanceCareHomeId = $("#cmbRemittanceCareHomeId").data("kendoComboBox");
    var cmdRemittanceResisdentId = $("#cmdRemittanceResisdentId").data("kendoComboBox");

    var cmbRemittanceStatusId = $("#cmbRemittanceStatusId").data("kendoComboBox");
    var cmbRemittancesStatus = $("#cmbRemittancesStatus").data("kendoComboBox");

    var isActive = 0;
    if (cmbRemittancesStatus) {
        if (cmbRemittancesStatus.selectedIndex == 0) {
            isActive = 1;
        }
    }

    var dataUrl = ipAddress + "/carehome/dols/";
    var method = operation;
    var reqObj = {};
    reqObj.isDeleted = 0;
    reqObj.createdBy = parseInt(sessionStorage.userId);
    reqObj.toDate = null;
    reqObj.remittanceStatusId = Number(cmbRemittanceStatusId.value());
    reqObj.fromDate = null;
    reqObj.createdDate = null;
    reqObj.careHomeId = Number(cmbRemittanceCareHomeId.value());
    reqObj.residentId = Number(cmdRemittanceResisdentId.value());
    reqObj.bandingRate = $('#txtRemittanceBandingRate').val();
    reqObj.isActive = isActive;
    if (operation.toLowerCase() == "put") {
        reqObj.id = $('#txtDolsID').val();
    }
    createAjaxObject(dataUrl, reqObj, method, onCreate, onError);
    // fncgetroom();
}

function saveResisdentTerms() {
        var cmbrtCareHome = $("#cmbrtCareHome").data("kendoComboBox");
        var cmbCareHome = $("#cmbCareHome").data("kendoComboBox");

        var cmbStatus = $("#cmbStatus").data("kendoComboBox");

        var isActive = 0;
        if (cmbStatus) {
            if (cmbStatus.selectedIndex == 0) {
                isActive = 1;
            }
        }


        var dataUrl = ipAddress + "/carehome/resident-terms/";
        var method = operation;
        var reqObj = {};
        reqObj.isDeleted = 0;
        reqObj.createdBy = parseInt(sessionStorage.userId);
        reqObj.name = $('#roomName').val();
        reqObj.careHomeId = Number(cmbCareHome.value());
        reqObj.isActive = isActive;
        if (operation.toLowerCase() == "put") {
            reqObj.id = $('#roomid').val();
        }
        createAjaxObject(dataUrl, reqObj, method, onCreate, onError);
        // fncgetroom();
    }

    function saveRoom() {
        nameglobal = sessionStorage.getItem("nameglobal");

        var cmbcarehome = $("#cmbCareHome").data("kendoComboBox");
        var cmbStatus = $("#cmbStatus").data("kendoComboBox");

        var isActive = 0;
        var cmbStatus = $("#cmbStatus").data("kendoComboBox");
        if (cmbStatus) {
            if (cmbStatus.selectedIndex == 0) {
                isActive = 1;
            }
        }

        var dataUrl;
        if (nameglobal == "roommaster") {
            dataUrl = ipAddress + "/carehome/rooms/";
        }
        else if (nameglobal == "fundings") {
            dataUrl = ipAddress + "/carehome/fundings/";
        }
        else if (nameglobal == "residentterms") {
            dataUrl = ipAddress + "/carehome/resident-terms/";
        }
        else if (nameglobal == "remittance_status") {
            dataUrl = ipAddress + "/carehome/remittance-status/";
        }
        else if (nameglobal == "purchase_order_status") {
            dataUrl = ipAddress + "/carehome/purchase-order-status/";
        }
        else if (nameglobal == "hospitals") {
            dataUrl = ipAddress + "/carehome/hospitals/";
        }
        else if (nameglobal == "beds") {
            dataUrl = ipAddress + "/carehome/beds/";
        }


        //var dataUrl = ipAddress+"/carehome/rooms/";
        var method = operation;
        var reqObj = {};
        reqObj.isDeleted = 0;
        reqObj.createdBy = parseInt(sessionStorage.userId);
        reqObj.name = $('#roomName').val();
        reqObj.careHomeId = Number(cmbcarehome.value());
        reqObj.isActive = isActive;
        if (operation.toLowerCase() == "put") {
            reqObj.id = $('#roomid').val();
        }
        createAjaxObject(dataUrl, reqObj, method, onCreate, onError);
        fncgetroom();
    }

    function fncGetGender() {
        getAjaxObject(ipAddress + "/master/Gender/list/", "GET", getGenderValueList, onError);
    }

    var genderData;
    function getGenderValueList(dataObj) {
        nameglobal = sessionStorage.getItem("nameglobal");
        var dArray = getTableListArray(dataObj);
        if (dArray && dArray.length > 0) {
            genderData = dArray;
            if (nameglobal == "resisdentsmaster") {
                setDataForSelection(dArray, "resisdentGender", onResidentGenderChange, ["desc", "idk"], 0, "");
            }
            else {
                setDataForSelection(dArray, "cmbGender", onGenderChange, ["desc", "idk"], 0, "");
                // setDataForSelection(dArray, "cmdResisdentGender", onGenderChange, ["desc", "idk"], 0, "");
                // setDataForSelection(dArray, "cmdPurchaseResisdentGender", onGenderChange, ["desc", "idk"], 0, "");
                setDataForSelection(dArray, "cmdDOLResisdentGender", onGenderChange, ["desc", "idk"], 0, "");

            }
        }
    }

    function onGenderChange() {
        onComboChange("cmbGender");
    }

    function onResidentGenderChange() {
        onComboChange("resisdentGender");
    }

    function onStatusChange() {
        var cmbStatus = $("#cmbStatus").data("kendoComboBox");
        if (cmbStatus && cmbStatus.selectedIndex < 0) {
            cmbStatus.select(0);
        }
    }

    function getActivityNameById(aId) {
        for (var i = 0; i < careHome.length; i++) {
            var item = careHome[i];
            if (item && item.Value == aId) {
                return i;
            }
        }
        return 0;
    }

    function getRemittanceStatusById(aId) {
        for (var i = 0; i < remittance_status.length; i++) {
            var item = remittance_status[i];
            if (item && item.Value == aId) {
                return i;
            }
        }
        return 0;
    }

    function getPOStatusById(aId) {
        for (var i = 0; i < puchaseOrder_status.length; i++) {
            var item = puchaseOrder_status[i];
            if (item && item.Value == aId) {
                return i;
            }
        }
        return 0;
    }

    function getHospitalById(aId) {
        for (var i = 0; i < hospitals.length; i++) {
            var item = hospitals[i];
            if (item && item.Value == aId) {
                return i;
            }
        }
        return 0;
    }





    function getBedById(aId) {
        for (var i = 0; i < fncBeds.length; i++) {
            var item = fncBeds[i];
            if (item && item.Value == aId) {
                return i;
            }
        }
        return 0;
    }

    function getRoomByBedId(aId) {
        for (var i = 0; i < beds.length; i++) {
            var item = beds[i].id;
            if (item && item == aId) {
                return getBedRoomById(beds[i].roomId);
            }
        }
        return 0;
    }

    function getGenderNameById(aId) {
        for (var i = 0; i < genderData.length; i++) {
            var item = genderData[i];
            if (item && item.desc == aId) {
                return i;
            }
        }
        return 0;
    }


    function getBedRoomById(aId) {
        for (var i = 0; i < bedRooms.length; i++) {
            var item = bedRooms[i];
            if (item && item.Value == aId) {
                return i;
            }
        }
        return 0;
    }

    function getfundingById(aId) {
        for (var i = 0; i < fundings.length; i++) {
            var item = fundings[i];
            if (item && item.Value == aId) {
                return i;
            }
        }
        return 0;
    }

    function getResidentTermsById(aId) {
        for (var i = 0; i < resisdent_terms.length; i++) {
            var item = resisdent_terms[i];
            if (item && item.Value == aId) {
                return i;
            }
        }
        return 0;
    }

    function setComboReset(cmbId) {
        var cmb = $("#" + cmbId).data("kendoComboBox");
        if (cmb) {
            cmb.select(0);
        }
    }


    function reset() {
        var sessionSelectterm = sessionStorage.nameglobal;
        if(sessionSelectterm == "carehomemaster"){
            $('#careHomeName').val('');
            $('#careHomeAbbreviation').val('');
            $('input[name="care-phone"]').val('');
            $('input[name="care-address1"]').val('');
            $('input[name="care-address2"]').val('');
            $('input[name="care-email"]').val('');
            $('input[name="care-city"]').val('');
            $('input[name="care-state"]').val('');
            $('input[name="care-country"]').val('');
            $('input[name="care-postalcode"]').val('');
            $('input[name="careManagement-lastname"]').val('');
            $('input[name="careManagement-firstname"]').val('');
            $('input[name="careManagement-phone"]').val('');
            $('input[name="careManagement-email"]').val('');
            $('input[name="careAdmin-lastname"]').val('');
            $('input[name="careAdmin-firstname"]').val('');
            $('input[name="careAdmin-phone"]').val('');
            $('input[name="careAdmin-email"]').val('');
            $('input[name="care-website"]').val('');
            $('input[name="care-notes"]').val('');
            $('input[name="care-fax"]').val('');
        }
        else if(nameglobal == "bed_allocation"){
            $('#txtBedAllocationId').val('');
            setComboReset("cmbBedAllocStatus");
            setComboReset("cmbCareHomeId");
            setComboReset("cmbRoomId");
            setComboReset("cmbBedId");
            setComboReset("cmbBedAllocStatus");
            $('#cmdResisdentId').val('');
            $('#txtResidentDOB').val('');
            $('#txtResidentGender').val('');
            setComboReset("cmbFundingId");
            setComboReset("cmdResisdentTermId");

            $("#txtFromDate").datepicker();
            $("#txtFromDate").datepicker("setDate", "");
            $("#txtToDate").datepicker();
            $("#txtToDate").datepicker("setDate", "");
            $("#txtOccupiedDate").datepicker();
            $("#txtOccupiedDate").datepicker("setDate", "");
            $("#txtVacantDate").datepicker();
            $("#txtVacantDate").datepicker("setDate", "");
        }
        else if(nameglobal == "resisdentsmaster") {
            $('#residentID').val('');
            setComboReset("resisdentCareHomeId");
            setComboReset("residents-status");
            $("#resisdentBirthDate").datepicker();
            $("#resisdentBirthDate").datepicker("setDate", "");
            setComboReset("resisdentGender");
            $('input[name="residents-fname"]').val('');
            $('input[name="residents-mname"]').val('');
            $('input[name="residents-lname"]').val('');
            $('#txtAdd1').val('');
            $('#txtAdd2').val('');
            $('#txtCity').val('');
            $('#txtState').val('');
            $('#txtCountry').val('');
            $('#cmbZip').val('');
            $('#txtHPhone').val('');
            $('#txtCell').val('');
            $('#txtEmail').val('');
            $('#txtWP').val('');
            setComboReset("cmbSMS");
        }
        else if(nameglobal == "purchase_order") {
            $('#txtPurchaseOrderID').val('');
            setComboReset("cmbPurchaseOrderStatus");
            setComboReset("cmbPurchaseCareHomeId");
            setComboReset("cmbPurchaseOrderStatusId");
            setComboReset("cmbHospitalId");
            $('#txtPurchaseOrderNumber').val('');
            $('#txtPurchaseOrderResisdentId').val('');
            $('#txtPurchaseResisdentDOB').val('');
            $('#txtPurchaseResisdentGender').val('');
            setComboReset("cmdPurchaseResisdentTermId");
        }
        else if(nameglobal == "remittances"){
            $('#txtRemittanceID').val('');
            setComboReset("cmbRemittancesStatus");
            setComboReset("cmbRemittanceCareHomeId");
            $('#cmbRemittanceName').val('');
            setComboReset("cmbRemittanceStatusId");
            // setComboReset("cmdRemittanceResisdentId");
            $('#cmdRemittanceResisdentId').val('');
            $("#txtRemittanceFromDate").datepicker();
            $("#txtRemittanceFromDate").datepicker("setDate", "");
            $("#txtRemittanceToDate").datepicker();
            $("#txtRemittanceToDate").datepicker("setDate", "");

            $('#txtRemiitanceResidentDOB').val('');
            $('#txtRemittanceResidentGender').val('');

            $('#txtTopupRate').val('');
            $('#txtRemittanceBandingRate').val('');
        }
        else if(nameglobal == "dols"){
            $('#txtDolsID').val('');
            $('#cmbDolsResidentId').val('');
            $('#txtDOLResisdentDOB').val('');
            $('#txtDOLResisdentGender').val('');
            $('#txtDolName').val('');
            $('#txtSafeGuard').val('');
            $("#txtDateGenerated").datepicker();
            $("#txtDateGenerated").datepicker("setDate", "");
            $("#txtDateExpired").datepicker();
            $("#txtDateExpired").datepicker("setDate", "");

            setComboReset("cmbDOLStatus");
            setComboReset("cmbDolsCareHomeId");


        }
        else {
            $('#roomid').val('');
            $('#roomName').val('');
            var cmbStatus = $("#cmbStatus").data("kendoComboBox");
            if (cmbStatus) {
                cmbStatus.select(0);
                cmbStatus.enable(false);
            }
            var cmbType = $("#cmbCareHome").data("kendoComboBox");
            if (cmbType) {
                cmbType.select(0);
            }
        }

    }


    function saveResisdent() {

        var resisdentCareHomeId = $("#resisdentCareHomeId").data("kendoComboBox");
        var residents_status = $("#residents-status").data("kendoComboBox");
        var dt;
        var strDate = "";
        var dt = document.getElementById("resisdentBirthDate").value;
        var dob = null;
        var mm = 0;
        var dd = 0;
        var yy = 0;
        var cntry = sessionStorage.countryName;
        if (cntry.indexOf("India") >= 0) {
            var dtArray = dt.split("/");
            dob = (dtArray[0] + "/" + dtArray[1] + "/" + dtArray[2]);
        } else if (cntry.indexOf("United Kingdom") >= 0) {
            var dtArray = dt.split("/");
            dob = (dtArray[1] + "/" + dtArray[0] + "/" + dtArray[2]);
        } else {
            var dtArray = dt.split("/");
            dob = (dtArray[0] + "/" + dtArray[1] + "/" + dtArray[2]);
        }
        if (dob) {
            var DOB = new Date(dob);
            strDate = kendo.toString(DOB, "yyyy-MM-dd");
        }


        // if (strDate) {
        //     var stDate = new Date(strDate);
        //     var currDate = new Date();
        //     if (currDate.getTime() < stDate.getTime()) {
        //         var strMsg = "Invalid Date of Birth ";
        //         $('.customAlert').append('<div class="alert alert-danger">' + strMsg + '</div>');
        //         return false;
        //     }
        // }
        var expiryDateVal = new Date(dt).getTime();

        // var resisdentBirthDate = $("#resisdentBirthDate").data("kendoDatePicker");
        // if (resisdentBirthDate) {
        //     dt = resisdentBirthDate.value();
        // }

        var isActive = 0;
        var resisdentGender = $("#resisdentGender").data("kendoComboBox");
        if (residents_status) {
            if (residents_status.selectedIndex == 0) {
                isActive = 1;
            }
        }

        var dataUrl = ipAddress + "/carehome/residents/";

        var method = operation;
        var reqObj = {};
        reqObj.isDeleted = 0;
        reqObj.createdBy = parseInt(sessionStorage.userId);
        reqObj.lastName = $('#residents-lname').val();
        reqObj.gender = resisdentGender.text();
        reqObj.dateOfBirth = expiryDateVal;
        reqObj.firstName = $('#residents-fname').val();
        reqObj.middleName = $('#residents-mname').val();
        reqObj.careHomeId = Number(resisdentCareHomeId.value());
        reqObj.isActive = isActive;
        if (operation.toLowerCase() == "put") {
            reqObj.id = $('#residentID').val();
        }
        createAjaxObject(dataUrl, reqObj, method, getResisdentCommunictiondetails, onError);
    }


    function saveRemittanceList() {
        var cmbRemittanceStatusId = $("#cmbRemittanceStatusId").data("kendoComboBox");
        var cmbRemittancesStatus = $("#cmbRemittancesStatus").data("kendoComboBox");
        var cmbRemittanceCareHomeId = $("#cmbRemittanceCareHomeId").data("kendoComboBox");
        var isActive = 0;
        if (cmbRemittancesStatus) {
            if (cmbRemittancesStatus.selectedIndex == 0) {
                isActive = 1;
            }
        }

        var dataUrl = ipAddress + "/carehome/remittances/";

        var method = operation;
        var reqObj = {};
        reqObj.isDeleted = 0;
        reqObj.createdBy = parseInt(sessionStorage.userId);
        reqObj.toDate = GetDateTime("txtRemittanceToDate");
        // reqObj.toDate = null;
        reqObj.remittanceStatusId = Number(cmbRemittanceStatusId.value());
        // reqObj.fromDate = null;
        reqObj.fromDate = GetDateTime("txtRemittanceFromDate");
        reqObj.careHomeId = Number(cmbRemittanceCareHomeId.value());
        reqObj.residentId = remittanceResidentId;
        reqObj.topUpRate = parseInt($('#txtTopupRate').val());
        reqObj.bandingRate = parseInt($('#txtRemittanceBandingRate').val());
        reqObj.isActive = isActive;
        reqObj.createdDate = null;
        if (operation.toLowerCase() == "put") {
            reqObj.id = $('#txtRemittanceID').val();
        }
        createAjaxObject(dataUrl, reqObj, method, onCreate, onError);
    }

    function saveBedAllocation() {

        var cmbFundingId = $("#cmbFundingId").data("kendoComboBox");
        var cmbBedId = $("#cmbBedId").data("kendoComboBox");
        var cmbBedAllocStatus = $("#cmbBedAllocStatus").data("kendoComboBox");
        var cmbCareHomeId = $("#cmbCareHomeId").data("kendoComboBox");
        var cmdResisdentTermId = $("#cmdResisdentTermId").data("kendoComboBox");
        // var cmdResisdentId = $("#cmdResisdentId").data("kendoComboBox");
        var isActive = 0;
        if (cmbBedAllocStatus) {
            if (cmbBedAllocStatus.selectedIndex == 0) {
                isActive = 1;
            }
        }

        var dataUrl = ipAddress + "/carehome/bed-allocations/";

        var method = operation;
        var reqObj = {};
        reqObj.isDeleted = 0;
        reqObj.createdBy = parseInt(sessionStorage.userId);
        reqObj.fundingId = Number(cmbFundingId.value());
        reqObj.toDate = GetDateTime("txtToDate");
        reqObj.bedId = Number(cmbBedId.value());
        reqObj.vacantDate = GetDateTime("txtVacantDate");
        reqObj.fromDate = GetDateTime("txtFromDate");
        reqObj.careHomeId = Number(cmbCareHomeId.value());
        reqObj.residentTermId = Number(cmdResisdentTermId.value());
        reqObj.residentId = bedAllocationResidentId;
        reqObj.occupiedDate = GetDateTime("txtOccupiedDate");
        reqObj.isActive = isActive;
        if (operation.toLowerCase() == "put") {
            reqObj.id = $('#txtBedAllocationId').val();
        }
        createAjaxObject(dataUrl, reqObj, method, onCreate, onError);
        //fncOnGetDetails();
    }

    function GetDateTime(Id){
        var strDate = "";
        var dt = document.getElementById(''+ Id +'').value;
        var dob = null;
        var mm = 0;
        var dd = 0;
        var yy = 0;
        var cntry = sessionStorage.countryName;
        if (cntry.indexOf("India") >= 0) {
            var dtArray = dt.split("/");
            dob = (dtArray[0] + "/" + dtArray[1] + "/" + dtArray[2]);
        } else if (cntry.indexOf("United Kingdom") >= 0) {
            var dtArray = dt.split("/");
            dob = (dtArray[1] + "/" + dtArray[0] + "/" + dtArray[2]);
        } else {
            var dtArray = dt.split("/");
            dob = (dtArray[0] + "/" + dtArray[1] + "/" + dtArray[2]);
        }
        if (dob) {
            var DOB = new Date(dob);
            strDate = kendo.toString(DOB, "yyyy-MM-dd");
        }

        if (strDate) {
            var stDate = new Date(strDate);
            var currDate = new Date();
            if (currDate.getTime() < stDate.getTime()) {
                var strMsg = "Invalid Date of Birth ";
                $('.customAlert').append('<div class="alert alert-danger">' + strMsg + '</div>');
                return false;
            }
        }

        var returnValue = new Date(dt).getTime();

        return returnValue;

    }

    function savePurchaseOrder() {
        var cmbHospitalId = $("#cmbHospitalId").data("kendoComboBox");
        var cmbPurchaseOrderStatusId = $("#cmbPurchaseOrderStatusId").data("kendoComboBox");
        var cmbPurchaseCareHomeId = $("#cmbPurchaseCareHomeId").data("kendoComboBox");
        // var txtPurchaseOrderResisdentId = $("#txtPurchaseOrderResisdentId").data("kendoComboBox");
        var cmbPurchaseOrderStatus = $("#cmbPurchaseOrderStatus").data("kendoComboBox");

        var isActive = 0;
        if (cmbPurchaseOrderStatus) {
            if (cmbPurchaseOrderStatus.selectedIndex == 0) {
                isActive = 1;
            }
        }

        var dataUrl = ipAddress + "/carehome/purchase-orders/";

        var method = operation;
        var reqObj = {};
        reqObj.isDeleted = 0;
        reqObj.createdBy = parseInt(sessionStorage.userId);
        reqObj.hospitalId = Number(cmbHospitalId.value());
        reqObj.purchaseOrderStatusId = Number(cmbPurchaseOrderStatusId.value());
        reqObj.purchaseOrderNumber = $('#txtPurchaseOrderNumber').val();
        reqObj.careHomeId = Number(cmbPurchaseCareHomeId.value());
        reqObj.residentId = purchaseOrderResidentId;
        reqObj.isActive = isActive;
        if (operation.toLowerCase() == "put") {
            reqObj.id = $('#txtPurchaseOrderID').val();
        }
        createAjaxObject(dataUrl, reqObj, method, onCreate, onError);
    }

    function fncOnGetDetails() {
        nameglobal = sessionStorage.getItem("nameglobal");
        var url;

        if (nameglobal == "roommaster") {
            url = ipAddress + "/carehome/rooms/?is-active=1&is-deleted=0";
            getAjaxObject(url, "GET", getRoomList, onError);
        }
        else if (nameglobal == "residentterms") {
            url = ipAddress + "/carehome/resident-terms/?is-active=1&is-deleted=0";
            getAjaxObject(url, "GET", getRoomList, onError);
        }
        else if (nameglobal == "fundings") {
            url = ipAddress + "/carehome/fundings/?is-active=1&is-deleted=0";
            getAjaxObject(url, "GET", getRoomList, onError);
        }
        else if (nameglobal == "remittance_status") {
            url = ipAddress + "/carehome/remittance-status/?is-active=1&is-deleted=0";
            getAjaxObject(url, "GET", getRoomList, onError);
        }
        else if (nameglobal == "purchase_order_status") {
            url = ipAddress + "/carehome/purchase-order-status/?is-active=1&is-deleted=0";
            getAjaxObject(url, "GET", getRoomList, onError);
        }
        else if (nameglobal == "hospitals") {
            url = ipAddress + "/carehome/hospitals/?is-active=1&is-deleted=0";
            getAjaxObject(url, "GET", getRoomList, onError);
        }
        else if (nameglobal == "carehomemaster") {
            fncgetCareHome();
        }
        else if (nameglobal == "resisdentsmaster") {
            fncGetResisdentList();
        }
        else if (nameglobal == "beds") {
            fncgetBeds();
        }
    }

    function getComboListIndex(cmbId, attr, attrVal) {
        var cmb = $("#" + cmbId).data("kendoComboBox");
        if (cmb) {
            var ds = cmb.dataSource;
            var totalRec = ds.total();
            for (var i = 0; i < totalRec; i++) {
                var dtItem = ds.at(i);
                if (dtItem && dtItem[attr].toLowerCase() == attrVal.toLowerCase()) {
                    cmb.select(i);
                    return i;
                }
            }
        }
        return -1;
    }

    function getTableListArray(dataObj) {
        var dataArray = [];
        if (dataObj && dataObj.response && dataObj.response.codeTable) {
            if ($.isArray(dataObj.response.codeTable)) {
                dataArray = dataObj.response.codeTable;
            } else {
                dataArray.push(dataObj.response.codeTable);
            }
        }
        var tempDataArry = [];
        var obj = {};
        obj.desc = "";
        obj.zip = "";
        obj.value = "";
        obj.idk = "";
        tempDataArry.push(obj);
        for (var i = 0; i < dataArray.length; i++) {
            if (dataArray[i].isActive == 1) {
                var obj = dataArray[i];
                obj.idk = dataArray[i].id;
                obj.status = dataArray[i].Status;
                tempDataArry.push(obj);
            }
        }
        return tempDataArry;
    }

    function saveBed() {
        nameglobal = sessionStorage.getItem("nameglobal");

        var bedCareHome = $("#bedCareHome").data("kendoComboBox");
        var isActive = 0;
        var cmbBedListStatus = $("#cmbBedListStatus").data("kendoComboBox");
        if (cmbBedListStatus) {
            if (cmbBedListStatus.selectedIndex == 0) {
                isActive = 1;
            }
        }

        var cmbBedRoom = $("#cmbBedRoom").data("kendoComboBox");
        var dataUrl;
        dataUrl = ipAddress + "/carehome/beds/";
        var method = operation;
        var reqObj = {};
        reqObj.isDeleted = 0;
        reqObj.createdBy = parseInt(sessionStorage.userId);
        reqObj.name = $('#bedName').val();
        // reqObj.roomId = Number(cmbBedRate.value());
        reqObj.rate = parseInt($('#bedRate').val());
        reqObj.careHomeId = Number(bedCareHome.value());
        reqObj.roomId = Number(cmbBedRoom.value());
        reqObj.isActive = isActive;
        if (operation.toLowerCase() == "put") {
            reqObj.id = $('#bedid').val();
        }
        createAjaxObject(dataUrl, reqObj, method, onCreate, onError);
        searchOnLoad('active')
    }

    function collapseMasterMenu() {
    $("#addMasterMenu").removeClass("in");
    $("#addMasterMenu").addClass("out");
}

    function getTableValueList(status){
    nameglobal = sessionStorage.getItem("nameglobal");
    var url;

    if (nameglobal == "roommaster") {
        var txtTableName = $("#cmbCareHomeName").data("kendoComboBox");
        buildRoomListGrid([]);
        if(txtTableName && txtTableName.text() != ""){
            ///gender/list
            var strTableName = txtTableName.value();
            if(status == "active") {
                getAjaxObject(ipAddress+"/carehome/rooms/?is-active=1&is-deleted=0&careHomeId="+strTableName,"GET",getRoomList,onError);
            }
            else if(status == "inactive"){
                getAjaxObject(ipAddress+"/carehome/rooms/?is-active=0&is-deleted=1&careHomeId="+strTableName,"GET",getRoomList,onError);
            }
        }
    }
    else if (nameglobal == "residentterms") {
        var txtTableName = $("#cmbRTCareHomeName").data("kendoComboBox");
        //buildRoomListGrid([]);
        if(txtTableName && txtTableName.text() != ""){
            ///gender/list
            var strTableName = txtTableName.value();
            if(status == "active") {
                getAjaxObject(ipAddress+"/carehome/resident-terms/?is-active=1&is-deleted=0&careHomeId="+strTableName,"GET",getRTList,onError);
            }
            else if(status == "inactive"){
                getAjaxObject(ipAddress+"/carehome/resident-terms/?is-active=0&is-deleted=1&careHomeId="+strTableName,"GET",getRTList,onError);
            }
        }
    }
    else if (nameglobal == "fundings") {
        var txtTableName = $("#cmbFundingCareHomeName").data("kendoComboBox");
        //buildRoomListGrid([]);
        if(txtTableName && txtTableName.text() != ""){
            var strTableName = txtTableName.value();
            if(status == "active") {
                getAjaxObject(ipAddress+"/carehome/fundings/?is-active=1&is-deleted=0&careHomeId="+strTableName,"GET",getfundingsList,onError);
            }
            else if(status == "inactive"){
                getAjaxObject(ipAddress+"/carehome/fundings/?is-active=0&is-deleted=1&careHomeId="+strTableName,"GET",getfundingsList,onError);
            }
        }
    }
    else if (nameglobal == "remittance_status") {
        var txtTableName = $("#cmbRSeHomeName").data("kendoComboBox");
        //buildRoomListGrid([]);
        if(txtTableName && txtTableName.text() != ""){
            var strTableName = txtTableName.value();
            if(status == "active") {
                getAjaxObject(ipAddress+"/carehome/remittance-status/?is-active=1&is-deleted=0&careHomeId="+strTableName,"GET",getRemittanceStatusList,onError);
            }
            else if(status == "inactive"){
                getAjaxObject(ipAddress+"/carehome/remittance-status/?is-active=0&is-deleted=1&careHomeId="+strTableName,"GET",getRemittanceStatusList,onError);
            }
        }
    }
    else if (nameglobal == "purchase_order_status") {
        var txtTableName = $("#cmbPOSeHomeName").data("kendoComboBox");
        //buildRoomListGrid([]);
        if(txtTableName && txtTableName.text() != ""){
            var strTableName = txtTableName.value();
            if(status == "active") {
                getAjaxObject(ipAddress+"/carehome/purchase-order-status/?is-active=1&is-deleted=0&careHomeId="+strTableName,"GET",getPurchaseList,onError);
            }
            else if(status == "inactive"){
                getAjaxObject(ipAddress+"/carehome/purchase-order-status/?is-active=0&is-deleted=1&careHomeId="+strTableName,"GET",getPurchaseList,onError);
            }
        }
    }
    else if (nameglobal == "hospitals") {
        var txtTableName = $("#cmbHOSCareHomeName").data("kendoComboBox");
        if(txtTableName && txtTableName.text() != ""){
            var strTableName = txtTableName.value();
            if(status == "active") {
                getAjaxObject(ipAddress+"/carehome/hospitals/?is-active=1&is-deleted=0&careHomeId="+strTableName,"GET",getHospitalsList,onError);
            }
            else if(status == "inactive"){
                getAjaxObject(ipAddress+"/carehome/hospitals/?is-active=0&is-deleted=1&careHomeId="+strTableName,"GET",getHospitalsList,onError);
            }
        }
    }
    else if (nameglobal == "resisdentsmaster") {
        var txtTableName = $("#cmbResisdentsCareHomeName").data("kendoComboBox");
        //buildRoomListGrid([]);
        if(txtTableName && txtTableName.text() != ""){
            var strTableName = txtTableName.value();
            if(status == "active") {
                getAjaxObject(ipAddress+"/carehome/residents/?is-active=1&is-deleted=0&careHomeId="+strTableName,"GET",getResisdentList,onError);
            }
            else if(status == "inactive"){
                getAjaxObject(ipAddress+"/carehome/residents/?is-active=0&is-deleted=1&careHomeId="+strTableName,"GET",getResisdentList,onError);
            }
        }
    }
    else if (nameglobal == "beds") {
        var txtTableName = $("#cmbBedsCareHomeName").data("kendoComboBox");
        //buildRoomListGrid([]);
        if(txtTableName && txtTableName.text() != ""){
            var strTableName = txtTableName.value();
            if(status == "active") {
                getAjaxObject(ipAddress+"/carehome/beds/?is-active=1&is-deleted=0&careHomeId="+strTableName,"GET",getBedList,onError);
            }
            else if(status == "inactive"){
                getAjaxObject(ipAddress+"/carehome/beds/?is-active=0&is-deleted=1&careHomeId="+strTableName,"GET",getBedList,onError);
            }
        }
    }
    else if (nameglobal == "dols") {
        var txtTableName = $("#cmbDOLCareHomeName").data("kendoComboBox");
        //buildRoomListGrid([]);
        if(txtTableName && txtTableName.text() != ""){
            var strTableName = txtTableName.value();
            if(status == "active") {
                getAjaxObject(ipAddress+"/carehome/dols/?is-active=1&is-deleted=0&careHomeId="+strTableName,"GET",getDolsList,onError);
            }
            else if(status == "inactive"){
                getAjaxObject(ipAddress+"/carehome/dols/?is-active=0&is-deleted=1&careHomeId="+strTableName,"GET",getDolsList,onError);
            }
        }
    }
    else if (nameglobal == "bed_allocation") {
        var txtTableName = $("#cmbBACareHomeName").data("kendoComboBox");
        //buildRoomListGrid([]);
        if(txtTableName && txtTableName.text() != ""){
            var strTableName = txtTableName.value();
            if(status == "active") {
                getAjaxObject(ipAddress+"/carehome/bed-allocations/?is-active=1&is-deleted=0&careHomeId="+strTableName,"GET",getBedAllocationList,onError);
            }
            else if(status == "inactive"){
                getAjaxObject(ipAddress+"/carehome/bed-allocations/?is-active=0&is-deleted=1&careHomeId="+strTableName,"GET",getBedAllocationList,onError);
            }
        }
    }
    else if (nameglobal == "remittances") {
        var txtTableName = $("#cmbRCareHomeName").data("kendoComboBox");
        //buildRoomListGrid([]);
        if(txtTableName && txtTableName.text() != ""){
            var strTableName = txtTableName.value();
            if(status == "active") {
                getAjaxObject(ipAddress+"/carehome/remittances/?is-active=1&is-deleted=0&careHomeId="+strTableName,"GET",getRemittanceList,onError);
            }
            else if(status == "inactive"){
                getAjaxObject(ipAddress+"/carehome/remittances/?is-active=0&is-deleted=1&careHomeId="+strTableName,"GET",getRemittanceList,onError);
            }
        }
    }
    else if (nameglobal == "purchase_order") {
        var txtTableName = $("#cmbPOCareHomeName").data("kendoComboBox");
        //buildRoomListGrid([]);
        if(txtTableName && txtTableName.text() != ""){
            var strTableName = txtTableName.value();
            if(status == "active") {
                getAjaxObject(ipAddress+"/carehome/purchase-orders/?is-active=1&is-deleted=0&careHomeId="+strTableName,"GET",getPurchaseOrderList,onError);
            }
            else if(status == "inactive"){
                getAjaxObject(ipAddress+"/carehome/purchase-orders/?is-active=0&is-deleted=1&careHomeId="+strTableName,"GET",getPurchaseOrderList,onError);
            }
        }
    }
}

    function onClickFilterBtn(e) {
    e.preventDefault();
    getTableValueList('active');
}

    function onSelectCareHomeName(){
    var cmbCareHomeName = $("#cmbCareHomeName").data("kendoComboBox");
    var resisdentCareHomeId = $("#resisdentCareHomeId").data("kendoComboBox");

    if(cmbCareHomeName && cmbCareHomeName.text() != "") {
        var strTableName = cmbCareHomeName.value();
        resisdentCareHomeId.select(strTableName);
    }
}
    var careHomeId, resisdentId, cityId;

    function getCareHomeDetails(obj) {
    if (obj && obj.response && obj.response.status && obj.response.status.code == "1") {
         careHomeId = obj.response.careHomes[0].id;
        getAjaxObject(ipAddress + "/homecare/communications/?parentId="+ careHomeId +"&parentTypeId=801&fields=*,address.*", "GET", saveCarehomeCommunication, onError);
    }
}

    function saveCarehomeCommunication(obj) {
    if (obj && obj.response && obj.response.status && obj.response.status.code == "1") {
        if (IsFlag == 1){
            if(obj.response.communications){
                $('input[name="care-address1"]').val(obj.response.communications[0].address1);
                $('input[name="care-address2"]').val(obj.response.communications[0].address2);
                $('input[name="care-phone"]').val(obj.response.communications[0].cellPhone);
                $('input[name="care-email"]').val(obj.response.communications[0].email);
                $('input[name="care-fax"]').val(obj.response.communications[0].fax);
                $('input[name="care-country"]').val(obj.response.communications[0].addressCountry);
                $('input[name="care-state"]').val(obj.response.communications[0].addressCountry);
                $('input[name="care-state"]').val(obj.response.communications[0].addressState);
                $('input[name="care-city"]').val(obj.response.communications[0].addressCity);
                $('input[name="care-postalcode"]').val(obj.response.communications[0].addressZip);
                cityId = obj.response.communications[0].cityId;
            getAjaxObject(ipAddress + "/homecare/communications/?parentId="+ careHomeId +"&parentTypeId=802", "GET", saveManagementCommunication, onError);
        }
    }
        else{
            var reqObj = {};
            var updateArr = [];
            reqObj.address1 = $('input[name="care-address1"]').val();
            reqObj.address2 = $('input[name="care-address2"]').val();
            reqObj.cellPhone = $('input[name="care-phone"]').val();
            if(zipSelItem){
                reqObj.cityId = zipSelItem.idk;
            }
            else{
                reqObj.cityId = cityId;
            }

            reqObj.createdBy= parseInt(sessionStorage.userId);
            reqObj.defaultCommunication= 1
            reqObj.email= $('input[name="care-email"]').val();
            reqObj.fax= $('input[name="care-fax"]').val();
            reqObj.isActive = 1
            reqObj.isDeleted= 0
            reqObj.parentTypeId = 801
            reqObj.sms = null;
            reqObj.parentId = careHomeId;
            if(obj.response.communications){
                if(obj.response.communications[0].id != null){
                    reqObj.id = obj.response.communications[0].id;
                    updateArr.push(reqObj);
                    createAjaxObjectAsync(ipAddress + '/homecare/communications/batch/', updateArr, "PUT", getManagementCommunictiondetails, onError);
                }
            }
            else
            {
                updateArr.push(reqObj);
                createAjaxObjectAsync(ipAddress + '/homecare/communications/batch/', updateArr, "POST", getManagementCommunictiondetails, onError);
            }
        }
    }
}

    function getManagementCommunictiondetails(obj) {
        if (obj && obj.response && obj.response.status && obj.response.status.code == "1") {
            // getAjaxObject(ipAddress + "/homecare/communications/?parentId="+ careHomeId +"&parentTypeId=802", "GET", saveManagementCommunication, onError);
            getAjaxObject(ipAddress + "/homecare/communications/?parentId="+ careHomeId +"&parentTypeId=802&fields=*,address.*", "GET", saveManagementCommunication, onError);
        }
    }

    function saveManagementCommunication(obj){
        if (obj && obj.response && obj.response.status && obj.response.status.code == "1") {
            if(IsFlag == 1){
                if(obj.response.communications){
                    $('input[name="careManagement-phone"]').val(obj.response.communications[0].cellPhone);
                    $('input[name="careManagement-email"]').val(obj.response.communications[0].email);
                }
                getAjaxObject(ipAddress + "/homecare/communications/?parentId="+ careHomeId +"&parentTypeId=803", "GET", saveAdminCommunication, onError);
            }
            else{
                var reqObj = {};
                var updateArr = [];
                reqObj.cellPhone = $('input[name="careManagement-phone"]').val();
                // reqObj.cityId = zipSelItem.idk
                reqObj.createdBy= parseInt(sessionStorage.userId);
                reqObj.defaultCommunication= 1
                reqObj.email= $('input[name="careManagement-email"]').val();
                reqObj.fax= null
                reqObj.isActive = 1
                reqObj.isDeleted= 0
                reqObj.parentTypeId = 802
                reqObj.sms = null;
                reqObj.parentId = careHomeId;
                if(obj.response.communications){
                    if(obj.response.communications[0].id != null){
                        reqObj.id = obj.response.communications[0].id;
                        updateArr.push(reqObj);
                        createAjaxObjectAsync(ipAddress + '/homecare/communications/batch/', updateArr, "PUT", getAdminCommunictiondetails, onError);
                    }
                }
                else
                {
                    updateArr.push(reqObj);
                    createAjaxObjectAsync(ipAddress + '/homecare/communications/batch/', updateArr, "POST", getAdminCommunictiondetails, onError);
                }
            }
        }
}

    function getAdminCommunictiondetails(obj) {
        if (obj && obj.response && obj.response.status && obj.response.status.code == "1") {
            getAjaxObject(ipAddress + "/homecare/communications/?parentId="+ careHomeId +"&parentTypeId=803&fields=*,address.*", "GET", saveAdminCommunication, onError);
        }
    }

    var residentId;

    function getResisdentCommunictiondetails(obj) {
        if ((obj && obj.response && obj.response.status && obj.response.status.code == "1") && IsFlag == 0) {
            residentId = obj.response.residents.id;
            getAjaxObject(ipAddress + "/homecare/communications/?parentId="+ obj.response.residents.id +"&parentTypeId=804&fields=*,address.*", "GET", saveResisdentCommunication, onError);
        }
        else {
            residentId = $('#residentID').val();
            getAjaxObject(ipAddress + "/homecare/communications/?parentId="+ obj.idk +"&parentTypeId=804&fields=*,address.*", "GET", saveResisdentCommunication, onError);
        }
    }

    function saveAdminCommunication(obj){
    if (obj && obj.response && obj.response.status && obj.response.status.code == "1") {
        if(IsFlag == 1){
            if(obj.response.communications){
                $('input[name="careAdmin-phone"]').val(obj.response.communications[0].cellPhone);
                $('input[name="careAdmin-email"]').val(obj.response.communications[0].email);
            }

        }
        else {
            var reqObj = {};
            var updateArr = [];
            reqObj.cellPhone = $('input[name="careAdmin-phone"]').val();
            // reqObj.cityId = zipSelItem.idk
            reqObj.createdBy= parseInt(sessionStorage.userId);
            reqObj.defaultCommunication= 1
            reqObj.email= $('input[name="careAdmin-email"]').val();
            reqObj.fax= null
            reqObj.isActive = 1
            reqObj.isDeleted= 0
            reqObj.parentTypeId = 803
            reqObj.sms = null;
            reqObj.parentId = careHomeId;
            if(obj.response.communications){
                if(obj.response.communications[0].id != null){
                    reqObj.id = obj.response.communications[0].id;
                    updateArr.push(reqObj);
                    createAjaxObjectAsync(ipAddress + '/homecare/communications/batch/', updateArr, "PUT", onCreate, onError);
                }
            }
            else
            {
                updateArr.push(reqObj);
                createAjaxObjectAsync(ipAddress + '/homecare/communications/batch/', updateArr, "POST", onCreate, onError);
            }
        }
    }
}

    function saveResisdentCommunication(obj) {
        if (obj && obj.response && obj.response.status && obj.response.status.code == "1") {
            if(IsFlag == 1){
                if(obj.response.communications){
                    var cmbSMS = $("#cmbSMS").data("kendoComboBox");

                    $("#txtAdd1").val(obj.response.communications[0].address1);
                    $("#txtAdd2").val(obj.response.communications[0].address2);
                    $("#txtCity").val(obj.response.communications[0].addressCity);
                    $("#txtState").val(obj.response.communications[0].addressState);
                    // $("#txtZip4").val(obj.response.communications[0].addressZip4);
                    $("#txtCountry").val(obj.response.communications[0].addressCountry);
                    $("#cmbZip").val(obj.response.communications[0].addressZip);
                    $("#txtHPhone").val(obj.response.communications[0].homePhone);
                    $("#txtCell").val(obj.response.communications[0].cellPhone);
                    $("#txtEmail").val(obj.response.communications[0].email);
                    $("#txtWP").val(obj.response.communications[0].workPhone);
                    getComboListIndex("cmbSMS", "desc", obj.response.communications[0].sms);
                    cityId = obj.response.communications[0].cityId;

                }

            }else{
                var cmbSMS = $("#cmbSMS").data("kendoComboBox");
                var updateArr = [];
                var reqObj = {};
                reqObj.address1 = $("#txtAdd1").val();
                reqObj.address2 = $("#txtAdd2").val();;
                reqObj.cellPhone = $("#txtCell").val();
                // reqObj.addressCity = $("#txtCity").val();
                // reqObj.addressState = $("#txtState").val();
                // reqObj.addressZip4 = $("#txtZip4").val();
                // reqObj.addressCountry = $("#txtCountry").val();
                // reqObj.addressZip = $("#cmbZip").val();
                if(zipSelItem){
                    reqObj.cityId = zipSelItem.idk;
                }
                else{
                    reqObj.cityId = cityId;
                }

                reqObj.createdBy= Number(sessionStorage.userId);
                reqObj.defaultCommunication= 1
                reqObj.email= $("#txtEmail").val();
                reqObj.fax= null
                reqObj.homePhone= $("#txtHPhone").val();
                reqObj.workPhone= $("#txtWP").val();
                reqObj.isActive = 1
                reqObj.isDeleted= 0
                reqObj.parentId =  residentId;
                reqObj.parentTypeId = 804
                reqObj.sms = cmbSMS.value();
                if(obj.response.communications){
                    if(obj.response.communications[0].id != null){
                        reqObj.id = obj.response.communications[0].id;
                        updateArr.push(reqObj);
                        createAjaxObjectAsync(ipAddress + '/homecare/communications/batch/', updateArr, "PUT", onCreate, onError);
                    }
                }
                else
                {
                    updateArr.push(reqObj);
                    createAjaxObjectAsync(ipAddress + '/homecare/communications/batch/', updateArr, "POST", onCreate, onError);
                }
            }
        }
    }



var $selfZipRow = "";
function onClickZipSearch() {
    var popW = 600;
    var popH = 500;

    parentRef.searchZip = true;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search Zip";
    var cntry = sessionStorage.countryName;
    if (cntry.indexOf("India") >= 0) {
        profileLbl = "Search Postal Code";
    } else if (cntry.indexOf("United Kingdom") >= 0) {
        profileLbl = "Search Postal Code";
    } else if (cntry.indexOf("United State") >= 0) {
        profileLbl = "Search Zip";
    } else {
        profileLbl = "Search Zip";
    }
    $selfZipRow = $(this);
    devModelWindowWrapper.openPageWindow("../../html/masters/zipList.html", profileLbl, popW, popH, true, onCloseSearchZipAction);

    if (!$(this).closest('tr').attr('data-item-edited') == true && !$(this).closest('tr').attr('data-item-new') == true) {
        $(this).closest('tr').attr('data-item-edited', true);
    }
}


var zipSelItem = null;

function onCloseSearchZipAction(evt, returnData) {
    console.log(returnData);
    if (returnData && returnData.status == "success") {
        if(nameglobal != "carehomemaster"){
            var selItem = returnData.selItem;
            if (selItem) {
                $('#txtCity').val('');
                $('#txtState').val('');
                // $('#txtZip4').val('');
                $('#txtCountry').val('');
                $('#cmbZip').val('');

                zipSelItem = selItem;
                cityId = zipSelItem.idk;
                $(this).find('input').attr('data-attr-city-id', cityId);
                if (zipSelItem.zip) {
                    $('#cmbZip').val(zipSelItem.zip);
                }
                // if (zipSelItem.zipFour) {
                //     $('#txtZip4').val(zipSelItem.zipFour);
                // }
                if (zipSelItem.state) {
                    $('#txtState').val(zipSelItem.state);
                }
                if (zipSelItem.country) {
                    $('#txtCountry').val(zipSelItem.country);
                }
                if (zipSelItem.city) {
                    $('#txtCity').val(zipSelItem.city);
                }
                // if (!$elem.attr('data-item-new') == true && !$elem.attr('data-item-edited') == true) {
                //     $elem.attr("data-item-edited", true);
                // }
            }
        }
        else{
            var selItem = returnData.selItem;
            if (selItem) {
                $('input[name="care-city"]').val('');
                $('input[name="care-state"]').val('');
                $('input[name="care-country"]').val('');
                $('input[name="care-postalcode"]').val('');

                zipSelItem = selItem;
                cityId = zipSelItem.idk;
                // $(this).find('input').attr('data-attr-city-id', cityId);
                if (zipSelItem.zip) {
                    $('input[name="care-postalcode"]').val(zipSelItem.zip);
                }
                if (zipSelItem.state) {
                    $('input[name="care-state"]').val(zipSelItem.state);
                }
                if (zipSelItem.country) {
                    $('input[name="care-country"]').val(zipSelItem.country);
                }
                if (zipSelItem.city) {
                    $('input[name="care-city"]').val(zipSelItem.city);
                }
                // if (!$elem.attr('data-item-new') == true && !$elem.attr('data-item-edited') == true) {
                //     $elem.attr("data-item-edited", true);
                // }
            }
        }
    }
}



function getSMSValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbSMS", onSMSChange, ["desc", "idk"], 0, "");
    }
}

function onSMSChange() {
    onComboChange("cmbSMS");
}

function onSuccessCreateAnother(resp) {
    if (dataObj && dataObj.response && dataObj.response.status) {
        customAlert.info("info", resp.response.status.message);
    }
    $('[data-medicate-content]').show();
    $('[data-medicate-content="1"]').hide();
    //fncgetCareHome();
}

function fncGetCarehomedetails(obj){
    if ((obj && obj.response.status.code == 1) || IsFlag == 1) {
            var careHomeName = $("#careHomeName").val();
            getAjaxObject(ipAddress + "/carehome/care-homes/?is-active=1&is-deleted=0&name="+ careHomeName, "GET", getCareHomeDetails, onError);
        }
        else{
        customAlert.info("info", obj.response.status.message);
    }
}

function fncGetMasersData(){

    var hospitaldataOptions = {
        pagination: false,
        changeCallBack: onHospitalGridChange
    }

    angularUIgridWrapper_hospitals = new AngularUIGridWrapper("dgridHospitalList", hospitaldataOptions);
    angularUIgridWrapper_hospitals.init();
    buildHospitalListGrid([]);
    // fncgethospitals();
}

function onClickResidentSearch() {
    var popW = "60%";
    var popH = 500;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search Resident";
    devModelWindowWrapper.openPageWindow("../../html/masters/residentList.html", profileLbl, popW, popH, true, closePtAddAction);
}

function closePtAddAction(evt,returnData){
    if(returnData && returnData.status == "success") {
        var residentName;
        // console.log(returnData);
        if (nameglobal == "bed_allocation") {
            if (returnData.selItem) {
                $("#txtResidentGender").val(returnData.selItem.GR);

                bedAllocationResidentId = returnData.selItem.PID;
                if (returnData.selItem.FN != "") {
                    residentName = returnData.selItem.FN;
                }
                if (returnData.selItem.MN != "") {
                    residentName = residentName + " " + returnData.selItem.MN;
                }
                if (returnData.selItem.LN != "") {
                    residentName = residentName + " " + returnData.selItem.LN;
                }
                $("#cmdResisdentId").val(residentName);

                if (returnData.selItem.DOB) {
                    var dt = new Date(returnData.selItem.DOB);
                    if (dt) {
                        var strDT = "";
                        var cntry = sessionStorage.countryName;
                        if (cntry.indexOf("India") >= 0) {
                            strDT = kendo.toString(dt, "dd/MM/yyyy");
                        } else if (cntry.indexOf("United Kingdom") >= 0) {
                            strDT = kendo.toString(dt, "dd/MM/yyyy");
                        } else {
                            strDT = kendo.toString(dt, "MM/dd/yyyy");
                        }
                        $("#txtResidentDOB").val(strDT);
                    }
                }
            }
        }
        else if(nameglobal == "purchase_order") {
            if (returnData.selItem) {
                $("#txtPurchaseResisdentGender").val(returnData.selItem.GR);

                purchaseOrderResidentId = returnData.selItem.PID;
                if (returnData.selItem.FN != "") {
                    residentName = returnData.selItem.FN;
                }
                if (returnData.selItem.MN != "") {
                    residentName = residentName + " " + returnData.selItem.MN;
                }
                if (returnData.selItem.LN != "") {
                    residentName = residentName + " " + returnData.selItem.LN;
                }
                $("#txtPurchaseOrderResisdentId").val(residentName);

                if (returnData.selItem.DOB) {
                    var dt = new Date(returnData.selItem.DOB);
                    if (dt) {
                        var strDT = "";
                        var cntry = sessionStorage.countryName;
                        if (cntry.indexOf("India") >= 0) {
                            strDT = kendo.toString(dt, "dd/MM/yyyy");
                        } else if (cntry.indexOf("United Kingdom") >= 0) {
                            strDT = kendo.toString(dt, "dd/MM/yyyy");
                        } else {
                            strDT = kendo.toString(dt, "MM/dd/yyyy");
                        }
                        $("#txtPurchaseResisdentDOB").val(strDT);
                    }
                }
            }
        }else if(nameglobal == "remittances") {
            if (returnData.selItem) {
                $("#txtRemittanceResidentGender").val(returnData.selItem.GR);

                remittanceResidentId = returnData.selItem.PID;
                if (returnData.selItem.FN != "") {
                    residentName = returnData.selItem.FN;
                }
                if (returnData.selItem.MN != "") {
                    residentName = residentName + " " + returnData.selItem.MN;
                }
                if (returnData.selItem.LN != "") {
                    residentName = residentName + " " + returnData.selItem.LN;
                }
                $("#cmdRemittanceResisdentId").val(residentName);

                if (returnData.selItem.DOB) {
                    var dt = new Date(returnData.selItem.DOB);
                    if (dt) {
                        var strDT = "";
                        var cntry = sessionStorage.countryName;
                        if (cntry.indexOf("India") >= 0) {
                            strDT = kendo.toString(dt, "dd/MM/yyyy");
                        } else if (cntry.indexOf("United Kingdom") >= 0) {
                            strDT = kendo.toString(dt, "dd/MM/yyyy");
                        } else {
                            strDT = kendo.toString(dt, "MM/dd/yyyy");
                        }
                        $("#txtRemiitanceResidentDOB").val(strDT);
                    }
                }
            }
        }else if(nameglobal == "dols") {
            if (returnData.selItem) {
                $("#txtDOLResisdentGender").val(returnData.selItem.GR);

                dolsResidentId = returnData.selItem.PID;
                if (returnData.selItem.FN != "") {
                    residentName = returnData.selItem.FN;
                }
                if (returnData.selItem.MN != "") {
                    residentName = residentName + " " + returnData.selItem.MN;
                }
                if (returnData.selItem.LN != "") {
                    residentName = residentName + " " + returnData.selItem.LN;
                }
                $("#cmbDolsResidentId").val(residentName);

                if (returnData.selItem.DOB) {
                    var dt = new Date(returnData.selItem.DOB);
                    if (dt) {
                        var strDT = "";
                        var cntry = sessionStorage.countryName;
                        if (cntry.indexOf("India") >= 0) {
                            strDT = kendo.toString(dt, "dd/MM/yyyy");
                        } else if (cntry.indexOf("United Kingdom") >= 0) {
                            strDT = kendo.toString(dt, "dd/MM/yyyy");
                        } else {
                            strDT = kendo.toString(dt, "MM/dd/yyyy");
                        }
                        $("#txtDOLResisdentDOB").val(strDT);
                    }
                }
            }
        }
    }
}

function onResidentListData(dataObj){
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.residents) {
            var residentName;
            if (nameglobal == "purchase_order") {
                if (dataObj.response.residents[0].firstName != "") {
                    residentName = dataObj.response.residents[0].firstName;
                }
                if (dataObj.response.residents[0].middleName != "") {
                    residentName = residentName + " " + dataObj.response.residents[0].middleName;
                }
                if (dataObj.response.residents[0].lastName != "") {
                    residentName = residentName + " " + dataObj.response.residents[0].lastName;
                }
                $("#txtPurchaseOrderResisdentId").val(residentName);

                $("#txtPurchaseResisdentGender").val(dataObj.response.residents[0].gender);

                if (dataObj.response.residents[0].dateOfBirth) {
                    var dt = new Date(dataObj.response.residents[0].dateOfBirth);
                    if (dt) {
                        var strDT = "";
                        var cntry = sessionStorage.countryName;
                        if (cntry.indexOf("India") >= 0) {
                            strDT = kendo.toString(dt, "dd/MM/yyyy");
                        } else if (cntry.indexOf("United Kingdom") >= 0) {
                            strDT = kendo.toString(dt, "dd/MM/yyyy");
                        } else {
                            strDT = kendo.toString(dt, "MM/dd/yyyy");
                        }
                        $("#txtPurchaseResisdentDOB").val(strDT);
                    }
                }
            }
            else if(nameglobal == "bed_allocation") {
                if (dataObj.response.residents[0].firstName != "") {
                    residentName = dataObj.response.residents[0].firstName;
                }
                if (dataObj.response.residents[0].middleName != "") {
                    residentName = residentName + " " + dataObj.response.residents[0].middleName;
                }
                if (dataObj.response.residents[0].lastName != "") {
                    residentName = residentName + " " + dataObj.response.residents[0].lastName;
                }
                $("#cmdResisdentId").val(residentName);

                $("#txtResidentGender").val(dataObj.response.residents[0].gender);

                if (dataObj.response.residents[0].dateOfBirth) {
                    var dt = new Date(dataObj.response.residents[0].dateOfBirth);
                    if (dt) {
                        var strDT = "";
                        var cntry = sessionStorage.countryName;
                        if (cntry.indexOf("India") >= 0) {
                            strDT = kendo.toString(dt, "dd/MM/yyyy");
                        } else if (cntry.indexOf("United Kingdom") >= 0) {
                            strDT = kendo.toString(dt, "dd/MM/yyyy");
                        } else {
                            strDT = kendo.toString(dt, "MM/dd/yyyy");
                        }
                        $("#txtResidentDOB").val(strDT);
                    }
                }
            }
            else if(nameglobal == "remittances") {
                if (dataObj.response.residents[0].firstName != "") {
                    residentName = dataObj.response.residents[0].firstName;
                }
                if (dataObj.response.residents[0].middleName != "") {
                    residentName = residentName + " " + dataObj.response.residents[0].middleName;
                }
                if (dataObj.response.residents[0].lastName != "") {
                    residentName = residentName + " " + dataObj.response.residents[0].lastName;
                }
                $("#cmdRemittanceResisdentId").val(residentName);

                $("#txtRemittanceResidentGender").val(dataObj.response.residents[0].gender);

                if (dataObj.response.residents[0].dateOfBirth) {
                    var dt = new Date(dataObj.response.residents[0].dateOfBirth);
                    if (dt) {
                        var strDT = "";
                        var cntry = sessionStorage.countryName;
                        if (cntry.indexOf("India") >= 0) {
                            strDT = kendo.toString(dt, "dd/MM/yyyy");
                        } else if (cntry.indexOf("United Kingdom") >= 0) {
                            strDT = kendo.toString(dt, "dd/MM/yyyy");
                        } else {
                            strDT = kendo.toString(dt, "MM/dd/yyyy");
                        }
                        $("#txtRemiitanceResidentDOB").val(strDT);
                    }
                }
            }
            else if(nameglobal == "dols") {
                if (dataObj.response.residents[0].firstName != "") {
                    residentName = dataObj.response.residents[0].firstName;
                }
                if (dataObj.response.residents[0].middleName != "") {
                    residentName = residentName + " " + dataObj.response.residents[0].middleName;
                }
                if (dataObj.response.residents[0].lastName != "") {
                    residentName = residentName + " " + dataObj.response.residents[0].lastName;
                }
                $("#cmbDolsResidentId").val(residentName);

                $("#txtDOLResisdentGender").val(dataObj.response.residents[0].gender);

                if (dataObj.response.residents[0].dateOfBirth) {
                    var dt = new Date(dataObj.response.residents[0].dateOfBirth);
                    if (dt) {
                        var strDT = "";
                        var cntry = sessionStorage.countryName;
                        if (cntry.indexOf("India") >= 0) {
                            strDT = kendo.toString(dt, "dd/MM/yyyy");
                        } else if (cntry.indexOf("United Kingdom") >= 0) {
                            strDT = kendo.toString(dt, "dd/MM/yyyy");
                        } else {
                            strDT = kendo.toString(dt, "MM/dd/yyyy");
                        }
                        $("#txtDOLResisdentDOB").val(strDT);
                    }
                }
            }
        }
    }
}

function onClickBrowse(e) {
    removeSelectedButtons();
    $("#btnBrowse").addClass("selClass");
    isBrowseFlag = true;
    $("#video").hide();
    $("#canvas").hide();
    $("#imgPhoto").show();
    console.log(e);
    if (e.currentTarget && e.currentTarget.id) {
        var btnId = e.currentTarget.id;
        var fileTagId = ("fileElem" + btnId);
        $("#fileElem").click();
    }
}

function removeSelectedButtons() {
    $("#btnBrowse").removeClass("selClass");
    $("#btnStartVideo").removeClass("selClass");
    $("#btnTakePhoto").removeClass("selClass");
    $("#btnUpload").removeClass("selClass");
}

function onClickBrowseOver() {
    removeOverSelection();
    $("#btnBrowse").addClass("borderClass");
}

function removeOverSelection() {
    $("#btnBrowse").removeClass("borderClass");
    $("#btnStartVideo").removeClass("borderClass");
    $("#btnTakePhoto").removeClass("borderClass");
    $("#btnUpload").removeClass("borderClass");
}

function onClickBrowseOut() {
    removeOverSelection();
}

var imagePhotoData = null;

function onSelectionFiles(event) {
    var imageCompressor = new ImageCompressor();
    console.log(event);
    files = event.target.files;
    fileName = "";
    //$('#txtFU').val("");
    if (files) {
        if (files.length > 0) {
            fileName = files[0].name;
            if (patientId != "") {
                var oFReader = new FileReader();
                oFReader.readAsDataURL(files[0]);
                oFReader.onload = function(oFREvent) {
                    document.getElementById("imgPhoto").src = oFREvent.target.result;
                    if (imageCompressor) {
                        imageCompressor.run(oFREvent.target.result, compressorSettings, function(small) {
                            document.getElementById("imgPhoto").src = small;
                            //isBrowseFlag = false;
                            imagePhotoData = small;
                            return small;
                        });
                    }

                }
            }
        }
    }
}
$(document).ready(function() {
    init();
    function init() {
        bindEvents();
    }
    function bindEvents() {
        $('.tabs a').on('click', function() {
            var $self = $(this);
            $('.tabs a').removeClass('activeTab');
            $self.addClass('activeTab');
            var selfAttr = $self.attr('href');
            $('[aria-tab-content]').hide();
            $(selfAttr).show();
        });
    }
});
var cityId = "";
var zipSelItem = null;
var ADD = "add";
var UPDATE = "edit";
var VIEW = "view";
var DELETE = "delete";
var operation = "";
var employeeId = null;

$(document).ready(function(){
    $("#pnlPatient",parent.document).css("display","none");
    sessionStorage.setItem("IsSearchPanel", "1");
    themeAPIChange();
	if(parent.frames['iframe'])
    parentRef = parent.frames['iframe'].window;
    
    init();
    buttonEvents();
    operation = "add";
});


function buttonEvents(){
    $("#btnRegister").off("click",onClickRegister);
    $("#btnRegister").on("click",onClickRegister);
    

    $("#btnCitySearch").off("click");
    $("#btnCitySearch").on("click", onClickZipSearch);
}

function onClickZipSearch() {
    var popW = 600;
    var popH = 500;
    if(parentRef)
        parentRef.searchZip = true;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search Zip";
    var cntry = sessionStorage.countryName;
    if (cntry.indexOf("India") >= 0) {
        profileLbl = "Search Postal Code";
    } else if (cntry.indexOf("United Kingdom") >= 0) {
        if(IsPostalCodeManual == "1"){
            profileLbl = "Search City";
        }
        else{
            profileLbl = "Search Postal Code";
        }

    } else if (cntry.indexOf("United State") >= 0) {
        profileLbl = "Search Zip";
    } else {
        profileLbl = "Search Zip";
    }
    $selfZipRow = $(this);
    devModelWindowWrapper.openPageWindow("../../html/masters/zipList.html", profileLbl, popW, popH, true, onCloseSearchZipAction);

}

function onCloseSearchZipAction(evt, returnData) {
    if (returnData && returnData.status == "success") {
        var selItem = returnData.selItem;
        zipSelItem = selItem;
        cityId = zipSelItem.idk;

        $("#txtCity").val("");
        $("#txtState").val("");
        $("#txtCountry").val("");
        $("#txtPostalCode").val("");

        if (zipSelItem.zip) {
            $("#txtPostalCode").val(zipSelItem.zip);
        }
        if (zipSelItem.state) {
            $("#txtState").val(zipSelItem.state);
        }
        if (zipSelItem.country) {
            $("#txtCountry").val(zipSelItem.country);
        }
        if (zipSelItem.city) {
            $("#txtCity").val(zipSelItem.city);
        }

       
    }
}



function themeAPIChange(){
    getAjaxObject(ipAddress+"/homecare/settings/?id=2","GET",getThemeValue,onError);
}

function getThemeValue(dataObj){
    var tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.activityTypes){
            if($.isArray(dataObj.response.settings)){
                tempCompType = dataObj.response.settings;
            }else{
                tempCompType.push(dataObj.response.settings);
            }
        }
        if(tempCompType.length > 0){
            themesChange(tempCompType[0].value);
        }
    }
}

function themesChange(themeValue){
    if(themeValue == 2){
        loadAPi("../../Theme2/Theme02.css");
    }
    else if(themeValue == 3){
        loadAPi("../../Theme3/Theme03.css");
    }
}

function init(){
    $("#cmbGender").kendoComboBox();

    initDateField("dtHireDate");
    initDateField("dtReleased");
    initDateField("dtDOB");


    getAjaxObject(ipAddress + "/master/Gender/list/", "GET", getGenderValueList, onError);
}



function initDateField(id){
    var cntry = sessionStorage.countryName;
    if (cntry.indexOf("India") >= 0) {
        $("#"+id).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            yearRange: "-200:+200",
            maxDate: '0',
            onSelect: function(e, e1) {
                // var dob = document.getElementById("dtDOB").value;
                // var dob = ((e1.selectedMonth + 1) + "-" + e1.selectedDay + "-" + e1.selectedYear);
                // var DOB = new Date(dob);
                // var today = new Date();
                // var age = today.getTime() - DOB.getTime();
                // age = Math.floor(age / (1000 * 60 * 60 * 24 * 365.25));
                // document.getElementById('txtAge').value = age;
            }
        });

       

    } else if (cntry.indexOf("United Kingdom") >= 0) {
        $("#"+id).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            yearRange: "-200:+200",
            maxDate: '0',
            onSelect: function(e, e1) {
                // var dob = ((e1.selectedMonth + 1) + "-" + e1.selectedDay + "-" + e1.selectedYear);
                // //var dob = document.getElementById("dtDOB").value;
                // var DOB = new Date(dob);
                // var today = new Date();
                // var age = today.getTime() - DOB.getTime();
                // age = Math.floor(age / (1000 * 60 * 60 * 24 * 365.25));
                // document.getElementById('txtAge').value = age;
            }
        });

    } else {
        $("#"+id).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'mm/dd/yy',
            yearRange: "-200:+200",
            maxDate: '0',
            onSelect: function(e, e1) {
                // var dob = ((e1.selectedMonth + 1) + "-" + e1.selectedDay + "-" + e1.selectedYear);
                // // var dob = document.getElementById("dtDOB").value;
                // var DOB = new Date(dob);
                // var today = new Date();
                // var age = today.getTime() - DOB.getTime();
                // age = Math.floor(age / (1000 * 60 * 60 * 24 * 365.25));
                // document.getElementById('txtAge').value = age;
            }
        });

    }
}

function getGenderValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbGender", onGenderChange, ["desc", "idk"], 0, "");
    }
}

function onGenderChange() {
    onComboChange("cmbGender");
}

function onComboChange(cmbId) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb && cmb.selectedIndex < 0) {
        cmb.select(0);
    }
}

function onClickRegister(){
    var strTitle = $("#txtTitle").val();
    var strFirstName = $("#txtFirstName").val();
    var strMiddleName = $("#txtMiddleName").val();
    var strLastName = $("#txtLastName").val();
    var strSuffix = $("#txtSuffix").val();
    var strCompany = $("#txtCompany").val();
    var strDisplayName = $("#txtDisplayName").val();
    var strAddress = $("#txtAddress").val();

    var strNotes = $("#txtNotes").val();
    var strEmail = $("#txtEmail").val();
    var strPhone = $("#txtPhone").val();
    var strMobile = $("#txtMobile").val();
    var strCostRate = $("#txtCostRate").val();
    var strBillingRate = $("#txtBillingRate").val();
    var chkBillableByDefault = $("#chkBillableByDefault").is(":checked");
    var strEmployeeIdNo = $("#txtEmployeeIdNo").val();
    var strEmployeeId = $("#txtEmployeeId").val(); //Need property name
    var cmbGender = $("#cmbGender").data("kendoComboBox");
    var dtHireDate = GetDateTime("dtHireDate");
    var dtReleased = GetDateTime("dtReleased");
    var dtDOB = GetDateTime("dtDOB");
    var strCity = $("#txtCity").val();
    var strState = $("#txtState").val();
    var strCountry = $("#txtCountry").val();
    var strZip = $("#txtPostalCode").val();
    

    if (strFirstName != "" && strLastName != "" && cmbGender.value() != "" && strAddress != "") {

        var flag = true;
        var strErrorMsg = "";

        var dataObj = {};
        dataObj = {};
        dataObj.title = strTitle;
        dataObj.firstName = strFirstName;
        dataObj.middleName = strMiddleName;
        dataObj.lastName = strLastName;
        dataObj.suffix = strSuffix;
        dataObj.company = strCompany;
        dataObj.displayName = strDisplayName;
        dataObj.notes = strNotes;
        dataObj.billingRate = strBillingRate;
        dataObj.billable = chkBillableByDefault === true ? 1 : 0;
        dataObj.employeeNumber = strEmployeeIdNo;
        dataObj.gender = cmbGender.value();
        dataObj.isActive = 1;
        dataObj.isDeleted = 0;
        dataObj.dateOfHired = dtHireDate;
        dataObj.dateOfReleased = dtReleased;
        dataObj.dateOfBirth = dtDOB;
        dataObj.costRate = strCostRate;


        var comm = [];
        var comObj = {};
        comObj.email = strEmail;
        comObj.homePhone = strPhone;
        comObj.cellPhone = strMobile;
        comObj.cityId = Number(cityId);
        comObj.addressCity = strCity;
        comObj.addressState = strState;
        comObj.addressCountry = strCountry;
        comObj.addressZip = strZip;
        comObj.address1 = strAddress;
        
        comm.push(comObj);
        dataObj.communications = comm;
        



        if (strEmail && !validateEmail(strEmail)) {
            flag = false;
            strErrorMsg = "your EmailId is invalid,Please enter the valid EmailId";
            // customAlert.error("Error", "your EmailId is invalid,Please enter the valid EmailId");
            // return;
        }

        if(flag){
            if (operation === ADD) {

                dataObj.createdBy = Number(sessionStorage.userId);
                console.log(dataObj);
                var dataUrl = ipAddress + "/homecare/employees/";
                createAjaxObject(dataUrl, dataObj, "POST", onCreate, onError);
            } else if (operation === UPDATE) {
                dataObj.modifiedBy = Number(sessionStorage.userId); //"101";
                dataObj.id = employeeId;
                var dataUrl = ipAddress + "/homecare/employees/";
                //createAjaxObject(dataUrl, dataObj, "PUT", onCreate, onError);
            }
        }
        else {
            $("body,html").animate({
                scrollTop: 0
            }, 500);
            // $('.customAlert').append('<div class="alert alert-danger">'+ strErrorMsg + '</div>');
            customAlert.error("Error",strErrorMsg);
        }

    }
    else{
        customAlert.error("Error","Please fill all the Required Fields");
    }
    

}

function getTableListArray(dataObj) {
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.codeTable) {
        if ($.isArray(dataObj.response.codeTable)) {
            dataArray = dataObj.response.codeTable;
        } else {
            dataArray.push(dataObj.response.codeTable);
        }
    }
    var tempDataArry = [];
    var obj = {};
    obj.desc = "";
    obj.zip = "";
    obj.value = "";
    obj.idk = "";
    tempDataArry.push(obj);
    for (var i = 0; i < dataArray.length; i++) {
        if (dataArray[i].isActive == 1) {
            var obj = dataArray[i];
            obj.idk = dataArray[i].id;
            obj.status = dataArray[i].Status;
            tempDataArry.push(obj);
        }
    }
    return tempDataArry;
}

// $(function () {

//     // $("form").clientSideCaptcha({
//     //     input: "#txtCaptcha",
//     //     display: "#captcha"
//     // });

//     // // additional validation method for firstname and lastname
//     // $.validator.addMethod("regexfirstname", function (value, element, regexpr) {
//     //     return regexpr.test(value);
//     // }, "Only alphabets are allowed.");

//     // // additional validation method for mobile number
//     // $.validator.addMethod("regexmobile", function (value, element, regexpr) {
//     //     return regexpr.test(value);
//     // }, "Mobile must start with 7/8/9 and length 10.");

//     // // additional validation method for email
//     // $.validator.addMethod("regexemail", function (value, element, regexpr) {
//     //     return regexpr.test(value);
//     // }, "Enter valid email.");

//     // // additional validation method for password
//     // $.validator.addMethod("regexpassword", function (value, element, regexpr) {
//     //     return regexpr.test(value);
//     // }, "Invalid password.");

//     // $.validator.addMethod("Captcha", function (value, element) {
//     //     var captcha = '';
//     //     $('#captcha td').each(function (i, e) {
//     //         var letter = $(this).text();
//     //         captcha = captcha + letter;
//     //     });
//     //     if (value !== captcha)
//     //         return false;
//     //     else
//     //         return true;
//     // }, "Enter correct captcha.");

//     // $.validator.addMethod("ddlUserTypeFunction", function (value, element) {

//     //     if (value == "0")
//     //         return false;
//     //     else
//     //         return true;
//     // }, "Please select a UserType.");

//     // $.validator.addMethod("ddlCountryCodeFunction", function (value, element) {

//     //     if (value == "-1")
//     //         return false;
//     //     else
//     //         return true;
//     // }, "Please select a CountryCode.");

//     // $("#register-form").validate({

//     //     // Specify the validation rules
//     //     rules: {
//     //         FirstName: {
//     //             required: true,
//     //             maxlength: 15,
//     //             regexfirstname: /^[a-zA-Z ]+$/
//     //         },
//     //         LastName: {
//     //             required: true,
//     //             maxlength: 15,
//     //             regexfirstname: /^[a-zA-Z ]+$/
//     //         },
//     //         MobileNumber: {
//     //             required: true,
//     //             number: true,
//     //             //regexmobile: /^([7,8,9]\d{9}|8\d{8})$/,
//     //             regexmobile: /^[789]/,
//     //             minlength: 10
//     //         },
//     //         Email: {
//     //             required: true,
//     //             regexemail: /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/
//     //         },
//     //         Password: {
//     //             required: true,
//     //             regexpassword: /^[^<>]+$/,
//     //         },
//     //         ConfirmPassword: {
//     //             required: true,
//     //             regexpassword: /^[^<>]+$/,
//     //             equalTo: '#txtPassword'
//     //         },
//     //         CompanyName: {
//     //             required: true,
//     //             regexpassword: /^[^<>]+$/,
//     //         },
//     //         Captcha: {
//     //             required: true,
//     //             regexpassword: /^[^<>]+$/,
//     //             Captcha: true
//     //         },
//     //         ddlUserType: {
//     //             ddlUserTypeFunction: true
//     //         },
//     //         ddlCountryCode: {
//     //             ddlCountryCodeFunction: true
//     //         }

//     //     },

//     //     // Specify the validation error messages
//     //     messages: {
//     //         FirstName: {
//     //             required: "Firstname is required.",
//     //             maxlength: "Please enter not more than 15 characters.",
//     //             regexfirstname: "Only alphabets are allowed."
//     //         },
//     //         LastName: {
//     //             required: "Lastname is required.",
//     //             maxlength: "Please enter not more than 15 characters.",
//     //             regexfirstname: "Only alphabets are allowed."
//     //         },
//     //         MobileNumber: {
//     //             required: "Mobile Number is required.",
//     //             number: "Only numbers are allowed.",
//     //             regexmobile: "Mobile must start with 7/8/9.",
//     //             minlength: "Enter complete mobile number."
//     //         },
//     //         Email: {
//     //             required: "Email is required.",
//     //             regexemail: "Enter valid email."
//     //         },
//     //         Password: {
//     //             required: "Password is required.",
//     //             regexpassword: "Invalid password."
//     //         },
//     //         ConfirmPassword: {
//     //             required: "ConfirmPassword is required.",
//     //             regexpassword: "Invalid password.",
//     //             equalTo: "Password and ConfirmPassword must match."
//     //         },
//     //         CompanyName: {
//     //             required: "CompanyName is required.",
//     //             regexpassword: "Invalid data."
//     //         },
//     //         Captcha: {
//     //             required: "Captcha is required.",
//     //             regexpassword: "Invalid captcha.",
//     //             Captcha: "Enter correct captcha."
//     //         },
//     //         ddlUserType: {
//     //             ddlUserTypeFunction: "Please select a UserType."
//     //         },
//     //         ddlCountryCode: {
//     //             ddlCountryCodeFunction: "Please select a Countrycode."
//     //         }
//     //     },

//     //     submitHandler: function (form) {
//     //         $('#btnRegister').prop('disabled', true);
//     //         UserRegistration();
//     //     }
//     // });



    
//         // additional validation method for firstname and lastname
//         $.validator.addMethod("regexfirstname", function (value, element, regexpr) {
//             return regexpr.test(value);
//         }, "Only alphabets are allowed.");
       

//         $("#register-form").validate({

//             // Specify the validation rules
//             rules: {
//                 FirstName: {
//                     required: true,
//                     maxlength: 15,
//                     regexfirstname: /^[a-zA-Z ]+$/
//                 },
//                 LastName: {
//                     required: true,
//                     maxlength: 15,
//                     regexfirstname: /^[a-zA-Z ]+$/
//                 }

//             },

//             // Specify the validation error messages
//             messages: {
//                 FirstName: {
//                     required: "Firstname is required.",
//                     maxlength: "Please enter not more than 15 characters.",
//                     regexfirstname: "Only alphabets are allowed."
//                 },
//                 LastName: {
//                     required: "Lastname is required.",
//                     maxlength: "Please enter not more than 15 characters.",
//                     regexfirstname: "Only alphabets are allowed."
//                 }
//             },

//             submitHandler: function (form) {
//                 //$('#btnRegister').prop('disabled', true);
//                 fncRegisterEmployee();
//             }
//         });
    
// });


function fncRegisterEmployee(){

}


function validateEmail(sEmail) {
    console.log(sEmail);
    var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    if (filter.test(sEmail)) {
    return true;
    }
    else {
    return false;
    }
}


function onCreate(dataObj) {
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            /**/
        	if(operation == ADD){
               
                displaySessionErrorPopUp("Info", "Employee Created Successfully", function(res) {
                    clearFields();
                })
        	}else{
                
                displaySessionErrorPopUp("Info", "Employee Information is updated successfully", function(res) {
                    clearFields();
                })
        	}
        } else {
            customAlert.error("error", dataObj.response.status.message);
        }
    }
    /**/
}


function clearFields(){
    $("#txtTitle").val("");
    $("#txtFirstName").val("");
    $("#txtMiddleName").val("");
    $("#txtLastName").val("");
    $("#txtSuffix").val("");
    $("#txtCompany").val("");
    $("#txtDisplayName").val("");
    $("#txtAddress").val("");

    $("#txtNotes").val("");
    $("#txtEmail").val("");
    $("#txtPhone").val("");
    $("#txtMobile").val("");
    $("#txtCostRate").val("");
    $("#txtBillingRate").val("");
    $("#chkBillableByDefault").prop("checked",false);
    $("#txtEmployeeIdNo").val("");
    $("#txtEmployeeId").val(""); //Need property name
    $("#cmbGender").data("kendoComboBox").select(0);
    
    $("#dtHireDate").val("");
    $("#dtReleased").val("");
    $("#dtDOB").val("");
    $("#txtCity").val("");
    $("#txtState").val("");
    $("#txtCountry").val("");
    $("#txtPostalCode").val("");

    cityId = null;
}

function onError(errObj) {
    console.log(errObj);
    customAlert.error("Error", "Error");
}
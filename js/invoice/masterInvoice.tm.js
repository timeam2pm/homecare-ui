$(document).ready(function() {
    var selItem = "";
    var node = "";
    var wrapperElem;
    var statusArr = [{ Key: 'Active', Value: 'Active' }, { Key: 'InActive', Value: 'InActive' }];
    var TransactionPartyArr = [{ Key: 'Party', Value: 'Group' }, { Key: 'Party', Value: 'Party' }, { Key: 'Both', Value: 'Both' }];

    $(".treeview-sprites").kendoTreeView({
        dataSource: [
            { text: "Party", expanded: true, spriteCssClass: "rootfolder" }
        ],
        select: function(e) {
            console.log(e);
        }
    });

    $(".treeview-sprites-menu").kendoContextMenu({
        // listen to right-clicks on treeview container
        target: ".treeview-sprites",

        // show when node text is clicked
        filter: ".k-in",

        // handle item clicks
        change: function(e) {
            console.log(e);
        },
        open: function(e) {
            console.log(e);
            var tv = $('.treeview-sprites').data('kendoTreeView'),
                selected = tv.select(),
                item = tv.dataItem(selected);
            node = $(e.target);
            console.log(node.text());
            var str1 = $(node).html();
            if (str1.indexOf("rootfolder") < 0) {
                setTimeout(function() {
                    var mn = $(".treeview-sprites-menu").data("kendoContextMenu");
                    mn.close();
                }, 1);
            }
        },
        select: function(e) {
            var button = $(e.item);
            node = $(e.target);
            selItem = button.text();
            if (selItem == "Group") {
	            wrapperElem = node.closest('.master-party-content').attr('data-content-data-type');
	            $('[data-content-data-type="'+wrapperElem+'"]').find('.box.wide').show();
	        } else {
	        	$('#invoice-add').trigger('click');
	        }
        }
    });

    init();

    function init() {
        bindEvents();
        validateEmail('master-party-email');
        allowNumerics('txtID');
        allowPhoneNumber('workPhone-owner');
        allowPhoneNumber('mobilePhone-owner');
        allowPhoneNumber('homePhone-contact');
        allowPhoneNumber('mobilePhone-contact');
        $("#cmbStatusParty").kendoComboBox();
        $("#cmbTransactionParty").kendoComboBox();
        setDataForSelection(statusArr, "cmbStatusParty", onPtChange, ["Value", "Key"], 0, "");
        setDataForSelection(TransactionPartyArr, "cmbTransactionParty", onTransPartyChange, ["Value", "Key"], 0, "");
    }

    function validateEmail(sEmail) {
	    console.log(sEmail);
	    var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	    if (filter.test(sEmail)) {
	        return true;
	    }
	    else {
	        return false;
	    }
	}

	function onPtChange() {
	    onComboChange("cmbStatusParty");
	}
	function onTransPartyChange() {
		onComboChange("cmbTransactionParty");
	}

	function onStatusChange() {
	    var cmbStatus = $("#cmbStatusParty").data("kendoComboBox");
	    if (cmbStatus && cmbStatus.selectedIndex < 0) {
	        cmbStatus.select(0);
	    }
	}
	function onTransactionPartyChange() {
	    var cmbTransactionParty = $("#cmbTransactionParty").data("kendoComboBox");
	    if (cmbTransactionParty && cmbTransactionParty.selectedIndex < 0) {
	        cmbTransactionParty.select(0);
	    }
	}

	function appendNodeToParent() {
		var person = $('#appendNodeText').val();
		if(person != '') {
	        // console.log(node);
	        var treeview = $('.treeview-sprites').data('kendoTreeView');

	        var bar = treeview.findByText(node.text());
	        treeview.select(bar);
	        if (selItem == "Group") {
	            treeview.append({ text: person, expanded: true, spriteCssClass: "rootfolder" }, bar);
	            $('#appendNodeText').val('');
	            $('[data-content-data-type="'+wrapperElem+'"]').find('.box.wide').hide();
	        } else {
	            /*treeview.append({ text: person }, bar);
	            $('#appendNodeText').val('');
	            $('[data-content-data-type="'+wrapperElem+'"]').find('.box.wide').hide();*/
	        }
	    }
    }

    function cancelNodeToParent() {
    	$('[data-content-data-type="'+wrapperElem+'"]').find('.box.wide').hide();
    }

    function onClickPartyMasterItem() {
    	var popW = "62%";
        var popH = "75%";

        var profileLbl;
        var devModelWindowWrapper = new kendoWindowWrapper();
        profileLbl = "Create Party Master Item";
        devModelWindowWrapper.openPageWindow("../../html/invoice/createPartyMasterItem.html", profileLbl, popW, popH, true, onClosePartyMasterItem);
    }
    function onCancelPartyMasterItem() {
    	
    }

    function onClosePartyMasterItem() {
        //alert();
    }

    function bindEvents() {
        $('#invoice-backToList').on('click', function(e) {
            e.preventDefault();
            $('[data-medicate-content="2"]').hide();
        });

        $('#invoice-add').off('click', onClickPartyMasterItem);
        $('#invoice-add').on('click', onClickPartyMasterItem);

        $('#appendNodeToSelected').off('click', appendNodeToParent);
        $('#appendNodeToSelected').on('click', appendNodeToParent);

        $('#cancelNodeToSelected').off('click', cancelNodeToParent);
        $('#cancelNodeToSelected').on('click', cancelNodeToParent);

        $('#partyMaster-cancel-btn').off('click', onCancelPartyMasterItem);
        $('#partyMaster-cancel-btn').on('click', onCancelPartyMasterItem);
    }
});
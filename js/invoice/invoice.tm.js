$(document).ready(function() {
    init();
    function init() {
    	bindEvents();
    }

    function bindEvents() {
	    $('#invoice-add').on('click',function(e) {
			e.preventDefault();
			$('[data-medicate-content]').hide();
			$('[data-medicate-content="1"]').show();
		});
		$('#invoice-backToList').on('click',function(e) {
			e.preventDefault();
			$('[data-medicate-content]').hide();
			$('[data-medicate-content="2"]').show();
		});
		$('#addPartyMaster').on('click', function(e) {
			$('.subnav-wrapper').hide();
			$("#ptframe").attr('src', "../../html/invoice/masterInvoice.html");
			setTimeout(function() {
				var iframeCouncil = $("[name='iframe']").contents();
				showDisplayContent(e);
				iframeCouncil.find('.master-party-content').hide();
				iframeCouncil.find('#invoiceList-master').show();
			    iframeCouncil.find('#invoiceList-master [data-content-data-type="master-party"]').show();
			    iframeCouncil.find('[data-attr-content="master-party"]').show();
		    	iframeCouncil.find('[data-attr-content="master-party"] [data-attr-content-type="partyMaster"]').show();
			}, 2500);
	    });
	    $('#addItemMaster').on('click', function(e) {
	    	$('.subnav-wrapper').hide();
			$("#ptframe").attr('src', "../../html/invoice/masterItemInvoice.html");
			setTimeout(function() {
		    	var iframeCouncil = $("[name='iframe']").contents();
				showDisplayContent(e);
				iframeCouncil.find('.master-party-content').hide();
				iframeCouncil.find('#invoiceList-master').show();
			    iframeCouncil.find('#invoiceList-master [data-content-data-type="master-item"]').show();
			    iframeCouncil.find('[data-attr-content="master-party"]').show();
		    	iframeCouncil.find('[data-attr-content="master-party"] [data-attr-content-type="itemMaster"]').show();
		    }, 2500);
	    });
	    $('#addEmployeeMaster').on('click', function(e) {
	    	$('.subnav-wrapper').hide();
			$("#ptframe").attr('src', "../../html/invoice/addEmployee.html");
			setTimeout(function() {
				var iframeCouncil = $("[name='iframe']").contents();
		        showDisplayContent(e);
		        iframeCouncil.find('.master-party-content').hide();
		        iframeCouncil.find('#invoiceList-master').show();
			    iframeCouncil.find('#invoiceList-master [data-content-data-type="master-employee"]').show();
			    iframeCouncil.find('[data-attr-content="master-party"]').show();
		        iframeCouncil.find('[data-attr-content="master-party"] [data-attr-content-type="employeeMaster"]').show();
			}, 2500);
	    });
	    $('#addSalesInvoice').on('click', function(e) {
	    	$('.subnav-wrapper').hide();
	    	$("#ptframe").attr('src', "../../html/invoice/billingSalesInvoice.html");
	    	/*var iframeCouncil = $("[name='iframe']").contents();
	        showDisplayContent(e);
	        iframeCouncil.find('.invoice-btn-wrapper').hide();
	        iframeCouncil.find('.backtolistWrapper').hide();
		    iframeCouncil.find('#invoiceList-billing, #invoiceList-billing [data-content-data-type="billing-content-salesInvoice"]').show();
		    iframeCouncil.find('[data-attr-content="billing"]').show();
	        iframeCouncil.find('[data-attr-content="billing"] [data-attr-content-type="salesInvoiceBilling"]').show();*/
	    });
	    $('#addExpenses').on('click', function(e) {
	    	e.preventDefault();
	    	$('.subnav-wrapper').hide();
	    	$("#ptframe").attr('src', "../../html/invoice/expenses.html");
	    });
	    $('#addGroupLeader').on('click', function(e) {
	    	$('.subnav-wrapper').hide();
	    	$("#ptframe").attr('src', "../../html/invoice/groupLeader.html");
	    	/*var iframeCouncil = $("[name='iframe']").contents();
	        showDisplayContent(e);
	        iframeCouncil.find('.invoice-btn-wrapper').hide();
	        iframeCouncil.find('.backtolistWrapper').hide();
		    iframeCouncil.find('#invoiceList-billing, #invoiceList-billing [data-content-data-type="billing-content-salesInvoice"]').show();
		    iframeCouncil.find('[data-attr-content="billing"]').show();
	        iframeCouncil.find('[data-attr-content="billing"] [data-attr-content-type="salesInvoiceBilling"]').show();*/
	    });
		$('.mainNav-link').on('click',function() {
			var iframeCouncil = $("[name='iframe']").contents();
			$(this).addClass('activeList').closest('li').siblings().find(".mainNav-link").removeClass('activeList');
			$('.hasError-rxNorm').remove();
			iframeCouncil.find('.sidebar-info-content').show();
			//iframeCouncil.find('[data-attr-content]').hide();
			//iframeCouncil.find('.contentWrapper').hide();
			iframeCouncil.find('.sidebar-nav li').removeClass('activeList');
			if($('.mainNav-list').find('.mainNav-link.activeList').attr('data-link-name').toLowerCase() == "dashboard") {
				//iframeCouncil.find('.sidebar-info-content').hide();
				//iframeCouncil.find('[data-attr-content="dashboard"]').show();
				//iframeCouncil.find('#invoiceList-dashboard').show();
				iframeCouncil.find('[data-link-name="dashboard"]').closest('li').addClass('activeList');
			}
			else if($('.mainNav-list').find('.mainNav-link.activeList').attr('data-link-name').toLowerCase() == "master") {
				//iframeCouncil.find('[data-attr-content="master"]').show();
				//iframeCouncil.find('#invoiceList-master').show();
				iframeCouncil.find('[data-link-name="master"]').closest('li').addClass('activeList');
			}
			else if($('.mainNav-list').find('.mainNav-link.activeList').attr('data-link-name').toLowerCase() == "settings" ) {
				//iframeCouncil.find('[data-attr-content="settings"]').show();
				//iframeCouncil.find('#invoiceList-settings').show();
				iframeCouncil.find('[data-link-name="settings"]').closest('li').addClass('activeList');
			}
			else if($('.mainNav-list').find('.mainNav-link.activeList').attr('data-link-name').toLowerCase() == "billing") {
				//iframeCouncil.find('[data-attr-content="billing"]').show();
				//iframeCouncil.find('#invoiceList-billing').show();
				iframeCouncil.find('[data-link-name="billing"]').closest('li').addClass('activeList');
			}
			else if($('.mainNav-list').find('.mainNav-link.activeList').attr('data-link-name').toLowerCase() == "report") {
				//iframeCouncil.find('[data-attr-content="report"]').show();
				//iframeCouncil.find('#invoiceList-report').show();
				iframeCouncil.find('[data-link-name="report"]').closest('li').addClass('activeList');
			}
			else if($('.mainNav-list').find('.mainNav-link.activeList').attr('data-link-name').toLowerCase() == "gst") {
				//iframeCouncil.find('[data-attr-content="gst"]').show();
				//iframeCouncil.find('#invoiceList-gst').show();
				iframeCouncil.find('[data-link-name="gst"]').closest('li').addClass('activeList');
			}
			else if($('.mainNav-list').find('.mainNav-link.activeList').attr('data-link-name').toLowerCase() == "deposits") {
				//iframeCouncil.find('[data-attr-content="deposits"]').show();
				//iframeCouncil.find('#invoiceList-deposits').show();
				iframeCouncil.find('[data-link-name="deposits"]').closest('li').addClass('activeList');
			}
			else if($('.mainNav-list').find('.mainNav-link.activeList').attr('data-link-name').toLowerCase() == "gropuleader") {
				//iframeCouncil.find('[data-attr-content="deposits"]').show();
				//iframeCouncil.find('#invoiceList-deposits').show();
				iframeCouncil.find('[data-link-name="groupleader"]').closest('li').addClass('activeList');
			}
		});
		function showDisplayContent(e) {
		    e.preventDefault();
		    var iframeCouncil = $("[name='iframe']").contents();
		    var mainDataContent = iframeCouncil.find('[data-attr-content]');
		    var subnavContentName = $(this).closest('.subnav-wrapper').attr('data-subnav-content');
		    $('[data-subnav-content="'+subnavContentName+'"]').hide();
		    mainDataContent.hide();
		    iframeCouncil.find('.contentWrapper').hide();
		    iframeCouncil.find('.content-data').hide();
		};
	}
});
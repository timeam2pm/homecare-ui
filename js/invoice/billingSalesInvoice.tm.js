$(document).ready(function() {
	function bindEvents() {
		$('.deliveryTaxLink').on('click', function() {
			$('.invoiceDeliveryTax').toggle();
			$(this).toggleClass('openLink');
		});
		$('.billableTaxLink').on('click', function() {
			$('.invoiceBillableDetails').toggle();
			$(this).toggleClass('openLink');
		});
		$('body').on('click','.addToInvoiceTable', function() {
			$(this).remove();
			var tableRows = $('.invoicetableRows').length;
			$('#invoiceListDataWrapper').append('<span><a href="#" class="addToInvoiceTable" style="font-size: 21px; position: absolute; left: -5px;">+</a></span><ul class="invoicetableRows"><li><input type="text" value="'+tableRows+'" /></li><li><input type="text" value="" /></li><li><input type="text" value="" /></li><li><input type="text" value="" /></li><li><input type="text" value="" /></li><li><input type="text" value="" /></li><li><input type="text" name="invrows-cases" value="" /></li><li><input type="text" name="invrows-qty" value="" /></li><li><input type="text" name="invrows-rate" value="" /></li><li><input type="text" name="invrows-amount" value="" readonly /></li><li><input type="text" name="invrows-cgst%" value="" class="setPercentWidth" /></li><li><input type="text" name="invrows-cgstamount" value="" readonly /></li><li><input type="text" name="invrows-sgst%" value="" class="setPercentWidth" /></li><li><input type="text" name="invrows-sgstamount" value="" readonly /></li><li><input type="text" name="invrows-igst%" value="" class="setPercentWidth" /></li><li><input type="text" name="invrows-igstamount" value="" readonly /></li><li><input type="text" name="invrows-total" value="" readonly /></li></ul>');
			setNumValidation();
		});
		$('#invoiceListDataWrapper').on('input','[name="invrows-cases"]', function() {
			var total = calcRowItems($('[name="invrows-cases"]'));
			$('[name="invcalc-cases"]').val(total);
		});
		$('#invoiceListDataWrapper').on('input','[name="invrows-amount"]', function() {
			var total = calcRowItems($('[name="invrows-amount"]'));
			$('[name="invcalc-amount"]').val(total);
		});
		$('#invoiceListDataWrapper').on('input','[name="invrows-cgstamount"]', function() {
			var total = calcRowItems($('[name="invrows-cgstamount"]'));
			$('[name="invcalc-cgstamount"]').val(total);
		});
		$('#invoiceListDataWrapper').on('input','[name="invrows-sgstamount"]', function() {
			var total = calcRowItems($('[name="invrows-sgstamount"]'));
			$('[name="invcalc-sgstamount"]').val(total);
		});
		$('#invoiceListDataWrapper').on('input','[name="invrows-igstamount"]', function() {
			var total = calcRowItems($('[name="invrows-igstamount"]'));
			$('[name="invcalc-igstamount"]').val(total);
		});
		$('#invoiceListDataWrapper').on('input','[name="invrows-total"]', function() {
			var total = calcRowItems($('[name="invrows-total"]'));
			$('[name="invcalc-total"]').val(total);
		});
		$('#invoiceListDataWrapper').on('input','[name="invrows-qty"], [name="invrows-rate"]', function() {
			var tabRowList = $(this).closest('.invoicetableRows');
			if(tabRowList.find('[name="invrows-qty"]').val().length > 0 && tabRowList.find('[name="invrows-rate"]').val().length > 0) {
				var qtyVal = parseInt(tabRowList.find('[name="invrows-qty"]').val());
				var rateVal = parseInt(tabRowList.find('[name="invrows-rate"]').val());
				tabRowList.find('[name="invrows-amount"]').val(qtyVal * rateVal);
			} else {
				tabRowList.find('[name="invrows-amount"]').val(0);
			}
			$('[name="invcalc-amount"]').val(calcRowItems($('[name="invrows-amount"]')));
			/*cgst amount*/
			calcCGSTTax(tabRowList);
			calcSGSTTax(tabRowList);
			calcIGSTTax(tabRowList);
			calcTotalTax(tabRowList);
		});
		$('#invoiceListDataWrapper').on('input','[name="invrows-cgst%"]', function() {
			var tabRowList = $(this).closest('.invoicetableRows');
			calcCGSTTax(tabRowList);
			calcTotalTax(tabRowList);
		});
		$('#invoiceListDataWrapper').on('input','[name="invrows-sgst%"]', function() {
			var tabRowList = $(this).closest('.invoicetableRows');
			calcSGSTTax(tabRowList);
			calcTotalTax(tabRowList);
		});
		$('#invoiceListDataWrapper').on('input','[name="invrows-igst%"]', function() {
			var tabRowList = $(this).closest('.invoicetableRows');
			calcIGSTTax(tabRowList);
			calcTotalTax(tabRowList);
		});
		function calcTotalTax(elem) {
			var amountVal = parseFloat(elem.find('[name="invrows-amount"]').val());
			var cgstamountVal = parseFloat(elem.find('[name="invrows-cgstamount"]').val());
			var sgstamountVal = parseFloat(elem.find('[name="invrows-sgstamount"]').val());
			var igstamountVal = parseFloat(elem.find('[name="invrows-igstamount"]').val());
			elem.find('[name="invrows-total"]').val(amountVal + cgstamountVal + sgstamountVal + igstamountVal);
			var calcTaxAmount = calcRowItems($('[name="invrows-total"]'));
			$('[name="invcalc-total"]').val(calcTaxAmount);
			$('[name="invTotal-beforeTax"]').val($('[name="invcalc-amount"]').val());
			$('[name="invTotal-cgst"]').val($('[name="invcalc-cgstamount"]').val());
			$('[name="invTotal-sgst"]').val($('[name="invcalc-sgstamount"]').val());
			$('[name="invTotal-igst"]').val($('[name="invcalc-igstamount"]').val());
			$('[name="invTotal-grandTotal"]').val($('[name="invcalc-total"]').val());
		}
	}
	function calcRowItems(elem) {
		var totVal = 0;
		elem.each(function() {
			var self = $(this);
			totVal = totVal + (self.val() != "" ? parseFloat(self.val()) : 0);
		});
		return Math.round(totVal);
	}
	function calcTaxItems(amount, gstPerc) {
		var amountVal = parseFloat(amount.val());
		var gstPerVal = parseFloat(gstPerc.val());
		var calcTaxAmount = parseFloat((amountVal / 100) * gstPerVal);
		return calcTaxAmount;
	}
	function calcCGSTTax(elem) {
		if(elem.find('[name="invrows-cgst%"]').val().length > 0) {
			var calcTaxAmount = calcTaxItems(elem.find('[name="invrows-amount"]'), elem.find('[name="invrows-cgst%"]'));
			elem.find('[name="invrows-cgstamount"]').val(calcTaxAmount);
		} else {
			elem.find('[name="invrows-cgstamount"]').val(0);
		}
		$('[name="invcalc-cgstamount"]').val(calcRowItems($('[name="invrows-cgstamount"]')));
	}
	function calcSGSTTax(elem) {
		if(elem.find('[name="invrows-sgst%"]').val().length > 0) {
			var calcTaxAmount = calcTaxItems(elem.find('[name="invrows-amount"]'), elem.find('[name="invrows-sgst%"]'));
			elem.find('[name="invrows-sgstamount"]').val(calcTaxAmount);
		} else {
			elem.find('[name="invrows-sgstamount"]').val(0);
		}
		$('[name="invcalc-sgstamount"]').val(calcRowItems($('[name="invrows-sgstamount"]')));
	}
	function calcIGSTTax(elem) {
		if(elem.find('[name="invrows-igst%"]').val().length > 0) {
			var calcTaxAmount = calcTaxItems(elem.find('[name="invrows-amount"]'), elem.find('[name="invrows-igst%"]'));
			elem.find('[name="invrows-igstamount"]').val(calcTaxAmount);
		} else {
			elem.find('[name="invrows-igstamount"]').val(0);
		}
		$('[name="invcalc-igstamount"]').val(calcRowItems($('[name="invrows-igstamount"]')));
	}
	function allowNumbers(elem){
		elem.keypress(function (e) {
			if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
				return false;
			}
	   });
	}
	function allowDecimal(elem){
		elem.keypress(function (e) {
			 if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 95 && e.which != 46) {
				return false;
			}
	   });
	}
	function setNumValidation() {
		allowNumbers($('[name="invrows-cgstamount"]'));
		allowNumbers($('[name="invrows-sgstamount"]'));
		allowNumbers($('[name="invrows-igstamount"]'));
		allowNumbers($('[name="invrows-amount"]'));
		allowNumbers($('[name="invrows-qty"]'));
		allowNumbers($('[name="invrows-rate"]'));
		allowNumbers($('[name="invrows-cases"]'));
		allowNumbers($('[name="invrows-total"]'));
		allowDecimal($('[name="invrows-cgst%"]'));
		allowDecimal($('[name="invrows-sgst%"]'));
		allowDecimal($('[name="invrows-igst%"]'));
	}
	function init() {
		bindEvents();
		setNumValidation();
	}
	init();
});
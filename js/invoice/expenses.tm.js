$(document).ready(function() {
	function init() {
		$(".datepicker").kendoDatePicker();
		bindEvents();
	}
	function bindEvents() {
		$('.tab-list').on('click', function(e) {
			e.preventDefault();
			var $self = $(this);
			$self.addClass('active-tabList').siblings('.tab-list').removeClass('active-tabList');
			var content = $self.attr('data-expense-list');
			$('[data-expense-content]').hide();
			$('[data-expense-content='+ content +']').show();
		});
	}
	init();
});
var angularUIgridWrapper;
var angularUIgridWrapper1;
var parentRef = null;
var recordType = "1";
var dataArray = [];
var dataTypeArray = [{Key:'Text',Value:'1'},{Key:'Number',Value:'2'},{Key:'Boolean',Value:'3'}];
var unitArray =  [{Key:'Kg',Value:'1'},{Key:'Lt',Value:'2'}];
var statusArray =  [{Key:'ACTIVE',Value:'1'},{Key:'INACTIVE',Value:'2'}];

$(document).ready(function(){
    if( parent.frames['iframe'])
        parentRef = parent.frames['iframe'].window;
    //recordType = parentRef.dietType;
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("dgridFormDesignTemplateList", dataOptions);
    angularUIgridWrapper.init();

    var dataOptions1 = {
        pagination: false,
        changeCallBack: onChange1
    }
    angularUIgridWrapper1 = new AngularUIGridWrapper("dgridFromTemplateList", dataOptions1);
    angularUIgridWrapper1.init();

    buildDeviceListGrid([]);
    buildDeviceListGrid1([]);
});

$(window).load(function(){
    loading = false;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
    $("#btnEdit").prop("disabled", true);
    $("#btnDelete").prop("disabled", true);
    $("#cmbTemplates").kendoComboBox();
    $("#txtCompType").kendoComboBox();
    $("#txtDT").kendoComboBox();
    $("#cmbUnits").kendoComboBox();
    $("#cmbStatus").kendoComboBox();

    //setDataForSelection(dataTypeArray, "txtDT", function(){}, ["Key", "Value"], 0, "");
    //setDataForSelection(unitArray, "cmbUnits", function(){}, ["Key", "Value"], 0, "");
    setDataForSelection(statusArray, "cmbStatus", function(){}, ["Key", "Value"], 0, "");

    $("#txtChars").kendoNumericTextBox({step:1,min:1,max:255,decimals: 0,value:10,format: "0",change:onChangeMETime});
    //getTemplates();
    var ipaddress = ipAddress+"/homecare/templates/?is-active=1&is-deleted=0";
    getAjaxObject(ipaddress,"GET",handleGetTemplateList,onError);
    getDataTypes();
    getUnits();
    //init();
    buttonEvents();
    adjustHeight();
}
function getComponents(){
    getAjaxObject(ipAddress+"/master/type/list/?name=component","GET",getComponentTypes,onError);
}
function getDataTypes(){
    getAjaxObject(ipAddress+"/homecare/data-types/","GET",onGetDataTypes,onError);
}
var formDataTypes = [];
function onGetDataTypes(dataObj){
    console.log(dataObj);
    var tempDataType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.dataTypes){
            if($.isArray(dataObj.response.dataTypes)){
                tempDataType = dataObj.response.dataTypes;
            }else{
                tempDataType.push(dataObj.response.dataTypes);
            }
        }
    }
    formDataTypes = JSON.parse(JSON.stringify(tempDataType));
    //setDataForSelection(tempDataType, "txtDT", function(){}, ["value", "id"], 0, "");
    getComponents();
}

function getUnits(){
    getAjaxObject(ipAddress+"/homecare/units/","GET",onGetUnits,onError);
}
function onGetUnits(dataObj){
    console.log(dataObj);
    var tempUnits = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.units){
            if($.isArray(dataObj.response.units)){
                tempUnits = dataObj.response.units;
            }else{
                tempUnits.push(dataObj.response.units);
            }
        }
    }
    if(tempUnits.length>0){
        var obj = {};
        obj.id = "";
        obj.value = "";
        tempUnits.unshift(obj)
    }
    setDataForSelection(tempUnits, "cmbUnits", function(){}, ["value", "id"], 0, "");
}
function getComponentTypes(dataObj){
    var tempCompType = [];
    compType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.typeMaster){
            if($.isArray(dataObj.response.typeMaster)){
                tempCompType = dataObj.response.typeMaster;
            }else{
                tempCompType.push(dataObj.response.typeMaster);
            }
        }
    }
    for(var i=0;i<tempCompType.length;i++){
        var obj = {};
        obj.Key = tempCompType[i].type;
        obj.Value = tempCompType[i].id;
        compType.push(obj);
    }
    setDataForSelection(compType, "txtCompType", onComponentChange, ["Key", "Value"], 0, "");
    onComponentChange();
}
var COMP_TEXT = "text box";
var COMP_TEXT_AREA = "text area"
var COMP_BOOLEAN = "boolean";
var COMP_CHECK_BOX = "check box"
var COMP_RADIO_BUTTON = "radio button"
var COMP_GROUP_CHECK_BOX = "group check box"
var COMP_IMAGE = "image"
var COMP_DATE_PICKER = "date picker";

var DT_INT = "INT";
var DT_VARCHAR = "VARCHAR";
var DT_DATETIME = "DATETIME";
var DT_DECIMAL = "DECIMAL";
var DT_DATE = "DATE";


function onComponentChange(){
    var tempDataType = [];
    setDataForSelection([], "txtDT", function(){}, ["", ""], 0, "");
    //getNumericBoxValue("txtChars");
    setNumericBoxValue("txtChars", "");
    enableDisableNumericBox("txtChars", false);
    var txtDT = $("#txtDT").data("kendoComboBox");
    if(txtDT){
        txtDT.text("");
        txtDT.value("");
    }
    var txtCompType = $("#txtCompType").data("kendoComboBox");
    if(txtCompType && txtCompType.selectedIndex>=0){
        if(txtCompType){
            if(txtCompType.text() == COMP_TEXT || txtCompType.text() == COMP_TEXT_AREA){
                enableDisableNumericBox("txtChars", true);
                setNumericBoxValue("txtChars", "20");
                for(var i=0;i<formDataTypes.length;i++){
                    var item = formDataTypes[i];
                    if(item && (item.value == DT_INT || item.value == DT_VARCHAR || item.value == DT_DECIMAL)){
                        tempDataType.push(item);
                    }
                }
            }else if(txtCompType.text() == COMP_DATE_PICKER){
                for(var i=0;i<formDataTypes.length;i++){
                    var item = formDataTypes[i];
                    if(item && (item.value == DT_DATE || item.value == DT_DATETIME)){
                        tempDataType.push(item);
                    }
                }
            }
        }
    }
    setDataForSelection(tempDataType, "txtDT", function(){}, ["value", "id"], 0, "");
}
function onChangeMETime(event){
    onChangeNumericBox(event);
}
function onChangeNumericBox(event){
    if(event && event.sender && event.sender.element){
        var boxId = $(event.sender.element).attr("id");
        if(boxId){
            var txtSpBandBegin1 = $("#"+boxId).data("kendoNumericTextBox");
            if(txtSpBandBegin1){
                if(txtSpBandBegin1.value() == ""){
                    return;
                }
                if(txtSpBandBegin1 && txtSpBandBegin1.value() !=0  && !txtSpBandBegin1.value()){
                    var maxVal = txtSpBandBegin1.min();
                    //txtSpBandBegin1.value(maxVal);
                }
            }
        }
    }
}
function handleGetTemplateList(dataObj){
    console.log(dataObj);
    var vacation = [];
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            if(dataObj.response.templates){
                if($.isArray(dataObj.response.templates)){
                    vacation = dataObj.response.templates;
                }else{
                    vacation.push(dataObj.response.templates);
                }
            }
        }
    }
    for(var j=0;j<vacation.length;j++){
        vacation[j].idk = vacation[j].id;
    }
    setDataForSelection(vacation, "cmbTemplates", onTemplateChange, ["name", "idk"], 0, "");
    onTemplateChange();
    //buildDeviceListGrid1(vacation);
}
function onTemplateChange(){
    var cmbTemplates = $("#cmbTemplates").data("kendoComboBox");
    buildDeviceListGrid1([]);
    var selectedItems = cmbTemplates.dataItem();
    var miniTemplates = selectedItems.miniTemplates;
    var arr = [];
    if($.isArray(miniTemplates)){
        arr = miniTemplates;
    }else{
        arr.push(miniTemplates);
    }
    buildDeviceListGrid1([]);
    arr = miniTemplates;
    var miniArray = [];
    for(var x1=0;x1<arr.length;x1++){
        arr[x1].idk = arr[x1].id;
        var obj = {};
        obj.idk = arr[x1].id;
        obj.name = arr[x1].name;
        miniArray.push(obj);
    }
    setTimeout(function(){
        buildDeviceListGrid1(miniArray);
        onChange1();
    },100);

    //
}
/*function getTemplates(){
	var ipaddress = ipAddress+"/homecare/form-templates/";
	getAjaxObject(ipaddress,"GET",handleGetTemplateList,onError);
}*/
/*function handleGetTemplateList(dataObj){
	var vacation = [];
	if(dataObj && dataObj.response && dataObj.response.status ){
		if(dataObj.response.status.code == "1"){
			if(dataObj.response.formTemplates){
				if($.isArray(dataObj.response.formTemplates)){
					vacation = dataObj.response.formTemplates;
				}else{
					vacation.push(dataObj.response.formTemplates);
				}
			}
		}
	}
	for(var j=0;j<vacation.length;j++){
		vacation[j].idk = vacation[j].id;
	}
	setDataForSelection(vacation, "cmbTemplates", function(){}, ["name", "idk"], 0, "");
}*/
function init(){
    $("#btnEdit").prop("disabled", true);
    $("#btnDelete").prop("disabled", true);
    buildDeviceListGrid([]);
    //buildDeviceListGrid1([]);
    var selectedItems = angularUIgridWrapper1.getSelectedRows();
    console.log(selectedItems);
    if(selectedItems && selectedItems.length>0){
        var ipaddress = ipAddress+"/homecare/components/?is-active=1&is-deleted=0&miniTemplateId="+selectedItems[0].idk+"&fields=*,dataType.*,unit.*,component.*";
        getAjaxObject(ipaddress,"GET",handleGetVacationList,onError);
    }

    //getAjaxObject(ipAddress+"/homecare/activity-types/?is-active=1&is-deleted=0&sort=display-order","GET",getActivityTypes,onError);
}

function handleGetVacationList(dataObj){
    console.log(dataObj);
    var vacation = [];
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            if(dataObj.response.components){
                if($.isArray(dataObj.response.components)){
                    vacation = dataObj.response.components;
                }else{
                    vacation.push(dataObj.response.components);
                }
            }
        }
    }
    for(var j=0;j<vacation.length;j++){
        vacation[j].idk = vacation[j].id;
        vacation[j].length1 = vacation[j].length;
    }
    buildDeviceListGrid(vacation);
}
function onError(errorObj){
    console.log(errorObj);
}

function buttonEvents(){
    $("#btnSave").off("click",onClickSave);
    $("#btnSave").on("click",onClickSave);


    $("#btnCancelDet").off("click",onClickCancel);
    $("#btnCancelDet").on("click",onClickCancel);


    $("#btnEdit").off("click",onClickEdit);
    $("#btnEdit").on("click",onClickEdit);

    $("#btnDelete").off("click",onClickDelete);
    $("#btnDelete").on("click",onClickDelete);
}

function onClickDelete(){
    customAlert.confirm("Confirm", "Are you sure to delete?",function(response){
        if(response.button == "Yes"){
            var dataObj = {};
            dataObj.createdBy = Number(sessionStorage.userId);;
            dataObj.isDeleted = 1;
            dataObj.isActive = 0;
            var dataUrl = ipAddress +"/homecare/components/";
            var method = "DELETE";
            var selectedItems = angularUIgridWrapper.getSelectedRows();
            dataObj.id = selectedItems[0].idk;
            createAjaxObject(dataUrl, dataObj, method, onCreateDelete, onError);
        }
    });

}
function onCreateDelete(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "Template deleted successfully";
            customAlert.error("Info", msg);
            operation = ADD;
            init();
        }
    }
}
function onClickSave(){
    try{
        var txtCompType = $("#txtCompType").data("kendoComboBox");
        var txtDT = $("#txtDT").data("kendoComboBox");
        var cmbUnits = $("#cmbUnits").data("kendoComboBox");
        var selectedItems = angularUIgridWrapper1.getSelectedRows();
        console.log(selectedItems);
        var strTemp = $("#txtCompName").val();
        strTemp = $.trim(strTemp);
        if(strTemp != ""){
            var reqData = {};
            reqData.name = strTemp;
            reqData.label = strTemp;
            reqData.notes = $("#txtNotes").val();
            reqData.miniTemplateId = Number(selectedItems[0].idk);//Number(cmbTemplates.value());
            //reqData.componentType = txtCompType.text();
            reqData.componentTypeId = Number(txtCompType.value());
            //reqData.dataTypeValue = txtDT.text();
            reqData.dataTypeId = Number(txtDT.value());
            //reqData.dataTypeCode = Number(txtDT.value());

            //reqData.unitValue = cmbUnits.text();
            if(cmbUnits.value() == ""){
                reqData.unitId = null;
            }else{
                reqData.unitId = Number(cmbUnits.value());
            }

            reqData.length = getNumericBoxValue("txtChars");//10;//Number(cmbUnits.value());

            reqData.isActive = 1;
            reqData.isDeleted = 0;
            reqData.createdBy = Number(sessionStorage.userId);

            console.log(reqData);
            var method = "POST";
            if(operation == UPDATE){
                var selectedItems1 = angularUIgridWrapper.getSelectedRows();
                reqData.id = selectedItems1[0].idk;
                method = "PUT";
            }
            dataUrl = ipAddress +"/homecare/components/";
            createAjaxObject(dataUrl, reqData, method, onCreate, onError);
        }
    }catch(ex){
        console.log(ex);
    }
}
function getNumericBoxValue(boxId){
    var returnVal = "";
    var numerictextbox = $("#"+boxId).data("kendoNumericTextBox");
    if(numerictextbox){
        returnVal =  numerictextbox.value();
        returnVal = Number(returnVal);
    }
    if(returnVal == null){
        returnVal = "";
    }
    return returnVal;
}
function setNumericBoxValue(boxId,dataVal){
    var numerictextbox = $("#"+boxId).data("kendoNumericTextBox");
    if(numerictextbox){
        numerictextbox.value(dataVal);
    }
}
function enableDisableNumericBox(boxId,flag){
    var numerictextbox = $("#"+boxId).data("kendoNumericTextBox");
    if(numerictextbox){
        numerictextbox.enable(flag);
    }
}
function onCreate(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "";
            msg = "Components are Created  Successfully";
            if(operation == UPDATE){
                msg = "Components are Updated  Successfully";
            }
            displaySessionErrorPopUp("Info", msg, function(res) {
                resetData();
                operation = ADD;
                //onChange1();
                init();
            })
        }else{
            customAlert.error("Error", dataObj.response.status.message);
        }
    }else{

    }
}

function resetData(){
    $("#txtNotes").val("");
    $("#txtCompName").val("");
}
function adjustHeight(){
    var defHeight = 300;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    if(angularUIgridWrapper){
        angularUIgridWrapper.adjustGridHeight((cmpHeight-50));
    }
    if(angularUIgridWrapper1){
        angularUIgridWrapper1.adjustGridHeight((cmpHeight+100));
    }
}

function buildDeviceListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Name",
        "field": "name",
    });
    gridColumns.push({
        "title": "Data Type",
        "field": "dataTypeValue",
    });
    gridColumns.push({
        "title": "Component Type",
        "field": "componentType",
    });
    gridColumns.push({
        "title": "Chars",
        "field": "length1",
    });
    gridColumns.push({
        "title": "UOM",
        "field": "unitValue",
    });
    gridColumns.push({
        "title": "Display Order",
        "field": "displayOrder",
    });
    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}
function buildDeviceListGrid1(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Template",
        "field": "name",
    });
    angularUIgridWrapper1.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}

var selRow = null;
function onClickAction(){

}

var prevSelectedItem =[];
function onChange(){
    setTimeout(function() {
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if (selectedItems && selectedItems.length > 0) {
            var obj = selectedItems[0];
            atID = obj.idk;
            $("#btnEdit").prop("disabled", false);
            $("#btnDelete").prop("disabled", false);
            //onClickEdit();
        } else {
            $("#btnEdit").prop("disabled", true);
            $("#btnDelete").prop("disabled", true);
            //resetData();
        }
    });
}
function onChange1(){
    setTimeout(function() {
        var selectedItems = angularUIgridWrapper1.getSelectedRows();
        console.log(selectedItems);
        buildDeviceListGrid([]);
        if(selectedItems && selectedItems.length>0){
            $("#divEvents").removeClass("addEvents");
            /*var miniTemplates = selectedItems[0].miniTemplates;
            var arr = [];
            if($.isArray(miniTemplates)){
                arr = miniTemplates;
            }else{
                arr.push(miniTemplates);
            }
            for(var x1=0;x1<miniTemplates.length;x1++){
                miniTemplates[x1].idk = miniTemplates[x1].id;
            }
            buildDeviceListGrid(miniTemplates);*/
            init();
        }else{
            $("#divEvents").addClass("addEvents");
        }
    });
}
var operation = "ADD";
var ADD = "ADD";
var UPDATE = "UPDATE";
var atID = "";
var ds  = "";
function onClickEdit(){
    var selectedItems = angularUIgridWrapper.getSelectedRows();
    console.log(selectedItems);
    if (selectedItems && selectedItems.length > 0) {
        var obj = selectedItems[0];
        operation = UPDATE;
        $("#txtCompName").val(obj.name);
        $("#txtNotes").val(obj.notes);
        enableDisableNumericBox("txtChars",true);
        var txtChars = $("#txtChars").data("kendoNumericTextBox");
        if(txtChars){
            txtChars.value(obj.length1);
        }
        getComboListIndex("txtCompType", "Value", obj.componentTypeId);
        onComponentChange();
        getComboListIndex("txtDT", "id", obj.dataTypeId);
        getComboListIndex("cmbUnits", "id", obj.unitId);
    }
}

function getComboListIndex(cmbId, attr, attrVal) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb) {
        var ds = cmb.dataSource;
        var totalRec = ds.total();
        for (var i = 0; i < totalRec; i++) {
            var dtItem = ds.at(i);
            if (dtItem && dtItem[attr] == attrVal) {
                cmb.select(i);
                return i;
            }
        }
    }
    return -1;
}
function closeVideoScreen(evt,returnData){

}
var operation = "add";
var selFileResourceDataItem = null;
function onClickCancel(){
    var obj = {};
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}

var angularUIgridWrapper;
var angularUIgridWrapper1;
var parentRef = null;
var recordType = "1";
var dataArray = [];
$(document).ready(function(){
    $("#pnlPatient",parent.document).css("display","none");
    sessionStorage.setItem("IsSearchPanel", "1");
    themeAPIChange();
    if(parent.frames['iframe'])
        parentRef = parent.frames['iframe'].window;
    //recordType = parentRef.dietType;
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("dgridMiniTemplateList", dataOptions);
    angularUIgridWrapper.init();

    var dataOptions1 = {
        pagination: false,
        changeCallBack: onChange1
    }
    angularUIgridWrapper1 = new AngularUIGridWrapper("dgridSelTemplateList", dataOptions1);
    angularUIgridWrapper1.init();

    buildDeviceListGrid([]);
    buildDeviceListGrid1([]);
});

$(window).load(function(){
    loading = false;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
    $("#btnEdit").prop("disabled", true);
    $("#btnDelete").prop("disabled", true);
    // $("#cmbTemplates").kendoComboBox();
    //getTemplates();
    var ipaddress = ipAddress+"/homecare/templates/?is-active=1&is-deleted=0";
    getAjaxObject(ipaddress,"GET",handleGetTemplateList,onError);
    //init();
    buttonEvents();
    adjustHeight();
}
function handleGetTemplateList(dataObj){
    console.log(dataObj);
    var vacation = [];
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            if(dataObj.response.templates){
                if($.isArray(dataObj.response.templates)){
                    vacation = dataObj.response.templates;
                }else{
                    vacation.push(dataObj.response.templates);
                }
            }
        }
    }
    for(var j=0;j<vacation.length;j++){
        vacation[j].idk = vacation[j].id;
    }
    buildDeviceListGrid1(vacation);
}
/*function getTemplates(){
	var ipaddress = ipAddress+"/homecare/form-templates/";
	getAjaxObject(ipaddress,"GET",handleGetTemplateList,onError);
}*/
/*function handleGetTemplateList(dataObj){
	var vacation = [];
	if(dataObj && dataObj.response && dataObj.response.status ){
		if(dataObj.response.status.code == "1"){
			if(dataObj.response.formTemplates){
				if($.isArray(dataObj.response.formTemplates)){
					vacation = dataObj.response.formTemplates;
				}else{
					vacation.push(dataObj.response.formTemplates);
				}
			}
		}
	}
	for(var j=0;j<vacation.length;j++){
		vacation[j].idk = vacation[j].id;
	}
	setDataForSelection(vacation, "cmbTemplates", function(){}, ["name", "idk"], 0, "");
}*/
function init(){
    $("#btnEdit").prop("disabled", true);
    $("#btnDelete").prop("disabled", true);
    buildDeviceListGrid([]);
    //buildDeviceListGrid1([]);
    var selectedItems = angularUIgridWrapper1.getSelectedRows();
    console.log(selectedItems);
    buildDeviceListGrid([]);
    if(selectedItems && selectedItems.length>0 && selectedItems[0].idk){
        var ipaddress = ipAddress+"/homecare/mini-templates/?is-active=1&is-deleted=0&templateId="+selectedItems[0].idk;
        getAjaxObject(ipaddress,"GET",handleGetVacationList,onError);
    }

    //getAjaxObject(ipAddress+"/homecare/activity-types/?is-active=1&is-deleted=0&sort=display-order","GET",getActivityTypes,onError);
}

function handleGetVacationList(dataObj){
    console.log(dataObj);
    var vacation = [];
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            if(dataObj.response.miniTemplates){
                if($.isArray(dataObj.response.miniTemplates)){
                    vacation = dataObj.response.miniTemplates;
                }else{
                    vacation.push(dataObj.response.miniTemplates);
                }
            }
        }
    }
    for(var j=0;j<vacation.length;j++){
        vacation[j].idk = vacation[j].id;
    }
    buildDeviceListGrid(vacation);
}
function onError(errorObj){
    console.log(errorObj);
}

function buttonEvents(){
    $("#btnSave").off("click",onClickSave);
    $("#btnSave").on("click",onClickSave);


    $("#btnCancelDet").off("click",onClickCancel);
    $("#btnCancelDet").on("click",onClickCancel);


    $("#btnEdit").off("click",onClickEdit);
    $("#btnEdit").on("click",onClickEdit);

    $("#btnDelete").off("click",onClickDelete);
    $("#btnDelete").on("click",onClickDelete);

    $('#btnAdd').on('click', function (e) {
        e.preventDefault();
        $("#ptframe",parent.document).attr('src', "../../html/forms/templateDesignList.html");
    });
}

function onClickDelete(){
    customAlert.confirm("Confirm", "Are you sure to delete?",function(response){
        if(response.button == "Yes"){
            var dataObj = {};
            dataObj.createdBy = Number(sessionStorage.userId);
            dataObj.isDeleted = 1;
            dataObj.isActive = 0;
            var dataUrl = ipAddress +"/homecare/mini-templates/";
            var method = "DELETE";
            var selectedItems = angularUIgridWrapper.getSelectedRows();
            dataObj.id = selectedItems[0].idk;
            createAjaxObject(dataUrl, dataObj, method, onCreateDelete, onError);
        }
    });

}
function onCreateDelete(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "Mini Template deleted successfully";
            customAlert.error("Info", msg);
            operation = ADD;
            init();
        }
    }
}
function onClickSave(){
    try{
        //var cmbTemplates = $("#cmbTemplates").data("kendoComboBox");
        var selectedItems = angularUIgridWrapper1.getSelectedRows();
        console.log(selectedItems);
        var strTemp = $("#txtMiniTemplateName").val();
        // var cols = $('#noOfColumns').val() || 2;
        var cols = 2;
        strTemp = $.trim(strTemp);
        if(strTemp != ""){
            var reqData = {};
            reqData.name = strTemp;
            reqData.label = cols;
            reqData.notes = $("#txtNotes").val();
            reqData.templateId = Number(selectedItems[0].idk);//Number(cmbTemplates.value());
            //reqData.formTemplateId = cmbTemplates.value();
            reqData.isActive = 1;
            reqData.isDeleted = 0;
            reqData.createdBy = Number(sessionStorage.userId);

            console.log(reqData);
            var method = "POST";
            if(operation == UPDATE){
                var selectedItems1 = angularUIgridWrapper.getSelectedRows();
                reqData.id = selectedItems1[0].idk;
                method = "PUT";
            }
            dataUrl = ipAddress +"/homecare/mini-templates/";
            createAjaxObject(dataUrl, reqData, method, onCreate, onError);
        }
    }catch(ex){
        console.log(ex);
    }
}
function onCreate(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "";
            msg = "Mini Template Created Successfully";
            if(operation == UPDATE){
                msg = "Mini Template Updated Successfully";
            }
            displaySessionErrorPopUp("Info", msg, function(res) {
                resetData();
                operation = ADD;
                //onChange1();
                init();
            })
        }else{
            customAlert.error("Error", dataObj.response.status.message);
        }
    }else{

    }
}

function resetData(){
    $("#txtNotes").val("");
    $("#txtMiniTemplateName").val("");
}
function adjustHeight(){
    var defHeight = 260;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    if(angularUIgridWrapper){
        angularUIgridWrapper.adjustGridHeight(cmpHeight+8);
    }
    if(angularUIgridWrapper1){
        angularUIgridWrapper1.adjustGridHeight((cmpHeight+160));
    }
}

function buildDeviceListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Name",
        "field": "name",
    });
    gridColumns.push({
        "title": "Display Name",
        "field": "notes",
    });
    gridColumns.push({
        "title": "Display Order",
        "field": "displayOrder",
    });
    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}
function buildDeviceListGrid1(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Template",
        "field": "name",
        "width":"100%"
    });
    angularUIgridWrapper1.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}

var selRow = null;
function onClickAction(){

}

var prevSelectedItem =[];
function onChange(){
    setTimeout(function() {
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if (selectedItems && selectedItems.length > 0) {
            var obj = selectedItems[0];
            atID = obj.idk;
            $("#btnEdit").prop("disabled", false);
            $("#btnDelete").prop("disabled", false);
            //onClickEdit();
        } else {
            $("#btnEdit").prop("disabled", true);
            $("#btnDelete").prop("disabled", true);
            //resetData();
        }
    });
}
function onChange1(){
    setTimeout(function() {
        var selectedItems = angularUIgridWrapper1.getSelectedRows();
        console.log(selectedItems);
        buildDeviceListGrid([]);
        if(selectedItems && selectedItems.length>0){
            $("#divEvents").removeClass("addEvents");
            /*var miniTemplates = selectedItems[0].miniTemplates;
            var arr = [];
            if($.isArray(miniTemplates)){
                arr = miniTemplates;
            }else{
                arr.push(miniTemplates);
            }
            for(var x1=0;x1<miniTemplates.length;x1++){
                miniTemplates[x1].idk = miniTemplates[x1].id;
            }
            buildDeviceListGrid(miniTemplates);*/
            init();
        }else{
            $("#divEvents").addClass("addEvents");
        }
    });
}
var operation = "ADD";
var ADD = "ADD";
var UPDATE = "UPDATE";
var atID = "";
var ds  = "";
function onClickEdit(){
    var selectedItems = angularUIgridWrapper.getSelectedRows();
    console.log(selectedItems);
    if (selectedItems && selectedItems.length > 0) {
        var obj = selectedItems[0];
        operation = UPDATE;
        $("#txtMiniTemplateName").val(obj.name);
        $('#noOfColumns').val(obj.label)
        $("#txtNotes").val(obj.notes);
        // getComboListIndex("cmbTemplates", "idk", obj.formTemplateId);
    }
}

function getComboListIndex(cmbId, attr, attrVal) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb) {
        var ds = cmb.dataSource;
        var totalRec = ds.total();
        for (var i = 0; i < totalRec; i++) {
            var dtItem = ds.at(i);
            if (dtItem && dtItem[attr] == attrVal) {
                cmb.select(i);
                return i;
            }
        }
    }
    return -1;
}
function closeVideoScreen(evt,returnData){

}
var operation = "add";
var selFileResourceDataItem = null;
function onClickCancel(){
    var obj = {};
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}


function themeAPIChange(){
    getAjaxObject(ipAddress+"/homecare/settings/?id=2","GET",getThemeValue,onError);
}

function getThemeValue(dataObj){
    console.log(dataObj);
    var tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.activityTypes){
            if($.isArray(dataObj.response.settings)){
                tempCompType = dataObj.response.settings;
            }else{
                tempCompType.push(dataObj.response.settings);
            }
        }
        if(tempCompType.length > 0){
            themesChange(tempCompType[0].value);
        }
    }
}

function themesChange(themeValue){
    if(themeValue == 2){
        loadAPi("../../Theme2/Theme02.css");
    }
    else if(themeValue == 3){
        loadAPi("../../Theme3/Theme03.css");
    }
}

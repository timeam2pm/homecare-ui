var angularUIgridWrapper;
var angularUIgridWrapper1;
var parentRef = null;
var recordType = "1";
var dataArray = [];
var selectedMinitemplates = [];
var curMinitTemplate  ={};
var dataTypeArray = [{Key:'Text',Value:'1'},{Key:'Number',Value:'2'},{Key:'Boolean',Value:'3'}];
var unitArray =  [{Key:'Kg',Value:'1'},{Key:'Lt',Value:'2'}];
var statusArray =  [{Key:'ACTIVE',Value:'1'},{Key:'INACTIVE',Value:'2'}];
var components =[];
var formdataArr = [{Key:'Normal',Value:'Normal'},{Key:'Bold',Value:'Bold'},{Key:'Italic',Value:'Italic'}];
var formatdataArr = [{Key:'Right',Value:'Right'},{Key:'Left',Value:'Left'}];
var colourdataArr = [{Key:'Black',Value:'Black'},{Key:'Red',Value:'Red'},{Key:'Green',Value:'Green'},{Key:'Yellow',Value:'Yellow'},{Key:'Bllue',Value:'Blue'},{Key:'Green',Value:'Green'}];

$(document).ready(function(){
    if(parent.frames['iframe'])
        parentRef = parent.frames['iframe'].window;
    $("#divMain",parent.document).css("display","none");
    $("#divPatInfo",parent.document).css("display","");
    $("#lblTitleName",parent.document).html("Form Designer");

    //recordType = parentRef.dietType;
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    /*angularUIgridWrapper = new AngularUIGridWrapper("dgridFormDesignTemplateList", dataOptions);
    angularUIgridWrapper.init();*/

    var dataOptions1 = {
        pagination: false,
        changeCallBack: onChange1
    }
    //angularUIgridWrapper1 = new AngularUIGridWrapper("dgridFromTemplateList", dataOptions1);
    //angularUIgridWrapper1.init();

    buildDeviceListGrid([]);
    buildDeviceListGrid1([]);
});

$(window).load(function(){
    loading = false;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
    $("#btnEdit").prop("disabled", true);
    $("#btnDelete").prop("disabled", true);
    $("#cmbTemplates").kendoComboBox();

    $("#txtCompType").kendoComboBox();
    $("#txtDT").kendoComboBox();
    $("#cmbUnits").kendoComboBox();
    $("#cmbStatus").kendoComboBox();
    $("#cmbMiniTemplates").kendoComboBox();
    $("#cmbNamefontweight").kendoComboBox();
    $("#cmbNameformat").kendoComboBox();
    $("#cmbNamecolor").kendoComboBox();
    $("#txtDataType").kendoComboBox();

    setDataForSelection(formdataArr, "cmbNamefontweight", onNamefontweightChange, ["Key", "Value"], 0, "");
    setDataForSelection(formatdataArr, "cmbNameformat", onNameformatChange, ["Key", "Value"], 0, "");
    setDataForSelection(colourdataArr, "cmbNamecolor", onNameColorChange, ["Key", "Value"], 0, "");

    //setDataForSelection(dataTypeArray, "txtDT", function(){}, ["Key", "Value"], 0, "");
    //setDataForSelection(unitArray, "cmbUnits", function(){}, ["Key", "Value"], 0, "");
    setDataForSelection(statusArray, "cmbStatus", function(){}, ["Key", "Value"], 0, "");

    $("#txtChars").kendoNumericTextBox({step:1,min:1,max:255,decimals: 0,value:10,format: "0",change:onChangeMETime});
    //getTemplates();
    var ipaddress = ipAddress+"/homecare/templates/?is-active=1&is-deleted=0";
    getAjaxObject(ipaddress,"GET",handleGetTemplateList,onError);
    getDataTypes();
    getUnits();
    //init();
    buttonEvents();
    adjustHeight();
}
function getComponents(){
    getAjaxObject(ipAddress+"/master/type/list/?name=component","GET",getComponentTypes,onError);
}
function getDataTypes(){
    getAjaxObject(ipAddress+"/homecare/data-types/","GET",onGetDataTypes,onError);
}
var formDataTypes = [];
function onGetDataTypes(dataObj){
    console.log(dataObj);
    var tempDataType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.dataTypes){
            if($.isArray(dataObj.response.dataTypes)){
                tempDataType = dataObj.response.dataTypes;
            }else{
                tempDataType.push(dataObj.response.dataTypes);
            }
        }
    }
    formDataTypes = JSON.parse(JSON.stringify(tempDataType));


    tempDataType = tempDataType.filter(function(item){
        return item.code.toLowerCase().indexOf('date') < 0;
    });


    setDataForSelection(tempDataType, "txtDataType", onDataTypeChange, ["code", "id"], 0, "");
    //setDataForSelection(tempDataType, "txtDT", function(){}, ["value", "id"], 0, "");
    getComponents();
}
function onDataTypeChange(){
    $("#listMaxValue").hide();
    $("#listMinValue").hide();
    $("#listFormat").hide();
    $("#listEncrypted").hide();
    $('#maxChars').hide();

    var txtDataTypeVal = $("#txtDataType").val();
    if(txtDataTypeVal == '1'){
        $("#listMaxValue").show();
        $("#listMinValue").show();
    }
    else if(txtDataTypeVal == '4'){
        $("#listFormat").show();
    }
    else if(txtDataTypeVal == '2'){
        $("#listEncrypted").show();
        $('#maxChars').show();
    }

    console.log(txtDataTypeVal);
}
function getUnits(){
    getAjaxObject(ipAddress+"/homecare/units/","GET",onGetUnits,onError);
}
function onGetUnits(dataObj){
    console.log(dataObj);
    var tempUnits = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.units){
            if($.isArray(dataObj.response.units)){
                tempUnits = dataObj.response.units;
            }else{
                tempUnits.push(dataObj.response.units);
            }
        }
    }
    if(tempUnits.length>0){
        var obj = {};
        obj.id = "";
        obj.value = "";
        tempUnits.unshift(obj)
    }
    setDataForSelection(tempUnits, "cmbUnits", function(){}, ["value", "id"], 0, "");
}
function getComponentTypes(dataObj){
    var tempCompType = [];
    compType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.typeMaster){
            if($.isArray(dataObj.response.typeMaster)){
                tempCompType = dataObj.response.typeMaster;
            }else{
                tempCompType.push(dataObj.response.typeMaster);
            }
        }
    }
    for(var i=0;i<tempCompType.length;i++){
        var obj = {};
        obj.Key = tempCompType[i].type;
        obj.Value = tempCompType[i].id;
        compType.push(obj);
    }
    setDataForSelection(compType, "txtCompType", onComponentChange, ["Key", "Value"], 0, "");
    //setDataForSelection(tempDataType, "txtDT", function(){}, ["value", "id"], 0, "");
    onComponentChange();
}
var COMP_TEXT = "text box";
var COMP_TEXT_AREA = "text area"
var COMP_BOOLEAN = "boolean";
var COMP_CHECK_BOX = "check box"
var COMP_RADIO_BUTTON = "radio button"
var COMP_GROUP_CHECK_BOX = "group check box"
var COMP_IMAGE = "image"
var COMP_DATE_PICKER = "date picker";

var DT_INT = "INT";
var DT_VARCHAR = "VARCHAR";
var DT_DATETIME = "DATETIME";
var DT_DECIMAL = "DECIMAL";
var DT_DATE = "DATE";


function onComponentChange(){
    var tempDataType = [];
    setDataForSelection([], "txtDT", function(){}, ["", ""], 0, "");
    //getNumericBoxValue("txtChars");
    setNumericBoxValue("txtChars", "");
    enableDisableNumericBox("txtChars", false);
    var txtDT = $("#txtDT").data("kendoComboBox");
    if(txtDT){
        txtDT.text("");
        txtDT.value("");
    }
    var txtCompType = $("#txtCompType").data("kendoComboBox");
    if(txtCompType && txtCompType.selectedIndex>=0){
        if(txtCompType){
            if(txtCompType.text() == COMP_TEXT || txtCompType.text() == COMP_TEXT_AREA){
                enableDisableNumericBox("txtChars", true);
                setNumericBoxValue("txtChars", "20");
                for(var i=0;i<formDataTypes.length;i++){
                    var item = formDataTypes[i];
                    if(item && (item.value == DT_INT || item.value == DT_VARCHAR || item.value == DT_DECIMAL)){
                        tempDataType.push(item);
                    }
                }
            }else if(txtCompType.text() == COMP_DATE_PICKER){
                for(var i=0;i<formDataTypes.length;i++){
                    var item = formDataTypes[i];
                    if(item && (item.value == DT_DATE || item.value == DT_DATETIME)){
                        tempDataType.push(item);
                    }
                }
            }
        }
    }
    setDataForSelection(tempDataType, "txtDT", function(){}, ["value", "id"], 0, "");
}
function onChangeMETime(event){
    onChangeNumericBox(event);
}
function onChangeNumericBox(event){
    if(event && event.sender && event.sender.element){
        var boxId = $(event.sender.element).attr("id");
        if(boxId){
            var txtSpBandBegin1 = $("#"+boxId).data("kendoNumericTextBox");
            if(txtSpBandBegin1){
                if(txtSpBandBegin1.value() == ""){
                    return;
                }
                if(txtSpBandBegin1 && txtSpBandBegin1.value() !=0  && !txtSpBandBegin1.value()){
                    var maxVal = txtSpBandBegin1.min();
                    //txtSpBandBegin1.value(maxVal);
                }
            }
        }
    }
}
function handleGetTemplateList(dataObj){
    console.log(dataObj);
    var vacation = [];
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            if(dataObj.response.templates){
                if($.isArray(dataObj.response.templates)){
                    vacation = dataObj.response.templates;
                }else{
                    vacation.push(dataObj.response.templates);
                }
            }
        }
    }
    for(var j=0;j<vacation.length;j++){
        vacation[j].idk = vacation[j].id;
    }
    setDataForSelection(vacation, "cmbTemplates", onTemplateChange, ["name", "idk"], 0, "");
    onTemplateChange();
    //buildDeviceListGrid1(vacation);
}
function onTemplateChange(){
    var cmbTemplates = $("#cmbTemplates").data("kendoComboBox");
    //buildDeviceListGrid1([]);
    var selectedItems = cmbTemplates.dataItem();
    var miniTemplates = selectedItems.miniTemplates;
    var arr = [];
    if($.isArray(miniTemplates)){
        arr = miniTemplates;
    }else{
        arr.push(miniTemplates);
    }
    selectedMinitemplates = miniTemplates;
    //buildDeviceListGrid1([]);
    arr = miniTemplates;
    var miniArray = [];
    for(var x1=0;x1<arr.length;x1++){
        arr[x1].idk = arr[x1].id;
        var obj = {};
        if(!arr[x1].isDeleted){
            obj.idk = arr[x1].id;
            obj.name = arr[x1].name;
            miniArray.push(obj);
        }

    }
    setDataForSelection(miniArray, "cmbMiniTemplates", onTemplateChange1, ["name", "idk"], 0, "");
    setTimeout(function(){

        //buildDeviceListGrid1(miniArray);
        //onChange1();
        init();
        adjustHeight();
    },100);
    $('.nav-tabs a[href="#addFields"]').tab('show');

    //
}
function onTemplateChange1(){
    var cmbTemplates = $("#cmbMiniTemplates").data("kendoComboBox");
    //buildDeviceListGrid1([]);
    var selectedItems = cmbTemplates.dataItem();
    console.log(selectedItems);
    $('.nav-tabs a[href="#addFields"]').tab('show');
    /*var miniTemplates = selectedItems.miniTemplates;
    var arr = [];
    if($.isArray(miniTemplates)){
        arr = miniTemplates;
    }else{
        arr.push(miniTemplates);
    }
     //buildDeviceListGrid1([]);
    arr = miniTemplates;
    var miniArray = [];
    for(var x1=0;x1<arr.length;x1++){
        arr[x1].idk = arr[x1].id;
        var obj = {};
        obj.idk = arr[x1].id;
        obj.name = arr[x1].name;
        miniArray.push(obj);
    }*/
    setTimeout(function(){
        //setDataForSelection(miniArray, "cmbMiniTemplates", onTemplateChange1, ["name", "idk"], 0, "");
        //buildDeviceListGrid1(miniArray);
        //onChange1();
        init();
        adjustHeight();

    },100);

    //
}
/*function getTemplates(){
	var ipaddress = ipAddress+"/homecare/form-templates/";
	getAjaxObject(ipaddress,"GET",handleGetTemplateList,onError);
}*/
/*function handleGetTemplateList(dataObj){
	var vacation = [];
	if(dataObj && dataObj.response && dataObj.response.status ){
		if(dataObj.response.status.code == "1"){
			if(dataObj.response.formTemplates){
				if($.isArray(dataObj.response.formTemplates)){
					vacation = dataObj.response.formTemplates;
				}else{
					vacation.push(dataObj.response.formTemplates);
				}
			}
		}
	}
	for(var j=0;j<vacation.length;j++){
		vacation[j].idk = vacation[j].id;
	}
	setDataForSelection(vacation, "cmbTemplates", function(){}, ["name", "idk"], 0, "");
}*/
function init(type){
    if(type && type == 'modify'){
        var sel = 'body[ng-controller="AppCtrl"]';
        var ctrlScope = angular.element(document.body).scope();
        var $body = angular.element(document.body);
        var $rootScope = $body.scope().$root;
        AppCtrl(ctrlScope,$rootScope);
    }
    $("#btnEdit").prop("disabled", true);
    $("#btnDelete").prop("disabled", true);
    //buildDeviceListGrid([]);
    //buildDeviceListGrid1([]);
    if($("#cmbMiniTemplates").data("kendoComboBox"))
        var selectedItems = $("#cmbMiniTemplates").data("kendoComboBox").dataItem();
    console.log(selectedItems);
    console.log(selectedMinitemplates);
    curMinitTemplate = _.findWhere(selectedMinitemplates,{idk:selectedItems.idk});
    console.log(curMinitTemplate);
    if(selectedItems){
        var ipaddress = ipAddress+"/homecare/components/?is-active=1&is-deleted=0&miniTemplateId="+selectedItems.idk+"&fields=*,dataType.*,unit.*,component.*";
        getAjaxObject(ipaddress,"GET",handleGetVacationList,onError);

    }
    adjustHeight();
    //getAjaxObject(ipAddress+"/homecare/activity-types/?is-active=1&is-deleted=0&sort=display-order","GET",getActivityTypes,onError);
}

function handleGetVacationList(dataObj){
    console.log(dataObj);
    var vacation = [];
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            if(dataObj.response.components){
                if($.isArray(dataObj.response.components)){
                    vacation = dataObj.response.components;
                }else{
                    vacation.push(dataObj.response.components);
                }
            }
        }
    }
    for(var j=0;j<vacation.length;j++){
        vacation[j].idk = vacation[j].id;
        vacation[j].length1 = vacation[j].length;
    }
    buildDeviceListGrid(vacation);
    components = dataObj.response.components || [];
    components = _.sortBy(components, 'displayOrder');
    prepareComponents(components);
    console.log(components);
}
function onError(errorObj){
    console.log(errorObj);
}

function buttonEvents(){
    $("#btnSave").off("click",onClickSave);
    $("#btnSave").on("click",onClickSave);

    $("#btnCancel").off("click",onTemplateChange1);
    $("#btnCancel").on("click",onTemplateChange1);


    $("#btnCancelDet").off("click",onClickCancel);
    $("#btnCancelDet").on("click",onClickCancel);


    $("#btnEdit").off("click",onClickEdit);
    $("#btnEdit").on("click",onClickEdit);

    $("#btnDelete").off("click",onClickDelete);
    $("#btnDelete").on("click",onClickDelete);
}

function onClickDelete(id){
    customAlert.confirm("Confirm", "Are you sure to delete?",function(response){
        if(response.button == "Yes"){
            var dataObj = {};
            dataObj.createdBy = Number(sessionStorage.userId);;
            dataObj.isDeleted = 1;
            dataObj.isActive = 0;
            var dataUrl = ipAddress +"/homecare/components/";
            var method = "DELETE";
            //var selectedItems = angularUIgridWrapper.getSelectedRows();
            dataObj.id = id;
            createAjaxObject(dataUrl, dataObj, method, onCreateDelete, onError);
        }
        else{
            init('modify');
        }
    });

}
function onCreateDelete(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "Component deleted successfully";
            customAlert.error("Info", msg);
            operation = ADD;
            //location.reload();
            init('modify');
            $('.nav-tabs a[href="#addFields"]').tab('show');
        }
    }
}
var component = {
    //"notes": "",
    // "columns": "",
    // "predefinedValue": null,
    "displayOrder": 1,
    "isActive": 1,
    "noDuplicates": 0,
    //"minimumValue": "",
    "isDeleted": 0,
    //"componentTypeId": 1,
    "isEncrypted": 0,
    //"unitId": null,
    //"modifiedBy": null,
    //"placeholderText": "Enter name",
    //"id": 1,
    "isRequired": 0,
    //"dataTypeId": 1,
    //"length": null,
    //"format": "format",
    //"label": "comp",
    //"createdDate": null,
    //"createdBy": 101,
    //"miniTemplateId": 1,
    //"hint": "hint",
    //"name": "comp",
    //"modifiedDate": null,
    "maximumValue": "100"
}
//jQuery.extend({}, objectIsOld);

function onClickSave(){
    try{
        /*var txtCompType = $("#txtCompType").data("kendoComboBox");
        var txtDT = $("#txtDT").data("kendoComboBox");
        var cmbUnits = $("#cmbUnits").data("kendoComboBox");
         var selectedItems = angularUIgridWrapper1.getSelectedRows();*/
        //console.log(ajaxReqObject.component);
        var isFlag = false;
        if(dragAndDrop != true) {
            if (ajaxReqObject && ajaxReqObject.component && ajaxReqObject.component.name) {
                isFlag = true;
                ajaxReqObject.component.maximumValue = $('#fieldMaxValue').val() && $('#fieldMaxValue').val().length > 0 ? parseInt($('#fieldMaxValue').val()) : null;
                ajaxReqObject.component.minimumValue = $('#fieldMinValue').val() && $('#fieldMinValue').val().length > 0 ? parseInt($('#fieldMinValue').val()) : null;
                ajaxReqObject.component.format = $('#fieldFormat').val() || null;
                ajaxReqObject.component.dataTypeId = $("#txtDataType").val() != undefined && $("#txtDataType").val().length > 0 ? parseInt($("#txtDataType").data("kendoComboBox").value()) : '';
                if (ajaxReqObject.component.componentTypeId == 8) {
                    ajaxReqObject.component.dataTypeId = 5;
                } else if (ajaxReqObject.component.componentTypeId == 19) {
                    ajaxReqObject.component.dataTypeId = 3;
                }
                ajaxReqObject.component.unitId = parseInt($("#cmbUnits").data("kendoComboBox").value());
                // ajaxReqObject.component.length = getNumericBoxValue("txtMaxChars");
                ajaxReqObject.component.length = Number($("#txtMaxChars").val());
                ajaxReqObject.component.miniTemplateId = parseInt($("#cmbMiniTemplates").data("kendoComboBox").value());
            }
            if (isFlag) {

                Object.keys(ajaxReqObject.component).forEach(function (k) {
                    console.log(k + ' - ' + ajaxReqObject.component[k]);
                    if (k.toLowerCase() == 'length1')
                        delete ajaxReqObject.component[k];

                    if (ajaxReqObject.component[k] == null || _.isNaN(ajaxReqObject.component[k]))
                        delete ajaxReqObject.component[k];
                });

                console.log(finalObject);
                var eleFound = false;
                for (var i = 0; i < finalObject.length; i++) {
                    if (ajaxReqObject && ajaxReqObject.id == finalObject[i].id) {
                        eleFound = true;
                        break;
                    }
                }
                if (!eleFound)
                    finalObject.push(ajaxReqObject);
                var editMode = false;
                for (var i = 0; i < components.length; i++) {
                    if (ajaxReqObject.id == components[i].id) {
                        editMode = true;
                        Object.keys(ajaxReqObject.component).forEach(function (k) {
                            console.log(k + ' - ' + ajaxReqObject.component[k]);
                            if (k.toLowerCase() == 'length1'
                                || k.toLowerCase() == 'createddate'
                                || k.toLowerCase() == 'modifieddate'
                                || k.toLowerCase() == 'datatypecode'
                                || k.toLowerCase() == 'unitcode'
                                || k.toLowerCase() == 'unitvalue'
                                || k.toLowerCase() == 'datatypevalue'
                                || k.toLowerCase() == 'componenttype'
                                || k.toLowerCase() == 'idk')
                                delete ajaxReqObject.component[k];

                        });
                        ajaxReqObject.modifiedBy = Number(sessionStorage.userId);
                        /*if(ajaxReqObject.component.createdDate)
                        delete ajaxReqObject.component[createdDate];
                        if(ajaxReqObject.component.modifiedDate)
                        delete ajaxReqObject.component[modifiedDate];
                        if(ajaxReqObject.component.dataTypeCode)
                        delete ajaxReqObject.component[dataTypeCode];
                        if(ajaxReqObject.component.unitCode)
                        delete ajaxReqObject.component[unitCode];
                        if(ajaxReqObject.component.unitValue)
                        delete ajaxReqObject.component[unitValue];
                        if(ajaxReqObject.component.dataTypeValue)
                        delete ajaxReqObject.component[dataTypeValue];
                        if(ajaxReqObject.component.componentType)
                        delete ajaxReqObject.component[componentType];
                        if(ajaxReqObject.component.idk)
                        delete ajaxReqObject.component[idk];*/

                        break;
                    }
                }


                var strTemp = $("#txtCompName").val();
                strTemp = $.trim(strTemp);
                //if(strTemp != ""){
                var reqData = {};

                var method = "POST";
                /*if(operation == UPDATE){
                    var selectedItems1 = angularUIgridWrapper.getSelectedRows();
                    reqData.id = selectedItems1[0].idk;
                    method = "PUT";
                }*/
                dataUrl = ipAddress + "/homecare/components/";
                reqData = _.pluck(finalObject, 'component');
                if (reqData) {
                    for (var i = 0; i < reqData.length; i++) {
                        if (editMode) {
                            reqData[i].id = ajaxReqObject.id;
                            method = "PUT";
                            operation = "UPDATE"
                        }
                        createAjaxObject(dataUrl, reqData[i], method, onCreate, onError);
                    }
                }
            }
            else{
                customAlert.error("Error", "Please enter field name.")
            }

        }


    }catch(ex){
        console.log(ex);
    }
}
function getNumericBoxValue(boxId){
    var returnVal = "";
    var numerictextbox = $("#"+boxId).data("kendoNumericTextBox");
    if(numerictextbox){
        returnVal =  numerictextbox.value();
        returnVal = Number(returnVal);
    }
    if(returnVal == null){
        returnVal = "";
    }
    return returnVal;
}
function setNumericBoxValue(boxId,dataVal){
    var numerictextbox = $("#"+boxId).data("kendoNumericTextBox");
    if(numerictextbox){
        numerictextbox.value(dataVal);
    }
}
function enableDisableNumericBox(boxId,flag){
    var numerictextbox = $("#"+boxId).data("kendoNumericTextBox");
    if(numerictextbox){
        numerictextbox.enable(flag);
    }
}
function onCreate(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "";
            msg = "Component Created  Successfully";
            if(operation == UPDATE){
                msg = "Component Updated  Successfully";
            }
            resetFields();
            displaySessionErrorPopUp("Info", msg, function(res) {
                resetData();
                operation = ADD;
                $('.nav-tabs a[href="#addFields"]').tab('show');
                //onChange1();
                init('modify');
            })
            /*var sel = 'body[ng-controller="AppCtrl"]';
            var ctrlScope = angular.element(sel).scope();
            var $body = angular.element(document.body);
            var $rootScope = $body.scope().$root;
            AppCtrl(ctrlScope,$rootScope);*/
            //location.reload();



        }else{
            customAlert.error("Error", dataObj.response.status.message);
        }
    }else{

    }
}

function resetData(){
    $("#txtNotes").val("");
    $("#txtCompName").val("");
}
function adjustHeight(){
    /*var defHeight = 300;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    if(angularUIgridWrapper){
        angularUIgridWrapper.adjustGridHeight((cmpHeight-50));
    }
    if(angularUIgridWrapper1){
        angularUIgridWrapper1.adjustGridHeight((cmpHeight+100));
    }*/
    var defHeight = 150;
    var cmpHeight = 500;
    $("#ptframe").css({ height: (cmpHeight) + 'px !important' });
}

function buildDeviceListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Name",
        "field": "name",
    });
    gridColumns.push({
        "title": "Data Type",
        "field": "dataTypeValue",
    });
    gridColumns.push({
        "title": "Component Type",
        "field": "componentType",
    });
    gridColumns.push({
        "title": "Chars",
        "field": "length1",
    });
    gridColumns.push({
        "title": "UOM",
        "field": "unitValue",
    });
    gridColumns.push({
        "title": "Display Order",
        "field": "displayOrder",
    });
    // angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}
function buildDeviceListGrid1(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Template",
        "field": "name",
    });
    //angularUIgridWrapper1.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}

var selRow = null;
function onClickAction(){

}

var prevSelectedItem =[];
function onChange(){
    setTimeout(function() {
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if (selectedItems && selectedItems.length > 0) {
            var obj = selectedItems[0];
            atID = obj.idk;
            $("#btnEdit").prop("disabled", false);
            $("#btnDelete").prop("disabled", false);
            //onClickEdit();
        } else {
            $("#btnEdit").prop("disabled", true);
            $("#btnDelete").prop("disabled", true);
            //resetData();
        }
    });
}
function onChange1(){
    setTimeout(function() {
        //var selectedItems = angularUIgridWrapper1.getSelectedRows();
        var selectedItems = $("#cmbMiniTemplates").data("kendoComboBox");
        console.log(selectedItems.dataItem());
        // buildDeviceListGrid([]);
        if(selectedItems && selectedItems.length>0){
            $("#divEvents").removeClass("addEvents");
            /*var miniTemplates = selectedItems[0].miniTemplates;
            var arr = [];
            if($.isArray(miniTemplates)){
                arr = miniTemplates;
            }else{
                arr.push(miniTemplates);
            }
            for(var x1=0;x1<miniTemplates.length;x1++){
                miniTemplates[x1].idk = miniTemplates[x1].id;
            }
            buildDeviceListGrid(miniTemplates);*/
            //init();
        }else{
            $("#divEvents").addClass("addEvents");
        }
    });
}
var operation = "ADD";
var ADD = "ADD";
var UPDATE = "UPDATE";
var atID = "";
var ds  = "";
function onClickEdit(){
    var selectedItems = angularUIgridWrapper.getSelectedRows();
    console.log(selectedItems);
    if (selectedItems && selectedItems.length > 0) {
        var obj = selectedItems[0];
        operation = UPDATE;
        $("#txtCompName").val(obj.name);
        $("#txtNotes").val(obj.notes);
        enableDisableNumericBox("txtChars",true);
        var txtChars = $("#txtChars").data("kendoNumericTextBox");
        if(txtChars){
            txtChars.value(obj.length1);
        }
        getComboListIndex("txtCompType", "Value", obj.componentTypeId);
        onComponentChange();
        getComboListIndex("txtDT", "id", obj.dataTypeId);
        getComboListIndex("cmbUnits", "id", obj.unitId);
    }
}

function getComboListIndex(cmbId, attr, attrVal) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb) {
        var ds = cmb.dataSource;
        var totalRec = ds.total();
        for (var i = 0; i < totalRec; i++) {
            var dtItem = ds.at(i);
            if (dtItem && dtItem[attr] == attrVal) {
                cmb.select(i);
                return i;
            }
        }
    }
    return -1;
}
function closeVideoScreen(evt,returnData){

}
var operation = "add";
var selFileResourceDataItem = null;
function onClickCancel(){
    var obj = {};
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}

function onNamefontweightChange(){
    var cmbNamefontweight = $("#cmbNamefontweight").data("kendoComboBox");
    if(cmbNamefontweight && cmbNamefontweight.selectedIndex<0){
        cmbNamefontweight.select(0);
    }
}

function onNameformatChange(){
    var cmbnameformat = $("#cmbnameformat").data("kendoComboBox");
    if(cmbnameformat && cmbnameformat.selectedIndex<0){
        cmbnameformat.select(0);
    }
}

function onNameColorChange(){
    var cmbnamecolor = $("#cmbnamecolor").data("kendoComboBox");
    if(cmbnamecolor && cmbnamecolor.selectedIndex<0){
        cmbnamecolor.select(0);
    }
}

function onDragSave(){

    Loader.showLoader();
    var method = "PUT";
    var operation = "UPDATE"
    var dataUrl = ipAddress +"/homecare/components/";
    for(var i=0;i<savedComponent.length;i++){
        var ajaxReqObject = savedComponent[i];
        if(ajaxReqObject)
        {
            editMode = true;
            Object.keys(ajaxReqObject).forEach(function(k){
                console.log(k + ' - ' + ajaxReqObject[k]);
                if (k.toLowerCase() == 'length1'
                    || k.toLowerCase() == 'createddate'
                    || k.toLowerCase() == 'modifieddate'
                    || k.toLowerCase() == 'datatypecode'
                    || k.toLowerCase() == 'unitcode'
                    || k.toLowerCase() == 'unitvalue'
                    || k.toLowerCase() == 'datatypevalue'
                    || k.toLowerCase() == 'componenttype'
                    || k.toLowerCase() == 'idk')
                    delete ajaxReqObject[k];

            });
            ajaxReqObject.modifiedBy = Number(sessionStorage.userId);

            createDragAndDropAjaxObj(dataUrl, ajaxReqObject, method, onCreate, onError);
        }

        Loader.hideLoader();
        dragAndDrop = false;
        init('modify');

    }
}


function createDragAndDropAjaxObj(dataUrl, dataObj, method, onCreate, onError){
    if (dataUrl.indexOf("?") >= 0) {
        dataUrl = dataUrl + "&access_token=" + sessionStorage.access_token;//+"&tenant="+sessionStorage.tenant;;
    } else {
        dataUrl = dataUrl + "?access_token=" + sessionStorage.access_token;//+"&tenant="+sessionStorage.tenant;;
    }
    Loader.showLoader();
    $.ajax({
        type: method,
        url: dataUrl,
        data: JSON.stringify(dataObj),
        context: this,
        cache: false,
        success: function(data, statusCode, jqXHR) {
            /*Loader.hideLoader();
            successFunction(data);*/
        },
        error: function(jqXHR, textStatus, errorThrown) {
            /*Loader.hideLoader();
            errorFunction(jqXHR);*/
        },
        headers: {
            'tenant': sessionStorage.tenant
        },
        contentType: "application/json",
    });
}


function resetFields(){
    //$("#commonLabel #flvalid").val("Label");
    $("#txtDataType").data("kendoComboBox").select(0);
    $("#cmbUnits").data("kendoComboBox").select(0);
    $("#piprevalue").val("");
    $("#txtMaxChars").val("");
    $("#fieldRequired").prop('checked', false);

    //$("#specLabel #flvalid").val("Label");
    $("#fieldInstructions").val("");
    $("#cmbNamefontweight").data("kendoComboBox").select(0);
    $("#fieldLabelText").val("");
    $("#cmbNameformat").data("kendoComboBox").select(0);
    $("#cmbNamecolor").data("kendoComboBox").select(0);
    $("#fieldMinValue").val("");
    $("#fieldMaxValue").val("");


}
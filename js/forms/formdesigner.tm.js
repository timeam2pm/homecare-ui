var app = angular.module('myapp', []);
app.controller('AppCtrl', AppCtrl);
var currFid = "";

var ajaxReqObject = {};
var dragAndDrop = false;
var finalObject = [];
var savedComponent = [];

var componetTypes = [{
    "id": 1,
    "type": "text"
}, {
    "id": 2,
    "type": "textarea"
}, {
    "id": 4,
    "type": "checkbox"
}, {
    "id": 5,
    "type": "radio"
}, {
    "id": 6,
    "type": "groupcheckbox"
}, {
    "id": 7,
    "type": "image"
}, {
    "id": 8,
    "type": "datepicker"
}, {
    "id": 9,
    "type": "dropdown"
}, {
    "id": 10,
    "type": "email"
},
    {
        "id": 11,
        "type": "name"
    }, {
        "id": 12,
        "type": "address"
    }, {
        "id": 13,
        "type": "phone"
    }, {
        "id": 14,
        "type": "website"
    }, {
        "id": 15,
        "type": "rating"
    }, {
        "id": 16,
        "type": "likert"
    }, {
        "id": 17,
        "type": "time"
    }, {
        "id": 18,
        "type": "price"
    }, {
        "id": 18,
        "type": "number"
    }, {
        "id": 19,
        "type": "datetime"
    }, {
        "id": 20,
        "type": "label"
    }, {
        "id": 21,
        "type": "hline"
    }, {
        "id": 22,
        "type": "vline"
    }, {
        "id": 23,
        "type": "serviceuser"
    }, {
        "id": 24,
        "type": "staff"
    }];
function AppCtrl($scope, $rootScope) {
    noOfFormElements = 0;
    var noOfFancyElements = 0;
    $('#dynamic-from').empty()
    var currFid = "";
    var ajaxReqObject = {};
    var finalObject = [];
    var savedComponent = [];

    $scope.setFormFields = function (eleName, type) {
        $('#btnSave').attr('disabled', true);
        noOfFormElements = $('#dynamic-from')[0].children.length;
        noOfFormElements = noOfFormElements + 1;
        // var value = type.toLowerCase() == 'button' ? 'value="button"': '';
        // var className = type == "text" || type == "textarea" ? "form-control"
        // : type ;
        // var ele = '<'+ eleName +' type="'+type+'" '+ value +
        // 'class="'+className+'">';
        // var labelClass = type==="radio" ? "class='radio-inline' " : "";
        // var formDiv = '<div class="form-group"><label
        // for="'+type+'"'+labelClass+ '>'+type+':</label>';
        // var options = "<option value=option1>option 1</option><option
        // value=option2>option 2</option><option value=option3>option
        // 3</option></select>";
        // if(eleName.toLowerCase() == 'select')
        // ele = formDiv + ele + options + '</div>';
        // else
        // ele = formDiv + ele + '</div>';
        var ele = "";
        var id = "field" + noOfFormElements;
        if (eleName.toLowerCase() == 'input')
            ele = prepareInputElements(eleName, type, noOfFormElements);
        else if (eleName.toLowerCase() == 'dropdown')
            ele = prepareDropDowns(eleName, type, noOfFormElements);
        else if (eleName.toLowerCase() == 'textarea')
            ele = prepareTextArea(eleName, type, noOfFormElements);
        else if (eleName.toLowerCase() == 'hline')
            ele = prepareInputElements(eleName, type, noOfFormElements);
        else if (eleName.toLowerCase() == 'serviceuser')
            ele = prepareServiceUserElements(eleName, type, noOfFormElements);
        else if (eleName.toLowerCase() == 'staff')
            ele = prepareStaffElements(eleName, type, noOfFormElements);
        else if (eleName.toLowerCase() == 'image')
            ele = prepareInputElements(eleName, type, noOfFormElements);

        $('#dynamic-from').append(ele);
        fieldSettings($(ele), type, '', '', 'fields');

        if (eleName.toLowerCase() == 'input') {
            if (type.toLowerCase() == 'text') {
                $("#lblComponentType").text("Textbox");
            }
            else if (type.toLowerCase() == 'label') {
                $("#lblComponentType").text("Label");
            }
            else if (type.toLowerCase() == 'number') {
                $("#lblComponentType").text("Number");
            }
            else if (type.toLowerCase() == 'checkbox') {
                $("#lblComponentType").text("Checkbox");
            }
            else if (type.toLowerCase() == 'radio') {
                $("#lblComponentType").text("Radio");
            }
            else if (type.toLowerCase() == 'hline') {
                $("#lblComponentType").text("Horizontal line");
            }
        }
        else if (eleName.toLowerCase() == 'dropdown') {
            $("#lblComponentType").text("Dropdown");
        }
        else if (eleName.toLowerCase() == 'textarea') {
            $("#lblComponentType").text("Textarea");
        }
        else if (eleName.toLowerCase() == 'serviceuser') {
            $("#lblComponentType").text("Serviceuser");
        }
        else if (eleName.toLowerCase() == 'staff') {
            $("#lblComponentType").text("Staff");
        }
        else if (eleName.toLowerCase() == 'image') {
            $("#lblComponentType").text("Image");
        }



    };
    $scope.setFancyFields = function (type, className) {
        $('#btnSave').attr('disabled', true);
        noOfFormElements = $('#dynamic-from')[0].children.length;

        noOfFancyElements = noOfFormElements + 1;
        var ele = "";
        var id = noOfFancyElements;

        var deleteEle = '<a href="javascript:void(0);" class="trash" onclick="removeEle(this,\'cfl'
            + id
            + '\')"><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a>';
        if (type.toLowerCase() == 'name') {
            var onclick = 'onclick="fieldSettings(this,\'name\',\'\',\'\',\'comefrom\')"';
            // ele = '<div class="customfields cnames maindiv col-xs-12" '+onclick+' id="cfl'
            // 		+ id
            // 		+ '"><div class="leftBlock"><label class="title" id="titleLabel">Your Name</label></div><div class="rightBlock"><div class="namegroup"><input type="text" class="form-control first" id="cff'
            // 		+ id
            // 		+ '"/><span>First</span></div><div class="namegroup"><input type="text" class="form-control middle" id="cfm'
            // 		+ id
            // 		+ '"/><span>Middle</span></div><div class="namegroup"><input type="text" class="form-control last" id="cfl'
            // 		+ id
            // 		+ '"/><span>Last</span></div><div class="deleteblock">'
            // 		+ deleteEle + '</div></div></div>';

            // var firstRow = '<div class="col-xs-1"><label>Last</label></div><div class="col-xs-1"><label>First</label></div><div class="col-xs-1"><label>Middle</label></div><br clear="all"/>';
            // var secondRow = '<div class="col-xs-1"><label>DOB</label></div><div class="col-xs-1"><label>Gender</label></div><br clear="all"/>';
            //
            // var fourthRow = '<div class="col-xs-2"><label>Address1</label></div><div class="col-xs-2"><label> Address2</label></div><br clear="all"/><div class="col-xs-1"><label>City</label></div><div class="col-xs-2"><label>State/County</label></div>';
            // var fifthRow = '<div class="col-xs-1"><label>Country</label></div><br clear="all"/><div class="col-xs-3"><label>Home Phone</label></div><br clear="all"/><div class="col-xs-3"><label>Cell Phone</label></div><br clear="all"/>';
            ele = '<div class="customfields cnames maindiv col-xs-12" ' + onclick + ' id="cfl'
                + id
                + '"><div class="leftBlock"><label class="title" id="titleLabel">Your Name</label></div><div class="rightBlock"><div class="col-xs-1"><label id="cfl'
                + id
                + '">Last</label></div><div class="col-xs-1"><label id="cff'
                + id
                + '">First</label></div><div class="col-xs-1"><label id="cfm'
                + id
                + '">Middle</label></div><div class="deleteblock">'
                + deleteEle + '</div></div></div>';


        }
        // else if (type && type.toLowerCase() == 'label') {
        // ele = '<div class="customfields cphone maindiv"
        // onclick="fieldSettings(this,\'label\')" id="cfl'
        // + id
        // + '"><label id="label'
        // + id
        // + '">Label</label>' + deleteEle + '</div>';
        // }

        else if (type && type.toLowerCase() == 'phone') {
            var onclick = 'onclick="fieldSettings(this,\'phone\',\'\',\'\',\'comefrom\')"';
            ele = '<div class="customfields cphone maindiv" ' + onclick + ' id="cfl'
                + id
                + '"><div class="leftBlock"><label id="titleLabel">Phone Label</label></div><div class="rightBlock"><input type="tel" class="form-control" id="cfi'
                + id + '"/>' + deleteEle + '</div></div>';
        } else if (type && type.toLowerCase() == 'website') {
            var onclick = 'onclick="fieldSettings(this,\'website\',\'\',\'\',\'comefrom\')"';
            ele = '<div class="customfields cwebsite" ' + onclick + ' id="cfl'
                + id
                + '"><div class="leftBlock"><label id="titleLabel">Webstite Label</label></div><div class="rightBlock"><input type="url" class="form-control" id="cfi'
                + id + '"/>' + deleteEle + '</div></div>';
        } else if (type && type.toLowerCase() == 'datetime') {
            var onclick = 'onclick="fieldSettings(this,\'datetime\',\'\',\'\',\'comefrom\')"';
            ele = '<div class="customfields cdatetime maindiv" ' + onclick + ' id="cfl'
                + id
                + '"><div class="leftBlock"><label id="titleLabel">Date & Time Label</label></div><div class="rightBlock"><input type="datetime-local" class="form-control" id="cfi'
                + id + '"/>' + deleteEle + '</div></div>';
        } else if (type && type.toLowerCase() == 'datepicker') {
            var onclick = 'onclick="fieldSettings(this,\'datepicker\',\'\',\'\',\'comefrom\')"';
            ele = '<div class="customfields cdatetime maindiv" ' + onclick + ' id="cfl'
                + id
                + '"><div class="leftBlock"><label id="titleLabel">Date Label</label></div><div class="rightBlock"><input type="date" class="form-control" id="cfi'
                + id + '"/>' + deleteEle + '</div></div>';
        }
        else if (type && type.toLowerCase() == 'address') {
            var onclick = 'onclick="fieldSettings(this,\'address\',\'\',\'\',\'comefrom\')"';
            ele = '<div class="customfields caddress maindiv" ' + onclick + ' id="cfl'
                + id
                + '"><div class="leftBlock"><label  class="title" id="titleLabel">Address Label</label></div><div class="rightBlock"><div class="add-blocks"><label>Address1</label><input type="input"  class="add1 form-control" id="cfadd'
                + id
                + '"/></div><div class="add-blocks"><label>Address2</label><input type="input" class="add2 form-control" id="cfadd2'
                + id
                + '"/></div><div class="add-blocks"><label>City</label><input type="input"  class="city form-control" id="cfcity'
                + id
                + '"/></div><div class="add-blocks"><label>State</label><input type="input" class="state form-control" id="cfstate'
                + id
                + '"/></div><div class="add-blocks"><label>Postal / Zip Code</label><input type="input" class="postal form-control" id="cfpostal'
                + id
                + '"/></div><div class="add-blocks"><label>Country</label><input type="input" class="country form-control" id="cfcount'
                + id + '"/></div>' + deleteEle + '</div></div>';
        } else if (type && type.toLowerCase() == 'rating') {
            //var onclick = 'onclick="fieldSettings(this,\'rating\',\'\',\'\',\'comefrom\')';
            ele = '<div class="customfields crating maindiv" id="rating'
                + id
                + '"><div class="leftBlock"><label id="titleLabel">Rating Label</label></div><div class="rightBlock"><span class="star  clicked" value="1" tabindex="1"><i class="fa fa-star fa-2x" aria-hidden="true"></i></span><span class="star  clicked" value="1" tabindex="1"><i class="fa fa-star fa-2x" aria-hidden="true"></i></span><span class="star  clicked" value="1" tabindex="1"><i class="fa fa-star fa-2x" aria-hidden="true"></i></span><span class="star" value="1" tabindex="1"><i class="fa fa-star fa-2x" aria-hidden="true"></i></span><span class="star" value="1" tabindex="1"><i class="fa fa-star fa-2x" aria-hidden="true"></i></span>'
                + deleteEle + '</div></div>';
        } else if (type && type.toLowerCase() == 'likert') {
            var onclick = 'onclick="fieldSettings(this,\'likert\',\'\',\'\',\'comefrom\')"';
            ele = '<div class="customfields clikert maindiv" ' + onclick + ' id = "likert'
                + id
                + '"><table cellspacing="0"> <caption id="titleLabel" class="title">Likert Label<span id="req_2" class="req"></span></caption> <thead> <tr> <th>&nbsp;</th> <td id="tablecol1">Strongly Disagree</td><td id="tablecol2"> Disagree</td><td id="tablecol3">Agree</td><td id="tablecol4">Strongly Agree</td></tr></thead> <tbody> <tr class="statement"> <th><label for="Field1" id="statement1">Statement One</label></th> <td title="Strongly Disagree"><input id="Field_1" name="Field1" type="radio" tabindex="1" value="Strongly Disagree" ></td><td title="Disagree"><input id="Field_2" name="Field1" type="radio" tabindex="2" value="Disagree" ></td><td title="Agree"><input id="Field_3" name="Field1" type="radio" tabindex="3" value="Agree" ></td><td title="Strongly Agree"><input id="Field_4" name="Field1" type="radio" tabindex="4" value="Strongly Agree" ></td></tr><tr class="alt statement"> <th><label for="Field2" id="statement2">Statement Two</label></th> <td title="Strongly Disagree"><input id="Field_1" name="Field2" type="radio" tabindex="5" value="Strongly Disagree" ></td><td title="Disagree"><input id="Field_2" name="Field2" type="radio" tabindex="6" value="Disagree" ></td><td title="Agree"><input id="Field_3" name="Field2" type="radio" tabindex="7" value="Agree" ></td><td title="Strongly Agree"><input id="Field_4" name="Field2" type="radio" tabindex="8" value="Strongly Agree" ></td></tr><tr class="statement" > <th><label for="Field3" id="statement3">Statement Three</label></th> <td title="Strongly Disagree"><input id="Field_1" name="Field3" type="radio" tabindex="9" value="Strongly Disagree" ></td><td title="Disagree"><input id="Field_2" name="Field3" type="radio" tabindex="10" value="Disagree" ></td><td title="Agree"><input id="Field_3" name="Field3" type="radio" tabindex="11" value="Agree" ></td><td title="Strongly Agree"><input id="Field_4" name="Field3" type="radio" tabindex="12" value="Strongly Agree" ></td></tr></tbody></table>'
                + deleteEle + '</div>';
        }
        $('#dynamic-from').append(ele);
        fieldSettings($(ele), type, '', className, 'fancyfields');


        if (eleName.toLowerCase() == 'datepicker') {
            $("#lblComponentType").text("Datepicker");
        }
        else if (eleName.toLowerCase() == 'datetime') {
            $("#lblComponentType").text("Datetime");
        }
    }
    $scope.stopNavigation = function (event) {
        event.preventDefault();
        event.stopPropagation();
        $('.nav-tabs a[href="#addFields"]').tab('show');
    };
    $scope.validateFields = function (event) {
        event.preventDefault();
        event.stopPropagation();
    }
    $scope.validateFields1 = function (event) {
        currFid = $("#" + currFid.id)[0];
        // var isValid = $(currFid).find('#instruction').text() == "" ||
        // $(currFid).find('#instruction').text() == undefined? false: true;
        var isInvalid = $(currFid).find('#titleLabel').text().toLowerCase() == ""
            || $(currFid).find('#titleLabel').text().toLowerCase() == "text area"
            || $(currFid).find('#titleLabel').text().toLowerCase() == "label"
            || $(currFid).find('#titleLabel').text().toLowerCase() == "field name"
            || $(currFid).find('#titleLabel').text().toLowerCase() == "selection"
            || $(currFid).find('#titleLabel').text().toLowerCase() == "your name"
            || $(currFid).find('#titleLabel').text().toLowerCase() == ""
            || $(currFid).find('#titleLabel').text().toLowerCase() == "address label"
            || $(currFid).find('#titleLabel').text().toLowerCase() == "rating label"
            || $(currFid).find('#titleLabel').text().toLowerCase() == "website label"
            || $(currFid).find('#titleLabel').text().toLowerCase() == "datetimelabel"
            || $(currFid).find('#titleLabel').text().toLowerCase() == "name label"
            || $(currFid).find('#titleLabel').text().toLowerCase() == "rating label"
            || $(currFid).find('#titleLabel').text().toLowerCase() == "likert label"
            || $(currFid).find('#titleLabel').text().toLowerCase() == "phone label";
        // || $(currFid).find('#titleLabel').text() == undefined ? false
        // : true;
        if ($('#' + currFid.id + ' button').text() != undefined
            && $('#' + currFid.id + ' button').text().toLowerCase() == 'button name') {
            isInvalid = false;
            currFid = $("#" + currFid.id)[0];
        }
        /*
         * if ($("#" + currFid.id)[0].className.indexOf('customfields') > -1) {
         * isInvalid = false; currFid = $("#" + currFid.id)[0]; }
         */
        if (isInvalid) {
            event.preventDefault();
            event.stopPropagation();

        } else {

            var formLength = $('#dynamic-from')[0].children.length;
            var position;
            for (var i = 0; i < formLength; i++) {
                if ($('#dynamic-from')[0].children[i].id == currFid.id) {
                    position = i;
                    break;
                }
            }
            if (position > finalObject.length - 1) {
                var obj = jQuery.extend({}, ajaxReqObject);
                // var obj = setComponentObject('',currFid,'','',true);
                finalObject.push(obj);
            }

            console.log(finalObject);
        }
    }
}
function prepareServiceUserElements(eleName, type, noOfFormElements) {
    var id = noOfFormElements;
    var groupClass = "";
    var ele = "";
    var className = "";
    var style = "";
    var formDiv = ""
    var ele = '<div class="col-xs-12 maindiv serviceuser noPadding" onclick="fieldSettings(this,\'serviceuser\')" id="main'
        + id + '">';
    var label = '<label for="fi' + id
        + '" id="titleLabel">' + eleName + '</label>';
    var deleteEle = '<a href="javascript:void(0);" class="trash" onclick="removeEle(this,\'cfl'
        + id
        + '\')"><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a>';

    // var firstRow = '<div class="namegroup"><input type="text" class="form-control" id="dob" /><label>ID:</label></div><div class="namegroup"><input type="text" class="form-control" id="first" /><label>First</label></div><div class="namegroup"><input type="text" class="form-control" id="middle" /><label>Middle</label></div><div class="namegroup"><input type="text" class="form-control" id="last" /><label>Last</label></div>';
    // var secondRow = '<div class="namegroup"><input type="text" class="form-control" id="dob" /><label>DOB:</label></div><div class="namegroup"><input type="text" class="form-control" id="gender"><label>Gender</label></div><div class="namegroup"><input type="text" class="form-control" id="marritalstatus" /><label>Marrital Status</label></div><div class="namegroup"><input type="text" class="form-control" id="race" /><label>Race</label></div>';
    // var thirdRow = '<div class="namegroup"><input type="text" class="form-control" id="ethnicity" /><label>Ethnicity</label></div><div class="namegroup"><input type="text" class="form-control" id="prelang" /><label>Preferred Language</label></div><div class="namegroup"><input type="text" class="form-control" id="second" /><label>Second</label></div><div class="namegroup"><input type="text" class="form-control" id="third" /><label>Third</label></div>';
    // var fourthRow = '<div class="namegroup"><input type="text" class="form-control" id="address1" /><label>Address1</label></div><div class="namegroup"><input type="text" class="form-control" id="address2" /><label>Address2</label></div><div class="namegroup"><input type="text" class="form-control" id="city" /><label>City</label></div><div class="namegroup"><input type="text" class="form-control" id="state" /><label>State/County</label></div>';
    // var fifthRow = '<div class="namegroup"><input type="text" class="form-control" id="country" /><label>Country</label></div><div class="namegroup"><input type="text" class="form-control" id="postal" /><label>Postal/Zip Code</label></div><div class="namegroup"><input type="text" class="form-control" id="homephone" /><label>Home Phone</label></div><div class="namegroup"><input type="text" class="form-control" id="mobile" /><label>Mobile</label></div>';


    var firstRow = '<div class="col-xs-1"><label>Last</label></div><div class="col-xs-1"><label>First</label></div><div class="col-xs-1"><label>Middle</label></div><br clear="all"/>';
    var secondRow = '<div class="col-xs-1"><label>DOB</label></div><div class="col-xs-1"><label>Gender</label></div><br clear="all"/>';

    var fourthRow = '<div class="col-xs-2"><label>Address1</label></div><div class="col-xs-2"><label> Address2</label></div><br clear="all"/><div class="col-xs-1"><label>City</label></div><div class="col-xs-2"><label>State/County</label></div>';
    var fifthRow = '<div class="col-xs-1"><label>Country</label></div><br clear="all"/><div class="col-xs-3"><label>Home Phone</label></div><br clear="all"/><div class="col-xs-3"><label>Cell Phone</label></div><br clear="all"/>';


    var sixthRow = '<div class="namegroup"><label>Email</label></div><div class="namegroup"><label>SMS</label></div>';
    formDiv = ele + '<div class="leftBlock">' + label + '</div><div class="rightBlock">' + firstRow + secondRow + fourthRow + fifthRow

        + deleteEle + '</div></div>';
    return formDiv;
}
function prepareStaffElements(eleName, type, noOfFormElements) {
    var id = noOfFormElements;
    var groupClass = "";
    var ele = "";
    var className = "";
    var style = "";
    var formDiv = ""
    var ele = '<div class="col-xs-12 maindiv serviceuser noPadding" onclick="fieldSettings(this,\'staff\')" id="main'
        + id + '">';
    var label = '<label for="fi' + id
        + '" id="titleLabel">' + eleName + '</label>';
    var deleteEle = '<a href="javascript:void(0);" class="trash" onclick="removeEle(this,\'cfl'
        + id
        + '\')"><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a>';

    // var firstRow = '<div class="namegroup"><input type="text" class="form-control" id="sid" /><label>ID:</label></div><div class="namegroup"><input type="text" class="form-control" id="first" /><label>First</label></div><div class="namegroup"><input type="text" class="form-control" id="last" /><label>Last</label></div>';
    // var secondRow = '<div class="namegroup"><input type="text" class="form-control" id="dob" /><label>DOB:</label></div><div class="namegroup"><input type="text" class="form-control" id="gender"><label>Gender</label></div>';
    // var thirdRow = '<div class="namegroup"><input type="text" class="form-control" id="prelang" /><label>Preferred Language</label></div><div class="namegroup"><input type="text" class="form-control" id="second" /><label>Second</label></div><div class="namegroup"><input type="text" class="form-control" id="third" /><label>Third</label></div>';
    // var fourthRow = '<div class="namegroup"><input type="text" class="form-control" id="address1" /><label>Address1</label></div><div class="namegroup"><input type="text" class="form-control" id="address2" /><label>Address2</label></div><div class="namegroup"><input type="text" class="form-control" id="city" /><label>City</label></div><div class="namegroup"><input type="text" class="form-control" id="state" /><label>State/County</label></div>';
    // var fifthRow = '<div class="namegroup"><input type="text" class="form-control" id="country" /><label>Country</label></div><div class="namegroup"><input type="text" class="form-control" id="postal" /><label>Postal/Zip Code</label></div><div class="namegroup"><input type="text" class="form-control" id="homephone" /><label>Home Phone</label></div><div class="namegroup"><input type="text" class="form-control" id="mobile" /><label>Mobile</label></div>';
    // var sixthRow = '<div class="namegroup"><input type="text" class="form-control" id="workphone" /><label>Work Phone</label></div><div class="namegroup"><input type="email" class="form-control" id="eamil" /><label>Email</label></div>';

    var firstRow = '<div class="col-xs-1"><label>Last</label></div><div class="col-xs-1"><label>First</label></div><div class="col-xs-1"><label>Middle</label></div><br clear="all"/>';
    var secondRow = '<div class="col-xs-1"><label>DOB</label></div><div class="col-xs-1"><label>Gender</label></div><br clear="all"/>';

    var fourthRow = '<div class="col-xs-2"><label>Address1</label></div><div class="col-xs-2"><label> Address2</label></div><br clear="all"/><div class="col-xs-1"><label>City</label></div><div class="col-xs-2"><label>State/County</label></div>';
    var fifthRow = '<div class="col-xs-1"><label>Country</label></div><br clear="all"/><div class="col-xs-3"><label>Home Phone</label></div><br clear="all"/><div class="col-xs-3"><label>Cell Phone</label></div><br clear="all"/>';




    formDiv = ele + '<div class="leftBlock">' + label + '</div><div class="rightBlock">' + firstRow + secondRow + fourthRow
        + fifthRow + deleteEle + '</div></div>';
    return formDiv;
}
function prepareInputElements(eleName, type, noOfFormElements) {
    var id = noOfFormElements;
    var groupClass = "";
    var ele = "";
    var className = "";
    var style = "";

    var label = '<label for="fi' + id + '" id="titleLabel">Label</label>';

    if (type.toLowerCase() == 'text') {

        className = 'class="form-control"';
        groupClass = "form-group";
        ele = '<input type="' + type + '" id="fi' + id + '" ' + className
            + '/>';
    } else if (type.toLowerCase() == 'label') {

    } else if (type.toLowerCase() == 'hline') {

        ele = '<hr id="fi' + id + '" ' + className + '/>';
        label = "";

    }
    else if (type.toLowerCase() == 'image') {
        className = 'class="img-preview";'

        ele = '<img src="" id="img" ' + className + '/>';
        label = '<label for="img' + id + '" id="titleLabel">Image</label>';

    }

    else if (type.toLowerCase() === 'checkbox') {
        groupClass = "checkbox";
        label = '<label id="titleLabel">Label</label>';

        var ele1 = '<div id="checkBoxOptions"><input type="' + type
            + '" id="fi' + id + '_1" ' + className + '/><label for="fi'
            + id + '_1" id="fl' + id + '_1" >First option</label></div>';
        /*
         * var ele2 = '<input type="' + type + '" id="fi' + id + '_1" ' +
         * className + '/><label for="fi' + id + '_1" id="fl' + id + '_1"
         * >Second option</label>'; var ele3 = '<input type="' + type + '"
         * id="fi' + id + '_2" ' + className + '/><label for="fi' + id + '_2"
         * id="fl' + id + '_2" >Third option</label></div>';
         */
        // ele = ele1 + ele2 + ele3;
        ele = ele1;
    } else if (type.toLowerCase() === 'radio') {
        groupClass = "radio";
        label = '<label id="titleLabel">Label</label>';
        var name = " name = 'optradio" + id + "'";
        var ele1 = '<div id="radioOptions"><input type="' + type + '" id="fi'
            + id + '_1" ' + className + name
            + '/><label class="radio-inline" for="fi' + id + '" id="fl'
            + id + '_1">Option 1</label>';
        var ele2 = '<input type="' + type + '" id="fi' + id + '_2" '
            + className + name + '/><label class="radio-inline" for="fi'
            + id + '" id="fl' + id + '_2"> Option 2</label>';
        var ele3 = '<input type="' + type + '" id="fi' + id + '_3" '
            + className + name + '/><label class="radio-inline" for="fi'
            + id + '" id="fl' + id + '_3"> Option 3</label></div>';
        ele = ele1 + ele2 + ele3;
    } else if (type.toLowerCase() == 'button') {
        label = "";
        groupClass = "btn-group";
        style = ' style="margin-bottom:15px;"';
        ele = '<button type="button" class="btn btn-success" id="fi' + id
            + '">Button Name</button>'
        var onclick = "fieldSettings(this,'button')";
        var deleteEle = '<a href="javascript:void(0);" class="trash" onclick="removeEle(this,\'main'
            + id
            + '\')"><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a>';
        var formDiv = '<div class="' + groupClass + ' maindiv"' + style
            + 'onclick="' + onclick + '" id="main' + id + '">' + '<div class="leftBlock">' + label + '</div><div class="rightBlock">' + ele
            + deleteEle + '</div></div>';
        return formDiv;
    } else {
        className = 'class="form-control"';
        groupClass = "form-group";
        ele = '<input type="' + type + '" id="fi' + id + '" ' + className
            + '/>';
    }
    var onclick = "fieldSettings(this,'" + type + "','','" + groupClass
        + "','comefrom')";
    var deleteEle = '<a href="javascript:void(0);" class="trash" onclick="removeEle(this,\'main'
        + id
        + '\')"><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a>';
    if (type.toLowerCase() == 'hline') {
        var formDiv = '<div class="' + groupClass + ' maindiv"' + style
            + ' onclick="' + onclick + '" id="main' + id + '">' + '<div class="">' + ele
            + deleteEle + '</div></div>';
    }
    else {
        var formDiv = '<div class="' + groupClass + ' maindiv"' + style
            + ' onclick="' + onclick + '" id="main' + id + '">' + '<div class="leftBlock">' + label + '</div><div class="rightBlock">' + ele
            + deleteEle + '</div></div>';
    }
    return formDiv;

};
function prepareTextArea(eleName, type, noOfFormElements) {
    var id = noOfFormElements;
    var groupClass = "form-group";
    var ele = "";
    var className = "form-control";
    var style = "";
    var rows = 5;
    var label = ' <label for="fi' + id + '" id="titleLabel">Text Area</label>';
    ele = '<textarea class="' + className + '" rows="' + rows + '" id="fi' + id
        + '"></textarea>';
    var onclick = "fieldSettings(this,'textarea','','" + groupClass
        + "','comefrom')";
    var deleteEle = '<a href="javascript:void(0);" class="trash" onclick="removeEle(this,\'main'
        + id
        + '\')"><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a>';
    var formDiv = '<div class="' + groupClass + ' maindiv"' + style
        + 'onclick="' + onclick + '" id="main' + id + '">' + '<div class="leftBlock">' + label + '</div><div class="rightBlock">' + ele
        + deleteEle + '</div></div>';
    return formDiv;
};
function prepareDropDowns(eleName, type, noOfFormElements) {
    var id = noOfFormElements;
    var groupClass = "form-group";
    var ele = "";
    var className = "form-control";
    var style = "";
    var rows = 5;
    var label = ' <label for="fi' + id + '" id="titleLabel">Selection</label>';
    var options = ' <option value="First Choice" id="option0"> First Choice</option><option value="Second Choice" id="option1"> Second Choice</option>';
    ele = '<select class="' + className + '" rows="' + rows + '" id="fi' + id
        + '" style="display: inline-block;">' + options + '</select>';
    var onclick = "fieldSettings(this,'dropdown','','" + groupClass
        + "','comefrom')";
    var deleteEle = '<a href="javascript:void(0);" class="trash" onclick="removeEle(this,\'main'
        + id
        + '\')"><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a>';
    var formDiv = '<div class="' + groupClass + ' maindiv"' + style
        + 'onclick="' + onclick + '" id="main' + id + '">' + '<div class="leftBlock">' + label + '</div><div class="rightBlock">' + ele
        + deleteEle + '</div></div>';
    return formDiv;
}
var currFid = "";
function fieldSettings(thisScope, type, empty, clsName, comeFrom, isSaved,
                       compId) {


    if (type.toLowerCase() == 'text') {
        $("#lblComponentType").text("Textbox");
    }
    else if (type.toLowerCase() == 'label') {
        $("#lblComponentType").text("Label");
    }
    else if (type.toLowerCase() == 'number') {
        $("#lblComponentType").text("Number");
    }
    else if (type.toLowerCase() == 'checkbox') {
        $("#lblComponentType").text("Checkbox");
    }
    else if (type.toLowerCase() == 'radio') {
        $("#lblComponentType").text("Radio");
    }
    else if (type.toLowerCase() == 'hline') {
        $("#lblComponentType").text("Horizontal line");
    }
    else if (type.toLowerCase() == 'dropdown') {
        $("#lblComponentType").text("Dropdown");
    }
    else if (type.toLowerCase() == 'textarea') {
        $("#lblComponentType").text("Textarea");
    }
    else if (type.toLowerCase() == 'serviceuser') {
        $("#lblComponentType").text("Serviceuser");
    }
    else if (type.toLowerCase() == 'staff') {
        $("#lblComponentType").text("Staff");
    }
    else if (type.toLowerCase() == 'image') {
        $("#lblComponentType").text("Image");
    }
    if (type.toLowerCase() == 'datepicker') {
        $("#lblComponentType").text("Datepicker");
    }
    else if (type.toLowerCase() == 'datetime') {
        $("#lblComponentType").text("Datetime");
    }



    // $(thisScope).find('.maindiv').addClass('bg-active');
    $(currFid).find('.trash').removeClass('trash-active');
    $(currFid).removeClass('bg-active');
    // currFid = $("#"+currFid.id)[0];
    thisScope = thisScope[0] && thisScope[0].className == 'clearfix' ? thisScope[1] : thisScope[0] || thisScope;
    console.log(typeof thisScope);
    var readonly = '';
    $('#flvalid').removeAttr('readonly');
    $('#checkboxLabelInput').removeAttr('readonly');
    if (isSaved && isSaved.toLowerCase() == 'savedfield') {
        readonly = 'readonly';

        if (type && type.toLowerCase() == 'checkbox') {
            $('#checkboxLabelInput').attr('readonly', true);
            $('#checkboxLabelInput').css('pointer-events', 'none');
        } else if (type && type.toLowerCase() == 'radio') {
            $('#radioLabelInput').attr('readonly', true);
        } else if (type && type.toLowerCase() == 'radio' || type
            && type.toLowerCase() == 'dropdown') {
            $('#labelField').attr('readonly', true);
        } else {
            //$('#flvalid').attr('readonly', true);
            //$('#flvalid').css('pointer-events', 'none');
        }
        console.log(ajaxReqObject);
    } else {
        $('#checkboxLabelInput').removeAttr('readonly');
        $('#checkboxLabelInput').css('pointer-events', 'auto');
        $('#flvalid').removeAttr('readonly');
        $('#flvalid').css('pointer-events', 'auto');

        $('#radioLabelInput').removeAttr('readonly', true);
        $('#labelField').removeAttr('readonly', true);
    }

    //

    if (compId) {
        ajaxReqObject.id = compId;
        ajaxReqObject.component = _.where(savedComponent, {
            id: ajaxReqObject.id
        })[0];
        ajaxReqObject.component.componentTypeId = _.findWhere(
            componetTypes, {
                type: type
            }).id;
    }

    //



    $("#flvalid").val('');
    $("#placeHolderName").val("");
    // $("#fieldRequired").val(false);
    $("#fieldInstructions").val("");
    $("#txtRemarks").val("");
    $("#fieldRemarks").prop("checked", false);
    $('#listPlaceHolderName').hide();
    $('#selectoptions').hide();
    $('#liNames').hide();
    $('#liAddress').hide();
    $('#listInstructions').hide();
    $('#listRadioButton').hide();
    $('#listDropdown').hide();
    $('#listRemarks').hide();
    $('#field').hide();
    $('#liLikert').hide();
    $('#liprevalues').hide();
    $('#piprevalue').val("");
    $('#dataType').hide();
    $('#maxChars').hide();
    $('#units').hide();
    $('#divfontweight').hide();
    $('#liLabeltext').hide();
    $('#divNameformat').hide();
    $('#divNamecolor').hide();
    $('#status').hide();
    $('#listCheckbox').hide();
    $('#specLabel').hide();
    $('#specLabel input').val("");
    $('#listRating').hide();
    $('#listMaxValue').hide();
    $('#listMinValue').hide();
    $('#listOptions').hide();// hide required field in field settings
    $('#listNoDuplicates').hide();
    $('#listEncrypted').hide();
    $('#lihline').hide();
    $('#listServiceUser').hide();
    $('#listStaff').hide();
    $('#listFormat').hide();
    $('#listImage').hide();
    // if(curMinitTemplate && (curMinitTemplate.label=="3" || curMinitTemplate.label=="4")){
    // 	$('#listMiniCols').show();
    // 	if(ajaxReqObject.component != undefined && (ajaxReqObject.component.label=='2' || ajaxReqObject.component.label=='3'|| ajaxReqObject.component.label=='4')){
    // 		$('#miniColType').val(ajaxReqObject.component.label);

    // 	}else
    // 		$('#miniColType').val('2');

    // }
    // else{
    // 	$('#listMiniCols').hide();
    // }

    $("#txtDataType").data("kendoComboBox").enable(true);

    currFid = thisScope;
    $('#lihline').hide();
    var label = "";
    var input = "";


    if ($("#" + thisScope.id).find('#units').length > 0) {
        if ($("#" + thisScope.id + ' #units').text().toLowerCase() == 'kg')
            $('#cmbUnits').data("kendoComboBox").select(1);
        else
            $('#cmbUnits').data("kendoComboBox").select(2);
    }
    if (type && type.toLowerCase() == 'text') {
        $('#listPlaceHolderName').hide();//$('#listPlaceHolderName').show();
        $('#field').show();
        $('#listInstructions').show();
        $('#listOptions').show();// hide required field in field settings
        $('#listNoDuplicates').show();
        // $('#listEncrypted').show();
        $('#liprevalues').show();
        $('#dataType').show();

        $('#maxChars').show();
        $('#units').show();
        $('#status').show();
        if (comeFrom && comeFrom.toLowerCase() == 'fields')
            $('#txtDataType').data("kendoComboBox").select(1);
        label = $('#' + thisScope.id).find("#titleLabel").text();
        input = thisScope.childNodes[1].childNodes[0];
        $("#fieldInstructions").val(
            $("#" + thisScope.id + " #instruction").text());
        var radiobtn = document.getElementById("fieldRequired");
        radiobtn.checked = input.required;
        $("#placeHolderName").val(input.placeholder);
        $("#piprevalue").val(input.value);

        if (ajaxReqObject.component && ajaxReqObject.component.dataTypeId == 1) {
            $('#listMinValue').show();
            $('#listMaxValue').show();
            $('#fieldMaxValue').val(ajaxReqObject.component.maximumValue || '');
            $('#fieldMinValue').val(ajaxReqObject.component.minimumValue || '');
        }
        $('#txtMaxChars').val(ajaxReqObject.component.length || '');

    } else if (type && type.toLowerCase() == 'label') {
        $('#field').show();
        $('#divfontweight').show();
        $('#liLabeltext').show();
        $('#divNameformat').show();
        $('#divNamecolor').show();
        // $('#flvalid').val($("#" + thisScope.id + " #titleLabel").text());
        //label = thisScope.childNodes[0];
        label = $('#' + thisScope.id).find("#titleLabel").text();
        //input = thisScope.childNodes[1].childNodes[0];
        // console.log(thisScope.childNodes[1]);

        $('#fieldLabelText').val($('#' + thisScope.id).find("#instruction").text());
    } else if (type && type.toLowerCase() == 'hline') {
        label = "";
        $('#lihline').show();
        $('#flvalid').val('Horizontal Line');
        $('#flvalid').attr('readonly', true);
    } else if (type && type.toLowerCase() == 'serviceuser') {
        label = "";
        $('#listServiceUser').show();
    } else if (type && type.toLowerCase() == 'staff') {
        label = "";
        $('#listStaff').show();
    } else if (type && type.toLowerCase() == 'image') {
        label = "";
        $('#listImage').show();
    }


    else if (type && type.toLowerCase() == 'textarea') {

        $('#listPlaceHolderName').hide();//$('#listPlaceHolderName').show();
        $('#field').hide();
        $('#listInstructions').show();
        $('#listOptions').show();// hide required field in field settings
        // $('#listNoDuplicates').show();
        // $('#listEncrypted').show();
        $('#liprevalues').show();
        $('#specLabel').show();
        $('#dataType').hide();
        // if(comeFrom && comeFrom.toLowerCase() == 'fields')
        $('#txtDataType').data("kendoComboBox").select(1);
        $("#txtDataType").data("kendoComboBox").readonly();
        $('#maxChars').show();
        // $('#units').show();
        label = $('#' + thisScope.id).find("#titleLabel").text();
        input = thisScope.childNodes[1].childNodes[0];
        $("#fieldInstructions").val(
            $("#" + thisScope.id + " #instruction").text());
        var radiobtn = document.getElementById("fieldRequired");
        radiobtn.checked = input.required;
        $("#placeHolderName").val(input.placeholder);
        $("#piprevalue").val(input.value);
        $('#specLabel #flvalid').val(
            $("#" + thisScope.id + " #titleLabel").text().replace('*', ''));
        if (ajaxReqObject.component && ajaxReqObject.component.dataTypeId == 1) {
            $('#listMinValue').show();
            $('#listMaxValue').show();
            $('#fieldMaxValue').val(ajaxReqObject.component.maximumValue || '');
            $('#fieldMinValue').val(ajaxReqObject.component.minimumValue || '');
        }
        $('#txtMaxChars').val(ajaxReqObject.component.length || '');
    } else if (type && type.toLowerCase() == 'radio') {
        $('#listPlaceHolderName').hide();
        $('#field').hide();
        $('#listInstructions').show();
        $('#listRadioButton').show();
        $('#status').show();
        $('#listOptions').show();
        $('#radioLabelInput').val(
            $("#" + thisScope.id + " #titleLabel").text().replace('*', ''));
        input = thisScope.childNodes[1].childNodes[0].childNodes[0];
        // $('#specLabel').show();

        /*
         * label = thisScope.childNodes[1]; input = thisScope.childNodes[0];
         */
        $("#listRemarks").show();
        var IsRemarks=  ajaxReqObject.component.isEncrypted;
        if(IsRemarks){
            var label = ajaxReqObject.component.label.split('\n');
            if (label && label.length > 0) {
                $("#txtRemarks").val(label[1]);
            }
            $("#divRemarks").show();
        }
        $("#fieldRemarks").prop("checked", IsRemarks);
        $("#fieldInstructions").val(
            $("#" + thisScope.id + " #instruction").text());
        var radiobtn = document.getElementById("fieldRequired");
        radiobtn.checked = input.required;
        var radioLbls = $('#' + thisScope.id + ' div label').length;
        // $("#placeHolderName").val( input.placeholder);
        $('#listRadioButton div').empty();
        for (var i = 1; i < radioLbls; i++) {
            var fieldInput = '<input type="text" onkeyup="updateLabels(this,\'label\',\'fl'
                + thisScope.id.replace('main', '')
                + '_'
                + i
                + '\')" id="radio_'
                + i
                + '" value="'
                + $('#' + thisScope.id + ' div label')[i].innerHTML
                + '" />';
            var addOption = '<a href="javascript:void(0);" onclick="addExtraFields1(this,\'radio\',\'radio\',\'\')"><img src="../../img/add.png" alt="Add" height="15" width="15"></a>';
            var removeOption = '<a href="javascript:void(0);" onclick="removeExtraFields1(this,\'radio\',\'radio\',\'\')"><img src="../../img/remove.png" alt="Remove" height="15" width="15"></a>';
            var optEle = '<div class="opt-ele">' + fieldInput + addOption + removeOption + '</div>';
            $('#listRadioButton #options').append(optEle);
        }
    } else if (type && type.toLowerCase() == 'button') {
        $("#listOptions").hide();
        $('#listPlaceHolderName').hide();
        $('#field').show();
        $('#listInstructions').show();
        $('#status').show();
        label = thisScope.childNodes[0];
        // input = thisScope.childNodes[1];
        $("#fieldInstructions").val(
            $("#" + thisScope.id + " #instruction").text());
        // $("#fieldRequired").val( input.required || "");
        // $("#placeHolderName").val( input.placeholder);
    } else if (type && type.toLowerCase() == 'checkbox') {
        $('#listPlaceHolderName').hide();
        $('#field').hide();
        $('#listInstructions').show();
        $('#listOptions').show();
        $('#status').show();
        $('#listCheckbox').show();
        $('#checkboxLabelInput').val(
            $("#" + thisScope.id + " #titleLabel").text().replace('*', ''));
        input = thisScope.childNodes[1].childNodes[0].childNodes[0];
        /*
         * label = thisScope.childNodes[1]; input = thisScope.childNodes[0];
         */

        $("#listRemarks").show();
        var IsRemarks=  ajaxReqObject.component.isEncrypted;
        if(IsRemarks){
            var label = ajaxReqObject.component.label.split('\n');
            if (label && label.length > 0) {
                $("#txtRemarks").val(label[1]);
            }
            $("#divRemarks").show();
        }
        $("#fieldRemarks").prop("checked", IsRemarks);
        $("#fieldInstructions").val(
            $("#" + thisScope.id + " #instruction").text());
        var checkBoxbtn = document.getElementById("fieldRequired");
        checkBoxbtn.checked = input.required;
        $("#placeHolderName").val(input.placeholder);
        var checkboxLbls = $('#' + thisScope.id + ' div label').length;
        $('#listCheckbox div').empty();
        for (var i = 1; i < checkboxLbls; i++) {
            var fieldInput = '<input type="text" onkeyup="updateLabels(this,\'label\',\'fl'
                + thisScope.id.replace('main', '')
                + '_'
                + i
                + '\')" id="checkbox_'
                + i
                + '" value="'
                + $('#' + thisScope.id + ' div label')[i].innerHTML
                + '" />';
            var addOption = '<a href="javascript:void(0);" onclick="addExtraFields1(this,\'checkbox\',\'checkbox\',\'\')"><img src="../../img/add.png" alt="Add" height="15" width="15"></a>';
            var removeOption = '<a href="javascript:void(0);" onclick="removeExtraFields1(this,\'checkbox\',\'checkbox\',\'\')"><img src="../../img/remove.png" alt="Remove" height="15" width="15"></a>';
            var optEle = '<div class="opt-ele">' + fieldInput + addOption + removeOption + '</div>';
            $('#listCheckbox #cOptions').append(optEle);
        }
    } else if (type && type.toLowerCase() == 'dropdown') {
        $('#listPlaceHolderName').hide();
        $('#listDropdown').show();
        $('#field').hide();
        $('#listInstructions').show();
        $('#listOptions').show();
        $('#status').show();
        label = $('#' + thisScope.id).find("#titleLabel").text();
        input = thisScope.childNodes[1].childNodes[0];
        $("#fieldInstructions").val(
            $("#" + thisScope.id + " #instruction").text());
        var checkBoxbtn = document.getElementById("fieldRequired");
        checkBoxbtn.checked = input.required;
        // $("#fieldRequired").val( input.required);
        /*
         * $('#opt1').val(input[0].text); $('#opt2').val(input[1].text);
         * $('#opt3').val(input[2].text);
         */
        $("#listRemarks").show();
        var IsRemarks=  ajaxReqObject.component.isEncrypted;
        if(IsRemarks){
            var label = ajaxReqObject.component.label.split('\n');
            if (label && label.length > 0) {
                $("#txtRemarks").val(label[1]);
            }
            $("#divRemarks").show();
        }
        $("#fieldRemarks").prop("checked", IsRemarks);

        $('#labelField').val(
            $('#' + thisScope.id + ' #titleLabel').text().replace('*', ''));

        $('#listDropdown div').empty();
        var options = $('#' + thisScope.id + ' option').length;
        for (var i = 0; i < options; i++) {
            var fieldInput = '<input type="text" onkeyup="updateLabels(this,\'label\',\'option'
                + i
                + '\')" id="option'
                + i
                + '" value="'
                + $('#' + thisScope.id + ' option')[i].innerHTML + '" />';
            $('#listDropdown div').append(fieldInput);
        }
    } else if (type && type.toLowerCase() == 'email') {
        $('#listPlaceHolderName').hide();//$('#listPlaceHolderName').show();
        $('#field').show();
        $('#listInstructions').show();
        $('#listOptions').show();
        $('#liprevalues').show();
        $('#dataType').hide();
        $('#maxChars').hide();
        $('#status').hide();
        label = $('#' + thisScope.id).find("#titleLabel").text();
        input = thisScope.childNodes[1].childNodes[0];
        $("#fieldInstructions").val(
            $("#" + thisScope.id + " #instruction").text());
        var radiobtn = document.getElementById("fieldRequired");
        radiobtn.checked = input.required;
        $("#placeHolderName").val(input.placeholder);
        $("#piprevalue").val(input.value);
    } else if (type && type.toLowerCase() == 'number' || type
        && type.toLowerCase() == 'price') {
        $('#listPlaceHolderName').hide();//$('#listPlaceHolderName').show();
        $('#field').show();
        $('#listInstructions').show();
        $('#listOptions').show();// hide required field in field settings
        // $('#listNoDuplicates').show();
        // $('#listEncrypted').show();
        $('#liprevalues').show();
        // $('#dataType').show();
        $('#maxChars').hide();
        $('#units').show();
        $('#status').show();
        $('#listMaxValue').show();
        $('#listMinValue').show();
        label = $('#' + thisScope.id).find("#titleLabel").text();
        input = thisScope.childNodes[1].childNodes[0];
        $("#fieldInstructions").val(
            $("#" + thisScope.id + " #instruction").text());
        var radiobtn = document.getElementById("fieldRequired");
        radiobtn.checked = input.required;
        $("#placeHolderName").val(input.placeholder);
        $('#fieldMinValue').val(input.min);
        $('#fieldMaxValue').val(input.max);
        $("#piprevalue").val(input.value);
    } else if (type && type.toLowerCase() == 'phone') {
        $('#listPlaceHolderName').hide();//$('#listPlaceHolderName').show();
        $('#field').show();
        $('#liprevalues').show();
        $('#listInstructions').show();
        $('#listOptions').show();
        $('#listNoDuplicates').show();
        $('#listMaxValue').show();
        $('#listMinValue').show();
        // $('#listEncrypted').show();
        $('#maxChars').show();
        $('#dataType').hide();
        label = $('#' + thisScope.id).find("#titleLabel").text();
        input = thisScope.childNodes[1].childNodes[0];
        $("#fieldInstructions").val(
            $("#" + thisScope.id + " #instruction").text());
        var radiobtn = document.getElementById("fieldRequired");
        radiobtn.checked = input.required;
        $("#placeHolderName").val(input.placeholder);
        $('#fieldMinValue').val(input.min);
        $('#fieldMaxValue').val(input.max);
        $("#piprevalue").val(input.value);

    } else if (type && type.toLowerCase() == 'website') {
        $('#listPlaceHolderName').hide();//$('#listPlaceHolderName').show();
        $('#field').show();
        $('#listInstructions').show();
        $('#listOptions').show();
        $('#liprevalues').show();
        $('#dataType').hide();
        ajaxReqObject.component.dataTypeId = 2;
        label = $('#' + thisScope.id).find("#titleLabel").text();
        input = thisScope.childNodes[1].childNodes[0];
        $("#fieldInstructions").val(
            $("#" + thisScope.id + " #instruction").text());
        var radiobtn = document.getElementById("fieldRequired");
        radiobtn.checked = input.required;
        // $("#fieldRequired").val(input.required);
        $("#placeHolderName").val(input.placeholder);
        $("#piprevalue").val(input.value);
    } else if (type && type.toLowerCase() == 'datetime') {
        $('#listPlaceHolderName').hide();
        $('#field').show();
        $('#listInstructions').show();
        $('#listOptions').show();
        $('#dataType').hide();
        label = $('#' + thisScope.id).find("#titleLabel").text();
        input = thisScope.childNodes[1].childNodes[0];
        $("#fieldInstructions").val(
            $("#" + thisScope.id + " #instruction").text());
        var radiobtn = document.getElementById("fieldRequired");
        radiobtn.checked = input.required;
        // $("#fieldRequired").val(input.required);
        // $("#placeHolderName").val(input.placeholder);
    } else if (type && type.toLowerCase() == 'datepicker') {
        $('#listPlaceHolderName').hide();
        $('#field').show();
        $('#listInstructions').show();
        $('#listOptions').show();
        $('#dataType').hide();
        label = $('#' + thisScope.id).find("#titleLabel").text();
        input = thisScope.childNodes[1].childNodes[0];
        $("#fieldInstructions").val(
            $("#" + thisScope.id + " #instruction").text());
        var radiobtn = document.getElementById("fieldRequired");
        radiobtn.checked = input.required;
        // $("#fieldRequired").val(input.required);
        // $("#placeHolderName").val(input.placeholder);
    } else if (type && type.toLowerCase() == 'name') {
        $('#liNames').show();
        $('#listInstructions').show();
        $('#listOptions').show();
        $('#dataType').hide();
        $('#liprevalues').hide();
        $('#maxChars').show();
        $('#units').hide();
        $('#listNoDuplicates').show();
        // $('#listEncrypted').show();
        $('#vLabel').val($(thisScope).find('.title').text().replace('*', ''));
        $("#fieldInstructions").val(
            $("#" + thisScope.id + " #instruction").text());
        var isRequired = $("#" + thisScope.id).find('#mandatory').length ? true
            : false;
        var checkBoxbtn = document.getElementById("fieldRequired");
        checkBoxbtn.checked = isRequired;
        // $('#vFristName').val($(thisScope).find('#cffid').text());
        // $('#vLastName').val($(thisScope).find('.cflid').text());
    } else if (type && type.toLowerCase() == 'address') {
        $('#liAddress').show();
        $('#listInstructions').show();
        $('#listOptions').show();
        $('#dataType').hide();
        $('#vaddLabel')
            .val($(thisScope).find('.title').text().replace('*', ''));
        $("#fieldInstructions").val(
            $("#" + thisScope.id + " #instruction").text());
        // $('#vFristName').val($(thisScope).find('#cffid').text());
        // $('#vLastName').val($(thisScope).find('.cflid').text());
    } else if (type && type.toLowerCase() == 'likert') {
        $('#liLikert').show();
        $('#listInstructions').show();
        $('#listOptions').show();
        $('#dataType').hide();
        $('#likertLabel').val(
            $(thisScope).find('.title').text().replace('*', ''));
        $("#fieldInstructions").val(
            $("#" + thisScope.id + " #instruction").text());
        $('#pstatement1').val($(currFid).find('table #statement1').text());
        $('#pstatement2').val($(currFid).find('table #statement2').text());
        $('#pstatement3').val($(currFid).find('table #statement3').text());
        $('#pcol1').val($(currFid).find('table #tablecol1').text());
        $('#pcol2').val($(currFid).find('table #tablecol2').text());
        $('#pcol3').val($(currFid).find('table #tablecol3').text());
        $('#pcol4').val($(currFid).find('table #tablecol4').text());
        // $('#vFristName').val($(thisScope).find('#cffid').text());
        // $('#vLastName').val($(thisScope).find('.cflid').text());
    } else if (type && type.toLowerCase() == 'rating') {
        $('#listMaxValue').hide();
        $('#listMinValue').hide();
        $('#listRating').show();
        $('#dataType').hide();
        $('#listInstructions').show();
        $("#fieldInstructions").val(
            $("#" + thisScope.id + " #instruction").text());
        $('#rating').val(
            $('#' + thisScope.id).find("#titleLabel").text());

    }
    if (!(type && type.toLowerCase() == 'hline')) {
        var labelVal = $("#" + currFid.id + ' #titleLabel').text();
        labelVal = labelVal.replace('*', '');
        $("#flvalid").val(labelVal);
    }
    /*
     * if (type && type.toLowerCase() == 'rating') return; else
     */
    $('.nav-tabs a[href="#fieldSettings"]').tab('show');
    // $("#addFields").removeClass('active');
    // $("#fieldSettings").addClass('active');
    // $("#addFields").removeClass('active');
    // $("#fieldSettings").addClass('active');
    if ($('#dynamic-from').children().length > 0) {
        $('#btnSave').removeAttr('disabled');
        $('#btnCancel').removeAttr('disabled');
    }

    else {
        $('#btnSave').attr('disabled', 'disabled');
        $('#btnCancel').attr('disabled', 'disabled');
    }
    if (comeFrom
        && (comeFrom.toLowerCase() == 'fields' || comeFrom.toLowerCase() == 'fancyfields')) {
        ajaxReqObject.id = currFid.id;
        ajaxReqObject.component = jQuery.extend({}, component);
        ajaxReqObject.component.componentTypeId = _.findWhere(componetTypes, {
            type: type
        }).id;
        ajaxReqObject.component.displayOrder = $('#dynamic-from')[0].children.length;
        ajaxReqObject.component.createdBy = Number(sessionStorage.userId);
    } else {
        var formLength = $('#dynamic-from')[0].children.length;
        var position = 0;
        for (var i = 0; i < formLength; i++) {
            if ($('#dynamic-from')[0].children[i].id == currFid.id) {
                position = i;
                break;
            }
        }

        ajaxReqObject.component.displayOrder = position + 1;;




        /*
         * ajaxReqObject = finalObject[position];
         * ajaxReqObject.component.componentTypeId =
         * _.findWhere(componetTypes,{type:type}).id;
         */
    }
    console.log(ajaxReqObject);
    console.log(finalObject);
    console.log(savedComponent);
    var currComponent = _.where(savedComponent, {
        id: ajaxReqObject.id
    })[0];
    if (currComponent && currComponent.componentTypeId == 20) {

        var notes = currComponent.notes.split('~');
        if (notes && notes.length > 0) {
            // $('#cmbNamefontweight').data("kendoComboBox").select(notes[0]);
            // $('#cmbNameformat').data("kendoComboBox").select(notes[1]);
            // $('#cmbNamecolor').data("kendoComboBox").select(notes[2]);

            $('#cmbNamefontweight').data("kendoComboBox").text(notes[0]);
            $('#cmbNameformat').data("kendoComboBox").text(notes[1]);
            $('#cmbNamecolor').data("kendoComboBox").text(notes[2]);
        }
    }
    if (type && type.toLowerCase() == 'phone')
        ajaxReqObject.component.dataTypeId = 1;
    if (ajaxReqObject && ajaxReqObject.component
        && ajaxReqObject.component.componentTypeId == 21) {
        ajaxReqObject.component.name = 'hline';
        ajaxReqObject.component.label = '';
        ajaxReqObject.component.format = '';
        ajaxReqObject.component.placeholderText = '';
        ajaxReqObject.component.predefinedValue = '';

        ajaxReqObject.component.hint = "";
        ajaxReqObject.component.maximumValue = "";
        ajaxReqObject.component.minimumValue = "";
        ajaxReqObject.component.isRequired = 0;
        ajaxReqObject.component.isEncrypted = 0;
        ajaxReqObject.component.isActive = 1;
        ajaxReqObject.component.noDuplicates = 0;
        ajaxReqObject.component.dataTypeId = 1;
        ajaxReqObject.component.unitId = null;
        ajaxReqObject.component.length = 10;
        ajaxReqObject.component.miniTemplateId = parseInt($("#cmbMiniTemplates")
            .data("kendoComboBox").value());

        // ajaxReqObject.component.displayOrder = finalObject.length + 1;
    } else if (ajaxReqObject && ajaxReqObject.component
        && ajaxReqObject.component.componentTypeId == 23) {
        ajaxReqObject.component.name = 'serviceuser';
        ajaxReqObject.component.label = 'serviceuser';
    } else if (ajaxReqObject && ajaxReqObject.component
        && ajaxReqObject.component.componentTypeId == 24) {
        ajaxReqObject.component.name = 'staff';
        ajaxReqObject.component.label = 'staff';
    }
    $('.nav-tabs a[href="#fieldSettings"]').tab('show');
    $("#ptframe").css({
        height: (500) + 'px !important'
    });
    console.log($('#miniColType').val());
    if (curMinitTemplate && curMinitTemplate.label == "3") {
        ajaxReqObject.component.label = $('#miniColType').val();
    }
    $(currFid).find('.trash').addClass('trash-active');
    $(currFid).addClass('bg-active');






};
function setValue(scope, type, index, className) {

    if(type.toLowerCase() == 'remarks'){
        var checked = $("#fieldRemarks").is(':checked');
        if(checked) {
            $("#divRemarks").show();
        }
        else{
            $("#divRemarks").hide();
        }
    }
    var label = "";
    var input = "";
    // if(currFid.childNodes[0].nextElementSibling &&
    // currFid.childNodes[0].nextElementSibling.nodeName.toLowerCase() ==
    // 'label'){
    // label = currFid.childNodes[1];
    // input = currFid.childNodes[2];
    // }
    // else{
    label = $(currFid).find('label').length > 0 ? $(currFid).find('label')[0]
        : currFid.childNodes[0];
    input = $(currFid).find('input').length > 0 ? $(currFid).find('input')[0]
        : $(currFid).find('textarea').length > 0 ? $(currFid).find(
            'textarea')[0] : currFid.childNodes[1];
    // }
    if (type.toLowerCase() == "label") {
        if (!($('#' + currFid.id).find('button').length > 0))
            label = $('#' + currFid.id).find('#titleLabel')[0];
        else
            label = $('#' + currFid.id).find('button')[0];
        if ($("#" + currFid.id + ' #' + label.id).find('#mandatory').length) {
            $("#" + currFid.id + ' #' + label.id).text(scope.value);
            $("#" + currFid.id + ' #' + label.id).append(
                '<span style="color:red" id="mandatory">*</span>');

        } else {
            $("#" + currFid.id + ' #' + label.id).text(scope.value);
        }
    } else if (type.toLowerCase() == "vfirstname") {
        $(currFid).find('.first').attr("placeholder", scope.value);
    } else if (type.toLowerCase() == "vlastname") {
        $(currFid).find('.last').attr("placeholder", scope.value);
    } else if (type.toLowerCase() == "vmiddlename") {
        $(currFid).find('.middle').attr("placeholder", scope.value);
    } else if (type.toLowerCase() == "address") {
        $(currFid).find('.' + className).attr("placeholder", scope.value);
    } else if (type.toLowerCase() == "prevalues") {
        $(currFid).find('.' + className).attr("placeholder", scope.value);
    }
    else if (type.toLowerCase() == "rating") {
        $("#" + currFid.id + ' #' + label.id).text(scope.value);
    }
    // $("#"+label.id).text(scope.value);
    else if (type.toLowerCase() == "input")
        $("#" + currFid.id + ' #' + input.id).val(scope.value);
    else if (type.toLowerCase() == 'required') {
        var required = scope.checked ? $("#" + input.id).attr("required",
            "required") : $("#" + input.id).removeAttr("required");
        if (scope.checked) {
            if (label.id.length > 0)
                $("#" + currFid.id + ' #' + label.id).append(
                    '<span style="color:red" id="mandatory">*</span>');
            else
                $("#" + currFid.id + ' #titleLabel').append(
                    '<span style="color:red" id="mandatory">*</span>');
        } else
            $("#" + currFid.id + ' #mandatory').remove();
    } else if (type.toLowerCase() == 'placeholder') {
        $("#" + currFid.id + ' #' + input.id).attr("placeholder", scope.value);

    } else if (type.toLowerCase() == 'instructions') {
        if ($("#" + currFid.id + " #instruction").length == 0)
            $("#" + currFid.id + ' .rightBlock').append(
                '  <span class="folder-name" id="instruction">'
                + scope.value + '</span>');
        else
            $("#" + currFid.id + " #instruction").html(scope.value);

    } else if (type.toLowerCase() == 'dropdown') {
        currFid.childNodes[2][index].text = scope.value;

    } else if (type.toLowerCase() == 'likert') {
        $(currFid).find('table #' + className).text(scope.value);

    } else if (type.toLowerCase() == 'addlikertrow') {
        var noHeadings = $(currFid).find('table thead td').length;
        var rows = $(currFid).find('table tbody tr').length;
        rows = rows + 1;

        var tdata = "";
        for (var i = 0; i < noHeadings; i++) {
            tdata += '<td><input id="Field_' + i + '" name="Field' + rows
                + '" type="radio" tabindex="1" value="' + i + '" ></td>';
        }

        var row = '<tr class="statement"> <th><label for="Field1" id="statement'
            + rows + '">Statement</label></th>' + tdata + '</tr>';

        $("#" + currFid.id).find('table tbody').append(row);
        var rowHolder = '<input type="text" id="pstatement' + rows
            + '" onkeyup="setValue(this,\'likert\',\'\',\'statement' + rows
            + '\')" value="Statement" style="margin-left:32%"/>';
        $('#likertrows').append(rowHolder);
    } else if (type.toLowerCase() == 'addlikertcol') {
        var noHeadings = $(currFid).find('table thead td').length;
        noHeadings = noHeadings + 1;
        var rows = $(currFid).find('table tbody tr').length;

        var col = '<td id="tablecol' + noHeadings + '">Column</td>';
        for (var i = 0; i < rows; i++) {
            var j = i + 1;
            var tdata = '<td><input id="Field_' + i + '" name="Field' + j
                + '" type="radio" tabindex="1" value="' + i + '" ></td>';
            $($("#" + currFid.id).find('table tbody tr')[i]).append(tdata);
        }

        $("#" + currFid.id).find('table thead tr').append(col);
        var colHolder = '<input type="text" id="pcol' + noHeadings
            + '" onkeyup="setValue(this,\'likert\',\'\',\'tablecol'
            + noHeadings + '\')" value="Column" style="margin-left:32%"/>';
        $('#likertcols').append(colHolder).val('Column');
    }
    else if (type.toLowerCase() == 'txtremarks') {
        if ($("#" + currFid.id + " #remarks").length == 0)
            $("#" + currFid.id + ' .rightBlock').append(
                '  <span class="folder-name" id="remarks">'
                + scope.value + '</span>');
        else
            $("#" + currFid.id + " #remarks").html(scope.value);

    }

    currFid = $("#" + currFid.id)[0];
    setComponentObject(scope, currFid, index, className);
}
function removeEle(thisScope, id, componentid) {
    /*
     * if(id && id.length>0) $(ele).parent().parent().remove();
     * $(ele).parent().remove();
     */
    tooltipEleFound = true;
    for (i = 0; tooltipEleFound; i++) {
        // thisScope = $(thisScope).parent();
        if ($(thisScope).hasClass("maindiv")) {
            tooltipEleFound = false;
            console.log($(thisScope).parent());
            break;
        } else {
            if ($(thisScope).parent().prop("tagName").toLowerCase() == 'body') {
                tooltipEleFound = false;
                break;
            } else
                thisScope = $(thisScope).parent();
        }

    }

    for (var i = 0; i < finalObject.length; i++) {
        if (id && finalObject[i] && id == finalObject[i].id) {
            finalObject.splice(i, 1);
            break;
        }
    }
    if (componentid != undefined && componentid != '') {
        onClickDelete(componentid);
    }
    $(thisScope).remove();
    event.preventDefault();
    event.stopPropagation();
    if ($('#dynamic-from').children().length > 0) {
        $('#btnSave').removeAttr('disabled');
    }

    else {
        $('#btnSave').attr('disabled', 'disabled');
    }
    $('.nav-tabs a[href="#addFields"]').tab('show');

}
function removeExtraFields1(scope, eleName, type, pos) {
    var prevEleLen = $(scope.parentElement).index();
    $(scope.parentElement).remove();
    if (eleName.toLowerCase() == 'radio') {
        $("#" + currFid.id + ' #radioOptions input').eq(prevEleLen).remove();
        $("#" + currFid.id + ' #radioOptions label').eq(prevEleLen).remove();
    }
    else if (eleName.toLowerCase() == 'checkbox') {
        $("#" + currFid.id + ' #checkBoxOptions input').eq(prevEleLen).remove();
        $("#" + currFid.id + ' #checkBoxOptions label').eq(prevEleLen).remove();
    }
}
function addExtraFields1(scope, eleName, type, pos) {
    var IsValid = true;
    console.log($('#' + currFid.id));
    var prevEleLen = $(scope.parentElement).index();
    console.log($(scope.parentElement).index());
    if (eleName.toLowerCase() == 'radio') {


        var $radiolst = $("#options input[type=text]");

        if ($radiolst !== null && $radiolst.length > 0) {
            $.each($radiolst, function (i, e) {
                if ($(e).val().length === 0) {
                    IsValid = false;
                    return false;
                }
            })
        }

        if (IsValid) {

            var id = currFid.id.replace('main', '');
            var className = '';
            var groupClass = "radio";
            var len = $("#" + currFid.id + ' #radioOptions input').length + 1;
            var label = '';
            var name = " name = 'optradio" + id + "'";
            var newEle = '<input type="' + type + '" id="fi' + id + '_' + len
                + '" ' + className + name
                + '/><label class="radio-inline" for="fi' + id + '_' + len
                + '" id="fl' + id + '_' + len + '">Option ' + len + ' </label>';
            //$("#" + currFid.id + ' #radioOptions label').eq(prevEleLen).append(newEle);
            //$(newEle).insertAfter($("#" + currFid.id + ' #radioOptions label').eq(prevEleLen));
            $("#" + currFid.id + '  #radioOptions').append(newEle);
            currFid = $("#" + currFid.id)[0];
            var fieldInput = '<input type="text" onkeyup="updateLabels(this,\'radio\',\'fl'
                + id
                + '_'
                + len
                + '\')" id="radio_'
                + (len)
                + '" value="Option ' + len + '" />';
            //+ $('#' + currFid.id + ' label')[prevEleLen+2].innerHTML + '" />';
            var addOption = '<a href="javascript:void(0);" onclick="addExtraFields1(this,\'radio\',\'radio\',\'\')"><img src="../../img/add.png" alt="Add" height="15" width="15"></a>';
            var removeOption = '<a href="javascript:void(0);" onclick="removeExtraFields1(this,\'radio\',\'radio\',\'\')"><img src="../../img/remove.png" alt="Remove" height="15" width="15"></a>';
            var optEle = '<div class="opt-ele">' + fieldInput + addOption + removeOption + '</div>';

            $("#options").append(optEle);
            //$(optEle).insertAfter(scope.parentElement);
            // $('#radioLabelInput').val($('#'+currFid.id + ' #titleLabel'));
        }
        else {
            alert("Enter values of all options.");
        }


    }
    if (eleName.toLowerCase() == 'checkbox') {

        var $checkboxlst = $("#cOptions input[type=text]");

        if ($checkboxlst !== null && $checkboxlst.length > 0) {
            $.each($checkboxlst, function (i, e) {
                if ($(e).val().length === 0) {
                    IsValid = false;
                    return false;
                }
            })
        }

        if (IsValid) {
            var id = currFid.id.replace('main', '');
            var className = '';
            var groupClass = "checkbox";
            var len = $("#" + currFid.id + ' #checkBoxOptions input').length + 1;
            var label = '';
            var name = " name = 'optcheckbox" + id + "'";
            var newEle = '<input type="' + type + '" id="fi' + id + '_' + len
                + '" ' + className + name
                + '/><label class="checkbox-inline" for="fi' + id + '_' + len
                + '" id="fl' + id + '_' + len + '">Option ' + len + ' </label>';
            $("#" + currFid.id + '  #checkBoxOptions').append(newEle);
            //$(newEle).insertAfter($("#" + currFid.id + ' #checkBoxOptions label').eq(prevEleLen));
            currFid = $("#" + currFid.id)[0];
            var lbltxt = $('#' + currFid.id + ' label#' + 'fl' + id + '_' + len).text();
            var fieldInput = '<input type="text" onkeyup="updateLabels(this,\'checkbox\',\'fl'
                + id
                + '_'
                + len
                + '\')" id="checkbox'
                + (len)
                + '" value="Option ' + len + '" />';
            //+ $('#' + currFid.id + ' label')[prevEleLen+2].innerHTML + '" />';
            var addOption = '<a href="javascript:void(0);" onclick="addExtraFields1(this,\'checkbox\',\'checkbox\',\'\')"><img src="../../img/add.png" alt="Add" height="15" width="15"></a>';
            var removeOption = '<a href="javascript:void(0);" onclick="removeExtraFields1(this,\'checkbox\',\'checkbox\',\'\')"><img src="../../img/remove.png" alt="Remove" height="15" width="15"></a>';
            var optEle = '<div class="opt-ele">' + fieldInput + addOption + removeOption + '</div>';
            $("#cOptions").append(optEle);
            //$(optEle).insertAfter(scope.parentElement);
            //$(scope.parentElement).append(optEle);
            //$('#listCheckbox div').append(fieldInput);
            // $('#checkboxLabelInput').val($('#'+currFid.id + ' #titleLabel'));
        }
        else {
            alert("Enter values of all options.");
        }


    }
    if (eleName.toLowerCase() == 'addoptions') {
        var id = currFid.id.replace('main', '');
        var className = '';
        var groupClass = "radio";
        var len = $("#" + currFid.id + ' select option').length;
        var label = '';
        var name = " name = 'optradio" + id + "'";
        var newEle = '<option value="option' + len + '"id="option' + (len)
            + '" >Option ' + len + ' </option>';
        $("#" + currFid.id + ' select').append(newEle);
        currFid = $("#" + currFid.id)[0];
        var fieldInput = '<input type="text" onkeyup="updateLabels(this,\'label\',\'option'

            + len
            + '\')" id="option'
            + (len)
            + '" value="'
            + $('#' + currFid.id + ' select option')[len].innerHTML
            + '" />';
        $('#listDropdown div').append(fieldInput);
        // $('#checkboxLabelInput').val($('#'+currFid.id + ' #titleLabel'));
    }

}
function addExtraFields(scope, eleName, type, pos) {
    var IsValid = true;
    console.log($('#' + currFid.id));
    if (eleName.toLowerCase() == 'radio') {
        var id = currFid.id.replace('main', '');
        var className = '';
        var groupClass = "radio";
        var len = $("#" + currFid.id + ' #radioOptions input').length + 1;
        var label = '';
        var name = " name = 'optradio" + id + "'";
        var newEle = '<input type="' + type + '" id="fi' + id + '_' + len
            + '" ' + className + name
            + '/><label class="radio-inline" for="fi' + id + '_' + len
            + '" id="fl' + id + '_' + len + '">Option ' + len + ' </label>';
        $("#" + currFid.id + ' #radioOptions').append(newEle);
        currFid = $("#" + currFid.id)[0];
        var fieldInput = '<input type="text" onkeyup="updateLabels(this,\'radio\',\'fl'
            + id
            + '_'
            + len
            + '\')" id="radio_'
            + (len)
            + '" value="'
            + $('#' + currFid.id + ' label')[len].innerHTML + '" />';
        $('#listRadioButton div').append(fieldInput);
        // $('#radioLabelInput').val($('#'+currFid.id + ' #titleLabel'));

    }
    if (eleName.toLowerCase() == 'checkbox') {
        var id = currFid.id.replace('main', '');
        var className = '';
        var groupClass = "checkbox";
        var len = $("#" + currFid.id + ' #checkBoxOptions input').length + 1;
        var label = '';
        var name = " name = 'optcheckbox" + id + "'";
        var newEle = '<input type="' + type + '" id="fi' + id + '_' + len
            + '" ' + className + name
            + '/><label class="checkbox-inline" for="fi' + id + '_' + len
            + '" id="fl' + id + '_' + len + '">Option ' + len + ' </label>';
        $("#" + currFid.id + '  #checkBoxOptions').append(newEle);
        currFid = $("#" + currFid.id)[0];
        var fieldInput = '<input type="text" onkeyup="updateLabels(this,\'checkbox\',\'fl'
            + id
            + '_'
            + len
            + '\')" id="checkbox'
            + (len)
            + '" value="'
            + $('#' + currFid.id + ' label')[len].innerHTML + '" />';
        $('#listCheckbox div').append(fieldInput);
        // $('#checkboxLabelInput').val($('#'+currFid.id + ' #titleLabel'));
    }
    if (eleName.toLowerCase() == 'addoptions') {


        var $dropdownoptions = $('#listDropdown input[id^="option"]');

        if ($dropdownoptions !== null && $dropdownoptions.length > 0) {

            $.each($dropdownoptions, function (i, e) {

                if ($(e).val().length === 0 && i > 0) {
                    IsValid = false;
                    return false;
                }
            })

        }

        if (IsValid) {
            var id = currFid.id.replace('main', '');
            var className = '';
            var groupClass = "radio";
            var len = $("#" + currFid.id + ' select option').length;
            var label = '';
            var name = " name = 'optradio" + id + "'";
            var newEle = '<option value="option' + len + '"id="option' + (len)
                + '" >Option ' + len + ' </option>';
            $("#" + currFid.id + ' select').append(newEle);
            currFid = $("#" + currFid.id)[0];
            var fieldInput = '<input type="text" onkeyup="updateLabels(this,\'label\',\'option'

                + len
                + '\')" id="option'
                + (len)
                + '" value="'
                + $('#' + currFid.id + ' select option')[len].innerHTML
                + '" />';
            $('#listDropdown div').append(fieldInput);
            // $('#checkboxLabelInput').val($('#'+currFid.id + ' #titleLabel'));
        }
        else {
            alert("Enter values of all options.");
        }

    }

}
function updateLabels(scope, type, uid) {
    $("#" + currFid.id + ' #' + uid).text(scope.value);
    setComponentObject(scope, currFid, '', '');
}
function setComponentObject(ele, dom, index, className, byElement) {

    // console.log(ele);
    // console.log($('#'+dom));
    // console.log(component);
    var labelName;
    if (!($('#' + dom.id).find('button').length > 0))
        labelName = $('#' + dom.id).find('#titleLabel').text().replace('*',
            '');
    else
        labelName = $('#' + dom.id).find('button')[0].innerHTML;
    // var reqObj = jQuery.extend({}, component);

    var labelText = $('#fieldLabelText').val();
    var delimiter = '\n';
    if (ajaxReqObject && ajaxReqObject.component && ajaxReqObject.component.componentTypeId == 20) {
        var checked = $("#fieldRemarks").is(':checked');
        if(checked)
            var remarks = $("#txtRemarks").val();
        labelText += delimiter+remarks;

        ajaxReqObject.component.label = labelText;
        ajaxReqObject.component.placeholderText = $("#cmbNamefontweight").data(
            "kendoComboBox").value();
        ajaxReqObject.component.format = $("#cmbNameformat").data(
            "kendoComboBox").value();
        ajaxReqObject.component.predefinedValue = $("#cmbNamecolor").data(
            "kendoComboBox").value();
    } else {
        var remarksName= labelName;
        var checked = $("#fieldRemarks").is(':checked');
        if(checked)
            var remarks = $("#txtRemarks").val();
        remarksName += delimiter+remarks;
        ajaxReqObject.component.label = remarksName;
        ajaxReqObject.component.placeholderText = $('#placeHolderName').val()
            || "";
        ajaxReqObject.component.predefinedValue = $('#piprevalue').val() || "";
    }

    ajaxReqObject.component.name = labelName;
    /*if ($('#fieldInstructions').val()
            && $('#fieldInstructions').val().length > 0)*/
    ajaxReqObject.component.hint = $('#fieldInstructions').val() || "";
    if ($('#fieldInstructions').val() && $('#fieldMaxValue').val().length > 0)
        ajaxReqObject.component.maximumValue = $('#fieldMaxValue').val() || "";
    if ($('#fieldInstructions').val() && $('#fieldMinValue').val().length > 0)
        ajaxReqObject.component.minimumValue = $('#fieldMinValue').val() || "";
    ajaxReqObject.component.isRequired = $('#fieldRequired').is(':checked') ? 1
        : 0;
    ajaxReqObject.component.isEncrypted = $('#fieldRemarks').is(':checked') ? 1
        : 0;
    ajaxReqObject.component.isActive = 1;
    ajaxReqObject.component.noDuplicates = $('#fieldNoDuplicates').is(
        ':checked') ? 1 : 0;
    // ajaxReqObject.component.dataTypeId =
    // parseInt($("#txtDT").data("kendoComboBox").value());
    ajaxReqObject.component.dataTypeId = parseInt($("#txtDataType").val());
    if ($("#cmbUnits").data("kendoComboBox").value()
        && $("#cmbUnits").data("kendoComboBox").value().length > 0)
        ajaxReqObject.component.unitId = parseInt($("#cmbUnits").data(
            "kendoComboBox").value());
    ajaxReqObject.component.length = parseInt($("#txtMaxChars").val());
    ajaxReqObject.component.miniTemplateId = parseInt($("#cmbMiniTemplates")
        .data("kendoComboBox").value());

    // ajaxReqObject.component.displayOrder = finalObject.length + 1;
    //console.log(ajaxReqObject);
    /*
     * if(byElement) {
     */
    /*

     * ajaxReqObject.component.label = labelName; ajaxReqObject.component.name =
     * labelName; ajaxReqObject.component.predefinedValue =
     * $('#piprevalue').val() || ""; ajaxReqObject.component.placeholderText =
     * $('#placeHolderName').val() || ""; ajaxReqObject.component.hint =
     * $('#fieldInstructions').val() || ""; ajaxReqObject.component.maximumValue =
     * $('#fieldMaxValue').val() || ""; ajaxReqObject.component.minimumValue =
     * $('#fieldMinValue').val() || ""; ajaxReqObject.component.isRequired =
     * $('#fieldRequired').is(':checked')? 1 : 0;
     * ajaxReqObject.component.isEncrypted =
     * $('#fieldEncrypted').is(':checked')? 1 : 0;
     * ajaxReqObject.component.isActive= 1; ajaxReqObject.component.noDuplicates =
     * $('#fieldNoDuplicates').is(':checked')? 1 : 0;
     * ajaxReqObject.component.dataTypeId =
     * parseInt($("#txtDT").data("kendoComboBox").value());
     * ajaxReqObject.component.unitId =
     * parseInt($("#cmbUnits").data("kendoComboBox").value());
     * ajaxReqObject.component.length = getNumericBoxValue("txtChars");
     * ajaxReqObject.component.miniTemplateId =
     * parseInt($("#cmbMiniTemplates").data("kendoComboBox").value());
     */
    //console.log($('#' + dom.id + ' #checkBoxOptions label').length);
    if ($('#' + dom.id + ' #checkBoxOptions label').length) {
        var notes = '';
        var checkboxlen = $('#' + dom.id + ' #checkBoxOptions label').length;
        for (var i = 0; i < $('#' + dom.id + ' #checkBoxOptions label').length; i++) {
            var delimiter = '\n';
            notes += $('#' + dom.id + ' #checkBoxOptions label')[i].innerHTML;
            if (i != checkboxlen - 1)
                notes += delimiter;

        }
        ajaxReqObject.component.notes = notes;
    } else if ($('#' + dom.id + ' #radioOptions label').length) {
        var notes = '';
        var radiolen = $('#' + dom.id + ' #radioOptions label').length;
        for (var i = 0; i < $('#' + dom.id + ' #radioOptions label').length; i++) {
            var delimiter = '\n';
            notes += $('#' + dom.id + ' #radioOptions label')[i].innerHTML;
            if (i != radiolen - 1)
                notes += delimiter;

        }
        ajaxReqObject.component.notes = notes;
    } else if ($('#' + dom.id + ' select option').length) {
        var notes = '';
        var radiolen = $('#' + dom.id + ' select option').length;
        for (var i = 0; i < $('#' + dom.id + ' select option').length; i++) {
            var delimiter = '\n';
            notes += $('#' + dom.id + ' select option')[i].innerHTML;
            if (i != radiolen - 1)
                notes += delimiter;

        }
        ajaxReqObject.component.notes = notes;
    }
    if (ajaxReqObject.component.componentTypeId == 20) {
        var notes = '';
        notes = $('#cmbNamefontweight').val() + '~' + $('#cmbNameformat').val()
            + '~' + $('#cmbNamecolor').val() + '~' + labelText || '';
        ajaxReqObject.component.notes = notes;

        if ($("#" + dom.id + " #instruction").length) {
            $("#" + dom.id + " #instruction").css("font-weight", "normal");
            $("#" + dom.id + " #instruction").css("font-style", "normal");
            if ($('#cmbNamefontweight').val().toLowerCase() === 'bold') {
                $("#" + dom.id + " #instruction").css("font-weight", $('#cmbNamefontweight').val());
            }
            else {
                $("#" + dom.id + " #instruction").css("font-style", $('#cmbNamefontweight').val());
            }

            $("#" + dom.id + " #instruction").css("color", $('#cmbNamecolor').val());
        }

    }

    //console.log($('#miniColType').val());
    // console.log($('#'+dom.id + ' #radioOptions label').length);
    // ajaxReqObject.component.displayOrder = finalObject.length + 1;
    // return ajaxReqObject;
    // }

}
function prepareComponents(components) {
    savedComponent = components;
    $('#dynamic-from').empty();
    console.log(components);

    if (components) {
        for (var i = 0; i < components.length; i++) {
            var type = _.findWhere(componetTypes, {
                id: components[i].componentTypeId
            }).type;
            var cele = components[i];
            var noOfFormElements = i + 1;
            var ele = setInputElements(cele, type, noOfFormElements);
            $('#dynamic-from').append(ele);
            // fieldSettings($(ele), type,'','','fields');
        }
    }
};
function setInputElements(component, type, noOfFormElements) {
    var id = noOfFormElements;
    var groupClass = "";
    var ele = "";
    var className = "";
    var style = "";
    var required = component.isRequired && component.isRequired == 1 ? 'required = "required"'
        : "";
    var requiredSpan = required.length > 0 ? '<span style="color:red" id="mandatory">*</span>'
        : "";
    var label = '<label for="fi' + id + '" id="titleLabel">' + component.name
        + requiredSpan + '</label>';
    var cHint = component.hint || '';
    var remarks = component.isEncrypted;
    var hintInstruction = '<span class="folder-name" id="instruction">' + cHint
        + '</span>';
    var hintRemarks;
    var lbl = component.label.split('\n');
    var rhint = "";
    if(lbl && lbl.length > 0){
        rhint = lbl[1];
    }

    hintRemarks = '<span class="folder-name" id="remarks">' + rhint
        + '</span>';

    var unit = "";
    unit = component.unitValue && component.unitValue.length > 0 ? '<span class="unit" id="units">' + component.unitValue
        + '</span>' : '';
    if (type.toLowerCase() == 'text') {

        className = 'class="form-control"';
        groupClass = "form-group";
        var predefinedValue = component.predefinedValue || '';
        var placeholderText = component.placeholderText || '';
        ele = '<input type="' + type + '" id="fi' + id + '" ' + className
            + required + 'value="' + predefinedValue + '" placeholder="'
            + placeholderText + '"/>';
    } else if (type.toLowerCase() == 'label') {

        label = '<label for="fi' + id + '" id="titleLabel">' + component.name
            + '</label>';
        // ele = '<label id="fi' + id + '">'+component.label+'</label>';

        var labelstyle = '', lbstyle = '';
        if (component.notes.indexOf('~') > 0) {

            var cmpnotesArr = component.notes.split('~');

            if (cmpnotesArr !== null && cmpnotesArr.length > 0) {

                if (cmpnotesArr[0].toLowerCase() === 'bold') {
                    labelstyle = 'font-weight:bold';
                }
                else if (cmpnotesArr[0].toLowerCase() === 'italic') {
                    labelstyle = 'font-style:italic';
                }
                else if (cmpnotesArr[0].toLowerCase() === 'normal') {
                    labelstyle = 'font-style:normal';
                }


                if (cmpnotesArr.length > 2) {
                    labelstyle = labelstyle + ';color:' + cmpnotesArr[2];
                }
            }
        }

        lbstyle = 'style="' + labelstyle + '"';



        hintInstruction = '<span class="labelfolder-name" id="instruction" ' + lbstyle + '>'
            + component.name + '</span>'
        var onclick = "fieldSettings(this,'label','','" + groupClass
            + "','comefrom',\'savedfield\'," + component.id + ")";
        var deleteEle = '<a href="javascript:void(0);" class="trash" onclick="removeEle(this,\'main'
            + id
            + '\','
            + component.id
            + ')"><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a>';
        var formDiv = '<div class="' + groupClass + ' maindiv"' + style
            + 'onclick="' + onclick + '" id="main' + id + '">' + '<div class="leftBlock">' + label + '</div><div class="rightBlock">'
            + ele + hintInstruction + deleteEle + '</div></div>';
        return formDiv;
    } else if (type.toLowerCase() == 'number' || type.toLowerCase() == 'price') {

        className = 'class="form-control"';
        groupClass = "form-group";
        var predefinedValue = component.predefinedValue || '';
        var placeholderText = component.placeholderText || '';
        var maximumValue = component.maximumValue || '';
        var minimumValue = component.minimumValue || '';
        ele = '<input type="number" id="fi' + id + '" ' + className + required
            + 'value="' + predefinedValue + '" placeholder="'
            + placeholderText + '" min="' + minimumValue + '" max="'
            + maximumValue + '"/>';
    } else if (type.toLowerCase() == 'hline') {
        ele = '<hr id="fi' + id + '" />';
        // return ele;

    } else if (type.toLowerCase() === 'checkbox'
        || type.toLowerCase() === 'groupcheckbox') {
        groupClass = "checkbox";
        type = "checkbox";
        label = '<label id="titleLabel">' + component.name + requiredSpan
            + '</label>';
        var eles = component.notes.split('\n');
        var ele1 = "";
        for (var i = 0; i < eles.length; i++) {
            ele1 += '<input type="checkbox" id="fi' + id + '_' + i + '" '
                + className + required + '/><label for="fi' + id + '_' + i
                + '" id="fl' + id + '_' + i + '" >' + eles[i] + '</label>';
        }
        /*
         * var ele1 = '<div><input type="' + type + '" id="fi' + id + '_0" ' +
         * className + required + '/><label for="fi' + id + '_0" id="fl' + id +
         * '_0" >First option</label>'; var ele2 = '<input type="' + type + '"
         * id="fi' + id + '_1" ' + className + required + '/><label for="fi' +
         * id + '_1" id="fl' + id + '_1" >Second option</label>'; var ele3 = '<input
         * type="' + type + '" id="fi' + id + '_2" ' + className + required +
         * '/><label for="fi' + id + '_2" id="fl' + id + '_2" >Third option</label></div>';
         */
        // ele = ele1 + ele2 + ele3;
        ele = '<div id="checkBoxOptions">' + ele1 + '</div>'+hintRemarks;
    } else if (type.toLowerCase() === 'radio') {
        groupClass = "radio";
        label = '<label id="titleLabel">' + component.name + requiredSpan
            + '</label>';
        var name = " name = 'optradio" + id + "'";
        var eles = component.notes.split('\n');
        var ele1 = "";
        for (var i = 0; i < eles.length; i++) {
            var index = i + 1;
            ele1 += '<input type="' + type + '" id="fi' + id + '_' + index
                + '" ' + className + required + '/><label for="fi' + id
                + '_' + index + '" id="fl' + id + '_' + index + '" >'
                + eles[i] + '</label>';
        }
        /*
         * var ele1 = '<div><input type="' + type + '" id="fi' + id + '_1" ' +
         * className + name + required + '/><label class="radio-inline"
         * for="fi' + id + '" id="fl' + id + '_0">Option 1</label>'; var ele2 = '<input
         * type="' + type + '" id="fi' + id + '_2" ' + className + name +
         * required + '/><label class="radio-inline" for="fi' + id + '" id="fl' +
         * id + '_1"> Option 2</label>'; var ele3 = '<input type="' + type + '"
         * id="fi' + id + '_3" ' + className + name + required + '/><label
         * class="radio-inline" for="fi' + id + '" id="fl' + id + '_2"> Option 3</label></div>';
         */
        // ele = ele1 + ele2 + ele3;
        ele = '<div id="radioOptions">' + ele1 + '</div>' + hintRemarks;
    } else if (type.toLowerCase() == 'button') {
        label = "";
        groupClass = "btn-group";
        style = ' style="margin-bottom:15px;"';
        ele = '<button type="button" class="btn btn-success" id="fi' + id
            + '">' + component.name + requiredSpan + '</button>'
        var onclick = "fieldSettings(this,\'" + component.name + "\','','"
            + groupClass + "','',\'savedfield\'," + component.id + ")";
        var deleteEle = '<a href="javascript:void(0);" class="trash" onclick="removeEle(this,\'main'
            + id
            + '\','
            + component.id
            + ')"><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a>';
        var formDiv = '<div class="' + groupClass + ' maindiv"' + style
            + 'onclick="' + onclick + '" id="main' + id + '">' + '<div class="leftBlock">' + label + '</div><div class="rightBlock">'
            + ele + deleteEle + hintInstruction + '</div></div>';
        return formDiv;
    } else if (type.toLowerCase() == 'email') {

        className = 'class="form-control"';
        groupClass = "form-group";
        var predefinedValue = component.predefinedValue || '';
        var placeholderText = component.placeholderText || '';
        ele = '<input type="email" id="fi' + id + '" ' + className + required
            + 'value="' + predefinedValue + '" placeholder="'
            + placeholderText + '"/>';
    } else if (type.toLowerCase() == 'textarea') {
        var id = noOfFormElements;
        var groupClass = "form-group";
        var ele = "";
        var className = "form-control";
        var style = "";
        var rows = 5;
        var predefinedValue = component.predefinedValue || '';
        var label = ' <label for="fi' + id + '" id="titleLabel">'
            + component.name + requiredSpan + '</label>';
        ele = '<textarea class="' + className + '" rows="' + rows + '" id="fi'
            + id + '"' + required + '>' + predefinedValue + '</textarea>';
        var onclick = "fieldSettings(this,'textarea','','" + groupClass
            + "','comefrom',\'savedfield\'," + component.id + ")";
        var deleteEle = '<a href="javascript:void(0);" class="trash" onclick="removeEle(this,\'main'
            + id
            + '\','
            + component.id
            + ')"><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a>';
        var formDiv = '<div class="' + groupClass + ' maindiv"' + style
            + 'onclick="' + onclick + '" id="main' + id + '">' + '<div class="leftBlock">' + label + '</div><div class="rightBlock">'
            + ele + hintInstruction + deleteEle + '</div></div>';
        return formDiv;
    } else if (type && type.toLowerCase() == 'datetime') {
        var groupClass = "''";
        var onclick = "fieldSettings(this,'datetime',''," + groupClass
            + ",'comefrom',\'savedfield\'," + component.id + ")";
        var ele = "";
        var deleteEle = '<a href="javascript:void(0);" class="trash" onclick="removeEle(this,\'cfl'
            + id
            + '\','
            + component.id
            + ')"><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a>';
        ele = '<div class="customfields cdatetime maindiv" onclick="' + onclick + '" id="cfl'
            + id
            + '"><div class="leftBlock"><label id="titleLabel">'
            + component.name
            + requiredSpan
            + '</label></div><div class="rightBlock"><input type="datetime-local" class="form-control" value="'
            + component.predefinedValue
            + '"id="cfi'
            + id
            + '"' + required + '/>'
            + hintInstruction + deleteEle + '</div></div>';
        return ele;
    } else if (type && type.toLowerCase() == 'datepicker') {
        var groupClass = "''";
        var onclick = "fieldSettings(this,'datepicker',''," + groupClass
            + ",'comefrom',\'savedfield\'," + component.id + ")";
        var ele = "";
        var deleteEle = '<a href="javascript:void(0);" class="trash" onclick="removeEle(this,\'cfl'
            + id
            + '\','
            + component.id
            + ')"><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a>';
        ele = '<div class="customfields cdatetime maindiv" onclick="' + onclick + '" id="cfl'
            + id
            + '"><div class="leftBlock"><label id="titleLabel">'
            + component.name
            + requiredSpan
            + '</label></div><div class="rightBlock"><input type="date" class="form-control" value="'
            + component.predefinedValue
            + '"id="cfi'
            + id
            + '"' + required + '/>'
            + hintInstruction + deleteEle + '</div></div>';
        return ele;
    } else if (type && type.toLowerCase() == 'dropdown') {
        var id = noOfFormElements;
        var groupClass = "form-group";
        var ele = "";
        var className = "form-control";
        var style = "";
        var rows = 5;
        var label = ' <label for="fi' + id + '" id="titleLabel">'
            + component.name + requiredSpan + '</label>';
        var eles = component.notes.split('\n');
        var options = "";
        for (var i = 0; i < eles.length; i++) {
            var index = i + 1;
            options += '<option value="1" id="option' + i + '">' + eles[i]
                + '</option>';

        }
        // var options = ' <option value="1"> First Choice</option><option
        // value="2"> Second Choice</option><option value="3"> Third
        // Choice</option>';
        ele = '<select class="' + className + '" rows="' + rows + '" id="fi'
            + id + '" style="display: inline-block;" ' + required + '>'
            + options + '</select>';
        var onclick = "fieldSettings(this,'dropdown','','" + groupClass
            + "','comefrom',\'savedfield\'," + component.id + ")";
        var deleteEle = '<a href="javascript:void(0);" class="trash" onclick="removeEle(this,\'main'
            + id
            + '\','
            + component.id
            + ')"><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a>';
        var formDiv = '<div class="' + groupClass + ' maindiv"' + style
            + 'onclick="' + onclick + '" id="main' + id + '">' + '<div class="leftBlock">' + label + '</div><div class="rightBlock">'
            + ele + hintRemarks + hintInstruction + deleteEle + '</div></div>';
        return formDiv;
    }

    else if (type.toLowerCase() == 'name') {
        var deleteEle = '<a href="javascript:void(0);" class="trash" onclick="removeEle(this,\'cfl'
            + id
            + '\','
            + component.id
            + ')"><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a>';
        ele = '<div class="customfields cnames maindiv col-xs-12 noPadding" onclick="fieldSettings(this,\'name\',\'\',\'\',\'comefrom\',\'savedfield\','
            + component.id
            + ')" id="cfl'
            + id
            + '"><div class="leftBlock"><span class="title" id="titleLabel">'
            + component.name
            + requiredSpan
            + '</span></div><div class="rightBlock"><div class="namegroup"><input type="text" class="form-control first" id="cff'
            + id
            + '"/><span>First</span></div><div class="namegroup"><input type="text" class="form-control middle" id="cfm'
            + id
            + '"/><span>Middle</span></div><div class="namegroup"><input type="text" class="form-control last" id="cfl'
            + id
            + '"/><span>Last</span></div>' + hintInstruction + '<div class="col-xs-1">'
            + deleteEle + '</div></div></div>';
        return ele;
    } else if (type && type.toLowerCase() == 'phone') {
        var deleteEle = '<a href="javascript:void(0);" class="trash" onclick="removeEle(this,\'cfl'
            + id
            + '\','
            + component.id
            + ')"><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a>';
        ele = '<div class="customfields cphone maindiv" onclick="fieldSettings(this,\'phone\',\'\',\'\',\'comefrom\',\'savedfield\','
            + component.id
            + ')" id="cfl'
            + id
            + '"><div class="leftBlock"><label id="titleLabel">'
            + component.name
            + requiredSpan
            + '</label></div><div class="rightBlock"><input type="tel" class="form-control" id="cfi'
            + id
            + '"/>' + hintInstruction + deleteEle + '</div></div>';
        return ele;
    } else if (type && type.toLowerCase() == 'website') {
        var deleteEle = '<a href="javascript:void(0);" class="trash" onclick="removeEle(this,\'cfl'
            + id
            + '\','
            + component.id
            + ')"><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a>';
        ele = '<div class="customfields cwebsite" onclick="fieldSettings(this,\'website\',\'\',\'\',\'comefrom\',\'savedfield\','
            + component.id
            + ')" id="cfl'
            + id
            + '"><div class="leftBlock"><label id="titleLabel">'
            + component.name
            + requiredSpan
            + '</label></div><div class="rightBlock"><input type="url" id="cfi'
            + id
            + '" '
            + className
            + required
            + 'value="'
            + component.predefinedValue
            + '" placeholder="'
            + component.placeholderText
            + '"/>'
            + hintInstruction + deleteEle + '</div></div>';
        return ele;
    } else if (type && type.toLowerCase() == 'datetime') {
        var deleteEle = '<a href="javascript:void(0);" class="trash" onclick="removeEle(this,\'cfl'
            + id
            + '\','
            + component.id
            + ')"><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a>';
        ele = '<div class="customfields cdatetime maindiv" onclick="fieldSettings(this,\'datetime\',\'\',\'\',\'comefrom\',\'savedfield\','
            + component.id
            + ')" id="cfl'
            + id
            + '"><div class="leftBlock"><label id="titleLabel">'
            + component.name
            + requiredSpan
            + '</label></div><div class="rightBlock"><input type="datetime-local" class="form-control" id="cfi'
            + id + '"/>' + hintInstruction + deleteEle + '</div></div>';
        return ele;
    } else if (type && type.toLowerCase() == 'address') {
        var deleteEle = '<a href="javascript:void(0);" class="trash" onclick="removeEle(this,\'cfl'
            + id
            + '\','
            + component.id
            + ')"><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a>';
        ele = '<div class="customfields caddress maindiv" onclick="fieldSettings(this,\'address\',\'\',\'\',\'comefrom\',\'savedfield\','
            + component.id
            + ')" id="cfl'
            + id
            + '"><div class="leftBlock"><span  class="title" id="titleLabel">'
            + component.name
            + requiredSpan
            + '</span></div><div class="rightBlock"><div class="add-blocks"><label>Address1</label><input type="input"  class="add1 form-control" id="cfadd'
            + id
            + '"/></div><div class="add-blocks"><label>Address2</label><input type="input" class="add2 form-control" id="cfadd2'
            + id
            + '"/></div><div class="add-blocks"><label>City</label><input type="input"  class="city form-control" id="cfcity'
            + id
            + '"/></div><div class="add-blocks"><label>State</label><input type="input" class="state form-control" id="cfstate'
            + id
            + '"/></div><div class="add-blocks"><label>Postal / Zip Code</label><input type="input" class="postal form-control" id="cfpostal'
            + id
            + '"/></div><div class="add-blocks"><label>Country</label><input type="input" class="country form-control" id="cfcount'
            + id + '"/></div>' + hintInstruction + deleteEle + '</div></div>';
        return ele;
    } else if (type && type.toLowerCase() == 'rating') {
        var deleteEle = '<a href="javascript:void(0);" class="trash" onclick="removeEle(this,\'cfl'
            + id
            + '\','
            + component.id
            + ')"><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a>';
        ele = '<div class="customfields crating maindiv" onclick="fieldSettings(this,\'rating\',\'\',\'\',\'comefrom\',\'savedfield\','
            + component.id
            + ')" id="rating'
            + id
            + '"><div class="leftBlock"><label id="titleLabel">'
            + component.name
            + requiredSpan
            + '</label></div><div class="rightBlock"><span class="star  clicked" value="1" tabindex="1"><i class="fa fa-star fa-2x" aria-hidden="true"></i></span><span class="star  clicked" value="1" tabindex="1"><i class="fa fa-star fa-2x" aria-hidden="true"></i></span><span class="star  clicked" value="1" tabindex="1"><i class="fa fa-star fa-2x" aria-hidden="true"></i></span><span class="star" value="1" tabindex="1"><i class="fa fa-star fa-2x" aria-hidden="true"></i></span><span class="star" value="1" tabindex="1"><i class="fa fa-star fa-2x" aria-hidden="true"></i></span>'
            + hintInstruction + deleteEle + '</div></div>';
        return ele;
    } else if (type && type.toLowerCase() == 'serviceuser') {
        label = '<label for="fi' + id
            + '" id="titleLabel">Service User</label>';
        deleteEle = '<a href="javascript:void(0);" class="trash" onclick="removeEle(this,\'cfl'
            + id
            + '\')"><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a>';
        groupClass = "serviceuser";
        // var firstRow = '<div class="namegroup"><input type="text" class="form-control" id="sid" /><label>ID:</label></div><div class="namegroup"><input type="text" class="form-control" id="first" /><label>First</label></div><div class="namegroup"><input type="text" class="form-control" id="middle" /><label>Middle</label></div><div class="namegroup"><input type="text" class="form-control" id="last" /><label>Last</label></div>';
        // var secondRow = '<div class="namegroup"><input type="text" class="form-control" id="dob" /><label>DOB:</label></div><div class="namegroup"><input type="text" class="form-control" id="gender">Gender</label></div><div class="namegroup"><input type="text" class="form-control" id="marritalstatus" /><label>Marrital Status</label></div><div class="namegroup"><input type="text" class="form-control" id="race" /><label>Race</label></div>';
        // var thirdRow = '<div class="namegroup"><input type="text" class="form-control" id="ethnicity" /><label>Ethnicity</label></div><div class="namegroup"><input type="text" class="form-control" id="prelang" /><label>Preferred Language</label></div><div class="namegroup"><input type="text" class="form-control" id="second" /><label>Second</label></div><div class="namegroup"><input type="text" class="form-control" id="third" /><label>Third</label></div>';
        // var fourthRow = '<div class="namegroup"><input type="text" class="form-control" id="address1" /><label>Address1</label></div><div class="namegroup"><input type="text" class="form-control" id="address2" /><label>Address2</label></div><div class="namegroup"><input type="text" class="form-control" id="city" /><label>City</label></div><div class="namegroup"><input type="text" class="form-control" id="state" /><label>State/County</label></div>';
        // var fifthRow = '<div class="namegroup"><input type="text" class="form-control" id="country" /><label>Country</label></div><div class="namegroup"><input type="text" class="form-control" id="postal" /><label>Postal/Zip Code</label></div><div class="namegroup"><input type="text" class="form-control" id="homephone" /><label>Home Phone</label></div><div class="namegroup"><input type="text" class="form-control" id="mobile" /><label>Mobile</label></div>';
        // var sixthRow = '<div class="namegroup"><input type="text" class="form-control" id="workphone" /><label>Work Phone</label></div><div class="namegroup"><input type="email" class="form-control" id="eamil" /><label>Email</label></div><div class="namegroup"><input type="text" class="form-control" id="sms" /><label>SMS</label></div>';

        var firstRow = '<div class="col-xs-1"><label>Last</label></div><div class="col-xs-1"><label>First</label></div><div class="col-xs-1"><label>Middle</label></div><br clear="all"/>';
        var secondRow = '<div class="col-xs-1"><label>DOB</label></div><div class="col-xs-1"><label>Gender</label></div><br clear="all"/>';

        var fourthRow = '<div class="col-xs-2"><label>Address1</label></div><div class="col-xs-2"><label> Address2</label></div><br clear="all"/><div class="col-xs-1"><label>City</label></div><div class="col-xs-2"><label>State/County</label></div>';
        var fifthRow = '<div class="col-xs-1"><label>Country</label></div><br clear="all"/><div class="col-xs-3"><label>Home Phone</label></div><br clear="all"/><div class="col-xs-3"><label>Cell Phone</label></div><br clear="all"/>';




        ele = firstRow + secondRow + fourthRow + fifthRow;
    } else if (type && type.toLowerCase() == 'staff') {
        label = '<label for="fi' + id
            + '" id="titleLabel">Staff</label>';
        deleteEle = '<a href="javascript:void(0);" class="trash" onclick="removeEle(this,\'cfl'
            + id
            + '\')"><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a>';
        groupClass = "serviceuser";
        // var firstRow = '<div class="namegroup"><input type="text" class="form-control" id="sid" /><label>ID:</label></div><div class="namegroup"><input type="text" class="form-control" id="first" /><label>First</label></div><div class="namegroup"><input type="text" class="form-control" id="last" /><label>Last</label></div>';
        // var secondRow = '<div class="namegroup"><input type="text" class="form-control" id="dob" /><label>DOB:</label></div><div class="namegroup"><input type="text" class="form-control" id="gender"><label>Gender</label></div>';
        // var thirdRow = '<div class="namegroup"><input type="text" class="form-control" id="prelang" /><label>Preferred Language</label></div><div class="namegroup"><input type="text" class="form-control" id="second" /><label>Second</label></div><div class="namegroup"><input type="text" class="form-control" id="third" /><label>Third</label></div>';
        // var fourthRow = '<div class="namegroup"><input type="text" class="form-control" id="address1" /><label>Address1</label></div><div class="namegroup"><input type="text" class="form-control" id="address2" /><label>Address2</label></div><div class="namegroup"><input type="text" class="form-control" id="city" /><label>City</label></div><div class="namegroup"><input type="text" class="form-control" id="state" /><label>State/County</label></div>';
        // var fifthRow = '<div class="namegroup"><input type="text" class="form-control" id="country" /><label>Country</label></div><div class="namegroup"><input type="text" class="form-control" id="postal" /><label>Postal/Zip Code</label></div><div class="namegroup"><input type="text" class="form-control" id="homephone" /><label>Home Phone</label></div><div class="namegroup"><input type="text" class="form-control" id="mobile" /><label>Mobile</label></div>';
        // var sixthRow = '<div class="namegroup"><input type="text" class="form-control" id="workphone" /><label>Work Phone</label></div><div class="namegroup"><input type="email" class="form-control" id="eamil" /><label>Email</label></div>';
        var firstRow = '<div class="col-xs-1"><label>Last</label></div><div class="col-xs-1"><label>First</label></div><div class="col-xs-1"><label>Middle</label></div><br clear="all"/>';
        var secondRow = '<div class="col-xs-1"><label>DOB</label></div><div class="col-xs-1"><label>Gender</label></div><br clear="all"/>';

        var fourthRow = '<div class="col-xs-2"><label>Address1</label></div><div class="col-xs-2"><label> Address2</label></div><br clear="all"/><div class="col-xs-1"><label>City</label></div><div class="col-xs-2"><label>State/County</label></div>';
        var fifthRow = '<div class="col-xs-1"><label>Country</label></div><br clear="all"/><div class="col-xs-3"><label>Home Phone</label></div><br clear="all"/><div class="col-xs-3"><label>Cell Phone</label></div><br clear="all"/>';
        ele = firstRow + secondRow + fourthRow + fifthRow;

    } else {
        groupClass = "form-group";
        ele = '<input type="' + type + '" id="fi' + id + '" ' + className
            + required + '/>';
    }

    var onclick = "fieldSettings(this,'" + type + "','','" + groupClass
        + "','comefrom',\'savedfield\'," + component.id + ")";
    var deleteEle = '<a href="javascript:void(0);" class="trash" onclick="removeEle(this,\'main'
        + id
        + '\','
        + component.id
        + ')"><i class="fa fa-trash fa-lg" aria-hidden="true"></i></a>';
    var formDiv = '';
    if (type && type.toLowerCase() == 'hline') {
        formDiv = '<div class="' + groupClass + ' maindiv"' + style
            + ' onclick="' + onclick + '" id="main' + id + '">' + ele
            + deleteEle + '</div>';
    }
    else {
        formDiv = '<div class="' + groupClass + ' maindiv"' + style
            + ' onclick="' + onclick + '" id="main' + id + '">' + '<div class="leftBlock">' + label + '</div><div class="rightBlock">' + ele
            + hintInstruction + unit + deleteEle + '</div></div>';
    }
    return formDiv;

};
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
function checkFormat(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode == 35 || charCode == 44 || charCode == 46) {
        return true;
    }
    return false;

}
$(document).ready(function () {
    $("#dynamic-from").sortable({
        update: function (event, ui) {
            setComponentsDisplayOrder();
        }
    }).disableSelection();
    /*$(".field-item").draggable({
        containment : "#container",
        helper : 'clone',
        revert : 'invalid'
    });

    $("#dynamic-from, #addFields").droppable({
        hoverClass : 'ui-state-highlight',
        accept: ":not(.ui-sortable-helper)",
        drop : function(ev, ui) {
            $(ui.draggable).clone().appendTo(this);
            $(ui.draggable).remove();
        }
    });*/
});
function setComponentsDisplayOrder() {
    console.log(savedComponent);
    console.log($('#dynamic-from').find('.maindiv'));
    for (var i = 0; i < savedComponent.length; i++) {

        var ele = $('#dynamic-from').find('.maindiv')[i];
        var id = getEleId(ele);
        for (var j = 0; j < savedComponent.length; j++) {
            if (id && id == savedComponent[j].id) {
                //var prevOrder = savedComponent[j].displayOrder;
                savedComponent[j].displayOrder = i + 1;
                //savedComponent[i].displayOrder = prevOrder;
            }
        }
        //savedComponent[i].displayOrder = i+1;
    }

    console.log(savedComponent);
    dragAndDrop = true;
    onDragSave();

}
function getEleId(ele) {
    var id = null;
    var attrs = ele.attributes;
    for (i = 0; i < attrs.length; i++) {
        if (attrs[i].nodeName.toLowerCase() == 'onclick') {
            var val = attrs[i].value;
            id = val.substring(
                val.lastIndexOf(",") + 1,
                val.lastIndexOf(")")
            );
            return id;
        }
    }
    return id;
}
function previewFile() {
    var preview = document.getElementById('img'); //selects the query named img
    var file = document.querySelector('input[type=file]').files[0]; //sames as here
    var reader = new FileReader();

    reader.onloadend = function () {
        preview.src = reader.result;
    }

    if (file) {
        reader.readAsDataURL(file); //reads the data as a URL
    } else {
        preview.src = "";
    }
}
function onMiniColTypeChange() {
    ajaxReqObject.component.label = $('#miniColType').val();
}
//previewFile();  //calls the function named previewFile()
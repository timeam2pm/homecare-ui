var patientInfoObject = null;
var providerInfoObject = null;
var parentRef = null;
var recordType = "1";
var dataArray = [];
var dataTypeArray = [{Key:'Text',Value:'1'},{Key:'Number',Value:'2'},{Key:'Boolean',Value:'3'}];
var unitArray =  [{Key:'Kg',Value:'1'},{Key:'Lt',Value:'2'}];
var statusArray =  [{Key:'ACTIVE',Value:'1'},{Key:'INACTIVE',Value:'2'}];
var selItem = null;
var selItem1 = null;
var resultDataObj = null;
var currZipId = "";
var addressComp = [];
var addedComponents = [];
var DataTypes = [
    {
        type : "int",
        validation : "isNumber(event)"
    },
    {
        type : "varchar",
        validation : "isVarchar(event)"
    },
    {
        type : "decimal",
        validation : "isDecimal(event)"
    }
];
var localFormData;
var pageName;
$(document).ready(function(){
    if(parent.frames['iframe'])
        parentRef = parent.frames['iframe'].window;
    $("#divMain",parent.document).css("display","none");
    $("#divPatInfo",parent.document).css("display","");
    $("#lblTitleName",parent.document).html("Form Data");
    //recordType = parentRef.dietType;
});

$(window).load(function(){
    loading = false;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
    pageName = localStorage.getItem("pageName");
    if(pageName != "form") {
        selItem = parentRef.selItem;
        selItem1 = parentRef.selItem1;
        resultDataObj = parentRef.dataObj;
    }
    else{

        selItem = JSON.parse(localStorage.getItem("localFormData"));
        selItem1 = JSON.parse(localStorage.getItem("selItem1"));
        resultDataObj = JSON.parse(localStorage.getItem("dataObj"));
    }

    if(selItem){
        $("#lblFrmData").text("Template - " + selItem.notes);
        $("#cmbZip1").kendoComboBox();
        adjustHeight();
        setFormData();

        buttonEvents();
    }
}
function init(){
    $("#btnEdit").prop("disabled", true);
    $("#btnDelete").prop("disabled", true);
    buildDeviceListGrid([]);
    //buildDeviceListGrid1([]);
    var selectedItems = angularUIgridWrapper1.getSelectedRows();
    console.log(selectedItems);
    $("#lblFrmData").text("");
    if(selectedItems && selectedItems.length>0){
        $("#lblFrmData").text(selectedItems[0].notes);
        var idk = selectedItems[0].idk;
        // https://stage.timeam.com/homecare/template-values/?access_token=d8ca6333-09cc-4e12-852c-b187b274c143&templateId=5&patient-id=101createdDate=1541780752000
        var ipaddress = ipAddress+"/homecare/template-values/?formTemplateId="+idk+"&patientId="+sessionStorage.patientId;+"&createdDate="+selectedItems.createdDate;
        getAjaxObject(ipaddress,"GET",handleGetVacationList,onError);

    }

    //getAjaxObject(ipAddress+"/homecare/activity-types/?is-active=1&is-deleted=0&sort=display-order","GET",getActivityTypes,onError);
}

function getAddedComponents(evt){

    var patientId = sessionStorage.patientId;
    addedComponents = [];
    var idk = $(evt.currentTarget).attr("idk");
    if(patientId){
        getAjaxObject(ipAddress + "/patient/"+patientId+"/", "GET", function(resp){
            patientInfoObject = resp.response;
            var providerId = sessionStorage.providerId;
            //getAjaxObject(ipAddress + "/provider/"+providerId+"/", "GET", function(resp) {
            if(sessionStorage.providerData)
                providerInfoObject = JSON.parse(sessionStorage.providerData);
            if(idk){
                var getipaddress = ipAddress+"/homecare/components/?is-active=1&is-deleted=0&miniTemplateId="+idk+"&fields=*,dataType.*,unit.*,component.*";

                getAjaxObject(getipaddress,"GET",function(dataObj){
                    console.log(dataObj);
                    if(dataObj && dataObj.response && dataObj.response.status ){
                        if(dataObj.response.status.code == "1"){
                            if(dataObj.response.components){
                                if($.isArray(dataObj.response.components)){
                                    addedComponents = dataObj.response.components;
                                }else{
                                    addedComponents.push(dataObj.response.components);
                                }
                            }
                        }
                    }
                    setTimeout(function(){onClickItem(evt);},100);

                },onError);
            }
        }, onError);
    }
    else if(sessionStorage.providerId && sessionStorage.providerId.length>0)
    {
        var providerId = sessionStorage.providerId;
        //getAjaxObject(ipAddress + "/provider/"+providerId+"/", "GET", function(resp) {
        providerInfoObject = JSON.parse(sessionStorage.providerData);
        if(idk){
            var getipaddress = ipAddress+"/homecare/components/?is-active=1&is-deleted=0&miniTemplateId="+idk+"&fields=*,dataType.*,unit.*,component.*";

            getAjaxObject(getipaddress,"GET",function(dataObj){
                console.log(dataObj);
                if(dataObj && dataObj.response && dataObj.response.status ){
                    if(dataObj.response.status.code == "1"){
                        if(dataObj.response.components){
                            if($.isArray(dataObj.response.components)){
                                addedComponents = dataObj.response.components;
                            }else{
                                addedComponents.push(dataObj.response.components);
                            }
                        }
                    }
                }
                setTimeout(function(){onClickItem(evt);},100);

            },onError);
        }
        // }, onError);
    }



}

function handleGetVacationListOfAdd(dataObj){
    console.log(dataObj);
}
var frmDesignArray = [];
function handleGetVacationList(dataObj){
    console.log(dataObj);
    frmDesignArray = [];
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            if(dataObj.response.formTemplateValues){
                if($.isArray(dataObj.response.formTemplateValues)){
                    frmDesignArray = dataObj.response.formTemplateValues;
                }else{
                    frmDesignArray.push(dataObj.response.formTemplateValues);
                }
            }
        }
    }
    for(var i=0;i<frmDesignArray.length;i++){
        var dt = new Date(frmDesignArray[i].createdDate);
        dt = kendo.toString(dt, "MM-dd-yyyy h:mm:tt");
        frmDesignArray[i].crDate = dt;
    }
    buildDeviceListGrid(frmDesignArray);
    /*var strHtml = "";
    $("#divFormData").html("");
    for(var j=0;j<frmDesignArray.length;j++){
        var frmObj = frmDesignArray[j];
        frmObj.idk = frmObj.id;
        frmObj.length1 = frmObj.length;
        var frmId = "f"+frmObj.idk;
        var unit  ="";
        if(frmObj.unitValue){
            unit = frmObj.unitValue;
        }else{

        }
        strHtml = strHtml+'<div class="col-sm-6 col-md-6 col-lg-6 col-xs-6 form-group">';
        strHtml = strHtml+'<label control-label input-sm noPadding">'+frmObj.name+' :</label>';
        strHtml = strHtml+'<div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 noPadding">';
        strHtml = strHtml+'<div class="col-sm-9 col-md-9 col-lg-9 col-xs-8 noPadding">';
        strHtml = strHtml+'	<input id="'+frmId+'" type="text" class="form-control input-sm"></input>';
        strHtml = strHtml+'</div>';
        strHtml = strHtml+'<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label input-sm noPadding">&nbsp;&nbsp;'+unit+'</label>';
        strHtml = strHtml+'</div></div>';
    }
    $("#divFormData").html(strHtml);*/
    //getFormData();
    //buildDeviceListGrid(vacation);
}
var fromArray = [];
var miniTemplates = [];
var COMP_TEXT = "text box";
var COMP_TEXT_AREA = "text area"
var COMP_BOOLEAN = "boolean";
var COMP_CHECK_BOX = "check box"
var COMP_RADIO_BUTTON = "radio button"
var COMP_GROUP_CHECK_BOX = "group check box"
var COMP_IMAGE = "image"
var COMP_DATE_PICKER = "date picker";
var COMP_DROPDOWN = "dropdown";
var COMP_EMAIL = "email";
var COMP_NAME = "name";
var COMP_ADDRESS = "address";
var COMP_PHONE = "phone";
var COMP_WEBSITE = "website";
var COMP_DATETIME = "datetime";
var COMP_RATING = "rating";
var COMP_TIME = "time";
var COMP_PRICE = "price";
var COMP_NUMBER = "number";
var COMP_LIKERT = "likert";
var COMP_HLINE = "horizontal line";
var COMP_LABEL = "label";
var COMP_SERVICEUSER = "service user";
var COMP_STAFF = "staff";

var DT_INT = "INT";
var DT_VARCHAR = "VARCHAR";
var DT_DATETIME = "DATETIME";
var DT_DECIMAL = "DECIMAL";
var DT_DATE = "DATE";
function setFormData(){
    var strHtml = "";
    $("#divPanel").html("");
    miniTemplates = resultDataObj.response.templateValues[0].componentValues;
    $("#txtNotes").val(resultDataObj.response.templateValues[0].notes);
    console.log(resultDataObj.response.templateValues[0]);
    console.log(_.where(miniTemplates,{miniTemplateId:6}));
    console.log(_.findWhere(selItem.miniTemplates,{id:parseInt(currId)}));

    var miniTemplatesArray = selItem.miniTemplates;
    for(var m=0;m<miniTemplatesArray.length;m++){
        var mItem = miniTemplatesArray[m];
        if(mItem&& mItem.isDeleted==0){
            var mItemHref = mItem.name;
            var mItemId = "m"+mItem.id;
            var pBodyItem = "p"+mItem.id;
            var strhref = "#mini"+mItem.id;
            strHtml = strHtml+'<div class="panel panel-default">';
            strHtml = strHtml+'<div class="panel-heading">';
            strHtml = strHtml+'<a data-toggle="collapse" href="'+strhref+'" class="pnlTitle" id="'+mItemId+'" idk="'+mItem.id+'" data-parent="#divPanel">'+mItem.name+'</a></div>';
            strHtml = strHtml+'<div id="mini'+mItem.id+'" class="panel-collapse collapse out"><div class="panel-body" id="'+pBodyItem+'"style="padding-bottom:0px;padding:10px"></div></div></div>';
        }
    }
    $("#divPanel").html(strHtml);
    for(var n=0;n<miniTemplatesArray.length;n++){
        var mItem = miniTemplatesArray[n];
        if(mItem && mItem.isDeleted==0){
            var pBodyItem = "p"+mItem.id;
            var mItemId = "m"+mItem.id;
            $("#"+mItemId).off("click");
            $("#"+mItemId).on("click",getAddedComponents);
        }
    }

}
var currId = "";
var rightside;
var providerImage;
var staffHTML;
function onClickItem(evt) {
    debugger;
    console.log(evt);
    currId = $(evt.currentTarget).attr("idk");
    var curMiniTemplate = _.findWhere(selItem.miniTemplates,{id:parseInt(currId)});
    //getAddedComponents(currId);
    console.log(_.where(miniTemplates,{miniTemplateId:parseInt(currId)}));
    /*var idk = currId;
    if($("#p"+currId).children().length == 0){
        var ipaddress = ipAddress+"/homecare/components/?is-active=1&is-deleted=0&fields=*,component.*,dataType.*,unit.*&miniTemplateId="+idk;
           getAjaxObject(ipaddress,"GET",handleGetVacationList,onError);
    }*/
    var strHtml = "";
    var addComp ={};
    var miniTemplateById = _.where(miniTemplates,{miniTemplateId:parseInt(currId)});
    miniTemplateById = _.sortBy(miniTemplateById, 'miniComponentDisplayOrder');
    for(var m=0;m<miniTemplateById.length;m++){
        if((miniTemplateById[m].miniComponentType == COMP_SERVICEUSER && !patientInfoObject))
            m++;
        if(m==miniTemplateById.length)
            break;
        var mItem = miniTemplateById[m];
        if(mItem){
            // $("#lblFrmData").text(mItem.miniTemplateName);
            mItem.idk = mItem.id;
            var frmId = "f"+mItem.id;
            var mValue = mItem.value|| '';
            var fcolValue  = '';
            var mLabel;
            console.log(m);
            if(_.findWhere(curMiniTemplate.componenets,{id:mItem.componentId})) {
                mLabel = _.findWhere(curMiniTemplate.componenets, {id: mItem.componentId}).label;
            }
            if(mLabel == '3'){
                console.log(mItem.value);
                mValue = mItem.value.split('|')[0];
                fcolValue = mItem.value.split('|')[1];
            }
            var unit  ="";
            var hint  ="";
            var maxLength = "";
            mItem.frmId = frmId;
            var requiredSpan = mItem.isRequired && mItem.miniComponentType !='label' && mItem.isRequired ==1?'<span style="color:red" id="mandatory">*</span>' : '';
            if(mItem.miniUnits){
                unit = mItem.miniUnits;
            }else{

            }
            addComp = _.findWhere(addedComponents,{id:mItem.componentId})|| {};
            if(addComp && addComp.hint)
                hint = addComp.hint;
            if(addComp && addComp.length)
                maxLength = addComp.length;
            // strHtml = strHtml+'<div class="clearfix"></div><div class="popupformElements">';
            if((mItem.miniComponentType == COMP_SERVICEUSER)){
                strHtml = strHtml+'<div class="clearfix"></div><div class="col-xs-12 popupformElements serviceUseraddressForm"><div class="addformserviceleftdiv">';
            }

            else if(mItem.miniComponentType == COMP_ADDRESS){
                strHtml = strHtml+'<div class="clearfix"></div><div class="col-xs-12 popupformElements subinputlist">';
            }
            else{
                if (mItem.miniComponentType != COMP_STAFF) {
                    //strHtml = strHtml + '<div class="clearfix"></div><div class="popupformElements">';
                    strHtml = strHtml+'<div class="col-xs-12">';
                }
            }

            if(!(mItem.miniComponentType == COMP_HLINE || mItem.miniComponentType == "hline")){
                //var clsName = 'class="form-rightData-new"';
                //var clsLeftBlock = 'class="leftBlock-new"';
                var clsName = 'class="form-rightData"';
                var clsLeftBlock = 'class="leftBlock"';
                //if(curMiniTemplate && curMiniTemplate.label == 3)
                //{
                //    clsName = 'class="col3 form-rightData-new"';
                //    clsLeftBlock = 'class="col3 leftBlock-new"';
                //}
                //if(curMiniTemplate && curMiniTemplate.label == 4)
                //{
                //    clsName = 'class="col3 fourthele form-rightData-new"';
                //    clsLeftBlock = 'class="col3 fourthele leftBlock-new"';
                //}

                if(mItem.miniComponentType == COMP_LABEL){
                    var style = ' style="';
                    var notes = mItem.miniComponentNotes;
                    if(notes){
                        var notesArray = notes.split("~");
                        if(notesArray[0] && notesArray[0].toLowerCase() == 'bold')
                            style = style+'font-weight:bold !important;"';
                        else if(notesArray[0])
                            style = style+'font-style:'+notesArray[0]+' !important;"';
                    }

                    strHtml = strHtml+'<div><label>'+mItem.miniComponentName+' </label></div>';
                }
                else{
                    var img = '';

                    if(mItem.miniComponentType == COMP_SERVICEUSER && patientInfoObject){
                        var imageServletUrl = ipAddress + "/download/patient/photo/" + patientInfoObject.patient.id + "/?" + Math.round(Math.random() * 1000000) + "&access_token=" + sessionStorage.access_token + "&tenant=" + sessionStorage.tenant;
                        img = '<img src="'+imageServletUrl+'" class="p-img" />';

                        if(providerInfoObject) {
                            providerImage = ipAddress + "/homecare/download/providers/photo/?" + Math.round(Math.random() * 1000000) + "&access_token=" + sessionStorage.access_token + "&tenant=" + sessionStorage.tenant + "&id=" + providerInfoObject.PID;

                        }
                    }
                    // else if(mItem.miniComponentType == COMP_STAFF && providerInfoObject){
                    // 	var imageServletUrl = ipAddress + "/homecare/download/providers/photo/?" + Math.round(Math.random() * 1000000)+"&access_token="+sessionStorage.access_token+"&tenant="+sessionStorage.tenant+"&id=" + providerInfoObject.PID;
                    // 	img = '<img src="'+imageServletUrl+'" class="p-img" />';
                    // }
                    if(mItem.miniComponentName == "serviceuser"){
                        mItem.miniComponentName = "Service User Details";
                    }

                    if (mItem.miniComponentType != COMP_STAFF) {
                        if (mItem.miniComponentType == COMP_SERVICEUSER) {
                            strHtml = strHtml + '<div ' + clsLeftBlock + '><label class="">' + mItem.miniComponentName + ' :</label>' + requiredSpan + img + '</div>';
                            strHtml = strHtml + '<div ' + clsName + '>';
                            strHtml = strHtml + '<div class="rightblock generalInformblock">';
                        }
                        else {
                            strHtml = strHtml + '<div><label>' + mItem.miniComponentName + ' </label></div>';
                        }
                        //strHtml = strHtml + '<div ' + clsLeftBlock + '><label class="">' + mItem.miniComponentName + ' :</label>' + requiredSpan + img + '</div>';
                        //strHtml = strHtml + '<div ' + clsName + '>';
                        //strHtml = strHtml + '<div class="rightblock generalInformblock">';
                    }
                }
            }
            if(mItem.miniComponentType == COMP_TEXT){
                strHtml = strHtml+'	<input id="'+frmId+'" type="text" class="form-control input-sm" value="'+mValue+'"  onkeypress="return '+_.findWhere(DataTypes,{type:mItem.miniDataTypeValue.toLowerCase()}).validation+'" maxlength="'+maxLength+'"></input>';
            }
            else if(mItem.miniComponentType == COMP_NUMBER || mItem.miniComponentType == COMP_PRICE){
                strHtml = strHtml+'	<input id="'+frmId+'" type="number" class="form-control input-sm" value="'+mValue+'"  onkeypress="return '+_.findWhere(DataTypes,{type:mItem.miniDataTypeValue.toLowerCase()}).validation+'" max="'+mItem.maximumValue+'" min="'+mItem.minimumValue+'"></input>';
            }else if(mItem.miniComponentType == COMP_TEXT_AREA){
                strHtml = strHtml+'	<textarea id="'+frmId+'"  class="form-control input-sm" value="'+mValue+'" onkeypress="return '+_.findWhere(DataTypes,{type:mItem.miniDataTypeValue.toLowerCase()}).validation+'" maxlength="'+maxLength+'">'+mValue+'</textarea>';
            }else if(mItem.miniComponentType == COMP_DATE_PICKER){
                strHtml = strHtml+'	<input id="'+frmId+'" type="text" class="form-control input-sm noBorder"></input>';
            }
            else if(mItem.miniComponentType == COMP_LABEL){
                var style = '';
                var labelText = '';
                var notes = mItem.miniComponentNotes;
                if(notes){

                    var notesArray = notes.split("~");
                    var rightClass = notesArray[1] && notesArray[1].toLowerCase() == 'right'? '<div class="form-rightData-new"><div class="rightblock">' : '<div><div >'
                    //strHtml = strHtml+rightClass;
                    if(notesArray[2] && notesArray[2])
                        style = 'style="color:'+notesArray[2]+';"';
                    /*if(notesArray[0] && notesArray[0].toLowerCase() == 'bold')
                        style = style+'font-weight:bold;"';
                    else if(notesArray[0])
                        style = style+'font-style:'+notesArray[0]+';"';*/
                    if(notesArray[3])
                        labelText = notesArray[3];
                }
                strHtml = strHtml + '<div id="'+frmId+'">'+labelText+'</div>';
            }

            else if(mItem.miniComponentType == COMP_HLINE || mItem.miniComponentType == "hline"){
                strHtml = strHtml+'<hr id="'+frmId+'" />';
            }
            else if(mItem.miniComponentType == COMP_RADIO_BUTTON || mItem.miniComponentType == COMP_CHECK_BOX){
                var notes = mItem.miniComponentNotes;
                console.log(notes);
                if(notes){
                    var notesArray = notes.split("\n");
                    //strHtml = strHtml+'<div form-group" >';
                    for(var n=0;n<notesArray.length;n++){
                        var mItem1 = notesArray[n];
                        if(mItem1){
                            if(mItem.miniComponentType == COMP_RADIO_BUTTON){
                                if(mValue == mItem1){
                                    strHtml = strHtml+'<div class="radio lblcustom"><label class="">'+mItem1+'<input id="rd" type="radio" name="'+frmId+'"  value="'+mItem1+'" /><span class="radiomark"></span></label></div>';
                                }else{
                                    strHtml = strHtml+'<div class="radio lblcustom"><label class="">'+mItem1+'<input id="rd" type="radio" name="'+frmId+'"  value="'+mItem1+'"  /><span class="radiomark"></span></label></div>';
                                }

                            }else if(mItem.miniComponentType == COMP_CHECK_BOX){
                                if(mValue == mItem1){
                                    strHtml = strHtml+'<div class="checkbox lblcustom"><label class="">'+mItem1+'<input id="chk" type="checkbox" name="'+frmId+'"  value="'+mItem1+'"   /><span class="checkmark"></span></label></div>';
                                }else{
                                    strHtml = strHtml+'<div class="checkbox lblcustom"><label class="">'+mItem1+'<input id="chk" type="checkbox" name="'+frmId+'"  value="'+mItem1+'"  /><span class="checkmark"></span></label></div>';
                                }

                            }
                            //strHtml = strHtml+'<span id="lbl">'+mItem1+'</span>&nbsp;&nbsp;&nbsp;&nbsp;';
                        }
                    }
                    strHtml = strHtml+'</div></div>';
                }
            }
            else if(mItem.miniComponentType == COMP_DROPDOWN){
                var notes = mItem.miniComponentNotes;
                if(notes){

                    var notesArray = notes.split("\n");
                    strHtml = strHtml+'<select id="'+frmId+'" class="form-control"><option value=""> </option>';
                    for(var d=0;d<notesArray.length;d++){
                        var sItem = notesArray[d];
                        if(sItem){
                            var selected = '';
                            if(sItem == mItem.value)
                                selected = "selected";
                            strHtml = strHtml+'<option  value="'+sItem+'" '+selected+' >'+sItem+'</option>';

                            //strHtml = strHtml+'<span id="lbl">'+mItem+'</span>&nbsp;&nbsp;&nbsp;&nbsp;';
                        }
                    }
                    strHtml = strHtml+'</select>';

                }
            }
            else if(mItem.miniComponentType == COMP_EMAIL){
                strHtml = strHtml+'	<input id="'+frmId+'" type="email" class="form-control input-sm" value="'+mValue+'"></input>';
            }
            else if(mItem.miniComponentType == COMP_WEBSITE){
                strHtml = strHtml+'	<input id="'+frmId+'" type="url" class="form-control input-sm" value="'+mValue+'"></input>';
            }
            else if(mItem.miniComponentType == COMP_PHONE){
                strHtml = strHtml+'<input id="'+frmId+'" type="tel" class="form-control input-sm" value="'+mValue+'"  onkeypress="return '+_.findWhere(DataTypes,{type:mItem.miniDataTypeValue.toLowerCase()}).validation+'"></input>';
            }
            else if(mItem.miniComponentType == COMP_DATETIME){
                //var date = '\/Date('+mValue+')\/';
                var nowDate = "";
                var datetimeVal = "";
                if(mValue.length){
                    nowDate = new Date(parseInt(mValue));
                    datetimeVal = nowDate.format("yyyy-mm-dd'T'")+nowDate.format("HH:MM:ss");
                }

                strHtml = strHtml+'	<input id="'+frmId+'" type="datetime-local" class="form-control input-sm" value='+datetimeVal+'></input>';
            }
            else if(mItem.miniComponentType == COMP_NAME){
                var names = mValue.split('~');
                strHtml = strHtml+'<div class="namegroup"><input type="text" placeholder="First" class="form-control first" id="cff'
                    + frmId
                    + '"  value="'+names[0]+'" onkeypress="return '+_.findWhere(DataTypes,{type:mItem.miniDataTypeValue.toLowerCase()}).validation+'"/></div><div class="namegroup"><input type="text" placeholder="Middle" class="form-control middle" id="cfm'
                    + frmId + '" value="'+(names[1]|| '')+'" onkeypress="return '+_.findWhere(DataTypes,{type:mItem.miniDataTypeValue.toLowerCase()}).validation+'"/></div><div class="namegroup"><input type="text" placeholder="Last" class="form-control last" id="cfl'
                    + frmId + '"  value="'+(names[2]|| '')+'" onkeypress="return '+_.findWhere(DataTypes,{type:mItem.miniDataTypeValue.toLowerCase()}).validation+'"/></div>';
            }
            else if(mItem.miniComponentType == COMP_ADDRESS){
                var adds = mValue.split('~');
                strHtml = strHtml+'<div class="add-blocks"><input type="text" placeholder="Address1" class="add1 form-control" id="cfadd1'
                    + frmId
                    + '"  value="'+(adds[0] || '')+'"/></div><div class="add-blocks"><input type="text" placeholder="Address2" class="add2 form-control" id="cfadd2'
                    + frmId
                    + '"  value="'+(adds[1] || '')+'"/></div><div class="add-blocks"><div class="noPadding postalWrapper"><input type="text" placeholder="City" class="city form-control" readonly id="cfcity'
                    + frmId
                    + '"  value="'+(adds[2] || '')+'"/><img id="btnZipSearch'+mItem.id+'" class="postalWrapper-img" src="../../img/Default/search-icon.png" title="Search" /><input type="hidden" id="cityIdHiddenValue"></div></div><div class="add-blocks"><input type="text" class="state form-control" placeholder="State / County"  readonly id="cfstate'
                    + frmId
                    + '"  value="'+(adds[3] || '')+'"/></div><div class="add-blocks"><input type="text" placeholder="Country" class="postal form-control" readonly id="cfcountry'
                    + frmId
                    + '"  value="'+(adds[4] || '')+'"/></div><div class="add-blocks" id="'+frmId+'"><div class=""><div class="noPadding postalWrapper"><input placeholder="postal/zip code" id="cmbZip'+frmId+'" value="'+(adds[5] || '')+'" type="text" class="form-control input-sm" readonly /></div></div></div>';
                // + '"  value="'+(adds[4] || '')+'"/></div><div class="add-blocks" id="'+frmId+'"><div class=""><div class="noPadding postalWrapper"><input placeholder="postal/zip code" id="cmbZip'+frmId+'" value="'+(adds[5] || '')+'" type="text" class="form-control input-sm" readonly /><img id="btnZipSearch'+mItem.id+'" class="postalWrapper-img" src="../../img/Default/search-icon.png" title="Search" /><input type="hidden" id="cityIdHiddenValue"></div></div></div>';
            }
            else if(mItem.miniComponentType == COMP_SERVICEUSER){
                if(patientInfoObject){
                    var languageVal = patientInfoObject.patient.language;
                    if(languageVal) {
                        languageVal = languageVal.split(',');
                    }

                    // var firstRow = '<div class="col-xs-3"><input type="text" class="form-control" readonly value="'+patientInfoObject.patient.id +'" id="sid'+frmId+'" /><label>ID:</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+patientInfoObject.patient.firstName +'" id="first'+frmId+'" /><label>First</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+patientInfoObject.patient.middleName  +'" id="middle'+frmId+'" /><label>Middle</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+patientInfoObject.patient.lastName +'" id="last'+frmId+'" /><label>Last</label></div>';
                    // var secondRow = '<div class="col-xs-3"><input type="text" class="form-control input-sm" readonly value="'+dob +'" id="dob'+frmId+'" /><label>DOB:</label></div><div class="col-xs-3"><input type="text" readonly value="'+patientInfoObject.patient.gender +'"  class="form-control" id="gender">Gender</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+patientInfoObject.patient.maritalStatus +'" id="marritalstatus'+frmId+'" /><label>Marrital Status</label></div><div class="col-xs-3"><input type="text" value="'+patientInfoObject.patient.race +'" readonly class="form-control" id="race'+frmId+'" /><label>Race</label></div>';
                    // var thirdRow = '<div class="col-xs-3"><input type="text" readonly value="'+patientInfoObject.patient.ethnicity +'" class="form-control" id="ethnicity'+frmId+'" /><label>Ethnicity</label></div><div class="col-xs-3"><input type="text" readonly value="'+languageVal[0] +'" class="form-control" id="prelang'+frmId+'" /><label>Preferred Language</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+languageVal[1] +'" id="second'+frmId+'" /><label>Second</label></div><div class="col-xs-3"><input type="text" class="form-control" value="'+languageVal[2] +'" id="third'+frmId+'" readonly /><label>Third</label></div>';
                    // var fourthRow = '<div class="col-xs-3"><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.address1 +'" id="address1'+frmId+'" /><label>Address1</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.address2 +'" id="address2'+frmId+'" /><label>Address2</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.city +'" id="city'+frmId+'" /><label>City</label></div><div class="col-xs-3"><input type="text" value="'+patientInfoObject.communication.state +'" class="form-control" readonly id="state'+frmId+'" /><label>State/County</label></div>';
                    // var fifthRow = '<div class="col-xs-3"><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.country +'" id="country'+frmId+'" /><label>Country</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.zip +'" id="postal'+frmId+'" /><label>Postal/Zip Code</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.homePhone +'" id="homephone'+frmId+'" /><label>Home Phone</label></div><div class="col-xs-3"><input type="text" value="'+patientInfoObject.communication.cellPhone +'" readonly class="form-control" id="mobile'+frmId+'" /><label>Mobile</label></div>';
                    // var sixthRow = '<div class="col-xs-3"><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.workPhone +'" id="workphone'+frmId+'" /><label>Work Phone</label></div><div class="col-xs-3"><input type="text" readonly value="'+patientInfoObject.communication.email +'" class="form-control" id="eamil'+frmId+'" /><label>Email</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.sms +'"  id="sms'+frmId+'" /><label>SMS</label></div>';


                    // var firstRow = '<div class="col-xs-3"><span class="form-control" id="sid'+frmId+'">'+patientInfoObject.patient.id+'</span><label>ID:</label></div><div class="col-xs-3"><span class="form-control">patientInfoObject.patient.firstName </span><label>First</label></div><div class="col-xs-3"><span class="form-control" id="middle'+frmId+'">patientInfoObject.patient.middleName</span>><label>Middle</label></div><div class="col-xs-3"><span class="form-control" id="last'+frmId+'">'+patientInfoObject.patient.lastName+'</span><label>Last</label></div>';
                    // var secondRow = '<div class="col-xs-3"><span class="form-control input-sm" id="dob'+frmId+'">'+dob+'</span><label>DOB:</label></div><div class="col-xs-3"><span class="form-control" id="gender">'+patientInfoObject.patient.gender +'</span><label>Gender</label></div><div class="col-xs-3"><span class="form-control" id="marritalstatus'+frmId+'">'+patientInfoObject.patient.maritalStatus+'</span><label>Marrital Status</label></div><div class="col-xs-3"><span class="form-control" id="race'+frmId+'">'+patientInfoObject.patient.race +'</span><label>Race</label></div>';
                    // var thirdRow = '<div class="col-xs-3"><span class="form-control" id="ethnicity'+frmId+'">'+patientInfoObject.patient.ethnicity+'</span><label>Ethnicity</label></div><div class="col-xs-3"><span class="form-control" id="prelang'+frmId+'">'+languageVal[0] +'</span><label>Preferred Language</label></div><div class="col-xs-3"><span class="form-control" id="second'+frmId+'">'+languageVal[1] +'</span><label>Second</label></div><div class="col-xs-3"><span class="form-control" id="third'+frmId+'">'+languageVal[2] +'</span><label>Third</label></div>';
                    // var fourthRow = '<div class="col-xs-3"><span class="form-control" id="address1'+frmId+'">'+patientInfoObject.communication.address1 +'</span><label>Address1</label></div><div class="col-xs-3"><span class="form-control" id="address2'+frmId+'">'+patientInfoObject.communication.address2 +'</span><label>Address2</label></div><div class="col-xs-3"><span class="form-control" id="city'+frmId+'">'+patientInfoObject.communication.city+' </span><label>City</label></div><div class="col-xs-3"><span class="form-control" id="state'+frmId+'">'+patientInfoObject.communication.state +'</span><label>State/County</label></div>';
                    // var fifthRow = '<div class="col-xs-3"><span class="form-control" id="country'+frmId+'">'+patientInfoObject.communication.country+'</span><label>Country</label></div><div class="col-xs-3"><span class="form-control" id="postal'+frmId+'">'+patientInfoObject.communication.zip+'</span><label>Postal/Zip Code</label></div><div class="col-xs-3"><span class="form-control" id="homephone'+frmId+'">'+patientInfoObject.communication.homePhone+'</span><label>Home Phone</label></div><div class="col-xs-3"><span value="'+patientInfoObject.communication.cellPhone +'" class="form-control" id="mobile'+frmId+'">'+patientInfoObject.communication.cellPhone+'</span><label>Mobile</label></div>';
                    // var sixthRow = '<div class="col-xs-3"><span class="form-control" id="workphone'+frmId+'">'+patientInfoObject.communication.workPhone +'</span><label>Work Phone</label></div><div class="col-xs-3"><span type="text" class="form-control" id="eamil'+frmId+'">'+patientInfoObject.communication.email+'</span><label>Email</label></div><div class="col-xs-3"><span class="form-control" id="sms'+frmId+'">'+patientInfoObject.communication.sms+'</span><label>SMS</label></div>';

                    // var firstRow = '<div class="col-xs-3"><span class="form-control" id="last'+frmId+'">'+patientInfoObject.patient.lastName+'</span>&nbsp;<label>Last</label></div><div class="col-xs-3"><span class="form-control">'+' '+patientInfoObject.patient.firstName+' </span>&nbsp;<label>First</label></div><div class="col-xs-3"><span class="form-control" id="middle'+frmId+'">'+' '+patientInfoObject.patient.middleName+'</span>&nbsp;<label>Middle</label></div><br/>';
                    // var secondRow = '<div class="col-xs-3"></div><div class="col-xs-3"><span class="form-control input-sm" id="dob'+frmId+'">'+dob+'</span>&nbsp;<label>DOB:</label></div><div class="col-xs-3"><span class="form-control" id="gender">'+patientInfoObject.patient.gender +'</span><label style="display:none">Gender</label></div><br/>';
                    //
                    // var fourthRow = '<div class="col-xs-3"></div><div class="col-xs-3"><span class="form-control" id="address1'+frmId+'">'+patientInfoObject.communication.address1 +'</span>&nbsp;<label>Address1</label></div><div class="col-xs-3"><span class="form-control" id="address2'+frmId+'">'+patientInfoObject.communication.address2 +'</span>&nbsp;<label>Address2</label></div><div class="col-xs-3"><span class="form-control" id="city'+frmId+'">'+patientInfoObject.communication.city+' </span>&nbsp;<label>City</label></div></br><div class="col-xs-3"><span class="form-control" id="state'+frmId+'">'+patientInfoObject.communication.state +'</span>&nbsp;<label>State/County</label></div>';
                    // var fifthRow = '<div class="col-xs-3"><span class="form-control" id="country'+frmId+'">'+patientInfoObject.communication.country+'</span><label>Country</label></div></br><div class="col-xs-3"><span class="form-control" id="homephone'+frmId+'">'+homePhone+'</span><label>Home Phone</label></div></br><div class="col-xs-3"><span class="form-control" id="mobile'+frmId+'">'+cellPhone+'</span><label>Cell Phone</label></div>';

                    // var ele = firstRow + secondRow+ fourthRow + fifthRow;
                    // strHtml = strHtml + ele;
                    var cellPhone = "";
                    if(patientInfoObject.communication.cellPhone != null){
                        cellPhone = "<b>Cell Phone </b>" + patientInfoObject.communication.cellPhone;
                    }

                    var homePhone = "";
                    if(patientInfoObject.communication.homePhone != null){
                        homePhone = "<b>Home Phone </b>" + patientInfoObject.communication.homePhone;
                    }

                    var dob = dformat(new Date(patientInfoObject.patient.dateOfBirth).getTime(), 'dd-MM-yyyy');

                    // var firstRow = '<div class="col-xs-3"><label>ID:</label><input type="text" class="form-control" readonly value="'+patientInfoObject.patient.id +'" id="sid'+frmId+'" /></div><div class="col-xs-3"><label>First</label><input type="text" class="form-control" readonly value="'+patientInfoObject.patient.firstName +'" id="first'+frmId+'" /></div><div class="col-xs-3"><label>Middle</label><input type="text" class="form-control" readonly value="'+patientInfoObject.patient.middleName  +'" id="middle'+frmId+'" /></div><div class="col-xs-3"><label>Last</label><input type="text" class="form-control" readonly value="'+patientInfoObject.patient.lastName +'" id="last'+frmId+'" /></div>';
                    // var secondRow = '<div class="col-xs-3"><label>DOB:</label><input type="text" class="form-control " readonly value="'+dob +'" id="dob'+frmId+'" /></div><div class="col-xs-3"><label>Gender</label><input type="text" readonly value="'+patientInfoObject.patient.gender +'"  class="form-control" id="gender"></div><div class="col-xs-3"><label>Marrital Status</label><input type="text" class="form-control" readonly value="'+patientInfoObject.patient.maritalStatus +'" id="marritalstatus'+frmId+'" /></div><div class="col-xs-3"><label>Race</label><input type="text" value="'+patientInfoObject.patient.race +'" readonly class="form-control" id="race'+frmId+'" /></div>';
                    // var thirdRow = '<div class="col-xs-3"><label>Ethnicity</label><input type="text" readonly value="'+patientInfoObject.patient.ethnicity +'" class="form-control" id="ethnicity'+frmId+'" /></div><div class="col-xs-3"><label>Preferred Language</label><input type="text" readonly value="'+languageVal[0] +'" class="form-control" id="prelang'+frmId+'" /></div><div class="col-xs-3"><label>Second</label><input type="text" class="form-control" readonly value="'+languageVal[1] +'" id="second'+frmId+'" /></div><div class="col-xs-3"><label>Third</label><input type="text" class="form-control" value="'+languageVal[2] +'" id="third'+frmId+'" readonly /></div>';
                    // var fourthRow = '<div class="col-xs-3"><label>Address1</label><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.address1 +'" id="address1'+frmId+'" /></div><div class="col-xs-3"><label>Address2</label><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.address2 +'" id="address2'+frmId+'" /></div><div class="col-xs-3"><label>City</label><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.city +'" id="city'+frmId+'" /></div><div class="col-xs-3"><label>State/County</label><input type="text" value="'+patientInfoObject.communication.state +'" class="form-control" readonly id="state'+frmId+'" /></div>';
                    // var fifthRow = '<div class="col-xs-3"><label>Country</label><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.country +'" id="country'+frmId+'" /></div><div class="col-xs-3"><label>Postal/Zip Code</label><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.zip +'" id="postal'+frmId+'" /></div><div class="col-xs-3"><label>Home Phone</label><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.homePhone +'" id="homephone'+frmId+'" /></div><div class="col-xs-3"><label>Mobile</label><input type="text" value="'+patientInfoObject.communication.cellPhone +'" readonly class="form-control" id="mobile'+frmId+'" /></div>';
                    // var sixthRow = '<div class="col-xs-3"><label>Work Phone</label><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.workPhone +'" id="workphone'+frmId+'" /></div><div class="col-xs-3"><label>Email</label><input type="text" readonly value="'+patientInfoObject.communication.email +'" class="form-control" id="eamil'+frmId+'" /></div><div class="col-xs-3"><label>SMS</label><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.sms +'"  id="sms'+frmId+'" /></div>';
                    var firstRow = '<div class="col-xs-3"><span class="form-control" id="last'+frmId+'">'+patientInfoObject.patient.lastName+'</span>&nbsp;<label>Last</label></div><div class="col-xs-3"><span class="form-control">'+' '+patientInfoObject.patient.firstName+' </span>&nbsp;<label>First</label></div><div class="col-xs-3"><span class="form-control" id="middle'+frmId+'">'+' '+patientInfoObject.patient.middleName+'</span>&nbsp;<label>Middle</label></div><br/>';
                    var secondRow = '<div class="col-xs-3"></div><div class="col-xs-3"><span class="form-control input-sm" id="dob'+frmId+'">'+dob+'</span>&nbsp;<label>DOB:</label></div><div class="col-xs-3"><span class="form-control" id="gender">'+patientInfoObject.patient.gender +'</span><label style="display:none">Gender</label></div><br clear="all"/>';

                    var fourthRow = '<div class="col-xs-3"></div><div class="col-xs-3"><span class="form-control" id="address1'+frmId+'">'+patientInfoObject.communication.address1 +'</span>&nbsp;<label>Address1</label></div><div class="col-xs-3"><span class="form-control" id="address2'+frmId+'">'+patientInfoObject.communication.address2 +'</span>&nbsp;<label>Address2</label></div><div class="col-xs-3"><span class="form-control" id="city'+frmId+'">'+patientInfoObject.communication.city+' </span>&nbsp;<label>City</label></div></br><div class="col-xs-3"><span class="form-control" id="state'+frmId+'">'+patientInfoObject.communication.state +'</span>&nbsp;<label>State/County</label></div>';
                    var fifthRow = '<div class="col-xs-3"><span class="form-control" id="country'+frmId+'">'+patientInfoObject.communication.country+'</span><label>Country</label></div></br><div class="col-xs-3"><span class="form-control" id="homephone'+frmId+'">'+homePhone+'</span><label>Home Phone</label></div></br><div class="col-xs-3"><span class="form-control" id="mobile'+frmId+'">'+cellPhone+'</span><label>Cell Phone</label></div>';
                    // var sixthRow = '<div class="col-xs-3"><span class="form-control" id="workphone'+frmId+'">'+patientInfoObject.communication.workPhone +'</span><label>Work Phone</label></div><div class="col-xs-3"><span type="text" class="form-control" id="eamil'+frmId+'">'+patientInfoObject.communication.email+'</span><label>Email</label></div><div class="col-xs-3"><span class="form-control" id="sms'+frmId+'">'+patientInfoObject.communication.sms+'</span><label>SMS</label></div>';
                    var ele = firstRow + secondRow+ fourthRow + fifthRow + '</div>';
                    strHtml = strHtml + ele ;
                }

                if(providerInfoObject){
                    var languageVal = providerInfoObject.language;
                    if(languageVal) {
                        languageVal = languageVal.split(',');
                    }

                    var cellPhone = "";
                    if(providerInfoObject.communications[0].cellPhone != undefined && providerInfoObject.communications[0].cellPhone != null && providerInfoObject.communications[0].cellPhone != ''){
                        cellPhone = "<b>Cell Phone </b>" + providerInfoObject.communications[0].cellPhone;
                    }

                    var homePhone = "";
                    if(providerInfoObject.communications[0].homePhone != null){
                        homePhone = "<b>Home Phone </b>" + providerInfoObject.communications[0].homePhone;
                    }


                    var dob = dformat(new Date(providerInfoObject.dateOfBirth).getTime(), 'dd-MM-yyyy');

                    // var firstRow = '<div class="col-xs-3"><div class="col-xs-6"><input type="text" class="form-control" readonly value="'+providerInfoObject.PID +'" id="sid'+frmId+'"/><label>ID:</label></div><div class="col-xs-6" style="padding-right:0;"><input type="text" class="form-control" readonly value="'+providerInfoObject.prefix +'" id="sid'+frmId+'" /><label>Prefix:</label></div></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.firstName +'" id="first'+frmId+'" /><label>First</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.lastName +'" id="last'+frmId+'" /><label>Last</label></div>';
                    // var secondRow = '<div class="col-xs-3"><input type="text" class="form-control " readonly value="'+dob +'" id="dob'+frmId+'" /><label>DOB:</label></div><div class="col-xs-3"><input type="text" readonly value="'+providerInfoObject.gender +'"  class="form-control" id="gender">Gender</label></div>';
                    // var thirdRow = '<div class="col-xs-3"><input type="text" readonly value="'+languageVal[0] +'" class="form-control" id="prelang'+frmId+'" /><label>Preferred Language</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+(languageVal[1] || '') +'" id="second'+frmId+'" /><label>Second</label></div><div class="col-xs-3"><input type="text" class="form-control" value="'+(languageVal[2] || '') +'" id="third'+frmId+'" readonly /><label>Third</label></div>';
                    // var fourthRow = '<div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.communications[0].address1 +'" id="address1'+frmId+'" /><label>Address1</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.communications[0].address2 +'" id="address2'+frmId+'" /><label>Address2</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.communications[0].city +'" id="city'+frmId+'" /><label>City</label></div><div class="col-xs-3"><input type="text" value="'+providerInfoObject.communications[0].state +'" class="form-control" readonly id="state'+frmId+'" /><label>State/County</label></div>';
                    // var fifthRow = '<div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.communications[0].country +'" id="country'+frmId+'" /><label>Country</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.communications[0].zip +'" id="postal'+frmId+'" /><label>Postal/Zip Code</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.communications[0].homePhone +'" id="homephone'+frmId+'" /><label>Home Phone</label></div><div class="col-xs-3"><input type="text" value="'+providerInfoObject.communications[0].cellPhone +'" readonly class="form-control" id="mobile'+frmId+'" /><label>Mobile</label></div>';
                    // var sixthRow = '<div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.communications[0].workPhone +'" id="workphone'+frmId+'" /><label>Work Phone</label></div><div class="col-xs-3"><input type="text" readonly value="'+providerInfoObject.communications[0].email +'" class="form-control" id="eamil'+frmId+'" /><label>Email</label></div>';
                    // 	var firstRow = '<div class="col-xs-3"><div class="col-xs-6"><span class="form-control" id="sid'+frmId+'">'+providerInfoObject.PID+'</span><label>ID:</label></div><div class="col-xs-6" style="padding-right:0;"><span class="form-control"  id="sid'+frmId+'" >providerInfoObject.prefix </span><label>Prefix:</label></div></div><div class="col-xs-3"><span class="form-control" id="first'+frmId+'">'+providerInfoObject.firstName+'</span>><label>First</label></div><div class="col-xs-3"><span class="form-control" id="last'+frmId+'">'+providerInfoObject.lastName+'</span><label>Last</label></div>';
                    // 	var secondRow = '<div class="col-xs-3"><span class="form-control input-sm" id="dob'+frmId+'">'+dob+'</span><label>DOB:</label></div><div class="col-xs-3"><span class="form-control" id="gender">'+providerInfoObject.gender +'</span><label>Gender</label></div>';
                    //
                    // 	var thirdRow = '<div class="col-xs-3"><span class="form-control" id="prelang'+frmId+'">'+languageVal[0] +'</span><label>Preferred Language</label></div><div class="col-xs-3"><span class="form-control" id="second'+frmId+'">'+(languageVal[1] || '')+'</span><label>Second</label></div><div class="col-xs-3"><span class="form-control" id="third'+frmId+'">'+(languageVal[2] || '') +'</span><label>Third</label></div>';
                    // var fourthRow = '<div class="col-xs-3"><span class="form-control" id="address1'+frmId+'">'+providerInfoObject.communication.address1 +'</span><label>Address1</label></div><div class="col-xs-3"><span class="form-control" id="address2'+frmId+'">'+providerInfoObject.communication.address2 +'</span><label>Address2</label></div><div class="col-xs-3"><span class="form-control" id="city'+frmId+'">'+providerInfoObject.communication.city+' </span><label>City</label></div><div class="col-xs-3"><span class="form-control" id="state'+frmId+'">'+providerInfoObject.communication.state +'</span><label>State/County</label></div>';
                    // var fifthRow = '<div class="col-xs-3"><span class="form-control" id="country'+frmId+'">'+providerInfoObject.communication.country+'</span><label>Country</label></div><div class="col-xs-3"><span class="form-control" id="postal'+frmId+'">'+providerInfoObject.communication.zip+'</span><label>Postal/Zip Code</label></div><div class="col-xs-3"><span class="form-control" id="homephone'+frmId+'">'+providerInfoObject.communication.homePhone+'</span><label>Home Phone</label></div><div class="col-xs-3"><span value="'+providerInfoObject.communication.cellPhone +'" class="form-control" id="mobile'+frmId+'">'+providerInfoObject.communication.cellPhone+'</span><label>Mobile</label></div>';
                    // var sixthRow = '<div class="col-xs-3"><span class="form-control" id="workphone'+frmId+'">'+providerInfoObject.communication.workPhone +'</span><label>Work Phone</label></div><div class="col-xs-3"><span type="text" class="form-control" id="eamil'+frmId+'">'+providerInfoObject.communication.email+'</span><label>Email</label></div><div class="col-xs-3"><span class="form-control" id="sms'+frmId+'">'+providerInfoObject.communication.sms+'</span><label>SMS</label></div>';

                    var firstRow = '<div class="col-xs-3"><span class="form-control" id="last'+frmId+'">'+providerInfoObject.lastName+'</span>&nbsp;<label>Last</label></div><div class="col-xs-3"><span class="form-control">'+' '+providerInfoObject.firstName+' </span>&nbsp;<label>First</label></div><div class="col-xs-3"><span class="form-control" id="middle'+frmId+'">'+' '+providerInfoObject.middleName+'</span>&nbsp;<label>Middle</label></div><br/>';
                    var secondRow = '<div class="col-xs-3"></div><div class="col-xs-3"><span class="form-control input-sm" id="dob'+frmId+'">'+dob+'</span>&nbsp;<label>DOB:</label></div><div class="col-xs-3"><span class="form-control" id="gender">'+providerInfoObject.gender +'</span><label style="display:none">Gender</label></div><br clear="all"/>';

                    var fourthRow = '<div class="col-xs-3"></div><div class="col-xs-3"><span class="form-control" id="address1'+frmId+'">'+providerInfoObject.communications[0].address1 +'</span>&nbsp;<label>Address1</label></div><div class="col-xs-3"><span class="form-control" id="address2'+frmId+'">'+providerInfoObject.communications[0].address2 +'</span>&nbsp;<label>Address2</label></div><div class="col-xs-3"><span class="form-control" id="city'+frmId+'">'+providerInfoObject.communications[0].city+' </span>&nbsp;<label>City</label></div></br><div class="col-xs-3"><span class="form-control" id="state'+frmId+'">'+providerInfoObject.communications[0].state +'</span>&nbsp;<label>State/County</label></div>';
                    var fifthRow = '<div class="col-xs-3"><span class="form-control" id="country'+frmId+'">'+providerInfoObject.communications[0].country+'</span><label>Country</label></div></br><div class="col-xs-3"><span class="form-control" id="homephone'+frmId+'">'+homePhone+'</span><label>Home Phone</label></div></br><div class="col-xs-3"><span class="form-control" id="mobile'+frmId+'">'+cellPhone+'</span><label>Cell Phone</label></div>';

                    var ele = firstRow + secondRow+ fourthRow + fifthRow;
                    //strHtml = strHtml + ele;
                    staffHTML = ele;
                }


            }

            else if(mItem.miniComponentType == COMP_LIKERT){
                strHtml = strHtml+'	<input id="'+frmId+'" type="datetime-local" class="form-control input-sm"></input>';
            }
            else if(mItem.miniComponentType == COMP_RATING){
                strHtml = strHtml+'<div class="customfields crating maindiv" id="crating'
                    + frmId
                    + '">';
                var ratingVal = '';
                for(var i=0;i<5;i++)
                {
                    var clicked = '';
                    if(i<parseInt(mValue))
                        clicked = 'clicked';
                    ratingVal += '<span class="star '+clicked+'" value="1" tabindex="1" onclick="setRating('+(i+1)+',\''+ frmId+'\')"><i class="fa fa-star fa-2x" aria-hidden="true"></i></span>';
                }
                strHtml = strHtml+ ratingVal +'</div>';
            }

            //strHtml = strHtml+'</div>';
            if(mItem.miniComponentType == COMP_HLINE || mItem.miniComponentType == "hline"){
                strHtml = strHtml+'</div>';
            }
            else{
                if (mItem.componentType != COMP_STAFF) {
                    strHtml = strHtml + '<label class="units" >' + unit + '</label>';
                    if (hint && hint.length > 0)
                        strHtml = strHtml + '<label class="hint">' + hint + '</label>';
                    var col3 = '';
                    var col3 = '';
                    //if (curMiniTemplate && curMiniTemplate.label == 3) {
                    //    if(fcolValue == "undefined"){
                    //        fcolValue = "";
                    //    }
                    //    if(mLabel == '3') {
                    //        var cinput = '<input type="text" class="form-control input-sm" id="fcol' + mItem.id + '"  value="' + (fcolValue || '') + '" style="width:100%;"/>';
                    //        col3 = '<div class="col3 thirdDiv">' + (mLabel == '3' ? cinput : '') + '</div>';
                    //    }
                    //}
                    //else if (curMiniTemplate && curMiniTemplate.label == 4) {
                    //    if(fcolValue == "undefined"){
                    //        fcolValue = "";
                    //    }
                    //    if(mLabel == '4') {
                    //        var cinput3 = '<input type="text" class="form-control input-sm" id="fcol' + mItem.id + '"  value="' + (fcolValue || '') + '" style="width:100%;"/>';
                    //        var cinput4 = '<input type="text" class="form-control input-sm" id="fcol1' + mItem.id + '"  value="' + (fcolValue || '') + '" style="width:100%;"/>';
                    //        col3 = '<div class="col3 thirdDiv fourthEle">' + (mLabel == '4' ? cinput3 : '') + '</div><div class="col3 fourthEle">' + (mLabel == '4' ? cinput4 : '') + '</div>';
                    //    }
                    //}
                }
                // strHtml = strHtml+'</div></div>'+col3+'</div>';
                if(mItem.miniComponentType == COMP_SERVICEUSER){

                    if(!providerInfoObject){

                        var dt = new Date();
                        dt = kendo.toString(dt, "dd-MM-yyyy h:mm:tt");

                        var fullDate = dt;

                        rightside = '<div class="col-xs-12"><div><div class="leftBlock-new">' +
                            '<label class="col-heading">Date Time When Filled Form :</label>' +
                            '<span class="" id="rDateTime">'+fullDate+'</span>' +
                            '</div></div></div>';
                    }
                    else{
                        // rightside = '<div class="col-xs-12 popupformElements"><div><div class="leftBlock-new"><label class="">staff :</label><img src="https://stage.timeam.com/homecare/download/providers/photo/?571838&amp;access_token=c161d936-70ab-4aa4-ba18-482089a79a69&amp;tenant=homecare1&amp;id=13" class="p-img"></div><div class="form-rightData-new"><div class="rightblock generalInformblock"><div class="col-xs-3"><div class="col-xs-6"><span class="form-control" id="sidf1042">13</span><label>ID:</label></div><div class="col-xs-6" style="padding-right:0;"><span class="form-control" id="sidf1042">providerInfoObject.prefix </span><label>Prefix:</label></div></div><div class="col-xs-3"><span class="form-control" id="firstf1042">Provider</span>&gt;<label>First</label></div><div class="col-xs-3"><span class="form-control" id="lastf1042">Provider</span><label>Last</label></div><div class="col-xs-3"><span class="form-control input-sm" id="dobf1042">12/26/1965</span><label>DOB:</label></div><div class="col-xs-3"><span class="form-control" id="gender">Female</span><label>Gender</label></div><div class="col-xs-3"><span class="form-control" id="prelangf1042">English</span><label>Preferred Language</label></div><div class="col-xs-3"><span class="form-control" id="secondf1042"> Spanish</span><label>Second</label></div><div class="col-xs-3"><span class="form-control" id="thirdf1042"> Arabic</span><label>Third</label></div><label class="units"></label></div></div><div class="addformservicerightdiv">fdf</div></div></div>';
                        // rightside = '<div class="col-xs-12 popupformElements"><div><div class="leftBlock-new"><label class="">staff :</label><img src='+providerImage+' class="p-img"></div><div class="form-rightData-new"><div class="rightblock generalInformblock"><div class="col-xs-3"><div class="col-xs-6"><span class="form-control" id="sidf1042">13</span><label>ID:</label></div><div class="col-xs-6" style="padding-right:0;"><span class="form-control" id="sidf1042">providerInfoObject.prefix </span><label>Prefix:</label></div></div><div class="col-xs-3"><span class="form-control" id="firstf1042">Provider</span>&gt;<label>First</label></div><div class="col-xs-3"><span class="form-control" id="lastf1042">Provider</span><label>Last</label></div><div class="col-xs-3"><span class="form-control input-sm" id="dobf1042">12/26/1965</span><label>DOB:</label></div><div class="col-xs-3"><span class="form-control" id="gender">Female</span><label>Gender</label></div><div class="col-xs-3"><span class="form-control" id="prelangf1042">English</span><label>Preferred Language</label></div><div class="col-xs-3"><span class="form-control" id="secondf1042"> Spanish</span><label>Second</label></div><div class="col-xs-3"><span class="form-control" id="thirdf1042"> Arabic</span><label>Third</label></div></div></div></div></div>';
                        // rightside = '<div class="col-xs-12 popupformElements"><div><div class="leftBlock-new"><label class="">staff :</label><img src='+providerImage+' class="p-img"></div><div class="form-rightData-new"><div class="rightblock generalInformblock"><div class="col-xs-3"><div class="col-xs-6"><span class="form-control" id="sidf1042">13</span><label>ID:</label></div><div class="col-xs-6" style="padding-right:0;"><span class="form-control" id="sidf1042">providerInfoObject.prefix </span><label>Prefix:</label></div></div><div class="col-xs-3"><span class="form-control" id="firstf1042">Provider</span>&gt;<label>First</label></div><div class="col-xs-3"><span class="form-control" id="lastf1042">Provider</span><label>Last</label></div><div class="col-xs-3"><span class="form-control input-sm" id="dobf1042">12/26/1965</span><label>DOB:</label></div><div class="col-xs-3"><span class="form-control" id="gender">Female</span><label>Gender</label></div><div class="col-xs-3"><span class="form-control" id="prelangf1042">English</span><label>Preferred Language</label></div><div class="col-xs-3"><span class="form-control" id="secondf1042"> Spanish</span><label>Second</label></div><div class="col-xs-3"><span class="form-control" id="thirdf1042"> Arabic</span><label>Third</label></div></div></div></div></div>';
                        rightside = '<div class="col-xs-12 popupformElements noBorder"><div><div class="leftBlock-new"><label class="col-heading">Staff Details :</label><img src='+ providerImage +' class="p-img"></div><div class="form-rightData-new"><div class="rightblock generalInformblock">'+staffHTML+'</div></div></div></div>';
                    }
                    strHtml = strHtml+'</div></div>'+col3+'<div class="addformserviceleftdiv">'+rightside+'</div></div>';
                }
                else{
                    // if(frmObj.componentType != COMP_STAFF) {
                    //     strHtml = strHtml + '</div></div>' + col3 + '</div>';
                    // }
                    strHtml = strHtml + col3 + '</div></div></div>';
                }

            }
            fromArray.push(mItem);
        }
    }
    for(var i=0;i<miniTemplateById.length;i++){
        if(miniTemplateById[i].miniComponentType == 'address')
            addressComp.push(miniTemplateById[i]);
    }
    //strHtml = strHtml+'<div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 form-group">';
    //strHtml = strHtml+'<div class="navBar" style="padding-bottom:2px">';
    strHtml = strHtml+'</div>'
    //strHtml = strHtml+'<button id="btn-save" class="btn-save">Save</button></div>';

    $("#p"+currId).html(strHtml);
    $("#saveMiniTemplate").off("click");
    $("#saveMiniTemplate").on("click",onClickSave);
    //$("#"+btnId).off("click");
    //$("#"+btnId).on("click",onClickSave);

    setTimeout(function(){
        for(var k=0;k<miniTemplateById.length;k++){
            var mItem = miniTemplateById[k];
            if(mItem){
                mItem.idk = mItem.id;
                var frmId = "f"+mItem.id;
                var mValue = '';
                if(mItem.value != undefined) {
                    var mValue = mItem.value;
                }
                var unit  ="";
                mItem.frmId = frmId;
                if(mItem.miniComponentType == COMP_DATE_PICKER){
                    if(mItem.miniDataTypeValue == DT_DATE){
                        $("#"+frmId).kendoDatePicker({format: "dd-MM-yyyy", value:new Date(mValue)});
                        var cDate = $("#"+frmId).data("kendoDatePicker");
                        if(cDate){
                            cDate.value(new Date(mValue));

                        }
                    }else if(mItem.miniDataTypeValue == DT_DATETIME){
                        $("#"+frmId).kendoDateTimePicker({value:new Date(mValue)});
                        var cDate = $("#"+frmId).data("kendoDateTimePicker");
                        if(cDate){
                            cDate.value(new Date(mValue));
                        }
                    }
                }
            }
        }
    },100);
    $("#btnAdd").off("click");
    $("#btnAdd").on("click",onClickAdd);

    if(addressComp.length){
        for(var i=0;i<addressComp.length;i++){
            //if(addressComp[i].componentTypeId == 12)
            $("#btnZipSearch"+addressComp[i].id).off("click");
            $("#btnZipSearch"+addressComp[i].id).on("click",onClickZipSearch);
            //addressComp.push(addressComp[i].id);
        }
    }
    /*$("#btnSave").off("click",onClickSave);
    $("#btnSave").on("click",onClickSave);*/

    /*$("#btnZipSearch").off("click");
    $("#btnZipSearch").on("click",onClickZipSearch);*/

    $("#AddbtnZipSearch").off("click");
    $("#AddbtnZipSearch").on("click", onClickAdditionalZipSearch);
}

var frmDesignArray = [];
var tempFormDesignArray = [];
function handleGetVacationList(dataObj){
    console.log(dataObj);
    frmDesignArray = [];
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            if(dataObj.response.miniComponents){
                if($.isArray(dataObj.response.miniComponents)){
                    frmDesignArray = dataObj.response.miniComponents;
                }else{
                    frmDesignArray.push(dataObj.response.miniComponents);
                }
            }

        }
    }
    var obj = {};
    obj.idk = currId;
    obj.values = frmDesignArray;
    obj.operation = "add";
    tempFormDesignArray.push(obj);
    var strHtml = "";
    $("#p"+currId).html("");
    for(var j=0;j<frmDesignArray.length;j++){
        var frmObj = frmDesignArray[j];
        frmObj.idk = frmObj.id;
        frmObj.length1 = frmObj.length;
        var frmId = "f"+frmObj.idk;
        var unit  ="";
        if(frmObj.unitValue){
            unit = frmObj.unitValue;
        }else{

        }
        strHtml = strHtml+'<div class="col-sm-6 col-md-6 col-lg-6 col-xs-6 form-group">';
        strHtml = strHtml+'<label control-label input-sm noPadding">'+frmObj.name+' :</label>';
        strHtml = strHtml+'<div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 noPadding">';
        strHtml = strHtml+'<div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 noPadding">';
        strHtml = strHtml+'	<input id="'+frmId+'" type="text" class="form-control input-sm"></input>';
        strHtml = strHtml+'</div>';
        strHtml = strHtml+'<label class="control-label input-sm noPadding" style="float:right;">&nbsp;&nbsp;'+unit+'</label>';
        strHtml = strHtml+'</div></div>';
    }
    var btnId = "b"+currId;
    strHtml = strHtml+'<div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 form-group">';
    strHtml = strHtml+'<div class="navBar" style="padding-bottom:2px">';
    strHtml = strHtml+'</div>'
    //strHtml = strHtml+'<button id="'+btnId+'" class="btn-save" btnId="'+currId+'">Save</button></div>';

    $("#p"+currId).html(strHtml);
    $("#"+btnId).off("click");
    $("#"+btnId).on("click",onClickSave);
}

function onClickSave(evt){
    //var btnId = evt.currentTarget;
    //btnId = $(btnId).attr("btnId");
    //var fromArray = getFormDesignValues(btnId);
    //console.log(fromArray);
    var miniCompArray = [];
    for(var f=0;f<fromArray.length;f++){
        var fId = fromArray[f].idk;
        fId = "f"+fId;
        var fData = "";//$("#"+fId).val();
        var frmObj = fromArray[f];
        var fId = fromArray[f].idk;
        fId = "f"+fId;
        var fData = "";
        var cmpId = fromArray[f].miniComponentId;

        if(frmObj.miniComponentType == COMP_DATE_PICKER){
            if(frmObj.miniDataTypeValue == DT_DATE){
                var cmbDate = $("#"+fId).data("kendoDatePicker");
                fData = cmbDate.value();
            }else if(frmObj.miniDataTypeValue == DT_DATETIME){
                var cmbDate = $("#"+fId).data("kendoDateTimePicker");
                fData = cmbDate.value();
            }
        }else if(frmObj.miniComponentType == COMP_TEXT || frmObj.miniComponentType == COMP_TEXT_AREA){
            fData = $("#"+fId).val();
        }else if(frmObj.miniComponentType == COMP_CHECK_BOX || frmObj.miniComponentType == COMP_RADIO_BUTTON){
            if(frmObj.miniComponentType == COMP_CHECK_BOX){
                fData = $('input[name='+fId+']').val();
            }else if(frmObj.miniComponentType == COMP_RADIO_BUTTON){
                fData = $("input[type='radio'][name="+fId+"]:checked").val();
            }
        }
        else if(frmObj.miniComponentType == COMP_DROPDOWN){
            fData = $("#"+fId).val();
        }
        else if(frmObj.miniComponentType == COMP_EMAIL){
            fData = $("#"+fId).val();
        }
        else if(frmObj.miniComponentType == COMP_PHONE){
            fData = $("#"+fId).val();
        }
        else if(frmObj.componentType == COMP_PRICE || frmObj.componentType == COMP_NUMBER){
            fData = $("#"+fId).val();
        }
        else if(frmObj.miniComponentType == COMP_WEBSITE){
            fData = $("#"+fId).val();
        }
        else if(frmObj.miniComponentType == COMP_NAME){
            fData = $("#cff"+fId).val() + '~' + $("#cfm"+fId).val() +'~'+ $("#cfl"+fId).val();
        }
        else if(frmObj.miniComponentType == COMP_GROUP_CHECK_BOX){
            var delimiter = "~";
            fData = "";
            $.each($("input[name="+fId+"]:checked"), function(){
                fData +=$(this).val() + delimiter;
            });
            if('~' == fData[fData.length -1])
                fData = fData.slice(0, -1);
        }
        else if(frmObj.miniComponentType == COMP_DATETIME){
            var dateVal = Date.parse($("#"+fId).val());
            fData = dateVal;
        }
        else if(frmObj.miniComponentType == COMP_ADDRESS){
            fData = $("#cfadd1"+fId).val() + '~' + $("#cfadd2"+fId).val() +'~'+ $("#cfcity"+fId).val() + '~' + $("#cfstate"+fId).val() +'~'+ $("#cfcountry"+fId).val()+'~'+ $("#cmbZip"+fId).val();

        }
        else if(frmObj.miniComponentType == COMP_RATING){
            fData = $("#crating"+fId).find('.clicked').length;

        }
        if($("#fcol"+fromArray[f].idk).val()){
            fData = fData + '|' + $("#fcol"+fromArray[f].idk).val();
        }

        var obj = {};
        obj.miniComponentId = cmpId;
        obj.id = fromArray[f].idk;
        obj.miniTemplateId = fromArray[f].miniTemplateId;
        //obj.miniUnits = fromArray[f].miniUnits;
        //obj.miniComponentName = fromArray[f].miniComponentName;
        obj.createdBy = resultDataObj.response.templateValues[0].createdBy;
        obj.value = fData;
        obj.isDeleted = 0;
        obj.modifiedBy = parseInt(sessionStorage.userId);
        obj.isActive = 1;
        miniCompArray.push(obj);
    }
    var reqObj = {};
    reqObj.notes = $("#txtNotes").val();
    reqObj.patientId = sessionStorage.patientId;;
    reqObj.isActive = 1;
    reqObj.isDeleted = 0;
    reqObj.reportedOn = new Date().getTime();
    reqObj.modifiedBy = parseInt(sessionStorage.userId);
    reqObj.createdBy = resultDataObj.response.templateValues[0].createdBy;
    reqObj.providerId = null;
    reqObj.templateId = Number(selItem.idk);
    reqObj.componentValues = miniCompArray;
    reqObj.id = resultDataObj.response.templateValues[0].id;

    dataUrl = ipAddress +"/homecare/template-values/";
    var method  = "PUT";
    createAjaxObject(dataUrl, reqObj, method, onCreate, onError);
}
function onCreate(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "";
            msg = "Form Values are   updated  Successfully";
            displaySessionErrorPopUp("Info", msg, function(res) {
            })
        }else{
            customAlert.error("Error", dataObj.response.status.message);
        }
    }else{

    }
}
function getFormDesignValues(btnId){
    for(var a=0;a<tempFormDesignArray.length;a++){
        var aItem = tempFormDesignArray[a];
        if(aItem && aItem.idk == btnId){
            return aItem.values;
        }
    }
    return null;
}
function getFormData(){
    var selectedItems = angularUIgridWrapper1.getSelectedRows();
    console.log(selectedItems);
    var ipaddress = ipAddress+"/homecare/component-values/?is-active=1&is-deleted=0&miniTemplateId="+selectedItems[0].idk;
    getAjaxObject(ipaddress,"GET",handleGetFormDataValues,onError);
}
function handleGetFormDataValues(dataObj){
    console.log(dataObj);
}
function onError(errorObj){
    console.log(errorObj);
}

function buttonEvents(){
    /*$("#btnSave").off("click",onClickSave);
    $("#btnSave").on("click",onClickSave);

    $("#btnView").off("click",onClickView);
    $("#btnView").on("click",onClickView);

    $("#btnReport").off("click",onClickReport);
    $("#btnReport").on("click",onClickReport);


    $("#btnCancelDet").off("click",onClickCancel);
    $("#btnCancelDet").on("click",onClickCancel);


    $("#btnEdit").off("click",onClickEdit);
    $("#btnEdit").on("click",onClickEdit);

    $("#btnDelete").off("click",onClickDelete);
    $("#btnDelete").on("click",onClickDelete);*/
    $("#btnAdd").off("click");
    $("#btnAdd").on("click",onClickAdd);


    $("#btnSave").off("click",onClickSave);
    $("#btnSave").on("click",onClickSave);

    $("#btnZipSearch").off("click");
    $("#btnZipSearch").on("click",onClickZipSearch);

    $("#AddbtnZipSearch").off("click");
    $("#AddbtnZipSearch").on("click", onClickAdditionalZipSearch);
}

function resetData(){
    for(var f=0;f<frmDesignArray.length;f++){
        var fId = frmDesignArray[f].idk;
        fId = "f"+fId;
        $("#"+fId).val("");
    }
}
function adjustHeight(){
    var defHeight = 150;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    var frm = (cmpHeight)+"px";
    $("#divFormData").css({height:frm});
}


function getComboListIndex(cmbId, attr, attrVal) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb) {
        var ds = cmb.dataSource;
        var totalRec = ds.total();
        for (var i = 0; i < totalRec; i++) {
            var dtItem = ds.at(i);
            if (dtItem && dtItem[attr] == attrVal) {
                cmb.select(i);
                return i;
            }
        }
    }
    return -1;
}
function closeVideoScreen(evt,returnData){

}
var operation = "add";
var selFileResourceDataItem = null;
function onClickCancel(){
    var obj = {};
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}
function onClickOK(){
    setTimeout(function(){
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if(selectedItems && selectedItems.length>0){
            var obj = {};
            obj.selItem = selectedItems[0];
            selAccountItem = selectedItems[0];
            addFacility("update");
            // var onCloseData = new Object();
            /*obj.status = "success";
            obj.operation = "ok";
               var windowWrapper = new kendoWindowWrapper();
               windowWrapper.closePageWindow(obj);*/
        }
    })
}

function onClickZipSearch(){
    console.log($('#'+this.id).parent());
    console.log($('#'+this.id).parent().parent());
    console.log($('#'+this.id).parent().parent().parent());
    currZipId = $('#'+this.id)[0].id;
    currZipId = currZipId.replace("btnZipSearch","f");
    var popW = 600;
    var popH = 500;
    if(parentRef)
        parentRef.searchZip = true;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search Zip";
    var cntry = sessionStorage.countryName;
    if(cntry.indexOf("India")>=0){
        profileLbl = "Search Postal Code";
    }else if(cntry.indexOf("United Kingdom")>=0){
        profileLbl = "Search Postal Code";
    }else if(cntry.indexOf("United State")>=0){
        profileLbl = "Search Zip";
    }else{
        profileLbl = "Search Zip";
    }
    devModelWindowWrapper.openPageWindow("../../html/masters/zipList.html", profileLbl, popW, popH, true, onCloseSearchZipAction);
}
function onClickAdditionalZipSearch() {

    var popW = 600;
    var popH = 500;
    if(parentRef)
        parentRef.searchZip = true;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search Zip";
    devModelWindowWrapper.openPageWindow("../../html/masters/zipList.html", profileLbl, popW, popH, true, onCloseSearchAdditionalZipAction);
}
var zipSelItem = null;
var AddzipSelItem = null;

function onCloseSearchZipAction(evt,returnData){
    console.log(returnData);
    //console.log(frmId);
    if(returnData && returnData.status == "success"){
        //console.log();
        $("#cmbZip"+currZipId).val("");
        $("#txtZip4"+currZipId).val("");
        $("#cfstate"+currZipId).val("");
        $("#cfcountry"+currZipId).val("");
        $("#cfcity"+currZipId).val("");
        var selItem = returnData.selItem;
        if(selItem){
            zipSelItem = selItem;
            if(zipSelItem){
                cityId = zipSelItem.idk;
                // if(zipSelItem.zip){
                // 	$("#cmbZip"+currZipId).val(zipSelItem.zip);
                // }
                if(zipSelItem.zipFour){
                    $("#txtZip4"+currZipId).val(zipSelItem.zipFour);
                }
                if(zipSelItem.state){
                    $("#cfstate"+currZipId).val(zipSelItem.state);
                }
                if(zipSelItem.country){
                    $("#cfcountry"+currZipId).val(zipSelItem.country);
                }
                if(zipSelItem.city){
                    $("#cfcity"+currZipId).val(zipSelItem.city);
                }
            }
        }
    }
}
function onCloseSearchAdditionalZipAction(evt, returnData) {
    console.log(returnData);
    if (returnData && returnData.status == "success") {
        //console.log();
        $("#AddcmbZip").val("");
        $("#AddtxtZip4").val("");
        $("#AddtxtState").val("");
        $("#AddtxtCountry").val("");
        $("#AddtxtCity").val("");
        var selItem = returnData.selItem;
        if (selItem) {
            AddzipSelItem = selItem;
            if (AddzipSelItem) {
                cityId = AddzipSelItem.idk;
                //AddzipSelItem.cityId = cityId;
                if (AddzipSelItem.zip) {
                    $("#AddcmbZip").val(AddzipSelItem.zip);
                }
                if (AddzipSelItem.zipFour) {
                    $("#AddtxtZip4").val(AddzipSelItem.zipFour);
                }
                if (AddzipSelItem.state) {
                    $("#AddtxtState").val(AddzipSelItem.state);
                }
                if (AddzipSelItem.country) {
                    $("#AddtxtCountry").val(AddzipSelItem.country);
                }
                if (AddzipSelItem.city) {
                    $("#AddtxtCity").val(AddzipSelItem.city);
                }
            }
        }
    }
}
function getZip(){
    getAjaxObject(ipAddress+"/city/list?is-active=1","GET",getZipList,onError);
}
function onClickAdd(){
    /*var obj = {};
     obj.status = "Add";
     obj.operation = "ok";
        var windowWrapper = new kendoWindowWrapper();
        windowWrapper.closePageWindow(obj);*/
    addFacility("add")

}
function setRating(item,id){

    $('#crating'+id+ ' span').removeClass('clicked');
    $('#crating'+id+ ' span').each(
        function(index) {
            if(index<item)
                $(this).addClass('clicked');
        }
    );
}
$("#txtNotes").on('keyup', function(e) {
    if($(this).val().length > 0) {
        $("#saveMiniTemplate").removeAttr("disabled");
    }
    else {
        $("#saveMiniTemplate").attr("disabled","disabled");
    }
});
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
function isVarchar() {
    /*var str = "dr4";
    var patt = new RegExp("^[a-zA-Z0-9]+$");
    var res = patt.test(str);
    return res;*/
}
function isDecimal() {
    //$(this).val($(this).val().replace(/[^0-9\.]/g,''));
    if ((event.which != 46 || event.currentTarget.value.indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        event.preventDefault();
    }
}

function onGetPatientInfo(dataObj) {
    patientInfoObject = dataObj.response;
}

function dformat(time, format) {
    var t = new Date(time);
    var tf = function (i) { return (i < 10 ? '0' : '') + i };
    return format.replace(/yyyy|MM|dd|HH|mm|ss/g, function (a) {
        switch (a) {
            case 'yyyy':
                return tf(t.getFullYear());
                break;
            case 'MM':
                return tf(t.getMonth() + 1);
                break;
            case 'mm':
                return tf(t.getMinutes());
                break;
            case 'dd':
                return tf(t.getDate());
                break;
            case 'HH':
                return tf(t.getHours());
                break;
            case 'ss':
                return tf(t.getSeconds());
                break;
        }
    })
}

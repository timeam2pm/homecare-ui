var angularUIgridWrapper;
var angularUIgridWrapper1;
var parentRef = null;
var recordType = "1";
var dataArray = [];
var dataTypeArray = [{Key:'Text',Value:'1'},{Key:'Number',Value:'2'},{Key:'Boolean',Value:'3'}];
var unitArray =  [{Key:'Kg',Value:'1'},{Key:'Lt',Value:'2'}];
var statusArray =  [{Key:'ACTIVE',Value:'1'},{Key:'INACTIVE',Value:'2'}];
var pageName;

$(document).ready(function(){
    $("#pnlPatient",parent.document).css("display","none");
    sessionStorage.setItem("IsSearchPanel", "1");
    pageName = localStorage.getItem("pageName");
    themeAPIChange();
    if(pageName != "form") {
        parentRef = parent.frames['iframe'].window;
        $("#logout").css("display","none");
    }
    else{
        $("#logout").css("display","block");
    }
    //recordType = parentRef.dietType;
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("dgridDataDesignTemplateList", dataOptions);
    angularUIgridWrapper.init();

    var dataOptions1 = {
        pagination: false,
        changeCallBack: onChange1
    }
    angularUIgridWrapper1 = new AngularUIGridWrapper("dgridDataTemplateList", dataOptions1);
    angularUIgridWrapper1.init();

    buildDeviceListGrid([]);
    buildDeviceListGrid1([]);
});

$(window).load(function(){
    loading = false;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
    $("#btnEdit").prop("disabled", true);
    $("#btnDelete").prop("disabled", true);
    $("#btnReport").prop("disabled", true);
    $("#btnSave").prop("disabled", true);

    //$("#cmbTemplates").kendoComboBox();
    //$("#txtCompType").kendoComboBox();
    //$("#txtDT").kendoComboBox();
    //$("#cmbUnits").kendoComboBox();
    $("#cmbProvider").kendoComboBox();
    $("#cmbPatients").kendoComboBox();

    //setDataForSelection(dataTypeArray, "txtDT", function(){}, ["Key", "Value"], 0, "");
    //setDataForSelection(unitArray, "cmbUnits", function(){}, ["Key", "Value"], 0, "");
    //setDataForSelection(statusArray, "cmbStatus", function(){}, ["Key", "Value"], 0, "");

    //$("#txtChars").kendoNumericTextBox({step:1,min:1,max:255,decimals: 0,value:10,format: "0",change:onChangeMETime});
    //getTemplates();
    var ipaddress = ipAddress+"/homecare/templates/?is-active=1&is-deleted=0";
    getAjaxObject(ipaddress,"GET",handleGetTemplateList,onError);

    // var patientListURL = ipAddress+"/patient/list/"+sessionStorage.userId;
    var patientListURL = ipAddress+"/patient/list?is-active=1";
    // if(sessionStorage.userTypeId == "200" || sessionStorage.userTypeId == "100"){
    // 	patientListURL = ipAddress+"/patient/list?is-active=1";
    // }else{
    //
    // }
    getAjaxObject(patientListURL,"GET",onPatientListData,onError);

    //getComponents();
    //getDataTypes();
    //getUnits();
    //init();
    buttonEvents();
    adjustHeight();
}
function onPatientListData(dataObj){
    var tempArray = [];
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.patient){
            if($.isArray(dataObj.response.patient)){
                tempArray = dataObj.response.patient;
            }else{
                tempArray.push(dataObj.response.patient);
            }
        }
    }else{
        customAlert.error("Error",dataObj.response.status.message);
    }

    for(var i=0;i<tempArray.length;i++){
        var obj = tempArray[i];
        if(obj){
            var dataItemObj = {};
            dataItemObj.PID = obj.id;
            dataItemObj.FN = obj.firstName;
            dataItemObj.LN = obj.lastName;
            dataItemObj.WD = obj.weight;
            dataItemObj.HD = obj.height;
            if(obj.middleName){
                dataItemObj.MN = obj.middleName;
            }else{
                dataItemObj.MN =  "";
            }
            dataItemObj.GR = obj.gender;
            if(obj.status){
                dataItemObj.ST = obj.status;
            }else{
                dataItemObj.ST = "ACTIVE";
            }
            dataArray.push(dataItemObj);
        }
    }
    setDataForSelection(dataArray, "cmbPatients", function(){}, ["FN", "PID"], 0, "");
}
function getComponents(){
    getAjaxObject(ipAddress+"/master/type/list/?name=component","GET",getComponentTypes,onError);
}
function getDataTypes(){
    getAjaxObject(ipAddress+"/homecare/data-types/","GET",onGetDataTypes,onError);
}
function onGetDataTypes(dataObj){
    console.log(dataObj);
    var tempDataType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.dataTypes){
            if($.isArray(dataObj.response.dataTypes)){
                tempDataType = dataObj.response.dataTypes;
            }else{
                tempDataType.push(dataObj.response.dataTypes);
            }
        }
    }
    setDataForSelection(tempDataType, "txtDT", function(){}, ["value", "id"], 0, "");
}
function getUnits(){
    getAjaxObject(ipAddress+"/homecare/units/","GET",onGetUnits,onError);
}
function onGetUnits(dataObj){
    console.log(dataObj);
    var tempUnits = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.units){
            if($.isArray(dataObj.response.units)){
                tempUnits = dataObj.response.units;
            }else{
                tempUnits.push(dataObj.response.units);
            }
        }
    }
    setDataForSelection(tempUnits, "cmbUnits", function(){}, ["value", "id"], 0, "");
}
function getComponentTypes(dataObj){
    var tempCompType = [];
    compType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.typeMaster){
            if($.isArray(dataObj.response.typeMaster)){
                tempCompType = dataObj.response.typeMaster;
            }else{
                tempCompType.push(dataObj.response.typeMaster);
            }
        }
    }
    for(var i=0;i<tempCompType.length;i++){
        var obj = {};
        obj.Key = tempCompType[i].type;
        obj.Value = tempCompType[i].id;
        compType.push(obj);
    }
    setDataForSelection(compType, "txtCompType", function(){}, ["Key", "Value"], 0, "");
}
function onChangeMETime(event){
    onChangeNumericBox(event);
}
function onChangeNumericBox(event){
    if(event && event.sender && event.sender.element){
        var boxId = $(event.sender.element).attr("id");
        if(boxId){
            var txtSpBandBegin1 = $("#"+boxId).data("kendoNumericTextBox");
            if(txtSpBandBegin1){
                if(txtSpBandBegin1.value() == ""){
                    return;
                }
                if(txtSpBandBegin1 && txtSpBandBegin1.value() !=0  && !txtSpBandBegin1.value()){
                    var maxVal = txtSpBandBegin1.min();
                    //txtSpBandBegin1.value(maxVal);
                }
            }
        }
    }
}
function handleGetTemplateList(dataObj){
    console.log(dataObj);
    var vacation = [];
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            if(dataObj.response.templates){
                if($.isArray(dataObj.response.templates)){
                    vacation = dataObj.response.templates;
                }else{
                    vacation.push(dataObj.response.templates);
                }
            }
        }
    }
    for(var j=0;j<vacation.length;j++){
        vacation[j].idk = vacation[j].id;
    }
    //setDataForSelection(vacation, "cmbTemplates", onTemplateChange, ["name", "idk"], 0, "");
    //onTemplateChange();
    buildDeviceListGrid1(vacation);
}
function onTemplateChange(){
    // $("#divFormData").html("");
    buildDeviceListGrid1([])
    var cmbTemplates = $("#cmbTemplates").data("kendoComboBox");
    buildDeviceListGrid1([]);
    var selectedItems = cmbTemplates.dataItem();
    var miniTemplates = selectedItems.miniTemplates;
    var arr = [];
    if($.isArray(miniTemplates)){
        arr = miniTemplates;
    }else{
        arr.push(miniTemplates);
    }
    var miniArray = [];
    for(var x1=0;x1<miniTemplates.length;x1++){
        miniTemplates[x1].idk = miniTemplates[x1].id;
        var obj = {};
        obj.idk = miniTemplates[x1].id;
        obj.name = miniTemplates[x1].name;
        obj.notes = miniTemplates[x1].notes;
        miniArray.push(obj);
    }
    buildDeviceListGrid1(miniArray);
    onChange1();
}
/*function getTemplates(){
	var ipaddress = ipAddress+"/homecare/form-templates/";
	getAjaxObject(ipaddress,"GET",handleGetTemplateList,onError);
}*/
/*function handleGetTemplateList(dataObj){
	var vacation = [];
	if(dataObj && dataObj.response && dataObj.response.status ){
		if(dataObj.response.status.code == "1"){
			if(dataObj.response.formTemplates){
				if($.isArray(dataObj.response.formTemplates)){
					vacation = dataObj.response.formTemplates;
				}else{
					vacation.push(dataObj.response.formTemplates);
				}
			}
		}
	}
	for(var j=0;j<vacation.length;j++){
		vacation[j].idk = vacation[j].id;
	}
	setDataForSelection(vacation, "cmbTemplates", function(){}, ["name", "idk"], 0, "");
}*/
function init(){
    $("#btnEdit").prop("disabled", true);
    $("#btnDelete").prop("disabled", true);
    $("#btnReport").prop("disabled", true);
    $("#btnSave").prop("disabled", true);
    buildDeviceListGrid([]);
    //buildDeviceListGrid1([]);
    var selectedItems = angularUIgridWrapper1.getSelectedRows();
    console.log(selectedItems);
    $("#lblFrmData").text("");
    var fname = sessionStorage.pfname || '';
    $("#cmbServiceUser").val(fname);
    var selectPatientId =  sessionStorage.patientId || '';
    if(selectPatientId.length){
        $("#btnReport").prop("disabled", false);
        $("#btnSave").prop("disabled", false);
    }
    sessionStorage.patientId = selectPatientId;
    if(selectedItems && selectedItems.length>0){

        $("#btnServiceUser").off("click");
        $("#btnServiceUser").on("click",onClickServiceUser);

        $("#btnStaff").off("click");
        $("#btnStaff").on("click",onClickStaff);

        $("#lblFrmData").text(selectedItems[0].name);
        var idk = selectedItems[0].idk;
        var ipaddress = ipAddress+"/homecare/template-values/?templateId="+idk+"&patientId="+selectPatientId+"&fields=notes,patientId,templateId,CreatedDate,providerId";
        getAjaxObject(ipaddress,"GET",handleGetVacationList,onError);
    }

    //getAjaxObject(ipAddress+"/homecare/activity-types/?is-active=1&is-deleted=0&sort=display-order","GET",getActivityTypes,onError);
}
var frmDesignArray = [];
function handleGetVacationList(dataObj){
    console.log(dataObj);
    frmDesignArray = [];
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            if(dataObj.response.templateValues){
                if($.isArray(dataObj.response.templateValues)){
                    frmDesignArray = dataObj.response.templateValues;
                }else{
                    frmDesignArray.push(dataObj.response.templateValues);
                }
            }
        }
    }
    for(var i=0;i<frmDesignArray.length;i++){
        var dt = new Date(frmDesignArray[i].createdDate);
        dt = kendo.toString(dt, "dd-MM-yyyy h:mm:tt");
        frmDesignArray[i].crDate = dt;
        frmDesignArray[i].idk = frmDesignArray[i].id;
    }
    buildDeviceListGrid(frmDesignArray);
}
function getFormData(){
    var selectedItems = angularUIgridWrapper1.getSelectedRows();
    console.log(selectedItems);
    var ipaddress = ipAddress+"/homecare/component-values/?is-active=1&is-deleted=0&miniTemplateId="+selectedItems[0].idk;
    getAjaxObject(ipaddress,"GET",handleGetFormDataValues,onError);
}
function handleGetFormDataValues(dataObj){
    console.log(dataObj);
}
function onError(errorObj){
    console.log(errorObj);
}

function buttonEvents(){
    $("#btnSave").off("click",onClickSave);
    $("#btnSave").on("click",onClickSave);

    $("#btnView").off("click",onClickView);
    $("#btnView").on("click",onClickView);

    $("#btnReport").off("click",onClickReport);
    $("#btnReport").on("click",onClickReport);


    $("#btnCancelDet").off("click",onClickCancel);
    $("#btnCancelDet").on("click",onClickCancel);


    $("#btnEdit").off("click",onClickEdit);
    $("#btnEdit").on("click",onClickEdit);

    $("#btnDelete").off("click",onClickDelete);
    $("#btnDelete").on("click",onClickDelete);

    $("#logout").off("click", onClickLogout);
    $("#logout").on("click", onClickLogout);


}

function onClickLogout() {
    var e = sessionStorage.loginHisID;
    if (e) {
        var o = ipAddress + "/homecare/user-login-history/",
            t = JSON.parse(sessionStorage.loginHisObj);
        t.id = e, t.logoutTime = (new Date).getTime(), t.modifiedBy = sessionStorage.userId, createAjaxObject(o, t, "PUT", function(e) {
            console.log(e), sessionStorage.uName = "", top.location.href = "../../html/login/login.html"
        })
    } else sessionStorage.uName = "", top.location.href = "../../html/login/login.html"
}
function onClickReport(){
    /*var popW = "70%";
    var popH = "70%";

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Template Patient Report";

    devModelWindowWrapper.openPageWindow("../../html/reports/patientTemplateReport.html", profileLbl, popW, popH, true, closeAddPatientAction);*/
    var selectedItems = angularUIgridWrapper.getSelectedRows();
    console.log(selectedItems);
    if (selectedItems && selectedItems.length > 0) {
        var obj = selectedItems[0];

        var ipaddress = ipAddress+"/homecare/template-values/?templateId="+obj.templateId+"&patientId="+obj.patientId+"&createdDate="+obj.createdDate;//+"&fields=*,dataType.*,unit.*,component.*";
        getAjaxObject(ipaddress,"GET",handleGetReportFormValues,onError);
        /*operation = UPDATE;
        $("#txtCompName").val(obj.name);
        $("#txtNotes").val(obj.notes);
        getComboListIndex("txtCompType", "Value", obj.componentTypeId);
        getComboListIndex("txtDT", "Value", obj.dataTypeId);
        getComboListIndex("cmbUnits", "Value", obj.unitId);*/
    }
}
function closeAddPatientAction(evt,returnData){

}
function onClickDelete(){
    customAlert.confirm("Confirm", "Are you sure to delete?",function(response){
        if(response.button == "Yes"){
            var dataObj = {};
            dataObj.createdBy = Number(sessionStorage.userId);;
            dataObj.isDeleted = 1;
            dataObj.isActive = 0;
            var dataUrl = ipAddress +"/homecare/components/";
            var method = "DELETE";
            var selectedItems = angularUIgridWrapper.getSelectedRows();
            dataObj.id = selectedItems[0].idk;
            createAjaxObject(dataUrl, dataObj, method, onCreateDelete, onError);
        }
    });

}
function onCreateDelete(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "Template deleted successfully";
            customAlert.error("Info", msg);
            operation = ADD;
            init();
        }
    }
}
function onClickView(){
    var selGridData = angularUIgridWrapper1.getAllRows();
    var selList = [];
    var strIds  = "";
    for (var i = 0; i < selGridData.length; i++) {
        var dataRow = selGridData[i].entity;
        if(dataRow.SEL){
            strIds = strIds+dataRow.idx+",";
            selList.push(dataRow);
        }
    }
    console.log(selList);
    if(selList.length>0){
        strIds = strIds.substring(0,strIds.length-1);
        var ipaddress = ipAddress+"/homecare/components/?is-active=1&is-deleted=0&fields=*,component.*,dataType.*,unit.*&miniTemplateId=:in:"+strIds;
        getAjaxObject(ipaddress,"GET",handleGetMiniComponentsValues,onError);
    }
}
function handleGetMiniComponentsValues(dataObj){
    console.log(dataObj);
}
function onCloseVacation(evt,returnData){
    init();
}
function onClickSave(){

    var selectedItems = angularUIgridWrapper1.getSelectedRows();
    console.log(selectedItems);
    if(selectedItems && selectedItems.length>0){
        var popW = "80%";
        var popH = "85%";

        var profileLbl;
        var devModelWindowWrapper = new kendoWindowWrapper();
        profileLbl = "Add Form Data";
        if(pageName != "form") {
            parentRef.selItem = selectedItems[0];
        }
        else{
            localStorage.setItem("localFormData",JSON.stringify(selectedItems[0]));
        }

        devModelWindowWrapper.openPageWindow("../../html/forms/letterEntry.html", profileLbl, popW, popH, true, onCloseVacation);
    }

    /*try{
          var selectedItems = angularUIgridWrapper1.getSelectedRows();
          console.log(selectedItems);
          var miniCompArray = [];
        for(var f=0;f<frmDesignArray.length;f++){
            var fId = frmDesignArray[f].idk;
            fId = "f"+fId;
            var fData = $("#"+fId).val();
            var cmpId = frmDesignArray[f].idk;

            var obj = {};
            obj.miniComponentId = cmpId;
            obj.miniTemplateId = selectedItems[0].idk;
            obj.value = fData;
            obj.isDeleted = 0;
            obj.createdBy = 101;
            obj.isActive = 1;
            miniCompArray.push(obj);
        }
        var cmbTemplates = $("#cmbTemplates").data("kendoComboBox");
        var reqObj = {};
        reqObj.notes = "";
        reqObj.patientId = 101;
        reqObj.isActive = 1;
        reqObj.isDeleted = 0;
        reqObj.reportedOn = new Date().getTime();
        reqObj.createdBy = 101;
        reqObj.providerId = 101;
        reqObj.formTemplateId = Number(cmbTemplates.value());
        reqObj.miniComponentValues = miniCompArray;

            console.log(		reqObj);
            var method = "POST";
            if(operation == UPDATE){
                var selectedItems1 = angularUIgridWrapper.getSelectedRows();
                reqObj.id = selectedItems1[0].idk;
                method = "PUT";
            }
            dataUrl = ipAddress +"/homecare/form-template-values/";
            createAjaxObject(dataUrl, reqObj, method, onCreate, onError);
    }catch(ex){
        console.log(ex);
    }*/
}
function onCreate(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "";
            msg = "Form  Created  Successfully";
            if(operation == UPDATE){
                msg = "Form  Updated  Successfully";
            }
            displaySessionErrorPopUp("Info", msg, function(res) {
                resetData();
                operation = ADD;
                //onChange1();
                //init();
            })
        }else{
            customAlert.error("Error", dataObj.response.status.message);
        }
    }else{

    }
}

function resetData(){
    for(var f=0;f<frmDesignArray.length;f++){
        var fId = frmDesignArray[f].idk;
        fId = "f"+fId;
        $("#"+fId).val("");
    }
}
function adjustHeight(){
    var defHeight = 200;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    var frm = (cmpHeight-250)+"px";
    //$("#divFormData").css({height:frm});
    if(angularUIgridWrapper){
        angularUIgridWrapper.adjustGridHeight((cmpHeight));
    }
    if(angularUIgridWrapper1){
        angularUIgridWrapper1.adjustGridHeight((cmpHeight+120));
    }
}

function buildDeviceListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Date",
        "field": "crDate",
    });
    gridColumns.push({
        "title": "Notes",
        "field": "notes",
    });
    /*gridColumns.push({
        "title": "Print",
        "field": "print",
	});*/
    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    /*$("#btnReport").prop("disabled", false);
     $("#btnSave").prop("disabled", false);*/
    //onIndividualRowClick();
}
function buildDeviceListGrid1(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    /*gridColumns.push({
           "title": "Select",
           "field": "SEL",
           "cellTemplate": showCheckBoxTemplate(),
           "width":"15%"
       });*/
    gridColumns.push({
        "title": "Templates",
        "field": "name",
    });
    angularUIgridWrapper1.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}
function showCheckBoxTemplate(){
    var node = '<div style="text-align:center;padding-top: 6px;"><input type="checkbox" class="check1" id="chkbox" ng-model="row.entity.SEL" style="width:20px;height:20px;margin:0px;"   onclick="onSelect(event);"></input></div>';
    return node;
}
function onSelect(evt){
    console.log(evt);
}
var selRow = null;
function onClickAction(){

}

var prevSelectedItem =[];
function onChange(){
    setTimeout(function() {
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if (selectedItems && selectedItems.length > 0) {
            var obj = selectedItems[0];
            atID = obj.idk;
            $("#btnEdit").prop("disabled", false);
            $("#btnDelete").prop("disabled", false);
            $("#btnReport").prop("disabled", false);
            $("#btnSave").prop("disabled", false);
            //onClickEdit();
        } else {
            $("#btnEdit").prop("disabled", true);
            $("#btnDelete").prop("disabled", true);
            $("#btnReport").prop("disabled", true);
            $("#btnSave").prop("disabled", true);
            //resetData();
        }
    });
}
function onChange1(){
    setTimeout(function() {
        var selectedItems = angularUIgridWrapper1.getSelectedRows();
        console.log(selectedItems);
        //buildDeviceListGrid([]);
        if(selectedItems && selectedItems.length>0){
            $("#divEvents").removeClass("addEvents");
            /*var miniTemplates = selectedItems[0].miniTemplates;
            var arr = [];
            if($.isArray(miniTemplates)){
                arr = miniTemplates;
            }else{
                arr.push(miniTemplates);
            }
            for(var x1=0;x1<miniTemplates.length;x1++){
                miniTemplates[x1].idk = miniTemplates[x1].id;
            }
            buildDeviceListGrid(miniTemplates);*/
            delete sessionStorage.patientId;
            delete sessionStorage.pfname;
            delete sessionStorage.providerId;
            delete sessionStorage.providerData;
            delete sessionStorage.staffname;
            $("#cmbServiceUser").val('');
            $("#cmbStaff").val('');
            init();
        }else{
            $("#divEvents").addClass("addEvents");
        }
    });
}
var operation = "ADD";
var ADD = "ADD";
var UPDATE = "UPDATE";
var atID = "";
var ds  = "";
function onClickEdit(){
    var selectedItems = angularUIgridWrapper.getSelectedRows();
    console.log(selectedItems);
    if (selectedItems && selectedItems.length > 0) {
        var obj = selectedItems[0];

        var ipaddress = ipAddress+"/homecare/template-values/?templateId="+obj.templateId+"&patientId="+obj.patientId+"&createdDate="+obj.createdDate;//+"&fields=*,dataType.*,unit.*,component.*";
        getAjaxObject(ipaddress,"GET",handleGetFormValues,onError);
        /*operation = UPDATE;
        $("#txtCompName").val(obj.name);
        $("#txtNotes").val(obj.notes);
        getComboListIndex("txtCompType", "Value", obj.componentTypeId);
        getComboListIndex("txtDT", "Value", obj.dataTypeId);
        getComboListIndex("cmbUnits", "Value", obj.unitId);*/
    }
}

function handleGetFormValues(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var selectedItems1 = angularUIgridWrapper.getSelectedRows();
            console.log(selectedItems1);
            var selectedItems = angularUIgridWrapper1.getSelectedRows();
            console.log(selectedItems);
            if(selectedItems && selectedItems.length>0){
                var popW = "80%";
                var popH = "85%";

                var profileLbl;
                var devModelWindowWrapper = new kendoWindowWrapper();
                profileLbl = "Edit form data";
                if(pageName != "form") {
                    parentRef.selItem1 = selectedItems1[0];
                    parentRef.selItem = selectedItems[0];
                    parentRef.dataObj = dataObj;
                }
                else{
                    localStorage.setItem("localFormData",JSON.stringify(selectedItems[0]));
                    localStorage.setItem("selItem1",JSON.stringify(selectedItems1[0]));
                    localStorage.setItem("dataObj",JSON.stringify(dataObj));
                }


                devModelWindowWrapper.openPageWindow("../../html/forms/letterEdit.html", profileLbl, popW, popH, true, onCloseVacation);
            }
        }
    }
}
function handleGetReportFormValues(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var selectedItems1 = angularUIgridWrapper.getSelectedRows();
            console.log(selectedItems1);
            var selectedItems = angularUIgridWrapper1.getSelectedRows();
            console.log(selectedItems);
            if(selectedItems && selectedItems.length>0){
                var popW = "70%";
                var popH = "60%";

                var profileLbl;
                var devModelWindowWrapper = new kendoWindowWrapper();
                profileLbl = "Template Report";
                parentRef.selItem1 = selectedItems1[0];
                parentRef.selItem = selectedItems[0];
                parentRef.dataObj = dataObj;
                devModelWindowWrapper.openPageWindow("../../html/reports/patientTemplateReport.html", profileLbl, popW, popH, true, onCloseVacation);
            }
        }
    }
}
function getComboListIndex(cmbId, attr, attrVal) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb) {
        var ds = cmb.dataSource;
        var totalRec = ds.total();
        for (var i = 0; i < totalRec; i++) {
            var dtItem = ds.at(i);
            if (dtItem && dtItem[attr] == attrVal) {
                cmb.select(i);
                return i;
            }
        }
    }
    return -1;
}
function closeVideoScreen(evt,returnData){

}
var operation = "add";
var selFileResourceDataItem = null;
function onClickCancel(){
    var obj = {};
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}
function onClickServiceUser(){
    console.log($('#'+this.id).parent());
    console.log($('#'+this.id).parent().parent());
    console.log($('#'+this.id).parent().parent().parent()[0].id);
    currZipId = $('#'+this.id).parent().parent().parent()[0].id;
    var popW = 800;
    var popH = 450;

    if(parentRef)
        parentRef.searchZip = true;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Service User";

    devModelWindowWrapper.openPageWindow("../../html/reports/patientList.html", profileLbl, popW, popH, true, onCloseServiceUserAction);
}
function onClickStaff(){
    var popW = 800;
    var popH = 450;

    // parentRef.searchZip = true;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Staff";

    devModelWindowWrapper.openPageWindow("../../html/masters/staffList.html", profileLbl, popW, popH, true, onCloseStaffAction);
}
function onCloseServiceUserAction(evt,returnData){
    console.log(returnData);
    //console.log(frmId);
    if(returnData && returnData.status == "success"){
        $("#btnReport").prop("disabled", false);
        $("#btnSave").prop("disabled", false);
        var selectPatientId = returnData.selItem.PID;
        var selectedItems = angularUIgridWrapper1.getSelectedRows();
        console.log(selectedItems);
        $("#lblFrmData").text("");
        //var cmbPatients = $("#cmbPatients").data("kendoComboBox");
        //var selectPatientId = "";
        $('#cmbServiceUser').val(returnData.selItem.firstName+' '+returnData.selItem.lastName);
        sessionStorage.patientId = selectPatientId;
        sessionStorage.pfname = returnData.selItem.firstName+' '+returnData.selItem.lastName;
        if(selectedItems && selectedItems.length>0){
            $("#lblFrmData").text(selectedItems[0].name);
            var idk = selectedItems[0].idk;
            var ipaddress = ipAddress+"/homecare/template-values/?templateId="+idk+"&patientId="+selectPatientId+"&fields=notes,patientId,templateId,CreatedDate,providerId";
            getAjaxObject(ipaddress,"GET",handleGetVacationList,onError);
        }
    }
}
function onCloseStaffAction(evt,returnData){
    console.log(returnData);
    //console.log(frmId);
    if(returnData && returnData.status == "success"){
        $("#btnReport").prop("disabled", false);
        $("#btnSave").prop("disabled", false);
        var selectProviderId = returnData.selItem.PID;
        var selectedItems = angularUIgridWrapper1.getSelectedRows();
        console.log(selectedItems);
        $("#lblFrmData").text("");
        //var cmbPatients = $("#cmbPatients").data("kendoComboBox");
        //var selectPatientId = "";
        $('#cmbStaff').val(returnData.selItem.firstName+' '+returnData.selItem.lastName);
        sessionStorage.providerId = selectProviderId;
        sessionStorage.providerData = JSON.stringify(returnData.selItem);
        sessionStorage.staffname = returnData.selItem.firstName+' '+returnData.selItem.lastName;
        if(selectedItems && selectedItems.length>0){
            $("#lblFrmData").text(selectedItems[0].notes);
            var idk = selectedItems[0].idk;
            var ipaddress = ipAddress+"/homecare/template-values/?templateId="+idk+"&providerId="+selectProviderId+"&fields=notes,patientId,templateId,CreatedDate,providerId";
            getAjaxObject(ipaddress,"GET",handleGetVacationList,onError);
        }
    }
}

function themeAPIChange(){
    getAjaxObject(ipAddress+"/homecare/settings/?id=2","GET",getThemeValue,onError);
}

function getThemeValue(dataObj){
    console.log(dataObj);
    var tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.activityTypes){
            if($.isArray(dataObj.response.settings)){
                tempCompType = dataObj.response.settings;
            }else{
                tempCompType.push(dataObj.response.settings);
            }
        }
        if(tempCompType.length > 0){
            themesChange(tempCompType[0].value);
        }
    }
}

function themesChange(themeValue){
    if(themeValue == 2){
        loadAPi("../../Theme2/Theme02.css");
    }
    else if(themeValue == 3){
        loadAPi("../../Theme3/Theme03.css");
    }
}

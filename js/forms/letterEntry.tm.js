var parentRef = null;
var patientInfoObject = null;
var providerInfoObject = null;
var recordType = "1";
var dataArray = [];
var dataTypeArray = [{Key:'Text',Value:'1'},{Key:'Number',Value:'2'},{Key:'Boolean',Value:'3'}];
var unitArray =  [{Key:'Kg',Value:'1'},{Key:'Lt',Value:'2'}];
var statusArray =  [{Key:'ACTIVE',Value:'1'},{Key:'INACTIVE',Value:'2'}];
var selItem = null;
var miniTemplates;
var currZipId = "";
var addressComp = [];
var addedComponents = [];
var fromArray = [];
var DataTypes = [
    {
        type : "int",
        validation : "isNumber(event)"
    },
    {
        type : "varchar",
        validation : "isVarchar(event)"
    },
    {
        type : "decimal",
        validation : "isDecimal(event)"
    }
];
var pageName;
var IsPostalCodeManual = sessionStorage.IsPostalCodeManual;
$(document).ready(function(){
    if(parent.frames['iframe'])
        parentRef = parent.frames['iframe'].window;
    themeAPIChange();
    pageName = localStorage.getItem("pageName");

    var expireTime = Number(sessionStorage.pt);
    expireTime = expireTime-3000;
    // var val = ((o.getTime() - (expireTime))/6e4).toFixed(0);
    // if(val == 3){
    var isRun = 0;
    setInterval(function () {
        var o = new Date;
        var expireTime = Number(sessionStorage.pt);
        expireTime = expireTime-3000;
        var val = ((o.getTime() - (expireTime))/6e4).toFixed(0);
        console.log("form data"+val);
        if(val == "2" && isRun == 0) {
            isRun = 1;
            console.log("save"+val);
            onClickSaveApi();
        }
    },800);
    //recordType = parentRef.dietType;
});

$(window).load(function(){
    loading = false;
    var cntry = sessionStorage.countryName;
    if(cntry.indexOf("India")>=0){
        $(".AddbtnZipSearch").css("display","none");
        $(".btnZipSearch").css("display","");

    }else if(cntry.indexOf("United Kingdom")>=0){
        if(IsPostalCodeManual == "1") {
            $(".AddbtnZipSearch").css("display","");
            $(".btnZipSearch").css("display","none");
        }
        else{
            $(".AddbtnZipSearch").css("display","none");
            $(".btnZipSearch").css("display","");
        }

    }else if(cntry.indexOf("United State")>=0){
        $(".AddbtnZipSearch").css("display","none");
        $(".btnZipSearch").css("display","");
    }
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){

    if(pageName != "form") {
        if (parentRef)
            selItem = parentRef.selItem;
    }
    else{
        selItem = JSON.parse(localStorage.getItem("localFormData"));
    }
    if(selItem){
        $("#lblFrmData").text("Template - "+selItem.notes);
        buttonEvents();
        adjustHeight();
        if(selItem.miniTemplates){
            setFormData();
            onGetSUandStaff();
            // var ipaddress = ipAddress+"/homecare/components/?is-active=1&is-deleted=0&fields=*,dataType.*,unit.*,component.*";
            // getAjaxObject(ipaddress,"GET",handleGetVacationList,onError);
        }

    }
}
function init(){
    $("#btnEdit").prop("disabled", true);
    $("#btnDelete").prop("disabled", true);
    buildDeviceListGrid([]);
    //buildDeviceListGrid1([]);
    var selectedItems = angularUIgridWrapper1.getSelectedRows();
    console.log(selectedItems);
    $("#lblFrmData").text("");
    if(selectedItems && selectedItems.length>0){
        $("#lblFrmData").text(selectedItems[0].notes);
        var idk = selectedItems[0].idk;


    }


    //getAjaxObject(ipAddress+"/homecare/activity-types/?is-active=1&is-deleted=0&sort=display-order","GET",getActivityTypes,onError);
}
var frmDesignArray = [];
function handleGetVacationList(dataObj){
    console.log(dataObj);
    frmDesignArray = [];
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            if(dataObj.response.formTemplateValues){
                if($.isArray(dataObj.response.formTemplateValues)){
                    frmDesignArray = dataObj.response.formTemplateValues;
                }else{
                    frmDesignArray.push(dataObj.response.formTemplateValues);
                }
            }
        }
    }
    for(var i=0;i<frmDesignArray.length;i++){
        var dt = new Date(frmDesignArray[i].createdDate);
        dt = kendo.toString(dt, "MM-dd-yyyy h:mm:tt");
        frmDesignArray[i].crDate = dt;
    }
    buildDeviceListGrid(frmDesignArray);
    /*var strHtml = "";
    $("#divFormData").html("");
    for(var j=0;j<frmDesignArray.length;j++){
        var frmObj = frmDesignArray[j];
        frmObj.idk = frmObj.id;
        frmObj.length1 = frmObj.length;
        var frmId = "f"+frmObj.idk;
        var unit  ="";
        if(frmObj.unitValue){
            unit = frmObj.unitValue;
        }else{

        }
        strHtml = strHtml+'<div class="col-sm-6 col-md-6 col-lg-6 col-xs-6 form-group">';
        strHtml = strHtml+'<label class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">'+frmObj.name+' :</label>';
        strHtml = strHtml+'<div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 noPadding">';
        strHtml = strHtml+'<div class="col-sm-9 col-md-9 col-lg-9 col-xs-8 noPadding">';
        strHtml = strHtml+'	<input id="'+frmId+'" type="text" class="form-control "></input>';
        strHtml = strHtml+'</div>';
        strHtml = strHtml+'<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 ">&nbsp;&nbsp;'+unit+'</label>';
        strHtml = strHtml+'</div></div>';
    }
    $("#divFormData").html(strHtml);*/
    //getFormData();
    //buildDeviceListGrid(vacation);
}
function setFormData(){
    var strHtml = "";
    $("#divPanel").html("");
    miniTemplates = selItem.miniTemplates;
    for(var m=0;m<miniTemplates.length;m++){
        var mItem = miniTemplates[m];
        console.log(mItem);
        if(mItem && mItem.isDeleted == 0){
            var mItemHref = mItem.name;
            var mItemId = "m"+mItem.id;
            var pBodyItem = "p"+mItem.id;
            var strhref = "#mini"+mItem.id;
            strHtml = strHtml+'<div class="panel panel-default">';
            strHtml = strHtml+'<div class="panel-heading" style="background: #ddd !important;border-color: #ddd !important;">';
            strHtml = strHtml+'<a data-toggle="collapse" href="'+strhref+'" class="pnlTitle collapsed enable-tabs" id="'+mItemId+'" idk="'+mItem.id+'">'+mItem.name+'</a></div>';
            strHtml = strHtml+'<div id="mini'+mItem.id+'" class="panel-collapse collapse"><div class="panel-body" id="'+pBodyItem+'"></div></div></div>';
        }
    }
    $("#divPanel").html(strHtml);
    for(var n=0;n<miniTemplates.length;n++){
        var mItem = miniTemplates[n];
        if(mItem && mItem.isDeleted == 0){
            var pBodyItem = "p"+mItem.id;
            var mItemId = "m"+mItem.id;
            $("#"+mItemId).off("click");
            $("#"+mItemId).on("click",getAddedComponents);

            $("#"+mItemId).off("click");
            $("#"+mItemId).on("click",onClickItem);


        }
    }
}
var currId = "";
var rightside;
var providerImage;
var staffHTML;
//
// function onClickItem(evt){
//
//     var patientId = sessionStorage.patientId;
//     console.log(evt);
// 	currId = $(evt.currentTarget).attr("idk");
// 	 var idk = currId;
// 	 if(patientId && patientId.length>0){
// 	    getAjaxObject(ipAddress + "/patient/"+patientId+"/", "GET", function(dataObj) {
// 	        patientInfoObject = dataObj.response;
// 	        var providerId = sessionStorage.providerId;
// 	        //getAjaxObject(ipAddress + "/provider/"+providerId+"/", "GET", function(resp) {
// 	        if(sessionStorage.providerData)
// 	        	providerInfoObject = JSON.parse(sessionStorage.providerData);
//
// 		    		 if($("#p"+currId).children().length == 0){
// 			             var ipaddress = ipAddress+"/homecare/components/?is-active=1&is-deleted=0&miniTemplateId="+idk+"&fields=*,dataType.*,unit.*,component.*";
// 			             getAjaxObject(ipaddress,"GET",handleGetVacationList,onError);
// 			    	 }
// 	    		 }, onError);
//     }
// 	 else if(sessionStorage.providerId && sessionStorage.providerId.length>0)
// {
// var providerId = sessionStorage.providerId;
//    //getAjaxObject(ipAddress + "/provider/"+providerId+"/", "GET", function(resp) {
//    	providerInfoObject = JSON.parse(sessionStorage.providerData);
// 	 if($("#p"+currId).children().length == 0){
//         var ipaddress = ipAddress+"/homecare/components/?is-active=1&is-deleted=0&miniTemplateId="+idk+"&fields=*,dataType.*,unit.*,component.*";
//         getAjaxObject(ipaddress,"GET",handleGetVacationList,onError);
// 	 }
//   // }, onError);
// }
//
//
// }


function onGetSUandStaff(){

    var patientId = sessionStorage.patientId;
    // console.log(evt);
    // currId = $(evt.currentTarget).attr("idk");
    // var idk = currId;
    if(patientId && patientId.length>0){
        getAjaxObject(ipAddress + "/patient/"+patientId+"/", "GET", function(dataObj) {
            patientInfoObject = dataObj.response;
            var providerId = sessionStorage.providerId;
            //getAjaxObject(ipAddress + "/provider/"+providerId+"/", "GET", function(resp) {
            if(sessionStorage.providerData)
                providerInfoObject = JSON.parse(sessionStorage.providerData);

            // if($("#p"+currId).children().length == 0){
            //     var ipaddress = ipAddress+"/homecare/components/?is-active=1&is-deleted=0&miniTemplateId="+idk+"&fields=*,dataType.*,unit.*,component.*";
            //     getAjaxObject(ipaddress,"GET",handleGetVacationList,onError);
            // }
        }, onError);
    }
    else if(sessionStorage.providerId && sessionStorage.providerId.length>0)
    {
        var providerId = sessionStorage.providerId;
        //getAjaxObject(ipAddress + "/provider/"+providerId+"/", "GET", function(resp) {
        providerInfoObject = JSON.parse(sessionStorage.providerData);
        // if($("#p"+currId).children().length == 0){
        //     var ipaddress = ipAddress+"/homecare/components/?is-active=1&is-deleted=0&miniTemplateId="+idk+"&fields=*,dataType.*,unit.*,component.*";
        //     getAjaxObject(ipaddress,"GET",handleGetVacationList,onError);
        // }
        // }, onError);
    }


}

function onClickItem(evt) {
    debugger;
    console.log(evt);
    currId = $(evt.currentTarget).attr("idk");
    var curMiniTemplate = _.findWhere(selItem.miniTemplates,{id:parseInt(currId)});
    //getAddedComponents(currId);
    console.log(_.where(miniTemplates,{miniTemplateId:parseInt(currId)}));
    /*var idk = currId;
    if($("#p"+currId).children().length == 0){
        var ipaddress = ipAddress+"/homecare/components/?is-active=1&is-deleted=0&fields=*,component.*,dataType.*,unit.*&miniTemplateId="+idk;
           getAjaxObject(ipaddress,"GET",handleGetVacationList,onError);
    }*/
    var strHtml = "";
    var addComp ={};
    var miniTemplateById = _.where(miniTemplates,{miniTemplateId:parseInt(currId)});
    miniTemplateById = _.sortBy(miniTemplateById, 'miniComponentDisplayOrder');
    for(var m=0;m<miniTemplateById.length;m++){
        if((miniTemplateById[m].miniComponentType == COMP_SERVICEUSER && !patientInfoObject))
            m++;
        if(m==miniTemplateById.length)
            break;
        var mItem = miniTemplateById[m];
        if(mItem){
            // $("#lblFrmData").text(mItem.miniTemplateName);
            mItem.idk = mItem.id;
            var frmId = "f"+mItem.id;
            var mValue = mItem.value|| '';
            var fcolValue  = '';
            var mLabel;
            console.log(m);
            if(_.findWhere(curMiniTemplate.componenets,{id:mItem.componentId})) {
                mLabel = _.findWhere(curMiniTemplate.componenets, {id: mItem.componentId}).label;
            }
            if(mLabel == '3'){
                console.log(mItem.value);
                mValue = mItem.value.split('|')[0];
                fcolValue = mItem.value.split('|')[1];
            }
            var unit  ="";
            var hint  ="";
            var maxLength = "";
            mItem.frmId = frmId;
            var requiredSpan = mItem.isRequired && mItem.miniComponentType !='label' && mItem.isRequired ==1?'<span style="color:red" id="mandatory">*</span>' : '';
            if(mItem.miniUnits){
                unit = mItem.miniUnits;
            }else{

            }
            addComp = _.findWhere(addedComponents,{id:mItem.componentId})|| {};
            if(addComp && addComp.hint)
                hint = addComp.hint;
            if(addComp && addComp.length)
                maxLength = addComp.length;
            // strHtml = strHtml+'<div class="clearfix"></div><div class="popupformElements">';
            if((mItem.miniComponentType == COMP_SERVICEUSER)){
                strHtml = strHtml+'<div class="clearfix"></div><div class="col-xs-12 popupformElements serviceUseraddressForm"><div class="addformserviceleftdiv">';
            }

            else if(mItem.miniComponentType == COMP_ADDRESS){
                strHtml = strHtml+'<div class="clearfix"></div><div class="col-xs-12 popupformElements subinputlist">';
            }
            else{
                if (mItem.miniComponentType != COMP_STAFF) {
                    //strHtml = strHtml + '<div class="clearfix"></div><div class="popupformElements">';
                    strHtml = strHtml+'<div class="col-xs-12">';
                }
            }

            if(!(mItem.miniComponentType == COMP_HLINE || mItem.miniComponentType == "hline")){
                //var clsName = 'class="form-rightData-new"';
                //var clsLeftBlock = 'class="leftBlock-new"';
                var clsName = 'class="form-rightData"';
                var clsLeftBlock = 'class="leftBlock"';
                //if(curMiniTemplate && curMiniTemplate.label == 3)
                //{
                //    clsName = 'class="col3 form-rightData-new"';
                //    clsLeftBlock = 'class="col3 leftBlock-new"';
                //}
                //if(curMiniTemplate && curMiniTemplate.label == 4)
                //{
                //    clsName = 'class="col3 fourthele form-rightData-new"';
                //    clsLeftBlock = 'class="col3 fourthele leftBlock-new"';
                //}

                if(mItem.miniComponentType == COMP_LABEL){
                    var style = ' style="';
                    var notes = mItem.miniComponentNotes;
                    if(notes){
                        var notesArray = notes.split("~");
                        if(notesArray[0] && notesArray[0].toLowerCase() == 'bold')
                            style = style+'font-weight:bold !important;"';
                        else if(notesArray[0])
                            style = style+'font-style:'+notesArray[0]+' !important;"';
                    }

                    strHtml = strHtml+'<div><label>'+mItem.miniComponentName+' </label></div>';
                }
                else{
                    var img = '';

                    if(mItem.miniComponentType == COMP_SERVICEUSER && patientInfoObject){
                        var imageServletUrl = ipAddress + "/download/patient/photo/" + patientInfoObject.patient.id + "/?" + Math.round(Math.random() * 1000000) + "&access_token=" + sessionStorage.access_token + "&tenant=" + sessionStorage.tenant;
                        img = '<img src="'+imageServletUrl+'" class="p-img" />';

                        if(providerInfoObject) {
                            providerImage = ipAddress + "/homecare/download/providers/photo/?" + Math.round(Math.random() * 1000000) + "&access_token=" + sessionStorage.access_token + "&tenant=" + sessionStorage.tenant + "&id=" + providerInfoObject.PID;

                        }
                    }
                    // else if(mItem.miniComponentType == COMP_STAFF && providerInfoObject){
                    // 	var imageServletUrl = ipAddress + "/homecare/download/providers/photo/?" + Math.round(Math.random() * 1000000)+"&access_token="+sessionStorage.access_token+"&tenant="+sessionStorage.tenant+"&id=" + providerInfoObject.PID;
                    // 	img = '<img src="'+imageServletUrl+'" class="p-img" />';
                    // }
                    if(mItem.miniComponentName == "serviceuser"){
                        mItem.miniComponentName = "Service User Details";
                    }

                    if (mItem.miniComponentType != COMP_STAFF) {
                        if (mItem.miniComponentType == COMP_SERVICEUSER) {
                            strHtml = strHtml + '<div ' + clsLeftBlock + '><label class="">' + mItem.miniComponentName + ' :</label>' + requiredSpan + img + '</div>';
                            strHtml = strHtml + '<div ' + clsName + '>';
                            strHtml = strHtml + '<div class="rightblock generalInformblock">';
                        }
                        else {
                            strHtml = strHtml + '<div><label>' + mItem.miniComponentName + ' </label></div>';
                        }
                        //strHtml = strHtml + '<div ' + clsLeftBlock + '><label class="">' + mItem.miniComponentName + ' :</label>' + requiredSpan + img + '</div>';
                        //strHtml = strHtml + '<div ' + clsName + '>';
                        //strHtml = strHtml + '<div class="rightblock generalInformblock">';
                    }
                }
            }
            if(mItem.miniComponentType == COMP_TEXT){
                strHtml = strHtml+'	<input id="'+frmId+'" type="text" class="form-control input-sm" value="'+mValue+'"  onkeypress="return '+_.findWhere(DataTypes,{type:mItem.miniDataTypeValue.toLowerCase()}).validation+'" maxlength="'+maxLength+'"></input>';
            }
            else if(mItem.miniComponentType == COMP_NUMBER || mItem.miniComponentType == COMP_PRICE){
                strHtml = strHtml+'	<input id="'+frmId+'" type="number" class="form-control input-sm" value="'+mValue+'"  onkeypress="return '+_.findWhere(DataTypes,{type:mItem.miniDataTypeValue.toLowerCase()}).validation+'" max="'+mItem.maximumValue+'" min="'+mItem.minimumValue+'"></input>';
            }else if(mItem.miniComponentType == COMP_TEXT_AREA){
                strHtml = strHtml+'	<textarea id="'+frmId+'"  class="form-control input-sm" value="'+mValue+'" onkeypress="return '+_.findWhere(DataTypes,{type:mItem.miniDataTypeValue.toLowerCase()}).validation+'" maxlength="'+maxLength+'">'+mValue+'</textarea>';
            }else if(mItem.miniComponentType == COMP_DATE_PICKER){
                strHtml = strHtml+'	<input id="'+frmId+'" type="text" class="form-control input-sm noBorder"></input>';
            }
            else if(mItem.miniComponentType == COMP_LABEL){
                var style = '';
                var labelText = '';
                var notes = mItem.miniComponentNotes;
                if(notes){

                    var notesArray = notes.split("~");
                    var rightClass = notesArray[1] && notesArray[1].toLowerCase() == 'right'? '<div class="form-rightData-new"><div class="rightblock">' : '<div><div >'
                    //strHtml = strHtml+rightClass;
                    if(notesArray[2] && notesArray[2])
                        style = 'style="color:'+notesArray[2]+';"';
                    /*if(notesArray[0] && notesArray[0].toLowerCase() == 'bold')
                        style = style+'font-weight:bold;"';
                    else if(notesArray[0])
                        style = style+'font-style:'+notesArray[0]+';"';*/
                    if(notesArray[3])
                        labelText = notesArray[3];
                }
                strHtml = strHtml + '<div id="'+frmId+'">'+labelText+'</div>';
            }

            else if(mItem.miniComponentType == COMP_HLINE || mItem.miniComponentType == "hline"){
                strHtml = strHtml+'<hr id="'+frmId+'" />';
            }
            else if(mItem.miniComponentType == COMP_RADIO_BUTTON || mItem.miniComponentType == COMP_CHECK_BOX){
                var notes = mItem.miniComponentNotes;
                console.log(notes);
                if(notes){
                    var notesArray = notes.split("\n");
                    //strHtml = strHtml+'<div form-group" >';
                    for(var n=0;n<notesArray.length;n++){
                        var mItem1 = notesArray[n];
                        if(mItem1){
                            if(mItem.miniComponentType == COMP_RADIO_BUTTON){
                                if(mValue == mItem1){
                                    strHtml = strHtml+'<div class="radio lblcustom"><label class="">'+mItem1+'<input id="rd" type="radio" name="'+frmId+'"  value="'+mItem1+'" /><span class="radiomark"></span></label></div>';
                                }else{
                                    strHtml = strHtml+'<div class="radio lblcustom"><label class="">'+mItem1+'<input id="rd" type="radio" name="'+frmId+'"  value="'+mItem1+'"  /><span class="radiomark"></span></label></div>';
                                }

                            }else if(mItem.miniComponentType == COMP_CHECK_BOX){
                                if(mValue == mItem1){
                                    strHtml = strHtml+'<div class="checkbox lblcustom"><label class="">'+mItem1+'<input id="chk" type="checkbox" name="'+frmId+'"  value="'+mItem1+'"   /><span class="checkmark"></span></label></div>';
                                }else{
                                    strHtml = strHtml+'<div class="checkbox lblcustom"><label class="">'+mItem1+'<input id="chk" type="checkbox" name="'+frmId+'"  value="'+mItem1+'"  /><span class="checkmark"></span></label></div>';
                                }

                            }
                            //strHtml = strHtml+'<span id="lbl">'+mItem1+'</span>&nbsp;&nbsp;&nbsp;&nbsp;';
                        }
                    }
                    strHtml = strHtml+'</div></div>';
                }
            }
            else if(mItem.miniComponentType == COMP_DROPDOWN){
                var notes = mItem.miniComponentNotes;
                if(notes){

                    var notesArray = notes.split("\n");
                    strHtml = strHtml+'<select id="'+frmId+'" class="form-control"><option value=""> </option>';
                    for(var d=0;d<notesArray.length;d++){
                        var sItem = notesArray[d];
                        if(sItem){
                            var selected = '';
                            if(sItem == mItem.value)
                                selected = "selected";
                            strHtml = strHtml+'<option  value="'+sItem+'" '+selected+' >'+sItem+'</option>';

                            //strHtml = strHtml+'<span id="lbl">'+mItem+'</span>&nbsp;&nbsp;&nbsp;&nbsp;';
                        }
                    }
                    strHtml = strHtml+'</select>';

                }
            }
            else if(mItem.miniComponentType == COMP_EMAIL){
                strHtml = strHtml+'	<input id="'+frmId+'" type="email" class="form-control input-sm" value="'+mValue+'"></input>';
            }
            else if(mItem.miniComponentType == COMP_WEBSITE){
                strHtml = strHtml+'	<input id="'+frmId+'" type="url" class="form-control input-sm" value="'+mValue+'"></input>';
            }
            else if(mItem.miniComponentType == COMP_PHONE){
                strHtml = strHtml+'<input id="'+frmId+'" type="tel" class="form-control input-sm" value="'+mValue+'"  onkeypress="return '+_.findWhere(DataTypes,{type:mItem.miniDataTypeValue.toLowerCase()}).validation+'"></input>';
            }
            else if(mItem.miniComponentType == COMP_DATETIME){
                //var date = '\/Date('+mValue+')\/';
                var nowDate = "";
                var datetimeVal = "";
                if(mValue.length){
                    nowDate = new Date(parseInt(mValue));
                    datetimeVal = nowDate.format("yyyy-mm-dd'T'")+nowDate.format("HH:MM:ss");
                }

                strHtml = strHtml+'	<input id="'+frmId+'" type="datetime-local" class="form-control input-sm" value='+datetimeVal+'></input>';
            }
            else if(mItem.miniComponentType == COMP_NAME){
                var names = mValue.split('~');
                strHtml = strHtml+'<div class="namegroup"><input type="text" placeholder="First" class="form-control first" id="cff'
                    + frmId
                    + '"  value="'+names[0]+'" onkeypress="return '+_.findWhere(DataTypes,{type:mItem.miniDataTypeValue.toLowerCase()}).validation+'"/></div><div class="namegroup"><input type="text" placeholder="Middle" class="form-control middle" id="cfm'
                    + frmId + '" value="'+(names[1]|| '')+'" onkeypress="return '+_.findWhere(DataTypes,{type:mItem.miniDataTypeValue.toLowerCase()}).validation+'"/></div><div class="namegroup"><input type="text" placeholder="Last" class="form-control last" id="cfl'
                    + frmId + '"  value="'+(names[2]|| '')+'" onkeypress="return '+_.findWhere(DataTypes,{type:mItem.miniDataTypeValue.toLowerCase()}).validation+'"/></div>';
            }
            else if(mItem.miniComponentType == COMP_ADDRESS){
                var adds = mValue.split('~');
                strHtml = strHtml+'<div class="add-blocks"><input type="text" placeholder="Address1" class="add1 form-control" id="cfadd1'
                    + frmId
                    + '"  value="'+(adds[0] || '')+'"/></div><div class="add-blocks"><input type="text" placeholder="Address2" class="add2 form-control" id="cfadd2'
                    + frmId
                    + '"  value="'+(adds[1] || '')+'"/></div><div class="add-blocks"><div class="noPadding postalWrapper"><input type="text" placeholder="City" class="city form-control" readonly id="cfcity'
                    + frmId
                    + '"  value="'+(adds[2] || '')+'"/><img id="btnZipSearch'+mItem.id+'" class="postalWrapper-img" src="../../img/Default/search-icon.png" title="Search" /><input type="hidden" id="cityIdHiddenValue"></div></div><div class="add-blocks"><input type="text" class="state form-control" placeholder="State / County"  readonly id="cfstate'
                    + frmId
                    + '"  value="'+(adds[3] || '')+'"/></div><div class="add-blocks"><input type="text" placeholder="Country" class="postal form-control" readonly id="cfcountry'
                    + frmId
                    + '"  value="'+(adds[4] || '')+'"/></div><div class="add-blocks" id="'+frmId+'"><div class=""><div class="noPadding postalWrapper"><input placeholder="postal/zip code" id="cmbZip'+frmId+'" value="'+(adds[5] || '')+'" type="text" class="form-control input-sm" readonly /></div></div></div>';
                // + '"  value="'+(adds[4] || '')+'"/></div><div class="add-blocks" id="'+frmId+'"><div class=""><div class="noPadding postalWrapper"><input placeholder="postal/zip code" id="cmbZip'+frmId+'" value="'+(adds[5] || '')+'" type="text" class="form-control input-sm" readonly /><img id="btnZipSearch'+mItem.id+'" class="postalWrapper-img" src="../../img/Default/search-icon.png" title="Search" /><input type="hidden" id="cityIdHiddenValue"></div></div></div>';
            }
            else if(mItem.miniComponentType == COMP_SERVICEUSER){
                if(patientInfoObject){
                    var languageVal = patientInfoObject.patient.language;
                    if(languageVal) {
                        languageVal = languageVal.split(',');
                    }

                    // var firstRow = '<div class="col-xs-3"><input type="text" class="form-control" readonly value="'+patientInfoObject.patient.id +'" id="sid'+frmId+'" /><label>ID:</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+patientInfoObject.patient.firstName +'" id="first'+frmId+'" /><label>First</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+patientInfoObject.patient.middleName  +'" id="middle'+frmId+'" /><label>Middle</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+patientInfoObject.patient.lastName +'" id="last'+frmId+'" /><label>Last</label></div>';
                    // var secondRow = '<div class="col-xs-3"><input type="text" class="form-control input-sm" readonly value="'+dob +'" id="dob'+frmId+'" /><label>DOB:</label></div><div class="col-xs-3"><input type="text" readonly value="'+patientInfoObject.patient.gender +'"  class="form-control" id="gender">Gender</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+patientInfoObject.patient.maritalStatus +'" id="marritalstatus'+frmId+'" /><label>Marrital Status</label></div><div class="col-xs-3"><input type="text" value="'+patientInfoObject.patient.race +'" readonly class="form-control" id="race'+frmId+'" /><label>Race</label></div>';
                    // var thirdRow = '<div class="col-xs-3"><input type="text" readonly value="'+patientInfoObject.patient.ethnicity +'" class="form-control" id="ethnicity'+frmId+'" /><label>Ethnicity</label></div><div class="col-xs-3"><input type="text" readonly value="'+languageVal[0] +'" class="form-control" id="prelang'+frmId+'" /><label>Preferred Language</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+languageVal[1] +'" id="second'+frmId+'" /><label>Second</label></div><div class="col-xs-3"><input type="text" class="form-control" value="'+languageVal[2] +'" id="third'+frmId+'" readonly /><label>Third</label></div>';
                    // var fourthRow = '<div class="col-xs-3"><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.address1 +'" id="address1'+frmId+'" /><label>Address1</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.address2 +'" id="address2'+frmId+'" /><label>Address2</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.city +'" id="city'+frmId+'" /><label>City</label></div><div class="col-xs-3"><input type="text" value="'+patientInfoObject.communication.state +'" class="form-control" readonly id="state'+frmId+'" /><label>State/County</label></div>';
                    // var fifthRow = '<div class="col-xs-3"><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.country +'" id="country'+frmId+'" /><label>Country</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.zip +'" id="postal'+frmId+'" /><label>Postal/Zip Code</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.homePhone +'" id="homephone'+frmId+'" /><label>Home Phone</label></div><div class="col-xs-3"><input type="text" value="'+patientInfoObject.communication.cellPhone +'" readonly class="form-control" id="mobile'+frmId+'" /><label>Mobile</label></div>';
                    // var sixthRow = '<div class="col-xs-3"><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.workPhone +'" id="workphone'+frmId+'" /><label>Work Phone</label></div><div class="col-xs-3"><input type="text" readonly value="'+patientInfoObject.communication.email +'" class="form-control" id="eamil'+frmId+'" /><label>Email</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.sms +'"  id="sms'+frmId+'" /><label>SMS</label></div>';


                    // var firstRow = '<div class="col-xs-3"><span class="form-control" id="sid'+frmId+'">'+patientInfoObject.patient.id+'</span><label>ID:</label></div><div class="col-xs-3"><span class="form-control">patientInfoObject.patient.firstName </span><label>First</label></div><div class="col-xs-3"><span class="form-control" id="middle'+frmId+'">patientInfoObject.patient.middleName</span>><label>Middle</label></div><div class="col-xs-3"><span class="form-control" id="last'+frmId+'">'+patientInfoObject.patient.lastName+'</span><label>Last</label></div>';
                    // var secondRow = '<div class="col-xs-3"><span class="form-control input-sm" id="dob'+frmId+'">'+dob+'</span><label>DOB:</label></div><div class="col-xs-3"><span class="form-control" id="gender">'+patientInfoObject.patient.gender +'</span><label>Gender</label></div><div class="col-xs-3"><span class="form-control" id="marritalstatus'+frmId+'">'+patientInfoObject.patient.maritalStatus+'</span><label>Marrital Status</label></div><div class="col-xs-3"><span class="form-control" id="race'+frmId+'">'+patientInfoObject.patient.race +'</span><label>Race</label></div>';
                    // var thirdRow = '<div class="col-xs-3"><span class="form-control" id="ethnicity'+frmId+'">'+patientInfoObject.patient.ethnicity+'</span><label>Ethnicity</label></div><div class="col-xs-3"><span class="form-control" id="prelang'+frmId+'">'+languageVal[0] +'</span><label>Preferred Language</label></div><div class="col-xs-3"><span class="form-control" id="second'+frmId+'">'+languageVal[1] +'</span><label>Second</label></div><div class="col-xs-3"><span class="form-control" id="third'+frmId+'">'+languageVal[2] +'</span><label>Third</label></div>';
                    // var fourthRow = '<div class="col-xs-3"><span class="form-control" id="address1'+frmId+'">'+patientInfoObject.communication.address1 +'</span><label>Address1</label></div><div class="col-xs-3"><span class="form-control" id="address2'+frmId+'">'+patientInfoObject.communication.address2 +'</span><label>Address2</label></div><div class="col-xs-3"><span class="form-control" id="city'+frmId+'">'+patientInfoObject.communication.city+' </span><label>City</label></div><div class="col-xs-3"><span class="form-control" id="state'+frmId+'">'+patientInfoObject.communication.state +'</span><label>State/County</label></div>';
                    // var fifthRow = '<div class="col-xs-3"><span class="form-control" id="country'+frmId+'">'+patientInfoObject.communication.country+'</span><label>Country</label></div><div class="col-xs-3"><span class="form-control" id="postal'+frmId+'">'+patientInfoObject.communication.zip+'</span><label>Postal/Zip Code</label></div><div class="col-xs-3"><span class="form-control" id="homephone'+frmId+'">'+patientInfoObject.communication.homePhone+'</span><label>Home Phone</label></div><div class="col-xs-3"><span value="'+patientInfoObject.communication.cellPhone +'" class="form-control" id="mobile'+frmId+'">'+patientInfoObject.communication.cellPhone+'</span><label>Mobile</label></div>';
                    // var sixthRow = '<div class="col-xs-3"><span class="form-control" id="workphone'+frmId+'">'+patientInfoObject.communication.workPhone +'</span><label>Work Phone</label></div><div class="col-xs-3"><span type="text" class="form-control" id="eamil'+frmId+'">'+patientInfoObject.communication.email+'</span><label>Email</label></div><div class="col-xs-3"><span class="form-control" id="sms'+frmId+'">'+patientInfoObject.communication.sms+'</span><label>SMS</label></div>';

                    // var firstRow = '<div class="col-xs-3"><span class="form-control" id="last'+frmId+'">'+patientInfoObject.patient.lastName+'</span>&nbsp;<label>Last</label></div><div class="col-xs-3"><span class="form-control">'+' '+patientInfoObject.patient.firstName+' </span>&nbsp;<label>First</label></div><div class="col-xs-3"><span class="form-control" id="middle'+frmId+'">'+' '+patientInfoObject.patient.middleName+'</span>&nbsp;<label>Middle</label></div><br/>';
                    // var secondRow = '<div class="col-xs-3"></div><div class="col-xs-3"><span class="form-control input-sm" id="dob'+frmId+'">'+dob+'</span>&nbsp;<label>DOB:</label></div><div class="col-xs-3"><span class="form-control" id="gender">'+patientInfoObject.patient.gender +'</span><label style="display:none">Gender</label></div><br/>';
                    //
                    // var fourthRow = '<div class="col-xs-3"></div><div class="col-xs-3"><span class="form-control" id="address1'+frmId+'">'+patientInfoObject.communication.address1 +'</span>&nbsp;<label>Address1</label></div><div class="col-xs-3"><span class="form-control" id="address2'+frmId+'">'+patientInfoObject.communication.address2 +'</span>&nbsp;<label>Address2</label></div><div class="col-xs-3"><span class="form-control" id="city'+frmId+'">'+patientInfoObject.communication.city+' </span>&nbsp;<label>City</label></div></br><div class="col-xs-3"><span class="form-control" id="state'+frmId+'">'+patientInfoObject.communication.state +'</span>&nbsp;<label>State/County</label></div>';
                    // var fifthRow = '<div class="col-xs-3"><span class="form-control" id="country'+frmId+'">'+patientInfoObject.communication.country+'</span><label>Country</label></div></br><div class="col-xs-3"><span class="form-control" id="homephone'+frmId+'">'+homePhone+'</span><label>Home Phone</label></div></br><div class="col-xs-3"><span class="form-control" id="mobile'+frmId+'">'+cellPhone+'</span><label>Cell Phone</label></div>';

                    // var ele = firstRow + secondRow+ fourthRow + fifthRow;
                    // strHtml = strHtml + ele;
                    var cellPhone = "";
                    if(patientInfoObject.communication.cellPhone != null){
                        cellPhone = "<b>Cell Phone </b>" + patientInfoObject.communication.cellPhone;
                    }

                    var homePhone = "";
                    if(patientInfoObject.communication.homePhone != null){
                        homePhone = "<b>Home Phone </b>" + patientInfoObject.communication.homePhone;
                    }

                    var dob = dformat(new Date(patientInfoObject.patient.dateOfBirth).getTime(), 'dd-MM-yyyy');

                    // var firstRow = '<div class="col-xs-3"><label>ID:</label><input type="text" class="form-control" readonly value="'+patientInfoObject.patient.id +'" id="sid'+frmId+'" /></div><div class="col-xs-3"><label>First</label><input type="text" class="form-control" readonly value="'+patientInfoObject.patient.firstName +'" id="first'+frmId+'" /></div><div class="col-xs-3"><label>Middle</label><input type="text" class="form-control" readonly value="'+patientInfoObject.patient.middleName  +'" id="middle'+frmId+'" /></div><div class="col-xs-3"><label>Last</label><input type="text" class="form-control" readonly value="'+patientInfoObject.patient.lastName +'" id="last'+frmId+'" /></div>';
                    // var secondRow = '<div class="col-xs-3"><label>DOB:</label><input type="text" class="form-control " readonly value="'+dob +'" id="dob'+frmId+'" /></div><div class="col-xs-3"><label>Gender</label><input type="text" readonly value="'+patientInfoObject.patient.gender +'"  class="form-control" id="gender"></div><div class="col-xs-3"><label>Marrital Status</label><input type="text" class="form-control" readonly value="'+patientInfoObject.patient.maritalStatus +'" id="marritalstatus'+frmId+'" /></div><div class="col-xs-3"><label>Race</label><input type="text" value="'+patientInfoObject.patient.race +'" readonly class="form-control" id="race'+frmId+'" /></div>';
                    // var thirdRow = '<div class="col-xs-3"><label>Ethnicity</label><input type="text" readonly value="'+patientInfoObject.patient.ethnicity +'" class="form-control" id="ethnicity'+frmId+'" /></div><div class="col-xs-3"><label>Preferred Language</label><input type="text" readonly value="'+languageVal[0] +'" class="form-control" id="prelang'+frmId+'" /></div><div class="col-xs-3"><label>Second</label><input type="text" class="form-control" readonly value="'+languageVal[1] +'" id="second'+frmId+'" /></div><div class="col-xs-3"><label>Third</label><input type="text" class="form-control" value="'+languageVal[2] +'" id="third'+frmId+'" readonly /></div>';
                    // var fourthRow = '<div class="col-xs-3"><label>Address1</label><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.address1 +'" id="address1'+frmId+'" /></div><div class="col-xs-3"><label>Address2</label><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.address2 +'" id="address2'+frmId+'" /></div><div class="col-xs-3"><label>City</label><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.city +'" id="city'+frmId+'" /></div><div class="col-xs-3"><label>State/County</label><input type="text" value="'+patientInfoObject.communication.state +'" class="form-control" readonly id="state'+frmId+'" /></div>';
                    // var fifthRow = '<div class="col-xs-3"><label>Country</label><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.country +'" id="country'+frmId+'" /></div><div class="col-xs-3"><label>Postal/Zip Code</label><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.zip +'" id="postal'+frmId+'" /></div><div class="col-xs-3"><label>Home Phone</label><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.homePhone +'" id="homephone'+frmId+'" /></div><div class="col-xs-3"><label>Mobile</label><input type="text" value="'+patientInfoObject.communication.cellPhone +'" readonly class="form-control" id="mobile'+frmId+'" /></div>';
                    // var sixthRow = '<div class="col-xs-3"><label>Work Phone</label><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.workPhone +'" id="workphone'+frmId+'" /></div><div class="col-xs-3"><label>Email</label><input type="text" readonly value="'+patientInfoObject.communication.email +'" class="form-control" id="eamil'+frmId+'" /></div><div class="col-xs-3"><label>SMS</label><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.sms +'"  id="sms'+frmId+'" /></div>';
                    var firstRow = '<div class="col-xs-3"><span class="form-control" id="last'+frmId+'">'+patientInfoObject.patient.lastName+'</span>&nbsp;<label>Last</label></div><div class="col-xs-3"><span class="form-control">'+' '+patientInfoObject.patient.firstName+' </span>&nbsp;<label>First</label></div><div class="col-xs-3"><span class="form-control" id="middle'+frmId+'">'+' '+patientInfoObject.patient.middleName+'</span>&nbsp;<label>Middle</label></div><br/>';
                    var secondRow = '<div class="col-xs-3"></div><div class="col-xs-3"><span class="form-control input-sm" id="dob'+frmId+'">'+dob+'</span>&nbsp;<label>DOB:</label></div><div class="col-xs-3"><span class="form-control" id="gender">'+patientInfoObject.patient.gender +'</span><label style="display:none">Gender</label></div><br clear="all"/>';

                    var fourthRow = '<div class="col-xs-3"></div><div class="col-xs-3"><span class="form-control" id="address1'+frmId+'">'+patientInfoObject.communication.address1 +'</span>&nbsp;<label>Address1</label></div><div class="col-xs-3"><span class="form-control" id="address2'+frmId+'">'+patientInfoObject.communication.address2 +'</span>&nbsp;<label>Address2</label></div><div class="col-xs-3"><span class="form-control" id="city'+frmId+'">'+patientInfoObject.communication.city+' </span>&nbsp;<label>City</label></div></br><div class="col-xs-3"><span class="form-control" id="state'+frmId+'">'+patientInfoObject.communication.state +'</span>&nbsp;<label>State/County</label></div>';
                    var fifthRow = '<div class="col-xs-3"><span class="form-control" id="country'+frmId+'">'+patientInfoObject.communication.country+'</span><label>Country</label></div></br><div class="col-xs-3"><span class="form-control" id="homephone'+frmId+'">'+homePhone+'</span><label>Home Phone</label></div></br><div class="col-xs-3"><span class="form-control" id="mobile'+frmId+'">'+cellPhone+'</span><label>Cell Phone</label></div>';
                    // var sixthRow = '<div class="col-xs-3"><span class="form-control" id="workphone'+frmId+'">'+patientInfoObject.communication.workPhone +'</span><label>Work Phone</label></div><div class="col-xs-3"><span type="text" class="form-control" id="eamil'+frmId+'">'+patientInfoObject.communication.email+'</span><label>Email</label></div><div class="col-xs-3"><span class="form-control" id="sms'+frmId+'">'+patientInfoObject.communication.sms+'</span><label>SMS</label></div>';
                    var ele = firstRow + secondRow+ fourthRow + fifthRow + '</div>';
                    strHtml = strHtml + ele ;
                }

                if(providerInfoObject){
                    var languageVal = providerInfoObject.language;
                    if(languageVal) {
                        languageVal = languageVal.split(',');
                    }

                    var cellPhone = "";
                    if(providerInfoObject.communications[0].cellPhone != undefined && providerInfoObject.communications[0].cellPhone != null && providerInfoObject.communications[0].cellPhone != ''){
                        cellPhone = "<b>Cell Phone </b>" + providerInfoObject.communications[0].cellPhone;
                    }

                    var homePhone = "";
                    if(providerInfoObject.communications[0].homePhone != null){
                        homePhone = "<b>Home Phone </b>" + providerInfoObject.communications[0].homePhone;
                    }


                    var dob = dformat(new Date(providerInfoObject.dateOfBirth).getTime(), 'dd-MM-yyyy');

                    // var firstRow = '<div class="col-xs-3"><div class="col-xs-6"><input type="text" class="form-control" readonly value="'+providerInfoObject.PID +'" id="sid'+frmId+'"/><label>ID:</label></div><div class="col-xs-6" style="padding-right:0;"><input type="text" class="form-control" readonly value="'+providerInfoObject.prefix +'" id="sid'+frmId+'" /><label>Prefix:</label></div></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.firstName +'" id="first'+frmId+'" /><label>First</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.lastName +'" id="last'+frmId+'" /><label>Last</label></div>';
                    // var secondRow = '<div class="col-xs-3"><input type="text" class="form-control " readonly value="'+dob +'" id="dob'+frmId+'" /><label>DOB:</label></div><div class="col-xs-3"><input type="text" readonly value="'+providerInfoObject.gender +'"  class="form-control" id="gender">Gender</label></div>';
                    // var thirdRow = '<div class="col-xs-3"><input type="text" readonly value="'+languageVal[0] +'" class="form-control" id="prelang'+frmId+'" /><label>Preferred Language</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+(languageVal[1] || '') +'" id="second'+frmId+'" /><label>Second</label></div><div class="col-xs-3"><input type="text" class="form-control" value="'+(languageVal[2] || '') +'" id="third'+frmId+'" readonly /><label>Third</label></div>';
                    // var fourthRow = '<div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.communications[0].address1 +'" id="address1'+frmId+'" /><label>Address1</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.communications[0].address2 +'" id="address2'+frmId+'" /><label>Address2</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.communications[0].city +'" id="city'+frmId+'" /><label>City</label></div><div class="col-xs-3"><input type="text" value="'+providerInfoObject.communications[0].state +'" class="form-control" readonly id="state'+frmId+'" /><label>State/County</label></div>';
                    // var fifthRow = '<div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.communications[0].country +'" id="country'+frmId+'" /><label>Country</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.communications[0].zip +'" id="postal'+frmId+'" /><label>Postal/Zip Code</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.communications[0].homePhone +'" id="homephone'+frmId+'" /><label>Home Phone</label></div><div class="col-xs-3"><input type="text" value="'+providerInfoObject.communications[0].cellPhone +'" readonly class="form-control" id="mobile'+frmId+'" /><label>Mobile</label></div>';
                    // var sixthRow = '<div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.communications[0].workPhone +'" id="workphone'+frmId+'" /><label>Work Phone</label></div><div class="col-xs-3"><input type="text" readonly value="'+providerInfoObject.communications[0].email +'" class="form-control" id="eamil'+frmId+'" /><label>Email</label></div>';
                    // 	var firstRow = '<div class="col-xs-3"><div class="col-xs-6"><span class="form-control" id="sid'+frmId+'">'+providerInfoObject.PID+'</span><label>ID:</label></div><div class="col-xs-6" style="padding-right:0;"><span class="form-control"  id="sid'+frmId+'" >providerInfoObject.prefix </span><label>Prefix:</label></div></div><div class="col-xs-3"><span class="form-control" id="first'+frmId+'">'+providerInfoObject.firstName+'</span>><label>First</label></div><div class="col-xs-3"><span class="form-control" id="last'+frmId+'">'+providerInfoObject.lastName+'</span><label>Last</label></div>';
                    // 	var secondRow = '<div class="col-xs-3"><span class="form-control input-sm" id="dob'+frmId+'">'+dob+'</span><label>DOB:</label></div><div class="col-xs-3"><span class="form-control" id="gender">'+providerInfoObject.gender +'</span><label>Gender</label></div>';
                    //
                    // 	var thirdRow = '<div class="col-xs-3"><span class="form-control" id="prelang'+frmId+'">'+languageVal[0] +'</span><label>Preferred Language</label></div><div class="col-xs-3"><span class="form-control" id="second'+frmId+'">'+(languageVal[1] || '')+'</span><label>Second</label></div><div class="col-xs-3"><span class="form-control" id="third'+frmId+'">'+(languageVal[2] || '') +'</span><label>Third</label></div>';
                    // var fourthRow = '<div class="col-xs-3"><span class="form-control" id="address1'+frmId+'">'+providerInfoObject.communication.address1 +'</span><label>Address1</label></div><div class="col-xs-3"><span class="form-control" id="address2'+frmId+'">'+providerInfoObject.communication.address2 +'</span><label>Address2</label></div><div class="col-xs-3"><span class="form-control" id="city'+frmId+'">'+providerInfoObject.communication.city+' </span><label>City</label></div><div class="col-xs-3"><span class="form-control" id="state'+frmId+'">'+providerInfoObject.communication.state +'</span><label>State/County</label></div>';
                    // var fifthRow = '<div class="col-xs-3"><span class="form-control" id="country'+frmId+'">'+providerInfoObject.communication.country+'</span><label>Country</label></div><div class="col-xs-3"><span class="form-control" id="postal'+frmId+'">'+providerInfoObject.communication.zip+'</span><label>Postal/Zip Code</label></div><div class="col-xs-3"><span class="form-control" id="homephone'+frmId+'">'+providerInfoObject.communication.homePhone+'</span><label>Home Phone</label></div><div class="col-xs-3"><span value="'+providerInfoObject.communication.cellPhone +'" class="form-control" id="mobile'+frmId+'">'+providerInfoObject.communication.cellPhone+'</span><label>Mobile</label></div>';
                    // var sixthRow = '<div class="col-xs-3"><span class="form-control" id="workphone'+frmId+'">'+providerInfoObject.communication.workPhone +'</span><label>Work Phone</label></div><div class="col-xs-3"><span type="text" class="form-control" id="eamil'+frmId+'">'+providerInfoObject.communication.email+'</span><label>Email</label></div><div class="col-xs-3"><span class="form-control" id="sms'+frmId+'">'+providerInfoObject.communication.sms+'</span><label>SMS</label></div>';

                    var firstRow = '<div class="col-xs-3"><span class="form-control" id="last'+frmId+'">'+providerInfoObject.lastName+'</span>&nbsp;<label>Last</label></div><div class="col-xs-3"><span class="form-control">'+' '+providerInfoObject.firstName+' </span>&nbsp;<label>First</label></div><div class="col-xs-3"><span class="form-control" id="middle'+frmId+'">'+' '+providerInfoObject.middleName+'</span>&nbsp;<label>Middle</label></div><br/>';
                    var secondRow = '<div class="col-xs-3"></div><div class="col-xs-3"><span class="form-control input-sm" id="dob'+frmId+'">'+dob+'</span>&nbsp;<label>DOB:</label></div><div class="col-xs-3"><span class="form-control" id="gender">'+providerInfoObject.gender +'</span><label style="display:none">Gender</label></div><br clear="all"/>';

                    var fourthRow = '<div class="col-xs-3"></div><div class="col-xs-3"><span class="form-control" id="address1'+frmId+'">'+providerInfoObject.communications[0].address1 +'</span>&nbsp;<label>Address1</label></div><div class="col-xs-3"><span class="form-control" id="address2'+frmId+'">'+providerInfoObject.communications[0].address2 +'</span>&nbsp;<label>Address2</label></div><div class="col-xs-3"><span class="form-control" id="city'+frmId+'">'+providerInfoObject.communications[0].city+' </span>&nbsp;<label>City</label></div></br><div class="col-xs-3"><span class="form-control" id="state'+frmId+'">'+providerInfoObject.communications[0].state +'</span>&nbsp;<label>State/County</label></div>';
                    var fifthRow = '<div class="col-xs-3"><span class="form-control" id="country'+frmId+'">'+providerInfoObject.communications[0].country+'</span><label>Country</label></div></br><div class="col-xs-3"><span class="form-control" id="homephone'+frmId+'">'+homePhone+'</span><label>Home Phone</label></div></br><div class="col-xs-3"><span class="form-control" id="mobile'+frmId+'">'+cellPhone+'</span><label>Cell Phone</label></div>';

                    var ele = firstRow + secondRow+ fourthRow + fifthRow;
                    //strHtml = strHtml + ele;
                    staffHTML = ele;
                }


            }

            else if(mItem.miniComponentType == COMP_LIKERT){
                strHtml = strHtml+'	<input id="'+frmId+'" type="datetime-local" class="form-control input-sm"></input>';
            }
            else if(mItem.miniComponentType == COMP_RATING){
                strHtml = strHtml+'<div class="customfields crating maindiv" id="crating'
                    + frmId
                    + '">';
                var ratingVal = '';
                for(var i=0;i<5;i++)
                {
                    var clicked = '';
                    if(i<parseInt(mValue))
                        clicked = 'clicked';
                    ratingVal += '<span class="star '+clicked+'" value="1" tabindex="1" onclick="setRating('+(i+1)+',\''+ frmId+'\')"><i class="fa fa-star fa-2x" aria-hidden="true"></i></span>';
                }
                strHtml = strHtml+ ratingVal +'</div>';
            }

            //strHtml = strHtml+'</div>';
            if(mItem.miniComponentType == COMP_HLINE || mItem.miniComponentType == "hline"){
                strHtml = strHtml+'</div>';
            }
            else{
                if (mItem.componentType != COMP_STAFF) {
                    strHtml = strHtml + '<label class="units" >' + unit + '</label>';
                    if (hint && hint.length > 0)
                        strHtml = strHtml + '<label class="hint">' + hint + '</label>';
                    var col3 = '';
                    var col3 = '';
                    //if (curMiniTemplate && curMiniTemplate.label == 3) {
                    //    if(fcolValue == "undefined"){
                    //        fcolValue = "";
                    //    }
                    //    if(mLabel == '3') {
                    //        var cinput = '<input type="text" class="form-control input-sm" id="fcol' + mItem.id + '"  value="' + (fcolValue || '') + '" style="width:100%;"/>';
                    //        col3 = '<div class="col3 thirdDiv">' + (mLabel == '3' ? cinput : '') + '</div>';
                    //    }
                    //}
                    //else if (curMiniTemplate && curMiniTemplate.label == 4) {
                    //    if(fcolValue == "undefined"){
                    //        fcolValue = "";
                    //    }
                    //    if(mLabel == '4') {
                    //        var cinput3 = '<input type="text" class="form-control input-sm" id="fcol' + mItem.id + '"  value="' + (fcolValue || '') + '" style="width:100%;"/>';
                    //        var cinput4 = '<input type="text" class="form-control input-sm" id="fcol1' + mItem.id + '"  value="' + (fcolValue || '') + '" style="width:100%;"/>';
                    //        col3 = '<div class="col3 thirdDiv fourthEle">' + (mLabel == '4' ? cinput3 : '') + '</div><div class="col3 fourthEle">' + (mLabel == '4' ? cinput4 : '') + '</div>';
                    //    }
                    //}
                }
                // strHtml = strHtml+'</div></div>'+col3+'</div>';
                if(mItem.miniComponentType == COMP_SERVICEUSER){

                    if(!providerInfoObject){

                        var dt = new Date();
                        dt = kendo.toString(dt, "dd-MM-yyyy h:mm:tt");

                        var fullDate = dt;

                        rightside = '<div class="col-xs-12"><div><div class="leftBlock-new">' +
                            '<label class="col-heading">Date Time When Filled Form :</label>' +
                            '<span class="" id="rDateTime">'+fullDate+'</span>' +
                            '</div></div></div>';
                    }
                    else{
                        // rightside = '<div class="col-xs-12 popupformElements"><div><div class="leftBlock-new"><label class="">staff :</label><img src="https://stage.timeam.com/homecare/download/providers/photo/?571838&amp;access_token=c161d936-70ab-4aa4-ba18-482089a79a69&amp;tenant=homecare1&amp;id=13" class="p-img"></div><div class="form-rightData-new"><div class="rightblock generalInformblock"><div class="col-xs-3"><div class="col-xs-6"><span class="form-control" id="sidf1042">13</span><label>ID:</label></div><div class="col-xs-6" style="padding-right:0;"><span class="form-control" id="sidf1042">providerInfoObject.prefix </span><label>Prefix:</label></div></div><div class="col-xs-3"><span class="form-control" id="firstf1042">Provider</span>&gt;<label>First</label></div><div class="col-xs-3"><span class="form-control" id="lastf1042">Provider</span><label>Last</label></div><div class="col-xs-3"><span class="form-control input-sm" id="dobf1042">12/26/1965</span><label>DOB:</label></div><div class="col-xs-3"><span class="form-control" id="gender">Female</span><label>Gender</label></div><div class="col-xs-3"><span class="form-control" id="prelangf1042">English</span><label>Preferred Language</label></div><div class="col-xs-3"><span class="form-control" id="secondf1042"> Spanish</span><label>Second</label></div><div class="col-xs-3"><span class="form-control" id="thirdf1042"> Arabic</span><label>Third</label></div><label class="units"></label></div></div><div class="addformservicerightdiv">fdf</div></div></div>';
                        // rightside = '<div class="col-xs-12 popupformElements"><div><div class="leftBlock-new"><label class="">staff :</label><img src='+providerImage+' class="p-img"></div><div class="form-rightData-new"><div class="rightblock generalInformblock"><div class="col-xs-3"><div class="col-xs-6"><span class="form-control" id="sidf1042">13</span><label>ID:</label></div><div class="col-xs-6" style="padding-right:0;"><span class="form-control" id="sidf1042">providerInfoObject.prefix </span><label>Prefix:</label></div></div><div class="col-xs-3"><span class="form-control" id="firstf1042">Provider</span>&gt;<label>First</label></div><div class="col-xs-3"><span class="form-control" id="lastf1042">Provider</span><label>Last</label></div><div class="col-xs-3"><span class="form-control input-sm" id="dobf1042">12/26/1965</span><label>DOB:</label></div><div class="col-xs-3"><span class="form-control" id="gender">Female</span><label>Gender</label></div><div class="col-xs-3"><span class="form-control" id="prelangf1042">English</span><label>Preferred Language</label></div><div class="col-xs-3"><span class="form-control" id="secondf1042"> Spanish</span><label>Second</label></div><div class="col-xs-3"><span class="form-control" id="thirdf1042"> Arabic</span><label>Third</label></div></div></div></div></div>';
                        // rightside = '<div class="col-xs-12 popupformElements"><div><div class="leftBlock-new"><label class="">staff :</label><img src='+providerImage+' class="p-img"></div><div class="form-rightData-new"><div class="rightblock generalInformblock"><div class="col-xs-3"><div class="col-xs-6"><span class="form-control" id="sidf1042">13</span><label>ID:</label></div><div class="col-xs-6" style="padding-right:0;"><span class="form-control" id="sidf1042">providerInfoObject.prefix </span><label>Prefix:</label></div></div><div class="col-xs-3"><span class="form-control" id="firstf1042">Provider</span>&gt;<label>First</label></div><div class="col-xs-3"><span class="form-control" id="lastf1042">Provider</span><label>Last</label></div><div class="col-xs-3"><span class="form-control input-sm" id="dobf1042">12/26/1965</span><label>DOB:</label></div><div class="col-xs-3"><span class="form-control" id="gender">Female</span><label>Gender</label></div><div class="col-xs-3"><span class="form-control" id="prelangf1042">English</span><label>Preferred Language</label></div><div class="col-xs-3"><span class="form-control" id="secondf1042"> Spanish</span><label>Second</label></div><div class="col-xs-3"><span class="form-control" id="thirdf1042"> Arabic</span><label>Third</label></div></div></div></div></div>';
                        rightside = '<div class="col-xs-12 popupformElements noBorder"><div><div class="leftBlock-new"><label class="col-heading">Staff Details :</label><img src='+ providerImage +' class="p-img"></div><div class="form-rightData-new"><div class="rightblock generalInformblock">'+staffHTML+'</div></div></div></div>';
                    }
                    strHtml = strHtml+'</div></div>'+col3+'<div class="addformserviceleftdiv">'+rightside+'</div></div>';
                }
                else{
                    // if(frmObj.componentType != COMP_STAFF) {
                    //     strHtml = strHtml + '</div></div>' + col3 + '</div>';
                    // }
                    strHtml = strHtml + col3 + '</div></div></div>';
                }

            }
            fromArray.push(mItem);
        }
    }
    for(var i=0;i<miniTemplateById.length;i++){
        if(miniTemplateById[i].miniComponentType == 'address')
            addressComp.push(miniTemplateById[i]);
    }
    //strHtml = strHtml+'<div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 form-group">';
    //strHtml = strHtml+'<div class="navBar" style="padding-bottom:2px">';
    strHtml = strHtml+'</div>'
    //strHtml = strHtml+'<button id="btn-save" class="btn-save">Save</button></div>';

    $("#p"+currId).html(strHtml);
    $("#saveMiniTemplate").off("click");
    $("#saveMiniTemplate").on("click",onClickSave);
    //$("#"+btnId).off("click");
    //$("#"+btnId).on("click",onClickSave);

    setTimeout(function(){
        for(var k=0;k<miniTemplateById.length;k++){
            var mItem = miniTemplateById[k];
            if(mItem){
                mItem.idk = mItem.id;
                var frmId = "f"+mItem.id;
                var mValue = '';
                if(mItem.value != undefined) {
                    var mValue = mItem.value;
                }
                var unit  ="";
                mItem.frmId = frmId;
                if(mItem.miniComponentType == COMP_DATE_PICKER){
                    if(mItem.miniDataTypeValue == DT_DATE){
                        $("#"+frmId).kendoDatePicker({format: "dd-MM-yyyy", value:new Date(mValue)});
                        var cDate = $("#"+frmId).data("kendoDatePicker");
                        if(cDate){
                            cDate.value(new Date(mValue));

                        }
                    }else if(mItem.miniDataTypeValue == DT_DATETIME){
                        $("#"+frmId).kendoDateTimePicker({value:new Date(mValue)});
                        var cDate = $("#"+frmId).data("kendoDateTimePicker");
                        if(cDate){
                            cDate.value(new Date(mValue));
                        }
                    }
                }
            }
        }
    },100);
    $("#btnAdd").off("click");
    $("#btnAdd").on("click",onClickAdd);

    if(addressComp.length){
        for(var i=0;i<addressComp.length;i++){
            //if(addressComp[i].componentTypeId == 12)
            $("#btnZipSearch"+addressComp[i].id).off("click");
            $("#btnZipSearch"+addressComp[i].id).on("click",onClickZipSearch);
            //addressComp.push(addressComp[i].id);
        }
    }
    /*$("#btnSave").off("click",onClickSave);
    $("#btnSave").on("click",onClickSave);*/

    /*$("#btnZipSearch").off("click");
    $("#btnZipSearch").on("click",onClickZipSearch);*/

    $("#AddbtnZipSearch").off("click");
    $("#AddbtnZipSearch").on("click", onClickAdditionalZipSearch);
}

function onClickItem2(evt){
    console.log(evt);
    currId = $(evt.currentTarget).attr("idk");
    var curMiniTemplate = _.findWhere(selItem.miniTemplates,{id:parseInt(currId)});
    //getAddedComponents(currId);
    console.log(_.where(miniTemplates,{miniTemplateId:parseInt(currId)}));
    /*var idk = currId;
    if($("#p"+currId).children().length == 0){
        var ipaddress = ipAddress+"/homecare/components/?is-active=1&is-deleted=0&fields=*,component.*,dataType.*,unit.*&miniTemplateId="+idk;
           getAjaxObject(ipaddress,"GET",handleGetVacationList,onError);
    }*/
    var strHtml = "";
    var addComp ={};
    var miniTemplateById = _.where(miniTemplates,{miniTemplateId:parseInt(currId)});
    miniTemplateById = _.sortBy(miniTemplateById, 'miniComponentDisplayOrder');
    for(var m=0;m<miniTemplateById.length;m++){
        if((miniTemplateById[m].miniComponentType == COMP_SERVICEUSER && !patientInfoObject))
            m++;
        if(m==miniTemplateById.length)
            break;
        var mItem = miniTemplateById[m];
        if(mItem){
            // $("#lblFrmData").text(mItem.miniTemplateName);
            mItem.idk = mItem.id;
            var frmId = "f"+mItem.id;
            var mValue = mItem.value|| '';
            var fcolValue  = '';
            var mLabel;
            console.log(m);
            if(_.findWhere(curMiniTemplate.componenets,{id:mItem.componentId})) {
                mLabel = _.findWhere(curMiniTemplate.componenets, {id: mItem.componentId}).label;
            }
            if(mLabel == '3'){
                console.log(mItem.value);
                mValue = mItem.value.split('|')[0];
                fcolValue = mItem.value.split('|')[1];
            }
            var unit  ="";
            var hint  ="";
            var maxLength = "";
            mItem.frmId = frmId;
            var requiredSpan = mItem.isRequired && mItem.miniComponentType !='label' && mItem.isRequired ==1?'<span style="color:red" id="mandatory">*</span>' : '';
            if(mItem.miniUnits){
                unit = mItem.miniUnits;
            }else{

            }
            addComp = _.findWhere(addedComponents,{id:mItem.componentId})|| {};
            if(addComp && addComp.hint)
                hint = addComp.hint;
            if(addComp && addComp.length)
                maxLength = addComp.length;
            // strHtml = strHtml+'<div class="clearfix"></div><div class="popupformElements">';
            if((mItem.miniComponentType == COMP_SERVICEUSER)){
                strHtml = strHtml+'<div class="clearfix"></div><div class="col-xs-12 popupformElements serviceUseraddressForm"><div class="addformserviceleftdiv">';
            }

            else if(mItem.miniComponentType == COMP_ADDRESS){
                strHtml = strHtml+'<div class="clearfix"></div><div class="col-xs-12 popupformElements subinputlist">';
            }
            else{
                if (mItem.miniComponentType != COMP_STAFF) {
                    strHtml = strHtml+'<div class="clearfix"></div><div class="popupformElements">';
                }
            }

            if(!(mItem.miniComponentType == COMP_HLINE) && (mItem.miniComponentType != 'horizontal line')) {
                var clsName = 'class="form-rightData"';
                var clsLeftBlock = 'class="leftBlock"';
                if(curMiniTemplate && curMiniTemplate.label == 3)
                {
                    clsName = 'class="col3 form-rightData"';
                    clsLeftBlock = 'class="col3 leftBlock"';
                }

                if(curMiniTemplate && curMiniTemplate.label == 4)
                {
                    clsName = 'class="col3 fourthEle form-rightData"';
                    clsLeftBlock = 'class="col3 fourthEle leftBlock"';
                }


                if(mItem.miniComponentType == COMP_LABEL){
                    var style = ' style="';
                    var notes = mItem.miniComponentNotes;
                    if(notes){
                        var notesArray = notes.split("~");
                        if(notesArray[0] && notesArray[0].toLowerCase() == 'bold')
                            style = style+'font-weight:bold !important;"';
                        else if(notesArray[0])
                            style = style+'font-style:'+notesArray[0]+' !important;"';
                    }
                    strHtml = strHtml+'<div '+clsLeftBlock+'><label '+style+'>'+mItem.miniComponentName+' </label>'+requiredSpan+'</div>';
                }
                else{
                    var img = '';

                    if(mItem.miniComponentType == COMP_SERVICEUSER && patientInfoObject){
                        var imageServletUrl = ipAddress + "/download/patient/photo/" + patientInfoObject.patient.id + "/?" + Math.round(Math.random() * 1000000) + "&access_token=" + sessionStorage.access_token + "&tenant=" + sessionStorage.tenant;
                        img = '<img src="'+imageServletUrl+'" class="p-img" />';

                        if(providerInfoObject) {
                            providerImage = ipAddress + "/homecare/download/providers/photo/?" + Math.round(Math.random() * 1000000) + "&access_token=" + sessionStorage.access_token + "&tenant=" + sessionStorage.tenant + "&id=" + providerInfoObject.PID;

                        }
                    }
                    // else if(mItem.miniComponentType == COMP_STAFF && providerInfoObject){
                    // 	var imageServletUrl = ipAddress + "/homecare/download/providers/photo/?" + Math.round(Math.random() * 1000000)+"&access_token="+sessionStorage.access_token+"&tenant="+sessionStorage.tenant+"&id=" + providerInfoObject.PID;
                    // 	img = '<img src="'+imageServletUrl+'" class="p-img" />';
                    // }
                    if(mItem.miniComponentName == "serviceuser"){
                        mItem.miniComponentName = "Service User Details";
                    }

                    if (mItem.miniComponentType != COMP_STAFF) {
                        strHtml = strHtml + '<div ' + clsLeftBlock + '><label class="">' + mItem.miniComponentName + ' :</label>' + requiredSpan + img + '</div>';
                        strHtml = strHtml + '<div ' + clsName + '>';
                        strHtml = strHtml + '<div class="rightblock generalInformblock">';
                    }
                }
            }
            if(mItem.miniComponentType == COMP_TEXT){
                strHtml = strHtml+'	<input id="'+frmId+'" type="text" class="form-control input-sm" value="'+mValue+'"  onkeypress="return '+_.findWhere(DataTypes,{type:mItem.miniDataTypeValue.toLowerCase()}).validation+'" maxlength="'+maxLength+'"></input>';
            }
            else if(mItem.miniComponentType == COMP_NUMBER || mItem.miniComponentType == COMP_PRICE){
                strHtml = strHtml+'	<input id="'+frmId+'" type="number" class="form-control input-sm" value="'+mValue+'"  onkeypress="return '+_.findWhere(DataTypes,{type:mItem.miniDataTypeValue.toLowerCase()}).validation+'" max="'+mItem.maximumValue+'" min="'+mItem.minimumValue+'"></input>';
            }else if(mItem.miniComponentType == COMP_TEXT_AREA){
                strHtml = strHtml+'	<textarea id="'+frmId+'"  class="form-control input-sm" value="'+mValue+'" onkeypress="return '+_.findWhere(DataTypes,{type:mItem.miniDataTypeValue.toLowerCase()}).validation+'" maxlength="'+maxLength+'">'+mValue+'</textarea>';
            }else if(mItem.miniComponentType == COMP_DATE_PICKER){
                strHtml = strHtml+'	<input id="'+frmId+'" type="text" class="form-control input-sm noBorder"></input>';
            }
            else if(mItem.miniComponentType == COMP_LABEL){
                var style = '';
                var labelText = '';
                var notes = mItem.miniComponentNotes;
                if(notes){

                    var notesArray = notes.split("~");
                    var rightClass = notesArray[1] && notesArray[1].toLowerCase() == 'right'? '<div class="form-rightData"><div class="rightblock">' : '<div><div >'
                    strHtml = strHtml+rightClass;
                    if(notesArray[2] && notesArray[2])
                        style = 'style="color:'+notesArray[2]+';"';
                    /*if(notesArray[0] && notesArray[0].toLowerCase() == 'bold')
                        style = style+'font-weight:bold;"';
                    else if(notesArray[0])
                        style = style+'font-style:'+notesArray[0]+';"';*/
                    if(notesArray[3])
                        labelText = notesArray[3];
                }
                strHtml = strHtml + '<span id="'+frmId+'" class="labelFiled" '+style+'>'+labelText+'</span>';
            }

            else if(mItem.miniComponentType == COMP_HLINE || mItem.miniComponentType == "hline"){
                strHtml = strHtml+'<hr id="'+frmId+'" />';
            }
            else if(mItem.miniComponentType == COMP_RADIO_BUTTON || mItem.miniComponentType == COMP_CHECK_BOX){
                var notes = mItem.miniComponentNotes;
                console.log(notes);
                if(notes){
                    var notesArray = notes.split("\n");
                    //strHtml = strHtml+'<div form-group" >';
                    for(var n=0;n<notesArray.length;n++){
                        var mItem1 = notesArray[n];
                        if(mItem1){
                            if(mItem.miniComponentType == COMP_RADIO_BUTTON){
                                if(mValue == mItem1){
                                    strHtml = strHtml+'<div class="radio lblcustom"><label class="">'+mItem1+'<input id="rd" type="radio" name="'+frmId+'"  value="'+mItem1+'" checked  /><span class="radiomark"></span></label></div>';
                                }else{
                                    strHtml = strHtml+'<div class="radio lblcustom"><label class="">'+mItem1+'<input id="rd" type="radio" name="'+frmId+'"  value="'+mItem1+'"  /><span class="radiomark"></span></label></div>';
                                }

                            }else if(mItem.miniComponentType == COMP_CHECK_BOX){
                                if(mValue == mItem1){
                                    strHtml = strHtml+'<div class="checkbox lblcustom"><label class="">'+mItem1+'<input id="chk" type="checkbox" name="'+frmId+'"  value="'+mItem1+'" checked  /><span class="checkmark"></span></label></div>';
                                }else{
                                    strHtml = strHtml+'<div class="checkbox lblcustom"><label class="">'+mItem1+'<input id="chk" type="checkbox" name="'+frmId+'"  value="'+mItem1+'"  /><span class="checkmark"></span></label></div>';
                                }

                            }
                            //strHtml = strHtml+'<span id="lbl">'+mItem1+'</span>&nbsp;&nbsp;&nbsp;&nbsp;';
                        }
                    }
                    strHtml = strHtml+'</div></div>';
                }
            }
            else if(mItem.miniComponentType == COMP_DROPDOWN){
                var notes = mItem.miniComponentNotes;
                if(notes){

                    var notesArray = notes.split("\n");
                    strHtml = strHtml+'<select id="'+frmId+'" class="form-control"><option value=""> </option>';
                    for(var d=0;d<notesArray.length;d++){
                        var sItem = notesArray[d];
                        if(sItem){
                            var selected = '';
                            if(sItem == mItem.value)
                                selected = "selected";
                            strHtml = strHtml+'<option  value="'+sItem+'" '+selected+' >'+sItem+'</option>';

                            //strHtml = strHtml+'<span id="lbl">'+mItem+'</span>&nbsp;&nbsp;&nbsp;&nbsp;';
                        }
                    }
                    strHtml = strHtml+'</select>';

                }
            }
            else if(mItem.miniComponentType == COMP_EMAIL){
                strHtml = strHtml+'	<input id="'+frmId+'" type="email" class="form-control input-sm" value="'+mValue+'"></input>';
            }
            else if(mItem.miniComponentType == COMP_WEBSITE){
                strHtml = strHtml+'	<input id="'+frmId+'" type="url" class="form-control input-sm" value="'+mValue+'"></input>';
            }
            else if(mItem.miniComponentType == COMP_PHONE){
                strHtml = strHtml+'<input id="'+frmId+'" type="tel" class="form-control input-sm" value="'+mValue+'"  onkeypress="return '+_.findWhere(DataTypes,{type:mItem.miniDataTypeValue.toLowerCase()}).validation+'"></input>';
            }
            else if(mItem.miniComponentType == COMP_DATETIME){
                //var date = '\/Date('+mValue+')\/';
                var nowDate = "";
                var datetimeVal = "";
                if(mValue.length){
                    nowDate = new Date(parseInt(mValue));
                    datetimeVal = nowDate.format("yyyy-mm-dd'T'")+nowDate.format("HH:MM:ss");
                }

                strHtml = strHtml+'	<input id="'+frmId+'" type="datetime-local" class="form-control input-sm" value='+datetimeVal+'></input>';
            }
            else if(mItem.miniComponentType == COMP_NAME){
                var names = mValue.split('~');
                strHtml = strHtml+'<div class="namegroup"><input type="text" placeholder="First" class="form-control first" id="cff'
                    + frmId
                    + '"  value="'+names[0]+'" onkeypress="return '+_.findWhere(DataTypes,{type:mItem.miniDataTypeValue.toLowerCase()}).validation+'"/></div><div class="namegroup"><input type="text" placeholder="Middle" class="form-control middle" id="cfm'
                    + frmId + '" value="'+(names[1]|| '')+'" onkeypress="return '+_.findWhere(DataTypes,{type:mItem.miniDataTypeValue.toLowerCase()}).validation+'"/></div><div class="namegroup"><input type="text" placeholder="Last" class="form-control last" id="cfl'
                    + frmId + '"  value="'+(names[2]|| '')+'" onkeypress="return '+_.findWhere(DataTypes,{type:mItem.miniDataTypeValue.toLowerCase()}).validation+'"/></div>';
            }
            else if(mItem.miniComponentType == COMP_ADDRESS){
                var adds = mValue.split('~');
                strHtml = strHtml+'<div class="add-blocks"><input type="text" placeholder="Address1" class="add1 form-control" id="cfadd1'
                    + frmId
                    + '"  value="'+(adds[0] || '')+'"/></div><div class="add-blocks"><input type="text" placeholder="Address2" class="add2 form-control" id="cfadd2'
                    + frmId
                    + '"  value="'+(adds[1] || '')+'"/></div><div class="add-blocks"><input type="text" placeholder="City" class="city form-control" readonly id="cfcity'
                    + frmId
                    + '"  value="'+(adds[2] || '')+'"/><img id="btnZipSearch'+mItem.id+'" class="postalWrapper-img" src="../../img/Default/search-icon.png" title="Search" /><input type="hidden" id="cityIdHiddenValue"></div><div class="add-blocks"><input type="text" class="state form-control" placeholder="State / County"  readonly id="cfstate'
                    + frmId
                    + '"  value="'+(adds[3] || '')+'"/></div><div class="add-blocks"><input type="text" placeholder="Country" class="postal form-control" readonly id="cfcountry'
                    + frmId
                    + '"  value="'+(adds[4] || '')+'"/></div><div class="add-blocks" id="'+frmId+'"><div class=""><div class="noPadding postalWrapper"><input placeholder="postal/zip code" id="cmbZip'+frmId+'" value="'+(adds[5] || '')+'" type="text" class="form-control input-sm" readonly /></div></div></div>';
            }
            else if(mItem.miniComponentType == COMP_SERVICEUSER){
                if(patientInfoObject){
                    var languageVal = patientInfoObject.patient.language;
                    if(languageVal) {
                        languageVal = languageVal.split(',');
                    }

                    // var firstRow = '<div class="col-xs-3"><input type="text" class="form-control" readonly value="'+patientInfoObject.patient.id +'" id="sid'+frmId+'" /><label>ID:</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+patientInfoObject.patient.firstName +'" id="first'+frmId+'" /><label>First</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+patientInfoObject.patient.middleName  +'" id="middle'+frmId+'" /><label>Middle</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+patientInfoObject.patient.lastName +'" id="last'+frmId+'" /><label>Last</label></div>';
                    // var secondRow = '<div class="col-xs-3"><input type="text" class="form-control input-sm" readonly value="'+dob +'" id="dob'+frmId+'" /><label>DOB:</label></div><div class="col-xs-3"><input type="text" readonly value="'+patientInfoObject.patient.gender +'"  class="form-control" id="gender">Gender</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+patientInfoObject.patient.maritalStatus +'" id="marritalstatus'+frmId+'" /><label>Marrital Status</label></div><div class="col-xs-3"><input type="text" value="'+patientInfoObject.patient.race +'" readonly class="form-control" id="race'+frmId+'" /><label>Race</label></div>';
                    // var thirdRow = '<div class="col-xs-3"><input type="text" readonly value="'+patientInfoObject.patient.ethnicity +'" class="form-control" id="ethnicity'+frmId+'" /><label>Ethnicity</label></div><div class="col-xs-3"><input type="text" readonly value="'+languageVal[0] +'" class="form-control" id="prelang'+frmId+'" /><label>Preferred Language</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+languageVal[1] +'" id="second'+frmId+'" /><label>Second</label></div><div class="col-xs-3"><input type="text" class="form-control" value="'+languageVal[2] +'" id="third'+frmId+'" readonly /><label>Third</label></div>';
                    // var fourthRow = '<div class="col-xs-3"><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.address1 +'" id="address1'+frmId+'" /><label>Address1</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.address2 +'" id="address2'+frmId+'" /><label>Address2</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.city +'" id="city'+frmId+'" /><label>City</label></div><div class="col-xs-3"><input type="text" value="'+patientInfoObject.communication.state +'" class="form-control" readonly id="state'+frmId+'" /><label>State/County</label></div>';
                    // var fifthRow = '<div class="col-xs-3"><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.country +'" id="country'+frmId+'" /><label>Country</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.zip +'" id="postal'+frmId+'" /><label>Postal/Zip Code</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.homePhone +'" id="homephone'+frmId+'" /><label>Home Phone</label></div><div class="col-xs-3"><input type="text" value="'+patientInfoObject.communication.cellPhone +'" readonly class="form-control" id="mobile'+frmId+'" /><label>Mobile</label></div>';
                    // var sixthRow = '<div class="col-xs-3"><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.workPhone +'" id="workphone'+frmId+'" /><label>Work Phone</label></div><div class="col-xs-3"><input type="text" readonly value="'+patientInfoObject.communication.email +'" class="form-control" id="eamil'+frmId+'" /><label>Email</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.sms +'"  id="sms'+frmId+'" /><label>SMS</label></div>';


                    // var firstRow = '<div class="col-xs-3"><span class="form-control" id="sid'+frmId+'">'+patientInfoObject.patient.id+'</span><label>ID:</label></div><div class="col-xs-3"><span class="form-control">patientInfoObject.patient.firstName </span><label>First</label></div><div class="col-xs-3"><span class="form-control" id="middle'+frmId+'">patientInfoObject.patient.middleName</span>><label>Middle</label></div><div class="col-xs-3"><span class="form-control" id="last'+frmId+'">'+patientInfoObject.patient.lastName+'</span><label>Last</label></div>';
                    // var secondRow = '<div class="col-xs-3"><span class="form-control input-sm" id="dob'+frmId+'">'+dob+'</span><label>DOB:</label></div><div class="col-xs-3"><span class="form-control" id="gender">'+patientInfoObject.patient.gender +'</span><label>Gender</label></div><div class="col-xs-3"><span class="form-control" id="marritalstatus'+frmId+'">'+patientInfoObject.patient.maritalStatus+'</span><label>Marrital Status</label></div><div class="col-xs-3"><span class="form-control" id="race'+frmId+'">'+patientInfoObject.patient.race +'</span><label>Race</label></div>';
                    // var thirdRow = '<div class="col-xs-3"><span class="form-control" id="ethnicity'+frmId+'">'+patientInfoObject.patient.ethnicity+'</span><label>Ethnicity</label></div><div class="col-xs-3"><span class="form-control" id="prelang'+frmId+'">'+languageVal[0] +'</span><label>Preferred Language</label></div><div class="col-xs-3"><span class="form-control" id="second'+frmId+'">'+languageVal[1] +'</span><label>Second</label></div><div class="col-xs-3"><span class="form-control" id="third'+frmId+'">'+languageVal[2] +'</span><label>Third</label></div>';
                    // var fourthRow = '<div class="col-xs-3"><span class="form-control" id="address1'+frmId+'">'+patientInfoObject.communication.address1 +'</span><label>Address1</label></div><div class="col-xs-3"><span class="form-control" id="address2'+frmId+'">'+patientInfoObject.communication.address2 +'</span><label>Address2</label></div><div class="col-xs-3"><span class="form-control" id="city'+frmId+'">'+patientInfoObject.communication.city+' </span><label>City</label></div><div class="col-xs-3"><span class="form-control" id="state'+frmId+'">'+patientInfoObject.communication.state +'</span><label>State/County</label></div>';
                    // var fifthRow = '<div class="col-xs-3"><span class="form-control" id="country'+frmId+'">'+patientInfoObject.communication.country+'</span><label>Country</label></div><div class="col-xs-3"><span class="form-control" id="postal'+frmId+'">'+patientInfoObject.communication.zip+'</span><label>Postal/Zip Code</label></div><div class="col-xs-3"><span class="form-control" id="homephone'+frmId+'">'+patientInfoObject.communication.homePhone+'</span><label>Home Phone</label></div><div class="col-xs-3"><span value="'+patientInfoObject.communication.cellPhone +'" class="form-control" id="mobile'+frmId+'">'+patientInfoObject.communication.cellPhone+'</span><label>Mobile</label></div>';
                    // var sixthRow = '<div class="col-xs-3"><span class="form-control" id="workphone'+frmId+'">'+patientInfoObject.communication.workPhone +'</span><label>Work Phone</label></div><div class="col-xs-3"><span type="text" class="form-control" id="eamil'+frmId+'">'+patientInfoObject.communication.email+'</span><label>Email</label></div><div class="col-xs-3"><span class="form-control" id="sms'+frmId+'">'+patientInfoObject.communication.sms+'</span><label>SMS</label></div>';

                    // var firstRow = '<div class="col-xs-3"><span class="form-control" id="last'+frmId+'">'+patientInfoObject.patient.lastName+'</span>&nbsp;<label>Last</label></div><div class="col-xs-3"><span class="form-control">'+' '+patientInfoObject.patient.firstName+' </span>&nbsp;<label>First</label></div><div class="col-xs-3"><span class="form-control" id="middle'+frmId+'">'+' '+patientInfoObject.patient.middleName+'</span>&nbsp;<label>Middle</label></div><br/>';
                    // var secondRow = '<div class="col-xs-3"></div><div class="col-xs-3"><span class="form-control input-sm" id="dob'+frmId+'">'+dob+'</span>&nbsp;<label>DOB:</label></div><div class="col-xs-3"><span class="form-control" id="gender">'+patientInfoObject.patient.gender +'</span><label style="display:none">Gender</label></div><br/>';
                    //
                    // var fourthRow = '<div class="col-xs-3"></div><div class="col-xs-3"><span class="form-control" id="address1'+frmId+'">'+patientInfoObject.communication.address1 +'</span>&nbsp;<label>Address1</label></div><div class="col-xs-3"><span class="form-control" id="address2'+frmId+'">'+patientInfoObject.communication.address2 +'</span>&nbsp;<label>Address2</label></div><div class="col-xs-3"><span class="form-control" id="city'+frmId+'">'+patientInfoObject.communication.city+' </span>&nbsp;<label>City</label></div></br><div class="col-xs-3"><span class="form-control" id="state'+frmId+'">'+patientInfoObject.communication.state +'</span>&nbsp;<label>State/County</label></div>';
                    // var fifthRow = '<div class="col-xs-3"><span class="form-control" id="country'+frmId+'">'+patientInfoObject.communication.country+'</span><label>Country</label></div></br><div class="col-xs-3"><span class="form-control" id="homephone'+frmId+'">'+homePhone+'</span><label>Home Phone</label></div></br><div class="col-xs-3"><span class="form-control" id="mobile'+frmId+'">'+cellPhone+'</span><label>Cell Phone</label></div>';

                    // var ele = firstRow + secondRow+ fourthRow + fifthRow;
                    // strHtml = strHtml + ele;
                    var cellPhone = "";
                    if(patientInfoObject.communication.cellPhone != null){
                        cellPhone = "<b>Cell Phone </b>" + patientInfoObject.communication.cellPhone;
                    }

                    var homePhone = "";
                    if(patientInfoObject.communication.homePhone != null){
                        homePhone = "<b>Home Phone </b>" + patientInfoObject.communication.homePhone;
                    }

                    var dob = dformat(new Date(patientInfoObject.patient.dateOfBirth).getTime(), 'dd-MM-yyyy');

                    // var firstRow = '<div class="col-xs-3"><label>ID:</label><input type="text" class="form-control" readonly value="'+patientInfoObject.patient.id +'" id="sid'+frmId+'" /></div><div class="col-xs-3"><label>First</label><input type="text" class="form-control" readonly value="'+patientInfoObject.patient.firstName +'" id="first'+frmId+'" /></div><div class="col-xs-3"><label>Middle</label><input type="text" class="form-control" readonly value="'+patientInfoObject.patient.middleName  +'" id="middle'+frmId+'" /></div><div class="col-xs-3"><label>Last</label><input type="text" class="form-control" readonly value="'+patientInfoObject.patient.lastName +'" id="last'+frmId+'" /></div>';
                    // var secondRow = '<div class="col-xs-3"><label>DOB:</label><input type="text" class="form-control " readonly value="'+dob +'" id="dob'+frmId+'" /></div><div class="col-xs-3"><label>Gender</label><input type="text" readonly value="'+patientInfoObject.patient.gender +'"  class="form-control" id="gender"></div><div class="col-xs-3"><label>Marrital Status</label><input type="text" class="form-control" readonly value="'+patientInfoObject.patient.maritalStatus +'" id="marritalstatus'+frmId+'" /></div><div class="col-xs-3"><label>Race</label><input type="text" value="'+patientInfoObject.patient.race +'" readonly class="form-control" id="race'+frmId+'" /></div>';
                    // var thirdRow = '<div class="col-xs-3"><label>Ethnicity</label><input type="text" readonly value="'+patientInfoObject.patient.ethnicity +'" class="form-control" id="ethnicity'+frmId+'" /></div><div class="col-xs-3"><label>Preferred Language</label><input type="text" readonly value="'+languageVal[0] +'" class="form-control" id="prelang'+frmId+'" /></div><div class="col-xs-3"><label>Second</label><input type="text" class="form-control" readonly value="'+languageVal[1] +'" id="second'+frmId+'" /></div><div class="col-xs-3"><label>Third</label><input type="text" class="form-control" value="'+languageVal[2] +'" id="third'+frmId+'" readonly /></div>';
                    // var fourthRow = '<div class="col-xs-3"><label>Address1</label><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.address1 +'" id="address1'+frmId+'" /></div><div class="col-xs-3"><label>Address2</label><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.address2 +'" id="address2'+frmId+'" /></div><div class="col-xs-3"><label>City</label><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.city +'" id="city'+frmId+'" /></div><div class="col-xs-3"><label>State/County</label><input type="text" value="'+patientInfoObject.communication.state +'" class="form-control" readonly id="state'+frmId+'" /></div>';
                    // var fifthRow = '<div class="col-xs-3"><label>Country</label><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.country +'" id="country'+frmId+'" /></div><div class="col-xs-3"><label>Postal/Zip Code</label><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.zip +'" id="postal'+frmId+'" /></div><div class="col-xs-3"><label>Home Phone</label><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.homePhone +'" id="homephone'+frmId+'" /></div><div class="col-xs-3"><label>Mobile</label><input type="text" value="'+patientInfoObject.communication.cellPhone +'" readonly class="form-control" id="mobile'+frmId+'" /></div>';
                    // var sixthRow = '<div class="col-xs-3"><label>Work Phone</label><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.workPhone +'" id="workphone'+frmId+'" /></div><div class="col-xs-3"><label>Email</label><input type="text" readonly value="'+patientInfoObject.communication.email +'" class="form-control" id="eamil'+frmId+'" /></div><div class="col-xs-3"><label>SMS</label><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.sms +'"  id="sms'+frmId+'" /></div>';
                    var firstRow = '<div class="col-xs-3"><span class="form-control" id="last'+frmId+'">'+patientInfoObject.patient.lastName+'</span>&nbsp;<label>Last</label></div><div class="col-xs-3"><span class="form-control">'+' '+patientInfoObject.patient.firstName+' </span>&nbsp;<label>First</label></div><div class="col-xs-3"><span class="form-control" id="middle'+frmId+'">'+' '+patientInfoObject.patient.middleName+'</span>&nbsp;<label>Middle</label></div><br/>';
                    var secondRow = '<div class="col-xs-3"></div><div class="col-xs-3"><span class="form-control input-sm" id="dob'+frmId+'">'+dob+'</span>&nbsp;<label>DOB:</label></div><div class="col-xs-3"><span class="form-control" id="gender">'+patientInfoObject.patient.gender +'</span><label style="display:none">Gender</label></div><br clear="all"/>';

                    var fourthRow = '<div class="col-xs-3"></div><div class="col-xs-3"><span class="form-control" id="address1'+frmId+'">'+patientInfoObject.communication.address1 +'</span>&nbsp;<label>Address1</label></div><div class="col-xs-3"><span class="form-control" id="address2'+frmId+'">'+patientInfoObject.communication.address2 +'</span>&nbsp;<label>Address2</label></div><div class="col-xs-3"><span class="form-control" id="city'+frmId+'">'+patientInfoObject.communication.city+' </span>&nbsp;<label>City</label></div></br><div class="col-xs-3"><span class="form-control" id="state'+frmId+'">'+patientInfoObject.communication.state +'</span>&nbsp;<label>State/County</label></div>';
                    var fifthRow = '<div class="col-xs-3"><span class="form-control" id="country'+frmId+'">'+patientInfoObject.communication.country+'</span><label>Country</label></div></br><div class="col-xs-3"><span class="form-control" id="homephone'+frmId+'">'+homePhone+'</span><label>Home Phone</label></div></br><div class="col-xs-3"><span class="form-control" id="mobile'+frmId+'">'+cellPhone+'</span><label>Cell Phone</label></div>';
                    // var sixthRow = '<div class="col-xs-3"><span class="form-control" id="workphone'+frmId+'">'+patientInfoObject.communication.workPhone +'</span><label>Work Phone</label></div><div class="col-xs-3"><span type="text" class="form-control" id="eamil'+frmId+'">'+patientInfoObject.communication.email+'</span><label>Email</label></div><div class="col-xs-3"><span class="form-control" id="sms'+frmId+'">'+patientInfoObject.communication.sms+'</span><label>SMS</label></div>';
                    var ele = firstRow + secondRow+ fourthRow + fifthRow + '</div>';
                    strHtml = strHtml + ele ;
                }

                if(providerInfoObject){
                    var languageVal = providerInfoObject.language;
                    if(languageVal) {
                        languageVal = languageVal.split(',');
                    }

                    var cellPhone = "";
                    if(providerInfoObject.communications[0].cellPhone != undefined && providerInfoObject.communications[0].cellPhone != null && providerInfoObject.communications[0].cellPhone != ''){
                        cellPhone = "<b>Cell Phone </b>" + providerInfoObject.communications[0].cellPhone;
                    }

                    var homePhone = "";
                    if(providerInfoObject.communications[0].homePhone != null){
                        homePhone = "<b>Home Phone </b>" + providerInfoObject.communications[0].homePhone;
                    }


                    var dob = dformat(new Date(providerInfoObject.dateOfBirth).getTime(), 'dd-MM-yyyy');

                    // var firstRow = '<div class="col-xs-3"><div class="col-xs-6"><input type="text" class="form-control" readonly value="'+providerInfoObject.PID +'" id="sid'+frmId+'"/><label>ID:</label></div><div class="col-xs-6" style="padding-right:0;"><input type="text" class="form-control" readonly value="'+providerInfoObject.prefix +'" id="sid'+frmId+'" /><label>Prefix:</label></div></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.firstName +'" id="first'+frmId+'" /><label>First</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.lastName +'" id="last'+frmId+'" /><label>Last</label></div>';
                    // var secondRow = '<div class="col-xs-3"><input type="text" class="form-control " readonly value="'+dob +'" id="dob'+frmId+'" /><label>DOB:</label></div><div class="col-xs-3"><input type="text" readonly value="'+providerInfoObject.gender +'"  class="form-control" id="gender">Gender</label></div>';
                    // var thirdRow = '<div class="col-xs-3"><input type="text" readonly value="'+languageVal[0] +'" class="form-control" id="prelang'+frmId+'" /><label>Preferred Language</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+(languageVal[1] || '') +'" id="second'+frmId+'" /><label>Second</label></div><div class="col-xs-3"><input type="text" class="form-control" value="'+(languageVal[2] || '') +'" id="third'+frmId+'" readonly /><label>Third</label></div>';
                    // var fourthRow = '<div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.communications[0].address1 +'" id="address1'+frmId+'" /><label>Address1</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.communications[0].address2 +'" id="address2'+frmId+'" /><label>Address2</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.communications[0].city +'" id="city'+frmId+'" /><label>City</label></div><div class="col-xs-3"><input type="text" value="'+providerInfoObject.communications[0].state +'" class="form-control" readonly id="state'+frmId+'" /><label>State/County</label></div>';
                    // var fifthRow = '<div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.communications[0].country +'" id="country'+frmId+'" /><label>Country</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.communications[0].zip +'" id="postal'+frmId+'" /><label>Postal/Zip Code</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.communications[0].homePhone +'" id="homephone'+frmId+'" /><label>Home Phone</label></div><div class="col-xs-3"><input type="text" value="'+providerInfoObject.communications[0].cellPhone +'" readonly class="form-control" id="mobile'+frmId+'" /><label>Mobile</label></div>';
                    // var sixthRow = '<div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.communications[0].workPhone +'" id="workphone'+frmId+'" /><label>Work Phone</label></div><div class="col-xs-3"><input type="text" readonly value="'+providerInfoObject.communications[0].email +'" class="form-control" id="eamil'+frmId+'" /><label>Email</label></div>';
                    // 	var firstRow = '<div class="col-xs-3"><div class="col-xs-6"><span class="form-control" id="sid'+frmId+'">'+providerInfoObject.PID+'</span><label>ID:</label></div><div class="col-xs-6" style="padding-right:0;"><span class="form-control"  id="sid'+frmId+'" >providerInfoObject.prefix </span><label>Prefix:</label></div></div><div class="col-xs-3"><span class="form-control" id="first'+frmId+'">'+providerInfoObject.firstName+'</span>><label>First</label></div><div class="col-xs-3"><span class="form-control" id="last'+frmId+'">'+providerInfoObject.lastName+'</span><label>Last</label></div>';
                    // 	var secondRow = '<div class="col-xs-3"><span class="form-control input-sm" id="dob'+frmId+'">'+dob+'</span><label>DOB:</label></div><div class="col-xs-3"><span class="form-control" id="gender">'+providerInfoObject.gender +'</span><label>Gender</label></div>';
                    //
                    // 	var thirdRow = '<div class="col-xs-3"><span class="form-control" id="prelang'+frmId+'">'+languageVal[0] +'</span><label>Preferred Language</label></div><div class="col-xs-3"><span class="form-control" id="second'+frmId+'">'+(languageVal[1] || '')+'</span><label>Second</label></div><div class="col-xs-3"><span class="form-control" id="third'+frmId+'">'+(languageVal[2] || '') +'</span><label>Third</label></div>';
                    // var fourthRow = '<div class="col-xs-3"><span class="form-control" id="address1'+frmId+'">'+providerInfoObject.communication.address1 +'</span><label>Address1</label></div><div class="col-xs-3"><span class="form-control" id="address2'+frmId+'">'+providerInfoObject.communication.address2 +'</span><label>Address2</label></div><div class="col-xs-3"><span class="form-control" id="city'+frmId+'">'+providerInfoObject.communication.city+' </span><label>City</label></div><div class="col-xs-3"><span class="form-control" id="state'+frmId+'">'+providerInfoObject.communication.state +'</span><label>State/County</label></div>';
                    // var fifthRow = '<div class="col-xs-3"><span class="form-control" id="country'+frmId+'">'+providerInfoObject.communication.country+'</span><label>Country</label></div><div class="col-xs-3"><span class="form-control" id="postal'+frmId+'">'+providerInfoObject.communication.zip+'</span><label>Postal/Zip Code</label></div><div class="col-xs-3"><span class="form-control" id="homephone'+frmId+'">'+providerInfoObject.communication.homePhone+'</span><label>Home Phone</label></div><div class="col-xs-3"><span value="'+providerInfoObject.communication.cellPhone +'" class="form-control" id="mobile'+frmId+'">'+providerInfoObject.communication.cellPhone+'</span><label>Mobile</label></div>';
                    // var sixthRow = '<div class="col-xs-3"><span class="form-control" id="workphone'+frmId+'">'+providerInfoObject.communication.workPhone +'</span><label>Work Phone</label></div><div class="col-xs-3"><span type="text" class="form-control" id="eamil'+frmId+'">'+providerInfoObject.communication.email+'</span><label>Email</label></div><div class="col-xs-3"><span class="form-control" id="sms'+frmId+'">'+providerInfoObject.communication.sms+'</span><label>SMS</label></div>';

                    var firstRow = '<div class="col-xs-3"><span class="form-control" id="last'+frmId+'">'+providerInfoObject.lastName+'</span>&nbsp;<label>Last</label></div><div class="col-xs-3"><span class="form-control">'+' '+providerInfoObject.firstName+' </span>&nbsp;<label>First</label></div><div class="col-xs-3"><span class="form-control" id="middle'+frmId+'">'+' '+providerInfoObject.middleName+'</span>&nbsp;<label>Middle</label></div><br/>';
                    var secondRow = '<div class="col-xs-3"></div><div class="col-xs-3"><span class="form-control input-sm" id="dob'+frmId+'">'+dob+'</span>&nbsp;<label>DOB:</label></div><div class="col-xs-3"><span class="form-control" id="gender">'+providerInfoObject.gender +'</span><label style="display:none">Gender</label></div><br clear="all"/>';

                    var fourthRow = '<div class="col-xs-3"></div><div class="col-xs-3"><span class="form-control" id="address1'+frmId+'">'+providerInfoObject.communications[0].address1 +'</span>&nbsp;<label>Address1</label></div><div class="col-xs-3"><span class="form-control" id="address2'+frmId+'">'+providerInfoObject.communications[0].address2 +'</span>&nbsp;<label>Address2</label></div><div class="col-xs-3"><span class="form-control" id="city'+frmId+'">'+providerInfoObject.communications[0].city+' </span>&nbsp;<label>City</label></div></br><div class="col-xs-3"><span class="form-control" id="state'+frmId+'">'+providerInfoObject.communications[0].state +'</span>&nbsp;<label>State/County</label></div>';
                    var fifthRow = '<div class="col-xs-3"><span class="form-control" id="country'+frmId+'">'+providerInfoObject.communications[0].country+'</span><label>Country</label></div></br><div class="col-xs-3"><span class="form-control" id="homephone'+frmId+'">'+homePhone+'</span><label>Home Phone</label></div></br><div class="col-xs-3"><span class="form-control" id="mobile'+frmId+'">'+cellPhone+'</span><label>Cell Phone</label></div>';

                    var ele = firstRow + secondRow+ fourthRow + fifthRow;
                    //strHtml = strHtml + ele;
                    staffHTML = ele;
                }


            }
            // else if(mItem.miniComponentType == COMP_STAFF){
            // 	console.log(providerInfoObject);
            // 	if(providerInfoObject){
            //     var languageVal = providerInfoObject.language;
            //     if(languageVal) {
            //         languageVal = languageVal.split(',');
            //     }
            //
            //     var dob = dformat(new Date(providerInfoObject.dateOfBirth).getTime(), 'MM/dd/yyyy');
            //
            //     // var firstRow = '<div class="col-xs-3"><div class="col-xs-6"><input type="text" class="form-control" readonly value="'+providerInfoObject.PID +'" id="sid'+frmId+'"/><label>ID:</label></div><div class="col-xs-6" style="padding-right:0;"><input type="text" class="form-control" readonly value="'+providerInfoObject.prefix +'" id="sid'+frmId+'" /><label>Prefix:</label></div></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.firstName +'" id="first'+frmId+'" /><label>First</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.lastName +'" id="last'+frmId+'" /><label>Last</label></div>';
            // 	// var secondRow = '<div class="col-xs-3"><input type="text" class="form-control input-sm" readonly value="'+dob +'" id="dob'+frmId+'" /><label>DOB:</label></div><div class="col-xs-3"><input type="text" readonly value="'+providerInfoObject.gender +'"  class="form-control" id="gender">Gender</label></div>';
            // 	// var thirdRow = '<div class="col-xs-3"><input type="text" readonly value="'+languageVal[0] +'" class="form-control" id="prelang'+frmId+'" /><label>Preferred Language</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+(languageVal[1] || '') +'" id="second'+frmId+'" /><label>Second</label></div><div class="col-xs-3"><input type="text" class="form-control" value="'+(languageVal[2] || '') +'" id="third'+frmId+'" readonly /><label>Third</label></div>';
            // 	// var fourthRow = '<div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.communications[0].address1 +'" id="address1'+frmId+'" /><label>Address1</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.communications[0].address2 +'" id="address2'+frmId+'" /><label>Address2</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.communications[0].city +'" id="city'+frmId+'" /><label>City</label></div><div class="col-xs-3"><input type="text" value="'+providerInfoObject.communications[0].state +'" class="form-control" readonly id="state'+frmId+'" /><label>State/County</label></div>';
            // 	// var fifthRow = '<div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.communications[0].country +'" id="country'+frmId+'" /><label>Country</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.communications[0].zip +'" id="postal'+frmId+'" /><label>Postal/Zip Code</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.communications[0].homePhone +'" id="homephone'+frmId+'" /><label>Home Phone</label></div><div class="col-xs-3"><input type="text" value="'+providerInfoObject.communications[0].cellPhone +'" readonly class="form-control" id="mobile'+frmId+'" /><label>Mobile</label></div>';
            // 	// var sixthRow = '<div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.communications[0].workPhone +'" id="workphone'+frmId+'" /><label>Work Phone</label></div><div class="col-xs-3"><input type="text" readonly value="'+providerInfoObject.communications[0].email +'" class="form-control" id="eamil'+frmId+'" /><label>Email</label></div>';
            //
            // 	var firstRow = '<div class="col-xs-3"><div class="col-xs-6"><span class="form-control" id="sid'+frmId+'">'+providerInfoObject.PID+'</span><label>ID:</label></div><div class="col-xs-6" style="padding-right:0;"><span class="form-control"  id="sid'+frmId+'" >providerInfoObject.prefix </span><label>Prefix:</label></div></div><div class="col-xs-3"><span class="form-control" id="first'+frmId+'">'+providerInfoObject.firstName+'</span>><label>First</label></div><div class="col-xs-3"><span class="form-control" id="last'+frmId+'">'+providerInfoObject.lastName+'</span><label>Last</label></div>';
            // 	var secondRow = '<div class="col-xs-3"><span class="form-control input-sm" id="dob'+frmId+'">'+dob+'</span><label>DOB:</label></div><div class="col-xs-3"><span class="form-control" id="gender">'+providerInfoObject.gender +'</span><label>Gender</label></div>';
            //
            // 	var thirdRow = '<div class="col-xs-3"><span class="form-control" id="prelang'+frmId+'">'+languageVal[0] +'</span><label>Preferred Language</label></div><div class="col-xs-3"><span class="form-control" id="second'+frmId+'">'+(languageVal[1] || '')+'</span><label>Second</label></div><div class="col-xs-3"><span class="form-control" id="third'+frmId+'">'+(languageVal[2] || '') +'</span><label>Third</label></div>';
            // 	var fourthRow = '<div class="col-xs-3"><span class="form-control" id="address1'+frmId+'">'+providerInfoObject.communication.address1 +'</span><label>Address1</label></div><div class="col-xs-3"><span class="form-control" id="address2'+frmId+'">'+providerInfoObject.communication.address2 +'</span><label>Address2</label></div><div class="col-xs-3"><span class="form-control" id="city'+frmId+'">'+providerInfoObject.communication.city+' </span><label>City</label></div><div class="col-xs-3"><span class="form-control" id="state'+frmId+'">'+providerInfoObject.communication.state +'</span><label>State/County</label></div>';
            // 	var fifthRow = '<div class="col-xs-3"><span class="form-control" id="country'+frmId+'">'+providerInfoObject.communication.country+'</span><label>Country</label></div><div class="col-xs-3"><span class="form-control" id="postal'+frmId+'">'+providerInfoObject.communication.zip+'</span><label>Postal/Zip Code</label></div><div class="col-xs-3"><span class="form-control" id="homephone'+frmId+'">'+providerInfoObject.communication.homePhone+'</span><label>Home Phone</label></div><div class="col-xs-3"><span value="'+providerInfoObject.communication.cellPhone +'" class="form-control" id="mobile'+frmId+'">'+providerInfoObject.communication.cellPhone+'</span><label>Mobile</label></div>';
            // 	var sixthRow = '<div class="col-xs-3"><span class="form-control" id="workphone'+frmId+'">'+providerInfoObject.communication.workPhone +'</span><label>Work Phone</label></div><div class="col-xs-3"><span type="text" class="form-control" id="eamil'+frmId+'">'+providerInfoObject.communication.email+'</span><label>Email</label></div><div class="col-xs-3"><span class="form-control" id="sms'+frmId+'">'+providerInfoObject.communication.sms+'</span><label>SMS</label></div>';
            //
            // 	var ele = firstRow + secondRow+ thirdRow + fourthRow + fifthRow + sixthRow;
            // 	strHtml = strHtml + ele;
            // }
            // 	/*else{
            // 		 //var dob = dformat(new Date(providerInfoObject.dateOfBirth).getTime(), 'MM/dd/yyyy');
            //
            // 			var firstRow = '<div class="col-xs-3"><div class="col-xs-6"><input type="text" class="form-control" readonly value="" id="sid'+frmId+'" style="width:90px"/><label>ID:</label></div><div class="col-xs-4"><input type="text" class="form-control" readonly  style="width:74px" value="" id="sid'+frmId+'" /><label>Prefix:</label></div></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="" id="first'+frmId+'" /><label>First</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="" id="last'+frmId+'" /><label>Last</label></div>';
            // 			var secondRow = '<div class="col-xs-3"><input type="text" class="form-control input-sm" readonly value="" id="dob'+frmId+'" /><label>DOB:</label></div><div class="col-xs-3"><input type="text" readonly value=""  class="form-control" id="gender">Gender</label></div>';
            // 			var thirdRow = '<div class="col-xs-3"><input type="text" readonly value="" class="form-control" id="prelang'+frmId+'" /><label>Preferred Language</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="" id="second'+frmId+'" /><label>Second</label></div><div class="col-xs-3"><input type="text" class="form-control" value="" id="third'+frmId+'" readonly /><label>Third</label></div>';
            // 			var fourthRow = '<div class="col-xs-3"><input type="text" class="form-control" readonly value="" id="address1'+frmId+'" /><label>Address1</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="" id="address2'+frmId+'" /><label>Address2</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="" id="city'+frmId+'" /><label>City</label></div><div class="col-xs-3"><input type="text" value="" class="form-control" readonly id="state'+frmId+'" /><label>State/County</label></div>';
            // 			var fifthRow = '<div class="col-xs-3"><input type="text" class="form-control" readonly value="" id="country'+frmId+'" /><label>Country</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="" id="postal'+frmId+'" /><label>Postal/Zip Code</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="" id="homephone'+frmId+'" /><label>Home Phone</label></div><div class="col-xs-3"><input type="text" value="" readonly class="form-control" id="mobile'+frmId+'" /><label>Mobile</label></div>';
            // 			var sixthRow = '<div class="col-xs-3"><input type="text" class="form-control" readonly value="" id="workphone'+frmId+'" /><label>Work Phone</label></div><div class="col-xs-3"><input type="text" readonly value="" class="form-control" id="eamil'+frmId+'" /><label>Email</label></div>';
            // 			var ele = firstRow + secondRow+ thirdRow + fourthRow + fifthRow + sixthRow;
            // 			strHtml = strHtml + ele;
            // 	}*/
            // }
            else if(mItem.miniComponentType == COMP_LIKERT){
                strHtml = strHtml+'	<input id="'+frmId+'" type="datetime-local" class="form-control input-sm"></input>';
            }
            else if(mItem.miniComponentType == COMP_RATING){
                strHtml = strHtml+'<div class="customfields crating maindiv" id="crating'
                    + frmId
                    + '">';
                var ratingVal = '';
                for(var i=0;i<5;i++)
                {
                    var clicked = '';
                    if(i<parseInt(mValue))
                        clicked = 'clicked';
                    ratingVal += '<span class="star '+clicked+'" value="1" tabindex="1" onclick="setRating('+(i+1)+',\''+ frmId+'\')"><i class="fa fa-star fa-2x" aria-hidden="true"></i></span>';
                }
                strHtml = strHtml+ ratingVal +'</div>';
            }

            //strHtml = strHtml+'</div>';
            if(mItem.miniComponentType == COMP_HLINE || mItem.miniComponentType == "hline"){
                strHtml = strHtml+'</div>';
            }
            else{
                if (mItem.componentType != COMP_STAFF) {
                    strHtml = strHtml + '<label class="units" >' + unit + '</label>';
                    if (hint && hint.length > 0)
                        strHtml = strHtml + '<label class="hint">' + hint + '</label>';
                    var col3 = '';
                    var col3 = '';
                    if (curMiniTemplate && curMiniTemplate.label == 3) {
                        if(mLabel == '3') {
                            var cinput = '<input type="text" class="form-control input-sm" id="fcol' + mItem.id + '"  value="' + (fcolValue || '') + '" style="width:100%;"/>';
                            col3 = '<div class="col3 thirdDiv">' + (mLabel == '3' ? cinput : '') + '</div>';
                        }
                    }
                    else if (curMiniTemplate && curMiniTemplate.label == 4) {
                        if(fcolValue == "undefined"){
                            fcolValue = "";
                        }
                        if(mLabel == '4') {
                            var cinput3 = '<input type="text" class="form-control input-sm" id="fcol' + mItem.id + '"  value="' + (fcolValue || '') + '" style="width:100%;"/>';
                            var cinput4 = '<input type="text" class="form-control input-sm" id="fcol1' + mItem.id + '"  value="' + (fcolValue || '') + '" style="width:100%;"/>';
                            col3 = '<div class="col3 thirdDiv fourthEle">' + (mLabel == '4' ? cinput3 : '') + '</div><div class="col3 fourthEle">' + (mLabel == '4' ? cinput4 : '') + '</div>';
                        }
                    }
                }
                // strHtml = strHtml+'</div></div>'+col3+'</div>';
                if(mItem.miniComponentType == COMP_SERVICEUSER){

                    if(!providerInfoObject){

                        var dt = new Date();
                        dt = kendo.toString(dt, "dd-MM-yyyy h:mm:tt");

                        var fullDate = dt;

                        rightside = '<div class="col-xs-12"><div><div class="leftBlock">' +
                            '<label class="col-heading">Date Time When Filled Form :</label>' +
                            '<span class="" id="rDateTime">'+fullDate+'</span>' +
                            '</div></div></div>';
                    }
                    else{
                        // rightside = '<div class="col-xs-12 popupformElements"><div><div class="leftBlock"><label class="">staff :</label><img src="https://stage.timeam.com/homecare/download/providers/photo/?571838&amp;access_token=c161d936-70ab-4aa4-ba18-482089a79a69&amp;tenant=homecare1&amp;id=13" class="p-img"></div><div class="form-rightData"><div class="rightblock generalInformblock"><div class="col-xs-3"><div class="col-xs-6"><span class="form-control" id="sidf1042">13</span><label>ID:</label></div><div class="col-xs-6" style="padding-right:0;"><span class="form-control" id="sidf1042">providerInfoObject.prefix </span><label>Prefix:</label></div></div><div class="col-xs-3"><span class="form-control" id="firstf1042">Provider</span>&gt;<label>First</label></div><div class="col-xs-3"><span class="form-control" id="lastf1042">Provider</span><label>Last</label></div><div class="col-xs-3"><span class="form-control input-sm" id="dobf1042">12/26/1965</span><label>DOB:</label></div><div class="col-xs-3"><span class="form-control" id="gender">Female</span><label>Gender</label></div><div class="col-xs-3"><span class="form-control" id="prelangf1042">English</span><label>Preferred Language</label></div><div class="col-xs-3"><span class="form-control" id="secondf1042"> Spanish</span><label>Second</label></div><div class="col-xs-3"><span class="form-control" id="thirdf1042"> Arabic</span><label>Third</label></div><label class="units"></label></div></div><div class="addformservicerightdiv">fdf</div></div></div>';
                        // rightside = '<div class="col-xs-12 popupformElements"><div><div class="leftBlock"><label class="">staff :</label><img src='+providerImage+' class="p-img"></div><div class="form-rightData"><div class="rightblock generalInformblock"><div class="col-xs-3"><div class="col-xs-6"><span class="form-control" id="sidf1042">13</span><label>ID:</label></div><div class="col-xs-6" style="padding-right:0;"><span class="form-control" id="sidf1042">providerInfoObject.prefix </span><label>Prefix:</label></div></div><div class="col-xs-3"><span class="form-control" id="firstf1042">Provider</span>&gt;<label>First</label></div><div class="col-xs-3"><span class="form-control" id="lastf1042">Provider</span><label>Last</label></div><div class="col-xs-3"><span class="form-control input-sm" id="dobf1042">12/26/1965</span><label>DOB:</label></div><div class="col-xs-3"><span class="form-control" id="gender">Female</span><label>Gender</label></div><div class="col-xs-3"><span class="form-control" id="prelangf1042">English</span><label>Preferred Language</label></div><div class="col-xs-3"><span class="form-control" id="secondf1042"> Spanish</span><label>Second</label></div><div class="col-xs-3"><span class="form-control" id="thirdf1042"> Arabic</span><label>Third</label></div></div></div></div></div>';
                        // rightside = '<div class="col-xs-12 popupformElements"><div><div class="leftBlock"><label class="">staff :</label><img src='+providerImage+' class="p-img"></div><div class="form-rightData"><div class="rightblock generalInformblock"><div class="col-xs-3"><div class="col-xs-6"><span class="form-control" id="sidf1042">13</span><label>ID:</label></div><div class="col-xs-6" style="padding-right:0;"><span class="form-control" id="sidf1042">providerInfoObject.prefix </span><label>Prefix:</label></div></div><div class="col-xs-3"><span class="form-control" id="firstf1042">Provider</span>&gt;<label>First</label></div><div class="col-xs-3"><span class="form-control" id="lastf1042">Provider</span><label>Last</label></div><div class="col-xs-3"><span class="form-control input-sm" id="dobf1042">12/26/1965</span><label>DOB:</label></div><div class="col-xs-3"><span class="form-control" id="gender">Female</span><label>Gender</label></div><div class="col-xs-3"><span class="form-control" id="prelangf1042">English</span><label>Preferred Language</label></div><div class="col-xs-3"><span class="form-control" id="secondf1042"> Spanish</span><label>Second</label></div><div class="col-xs-3"><span class="form-control" id="thirdf1042"> Arabic</span><label>Third</label></div></div></div></div></div>';
                        rightside = '<div class="col-xs-12 popupformElements noBorder"><div><div class="leftBlock"><label class="col-heading">Staff Details :</label><img src='+ providerImage +' class="p-img"></div><div class="form-rightData"><div class="rightblock generalInformblock">'+staffHTML+'</div></div></div></div>';
                    }
                    strHtml = strHtml+ col3+'</div></div><div class="addformserviceleftdiv">'+rightside+'</div></div>';
                }
                else{
                    // if(frmObj.componentType != COMP_STAFF) {
                    //     strHtml = strHtml + '</div></div>' + col3 + '</div>';
                    // }
                    strHtml = strHtml + col3 +'</div></div></div>';
                }

            }
            fromArray.push(mItem);
        }
    }
    for(var i=0;i<miniTemplateById.length;i++){
        if(miniTemplateById[i].miniComponentType == 'address')
            addressComp.push(miniTemplateById[i]);
    }
    strHtml = strHtml+'<div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 form-group">';
    strHtml = strHtml+'<div class="navBar" style="padding-bottom:2px">';
    strHtml = strHtml+'</div>'
    //strHtml = strHtml+'<button id="btn-save" class="btn-save">Save</button></div>';

    $("#p"+currId).html(strHtml);
    $("#saveMiniTemplate").off("click");
    $("#saveMiniTemplate").on("click",onClickSave);
    //$("#"+btnId).off("click");
    //$("#"+btnId).on("click",onClickSave);

    setTimeout(function(){
        for(var k=0;k<miniTemplateById.length;k++){
            var mItem = miniTemplateById[k];
            if(mItem){
                mItem.idk = mItem.id;
                var frmId = "f"+mItem.id;
                var mValue = '';
                if(mItem.value != undefined) {
                    var mValue = mItem.value;
                }
                var unit  ="";
                mItem.frmId = frmId;
                if(mItem.miniComponentType == COMP_DATE_PICKER){
                    if(mItem.miniDataTypeValue == DT_DATE){
                        $("#"+frmId).kendoDatePicker({format: "dd-MM-yyyy", value:new Date(mValue)});
                        var cDate = $("#"+frmId).data("kendoDatePicker");
                        if(cDate){
                            cDate.value(new Date(mValue));

                        }
                    }else if(mItem.miniDataTypeValue == DT_DATETIME){
                        $("#"+frmId).kendoDateTimePicker({value:new Date(mValue)});
                        var cDate = $("#"+frmId).data("kendoDateTimePicker");
                        if(cDate){
                            cDate.value(new Date(mValue));
                        }
                    }
                }
            }
        }
    },100);
    $("#btnAdd").off("click");
    $("#btnAdd").on("click",onClickAdd);

    if(addressComp.length){
        for(var i=0;i<addressComp.length;i++){
            //if(addressComp[i].componentTypeId == 12)
            $("#btnZipSearch"+addressComp[i].id).off("click");
            $("#btnZipSearch"+addressComp[i].id).on("click",onClickZipSearch);
            //addressComp.push(addressComp[i].id);
        }
    }
    /*$("#btnSave").off("click",onClickSave);
    $("#btnSave").on("click",onClickSave);*/

    /*$("#btnZipSearch").off("click");
    $("#btnZipSearch").on("click",onClickZipSearch);*/

    $("#AddbtnZipSearch").off("click");
    $("#AddbtnZipSearch").on("click", onClickAdditionalZipSearch);
}

var COMP_TEXT = "text box";
var COMP_TEXT_AREA = "text area"
var COMP_BOOLEAN = "boolean";
var COMP_CHECK_BOX = "check box"
var COMP_RADIO_BUTTON = "radio button"
var COMP_GROUP_CHECK_BOX = "group check box"
var COMP_IMAGE = "image"
var COMP_DATE_PICKER = "date picker";
var COMP_DROPDOWN = "dropdown";
var COMP_EMAIL = "email";
var COMP_NAME = "name";
var COMP_ADDRESS = "address";
var COMP_PHONE = "phone";
var COMP_NUMBER = "number";
var COMP_WEBSITE = "website";
var COMP_DATETIME = "datetime";
var COMP_RATING = "rating";
var COMP_TIME = "time";
var COMP_PRICE = "price";
var COMP_LIKERT = "likert";
var COMP_LABEL = "label";
var COMP_HLINE = "hline";
var COMP_SERVICEUSER = "service user";
var COMP_STAFF = "staff";

var DT_INT = "INT";
var DT_VARCHAR = "VARCHAR";
var DT_DATETIME = "DATETIME";
var DT_DECIMAL = "DECIMAL";
var DT_DATE = "DATE";

var frmDesignArray = [];
var frmDesignArrayById = [];
var tempFormDesignArray = [];
var addressComp = [];

function handleGetVacationList(dataObj) {
    console.log(dataObj);
    console.log(_.findWhere(selItem.miniTemplates, {id: parseInt(currId)}));
    var curMiniTemplate = _.findWhere(selItem.miniTemplates, {id: parseInt(currId)});
    //frmDesignArray = [];
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            if (dataObj.response.components) {
                if ($.isArray(dataObj.response.components)) {
                    frmDesignArray = dataObj.response.components;
                } else {
                    frmDesignArray.push(dataObj.response.components);
                }
                frmDesignArray = _.sortBy(frmDesignArray, 'displayOrder');
                if (frmDesignArray) {
                    var miniTemp = {};
                    miniTemp.id = currId;
                    miniTemp.components = dataObj.response.components;
                    var found = false;
                    for (var j = 0; j < frmDesignArrayById.lenth; j++) {
                        if (frmDesignArrayById[j].id == currId) {
                            found = true;
                            break;
                        }
                    }
                    if (found)
                        frmDesignArrayById[j].components = dataObj.response.components;
                    else
                        frmDesignArrayById.push(miniTemp);
                }
            }
        }
    }
}


function handleGetVacationList1(dataObj){
    console.log(dataObj);
    console.log(_.findWhere(selItem.miniTemplates,{id:parseInt(currId)}));
    var curMiniTemplate = _.findWhere(selItem.miniTemplates,{id:parseInt(currId)});
    //frmDesignArray = [];
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            if(dataObj.response.components){
                if($.isArray(dataObj.response.components)){
                    frmDesignArray=dataObj.response.components;
                }else{
                    frmDesignArray.push(dataObj.response.components);
                }
                frmDesignArray = _.sortBy(frmDesignArray, 'displayOrder');
                if(frmDesignArray)
                {
                    var miniTemp = {};
                    miniTemp.id = currId;
                    miniTemp.components =dataObj.response.components;
                    var found =false;
                    for(var j=0;j<frmDesignArrayById.lenth;j++)
                    {
                        if(frmDesignArrayById[j].id == currId)
                        {
                            found = true;
                            break;
                        }
                    }
                    if(found)
                        frmDesignArrayById[j].components = dataObj.response.components;
                    else
                        frmDesignArrayById.push(miniTemp);
                    //addressComp.push(_.each(frmDesignArray,function(key){ if(frmDesignArray[key].miniTemplateId = 14) return frmDesignArray[key]}));
                    for(var i=0;i<frmDesignArray.length;i++){
                        if(frmDesignArray[i].componentTypeId == 12)
                            addressComp.push(frmDesignArray[i]);
                    }

                }
                console.log(frmDesignArrayById);
            }
        }
    }
    var rightside;
    var providerImage;
    var staffHTML;
    console.log(frmDesignArray);
    if(frmDesignArray.length>0){
        var obj = {};
        obj.idk = currId;
        obj.values = frmDesignArray;
        obj.operation = "add";
        tempFormDesignArray.push(obj);
        var strHtml = "";
        $("#p"+currId).html("");

        for(var j=0;j<frmDesignArray.length;j++){
            if((frmDesignArray[j].componentType == COMP_SERVICEUSER && !patientInfoObject))
            // if((frmDesignArray[j].componentType == COMP_SERVICEUSER && !patientInfoObject) || (frmDesignArray[j].componentType == COMP_STAFF && !providerInfoObject))
                j++;
            if(j==frmDesignArray.length)
                break;
            var frmObj = frmDesignArray[j];
            console.log(frmObj);
            var requiredSpan = frmObj.isRequired ==1?'<span style="color:red" id="mandatory">*</span>' : '';
            frmObj.idk = frmObj.id;
            frmObj.length1 = frmObj.length;
            var frmId = "f"+frmObj.idk;
            var unit  ="";
            var hint  ="";
            if(frmObj.unitValue){
                unit = frmObj.unitValue;
            }else{

            }
            if(frmObj.hint)
                hint = frmObj.hint;
            if((frmDesignArray[j].componentType == COMP_SERVICEUSER)){
                strHtml = strHtml+'<div class="col-xs-12 popupformElements serviceUseraddressForm"><div class="addformserviceleftdiv">';
            }

            else if(frmDesignArray[j].componentType == COMP_ADDRESS){
                strHtml = strHtml+'<div class="col-xs-12 popupformElements subinputlist">';
            }
            else{
                if (frmObj.componentType != COMP_STAFF) {
                    strHtml = strHtml + '<div class="col-xs-12 popupformElements">';
                }
            }

            //strHtml = strHtml+'<div class="col-xs-12 popupformElements">';
            if(!(frmObj.componentType == COMP_HLINE || frmObj.componentType.toLowerCase() =='horizontal line')){
                var clsName = 'class="form-rightData"';
                var clsLeftBlock = 'class="leftBlock"';
                if(curMiniTemplate && curMiniTemplate.label == 3)
                {
                    clsName = 'class="col3 form-rightData"';
                    clsLeftBlock = 'class="col3 leftBlock"';
                }
                /*if(!(frmObj.componentType == COMP_LABEL)){
                strHtml = strHtml+'<div class="form-rightData">';
                strHtml = strHtml+'<div class="rightblock">';
                }*/
                if(frmObj.componentType == COMP_LABEL)
                {
                    var style = ' style="';
                    var notes = frmObj.notes;
                    if(notes){
                        var notesArray = notes.split("~");
                        if(notesArray[0] && notesArray[0].toLowerCase() == 'bold')
                            style = style+'font-weight:bold !important;"';
                        else if(notesArray[0])
                            style = style+'font-style:'+notesArray[0]+' !important;"';
                    }
                    strHtml = strHtml + '<div '+clsLeftBlock+'><label '+style+'>'+frmObj.name+' </label>'+requiredSpan+'</div>';
                }
                else {
                    var img = '';

                    if (frmObj.componentType == COMP_SERVICEUSER && patientInfoObject) {
                        var imageServletUrl = ipAddress + "/download/patient/photo/" + patientInfoObject.patient.id + "/?" + Math.round(Math.random() * 1000000) + "&access_token=" + sessionStorage.access_token + "&tenant=" + sessionStorage.tenant;
                        img = '<img src="' + imageServletUrl + '" class="p-img" />';

                        if (providerInfoObject) {
                            providerImage = ipAddress + "/homecare/download/providers/photo/?" + Math.round(Math.random() * 1000000) + "&access_token=" + sessionStorage.access_token + "&tenant=" + sessionStorage.tenant + "&id=" + providerInfoObject.PID;
                            // img = '<img src="' + imageServletUrl + '" class="p-img" />';
                            // providerImage = imageServletUrl;
                            //strHtml = strHtml+'<div><div '+clsLeftBlock+'><label class="">'+frmObj.name+' :</label>'+requiredSpan+img+'</div>';
                        }
                    }
                    // else if (frmObj.componentType == COMP_STAFF && providerInfoObject) {
                    //     var providerImage = ipAddress + "/homecare/download/providers/photo/?" + Math.round(Math.random() * 1000000) + "&access_token=" + sessionStorage.access_token + "&tenant=" + sessionStorage.tenant + "&id=" + providerInfoObject.PID;
                    //     // img = '<img src="' + imageServletUrl + '" class="p-img" />';
                    //     // providerImage = imageServletUrl;
                    //     //strHtml = strHtml+'<div><div '+clsLeftBlock+'><label class="">'+frmObj.name+' :</label>'+requiredSpan+img+'</div>';
                    // }
                    else {
                        if (frmObj.componentType != COMP_STAFF) {
                            strHtml = strHtml + '<div ' + clsLeftBlock + '><label class="">' + frmObj.name + ' :</label>' + requiredSpan + img + '</div>';
                        }
                    }

                    if (frmObj.name == "serviceuser") {
                        frmObj.name = "Service User Details";
                        strHtml = strHtml + '<div ' + clsLeftBlock + '><label class="">' + frmObj.name + ' :</label>' + requiredSpan + img + '</div>';
                    }

                    if (frmObj.componentType != COMP_STAFF) {
                        strHtml = strHtml + '<div ' + clsName + '>';
                        strHtml = strHtml + '<div class="rightblock generalInformblock">';
                    }

                }

            }
            if(frmObj.componentType == COMP_TEXT){
                strHtml = strHtml+'	<input id="'+frmId+'" type="text" class="form-control" onkeypress="return '+_.findWhere(DataTypes,{type:frmObj.dataTypeValue.toLowerCase()}).validation+'" maxlength="'+frmObj.maximumValue+'" minlength="'+frmObj.minimumValue+'"></input>';
            }
            else if(frmObj.componentType == COMP_NUMBER || frmObj.componentType == COMP_PRICE){
                strHtml = strHtml+'	<input id="'+frmId+'" type="number" class="form-control" onkeypress="return '+_.findWhere(DataTypes,{type:frmObj.dataTypeValue.toLowerCase()}).validation+'" maxlength="'+frmObj.maximumValue+'" minlength="'+frmObj.minimumValue+'"></input>';
            }else if(frmObj.componentType == COMP_TEXT_AREA){
                strHtml = strHtml+'	<textarea id="'+frmId+'"  class="form-control "onkeypress="return '+_.findWhere(DataTypes,{type:frmObj.dataTypeValue.toLowerCase()}).validation+'" maxlength="'+frmObj.maximumValue+'" minlength="'+frmObj.minimumValue+'"></textarea>';
            }else if(frmObj.componentType == COMP_DATE_PICKER){
                strHtml = strHtml+'	<input id="'+frmId+'"  class="form-control noBorder"></input>';
            }else if(frmObj.componentType == COMP_RADIO_BUTTON || frmObj.componentType == COMP_CHECK_BOX){
                var notes = frmObj.notes;
                console.log(notes);
                if(notes){
                    var notesArray = notes.split("\n");
                    strHtml = strHtml;
                    for(var m=0;m<notesArray.length;m++){
                        var mItem = notesArray[m];
                        if(mItem){
                            if(frmObj.componentType == COMP_RADIO_BUTTON){



                                strHtml = strHtml+'<div class="radio lblcustom"><label>'+mItem+'<input id="rd" type="radio" name="'+frmId+'"  value="'+mItem+'"  /><span class="radiomark"></span></label></div>';
                            }else if(frmObj.componentType == COMP_CHECK_BOX){



                                strHtml = strHtml+'<div class="checkbox lblcustom"><label>'+mItem+'<input id="chk" type="checkbox" name="'+frmId+'"  value="'+mItem+'"  /><span class="checkmark"></span></label></div>';
                            }
                            //strHtml = strHtml+'<span id="lbl">'+mItem+'</span></div>';
                        }
                    }
                    strHtml = strHtml+'</div></div>';
                }
            }
            else if(frmObj.componentType == COMP_DROPDOWN){
                var notes = frmObj.notes;
                if(notes){

                    var notesArray = notes.split("\n");
                    strHtml = strHtml+'<select id="'+frmId+'" class="form-control"><option value=""> </option>';
                    for(var m=0;m<notesArray.length;m++){
                        var mItem = notesArray[m];
                        if(mItem){
                            strHtml = strHtml+'<option  value="'+mItem+'">'+mItem+'</option>';

                            //strHtml = strHtml+'<span id="lbl">'+mItem+'</span>&nbsp;&nbsp;&nbsp;&nbsp;';
                        }
                    }
                    strHtml = strHtml+'</select></div>';

                }
            }
            else if(frmObj.componentType == COMP_EMAIL){
                strHtml = strHtml+'	<input id="'+frmId+'" type="email" class="form-control" ></input>';
            }
            else if(frmObj.componentType == COMP_LABEL){
                var style = '';
                var labelText = '';
                var notes = frmObj.notes;
                if(notes){

                    //strHtml = strHtml+'<div class="rightblock">';
                    var notesArray = notes.split("~");
                    var rightClass = notesArray[1] && notesArray[1].toLowerCase() == 'right'? '<div class="form-rightData"><div class="rightblock">' : '<div><div >'
                    strHtml = strHtml+rightClass;
                    if(notesArray[2] && notesArray[2])
                        style = 'style="color:'+notesArray[2]+';"';
                    /*if(notesArray[0] && notesArray[0].toLowerCase() == 'bold')
                        style = style+'font-weight:bold;"';
                    else if(notesArray[0])
                        style = style+'font-style:'+notesArray[0]+';"';*/
                    if(notesArray[3])
                        labelText = notesArray[3];
                }
                strHtml = strHtml + '<span id="'+frmId+'" class="labelFiled" '+style+'>'+frmObj.name+'</span>';
            }
            else if(frmObj.componentType == COMP_HLINE || frmObj.componentType.toLowerCase() =='horizontal line'){
                strHtml = strHtml + '<hr id="'+frmId+'" />';
                //break;
            }
            else if(frmObj.componentType == COMP_WEBSITE){
                strHtml = strHtml+'	<input id="'+frmId+'" type="url" class="form-control " ></input>';
            }
            else if(frmObj.componentType == COMP_PHONE){
                strHtml = strHtml+'<input id="'+frmId+'" type="tel" class="form-control " ></input>';
            }
            else if(frmObj.componentType == COMP_DATETIME){
                strHtml = strHtml+'	<input id="'+frmId+'" type="datetime-local" class="form-control " ></input>';
            }
            else if(frmObj.componentType == COMP_NAME){
                strHtml = strHtml+'<div class="namegroup"><input type="text" placeholder="First" class="form-control first" id="cff'
                    + frmId
                    + '"/></div><div class="namegroup"><input type="text" placeholder="Middle" class="form-control middle" id="cfm'
                    + frmId + '"/></div><div class="namegroup"><input type="text" placeholder="Last" class="form-control last" id="cfl'
                    + frmId + '"/></div>';
            }
            else if(frmObj.componentType == COMP_ADDRESS){
                strHtml = strHtml+'<div class="add-blocks"><input type="text"  placeholder="Address1" class="form-control add1" id="cfadd'
                    + frmId
                    + '"/></div><div class="add-blocks"><input type="text" class="form-control add2" placeholder="Address2" id="cfadd2'
                    + frmId
                    + '"/></div><div class="add-blocks" id="'+frmId+'"><div class=""><div class=""><input type="text"  class="form-control city" placeholder="City" readonly id="cfcity'
                    + frmId
                    + '"/><img id="AddbtnZipSearch'+ frmObj.id+'" class="postalWrapper-img-city AddbtnZipSearch" src="../../img/Default/search-icon.png" title="Search" /></div></div></div><div class="add-blocks"><input type="text" placeholder="State / County" class="form-control state" readonly id="cfstate'
                    + frmId
                    + '"/></div><div class="add-blocks"><input type="text" placeholder="Country" class="postal form-control" readonly id="cfcountry'
                    + frmId
                    + '"/></div><div class="add-blocks" id="'+frmId+'"><div class=""><div class="noPadding postalWrapper"><input placeholder="Postal/ZipCode" id="cmbZip'+frmId+'"  type="text" class="form-control"  /><img style="display: none" id="btnZipSearch'+frmObj.id+'" class="postalWrapper-img btnZipSearch" src="../../img/Default/search-icon.png" title="Search" /><input type="hidden" id="cityIdHiddenValue"></div></div></div>';
            }
            else if(frmObj.componentType == COMP_SERVICEUSER){
                console.log(patientInfoObject);
                if(patientInfoObject){
                    var languageVal = patientInfoObject.patient.language;
                    if(languageVal) {
                        languageVal = languageVal.split(',');
                    }
                    var cellPhone = "";
                    if(patientInfoObject.communication.cellPhone != null){
                        cellPhone = "<b>Cell Phone </b>" + patientInfoObject.communication.cellPhone;
                    }

                    var homePhone = "";
                    if(patientInfoObject.communication.homePhone != null){
                        homePhone = "<b>Home Phone </b>" + patientInfoObject.communication.homePhone;
                    }

                    var dob = dformat(new Date(patientInfoObject.patient.dateOfBirth).getTime(), 'dd-MM-yyyy');

                    // var firstRow = '<div class="col-xs-3"><label>ID:</label><input type="text" class="form-control" readonly value="'+patientInfoObject.patient.id +'" id="sid'+frmId+'" /></div><div class="col-xs-3"><label>First</label><input type="text" class="form-control" readonly value="'+patientInfoObject.patient.firstName +'" id="first'+frmId+'" /></div><div class="col-xs-3"><label>Middle</label><input type="text" class="form-control" readonly value="'+patientInfoObject.patient.middleName  +'" id="middle'+frmId+'" /></div><div class="col-xs-3"><label>Last</label><input type="text" class="form-control" readonly value="'+patientInfoObject.patient.lastName +'" id="last'+frmId+'" /></div>';
                    // var secondRow = '<div class="col-xs-3"><label>DOB:</label><input type="text" class="form-control " readonly value="'+dob +'" id="dob'+frmId+'" /></div><div class="col-xs-3"><label>Gender</label><input type="text" readonly value="'+patientInfoObject.patient.gender +'"  class="form-control" id="gender"></div><div class="col-xs-3"><label>Marrital Status</label><input type="text" class="form-control" readonly value="'+patientInfoObject.patient.maritalStatus +'" id="marritalstatus'+frmId+'" /></div><div class="col-xs-3"><label>Race</label><input type="text" value="'+patientInfoObject.patient.race +'" readonly class="form-control" id="race'+frmId+'" /></div>';
                    // var thirdRow = '<div class="col-xs-3"><label>Ethnicity</label><input type="text" readonly value="'+patientInfoObject.patient.ethnicity +'" class="form-control" id="ethnicity'+frmId+'" /></div><div class="col-xs-3"><label>Preferred Language</label><input type="text" readonly value="'+languageVal[0] +'" class="form-control" id="prelang'+frmId+'" /></div><div class="col-xs-3"><label>Second</label><input type="text" class="form-control" readonly value="'+languageVal[1] +'" id="second'+frmId+'" /></div><div class="col-xs-3"><label>Third</label><input type="text" class="form-control" value="'+languageVal[2] +'" id="third'+frmId+'" readonly /></div>';
                    // var fourthRow = '<div class="col-xs-3"><label>Address1</label><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.address1 +'" id="address1'+frmId+'" /></div><div class="col-xs-3"><label>Address2</label><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.address2 +'" id="address2'+frmId+'" /></div><div class="col-xs-3"><label>City</label><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.city +'" id="city'+frmId+'" /></div><div class="col-xs-3"><label>State/County</label><input type="text" value="'+patientInfoObject.communication.state +'" class="form-control" readonly id="state'+frmId+'" /></div>';
                    // var fifthRow = '<div class="col-xs-3"><label>Country</label><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.country +'" id="country'+frmId+'" /></div><div class="col-xs-3"><label>Postal/Zip Code</label><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.zip +'" id="postal'+frmId+'" /></div><div class="col-xs-3"><label>Home Phone</label><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.homePhone +'" id="homephone'+frmId+'" /></div><div class="col-xs-3"><label>Mobile</label><input type="text" value="'+patientInfoObject.communication.cellPhone +'" readonly class="form-control" id="mobile'+frmId+'" /></div>';
                    // var sixthRow = '<div class="col-xs-3"><label>Work Phone</label><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.workPhone +'" id="workphone'+frmId+'" /></div><div class="col-xs-3"><label>Email</label><input type="text" readonly value="'+patientInfoObject.communication.email +'" class="form-control" id="eamil'+frmId+'" /></div><div class="col-xs-3"><label>SMS</label><input type="text" class="form-control" readonly value="'+patientInfoObject.communication.sms +'"  id="sms'+frmId+'" /></div>';
                    var firstRow = '<div class="col-xs-3"><span class="form-control" id="last'+frmId+'">'+patientInfoObject.patient.lastName+'</span>&nbsp;<label>Last</label></div><div class="col-xs-3"><span class="form-control">'+' '+patientInfoObject.patient.firstName+' </span>&nbsp;<label>First</label></div><div class="col-xs-3"><span class="form-control" id="middle'+frmId+'">'+' '+patientInfoObject.patient.middleName+'</span>&nbsp;<label>Middle</label></div><br/>';
                    var secondRow = '<div class="col-xs-3"></div><div class="col-xs-3"><span class="form-control input-sm" id="dob'+frmId+'">'+dob+'</span>&nbsp;<label>DOB:</label></div><div class="col-xs-3"><span class="form-control" id="gender">'+patientInfoObject.patient.gender +'</span><label style="display:none">Gender</label></div><br clear="all"/>';

                    var fourthRow = '<div class="col-xs-3"></div><div class="col-xs-3"><span class="form-control" id="address1'+frmId+'">'+patientInfoObject.communication.address1 +'</span>&nbsp;<label>Address1</label></div><div class="col-xs-3"><span class="form-control" id="address2'+frmId+'">'+patientInfoObject.communication.address2 +'</span>&nbsp;<label>Address2</label></div><div class="col-xs-3"><span class="form-control" id="city'+frmId+'">'+patientInfoObject.communication.city+' </span>&nbsp;<label>City</label></div></br><div class="col-xs-3"><span class="form-control" id="state'+frmId+'">'+patientInfoObject.communication.state +'</span>&nbsp;<label>State/County</label></div>';
                    var fifthRow = '<div class="col-xs-3"><span class="form-control" id="country'+frmId+'">'+patientInfoObject.communication.country+'</span><label>Country</label></div></br><div class="col-xs-3"><span class="form-control" id="homephone'+frmId+'">'+homePhone+'</span><label>Home Phone</label></div></br><div class="col-xs-3"><span class="form-control" id="mobile'+frmId+'">'+cellPhone+'</span><label>Cell Phone</label></div>';
                    // var sixthRow = '<div class="col-xs-3"><span class="form-control" id="workphone'+frmId+'">'+patientInfoObject.communication.workPhone +'</span><label>Work Phone</label></div><div class="col-xs-3"><span type="text" class="form-control" id="eamil'+frmId+'">'+patientInfoObject.communication.email+'</span><label>Email</label></div><div class="col-xs-3"><span class="form-control" id="sms'+frmId+'">'+patientInfoObject.communication.sms+'</span><label>SMS</label></div>';
                    var ele = firstRow + secondRow+ fourthRow + fifthRow + '</div>';
                    strHtml = strHtml + ele ;
                    // rightside= ele;

                }
                if(providerInfoObject){
                    var languageVal = providerInfoObject.language;
                    if(languageVal) {
                        languageVal = languageVal.split(',');
                    }

                    var cellPhone = "";
                    if(providerInfoObject.communications[0].cellPhone != undefined && providerInfoObject.communications[0].cellPhone != null && providerInfoObject.communications[0].cellPhone != ''){
                        cellPhone = "<b>Cell Phone </b>" + providerInfoObject.communications[0].cellPhone;
                    }

                    var homePhone = "";
                    if(providerInfoObject.communications[0].homePhone != null && providerInfoObject.communications[0].homePhone != ''){
                        homePhone = "<b>Home Phone </b>" + providerInfoObject.communications[0].homePhone;
                    }


                    var dob = dformat(new Date(providerInfoObject.dateOfBirth).getTime(), 'dd-MM-yyyy');

                    // var firstRow = '<div class="col-xs-3"><div class="col-xs-6"><input type="text" class="form-control" readonly value="'+providerInfoObject.PID +'" id="sid'+frmId+'"/><label>ID:</label></div><div class="col-xs-6" style="padding-right:0;"><input type="text" class="form-control" readonly value="'+providerInfoObject.prefix +'" id="sid'+frmId+'" /><label>Prefix:</label></div></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.firstName +'" id="first'+frmId+'" /><label>First</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.lastName +'" id="last'+frmId+'" /><label>Last</label></div>';
                    // var secondRow = '<div class="col-xs-3"><input type="text" class="form-control " readonly value="'+dob +'" id="dob'+frmId+'" /><label>DOB:</label></div><div class="col-xs-3"><input type="text" readonly value="'+providerInfoObject.gender +'"  class="form-control" id="gender">Gender</label></div>';
                    // var thirdRow = '<div class="col-xs-3"><input type="text" readonly value="'+languageVal[0] +'" class="form-control" id="prelang'+frmId+'" /><label>Preferred Language</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+(languageVal[1] || '') +'" id="second'+frmId+'" /><label>Second</label></div><div class="col-xs-3"><input type="text" class="form-control" value="'+(languageVal[2] || '') +'" id="third'+frmId+'" readonly /><label>Third</label></div>';
                    // var fourthRow = '<div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.communications[0].address1 +'" id="address1'+frmId+'" /><label>Address1</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.communications[0].address2 +'" id="address2'+frmId+'" /><label>Address2</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.communications[0].city +'" id="city'+frmId+'" /><label>City</label></div><div class="col-xs-3"><input type="text" value="'+providerInfoObject.communications[0].state +'" class="form-control" readonly id="state'+frmId+'" /><label>State/County</label></div>';
                    // var fifthRow = '<div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.communications[0].country +'" id="country'+frmId+'" /><label>Country</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.communications[0].zip +'" id="postal'+frmId+'" /><label>Postal/Zip Code</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.communications[0].homePhone +'" id="homephone'+frmId+'" /><label>Home Phone</label></div><div class="col-xs-3"><input type="text" value="'+providerInfoObject.communications[0].cellPhone +'" readonly class="form-control" id="mobile'+frmId+'" /><label>Mobile</label></div>';
                    // var sixthRow = '<div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.communications[0].workPhone +'" id="workphone'+frmId+'" /><label>Work Phone</label></div><div class="col-xs-3"><input type="text" readonly value="'+providerInfoObject.communications[0].email +'" class="form-control" id="eamil'+frmId+'" /><label>Email</label></div>';
                    // 	var firstRow = '<div class="col-xs-3"><div class="col-xs-6"><span class="form-control" id="sid'+frmId+'">'+providerInfoObject.PID+'</span><label>ID:</label></div><div class="col-xs-6" style="padding-right:0;"><span class="form-control"  id="sid'+frmId+'" >providerInfoObject.prefix </span><label>Prefix:</label></div></div><div class="col-xs-3"><span class="form-control" id="first'+frmId+'">'+providerInfoObject.firstName+'</span>><label>First</label></div><div class="col-xs-3"><span class="form-control" id="last'+frmId+'">'+providerInfoObject.lastName+'</span><label>Last</label></div>';
                    // 	var secondRow = '<div class="col-xs-3"><span class="form-control input-sm" id="dob'+frmId+'">'+dob+'</span><label>DOB:</label></div><div class="col-xs-3"><span class="form-control" id="gender">'+providerInfoObject.gender +'</span><label>Gender</label></div>';
                    //
                    // 	var thirdRow = '<div class="col-xs-3"><span class="form-control" id="prelang'+frmId+'">'+languageVal[0] +'</span><label>Preferred Language</label></div><div class="col-xs-3"><span class="form-control" id="second'+frmId+'">'+(languageVal[1] || '')+'</span><label>Second</label></div><div class="col-xs-3"><span class="form-control" id="third'+frmId+'">'+(languageVal[2] || '') +'</span><label>Third</label></div>';
                    // var fourthRow = '<div class="col-xs-3"><span class="form-control" id="address1'+frmId+'">'+providerInfoObject.communication.address1 +'</span><label>Address1</label></div><div class="col-xs-3"><span class="form-control" id="address2'+frmId+'">'+providerInfoObject.communication.address2 +'</span><label>Address2</label></div><div class="col-xs-3"><span class="form-control" id="city'+frmId+'">'+providerInfoObject.communication.city+' </span><label>City</label></div><div class="col-xs-3"><span class="form-control" id="state'+frmId+'">'+providerInfoObject.communication.state +'</span><label>State/County</label></div>';
                    // var fifthRow = '<div class="col-xs-3"><span class="form-control" id="country'+frmId+'">'+providerInfoObject.communication.country+'</span><label>Country</label></div><div class="col-xs-3"><span class="form-control" id="postal'+frmId+'">'+providerInfoObject.communication.zip+'</span><label>Postal/Zip Code</label></div><div class="col-xs-3"><span class="form-control" id="homephone'+frmId+'">'+providerInfoObject.communication.homePhone+'</span><label>Home Phone</label></div><div class="col-xs-3"><span value="'+providerInfoObject.communication.cellPhone +'" class="form-control" id="mobile'+frmId+'">'+providerInfoObject.communication.cellPhone+'</span><label>Mobile</label></div>';
                    // var sixthRow = '<div class="col-xs-3"><span class="form-control" id="workphone'+frmId+'">'+providerInfoObject.communication.workPhone +'</span><label>Work Phone</label></div><div class="col-xs-3"><span type="text" class="form-control" id="eamil'+frmId+'">'+providerInfoObject.communication.email+'</span><label>Email</label></div><div class="col-xs-3"><span class="form-control" id="sms'+frmId+'">'+providerInfoObject.communication.sms+'</span><label>SMS</label></div>';

                    var firstRow = '<div class="col-xs-3"><span class="form-control" id="last'+frmId+'">'+providerInfoObject.lastName+'</span>&nbsp;<label>Last</label></div><div class="col-xs-3"><span class="form-control">'+' '+providerInfoObject.firstName+' </span>&nbsp;<label>First</label></div><div class="col-xs-3"><span class="form-control" id="middle'+frmId+'">'+' '+providerInfoObject.middleName+'</span>&nbsp;<label>Middle</label></div><br/>';
                    var secondRow = '<div class="col-xs-3"></div><div class="col-xs-3"><span class="form-control input-sm" id="dob'+frmId+'">'+dob+'</span>&nbsp;<label>DOB:</label></div><div class="col-xs-3"><span class="form-control" id="gender">'+providerInfoObject.gender +'</span><label style="display:none">Gender</label></div><br clear="all"/>';

                    var fourthRow = '<div class="col-xs-3"></div><div class="col-xs-3"><span class="form-control" id="address1'+frmId+'">'+providerInfoObject.communications[0].address1 +'</span>&nbsp;<label>Address1</label></div><div class="col-xs-3"><span class="form-control" id="address2'+frmId+'">'+providerInfoObject.communications[0].address2 +'</span>&nbsp;<label>Address2</label></div><div class="col-xs-3"><span class="form-control" id="city'+frmId+'">'+providerInfoObject.communications[0].city+' </span>&nbsp;<label>City</label></div></br><div class="col-xs-3"><span class="form-control" id="state'+frmId+'">'+providerInfoObject.communications[0].state +'</span>&nbsp;<label>State/County</label></div>';
                    var fifthRow = '<div class="col-xs-3"><span class="form-control" id="country'+frmId+'">'+providerInfoObject.communications[0].country+'</span><label>Country</label></div></br><div class="col-xs-3"><span class="form-control" id="homephone'+frmId+'">'+homePhone+'</span><label>Home Phone</label></div></br><div class="col-xs-3"><span class="form-control" id="mobile'+frmId+'">'+cellPhone+'</span><label>Cell Phone</label></div>';

                    var ele = firstRow + secondRow+ fourthRow + fifthRow;
                    //strHtml = strHtml + ele;
                    staffHTML = ele;
                }
            }
            // else if(frmObj.componentType == COMP_SERVICEUSER){
            // 	console.log(providerInfoObject);
            // 	if(providerInfoObject){
            //     var languageVal = providerInfoObject.language;
            //     if(languageVal) {
            //         languageVal = languageVal.split(',');
            //     }
            //
            // 	var cellPhone = "";
            // 	if(providerInfoObject.communications[0].cellPhone != undefined && providerInfoObject.communications[0].cellPhone != null){
            // 		cellPhone = "<b>Cell Phone </b>" + providerInfoObject.communications[0].cellPhone;
            // 	}
            //
            // 	var homePhone = "";
            // 	if(providerInfoObject.communications[0].homePhone != null){
            // 		homePhone = "<b>Home Phone </b>" + providerInfoObject.communications[0].homePhone;
            // 	}
            //
            //
            // 	var dob = dformat(new Date(providerInfoObject.dateOfBirth).getTime(), 'dd-MM-yyyy');
            //
            // 	// var firstRow = '<div class="col-xs-3"><div class="col-xs-6"><input type="text" class="form-control" readonly value="'+providerInfoObject.PID +'" id="sid'+frmId+'"/><label>ID:</label></div><div class="col-xs-6" style="padding-right:0;"><input type="text" class="form-control" readonly value="'+providerInfoObject.prefix +'" id="sid'+frmId+'" /><label>Prefix:</label></div></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.firstName +'" id="first'+frmId+'" /><label>First</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.lastName +'" id="last'+frmId+'" /><label>Last</label></div>';
            // 	// var secondRow = '<div class="col-xs-3"><input type="text" class="form-control " readonly value="'+dob +'" id="dob'+frmId+'" /><label>DOB:</label></div><div class="col-xs-3"><input type="text" readonly value="'+providerInfoObject.gender +'"  class="form-control" id="gender">Gender</label></div>';
            // 	// var thirdRow = '<div class="col-xs-3"><input type="text" readonly value="'+languageVal[0] +'" class="form-control" id="prelang'+frmId+'" /><label>Preferred Language</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+(languageVal[1] || '') +'" id="second'+frmId+'" /><label>Second</label></div><div class="col-xs-3"><input type="text" class="form-control" value="'+(languageVal[2] || '') +'" id="third'+frmId+'" readonly /><label>Third</label></div>';
            // 	// var fourthRow = '<div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.communications[0].address1 +'" id="address1'+frmId+'" /><label>Address1</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.communications[0].address2 +'" id="address2'+frmId+'" /><label>Address2</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.communications[0].city +'" id="city'+frmId+'" /><label>City</label></div><div class="col-xs-3"><input type="text" value="'+providerInfoObject.communications[0].state +'" class="form-control" readonly id="state'+frmId+'" /><label>State/County</label></div>';
            // 	// var fifthRow = '<div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.communications[0].country +'" id="country'+frmId+'" /><label>Country</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.communications[0].zip +'" id="postal'+frmId+'" /><label>Postal/Zip Code</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.communications[0].homePhone +'" id="homephone'+frmId+'" /><label>Home Phone</label></div><div class="col-xs-3"><input type="text" value="'+providerInfoObject.communications[0].cellPhone +'" readonly class="form-control" id="mobile'+frmId+'" /><label>Mobile</label></div>';
            // 	// var sixthRow = '<div class="col-xs-3"><input type="text" class="form-control" readonly value="'+providerInfoObject.communications[0].workPhone +'" id="workphone'+frmId+'" /><label>Work Phone</label></div><div class="col-xs-3"><input type="text" readonly value="'+providerInfoObject.communications[0].email +'" class="form-control" id="eamil'+frmId+'" /><label>Email</label></div>';
            // 	// 	var firstRow = '<div class="col-xs-3"><div class="col-xs-6"><span class="form-control" id="sid'+frmId+'">'+providerInfoObject.PID+'</span><label>ID:</label></div><div class="col-xs-6" style="padding-right:0;"><span class="form-control"  id="sid'+frmId+'" >providerInfoObject.prefix </span><label>Prefix:</label></div></div><div class="col-xs-3"><span class="form-control" id="first'+frmId+'">'+providerInfoObject.firstName+'</span>><label>First</label></div><div class="col-xs-3"><span class="form-control" id="last'+frmId+'">'+providerInfoObject.lastName+'</span><label>Last</label></div>';
            // 	// 	var secondRow = '<div class="col-xs-3"><span class="form-control input-sm" id="dob'+frmId+'">'+dob+'</span><label>DOB:</label></div><div class="col-xs-3"><span class="form-control" id="gender">'+providerInfoObject.gender +'</span><label>Gender</label></div>';
            // 	//
            // 	// 	var thirdRow = '<div class="col-xs-3"><span class="form-control" id="prelang'+frmId+'">'+languageVal[0] +'</span><label>Preferred Language</label></div><div class="col-xs-3"><span class="form-control" id="second'+frmId+'">'+(languageVal[1] || '')+'</span><label>Second</label></div><div class="col-xs-3"><span class="form-control" id="third'+frmId+'">'+(languageVal[2] || '') +'</span><label>Third</label></div>';
            // 		// var fourthRow = '<div class="col-xs-3"><span class="form-control" id="address1'+frmId+'">'+providerInfoObject.communication.address1 +'</span><label>Address1</label></div><div class="col-xs-3"><span class="form-control" id="address2'+frmId+'">'+providerInfoObject.communication.address2 +'</span><label>Address2</label></div><div class="col-xs-3"><span class="form-control" id="city'+frmId+'">'+providerInfoObject.communication.city+' </span><label>City</label></div><div class="col-xs-3"><span class="form-control" id="state'+frmId+'">'+providerInfoObject.communication.state +'</span><label>State/County</label></div>';
            // 		// var fifthRow = '<div class="col-xs-3"><span class="form-control" id="country'+frmId+'">'+providerInfoObject.communication.country+'</span><label>Country</label></div><div class="col-xs-3"><span class="form-control" id="postal'+frmId+'">'+providerInfoObject.communication.zip+'</span><label>Postal/Zip Code</label></div><div class="col-xs-3"><span class="form-control" id="homephone'+frmId+'">'+providerInfoObject.communication.homePhone+'</span><label>Home Phone</label></div><div class="col-xs-3"><span value="'+providerInfoObject.communication.cellPhone +'" class="form-control" id="mobile'+frmId+'">'+providerInfoObject.communication.cellPhone+'</span><label>Mobile</label></div>';
            // 		// var sixthRow = '<div class="col-xs-3"><span class="form-control" id="workphone'+frmId+'">'+providerInfoObject.communication.workPhone +'</span><label>Work Phone</label></div><div class="col-xs-3"><span type="text" class="form-control" id="eamil'+frmId+'">'+providerInfoObject.communication.email+'</span><label>Email</label></div><div class="col-xs-3"><span class="form-control" id="sms'+frmId+'">'+providerInfoObject.communication.sms+'</span><label>SMS</label></div>';
            //
            //         var firstRow = '<div class="col-xs-3"><span class="form-control" id="last'+frmId+'">'+providerInfoObject.lastName+'</span>&nbsp;<label>Last</label></div><div class="col-xs-3"><span class="form-control">'+' '+providerInfoObject.firstName+' </span>&nbsp;<label>First</label></div><div class="col-xs-3"><span class="form-control" id="middle'+frmId+'">'+' '+providerInfoObject.middleName+'</span>&nbsp;<label>Middle</label></div><br/>';
            //         var secondRow = '<div class="col-xs-3"></div><div class="col-xs-3"><span class="form-control input-sm" id="dob'+frmId+'">'+dob+'</span>&nbsp;<label>DOB:</label></div><div class="col-xs-3"><span class="form-control" id="gender">'+providerInfoObject.gender +'</span><label style="display:none">Gender</label></div><br clear="all"/>';
            //
            //         var fourthRow = '<div class="col-xs-3"></div><div class="col-xs-3"><span class="form-control" id="address1'+frmId+'">'+providerInfoObject.communications[0].address1 +'</span>&nbsp;<label>Address1</label></div><div class="col-xs-3"><span class="form-control" id="address2'+frmId+'">'+providerInfoObject.communications[0].address2 +'</span>&nbsp;<label>Address2</label></div><div class="col-xs-3"><span class="form-control" id="city'+frmId+'">'+providerInfoObject.communications[0].city+' </span>&nbsp;<label>City</label></div></br><div class="col-xs-3"><span class="form-control" id="state'+frmId+'">'+providerInfoObject.communications[0].state +'</span>&nbsp;<label>State/County</label></div>';
            //         var fifthRow = '<div class="col-xs-3"><span class="form-control" id="country'+frmId+'">'+providerInfoObject.communications[0].country+'</span><label>Country</label></div></br><div class="col-xs-3"><span class="form-control" id="homephone'+frmId+'">'+homePhone+'</span><label>Home Phone</label></div></br><div class="col-xs-3"><span class="form-control" id="mobile'+frmId+'">'+cellPhone+'</span><label>Cell Phone</label></div>';
            //
            // 	var ele = firstRow + secondRow+ fourthRow + fifthRow;
            // 	//strHtml = strHtml + ele;
            //         staffHTML = ele;
            // }
            // 	/*else{
            // 		 //var dob = dformat(new Date(providerInfoObject.dateOfBirth).getTime(), 'MM/dd/yyyy');
            //
            // 			var firstRow = '<div class="col-xs-3"><div class="col-xs-6"><input type="text" class="form-control" readonly value="" id="sid'+frmId+'"/><label>ID:</label></div><div class="col-xs-6" style="padding-right:0;"><input type="text" class="form-control" readonly value="" id="sid'+frmId+'" /><label>Prefix:</label></div></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="" id="first'+frmId+'" /><label>First</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="" id="last'+frmId+'" /><label>Last</label></div>';
            // 			var secondRow = '<div class="col-xs-3"><input type="text" class="form-control " readonly value="" id="dob'+frmId+'" /><label>DOB:</label></div><div class="col-xs-3"><input type="text" readonly value=""  class="form-control" id="gender">Gender</label></div>';
            // 			var thirdRow = '<div class="col-xs-3"><input type="text" readonly value="" class="form-control" id="prelang'+frmId+'" /><label>Preferred Language</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="" id="second'+frmId+'" /><label>Second</label></div><div class="col-xs-3"><input type="text" class="form-control" value="" id="third'+frmId+'" readonly /><label>Third</label></div>';
            // 			var fourthRow = '<div class="col-xs-3"><input type="text" class="form-control" readonly value="" id="address1'+frmId+'" /><label>Address1</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="" id="address2'+frmId+'" /><label>Address2</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="" id="city'+frmId+'" /><label>City</label></div><div class="col-xs-3"><input type="text" value="" class="form-control" readonly id="state'+frmId+'" /><label>State/County</label></div>';
            // 			var fifthRow = '<div class="col-xs-3"><input type="text" class="form-control" readonly value="" id="country'+frmId+'" /><label>Country</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="" id="postal'+frmId+'" /><label>Postal/Zip Code</label></div><div class="col-xs-3"><input type="text" class="form-control" readonly value="" id="homephone'+frmId+'" /><label>Home Phone</label></div><div class="col-xs-3"><input type="text" value="" readonly class="form-control" id="mobile'+frmId+'" /><label>Mobile</label></div>';
            // 			var sixthRow = '<div class="col-xs-3"><input type="text" class="form-control" readonly value="" id="workphone'+frmId+'" /><label>Work Phone</label></div><div class="col-xs-3"><input type="text" readonly value="" class="form-control" id="eamil'+frmId+'" /><label>Email</label></div>';
            // 			var ele = firstRow + secondRow+ thirdRow + fourthRow + fifthRow + sixthRow;
            // 			strHtml = strHtml + ele;
            // 	}*/
            // }
            else if(frmObj.componentType == COMP_LIKERT){
                strHtml = strHtml+'	<input id="'+frmId+'" type="datetime-local" class="form-control "></input>';
            }
            else if(frmObj.componentType == COMP_RATING){
                strHtml = strHtml+'<div class="customfields crating maindiv" id="crating'
                    + frmId
                    + '"><span class="star" value="1" tabindex="1" onclick="setRating(1,\''+ frmId+'\')"><i class="fa fa-star fa-2x" aria-hidden="true"></i></span><span class="star" value="1" tabindex="1" onclick="setRating(2,\''+ frmId+'\')"><i class="fa fa-star fa-2x" aria-hidden="true"></i></span><span class="star" value="1" tabindex="1" onclick="setRating(3,\''+ frmId+'\')"><i class="fa fa-star fa-2x" aria-hidden="true"></i></span><span class="star" value="1" tabindex="1" onclick="setRating(4,\''+ frmId+'\')"><i class="fa fa-star fa-2x" aria-hidden="true"></i></span><span class="star" value="1" tabindex="1" onclick="setRating(5,\''+ frmId+'\')"><i class="fa fa-star fa-2x" aria-hidden="true"></i></span></div>';
            }

            if(frmObj.componentType == COMP_HLINE || frmObj.componentType.toLowerCase() =='horizontal line')
            {
                strHtml = strHtml+'</div>';
            }
            // else if(frmObj.componentType == COMP_STAFF || frmObj.componentType == COMP_SERVICEUSER){
            //     strHtml = strHtml + '<div>ele</div>';
            // }
            else{
                if (frmObj.componentType != COMP_STAFF) {
                    strHtml = strHtml + '<label class="units">' + unit + '</label>';
                    if (hint && hint.length > 0)
                        strHtml = strHtml + '<span class="hint">' + hint + '</label>';
                    var col3 = '';
                    if (curMiniTemplate && curMiniTemplate.label == 3) {

                        var cinput = '<input type="text" class="form-control " id="fcol' + frmId + '" style="width:100%;"/>';
                        col3 = '<div class="col3 thirdDiv">' + (frmObj.label == '3' ? cinput : '') + '</div>';
                    }
                }
                if(frmObj.componentType == COMP_SERVICEUSER){

                    if(!providerInfoObject){
                        var dt = new Date();
                        dt = kendo.toString(dt, "dd-MM-yyyy h:mm:tt");

                        var fullDate = dt;

                        rightside = '<div class="col-xs-12"><div><div class="leftBlock">' +
                            '<label class="col-heading">Date Time When Filled Form :</label>' +
                            '<span class="" id="rDateTime">'+fullDate+'</span>' +
                            '</div></div></div>';
                    }
                    else{
                        // rightside = '<div class="col-xs-12 popupformElements"><div><div class="leftBlock"><label class="">staff :</label><img src="https://stage.timeam.com/homecare/download/providers/photo/?571838&amp;access_token=c161d936-70ab-4aa4-ba18-482089a79a69&amp;tenant=homecare1&amp;id=13" class="p-img"></div><div class="form-rightData"><div class="rightblock generalInformblock"><div class="col-xs-3"><div class="col-xs-6"><span class="form-control" id="sidf1042">13</span><label>ID:</label></div><div class="col-xs-6" style="padding-right:0;"><span class="form-control" id="sidf1042">providerInfoObject.prefix </span><label>Prefix:</label></div></div><div class="col-xs-3"><span class="form-control" id="firstf1042">Provider</span>&gt;<label>First</label></div><div class="col-xs-3"><span class="form-control" id="lastf1042">Provider</span><label>Last</label></div><div class="col-xs-3"><span class="form-control input-sm" id="dobf1042">12/26/1965</span><label>DOB:</label></div><div class="col-xs-3"><span class="form-control" id="gender">Female</span><label>Gender</label></div><div class="col-xs-3"><span class="form-control" id="prelangf1042">English</span><label>Preferred Language</label></div><div class="col-xs-3"><span class="form-control" id="secondf1042"> Spanish</span><label>Second</label></div><div class="col-xs-3"><span class="form-control" id="thirdf1042"> Arabic</span><label>Third</label></div><label class="units"></label></div></div><div class="addformservicerightdiv">fdf</div></div></div>';
                        // rightside = '<div class="col-xs-12 popupformElements"><div><div class="leftBlock"><label class="">staff :</label><img src='+providerImage+' class="p-img"></div><div class="form-rightData"><div class="rightblock generalInformblock"><div class="col-xs-3"><div class="col-xs-6"><span class="form-control" id="sidf1042">13</span><label>ID:</label></div><div class="col-xs-6" style="padding-right:0;"><span class="form-control" id="sidf1042">providerInfoObject.prefix </span><label>Prefix:</label></div></div><div class="col-xs-3"><span class="form-control" id="firstf1042">Provider</span>&gt;<label>First</label></div><div class="col-xs-3"><span class="form-control" id="lastf1042">Provider</span><label>Last</label></div><div class="col-xs-3"><span class="form-control input-sm" id="dobf1042">12/26/1965</span><label>DOB:</label></div><div class="col-xs-3"><span class="form-control" id="gender">Female</span><label>Gender</label></div><div class="col-xs-3"><span class="form-control" id="prelangf1042">English</span><label>Preferred Language</label></div><div class="col-xs-3"><span class="form-control" id="secondf1042"> Spanish</span><label>Second</label></div><div class="col-xs-3"><span class="form-control" id="thirdf1042"> Arabic</span><label>Third</label></div></div></div></div></div>';
                        // rightside = '<div class="col-xs-12 popupformElements"><div><div class="leftBlock"><label class="">staff :</label><img src='+providerImage+' class="p-img"></div><div class="form-rightData"><div class="rightblock generalInformblock"><div class="col-xs-3"><div class="col-xs-6"><span class="form-control" id="sidf1042">13</span><label>ID:</label></div><div class="col-xs-6" style="padding-right:0;"><span class="form-control" id="sidf1042">providerInfoObject.prefix </span><label>Prefix:</label></div></div><div class="col-xs-3"><span class="form-control" id="firstf1042">Provider</span>&gt;<label>First</label></div><div class="col-xs-3"><span class="form-control" id="lastf1042">Provider</span><label>Last</label></div><div class="col-xs-3"><span class="form-control input-sm" id="dobf1042">12/26/1965</span><label>DOB:</label></div><div class="col-xs-3"><span class="form-control" id="gender">Female</span><label>Gender</label></div><div class="col-xs-3"><span class="form-control" id="prelangf1042">English</span><label>Preferred Language</label></div><div class="col-xs-3"><span class="form-control" id="secondf1042"> Spanish</span><label>Second</label></div><div class="col-xs-3"><span class="form-control" id="thirdf1042"> Arabic</span><label>Third</label></div></div></div></div></div>';
                        rightside = '<div class="col-xs-12 popupformElements noBorder"><div><div class="leftBlock"><label class="col-heading">Staff Details :</label><img src='+ providerImage +' class="p-img"></div><div class="form-rightData"><div class="rightblock generalInformblock">'+staffHTML+'</div></div></div></div>';
                    }
                    strHtml = strHtml + col3 +'</div></div><div class="addformserviceleftdiv">'+rightside+'</div></div>';
                }
                else{
                    if(frmObj.componentType != COMP_STAFF) {
                        strHtml = strHtml + col3 + '</div></div></div>';
                    }
                    // strHtml = strHtml + '</div></div>' + col3 + '</div>';
                }
            }
        }
        var btnId = "b"+currId;
        strHtml = strHtml+'<div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 form-group">';
        strHtml = strHtml+'<div class="navBar" style="padding-bottom:2px">';
        strHtml = strHtml+'</div>';
        //strHtml = strHtml+'<button id="'+btnId+'" class="btn-save" btnId="'+currId+'" style="display:none;">Save</button></div>';

        $("#p"+currId).html(strHtml);
        $("#saveMiniTemplate").off("click");
        $("#saveMiniTemplate").on("click",onClickSave);

        $("#"+btnId).off("click");
        $("#"+btnId).on("click",onClickSave);

        setTimeout(function(){
            for(var k=0;k<frmDesignArray.length;k++){
                var frmObj = frmDesignArray[k];
                frmObj.idk = frmObj.id;
                frmObj.length1 = frmObj.length;
                var frmId = "f"+frmObj.idk;
                if(frmObj.componentType == COMP_DATE_PICKER){
                    if(frmObj.dataTypeValue == DT_DATE){
                        $("#"+frmId).kendoDatePicker({ format: "dd-MM-yyyy"});
                    }else if(frmObj.dataTypeValue == DT_DATETIME){
                        $("#"+frmId).kendoDateTimePicker();
                    }
                }
            }
        },100);
        $("#btnAdd").off("click");
        $("#btnAdd").on("click",onClickAdd);
        if(addressComp.length){
            for(var i=0;i<addressComp.length;i++){
                //if(addressComp[i].componentTypeId == 12)
                $("#btnZipSearch"+addressComp[i].id).off("click");
                $("#btnZipSearch"+addressComp[i].id).on("click",onClickZipSearch);
                //addressComp.push(addressComp[i].id);
                $("#AddbtnZipSearch"+addressComp[i].id).off("click");
                $("#AddbtnZipSearch"+addressComp[i].id).on("click", onClickZipSearch);
            }
        }
        console.log(addressComp);

        /*$("#btnSave").off("click",onClickSave);
        $("#btnSave").on("click",onClickSave);*/

        /*$("#btnZipSearch").off("click");
        $("#btnZipSearch").on("click",onClickZipSearch);*/

        /*$(".btnZipSearch").off("click");
        $(".btnZipSearch").on("click",onClickZipSearch);*/

        // $("#AddbtnZipSearch").off("click");
        // $("#AddbtnZipSearch").on("click", onClickAdditionalZipSearch);
    }
}
function startChange(e){
    console.log(e);
}

function onClickFormSave(evt){
    if($("#txtNotes").val() != '') {
        $("#notes-mandatory-field").css("display","none");
        var fromArray = [];
        for (var j = 0; j < miniTemplates.length; j++) {
            found = true;
            // for(var k=0;k<frmDesignArrayById.length;k++)
            // {
            // 	if(miniTemplates[j].id == frmDesignArrayById[k].id)
            // 		break;
            // 	else{
            // 		found = false;
            // 	}
            // }
            if (miniTemplates[j].isDeleted == 0) {
                var obj = {};
                obj.id = miniTemplates[j].id;
                obj.components = miniTemplates[j].componenets || [];
                for (var c = 0; c < obj.components.length; c++) {
                    if(obj.components[c].isDeleted == 0 && obj.components[c].isActive == 1) {
                        obj.components[c].idk = obj.components[c].id;
                    }
                }
                frmDesignArrayById.push(obj);

            }

        }




        var tempId = selItem.idk;
        var frmTempObj = getFormTemplateUserValueArray(tempId);
        var method = "POST";
        var frmIdk = "";
        var canProceed = false;
        var minComponents = [];
        if (frmTempObj) {
            method = "PUT";
            frmIdk = frmTempObj.id;
            components = frmTempObj.componentValues;
        }
        var btnId = evt.currentTarget;
        btnId = $(btnId).attr("btnId");


        var miniCompArray = [];
        for (var g = 0; g < frmDesignArrayById.length; g++) {
            fromArray = frmDesignArrayById[g].components;
            console.log(fromArray);
            for (var f = 0; f < fromArray.length; f++) {

                var frmObj = fromArray[f];
                console.log(frmObj);
                if (fromArray[f].idk) {
                    var fId = fromArray[f].idk;
                    fId = "f" + fId;
                    var fData = "";
                    if (frmObj.componentType == COMP_DATE_PICKER) {
                        if (frmObj.dataTypeValue == DT_DATE) {
                            var cmbDate = $("#" + fId).data("kendoDatePicker");
                            fData = cmbDate.value();
                        } else if (frmObj.dataTypeValue == DT_DATETIME) {
                            var cmbDate = $("#" + fId).data("kendoDateTimePicker");
                            fData = cmbDate.value();
                        }
                    } else if (frmObj.componentType == COMP_TEXT || frmObj.componentType == COMP_TEXT_AREA) {
                        fData = $("#" + fId).val();
                    } else if (frmObj.componentType == COMP_CHECK_BOX || frmObj.componentType == COMP_RADIO_BUTTON) {
                        if (frmObj.componentType == COMP_CHECK_BOX) {
                            fData = $('input[name=' + fId + ']').val();
                        } else if (frmObj.componentType == COMP_RADIO_BUTTON) {
                            fData = $("input[type='radio'][name=" + fId + "]:checked").val();
                        }

                    }
                    else if (frmObj.componentType == COMP_DROPDOWN) {
                        fData = $("#" + fId).val();
                    }
                    else if (frmObj.componentType == COMP_EMAIL) {
                        fData = $("#" + fId).val();
                    }
                    else if (frmObj.componentType == COMP_PHONE) {
                        fData = $("#" + fId).val();
                    }
                    else if (frmObj.componentType == COMP_PRICE || frmObj.componentType == COMP_NUMBER) {
                        fData = $("#" + fId).val();
                    }
                    else if (frmObj.componentType == COMP_WEBSITE) {
                        fData = $("#" + fId).val();
                    }
                    else if (frmObj.componentType == COMP_NAME) {
                        fData = $("#cff" + fId).val() + '~' + $("#cfm" + fId).val() + '~' + $("#cfl" + fId).val();
                    }
                    else if (frmObj.componentType == COMP_GROUP_CHECK_BOX) {
                        var delimiter = "~";
                        fData = "";
                        $.each($("input[name=" + fId + "]:checked"), function () {
                            fData += $(this).val() + delimiter;
                        });
                        if ('~' == fData[fData.length - 1])
                            fData = fData.slice(0, -1);
                    }
                    else if (frmObj.componentType == COMP_DATETIME) {
                        var dateVal = Date.parse($("#" + fId).val());
                        fData = dateVal;
                    }
                    else if (frmObj.componentType == COMP_ADDRESS) {
                        fData = $("#cfadd" + fId).val() + '~' + $("#cfadd2" + fId).val() + '~' + $("#cfcity" + fId).val() + '~' + $("#cfstate" + fId).val() + '~' + $("#cfcountry" + fId).val() + '~' + $("#cmbZip" + fId).val();
                    }
                    else if (frmObj.componentType == COMP_RATING) {
                        fData = $("#crating" + fId).find('.clicked').length;
                    }
                    if (frmObj.label == "3") {
                        fData = fData + '|' + $("#fcol" + fId).val();
                    }

                    var cmpId = fromArray[f].idk;
                    var obj = {};
                    // if(fromArray[f].isRequired && (!fData ||fData.length<1) && fromArray[f].componentType != 'label'){
                    // 	$('#mandatory-field').css('display','block');
                    // 	canProceed = false;
                    // 	break;
                    // }
                    // else{
                    canProceed = true;
                    $('#mandatory-field').css('display', 'none');
                    obj.componentId = cmpId;
                    obj.miniTemplateId = fromArray[f].miniTemplateId;
                    obj.value = fData;
                    obj.isDeleted = 0;
                    obj.createdBy = Number(sessionStorage.userId);
                    obj.isActive = 1;


                    if (frmTempObj) {
                        var miniCompId = "";
                        for (var f = 0; f < components.length; f++) {
                            var fComp = components[f];
                            if (fComp && fComp.componentId == cmpId) {
                                miniCompId = fComp.id;
                                break;
                            }
                        }
                        obj.id = miniCompId;
                    }
                    miniCompArray.push(obj);
                    // }
                }
            }
        }



        var reqObj = {};
        if (canProceed) {
            reqObj.notes = $("#txtNotes").val();
            reqObj.patientId = Number(sessionStorage.patientId);
            reqObj.isActive = 1;
            reqObj.isDeleted = 0;
            reqObj.reportedOn = new Date().getTime();
            reqObj.createdBy = Number(sessionStorage.userId);
            reqObj.providerId = Number(sessionStorage.providerId) || null;
            reqObj.templateId = Number(selItem.idk);
            var miniComponents = miniCompArray;
            reqObj.componentValues = miniComponents;
            if (frmTempObj) {
                reqObj.id = frmIdk;
            }

            dataUrl = ipAddress + "/homecare/template-values/";

            createAjaxObject(dataUrl, reqObj, method, onCreateForm, onError);
        }
    }
    else{
        $("#notes-mandatory-field").css("display","");
    }
}

//
// function onClickFormSave(evt){
//     //var btnId = evt.currentTarget;
//     //btnId = $(btnId).attr("btnId");
//     //var fromArray = getFormDesignValues(btnId);
//     //console.log(fromArray);
//     var miniCompArray = [];
//     for(var f=0;f<fromArray.length;f++){
//         var fId = fromArray[f].idk;
//         fId = "f"+fId;
//         var fData = "";//$("#"+fId).val();
//         var frmObj = fromArray[f];
//         var fId = fromArray[f].idk;
//         fId = "f"+fId;
//         var fData = "";
//         var cmpId = fromArray[f].miniComponentId;
//
//         if(frmObj.miniComponentType == COMP_DATE_PICKER){
//             if(frmObj.miniDataTypeValue == DT_DATE){
//                 var cmbDate = $("#"+fId).data("kendoDatePicker");
//                 fData = cmbDate.value();
//             }else if(frmObj.miniDataTypeValue == DT_DATETIME){
//                 var cmbDate = $("#"+fId).data("kendoDateTimePicker");
//                 fData = cmbDate.value();
//             }
//         }else if(frmObj.miniComponentType == COMP_TEXT || frmObj.miniComponentType == COMP_TEXT_AREA){
//             fData = $("#"+fId).val();
//         }else if(frmObj.miniComponentType == COMP_CHECK_BOX || frmObj.miniComponentType == COMP_RADIO_BUTTON){
//             if(frmObj.miniComponentType == COMP_CHECK_BOX){
//                 fData = $('input[name='+fId+']').val();
//             }else if(frmObj.miniComponentType == COMP_RADIO_BUTTON){
//                 fData = $("input[type='radio'][name="+fId+"]:checked").val();
//             }
//         }
//         else if(frmObj.miniComponentType == COMP_DROPDOWN){
//             fData = $("#"+fId).val();
//         }
//         else if(frmObj.miniComponentType == COMP_EMAIL){
//             fData = $("#"+fId).val();
//         }
//         else if(frmObj.miniComponentType == COMP_PHONE){
//             fData = $("#"+fId).val();
//         }
//         else if(frmObj.componentType == COMP_PRICE || frmObj.componentType == COMP_NUMBER){
//             fData = $("#"+fId).val();
//         }
//         else if(frmObj.miniComponentType == COMP_WEBSITE){
//             fData = $("#"+fId).val();
//         }
//         else if(frmObj.miniComponentType == COMP_NAME){
//             fData = $("#cff"+fId).val() + '~' + $("#cfm"+fId).val() +'~'+ $("#cfl"+fId).val();
//         }
//         else if(frmObj.miniComponentType == COMP_GROUP_CHECK_BOX){
//             var delimiter = "~";
//             fData = "";
//             $.each($("input[name="+fId+"]:checked"), function(){
//                 fData +=$(this).val() + delimiter;
//             });
//             if('~' == fData[fData.length -1])
//                 fData = fData.slice(0, -1);
//         }
//         else if(frmObj.miniComponentType == COMP_DATETIME){
//             var dateVal = Date.parse($("#"+fId).val());
//             fData = dateVal;
//         }
//         else if(frmObj.miniComponentType == COMP_ADDRESS){
//             fData = $("#cfadd1"+fId).val() + '~' + $("#cfadd2"+fId).val() +'~'+ $("#cfcity"+fId).val() + '~' + $("#cfstate"+fId).val() +'~'+ $("#cfcountry"+fId).val()+'~'+ $("#cmbZip"+fId).val();
//
//         }
//         else if(frmObj.miniComponentType == COMP_RATING){
//             fData = $("#crating"+fId).find('.clicked').length;
//
//         }
//         if($("#fcol"+fromArray[f].idk).val()){
//             fData = fData + '|' + $("#fcol"+fromArray[f].idk).val();
//         }
//
//         var obj = {};
//         obj.miniComponentId = cmpId;
//         obj.id = fromArray[f].idk;
//         obj.miniTemplateId = fromArray[f].miniTemplateId;
//         //obj.miniUnits = fromArray[f].miniUnits;
//         //obj.miniComponentName = fromArray[f].miniComponentName;
//         obj.createdBy = parseInt(sessionStorage.userId);
//         obj.value = fData;
//         obj.isDeleted = 0;
//         obj.modifiedBy = parseInt(sessionStorage.userId);
//         obj.isActive = 1;
//         miniCompArray.push(obj);
//     }
//     var reqObj = {};
//     reqObj.notes = $("#txtNotes").val();
//     reqObj.patientId = sessionStorage.patientId;
//     reqObj.isActive = 1;
//     reqObj.isDeleted = 0;
//     reqObj.reportedOn = new Date().getTime();
//     reqObj.createdBy = parseInt(sessionStorage.userId);
//     reqObj.providerId = null;
//     reqObj.templateId = Number(selItem.idk);
//     reqObj.componentValues = miniCompArray;
//
//
//     dataUrl = ipAddress +"/homecare/template-values/";
//     var method = "POST";
//
//     createAjaxObject(dataUrl, reqObj, method, onCreateForm, onError);
// }


// function onClickSaveApi(){
//
//         // for (var j = 0; j < miniTemplates.length; j++) {
//         //     found = true;
//         //     // for(var k=0;k<frmDesignArrayById.length;k++)
//         //     // {
//         //     // 	if(miniTemplates[j].id == frmDesignArrayById[k].id)
//         //     // 		break;
//         //     // 	else{
//         //     // 		found = false;
//         //     // 	}
//         //     // }
//         //     if (miniTemplates[j].isDeleted == 0) {
//         //         var obj = {};
//         //         obj.id = miniTemplates[j].id;
//         //         obj.components = miniTemplates[j].componenets || [];
//         //         for (var c = 0; c < obj.components.length; c++) {
//         //             obj.components[c].idk = obj.components[c].id;
//         //         }
//         //         frmDesignArrayById.push(obj);
// 		//
//         //     }
// 		//
//         // }
//     for(var j=0;j<miniTemplates.length;j++)
//     {
//         found = true;
//         for(var k=0;k<frmDesignArrayById.length;k++)
//         {
//             if(miniTemplates[j].id == frmDesignArrayById[k].id)
//                 break;
//             else{
//                 found = false;
//             }
//         }
//         if(!found&&miniTemplates[j].isDeleted==0){
//             var obj= {};
//             obj.id=miniTemplates[j].id;
//             obj.components  = miniTemplates[j].componenets || [];
//             for(var c=0;c<obj.components.length;c++){
//                 obj.components[c].idk = obj.components[c].id;
//             }
//             frmDesignArrayById.push(obj);
//
//         }
//
//     }
//
//         var tempId = selItem.idk;
//         var frmTempObj = getFormTemplateUserValueArray(tempId);
//         var method = "POST";
//         var frmIdk = "";
//         var canProceed = false;
//         var minComponents = [];
//         if (frmTempObj) {
//             method = "PUT";
//             frmIdk = frmTempObj.id;
//             components = frmTempObj.componentValues;
//         }
//         // var btnId = evt.currentTarget;
//         // btnId = $(btnId).attr("btnId");
//         var fromArray = [];
//
//         var miniCompArray = [];
//         for (var g = 0; g < frmDesignArrayById.length; g++) {
//             fromArray = frmDesignArrayById[g].components;
//             console.log(fromArray);
//
//         }
//
//     for (var f = 0; f < fromArray.length; f++) {
//
//         var frmObj = fromArray[f];
//         console.log(frmObj);
//         var fId = fromArray[f].idk;
//         fId = "f" + fId;
//         var fData = "";
//         if (frmObj.componentType == COMP_DATE_PICKER) {
//             if (frmObj.dataTypeValue == DT_DATE) {
//                 var cmbDate = $("#" + fId).data("kendoDatePicker");
//                 fData = cmbDate.value();
//             } else if (frmObj.dataTypeValue == DT_DATETIME) {
//                 var cmbDate = $("#" + fId).data("kendoDateTimePicker");
//                 fData = cmbDate.value();
//             }
//         } else if (frmObj.componentType == COMP_TEXT || frmObj.componentType == COMP_TEXT_AREA) {
//             fData = $("#" + fId).val();
//         } else if (frmObj.componentType == COMP_CHECK_BOX || frmObj.componentType == COMP_RADIO_BUTTON) {
//             if (frmObj.componentType == COMP_CHECK_BOX) {
//                 fData = $('input[name=' + fId + ']').val();
//             } else if (frmObj.componentType == COMP_RADIO_BUTTON) {
//                 fData = $("input[type='radio'][name=" + fId + "]:checked").val();
//             }
//
//         }
//         else if (frmObj.componentType == COMP_DROPDOWN) {
//             fData = $("#" + fId).val();
//         }
//         else if (frmObj.componentType == COMP_EMAIL) {
//             fData = $("#" + fId).val();
//         }
//         else if (frmObj.componentType == COMP_PHONE) {
//             fData = $("#" + fId).val();
//         }
//         else if (frmObj.componentType == COMP_PRICE || frmObj.componentType == COMP_NUMBER) {
//             fData = $("#" + fId).val();
//         }
//         else if (frmObj.componentType == COMP_WEBSITE) {
//             fData = $("#" + fId).val();
//         }
//         else if (frmObj.componentType == COMP_NAME) {
//             fData = $("#cff" + fId).val() + '~' + $("#cfm" + fId).val() + '~' + $("#cfl" + fId).val();
//         }
//         else if (frmObj.componentType == COMP_GROUP_CHECK_BOX) {
//             var delimiter = "~";
//             fData = "";
//             $.each($("input[name=" + fId + "]:checked"), function () {
//                 fData += $(this).val() + delimiter;
//             });
//             if ('~' == fData[fData.length - 1])
//                 fData = fData.slice(0, -1);
//         }
//         else if (frmObj.componentType == COMP_DATETIME) {
//             var dateVal = Date.parse($("#" + fId).val());
//             fData = dateVal;
//         }
//         else if (frmObj.componentType == COMP_ADDRESS) {
//             fData = $("#cfadd" + fId).val() + '~' + $("#cfadd2" + fId).val() + '~' + $("#cfcity" + fId).val() + '~' + $("#cfstate" + fId).val() + '~' + $("#cfcountry" + fId).val() + '~' + $("#cmbZip" + fId).val();
//         }
//         else if (frmObj.componentType == COMP_RATING) {
//             fData = $("#crating" + fId).find('.clicked').length;
//         }
//         if (frmObj.label == "3") {
//             fData = fData + '|' + $("#fcol" + fId).val();
//         }
//
//         var cmpId = fromArray[f].idk;
//         var obj = {};
//         // if(fromArray[f].isRequired && (!fData ||fData.length<1) && fromArray[f].componentType != 'label'){
//         // 	$('#mandatory-field').css('display','block');
//         // 	canProceed = false;
//         // 	break;
//         // }
//         // else{
//         canProceed = true;
//         $('#mandatory-field').css('display', 'none');
//         // obj.componentId = cmpId;
//         obj.miniTemplateId = fromArray[f].miniTemplateId;
//         obj.id = fromArray[f].idk;
//         obj.value = fData;
//         obj.isDeleted = 0;
//         obj.createdBy = Number(sessionStorage.userId);
//         obj.modifiedBy = parseInt(sessionStorage.userId);
//         obj.isActive = 1;
//
//
//         if (frmTempObj) {
//             var miniCompId = "";
//             for (var f = 0; f < components.length; f++) {
//                 var fComp = components[f];
//                 if (fComp && fComp.componentId == cmpId) {
//                     miniCompId = fComp.id;
//                     break;
//                 }
//             }
//             obj.id = miniCompId;
//         }
//         miniCompArray.push(obj);
//         // }
//     }
//         var reqObj = {};
//         if (canProceed) {
//             reqObj.notes = $("#txtNotes").val();
//             reqObj.patientId = Number(sessionStorage.patientId);
//             reqObj.isActive = 1;
//             reqObj.isDeleted = 0;
//             reqObj.reportedOn = new Date().getTime();
//             reqObj.createdBy = Number(sessionStorage.userId);
//             reqObj.providerId = Number(sessionStorage.providerId) || null;
//             reqObj.modifiedBy = parseInt(sessionStorage.userId);
//             reqObj.templateId = Number(selItem.idk);
//             var miniComponents = miniCompArray;
//             reqObj.componentValues = miniComponents;
//             if (formDataId != '' && formDataId != undefined) {
//                 reqObj.id = formDataId;
//                 method = "PUT";
//             }
//             if (frmTempObj) {
//                 reqObj.id = frmIdk;
//             }
//
//             dataUrl = ipAddress + "/homecare/template-values/";
//
//             createAjaxObject(dataUrl, reqObj, method, onEnptyResponse, onError);
//         }
//
// }

function onClickSaveApi(){
    //var btnId = evt.currentTarget;
    //btnId = $(btnId).attr("btnId");
    //var fromArray = getFormDesignValues(btnId);
    //console.log(fromArray);
    var miniCompArray = [];
    for(var f=0;f<fromArray.length;f++){
        var fId = fromArray[f].idk;
        fId = "f"+fId;
        var fData = "";//$("#"+fId).val();
        var frmObj = fromArray[f];
        var fId = fromArray[f].idk;
        fId = "f"+fId;
        var fData = "";
        var cmpId = fromArray[f].miniComponentId;

        if(frmObj.miniComponentType == COMP_DATE_PICKER){
            if(frmObj.miniDataTypeValue == DT_DATE){
                var cmbDate = $("#"+fId).data("kendoDatePicker");
                fData = cmbDate.value();
            }else if(frmObj.miniDataTypeValue == DT_DATETIME){
                var cmbDate = $("#"+fId).data("kendoDateTimePicker");
                fData = cmbDate.value();
            }
        }else if(frmObj.miniComponentType == COMP_TEXT || frmObj.miniComponentType == COMP_TEXT_AREA){
            fData = $("#"+fId).val();
        }else if(frmObj.miniComponentType == COMP_CHECK_BOX || frmObj.miniComponentType == COMP_RADIO_BUTTON){
            if(frmObj.miniComponentType == COMP_CHECK_BOX){
                fData = $('input[name='+fId+']').val();
            }else if(frmObj.miniComponentType == COMP_RADIO_BUTTON){
                fData = $("input[type='radio'][name="+fId+"]:checked").val();
            }
        }
        else if(frmObj.miniComponentType == COMP_DROPDOWN){
            fData = $("#"+fId).val();
        }
        else if(frmObj.miniComponentType == COMP_EMAIL){
            fData = $("#"+fId).val();
        }
        else if(frmObj.miniComponentType == COMP_PHONE){
            fData = $("#"+fId).val();
        }
        else if(frmObj.componentType == COMP_PRICE || frmObj.componentType == COMP_NUMBER){
            fData = $("#"+fId).val();
        }
        else if(frmObj.miniComponentType == COMP_WEBSITE){
            fData = $("#"+fId).val();
        }
        else if(frmObj.miniComponentType == COMP_NAME){
            fData = $("#cff"+fId).val() + '~' + $("#cfm"+fId).val() +'~'+ $("#cfl"+fId).val();
        }
        else if(frmObj.miniComponentType == COMP_GROUP_CHECK_BOX){
            var delimiter = "~";
            fData = "";
            $.each($("input[name="+fId+"]:checked"), function(){
                fData +=$(this).val() + delimiter;
            });
            if('~' == fData[fData.length -1])
                fData = fData.slice(0, -1);
        }
        else if(frmObj.miniComponentType == COMP_DATETIME){
            var dateVal = Date.parse($("#"+fId).val());
            fData = dateVal;
        }
        else if(frmObj.miniComponentType == COMP_ADDRESS){
            fData = $("#cfadd1"+fId).val() + '~' + $("#cfadd2"+fId).val() +'~'+ $("#cfcity"+fId).val() + '~' + $("#cfstate"+fId).val() +'~'+ $("#cfcountry"+fId).val()+'~'+ $("#cmbZip"+fId).val();

        }
        else if(frmObj.miniComponentType == COMP_RATING){
            fData = $("#crating"+fId).find('.clicked').length;

        }
        if($("#fcol"+fromArray[f].idk).val()){
            fData = fData + '|' + $("#fcol"+fromArray[f].idk).val();
        }

        var obj = {};
        obj.miniComponentId = cmpId;
        obj.id = fromArray[f].idk;
        obj.miniTemplateId = fromArray[f].miniTemplateId;
        //obj.miniUnits = fromArray[f].miniUnits;
        //obj.miniComponentName = fromArray[f].miniComponentName;
        obj.createdBy = parseInt(sessionStorage.userId);
        obj.value = fData;
        obj.isDeleted = 0;
        obj.modifiedBy = parseInt(sessionStorage.userId);
        obj.isActive = 1;
        miniCompArray.push(obj);
    }
    var reqObj = {};
    reqObj.notes = $("#txtNotes").val();
    reqObj.patientId = sessionStorage.patientId;
    reqObj.isActive = 1;
    reqObj.isDeleted = 0;
    reqObj.reportedOn = new Date().getTime();
    reqObj.modifiedBy = parseInt(sessionStorage.userId);
    reqObj.createdBy = parseInt(sessionStorage.userId);
    reqObj.providerId = null;
    reqObj.templateId = Number(selItem.idk);
    reqObj.componentValues = miniCompArray;
    reqObj.id = formDataId;

    dataUrl = ipAddress +"/homecare/template-values/";
    var method  = "PUT";
    createAjaxObject(dataUrl, reqObj, method, onEnptyResponse, onError);
}


// function onClickSave(evt){
//     var fromArray = [];
//     console.log(miniTemplates);
//     console.log(frmDesignArrayById);
//
//     for(var j=0;j<miniTemplates.length;j++)
//     {
//         found = true;
//         for(var k=0;k<frmDesignArrayById.length;k++)
//         {
//             if(miniTemplates[j].id == frmDesignArrayById[k].id)
//                 break;
//             else{
//                 found = false;
//             }
//         }
//         if(!found&&miniTemplates[j].isDeleted==0){
//             var obj= {};
//             obj.id=miniTemplates[j].id;
//             obj.components  = miniTemplates[j].componenets || [];
//             for(var c=0;c<obj.components.length;c++){
//                 obj.components[c].idk = obj.components[c].id;
//
//             }
//             frmDesignArrayById.push(obj);
//
//         }
//
//     }
//
//
//
//     var tempId = selItem.idk;
//     var frmTempObj = getFormTemplateUserValueArray(tempId);
//     var method  = "POST";
//     var frmIdk = "";
//     var canProceed = false;
//     var minComponents = [];
//     if(frmTempObj){
//         method  = "PUT";
//         frmIdk = frmTempObj.id;
//         components = frmTempObj.componentValues;
//     }
//     // var btnId = evt.currentTarget;
//     // btnId = $(btnId).attr("btnId");
//
//
//     var miniCompArray = [];
//     for(var g=0;g<frmDesignArrayById.length;g++) {
//     	if(frmDesignArrayById[g].components) {
//             fromArray = frmDesignArrayById[g].components;
//         }
//     }
//         console.log(fromArray);
//         for(var f=0;f<fromArray.length;f++){
//
//             var frmObj = fromArray[f];
//             console.log(frmObj);
//             var fId = fromArray[f].idk;
//             fId = "f"+fId;
//             var fData = "";
//             if(frmObj.componentType == COMP_DATE_PICKER){
//                 if(frmObj.dataTypeValue == DT_DATE){
//                     var cmbDate = $("#"+fId).data("kendoDatePicker");
//                     fData = cmbDate.value();
//                 }else if(frmObj.dataTypeValue == DT_DATETIME){
//                     var cmbDate = $("#"+fId).data("kendoDateTimePicker");
//                     fData = cmbDate.value();
//                 }
//             }else if(frmObj.componentType == COMP_TEXT || frmObj.componentType == COMP_TEXT_AREA){
//                 fData = $("#"+fId).val();
//             }else if(frmObj.componentType == COMP_CHECK_BOX || frmObj.componentType == COMP_RADIO_BUTTON){
//                 if(frmObj.componentType == COMP_CHECK_BOX){
//                     fData = $('input[name='+fId+']').val();
//                 }else if(frmObj.componentType == COMP_RADIO_BUTTON){
//                     fData = $("input[type='radio'][name="+fId+"]:checked").val();
//                 }
//
//             }
//             else if(frmObj.componentType == COMP_DROPDOWN){
//                 fData = $("#"+fId).val();
//             }
//             else if(frmObj.componentType == COMP_EMAIL){
//                 fData = $("#"+fId).val();
//             }
//             else if(frmObj.componentType == COMP_PHONE){
//                 fData = $("#"+fId).val();
//             }
//             else if(frmObj.componentType == COMP_PRICE || frmObj.componentType == COMP_NUMBER){
//                 fData = $("#"+fId).val();
//             }
//             else if(frmObj.componentType == COMP_WEBSITE){
//                 fData = $("#"+fId).val();
//             }
//             else if(frmObj.componentType == COMP_NAME){
//                 fData = $("#cff"+fId).val() + '~' + $("#cfm"+fId).val() +'~'+ $("#cfl"+fId).val();
//             }
//             else if(frmObj.componentType == COMP_GROUP_CHECK_BOX){
//                 var delimiter = "~";
//                 fData = "";
//                 $.each($("input[name="+fId+"]:checked"), function(){
//                     fData +=$(this).val() + delimiter;
//                 });
//                 if('~' == fData[fData.length -1])
//                     fData = fData.slice(0, -1);
//             }
//             else if(frmObj.componentType == COMP_DATETIME){
//                 var dateVal = Date.parse($("#"+fId).val());
//                 fData = dateVal;
//             }
//             else if(frmObj.componentType == COMP_ADDRESS){
//                 fData = $("#cfadd"+fId).val() + '~' + $("#cfadd2"+fId).val() +'~'+ $("#cfcity"+fId).val() +'~'+ $("#cfstate"+fId).val() + '~' + $("#cfcountry"+fId).val() +'~'+ $("#cmbZip"+fId).val();
//             }
//             else if(frmObj.componentType == COMP_RATING){
//                 fData = $("#crating"+fId).find('.clicked').length;
//             }
//             if(frmObj.label == "3"){
//                 fData = fData + '|' + $("#fcol"+fId).val();
//             }
//
//             var cmpId = fromArray[f].idk;
//             var obj = {};
//             if(fromArray[f].isRequired && (!fData ||fData.length<1) && fromArray[f].componentType != 'label'){
//                 $('#mandatory-field').css('display','block');
//                 canProceed = false;
//                 break;
//             }
//             else{
//                 canProceed = true;
//                 $('#mandatory-field').css('display','none');
//                 obj.id = fromArray[f].idk;
//                 // obj.componentId = cmpId;
//
//
//                 obj.miniTemplateId = fromArray[f].miniTemplateId;
//                 obj.value = fData;
//                 obj.isDeleted = 0;
//                 obj.createdBy = Number(sessionStorage.userId);
//                 obj.modifiedBy = parseInt(sessionStorage.userId);
//                 obj.isActive = 1;
//                 if(frmTempObj){
//                     var miniCompId = "";
//                     for(var f=0;f<components.length;f++){
//                         var fComp = components[f];
//                         if(fComp && fComp.componentId == cmpId){
//                             miniCompId = fComp.id;
//                             break;
//                         }
//                     }
//                     obj.id = miniCompId;
//                 }
//                 miniCompArray.push(obj);
//             }
//         }
//     // }
//     var reqObj = {};
//     if(canProceed){
//         reqObj.notes = $("#txtNotes").val();
//         reqObj.patientId = Number(sessionStorage.patientId);
//         reqObj.isActive = 1;
//         reqObj.isDeleted = 0;
//         reqObj.reportedOn = new Date().getTime();
//         reqObj.createdBy = Number(sessionStorage.userId);
//         reqObj.modifiedBy = parseInt(sessionStorage.userId);
//         reqObj.providerId = Number(sessionStorage.providerId) || null;
//         reqObj.templateId = Number(selItem.idk);
//         var miniComponents = miniCompArray;
//         reqObj.componentValues = miniComponents;
//         if (formDataId != '' && formDataId != undefined) {
//             reqObj.id = formDataId;
//             method = "PUT";
//         }
//
//         if(frmTempObj){
//             reqObj.id = frmIdk;
//         }
//
//         dataUrl = ipAddress +"/homecare/template-values/";
//
//         createAjaxObject(dataUrl, reqObj, method, onCreate, onError);
//     }
// }

function onClickSave(evt){
    //var btnId = evt.currentTarget;
    //btnId = $(btnId).attr("btnId");
    //var fromArray = getFormDesignValues(btnId);
    //console.log(fromArray);
    var miniCompArray = [];
    for(var f=0;f<fromArray.length;f++){
        var fId = fromArray[f].idk;
        fId = "f"+fId;
        var fData = "";//$("#"+fId).val();
        var frmObj = fromArray[f];
        var fId = fromArray[f].idk;
        fId = "f"+fId;
        var fData = "";
        var cmpId = fromArray[f].miniComponentId;

        if(frmObj.miniComponentType == COMP_DATE_PICKER){
            if(frmObj.miniDataTypeValue == DT_DATE){
                var cmbDate = $("#"+fId).data("kendoDatePicker");
                fData = cmbDate.value();
            }else if(frmObj.miniDataTypeValue == DT_DATETIME){
                var cmbDate = $("#"+fId).data("kendoDateTimePicker");
                fData = cmbDate.value();
            }
        }else if(frmObj.miniComponentType == COMP_TEXT || frmObj.miniComponentType == COMP_TEXT_AREA){
            fData = $("#"+fId).val();
        }else if(frmObj.miniComponentType == COMP_CHECK_BOX || frmObj.miniComponentType == COMP_RADIO_BUTTON){
            if(frmObj.miniComponentType == COMP_CHECK_BOX){
                fData = $('input[name='+fId+']').val();
            }else if(frmObj.miniComponentType == COMP_RADIO_BUTTON){
                fData = $("input[type='radio'][name="+fId+"]:checked").val();
            }
        }
        else if(frmObj.miniComponentType == COMP_DROPDOWN){
            fData = $("#"+fId).val();
        }
        else if(frmObj.miniComponentType == COMP_EMAIL){
            fData = $("#"+fId).val();
        }
        else if(frmObj.miniComponentType == COMP_PHONE){
            fData = $("#"+fId).val();
        }
        else if(frmObj.componentType == COMP_PRICE || frmObj.componentType == COMP_NUMBER){
            fData = $("#"+fId).val();
        }
        else if(frmObj.miniComponentType == COMP_WEBSITE){
            fData = $("#"+fId).val();
        }
        else if(frmObj.miniComponentType == COMP_NAME){
            fData = $("#cff"+fId).val() + '~' + $("#cfm"+fId).val() +'~'+ $("#cfl"+fId).val();
        }
        else if(frmObj.miniComponentType == COMP_GROUP_CHECK_BOX){
            var delimiter = "~";
            fData = "";
            $.each($("input[name="+fId+"]:checked"), function(){
                fData +=$(this).val() + delimiter;
            });
            if('~' == fData[fData.length -1])
                fData = fData.slice(0, -1);
        }
        else if(frmObj.miniComponentType == COMP_DATETIME){
            var dateVal = Date.parse($("#"+fId).val());
            fData = dateVal;
        }
        else if(frmObj.miniComponentType == COMP_ADDRESS){
            fData = $("#cfadd1"+fId).val() + '~' + $("#cfadd2"+fId).val() +'~'+ $("#cfcity"+fId).val() + '~' + $("#cfstate"+fId).val() +'~'+ $("#cfcountry"+fId).val()+'~'+ $("#cmbZip"+fId).val();

        }
        else if(frmObj.miniComponentType == COMP_RATING){
            fData = $("#crating"+fId).find('.clicked').length;

        }
        if($("#fcol"+fromArray[f].idk).val()){
            fData = fData + '|' + $("#fcol"+fromArray[f].idk).val();
        }

        var obj = {};
        obj.miniComponentId = cmpId;
        obj.id = fromArray[f].idk;
        obj.miniTemplateId = fromArray[f].miniTemplateId;
        //obj.miniUnits = fromArray[f].miniUnits;
        //obj.miniComponentName = fromArray[f].miniComponentName;
        obj.createdBy = parseInt(sessionStorage.userId);
        obj.value = fData;
        obj.isDeleted = 0;
        obj.modifiedBy = parseInt(sessionStorage.userId);
        obj.isActive = 1;
        miniCompArray.push(obj);
    }
    var reqObj = {};
    reqObj.notes = $("#txtNotes").val();
    reqObj.patientId = sessionStorage.patientId;
    reqObj.isActive = 1;
    reqObj.isDeleted = 0;
    reqObj.reportedOn = new Date().getTime();
    reqObj.modifiedBy = parseInt(sessionStorage.userId);
    reqObj.createdBy = parseInt(sessionStorage.userId);
    reqObj.providerId = null;
    reqObj.templateId = Number(selItem.idk);
    reqObj.componentValues = miniCompArray;
    reqObj.id = formDataId;

    dataUrl = ipAddress +"/homecare/template-values/";
    var method  = "PUT";
    createAjaxObject(dataUrl, reqObj, method, onCreate, onError);
}

function getFormTemplateUserValueArray(frmId){
    for(var f=0;f<userFormTemplateArray.length;f++){
        var fItem = userFormTemplateArray[f];
        if(fItem && fItem.formTemplateId == frmId){
            return fItem;
        }
    }
    return null;
}
var userFormTemplateArray = [];
var formDataId;
function onCreate(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            userFormTemplateArray.push(dataObj.response["template-values"]);
            // if(dataObj.response.status.template-values){
            //     formDataId = dataObj.response.status.template-values.id;
            // }
            var msg = "";
            msg = "Form Values are entered  Successfully";

            displaySessionErrorPopUp("Info", msg, function(res) {
            })
        }else{
            customAlert.error("Error", dataObj.response.status.message);
        }
    }else{

    }
}

function onEnptyResponse(dataObj){


}

function onCreateForm(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            userFormTemplateArray.push(dataObj.response["template-values"]);
            if(dataObj.response["template-values"]){
                formDataId = dataObj.response["template-values"].id;
                var els = document.querySelectorAll('.enable-tabs')
                for (var i = 0; i < els.length; i++) {
                    els[i].classList.remove('enable-tabs')
                }

                $("#saveMiniTemplate").removeAttr("disabled");
                $("#saveMiniTemplate").css("display","");
                $(".panel-heading").css("background-color","#ffddb9 !important")
                $(".panel-heading").css("border-color","#ffd2 !important")
            }
            $("#divNotesBlock").css("display","none");
            var msg = "";
            msg = "Notes saved successfully. Now you can enter the Form data";

            displaySessionErrorPopUp("Info", msg, function(res) {
                var ipaddress = ipAddress+"/homecare/template-values/?templateId="+dataObj.response["template-values"].templateId+"&patientId="+dataObj.response["template-values"].patientId+"&id="+dataObj.response["template-values"].id;//+"&fields=*,dataType.*,unit.*,component.*";
                getAjaxObject(ipaddress,"GET",handleGetReportFormValues,onError);
            })
        }else{
            customAlert.error("Error", dataObj.response.status.message);
        }
    }else{

    }
}

function handleGetReportFormValues(dataObj){
    if(parentRef)
        parentRef.dataObj = dataObj;
    miniTemplates = dataObj.response.templateValues[0].componentValues;
}

function getFormDesignValues(btnId){
    for(var a=0;a<tempFormDesignArray.length;a++){
        var aItem = tempFormDesignArray[a];
        if(aItem && aItem.idk == btnId){
            return aItem.values;
        }
    }
    return null;
}
function getFormData(){
    var selectedItems = angularUIgridWrapper1.getSelectedRows();
    console.log(selectedItems);
    var ipaddress = ipAddress+"/homecare/component-values/?is-active=1&is-deleted=0&miniTemplateId="+selectedItems[0].idk;
    getAjaxObject(ipaddress,"GET",handleGetFormDataValues,onError);
}
function handleGetFormDataValues(dataObj){
    console.log(dataObj);
}
function onError(errorObj){
    console.log(errorObj);
}

function buttonEvents(){
    /*$("#btnSave").off("click",onClickSave);
    $("#btnSave").on("click",onClickSave);

    $("#btnView").off("click",onClickView);
    $("#btnView").on("click",onClickView);

    $("#btnReport").off("click",onClickReport);
    $("#btnReport").on("click",onClickReport);


    $("#btnCancelDet").off("click",onClickCancel);
    $("#btnCancelDet").on("click",onClickCancel);


    $("#btnEdit").off("click",onClickEdit);
    $("#btnEdit").on("click",onClickEdit);

    $("#btnDelete").off("click",onClickDelete);
    $("#btnDelete").on("click",onClickDelete);*/

    $("#btnCancel").off("click",onClickCancel);
    $("#btnCancel").on("click",onClickCancel);

    $("#btnSaveNotes").off("click");
    $("#btnSaveNotes").on("click",onClickFormSave);
}

function resetData(){
    for(var f=0;f<frmDesignArray.length;f++){
        var fId = frmDesignArray[f].idk;
        fId = "f"+fId;
        $("#"+fId).val("");
    }
}
function adjustHeight(){
    var defHeight = 150;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    var frm = (cmpHeight)+"px";
    $("#divFormData").css({height:frm});
}


function getComboListIndex(cmbId, attr, attrVal) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb) {
        var ds = cmb.dataSource;
        var totalRec = ds.total();
        for (var i = 0; i < totalRec; i++) {
            var dtItem = ds.at(i);
            if (dtItem && dtItem[attr] == attrVal) {
                cmb.select(i);
                return i;
            }
        }
    }
    return -1;
}
function closeVideoScreen(evt,returnData){

}
var operation = "add";
var selFileResourceDataItem = null;
function onClickCancel(){
    var obj = {};
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}
function onClickOK(){
    setTimeout(function(){
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if(selectedItems && selectedItems.length>0){
            var obj = {};
            obj.selItem = selectedItems[0];
            selAccountItem = selectedItems[0];
            addFacility("update");
            // var onCloseData = new Object();
            /*obj.status = "success";
            obj.operation = "ok";
               var windowWrapper = new kendoWindowWrapper();
               windowWrapper.closePageWindow(obj);*/
        }
    })
}

function onClickZipSearch(){
    console.log($('#'+this.id).parent());
    console.log($('#'+this.id).parent().parent());
    console.log($('#'+this.id).parent().parent().parent()[0].id);
    // currZipId = $('#'+this.id).parent().parent().parent()[0].id;
    currZipId = $('#'+this.id)[0].id;
    currZipId = currZipId.replace("btnZipSearch","f");
    var popW = 600;
    var popH = 500;
    if(parentRef)
        parentRef.searchZip = true;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search Zip";
    var cntry = sessionStorage.countryName;
    if(cntry.indexOf("India")>=0){
        profileLbl = "Search Postal Code";
    }else if(cntry.indexOf("United Kingdom")>=0){
        profileLbl = "Search Postal Code";
    }else if(cntry.indexOf("United State")>=0){
        profileLbl = "Search Zip";
    }else{
        profileLbl = "Search Zip";
    }
    devModelWindowWrapper.openPageWindow("../../html/masters/zipList.html", profileLbl, popW, popH, true, onCloseSearchZipAction);
}
function onClickAdditionalZipSearch() {

    var popW = 600;
    var popH = 500;
    if(parentRef)
        parentRef.searchZip = true;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search Zip";
    devModelWindowWrapper.openPageWindow("../../html/masters/zipList.html", profileLbl, popW, popH, true, onCloseSearchAdditionalZipAction);
}
var zipSelItem = null;
var AddzipSelItem = null;

function onCloseSearchZipAction(evt,returnData){
    console.log(returnData);
    //console.log(frmId);
    if(returnData && returnData.status == "success"){
        //console.log();
        $("#cmbZip"+currZipId).val("");
        $("#txtZip4"+currZipId).val("");
        $("#cfstate"+currZipId).val("");
        $("#cfcountry"+currZipId).val("");
        $("#cfcity"+currZipId).val("");
        var selItem = returnData.selItem;
        if(selItem){
            zipSelItem = selItem;
            if(zipSelItem){
                cityId = zipSelItem.idk;
                // if(zipSelItem.zip){
                // 	$("#cmbZip"+currZipId).val(zipSelItem.zip);
                // }
                if(zipSelItem.zipFour){
                    $("#txtZip4"+currZipId).val(zipSelItem.zipFour);
                }
                if(zipSelItem.state){
                    $("#cfstate"+currZipId).val(zipSelItem.state);
                }
                if(zipSelItem.country){
                    $("#cfcountry"+currZipId).val(zipSelItem.country);
                }
                if(zipSelItem.city){
                    $("#cfcity"+currZipId).val(zipSelItem.city);
                }
            }
        }
    }
}
function onCloseSearchAdditionalZipAction(evt, returnData) {
    console.log(returnData);
    if (returnData && returnData.status == "success") {
        //console.log();
        $("#AddcmbZip").val("");
        $("#AddtxtZip4").val("");
        $("#AddtxtState").val("");
        $("#AddtxtCountry").val("");
        $("#AddtxtCity").val("");
        var selItem = returnData.selItem;
        if (selItem) {
            AddzipSelItem = selItem;
            if (AddzipSelItem) {
                cityId = AddzipSelItem.idk;
                //AddzipSelItem.cityId = cityId;
                if (AddzipSelItem.zip) {
                    $("#AddcmbZip").val(AddzipSelItem.zip);
                }
                if (AddzipSelItem.zipFour) {
                    $("#AddtxtZip4").val(AddzipSelItem.zipFour);
                }
                if (AddzipSelItem.state) {
                    $("#AddtxtState").val(AddzipSelItem.state);
                }
                if (AddzipSelItem.country) {
                    $("#AddtxtCountry").val(AddzipSelItem.country);
                }
                if (AddzipSelItem.city) {
                    $("#AddtxtCity").val(AddzipSelItem.city);
                }
            }
        }
    }
}
function getZip(){
    getAjaxObject(ipAddress+"/city/list?is-active=1","GET",getZipList,onError);
}
function onClickAdd(){
    /*var obj = {};
     obj.status = "Add";
     obj.operation = "ok";
        var windowWrapper = new kendoWindowWrapper();
        windowWrapper.closePageWindow(obj);*/
    addFacility("add")

}
function setRating(item,id){

    $('#crating'+id+ ' span').removeClass('clicked');
    $('#crating'+id+ ' span').each(
        function(index) {
            if(index<item)
                $(this).addClass('clicked');
        }
    );
}
$("#txtNotes").on('keyup', function(e) {
    if($(this).val().length > 0) {
        // $("#saveMiniTemplate").removeAttr("disabled");
        // $("#saveMiniTemplate").css("display","block");
    }
    else {
        $("#saveMiniTemplate").attr("disabled","disabled");
        $("#saveMiniTemplate").css("display","none");
    }
});
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
function isVarchar() {
    /*var str = "dr4";
    var patt = new RegExp("^[a-zA-Z0-9]+$");
    var res = patt.test(str);
    return res;*/
}
function isDecimal() {
    //$(this).val($(this).val().replace(/[^0-9\.]/g,''));
    if ((event.which != 46 || event.currentTarget.value.indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        event.preventDefault();
    }
}

function onGetPatientInfo(dataObj) {
    patientInfoObject = dataObj.response;
}


function themeAPIChange(){
    getAjaxObject(ipAddress+"/homecare/settings/?id=2","GET",getThemeValue,onError);
}

function getThemeValue(dataObj){
    console.log(dataObj);
    var tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.activityTypes){
            if($.isArray(dataObj.response.settings)){
                tempCompType = dataObj.response.settings;
            }else{
                tempCompType.push(dataObj.response.settings);
            }
        }
        if(tempCompType.length > 0){
            themesChange(tempCompType[0].value);
        }
    }
}

function themesChange(themeValue){
    if(themeValue == 2){
        loadAPi("../../Theme2/Theme02.css");
    }
    else if(themeValue == 3){
        loadAPi("../../Theme3/Theme03.css");
    }
}


function getAddedComponents(evt){

    var patientId = sessionStorage.patientId;
    addedComponents = [];
    var idk = $(evt.currentTarget).attr("idk");
    if(patientId){
        getAjaxObject(ipAddress + "/patient/"+patientId+"/", "GET", function(resp){
            patientInfoObject = resp.response;
            var providerId = sessionStorage.providerId;
            //getAjaxObject(ipAddress + "/provider/"+providerId+"/", "GET", function(resp) {
            if(sessionStorage.providerData)
                providerInfoObject = JSON.parse(sessionStorage.providerData);
            if(idk){
                var getipaddress = ipAddress+"/homecare/components/?is-active=1&is-deleted=0&miniTemplateId="+idk+"&fields=*,dataType.*,unit.*,component.*";

                getAjaxObject(getipaddress,"GET",function(dataObj){
                    console.log(dataObj);
                    if(dataObj && dataObj.response && dataObj.response.status ){
                        if(dataObj.response.status.code == "1"){
                            if(dataObj.response.components){
                                if($.isArray(dataObj.response.components)){
                                    addedComponents = dataObj.response.components;
                                }else{
                                    addedComponents.push(dataObj.response.components);
                                }
                            }
                        }
                    }
                    setTimeout(function(){onClickItem(evt);},100);

                },onError);
            }
        }, onError);
    }
    else if(sessionStorage.providerId && sessionStorage.providerId.length>0)
    {
        var providerId = sessionStorage.providerId;
        //getAjaxObject(ipAddress + "/provider/"+providerId+"/", "GET", function(resp) {
        providerInfoObject = JSON.parse(sessionStorage.providerData);
        if(idk){
            var getipaddress = ipAddress+"/homecare/components/?is-active=1&is-deleted=0&miniTemplateId="+idk+"&fields=*,dataType.*,unit.*,component.*";

            getAjaxObject(getipaddress,"GET",function(dataObj){
                console.log(dataObj);
                if(dataObj && dataObj.response && dataObj.response.status ){
                    if(dataObj.response.status.code == "1"){
                        if(dataObj.response.components){
                            if($.isArray(dataObj.response.components)){
                                addedComponents = dataObj.response.components;
                            }else{
                                addedComponents.push(dataObj.response.components);
                            }
                        }
                    }
                }
                setTimeout(function(){onClickItem(evt);},100);

            },onError);
        }
        // }, onError);
    }



}
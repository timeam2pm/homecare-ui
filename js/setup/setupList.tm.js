var operation = "";
var ADD = "add";
var UPDATE = "update";
var VIEW = "view";
var DELETE = "delete";

var parentRef = null;

$(document).ready(function(){
	parentRef = parent.frames['iframe'].window;
});

$(window).load(function(){
	var pnlHeight = window.innerHeight;
	pnlHeight = pnlHeight-30;
	$("#leftPanel").height(pnlHeight);
	$("#centerPanel").height(pnlHeight);
	$("#rightPanel").height(pnlHeight);
	$("#leftButtonBar").height(pnlHeight);
	init()
});

function init(){
	if(sessionStorage.clientTypeId == "2"){
				
				$("#addUserPatientMap").text("Service User Mapping");
			}
    buttonEvents();
}
function buttonEvents(){
     $("#chkNetSpeed").off("click");
    $("#chkNetSpeed").on("click",onClickNetSpeed);
	
	 $("#uploadLogo").off("click");
    $("#uploadLogo").on("click",onClickUploadLogo);
	
    $("#addUser").off("click");
    $("#addUser").on("click",onClickAddUser);
	
	 $("#addCustUser").off("click");
    $("#addCustUser").on("click",onClickAddCustomUser);
    
    $("#searchUser").off("click");
    $("#searchUser").on("click",onClickSearchUser);
	
	$("#enableUser").off("click");
    $("#enableUser").on("click",onClickEnableUser);
    
    $("#addUserRoles").off("click");
    $("#addUserRoles").on("click",onClickUserRoles);
    
    $("#addUserPatientMap").off("click");
    $("#addUserPatientMap").on("click",onClickUserPatientMap);
    
    $("#addUserStaffMap").off("click");
    $("#addUserStaffMap").on("click",onClickUserStaffMap);
    
    $("#addAdmin").off("click");
    $("#addAdmin").on("click",onClickAdmin);
    
    $("#addScreenPermi").off("click");
    $("#addScreenPermi").on("click",onClickScreenPermissions);
    
    $("#addCarerManagerMap").off("click");
    $("#addCarerManagerMap").on("click",onClickCarerManagerMapping);
	
	$("#addManager").off("click");
    $("#addManager").on("click",onClickCarerManager);
	
	$("#addAppUtility").off("click");
    $("#addAppUtility").on("click",onClickAddUtility);
    
   // $("#addProvider").off("click",onClickAddProvider);
   // $("#addProvider").on("click",onClickAddProvider);
    
    //$("#searchProvider").off("click",onClickSearchProvider);
    //$("#searchProvider").on("click",onClickSearchProvider);
}
function showPatientPanel(){
	
}
function onClickUploadLogo(){
	var popW = "80%";
    var popH = 400;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
   
    parent.setWIndowHeaderColor("6e457e","000");
    devModelWindowWrapper.openPageWindow("../../html/masters/uploadLogo.html", "Upload Logo", popW, popH, true, onCloseUploadLogo);
}
function onCloseUploadLogo(evt,ret){
	
}
function onClickCarerManagerMapping(){
	var popW = "70%";
    var popH = 500;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
   
    devModelWindowWrapper.openPageWindow("../../html/masters/carerManagerList.html", "Carer Manager Map", popW, popH, true, onCloseUploadLogo);
}
function onCloseUploadLogo(evt,res){
	
}

function onClickAddUtility(){
	var popW = "70%";
    var popH = "75%";

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    
    profileLbl = "Appointments";
    
    
    //parentRef.selItem = selUserObject;
    //parentRef.operation = operation;
    // devModelWindowWrapper.openPageWindow("../../html/masters/providerAppointmentList.html", profileLbl, popW, popH, true, onCloseManagerGroups);
    devModelWindowWrapper.openPageWindow("../../html/patients/deleteAppointmentLogin.html", profileLbl, popW, popH, true, onCloseManagerGroups);
}
function onClickCarerManager(){
	console.log("Manager");
	var popW = "60%";
    var popH = "75%";

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    
    profileLbl = "Organization Hierarchy";
    
    
    //parentRef.selItem = selUserObject;
    //parentRef.operation = operation;
    devModelWindowWrapper.openPageWindow("../../html/masters/createManagerGroups.html", profileLbl, popW, popH, true, onCloseManagerGroups);
}
function onCloseManagerGroups(evt,rData){
	
}
function onClickNetSpeed(){
	var popW = "80%";
    var popH = "80%";
	
	var videoUrl = "http://openspeedtest.com/Get-widget.php";
	window.open(videoUrl, "popupWindow", "width=1000,height=600,scrollbars=yes");

   // var profileLbl;
   // var devModelWindowWrapper = new kendoWindowWrapper();
    	//profileLbl = "Internet Speed";
    
    //parentRef.selItem = selUserObject;
   // parentRef.operation = operation;
  //  devModelWindowWrapper.openPageWindow("../../html/patients/showInetSpeed.html", profileLbl, popW, popH, true, onCloseINetSpeedAction);
}
function onCloseINetSpeedAction(evt,ret){
	
}
var selUserObject = null;
function addUser(){
	showPatientPanel();
	var popW = "60%";
    var popH = "75%";

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    if(operation == ADD){
    	profileLbl = "Create User";
    }else{
    	profileLbl = "Edit User";
    }
    
    parentRef.selItem = selUserObject;
    parentRef.operation = operation;
    devModelWindowWrapper.openPageWindow("../../html/masters/createUserAccount.html", profileLbl, popW, popH, true, onCloseCreateUserAction);
}
function addCustomUser(){
	showPatientPanel();
	var popW = "60%";
    var popH = "75%";

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    if(operation == ADD){
    	profileLbl = "Create User";
    }else{
    	profileLbl = "Edit User";
    }
    
    parentRef.selItem = selUserObject;
    parentRef.operation = operation;
    devModelWindowWrapper.openPageWindow("../../html/masters/createCustomUserAccount.html", profileLbl, popW, popH, true, onCloseCreateCustomUserAction);
}
function onCloseCreateCustomUserAction(evt,returnData){
	if(returnData){
		if(returnData.status == "success"){
			customAlert.error("Info","User created successfully");
			//onClickSearchUser();
		}
	}
}
function onClickAddUser(){
	operation = ADD;
	addUser();
}
function onClickAddCustomUser(){
	operation = ADD;
	addCustomUser();
}
function onCloseCreateUserAction(evt,returnData){
	if(returnData){
		if(returnData.status == "search"){
			onClickSearchUser();
		}
	}
}
function onClickEnableUser(){
	showPatientPanel();
	var popW = "60%";
    var popH = 450;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Update User Details";
    devModelWindowWrapper.openPageWindow("../../html/masters/userEnableList.html", profileLbl, popW, popH, true, onCloseUserEnableListAction);
}
function onCloseUserEnableListAction(evt,r){
	
}
function onClickSearchUser(){
	showPatientPanel();
	var popW = "60%";
    var popH = 510;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search User";
    devModelWindowWrapper.openPageWindow("../../html/masters/userList.html", profileLbl, popW, popH, true, onCloseUserListAction);
}
function onCloseUserListAction(evt,returnData){
	if(returnData){
		if(returnData.status == "Add"){
			operation = ADD;
			addUser();
		}else if(returnData.status == "success"){
			selUserObject = returnData.selItem;
			operation = UPDATE;
			addUser();
		}
	}
}
function onClickUserRoles(){
}


function onClickUserStaffMap(){
	showPatientPanel();
	var popW = "80%";
    var popH = "80%";

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Staff User Mapping";
    devModelWindowWrapper.openPageWindow("../../html/masters/staffUserMapping.html", profileLbl, popW, popH, true, onCloseUserPatientMapAction);
}

function onClickUserPatientMap(){
	showPatientPanel();
	var popW = "80%";
    var popH = "80%";

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Service User Mapping";
    devModelWindowWrapper.openPageWindow("../../html/masters/userMapping.html", profileLbl, popW, popH, true, onCloseUserPatientMapAction);
}
function onCloseUserPatientMapAction(){

}
function onClickAdmin(){
	
}
function onClickScreenPermissions(){
	showPatientPanel();
	var popW = "80%";
    var popH = 600;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "App Permissions";
    devModelWindowWrapper.openPageWindow("../../html/masters/securityPermissions.html", profileLbl, popW, popH, true, onCloseUserPermissions);
}
function onCloseUserPermissions(evt,returnData){
	
}
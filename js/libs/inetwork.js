window.top.getLCount = 0;
var clientAPIManager = (function () {
	serviceURL: '';
	requestType: "POST";
	parameters: null;
	errorCallback: null;
	responseType: "";
	PARAMS_URL = "../../../ui/html/conf/properties.xml";
	frontController = "";
	
	return {		
		getMeOutOfInvalidSession: function(){
			$.when(kendo.ui.ExtAlertDialog.show({
				title: "Info", 
				message: "Invalid Session. Please Re-Login",
				icon: "k-ext-information" })
			).done(function (response) {
				var loginURL = sessionStorage.redirectPageOnLogout;
				if(loginURL == undefined)loginURL="login.html";
				clientAPIManager.closeSession();
				//alert("Invalid session. Please login again.");

				//For handling browser back, close, redirect, etc cleaning up session
				sessionStorage.validNavigation = true;
				
				//top.location.href = "../../../../app/login/html/"+loginURL; 
				});
		},
		closeSession: function(){
			sessionStorage.sessionID = "";
		},
		logOutSession: function(){			
			var loginURL = sessionStorage.redirectPageOnLogout;
			if(loginURL == undefined)loginURL="login.html";
			clientAPIManager.closeSession();
			//For handling browser back, close, redirect, etc cleaning up session
			sessionStorage.validNavigation = true;
			
			//top.location.href = "../../../../app/login/html/"+loginURL; 
		},
		checkReference: function(){
			var isUnloadFlagSet = false;
			if($("#iframe")[0].contentWindow.unloadFlagOnLogout != null)
			isUnloadFlagSet = $("#iframe")[0].contentWindow.unloadFlagOnLogout;

			if(isUnloadFlagSet)
			{
				$("#iframe")[0].contentWindow.unloadFunctionOnLogout();
			}
			
			return isUnloadFlagSet;
		},
		cleanLogOutSession: function(){
			var getUnloadFlagVal = this.checkReference();
			//alert('unload flag: '+getUnloadFlagVal);
			if(!getUnloadFlagVal){
				this.sendRequest( "AdministrationService", "logout", true, "POST", "", this.cleanup, this.cleanup, true );
			}
		},
		cleanup: function(){
			var loginURL = sessionStorage.redirectPageOnLogout;
			if(loginURL == undefined)loginURL="login.html";
			setTimeout(function(){
				sessionStorage.clear();
			},500);
			//For handling browser back, close, redirect, etc cleaning up session
			sessionStorage.validNavigation = true;
			top.location.href = "../../../../app/login/html/"+loginURL; 
		},
		continueLogoutEvent: function(){
			this.sendRequest( "AdministrationService", "logout", true, "POST", "", this.cleanup, this.cleanup, true );
		},
		cleanLogOutSessionOnLoginPage: function(){
			this.sendRequest( "AdministrationService", "logout", true, "POST", "", redirectToLogin, redirectToLogin, true );
		},
		validateNonIframeAccess: function(){
			if(self.location==top.location)
			{
				//this.getMeOutOfInvalidSession();
			}
		},
		handleLoadBalanceSession: function(){
			clientAPIManager.closeSession();
			alert("handleLoadBalanceSession: handler");
		},
		setSessionId: function(sessionid){
			sessionStorage.sessionID = sessionid;
		},
		getSessionId: function(){
			return sessionStorage.sessionID;
		},
		getScreenName: function(){
			return (sessionStorage.curPage == undefined) ? "" : sessionStorage.curPage;
		},
		setMenuXML: function(menuXML){
			sessionStorage.menu = menuXML;
		},
		getMenuXML: function(){
			return sessionStorage.menu;
		},		
		handleHttpError: function(jqXHR, textStatus, errorThrown, httpErrorCallback){
			if(typeof(httpErrorCallback)=="undefined"){
				this.displayErrorPopUp(" HTTP ERROR "," Not Able To Connect To Server");
			}else{
				httpErrorCallback(jqXHR, textStatus, errorThrown);
			}
		},
		displayErrorPopUp: function() {
			alert("Error in network");
		},
		readApplicationParams: function(){},
		getServiceURL: function(address, locator, action, queryParams){
			var  postUrl = sessionStorage.rootURL +"RestfullServices/"+ address + "/" + locator + "/" + action + "/?sessionId="+this.getSessionId()+"&screenName="+this.getScreenName();
			if(!(queryParams==null || queryParams == "" || queryParams == undefined || queryParams == "undefined")){
				var intindex = queryParams.indexOf('&');
				if (intindex==0) {
					postUrl = postUrl+queryParams;
				}else{
					postUrl = postUrl+ "&"+queryParams;
				}
			}
			return postUrl;
		},
		getRequestURL: function( service, action, isSessioned ){
			var flexServlet = sessionStorage.frontController;
			flexServlet = sessionStorage.rootURL+flexServlet;
			if(isSessioned){
			   return flexServlet+ "?serviceName="+service+"&action="+action+"&sessionId="+this.getSessionId();
			}else{ 
			   return flexServlet+ "?serviceName="+service+"&action="+action;
			}   
		},
		sendIframeRequest: function(url, queryParams){
			sessionStorage.validNavigation = true;
			var strScreenName = sessionStorage.curPage;
			var queryParamsFromURL = url.split("?");
			if(($.isArray(queryParamsFromURL) == true) && queryParamsFromURL.length > 1)
				var url = sessionStorage.rootURL+url.split("?")[0]+"?sessionId="+this.getSessionId() + "&zx=" + Math.random() + "&screenName=" + strScreenName+"&"+queryParamsFromURL[1];
			else
				var url = sessionStorage.rootURL+url.split("?")[0]+"?sessionId="+this.getSessionId() + "&zx=" + Math.random() + "&screenName=" + strScreenName;
			if(queryParams!="")
			{
				if(!(queryParams==null || queryParams == "" || queryParams == undefined || queryParams == "undefined")){
					var intindex = queryParams.indexOf('&');
					 if (intindex==0) {
						url = url+queryParams;
					 }else{
						url = url+ "&"+queryParams;
					 }
				}
			}
			top.$("#iframe").attr('src', url) ;
		},
		loadPage: function(url, queryParams){
			sessionStorage.validNavigation = true;
			var strScreenName = sessionStorage.curPage;
			var url = sessionStorage.rootURL+url+"?sessionId="+this.getSessionId() + "&zx=" + Math.random() + "&screenName=" + strScreenName;
			if(queryParams!="")
			{
				if(!(queryParams==null || queryParams == "" || queryParams == undefined || queryParams == "undefined")){
					var intindex = queryParams.indexOf('&');
					 if (intindex==0) {
						url = url+queryParams;
					 }else{
						url = url+ "&"+queryParams;
					 }
				}
			}
			location.href = url;
		},
		sendRequest: function( service, action, isSessioned, requestType, parameters, successCallback, errorCallback,showLoader ){
			if (requestType !== null && requestType !== ''){
				this.requestType = requestType;
			}
			
			if (showLoader == null || showLoader == ''){
				showLoader = false;
			}
			
			this.parameters = parameters;
			var fireingurl = this.getRequestURL( service, action, isSessioned );
			$.ajax({
				type: this.requestType,
				url: fireingurl,
				dataType: "xml", 
				context: this,
				cache: false,
				data: this.parameters,
				beforeSend: function(){
					// show loader
					if(showLoader){
						window.top.getLCount += 1;
						Loader.showLoader();
					}
				},
				success: function( data, statusCode, jqXHR ){
					var contentType = jqXHR.getResponseHeader("Content-Type");
					if ((jqXHR.status == 200)){
						//var xmlResponse = $(data).find('Response');
						//var status = $.trim( $(xmlResponse).find('Status').text() );
						var status = $.trim($($(data).find('Response').find('Status')[0]).text());
						if(status.indexOf("success") != -1){
							successCallback(data);
						}else{
							//var responseDesc = $.trim( $(xmlResponse).find('Description').text() );
							var responseDesc = $.trim( $(data).find('Description').text() );
							if( responseDesc.indexOf("Session Invalid") > -1 ){
								clientAPIManager.getMeOutOfInvalidSession();
							}else if( responseDesc.indexOf("LoadBalancer Server Failure.Switching") > -1 ){
								clientAPIManager.handleLoadBalanceSession();
							}else{
								errorCallback( data );	
							}
						}
					}
					
					//Hide Loader
					if(showLoader)
						Loader.hideLoader();
				},
				error: function( jqXHR, textStatus, errorThrown ){
					//alert("in error handler");
				
					if(jqXHR.status != 401){
						if(errorCallback === null || errorCallback === undefined){
							this.error(jqXHR, textStatus, errorThrown);
						}else{
							errorCallback(jqXHR, textStatus, errorThrown);
						}
					}
				
					//Hide Loader
					if(showLoader)
						Loader.hideLoader();
				}
			})
		},
		sendJsonRequest: function( address, locator, action, requestType, jsonData, queryParams, successCallback, errorCallback,showLoader,httpErrorCallback){
			this.validateNonIframeAccess();
			if (requestType !== null && requestType !== ''){
				this.requestType = requestType;
			}
			
			if (showLoader == null || showLoader == ''){
				showLoader = false;
			}
			
			this.parameters = jsonData;
			
			$.ajax({
				type: this.requestType,
				url: this.getServiceURL( address, locator, action, queryParams),
				contentType: "application/json; charset=utf-8",
				context: this,
				cache: false,
				data: JSON.stringify(this.parameters),
				beforeSend: function(){
					// show loader
					if(showLoader){
						window.top.getLCount += 1;
						Loader.showLoader();
					}
				},
				success: function( data, statusCode, jqXHR ){
					if (jqXHR.status == 200)
					{
						//console.log(data);
						if(typeof (data)!="undefined" && (typeof(data.RestData) == "undefined" && typeof(data.Response) != "undefined")) {
							data.RestData = data.Response;
							data.Response = null;
						}
						if(typeof (data)!="undefined" && (typeof(data.RestData)!="undefined" && (typeof(data.RestData.Status)!="undefined" || typeof(data.RestData.status)!="undefined")))
						{
							if(data.RestData.Status=="success" || data.RestData.status=="success")
							{
								successCallback(data.RestData);
							}
							else if(typeof(data.RestData.Description)!="undefined")
							{
								var responseDesc = data.RestData.Description;
								if(responseDesc==undefined || responseDesc == "undefined" || responseDesc == null){
									errorCallback(data);
									if(showLoader)
										Loader.hideLoader();
									return;	
								}
								try{
									responseDesc.indexOf("Session Invalid")
								 } catch (e) {
									errorCallback("Unexpected error.");	
									if(showLoader)
										Loader.hideLoader();
									return;	
								 }
								if( responseDesc.indexOf("Session Invalid") > -1 )
								{
									clientAPIManager.getMeOutOfInvalidSession();
								}
								else if( responseDesc.indexOf("LoadBalancer Server Failure.Switching") > -1 )
								{
									clientAPIManager.handleLoadBalanceSession();
								}
								else
								{
									errorCallback(data);	
								}
							}
							else
							{
								errorCallback(data);	
							}
						}
						else
						{
							errorCallback("Unexpected data format error.");	
						}
					}else{
						this.handleHttpError(jqXHR, "", "", httpErrorCallback);
					}
					//Hide Loader
					if(showLoader)
						Loader.hideLoader();
				},
				error: function( jqXHR, textStatus, errorThrown ){
					if (jqXHR.status != 200)
					{
						this.handleHttpError(jqXHR, textStatus, errorThrown, httpErrorCallback);
					}
					else
					{
						if(errorCallback === null || errorCallback === undefined){
							this.error(jqXHR, textStatus, errorThrown);
						}else{
							//errorCallback(jqXHR, textStatus, errorThrown);
							this.handleHttpError(jqXHR, "", "", httpErrorCallback);
						}
					}
					//Hide Loader
					if(showLoader)
						Loader.hideLoader();
				}
			});
		},
		sendJsonRequestSynchronous: function( address, locator, action, requestType, jsonData, queryParams, successCallback, errorCallback,showLoader,httpErrorCallback){
			this.validateNonIframeAccess();
			if (requestType !== null && requestType !== ''){
				this.requestType = requestType;
			}
			
			if (showLoader == null || showLoader == ''){
				showLoader = false;
			}
			
			this.parameters = jsonData;
			
			$.ajax({
				type: this.requestType,
				url: this.getServiceURL( address, locator, action, queryParams),
				contentType: "application/json; charset=utf-8",
				context: this,
				cache: false,
				async: false,
				data: JSON.stringify(this.parameters),
				beforeSend: function(){
					// show loader
					if(showLoader)
						Loader.showLoader();
				},
				success: function( data, statusCode, jqXHR ){
					if (jqXHR.status == 200)
					{
						if(typeof (data)!="undefined" && typeof(data.RestData)!="undefined" && typeof(data.RestData.Status)!="undefined")
						{
							if(data.RestData.Status=="success")
							{
								successCallback(data.RestData);
							}
							else if(typeof(data.RestData.Description)!="undefined")
							{
								var responseDesc = data.RestData.Description;
								if( responseDesc.indexOf("Session Invalid") > -1 )
								{
									clientAPIManager.getMeOutOfInvalidSession();
								}
								else if( responseDesc.indexOf("LoadBalancer Server Failure.Switching") > -1 )
								{
									clientAPIManager.handleLoadBalanceSession();
								}
								else
								{
									errorCallback(data);	
								}
							}
							else
							{
								errorCallback(data);	
							}
						}
						else
						{
							errorCallback("Unexpected data format error.");	
						}
					}else{
						this.handleHttpError(jqXHR, "", "", httpErrorCallback);
					}
					//Hide Loader
					if(showLoader)
						Loader.hideLoader();
				},
				error: function( jqXHR, textStatus, errorThrown ){
					if (jqXHR.status != 200)
					{
						this.handleHttpError(jqXHR, textStatus, errorThrown, httpErrorCallback);
					}
					else
					{
						if(errorCallback === null || errorCallback === undefined){
							this.error(jqXHR, textStatus, errorThrown);
						}else{
							//errorCallback(jqXHR, textStatus, errorThrown);
							this.handleHttpError(jqXHR, "", "", httpErrorCallback);
						}
					}
					//Hide Loader
					if(showLoader)
						Loader.hideLoader();
				}
			});
		}
		
			
	}
	
})();

function redirectToLogin() {
	if(sessionStorage.redirectPageOnLogout != undefined)
		top.location.href = sessionStorage.redirectPageOnLogout;
	else
		top.location.href = "login.html";
}



/**
    @description It shows loading image at the center of the page covering rest of the page with a light gray background.
    @class Loader
    @requires JQuery
 */
var Loader = (function(){
	var callback;

	function setDisplay(img, background){
		var width = $('body').width(); 
		var height = $(window).height();
        var scrollHeight;
		if(document.body && document.body.scrollHeight && document.body.scrollHeight != null){
            scrollHeight = document.body.scrollHeight;
		}

		var backgroundHeight = typeof scrollHeight !== 'undefined' && scrollHeight > height ? scrollHeight : height;
		img.css({"left": (width - 74)/2, "top": $(document).scrollTop() + (height - 40)/2 })
		/*
		if(htz.config.deviceGroup === 'desktop'){
			img.css({"left": (width - 74)/2, "top": $(document).scrollTop() + 100 })
		}
		else {
			img.css({"left": (width - 74)/2, "top": $(document).scrollTop() + (height - 40)/2 })
		}
		*/
		background.css({"width":$(document).width(), "height":backgroundHeight});
	}

	function show() {
		var background = $("<div />").attr("id", "loading_bg").addClass("loading-bg");
		var img = $("<div />").attr("id", "loading-fg").addClass("loading-fg");
		
		setDisplay(img, background);

		background.appendTo($("body")).show();
		img.appendTo($("body")).show();
	}

	$(document).ready(function(){
		$("input[type='submit'], button.submit").click(function(){ Loader.showLoader(); }); 
		$(window).bind('resize.loader', Loader.resize);
 
 		window.onorientationchange = function(){
			Loader.resize();
  		};		
	});
		
	return /** @lends Loader */ {
		resize : function(){
			var img = $('#loading-fg'), background = $('#loading_bg');

			if(img.length && background.length){
				setDisplay(img, background);
			}
		},
		/**
			@description It shows the loader
			@function 
			@param {object} options  timeout, callback can be passed as option in json format. timeout in millis. callback is the function which will executed after timeout.  
			@example  
			    options: {timeout:2000, callback: callback function}
		*/
		showLoader : function(options) {
			if(window.top.getLCount > 1){
				return;
			}
			show();
			if(options && options.timeout) {
				setTimeout(Loader.hideLoader, options.timeout); 
				var optCallback = options.callback;
				if(optCallback && (typeof optCallback == 'function')) {
					callback = optCallback;
				}
			}
		},
		/**
			@description It hides the loader
			@function 
		*/
		hideLoader : function() {
			if(window.top.getLCount > 1){
				window.top.getLCount -= 1;
				return;
			}
			window.top.getLCount = 0;
			$("#loading_bg, #loading-fg").remove();
			if(callback) {
				callback(); 
				callback = null;
			}
		}
	};
})();
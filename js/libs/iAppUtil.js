var statusArray = [{Key:"ACTIVE",Value:"ACTIVE"},{Key:"INACTIVE",Value:"INACTIVE"}];

var ACTIVE = "ACTIVE";
var INACTIVE = "INACTIVE";

function setDataForSelection(data, container, changeMethod, namesArr, selIndex, optionLabelValue) {
    var selList = [];
    if ($.isArray(data) == false) {
        var obj = {};
        obj[namesArr[0]] = data[namesArr[0]];
        obj[namesArr[1]] = data[namesArr[1]];
        selList.push(obj);

    } else {
        selList = data;
    }

    $("#" + container).kendoComboBox({
        dataTextField: namesArr[0],
        dataValueField: namesArr[1],
        dataSource: selList,
        optionLabel: " ",
        change: function(e) {
            $("#" + container).parent().find("span.k-dropdown-wrap").attr("title", $("#" + container).data("kendoComboBox").text());
            try {
                changeMethod(e);
            } catch (e1) {

            }
        },
        dataBound: function(e) {
            setTimeout(function() {
                var dropDownList = $("#" + container).data("kendoComboBox");
                /*if (dropDownList.list.width() >= $("#" + container).width()) {
                    dropDownList.list.width(dropDownList.list.width() + 20);
                }*/
                if(dropDownList && dropDownList.list){
                	dropDownList.list.css("min-width", $("#" + container).width());
                }
            }, 200);
        },
        template: function(e) {
            var tip = e[namesArr[0]];
            return '<div class="overFlow" title="' + tip + '">' + tip + '</div>';
        }
    });
    var containerTypeDropdownlist = $("#" + container).data("kendoComboBox");
    if(containerTypeDropdownlist) {
	    containerTypeDropdownlist.select(selIndex);
    	$("#" + container).parent().find("span.k-dropdown-wrap").attr("title", $("#" + container).data("kendoComboBox").text());
    }
    //$("#" + container).data('kendoComboBox').trigger("change");
}
function getComboBoxDataItem(cmbId){
	var cmb = $("#"+cmbId).data("kendoComboBox");
	if(cmb){
		return cmb.dataItem();
	}
	return null;
}
function getComboBoxReference(cmbId){
	var cmb = $("#"+cmbId).data("kendoComboBox");
	if(cmb){
		return cmb;
	}
	return null;
}
function getTreeReference(treeId){
	var treeView = $('#'+treeId).data('kendoTreeView');
	if(treeView){
		return treeView;
	}
	return null;
}
function getSelectTreeNode(treeId){
	var tv = $('#'+treeId).data('kendoTreeView');
	if(tv){
		var selTreeItem = tv.dataItem(tv.select());
		if(selTreeItem){
			return selTreeItem;
		}
	}
	return null;
}
function removeNullPayLoads(item){
	for (var i in item){
		//console.log($.isEmptyObject(item[i]));
		if($.isEmptyObject(item[i])){
			item[i] = "";
		}
	}
	return item;
}
function removeNullObject(itemObj){
	if($.isEmptyObject(itemObj)){
			return "";
	}
		return itemObj;
}
function minimizePanel(divId,pnlId){
	$("#"+divId).slideUp(function(){
		if($("#"+divId).is(":visible")){
			$("#"+pnlId).attr("src","../../../../iapp/ui/styles/default/images/Default/upIcon.gif");
		}else{
			$("#"+pnlId).attr("src","../../../../iapp/ui/styles/default/images/Default/downIcon.gif");
		}
	});
}
function maximizePanel(divId,pnlId){
	$("#"+divId).slideDown(function(){
		if($("#"+divId).is(":visible")){
			$("#"+pnlId).attr("src","../../../../iapp/ui/styles/default/images/Default/upIcon.gif");
		}else{
			$("#"+pnlId).attr("src","../../../../iapp/ui/styles/default/images/Default/downIcon.gif");
		}
	});
}
function toggleMinMaxPanel(divId,pnlId){
	$("#"+divId).toggle(function(){
		if($("#"+divId).is(":visible")){
			$("#"+pnlId).attr("src","../../../../iapp/ui/styles/default/images/Default/upIcon.gif");
		}else{
			$("#"+pnlId).attr("src","../../../../iapp/ui/styles/default/images/Default/downIcon.gif");
		}
	});
}
function allowNumerics(ctrlId){
	$("#"+ctrlId).keypress(function (e) {
		 if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
			return false;
		}
   });
}
function allowDecimals(ctrlId){
	$("#"+ctrlId).keypress(function (e) {
		 if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 95 && e.which != 46) {
			return false;
		}
   });
}
function allowSessionTime(ctrlId){
	$("#"+ctrlId).keypress(function (e) {
		//console.log(e.which);
		 if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which !=45) {
			return false;
		}
   });
}
function allowNegativeValue(ctrlId){
	$("#"+ctrlId).keypress(function (e) {
		//console.log(e.which);
		 if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which !=45) {
			return false;
		}
   });
}
function allowNegativeDecimalValue(ctrlId){
	$("#"+ctrlId).keypress(function (e) {
		//console.log(e.which);
		 if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which !=45 && e.which !=46) {
			return false;
		}
   });
}
function allowAlphabets(ctrlId){
	$("#"+ctrlId).keypress(function (e) {
		//console.log(e.which);
		 if ((e.which > 64 && e.which < 91) || (e.which > 96 && e.which < 123)){
              return true;
			}else{
              return false;
		}
   });
}
function restrictLessAndGreathan(ctrlId){
	$("#"+ctrlId).keypress(function (e) {
		console.log(e.which);
		 if (e.which == 60 || e.which == 62) {
			return false;
		}
   });
}
function allowPhoneNumber(ctrlId){
	$("#"+ctrlId).keypress(function (e) {
			//console.log(e.which);
		 if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 40 && e.which != 41 && e.which != 45) {
			return false;
		}
   });
}
function validateEmail(field) {
	var field = field.trim();
	var flag = true;
	var regex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	if (regex.test(field)) {
		flag = true;
	} else {
		flag = false;
	}
	return flag;
}
function getIndexByText(cmbId,attr,searchStr){
	var cmb = $("#"+cmbId).data("kendoComboBox");
	if(cmb){
		var ds = cmb.dataSource;
		if(ds && ds.total()>0){
			var recLength = ds.total();
			for(var i=0;i<recLength;i++){
				var item = ds.at(i);
				if(item && item[attr] == searchStr){
					return i;
				}
			}
		}
	}
	return -1;
}
function distanceBetweenTwoLatLng(lat1,lon1,lat2,lon2){ // find the distance between 2 latlng. 
	var R = 6371;  
	var dLat = (lat2-lat1)*Math.PI/180;  
	var dLon = (lon2-lon1)*Math.PI/180;   
	var a = Math.sin(dLat/2) * Math.sin(dLat/2) +  
			Math.cos(lat1*Math.PI/180) * Math.cos(lat2*Math.PI/180) *   
			Math.sin(dLon/2) * Math.sin(dLon/2);   
	var c = 2 * Math.asin(Math.sqrt(a));   
	var d = R * c;  
	return d;
}
var pWid = 300;
var pHei = 150;

function getDimesionsforShowAlertPopup(alertmessage) {
    if (alertmessage.length < 100) {
        pWid = 300;
        pHei = 120;
    } else if (alertmessage.length < 150) {
        pWid = 300;
        pHei = 150;
    } else if (alertmessage.length >= 150 && alertmessage.length <= 200) {
        pWid = 350;
        pHei = 180;
    } else if (alertmessage.length > 200 && alertmessage.length < 250) {
        pWid = 400;
        pHei = 200;
    } else if (alertmessage.length > 250) {
        pWid = 500;
        pHei = 250;
    }
}

function getTrimValue(ctrlId){
	return $.trim($("#"+ctrlId).val());
}
function showInfoPopUp(alerttitle, alertmessage) {
    console.log(alertmessage + "  \n\n " + alerttitle);
    getDimesionsforShowAlertPopup(alertmessage);
    $.when(kendo.ui.ExtAlertDialog.show({
        title: alerttitle,
        message: alertmessage,
        icon: "k-ext-information",
        width: pWid,
        height: pHei
    }))
}

function showErrorPopUp(alerttitle, alertmessage) {
    console.log(alertmessage + "  \n\n " + alerttitle);
    getDimesionsforShowAlertPopup(alertmessage);
    $.when(kendo.ui.ExtAlertDialog.show({
        title: alerttitle,
        message: alertmessage,
        icon: "k-ext-error",
        width: pWid,
        height: pHei
    }))
}

function setMultipleDataForSelection(data, container, changeMethod, namesArr, selIndex, optionLabelValue) {
    var selList = [];
    if ($.isArray(data) == false) {
        var obj = {};
        obj[namesArr[0]] = data[namesArr[0]];
        obj[namesArr[1]] = data[namesArr[1]];
        selList.push(obj);

    } else {
        selList = data;
    }

    $("#" + container).igCombo({
        width: "100%",
        dataSource: selList,
        textKey: namesArr[0],
        valueKey: namesArr[1],
        multiSelection: {
            enabled: true,
            showCheckboxes: true
        },
        dropDownOrientation: "bottom"
    });
}

function getComboListMultipleIndex(cmbId, attr, attrVal) {
    var cmb = $("#" + cmbId).data("igCombo");
    if (cmb && attrVal != null) {
        var ds = cmb.options.dataSource.settings.dataSource;
        var totalRec = ds.length;
        var attrValArr = attrVal.split(',');
        var attrValIndex = "";
        var indArr = [];
        for (var i = 0; i < totalRec; i++) {
            var dtItem = ds[i];
            for (var j = 0; j < attrValArr.length; j++) {
            	if (dtItem && dtItem[attr] == attrValArr[j].trim()) {
	                $('.ui-igcombo-listitem').eq(i).addClass('ui-state-active');
	                $('.ui-igcombo-listitem').eq(i).find('.ui-icon').addClass('ui-icon-check ui-igcombo-checkbox-on ui-igcheckbox-small-on');
	                $('.ui-igcombo-listitem').eq(i).find('.ui-icon').removeClass('ui-igcombo-checkbox-off ui-igcheckbox-small-off');
	                indArr.push(i+1);
	            }
            }
        }
        if(indArr.length > 0) {
        	attrValIndex = indArr.join(',');
        }
        $("#"+cmbId).val(attrVal);
        $("#"+cmbId).closest('.ui-igcombo').find('.ui-igcombo-hidden-field').val(attrValIndex);
    }
    return -1;
}
/*function getComboBoxMultipleDataItem(cmbId){
	var cmb = $("#"+cmbId).data("igCombo");
	if(cmb){
		return cmb.dataItem();
	}
	return null;
}*/

// Creating function variable
			
var kendoRangePicker = function () { 	
};

kendoRangePicker.prototype.rpContainerId = "";

//LABELS TEXT
kendoRangePicker.prototype.CHOOSE_RANGE_LABEL = "Date Selector: ";		
kendoRangePicker.prototype.FROM_DATE_LABEL = "FROM: ";
kendoRangePicker.prototype.TO_DATE_LABEL = "TO: ";

//SELECT BOX OPTIONS LIST
kendoRangePicker.prototype.CURRENT_DAY = "Current day";
kendoRangePicker.prototype.YESTERDAY_DAY = "Yesterday";
kendoRangePicker.prototype.CURRENT_WEEK = "Current week";
kendoRangePicker.prototype.PREV_WEEK = "Previous week";
kendoRangePicker.prototype.LAST_1_WEEK = "Last 1 week";
kendoRangePicker.prototype.CURRENT_MONTH = "Current month";
kendoRangePicker.prototype.PREV_MONTH = "Previous month";
kendoRangePicker.prototype.LAST_1_MONTH = "Last 1 month";
kendoRangePicker.prototype.CURRENT_QUARTER = "Current quarter";
kendoRangePicker.prototype.PREV_QUARTER = "Previous quarter";
kendoRangePicker.prototype.LAST_3_MONTHS = "Last 3 months";
kendoRangePicker.prototype.CURRENT_HALF_YEAR = "Current half year";
kendoRangePicker.prototype.PREV_HALF_YEAR = "Previous half year";
kendoRangePicker.prototype.LAST_6_MONTHS = "Last 6 months";
kendoRangePicker.prototype.CURRENT_YEAR = "Current year";
kendoRangePicker.prototype.PREV_YEAR = "Previous year";
kendoRangePicker.prototype.LAST_1_YEAR = "Last 1 year";

// create picker method
kendoRangePicker.prototype.createPicker = function (containerId, enableTime,mandatory) {
	this.rpContainerId = containerId;

		// writing html code for picker
	$('#pickerDiv'+containerId).html('');
	
	//If the parameter is true, we need to create time picker, else create just date picker
	
	var dateTimeStr = "<div id='pickerDiv"+containerId+"' class='spaceStyle'>";
		//dateTimeStr = dateTimeStr+"<div id='dateRangesLabel"+containerId+"' style='float:left;width:100px;text-align:right;padding-right:0px'>"+this.CHOOSE_RANGE_LABEL+"</div>";
		dateTimeStr = dateTimeStr+"<span for='dateRangesLabel"+containerId+"' class='rangePickerLabel'> "+this.CHOOSE_RANGE_LABEL+"</span>";
		//dateTimeStr = dateTimeStr+"<span class='smallGapStyle'></span>";
		dateTimeStr = dateTimeStr+"<select id='dateRanges"+containerId+"' style='padding-left:2px;'>";
		dateTimeStr = dateTimeStr+"<option>Select range..</option>";
		dateTimeStr = dateTimeStr+"<option value='0'>"+this.CURRENT_DAY+"</option>";
		dateTimeStr = dateTimeStr+"<option value='1'>"+this.YESTERDAY_DAY+"</option>";
		dateTimeStr = dateTimeStr+"<option value='2'>"+this.CURRENT_WEEK+"</option>";
		dateTimeStr = dateTimeStr+"<option value='3'>"+this.PREV_WEEK+"</option>";
		dateTimeStr = dateTimeStr+"<option value='4'>"+this.LAST_1_WEEK+"</option>";
		dateTimeStr = dateTimeStr+"<option value='5'>"+this.CURRENT_MONTH+"</option>";
		dateTimeStr = dateTimeStr+"<option value='6'>"+this.PREV_MONTH+"</option>";
		dateTimeStr = dateTimeStr+"<option value='7'>"+this.LAST_1_MONTH+"</option>";
		dateTimeStr = dateTimeStr+"<option value='8'>"+this.CURRENT_QUARTER+"</option>";
		dateTimeStr = dateTimeStr+"<option value='9'>"+this.PREV_QUARTER+"</option>";
		dateTimeStr = dateTimeStr+"<option value='10'>"+this.LAST_3_MONTHS+"</option>";
		dateTimeStr = dateTimeStr+"<option value='11'>"+this.CURRENT_HALF_YEAR+"</option>";
		dateTimeStr = dateTimeStr+"<option value='12'>"+this.PREV_HALF_YEAR+"</option>";
		dateTimeStr = dateTimeStr+"<option value='13'>"+this.LAST_6_MONTHS+"</option>";
		dateTimeStr = dateTimeStr+"<option value='14'>"+this.CURRENT_YEAR+"</option>";
		dateTimeStr = dateTimeStr+"<option value='15'>"+this.PREV_YEAR+"</option>";
		dateTimeStr = dateTimeStr+"<option value='16'>"+this.LAST_1_YEAR+"</option></select>";
		dateTimeStr = dateTimeStr+"<span class='smallGapStyle'></span>";
		dateTimeStr = dateTimeStr+"<label for='startdatetimepickerlbl"+containerId+"' class='rangePickerStyle' > "+this.FROM_DATE_LABEL+"</label>";
		//dateTimeStr = dateTimeStr+"<span class='smallGapStyle'></span>";
		dateTimeStr = dateTimeStr+"<input id='startdatetimepicker"+containerId+"' />";
		
		if(enableTime == true){
			dateTimeStr = dateTimeStr+"<div style='display:none;padding-left:2px;'>";
			dateTimeStr = dateTimeStr+"<input id='sdhours"+containerId+"' type='number' maxlength='2' value='00' min='00' max='23' step='1' />";
			dateTimeStr = dateTimeStr+"<span>:</span> ";
			dateTimeStr = dateTimeStr+"<input id='sdminutes"+containerId+"' type='number' maxlength='2' value='00' min='00' max='59' step='1'  /> ";
			dateTimeStr = dateTimeStr+"<span>:</span> ";
			dateTimeStr = dateTimeStr+"<input id='sdseconds"+containerId+"' type='number' maxlength='2' value='00' min='00' max='59' step='1' />"; dateTimeStr = dateTimeStr+"</div>  ";
		}
		if(mandatory == "true"){
			dateTimeStr = dateTimeStr+"<span class='mandatoryClass'> *</span>";
		}
		dateTimeStr = dateTimeStr+"<span class='smallGapStyle'></span>";
		dateTimeStr = dateTimeStr+"<label for='enddatetimepickerlbl"+containerId+"' class='rangePickerStyle'>"+this.TO_DATE_LABEL+"</label><span style='width:2px'></span>";
		dateTimeStr = dateTimeStr+"<input id='enddatetimepicker"+containerId+"' />";
		if(enableTime == true){
			dateTimeStr = dateTimeStr+"<div style='display:none;padding-left:2px'>";
			dateTimeStr = dateTimeStr+"<input id='edhours"+containerId+"' type='number' maxlength='2' value='00' min='00' max='23' step='1' />";
			dateTimeStr = dateTimeStr+"<span>:</span> ";
			dateTimeStr = dateTimeStr+"<input id='edminutes"+containerId+"' type='number' maxlength='2' value='00' min='00' max='59' step='1'  /> ";
			dateTimeStr = dateTimeStr+"<span>:</span> ";
			dateTimeStr = dateTimeStr+"<input id='edseconds"+containerId+"' type='number' maxlength='2' value='00' min='00' max='59' step='1' /> ";
			dateTimeStr = dateTimeStr+"</div>  ";
		}
		if(mandatory == "true"){
			dateTimeStr = dateTimeStr+"<span class='mandatoryClass'> *</span>";
		}
		dateTimeStr = dateTimeStr+"</div>";
		$(dateTimeStr).appendTo('#'+containerId+'');
		
	/*if(enableTime == true) {
		$("<div id='pickerDiv"+containerId+"'><label for='startdatetimepicker' class='rangePickerLabel'>"+this.CHOOSE_RANGE_LABEL+"</label><select id='dateRanges"+containerId+"' class='rangePickerCombo'><option>Select range..</option><option value='0'>"+this.CURRENT_DAY+"</option><option value='1'>"+this.YESTERDAY_DAY+"</option><option value='2'>"+this.CURRENT_WEEK+"</option><option value='3'>"+this.PREV_WEEK+"</option><option value='4'>"+this.LAST_1_WEEK+"</option><option value='5'>"+this.CURRENT_MONTH+"</option><option value='6'>"+this.PREV_MONTH+"</option><option value='7'>"+this.LAST_1_MONTH+"</option><option value='8'>"+this.CURRENT_QUARTER+"</option><option value='9'>"+this.PREV_QUARTER+"</option><option value='10'>"+this.LAST_3_MONTHS+"</option><option value='11'>"+this.CURRENT_HALF_YEAR+"</option><option value='12'>"+this.PREV_HALF_YEAR+"</option><option value='13'>"+this.LAST_6_MONTHS+"</option><option value='14'>"+this.CURRENT_YEAR+"</option><option value='15'>"+this.PREV_YEAR+"</option><option value='16'>"+this.LAST_1_YEAR+"</option></select><label for='startdatetimepicker"+containerId+"' class='rangePickerLabel'>"+this.FROM_DATE_LABEL+"<input id='startdatetimepicker"+containerId+"' /> <div style='display:inline;'> <input id='sdhours"+containerId+"' type='number' maxlength='2' value='00' min='00' max='23' step='1' />  <span>:</span> <input id='sdminutes"+containerId+"' type='number' maxlength='2' value='00' min='00' max='59' step='1'  /> <span>:</span> <input id='sdseconds"+containerId+"' type='number' maxlength='2' value='00' min='00' max='59' step='1' /> </div> </label><label for='enddatetimepicker"+containerId+"' class='rangePickerLabel'>"+this.TO_DATE_LABEL+"<input id='enddatetimepicker"+containerId+"' /> <div style='display:inline;'> <input id='edhours"+containerId+"' type='number' maxlength='2' value='00' min='00' max='23' step='1' />  <span>:</span> <input id='edminutes"+containerId+"' type='number' maxlength='2' value='00' min='00' max='59' step='1'  /> <span>:</span> <input id='edseconds"+containerId+"' type='number' maxlength='2' value='00' min='00' max='59' step='1' /> </div> </label></div>").appendTo('#'+containerId+'');
	} else {
		$("<div id='pickerDiv"+containerId+"'><label for='startdatetimepicker' class='rangePickerLabel'>"+this.CHOOSE_RANGE_LABEL+"</label><select id='dateRanges"+containerId+"' class='rangePickerCombo'><option>Select range..</option><option value='0'>"+this.CURRENT_DAY+"</option><option value='1'>"+this.YESTERDAY_DAY+"</option><option value='2'>"+this.CURRENT_WEEK+"</option><option value='3'>"+this.PREV_WEEK+"</option><option value='4'>"+this.LAST_1_WEEK+"</option><option value='5'>"+this.CURRENT_MONTH+"</option><option value='6'>"+this.PREV_MONTH+"</option><option value='7'>"+this.LAST_1_MONTH+"</option><option value='8'>"+this.CURRENT_QUARTER+"</option><option value='9'>"+this.PREV_QUARTER+"</option><option value='10'>"+this.LAST_3_MONTHS+"</option><option value='11'>"+this.CURRENT_HALF_YEAR+"</option><option value='12'>"+this.PREV_HALF_YEAR+"</option><option value='13'>"+this.LAST_6_MONTHS+"</option><option value='14'>"+this.CURRENT_YEAR+"</option><option value='15'>"+this.PREV_YEAR+"</option><option value='16'>"+this.LAST_1_YEAR+"</option></select><label for='startdatetimepicker"+containerId+"' class='rangePickerLabel'>"+this.FROM_DATE_LABEL+"<input id='startdatetimepicker"+containerId+"' /> </label><label for='enddatetimepicker"+containerId+"' class='rangePickerLabel'>"+this.TO_DATE_LABEL+"<input id='enddatetimepicker"+containerId+"' /></label></div>").appendTo('#'+containerId+'');	
	}*/
	
	// binding options to kendo combo box
	$('#dateRanges'+containerId+'').kendoComboBox();
	$('#dateRanges'+containerId+'').attr('data-ctrid',containerId);
	$('#sdhours'+containerId+'').on("change",function(){
		startDateChange(containerId);
	});
	$('#sdminutes'+containerId+'').on("change",function(){
		startDateChange(containerId);
	});
	$('#sdseconds'+containerId+'').on("change",function(){
		startDateChange(containerId);
	});
	
	$('#edhours'+containerId+'').on("change",function(){
		startDateChange(containerId);
	});
	$('#edminutes'+containerId+'').on("change",function(){
		startDateChange(containerId);
	});
	$('#edseconds'+containerId+'').on("change",function(){
		startDateChange(containerId);
	});

	// from date kendo date picker
	var k_startdatetimepicker = $("#startdatetimepicker"+containerId+"").kendoDatePicker({
		change: this.startDateChange,
		dateFormat: "dd-mm-yyyy",  //date format
		format: "dd-MM-yyyy"  //date format
	}).data("kendoDatePicker");

	$("#startdatetimepicker"+containerId+"").attr('data-ctrid',containerId);
	
	// to date kendo date picker
	var k_enddatetimepicker = $("#enddatetimepicker"+containerId+"").kendoDatePicker({
		change: this.endDateChange,
		dateFormat: "dd-mm-yyyy",  //date format
		format: "dd-MM-yyyy"  //date format
	}).data("kendoDatePicker");	
	
	$("#enddatetimepicker"+containerId+"").attr('data-ctrid',containerId);
	
	//If the parameter is true, we need to create time picker, else create just date picker
	if(enableTime == true) {
		$("#sdhours"+containerId+", #sdminutes"+containerId+", #sdseconds"+containerId+", #edhours"+containerId+", #edminutes"+containerId+", #edseconds"+containerId).width(30).kendoNumericTextBox({
			format: "{0:n0}",
			spinners: false
		});
	}

  
	// Kendo comboBox onChange function
  $('#dateRanges'+containerId+'').change(function() {
	var containerId = $(this).attr('data-ctrid');
	
	var k_startdatetimepicker = $("#startdatetimepicker"+containerId+"").data("kendoDatePicker");
	var k_enddatetimepicker = $("#enddatetimepicker"+containerId+"").data("kendoDatePicker");
  
  //retrieving value from the selected item
  var val = $('#dateRanges'+containerId+'').val();
  if(val == "Select range.."){
	k_startdatetimepicker.min(new Date(1900, 0, 1));
	k_startdatetimepicker.max(new Date(2099, 11, 31));
	k_enddatetimepicker.min(new Date(1900, 0, 1));
	k_enddatetimepicker.max(new Date(2099, 11, 31));
	k_startdatetimepicker.value(null);
	//startChange();
	k_enddatetimepicker.value(null);
	//endChange();
	
  }
  
  if(val == 0){ // today's date
  var todaysDate = getOnlyDate();
	k_startdatetimepicker.min(new Date(1900, 0, 1));
	k_startdatetimepicker.max(new Date(2099, 11, 31));
  
	k_startdatetimepicker.value(todaysDate);
	startChange(containerId);
	if(k_startdatetimepicker.value() == null){
	
		alert(k_startdatetimepicker.value(todaysDate));
	}
		
	k_enddatetimepicker.value(todaysDate);
	endChange(containerId);
	
  }
  if(val == 1){ // yesterday's date
  
  var yesterDay = getOnlyDate();
  k_startdatetimepicker.min(new Date(1900, 0, 1));
	k_startdatetimepicker.max(new Date(2099, 11, 31));
  yesterDay.setDate(yesterDay.getDate() - 1);
	k_startdatetimepicker.value(yesterDay);
	startChange(containerId); 
	k_enddatetimepicker.value(yesterDay);
	endChange(containerId);	
  }
  if(val == 2){ // current week
	  var curr = getOnlyDate(); // get current date
	  k_startdatetimepicker.min(new Date(1900, 0, 1));
	k_startdatetimepicker.max(new Date(2099, 11, 31));
	  var first = curr.getDate() - curr.getDay(); // First day of the week
	  var firstday = new Date(curr.setDate(first));
	  k_startdatetimepicker.value(firstday);
	 startChange(containerId); 
	 var currentWeek = getOnlyDate();
	k_enddatetimepicker.value(currentWeek);
	endChange(containerId);	 
  }
  if(val == 3){ // previous week
	  var curr = getOnlyDate();
	  k_startdatetimepicker.min(new Date(1900, 0, 1));
	k_startdatetimepicker.max(new Date(2099, 11, 31));
		var diffLastSunday = - curr.getDay()-7;
		var lastDay = - curr.getDay()-1;
		var lastSunday = new Date(curr.getTime() + (diffLastSunday * 3600 * 24 * 1000)); 
		var lastWeekDay = new Date(curr.getTime() + (lastDay * 3600 * 24 * 1000));  		
	  k_startdatetimepicker.value(lastSunday);
	 startChange(containerId);
	 k_enddatetimepicker.value(lastWeekDay);
	endChange(containerId);
  }
  if(val == 4){ // Last 1 week
	  var firstDate = getOnlyDate();
	  k_startdatetimepicker.min(new Date(1900, 0, 1));
	k_startdatetimepicker.max(new Date(2099, 11, 31));
	firstDate.setDate(firstDate.getDate() - 7);
	k_startdatetimepicker.value(firstDate);
	startChange(containerId); 
	 var todayDate = getOnlyDate();
	k_enddatetimepicker.value(todayDate);
	endChange(containerId); 
  }
  if(val == 5){ // Current month
	 var date = getOnlyDate();
	 k_startdatetimepicker.min(new Date(1900, 0, 1));
	k_startdatetimepicker.max(new Date(2099, 11, 31));
	var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
	//var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0); 
	  k_startdatetimepicker.value(firstDay);
	 startChange(containerId); 
		var todayDate = getOnlyDate();
	 k_enddatetimepicker.value(todayDate);
	endChange(containerId); 
  }
  if(val == 6){ // Previous month
	 var today = getOnlyDate();
	 k_startdatetimepicker.min(new Date(1900, 0, 1));
	k_startdatetimepicker.max(new Date(2099, 11, 31));
    var previousMonth = new Date(today.getFullYear(), today.getMonth() - 1, 1); 
	var lastDay = new Date(today.getFullYear(), today.getMonth(), 0); 	
	  k_startdatetimepicker.value(previousMonth);
	 startChange(containerId); 
    k_enddatetimepicker.value(lastDay);
	endChange(containerId);	 
  }
  if(val == 7){ // Last 1 month
	 var firstDate = getOnlyDate();
	 k_startdatetimepicker.min(new Date(1900, 0, 1));
	k_startdatetimepicker.max(new Date(2099, 11, 31));
	firstDate.setDate(firstDate.getDate() - 30);
	k_startdatetimepicker.value(firstDate);
	startChange(containerId); 
	 var todayDate = getOnlyDate();
	k_enddatetimepicker.value(todayDate);
	endChange(containerId); 
  }
  if(val == 8){ // Current quarter
	 var today             = getOnlyDate();
	 k_startdatetimepicker.min(new Date(1900, 0, 1));
	k_startdatetimepicker.max(new Date(2099, 11, 31));
	 var currentYear       = today.getFullYear();
   	 var currentMonth      = today.getMonth(); 
	 var firstDayCurrQuart = new Date( currentYear, firstMonthOfQuarter( currentMonth ), 1 );
	  k_startdatetimepicker.value(firstDayCurrQuart);
	 startChange(containerId);
	var todayDate = getOnlyDate();
	k_enddatetimepicker.value(todayDate);
	endChange(containerId); 	 
  }
  if(val == 9){ // Previous quarter
	 var today             = getOnlyDate();
	 k_startdatetimepicker.min(new Date(1900, 0, 1));
	k_startdatetimepicker.max(new Date(2099, 11, 31));
	 var currentYear       = today.getFullYear();
   	 var currentMonth      = today.getMonth(); 
		currentYear = currentMonth - 3 > 0 ? currentYear : currentYear - 1;
		currentMonth = currentMonth - 3 > 0 ? currentMonth - 3 : 11 - currentMonth;
	 var firstDayPrevQuart = new Date( currentYear, firstMonthOfQuarter( currentMonth ), 1 );
	 var lastDayPrevQuart = new Date(firstDayPrevQuart.getFullYear(), firstDayPrevQuart.getMonth() + 3, 0);
		
	  k_startdatetimepicker.value(firstDayPrevQuart);
	 startChange(containerId);
	k_enddatetimepicker.value(lastDayPrevQuart);
	endChange(containerId); 
	 
  }
   if(val == 10){ // Last 3 months
	 
	 var present = getOnlyDate();
	 k_startdatetimepicker.min(new Date(1900, 0, 1));
	k_startdatetimepicker.max(new Date(2099, 11, 31));
	 var startDate = new Date(present.getFullYear(), present.getMonth()-3,present.getDate()+1 );
	  k_startdatetimepicker.value(startDate);
	 startChange(containerId);
	 var today = getOnlyDate();
	 k_enddatetimepicker.value(today);
	 endChange(containerId);	
  }
  if(val == 11){ // Current half year
	 var today             = getOnlyDate();
	 k_startdatetimepicker.min(new Date(1900, 0, 1));
	k_startdatetimepicker.max(new Date(2099, 11, 31));
	 var currentYear       = today.getFullYear();
   	 var currentMonth      = today.getMonth(); 
	 var currentHalfYear;
	if(currentMonth < 6){
		currentHalfYear = new Date(new Date().getFullYear(), 0, 1);
	}else{
		currentHalfYear = new Date(new Date().getFullYear(), 6, 1);
	}
		
	  k_startdatetimepicker.value(currentHalfYear);
	 startChange(containerId); 
		var present = getOnlyDate();
	 k_enddatetimepicker.value(present);
	 endChange(containerId);
  }
  if(val == 12){ // Previous half year
	 var today             = getOnlyDate();
	 k_startdatetimepicker.min(new Date(1900, 0, 1));
	k_startdatetimepicker.max(new Date(2099, 11, 31));
	 var currentYear       = today.getFullYear();
   	 var currentMonth      = today.getMonth(); 
	var PreviousHalfYear,lastDay;
	if(currentMonth < 6){
		PreviousHalfYear = new Date(new Date().getFullYear() -1, 6, 1);
		lastDay = new Date(new Date().getFullYear() -1, 12, 0);
	}else{
		PreviousHalfYear = new Date(new Date().getFullYear(), 0, 1);
		lastDay = new Date(new Date().getFullYear(), 6, 0);
	}
		
	  k_startdatetimepicker.value(PreviousHalfYear);
	 startChange(containerId); 
	k_enddatetimepicker.value(lastDay);
	 endChange(containerId);	 
  }
  
  if(val == 13){ // Last 6 months
	 var present = getOnlyDate();
	 k_startdatetimepicker.min(new Date(1900, 0, 1));
	k_startdatetimepicker.max(new Date(2099, 11, 31));
	 var startDate = new Date(present.getFullYear(), present.getMonth()-6,present.getDate()+1 );
	  k_startdatetimepicker.value(startDate);
	 startChange(containerId);
	 var today = getOnlyDate();
	 k_enddatetimepicker.value(today);
	 endChange(containerId);
  }
  if(val == 14){ // Current year
	 var today = getOnlyDate();
	 k_startdatetimepicker.min(new Date(1900, 0, 1));
	k_startdatetimepicker.max(new Date(2099, 11, 31));
     var currentYear = new Date(today.getFullYear(), 0, 1);  
	  k_startdatetimepicker.value(currentYear);
	 startChange(containerId); 
	var present = getOnlyDate();
	 k_enddatetimepicker.value(present);
	 endChange(containerId);	 
  }
  if(val == 15){ // Previous year
	 var today = getOnlyDate();
	 k_startdatetimepicker.min(new Date(1900, 0, 1));
	k_startdatetimepicker.max(new Date(2099, 11, 31));
     var prevYear = new Date(today.getFullYear() - 1, 0, 1); 
	 var prevYearLastDay = new Date(today.getFullYear() - 1, 12, 0);  
	  k_startdatetimepicker.value(prevYear);
	 startChange(containerId);
	 k_enddatetimepicker.value(prevYearLastDay);
	 endChange(containerId);
  }
  if(val == 16){ // Last 1 year
	 var present = getOnlyDate();
	 k_startdatetimepicker.min(new Date(1900, 0, 1));
	k_startdatetimepicker.max(new Date(2099, 11, 31));
	 var startDate = new Date(present.getFullYear(), present.getMonth()-12,present.getDate()+1 );
	  k_startdatetimepicker.value(startDate);
	 startChange(containerId);
	 var today = getOnlyDate();
	 k_enddatetimepicker.value(today);
	 endChange(containerId);
  }



	//Check if range controls have data and also if we are dealing with a date time component, add hours as 23:59:59.
	var calEndDate = k_enddatetimepicker.value();
	if(calEndDate!=null && calEndDate!="") {
		if($("#edhours"+containerId)!= null && $("#edhours"+containerId).data("kendoNumericTextBox")!=null) {
			$("#edhours"+containerId).data("kendoNumericTextBox").value(23);
			$("#edminutes"+containerId).data("kendoNumericTextBox").value(59);
			$("#edseconds"+containerId).data("kendoNumericTextBox").value(59);
			$("#sdhours"+containerId).data("kendoNumericTextBox").value(00);
			$("#sdminutes"+containerId).data("kendoNumericTextBox").value(00);
			$("#sdseconds"+containerId).data("kendoNumericTextBox").value(00);
		}
	}
});
  
localStorage.setItem('containerId',containerId);
}; // CLOSING RANGE FUNCTION

kendoRangePicker.prototype.getStartDateTime = function () {
	return getRangePickerStartDate(this.rpContainerId);
}


function getCPDateTime(containerId) {
	var dt = $('#cdtpdatetimepicker'+containerId).data('kendoDatePicker').value();
	var hours = 23;
	var mins = 59;
	var secs = 59;
	if($('#cdtphours'+containerId)!=null && $('#cdtphours'+containerId).data('kendoNumericTextBox') != null) {
		hours = $('#cdtphours'+containerId).data('kendoNumericTextBox').value();
	}
	if($('#cdtpminutes'+containerId)!=null && $('#cdtpminutes'+containerId).data('kendoNumericTextBox') != null) {
		mins = $('#cdtpminutes'+containerId).data('kendoNumericTextBox').value();
	}
	if($('#cdtpseconds'+containerId)!=null && $('#cdtpseconds'+containerId).data('kendoNumericTextBox') != null) {
		secs = $('#cdtpseconds'+containerId).data('kendoNumericTextBox').value();
	}

	dt.setHours(hours);
	dt.setMinutes(mins);
	dt.setSeconds(secs);
	
	return dt;
}

function getRangePickerStartDate(containerId) {
	var k_startdatetimepicker = $("#startdatetimepicker"+containerId+"").data("kendoDatePicker");
	var startDateHours = 0;
	var startDateMinutes = 0;
	var startDateSeconds = 0;
	
	if($('#sdhours'+containerId)!=null && $('#sdhours'+containerId).data('kendoNumericTextBox') != null) {
		startDateHours = $('#sdhours'+containerId).data('kendoNumericTextBox').value();
	}
	if($('#sdminutes'+containerId)!=null && $('#sdminutes'+containerId).data('kendoNumericTextBox') != null) {
		startDateMinutes = $('#sdminutes'+containerId).data('kendoNumericTextBox').value();
	}
	if($('#sdseconds'+containerId)!=null && $('#sdseconds'+containerId).data('kendoNumericTextBox') != null) {
		startDateSeconds = $('#sdseconds'+containerId).data('kendoNumericTextBox').value();
	}

	var startDate = k_startdatetimepicker.value();
	if(startDate){
		startDate = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate(), startDateHours, startDateMinutes, startDateSeconds);
	}	

	return startDate;
}

kendoRangePicker.prototype.getEndDateTime = function () {
	
	return getRangePickerEndDate(this.rpContainerId);
}
kendoRangePicker.prototype.setRangeLabelStyle = function (cls) {
	if(this.rpContainerId){
		$("#"+this.rpContainerId).find("#dateRangesLabel"+this.rpContainerId).addClass(cls);
		//console.log($("#"+this.rpContainerId).find("dateRangesLabel"+this.rpContainerId));
		//alert(this.rpContainerId+','+wdth);
	}
}
function getRangePickerEndDate(containerId) {
	var k_enddatetimepicker = $("#enddatetimepicker"+containerId+"").data("kendoDatePicker");
	var endDateHours = 23;
	var endDateMinutes = 59;
	var endDateSeconds = 59;

	if($('#edhours'+containerId)!=null &&  $('#edhours'+containerId).data('kendoNumericTextBox') != null) {
		endDateHours = $('#edhours'+containerId).data('kendoNumericTextBox').value();
	}
	if($('#edminutes'+containerId)!=null && $('#edminutes'+containerId).data('kendoNumericTextBox') != null) {
		endDateMinutes = $('#edminutes'+containerId).data('kendoNumericTextBox').value();
	}
	if($('#edseconds'+containerId)!=null && $('#edseconds'+containerId).data('kendoNumericTextBox') != null) {
		endDateSeconds = $('#edseconds'+containerId).data('kendoNumericTextBox').value();
	}
	
	var	endDate = k_enddatetimepicker.value();	
	if(endDate){
		endDate = new Date(endDate.getFullYear(), endDate.getMonth(), endDate.getDate(), endDateHours, endDateMinutes, endDateSeconds);	
	}
	
	return endDate;
}
//getOnlyDate method
function getOnlyDate()
{
  var todaysDate = new Date();
  //todaysDate = new Date(todaysDate.getMonth()+"/"+todaysDate.getDate()+"/"+todaysDate.getFullYear());
  todaysDate = new Date(todaysDate.getFullYear(), todaysDate.getMonth(), todaysDate.getDate());
  return todaysDate;
}
function firstMonthOfQuarter( monthNum )
{
    switch( monthNum )
    {
        case 0:
        case 1:
        case 2: return 0;
        case 3:
        case 4:
        case 5: return 3;
        case 6:
        case 7:
        case 8: return 6;
        case 9:
        case 10:
        case 11: return 9;
    }
}
// startChange method
kendoRangePicker.prototype.startDateChange = function() { 

	//startChange(this.rpContainerId);
	startDateChange(this.dateView._dateViewID.substring(19,this.dateView._dateViewID.indexOf('_dateview')));
}

function startDateChange(containerId) {
	 var k_startdatetimepicker = $("#startdatetimepicker"+containerId+"").data('kendoDatePicker');
	 var k_enddatetimepicker = $("#enddatetimepicker"+containerId+"").data('kendoDatePicker');
	 
      var startDate = k_startdatetimepicker.value(),
          endDate = k_enddatetimepicker.value();

      if (startDate) {
          startDate = new Date(startDate);
          startDate.setDate(startDate.getDate());
          k_enddatetimepicker.min(startDate);
      } else if (endDate) {
          k_startdatetimepicker.max(new Date(endDate));
      } else {
          endDate = getOnlyDate();
          k_startdatetimepicker.max(endDate);
          k_enddatetimepicker.min(endDate);
      }
	  var dateRangeCmb =  $('#dateRanges'+containerId).data("kendoComboBox");
	 if(dateRangeCmb){
		dateRangeCmb.select(0);
	 }
  }
function startChange(containerId) {
	 var k_startdatetimepicker = $("#startdatetimepicker"+containerId+"").data('kendoDatePicker');
	 var k_enddatetimepicker = $("#enddatetimepicker"+containerId+"").data('kendoDatePicker');
	 
      var startDate = k_startdatetimepicker.value(),
          endDate = k_enddatetimepicker.value();

      if (startDate) {
          startDate = new Date(startDate);
          startDate.setDate(startDate.getDate());
          k_enddatetimepicker.min(startDate);
      } else if (endDate) {
          k_startdatetimepicker.max(new Date(endDate));
      } else {
          endDate = getOnlyDate();
          k_startdatetimepicker.max(endDate);
          k_enddatetimepicker.min(endDate);
      }
	  
  }
  
 

  // endChange method
kendoRangePicker.prototype.endDateChange = function() {
	//endChange(this.rpContainerId);
	endDateChange(this.dateView._dateViewID.substring(17,this.dateView._dateViewID.indexOf('_dateview')));
}
function endDateChange(containerId) {
	  var k_startdatetimepicker = $("#startdatetimepicker"+containerId+"").data('kendoDatePicker');
	  var k_enddatetimepicker = $("#enddatetimepicker"+containerId+"").data('kendoDatePicker');

      var endDate = k_enddatetimepicker.value(),
          startDate = k_startdatetimepicker.value();
      //console.log(endDate, startDate);

      if (endDate) {
          endDate = new Date(endDate);
          endDate.setDate(endDate.getDate());
          k_startdatetimepicker.max(endDate);
      } else if (startDate) {
          k_enddatetimepicker.min(new Date(startDate));
      } else {
          endDate = getOnlyDate();
          k_startdatetimepicker.max(endDate);
          k_enddatetimepicker.min(endDate);
      }
	  var dateRangeCmb =  $('#dateRanges'+containerId).data("kendoComboBox");
	 if(dateRangeCmb){
		dateRangeCmb.select(0);
	 }
  }
  
function endChange(containerId) {
	  var k_startdatetimepicker = $("#startdatetimepicker"+containerId+"").data('kendoDatePicker');
	  var k_enddatetimepicker = $("#enddatetimepicker"+containerId+"").data('kendoDatePicker');

      var endDate = k_enddatetimepicker.value(),
          startDate = k_startdatetimepicker.value();
      //console.log(endDate, startDate);

      if (endDate) {
          endDate = new Date(endDate);
          endDate.setDate(endDate.getDate());
          k_startdatetimepicker.max(endDate);
      } else if (startDate) {
          k_enddatetimepicker.min(new Date(startDate));
      } else {
          endDate = getOnlyDate();
          k_startdatetimepicker.max(endDate);
          k_enddatetimepicker.min(endDate);
      }
  }
  
 // method to retrieving dates for quarter 
kendoRangePicker.prototype.firstMonthOfQuarter = function( monthNum )
{
    switch( monthNum )
    {
        case 0:
        case 1:
        case 2: return 0;
        case 3:
        case 4:
        case 5: return 3;
        case 6:
        case 7:
        case 8: return 6;
        case 9:
        case 10:
        case 11: return 9;
    }
}





			
var kendoCustomDateTimePicker = function () { 	
};

kendoCustomDateTimePicker.prototype.rpContainerId = "";

//LABELS TEXT
kendoCustomDateTimePicker.prototype.LABEL = "Date: ";		


// create picker method
kendoCustomDateTimePicker.prototype.createPicker = function (containerId, enableTime) {
	
	this.rpContainerId = containerId;

	// writing html code for picker
	$('#cdtppickerDiv'+containerId).html('');
	
	//If the parameter is true, we need to create time picker, else create just date picker
	var dateStr = "<div id='cdtppickerDiv"+containerId+"' class='spaceStyle'>";
	dateStr = dateStr+"<label for='cdtpdatetimepickerlbl"+containerId+"' class='rangePickerLabel' style='margin-right:2px'>"+this.LABEL+ "</label>";
	dateStr = dateStr+"<input id='cdtpdatetimepicker"+containerId+"'  />";
	if(enableTime == true) {
		dateStr = dateStr+"<div style='display:inline;padding-left:2px'>";
		dateStr = dateStr+"<input id='cdtphours"+containerId+"' type='number' maxlength='2' value='00' min='00' max='23' step='1' margin-left:2px />";
		dateStr = dateStr+"<span>:</span>";
		dateStr = dateStr+"<input id='cdtpminutes"+containerId+"' type='number' maxlength='2' value='00' min='00' max='59' step='1'  />";
		dateStr = dateStr+"<span>:</span>";
		dateStr = dateStr+"<input id='cdtpseconds"+containerId+"' type='number' maxlength='2' value='00' min='00' max='59' step='1' />";
		dateStr = dateStr+"</div></div>"
	}else{
	}
	$(dateStr).appendTo('#'+containerId+'')
	/*if(enableTime == true) {
		$("<div id='cdtppickerDiv"+containerId+"'><label for='cdtpdatetimepicker"+containerId+"' class='rangePickerLabel'>"+this.LABEL+"<input id='cdtpdatetimepicker"+containerId+"' /> <div style='display:inline;'> <input id='cdtphours"+containerId+"' type='number' maxlength='2' value='00' min='00' max='23' step='1' />  <span>:</span> <input id='cdtpminutes"+containerId+"' type='number' maxlength='2' value='00' min='00' max='59' step='1'  /> <span>:</span> <input id='cdtpseconds"+containerId+"' type='number' maxlength='2' value='00' min='00' max='59' step='1' /> </div> </label></div>").appendTo('#'+containerId+'');
	} else {
		$("<div id='cdtppickerDiv"+containerId+"'><label for='cdtpdatetimepicker"+containerId+"' class='rangePickerLabel'>"+this.LABEL+"<input id='cdtpdatetimepicker"+containerId+"' /> </label></div>").appendTo('#'+containerId+'');	
	}*/
	

	// from date kendo date picker
	var k_startdatetimepicker = $("#cdtpdatetimepicker"+containerId+"").kendoDatePicker({
		change: this.startChange,
		dateFormat: "dd-mm-yyyy",  //date format
		format: "dd-MM-yyyy"  //date format
	}).data("kendoDatePicker");


	//If the parameter is true, we need to create time picker, else create just date picker
	if(enableTime == true) {
		$("#cdtphours"+containerId+", #cdtpminutes"+containerId+", #cdtpseconds"+containerId).width(30).kendoNumericTextBox({
			format: "{0:n0}",
			spinners: false
		});
	}
}
kendoCustomDateTimePicker.prototype.getDateTime = function () {

    return getCustomDateTimePicker(this.rpContainerId);
}

function getCustomDateTimePicker(containerId) {
    var cust_datetimepicker = $("#cdtpdatetimepicker"+containerId+"").data("kendoDatePicker");
    var custDateHours = 23;
    var custDateMinutes = 59;
    var custDateSeconds = 59;
    
    if($('#cdtphours'+containerId)!=null && $('#cdtphours'+containerId).data('kendoNumericTextBox') != null) {
        custDateHours = $('#cdtphours'+containerId).data('kendoNumericTextBox').value();
    }
    if($('#cdtpminutes'+containerId)!=null && $('#cdtpminutes'+containerId).data('kendoNumericTextBox') != null) {
        custDateMinutes = $('#cdtpminutes'+containerId).data('kendoNumericTextBox').value();
    }
    if($('#cdtpseconds'+containerId)!=null && $('#cdtpseconds'+containerId).data('kendoNumericTextBox') != null) {
        custDateSeconds = $('#cdtpseconds'+containerId).data('kendoNumericTextBox').value();
    }

    var objDate = cust_datetimepicker.value();
    if(objDate){
        objDate = new Date(objDate.getFullYear(), objDate.getMonth(), objDate.getDate(), custDateHours, custDateMinutes, custDateSeconds);
    }    

    return objDate;
}

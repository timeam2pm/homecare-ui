var angularUIgridWrapper;
var parentRef = null;
var recordType = "1";
var dataArray = [];
$(document).ready(function(){
    parentRef = parent.frames['iframe'].window;
    //recordType = parentRef.dietType;
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("dgridMiniTemplateList", dataOptions);
    angularUIgridWrapper.init();
    buildDeviceListGrid([]);
});

$(window).load(function(){
    loading = false;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
    $("#btnEdit").prop("disabled", true);
    $("#btnDelete").prop("disabled", true);
    $("#cmbMiniTemplates").kendoComboBox();
    $("#cmbComponent").kendoComboBox();
    getTemplates();
    getComponents();
    init();
    buttonEvents();
    adjustHeight();
}

function getTemplates(){
    var ipaddress = ipAddress+"/homecare/mini-templates/";
    getAjaxObject(ipaddress,"GET",handleGetTemplateList,onError);
}
function handleGetTemplateList(dataObj){
    var vacation = [];
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            if(dataObj.response.miniTemplates){
                if($.isArray(dataObj.response.miniTemplates)){
                    vacation = dataObj.response.miniTemplates;
                }else{
                    vacation.push(dataObj.response.miniTemplates);
                }
            }
        }
    }
    for(var j=0;j<vacation.length;j++){
        vacation[j].idk = vacation[j].id;
    }
    setDataForSelection(vacation, "cmbMiniTemplates", function(){}, ["name", "idk"], 0, "");
}
function getComponents(){
    getAjaxObject(ipAddress+"/master/type/list/?name=component","GET",getComponentTypes,onError);
}
function getComponentTypes(dataObj){
    var tempCompType = [];
    compType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.typeMaster){
            if($.isArray(dataObj.response.typeMaster)){
                tempCompType = dataObj.response.typeMaster;
            }else{
                tempCompType.push(dataObj.response.typeMaster);
            }
        }
    }
    for(var i=0;i<tempCompType.length;i++){
        var obj = {};
        obj.Key = tempCompType[i].type;
        obj.Value = tempCompType[i].id;
        compType.push(obj);
    }
    setDataForSelection(compType, "cmbComponent", function(){}, ["Key", "Value"], 0, "");
}
function init(){
    $("#btnEdit").prop("disabled", true);
    $("#btnDelete").prop("disabled", true);
    buildDeviceListGrid([]);
    var ipaddress = ipAddress+"/homecare/mini-components/";
    getAjaxObject(ipaddress,"GET",handleGetVacationList,onError);
    //getAjaxObject(ipAddress+"/homecare/activity-types/?is-active=1&is-deleted=0&sort=display-order","GET",getActivityTypes,onError);
}

function handleGetVacationList(dataObj){
    console.log(dataObj);
    var vacation = [];
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            if(dataObj.response.miniComponents){
                if($.isArray(dataObj.response.miniComponents)){
                    vacation = dataObj.response.miniComponents;
                }else{
                    vacation.push(dataObj.response.miniComponents);
                }
            }
        }
    }
    for(var j=0;j<vacation.length;j++){
        vacation[j].idk = vacation[j].id;
    }
    buildDeviceListGrid(vacation);
}
function onError(errorObj){
    console.log(errorObj);
}

function buttonEvents(){
    $("#btnSave").off("click",onClickSave);
    $("#btnSave").on("click",onClickSave);


    $("#btnCancelDet").off("click",onClickCancel);
    $("#btnCancelDet").on("click",onClickCancel);


    $("#btnEdit").off("click",onClickEdit);
    $("#btnEdit").on("click",onClickEdit);

    $("#btnDelete").off("click",onClickDelete);
    $("#btnDelete").on("click",onClickDelete);
}

function onClickDelete(){
    customAlert.confirm("Confirm", "Are you sure to delete?",function(response){
        if(response.button == "Yes"){
            var dataObj = {};
            dataObj.createdBy = Number(sessionStorage.userId);;
            dataObj.isDeleted = 1;
            dataObj.isActive = 0;
            var dataUrl = ipAddress +"/homecare/mini-components/";
            var method = "DELETE";
            var selectedItems = angularUIgridWrapper.getSelectedRows();
            dataObj.id = selectedItems[0].idk;
            createAjaxObject(dataUrl, dataObj, method, onCreateDelete, onError);
        }
    });

}
function onCreateDelete(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "Template deleted successfully";
            customAlert.error("Info", msg);
            operation = ADD;
            init();
        }
    }
}
function onClickSave(){
    try{
        var cmbMiniTemplates = $("#cmbMiniTemplates").data("kendoComboBox");
        var cmbComponent = $("#cmbComponent").data("kendoComboBox");

        var reqData = {};
        reqData.name = $("#txtComponentName").val();
        reqData.label = $("#txtComponentName").val();
        reqData.notes = $("#taNotes").val();
        reqData.miniTemplateId = Number(cmbMiniTemplates.value());
        reqData.componentTypeId = Number(cmbComponent.value());
        //reqData.formTemplateId = cmbTemplates.value();
        reqData.isActive = 1;
        reqData.isDeleted = 0;
        reqData.createdBy = 101;

        console.log(reqData);
        var method = "POST";
        if(operation == UPDATE){
            var selectedItems = angularUIgridWrapper.getSelectedRows();
            reqData.id = selectedItems[0].idk;
            method = "PUT";
        }
        dataUrl = ipAddress +"/homecare/mini-components/";
        createAjaxObject(dataUrl, reqData, method, onCreate, onError);
    }catch(ex){
        console.log(ex);
    }
}
function onCreate(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "Template Created  Successfully";
            displaySessionErrorPopUp("Info", msg, function(res) {
                resetData();
                operation = ADD;
                init();
            })
        }else{
            customAlert.error("Error", dataObj.response.status.message);
        }
    }else{

    }
}

function resetData(){
    $("#taNotes").val("");
    $("#txtComponentName").val("");
    var cmbComponent = $("#cmbComponent").data("kendoComboBox");
    if(cmbComponent){
        cmbComponent.select(0);
    }
    var cmbMiniTemplates = $("#cmbMiniTemplates").data("kendoComboBox");
    if(cmbMiniTemplates){
        cmbMiniTemplates.select(0);
    }
}
function adjustHeight(){
    var defHeight = 280;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    if(angularUIgridWrapper){
        angularUIgridWrapper.adjustGridHeight(cmpHeight);
    }
}

function buildDeviceListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Name",
        "field": "name",
    });
    gridColumns.push({
        "title": "Component",
        "field": "componentTypeId",
    });
    gridColumns.push({
        "title": "Mini Template",
        "field": "miniTemplateId",
    });
    gridColumns.push({
        "title": "Notes",
        "field": "notes",
    });
    gridColumns.push({
        "title": "Display Order",
        "field": "displayOrder",
    });
    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}

var selRow = null;
function onClickAction(){

}

var prevSelectedItem =[];
function onChange(){
    setTimeout(function() {
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if (selectedItems && selectedItems.length > 0) {
            var obj = selectedItems[0];
            atID = obj.idk;
            $("#btnEdit").prop("disabled", false);
            $("#btnDelete").prop("disabled", false);
            //onClickEdit();
        } else {
            $("#btnEdit").prop("disabled", true);
            $("#btnDelete").prop("disabled", true);
            //resetData();
        }
    });

}
var operation = "ADD";
var ADD = "ADD";
var UPDATE = "UPDATE";
var atID = "";
var ds  = "";
function onClickEdit(){
    var selectedItems = angularUIgridWrapper.getSelectedRows();
    console.log(selectedItems);
    if (selectedItems && selectedItems.length > 0) {
        var obj = selectedItems[0];
        operation = UPDATE;
        $("#txtComponentName").val(obj.name);
        $("#taNotes").val(obj.notes);
        var cmbIdx =  getComboListIndex("cmbComponent", "Value", obj.componentTypeId);
        if(cmbIdx>=0){
            var cmbComponent = $("#cmbComponent").data("kendoComboBox");
            if(cmbComponent){
                cmbComponent.select(cmbIdx);
            }
        }
        var tmbIdx =  getComboListIndex("cmbMiniTemplates", "idk", obj.miniTemplateId);
        if(tmbIdx>=0){
            var cmbMiniTemplates = $("#cmbMiniTemplates").data("kendoComboBox");
            if(cmbMiniTemplates){
                cmbMiniTemplates.select(tmbIdx);
            }
        }
    }
}

function getComboListIndex(cmbId, attr, attrVal) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb) {
        var ds = cmb.dataSource;
        var totalRec = ds.total();
        for (var i = 0; i < totalRec; i++) {
            var dtItem = ds.at(i);
            if (dtItem && dtItem[attr] == attrVal) {
                cmb.select(i);
                return i;
            }
        }
    }
    return -1;
}
function closeVideoScreen(evt,returnData){

}
var operation = "add";
var selFileResourceDataItem = null;
function onClickCancel(){
    var obj = {};
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}

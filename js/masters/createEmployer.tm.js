
var parentRef = null;
var operation = "";
var selItem = null;
var ADD = "add";
var UPDATE = "update";
var VIEW = "view";
var DELETE = "delete";
var statusArr = [{Key:'Active',Value:'Active'},{Key:'InActive',Value:'InActive'}];
$(document).ready(function(){
    parentRef = parent.frames['iframe'].window;

});

function adjustHeight(){
    var defHeight = 45;
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 40;
    }
}

$(window).load(function(){
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
    init();
});

function init(){
    operation = parentRef.operation;
    selItem = parentRef.selItem;
    $("#cmbStatus").kendoComboBox();
    setDataForSelection(statusArr, "cmbStatus", onStatusChange, ["Value", "Key"], 0, "");
    if(operation == UPDATE && selItem){
        console.log(selItem);
        $("#txtAbbrevation").val(selItem.abbr);
        $("#txtName").val(selItem.country);
        $("#txtCode").val(selItem.code);

        var cmbStatus = $("#cmbStatus").data("kendoComboBox");
        if(cmbStatus){
            if(selItem.isActive == 1){
                cmbStatus.select(0);
            }else{
                cmbStatus.select(1);
            }
        }
    }else{
        var cmbStatus = $("#cmbStatus").data("kendoComboBox");
        if(cmbStatus){
            cmbStatus.enable(false);
        }
    }
    buttonEvents();
}
function onStatusChange(){
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if(cmbStatus && cmbStatus.selectedIndex<0){
        cmbStatus.select(0);
    }
}
function buttonEvents(){
    $("#btnCancel").off("click",onClickCancel);
    $("#btnCancel").on("click",onClickCancel);

    $("#btnSave").off("click",onClickSave);
    $("#btnSave").on("click",onClickSave);

    $("#btnSearch").off("click",onClickSearch);
    $("#btnSearch").on("click",onClickSearch);

    $("#btnReset").off("click",onClickReset);
    $("#btnReset").on("click",onClickReset);
}

function onClickReset(){
    operation = ADD;
    selItem = null;
    $("#txtAbbrevation").val("");
    $("#txtCode").val("");
    $("#txtName").val("");
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if(cmbStatus){
        cmbStatus.select(0);
        cmbStatus.enable(false);
    }
}
function validation(){
    var flag = true;
    var strAbbr = $("#txtAbbrevation").val();
    strAbbr = $.trim(strAbbr);
    if(strAbbr == ""){
        customAlert.error("Error","Enter Abbrevation");
        flag = false;
        return false;
    }
    var strCode = $("#txtCode").val();
    strCode = $.trim(strCode);
    if(strCode == ""){
        customAlert.error("Error","Enter Code");
        flag = false;
        return false;
    }
    var strName = $("#txtName").val();
    strName = $.trim(strName);
    if(strName == ""){
        customAlert.error("Error","Enter Country");
        flag = false;
        return false;
    }

    return flag;
}

function onClickSave(){
    if(validation()){
        var strAbbr = $("#txtAbbrevation").val();
        strAbbr = $.trim(strAbbr);
        var strCode = $("#txtCode").val();
        strCode = $.trim(strCode);
        var strName = $("#txtName").val();
        strName = $.trim(strName);
        var isActive = 0;
        var cmbStatus = $("#cmbStatus").data("kendoComboBox");
        if(cmbStatus){
            if(cmbStatus.selectedIndex == 0){
                isActive = 1;
            }
        }
        var dataObj = {};
        dataObj.abbr = strAbbr;
        dataObj.country = strName;
        dataObj.code = strCode;
        dataObj.isActive = isActive;
        dataObj.createdBy = "101";//sessionStorage.uName;
        dataObj.isDeleted = "0";
        if(operation == ADD){
            var dataUrl = ipAddress+"/country/create";
            createAjaxObject(dataUrl,dataObj,"POST",onCreate,onError);
        }else if(selItem){
            dataObj.id = selItem.idk;
            var dataUrl = ipAddress+"/country/update";
            createAjaxObject(dataUrl,dataObj,"POST",onCreate,onError);
        }
    }
}

function onCreate(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.status){
        if(dataObj.status.code == "1"){
            if(operation == ADD){
                customAlert.info("info", "Country Created Successfully");
            }else{
                customAlert.info("info", "Country Updated Successfully");
            }

        }else{
            customAlert.error("error", dataObj.status.message);
        }
    }
    /*var obj = {};
    obj.status = "success";
    obj.operation = operation;
    popupClose(obj);*/
}

function onError(errObj){
    console.log(errObj);
    customAlert.error("Error","Unable to create Country");
}
function onClickCancel(){
    var obj = {};
    obj.status = "false";
    popupClose(obj);
}
function onClickSearch(){
    var obj = {};
    obj.status = "search";
    popupClose(obj);
}
function popupClose(obj){
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}



var parentRef = null;
var operation = "";
var selItem = null;
var ADD = "add";
var UPDATE = "edit";
var VIEW = "view";
var DELETE = "delete";
var patientId = "";
var statusArr = [{Key:'Active',Value:'Active'},{Key:'InActive',Value:'InActive'}];

var typeArr = [{Key:'100',Value:'Doctor'},{Key:'200',Value:'Nurse'},{Key:'201',Value:'Caregiver'}];

var patientInfoObject = null;
var commId = "";

$(document).ready(function(){
	parentRef = parent.frames['iframe'].window;
	var pnlHeight = window.innerHeight;
	var imgHeight = pnlHeight-100;
	$("#divTop").height(imgHeight);
	
});

function adjustHeight(){
	var defHeight = 45;
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 40;
    }
}

$(window).load(function(){
	if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
	init();
});

function init(){
	allowNumerics("txtID");
	allowNumerics("txtFC");
	allowAlphaNumeric("txtExtID1");
	allowAlphaNumeric("txtExtID2");
	allowAlphabets("txtAbbreviation");
	allowAlphabets("txtFN");
	allowAlphabets("txtMN");
	allowAlphabets("txtLN");
	allowAlphabets("txtBAN");
	allowAlphabets("txtFAN");
	allowAlphabets("txtNEIC");
	allowAlphabets("txtAS");
	allowAlphabets("txtNT");
	allowAlphabets("txtUPIN");
	allowAlphabets("txtDEA");
	allowAlphabets("txtNPI");
	allowAlphaNumeric("txtTI");
	allowAlphabets("txtPCP");
	allowAlphaNumeric("txtBD");
	allowAlphaNumeric("txtFC");
	allowAlphaNumeric("txtAdd1");
	allowAlphaNumeric("txtAdd2");
	allowPhoneNumber("txtHPhone");
	allowPhoneNumber("txtExtension");
	allowPhoneNumber("txtWPhone");
	allowPhoneNumber("txtExtension1");
	allowPhoneNumber("txtCell");
	validateEmail("txtEmail1");
	
	
	
	//txtNEIC
	operation = parentRef.operation;
	patientId = parentRef.patientId;
	//selItem = parentRef.selItem;
	$("#txtType").kendoComboBox();
	$("#cmbPrefix").kendoComboBox();
	$("#cmbStatus").kendoComboBox();
	//$("#cmbAbbreviation").kendoComboBox();
	$("#cmbZip1").kendoComboBox();
	$("#cmbSMS").kendoComboBox();
	$("#cmbType").kendoComboBox();
	$("#cmbPrefix").kendoComboBox();
	$("#cmbSuffix").kendoComboBox();
	
	setDataForSelection(typeArr, "txtType", function(){}, ["Value", "Key"], 0, "");
	
	getZip();
	buttonEvents();
}
function onStatusChange(){
	var cmbStatus = $("#cmbStatus").data("kendoComboBox");
	if(cmbStatus && cmbStatus.selectedIndex<0){
		cmbStatus.select(0);
	}
}
function buttonEvents(){
	$("#btnCancel").off("click",onClickCancel);
	$("#btnCancel").on("click",onClickCancel);
	
	$("#btnSave").off("click",onClickSave);
	$("#btnSave").on("click",onClickSave);
	
	$("#btnSearch").off("click",onClickSearch);
	$("#btnSearch").on("click",onClickSearch);
	
	$("#btnReset").off("click",onClickReset);
	$("#btnReset").on("click",onClickReset);
	
	$("#btnZipSearch").off("click");
	$("#btnZipSearch").on("click",onClickZipSearch);
}
function onClickZipSearch(){
	var popW = 600;
    var popH = 500;
    
    parentRef.searchZip = true;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search Zip";
    devModelWindowWrapper.openPageWindow("../../html/masters/zipList.html", profileLbl, popW, popH, true, onCloseSearchZipAction);
}
var zipSelItem = null;
var cityId = "";
function onCloseSearchZipAction(evt,returnData){
	console.log(returnData);
	if(returnData && returnData.status == "success"){
		//console.log();
		$("#cmbZip").val("");
		$("#txtZip4").val("");
		$("#txtState").val("");
		$("#txtCountry").val("");
		$("#txtCity").val("");
		var selItem = returnData.selItem;
		if(selItem){
			zipSelItem = selItem;
			if(zipSelItem){
				cityId = zipSelItem.idk;
				if(zipSelItem.zip){
					$("#cmbZip").val(zipSelItem.zip);
				}
				if(zipSelItem.zipFour){
					$("#txtZip4").val(zipSelItem.zipFour);
				}
				if(zipSelItem.state){
					$("#txtState").val(zipSelItem.state);
				}
				if(zipSelItem.country){
					$("#txtCountry").val(zipSelItem.country);
				}
				if(zipSelItem.city){
					$("#txtCity").val(zipSelItem.city);
				}
			}
		}
	}
}
function getZip(){
	getAjaxObject(ipAddress+"/city/list/","GET",getZipList,onError);
}
function getZipList(dataObj){
	//var dArray = getTableListArray(dataObj);
	var dArray = [];
	if(dataObj && dataObj.response && dataObj.response.cityext){
		if($.isArray(dataObj.response.cityext)){
			dArray = dataObj.response.cityext;
		}else{
			dArray.push(dataObj.response.cityext);
		}
	}
	if(dArray && dArray.length>0){
		//setDataForSelection(dArray, "cmbZip", onZipChange, ["zip", "id"], 0, "");
		onZipChange();
	}
	getPrefix();
}
function getPrefix(){
	getAjaxObject(ipAddress+"/Prefix/list/","GET",getCodeTableValueList,onError);
}
function onComboChange(cmbId){
	var cmb = $("#"+cmbId).data("kendoComboBox");
	if(cmb && cmb.selectedIndex<0){
		cmb.select(0);
	}
}

function onPrefixChange(){
	onComboChange("cmbPrefix");
}
function onPtChange(){
	onComboChange("cmbStatus");
} 
function onGenderChange(){
	onComboChange("cmbGender");
}
function onSMSChange(){
	onComboChange("cmbSMS");
}
function onLanChange(){
	onComboChange("cmbLan");
}
function onRaceChange(){
	onComboChange("cmbRace");
}
function onEthnicityChange(){
	onComboChange("cmbEthicity");
}

function onZipChange(){
	onComboChange("cmbZip");
	var cmbZip = $("#cmbZip").data("kendoComboBox");
	if(cmbZip){
		var dataItem = cmbZip.dataItem();
		if(dataItem){
			$("#txtZip4").val(dataItem.zipFour);
			$("#txtCountry").val(dataItem.country);
			$("#txtState").val(dataItem.state);
			$("#txtCity").val(dataItem.city);
		}
	}
}
function getCodeTableValueList(dataObj){
	var dArray = getTableListArray(dataObj);
	if(dArray && dArray.length>0){
		setDataForSelection(dArray, "cmbPrefix", onPrefixChange, ["value", "idk"], 0, "");
	}
	getAjaxObject(ipAddress+"/Patient_Status/list/","GET",getPatientStatusValueList,onError);
}

function getPatientStatusValueList(dataObj){
	var dArray = getTableListArray(dataObj);
	if(dArray && dArray.length>0){
		setDataForSelection(dArray, "cmbStatus", onPtChange, ["desc", "idk"], 0, "");
	}
	getAjaxObject(ipAddress+"/Gender/list/","GET",getGenderValueList,onError);
}
function getGenderValueList(dataObj){
	var dArray = getTableListArray(dataObj);
	if(dArray && dArray.length>0){
		setDataForSelection(dArray, "cmbGender", onGenderChange, ["desc", "idk"], 0, "");
	}
	getAjaxObject(ipAddress+"/SMS/list/","GET",getSMSValueList,onError);
}
function getSMSValueList(dataObj){
	var dArray = getTableListArray(dataObj);
	if(dArray && dArray.length>0){
		setDataForSelection(dArray, "cmbSMS", onSMSChange, ["desc", "idk"], 0, "");
	}
	getAjaxObject(ipAddress+"/Language/list/","GET",getLanguageValueList,onError);
}
function getLanguageValueList(dataObj){
	var dArray = getTableListArray(dataObj);
	if(dArray && dArray.length>0){
		setDataForSelection(dArray, "cmbLan", onLanChange, ["desc", "idk"], 0, "");
	}
	getAjaxObject(ipAddress+"/Race/list/","GET",getRaceValueList,onError);
}
function getRaceValueList(dataObj){
	var dArray = getTableListArray(dataObj);
	if(dArray && dArray.length>0){
		setDataForSelection(dArray, "cmbRace", onRaceChange, ["desc", "idk"], 0, "");
	}
	getAjaxObject(ipAddress+"/Ethnicity/list/","GET",getEthnicityValueList,onError);
}
function getEthnicityValueList(dataObj){
	var dArray = getTableListArray(dataObj);
	if(dArray && dArray.length>0){
		setDataForSelection(dArray, "cmbEthicity", onEthnicityChange, ["desc", "idk"], 0, "");
	}
	
	getAjaxObject(ipAddress+"/facility/list","GET",getFacilityList,onError);
}
function getFacilityList(dataObj){
	console.log(dataObj);
	var dataArray = [];
	if(dataObj){
		if($.isArray(dataObj.response.facility)){
			dataArray = dataObj.response.facility;
		}else{
			dataArray.push(dataObj.response.facility);
		}
	}
	var tempDataArry = [];
	for(var i=0;i<dataArray.length;i++){
		dataArray[i].idk = dataArray[i].id; 
		dataArray[i].Status = "InActive";
		if(dataArray[i].isActive == 1){
			dataArray[i].Status = "Active";
		}
	}
	
	setDataForSelection(dataArray, "txtFAN", onFacilityChange, ["name", "idk"], 0, "");
	onFacilityChange();
	if(operation == UPDATE && patientId != ""){
		getPatientInfo();
	}
}
function onFacilityChange(){
	var txtFAN = $("#txtFAN").data("kendoComboBox");
	if(txtFAN){
		var txtFId = txtFAN.value();
		getAjaxObject(ipAddress+"/facility/"+txtFId,"GET",onGetFacilityInfo,onError);
	}
}
var billActNo = "";
function onGetFacilityInfo(dataObj){
	billActNo = "";
	if(dataObj && dataObj.response && dataObj.response.facility){
		$("#txtBAN").val(dataObj.response.facility.name);
		billActNo = dataObj.response.facility.billingAccountId;
	}
}
function getPatientInfo(){
	getAjaxObject(ipAddress+"/provider/"+patientId,"GET",onGetPatientInfo,onError);
}
function getComboListIndex(cmbId,attr,attrVal){
	var cmb = $("#"+cmbId).data("kendoComboBox");
	if(cmb){
		var ds = cmb.dataSource;
		var totalRec = ds.total();
		for(var i=0;i<totalRec;i++){
			var dtItem = ds.at(i);
			if(dtItem && dtItem[attr] == attrVal){
				cmb.select(i);
				return i;
			}
		}
	}
	return -1;
}
function getComboListIndex(cmbId,attr,attrVal){
	var cmb = $("#"+cmbId).data("kendoComboBox");
	if(cmb){
		var ds = cmb.dataSource;
		var totalRec = ds.total();
		for(var i=0;i<totalRec;i++){
			var dtItem = ds.at(i);
			if(dtItem && dtItem[attr] == attrVal){
				cmb.select(i);
				return i;
			}
		}
	}
	return -1;
}
function onGetPatientInfo(dataObj){
	patientInfoObject = dataObj;
	if(dataObj && dataObj.response && dataObj.response.provider){
		$("#txtID").val(dataObj.response.provider.id);
		$("#txtExtID1").val(dataObj.response.provider.externalId1);
		$("#txtExtID2").val(dataObj.response.provider.externalId2);
		$("#txtAbbreviation").val(dataObj.response.provider.abbreviation);
		
		getComboListIndex("cmbPrefix","value",dataObj.response.provider.prefix);
		$("#txtFN").val(dataObj.response.provider.firstName);
		$("#txtLN").val(dataObj.response.provider.lastName);
		$("#txtMN").val(dataObj.response.provider.middleName);
		$("#txtWeight").val(dataObj.response.provider.weight);
		$("#txtHeight").val(dataObj.response.provider.height);
		$("#txtNN").val(dataObj.response.provider.nickname);
		$("#txtFC").val(dataObj.response.provider.financeCharges);
		$("#txtAS").val(dataObj.response.provider.amaSpeciality);
		$("#txtNEIC").val(dataObj.response.provider.neicSpeciality);
		$("#txtNPI").val(dataObj.response.provider.npi);
		$("#txtNT").val(dataObj.response.provider.nuucType);
		$("#txtTI").val(dataObj.response.provider.taxonomyId);
		$("#txtUPIN").val(dataObj.response.provider.upin);
		
		getComboListIndex("txtType","Key",dataObj.response.provider.type);
		
		var strFId = dataObj.response.provider.facilityId;
		//$("#txtType").val(dataObj.response.provider.type);
		
		getComboListIndex("txtFAN","idx",dataObj.response.provider.strFId);
		//setDataForSelection(dataArray, "txtFAN", onFacilityChange, ["name", "idk"], 0, "");
		onFacilityChange();
	}
	if(dataObj && dataObj.response && dataObj.response.communications){
		var commObj =  dataObj.response.communications[0];
		if(commObj){
			cityId = commObj.cityId;
			$("#txtAdd1").val(commObj.address1);
			$("#txtAdd2").val(commObj.address2);
			$("#txtCity").val(commObj.city);
			$("#txtCountry").val(commObj.country);
			$("#txtHPhone").val(commObj.homePhone);
			$("#txtExtension").val(commObj.homePhoneExt);
			$("#txtWPhone").val(commObj.workPhone);
			$("#txtExtension1").val(commObj.workPhoneExt);
			$("#txtState").val(commObj.state);
			$("#cmbZip").val(commObj.zip);
			$("#txtZip4").val(commObj.zipFour);
		}
	}
}
function getTableListArray(dataObj){
	var dataArray = [];
	if(dataObj && dataObj.response && dataObj.response.codeTable){
		if($.isArray(dataObj.response.codeTable)){
			dataArray = dataObj.response.codeTable;
		}else{
			dataArray.push(dataObj.response.codeTable);
		}
	}
	var tempDataArry = [];
	var obj = {};
	obj.desc = "";
	obj.zip = "";
	obj.value = "";
	obj.idk = "";
	tempDataArry.push(obj);
	for(var i=0;i<dataArray.length;i++){
		if(dataArray[i].isActive == 1){
			var obj = dataArray[i];
			obj.idk = dataArray[i].id;
			obj.status = dataArray[i].Status;
			tempDataArry.push(obj);
		}
	}
	return tempDataArry;
}
function onClickReset(){
	operation = ADD;
	patientId = "";
	$("#txtID").val("");
	$("#txtExtID1").val("");
	$("#txtExtID2").val("");
	setComboReset("cmbPrefix");
	$("#txtFN").val("");
	$("#txtLN").val("");
	$("#txtMN").val("");
	$("#txtWeight").val("");
	$("#txtHeight").val("");
	$("#txtNN").val("");
	$("#txtAbbreviation").val("");
	$("#txtType").val("");
	$("#txtBAN").val("");
	$("#txtFAN").val("");
	$("#txtNEIC").val("");
	$("#txtAS").val("");
	$("#txtNT").val("");
	$("#txtUPIN").val("");
	$("#txtDEA").val("");
	$("#txtNPI").val("");
	$("#txtTI").val("");
	$("#txtPCP").val("");
	$("#txtBD").val("");
	$("#txtFC").val("");
	//$("#txtZip4").val("");
	

		/*var dtDOB = $("#dtDOB").data("kendoDatePicker");
		if(dtDOB){
			dtDOB.value("");
		}*/
	$("#txtSSN").val("");
	setComboReset("cmbStatus");
	setComboReset("cmbGender");
	setComboReset("cmbEthicity");
	setComboReset("cmbRace");
	setComboReset("cmbLan");
		$("#txtAdd1").val("");
		$("#txtAdd2").val("");
		setComboReset("cmbSMS");
		$("#txtCell").val("");
		$("#txtExtension1").val("");
		$("#txtWPhone").val("");
		$("#txtExtension").val("");
		$("#txtHPhone").val("");
		$("#txtEmail1").val("");
		
		$("#rdHome").prop("checked",true);

}

function setComboReset(cmbId){
	var cmb = $("#"+cmbId).data("kendoComboBox");
	if(cmb){
		cmb.select(0);
	}
}

function validation(){
var flag = true;		
var txtFC = $("#txtFC").val();
txtFC = $.trim(txtFC);
if(txtFC == ""){
	customAlert.error("Error","Enter Finance charges");
	flag = false;
	return false;
}
if(cityId == ""){
	customAlert.error("Error","Select City ");
	flag = false;
	return false;
}
/*var strAbbr = $("#txtAbbrevation").val();
	strAbbr = $.trim(strAbbr);
	if(strAbbr == ""){
		customAlert.error("Error","Enter Abbrevation");
		flag = false;
		return false;
	}
	var strCode = $("#txtCode").val();
	strCode = $.trim(strCode);
	if(strCode == ""){
		customAlert.error("Error","Enter Code");
		flag = false;
		return false;
	}
	var strName = $("#txtName").val();
	strName = $.trim(strName);
	if(strName == ""){
		customAlert.error("Error","Enter Country");
		flag = false;
		return false;
	}*/
	
	return flag;
}

function getComboDataItem(cmbId){
	var dItem = null;
	var cmb = $("#"+cmbId).data("kendoComboBox");
	if(cmb && cmb.dataItem()){
		dItem =  cmb.dataItem();
		return cmb.text();
	}
	return "";
}

function getComboDataItemValue(cmbId){
	var dItem = null;
	var cmb = $("#"+cmbId).data("kendoComboBox");
	if(cmb && cmb.dataItem()){
		dItem =  cmb.dataItem();
		return cmb.value();
	}
	return "";
}

function onClickSave(){
	var sEmail = $('#txtEmail').val();
	if(validation()){
		var strId = $("#txtID").val();
		var strExtId1 = $("#txtExtID1").val();
		var strExtId2 = $("#txtExtID2").val();
		var strPrefix = getComboDataItem("cmbPrefix");
		var strNickName = $("#txtNN").val();
		var strStatus = getComboDataItem("cmbStatus");
		var strFN = $("#txtFN").val();
		var strMN = $("#txtMN").val();
		var strLN = $("#txtLN").val();
		
		var dtItem = $("#dtDOB").data("kendoDatePicker");
		var strDate = "";
		if(dtItem && dtItem.value()){
			strDate = kendo.toString(dtItem.value(),"yyyy-MM-dd");
		}
		
		var strSSN = $("#txtSSN").val();
		var strGender = getComboDataItem("cmbGender");
		var strAdd1 = $("#txtAdd1").val();
		var strAdd2 = $("#txtAdd2").val();
		var strCity = $("#txtCity").val();
		var strState = $("#txtState").val();
		var strCountry = $("#txtCountry").val();
		
		var strZip = getComboDataItemValue("cmbZip");
		var strZip4 = $("#txtZip4").val();
		var strHPhone = $("#txtHPhone").val();
		var strExt = $("#txtExtension").val();
		var strWp = $("#txtWP").val();
		var strWpExt = $("#txtWPExt").val();
		var strCell = $("#txtCell").val();
		
		var strSMS = getComboDataItem("cmbSMS");
		
		var strEmail = $("#txtEmail").val();
		var strLan = getComboDataItem("cmbLan");
		var strRace = getComboDataItem("cmbRace");
		var strEthinicity = getComboDataItem("cmbEthicity");
		
		var dataObj = {};
		dataObj.provider = {};
		dataObj.provider.createdBy = 101;
		dataObj.provider.externalId1 = strExtId1;
		dataObj.provider.externalId2 = strExtId2;
		dataObj.provider.abbreviation = $("#txtAbbreviation").val();
		var txtType = $("#txtType").data("kendoComboBox");
		var strTType = "";
		if(txtType){
			strTType = Number(txtType.value());
		}
		dataObj.provider.type = strTType;//$("#txtType").val();;
		dataObj.provider.prefix = strPrefix;//$("#txtEmail").val();;
		dataObj.provider.firstName = strFN;
		dataObj.provider.middleName = strMN;
		dataObj.provider.lastName = strLN;
		dataObj.provider.nickname = strNickName;
		dataObj.provider.billingAccountId = Number(billActNo);
		var txtFAN = $("#txtFAN").data("kendoComboBox");
		var fId = "";
		if(txtFAN){
			fId = txtFAN.value();
		}
		dataObj.provider.facilityId = Number(fId);
		dataObj.provider.neicSpeciality = $("#txtNEIC").val();
		dataObj.provider.amaSpeciality = $("#txtAS").val();
		dataObj.provider.nuucType = $("#txtNT").val();;
		dataObj.provider.upin = $("#txtUPIN").val();;
		dataObj.provider.dea = $("#txtDEA").val();;
		dataObj.provider.npi = $("#txtNPI").val();
		dataObj.provider.taxonomyId = $("#txtTI").val();
		dataObj.provider.pcp = $("#txtPCP").val();
		dataObj.provider.billableDoctor = $("#txtBD").val();
		dataObj.provider.financeCharges = $("#txtFC").val();
		dataObj.provider.isActive = 1;
		dataObj.provider.userId = 101;
		
		var comm = [];
		var comObj = {};
		comObj.country = $("#txtCountry").val();//"101";
		comObj.city = $("#txtCity").val();;
		comObj.cityId = cityId;
		comObj.isActive = 1;
		comObj.countryId = cityId;
		comObj.isDeleted = 0;
		comObj.zipFour = $("#txtZip4").val();//"2323";
		comObj.sms = "yes";
		comObj.state = $("#txtState").val();
		comObj.workPhoneExt = $("#txtWExtension").val();
		comObj.email = $("#txtEmail").val();
		comObj.zip = strZip;
		comObj.parentTypeId = 500;
		comObj.address2 = $("#txtAdd2").val();
		comObj.address1 = $("#txtAdd1").val();
		comObj.homePhone = $("#txtHPhone").val();
		comObj.stateId = 1;
		comObj.areaCode = "232";
		comObj.homePhoneExt = $("#txtExtension").val();
		comObj.workPhone = $("#txtWPhone").val();
		comObj.cellPhone = $("#txtCell").val();
		
		comObj.defaultCommunication = 1;
		if(operation == UPDATE){
		}
		
		dataObj.communications = comm;
		comm.push(comObj);
				if(operation == ADD){
				var dataUrl = ipAddress+"/provider/create";
				createAjaxObject(dataUrl,dataObj,"POST",onCreate,onError);
			}else if(operation == UPDATE){
				dataObj.provider.modifiedBy = "101";
				dataObj.provider.id = patientId;
				dataObj.provider.isDeleted = "0";
				var dataUrl = ipAddress+"/provider/update";
				createAjaxObject(dataUrl,dataObj,"POST",onCreate,onError);
			}
	}
}

function onCreate(dataObj){
	console.log(dataObj);
	if(dataObj && dataObj.response && dataObj.response.status){
		if(dataObj.response.status.code == "1"){
			var obj = {};
			obj.status = "success";
			obj.operation = operation;
			popupClose(obj);
			
		}else{
			customAlert.error("error", dataObj.response.status.message);
		}
	}
	/*var obj = {};
	obj.status = "success";
	obj.operation = operation;
	popupClose(obj);*/
}

function onError(errObj){
	console.log(errObj);
	customAlert.error("Error","Error");
}
function onClickCancel(){
	var obj = {};
	obj.status = "false";
	popupClose(obj);
}
function onClickSearch(){
	var obj = {};
	obj.status = "search";
	popupClose(obj);
}
function popupClose(obj){
	var windowWrapper = new kendoWindowWrapper();
	windowWrapper.closePageWindow(obj);
}



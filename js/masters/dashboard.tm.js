var todaysApptDetails = [];
var apptMaster = [];
$(document).ready(function () {
    // Knob CHart

    // var start = moment().subtract(29, 'days');
    // var start = moment(0, "YYYYMMDD");
    var start = moment();
    var end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
        var sDate = new Date(start);
        sDate.setHours(0);
        sDate.setMinutes(0);
        sDate.setSeconds(0);
        sDate.setMilliseconds(0);
        var eDate = new Date(end);
        eDate.setHours(23);
        eDate.setMinutes(59);
        eDate.setSeconds(59);
        eDate.setMilliseconds(0);
        default1(sDate.getTime(),eDate.getTime());
        redflagDetails(sDate.getTime(),eDate.getTime());
    }


    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);
    cb(start, end);




    $('.knob').each(function () {

        var elm = $(this);
        var perc = elm.attr("value");

        elm.knob();

        $({
            value: 0
        }).animate({
            value: perc
        }, {
            duration: 1000,
            easing: 'swing',
            progress: function () {
                elm.val(Math.ceil(this.value)).trigger('change')
                // alert(this.value);
            }
        });
    });
    // var now = new Date();
    // now.setDate(1);
    // now.setHours(0);
    // now.setMinutes(0);
    // now.setSeconds(0);
    // now.setMilliseconds(0);
    // var past = now.setMonth(now.getMonth() - 1, 1);
    //
    // var lastdayoflastmonth = new Date();
    // lastdayoflastmonth.setMonth(lastdayoflastmonth.getMonth(), 1);
    // lastdayoflastmonth.setDate(0);
    // lastdayoflastmonth.setHours(23);
    // lastdayoflastmonth.setMinutes(59);
    // lastdayoflastmonth.setSeconds(59);
    // lastdayoflastmonth.setMilliseconds(0);
    // alert(lastdayoflastmonth.getTime());


    $("#btnPrint").off("click", onClickPrint);
    $("#btnPrint").on("click", onClickPrint);


    getTodaysApptDetails();

});

function getTodaysApptDetails(){

    var dt = new Date();
    var day = dt.getDate();
    var month = dt.getMonth();
    month = month + 1;
    var year = dt.getFullYear();

    var stDate = month + "/" + day + "/" + year;
    stDate = stDate + " 00:00:00";

    var startDate = new Date(stDate);
    var stDateTime = startDate.getTime();

    var etDate = month + "/" + day + "/" + year;
    etDate = etDate + " 23:59:59";

    var endtDate = new Date(etDate);
    var edDateTime = endtDate.getTime();

    var apiURL = ipAddress + "/appointment/list/?from-date=" + stDateTime + "&to-date=" + edDateTime + "&is-active=1";

    getAjaxObject(apiURL,"GET",onGetTodaysApptDetails,onError);

}

var formatted_date = null;
function onGetTodaysApptDetails(dataObj){
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.appointment) {
            if ($.isArray(dataObj.response.appointment)) {
                dataArray = dataObj.response.appointment;
            } else {
                dataArray.push(dataObj.response.appointment);
            }
        }
    }

    var months = ["JAN", "FEB", "MAR","APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
    var current_datetime = new Date();
    formatted_date = kendo.toString(current_datetime, "dd/MM/yyyy hh:mm:ss tt");

    if(dataArray !== null && dataArray.length > 0){

        apptMaster = dataArray;
        todaysApptDetails = apptMaster;

        $("#spnAppointmentCount").text(dataArray.length);
        $("#spnTodayDate").text(formatted_date);
    }
    else{
        $("#spnAppointmentCount").text("0");
        $("#spnTodayDate").text(formatted_date);
    }
}

function onClickPrint(){
    if ($.trim($('#redFlagDetails').html()) !== "") {



        var contents = document.getElementById("redFlagDetails").outerHTML;// $("#divTaskReportsViewer").html();
        var frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({ "position": "absolute", "top": "-1000000px" });
        $("body").append(frame1);
        var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();

        //Create a new HTML document.
        frameDoc.document.write('<html><title>Red Flag Details</title><head>');
        frameDoc.document.write('<link href="../../css/Theme01.css" rel="stylesheet" type="text/css" />');
        frameDoc.document.write('<link href="../../css/report/invoicestyle.css" rel="stylesheet" type="text/css" />');

        frameDoc.document.write('</head><body style="background-color: #fff;">');



        frameDoc.document.write('<div class="timesheetinvoiceBlock">');

        //Append the DIV contents.
        frameDoc.document.write(contents);

        frameDoc.document.write('</div>');
        //frameDoc.document.write('</div> </div> </div>');
        frameDoc.document.write('</body></html>');


        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();

        }, 500);
    }
    else {
        customAlert.info("info","No data to print.");
    }
}


function default1(a,b){
    // var fromDate = "1451606400000";
    //
    // var tillDate = new Date();

    getAjaxObject(ipAddress+"/homecare/dashboard/?from-date="+a+"&to-date="+b,"GET",getDetails,onError);

    secondBlockdetails();

}

function getDetails(dataObj) {
    var serviceUserDetails = [];
    var staffDetails = [];
    var SUfemaleCount = 0, SUmaleCount = 0, staffMaleCount = 0, staffFemaleCount = 0;
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        // if (dataObj.response.dashboard) {
        //     if(dataObj.response.dashboard.serviceUserDetails) {
        //         if(dataObj.response.dashboard.serviceUserDetails[0].gender.toLowerCase()=="male") {
        //             SUmaleCount = dataObj.response.dashboard.serviceUserDetails[0].count;
        //         }
        //         if(dataObj.response.dashboard.serviceUserDetails[1] && dataObj.response.dashboard.serviceUserDetails[1].gender.toLowerCase()=="female") {
        //             SUfemaleCount = dataObj.response.dashboard.serviceUserDetails[1].count;
        //         }
        //     }
        //     if(dataObj.response.dashboard.staffDetails) {
        //         if(dataObj.response.dashboard.staffDetails[0].gender.toLowerCase()=="male") {
        //             staffMaleCount = dataObj.response.dashboard.staffDetails[0].count;
        //         }
        //         if(dataObj.response.dashboard.staffDetails[1] && dataObj.response.dashboard.staffDetails[1].gender.toLowerCase()=="female") {
        //             staffFemaleCount = dataObj.response.dashboard.staffDetails[1].count;
        //         }
        //     }
        //
        //     $("#spStaffTotal").text(staffFemaleCount + staffMaleCount);
        //     $("#smstaffMF").text("Male/ Female (" + staffMaleCount + "/" + staffFemaleCount + ")");
        //     $("#spSUTotal").text(SUfemaleCount + SUmaleCount);
        //     $("#smMF").text("Male/ Female (" + SUmaleCount + "/" + SUfemaleCount + ")");
        // }
        if (dataObj.response.dashboard) {
            if (dataObj.response.dashboard.serviceUserDetails) {
                serviceUserDetails = dataObj.response.dashboard.serviceUserDetails;
            }
            if (dataObj.response.dashboard.staffDetails) {
                staffDetails = dataObj.response.dashboard.staffDetails;
            }
        }

        for (var i = 0; i < serviceUserDetails.length; i++) {
            if (serviceUserDetails[i].gender && serviceUserDetails[i].gender != null && serviceUserDetails[i].gender != "" && serviceUserDetails[i].gender.toLowerCase() == "male") {
                SUmaleCount = serviceUserDetails[i].count;
            }
            if (serviceUserDetails[i].gender && serviceUserDetails[i].gender != null && serviceUserDetails[i].gender != "" && serviceUserDetails[i].gender.toLowerCase() == "female") {
                SUfemaleCount = serviceUserDetails[i].count;
            }
        }

        for (var i = 0; i < staffDetails.length; i++) {
            if (staffDetails[i].gender && staffDetails[i].gender != null && staffDetails[i].gender != "" && staffDetails[i].gender.toLowerCase() == "male") {
                staffMaleCount = staffMaleCount + staffDetails[i].count;
            }
            if (staffDetails[i].gender && staffDetails[i].gender != null && staffDetails[i].gender != "" && staffDetails[i].gender.toLowerCase() == "female") {
                staffFemaleCount = staffFemaleCount + staffDetails[i].count;
            }
        }

        $("#spStaffTotal").text(staffFemaleCount + staffMaleCount);
        $("#smstaffMF").text("Male/ Female (" + staffMaleCount + "/" + staffFemaleCount + ")");
        $("#spSUTotal").text(SUfemaleCount + SUmaleCount);
        $("#smMF").text("Male/ Female (" + SUmaleCount + "/" + SUfemaleCount + ")");
    }
}
function onError(errorObj){
    console.log(errorObj);
}

function secondBlockdetails(){
    var now = new Date();
    now.setHours(0);
    now.setMinutes(0);
    now.setSeconds(0);
    now.setMilliseconds(0);

    var lastdayoflastmonth = new Date();
    lastdayoflastmonth.setHours(23);
    lastdayoflastmonth.setMinutes(59);
    lastdayoflastmonth.setSeconds(59);
    lastdayoflastmonth.setMilliseconds(0);

    var fromDate = "1451606400000";

    var tillDate = new Date();


    getAjaxObject(ipAddress+"/homecare/dashboard/?from-date="+fromDate+"&to-date="+tillDate.getTime(),"GET",getSecondDetails,onError);
}

function getSecondDetails(dataObj) {
    var SUfemaleCount=0, SUmaleCount =0;
    var SFCareMaleCount =0, SFCareFemaleCount = 0, SFManaMaleCount =0, SFManaFemaleCount = 0, SFBackMaleCount =0, SFBackFemaleCount = 0;
    var serviceUserDetails = [];
    var staffDetails = [];
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.dashboard) {
            if (dataObj.response.dashboard.serviceUserDetails) {
                serviceUserDetails = dataObj.response.dashboard.serviceUserDetails;
            }
            if (dataObj.response.dashboard.staffDetails) {
                staffDetails = dataObj.response.dashboard.staffDetails;
            }
        }

        for (var i = 0; i < serviceUserDetails.length; i++) {
            if (serviceUserDetails[i].gender && serviceUserDetails[i].gender != null && serviceUserDetails[i].gender != "" && serviceUserDetails[i].gender.toLowerCase() == "male") {
                SUmaleCount = serviceUserDetails[i].count;
            }
            if (serviceUserDetails[i].gender && serviceUserDetails[i].gender != null && serviceUserDetails[i].gender != "" && serviceUserDetails[i].gender.toLowerCase() == "female") {
                SUfemaleCount = serviceUserDetails[i].count;
            }
        }

        for (var i = 0; i < staffDetails.length; i++) {
            if (staffDetails[i].gender && staffDetails[i].gender != null && staffDetails[i].gender != "" && staffDetails[i].gender.toLowerCase() == "male") {
                if (staffDetails[i].value.toLowerCase().includes("care")) {
                    SFCareMaleCount = staffDetails[i].count;
                }
                if (staffDetails[i].value.toLowerCase().includes("doctor")) {
                    SFManaMaleCount = staffDetails[i].count;
                }
                if (staffDetails[i].value.toLowerCase().includes("nurse")) {
                    SFBackMaleCount = staffDetails[i].count;
                }

            }
            if (staffDetails[i].gender && staffDetails[i].gender != null && staffDetails[i].gender != "" && staffDetails[i].gender.toLowerCase() == "female") {
                if (staffDetails[i].value.toLowerCase().includes("care")) {
                    SFCareFemaleCount = staffDetails[i].count;
                }
                if (staffDetails[i].value.toLowerCase().includes("doctor")) {
                    SFManaFemaleCount = staffDetails[i].count;
                }
                if (staffDetails[i].value.toLowerCase().includes("nurse")) {
                    SFBackFemaleCount = staffDetails[i].count;
                }
            }
        }
        localStorage.setItem("SUfemaleCount",SUfemaleCount);
        localStorage.setItem("SUmaleCount",SUmaleCount);
        $("#secondSUMale").text(SUmaleCount);SUfemaleCount
        $("#secondSUFemaleMale").text(SUfemaleCount);
        var date = new Date();
        var curDate = GetDateTimeEditDay(date);
        // $("#secondstaffDate").text("As M " + curDate);
        $("#secondstaffDate").text("From till Date");

        $("#thirMM").text(SFManaMaleCount);
        $("#thirMF").text(SFManaFemaleCount);
        $("#thirMT").text(SFManaFemaleCount + SFManaMaleCount);

        $("#thirCM").text(SFCareMaleCount);
        $("#thirCF").text(SFCareFemaleCount);
        $("#thirCT").text(SFCareFemaleCount + SFCareMaleCount);

        $("#thirBM").text(SFBackMaleCount);
        $("#thirBF").text(SFBackFemaleCount);
        $("#thirBT").text(SFBackMaleCount + SFBackFemaleCount);

        $("#thirTM").text(SFManaMaleCount + SFCareMaleCount + SFBackMaleCount);
        $("#thirTF").text(SFManaFemaleCount + SFCareFemaleCount + SFBackFemaleCount);
        $("#thirTT").text(SFManaMaleCount + SFCareMaleCount + SFBackMaleCount + SFManaFemaleCount + SFCareFemaleCount + SFBackFemaleCount);


    }
}

function closePopup(id) {
    $('#' + id).hide();
}

function openPopup(id) {
    if(redFlagDetails.length > 0) {
        $('#redflagPopup').show();
    }
    else{
        // customAlert.info("Info","No record(s) found.");
        alert("No record(s) found.");
    }

}

function openApptPopup(id) {
    todaysApptDetails = apptMaster;
    if(todaysApptDetails && todaysApptDetails.length > 0) {
        $('#todayApptsPopup').show();
        openTodaysApptsPopUp();
    }
    else{
        // customAlert.info("Info","No record(s) found.");
        alert("No record(s) found.");
    }

}

function openTodaysApptsPopUp(){
    if(todaysApptDetails && todaysApptDetails.length > 0){

        var lookup = {};
        var patientIds = [];

        for (var item, c = 0; item = todaysApptDetails[c++];) {
            if (item && item.patientId) {

                var pid = item.patientId;

                if (!(pid in lookup)) {
                    lookup[pid] = 1;
                    patientIds.push(pid);
                }
            }
        }

        var lookup = {};
        var providerIds = [];

        for (var item, c = 0; item = todaysApptDetails[c++];) {
            if (item && item.providerId) {

                var staffid = item.providerId;

                if (!(staffid in lookup)) {
                    lookup[staffid] = 1;
                    providerIds.push(staffid);
                }
            }
        }


        var apptsByType = [];

        $("#todayApptsPopup").show();

        var completedAppts = "";
        completedAppts = todaysApptDetails.filter(function(e){
            return e.composition && e.composition.appointmentType && e.composition.appointmentType.desc && e.composition.appointmentType.desc.toLowerCase() === "finished";
        });

        // completedAppts.push(Appts);

        var obj = {};

        if(completedAppts !== null && completedAppts.length > 0){

            $("#lblCompletedAppointments").text(completedAppts.length);

            obj.appointmentType = "Completed Appointments";
            obj.appointments = completedAppts;

            apptsByType.push(obj);

            for (var i = 0; i < completedAppts.length > 0; i++) {

                if (todaysApptDetails && todaysApptDetails.length > 0) {
                    var apptindex = todaysApptDetails.findIndex((a) => a.id === completedAppts[i].id);
                    if (apptindex > -1) {
                        todaysApptDetails.splice(apptindex, 1);
                    }
                }
            }
        }

        if (todaysApptDetails && todaysApptDetails.length > 0) {
            var lateAppts = todaysApptDetails.filter(function(e){

                if(e.inTime && e.dateOfAppointment){
                    var inTime = new Date(e.dateOfAppointment);
                    inTime.setHours(0,0,0,0);

                    var inttime = inTime.getTime() + (e.inTime * 60000);

                    if(inttime > e.dateOfAppointment){
                        return e;
                    }
                    else{
                        return null;
                    }
                }
            });

            if(lateAppts !== null && lateAppts.length > 0){

                $("#lblLateAppointments").text(lateAppts.length);

                obj = {};
                obj.appointmentType = "Late Appointments";
                obj.appointments = lateAppts;

                apptsByType.push(obj);

                for (var j = 0; j < lateAppts.length > 0; j++) {

                    if (todaysApptDetails && todaysApptDetails.length > 0) {
                        var apptindex = todaysApptDetails.findIndex((a) => a.id === lateAppts[j].id);
                        if (apptindex > -1) {
                            todaysApptDetails.splice(apptindex, 1);
                        }
                    }
                }
            }
        }

        if (todaysApptDetails && todaysApptDetails.length > 0) {

            var missedAppts = todaysApptDetails.filter(function(e){
                return e.composition && e.composition.appointmentType && e.composition.appointmentType.desc && e.composition.appointmentType.desc.toLowerCase() === "cancel";
            });

            if(missedAppts !== null && missedAppts.length > 0){

                $("#lblMissedAppointments").text(missedAppts.length);

                obj = {};
                obj.appointmentType = "Missed Appointments";
                obj.appointments = missedAppts;

                apptsByType.push(obj);

                for (var k = 0; k < missedAppts.length > 0; k++) {

                    if (todaysApptDetails && todaysApptDetails.length > 0) {
                        var apptindex = todaysApptDetails.findIndex((a) => a.id === missedAppts[k].id);
                        if (apptindex > -1) {
                            todaysApptDetails.splice(apptindex, 1);
                        }
                    }
                }
            }
        }


        $("#lblSUCount").text(patientIds.length);
        $("#lblStaffCount").text(providerIds.length);
        $("#divTodayApptDetails").empty();
        $("#todayDate").text(formatted_date);


        if (apptsByType !== null && apptsByType.length > 0) {

            for (var m = 0; m < apptsByType.length; m++) {

                var strHTML = '';

                strHTML = strHTML + '<div class="table-block">';
                strHTML = strHTML + '<div class="timeSheetReportdivHeading">';

                strHTML = strHTML + apptsByType[m].appointmentType;


                strHTML = strHTML + '</div></div>';
                strHTML = strHTML + '<div class="col-xs-12 taskReportTblBlock"><div class="table-responsive">';
                strHTML = strHTML + '<table class="tblTaskreport weeklyCareReportTbl"><thead><tr>';


                var cols = ["In Time", "Out Time", "Date of Appointment", "Service User","Staff"];

                for (var n = 0; n < cols.length; n++) {

                    strHTML = strHTML + '<th><span class="spnReportTitle">' + cols[n] + '</th>';
                }
                strHTML = strHTML + '</tr></thead>';
                strHTML = strHTML + '<tbody>';

                var columns = 4;
                var rows = apptsByType[m].appointments.length;


                for (var rownumber = 0; rownumber < rows; rownumber++) {

                    strHTML = strHTML + '<tr>';

                    var tempAppointment = apptsByType[m].appointments[rownumber];


                    if (tempAppointment.inTime) {
                        var inTime = kendo.toString(new Date(tempAppointment.inTime), "hh:mm tt");
                        strHTML = strHTML + '<td>'+inTime+'</td>';
                    }
                    else{
                        strHTML = strHTML + '<td></td>';
                    }


                    if (tempAppointment.outTime) {
                        var outTime = kendo.toString(new Date(tempAppointment.outTime), "hh:mm tt");
                        strHTML = strHTML + '<td>'+outTime+'</td>';
                    }
                    else{
                        strHTML = strHTML + '<td></td>';
                    }


                    if (tempAppointment.dateOfAppointment) {
                        var date = new Date(GetDateTimeEditDay(tempAppointment.dateOfAppointment));
                        var day = date.getDay();
                        var dow = getWeekDayName(day);
                        var apptDateTime = dow + " " + GetDateTimeEdit(tempAppointment.dateOfAppointment);

                        strHTML = strHTML + '<td>' + apptDateTime + '</td>';
                    }
                    else{
                        strHTML = strHTML + '<td></td>';
                    }

                    if (tempAppointment.composition && tempAppointment.composition.patient && tempAppointment.composition.patient.firstName && tempAppointment.composition.patient.lastName) {
                        var suName = tempAppointment.composition.patient.lastName + " " + tempAppointment.composition.patient.firstName + " " + tempAppointment.composition.patient.middleName;
                        strHTML = strHTML + '<td>' + suName + '</td>';
                    }
                    else{
                        strHTML = strHTML + '<td></td>';
                    }

                    if (tempAppointment.composition && tempAppointment.composition.provider && tempAppointment.composition.provider.firstName && tempAppointment.composition.provider.lastName) {
                        var suName = tempAppointment.composition.provider.lastName + " " + tempAppointment.composition.provider.firstName + " " + tempAppointment.composition.provider.middleName;
                        strHTML = strHTML + '<td>' + suName + '</td>';
                    }
                    else{
                        strHTML = strHTML + '<td></td>';
                    }


                    strHTML = strHTML + '</tr>';
                }
                strHTML = strHTML + '</tbody></table></div></div>';

                $("#divTodayApptDetails").append(strHTML);
            }
        }

    }
    else{
        alert("No record(s) found.");
    }
}

function redflagDetails(start,end){
    // getAjaxObject(ipAddress+"/homecare/reports/patient-appointment-activities/?date-of-appointment=:bt:1485267674000,1577197274000","GET",getredflagDetails,onError);
    var url = ipAddress+"/homecare/reports/patient-appointment-activities/?date-of-appointment=:bt:"+start+','+end;
    getAjaxObject(url,"GET",getredflagDetails,onError);
}
var redFlagDetails = [];
function getredflagDetails(dataObj) {
    $("#redFlagDetails").empty();
    $("#spRedFlagCount").empty();
    $("#spRedFlagCount").text(0);
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.patientAppointmentActivites) {
            if ($.isArray(dataObj.response.patientAppointmentActivites)) {
                dataArray = dataObj.response.patientAppointmentActivites;
            } else {
                dataArray.push(dataObj.response.patientAppointmentActivites);
            }
        }
        redFlagDetails = dataArray;

        if (dataArray !== null && dataArray.length > 0) {
            $("#spRedFlagCount").text(dataArray.length);
            var appointments = [];

            var lookup = {};
            var providerIds = [];

            for (var item, c = 0; item = dataArray[c++];) {
                if (item && item.providerId) {

                    var providerId = item.providerId;

                    if (!(providerId in lookup)) {
                        lookup[providerId] = 1;
                        providerIds.push(providerId);
                    }
                }
            }

            var providerAppointments = [];

            if (providerIds !== null && providerIds.length > 0) {

                for (var j = 0; j < providerIds.length; j++) {

                    var providerObj = {};


                    var tmpStaffAppts = $.grep(dataArray, function (e) {
                        return e.providerId === providerIds[j];
                    });

                    if (tmpStaffAppts !== null && tmpStaffAppts.length > 0) {

                        tmpStaffAppts.sort(sortByDateDec);

                        if (tmpStaffAppts[0] && tmpStaffAppts[0].providerId) {
                            providerObj.id = tmpStaffAppts[0].providerId;
                            providerObj.providerName = tmpStaffAppts[0].providerLastName +' '+tmpStaffAppts[0].providerFirstName;
                        }
                        providerObj.appointments = tmpStaffAppts;
                    }

                    providerAppointments.push(providerObj);
                }

                if (providerAppointments !== null && providerAppointments.length > 0) {
                    providerAppointments.sort(sortByReportStaff);
                    bindRedFlagDetails(providerAppointments);
                }
            }

        }
        // else {
        //     customAlert.info("Info", "No records found");
        // }

    }
}


function bindRedFlagDetails(providerAppointments){


    if (providerAppointments !== null && providerAppointments.length > 0) {

        for (var i = 0; i < providerAppointments.length; i++) {

            var strHTML = '';

            strHTML = strHTML + '<div class="table-block">';
            strHTML = strHTML + '<div class="timeSheetReportdivHeading">';

            strHTML = strHTML + '#'+ providerAppointments[i].id +' - '+providerAppointments[i].providerName;


            strHTML = strHTML + '</div></div>';
            strHTML = strHTML + '<div class="col-xs-12 taskReportTblBlock"><div class="table-responsive">';
            strHTML = strHTML + '<table class="tblTaskreport weeklyCareReportTbl"><thead><tr>';


            var cols=["Appt Date & Time","SU Name","Task Group","Component","Notes"];

            for (var j = 0; j < cols.length; j++) {

                strHTML = strHTML + '<th><span class="spnReportTitle">' + cols[j] + '</th>';
            }
            strHTML = strHTML + '</tr></thead>';
            strHTML = strHTML + '<tbody>';

            var columns = 4;
            var rows = providerAppointments[i].appointments.length;


            for (var rownumber = 0; rownumber < rows; rownumber++) {

                strHTML = strHTML + '<tr>';

                var tempAppointment = providerAppointments[i].appointments[rownumber];


                if (tempAppointment.dateOfAppointment) {
                    var date = new Date(GetDateTimeEditDay(tempAppointment.dateOfAppointment));
                    var day = date.getDay();
                    var dow = getWeekDayName(day);
                    var apptDateTime = dow + " " + GetDateTimeEdit(tempAppointment.dateOfAppointment);

                    strHTML = strHTML + '<td><a href="javascript:void(0)" onclick="onClickVisitReport('+providerAppointments[i].appointments[rownumber].id +')">' + apptDateTime + '</a></td>';

                }

                // if(tempAppointment.patientLastName && tempAppointment.patientFirstName && tempAppointment.patientMiddleName){
                var suName = tempAppointment.patientLastName + " " + tempAppointment.patientFirstName + " " + tempAppointment.patientMiddleName;
                strHTML = strHTML + '<td>' + suName + '</td>';
                // }

                if (tempAppointment.activityType) {
                    strHTML = strHTML + '<td>' + tempAppointment.activityType + '</td>';
                }

                if (tempAppointment.activity) {
                    strHTML = strHTML + '<td>' + tempAppointment.activity + '</td>';
                }

                if (tempAppointment.notes && tempAppointment.notes != null && tempAppointment.notes != "") {
                    strHTML = strHTML + '<td>' + tempAppointment.notes + '</td>';
                }

                strHTML = strHTML + '</tr>';
            }
            strHTML = strHTML + '</tbody></table></div></div>';

            $("#redFlagDetails").append(strHTML);
        }

    }
}

var sortByDateDec = function (x, y) {
    // This is a comparison function that will result in dates being sorted in
    // DESCENDING order.
    var date1 = new Date(x.dateOfAppointment);
    var date2 = new Date(y.dateOfAppointment);
    if (date1 > date2) return -1;
    if (date1 < date2) return 1;
    return 0;
};


function sortByReportStaff(a, b) {

    var providerNameA = a.providerName.toUpperCase();
    var providerNameB = b.providerName.toUpperCase();

    var comparison = 0;
    if (providerNameA > providerNameB) {
        comparison = 1;
    } else if (providerNameA < providerNameB) {
        comparison = -1;
    }
    return comparison;

}

function onClickVisitReport(idk) {
    setTimeout(function () {
        var popW = "80%";
        var popH = "70%";
        var profileLbl;
        var devModelWindowWrapper = new kendoWindowWrapper();
        profileLbl = "Visit Report";
        parentRef = parent.frames['iframe'].window;
        parentRef.idk = idk;
        devModelWindowWrapper.openPageWindow("../../html/reports/patientActivity.html", profileLbl, popW, popH, true, onCloseSearchZipAction);
    }, 100);
}
function onCloseSearchZipAction(evt,returnData){

}
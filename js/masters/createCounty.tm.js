var parentRef = null;
var operation = "";
var selItem = null;
var ADD = "add";
var UPDATE = "update";
var VIEW = "view";
var DELETE = "delete";

var statusArr = [{Key:'Active',Value:'Active'},{Key:'InActive',Value:'InActive'}];

$(document).ready(function(){
    parentRef = parent.frames['iframe'].window;
});

function adjustHeight(){
    var defHeight = 45;
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 40;
    }
}

$(window).load(function(){
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
    init();
});

function init(){
    operation = parentRef.operation;
    selItem = parentRef.selItem;
    $("#cmbCountry").kendoComboBox();
    $("#cmbStatus").kendoComboBox();

    setDataForSelection(statusArr, "cmbStatus", onStatusChange, ["Value", "Key"], 0, "");

    getAjaxObject(ipAddress+"/country/list/","GET",getCountryList,onError);

    buttonEvents();
}

function onError(errorObj){
    console.log(errorObj);
}
function getCountryList(dataObj){
    console.log(dataObj);
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.country){
        if($.isArray(dataObj.response.country)){
            dataArray = dataObj.response.country;
        }else{
            dataArray.push(dataObj.response.country);
        }
    }
    var tempDataArry = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
    }
    console.log(dataArray);
    setDataForSelection(dataArray, "cmbCountry", onCountryChange, ["country", "idk"], 0, "");

    if(operation == UPDATE && selItem){
        console.log(selItem);
        $("#txtAbbrevation").val(selItem.abbr);
        $("#txtName").val(selItem.county);
        $("#txtCode").val(selItem.code);
        var cmbStatus = $("#cmbStatus").data("kendoComboBox");
        if(cmbStatus){
            if(selItem.isActive == 1){
                cmbStatus.select(0);
            }else{
                cmbStatus.select(1);
            }
        }
        var countryIdx = getComboListIndex("cmbCountry","country",selItem.countryId);
        if(countryIdx>=0){
            var cmbCountry = $("#cmbCountry").data("kendoComboBox");
            if(cmbCountry){
                cmbCountry.select(countryIdx);
            }
        }
    }else{
        var cmbStatus = $("#cmbStatus").data("kendoComboBox");
        if(cmbStatus){
            cmbStatus.enable(false);
        }
    }
}
function getComboListIndex(cmbId,attr,attrVal){
    var cmb = $("#"+cmbId).data("kendoComboBox");
    if(cmb){
        var ds = cmb.dataSource;
        var totalRec = ds.total();
        for(var i=0;i<totalRec;i++){
            var dtItem = ds.at(i);
            if(dtItem && dtItem[attr] == attrVal){
                return i;
            }
        }
    }
    return -1;
}
function onCountryChange(){
    var cmbCountry = $("#cmbCountry").data("kendoComboBox");
    if(cmbCountry && cmbCountry.selectedIndex<0){
        cmbCountry.select(0);
    }
}
function onStatusChange(){
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if(cmbStatus && cmbStatus.selectedIndex<0){
        cmbStatus.select(0);
    }
}
function buttonEvents(){
    $("#btnCancel").off("click",onClickCancel);
    $("#btnCancel").on("click",onClickCancel);

    $("#btnSave").off("click",onClickSave);
    $("#btnSave").on("click",onClickSave);

    $("#btnSearch").off("click",onClickSearch);
    $("#btnSearch").on("click",onClickSearch);

    $("#btnReset").off("click",onClickReset);
    $("#btnReset").on("click",onClickReset);

}
function onClickReset(){
    operation = ADD;
    selItem = null;
    $("#txtAbbrevation").val("");
    $("#txtCode").val("");
    $("#txtName").val("");
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if(cmbStatus){
        cmbStatus.select(0);
        cmbStatus.enable(false);
    }
    var cmbCountry = $("#cmbCountry").data("kendoComboBox");
    if(cmbCountry){
        cmbCountry.select(0);
    }
}
function validation(){
    var flag = true;
    var strAbbr = $("#txtAbbrevation").val();
    strAbbr = $.trim(strAbbr);
    if(strAbbr == ""){
        customAlert.error("Error","Enter Abbrevation");
        flag = false;
        return false;
    }
    var strCode = $("#txtCode").val();
    strCode = $.trim(strCode);
    if(strCode == ""){
        customAlert.error("Error","Enter Code");
        flag = false;
        return false;
    }
    var strName = $("#txtName").val();
    strName = $.trim(strName);
    if(strName == ""){
        customAlert.error("Error","Enter Country");
        flag = false;
        return false;
    }

    return flag;
}

function onClickSave(){
    if(validation()){
        var strAbbr = $("#txtAbbrevation").val();
        strAbbr = $.trim(strAbbr);
        var strCode = $("#txtCode").val();
        strCode = $.trim(strCode);
        var strName = $("#txtName").val();
        strName = $.trim(strName);
        var isActive = 0;
        var cmbStatus = $("#cmbStatus").data("kendoComboBox");
        if(cmbStatus){
            if(cmbStatus.selectedIndex == 0){
                isActive = 1;
            }
        }
        var dataObj = {};
        dataObj.abbr = strAbbr;
        dataObj.county = strName;
        dataObj.code = strCode;
        dataObj.createdBy = "101";
        dataObj.isActive = isActive;
        dataObj.isDeleted = "0";

        var cmbCountry = $("#cmbCountry").data("kendoComboBox");
        dataObj.countryId = cmbCountry.value();

        if(operation == ADD){
            var dataUrl = ipAddress+"/county/create";
            createAjaxObject(dataUrl,dataObj,"POST",onCreate,onError);
        }else{
            dataObj.id = selItem.idk;
            var dataUrl = ipAddress+"/county/update";
            createAjaxObject(dataUrl,dataObj,"POST",onCreate,onError);
        }
    }
}

function onCreate(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            var obj = {};
            obj.status = "success";
            obj.operation = operation;
            popupClose(obj);
        }else{
            customAlert.error("error", dataObj.response.status.message);
        }
    }
    /*var obj = {};
    obj.status = "success";
    obj.operation = operation;
    popupClose(obj);*/
}

function onClickSearch(){
    var obj = {};
    obj.status = "search";
    popupClose(obj);
}

function onError(errObj){
    console.log(errObj);
    customAlert.error("Error","Unable to create Country");
}
function onClickCancel(){
    var obj = {};
    obj.status = "false";
    popupClose(obj);
}
function popupClose(obj){
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}



var angularUIgridWrapper;
var parentRef = null;

$(document).ready(function(){
    parentRef = parent.frames['iframe'].window;
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("dgridStateList", dataOptions);
    angularUIgridWrapper.init();
    buildStateListGrid([]);
});


$(window).load(function(){
    loading = false;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
    init();
    buttonEvents();
    adjustHeight();
}

function init(){
    $("#btnSave").prop("disabled", true);
    $("#btnDelete").prop("disabled", true);
    buildStateListGrid([]);
    getAjaxObject(ipAddress+"/state/list/","GET",getStateList,onError);
}
function onError(errorObj){
    console.log(errorObj);
}
function getStateList(dataObj){
    console.log(dataObj);
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.stateExt){
        if($.isArray(dataObj.response.stateExt)){
            dataArray = dataObj.response.stateExt;
        }else{
            dataArray.push(dataObj.response.stateExt);
        }
    }
    var tempDataArry = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
        }
    }
    buildStateListGrid(dataArray);
}
function buttonEvents(){
    $("#btnSave").off("click",onClickOK);
    $("#btnSave").on("click",onClickOK);

    $("#btnCancelDet").off("click",onClickCancel);
    $("#btnCancelDet").on("click",onClickCancel);

    $("#btnDelete").off("click",onClickDelete);
    $("#btnDelete").on("click",onClickDelete);

    $("#btnAdd").off("click");
    $("#btnAdd").on("click",onClickAdd);
}

function adjustHeight(){
    var defHeight = 150;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    angularUIgridWrapper.adjustGridHeight(cmpHeight);
}

function buildStateListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Abbrevation",
        "field": "abbr",
    });
    gridColumns.push({
        "title": "Area Code",
        "field": "countryCode",
    });
    gridColumns.push({
        "title": "State",
        "field": "state",
    });
    gridColumns.push({
        "title": "Country",
        "field": "country",
    });
    gridColumns.push({
        "title": "Status",
        "field": "Status",
    });
    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}
var prevSelectedItem =[];
function onChange(){
    setTimeout(function(){
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if(selectedItems && selectedItems.length>0){
            $("#btnSave").prop("disabled", false);
            $("#btnDelete").prop("disabled", false);
        }else{
            $("#btnSave").prop("disabled", true);
            $("#btnDelete").prop("disabled", true);
        }
    },100)

}

function onClickOK(){
    setTimeout(function(){
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if(selectedItems && selectedItems.length>0){
            var obj = {};
            obj.selItem = selectedItems[0];
            parentRef.operation = "update";
            parentRef.selItem = selectedItems[0];
            addState();
        }
    })
}
function onClickDelete(){
    customAlert.confirm("Confirm", "Are you sure you want delete?",function(response){
        if(response.button == "Yes"){
            var selectedItems = angularUIgridWrapper.getSelectedRows();
            console.log(selectedItems);
            if(selectedItems && selectedItems.length>0){
                var selItem = selectedItems[0];
                if(selItem){
                    var dataUrl = ipAddress+"/state/delete/";
                    var reqObj = {};
                    reqObj.id = selItem.idk;
                    reqObj.abbr = selItem.abbr;
                    reqObj.country = selItem.county;
                    reqObj.code = selItem.code;
                    reqObj.isDeleted = "1";
                    reqObj.modifiedBy = sessionStorage.userId;//101
                    createAjaxObject(dataUrl,reqObj,"POST",onDeleteCountryt,onError);
                }
            }
        }
    });
}
function onDeleteCountryt(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            customAlert.info("info", "State Deleted Successfully");
            init();
        }else{
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}
var operation = "add";
function onClickAdd(){
    /*var obj = {};
     obj.status = "Add";
     obj.operation = "ok";
        var windowWrapper = new kendoWindowWrapper();
        windowWrapper.closePageWindow(obj);*/

    parentRef.operation = "add";
    parentRef.selItem = null;
    addState();
}
function addState(){
    var popW = 600;
    var popH = 500;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    if(operation == "add"){
        profileLbl = "Add State";
    }else{
        profileLbl = "Edit State";
    }

    devModelWindowWrapper.openPageWindow("../../html/masters/createState.html", profileLbl, popW, popH, true, closeAddStateAction);
}
function closeAddStateAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        if(returnData.operation == "add"){
            customAlert.info("Info","State Created Successfully.");
        }else if(returnData.operation == "update"){
            customAlert.info("Info","State Updated Successfully.");
        }
        init();
    }
}
function onClickCancel(){
    var obj = {};
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}

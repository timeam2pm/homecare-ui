var angularUIgridWrapper;
var reportRef = null;
var operation = "";
var selItem = null;
var ADD = "add";
var UPDATE = "edit";
var VIEW = "view";
var patientInfoObject = null;
var commId = "";

var reportPageName;


$(document).ready(function(){
	var dataOptions = {
        pagination: false,
        changeCallBack: onChange
	}
	reportPageName = sessionStorage.reportPageName;
	if(reportPageName == 'designer'){
   	 	document.getElementById("btnReportDesign").innerHTML = "<span>Report Design</span>";
    }
    else if(reportPageName == 'viewer')
    {
    	document.getElementById("btnReportDesign").innerHTML = "<span>View</span>";
        // $("#btnReportDesign").off("click",onClickAdd);
        // $("#btnReportDesign").on("click",onClickAdd);
    }
    buttonEvents();
	angularUIgridWrapper = new AngularUIGridWrapper("dgridDAccountList", dataOptions);
	angularUIgridWrapper.init();
	buildAccountListGrid([]);
});


$(window).load(function(){
	reportRef = parent.frames['iframe'].window;

	loading = false;
	$(window).resize(adjustHeight);
onLoaded();
	if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100%");
    }
});

function onLoaded(){
	init();
    //buttonEvents();
	adjustHeight();
	$('.btnActive').trigger('click');
}

function init(){
	$("#btnEdit").prop("disabled", true);
	$("#btnDelete").prop("disabled", true);
	buildAccountListGrid([]);
	
	$('[data-toggle="tab"]').on('click', function(e) {
    	e.preventDefault();
    	$('.alert').remove();
    });
	//setDataForSelection(typeArr, "txtType", onTypeChange, ["Value", "Key"], 0, "");
	//setDataForSelection(filterArr, "cmbFilterByName", onFilterChange, ["Value", "Key"], 0, "");
	//getZip();
	getAjaxObject(ipAddress+"/homecare/reports/","GET",getAccountList,onError);
}
function onError(errorObj){
	console.log(errorObj);
}
function getAccountList(dataObj){
	
/*	console.log(dataObj);*/
	var dataArray = [];
	//var tempDataArry = [];
	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		if($.isArray(dataObj.response.reports)){
			dataArray = dataObj.response.reports;
		}else{
			dataArray.push(dataObj.response.reports);
		}
	}else{
		//customAlert.error("Error",dataObj.response.status.message);
	}
	
	for(var i=0;i<dataArray.length;i++){
		dataArray[i].idk = dataArray[i].id; 
		dataArray[i].Status = "InActive";
		if(dataArray[i].isActive == 1){
			dataArray[i].Status = "Active";
			var createdDate = new Date(dataArray[i].createdDate);
			var createdDateString = createdDate.toLocaleDateString();
			var modifiedDate = new Date(dataArray[i].modifiedDate);
			var modifiedDateString = modifiedDate.toLocaleDateString();
			dataArray[i].createdDateString = createdDateString;
			dataArray[i].modifiedDateString = modifiedDateString;
		}
		else{
			dataArray = [];
		}
	}
	buildAccountListGrid(dataArray);
}
function buttonEvents(){		
	$("#btnReportDesign").off("click",fncSearchServiceuser);
	$("#btnReportDesign").on("click",fncSearchServiceuser);
}
function searchOnLoad(status) {
	buildAccountListGrid([]);
	if(status == "active") {
		var urlExtn = '/homecare/reports/';
	}
	else if(status == "inactive") {
		var urlExtn = '/homecare/reports/';
	}
	getAjaxObject(ipAddress+urlExtn,"GET",getAccountList,onError);
}
function adjustHeight(){
	var defHeight = 250;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
	angularUIgridWrapper.adjustGridHeight(cmpHeight);
}
function onClickActive() {
	$(".btnInActive").removeClass("selectButtonBarClass");
	$(".btnActive").addClass("selectButtonBarClass");
}
function onClickInActive() {
	$(".btnActive").removeClass("selectButtonBarClass");
	$(".btnInActive").addClass("selectButtonBarClass");
}
function onClickFilterBtn(e) {
	e.preventDefault();
	$('.alert').remove();
	var filterName = $("#cmbFilterByName").val();
	var filterValue = $("#cmbFilterByValue").val();
	buildAccountListGrid([]);
}

function buildAccountListGrid(dataSource) {
	var gridColumns = [];
	var otoptions = {};
	otoptions.noUnselect = false;
	gridColumns.push({
        "title": "ID",
        "field": "idk",
	});
    gridColumns.push({
        "title": "Name",
        "field": "name",
	});
    gridColumns.push({
        "title": "Notes",
        "field": "notes",
	});
	angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions); 
	adjustHeight();
	//onIndividualRowClick();
}

function showAction(){
	var node = '<div style="text-align:center">';
	//node += '<input  type="button" id="btn" value="View" onclick="onClickAction(event)"></input>';
	node += '<a href="#" onclick="onClickAction(event)" style="cursor:pointer">View</a>';
	node += '</div>';
	return node;
}

var selRow = null;
function onClickAction(e){
	setTimeout(function(){
		selRow = angular.element($(e.target).parent()).scope();
		onClickGPSReport();
	})
}
function onClickGPSReport(){
	reportRef.screenType = "providers";
	reportRef.providerid = selRow.row.entity.userId;
	openReportPopup("../../html/patients/geoReport.html","GPS Report");
}
function openReportPopup(path,title){
	var popW = "60%";
    var popH = "75%";

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = title;
    devModelWindowWrapper.openPageWindow(path, profileLbl, popW, popH, false, closeCallReport);
}
function closeCallReport(evt,ret){
	
}
function getCountryZoneName() {
	 var countryName = sessionStorage.countryName;
        if (countryName.indexOf("India") >= 0) {
            $('.postalCode').html('Postal Code :<span class="mandatoryField">*</span>');
            $('.stateLabel').text("State");
            $('.zipFourWrapper').addClass('hideZipFourWrapper');
        } else if (countryName.indexOf("United Kingdom")) {
            $('.postalCode').html('Postal Code :<span class="mandatoryField">*</span>');
            $('.stateLabel').text("County");
            $('.zipFourWrapper').addClass('hideZipFourWrapper');
        } else {
        	$('.postalCode').html('Zip :<span class="mandatoryField">*</span>');
            $('.stateLabel').text("State");
            $('.zipFourWrapper').removeClass('hideZipFourWrapper');
        }
}
var prevSelectedItem =[];
function onChange(){
	setTimeout(function(){
		var selectedItems = angularUIgridWrapper.getSelectedRows();
		 console.log(selectedItems);
	},100)
}


var selDocItem = null;
function onClickOK(){
	 var selectedItems = angularUIgridWrapper.getSelectedRows();
	 console.log(selectedItems);
	 if(selectedItems && selectedItems.length>0){
		var obj = {};
		obj.selItem = selectedItems[0];
		selDocItem = selectedItems[0];
		reportRef.selDocItem = selDocItem;
	 }
}

function onClickDelete(){
	customAlert.confirm("Confirm", "Are you sure you want delete?",function(response){
		if(response.button == "Yes"){
			var selectedItems = angularUIgridWrapper.getSelectedRows();
			 console.log(selectedItems);
			 if(selectedItems && selectedItems.length>0){
				 var selItem = selectedItems[0];
				 if(selItem){
					 var dataUrl = ipAddress+"/homecare/reports/";
					 var reqObj = {};
					 reqObj.id = selItem.id;
					 reqObj.isDeleted = "1";
					 reqObj.isActive = "0";
					 reqObj.modifiedBy = sessionStorage.userId;
					 createAjaxObject(dataUrl,reqObj,"DELETE",onDeleteCountryt,onError);
				 }
			 }
		}
	});
}
function onDeleteCountryt(dataObj){
	console.log(dataObj);
	if(dataObj && dataObj.response.status){
		if(dataObj.response.status.code == "1"){
			customAlert.info("info", "Report Master Deleted Successfully");
			buildAccountListGrid([]);
			init();
		}else{
			customAlert.error("error", dataObj.message);
		}
	}
}
function onClickAdd(){
	addReportMaster("add");
}

function addReportMaster(opr){
	 onClickOK();
	 sessionStorage.setItem("reportAPI", selDocItem.api);
     sessionStorage.setItem("reportName", selDocItem.name.toLowerCase()); 
     if(reportPageName == 'designer'){
    	 location.href = '../../html/reports/designer.html';
     }
     else if(reportPageName == 'viewer'){
    	 location.href = '../../html/reports/reportView.html';
     }
}

function onClickCancel(){
	var obj = {};
	var windowWrapper = new kendoWindowWrapper();
	windowWrapper.closePageWindow(obj);
}

function onClickClose() {
    init();
    $('.selectButtonBarClass').trigger('click');
}


var zipSelItem = null;
var cityId = "";

function getPrefix() {
    getAjaxObject(ipAddress + "/master/Prefix/list?is-active=1", "GET", getCodeTableValueList, onError);
}

function onComboChange(cmbId) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb && cmb.selectedIndex < 0) {
        cmb.select(0);
    }
}

var billActNo = "";

function getTableFirstListArray(dataObj) {
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.codeTable) {
        if ($.isArray(dataObj.response.codeTable)) {
            dataArray = dataObj.response.codeTable;
        } else {
            dataArray.push(dataObj.response.codeTable);
        }
    }
    var tempDataArry = [];
    for (var i = 0; i < dataArray.length; i++) {
        if (dataArray[i].isActive == 1) {
            var obj = dataArray[i];
            obj.idk = dataArray[i].id;
            obj.status = dataArray[i].Status;
            tempDataArry.push(obj);
        }
    }
    return tempDataArry;
}


function onClickReset() {
    if(operation == ADD) {
    	operation = ADD;
    }
    else {
    	operation = UPDATE;
    }
    patientId = "";
    $("#txtID").val("");
    $("#txtRN").val("");
    $("#txtNotes").val("");
    $("#txtAPI").val("");
 }

function setComboReset(cmbId) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb) {
        cmb.select(0);
    }
}

function validation() {
    var flag = true;
    var txtFC = $("#txtFC").val();
    txtFC = $.trim(txtFC);
    if (txtFC == "") {
        customAlert.error("Error", "Enter Finance charges");
        flag = false;
        return false;
    }
    if (cityId == "") {
        customAlert.error("Error", "Select City ");
        flag = false;
        return false;
    }
    /*var strAbbr = $("#txtAbbrevation").val();
    	strAbbr = $.trim(strAbbr);
    	if(strAbbr == ""){
    		customAlert.error("Error","Enter Abbrevation");
    		flag = false;
    		return false;
    	}
    	var strCode = $("#txtCode").val();
    	strCode = $.trim(strCode);
    	if(strCode == ""){
    		customAlert.error("Error","Enter Code");
    		flag = false;
    		return false;
    	}
    	var strName = $("#txtName").val();
    	strName = $.trim(strName);
    	if(strName == ""){
    		customAlert.error("Error","Enter Country");
    		flag = false;
    		return false;
    	}*/

    return flag;
}

function getComboDataItem(cmbId) {
    var dItem = null;
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb && cmb.dataItem()) {
        dItem = cmb.dataItem();
        return cmb.text();
    }
    return "";
}

function getComboDataItemValue(cmbId) {
    var dItem = null;
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb && cmb.dataItem()) {
        dItem = cmb.dataItem();
        return cmb.value();
    }
    return "";
}


function onClickSave() {
    var sEmail = $('#txtEmail').val();
    /*if (validation()) {*/
    	$('.alert').remove();
        var strId = $("#txtID").val();
        var strExtId1 = $("#txtExtID1").val();
        var strExtId2 = $("#txtExtID2").val();
        var strPrefix = getComboDataItem("cmbPrefix");
        var strSuffix = getComboDataItem("cmbSuffix");
        var strNickName = $("#txtNN").val();
        var strStatus = getComboDataItem("cmbStatus");
        var strAbbr = $("#txtAbbreviation").val();
        var strFN = $("#txtFN").val();
        var strMN = $("#txtMN").val();
        var strLN = $("#txtLN").val();

        var dtItem = $("#dtDOB").data("kendoDatePicker");
        var strDate = "";
        if (dtItem && dtItem.value()) {
            strDate = kendo.toString(dtItem.value(), "yyyy-MM-dd");
        }

        var strSSN = $("#txtSSN").val();
        var strGender = getComboDataItem("cmbGender");
        var strAdd1 = $("#txtAdd1").val();
        var strAdd2 = $("#txtAdd2").val();
        var strCity = $("#txtCity").val();
        var strState = $("#txtState").val();
        var strCountry = $("#txtCountry").val();

        var strZip = $("#cmbZip").val();
        var strZip4 = $("#txtZip4").val();
        var strHPhone = $("#txtHPhone").val();
        var strExt = $("#txtExtension").val();
        var strWp = $("#txtWPhone").val();
        var strWpExt = $("#txtWPExt").val();
        var strCell = $("#txtCell").val();

        var strSMS = getComboDataItem("cmbSMS");

        var strEmail = $("#txtEmail").val();
        var strLan = getComboDataItem("cmbLan");
        var strRace = getComboDataItem("cmbRace");
        var strEthinicity = getComboDataItem("cmbEthicity");

        var dataObj = {};
        dataObj.createdBy = sessionStorage.userId;
        dataObj.externalId1 = strExtId1;
        dataObj.externalId2 = strExtId2;
        dataObj.abbreviation = $("#txtAbbreviation").val();
        var txtType = $("#txtType").data("kendoComboBox");
        var strTType = "";
        if (txtType) {
            strTType = Number(txtType.value());
        }
        dataObj.type = strTType; //$("#txtType").val();;
        dataObj.prefix = strPrefix; //$("#txtEmail").val();;
        dataObj.suffix = strSuffix;
        dataObj.firstName = strFN;
        dataObj.middleName = strMN;
        dataObj.lastName = strLN;
        dataObj.nickname = strNickName;
        dataObj.billingAccountName = $("#txtBAN").val();
        dataObj.billingAccountId = Number(billActNo);
        var txtFAN = $("#txtFAN").data("kendoComboBox");
        var fId = "";
        if (txtFAN) {
            fId = txtFAN.value();
        }
        var txtUserId = $("#cmbUserId").data("kendoComboBox");
        var cmbUserId = "";
        if(txtUserId) {
        	cmbUserId = txtUserId.value();
        }
        dataObj.facilityId = Number(fId);
        dataObj.neicSpeciality = $("#txtNEIC").val();
        dataObj.amaSpeciality = $("#txtAS").val();
        dataObj.nuucType = $("#txtNT").val();;
        dataObj.upin = $("#txtUPIN").val();;
        dataObj.dea = $("#txtDEA").val();;
        dataObj.npi = $("#txtNPI").val();
        dataObj.taxonomyId = $("#txtTI").val();
        dataObj.pcp = $("#txtPCP").val();
        dataObj.billableDoctor = $("#txtBD").val();
        var finCharges = $("#txtFC").val() != "" ? $("#txtFC").val() : 0.0;
        dataObj.financeCharges = finCharges;
        if(strStatus == "InActive") {
        	dataObj.isActive = 0;
        }
        else if(strStatus == "Active") {
	        dataObj.isActive = 1;
	    }
        dataObj.userId = Number(cmbUserId);

        var comm = [];
        var comObj = {};
        //comObj.country = $("#txtCountry").val(); //"101";
        //comObj.city = $("#txtCity").val();
        if (strZip) {
            comObj.cityId = zipSelItem.cityId || zipSelItem.idk;
        }
        if(strStatus == "InActive") {
        	comObj.isActive = 0;
        }
        else if(strStatus == "Active") {
	        comObj.isActive = 1;
	    }
        /*comObj.countryId = cityId;*/
        comObj.isDeleted = 0;
        //comObj.zipFour = $("#txtZip4").val(); //"2323";
        comObj.sms = getComboDataItem("cmbSMS");
        //comObj.state = $("#txtState").val();
        comObj.workPhoneExt = $("#txtWExtension").val();
        comObj.email = $("#txtEmail").val();
        //comObj.zip = strZip;
        comObj.parentTypeId = 500;
        comObj.address2 = $("#txtAdd2").val();
        comObj.address1 = $("#txtAdd1").val();
        comObj.homePhone = $("#txtHPhone").val();
        /*comObj.stateId = 1;
        comObj.areaCode = "232";*/
        comObj.homePhoneExt = $("#txtExtension").val();
        comObj.workPhone = $("#txtWPhone").val();
        comObj.cellPhone = $("#txtCell").val();

        comObj.defaultCommunication = 1;
        /*if (operation == UPDATE) {}*/

        if (operation == UPDATE) {
            var communicationLen = selDocItem.communications != null ? selDocItem.communications.length : 0;
	        var cityIndexVal = "";
	        //var dupCityIndexVal = "";
	        for(var i=0; i<communicationLen; i++) {
	        	if(selDocItem.communications[i].parentTypeId == 500) {
	        		cityIndexVal = i;
	        	}
	        	/*if(selDocItem.communications[i].parentTypeId == 501) {
	        		dupCityIndexVal = i;
	        	}*/
	        }
	        if(cityIndexVal != "" || (cityIndexVal == 0 && selDocItem.communications != null)) {
	        	comObj.id = selDocItem.communications[cityIndexVal].id;
	        	comObj.modifiedBy = sessionStorage.userId;
            	comObj.parentId = selDocItem.idk;
	        }
	        /*if(dupCityIndexVal != "") {
        		comObj1.id = selDocItem.communications[dupCityIndexVal].id;
        		comObj1.modifiedBy = sessionStorage.userId;
            	comObj1.parentId = selDocItem.idk;
	        }*/
            //comm1.modifiedDate = new Date().getTime();
        }
        else {
        	comObj.createdBy = Number(sessionStorage.userId);
        }

        comm.push(comObj);
        dataObj.communications = comm;
        if(strStatus && strStatus != "" && strAbbr != "" && strFN != "" && strMN != "" && strLN != "" && strTType != "" && strAdd1 != "" && strZip != "" && strWp != "" && strCell != "" && strEmail != "" && $("#txtFAN").val() != "" && cmbUserId != "") {
        	if(!validateEmail(strEmail)) {
	        	$("body").animate({
				    scrollTop: 0
				}, 500);
		    	$('.customAlert').append('<div class="alert alert-danger">Please enter valid Email Address</div>');
	        }
	        else {
		        if (operation == ADD) {
		            var dataUrl = ipAddress + "/provider/create";
		            createAjaxObject(dataUrl, dataObj, "POST", onCreate, onError);
		        } else if (operation == UPDATE) {
		            dataObj.modifiedBy = sessionStorage.userId;
		            dataObj.id = selDocItem.idk;
		            dataObj.isDeleted = "0";
		            var dataUrl = ipAddress + "/provider/update";
		            createAjaxObject(dataUrl, dataObj, "POST", onUpdate, onError);
		        }
		    }
	    }
	    else {
	    	$("body").animate({
			    scrollTop: 0
			}, 500);
	    	$('.customAlert').append('<div class="alert alert-danger">Please fill all the Required Fields</div>');
	    }
    /*}*/
}
function onTypeChange() {

}
function onFilterChange() {
	var cmbFilterByName = $("#cmbFilterByName").data("kendoComboBox");
    if (cmbFilterByName && cmbFilterByName.selectedIndex < 0) {
        cmbFilterByName.select(0);
    }
    if($("#cmbFilterByName").val() == "id" || $("#cmbFilterByName").val() == "type" || $("#cmbFilterByName").val() == "user-id" || $("#cmbFilterByName").val() == "facility-id") {
    	$("#cmbFilterByValue").removeAttr("disabled");
    	$("#cmbFilterByValue").attr("type","number");
    }
    else if($("#cmbFilterByName").val() == "All") {
    	$("#cmbFilterByValue").attr("disabled","disabled");
    	$("#providerFilter-btn").attr("disabled","disabled");
    	$("#cmbFilterByValue").val('');
    	buildAccountListGrid([]);
		getAjaxObject(ipAddress+"/homecare/reports/","DELETE",getAccountList,onError);
		
    }
    else {
    	$("#cmbFilterByValue").removeAttr("disabled");
    	$("#cmbFilterByValue").attr("type","text");
    }
}

function onCreate(dataObj) {
    console.log(dataObj);
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            var obj = {};
            obj.status = "success";
            obj.operation = operation;
            popupClose(obj);
            // $('.customAlert').append('<div class="alert alert-success">'+dataObj.response.status.message+'</div>');
            // $('#btnReset').trigger('click');
            // $('.tabContentTitle').text('Add Provider');
        } else {
        	// $('.customAlert').append('<div class="alert alert-danger">'+dataObj.response.status.message+'</div>');
            customAlert.error("error", dataObj.response.status.message);
        }
    }
    var obj = {};
    obj.status = "success";
    obj.operation = operation;
    popupClose(obj);
}
function onUpdate(dataObj){
	console.log(dataObj);
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            /*var obj = {};
            obj.status = "success";
            obj.operation = operation;
            popupClose(obj);*/
            $('.customAlert').append('<div class="alert alert-success">'+dataObj.response.status.message+'</div>');
            $('.tabContentTitle').text('Update Provider');
            var id = $('#txtID').val();
            getAjaxObject(ipAddress+"/provider/list?id="+id,"GET",updateList,onError);
        } else {
        	//$('.customAlert').append('<div class="alert alert-danger">'+dataObj.response.status.message+'</div>');
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}
function updateList(dataObj) {
	console.log(dataObj);
	selDocItem = dataObj.response.provider[0];
	selDocItem.idk = selDocItem.id;
	addReportMaster("update");
}

function onError(errObj) {
    console.log(errObj);
    customAlert.error("Error", "Error");
}

function onClickCancel() {
    var obj = {};
    obj.status = "false";
    popupClose(obj);
}

function onClickSearch() {
    /*var obj = {};
    obj.status = "search";
    popupClose(obj);*/
}

function popupClose(obj) {
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}
function closePtAddAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        console.log(returnData);
        var pID = returnData.selItem.PID;
        viewPatientId = pID;
        var pName = returnData.selItem.FN+" "+returnData.selItem.MN+" "+returnData.selItem.LN;
        viewpName = pName;
        if(pID != ""){
            /*$("#lblName").text(pID+" - "+pName);
            $("#txtPatient").val(pName);
            var imageServletUrl = ipAddress + "/download/patient/photo/" + pID + "/?" + Math.round(Math.random() * 1000000);
            $("#imgPhoto").attr("src", imageServletUrl);

            getAjaxObject(ipAddress + "/patient/" + pID, "GET", onGetPatientInfo, onError);*/
            sessionStorage.setItem("pID", pID);
            location.href = '../../html/reports/reportView.html';
        }
    }
}
function fncSearchServiceuser() {
    onClickOK();
    sessionStorage.setItem("reportAPI", selDocItem.api);
    sessionStorage.setItem("reportName", selDocItem.name.toLowerCase());
    sessionStorage.setItem("reportID", selDocItem.idk);


    if(reportPageName == 'designer'){
        location.href = '../../html/reports/designer.html';
    }
    else if(reportPageName == 'viewer') {
        /*location.href = '../../html/reports/reportView.html';*/

        var popW = "60%";
        var popH = 500;

        var profileLbl;
        var devModelWindowWrapper = new kendoWindowWrapper();
        profileLbl = "Search Patient";
        devModelWindowWrapper.openPageWindow("../../html/reports/reportSearchServiceUser.html", profileLbl, popW, popH, true, closePtAddAction);
    }
}
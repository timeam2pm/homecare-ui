var angularUIgridWrapper;
var parentRef = null;
var recordType = "1";
var dataArray = [];
var ADD = "add";
var UPDATE = "edit";
var VIEW = "view";
var DELETE = "delete";
var operation = "add";
var ds  = "";
var IsFlag = 1;
var cntry = sessionStorage.countryName;
var selectPatientId;
var dtFMT = "dd/MM/yyyy";
var appointmentdtFMT = "dd/MM/yyyy hh:mm tt";
var allDatesInWeek = [];
var billToName;
var selDate;
var appointmentId;

$(document).ready(function(){
    $("#pnlPatient",parent.document).css("display","none");
    sessionStorage.setItem("IsSearchPanel", "1");
    themeAPIChange();
    parentRef = parent.frames['iframe'].window;
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }

    var cntry = sessionStorage.countryName;
    if((cntry.indexOf("India")>=0) || (cntry.indexOf("United Kingdom")>=0)){
        $("#lblFacility").text("Location :");
    }else if(cntry.indexOf("United State")>=0) {
        $("#lblFacility").text("Facility :");
    }

    angularUIgridWrapper = new AngularUIGridWrapper("dgridCopyAppointmentsList", dataOptions);
    angularUIgridWrapper.init();
    buildDeviceListGrid([]);
    $(".popupClose").off("click", onClickCancel);
    $(".popupClose").on("click", onClickCancel);

    $("#btnCancel").off("click",onClickCancel);
    $("#btnCancel").on("click",onClickCancel);

});


$(window).load(function(){
    loading = false;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }

});

function onLoaded(){
    init();
    buttonEvents();
    adjustHeight();
    onClickSubmit();
}

function init(){
    $("#btnEdit").prop("disabled", true);
    $("#btnDelete").prop("disabled", true);

    $("#txtFDate").kendoDatePicker({ format: dtFMT, value: new Date() });


    callMastersData();
}



function onError(errorObj){
    console.log(errorObj);
}

function buttonEvents(){


    $("#btnSubmit").off("click",onClickSubmit);
    $("#btnSubmit").on("click",onClickSubmit);

    $("#btnEdit").off("click",onClickEdit);
    $("#btnEdit").on("click",onClickEdit);

    $("#btnDelete").off("click",onClickDelete);
    $("#btnDelete").on("click",onClickDelete);

    $("#btnCopy").off("click");
    $("#btnCopy").on("click",onClickCopy);

    $("#btnReset").off("click",onClickReset);
    $("#btnReset").on("click",onClickReset);

    $("#btnServiceUser").off("click");
    $("#btnServiceUser").on("click",onClickServiceUser);

    $("#btnStaff").off("click");
    $("#btnStaff").on("click",onClickStaff);

    $("#btnSave").off("click");
    $("#btnSave").on("click", function () {
        buildDeviceListGrid([]);
        appointmentSave();
    });

    $(".btnActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('active');
        onClickActive();
    });
    $(".btnInActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('inactive');
        onClickInActive();
    });

}

function searchOnLoad(status) {
    buildDeviceListGrid([]);
    if(status == "active") {
        var urlExtn = ipAddress+"/homecare/vacations/?fields=*&parentTypeId="+Number(parentRef.type)+"&parentId="+Number(parentRef.patientId)+"&is-active=1&is-deleted=0";

    }
    else if(status == "inactive") {
        var urlExtn =  ipAddress+"/homecare/vacations/?fields=*&parentTypeId="+Number(parentRef.type)+"&parentId="+Number(parentRef.patientId)+"&is-active=0&is-deleted=1"
    }
    getAjaxObject(urlExtn,"GET",handleGetVacationList,onError);
}


function onClickAdd(){
    $("#addPopup").show();
    $('#viewDivBlock').hide();
    $("#txtID").hide();
    $(".filter-heading").html("Add Staff Leave Request");
    parentRef.operation = "add";
    onClickReset();
}

function addReportMaster(opr){
    var popW = 500;
    var popH = "43%";

    //$('.listDataWrapper').hide();
    //$('.addOrRemoveWrapper').show();
    $('.alert').remove();
    var profileLbl = "Add Task Type";
    parentRef.operation = opr;
    operation = opr;
    if(opr == "add"){
        $('#btnSave').html('<span><img src="../../img/medication/Save.png" class="providerLink-btn-img" />Save</span>');
        $('.tabContentTitle').text('Add Task Type');
        $('#btnReset').show();
        //var billActName = $("#txtBAN").val();
        $('#btnReset').trigger('click');
        //$("#txtBAN").val(billActName);
    }else{

        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if (selectedItems && selectedItems.length > 0) {
            var obj = selectedItems[0];
            parentRef.id = obj.idk;
            parentRef.displayOrder  = obj.displayOrder;
            parentRef.type = obj.type;
            parentRef.DIO = obj.DIO;
            parentRef.activityGroup = obj.activityGroup;
            parentRef.isActive = obj.isActive;
            operation = UPDATE;
        }

        profileLbl = "Edit Task Type";
        $('.tabContentTitle').text('Edit Task Type');
        $('#btnReset').hide();
        $('#btnSave').html('<span><img src="../../img/medication/Save.png" class="providerLink-btn-img" />Update</span>');
        $('#btnReset').trigger('click');
    }
    var devModelWindowWrapper = new kendoWindowWrapper();
    devModelWindowWrapper.openPageWindow("../../html/masters/createactivityTypeList.html", profileLbl, popW, popH, true, closeaddReportMastertAction);
}
function closeaddReportMastertAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        var opr = returnData.operation;
        if(opr == "add"){
            customAlert.info("info", "Task Type created successfully");
        }else{
            customAlert.info("info", "Task Type updated successfully");
        }
        init();
    }
}
function onClickActive() {
    $(".btnInActive").removeClass("radioButton-active");
    $(".btnActive").addClass("radioButton-active");
}

function onClickInActive() {
    $(".btnActive").removeClass("radioButton-active");
    $(".btnInActive").addClass("radioButton-active");
}

function onClickDelete(){
    customAlert.confirm("Confirm", "Are you sure to delete?",function(response){
        if(response.button == "Yes"){
            var dataObj = {};
            dataObj.createdBy = Number(sessionStorage.userId);
            dataObj.isDeleted = 1;
            dataObj.isActive = 0;
            var dataUrl = ipAddress +"/homecare/vacations/";
            var method = "DELETE";
            var selectedItems = angularUIgridWrapper.getSelectedRows();
            dataObj.id = selectedItems[0].idk;
            createAjaxObject(dataUrl, dataObj, method, onCreateDelete, onError);
        }
    });
}

function onCreateDelete(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "Vacation deleted successfully";
            customAlert.error("Info", msg);
            operation = ADD;
            init();
        }
    }
}


function onClickCancel(){
    $(".filter-heading").html("View Appointments");
    $("#viewDivBlock").show();
    $("#divEditAppointment").hide();
}

function adjustHeight(){
    var defHeight = 280;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    angularUIgridWrapper.adjustGridHeight(cmpHeight);
}

function buildDeviceListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    otoptions.rowHeight = 100;
    gridColumns.push({
        "title": "Appt Date Time",
        "field": "dateTimeOfAppointment",
        "enableColumnnMenu": false,
        "width": "24%"
    });
    gridColumns.push({
        "title": "SU Name",
        "field": "name",
        "enableColumnMenu": false,
    });
    gridColumns.push({
        "title": "Staff Name",
        "field": "staffName",
        "enableColumnMenu": false,
    });
    gridColumns.push({
        "title": "Appt Reason",
        "field": "appointmentReasonDesc",
        "enableColumnMenu": false,
    });



    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
}

function onChange(){
    setTimeout(function() {
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        if (selectedItems && selectedItems.length > 0) {
            if(selectedItems[0].approvedBy != '') {
                $("#btnEdit").prop("disabled", false);
                $("#btnDelete").prop("disabled", false);
            }
            else {
                $("#btnEdit").prop("disabled", true);
                $("#btnDelete").prop("disabled", true);
            }
        }
    });
}
var apptselectedItems;
function onClickEdit() {
    $(".filter-heading").html("Edit Appointment");
    parentRef.operation = "edit";
    operation = "edit";
    $("#viewDivBlock").hide();
    $("#divEditAppointment").show();
    var selectedItems = angularUIgridWrapper.getSelectedRows();
    if (selectedItems && selectedItems.length > 0) {
        var obj = selectedItems[0];
        apptselectedItems = selectedItems[0];
        selectPatientId = obj.patientId;
        if (obj.idk) {
            appointmentId = obj.idk;
        }
        else {
            appointmentId = obj.id;
        }

        if (obj.composition && obj.composition.appointmentReason.value) {
            $("#cmbReason").val(obj.composition.appointmentReason.value);
        } else {
            $("#cmbReason").val(obj.appointmentReason);
        }
        if (obj.composition && obj.composition.appointmentType) {
            $("#cmbStatus").val(obj.composition.appointmentType.value);
        }
        else {
            $("#cmbStatus").val(obj.appointmentType);
        }

        selectProviderId = obj.providerId;
        if (selectProviderId != 0) {
            if (obj.composition && obj.composition.provider) {
                $("#txtStaff").val(obj.composition.provider.lastName + " " + obj.composition.provider.firstName + " " + (obj.composition.provider.middleName != null ? obj.composition.provider.middleName : " "));
            } else {
                $("#txtStaff").val(obj.staffName);
            }

        }
        else {
            $("#txtStaff").val("");
        }
        if (obj.composition && obj.composition.facility) {
            $("#txtFacility").val(obj.composition.facility.name);
        }

        if (obj.composition && obj.composition.patient) {
            $("#txtSU").val(obj.composition.patient.lastName + " " + obj.composition.patient.firstName + " " + (obj.composition.patient.middleName != null ? obj.composition.patient.middleName : " "));
        }

        var txtStartDate = $("#txtStartDate").data("kendoDateTimePicker");
        if (txtStartDate) {
            //txtStartDate.value(GetDateTimeEdit(obj.dateOfAppointment));
            $("#txtStartDate").val(GetDateTimeEdit(obj.dateOfAppointment));
        }
        var endappoinment = (obj.dateOfAppointment + (Number(obj.duration) * 60000));
        var txtEndDate = $("#txtEndDate").data("kendoDateTimePicker");
        if (txtEndDate) {
            //txtEndDate.value(GetDateTimeEdit(endappoinment));
            $("#txtEndDate").val(GetDateTimeEdit(endappoinment));
        }
        $("#txtPayout").val(obj.payoutHourlyRate);
        billToName = obj.billToName;
        getAjaxObject(ipAddress + "/carehome/patient-billing/?is-active=1&patientId=" + selectPatientId + "&fields=*,shift.*,travelMode.*,billingType.*,billingPeriod.*,billTo.name,billingRateType.code", "GET", getPatientData, onError);
    }
}


var operation = "add";
var selFileResourceDataItem = null;

function onStatusChange(){
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if(cmbStatus && cmbStatus.selectedIndex<0){
        cmbStatus.select(0);
    }
}

function onClickReset() {
    $("#txtReason").val("");
    $("#txtFDate").val("");
    $("#txtTDate").val("");
    operation = ADD;
}

function themeAPIChange(){
    getAjaxObject(ipAddress+"/homecare/settings/?id=2","GET",getThemeValue,onError);
}

function getThemeValue(dataObj){
    console.log(dataObj);
    var tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.activityTypes){
            if($.isArray(dataObj.response.settings)){
                tempCompType = dataObj.response.settings;
            }else{
                tempCompType.push(dataObj.response.settings);
            }
        }
        if(tempCompType.length > 0){
            themesChange(tempCompType[0].value);
        }
    }
}

function themesChange(themeValue){
    if(themeValue == 2){
        loadAPi("../../Theme2/Theme02.css");
    }
    else if(themeValue == 3){
        loadAPi("../../Theme3/Theme03.css");
    }
}

function getFacilityList(dataObj) {
    var dataArray = [];
    if (dataObj) {
        if ($.isArray(dataObj.response.facility)) {
            dataArray = dataObj.response.facility;
        } else {
            dataArray.push(dataObj.response.facility);
        }
    }
    var tempDataArry = [];
    for (var i = 0; i < dataArray.length; i++) {
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if (dataArray[i].isActive == 1) {
            dataArray[i].Status = "Active";
        }
        $("#cmbFacility").append('<option value="' + dataArray[i].idk + '">' + dataArray[i].name + '</option>');
    }
}

function onClickServiceUser(){
    var popW = 800;
    var popH = 450;

    if(parentRef)
        parentRef.searchZip = true;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Service User";
    devModelWindowWrapper.openPageWindow("../../html/patients/searchPatient.html", profileLbl, popW, popH, true, onCloseServiceUserAction);
}
function onCloseServiceUserAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        selectPatientId = returnData.selItem.PID;
        $('#txtSU').val(returnData.selItem.LN + " "+ returnData.selItem.FN+" "+returnData.selItem.MN);
    }
}

function onClickSubmit() {

    if ($("#txtFDate").val() !== "") {
        var txtAppPTDate = $("#txtFDate").data("kendoDatePicker");
        var selectedDate = txtAppPTDate.value();
        selDate = new Date(selectedDate);

        $("#txtStartDate").kendoDateTimePicker({
            change: startChange,
            min: new Date(selDate),
            format: appointmentdtFMT, interval: 15
        });
        $("#txtEndDate").kendoDateTimePicker({ format: appointmentdtFMT, interval: 15, min: new Date() });

        // var selectedDate = GetDateTime("txtDate");


        // for (let i = 1; i <= 7; i++) {
        //     var first = selectedDate.getDate() - (selectedDate.getDay() + 1) + i;
        //     var day = new Date(selectedDate.setDate(first));
        //
        //     allDatesInWeek.push(day);
        // }
        var day = selDate.getDate();
        var month = selDate.getMonth();
        month = month + 1;
        var year = selDate.getFullYear();

        var stDate = month + "/" + day + "/" + year;
        stDate = stDate + " 00:00:00";

        var startDate = new Date(stDate);
        var stDateTime = startDate.getTime();

        var etDate = month + "/" + day + "/" + year;
        etDate = etDate + " 23:59:59";

        var endtDate = new Date(etDate);
        var edDateTime = endtDate.getTime();

        //var startDate = new Date(stDate);
        //var endtDate = new Date(etDate);

        //var day = startDate.getDate() - startDate.getDay();
        //var pDate = new Date(startDate);
        //pDate.setDate(day);

        //endtDate.setDate(day + 6);
        //var stDateTime = pDate.getTime();
        //var edDateTime = endtDate.getTime();
        //var txtAppPTFacility = $("#cmbFacility").val();
        //parentRef.facilityId = txtAppPTFacility;
        //parentRef.patientId = selectPatientId;
        //parentRef.SUName = $("#txtSU").val();
        //parentRef.facilityName = $("#cmbFacility option:selected").text();
        //parentRef.selDate = pDate;
        //parentRef.copyDate = txtAppPTDate.value();

        buildDeviceListGrid([]);

        var patientListURL = ipAddress + "/appointment/list/?to-date=" + edDateTime + "&from-date=" + stDateTime + "&is-active=1";

        getAjaxObject(patientListURL, "GET", onPatientListData, onError);

    }
    else {
        customAlert.error("Error", "Please select date");
    }


}


function onPatientListData(dataObj){
    dtArray = [];
    buildDeviceListGrid([]);
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.appointment) {
            if ($.isArray(dataObj.response.appointment)) {
                dtArray = dataObj.response.appointment;
            } else {
                dtArray.push(dataObj.response.appointment);
            }
        }
    }
    dtArray.sort(function (a, b) {
        var nameA = a.dateOfAppointment; // ignore upper and lowercase
        var nameB = b.dateOfAppointment; // ignore upper and lowercase
        if (nameA < nameB) {
            return -1;
        }
        if (nameA > nameB) {
            return 1;
        }
        // names must be equal
        return 0;
    });

    if(dtArray.length > 0) {
        for (var i = 0; i < dtArray.length; i++) {
            // $("#btnCopy").css("display", "");
            dtArray[i].idk = dtArray[i].id;


            var date = new Date(GetDateTimeEditDay(dtArray[i].dateOfAppointment));
            var day = date.getDay();
            var dow = getWeekDayName(day);

            dtArray[i].dateTimeOfAppointment = dow + " " + GetDateTimeEdit(dtArray[i].dateOfAppointment);
            dtArray[i].name = dtArray[i].composition.patient.lastName + " " + dtArray[i].composition.patient.firstName + " " + dtArray[i].composition.patient.middleName;


            dtArray[i].appointmentReasonDesc = dtArray[i].composition.appointmentReason.desc;
            if(dtArray[i].providerId != 0) {
                dtArray[i].staffName = dtArray[i].composition.provider.lastName + " " + dtArray[i].composition.provider.firstName + " " + (dtArray[i].composition.provider.middleName != null ? dtArray[i].composition.provider.middleName : " ");
            }else{
                dtArray[i].staffName = "";
            }
        }


        buildDeviceListGrid(dtArray);
    }
    else{
        // $("#btnCopy").css("display", "none");
        customAlert.info("Info","No appointments");
    }
}

function onClickCopy(){
    var selGridData = angularUIgridWrapper.getAllRows();
    var selList = [];
    for (var i = 0; i < selGridData.length; i++) {

        var dataRow = selGridData[i].entity;
        dataRow.appointmentType = "Con";
        dataRow.appointmentTypeDesc = "Confirmed";
        dataRow.composition.appointmentType.desc="Confirmed";
        dataRow.composition.appointmentType.id=7;
        dataRow.composition.appointmentType.value="Con";
        selList.push(dataRow);
    }

    // if(selList.length>0){
    sessionStorage.setItem("appselList", JSON.stringify(selList));
    parentRef.appselList = selList;
    var popW = "60%";
    var popH = "80%";
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Copy To Appointments";
    parentRef.selValue = allDatesInWeek;
    devModelWindowWrapper.openPageWindow("../../html/masters/createCopyAppointment.html", profileLbl, popW, popH, true, copyAppointment);
    parent.setPopupWIndowHeaderColor("0bc56f","FFF");
    // }
}
function copyAppointment(dataObj){
    parentRef.selValue= null;
    parentRef.appselList=null;
    onClickSubmit();
}


function getWeekDayName(wk){
    var wn = "";
    if(wk == 1){
        return "Mon";
    }else if(wk == 2){
        return "Tue";
    }else if(wk == 3){
        return "Wed";
    }else if(wk == 4){
        return "Thu";
    }else if(wk == 5){
        return "Fri";
    }else if(wk == 6){
        return "Sat";
    }else if(wk == 0){
        return "Sun";
    }
    return wn;
}

function startChange() {
    var txtStartTime = $("#txtStartDate").data("kendoDateTimePicker");
    var txtEndTime = $("#txtEndDate").data("kendoDateTimePicker");
    var startTime = txtStartTime.value();



    if (startTime) {
        var sTime = new Date(startTime);
        var tTime = sTime.getTime();
        tTime = tTime + (15 * 60 * 1000);
        var strTime = new Date(tTime);
        txtEndTime.min(strTime);
        txtEndTime.value(strTime);
        // txtEndTime.max(today1);
        //end.value(startTime);
    }

}

function callMastersData() {
    getAjaxObject(ipAddress + "/master/appointment_type/list/?is-active=1", "GET", getAppointmentList, onError);
    getAjaxObject(ipAddress + "/master/appointment_reason/list/?is-active=1", "GET", getReasonList, onError);
    //getAjaxObject(ipAddress + "/carehome/patient-billing/?is-active=1&patientId=" + selectPatientId + "&fields=*,shift.*,travelMode.*,billingType.*,billingPeriod.*,billTo.name,billingRateType.code", "GET", getPatientData, onError);
}

function getAppointmentList(dataObj) {
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.codeTable) {
        if ($.isArray(dataObj.response.codeTable)) {
            dataArray = dataObj.response.codeTable;
        } else {
            dataArray.push(dataObj.response.codeTable);
        }
    }

    for (var i = 0; i < dataArray.length; i++) {
        if (dataArray[i].isActive == 1) {
            $("#cmbStatus").append('<option value="' + dataArray[i].value + '">' + dataArray[i].desc + '</option>');
        }
    }
    $("#cmbStatus").val("Con");
}

function getReasonList(dataObj) {
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.codeTable) {
        if ($.isArray(dataObj.response.codeTable)) {
            dataArray = dataObj.response.codeTable;
        } else {
            dataArray.push(dataObj.response.codeTable);
        }
    }

    for (var i = 0; i < dataArray.length; i++) {
        if (dataArray[i].isActive == 1) {
            $("#cmbReason").append('<option value="' + dataArray[i].value + '">' + dataArray[i].desc + '</option>');
        }
    }
}

function getPatientData(dataObj) {
    patientBillArray = [];

    var tempCompType = [];
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.patientBilling) {
            if ($.isArray(dataObj.response.patientBilling)) {
                tempCompType = dataObj.response.patientBilling;
            } else {
                tempCompType.push(dataObj.response.patientBilling);
            }
        }
    }
    for (var q = 0; q < tempCompType.length; q++) {
        var qItem = tempCompType[q];
        if (qItem) {
            qItem.value = qItem.billToname;
            qItem.text = qItem.billToname;
            patientBillArray.push(qItem);
            $("#cmbBilling").append('<option value="' + qItem.value + '">' + qItem.value + '</option>');
        }
    }


    $("#cmbBilling").val(billToName);


}

function appointmentSave() {
    buildDeviceListGrid([]);

    if (validation()) {
        if (!($("#txtStaff").val() != "")) {
            var msg = "You have not selected any staff still you want to add?";
            displayConfirmSessionErrorPopUp("Info", msg, function (res) {
                onClickappoinmentsave(0);
            });
        } else {
            onClickappoinmentsave(1);
        }

    }
    else {
        customAlert.error("Error", "Please select required fields");
    }

}

function validation() {
    var IsFlag = true;
    var txtStartDate = $("#txtStartDate").data("kendoDateTimePicker");
    var txtEndDate = $("#txtEndDate").data("kendoDateTimePicker");

    // if (!(txtStartDate.value() != null)) {
    //     IsFlag = false;
    // }
    // else if (!(txtEndDate.value() != null)) {
    //     IsFlag = false;
    // }
    if (!($("#cmbReason").val() != "")) {
        IsFlag = false;
    }
    else if (!($("#cmbStatus").val() != "")) {
        IsFlag = false;
    }
    else if (!($("#cmbBilling").val() != "")) {
        IsFlag = false;
    }
    else if (!($("#txtPayout").val() != "")) {
        IsFlag = false;
    }
    return IsFlag;
}

function onClickappoinmentsave(IsProvider){
    var txtStartDate = $("#txtStartDate").data("kendoDateTimePicker");
    var txtEndDate = $("#txtEndDate").data("kendoDateTimePicker");
    var sDate = new Date(txtStartDate.value());
    var eDate = new Date(txtEndDate.value());
    var dataList;
    if(sDate.getTime() > eDate.getTime()){
        customAlert.error("Error","End date should be greater than the start date");
    }else {
        if (appointmentId != null) {
            var obj = {};
            obj.createdBy = apptselectedItems.userId;
            obj.modifiedBy = Number(sessionStorage.userId);
            obj.isActive = 1;
            obj.id = appointmentId;
            obj.isDeleted = 0;
            // var startDate = $("#txtStartDate").data("kendoDateTimePicker");
            // var endDate = $("#txtEndDate").data("kendoDateTimePicker");
            // var sDate = new Date(startDate.value());
            // var eDate = new Date(endDate.value());
            // var diff = eDate.getTime() - sDate.getTime();
            // diff = diff / 1000;
            // diff = diff / 60;
            // var nSec = sDate.getTime();
            // var uSec = getGMTDateFromLocaleDate(nSec);

            obj.dateOfAppointment = apptselectedItems.dateOfAppointment;
            // obj.dateTimeOfAppointment = nSec;
            obj.duration = apptselectedItems.duration;
            // if(IsProvider == 0){
            //     obj.providerId = 0;
            // }else{
            obj.providerId = Number(selectProviderId);
            // }
            obj.facilityId = apptselectedItems.facilityId;
            obj.patientId = Number(selectPatientId);
            obj.appointmentType = $("#cmbStatus").val();
            obj.notes = apptselectedItems.notes;
            var selSUItem = onGetSUBillingDetails($("#cmbBilling").val());
            obj.shiftValue = selSUItem.shiftValue;
            obj.billingRateType = selSUItem.billingRateTypeCode;
            obj.patientBillingRate = selSUItem.rate;
            obj.billingRateTypeId = selSUItem.billingRateTypeId;
            obj.swiftBillingId = selSUItem.swiftBillingId;
            obj.payoutHourlyRate = $("#txtPayout").val();
            obj.billToName = $("#cmbBilling option:selected").text();

            obj.billToId = selSUItem.billToId;
            obj.appointmentReason = $("#cmbReason").val();
            // obj.appointmentReasonDesc = $("#cmbReason option:selected").text();
            // obj.appointmentTypeDesc = $("#cmbStatus option:selected").text();

            var arr = [];
            arr.push(obj);

            var dataUrl = ipAddress+"/appointment/update";
            createAjaxObject(dataUrl,arr,"POST",onAvlAppCreate,onError);

            // var msg = "Updated successfully";
            // displaySessionErrorPopUp("Info", msg, function (res) {
            //     $(".filter-heading").html("View Copy Appointments");
            //     $("#viewDivBlock").show();
            //     $("#addPopup").hide();
            // });

        }
    }
}

function onAvlAppCreate(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            var msg = "Appointment Updated Successfully";
            displaySessionErrorPopUp("Info", msg, function (res) {
                $(".filter-heading").html("View Copy Appointments");
                $("#viewDivBlock").show();
                $("#divEditAppointment").hide();
                onClickSubmit();
            });
        }else{
            customAlert.error("Error",dataObj.response.status.message);
        }
    }else{
        customAlert.error("Error",dataObj.response.status.message);
    }
}

function onGetSUBillingDetails(name){
    for (var i=0;patientBillArray.length >= 0;i++){
        if(patientBillArray[i].billToname.toLowerCase() == name.toLowerCase()){
            return patientBillArray[i];
        }
    }

}
function onClickStaff(){
    var popW = 800;
    var popH = 450;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Staff";

    devModelWindowWrapper.openPageWindow("../../html/masters/staffList.html", profileLbl, popW, popH, true, onCloseStaffAction);
}

function onCloseStaffAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        selectProviderId = returnData.selItem.PID;
        $("#txtStaff").val(returnData.selItem.lastName + ' '+ returnData.selItem.firstName+' '+ (returnData.selItem.middleName != null ? returnData.selItem.middleName : " "));
    }
}
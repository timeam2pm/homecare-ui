var operation = "";
var ADD = "add";
var UPDATE = "update";
var VIEW = "view";
var DELETE = "delete";

var parentRef = null;

$(document).ready(function(){
    parentRef = parent.frames['iframe'].window;
});

$(window).load(function(){
    var pnlHeight = window.innerHeight;
    pnlHeight = pnlHeight-30;
    $("#leftPanel").height(pnlHeight);
    $("#centerPanel").height(pnlHeight);
    $("#rightPanel").height(pnlHeight);
    $("#leftButtonBar").height(pnlHeight);
    init()
});

function init(){
    buttonEvents();
}
function buttonEvents(){
    $("#panelMovLeft").off("click",onClickLeftButton);
    $("#panelMovLeft").on("click",onClickLeftButton);

    $("#panelMoveRight").off("click",onClickRightButton);
    $("#panelMoveRight").on("click",onClickRightButton);

    $("#addPt").off("click",onClickAddPatient);
    $("#addPt").on("click",onClickAddPatient);

//    $("#searchPt").off("click",onClickSearchPatient);
    // $("#searchPt").on("click",onClickSearchPatient);
    $("#addAccount").off("click",onClickAddAccount);
    $("#addAccount").on("click",onClickAddAccount);

    $("#searchAccount").off("click",onClickSearchAccount);
    $("#searchAccount").on("click",onClickSearchAccount);

    $("#addEmployer").off("click",onClickAddEmployer);
    $("#addEmployer").on("click",onClickAddEmployer);

    $("#searchEmployer").off("click",onClickSearchEmployer);
    $("#searchEmployer").on("click",onClickSearchEmployer);

    $("#addCountry").off("click",onClickAddCountry);
    $("#addCountry").on("click",onClickAddCountry);

    $("#searchCountry").off("click",onClickSearchCountry);
    $("#searchCountry").on("click",onClickSearchCountry);

    $("#addDoctor").off("click",onClickAddDoctor);
    $("#addDoctor").on("click",onClickAddDoctor);

    $("#searchDoctor").off("click",onClickSearchDoctor);
    $("#searchDoctor").on("click",onClickSearchDoctor);



    $("#addCounty").off("click",onClickAddCounty);
    $("#addCounty").on("click",onClickAddCounty);

    $("#searchCounty").off("click",onClickSearchCounty);
    $("#searchCounty").on("click",onClickSearchCounty);

    $("#addState").off("click",onClickAddState);
    $("#addState").on("click",onClickAddState);

    $("#searchState").off("click",onClickSearchState);
    $("#searchState").on("click",onClickSearchState);


    $("#addZip").off("click",onClickAddZip);
    $("#addZip").on("click",onClickAddZip);

    $("#searchZip").off("click",onClickSearchZip);
    $("#searchZip").on("click",onClickSearchZip);

    $("#createTable").off("click",onClickCreateTable);
    $("#createTable").on("click",onClickCreateTable);

    $("#addValues").off("click",onClickAddValues);
    $("#addValues").on("click",onClickAddValues);

    $("#searchValues").off("click",onClickSearchValues);
    $("#searchValues").on("click",onClickSearchValues);

    $("#searchTable").off("click",onClickSearchCodeTable);
    $("#searchTable").on("click",onClickSearchCodeTable);

    $("#addResource").off("click");
    $("#addResource").on("click",onClickFileResource);

    $("#searchResource").off("click");
    $("#searchResource").on("click",onClickSearchDiet);

    $("#addUser").off("click");
    $("#addUser").on("click",onClickAddUser);

    $("#addUserRoles").off("click");
    $("#addUserRoles").on("click",onClickUserRoles);

    $("#addUserPatientMap").off("click");
    $("#addUserPatientMap").on("click",onClickUserPatientMap);

    $("#addAdmin").off("click");
    $("#addAdmin").on("click",onClickAdmin);

    $("#addScreenPermi").off("click");
    $("#addScreenPermi").on("click",onClickScreenPermissions);

    $("#addProvider").off("click",onClickAddProvider);
    $("#addProvider").on("click",onClickAddProvider);

    $("#searchProvider").off("click",onClickSearchProvider);
    $("#searchProvider").on("click",onClickSearchProvider);

    $("#addFacility").off("click",onClickAddFacility);
    $("#addFacility").on("click",onClickAddFacility);

    $("#searchFacility").off("click",onClickSearchFacility);
    $("#searchFacility").on("click",onClickSearchFacility);

    /* $("#addForm").off("click");
     $("#addForm").on("click",onClickForm);

     $("#addStrength").off("click");
     $("#addStrength").on("click",onClickStrength);

     $("#addQuantity").off("click");
     $("#addQuantity").on("click",onClickQuantity);

     $("#addRoute").off("click");
     $("#addRoute").on("click",onClickRoute);

     $("#addInstructions").off("click");
     $("#addInstructions").on("click",onClickInstructions);

     $("#addSchedule").off("click");
     $("#addSchedule").on("click",onClickSchedule);*/

    $("#addmedication").off("click");
    $("#addmedication").on("click",onClickMedication);

}

function adjustHeight(){
}

function onClickLeftButton(){
    console.log("left");
    if($("#leftPanel").is(":visible")){
        $("#leftPanel").hide();
        $("#centerPanel").removeClass("col-sm-7 col-md-7 col-lg-7 col-xs-12 panelBorderStyle");
        $("#centerPanel").addClass("col-sm-9 col-md-9 col-lg-9 col-xs-12 panelBorderStyle");
    }else{
        $("#leftPanel").show();
        $("#centerPanel").removeClass("col-sm-9 col-md-9 col-lg-9 col-xs-12 panelBorderStyle");
        $("#centerPanel").addClass("col-sm-7 col-md-7 col-lg-7 col-xs-12 panelBorderStyle");
    }
}

function onClickRightButton(){
    console.log("right");
    if($("#rightPanel").is(":visible")){
        $("#rightPanel").hide();
        $("#centerPanel").removeClass("col-sm-7 col-md-7 col-lg-7 col-xs-12 panelBorderStyle");
        $("#centerPanel").addClass("col-sm-10 col-md-10 col-lg-10 col-xs-12 panelBorderStyle");
    }else{
        $("#rightPanel").show();
        $("#centerPanel").removeClass("col-sm-10 col-md-10 col-lg-10 col-xs-12 panelBorderStyle");
        $("#centerPanel").addClass("col-sm-7 col-md-7 col-lg-7 col-xs-12 panelBorderStyle");
    }
}

function onClickAddPatient(){
    console.log("Add");
    $("#centerPanel").css("visibility","visible");
    //$(html).show();
}
/*function onClickAddAccount(){
	operation = ADD;
	addAccount();
}
function addAccount(){
	showPatientPanel();
	var popW = 600;
    var popH = 500;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    if(operation == ADD){
    	 profileLbl = "Add Account";
    }else{
    	 profileLbl = "Edit Account";
    }

    parentRef.operation = operation;
    parentRef.selItem = selAccountItem;

    devModelWindowWrapper.openPageWindow("../../html/masters/createAccount.html", profileLbl, popW, popH, true, closeAddAccountAction);
}
function selAccountItem(){

}
function closeAddAccountAction(evt,returnData){
	if(returnData && returnData.status == "success"){
		if(returnData.operation == ADD){
			customAlert.info("Info","Account Created Successfully.")
		}else if(returnData.operation == UPDATE){
			customAlert.info("Info","Account Updated Successfully.")
		}else if(returnData.operation == DELETE){
			customAlert.info("Info","Account deleted Successfully.")
		}
	}else if(returnData && returnData.status == "search"){
		onClickSearchAccount();
	}
}

function onClickSearchAccount(){
	showPatientPanel();
	var popW = 600;
    var popH = 500;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search Account";
    devModelWindowWrapper.openPageWindow("../../html/masters/accountList.html", profileLbl, popW, popH, true, onCloseSearchAccountAction);
}
var selAccountItem = null;
function onCloseSearchAccountAction(evt,returnData){
	if(returnData && returnData.status == "success"){
		operation = UPDATE;
		selAccountItem = returnData.selItem;
		addAccount();
	}else if(returnData && returnData.status == "Add"){
		operation = ADD;
		addAccount();
	}
}*/

function onClickMedication(){
    operation = ADD;
    addmedication();
}

function addmedication(){
    showPatientPanel();
    var popW = 600;
    var popH = 400;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();

    if(operation == ADD){
        profileLbl = "Add Medication";
    }else{
        profileLbl = "Edit Medication";
    }


    parentRef.operation = operation;
    parentRef.selItem = selMedicationItem;

    devModelWindowWrapper.openPageWindow("../../html/masters/createForm.html", profileLbl, popW, popH, true, closeAddMedicationAction);
}
function selMedicationItem(){

}
function closeAddMedicationAction(evt,returnData){
    /*if(returnData && returnData.status == "success"){
        if(returnData.operation == ADD){
            customAlert.info("Info","Account Created Successfully.")
        }else if(returnData.operation == UPDATE){
            customAlert.info("Info","Account Updated Successfully.")
        }else if(returnData.operation == DELETE){
            customAlert.info("Info","Account deleted Successfully.")
        }
    }else if(returnData && returnData.status == "search"){
        onClickSearchAccount();
    }*/
}
/*function onClickForm(){
	operation = ADD;
	addForm();
}

function addForm(){
	showPatientPanel();
	var popW = 900;
    var popH = 900;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();

    profileLbl = "Add Form";


    parentRef.operation = operation;
    parentRef.selItem = selFormItem;

    devModelWindowWrapper.openPageWindow("../../html/masters/createForm.html", profileLbl, popW, popH, true, closeAddFormAction);
}
function selFormItem(){

}
function closeAddFormAction(evt,returnData){
	/*if(returnData && returnData.status == "success"){
		if(returnData.operation == ADD){
			customAlert.info("Info","Account Created Successfully.")
		}else if(returnData.operation == UPDATE){
			customAlert.info("Info","Account Updated Successfully.")
		}else if(returnData.operation == DELETE){
			customAlert.info("Info","Account deleted Successfully.")
		}
	}else if(returnData && returnData.status == "search"){
		onClickSearchAccount();
	}
}*/
/*function onClickStrength(){
	operation = ADD;
	addStrength();
}

function addStrength(){
	showPatientPanel();
	var popW = 600;
    var popH = 500;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();

    profileLbl = "Add Strength";


    parentRef.operation = operation;
    parentRef.selItem = selStrengthItem;

    devModelWindowWrapper.openPageWindow("../../html/masters/createStrength.html", profileLbl, popW, popH, true, closeAddStrengthAction);
}
function selStrengthItem(){

}
function closeAddStrengthAction(evt,returnData){
	/*if(returnData && returnData.status == "success"){
		if(returnData.operation == ADD){
			customAlert.info("Info","Account Created Successfully.")
		}else if(returnData.operation == UPDATE){
			customAlert.info("Info","Account Updated Successfully.")
		}else if(returnData.operation == DELETE){
			customAlert.info("Info","Account deleted Successfully.")
		}
	}else if(returnData && returnData.status == "search"){
		onClickSearchAccount();
	}
}
function onClickQuantity(){
	operation = ADD;
	addQuantity();
}

function addQuantity(){
	showPatientPanel();
	var popW = 600;
    var popH = 500;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();

    profileLbl = "Add Quantity";


    parentRef.operation = operation;
    parentRef.selItem = selQuantityItem;

    devModelWindowWrapper.openPageWindow("../../html/masters/createQuantity.html", profileLbl, popW, popH, true, closeAddQuantityAction);
}
function selQuantityItem(){

}
function closeAddQuantityAction(evt,returnData){
	/*if(returnData && returnData.status == "success"){
		if(returnData.operation == ADD){
			customAlert.info("Info","Account Created Successfully.")
		}else if(returnData.operation == UPDATE){
			customAlert.info("Info","Account Updated Successfully.")
		}else if(returnData.operation == DELETE){
			customAlert.info("Info","Account deleted Successfully.")
		}
	}else if(returnData && returnData.status == "search"){
		onClickSearchAccount();
	}
}
function onClickRoute(){
	operation = ADD;
	addRoute();
}

function addRoute(){
	showPatientPanel();
	var popW = 600;
    var popH = 500;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();

    profileLbl = "Add Route";


    parentRef.operation = operation;
    parentRef.selItem = selRouteItem;

    devModelWindowWrapper.openPageWindow("../../html/masters/createRoute.html", profileLbl, popW, popH, true, closeAddRouteAction);
}
function selRouteItem(){

}
function closeAddRouteAction(evt,returnData){
	/*if(returnData && returnData.status == "success"){
		if(returnData.operation == ADD){
			customAlert.info("Info","Account Created Successfully.")
		}else if(returnData.operation == UPDATE){
			customAlert.info("Info","Account Updated Successfully.")
		}else if(returnData.operation == DELETE){
			customAlert.info("Info","Account deleted Successfully.")
		}
	}else if(returnData && returnData.status == "search"){
		onClickSearchAccount();
	}
}
function onClickInstructions(){
	operation = ADD;
	addInstructions();
}

function addInstructions(){
	showPatientPanel();
	var popW = 600;
    var popH = 500;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();

    profileLbl = "Add Instruction";


    parentRef.operation = operation;
    parentRef.selItem = selInstructionItem;

    devModelWindowWrapper.openPageWindow("../../html/masters/createInstructions.html", profileLbl, popW, popH, true, closeAddInstructionAction);
}
function selInstructionItem(){

}
function closeAddInstructionAction(evt,returnData){
	/*if(returnData && returnData.status == "success"){
		if(returnData.operation == ADD){
			customAlert.info("Info","Account Created Successfully.")
		}else if(returnData.operation == UPDATE){
			customAlert.info("Info","Account Updated Successfully.")
		}else if(returnData.operation == DELETE){
			customAlert.info("Info","Account deleted Successfully.")
		}
	}else if(returnData && returnData.status == "search"){
		onClickSearchAccount();
	}
}
function onClickSchedule(){
	operation = ADD;
	addSchedule();
}

function addSchedule(){
	showPatientPanel();
	var popW = 600;
    var popH = 500;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();

    profileLbl = "Add Schedule";


    parentRef.operation = operation;
    parentRef.selItem = selScheduleItem;

    devModelWindowWrapper.openPageWindow("../../html/masters/createSchedule.html", profileLbl, popW, popH, true, closeAddScheduleAction);
}
function selScheduleItem(){

}
function closeAddScheduleAction(evt,returnData){
	/*if(returnData && returnData.status == "success"){
		if(returnData.operation == ADD){
			customAlert.info("Info","Account Created Successfully.")
		}else if(returnData.operation == UPDATE){
			customAlert.info("Info","Account Updated Successfully.")
		}else if(returnData.operation == DELETE){
			customAlert.info("Info","Account deleted Successfully.")
		}
	}else if(returnData && returnData.status == "search"){
		onClickSearchAccount();
	}
}*/
function onClickAddAccount(){
    operation = ADD;
    selAccountItem = null;
    addAccount();
}
function addAccount(){
    showPatientPanel();
    var popW = 850;
    var popH = 600;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    if(operation == ADD){
        profileLbl = "Add Billing Account";
    }else{
        profileLbl = "Edit Billing Account";
    }

    parentRef.operation = operation;
    parentRef.selItem = selAccountItem;

    devModelWindowWrapper.openPageWindow("../../html/masters/createAccount.html", profileLbl, popW, popH, true, closeAddAccountAction);
}
function selAccountItem(){

}
function closeAddAccountAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        if(returnData.operation == ADD){
            customAlert.info("Info","Account Created Successfully.")
        }else if(returnData.operation == UPDATE){
            customAlert.info("Info","Account Updated Successfully.")
        }else if(returnData.operation == DELETE){
            customAlert.info("Info","Account deleted Successfully.")
        }
    }else if(returnData && returnData.status == "search"){
        setTimeout(function(){
            onClickSearchAccount();
        },100)

    }
}

function onClickSearchAccount(){
    showPatientPanel();
    var popW = 800;
    var popH = 500;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search Billing Account";
    devModelWindowWrapper.openPageWindow("../../html/masters/accountList.html", profileLbl, popW, popH, true, onCloseSearchAccountAction);
}
var selAccountItem = null;
function onCloseSearchAccountAction(evt,returnData){
    var st = false;
    if(returnData && returnData.status == "success"){
        st = true;
        operation = UPDATE;
        selAccountItem = returnData.selItem;
        //addAccount();
    }else if(returnData && returnData.status == "Add"){
        st = true;
        operation = ADD;
        selAccountItem = null;
        //addAccount();
    }

    if((operation == UPDATE || operation == ADD) && st){
        setTimeout(function(){
            addAccount();
        },1000);
    }
}

function onClickAddFacility(){
    operation = ADD;
    addFacility();
}
function addFacility(){
    showPatientPanel();
    var popW = 850;
    var popH = 550;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    if(operation == ADD){
        profileLbl = "Add Facility";
    }else{
        profileLbl = "Edit Facility";
    }

    parentRef.operation = operation;
    parentRef.selItem = selAccountItem;

    devModelWindowWrapper.openPageWindow("../../html/masters/createFacility.html", profileLbl, popW, popH, true, closeAddFacilitytAction);
}
function selAccountItem(){

}
function closeAddFacilitytAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        if(returnData.operation == ADD){
            customAlert.info("Info","Facility Created Successfully.")
        }else if(returnData.operation == UPDATE){
            customAlert.info("Info","Facility Updated Successfully.")
        }else if(returnData.operation == DELETE){
            customAlert.info("Info","Facility deleted Successfully.")
        }
    }else if(returnData && returnData.status == "search"){
        onClickSearchFacility();
    }
}

function onClickSearchFacility(){
    showPatientPanel();
    var popW = 850;
    var popH = 500;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search Facility";
    devModelWindowWrapper.openPageWindow("../../html/masters/facilityList.html", profileLbl, popW, popH, true, onCloseSearchFacilityAction);
}
var selAccountItem = null;
function onCloseSearchFacilityAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        operation = UPDATE;
        selAccountItem = returnData.selItem;
        addFacility();
    }else if(returnData && returnData.status == "Add"){
        operation = ADD;
        addFacility();
    }
}

function onClickAddProvider(){
    operation = ADD;
    addProvider();
}
function addProvider(){
    showPatientPanel();
    var popW = 700;
    var popH = 500;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    if(operation == ADD){
        profileLbl = "Add Provider";
    }else{
        profileLbl = "Edit Provider";
    }

    parentRef.operation = operation;
    parentRef.selItem = selProviderItem;

    devModelWindowWrapper.openPageWindow("../../html/masters/createProvider.html", profileLbl, popW, popH, true, closeAddProviderAction);
}
function selProviderItem(){

}
function closeAddProviderAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        if(returnData.operation == ADD){
            customAlert.info("Info","Provider Created Successfully.")
        }else if(returnData.operation == UPDATE){
            customAlert.info("Info","Provider Updated Successfully.")
        }else if(returnData.operation == DELETE){
            customAlert.info("Info","Provider deleted Successfully.")
        }
    }else if(returnData && returnData.status == "search"){
        onClickSearchProvider();
    }
}

function onClickSearchProvider(){
    showPatientPanel();
    var popW = 600;
    var popH = 500;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search Provider";
    devModelWindowWrapper.openPageWindow("../../html/masters/providerList.html", profileLbl, popW, popH, true, onCloseSearchProviderAction);
}
var selProviderItem = null;
function  onCloseSearchProviderAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        operation = UPDATE;
        selProviderItem = returnData.selItem;
        addProvider();
    }else if(returnData && returnData.status == "Add"){
        operation = ADD;
        addProvider();
    }
}
function onClickAddDoctor(){
    operation = ADD;
    addDoctor();
}
function addDoctor(){
    showPatientPanel();
    var popW = 850;
    var popH = 550;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    if(operation == ADD){
        profileLbl = "Add Doctor";
    }else{
        profileLbl = "Edit Doctor";
    }

    parentRef.operation = operation;
    parentRef.selItem = selAccountItem;

    devModelWindowWrapper.openPageWindow("../../html/masters/createDoctor.html", profileLbl, popW, popH, true, closeAddDoctortAction);
}
function selAccountItem(){

}
function closeAddDoctortAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        if(returnData.operation == ADD){
            customAlert.info("Info","Doctor Created Successfully.")
        }else if(returnData.operation == UPDATE){
            customAlert.info("Info","Doctor Updated Successfully.")
        }else if(returnData.operation == DELETE){
            customAlert.info("Info","Doctor deleted Successfully.")
        }
    }else if(returnData && returnData.status == "search"){
        onClickSearchDoctor();
    }
}

function onClickSearchDoctor(){
    showPatientPanel();
    var popW = 850;
    var popH = 500;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search  Doctor";
    devModelWindowWrapper.openPageWindow("../../html/masters/doctorList.html", profileLbl, popW, popH, true, onCloseSearchDoctorAction);
}
var selAccountItem = null;
function onCloseSearchDoctorAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        operation = UPDATE;
        selAccountItem = returnData.selItem;
        addDoctor();
    }else if(returnData && returnData.status == "Add"){
        operation = ADD;
        addDoctor();
    }
}
function onClickAddCountry(){
    operation = ADD;
    addCountry();
}
function addCountry(){
    showPatientPanel();
    var popW = 600;
    var popH = 500;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    if(operation == ADD){
        profileLbl = "Add Country";
    }else{
        profileLbl = "Edit Country";
    }

    parentRef.operation = operation;
    parentRef.selItem = selCountryItem;

    devModelWindowWrapper.openPageWindow("../../html/masters/createCountry.html", profileLbl, popW, popH, true, closeAddAction);
}
function closeAddAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        if(returnData.operation == ADD){
            customAlert.info("Info","Country Created Successfully.")
        }else if(returnData.operation == UPDATE){
            customAlert.info("Info","Country Updated Successfully.")
        }else if(returnData.operation == DELETE){
            customAlert.info("Info","Country deleted Successfully.")
        }
    }else if(returnData && returnData.status == "search"){
        onClickSearchCountry();
    }
}
function onClickSearchCountry(){
    showPatientPanel();
    var popW = 600;
    var popH = 500;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search Country";
    devModelWindowWrapper.openPageWindow("../../html/masters/countryList.html", profileLbl, popW, popH, true, onCloseSearchCountryAction);
}
var selCountryItem = null;
function onCloseSearchCountryAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        operation = UPDATE;
        selCountryItem = returnData.selItem;
        addCountry();
    }else if(returnData && returnData.status == "Add"){
        operation = ADD;
        addCountry();
    }
}
function onClickAddEmployer(){
    operation = ADD;
    addEmployer();
}
function addEmployer(){
    showPatientPanel();
    var popW = 600;
    var popH = 500;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    if(operation == ADD){
        profileLbl = "Add Employer";
    }else{
        profileLbl = "Edit Employer";
    }

    parentRef.operation = operation;
    parentRef.selItem = selEmployerItem;

    devModelWindowWrapper.openPageWindow("../../html/masters/createEmployer.html", profileLbl, popW, popH, true, closeAddEmployerAction);
}
function closeAddEmployerAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        if(returnData.operation == ADD){
            customAlert.info("Info","Employer Created Successfully.")
        }else if(returnData.operation == UPDATE){
            customAlert.info("Info","Employer Updated Successfully.")
        }else if(returnData.operation == DELETE){
            customAlert.info("Info","Employer deleted Successfully.")
        }
    }else if(returnData && returnData.status == "search"){
        onClickSearchEmployer();
    }
}
function onClickSearchEmployer(){
    showPatientPanel();
    var popW = 600;
    var popH = 500;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search Employer";
    devModelWindowWrapper.openPageWindow("../../html/masters/employerList.html", profileLbl, popW, popH, true, onCloseSearchEmployerAction);
}
var selEmployerItem = null;
function onCloseSearchEmployerAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        operation = UPDATE;
        selCountryItem = returnData.selItem;
        addEmployer();
    }else if(returnData && returnData.status == "Add"){
        operation = ADD;
        addEmployer();
    }
}
function onClickAddCounty(){
    operation = ADD;
    addCounty();
}
function addCounty(){
    showPatientPanel();
    var popW = 600;
    var popH = 500;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    if(operation == ADD){
        profileLbl = "Add County";
    }else{
        profileLbl = "Edit County";
    }

    parentRef.operation = operation;
    parentRef.selItem = selCountyItem;

    devModelWindowWrapper.openPageWindow("../../html/masters/createCounty.html", profileLbl, popW, popH, true, closeAddCountyAction);
}
function closeAddCountyAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        if(returnData.operation == ADD){
            customAlert.info("Info","County Created Successfully.")
        }else if(returnData.operation == UPDATE){
            customAlert.info("Info","County Updated Successfully.")
        }else if(returnData.operation == DELETE){
            customAlert.info("Info","County deleted Successfully.")
        }
    }else if(returnData && returnData.status == "search"){
        onClickSearchCounty();
    }
}

function onClickSearchCounty(){
    showPatientPanel();
    var popW = 600;
    var popH = 500;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search County";
    devModelWindowWrapper.openPageWindow("../../html/masters/countyList.html", profileLbl, popW, popH, true, onCloseSearchCountyAction);
}
var selCountyItem = null;
function onCloseSearchCountyAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        operation = UPDATE;
        selCountyItem = returnData.selItem;
        addCounty();
    }else if(returnData && returnData.status == "Add"){
        operation = ADD;
        addCounty();
    }
}

function onClickAddState(){
    operation = ADD;
    addState();
}
var selStateItem = null;
function addState(){
    showPatientPanel();
    var popW = 600;
    var popH = 500;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    if(operation == ADD){
        profileLbl = "Add State";
    }else{
        profileLbl = "Edit State";
    }

    parentRef.operation = operation;
    parentRef.selItem = selStateItem;

    devModelWindowWrapper.openPageWindow("../../html/masters/createState.html", profileLbl, popW, popH, true, closeAddStateAction);
}
function closeAddStateAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        if(returnData.operation == ADD){
            customAlert.info("Info","State Created Successfully.")
        }else if(returnData.operation == UPDATE){
            customAlert.info("Info","State Updated Successfully.")
        }else if(returnData.operation == DELETE){
            customAlert.info("Info","State deleted Successfully.")
        }
    }else if(returnData && returnData.status == "search"){
        onClickSearchState();
    }
}
function onClickSearchState(){
    showPatientPanel();
    var popW = 600;
    var popH = 500;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search State";
    devModelWindowWrapper.openPageWindow("../../html/masters/stateList.html", profileLbl, popW, popH, true, onCloseSearchStateAction);
}
function onCloseSearchStateAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        operation = UPDATE;
        selStateItem= returnData.selItem;
        addState();
    }else if(returnData && returnData.status == "Add"){
        operation = ADD;
        addState();
    }
}
function onClickSearchZip(){
    showPatientPanel();
    var popW = 600;
    var popH = 500;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search Zip";
    devModelWindowWrapper.openPageWindow("../../html/masters/zipList.html", profileLbl, popW, popH, true, onCloseSearchZipAction);
}
function onCloseSearchZipAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        operation = UPDATE;
        selZipItem= returnData.selItem;
        addZip();
    }else if(returnData && returnData.status == "Add"){
        operation = ADD;
        addZip();
    }
}
function onClickAddZip(){
    operation = ADD;
    addZip();
}
var selZipItem = null;
function addZip(){
    showPatientPanel();
    var popW = 600;
    var popH = 500;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    if(operation == ADD){
        profileLbl = "Add Zip";
    }else{
        profileLbl = "Edit Zip";
    }

    parentRef.operation = operation;
    parentRef.selItem = selZipItem;

    devModelWindowWrapper.openPageWindow("../../html/masters/createZip.html", profileLbl, popW, popH, true, closeAddZipAction);
}
function closeAddZipAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        if(returnData.operation == ADD){
            customAlert.info("Info","Zip Created Successfully.")
        }else if(returnData.operation == UPDATE){
            customAlert.info("Info","Zip Updated Successfully.")
        }else if(returnData.operation == DELETE){
            customAlert.info("Info","Zip deleted Successfully.")
        }
    }else if(returnData && returnData.status == "search"){
        onClickSearchZip();
    }
}
var selCodeTableItem = null;
function onClickCreateTable(){
    operation = ADD;
    addCreateTable();
}
function addCreateTable(){
    showPatientPanel();
    var popW = 600;
    var popH = 500;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    if(operation == ADD){
        profileLbl = "Create Table";
    }else{
        profileLbl = "Edit Table";
    }

    parentRef.operation = operation;
    parentRef.selItem = selCodeTableItem;

    devModelWindowWrapper.openPageWindow("../../html/masters/createCodeTable.html", profileLbl, popW, popH, true, closeAddCreateTableAction);
}
function closeAddCreateTableAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        if(returnData.operation == ADD){
            customAlert.info("Info","Code Table Created Successfully.")
        }else if(returnData.operation == UPDATE){
            customAlert.info("Info","Code Table Updated Successfully.")
        }else if(returnData.operation == DELETE){
            customAlert.info("Info","Code Table deleted Successfully.")
        }
    }else if(returnData && returnData.status == "search"){
        onClickSearchCodeTable();
    }
}
function onClickSearchCodeTable(){
    showPatientPanel();
    var popW = 600;
    var popH = 500;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search Table";
    devModelWindowWrapper.openPageWindow("../../html/masters/codeTableList.html", profileLbl, popW, popH, true, onCloseSearchCodeTableAction);
}
function onCloseSearchCodeTableAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        operation = UPDATE;
        selCodeTableItem= returnData.selItem;
        addCreateTable();
    }else if(returnData && returnData.status == "Add"){
        operation = ADD;
        addCreateTable();
    }
}
function onClickAddValues(){
    operation = ADD;
    addValuesToTable();
}
function addValuesToTable(){
    showPatientPanel();
    var popW = 620;
    var popH = 500;

    parentRef.operation = operation;
    parentRef.selItem = selCodeTableDataItem;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Code Table Values";
    devModelWindowWrapper.openPageWindow("../../html/masters/createTableValue.html", profileLbl, popW, popH, true, onCloseCodeTableValueAction);
}
function onCloseCodeTableValueAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        if(returnData.operation == ADD){
            customAlert.info("Info","Code Table Values are Created Successfully.")
        }else if(returnData.operation == UPDATE){
            customAlert.info("Info","Code Table values are Updated Successfully.")
        }else if(returnData.operation == DELETE){
            customAlert.info("Info","Code Table values are deleted Successfully.")
        }
    }else if(returnData && returnData.status == "search"){
        onClickSearchValues();
    }
}
function onClickSearchValues(){
    showPatientPanel();
    var popW = 600;
    var popH = 500;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Table Values";
    devModelWindowWrapper.openPageWindow("../../html/masters/tableValueList.html", profileLbl, popW, popH, true, onCloseTableValueListAction);
}
var selCodeTableDataItem = null;
function onCloseTableValueListAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        operation = UPDATE;
        selCodeTableDataItem= returnData.selItem;
        addValuesToTable();
    }else if(returnData && returnData.status == "Add"){
        operation = ADD;
        addValuesToTable();
    }
}
function onClickFileResource(){
    operation = ADD;
    addFileResource();
}
var selFileResourceDataItem = null;
function addFileResource(){
    showPatientPanel();
    var popW = "65%";
    var popH = 500;

    parentRef.operation = operation;
    parentRef.selItem = selFileResourceDataItem;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Add File";
    devModelWindowWrapper.openPageWindow("../../html/masters/createDiet.html", profileLbl, popW, popH, true, onCloseCreateDietAction);
}
function showPatientPanel(){
    /*if(navigator.userAgent.toLowerCase().indexOf('firefox')==1){
        $("#pnlPatient").css("top","50px");
    $("#iframe").css("top","110px");
    console.log(window.innerHeight);
    if(window.innerHeight>700 || window.innerHeight>650){
        $("#pnlPatient").css("top","50px");
        $("#iframe").css("top","116px");
    }else if(window.innerHeight>600){
        $("#pnlPatient").css("top","60px");
        $("#iframe").css("top","116px");
    }else{
        $("#pnlPatient").css("top","50px");
        $("#iframe").css("top","116px");
    }
}*/
}
var dietId = "";
function onCloseCreateDietAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        if(returnData.operation == ADD){
            customAlert.info("Info","File uploaded successfully.")
        }else if(returnData.operation == UPDATE){
            customAlert.info("Info","File modified successfully.")
        }else if(returnData.operation == DELETE){
            customAlert.info("Info","File  deleted successfully.")
        }
    }else if(returnData && returnData.status == "search"){
        dietId = returnData.dietType;
        onClickSearchDiet();
    }
}
function onClickSearchDiet(){
    showPatientPanel();
    var popW = "65%";
    var popH = 450;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    parentRef.dietType = dietId;
    profileLbl = "Search File";
    devModelWindowWrapper.openPageWindow("../../html/masters/dietList.html", profileLbl, popW, popH, true, onCloseDietListAction);
}
function onCloseDietListAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        operation = UPDATE;
        selFileResourceDataItem= returnData.selItem;
        addFileResource();
    }else if(returnData && returnData.status == "Add"){
        operation = ADD;
        addFileResource();
    }
}
function onClickAddUser(){
    showPatientPanel();
    var popW = "60%";
    var popH = 450;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Create User";
    devModelWindowWrapper.openPageWindow("../../html/masters/createUser.html", profileLbl, popW, popH, true, onCloseCreateUserAction);
}
function onCloseCreateUserAction(evt,returnData){

}
function onClickUserRoles(){
}

function onClickUserPatientMap(){
    showPatientPanel();
    var popW = "67%";
    var popH = 450;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Patient User Mapping";
    devModelWindowWrapper.openPageWindow("../../html/masters/userMapping.html", profileLbl, popW, popH, true, onCloseUserPatientMapAction);
}
function onCloseUserPatientMapAction(){

}
function onClickAdmin(){

}
function onClickScreenPermissions(){
    showPatientPanel();
    var popW = "70%";
    var popH = 600;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Screen Permissins";
    devModelWindowWrapper.openPageWindow("../../html/masters/createScreenPermissions.html", profileLbl, popW, popH, true, onCloseUserPermissions);
}
function onCloseUserPermissions(evt,returnData){

}
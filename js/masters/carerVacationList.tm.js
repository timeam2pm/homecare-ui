var angularUIgridWrapper;
var parentRef = null;
var recordType = "1";
var dataArray = [];
$(document).ready(function(){
    parentRef = parent.frames['iframe'].window;
    //recordType = parentRef.dietType;
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("dgridCarerList", dataOptions);
    angularUIgridWrapper.init();
    buildDeviceListGrid([]);
});

$(window).load(function(){
    loading = false;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
    $("#btnEdit").prop("disabled", true);
    $("#btnDelete").prop("disabled", true);
    $("#txtFDate").kendoDatePicker();
    $("#txtTDate").kendoDatePicker();
    $("#cmbManager").kendoComboBox();
    $("#cmbLType").kendoComboBox();
    getLeaveTypes();
    getManagers();
    init();
    buttonEvents();
    adjustHeight();
}

function init(){
    $("#btnEdit").prop("disabled", true);
    $("#btnDelete").prop("disabled", true);
    buildDeviceListGrid([]);
    getAjaxObject(ipAddress+"/homecare/vacations/?fields=*&parentTypeId="+Number(sessionStorage.userTypeId)+"&parentId="+Number(parentRef.patientId),"GET",handleGetVacationList,onError);
    //getAjaxObject(ipAddress+"/homecare/activity-types/?is-active=1&is-deleted=0&sort=display-order","GET",getActivityTypes,onError);
}

function handleGetVacationList(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var vacation = [];
            if(dataObj.response.vacations){
                if($.isArray(dataObj.response.vacations)){
                    vacation = dataObj.response.vacations;
                }else{
                    vacation.push(dataObj.response.vacations);
                }
            }
            for(var j=0;j<vacation.length;j++){
                vacation[j].FD = kendo.toString(new Date(vacation[j].fromDate),"MM-dd-yyyy");
                vacation[j].TD = kendo.toString(new Date(vacation[j].toDate),"MM-dd-yyyy");
                vacation[j].idk = vacation[j].id;
            }
            buildDeviceListGrid(vacation);
        }
    }
}
function onError(errorObj){
    console.log(errorObj);
}

function buttonEvents(){
    $("#btnSave").off("click",onClickSave);
    $("#btnSave").on("click",onClickSave);

    //$("#btnSaveDO").off("click",onClickSaveDO);
    //$("#btnSaveDO").on("click",onClickSaveDO);

    $("#btnCancelDet").off("click",onClickCancel);
    $("#btnCancelDet").on("click",onClickCancel);

    //$("#btnDelete").off("click",onClickDelete);
    //$("#btnDelete").on("click",onClickDelete);

    $("#btnEdit").off("click",onClickEdit);
    $("#btnEdit").on("click",onClickEdit);

    $("#btnDelete").off("click",onClickDelete);
    $("#btnDelete").on("click",onClickDelete);

    //$("#btnAdd").off("click");
    //$("#btnAdd").on("click",onClickAdd);

    //$("#btnDiet").off("click");
    //$("#btnDiet").on("click",onClickDiet);

    //$("#btnExcersize").off("click");
    //$("#btnExcersize").on("click",onClickExcersize);

    //$("#btnillness").off("click");
    //$("#btnillness").on("click",onClickillness);
}

function getLeaveTypes(){
    getAjaxObject(ipAddress+"/homecare/leave-types/?fields=id,value","GET",handleGetLeaveTypes,onError);
}

function handleGetLeaveTypes(dataObj){
    console.log(dataObj);
    var leaveArray = [];
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            if($.isArray(dataObj.response.leaveTypes)){
                leaveArray = dataObj.response.leaveTypes;
            }else{
                leaveArray.push(dataObj.response.leaveTypes);
            }
            for(var i=0;i<leaveArray.length;i++){
                leaveArray[i].idk = leaveArray[i].id;
            }

            setDataForSelection(leaveArray, "cmbLType", function(){}, ["value", "idk"], 0, "");
        }
    }
}

function getManagers(){
    getAjaxObject(ipAddress+"/user/list/?user-type-id=700&is-deleted=0","GET",getUserList,onError);
}
function getUserList(dataObj){
    console.log(dataObj);
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            if($.isArray(dataObj.response.user)){
                dataArray = dataObj.response.user;
            }else{
                dataArray.push(dataObj.response.user);
            }
        }
    }
    var tempDataArry = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
        }
    }
    setDataForSelection(dataArray, "cmbManager", function(){}, ["firstName", "idk"], 0, "");
}
function onClickDelete(){
    customAlert.confirm("Confirm", "Are you sure to delete?",function(response){
        if(response.button == "Yes"){
            var dataObj = {};
            dataObj.createdBy = Number(sessionStorage.userId);;
            dataObj.isDeleted = 1;
            dataObj.isActive = 0;
            var dataUrl = ipAddress +"/homecare/vacations/";
            var method = "DELETE";
            var selectedItems = angularUIgridWrapper.getSelectedRows();
            dataObj.id = selectedItems[0].idk;
            createAjaxObject(dataUrl, dataObj, method, onCreateDelete, onError);
        }
    });
}
function onCreateDelete(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "Vacation deleted successfully";
            customAlert.error("Info", msg);
            operation = ADD;
            init();
        }
    }
}
function onClickSave(){
    var txtFDate = $("#txtFDate").data("kendoDatePicker");
    var txtTDate = $("#txtTDate").data("kendoDatePicker");
    var cmbManager = $("#cmbManager").data("kendoComboBox");
    var cmbLType = $("#cmbLType").data("kendoComboBox");

    var reqData = {};
    reqData.reason = $("#txtReason").val();
    reqData.subject = $("#txtSubject").val();
    reqData.toDate = txtTDate.value().getTime();
    reqData.fromDate = txtFDate.value().getTime();
    reqData.fromEmail = $("#txtFEmail").val();
    reqData.toEmail = $("#txtTEmail").val();
    reqData.leaveTypeId = Number(cmbLType.value());
    reqData.parentTypeId = Number(sessionStorage.userTypeId);
    reqData.parentId = parentRef.patientId;
    reqData.managerIds = Number(cmbManager.value());
    reqData.isActive = 1;
    reqData.isDeleted = 0;
    reqData.createdBy = 101;

    console.log(reqData);
    var method = "POST";
    if(operation == UPDATE){
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        reqData.id = selectedItems[0].idk;
        method = "PUT";
    }
    dataUrl = ipAddress +"/homecare/vacations/";
    createAjaxObject(dataUrl, reqData, method, onCreate, onError);
}

function onCreate(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "Vacation created successfully";
            if(operation == UPDATE){
                msg = "Vacation updated successfully"
            }else{

            }
            displaySessionErrorPopUp("Info", msg, function(res) {
                resetData();
                operation = ADD;
                init();
            })
        }else{
            customAlert.error("Error", dataObj.response.status.message);
        }
    }else{

    }
}

function resetData(){
    $("#txtFEmail").val("");
    $("#txtTEmail").val("");
    $("#txtSubject").val("");
    $("#txtReason").val("");

    var txtFDate = $("#txtFDate").data("kendoDatePicker");
    var txtTDate = $("#txtTDate").data("kendoDatePicker");

    txtFDate.value("");
    txtTDate.value("");
}
function onClickDiet(){
    onDiet();
    recordType = "1";
    init();
}
function onClickExcersize(){
    onExcersize();
    recordType = "2";
    init();
}
function onClickillness(){
    onIllness();
    recordType = "5";
    init();
}
function onDiet(){
    $("#btnDiet").addClass("selectButtonBarClass");
    $("#btnExcersize").removeClass("selectButtonBarClass");
    $("#btnillness").removeClass("selectButtonBarClass");
}
function onExcersize(){
    $("#btnDiet").removeClass("selectButtonBarClass");
    $("#btnExcersize").addClass("selectButtonBarClass");
    $("#btnillness").removeClass("selectButtonBarClass");
}
function onIllness(){
    $("#btnDiet").removeClass("selectButtonBarClass");
    $("#btnExcersize").removeClass("selectButtonBarClass");
    $("#btnillness").addClass("selectButtonBarClass");
}
function adjustHeight(){
    var defHeight = 180;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    if(angularUIgridWrapper){
        angularUIgridWrapper.adjustGridHeight(cmpHeight);
    }
}

function buildDeviceListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "From Date",
        "field": "FD",
    });
    gridColumns.push({
        "title": "To Date",
        "field": "TD",
    });
    gridColumns.push({
        "title": "From Email",
        "field": "fromEmail",
    });
    gridColumns.push({
        "title": "To Email",
        "field": "toEmail",
    });
    gridColumns.push({
        "title": "Subject",
        "field": "subject",
    });
    gridColumns.push({
        "title": "Reason",
        "field": "reason",
    });
    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}

var selRow = null;
function onClickAction(){

}

var prevSelectedItem =[];
function onChange(){
    setTimeout(function() {
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if (selectedItems && selectedItems.length > 0) {
            var obj = selectedItems[0];
            atID = obj.idk;
            $("#btnEdit").prop("disabled", false);
            $("#btnDelete").prop("disabled", false);
        } else {
            $("#btnEdit").prop("disabled", true);
            $("#btnDelete").prop("disabled", true);
        }
    });

}
var operation = "ADD";
var ADD = "ADD";
var UPDATE = "UPDATE";
var atID = "";
var ds  = "";
function onClickEdit(){
    var selectedItems = angularUIgridWrapper.getSelectedRows();
    console.log(selectedItems);
    if (selectedItems && selectedItems.length > 0) {
        var obj = selectedItems[0];
        operation = UPDATE;
        $("#txtFEmail").val(obj.fromEmail);
        $("#txtTEmail").val(obj.toEmail);
        $("#txtSubject").val(obj.subject);
        $("#txtReason").val(obj.reason);

        var fdt = new Date(obj.fromDate);
        var tdt = new Date(obj.toDate);

        var txtFDate = $("#txtFDate").data("kendoDatePicker");
        var txtTDate = $("#txtTDate").data("kendoDatePicker");

        txtFDate.value(fdt);
        txtTDate.value(tdt);

    }
}


function closeVideoScreen(evt,returnData){

}
var operation = "add";
var selFileResourceDataItem = null;
function onClickCancel(){
    var obj = {};
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}

var angularUIgridWrapper = null;
var angularUISelgridWrapper = null;

var angularUIPTgridWrapper = null;
var angularUISTgridWrapper = null;

var parentRef = null;
var operation = "";
var selItem = null;
var ADD = "add";
var UPDATE = "update";
var VIEW = "view";
var DELETE = "delete";
var statusArr = [{Key:'Active',Value:'Active'},{Key:'InActive',Value:'InActive'}];
var typeArr = [{ Key: '100', Value: 'Doctor' }, { Key: '200', Value: 'Nurse' }, { Key: '201', Value: 'Caregiver' }];

$(document).ready(function(){
    sessionStorage.setItem("IsSearchPanel", "1");
    $("#pnlPatient",parent.document).hide();
    $("#divStaffInfo",parent.document).hide();
    $("#divstaffIdMain",parent.document).hide();
    parentRef = parent.frames['iframe'].window;
    var pnlHeight = window.innerHeight;
    var imgHeight = pnlHeight-50;
    $("#divTop").height(imgHeight);
});
$(document).ready(function(){
    parentRef = parent.frames['iframe'].window;
    patientId = parentRef.patientId
    // var dataOptions = {
    //     pagination: false,
    //     changeCallBack: onChange
    // }
    // angularUIgridWrapper = new AngularUIGridWrapper("dgSelUserIdList1", dataOptions);
    // angularUIgridWrapper.init();
    // buildFileResourceListGrid([]);
    //
    // var dataOptions1 = {
    //     pagination: false,
    //     changeCallBack: onChange1
    // }
    // angularUISelgridWrapper = new AngularUIGridWrapper("dgAvlUserIdList2", dataOptions1);
    // angularUISelgridWrapper.init();
    // buildFileResourceSelListGrid([]);
    //
    // var dataStOptions = {
    //     pagination: false,
    //     changeCallBack: onStChange
    // }
    // angularUISTgridWrapper = new AngularUIGridWrapper("dgridStaffPatient", dataStOptions);
    // angularUISTgridWrapper.init();
    // buildStaffGrid([]);

    var dataPtOptions = {
        pagination: false,
        changeCallBack: onPtChange
    }
    angularUIPTgridWrapper = new AngularUIGridWrapper("dgridUserPatient", dataPtOptions);
    angularUIPTgridWrapper.init();
    buildPatientGrid([]);
});


$(window).load(function(){
    parentRef = parent.frames['iframe'].window;
    $(window).resize(adjustHeight);
    loading = false;
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
    init();
    buttonEvents();
    adjustHeight();
}
function adjustHeight(){
    var defHeight = 200;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    cmpHeight = cmpHeight;
    // if(angularUIgridWrapper){
    //     angularUIgridWrapper.adjustGridHeight(cmpHeight);
    // }
    // if(angularUISelgridWrapper){
    //     angularUISelgridWrapper.adjustGridHeight(cmpHeight);
    // }

    if(angularUIPTgridWrapper){
        angularUIPTgridWrapper.adjustGridHeight(cmpHeight);
    }
}

function adjustHeightStaff(){
    var defHeight = 140;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    cmpHeight = cmpHeight/2;
    if(angularUIgridWrapper){
        angularUIgridWrapper.adjustGridHeight(cmpHeight);
    }
    if(angularUISelgridWrapper){
        angularUISelgridWrapper.adjustGridHeight(cmpHeight);
    }

    if(angularUISTgridWrapper){
        angularUISTgridWrapper.adjustGridHeight(cmpHeight);
    }
}



function buildFileResourceListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Select",
        "field": "SEL",
        "cellTemplate": showCheckBoxTemplate(),
        "width":"15%"
    });
    gridColumns.push({
        "title": "User ID",
        "field": "idk",
        "width":"25%"
    });
    gridColumns.push({
        "title": "User Name",
        "field": "userName",
        "width":"30%"
    });
    gridColumns.push({
        "title": "Full Name",
        "field": "fullName",
    });
    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}
var prevSelectedItem =[];
function onChange(){

}
function buildFileResourceSelListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Select",
        "field": "SEL",
        "cellTemplate": showCheckBoxTemplate(),
        "width":"15%"
    });
    gridColumns.push({
        "title": "User ID",
        "field": "userId",
        "width":"25%"
    });
    gridColumns.push({
        "title": "User Name",
        "field": "userName",
        "width":"30%"
    });
    gridColumns.push({
        "title": "Full Name",
        "field": "fullName",
    });
    angularUISelgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}
var prevSelectedItem =[];
function onChange1(){

}

function showCheckBoxTemplate(){
    var node = '<div style="text-align:center;padding-top: 6px;"><input type="checkbox" class="check1" id="chkbox" ng-model="row.entity.SEL" style="width:20px;height:20px;margin:0px;"   onclick="onSelect(event);"></input></div>';
    return node;
}

function buildPatientGrid(dataSource){
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Image",
        "field": "PID",
        "enableColumnMenu": false,
        "width": "14%",
        "cellTemplate":showPtImage()
    });
    gridColumns.push({
        "title": "ID",
        "field": "idk",
        "width": "8%",
    });

    gridColumns.push({
        "title": "Last Name",
        "field": "lastName",
        "width": "21%",
    });
    gridColumns.push({
        "title": "First Name",
        "field": "firstName",
        "width": "22%",
    });
    gridColumns.push({
        "title": "Gender",
        "field": "gender",
        "width": "15%",
    });
    gridColumns.push({
        "title": "User Type",
        "field": "userType",
        "width": "20%",
    });


    angularUIPTgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
}

function showPtImage(){
    var node = '<div><img src="{{row.entity.photo}}" onerror="this.src=\'../../img/AppImg/HosImages/blank.png\'" style="width:50px"><spn>';//this.src=\'../../img/AppImg/HosImages/patient1.png\'
    node = node+"</div>";
    return node;
}

var selPatientId = null;
var selUserId;
function onPtChange(){
    setTimeout(function(){
        console.log(angularUIPTgridWrapper.getSelectedRows());
        var selRows = angularUIPTgridWrapper.getSelectedRows();
        if(selRows && selRows.length>0){
            var selItem = selRows[0];
            if(selItem){
                // staffdata(selItem);

                selPatientId = selItem.PID;
                selUserId = selItem.userId;
                getStaffUserList();
                // buildFileResourceListGrid([]);
                // buildFileResourceSelListGrid([]);
                // getPatientMappedList();
                //getUserList();
            }
        }
    },50)

}


function onStChange(){

}
// $(window).load(function(){
//     if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
//         $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
//     }
//     init();
// });
function onClickSubRight(){
//	alert("right");
    var selGridData = angularUISelgridWrapper.getAllRows();
    var selList = [];
    for (var i = 0; i < selGridData.length; i++) {
        var dataRow = selGridData[i].entity;
        if(dataRow.SEL){
            angularUISelgridWrapper.deleteItem(dataRow);
            //dataRow.SEL = false;
            angularUIgridWrapper.insert(dataRow);
        }
    }
}
function onClickSubLeft(){
    //alert("click");
    var selGridData = angularUIgridWrapper.getAllRows();
    var selList = [];
    for (var i = 0; i < selGridData.length; i++) {
        var dataRow = selGridData[i].entity;
        if(dataRow.SEL){
            angularUIgridWrapper.deleteItem(dataRow);
            angularUISelgridWrapper.insert(dataRow);
        }
    }
}

function init(){
    operation = parentRef.operation;
    selItem = parentRef.selItem;
    // getPatientList();

    //getUserList();

    $("#cmbLang").igCombo();
    setMultipleDataForSelection(typeArr, "cmbLang", onLanChange, ["Value", "Key"], 0, "");
    $("#cmbLang").val("Caregiver");
    onClickFilterBtn();
}
function onStatusChange(){
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if(cmbStatus && cmbStatus.selectedIndex<0){
        cmbStatus.select(0);
    }
}
function buttonEvents(){
    $("#btnCancel").off("click",onClickCancel);
    $("#btnCancel").on("click",onClickCancel);

    $("#btnSave").off("click",onClickSave);
    $("#btnSave").on("click",onClickSave);

    $("#btnSubRight").off("click");
    $("#btnSubRight").on("click",onClickRight);

    $("#btnSubLeft").off("click");
    $("#btnSubLeft").on("click",onClickLeft);

    $("#btnAdd").off("click");
    $("#btnAdd").on("click",getStaffUsers);

    $(".btnActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('active');
        onClickActive();
    });
    $(".btnInActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('inactive');
        onClickInActive();
    });


    $("#providerFilter-btn").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        buildPatientGrid([]);
        onClickFilterBtn();

    });

}

function onClickFilterBtn() {
    var seldata =$("#cmbLang").val();
    var selKey = '';
    for (var key in typeArr) {
        if (seldata.includes(typeArr[key].Value)) {
            selKey = selKey + typeArr[key].Key + ",";
        }
    }
    selKey = selKey.trimEnd(',');
    buildPatientGrid([]);
    if(selKey != '') {
        if (userTypeStatus == "active") {
            urlExtn = ipAddress + "/provider/list?is-active=1&is-deleted=0&type=" + selKey;
        }
        else if (userTypeStatus == "inactive") {
            urlExtn = ipAddress + "/provider/list?is-active=0&is-deleted=1&type=" + selKey;
        }
        getAjaxObject(urlExtn,"GET",onStaffListData,onErrorMedication);
    }
    else{
        if (userTypeStatus == "active") {
            getPatientList();
        }
        else if (userTypeStatus == "inactive") {
            getAjaxObject(ipAddress + "/provider/list?is-active=0&is-deleted=1","GET",onStaffListData,onErrorMedication);
        }

    }

}

function getStaffUsers(){
    var popW = "90%";
    var popH = "80%";

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Staff User Mapping";
    devModelWindowWrapper.openPageWindow("../../html/masters/addStaffUser.html", profileLbl, popW, popH, true, onCloseResume);
}

function onClickActive() {
    $(".btnInActive").removeClass("radioButton-active");
    $(".btnActive").addClass("radioButton-active");
}

function onClickInActive() {
    $(".btnActive").removeClass("radioButton-active");
    $(".btnInActive").addClass("radioButton-active");
}
var userTypeStatus= "active";
function searchOnLoad(status) {
    buildPatientGrid([]);
    userTypeStatus = status;
    var urlExtn;
    // if(status == "active") {
    //     urlExtn = ipAddress + "/provider/list?is-active=1&is-deleted=0";
    // }
    // else if(status == "inactive") {
    //     urlExtn = ipAddress + "/provider/list?is-active=0&is-deleted=1";
    // }
    // getAjaxObject(urlExtn,"GET",onStaffListData,onErrorMedication);
    onClickFilterBtn();
}

function onCloseResume(evt,returnData){

}

function getStaffUserList(){
    getAjaxObject(ipAddress + "/homecare/tenant-users/?id="+selUserId, "GET", getUserIdValueList, onErrorMedication);
}

function getUserIdValueList(dataObj) {
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.tenantUsers) {
        if ($.isArray(dataObj.response.tenantUsers)) {
            dataArray = dataObj.response.tenantUsers;
        } else {
            dataArray.push(dataObj.response.tenantUsers);
        }
    }


    var tempDataArry = [];
    var obj = {};
    obj.firstAndLastName = "";
    obj.idk = "";
    obj.userTypeId = "";
    obj.userIdDup = "";
    obj.userType = "";
    tempDataArry.push(obj);
    for (var i = 0; i < dataArray.length; i++) {
        var obj = dataArray[i];
        obj.idk = obj.id;
        obj.status = obj.Status;
        obj.userIdDup = obj.idk;
        obj.firstAndLastName = obj.lastName + " " + obj.firstName;
        if (obj.userTypeId == 100) {
            obj.userType = "Doctor";
        }
        if (obj.userTypeId == 200) {
            obj.userType = "Nurse";
        }
        if (obj.userTypeId == 201) {
            obj.userType = "Caregiver";
        }
    }


    // $("#viewDivBlock").hide();
    $("#pnlPatient",parent.document).show();
    $("#divStaffInfo",parent.document).show();
    $("#divstaffIdMain",parent.document).show();
    $("#staffuserImg", parent.document).hide();
    $("#lblStaff", parent.document).html("User ID:");
    $("#lblstaffID", parent.document).html(obj.idk);
    $("#staffName", parent.document).html(obj.firstAndLastName);
    $("#divstaffDOB",parent.document).show();
    $("#lblstaffDOB", parent.document).html("User Type:");
    $("#staffDOB", parent.document).html(obj.userType);
    $("#divstaffGender",parent.document).hide();

}

function onLanChange() {
    onComboChange("cmbLang");
}

function getPatientList(){
    getAjaxObject(ipAddress+"/provider/list?is-active=1","GET",onStaffListData,onErrorMedication);
}
// function onStaffListData(dataObj){
// //     console.log(dataObj);
// //     var tempArray = [];
// //     var dataArray = [];
// //     if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
// //         if(dataObj.response.patient){
// //             if($.isArray(dataObj.response.patient)){
// //                 tempArray = dataObj.response.patient;
// //             }else{
// //                 tempArray.push(dataObj.response.patient);
// //             }
// //         }
// //
// //     }
// //
// //     for(var i=0;i<tempArray.length;i++){
// //         var obj = tempArray[i];
// //         if(obj){
// //             var dataItemObj = {};
// //             dataItemObj.PID = obj.id;
// //             dataItemObj.FN = obj.firstName;
// //             dataItemObj.LN = obj.lastName;
// //             dataItemObj.WD = obj.weight;
// //             dataItemObj.HD = obj.height;
// //             if(obj.middleName){
// //                 dataItemObj.MN = obj.middleName;
// //             }else{
// //                 dataItemObj.MN =  "";
// //             }
// //             dataItemObj.GR = obj.gender;
// //             if(obj.status){
// //                 dataItemObj.ST = obj.status;
// //             }else{
// //                 dataItemObj.ST = "ACTIVE";
// //             }
// //             dataItemObj.DOB = kendo.toString(new Date(obj.dateOfBirth),"MM/dd/yyyy");
// //
// //             dataArray.push(dataItemObj);
// //         }
// //     }
// //     buildPatientGrid(dataArray);
// // }


var staffData = [];
function onStaffListData(dataObj){
    var dataArray = [];
    //var tempDataArry = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if($.isArray(dataObj.response.provider)){
            dataArray = dataObj.response.provider;
        }else{
            dataArray.push(dataObj.response.provider);
        }
    }else{
        //customAlert.error("Error",dataObj.response.status.message);
    }
    for(var i=0;i<dataArray.length;i++) {
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].PID = dataArray[i].id;
        dataArray[i].Status = "InActive";
        dataArray[i].photo = ipAddress + "/homecare/download/providers/photo/?" + Math.round(Math.random() * 1000000) + "&access_token=" + sessionStorage.access_token + "&tenant=" + sessionStorage.tenant + "&id=" + dataArray[i].idk;
        // dataArray[i].language1 = dataArray[i].language
        if (dataArray[i].isActive == 1) {
            dataArray[i].Status = "Active";
        }
        var createdDate = new Date(dataArray[i].createdDate);
        var createdDateString = createdDate.toLocaleDateString();
        var modifiedDate = new Date(dataArray[i].modifiedDate);
        var modifiedDateString = modifiedDate.toLocaleDateString();
        dataArray[i].createdDateString = createdDateString;
        dataArray[i].modifiedDateString = modifiedDateString;
        if (dataArray[i].type == 100) {
            dataArray[i].userType = "Doctor";
        }
        if (dataArray[i].type == 200) {
            dataArray[i].userType = "Nurse";
        }
        if (dataArray[i].type == 201) {
            dataArray[i].userType = "Caregiver";
        }
    }
    buildPatientGrid(dataArray);

}

function getPatientMappedList(){
    // getAjaxObject(ipAddress+"/user-patient/list","GET",onGetUserPatientListData,onErrorMedication);
    getAjaxObject(ipAddress+"/user/by-patient/"+selPatientId,"GET",onGetUserPatientListData,onErrorMedication);
}
var userPatientListArray = [];
function onGetUserPatientListData(dataObj){
    userPatientListArray = [];
    var userPtList = [];
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            if($.isArray(dataObj.response.loginUser)){
                userPtList = dataObj.response.loginUser;
            }else{
                userPtList.push(dataObj.response.loginUser);
            }
        }else{
            customAlert.info("Error", dataObj.response.status.message);
        }
    }
    for(var y=0;y<userPtList.length;y++){
        var ptItem = userPtList[y];
        //if(ptItem && ptItem.patientId == selPatientId){
        ptItem.pkid = ptItem.id;
        ptItem.userId = ptItem.id;
        userPatientListArray.push(ptItem);
        //}
    }
    buildFileResourceSelListGrid(userPatientListArray);
    getUserList();
}
function getUserList(){
    getAjaxObject(ipAddress+"/user/list/500,600,700","GET",onGetUserListData,onErrorMedication);
}
var userList = [];
var userListArray = [];
function onGetUserListData(dataObj){
    console.log(dataObj);
    userListArray = [];
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            if($.isArray(dataObj.response.loginUser)){
                userListArray = dataObj.response.loginUser;
            }else{
                userListArray.push(dataObj.response.loginUser);
            }
        }else{
            customAlert.info("Error", dataObj.response.status.message);
        }
    }

    userList = [];
    for(var i=0;i<userListArray.length;i++){
        var item = userListArray[i];
        item.idk = item.id;
        item.userId = item.id;
        if(!isPatientUserExist(item.idk)){
            userList.push(item);
        }
    }
    buildFileResourceListGrid(userList);
}

function isPatientUserExist(userId){
    for(var x=0;x<userPatientListArray.length;x++){
        var userItem = userPatientListArray[x];
        if(userItem && userItem.userId == userId){
            return true;
        }
    }
    return false;
}



function onClickRight(){
    var selGridData = angularUIgridWrapper.getAllRows();
    var selList = [];
    for (var i = 0; i < selGridData.length; i++) {
        var dataRow = selGridData[i].entity;
        if(dataRow.SEL){
            angularUIgridWrapper.deleteItem(dataRow);
            angularUISelgridWrapper.insert(dataRow);
        }
    }
}
function onClickLeft(){
    var selGridData = angularUISelgridWrapper.getAllRows();
    var selList = [];
    for (var i = 0; i < selGridData.length; i++) {
        var dataRow = selGridData[i].entity;
        if(dataRow.SEL){
            angularUISelgridWrapper.deleteItem(dataRow);
            angularUIgridWrapper.insert(dataRow);
        }
    }
}
function onErrorMedication(errobj){
    console.log(errobj);
}


function validation(){
    var flag = true;
    /*var strAbbr = $("#txtAbbrevation").val();
        strAbbr = $.trim(strAbbr);
        if(strAbbr == ""){
            customAlert.error("Error","Enter Abbrevation");
            flag = false;
            return false;
        }
        var strCode = $("#txtCode").val();
        strCode = $.trim(strCode);
        if(strCode == ""){
            customAlert.error("Error","Enter Code");
            flag = false;
            return false;
        }
        var strName = $("#txtName").val();
        strName = $.trim(strName);
        if(strName == ""){
            customAlert.error("Error","Enter Country");
            flag = false;
            return false;
        }*/

    return flag;
}

function onClickSave(){
    if(validation()){

        var delUserList = angularUIgridWrapper.getAllRows();
        var gridUserList = angularUISelgridWrapper.getAllRows();

        var mapUserList = [];
        for(var u=0;u<gridUserList.length;u++){
            var uItem = gridUserList[u].entity;
            if(uItem && !uItem.pkid){
                var reqObj = {};
                reqObj.isActive = "1";
                reqObj.isDeleted = "0";
                reqObj.userId = uItem.userId;
                reqObj.patientId = selPatientId;
                reqObj.sendAlert = "1";
                reqObj.id = "";
                reqObj.createdBy = sessionStorage.userId;
                mapUserList.push(reqObj);
            }
        }
        for(var d=0;d<delUserList.length;d++){
            var uItem = delUserList[d].entity;
            if(uItem){
                if(uItem.pkid){
                    var reqObj = {};
                    reqObj.isActive = "1";
                    reqObj.isDeleted = "1";
                    reqObj.userId = uItem.userId;
                    reqObj.patientId = selPatientId;
                    reqObj.sendAlert = "1";
                    reqObj.id = uItem.pkid;
                    reqObj.modifiedBy = sessionStorage.userId;
                    mapUserList.push(reqObj);
                }
            }
        }
        console.log(mapUserList);

        var dataObj = {};
        dataObj = mapUserList;
        var dataUrl = ipAddress+"/user-patient/add-or-remove";
        createAjaxObject(dataUrl,dataObj,"POST",onCreate,onError);
    }
}

function onCreate(dataObj){
    console.log(dataObj);
    if(dataObj &&dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            customAlert.error("Info", "Patient User Mapped Successfully");
            setTimeout(function(){
                onPtChange();
            },100)
        }else{
            customAlert.error("error", dataObj.status.message);
        }
    }
    /*var obj = {};
    obj.status = "success";
    obj.operation = operation;
    popupClose(obj);*/
}

function onError(errObj){
    console.log(errObj);
    customAlert.error("Error","Unable to create UserMapping");
}
function onClickCancel(){
    var obj = {};
    obj.status = "false";
    popupClose(obj);
}
function onClickSearch(){
    var obj = {};
    obj.status = "search";
    popupClose(obj);
}
function popupClose(obj){
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}

function staffdata(dataObj) {
    if(dataObj){
        strPName = dataObj.firstName+" "+dataObj.middleName+" "+dataObj.lastName;
        $("#divStaffInfo",parent.document).show();
        $("#divstaffIdMain",parent.document).show();

        var imageServletUrl = "";
        if(dataObj.photo){
            imageServletUrl =ipAddress + "/homecare/download/providers/photo/?" + Math.round(Math.random() * 1000000) + "&access_token=" + sessionStorage.access_token + "&tenant=" + sessionStorage.tenant + "&id=" + dataObj.PID;
        }else if(dataObj.gender == "Male"){
            imageServletUrl = "../../img/AppImg/HosImages/male_profile.png";
        }else if(dataObj.gender == "Female"){
            imageServletUrl = "../../img/AppImg/HosImages/profile_female.png";//ipAddress+"/download/patient/photo/"+strPatientId+"/?"+Math.round(Math.random()*1000000);
        }
        $("#staffuserImg",parent.document).show();
        $("#staffuserImg",parent.document).attr("src",imageServletUrl);


        $("#lblstaffID",parent.document).html(dataObj.PID);
        $("#staffName",parent.document).html(strPName);
        $("#lblstaffGender",parent.document).html(dataObj.gender);
        var dt = new Date(dataObj.dateOfBirth);
        if(dt){
            var strDT = onGetDate(dataObj.dateOfBirth);
            var currDate = new Date();
            //console.log(currDate.getFullYear()-dt.getFullYear());
            var strAge = getAge(dt);// currDate.getFullYear()-dt.getFullYear();
            strDT = strDT+"  ("+strAge+" Y)"
            $("#staffDOB",parent.document).html(strDT);
            var str1 = strDT+' , '+dataObj.gender;//+" - "+dataObj.response.patient.firstName+" "+dataObj.response.patient.middleName+" "+dataObj.response.patient.lastName;
            $("#pt2").text(str1);
            //$("#ptDOB").val(strDT);
        }
    }

}


function buildStaffGrid(dataSource){
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Staff Id",
        "field": "PID",
        "enableColumnMenu": false,
    });
    gridColumns.push({
        "title": "First Name",
        "field": "firstName",
        "enableColumnMenu": false,
    });
    gridColumns.push({
        "title": "Last Name",
        "field": "lastName",
        "enableColumnMenu": false,
    });
    gridColumns.push({
        "title": "Middle Name",
        "field": "middleName",
        "enableColumnMenu": false,
    });
    gridColumns.push({
        "title": "Date of Birth",
        "field": "DOB",
        "enableColumnMenu": false,
    });
    gridColumns.push({
        "title": "Gender",
        "field": "gender",
        "enableColumnMenu": false,
    });

    angularUISTgridWrappergridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeightStaff();
}



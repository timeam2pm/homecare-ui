var angularUIgridWrapper = null;
var parentRef = null;
var ADD = "add";
var UPDATE = "edit";
var isExistAvail = false;
var selectedItems;
$(document).ready(function() {
    parentRef = parent.frames['iframe'].window;
    var patientID = parentRef.patientId;
    var operation = parentRef.operation;
    var API_URL = ipAddress + '/homecare/availabilities/';
    init();
    /*var resultObj = [];*/
    function init() {
        bindEvents();
        var dataOptions = {
            pagination: false,
            changeCallBack: onChange
        }
        angularUIgridWrapper = new AngularUIGridWrapper("viewProviderAppList", dataOptions);
        angularUIgridWrapper.init();
        buildMedicationListGrid([]);
        getProviderAvlTimings();
    }
    function getProviderAvlTimings(){
        removeAllCheckBoxes();
        $("#btnAEdit").prop("disabled", true);
        $("#btnADel").prop("disabled", true);
        opr = "add";
        buildMedicationListGrid([]);
        if(parentRef.TYPE == "AVL"){
            getAjaxObjectAsync(API_URL + '?parent-id='+patientID+'&parent-type-id='+parentRef.cid+'&is-active=1&is-deleted=0',"GET", getAvailabilityList, onError);
        }

        if(parentRef.TYPE == "ROSTER"){
            $("#divStart").hide();
            $("#btnSave").hide();
            $("#btnAEdit").hide();
            $("#btnADel").hide();
            getAjaxObjectAsync(API_URL + '?parent-id='+patientID+'&parent-type-id=500&is-active=1&is-deleted=0',"GET", getAvailabilityList, onError);
        }

    }
    function onChange(){
        setTimeout(function(){
            var selectedItems = angularUIgridWrapper.getSelectedRows();
            console.log(selectedItems);
            if(selectedItems && selectedItems.length>0){
                $("#btnAEdit").prop("disabled", false);
                $("#btnADel").prop("disabled", false);
            }else{
                $("#btnAEdit").prop("disabled", true);
                $("#btnADel").prop("disabled", true);
            }
        },100)
    }
    var dataAvlArray = [];

    function onClickCancel(){
        var obj = {};
        obj.status = "false";
        popupClose(obj);
    }
    function popupClose(obj) {
        var windowWrapper = new kendoWindowWrapper();
        windowWrapper.closePageWindow(obj);
    }
    var opr = "add";
    function onClickEdit(){
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if(selectedItems.length>0){
            opr = "edit";
            var dtItem = selectedItems[0];
            removeAllCheckBoxes();
            if(dtItem){
                //	var txtStartTime = $("#txtStartTime").data("kendoTimePicker");
                //txtStartTime.value(dtItem.ST);

                // var txtEndTime = $("#txtEndTime").data("kendoTimePicker");
                //txtEndTime.value(dtItem.ET);
                $("#txtStartTime").val(dtItem.ST);
                $("#txtEndTime").val(dtItem.ET);
                if(dtItem.dayOfWeek == "1"){
                    $("#provide-mon").prop("checked",true);
                }else if(dtItem.dayOfWeek == "2"){
                    $("#provide-tues").prop("checked",true);
                }else if(dtItem.dayOfWeek == "3"){
                    $("#provide-wed").prop("checked",true);
                }else if(dtItem.dayOfWeek == "4"){
                    $("#provide-thur").prop("checked",true);
                }else if(dtItem.dayOfWeek == "5"){
                    $("#provide-fri").prop("checked",true);
                }else if(dtItem.dayOfWeek == "6"){
                    $("#provide-sat").prop("checked",true);
                }else if(dtItem.dayOfWeek == "7"){
                    $("#provide-sun").prop("checked",true);
                }else if(dtItem.dayOfWeek == "8"){

                }
            }
        }

    }
    function removeAllCheckBoxes(){
        $('.weekdayswrap').find('input[type="checkbox"]').removeAttr('checked');
        $('.includeWrap').find('input[type="checkbox"]').removeAttr('checked');
    }
    var delArray = [];
    function onClickDelete(){
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if(selectedItems.length>0){
            var obj = selectedItems[0];
            obj.isDeleted = 1;
            obj.isActive = 0;
            delArray.push(obj)
            angularUIgridWrapper.deleteItem(selectedItems[0]);
            $("#btnAEdit").prop("disabled", true);
            $("#btnADel").prop("disabled", true);
            opr = "add";
        }
    }
    function bindEvents() {
        $('#provide-mon').on('change', function() {
            if(!$(this).is(':checked')) {
                $("#provide-selall").prop("checked",false);
            }
        });
        $('#provide-tues').on('change', function() {
            if(!$(this).is(':checked')) {
                $("#provide-selall").prop("checked",false);
            }
        });
        $('#provide-wed').on('change', function() {
            if(!$(this).is(':checked')) {
                $("#provide-selall").prop("checked",false);
            }
        });
        $('#provide-thur').on('change', function() {
            if(!$(this).is(':checked')) {
                $("#provide-selall").prop("checked",false);
            }
        });
        $('#provide-fri').on('change', function() {
            if(!$(this).is(':checked')) {
                $("#provide-selall").prop("checked",false);
            }
        });
        $('#provide-sat').on('change', function() {
            if(!$(this).is(':checked')) {
                $("#provide-selall").prop("checked",false);
            }
        });
        $('#provide-sun').on('change', function() {
            if(!$(this).is(':checked')) {
                $("#provide-selall").prop("checked",false);
            }
        });
        $('#provide-selall').on('change', function() {
            //$('.weekdayswrap').find('input[type="checkbox"]').removeAttr('checked');
            //$('.includeWrap').find('input[type="checkbox"]').removeAttr('checked');
            if($(this).is(':checked')) {
                $("#provide-mon").prop("checked",true);
                $("#provide-tues").prop("checked",true);
                $("#provide-wed").prop("checked",true);
                $("#provide-thur").prop("checked",true);
                $("#provide-fri").prop("checked",true);
                $("#provide-sat").prop("checked",true);
                $("#provide-sun").prop("checked",true);
                //$('.weekdayswrap').find('input[type="checkbox"]').attr('checked','true');
            } else {
                $("#provide-mon").prop("checked",false);
                $("#provide-tues").prop("checked",false);
                $("#provide-wed").prop("checked",false);
                $("#provide-thur").prop("checked",false);
                $("#provide-fri").prop("checked",false);
                $("#provide-sat").prop("checked",false);
                $("#provide-sun").prop("checked",false);
                //$('.weekdayswrap').find('input[type="checkbox"]').removeAttr('checked');
            }
        });
        $("#btnAReset").off("click", onClickCancel);
        $("#btnAReset").on("click", onClickCancel);

        $("#btnAEdit").off("click", onClickEdit);
        $("#btnAEdit").on("click", onClickEdit);

        $("#btnADel").off("click", onClickDelete);
        $("#btnADel").on("click", onClickDelete);

        $("#btnSave").off("click", onClickSave);
        $("#btnSave").on("click", onClickSave);

        $('#btnAdd').on('click', function() {
            //var st = $("#txtStartTime").data("kendoTimePicker");
            //var et = $("#txtEndTime").data("kendoTimePicker");

            var chkMon = $("#provide-mon").is(":checked");
            var chkTue = $("#provide-tues").is(":checked");
            var chkWed = $("#provide-wed").is(":checked");
            var chkThurs = $("#provide-thur").is(":checked");
            var chkFri = $("#provide-fri").is(":checked");
            var chkSat = $("#provide-sat").is(":checked");
            var chkSun = $("#provide-sun").is(":checked");

            var day = "";
            if(chkMon){
                day = "1"+",";
            }
            if(chkTue){
                day = day+"2"+",";
            }
            if(chkWed){
                day = day+"3"+",";
            }
            if(chkThurs){
                day = day+"4"+",";
            }
            if(chkFri){
                day = day+"5"+",";
            }
            if(chkSat){
                day = day+"6"+",";
            }
            if(chkSun){
                day = day+"0"+",";
            }
            var startTime = $('#txtStartTime').val();
            var endTime = $('#txtEndTime').val();

            if(startTime && endTime){

                var startTime_hourTime = startTime.split(':')[0];
                var startTime_minTime = startTime.split(':')[1].split(' ')[0];
                var startTime_AM_PM = startTime.split(':')[1].split(' ')[1];
                var startTimeValue = "";
                startTime_hourTime = Number(startTime_hourTime);
                startTime_minTime = Number(startTime_minTime);
                if(startTime_AM_PM.toUpperCase() == "AM") {
                    startTimeValue =  60*60 * startTime_hourTime + (  startTime_minTime*60 );
                } else if(startTime_AM_PM.toUpperCase() == "PM") {
                    startTimeValue =  60*60 * (parseInt(startTime_hourTime) + 12)  + ( startTime_minTime*60 );
                }

                var endTime_hourTime = endTime.split(':')[0];
                var endTime_minTime = endTime.split(':')[1].split(' ')[0];
                var endTime_AM_PM = endTime.split(':')[1].split(' ')[1];
                var endTimeValue = "";
                endTime_hourTime = Number(endTime_hourTime);
                endTime_minTime = Number(endTime_minTime);
                if(endTime_AM_PM.toUpperCase() == "AM") {
                    endTimeValue =  60*60 * endTime_hourTime + (endTime_minTime*60 );
                } else if(endTime_AM_PM.toUpperCase() == "PM") {
                    endTimeValue = 60*60 * (parseInt(endTime_hourTime) + 12) + ( endTime_minTime*60 );
                }

                if(endTimeValue>startTimeValue && day.length>0){
                    day = day.substring(0, day.length-1);
                    var daysArray = day.split(",");
                    var dArray = [];
                    if(opr == "edit"){
                        if(daysArray.length>1){
                            $('.customAlert').append('<div class="alert alert-success alert-qualify">You can select only one day of week</div>');
                            //customAlert.error("Error", "");
                            return false;
                        }
                    }
                    if(opr == "add"){
                        var dObj = {};
                        for(var i=0;i<daysArray.length;i++){
                            dObj = {};
                            dObj.parentTypeId = parentRef.cid;
                            dObj.isDeleted = 0;
                            dObj.isActive = 1;
                            dObj.parentId = parentRef.patientId;
                            dObj.createdBy = Number(sessionStorage.userId);
                            dObj.dayOfWeek = Number(daysArray[i]);
                            dObj.startTime = startTimeValue;
                            dObj.endTime = endTimeValue;
                            dObj.dayOfWeek == 1 ? dObj.dayName = "Monday" : "";
                            dObj.dayOfWeek == 2 ? dObj.dayName = "Tuesday" : "";
                            dObj.dayOfWeek == 3 ? dObj.dayName = "Wednesday" : "";
                            dObj.dayOfWeek == 4 ? dObj.dayName = "Thursday" : "";
                            dObj.dayOfWeek == 5 ? dObj.dayName = "Friday" : "";
                            dObj.dayOfWeek == 6 ? dObj.dayName = "Saturday" : "";
                            dObj.dayOfWeek == 0 ? dObj.dayName = "Sunday" : "";
                            dObj.Notes = $("#txtNotes").val();
                            dObj.ST = $("#txtStartTime").val();
                            dObj.ET = $("#txtEndTime").val();

                            dArray.push(dObj);

                            if(angularUIgridWrapper){
                                angularUIgridWrapper.insert(dObj);
                            }
                        }
                    }else{
                        var selectedItems = angularUIgridWrapper.getSelectedRows();
                        console.log(selectedItems);
                        if(selectedItems.length>0){
                            var dItem = selectedItems[0];

                            dItem.ST = $("#txtStartTime").val();
                            dItem.ET = $("#txtEndTime").val();
                            dItem.dayOfWeek = Number(day);
                            dItem.startTime = startTimeValue;
                            dItem.endTime = endTimeValue;
                            dItem.Notes = $("#txtNotes").val();
                            dItem.dayOfWeek == 1 ? dItem.dayName = "Monday" : "";
                            dItem.dayOfWeek == 2 ? dItem.dayName = "Tuesday" : "";
                            dItem.dayOfWeek == 3 ? dItem.dayName = "Wednesday" : "";
                            dItem.dayOfWeek == 4 ? dItem.dayName = "Thursday" : "";
                            dItem.dayOfWeek == 5 ? dItem.dayName = "Friday" : "";
                            dItem.dayOfWeek == 6 ? dItem.dayName = "Saturday" : "";
                            dItem.dayOfWeek == 0 ? dItem.dayName = "Sunday" : "";
                            angularUIgridWrapper.refreshGrid();

                            $("#btnAEdit").prop("disabled", true);
                            $("#btnADel").prop("disabled", true);
                            opr = "add";
                        }
                    }
                    removeAllCheckBoxes();
                    dataAvlArray = dArray;

                }else{
                    //customAlert.error("Error", "End Time should be greater than  Start Time");
                }

                /*var me1 = "POST";
                if(opr == "edit"){

                }

                */
            }else{
                //customAlert.error("Error", "Enter Proper Time");
            }

        });
        $("#txtStartTime").kendoMaskedTextBox({
            mask: "00:00 AM"
        });
        $("#txtEndTime").kendoMaskedTextBox({
            mask: "00:00 AM"
        });
        //$("#txtStartTime").kendoTimePicker({interval: 10});
        //$("#txtEndTime").kendoTimePicker({interval: 10});
    }
    function onPatientTMCreate(dataObj){
        allRosterRowIndex = allRosterRowIndex+1;
        if(allRosterRowCount == allRosterRowIndex){
            if($('.alert-qualify') && $('.alert-qualify').length == 0) {
                $('.customAlert').append('<div class="alert alert-success alert-qualify">'+dataObj.response.status.message+'</div>');
            } else {
                $('.alert.alert-alert-qualify').text(dataObj.response.status.message);
            }

            setTimeout(function(){
                onClickCancel();
            },1000);
            //getProviderAvlTimings();
        }else{
            saveRoster();
        }
    }
    function onClickSave(){
        saveAvailableTimings();
    }
    function onPatientUpdate(dataObj){
        customAlert.error("Info", "Updated Successfully");
        getProviderAvlTimings();
    }
    var allRosterRows = [];
    var allRosterRowCount = 0;
    var allRosterRowIndex = 0;
    var dataArray = [];
    function saveAvailableTimings(){
        dataArray = [];
        allRosterRowCount = 0;
        allRosterRowIndex = 0;
        allRosterRows = angularUIgridWrapper.getAllRows();//dataAvlArray;
        for(var j=0;j< allRosterRows.length;j++){
            dataArray.push(allRosterRows[j].entity);
        }
        for(var k=0;k<delArray.length;k++){
            if(delArray[k].idk){
                dataArray.push(delArray[k]);
            }
        }
        allRosterRowCount = dataArray.length;
        if(allRosterRowCount>0){
            saveRoster();
        }
    }
    function saveRoster(){
        if(allRosterRowCount>allRosterRowIndex){
            var objRosterEntiry = dataArray[allRosterRowIndex];
            var dataUrl = ipAddress + "/homecare/availabilities/";
            var me = "POST";
            if(objRosterEntiry.id){
                //delete objRosterEntiry['id'];
            }
            if(objRosterEntiry.idk){
                me = "PUT";
                objRosterEntiry['id'] = objRosterEntiry.idk;
            }

            var dObj = {};
            dObj.parentTypeId = parentRef.cid;
            dObj.dayOfWeek = objRosterEntiry.dayOfWeek;
            dObj.isDeleted = Number(objRosterEntiry.isDeleted);
            dObj.isActive = Number(objRosterEntiry.isActive);
            dObj.parentId = parentRef.patientId;
            dObj.createdBy = 101;
            dObj.startTime = objRosterEntiry.startTime;
            dObj.endTime = objRosterEntiry.endTime;
            dObj.facilityId = Number(parentRef.fid);
            if(objRosterEntiry.idk){
                dObj.id  = objRosterEntiry.idk;
            }
            if(parentRef.TYPE == "AVL"){
                createAjaxObject(dataUrl, dObj, me, onPatientTMCreate, onError);
            }

        }
    }
    function onCreateAvailability(dataObj) {
        console.log(dataObj);
        if (dataObj && dataObj.response && dataObj.response.status) {
            if (dataObj.response.status.code == "1") {
                if($('.alert-qualify') && $('.alert-qualify').length == 0) {
                    $('.customAlert').append('<div class="alert alert-success alert-qualify">'+dataObj.response.status.message+'</div>');
                } else {
                    $('.alert.alert-alert-qualify').text(dataObj.response.status.message);
                }
            } else {
                //$('.customAlert').append('<div class="alert alert-danger">'+dataObj.response.status.message+'</div>');
                // customAlert.error("error", dataObj.response.status.message);
            }
        }
    }
    function onUpdateAvailability(dataObj) {
        console.log(dataObj);
        if (dataObj && dataObj.response && dataObj.response.status) {
            if (dataObj.response.status.code == "1") {
                if($('.alert-qualify') && $('.alert-qualify').length == 0) {
                    $('.customAlert').append('<div class="alert alert-success alert-qualify">'+dataObj.response.status.message+'</div>');
                } else {
                    $('.alert.alert-alert-qualify').text(dataObj.response.status.message);
                }
                getProviderAvlTimings();
            } else {
                //$('.customAlert').append('<div class="alert alert-danger">'+dataObj.response.status.message+'</div>');
                //customAlert.error("error", dataObj.response.status.message);
            }
        }
    }
    function getAvailabilityList(dataObj) {
        console.log(dataObj);
        var dataArray = [];
        buildMedicationListGrid([]);
        if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1" && dataObj.response.availabilities){
            if($.isArray(dataObj.response.availabilities)){
                dataArray = dataObj.response.availabilities;
            }else{
                dataArray.push(dataObj.response.availabilities);
            }
        }
        for(var i=0 ; i < dataArray.length ; i++){
            dataArray[i].idk = dataArray[i].id;
            dataArray[i].Status = "InActive";
            dataArray[i].dayOfWeek == "1" ? dataArray[i].dayName = "Monday" : "";
            dataArray[i].dayOfWeek == "2" ? dataArray[i].dayName = "Tuesday" : "";
            dataArray[i].dayOfWeek == "3" ? dataArray[i].dayName = "Wednesday" : "";
            dataArray[i].dayOfWeek == "4" ? dataArray[i].dayName = "Thursday" : "";
            dataArray[i].dayOfWeek == "5" ? dataArray[i].dayName = "Friday" : "";
            dataArray[i].dayOfWeek == "6" ? dataArray[i].dayName = "Saturday" : "";
            dataArray[i].dayOfWeek == "0" ? dataArray[i].dayName = "Sunday" : "";
            dataArray[i].parentId = parentRef.patientId;
            dataArray[i].parentTypeId = parentRef.cid;

            var sTime = dataArray[i].startTime;
            var eTime = dataArray[i].endTime;
            var sec_num_start = parseInt(sTime, 10);

            var st = sTime;
            var sdt = new Date();
            sdt.setHours(0, 0, 0, 0);
            sdt.setSeconds(Number(st));
            dataArray[i].ST = kendo.toString(sdt,"t");

            var et = eTime;
            var edt = new Date();
            edt.setHours(0, 0, 0, 0);
            edt.setSeconds(Number(et));
            dataArray[i].ET = kendo.toString(edt,"t");
            dataArray[i].idk = dataArray[i].id;

            var hours_start   = Math.floor(sec_num_start / 3600);
            var minutes_start = Math.floor((sec_num_start - (hours_start * 3600)) / 60);
            var meridian_start = "AM";
            if(hours_start > 12) {
                hours_start = hours_start - 12;
                meridian_start = "PM";
            }
            dataArray[i].startTimeOfDay = hours_start + ':' + minutes_start + ' ' + meridian_start;
            var sec_num_end = parseInt(eTime, 10);
            var hours_end   = Math.floor(sec_num_end / 3600);
            var minutes_end = Math.floor((sec_num_end - (hours_end * 3600)) / 60);
            var meridian_end = "AM";
            if(hours_end > 12) {
                hours_end = hours_end - 12;
                meridian_end = "PM";
            }
            dataArray[i].endTimeOfDay = hours_end + ':' + minutes_end + ' ' + meridian_end;
            if(dataArray[i].isActive == 1){
                dataArray[i].Status = "Active";
                var createdDate = new Date(dataArray[i].createdDate);
                var createdDateString = createdDate.toLocaleDateString();
                var modifiedDate = new Date(dataArray[i].modifiedDate);
                var modifiedDateString = modifiedDate.toLocaleDateString();
                dataArray[i].createdDateString = createdDateString;
                dataArray[i].modifiedDateString = modifiedDateString;
            }
        }
        if(dataArray.length > 0) {
            dataArray.sort(function(a, b){
                return a.dayOfWeek - b.dayOfWeek;
            });
        }
        buildMedicationListGrid(dataArray);
    }
    function onError(errObj) {
        console.log(errObj);
        //customAlert.error("Error", "Error");
    }
    function buildMedicationListGrid(dataSource) {
        var gridColumns = [];
        var otoptions = {};
        otoptions.noUnselect = false;
        gridColumns.push({
            "title": "Day of Week",
            "field": "dayName",
        });
        gridColumns.push({
            "title": "Start Time",
            "field": "ST",
        });
        gridColumns.push({
            "title": "End Time",
            "field": "ET",
        });
        angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
        adjustHeight();
    }
    function adjustHeight(){
        var defHeight = 220;//+window.top.getAvailPageHeight();
        if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
            defHeight = 80;
        }
        var cmpHeight = window.innerHeight - defHeight;
        cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
        angularUIgridWrapper.adjustGridHeight(cmpHeight);
    }
});
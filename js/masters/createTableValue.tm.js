
var parentRef = null;
var operation = "";
var selItem = null;
var ADD = "add";
var UPDATE = "update";
var VIEW = "view";
var DELETE = "delete";
var statusArr = [{Key:'Active',Value:'Active'},{Key:'InActive',Value:'InActive'}];
$(document).ready(function(){
    parentRef = parent.frames['iframe'].window;
});
function onLoaded(){
    init();
    buttonEvents();
    adjustHeight();
}

var angularUIgridWrapper;
var txtTableName = "";

function adjustHeight(){
    var defHeight = 45;
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 40;
    }


    var defHeight = 150;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    angularUIgridWrapper.adjustGridHeight(200);
}


$(window).load(function(){
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
    init();
});

function init(){
    $("txtTableName").append("#txtTableName");
    //allowAlphabets("txtValue");
    //allowAlphabets("txtDesc");
    /*$('#txtValue, #txtDesc,#txtTableName').bind('keyup', function() {
    if(allFilled()) {
        $('#btnSave').removeAttr('disabled');
    }else{
        $('#btnSave').attr("disabled", "disabled");
    }
});

function allFilled() {
    var filled = true;
    $('body input').each(function() {
        if($(this).val() == '') filled = false;
    });
    return filled;
}*/


    operation = parentRef.operation;
    selItem = parentRef.selItem;
    $("#cmbStatus").kendoComboBox();
    setDataForSelection(statusArr, "cmbStatus", onStatusChange, ["Value", "Key"], 0, "");
    getCoreTables();

    buttonEvents();
}

function onlyAlphabets(e, t) {
    try {
        if (window.event) {
            var charCode = window.event.keyCode;
        }
        else if (e) {
            var charCode = e.which;
        }
        else { return true; }
        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))
            return true;
        else
            return false;
    }
    catch (err) {
        alert(err.Description);
    }
}
function getCoreTables(){
    getAjaxObject(ipAddress+"/codetable/list/","GET",getCodeTableList,onError);

}
function getCodeTableList(dataObj){
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.codeTableName){
        if($.isArray(dataObj.response.codeTableName)){
            dataArray = dataObj.response.codeTableName;
        }else{
            dataArray.push(dataObj.response.codeTableName);
        }
    }
    var tempDataArry = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
        }
    }
    setDataForSelection(dataArray, "txtTableName", onTableChange, ["tableName", "idk"], 0, "");
    if(operation == UPDATE && selItem){
        console.log(selItem);
        $("#txtValue").val(selItem.value);
        $("#txtDesc").val(selItem.desc);

        var cmbStatus = $("#cmbStatus").data("kendoComboBox");
        if(cmbStatus){
            if(selItem.isActive == 1){
                cmbStatus.select(0);
            }else{
                cmbStatus.select(1);
            }
        }
        var txtTableName = $("#txtTableName").data("kendoComboBox");
        if(txtTableName){
            var idx = getComboListIndex("txtTableName", "idk", selItem.tbk);
            //$("cmbStatus.selItem.tableName").append(txtTableName);
            if(idx>=0){
                txtTableName.select(idx);
                txtTableName.enable(false);
            }
        }

    }else{
        var cmbStatus = $("#cmbStatus").data("kendoComboBox");
        if(cmbStatus){
            cmbStatus.enable(false);
        }
        var txtTableName = $("#txtTableName").data("kendoComboBox");
        var idx = getComboListIndex("txtTableName", "idk", parentRef.tbk);
        //$("cmbStatus.selItem.tableName").append(txtTableName);
        if(idx>=0 && txtTableName){
            txtTableName.select(idx);
            //txtTableName.enable(false);
        }
    }
}
/*function getCodeTableList(dataObj){
	var dataArray = [];
	if(dataObj && dataObj.response && dataObj.response.codeTableName){
		if($.isArray(dataObj.response.codeTableName)){
			dataArray = dataObj.response.codeTableName;
		}else{
			dataArray.push(dataObj.response.codeTableName);
		}
	}
	var tempDataArry = [];
	for(var i=0;i<dataArray.length;i++){
		dataArray[i].idk = dataArray[i].id;
		dataArray[i].Status = "InActive";
		if(dataArray[i].isActive == 1){
			dataArray[i].Status = "Active";
		}
	}
	var obj = {};
	obj.tableName = "";
	obj.idk = "";
	dataArray.unshift(obj);
	setDataForSelection(dataArray, "txtTableName", onTableChange, ["tableName", "idk"], 0, "");
	onTableChange();
}*/
function getComboListIndex(cmbId,attr,attrVal){
    var cmb = $("#"+cmbId).data("kendoComboBox");
    if(cmb){
        var ds = cmb.dataSource;
        var totalRec = ds.total();
        for(var i=0;i<totalRec;i++){
            var dtItem = ds.at(i);
            if(dtItem && dtItem[attr] == attrVal){
                return i;
            }
        }
    }
    return -1;
}
function buildDeviceListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Value",
        "field": "value"
    });
    gridColumns.push({
        "title": "Description",
        "field": "desc"
    });
    gridColumns.push({
        "title": "Status",
        "field": "Status",
    });
    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}

function onChange(){

}
function onTableChange(){
    var txtTableName = $("#txtTableName").data("kendoComboBox");
    //$("txtTableName.selectedIndex").append(txtTableName);
    if(txtTableName && txtTableName.selectedIndex<0){
        txtTableName.select(0);
    }
    //buildDeviceListGrid([]);
    //$("txtTableName.selectedIndex").append("#txtTableName");
    if(txtTableName.selectedIndex>0){
        getTableValueList();
    }
}
function getTableValueList(){
    var txtTableName = $("#txtTableName").data("kendoComboBox");
    if(txtTableName && txtTableName.text() != ""){
        ///gender/list
        var strTableName = txtTableName.text();
        getAjaxObject(ipAddress+"/master/"+strTableName+"/list/","GET",getCodeTableValueList,onError);
    }
}

function getCodeTableValueList(dataObj){
    txtTableName = dataObj.response.codeTable;
    console.log(dataObj);
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.codeTable){
        if($.isArray(dataObj.response.codeTable)){
            dataArray = dataObj.response.codeTable;
        }else{
            dataArray.push(dataObj.response.codeTable);
        }
    }
    var tempDataArry = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
        }
    }
    //buildDeviceListGrid(dataArray);
}
function onStatusChange(){
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if(cmbStatus && cmbStatus.selectedIndex<0){
        cmbStatus.select(0);
    }
}
function buttonEvents(){
    $("#btnCancel").off("click",onClickCancel);
    $("#btnCancel").on("click",onClickCancel);

    $("#btnSave").off("click",onClickSave);
    $("#btnSave").on("click",onClickSave);

    $("#btnSearch").off("click",onClickSearch);
    $("#btnSearch").on("click",onClickSearch);

    $("#btnReset").off("click",onClickReset);
    $("#btnReset").on("click",onClickReset);
}

function onClickReset(){
    operation = ADD;
    selItem = null;
    $("#cmbTableName").val("");
    $("#txtValue").val("");
    $("#txtDesc").val("");
    $("#cmbStatus").val(1);
}

function validation(){
    var flag = true;
    var txtTableName = $("#txtTableName").data("kendoComboBox");
    if(txtTableName && txtTableName.text() == ""){
        customAlert.error("Error","Select Table Name");
        flag = false;
        return false;
    }
    var strValue = $("#txtValue").val();
    strValue = $.trim(strValue);
    if(strValue == ""){
        customAlert.error("Error","Please Enter Abbrevation");
        flag = false;
        return false;
    }

    var strDesc = $("#txtDesc").val();
    strDesc = $.trim(strDesc);
    if(strDesc == ""){
        customAlert.error("Error","Please Enter Description");
        flag = false;
        return false;
    }

    return flag;
}
function dporDownIndex(){

    var txtTableName = $("#txtTableName").data("kendoComboBox");
    for(var i=0;i<txtTableName.length;i++){
        if(txtTableName && txtTableName.selectedIndex<0){
            txtTableName.select(0);
            $("txtTableName.selectedIndex").append(txtTableName);
        }
    }

}

function onClickSave(){
    if(validation()){
        var txtTableName = $("#txtTableName").data("kendoComboBox");
        var strValue = $("#txtValue").val();
        strValue = $.trim(strValue);
        var strDesc = $("#txtDesc").val();
        strDesc = $.trim(strDesc);
        var isActive = 0;
        var cmbStatus = $("#cmbStatus").data("kendoComboBox");
        if(cmbStatus){
            if(cmbStatus.selectedIndex == 0){
                isActive = 1;
            }
        }
        var dataObj = {};
        dataObj.value = strValue;
        dataObj.note = strDesc;
        dataObj.desc = strDesc;
        dataObj.isActive = isActive;
        dataObj.createdBy = sessionStorage.userId;//"101";//sessionStorage.uName;
        dataObj.isDeleted = "0";
        if(operation == ADD){
            var dataUrl = ipAddress+"/master/"+txtTableName.text()+"/create";
            createAjaxObject(dataUrl,dataObj,"POST",onCreate,onError);
        }else if(selItem){
            dataObj.id = selItem.idk;
            var dataUrl = ipAddress+"/master/"+txtTableName.text()+"/update";
            createAjaxObject(dataUrl,dataObj,"POST",onCreate,onError);
        }

    }
}

function onCreate(dataObj){
    var rows ="";
    console.log(dataObj);
    if(dataObj && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            var obj = {};
            obj.status = "success";
            obj.operation = operation;
            popupClose(obj);
        }else{
            customAlert.error("error", dataObj.response.status.message);
        }
    }
    /*var obj = {};
    obj.status = "success";
    obj.operation = operation;
    popupClose(obj);*/
}




function onError(errObj){
    console.log(errObj);
    customAlert.error("Error","Unable to create record");
}
function onClickCancel(){
    var obj = {};
    obj.status = "false";
    popupClose(obj);
}
function onClickSearch(){
    var obj = {};
    obj.status = "search";
    popupClose(obj);
}
function popupClose(obj){
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}

/****************************           Create Account JS       ********************************/

var parentRef = null;
var operation = "";
var selItem = null;
var ADD = "add";
var UPDATE = "update";
var VIEW = "view";
var DELETE = "delete";
var patientId = "";
var communicationId = "";
var contactComunicationId = "";
var statusArr = [{ Key: 'Active', Value: 'Active' }, { Key: 'InActive', Value: 'InActive' }];

var patientInfoObject = null;
var commId = "";


$(document).ready(function() {
    parentRef = parent.frames['iframe'].window;
    selItem = parentRef.selItem;
    operation = parentRef.operation; //selItem;
    var pnlHeight = window.innerHeight;
    var imgHeight = pnlHeight - 90;
    $("#divTop").height(imgHeight);


});

function adjustHeight() {
    var defHeight = 45;
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 40;
    }
}

$(window).load(function() {
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
    init();
});

function init() {
    allowNumerics("txtID");
    allowAlphaNumeric("txtExtID1");
    allowAlphaNumeric("txtExtID2");
    allowAlphabets("txtAbbreviation");
    allowAlphabets("txtN");
    allowAlphabets("txtDN");
    allowAlphaNumeric("txtFTI");
    allowAlphaNumeric("txtFTEIN");
    allowAlphaNumeric("txtSTI");
    allowAlphabets("txtNOTT");
    allowAlphabets("txtNPI");
    allowAlphaNumeric("txtTI");
    allowPhoneNumber("txtWPhone");
    allowPhoneNumber("txtExtension");
    allowNumerics("txtFax");
    allowPhoneNumber("txtCell");
    validateEmail("txtEmail");
    allowAlphabets("txtNN");
    allowAlphabets("txtFN");
    allowAlphabets("txtMN");
    allowAlphabets("txtLN");
    allowPhoneNumber("txtHPhone");
    validateEmail("txtEmail1");
    allowPhoneNumber("txtHPhone");
    //allowAlphaNumeric("txtAdd1");
    //allowAlphaNumeric("txtAdd2");

    allowPhoneNumber("txtExtension1");

    //var window = $("#btnZipSearch").data("kendoWindow");
    /*$('.sample').keyup(function() {
        if($('#txtExtID1').val() != "" && $('#txtExtID2').val() != "" && $('#txtAbbreviation').val() != "" && $('#txtN').val() != "" && $('#txtDN').val() != "" && $('#txtFTI').val() != "" && $('#txtFTEIN').val() != "" && $('#txtSTI').val() != "" && $('#txtNOTT').val() != "" && $('#txtTI').val() != "" && $('#txtAdd1').val() != "" && $('#txtAdd1').val() != "" && $('#txtWPhone').val() != "" && $('#txtExtension').val() != "" && $('#txtFax').val() != "" && $('#txtCell').val() != "" && $('#txtEmail').val() != "" && $('#txtNN').val() != "" && $('#txtFN').val() != "" && $('#txtMN').val() != "" && $('#txtLN').val() != "" && $('#txtHPhone').val() != "" && $('#txtCell1').val() != "" && $('#txtEmail1').val() != "") {
           $('#btnSave').removeAttr('disabled');
        } else {
           $('#btnSave').attr('disabled', true);  
        }
        });*/

    operation = parentRef.operation;
    patientId = parentRef.patientId;
    //selItem = parentRef.selItem;
    $("#cmbFTIT").kendoComboBox();
    $("#cmbPrefix").kendoComboBox();
    $("#cmbStatus").kendoComboBox();
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if (cmbStatus) {
        cmbStatus.enable(true);
    }
    $("#cmbZip1").kendoComboBox();
    $("#cmbSMS").kendoComboBox();

    getZip();
    buttonEvents();

}
/*function IsEmail(email) {
        var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if(!regex.test(email)) {
           return false;
           alert("your emailId is Invalid  pleace enter the valid emailId");
        }else{
           return true;

        }
      }*/
function onStatusChange() {
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if (cmbStatus && cmbStatus.selectedIndex < 0) {
        cmbStatus.select(0);
    }
}

function buttonEvents() {
    /*$("#btnCancel").off("click", onClickCancel);
    $("#btnCancel").on("click", onClickCancel);*/

    $("#btnSave").off("click", onClickSave);
    $("#btnSave").on("click", onClickSave);

    $("#btnSearch").off("click", onClickSearch);
    $("#btnSearch").on("click", onClickSearch);

    $("#btnReset").off("click", onClickReset);
    $("#btnReset").on("click", onClickReset);

    $("#btnZipSearch").off("click");
    $("#btnZipSearch").on("click", onClickZipSearch);
}

function onClickZipSearch() {
    var popW = 600;
    var popH = 500;

    parentRef.searchZip = true;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search Zip";
    devModelWindowWrapper.openPageWindow("../../html/masters/zipList.html", profileLbl, popW, popH, true, onCloseSearchZipAction);
}
var zipSelItem = null;

function onCloseSearchZipAction(evt, returnData) {
    console.log(returnData);
    if (returnData && returnData.status == "success") {
        //console.log();
        $("#cmbZip").val("");
        $("#txtZip4").val("");
        $("#txtState").val("");
        $("#txtCountry").val("");
        $("#txtCity").val("");
        var selItem = returnData.selItem;
        if (selItem) {
            zipSelItem = selItem;
            if (zipSelItem) {
                cityId = zipSelItem.idk;
                if (zipSelItem.zip) {
                    $("#cmbZip").val(zipSelItem.zip);
                }
                if (zipSelItem.zipFour) {
                    $("#txtZip4").val(zipSelItem.zipFour);
                }
                if (zipSelItem.state) {
                    $("#txtState").val(zipSelItem.state);
                }
                if (zipSelItem.country) {
                    $("#txtCountry").val(zipSelItem.country);
                }
                if (zipSelItem.city) {
                    $("#txtCity").val(zipSelItem.city);
                }
            }
        }
    }
}

function getZip() {
    getAjaxObject(ipAddress + "/city/list/", "GET", getZipList, onError);
}

function getZipList(dataObj) {
    //var dArray = getTableListArray(dataObj);
    var dArray = [];
    if (dataObj && dataObj.response && dataObj.response.cityExt) {
        if ($.isArray(dataObj.response.cityExt)) {
            dArray = dataObj.response.cityExt;
        } else {
            dArray.push(dataObj.response.cityExt);
        }
    }
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbZip", onZipChange, ["zip", "id"], 0, "");
        onZipChange();
    }
    getPrefix();
}

function getPrefix() {
    getAjaxObject(ipAddress + "/Prefix/list/", "GET", getCodeTableValueList, onError);
}

function onComboChange(cmbId) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb && cmb.selectedIndex < 0) {
        cmb.select(0);
    }
}

function onPrefixChange() {
    onComboChange("cmbPrefix");
}

function onPtChange() {
    onComboChange("cmbStatus");
}

function onGenderChange() {
    onComboChange("cmbGender");
}

function onSMSChange() {
    onComboChange("cmbSMS");
}

function onLanChange() {
    onComboChange("cmbLan");
}

function onRaceChange() {
    onComboChange("cmbRace");
}

function onEthnicityChange() {
    onComboChange("cmbEthicity");
}

function onZipChange() {
    onComboChange("cmbZip");
    var cmbZip = $("#cmbZip").data("kendoComboBox");
    if (cmbZip) {
        var dataItem = cmbZip.dataItem();
        if (dataItem) {
            $("#txtZip4").val(dataItem.zipFour);
            $("#txtCountry").val(dataItem.country);
            $("#txtState").val(dataItem.state);
            $("#txtCity").val(dataItem.city);
        }
    }
}

function getCodeTableValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbPrefix", onPrefixChange, ["value", "idk"], 0, "");
    }
    getAjaxObject(ipAddress + "/Patient_Status/list/", "GET", getPatientStatusValueList, onError);
}

function getPatientStatusValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(statusArr, "cmbStatus", onStatusChange, ["Value", "Key"], 0, "");

    }
    getAjaxObject(ipAddress + "/Gender/list/", "GET", getGenderValueList, onError);
}

function getGenderValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbGender", onGenderChange, ["desc", "idk"], 0, "");
    }
    getAjaxObject(ipAddress + "/SMS/list/", "GET", getSMSValueList, onError);
}

function getSMSValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbSMS", onSMSChange, ["desc", "idk"], 0, "");
    }
    if (operation == UPDATE && selItem) {
        $("#btnReset").hide();
        getBillingAccountInfo();
        var cmbStatus = $("#cmbStatus").data("kendoComboBox");
        if (cmbStatus) {
            if (selItem.isActive == 1) {
                cmbStatus.select(0);
            } else {
                cmbStatus.select(1);
            }
        }
    } else {
        var cmbStatus = $("#cmbStatus").data("kendoComboBox");
        if (cmbStatus) {
            cmbStatus.enable(false);
        }
    }
    //$("#btnSave").removeAttr('disabled',false);
    //$("#btnZipSearch").kendoWindowWrapper("close");



    //getAjaxObject(ipAddress+"/Language/list/","GET",getLanguageValueList,onError);
}
/*function getLanguageValueList(dataObj){
	var dArray = getTableListArray(dataObj);
	if(dArray && dArray.length>0){
		setDataForSelection(dArray, "cmbLan", onLanChange, ["desc", "idk"], 0, "");
	}
	getAjaxObject(ipAddress+"/Race/list/","GET",getRaceValueList,onError);
}
function getRaceValueList(dataObj){
	var dArray = getTableListArray(dataObj);
	if(dArray && dArray.length>0){
		setDataForSelection(dArray, "cmbRace", onRaceChange, ["desc", "idk"], 0, "");
	}
	getAjaxObject(ipAddress+"/Ethnicity/list/","GET",getEthnicityValueList,onError);
}
function getEthnicityValueList(dataObj){
	var dArray = getTableListArray(dataObj);
	if(dArray && dArray.length>0){
		setDataForSelection(dArray, "cmbEthicity", onEthnicityChange, ["desc", "idk"], 0, "");
	}
	
	if(operation == UPDATE && patientId != ""){
		getPatientInfo();
	}
}*/
function getBillingAccountInfo() {
    getAjaxObject(ipAddress + "/billing-account/" + selItem.idk, "GET", onGetBillingAccountInfo, onError);
}

function getComboListIndex(cmbId, attr, attrVal) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb) {
        var ds = cmb.dataSource;
        var totalRec = ds.total();
        for (var i = 0; i < totalRec; i++) {
            var dtItem = ds.at(i);
            if (dtItem && dtItem[attr] == attrVal) {
                cmb.select(i);
                return i;
            }
        }
    }
    return -1;
}



function onGetBillingAccountInfo(dataObj) {
    //patientInfoObject = dataObj;
    if (dataObj && dataObj.response && dataObj.response.billingAccount) {
        communicationId = dataObj.response.billingAccount.communicationId;
        contactComunicationId = dataObj.response.billingAccount.contactComunicationId;
        $("#txtID").val(dataObj.response.billingAccount.id);
        $("#txtExtID1").val(dataObj.response.billingAccount.externalId1);
        $("#txtExtID2").val(dataObj.response.billingAccount.externalId2);
        getComboListIndex("cmbPrefix", "value", dataObj.response.billingAccount.prefix);
        $("#txtFN").val(dataObj.response.billingAccount.contactFirstName);
        $("#txtLN").val(dataObj.response.billingAccount.contactLastName);
        $("#txtMN").val(dataObj.response.billingAccount.contactMiddleName);
        //$("#txtWeight").val(dataObj.response.patient.weight);
        //$("#txtHeight").val(dataObj.response.patient.height);
        $("#txtNN").val(dataObj.response.billingAccount.contactNickName);
        $("#txtAbbreviation").val(dataObj.response.billingAccount.abbreviation);
        $("#txtN").val(dataObj.response.billingAccount.name);
        $("#txtDN").val(dataObj.response.billingAccount.displayName);
        $("#txtFTI").val(dataObj.response.billingAccount.federalTaxId);
        $("#txtFTEIN").val(dataObj.response.billingAccount.federalTaxEin);
        $("#txtSTI").val(dataObj.response.billingAccount.stateTaxId);
        $("#txtNOTT").val(dataObj.response.billingAccount.taxName);
        $("#txtNPI").val(dataObj.response.billingAccount.npi);
        $("#txtTI").val(dataObj.response.billingAccount.taxonomyId);
        $("#txtAdd1").val(dataObj.response.billingAccount.address1);
        $("#txtWPhone").val(dataObj.response.billingAccount.workPhone);
        $("#txtExtension").val(dataObj.response.billingAccount.workPhoneExt);
        $("#txtFax").val(dataObj.response.billingAccount.fax);
        $("#txtCell").val(dataObj.response.billingAccount.cellPhone);
        $("#txtAdd2").val(dataObj.response.billingAccount.address2);
        $("#txtEmail").val(dataObj.response.billingAccount.email);
        $("#txtEmail1").val(dataObj.response.billingAccount.email);


        /*var dt = new Date(dataObj.response.patient.dateOfBirth);
        if(dt){
        	var strDT = kendo.toString(dt,"MM/dd/yyyy");
        	var dtDOB = $("#dtDOB").data("kendoDatePicker");
        	if(dtDOB){
        		dtDOB.value(strDT);
        	}
        }*/
        //$("#txtSSN").val(dataObj.response.patient.ssn);

        getComboListIndex("cmbFTIT", "desc", dataObj.response.billingAccount.federalTaxIdType);
        getComboListIndex("cmbStatus", "desc", dataObj.response.billingAccount.status);
        getComboListIndex("cmbPrefix", "desc", dataObj.response.billingAccount.contactPrefix);

        //getComboListIndex("cmbGender","desc",dataObj.response.patient.gender);
        //getComboListIndex("cmbEthicity","desc",dataObj.response.patient.ethnicity);
        //getComboListIndex("cmbRace","desc",dataObj.response.patient.race);
        //getComboListIndex("cmbLan","desc",dataObj.response.patient.language);

        if (dataObj && dataObj.response && dataObj.response.communication) {

            var commArray = [];
            if ($.isArray(dataObj.response.communication)) {
                commArray = dataObj.response.communication;
            } else {
                commArray.push(dataObj.response.communication);
            }
            var comObj = commArray[0];
            commId = comObj.id;
            $("#txtAdd1").val(comObj.address1);
            $("#txtAdd2").val(comObj.address2);
            //	$("#txtSMS").val(comObj.sms);
            getComboListIndex("cmbSMS", "desc", comObj.sms);
            getComboListIndex("cmbZip", "idk", comObj.cityId);
            onZipChange();
            $("#txtCell").val(comObj.cellPhone);
            $("#txtExtension").val(comObj.workPhoneExt);
            $("#txtWPhone").val(comObj.workPhone);
            $("#txtExtension1").val(comObj.homePhoneExt);
            $("#txtHPhone").val(comObj.homePhone);
            $("#txtEmail").val(comObj.email);
            $("#txtCountry").val(comObj.country);
            $("#cmbZip").val(comObj.zip);
            $("#txtZip4").val(comObj.zipFour);
            $("#txtState").val(comObj.state);
            $("#txtCity").val(comObj.city);

            /*if(comObj.defaultCommunication == "1"){
            	$("#rdHome").prop("checked",true);
            }else if(comObj.defaultCommunication == "2"){
            	$("#rdWork").prop("checked",true);
            }else if(comObj.defaultCommunication == "3"){
            	$("#rdCell").prop("checked",true);
            }else if(comObj.defaultCommunication == "4"){
            	$("#rdEmail").prop("checked",true);
            }*/
        }

    }
}

function getTableListArray(dataObj) {
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.codeTable) {
        if ($.isArray(dataObj.response.codeTable)) {
            dataArray = dataObj.response.codeTable;
        } else {
            dataArray.push(dataObj.response.codeTable);
        }
    }
    var tempDataArry = [];
    var obj = {};
    obj.desc = "";
    obj.zip = "";
    obj.value = "";
    obj.idk = "";
    tempDataArry.push(obj);
    for (var i = 0; i < dataArray.length; i++) {
        if (dataArray[i].isActive == 1) {
            var obj = dataArray[i];
            obj.idk = dataArray[i].id;
            obj.status = dataArray[i].Status;
            tempDataArry.push(obj);
        }
    }
    return tempDataArry;
}

function onClickReset() {
    operation = ADD;
    //patientId = "";
    $("#txtID").val("");
    $("#txtExtID1").val("");
    $("#txtExtID2").val("");
    $("#txtAbbreviation").val("");
    $("#txtN").val("");
    $("#txtDN").val("");
    $("#txtFTI").val("");
    $("#txtFTEIN").val("");
    $("#txtSTI").val("");
    $("#txtNOTT").val("");
    $("#txtNPI").val("");
    $("#txtTI").val("");
    $("#txtNPI").val("");
    setComboReset("cmbPrefix");
    $("#txtFN").val("");
    $("#txtLN").val("");
    $("#txtMN").val("");
    /*$("#txtWeight").val("");
    $("#txtHeight").val("");
    $("#txtNN").val("");
    	var dtDOB = $("#dtDOB").data("kendoDatePicker");
    	if(dtDOB){
    		dtDOB.value("");
    	}*/
    /*$("#txtCity").val("");
    $("#txtState").val("");
    $("#txtCountry").val("");
    $("#txtZip4").val("");*/


    setComboReset("cmbStatus");
    setComboReset("cmbZip");
    setComboReset("cmbFTIT");
    /*setComboReset("cmbEthicity");
    setComboReset("cmbRace");
    setComboReset("cmbLan");*/
    $("#txtAdd1").val("");
    $("#txtAdd2").val("");
    setComboReset("cmbSMS");
    $("#txtCell").val("");
    $("#txtNN").val("");
    $("#txtExtension").val("");
    $("#txtWPhone").val("");
    $("#txtExtension1").val("");
    $("#txtHPhone").val("");
    $("#txtEmail").val("");
    $("#txtFax").val("");
    $("#txtCell1").val("");
    $("#txtEmail1").val("");

    $("#rdHome").prop("checked", true);

}

function setComboReset(cmbId) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb) {
        cmb.select(0);
    }
}

function validation() {
    var flag = true;
    var strAbbr = $("#txtAbbreviation").val();
    strAbbr = $.trim(strAbbr);
    if (strAbbr == "") {
        customAlert.error("Error", "Enter Abbrevation");
        flag = false;
        return false;
    }
    var strName = $("#txtN").val();
    strName = $.trim(strName);
    if (strName == "") {
        customAlert.error("Error", "Enter Name");
        flag = false;
        return false;
    }
    var strDName = $("#txtDN").val();
    strDName = $.trim(strDName);
    if (strDName == "") {
        customAlert.error("Error", "Enter Display Name");
        flag = false;
        return false;
    }

    return flag;
}

function getComboDataItem(cmbId) {
    var dItem = null;
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb && cmb.dataItem()) {
        dItem = cmb.dataItem();
        return cmb.text();
    }
    return "";
}

function getComboDataItemValue(cmbId) {
    var dItem = null;
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb && cmb.dataItem()) {
        dItem = cmb.dataItem();
        return cmb.value();
    }
    return "";
}

function onClickSave() {
    if (validation()) {
        var strId = $("#txtID").val();
        var strStatus = getComboDataItem("cmbStatus");

        var strSSN = $("#txtSSN").val();
        var strGender = getComboDataItem("cmbGender");
        var strAdd1 = $("#txtAdd1").val();
        var strAdd2 = $("#txtAdd2").val();
        var strCity = $("#txtCity").val();
        var strState = $("#txtState").val();
        var strCountry = $("#txtCountry").val();
        var strFax = $("#txtFax").val();

        //var strZip = getComboDataItemValue("cmbZip");
        var strZip4 = $("#txtZip4").val();
        var strHPhone = $("#txtHPhone").val();
        var strExt1 = $("#txtExtension1").val();
        var strWp = $("#txtWPhone").val();
        var strWpExt = $("#txtExtension").val();
        var strCell = $("#txtCell").val();

        var strSMS = getComboDataItem("cmbSMS");

        var strEmail = $("#txtEmail").val();

        var dataObj = {};
        dataObj.billingAccount = {};
        dataObj.billingAccount.id = strId;
        dataObj.billingAccount.createdBy = Number(sessionStorage.userId);
        dataObj.billingAccount.isActive = 1;
        if (strStatus && statusArr == "ACTIVE") {
            dataObj.billingAccount.isActive = 1;
        }
        dataObj.billingAccount.isDeleted = 0;
        //dataObj.id = strId;
        var strExtId1 = $("#txtExtID1").val();
        var strExtId2 = $("#txtExtID2").val();

        dataObj.billingAccount.externalId1 = strExtId1;
        dataObj.billingAccount.externalId2 = strExtId2;

        var strAbbr = $("#txtAbbreviation").val();
        dataObj.billingAccount.abbreviation = strAbbr;

        var strName = $("#txtN").val();
        var strDName = $("#txtDN").val();

        dataObj.billingAccount.name = strName;
        dataObj.billingAccount.displayName = strDName;

        var strFTIT = getComboDataItem("cmbFTIT");
        var cmbFTIT = $("#cmbFTIT").data("kendoComboBox");
        if (cmbFTIT) {
            strFTIT = cmbFTIT.text();
        }
        dataObj.billingAccount.federalTaxIdType = strFTIT; //strFTIT;

        var strFTI = $("#txtFTI").val();
        dataObj.billingAccount.federalTaxId = strFTI;

        var strFTEIN = $("#txtFTEIN").val();
        dataObj.billingAccount.federalTaxEin = strFTEIN;

        var strSTI = $("#txtSTI").val();
        dataObj.billingAccount.stateTaxId = strSTI;

        var strNOTT = $("#txtNOTT").val();
        dataObj.billingAccount.taxName = strNOTT;

        var strNPI = $("#txtNPI").val();
        dataObj.billingAccount.npi = strNPI;

        var strTI = $("#txtTI").val();
        dataObj.billingAccount.taxonomyId = strTI;

        var strPrefix = getComboDataItem("cmbPrefix");
        dataObj.billingAccount.contactPrefix = 43; //strPrefix;//$("#txtEmail").val();;

        dataObj.billingAccount.communicationId = null; //communicationId;

        var strNickName = $("#txtNN").val();
        dataObj.billingAccount.contactNickName = strNickName;

        var strFN = $("#txtFN").val();
        var strMN = $("#txtMN").val();
        var strLN = $("#txtLN").val();

        dataObj.billingAccount.contactFirstName = strFN;
        dataObj.billingAccount.contactMiddleName = strMN;
        dataObj.billingAccount.contactLastName = strLN;

        dataObj.billingAccount.contactComunicationId = null; //contactComunicationId;

        var comm1 = {};
        comm1.country = $("#txtCountry").val();
        comm1.city = $("#txtCity").val();
        comm1.state = $("#txtState").val();
        var cmbZip = $("#cmbZip").data("kendoComboBox");
        var cityId = "";
        var zip = "";
        if (cmbZip) {
            var dItem = cmbZip.dataItem();
            cityId = dItem.id;
            zip = cmbZip.text();
        }
        comm1.zip = zip;
        comm1.defaultCommunication = 1;
        comm1.cityId = cityId;
        comm1.isActive = 1;
        comm1.isDeleted = 0;
        comm1.countryId = 1;
        comm1.zipFour = $("#txtZip4").val();
        comm1.address1 = $("#txtAdd1").val();
        comm1.address2 = $("#txtAdd2").val();
        comm1.state = $("#txtState").val();

        comm1.sms = "Yes";

        comm1.workPhone = $("#txtWPhone").val();
        comm1.workPhoneExt = null; //$("#txtExtension").val();
        //comm1.fax = $("#txtFax").val();
        comm1.cellPhone = $("#txtCell").val();
        comm1.email = $("#txtEmail").val();
        comm1.createdDate = new Date().getTime();
        comm1.createdBy = Number(sessionStorage.userId);
        comm1.homePhone = "3333333333333";
        //comm1.homePhoneExt = "3333";
        comm1.stateId = 1;
        comm1.areaCode = null;
        comm1.parentTypeId = "300";
        if (operation == UPDATE) {
            //comm1.modifiedBy = sessionStorage.userId;
            //comm1.modifiedDate = new Date().getTime();
        } else {
            comm1.createdBy = Number(sessionStorage.userId);
            comm1.createdDate = new Date().getTime();
        }

        var comm2 = {};
        comm2.country = $("#txtCountry").val();
        comm2.city = $("#txtCity").val();
        comm2.state = $("#txtState").val();
        var cmbZip = $("#cmbZip").data("kendoComboBox");
        var cityId = "";
        var zip = "";
        comm2.cityId = cityId;
        comm2.zipFour = "2222"; //$("#txtZip4").val();
        comm2.country = zip; //$("#zip").val();
        comm2.cityId = cityId;
        comm2.address1 = ""; // $("#txtAdd1").val();
        comm2.address2 = ""; //$("#txtAdd2").val();
        comm2.state = ""; //$("#txtState").val();
        comm2.isDeleted = 0;
        comm2.isActive = 1;
        comm2.countryId = 12;
        comm2.sms = "yes";
        comm2.defaultCommunication = 1;
        comm2.workPhone = "121212"; // $("#txtWPhone").val();
        comm2.workPhoneExt = "12333"; //$("#txtExtension").val();
        //comm2.fax = ""
        comm2.cellPhone = $("#txtCell1").val();
        comm2.email = $("#txtEmail1").val();
        comm2.createdDate = new Date().getTime();
        comm2.homePhone = $("#txtHPhone").val();
        comm2.homePhoneExt = $("#txtExtension1").val();
        comm2.stateId = 1;
        comm2.areaCode = null;
        comm2.parentTypeId = "301";

        if (operation == UPDATE) {
            comm2.modifiedBy = sessionStorage.userId;
            comm2.modifiedDate = new Date().getTime();
        } else {
            comm2.createdBy = sessionStorage.userId;
            comm2.createdDate = new Date().getTime();
        }
        comm2.createdBy = Number(sessionStorage.userId);
        comm2.createdDate = new Date().getTime();

        var comm = [];
        comm.push(comm1);
        comm.push(comm2);

        dataObj.communication = comm;

        if (operation == UPDATE) {

            //dataObj.id = commId;
            //dataObj.isDeleted = "0";
            dataObj.billingAccount.modifiedBy = sessionStorage.userId;
            dataObj.billingAccount.modifiedDate = new Date().getTime();
        }

        console.log(dataObj);

        if (operation == ADD) {
            var dataUrl = ipAddress + "/billing-account/create";
            createAjaxObject(dataUrl, dataObj, "POST", onCreate, onError);
        } else if (operation == UPDATE) {
            //dataObj.modifiedBy = " ";
            //dataObj.patient.isDeleted = "0";
            var dataUrl = ipAddress + "/billing-account/update";
            createAjaxObject(dataUrl, dataObj, "POST", onCreate, onError);
        }
    }
}

function onCreate(dataObj) {
    console.log(dataObj);
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            var obj = {};
            obj.status = "success";
            obj.operation = operation;
            popupClose(obj);
        } else {
            customAlert.error("error", dataObj.response.status.message);
        }
    }

}

function onError(errObj) {
    console.log(errObj);
    customAlert.error("Error", "Error");
}
/*function onClickCancel() {
    $('.listDataWrapper').show();
    $('.addOrRemoveWrapper').hide();
    var obj = {};
    obj.status = "false";
    popupClose(obj);
    //window.close();
}*/

function onClickSearch() {
    var obj = {};
    obj.status = "search";
    popupClose(obj);
}

/*function popupClose(obj) {
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}*/
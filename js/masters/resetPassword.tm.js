var parentRef = null;
var operation = "";
var selItem = null;
var ADD = "add";
var UPDATE = "update";
var VIEW = "view";
var DELETE = "delete";
var statusArr = [{Key:'Active',Value:'Active'},{Key:'InActive',Value:'InActive'}];
$(document).ready(function(){
    parentRef = parent.frames['iframe'].window;
});

function adjustHeight(){
    var defHeight = 45;
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 40;
    }
}

$(window).load(function(){
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
    init();
});

function init(){
    operation = parentRef.operation;
    selItem = parentRef.selItem;
    buttonEvents();
    getSecurityQuestions();
}
function getSecurityQuestions(){
    getAjaxObject(ipAddress+"/security-question/list/","GET",getSecurityQuestionList,onError);
}
function getSecurityQuestionList(dataObj){
    console.log(dataObj);
    var arrQue = dataObj;
    setDataForSelection(arrQue, "txtSQ1", onQD1Change, ["question", "id"], 0, "");
    setDataForSelection(arrQue, "txtSQ2", onQD1Change, ["question", "id"], 0, "");
    setDataForSelection(arrQue, "txtSQ3", onQD1Change, ["question", "id"], 0, "");
}
function onQD1Change(){

}
function onError(errObj){
    console.log(errObj);
    customAlert.error("Error","Error");
}

function buttonEvents(){
    $("#btnCancel").off("click",onClickCancel);
    $("#btnCancel").on("click",onClickCancel);

    $("#btnSave").off("click",onClickSave);
    $("#btnSave").on("click",onClickSave);

}

function validation(){
    var flag = true;
    /*var strAbbr = $("#txtAbbrevation").val();
    strAbbr = $.trim(strAbbr);
    if(strAbbr == ""){
        customAlert.error("Error","Enter abbrevation");
        flag = false;
        return false;
    }
    var strCode = $("#txtCode").val();
    strCode = $.trim(strCode);
    if(strCode == ""){
        customAlert.error("Error","Enter calling code");
        flag = false;
        return false;
    }
    var strName = $("#txtName").val();
    strName = $.trim(strName);
    if(strName == ""){
        customAlert.error("Error","Enter country name");
        flag = false;
        return false;
    }*/

    return flag;
}

function onClickSave(){
    if(validation()){

    }
}

function onCreate(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            if(operation == ADD){
                customAlert.info("info", "Country Created Successfully");
            }else{
                customAlert.info("info", "Country Updated Successfully");
            }
        }else{
            customAlert.error("error", dataObj.response.status.message);
        }
    }
    /*var obj = {};
    obj.status = "success";
    obj.operation = operation;
    popupClose(obj);*/
}

function onError(errObj){
    console.log(errObj);
    customAlert.error("Error","Unable to create Country");
}
function onClickCancel(){
    var obj = {};
    obj.status = "false";
    popupClose(obj);
}
function popupClose(obj){
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}



var countryUIGridWrapper = null;
var stateUIGridWrapper = null;
var zipUIGridWrapper = null;
var activeListFlag = true;
var windowLoad = false;
var selectedItems;
var IsPostalCodeManual = sessionStorage.IsPostalCodeManual;
var IsPostalFlag = "0";

function bindEvents() {
    $('[data-medicate-content=2]').show();
    $('[data-medicate-content=1]').hide();
    $('.sidebar-nav-link').on('click', function() {
        getCountryZoneName();
        $(this).closest('li').addClass('activeList').siblings().removeClass('activeList');
        $('.communicationForm-form')[0].reset();
        $('.hasError').hide();
        $('.alert').remove();
        $('#communication-save').show();
        $('#communication-reset').show();
        $('#communication-update').hide();
        /*$('#communication-cancel').hide();*/
        /*$('[data-medicate-list="1"]').find('.tablist-link').text("Add");*/
        $('.navBar').find('.communication-btn-link').attr('disabled', 'disabled');
        $('.communicationForm-form').find('option').each(function() {
            if ($(this).attr('data-medicate-status') == 1) {
                $(this).attr('selected', 'selected');
            } else {
                $(this).removeAttr('selected');
            }
        });
        setStatus();
        var activeFormName;
        if ($(this).attr('data-link-name') == "country") {
            $('.countryForm').addClass('showForm').siblings('.singleForm').removeClass('showForm');
            searchOnLoad("country");
            activeFormName = "country";
            setStatus();
        } else if ($(this).attr('data-link-name') == "state") {
            $('.stateForm').addClass('showForm').siblings('.singleForm').removeClass('showForm');
            searchOnLoad("state");
            activeFormName = "state";
            setStatus();
        } else if ($(this).attr('data-link-name') == "zipcode") {
            $('.zipForm').addClass('showForm').siblings('.singleForm').removeClass('showForm');
            searchOnLoad("zip");
            activeFormName = "zip";
            setStatus();
        }
        if (windowLoad && $('.tab-list.active-tabList').attr('data-medicate-list') == "2") {
            searchOnLoad(activeFormName);
        }
    });

    $('.btn-add').on('click', function(e) {
        e.preventDefault();
        $('.hasError').hide();
        $('.alert').remove();
        /*setStatus();*/
        $('.navBar').find('.communication-btn-link').attr('disabled', 'disabled');
        /*$('.tab-list').removeClass('active-tabList');*/
        var tablinkNum = $(this).closest('.tab-list').addClass('active-tabList').attr('data-medicate-list');
        $('[data-medicate-content]').hide();
        $('[data-medicate-content=1]').show();
    });

    $("#communication-reset").on('click', function(e) {
        e.preventDefault();
        $('.communicationForm-form')[0].reset();
    });
    $("#communication-list-search").on('click', function(e) {
        e.preventDefault();
        if ($(".btnActive").hasClass("selectButtonBarClass")) {
            $(".btnActive").trigger('click');
        } else if ($(".btnInActive").hasClass("selectButtonBarClass")) {
            $(".btnInActive").trigger('click');
        }
    });
    $(".communication-delete").on('click', function(e) {
        e.preventDefault();
        $('.alert').remove();
        var deleteItemObj = {
            "id": selectedItems[0].idDuplicate,
            "isActive": 0,
            "isDeleted": 1,
            "modifiedBy": sessionStorage.userId
        }
        var urlExtn = getcommunicationForm() + '/delete';
        var dataUrl = ipAddress + urlExtn;
        createAjaxObject(dataUrl, deleteItemObj, "POST", onDeleteItem, onDeleteError);
    });
    $(".communication-edit-tableList").on('click', function(e) {
        e.preventDefault();
        $('[data-medicate-list="1"]').find('.tablist-link').trigger('click');
        $('[data-medicate-list="1"]').find('.tablist-link').text("Edit");
        if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name') == "country") {
            $(".singleForm:visible").find("#txtAbbrevation").val(selectedItems[0].abbr);
            $(".singleForm:visible").find("#txtCode").val(selectedItems[0].code);
            $(".singleForm:visible").find("#txtName").val(selectedItems[0].country);
            var cmbStatus = $("#cmbStatusCountry").data("kendoComboBox");
        } else if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name') == "state") {
            $(".singleForm:visible").find("#txtAbbrevation").val(selectedItems[0].abbr);
            $(".singleForm:visible").find("#txtCode").val(selectedItems[0].code);
            $(".singleForm:visible").find("#txtName").val(selectedItems[0].state);
            var cmbStatus = $("#cmbStatusState").data("kendoComboBox");
            var country = selectedItems[0].country;
            if (country) {
                var countryIdx = getComboListIndex("cmbCountry", "country", country);
                if (countryIdx >= 0) {
                    var cmbCountry = $("#cmbCountry").data("kendoComboBox");
                    if (cmbCountry) {
                        cmbCountry.select(countryIdx);
                    }
                }
            }
        } else if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name') == "zipcode") {
            $(".singleForm:visible").find("#txtCity").val(selectedItems[0].city);
            $(".singleForm:visible").find("#txtZip").val(selectedItems[0].zip);
            $(".singleForm:visible").find("#txtZip4").val(selectedItems[0].zipFour);
            $("#txtAreaCode").val(selectedItems[0].areaCode);
            /*$("#cmbState").data("kendoComboBox");
            $("#cmbCountry").data("kendoComboBox");*/
            var state = selectedItems[0].state;
            if (state) {
                var stateIdx = getComboListIndex("cmbState", "state", state);
                setComboBoxIndex("cmbState", stateIdx);
                onStateChange();
            }
            var cmbStatus = $("#cmbStatusZip").data("kendoComboBox");
        }
        if (cmbStatus) {
            if (selectedItems[0].isActive == 1) {
                cmbStatus.select(0);
            } else {
                cmbStatus.select(1);
            }
        }
        $('#communication-save').hide();
        $('#communication-reset').hide();
        $('#communication-update').show();
        /*$('#communication-cancel').show();*/
    });
    $("#communication-update").on('click', function(e) {
        e.preventDefault();
        $('.alert').remove();
        var urlExtn = getcommunicationForm() + '/update';
        var dataUrl = ipAddress + urlExtn;
        if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name') == "country") {
            var strAbbr = $(".singleForm:visible").find("#txtAbbrevation").val() || "";
            strAbbr = $.trim(strAbbr);
            var strCode = $(".singleForm:visible").find("#txtCode").val() || "";
            strCode = $.trim(strCode);
            var strName = $(".singleForm:visible").find("#txtName").val() || "";
            strName = $.trim(strName);
            var isActive = 0;
            var cmbStatus = $("#cmbStatusCountry").data("kendoComboBox");
            if (cmbStatus) {
                if (cmbStatus.selectedIndex == 0) {
                    isActive = 1;
                }
            }
            var dataObj = {
                "id": selectedItems[0].idDuplicate,
                "modifiedBy": sessionStorage.userId,
                "abbr": strAbbr,
                "country": strName,
                "code": strCode,
                "isActive": isActive,
                "isDeleted": 0
            };
            if (strAbbr != "" && ((strCode != "" && IsPostalFlag != "1") || IsPostalFlag == "1") && strName != "") {
                var dataUrl = ipAddress + urlExtn;
                createAjaxObject(dataUrl, dataObj, "POST", onUpdateItem, onUpdateError);
            } else {
                $('.hasError').show();
            }
        } else if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name') == "state") {
            var strAbbr = $(".singleForm:visible").find("#txtAbbrevation").val() || "";
            strAbbr = $.trim(strAbbr);
            var strCode = $(".singleForm:visible").find("#txtCode").val() || "";
            strCode = $.trim(strCode);
            var strName = $(".singleForm:visible").find("#txtName").val() || "";
            strName = $.trim(strName);
            var isActive = 0;
            var cmbStatus = $("#cmbStatusState").data("kendoComboBox");
            if (cmbStatus) {
                if (cmbStatus.selectedIndex == 0) {
                    isActive = 1;
                }
            }
            var cmbCountry = $(".singleForm:visible").find("#cmbCountry").data("kendoComboBox") || "";
            var dataObj = {
                "id": selectedItems[0].idDuplicate,
                "modifiedBy": sessionStorage.userId,
                "abbr": strAbbr,
                "state": strName,
                "code": strCode,
                "countryId": cmbCountry.value(),
                "isActive": isActive,
                "isDeleted": 0
            };
            if (strAbbr != "" && strName != "" && cmbCountry != "") {
                var dataUrl = ipAddress + urlExtn;
                createAjaxObject(dataUrl, dataObj, "POST", onUpdateItem, onUpdateError);
            } else {
                $('.hasError').show();
            }
        } else if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name') == "zipcode") {
            var strCity = $(".singleForm:visible").find("#txtCity").val() || "";
            strCity = $.trim(strCity);
            var strZip = $(".singleForm:visible").find("#txtZip").val() || "";
            strZip = $.trim(strZip);
            var strZip4 = $(".singleForm:visible").find("#txtZip4").val() || "";
            strZip4 = $.trim(strZip4);
            var cmbState = $("#cmbState").data("kendoComboBox");
            var cmbCountry = $("#cmbCountry").data("kendoComboBox");
            var isActive = 0;
            var cmbStatus = $("#cmbStatusZip").data("kendoComboBox");
            if (cmbStatus) {
                if (cmbStatus.selectedIndex == 0) {
                    isActive = 1;
                }
            }
            var stateItem = cmbState.dataItem();
            var strStateId = cmbState.value() || "";
            var strAreaCode = $("#txtAreaCode").val() || "";
            var dataObj = {
                "id": selectedItems[0].idDuplicate,
                "modifiedBy": sessionStorage.userId,
                "city": strCity,
                "zip": strZip,
                "zipFour": strZip4,
                "stateId": strStateId,
                "countyId": null,
                "areaCode": strAreaCode,
                "isActive": isActive,
                "isDeleted": 0
            };
            if (strCity != "" && strZip != "" && strStateId != "" && cmbCountry != "" && isActive != "") {
                var dataUrl = ipAddress + urlExtn;
                createAjaxObject(dataUrl, dataObj, "POST", onUpdateItem, onUpdateError);
            } else {
                $('.hasError').show();
            }
        }
    });
    $('#communication-back').on('click', function(e) {
        e.preventDefault();
        $('.communicationForm-form')[0].reset();
        $('#communication-save').show();
        $('#communication-reset').show();
        $('#communication-update').hide();
        /*$('#communication-cancel').hide();*/

        $('[data-medicate-content=2]').show();
        $('[data-medicate-content="1"]').hide();
        $('[data-medicate-list="2"]').find('.tablist-link').trigger('click');
    });
    $(".btnActive").on("click", function(e) {
        e.preventDefault();
        activeListFlag = true;
        $('.alert').remove();
        onClickActive();
        var urlExtn = getcommunicationForm() + '/list/';
        if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name') == "country") {
            buildCountryListGrid([]);
            searchOnLoad("country");
        } else if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name') == "state") {
            buildStateListGrid([]);
            searchOnLoad("state");
        } else if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name') == "zipcode") {
            buildZipListGrid([]);
            searchOnLoad("zip");
        }
    });
    $(".btnInActive").on("click", function(e) {
        e.preventDefault();
        activeListFlag = false;
        $('.alert').remove();
        onClickInActive();
        var urlExtn = getcommunicationForm() + '/list/';
        if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name') == "country") {
            buildCountryListGrid([]);
            searchOnLoad("country");
        } else if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name') == "state") {
            buildStateListGrid([]);
            searchOnLoad("state");
        } else if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name') == "zipcode") {
            buildZipListGrid([]);
            searchOnLoad("zip");
        }
    });
}

function getcommunicationForm() {
    var formUrl;
    if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name').toLowerCase() == "country") {
        formUrl = '/country';
    } else if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name').toLowerCase() == "state") {
        formUrl = '/state';
    } else if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name').toLowerCase() == "zipcode") {
        formUrl = '/city';
    }
    return formUrl;
}

function searchOnLoad(getName) {
    if (getName == "country") {
        //buildCountryListGrid([]);
        $("#lblTitleName",parent.document).html("Country");
        if(IsPostalFlag == "1") {
            $("#divCallingCode").css("display", "none");
        }
        $('#communicationCountryList').addClass('showOnTop');
        $('#communicationStateList').removeClass('showOnTop');
        $('#communicationZipList').removeClass('showOnTop');
        onStatusChangeCountry();
    } else if (getName == "state") {
        //buildStateListGrid([]);
        if(IsPostalFlag == "1"){
            $("#divAreaCode").css("display","none");
        }

        $("#lblTitleName",parent.document).html("County");
        $('#communicationCountryList').removeClass('showOnTop');
        $('#communicationStateList').addClass('showOnTop');
        $('#communicationZipList').removeClass('showOnTop');
        onStatusChangeState();
    } else if (getName == "zip") {
        //buildZipListGrid([]);
        if(IsPostalFlag == "1"){
            $("#lblTitleName",parent.document).html("City");
            $("#divCityAreaCode").css("display","none");
            $("#divCityZip").css("display","none");
        }
        else{
            $("#lblTitleName",parent.document).html("Postal Code");
        }

        $('#communicationCountryList').removeClass('showOnTop');
        $('#communicationStateList').removeClass('showOnTop');
        $('#communicationZipList').addClass('showOnTop');
        onStatusChangeZip();
    }
    if ($(".btnActive").hasClass('selectButtonBarClass')) {
        var urlExtn = getcommunicationForm() + '/list';
    } else {
        var urlExtn = getcommunicationForm() + '/list/?is-active=0';
    }
    getAjaxObject(ipAddress + urlExtn, "GET", getSearchList, onSearchError);
}

function onCreate(dataObj) {
    console.log(dataObj);
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            $('.communicationForm-form')[0].reset();
            $('.communicationForm-form').prepend('<div class="alert alert-success">Created Successfully.</div>');
        } else {
            $('.communicationForm-form')[0].reset();
            $('.communicationForm-form').prepend('<div class="alert alert-danger">'+dataObj.response.status.message+'</div>');
        }
    }
}

function onError(errObj) {
    console.log(errObj);
    customAlert.error("Error", "Error");
}

function onDeleteItem(respObj) {
    console.log(respObj);
    if (respObj && respObj.response && respObj.response.status) {
        if (respObj.response.status.code == "1") {
            if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name') == "country") {
                searchOnLoad("country");
                $('#communicationCountryList').prepend('<div class="alert alert-success">Deleted Successfully</div>');
            } else if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name') == "state") {
                searchOnLoad("state");
                $('#communicationStateList').prepend('<div class="alert alert-success">Deleted Successfully</div>');
            } else if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name') == "zipcode") {
                searchOnLoad("zip");
                $('#communicationZipList').prepend('<div class="alert alert-success">Deleted Successfully</div>');
            }
            $('.navBar').find('.communication-btn-link:visible').each(function() {
                $(this).attr('disabled', 'disabled');
            });
        } else {
            if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name') == "country") {
                $('#communicationCountryList').prepend('<div class="alert alert-danger">'+respObj.response.status.message+'</div>');
            } else if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name') == "state") {
                $('#communicationStateList').prepend('<div class="alert alert-danger">'+respObj.response.status.message+'</div>');
            } else if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name') == "zipcode") {
                $('#communicationZipList').prepend('<div class="alert alert-danger">'+respObj.response.status.message+'</div>');
            }
        }
    }
}

function onDeleteError(errObj) {
    console.log(errObj);
    customAlert.error("Error", "Error");
}

function onUpdateItem(respObj) {
    console.log(respObj);
    if (respObj && respObj.response && respObj.response.status) {
        if (respObj.response.status.code == "1") {
            $('.communicationForm-form').prepend('<div class="alert alert-success">Updated Successfully</div>');
            $('.communicationForm-form')[0].reset();
            $('#communication-save').show();
            $('#communication-reset').show();
            $('#communication-update').hide();
            /*$('#communication-cancel').hide();*/
            if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name').toLowerCase() == "tradename") {
                $('.tradeFormWrapper').find('.tradeFormWrapper-select').each(function() {
                    $(this).find('option:first-child').attr('selected', 'selected')
                });
            }
        } else {
            $('.communicationForm-form').prepend('<div class="alert alert-danger">' + respObj.response.status.message + '</div>');
        }
    }
}

function onUpdateError(errObj) {
    console.log(errObj);
    customAlert.error("Error", "Error");
}
var dataActiveList = [],
    dataInActiveList = [],
    dataListArr = [];

function getSearchList(resp) {
    var dataArray, dataActiveList = [],
        dataInActiveList = [];
    if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name').toLowerCase() == "country") {
        if (resp && resp.response && resp.response.country) {
            if ($.isArray(resp.response.country)) {
                dataArray = resp.response.country;
            } else {
                dataArray.push(resp.response.country);
            }
        }
    } else if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name').toLowerCase() == "state") {
        if (resp && resp.response && resp.response.stateExt) {
            if ($.isArray(resp.response.stateExt)) {
                dataArray = resp.response.stateExt;
            } else {
                dataArray.push(resp.response.stateExt);
            }
        }
    } else if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name').toLowerCase() == "zipcode") {
        if (resp && resp.response && resp.response.cityExt) {
            if ($.isArray(resp.response.cityExt)) {
                dataArray = resp.response.cityExt;
            } else {
                dataArray.push(resp.response.cityExt);
            }
        }
    }
    if (dataArray != undefined) {
        for (var i = 0; i < dataArray.length; i++) {
            dataArray[i].idDuplicate = dataArray[i].id;
            if (dataArray[i].isActive == 1) {
                dataArray[i].Status = "Active";
                dataActiveList.push(dataArray[i]);
            } else {
                dataArray[i].Status = "InActive";
                dataInActiveList.push(dataArray[i]);
            }
        }
    }
    if (activeListFlag) {
        if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name').toLowerCase() == "country") {
            buildCountryListGrid(dataActiveList);
        } else if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name').toLowerCase() == "state") {
            buildStateListGrid(dataActiveList);
        } else if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name').toLowerCase() == "zipcode") {
            buildZipListGrid(dataActiveList);
        }
    } else if (!activeListFlag) {
        if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name').toLowerCase() == "country") {
            buildCountryListGrid(dataInActiveList);
        } else if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name').toLowerCase() == "state") {
            buildStateListGrid(dataInActiveList);
        } else if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name').toLowerCase() == "zipcode") {
            buildZipListGrid(dataInActiveList);
        }
    }
}

function onSearchError(err) {
    console.log(err);
    customAlert.error("Error", "Error");
}

function buildCountryListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Abbreviation",
        "field": "abbr",
    });
    if(sessionStorage.countryName.indexOf("United Kingdom") >= 0){
        if(IsPostalCodeManual != "1"){
            gridColumns.push({
                "title": "Calling Code",
                "field": "code",
            });
        }
    }
    gridColumns.push({
        "title": "Country",
        "field": "country",
    });
    /* gridColumns.push({
         "title": "Status",
         "field": "Status",
     });*/
    countryUIGridWrapper.creategrid(dataSource, gridColumns, otoptions);
    adjustHeight();
}

function buildStateListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Abbreviation",
        "field": "abbr",
    });
    if(IsPostalFlag != "1") {
        gridColumns.push({
            "title": "Area Code",
            "field": "code",
        });
    }
    if (sessionStorage.countryName.indexOf("United Kingdom") >= 0) {
        $(".sidebar-nav-link").each(function() {
            if ($(this).attr('data-link-name') == 'state') {
                gridColumns.push({
                    "title": "County",
                    "field": "state",
                });
            }
        });
    } else {
        gridColumns.push({
            "title": "County",
            "field": "state",
        });
    }
    gridColumns.push({
        "title": "Country",
        "field": "country",
    });
    // gridColumns.push({
    //     "title": "Status",
    //     "field": "Status",
    // });
    stateUIGridWrapper.creategrid(dataSource, gridColumns, otoptions);
    adjustHeight();
}

function buildZipListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "City",
        "field": "city",
    });
    if (sessionStorage.countryName.indexOf("India") >= 0) {
        $(".sidebar-nav-link").each(function() {
            if ($(this).attr('data-link-name') == 'zipcode') {
                gridColumns.push({
                    "title": "Postal Code",
                    "field": "zip",
                });
            }
        });
        $('.zipFourWrapper').hide();
    }else if(sessionStorage.countryName.indexOf("United Kingdom") >= 0){
        if(IsPostalCodeManual != "1"){
            $(".sidebar-nav-link").each(function() {
                if ($(this).attr('data-link-name') == 'zipcode') {
                    gridColumns.push({
                        "title": "Postal Code",
                        "field": "zip",
                    });
                }
            });
            $('.zipFourWrapper').hide();
        }
    }
    else {
        gridColumns.push({
            "title": "Zip",
            "field": "zip",
        });
        gridColumns.push({
            "title": "Zip4",
            "field": "zipFour",
        });
        $('.zipFourWrapper').show();
    }
    if (sessionStorage.countryName.indexOf("United Kingdom") >= 0) {
        $(".sidebar-nav-link").each(function() {
            if ($(this).attr('data-link-name') == 'state') {
                gridColumns.push({
                    "title": "County",
                    "field": "state",
                });
            }
        });
    } else {
        gridColumns.push({
            "title": "County",
            "field": "state",
        });
    }
    gridColumns.push({
        "title": "Country",
        "field": "country",
    });
    // gridColumns.push({
    //     "title": "Status",
    //     "field": "Status",
    // });
    zipUIGridWrapper.creategrid(dataSource, gridColumns, otoptions);
    adjustHeight();
}

function adjustHeight() {
    var defHeight = 220; //+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name').toLowerCase() == "country") {
        countryUIGridWrapper.adjustGridHeight(cmpHeight);
    } else if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name').toLowerCase() == "state") {
        stateUIGridWrapper.adjustGridHeight(cmpHeight);
    } else if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name').toLowerCase() == "zipcode") {
        zipUIGridWrapper.adjustGridHeight(cmpHeight);
    }
}

function onChange() {
    setTimeout(function() {
        if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name').toLowerCase() == "country") {
            selectedItems = countryUIGridWrapper.getSelectedRows();
        } else if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name').toLowerCase() == "state") {
            selectedItems = stateUIGridWrapper.getSelectedRows();
        } else if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name').toLowerCase() == "zipcode") {
            selectedItems = zipUIGridWrapper.getSelectedRows();
        }
        console.log(selectedItems);
        disableButtons();
    });
}

function disableButtons() {
    $('.navBar').find('.communication-btn-link').each(function() {
        if (selectedItems && selectedItems.length > 0) {
            $(this).removeAttr('disabled');
        } else {
            $(this).attr('disabled', 'disabled');
        }
    });
}

function onClickActive() {
    $(".btnInActive").removeClass("selectButtonBarClass");
    $(".btnActive").addClass("selectButtonBarClass");
}

function onClickInActive() {
    $(".btnActive").removeClass("selectButtonBarClass");
    $(".btnInActive").addClass("selectButtonBarClass");
}

function setStatus() {
    var statusArr = [{ Key: 'Active', Value: 'Active' }, { Key: 'InActive', Value: 'InActive' }];
    $("#cmbStatusCountry").kendoComboBox();
    setDataForSelection(statusArr, "cmbStatusCountry", onStatusChangeCountry, ["Value", "Key"], 0, "");
    $("#cmbStatusState").kendoComboBox();
    setDataForSelection(statusArr, "cmbStatusState", onStatusChangeState, ["Value", "Key"], 0, "");
    $("#cmbStatusZip").kendoComboBox();
    setDataForSelection(statusArr, "cmbStatusZip", onStatusChangeZip, ["Value", "Key"], 0, "");
}

function init() {
    bindEvents();
    addCommunicationData();
}

function getCountryList(dataObj) {
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.country) {
        if ($.isArray(dataObj.response.country)) {
            dataArray = dataObj.response.country;
        } else {
            dataArray.push(dataObj.response.country);
        }
    }
    var tempDataArry = [];
    for (var i = 0; i < dataArray.length; i++) {
        dataArray[i].idk = dataArray[i].id;
    }
    console.log(dataArray);
    setDataForSelection(dataArray, "cmbCountry", onCountryChange, ["country", "idk"], 0, "");

    /*if (operation == UPDATE && selItem) {
        console.log(selItem);
        $("#txtAbbrevation").val(selItem.abbr);
        $("#txtName").val(selItem.state);
        $("#txtCode").val(selItem.code);
        $("#btnReset").hide();
        var country = selItem.country;
        if (country) {
            var countryIdx = getComboListIndex("cmbCountry", "country", country);
            if (countryIdx >= 0) {
                var cmbCountry = $("#cmbCountry").data("kendoComboBox");
                if (cmbCountry) {
                    cmbCountry.select(countryIdx);
                }
            }
        }
    } else {
        var cmbStatus = $("#cmbStatus").data("kendoComboBox");
        if (cmbStatus) {
            cmbStatus.select(0);
            cmbStatus.enable(false);
        }
    }*/
}

function getStateList(dataObj) {
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.stateExt) {
        if ($.isArray(dataObj.response.stateExt)) {
            dataArray = dataObj.response.stateExt;
        } else {
            dataArray.push(dataObj.response.stateExt);
        }
    }
    var tempDataArry = [];
    for (var i = 0; i < dataArray.length; i++) {
        dataArray[i].idk = dataArray[i].id;
    }
    console.log(dataArray);


    setDataForSelection(dataArray, "cmbState", onStateChange, ["state", "idk"], 0, "");
    /*if(operation == UPDATE && selItem) {
	    console.log(selItem);
	    $("#txtAbbrevation").val(selItem.abbr);
	    $("#txtName").val(selItem.state);
	    $("#txtCode").val(selItem.code);
	    $("#btnReset").hide();
	    var county = selItem.county;
	    // if(county){
	    // 	var countyIdx = getComboListIndex("cmbCounty","county",county);
	    // 	if(countyIdx>=0){
	    // 		var cmbCounty = $("#cmbCounty").data("kendoComboBox");
	    // 		if(cmbCounty){
	    // 			cmbCounty.select(countyIdx);
	    // 		}
	    // 	}
	    // }
	    var country = selItem.country;
	    if (country) {
	        var countryIdx = getComboListIndex("cmbCountry", "country", country);
	        if (countryIdx >= 0) {
	            var cmbCountry = $("#cmbCountry").data("kendoComboBox");
	            if (cmbCountry) {
	                cmbCountry.select(countryIdx);
	            }
	        }
	    }
	} else {
	    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
	    if (cmbStatus) {
	        cmbStatus.select(0);
	        cmbStatus.enable(false);
	    }
	}*/
}

function getCountyList(dataObj) {
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.countyext) {
        if ($.isArray(dataObj.response.countyext)) {
            dataArray = dataObj.response.countyext;
        } else {
            dataArray.push(dataObj.response.countyext);
        }
    }
    var tempDataArry = [];
    for (var i = 0; i < dataArray.length; i++) {
        dataArray[i].idk = dataArray[i].id;
    }
    console.log(dataArray);
    var obj = {};
    obj.county = "";
    obj.idk = "";
    dataArray.unshift(obj);
    /*if (operation == UPDATE && selItem) {
        console.log(selItem);
        $("#btnReset").hide();
        $("#txtAbbrevation").val(selItem.abbr);
        $("#txtName").val(selItem.state);
        $("#txtCode").val(selItem.code);
        var county = selItem.county;
        // if(county){
        // 	var countyIdx = getComboListIndex("cmbCounty","county",county);
        // 	if(countyIdx>=0){
        // 		var cmbCounty = $("#cmbCounty").data("kendoComboBox");
        // 		if(cmbCounty){
        // 			cmbCounty.select(countyIdx);
        // 		}
        // 	}
        // }
        var country = selItem.country;
        if (country) {
            var countryIdx = getComboListIndex("cmbCountry", "country", country);
            if (countryIdx >= 0) {
                var cmbCountry = $("#cmbCountry").data("kendoComboBox");
                if (cmbCountry) {
                    cmbCountry.select(countryIdx);
                }
            }
        }
    } else {
        var cmbStatus = $("#cmbStatus").data("kendoComboBox");
        if (cmbStatus) {
            cmbStatus.select(0);
            cmbStatus.enable(false);
        }
    }*/
}

function onStateChange() {
    var cmbState = $("#cmbState").data("kendoComboBox");
    if (cmbState && cmbState.selectedIndex == 0) {
        cmbState.selectedIndex = 0;
    }
    getCountryByState();
}

function onCountryChange() {
    var cmbCountry = $("#cmbCountry").data("kendoComboBox");
    if (cmbCountry && cmbCountry.selectedIndex == 0) {
        cmbStatus.selectedIndex = 0;
    }
}

function onStatusChangeCountry() {
    var cmbStatus = $("#cmbStatusCountry").data("kendoComboBox");
    if (cmbStatus && cmbStatus.selectedIndex == 0) {
        cmbStatus.selectedIndex = 0;
    }
}

function onStatusChangeState() {
    var cmbStatus = $("#cmbStatusState").data("kendoComboBox");
    if (cmbStatus && cmbStatus.selectedIndex == 0) {
        cmbStatus.selectedIndex = 0;
    }
}

function onStatusChangeZip() {
    var cmbStatus = $("#cmbStatusZip").data("kendoComboBox");
    if (cmbStatus && cmbStatus.selectedIndex == 0) {
        cmbStatus.selectedIndex = 0;
    }
    //getCountryByState();
}

function onCountyChange() {
    var cmbCounty = $("#cmbCounty").data("kendoComboBox");
    if (cmbCounty && cmbCounty.selectedIndex == 0) {
        cmbStatus.selectedIndex = 0;
    }
}

function getCountryByState() {
    $("#txtCountry").val("");
    var cmbState = $("#cmbState").data("kendoComboBox");
    if (cmbState) {
        var dataItem = cmbState.dataItem();
        var country = dataItem.country;
        $("#txtCountry").val(country);
    }
}

function getCountryZoneName() {

    if (sessionStorage.countryName.indexOf("India") >= 0) {
        $(".sidebar-nav-link").each(function() {
            if ($(this).attr('data-link-name') == 'zipcode') {
                $(this).text("Postal Code");
                $("#lblTitleName",parent.document).html("Postal Code");
            }
        });
        $('.zipFourWrapper').hide();
        $('.zipWrapper').find('label').html('Postal Code : <span class="mandatoryClass">*</span>');
    } else if (sessionStorage.countryName.indexOf("United Kingdom") >= 0) {
        $(".sidebar-nav-link").each(function() {
            if ($(this).attr('data-link-name') == 'zipcode') {
                if(IsPostalCodeManual == "1"){
                    $(this).text("City");
                    IsPostalFlag = "1";
                    $('.zipWrapper').css("display","none");
                }
                else{
                    $(this).text("Postal Code");
                }
            }
            if ($(this).attr('data-link-name') == 'state') {
                $(this).text("County");
            }
            $('.stateWrapper').find('label').html('County : <span class="mandatoryClass">*</span>');
        });
        $('.zipFourWrapper').hide();
        $('.zipWrapper').find('label').html('Postal Code : <span class="mandatoryClass">*</span>');
    } else {
        $('.zipFourWrapper').show();
    }

    /*$.getJSON('//freegeoip.net/json/?callback=', function(dataObj) {
        var dataUrl = ipAddress + "/user/location";
        console.log(dataObj);
        sessionStorage.country = dataObj.country_code;
        dataObj.createdBy = sessionStorage.userId;
        dataObj.countryCode = dataObj.country_code;
        dataObj.countryName = dataObj.country_name;
        dataObj.regionCode = dataObj.region_code;
        dataObj.regionName = dataObj.region_name;
        dataObj.timeZone = dataObj.time_zone;
        dataObj.metroCode = dataObj.metro_code;
        dataObj.zipCode = dataObj.zip_code;
        //sessionStorage.countryName = dataObj.country_name;
        if (sessionStorage.countryName.indexOf("India") >= 0) {
            $(".sidebar-nav-link").each(function() {
                if ($(this).attr('data-link-name') == 'zipcode') {
                    $(this).text("Postal Code");
                }
            });
            $('.zipFourWrapper').hide();
            $('.zipWrapper').find('label').html('Postal Code : <span class="mandatoryClass">*</span>');
        } else if (sessionStorage.countryName.indexOf("United Kingdom") >= 0) {
            $(".sidebar-nav-link").each(function() {
                if ($(this).attr('data-link-name') == 'zipcode') {
                    $(this).text("Postal Code");
                }
                if ($(this).attr('data-link-name') == 'state') {
                    $(this).text("County");
                }
            });
            $('.zipFourWrapper').hide();
            $('.zipWrapper').find('label').html('Postal Code : <span class="mandatoryClass">*</span>');
        } else {
            $('.zipFourWrapper').show();
        }
    });*/
}

function Validate(event) {
    var regex = new RegExp("^[0-9?]");
    var key = String.fromCharCode(event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
}

function onlyAlphabets(e) {
    try {
        if (window.event) {
            var charCode = window.event.keyCode;
        } else if (e) {
            var charCode = e.which;
        } else { return true; }
        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || (charCode == 32))
            return true;
        else
            return false;
    } catch (err) {
        alert(err.Description);
    }
}

$(document).ready(function() {
    $("#divMain",parent.document).css("display","none");
    $("#divPatInfo",parent.document).css("display","");

    init();
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    countryUIGridWrapper = new AngularUIGridWrapper("communicationCountryList", dataOptions);
    countryUIGridWrapper.init();
    stateUIGridWrapper = new AngularUIGridWrapper("communicationStateList", dataOptions);
    stateUIGridWrapper.init();
    zipUIGridWrapper = new AngularUIGridWrapper("communicationZipList", dataOptions);
    zipUIGridWrapper.init();
    if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name').toLowerCase() == "country") {
        buildCountryListGrid([]);
    } else if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name').toLowerCase() == "state") {
        buildStateListGrid([]);
    } else if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name').toLowerCase() == "zipcode") {
        buildZipListGrid([]);
    }
    $("#cmbState").kendoComboBox();
    $("#cmbCountry").kendoComboBox();
    getAjaxObject(ipAddress + "/country/list/?is-active=1", "GET", getCountryList, onError);
    getAjaxObject(ipAddress + "/state/list/", "GET", getStateList, onError);
    setStatus();
    getCountryZoneName();
});
$(window).load(function() {
    windowLoad = true;
    /*searchOnLoad();*/
});

function addCommunicationData() {
    $('.sidebar-nav-list:first-child').find('.sidebar-nav-link').trigger('click');
}
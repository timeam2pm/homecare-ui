var angularUIgridWrapper;
var angularUISelgridWrapper;
var angularUIgridWrapper1;

var parentRef = null;
var patientId = "";

var activityid = "1";

var ADL = "1";
var IADL = "2";
var CARE = "3";
var FLUID = "4";
var FOOD = "5";
var menuLoaded = false;

var typeArr = [{Key:'ADL',Value:'1'},{Key:'iADL',Value:'2'},{Key:'Care',Value:'3'},{Key:'Fluid',Value:'4'},{Key:'Food',Value:'5'}];
var compType = [{Key:'Text',Value:'1'},{Key:'Text Area',Value:'2'},{Key:'Boolean',Value:'3'},{Key:'Checkbox',Value:'4'},{Key:'Radio Button',Value:'5'}];


$(document).ready(function(){
    parentRef = parent.frames['iframe'].window;
    patientId = parentRef.patientId;
    sessionStorage.setItem("IsSearchPanel", "1");


    var dataOptionsTaskGroup = {
        pagination: false,
        changeCallBack: onChangeAppointmentReason
    }
    angularUIgridWrapper1 = new AngularUIGridWrapper("dgridActivTypeList1", dataOptionsTaskGroup);
    angularUIgridWrapper1.init();
    buildStateListGrid([]);

    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("dgAvlPatientActivityList", dataOptions);
    angularUIgridWrapper.init();
    buildFileResourceListGrid([]);

    var dataOptions1 = {
        pagination: false,
        changeCallBack: onChange1
    }
    angularUISelgridWrapper = new AngularUIGridWrapper("dgSelPatientActivityList1", dataOptions1);
    angularUISelgridWrapper.init();
    buildFileResourceSelListGrid([]);
});


$(window).load(function(){
    $(window).resize(adjustHeight);
    //loadMenus();
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function loadMenus(){
    var strMenu = "";
    $("#setupPlanMenu").html("");
    for(var i=0;i<typeArr.length;i++){
        var item = typeArr[i];
        var strItem = item.Key;
        var strVal = item.Value;
        if(i == 0){
            strMenu = strMenu+'<a href="#" class="list-group-item sub-item paddingLeftStyle subMenuStyle selectedMenu" id='+strVal+' name='+strVal+'>'+strItem+'</a>';
            activityid = strVal;
        }else{
            strMenu = strMenu+'<a href="#" class="list-group-item sub-item paddingLeftStyle subMenuStyle" id='+strVal+' name='+strVal+'>'+strItem+'</a>';
        }

    }
    //console.log(strItem);
    $("#setupPlanMenu").append(strMenu);
    for(var j=0;j<typeArr.length;j++){
        var item = typeArr[j];
        var strItem = item.Key;
        var strVal = item.Value;
        $("#"+strVal).off("click");
        $("#"+strVal).on("click",onClickItem);
    }
}
function onClickItem(evt){
    //console.log(evt);
    removeSelections();
    var strId = evt.currentTarget.id;
    $("#"+strId).addClass("selectedMenu");
    var strId = evt.currentTarget.name;
    var strActivity = getActivityNameById(strId);
    $("#lblActivity").text(strActivity);
    activityid = strId;
    showGrid();
    //init();
}
function removeSelections(){
    for(var i=0;i<typeArr.length;i++){
        var item = typeArr[i];
        var strItem = item.Key;
        var strVal = item.Value;
        $("#"+strVal).removeClass("selectedMenu");
    }
}
function onLoaded(){
    $("#cmbAppointmentReason").kendoComboBox();
    init();
    getAjaxObject(ipAddress+"/homecare/activity-types/?is-active=1&is-deleted=0&sort=type","GET",getTaskTypes,onError);
    var url = ipAddress+"/master/appointment_reason/list/?is-active=1&is-deleted=0";
    getAjaxObject(url,"GET",handleGetTemplateList,onError);

    buttonEvents();
    adjustHeight();
}

var recordType = "";
function init(){
    getAjaxObject(ipAddress+"/master/type/list/?name=component","GET",getComponentTypes,onError);
}
function getComponentTypes(dataObj){
    var tempCompType = [];
    compType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.typeMaster){
            if($.isArray(dataObj.response.typeMaster)){
                tempCompType = dataObj.response.typeMaster;
            }else{
                tempCompType.push(dataObj.response.typeMaster);
            }
        }
    }
    for(var i=0;i<tempCompType.length;i++){
        var obj = {};
        obj.Key = tempCompType[i].type;
        obj.Value = tempCompType[i].id;
        compType.push(obj);
    }
    getAjaxObject(ipAddress+"/master/type/list/?name=activity","GET",getActivityTypes,onError);
}

function getActivityTypes(dataObj){
    var tempCompType = [];
    typeArr = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.typeMaster){
            if($.isArray(dataObj.response.typeMaster)){
                tempCompType = dataObj.response.typeMaster;
            }else{
                tempCompType.push(dataObj.response.typeMaster);
            }
        }
    }
    tempCompType.sort(function(a, b) {
        var nameA = a.type.toUpperCase(); // ignore upper and lowercase
        var nameB = b.type.toUpperCase(); // ignore upper and lowercase
        if (nameA < nameB) {
            return -1;
        }
        if (nameA > nameB) {
            return 1;
        }
        // names must be equal
        return 0;
    });

    for(var i=0;i<tempCompType.length;i++){
        var obj = {};
        obj.Key = tempCompType[i].type;
        obj.Value = tempCompType[i].id;
        typeArr.push(obj);
    }
    // if(!menuLoaded){
    //     loadMenus();
    // }
    showGrid();
}
function showGrid(){
    buildFileResourceListGrid([]);
    buildFileResourceSelListGrid([]);
    getAjaxObject(ipAddress+"/activity/list?is-active=1&is-deleted=0&activity-type-id="+ activityid +"&sort=activity","GET",getStateList,onError);
}

function onError(errorObj){
    //console.log(errorObj);
}
var interestArr = [];
var dataArray = [];


function getActivityList(dataObj){
    //console.log(dataObj);
    dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if($.isArray(dataObj.response.activity)){
            dataArray = dataObj.response.activity;
        }else{
            dataArray.push(dataObj.response.activity);
        }
    }
    var tempDataArray = [];
    var reasonDataArray = [];
    var cmbAppointmentReason = $("#cmbAppointmentReason").data("kendoComboBox");

    for(var i=0;i<dataArray.length;i++){
        if(dataArray[i].id && dataArray[i].activityTypeId == activityid && dataArray[i].isDeleted == 0){
            dataArray[i].idk = dataArray[i].id;
            dataArray[i].Status = "InActive";
            if(dataArray[i].isActive == 1){
                dataArray[i].Status = "Active";
            }
            dataArray[i].activityID = getActivityNameById(dataArray[i].activityTypeId);
            dataArray[i].Type = getTypeNameById(dataArray[i].componentId);
            if(!getFileExist(dataArray[i].id)){
                if(dataArray[i].appointmentReasonId == Number(cmbAppointmentReason.value())){
                    reasonDataArray.push(dataArray[i]);
                }else{
                    tempDataArray.push(dataArray[i]);
                }
            }
        }
    }
    buildFileResourceSelListGrid(tempDataArray);
    buildFileResourceListGrid(reasonDataArray);
}

function getActivityNameById(aId){
    for(var i=0;i<typeArr.length;i++){
        var item = typeArr[i];
        if(item && item.Value == aId){
            return item.Key;
        }
    }
    return "";
}

function getComponentNameById(aId){
    for(var i=0;i<actComponents.length;i++){
        var item = actComponents[i];
        if(item && item.componentId == aId){
            return item.activity;
        }
    }
    return "";
}


function getTypeNameById(aId){
    for(var i=0;i<compType.length;i++){
        var item = compType[i];
        if(item && item.Value == aId){
            return item.Key;
        }
    }
    return "";
}
function getFileExist(id1){
    var flag = false;
    for(var i=0;i<interestArr.length;i++){
        var dataObj = interestArr[i];
        if(dataObj && dataObj.activityId == id1){
            flag = true;
            break;
        }
    }
    return flag;
}
function buttonEvents(){
    $("#btnCancelDet").off("click",onClickCancel);
    $("#btnCancelDet").on("click",onClickCancel);

    $("#btnAdd").off("click");
    $("#btnAdd").on("click",onClickAdd);

    $("#btnSubRight").off("click");
    $("#btnSubRight").on("click",onClickSubRight);

    $("#btnSubLeft").off("click");
    $("#btnSubLeft").on("click",onClickSubLeft);

    $("#btnADL").off("click",onClickAddADL);
    $("#btnADL").on("click",onClickAddADL);

    $("#btniADL").off("click",onClickAddiADL);
    $("#btniADL").on("click",onClickAddiADL);

    $("#btnCare").off("click",onClickAddCare);
    $("#btnCare").on("click",onClickAddCare);

    $("#btnFluid").off("click",onClickAddFluid);
    $("#btnFluid").on("click",onClickAddFluid);

    $("#btnFood").off("click",onClickAddFood);
    $("#btnFood").on("click",onClickAddFood);

}

function onClickAddADL(){
    $("#btnADL").addClass("selectButtonBarClass");
    $("#btniADL").removeClass("selectButtonBarClass");
    $("#btnCare").removeClass("selectButtonBarClass");
    $("#btnFluid").removeClass("selectButtonBarClass");
    $("#btnFood").removeClass("selectButtonBarClass");
    activityid = ADL;
    init();
}
function onClickAddiADL(){
    $("#btnADL").removeClass("selectButtonBarClass");
    $("#btniADL").addClass("selectButtonBarClass");
    $("#btnCare").removeClass("selectButtonBarClass");
    $("#btnFluid").removeClass("selectButtonBarClass");
    $("#btnFood").removeClass("selectButtonBarClass");
    activityid = IADL;
    init();
}
function onClickAddCare(){
    $("#btnADL").removeClass("selectButtonBarClass");
    $("#btniADL").removeClass("selectButtonBarClass");
    $("#btnCare").addClass("selectButtonBarClass");
    $("#btnFluid").removeClass("selectButtonBarClass");
    $("#btnFood").removeClass("selectButtonBarClass");
    activityid = CARE;
    init();
}
function onClickAddFluid(){
    $("#btnADL").removeClass("selectButtonBarClass");
    $("#btniADL").removeClass("selectButtonBarClass");
    $("#btnCare").removeClass("selectButtonBarClass");
    $("#btnFluid").addClass("selectButtonBarClass");
    $("#btnFood").removeClass("selectButtonBarClass");
    activityid = FLUID;
    init();
}
function onClickAddFood(){
    $("#btnADL").removeClass("selectButtonBarClass");
    $("#btniADL").removeClass("selectButtonBarClass");
    $("#btnCare").removeClass("selectButtonBarClass");
    $("#btnFluid").removeClass("selectButtonBarClass");
    $("#btnFood").addClass("selectButtonBarClass");
    activityid = FOOD;
    init();
}
function adjustHeight(){
    var defHeight = 170;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    if(angularUIgridWrapper){
        angularUIgridWrapper.adjustGridHeight(cmpHeight);
    }
    if(angularUISelgridWrapper){
        angularUISelgridWrapper.adjustGridHeight(cmpHeight);
    }

    $("#divButttons").height(cmpHeight+50);
}
function toggleSelectAll(e) {
    var checked = e.currentTarget.checked;
    var rows = angularUIgridWrapper.getScope().gridApi.core.getVisibleRows();
    for (var i = 0; i < rows.length; i++) {
        rows[i].entity["SEL"] = checked;
    }
}
function toggleSelectAll1(e) {
    var checked = e.currentTarget.checked;
    var rows = angularUISelgridWrapper.getScope().gridApi.core.getVisibleRows();
    for (var i = 0; i < rows.length; i++) {
        rows[i].entity["SEL"] = checked;
    }
}
function buildFileResourceListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Select",
        "field": "SEL",
        "cellTemplate": showCheckBoxTemplate(),
        "headerCellTemplate": "<input type='checkbox' class='check1' ng-model='TCSELECT' onclick='toggleSelectAll(event);' style='width:20px;height:20px;'></input>",
        "width":"15%"
    });
    gridColumns.push({
        "title": "Component",
        "field": "activity",
    });
    /*gridColumns.push({
     "title": "Charge",
     "field": "charge",
     });
     gridColumns.push({
     "title": "Duration",
     "field": "duration",
     });*/
    gridColumns.push({
        "title": "Required",
        "field": "REQ",
        "headerCellTemplate": "<div><span>Required</span><br /><input type='checkbox' class='check1' ng-model='TCSELEREQ' onclick='toggleReqAll(event);' style='width:20px;height:20px;'></input></div>",
        "cellTemplate": showRequireCheckBoxTemplate(),
    });
    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}
function showReqHeaderCheckBoxTemplate(){
    var node = '<div style="text-align:center;padding-top: 6px;"><input type="checkbox" class="check1" id="chkbox1" ng-model="row.entity.REQ" style="width:20px;height:20px;margin:0px;"   onclick="onReqSelect(event);"></input></div>';
    return node;
}
function toggleReqAll(event){
    var checked = event.currentTarget.checked;
    var rows = angularUIgridWrapper.getScope().gridApi.core.getVisibleRows();
    for (var i = 0; i < rows.length; i++) {
        rows[i].entity["REQ"] = checked;
    }
}
function showRequireCheckBoxTemplate(){
    var node = '<div style="text-align:center;padding-top: 6px;"><input type="checkbox" class="check1" id="chkbox" ng-model="row.entity.REQ" style="width:20px;height:20px;margin:0px;"   onclick="onSelect(event);"></input></div>';
    return node;
}
var prevSelectedItem =[];
function onChange(){

}
function buildFileResourceSelListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Select",
        "field": "SEL",
        "cellTemplate": showCheckBoxTemplate(),
        "headerCellTemplate": "<input type='checkbox' class='check1' ng-model='TCSELECT' onclick='toggleSelectAll1(event);' style='width:20px;height:20px;'></input>",
        "width":"15%"
    });
    gridColumns.push({
        "title": "Component",
        "field": "activity",
    });
    /*gridColumns.push({
     "title": "Charge",
     "field": "charge",
     });
     gridColumns.push({
     "title": "Duration",
     "field": "duration",
     });*/
    angularUISelgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();

    var selGridData = angularUIgridWrapper.getAllRows();
    angularUISelgridWrapper.deleteItem(selGridData);

}
var prevSelectedItem =[];
function onChange1(){

}

function showCheckBoxTemplate(){
    var node = '<div style="text-align:center;padding-top: 6px;"><input type="checkbox" class="check1" id="chkbox" ng-model="row.entity.SEL" style="width:20px;height:20px;margin:0px;"   onclick="onSelect(event);"></input></div>';
    return node;
}
function onSelect(evt){
    //console.log(evt);
}

function onClickSubRight(){
//	alert("right");
    var selGridData = angularUISelgridWrapper.getAllRows();
    var selList = [];
    var cmbAppointmentReason = $("#cmbAppointmentReason").data("kendoComboBox");
    for (var i = 0; i < selGridData.length; i++) {
        selGridData[i].entity.appointmentReasonId = Number(cmbAppointmentReason.value());
        var dataRow = selGridData[i].entity;
        if(dataRow.SEL){
            angularUISelgridWrapper.deleteItem(dataRow);
            //dataRow.SEL = false;
            angularUIgridWrapper.insert(dataRow);
        }
    }
}
function onClickSubLeft(){
    //alert("click");
    var selGridData = angularUIgridWrapper.getAllRows();
    var selList = [];
    for (var i = 0; i < selGridData.length; i++) {
        var dataRow = selGridData[i].entity;
        if(dataRow.SEL){
            angularUIgridWrapper.deleteItem(dataRow);
            angularUISelgridWrapper.insert(dataRow);
        }
    }
}

function popupClose(st){
    var onCloseData = new Object();
    onCloseData = st;
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(onCloseData);
}
function onClickCancel(){
    var obj = {};
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}

function onChangeAppointmentReason(){
    buildFileResourceListGrid([]);
    buildFileResourceSelListGrid([]);
    setTimeout(function(){
        var selectedItems = angularUIgridWrapper1.getSelectedRows();
        if(selectedItems && selectedItems.length>0){
            activityid = selectedItems[0].idk;
            showGrid();
        }else{
            $("#btnSave").prop("disabled", true);
            $("#btnDelete").prop("disabled", true);
        }
    },100)

}

function buildStateListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Group",
        "field": "type",
    });
    angularUIgridWrapper1.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
}

function getTaskTypes(dataObj){
    console.log(dataObj);

    var types = [];
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            if(dataObj.response.activityTypes){
                if($.isArray(dataObj.response.activityTypes)){
                    types = dataObj.response.activityTypes;
                }else{
                    types.push(dataObj.response.activityTypes);
                }
            }
            for(var i=0;i<types.length;i++){
                types[i].idk = types[i].id;
            }
        }
    }
    buildStateListGrid(types);
}

var actComponents;
function getStateList(dataObj){
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.activity){
        if($.isArray(dataObj.response.activity)){
            dataArray = dataObj.response.activity;
        }else{
            dataArray.push(dataObj.response.activity);
        }
    }
    var cmbAppointmentReason = $("#cmbAppointmentReason").data("kendoComboBox");
    var tempDataArray  = [];
    var reasonDataArray = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        if(dataArray[i].required){
            dataArray[i].REQ = true;
        }else{
            dataArray[i].REQ = false;
        }
        tempDataArray.push(dataArray[i]);
    }
    actComponents = dataArray;
    var listArray = dataArray;

    var cmbAppointmentReason = $("#cmbAppointmentReason").data("kendoComboBox");
    var appointmentReasonId = Number(cmbAppointmentReason.value());

    var values;
    for(var j=0;j<=(tempDataArray.length-1);j++) {
        var item = tempDataArray[j];
        if (appointmentReasonDataArray) {
            var FilappointmentReasonDataArray = _.where(appointmentReasonDataArray, {appointmentReasonId: appointmentReasonId})
            for (var i = 0; i <=(FilappointmentReasonDataArray.length-1); i++) {
                if (FilappointmentReasonDataArray[i].activityId == item.idk && FilappointmentReasonDataArray[i].isActive == 1) {
                    tempDataArray[j].appidk = FilappointmentReasonDataArray[i].idk;
                    if(FilappointmentReasonDataArray[i].required){
                        tempDataArray[j].REQ = true;
                    }else{
                        tempDataArray[j].REQ = false;
                    }
                    reasonDataArray.push(tempDataArray[j]);
                }
            }
        }
    }
    var val = _.difference(listArray, reasonDataArray);

    // removeByIndex(listArray, j);

    // for(var j=0;j<tempDataArray.length;j++) {
    //     var item = tempDataArray[j];
    //     if (appointmentReasonDataArray) {
    //         for (var i = 0; i < appointmentReasonDataArray.length; i++) {
    //             if (appointmentReasonDataArray[i].activityId == item.idk && appointmentReasonDataArray[i].appointmentReasonId != appointmentReasonId) {
    //                 listArray.push(tempDataArray[j]);
    //                 break;
    //             }
    //         }
    //     }
    // }
    // for(var j=0;j<reasonDataArray.length;j++) {
    //
    // }



    buildFileResourceListGrid(reasonDataArray);
    buildFileResourceSelListGrid(val);
}

var appointmentReasonDataArray = [];
function getAppointmentReasonList(dataObj){
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.appointmentReasonActivities){
        if($.isArray(dataObj.response.appointmentReasonActivities)){
            dataArray = dataObj.response.appointmentReasonActivities;
        }else{
            dataArray.push(dataObj.response.appointmentReasonActivities);
        }
    }

    var tempDataArray  = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        tempDataArray.push(dataArray[i]);
    }
    appointmentReasonDataArray = tempDataArray;

    // buildFileResourceListGrid(tempDataArray);
}

function handleGetTemplateList(dataObj){
    console.log(dataObj);
    var vacation = [];
    if(dataObj && dataObj.response && dataObj.response.codeTable ){
        if(dataObj.response.status.code == "1"){
            if(dataObj.response.codeTable){
                if($.isArray(dataObj.response.codeTable)){
                    vacation = dataObj.response.codeTable;
                }else{
                    vacation.push(dataObj.response.codeTable);
                }
            }
        }
    }
    for(var j=0;j<vacation.length;j++){
        vacation[j].idk = vacation[j].id;
    }
    setDataForSelection(vacation, "cmbAppointmentReason", onTemplateChange, ["desc", "idk"], 0, "");
    var cmbAppointmentReason = $("#cmbAppointmentReason").data("kendoComboBox");
    var appointmentReasonId = Number(cmbAppointmentReason.value());
    getAjaxObject(ipAddress+"/homecare/appointment-reason-activities/?fields=*,activity.*","GET",getAppointmentReasonList,onError);
    // /homecare/appointment-reason-activities/?access_token=37ff49f0-b4b2-4c9c-b272-d13408e71fbf&fields=*,activity.*&appointment-reason-id=9
}

function onTemplateChange() {
}

function removeByIndex(arr, index) {
    arr.splice(index, 1);
}
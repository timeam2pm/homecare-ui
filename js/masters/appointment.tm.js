var angularUIgridWrapper = null;
var activeListFlag = true;
var selectedItems;
var ADD = "add";
var UPDATE = "edit";
var VIEW = "view";
var DELETE = "delete";
var operation = "add";
var atID = "";
var parentRef = null;
var sessionAppointmentValue = sessionStorage.sessionAppointmentValue;


function bindEvents() {
    $("#btnAdd").off("click");
    $("#btnAdd").on("click",onClickAdd);

    $("#btnCancel").off("click",onClickCancel);
    $("#btnCancel").on("click",onClickCancel);

    $("#btnReset").off("click",onClickReset);
    $("#btnReset").on("click",onClickReset);

    $(".popupClose").off("click", onClickCancel);
    $(".popupClose").on("click", onClickCancel);

    $('#btnSave').on('click',function(e) {
        e.preventDefault();
        var urlExtn = getAppointmentForm() + '/create';
        var activeStatusVal = $("#cmbStatus").val();
        var abbreviationVal = $("#abbreviation").val();
        var descriptionVal = $("#description").val();
        var IsDeleted = 0;
        if(activeStatusVal == 1){
            IsDeleted = 0;
        }else{
            IsDeleted = 1;
        }

        var dataObj = {
            "isActive": activeStatusVal,
            "value": abbreviationVal,
            "desc": descriptionVal,
            "note": null,
            "isDeleted": IsDeleted
        }

        if(parentRef.operation == "edit"){
            dataObj.id = atID;
            dataObj.modifiedBy = sessionStorage.userId;
            urlExtn = getAppointmentForm() + '/update';
        }
        else{
            dataObj.createdBy= sessionStorage.userId;
        }

        if(abbreviationVal != "" && activeStatusVal != "" && descriptionVal != "") {
            var dataUrl = ipAddress+urlExtn;
            createAjaxObject(dataUrl,dataObj,"POST",onCreate,onError);
        }
        else {
            customAlert.error("Error","Please fill the Required Fields");
        }
    });

    $("#btnDelete").on('click',function(e) {
        e.preventDefault();


        getAjaxObject(ipAddress+"/homecare/activity-types/?is-active=1&is-deleted=0&sort=type&appointment-reason-id="+selectedItems[0].idDuplicate,"GET",deleteTaskTypes,onError);
    });

    // $("#btnEdit").off('click',"");
    $("#btnEdit").on('click',function(e) {
        e.preventDefault();

        $("#txtID").show();
        parentRef.operation = "edit";
        titlename();
        // getAppointmentForm();
        $("#viewDivBlock").hide();
        $("#addPopup").show();
        $("#abbreviation").val(selectedItems[0].value);
        $("#description").val(selectedItems[0].desc);
        $("#cmbStatus").val(selectedItems[0].isActive);
        $("#txtID").html("ID: "+ atID);
    });


    $(".btnActive").on("click", function(e) {
        e.preventDefault();
        activeListFlag = true;
        searchOnLoad();
        onClickActive();
    });
    $(".btnInActive").on("click", function(e) {
        e.preventDefault();
        activeListFlag = false;
        searchOnLoad();
        onClickInActive();
    });
}
function getAppointmentForm() {
    var formUrl;
    if(sessionAppointmentValue.toLowerCase() == "status") {
        formUrl = '/master/appointment_type/';
        $(".filter-heading").html("View Appointment Status");
    }
    else if(sessionAppointmentValue.toLowerCase() == "reason") {
        formUrl = '/master/appointment_reason';
        $(".filter-heading").html("View Appointment Reason");
    }
    return formUrl;
}
function searchOnLoad() {
    buildAppointmentListGrid([]);
    var urlExtn;
    if(activeListFlag) {
        urlExtn = getAppointmentForm() + '/list/?is-active=1&is-deleted=0';
    }
    else{
        urlExtn = getAppointmentForm() + '/list/?is-active=0&is-deleted=1';
    }
    getAjaxObject(ipAddress+urlExtn,"GET",getSearchList,onSearchError);
}
function onCreate(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1") {
            var msg = "Created Successfully";
            if(operation == UPDATE){
                msg = "Updated Successfully"
            }
            displaySessionErrorPopUp("Info", msg, function(res) {
                operation = ADD;
                searchOnLoad();
                onClickCancel();
                onClickActive();
            })
        }
        else {
            customAlert.error("Error", dataObj.response.status.message);
        }
    }
}
function onError(errObj){
    console.log(errObj);
    customAlert.error("Error","Error");
}
function onDeleteItem(respObj) {
    console.log(respObj);
    if(respObj && respObj.response && respObj.response.status){
        if(respObj.response.status.code == "1") {
            searchOnLoad();
            var msg = "Deleted successfully";
            customAlert.info("Info", msg);
        }
        else {
            customAlert.error("Error", "Error");
        }
    }
}
function onDeleteError(errObj){
    console.log(errObj);
    customAlert.error("Error","Error");
}

var dataActiveList=[], dataInActiveList=[], dataListArr = [];
function getSearchList(resp) {
    var dataArray, dataActiveList=[], dataInActiveList=[];
    if(resp && resp.response && resp.response.codeTable){
        if($.isArray(resp.response.codeTable)){
            dataArray = resp.response.codeTable;
        }else{
            dataArray.push(resp.response.codeTable);
        }
    }
    if(dataArray != undefined) {
        for(var i=0;i<dataArray.length;i++){
            dataArray[i].idDuplicate = dataArray[i].id;
            if(dataArray[i].isActive == 1){
                dataArray[i].Status = "Active";
                dataActiveList.push(dataArray[i]);
            }
            else {
                dataArray[i].Status = "InActive";
                dataInActiveList.push(dataArray[i]);
            }
        }
    }
    if(activeListFlag) {
        buildAppointmentListGrid(dataActiveList);
    }
    else if(!activeListFlag) {
        buildAppointmentListGrid(dataInActiveList);
    }
}
function onSearchError(err) {
    console.log(err);
    customAlert.error("Error","Error");
}
function buildAppointmentListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Abbreviation",
        "field": "value",
    });
    gridColumns.push({
        "title": "Description",
        "field": "desc",
    });
    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
}
function adjustHeight(){
    var defHeight = 220;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    angularUIgridWrapper.adjustGridHeight(cmpHeight);
}
function onChange(){
    setTimeout(function() {
        selectedItems = angularUIgridWrapper.getSelectedRows();
        /*console.log(selectedItems);*/
        disableButtons();
    });
}
function disableButtons() {
    if (selectedItems && selectedItems.length > 0) {
        var obj = selectedItems[0];
        atID = obj.idDuplicate;
        $("#btnEdit").prop("disabled", false);
        $("#btnDelete").prop("disabled", false);
    }else{
        $("#btnEdit").prop("disabled", true);
        $("#btnDelete").prop("disabled", true);
    }
}
function onClickActive() {
    $(".btnInActive").removeClass("radioButton-active");
    $(".btnActive").addClass("radioButton-active");
}

function onClickInActive() {
    $(".btnActive").removeClass("radioButton-active");
    $(".btnInActive").addClass("radioButton-active");
}

function init() {
    bindEvents();
}
$(document).ready(function() {
    $("#pnlPatient",parent.document).css("display","none");
    themeAPIChange();
    parentRef = parent.frames['iframe'].window;

    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("appointmentList", dataOptions);
    angularUIgridWrapper.init();

    init();
});
$(window).load(function(){
    // loading = false;
    windowLoad = true;
    searchOnLoad();

});


function themeAPIChange(){
    getAjaxObject(ipAddress+"/homecare/settings/?id=2","GET",getThemeValue,onError);
}


function getThemeValue(dataObj){
    console.log(dataObj);
    var tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.activityTypes){
            if($.isArray(dataObj.response.settings)){
                tempCompType = dataObj.response.settings;
            }else{
                tempCompType.push(dataObj.response.settings);
            }
        }
        if(tempCompType.length > 0){
            themesChange(tempCompType[0].value);
        }
    }
}

function themesChange(themeValue){
    if(themeValue == 2){
        loadAPi("../../Theme2/Theme02.css");
    }
    else if(themeValue == 3){
        loadAPi("../../Theme3/Theme03.css");
    }
}

function onClickAdd(){
    $("#addPopup").show();
    $('#viewDivBlock').hide();
    $("#txtID").hide();
    parentRef.operation = "add";
    operation = ADD;
    titlename();
    onClickReset();


}

function onClickReset() {
    if(operation == ADD) {
        operation = ADD;
    }
    else {
        operation = UPDATE;
    }
    parentRef.operation == "add";
    $("#abbreviation").val("");
    $("#description").val("");
    $("#cmbStatus").val(1);

}

function onClickCancel(){
    if(sessionAppointmentValue.toLowerCase() == "status") {
        $(".filter-heading").html("View Appointment Status");
    }
    else if(sessionAppointmentValue.toLowerCase() == "reason") {
        $(".filter-heading").html("View Appointment Reason");
    }
    $("#viewDivBlock").show();
    $("#addPopup").hide();
    parentRef.operation == "add";
}


function titlename(){
    if(sessionAppointmentValue.toLowerCase() == "status") {
        if(parentRef.operation == "add") {
            $(".filter-heading").html("Add Appointment Status");
        }
        else{
            $(".filter-heading").html("Edit Appointment Status");
        }
    }
    else if(sessionAppointmentValue.toLowerCase() == "reason") {
        if(parentRef.operation == "add") {
            $(".filter-heading").html("Add Appointment Reason");
        }
        else{
            $(".filter-heading").html("Edit Appointment Reason");
        }
    }
}

function deleteTaskTypes(dataObj) {
    var types = [];
    var obj = [];
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            if (dataObj.response.activityTypes) {
                if ($.isArray(dataObj.response.activityTypes)) {
                    types = dataObj.response.activityTypes;
                } else {
                    types.push(dataObj.response.activityTypes);
                }
            }
            for (var i = 0; i < types.length; i++) {
                var dataObj = {};
                dataObj.createdBy = Number(sessionStorage.userId);
                dataObj.isDeleted = 1;
                dataObj.isActive = 0;
                dataObj.id = types[i].id;
                obj.push(dataObj);
            }
            var dataUrl = ipAddress +"/homecare/activity-types/batch/";
            createAjaxObject(dataUrl, obj, "PUT", onCreateDeleteReason, onError);
        }
    }
}
function onCreateDeleteReason(dataObj) {
    var deleteItemObj = {
        "id": selectedItems[0].idDuplicate,
        "isActive": 0,
        "isDeleted": 1,
        "modifiedBy": sessionStorage.userId
    }
    var urlExtn = getAppointmentForm() + '/delete';
    var dataUrl = ipAddress+urlExtn;
    createAjaxObject(dataUrl,deleteItemObj,"POST",onDeleteItem,onDeleteError);
}
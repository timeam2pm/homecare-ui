var angularUIgridWrapper;

$(document).ready(function(){
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("dgridUserList", dataOptions);
    angularUIgridWrapper.init();
    buildScreenUserList([]);
});

$(window).load(function(){
    loading = false;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
    init();
    buttonEvents();
    adjustHeight();
}

function init(){
    //getAjaxObject(ipAddress+"/codetable/list/","GET",getCountryList,onError);
    var panelHeight = window.innerHeight-100;
    $("#divScreenList").height(panelHeight);
    $("#divScreenTreeList").height(panelHeight);
    $("#treeview").height(panelHeight-15);

    var userList = [];
    userList[0] = {uName:'Nurse1',Create:true,Delete:false,View:true};
    userList[1] = {uName:'Krishna',Create:true,Delete:true,View:true};
    userList[2] = {uName:'Nurse2',Create:false,Delete:false,View:true};
    buildScreenUserList(userList);

    getScreenListTree()
}
function getScreenListTree(){
    var tree = $("#treeview").data("kendoTreeView");
    if(tree){
        tree.destroy();
    }
    $("#treeview").kendoTreeView({
        change:onTreeItemSelect,
        dataSource: [
            { text: "Patient", menu:"true",expanded: false, items: [
                    { text: "Add Patient",menu:"false" },
                    { text: "Search Patient",menu:"false" },
                    { text: "Patient Call Chart",menu:"false" }
                ] },
            { text: "Master",menu:"true",items: [
                    { text: "Accounts",menu:"true",items:[{text:'Add Account',menu:"false"},{text:'Search Account',menu:"false"}] },
                    { text: "Facility",menu:"true",items:[{text:'Add Facility',menu:"false"},{text:'Search Facility',menu:"false"}] },
                    { text: "Doctor",menu:"true",items:[{text:'Add Doctor',menu:"false"},{text:'Search Doctor',menu:"false"}] },
                    { text: "Insurence Company",menu:"true",items:[{text:'Add Insurence Company',menu:"false"},{text:'Search Insurence Company',menu:"false"}] },
                    { text: "Employeer",menu:"true",items:[{text:'Add Employeer',menu:"false"},{text:'Search Employeer',menu:"false"}] },
                    { text: "Insurence Plan",menu:"true",items:[{text:'Add Insurence Plan',menu:"false"},{text:'Search Insurence Plan',menu:"false"}] },
                    { text: "Setup",menu:"true",items:[{text:'User Creation',menu:"false"},{text:'User Roles',menu:"false"},{text:'Screen Permissions',menu:"false"},{text:'Patient User Map',menu:"false"},{text:'Admin',menu:"false"},{text:'Call Routing',menu:"false"},{text:'Call Log',menu:"false"}] },
                    { text: "Code Table",menu:"true",items:[{text:'Create Table',menu:"false"},{text:'Search Table',menu:"false"},{text:'Add Values',menu:"false"},{text:'Search Values',menu:"false"}] },
                    { text: "Communications",menu:"true",items:[{text:'Country',menu:"true",items:[{text:'Add Country',menu:"false"},{text:'Search Country',menu:"false"}]},
                            {text:'County',menu:"false",items:[{text:'Add County',menu:"false"},{text:'Search County',menu:"false"}]},
                            {text:'State',menu:"false",items:[{text:'Add State',menu:"false"},{text:'Search State',menu:"false"}]},
                            {text:'Zip',menu:"false",items:[{text:'Add Zip',menu:"false"},{text:'Search Zip',menu:"false"}]}]},
                    { text: "File Resource",menu:"true",items:[{text:'Add Resource',menu:"false"},{text:'Search Resource',menu:"false"}] },
                    { text: "ICD",menu:"true",items:[{text:'Add ICD',menu:"false"},{text:'Search ICD',menu:"false"}] },
                    { text: "CPT",menu:"true",items:[{text:'Add CPT',menu:"false"},{text:'Search CPT',menu:"false"}] },
                    { text: "SnoMed",menu:"true",items:[{text:'Add SnoMed',menu:"false"},{text:'Search SnoMed',menu:"false"}] },
                    { text: "Appointment Types",menu:"true",items:[{text:'Add Appointment Types',menu:"false"},{text:'Search Appointment Types',menu:"false"}] },
                    { text: "Detail Codes",menu:"true",items:[{text:'Add Detail Codes',menu:"false"},{text:'Search Detail Codes',menu:"false"}] }

                ] },
            { text: "Setup", menu:"true",expanded: false, items: [
                    { text: "Admin",menu:"true",items:[{text:'User Creation',menu:"false"},{text:'User List',menu:"false"},{text:'User Roles',menu:"false"},{text:'Screen Permissions',menu:"false"},{text:'Patient User Mapping',menu:"false"},{text:'CallRouting',menu:"false"},{text:'Call Log',menu:"false"},{text:'Spedd Test',menu:"false"}] },

                ] }
        ]
    });
}
function onTreeItemSelect(e){
    console.log(e);
    var selItem = getSelectTreeNode("treeview");
    console.log(selItem);
}
function getSelectTreeNode(treeId){
    var tv = $('#'+treeId).data('kendoTreeView');
    if(tv){
        var selTreeItem = tv.dataItem(tv.select());
        if(selTreeItem){
            return selTreeItem;
        }
    }
    return null;
}
function onError(errorObj){
    console.log(errorObj);
}
function getCountryList(dataObj){
    console.log(dataObj);
    var dataArray = [];
    if(dataObj){
        if($.isArray(dataObj.response)){
            dataArray = dataObj.response;
        }else{
            dataArray.push(dataObj.response);
        }
    }
    var tempDataArry = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
        }
    }
    buildCodeTableListGrid(dataArray);
}
function buttonEvents(){
    $("#btnSave").off("click",onClickOK);
    $("#btnSave").on("click",onClickOK);

    $("#btnCancel").off("click",onClickCancel);
    $("#btnCancel").on("click",onClickCancel);

}

function adjustHeight(){
    var defHeight = 100;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    angularUIgridWrapper.adjustGridHeight(cmpHeight);
}

function buildScreenUserList(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "User Name",
        "field": "uName",
    });
    gridColumns.push({
        "title": "Create",
        "field": "Create",
        "width":"20%",
        "cellTemplate": showCreateCheckBoxTemplate()
    });
    gridColumns.push({
        "title": "Delete",
        "field": "Delete",
        "width":"20%",
        "cellTemplate": showDeleteCheckBoxTemplate()
    });
    gridColumns.push({
        "title": "View",
        "field": "View",
        "width":"20%",
        "cellTemplate": showViewCheckBoxTemplate()
    });
    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}
function showCreateCheckBoxTemplate(){
    var node = '<div style="text-align:center;padding-top: 6px;"><input type="checkbox" class="check1" id="chkbox" ng-model="row.entity.Create" style="width:20px;height:20px;margin:0px"></input></div>';
    return node;
}
function showDeleteCheckBoxTemplate(){
    var node = '<div style="text-align:center;padding-top: 6px;"><input type="checkbox" class="check1" id="chkbox" ng-model="row.entity.Delete" style="width:20px;height:20px;margin:0px"></input></div>';
    return node;
}
function showViewCheckBoxTemplate(){
    var node = '<div style="text-align:center;padding-top: 6px;"><input type="checkbox" class="check1" id="chkbox" ng-model="row.entity.View" style="width:20px;height:20px;margin:0px"></input></div>';
    return node;
}
var prevSelectedItem =[];
function onChange(){

}

function onClickOK(){
    setTimeout(function(){
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if(selectedItems && selectedItems.length>0){
            var obj = {};
            obj.selItem = selectedItems[0];
            // var onCloseData = new Object();
            obj.status = "success";
            obj.operation = "ok";
            var windowWrapper = new kendoWindowWrapper();
            windowWrapper.closePageWindow(obj);
        }
    })
}
function onClickCancel(){
    var obj = {};
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}

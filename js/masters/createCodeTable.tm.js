var parentRef = null;
var operation = "";
var selItem = null;
var ADD = "add";
var UPDATE = "update";
var VIEW = "view";
var DELETE = "delete";
var statusArr = [{Key:'Active',Value:'Active'},{Key:'InActive',Value:'InActive'}];
$(document).ready(function(){
    parentRef = parent.frames['iframe'].window;
});

function adjustHeight(){
    var defHeight = 120;
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 40;
    }
}

$(window).load(function(){
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
    init();
});

function init(){
    operation = parentRef.operation;
    selItem = parentRef.selItem;
    $("#cmbStatus").kendoComboBox();
    setDataForSelection(statusArr, "cmbStatus", onStatusChange, ["Value", "Key"], 0, "");
    var  cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if(cmbStatus){
        cmbStatus.enable(false);
    }
    buttonEvents();
}
function onStatusChange(){
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if(cmbStatus && cmbStatus.selectedIndex<0){
        cmbStatus.select(0);
    }
}

function onClickActive() {
    $(".btnInActive").removeClass("selectButtonBarClass");
    $(".btnActive").addClass("selectButtonBarClass");
}

function onClickInActive() {
    $(".btnActive").removeClass("selectButtonBarClass");
    $(".btnInActive").addClass("selectButtonBarClass");
}

function buttonEvents(){
    $("#btnCancel").off("click",onClickCancel);
    $("#btnCancel").on("click",onClickCancel);

    $("#btnSave").off("click",onClickSave);
    $("#btnSave").on("click",onClickSave);

    $("#btnReset").off("click");
    $("#btnReset").on("click",onClickReset);

    $("#btnSearch").off("click",onClickSearch);
    $("#btnSearch").on("click",onClickSearch);
}

function validation(){
    var flag = true;
    var strName = $("#txtName").val();
    strName = $.trim(strName);
    if(strName == ""){
        customAlert.error("Error","Enter Table Name");
        flag = false;
        return false;
    }
    var strCode = $("#txtCode").val();
    strCode = $.trim(strCode);
    /*if(strCode == ""){
        customAlert.error("Error","Enter Code");
        flag = false;
        return false;
    }*/
    return flag;
}

function onClickSave(){
    if(validation()){
        var strName = $("#txtName").val();
        strName = $.trim(strName);
        var strCode = $("#txtCode").val();
        strCode = $.trim(strCode);

        var cmbStatus = $("#cmbStatus").data("kendoComboBox");
        var isActive = 1;
        if(cmbStatus && cmbStatus.selectedIndex == 0){
            isActive = 1;
        }
        var dataObj = {};
        dataObj.tableName = strName;
        dataObj.codeValue = strCode;
        dataObj.isActive = isActive;
        dataObj.createdBy = sessionStorage.userId;//"101";//sessionStorage.uName;
        dataObj.isDeleted = "0";
        dataObj.codeName =  strName;

        if(operation == ADD){
            var dataUrl = ipAddress+"/codetable/create";
            createAjaxObject(dataUrl,dataObj,"POST",onCreate,onError);
        }else{
            dataObj.id = selItem.idk;
            var dataUrl = ipAddress+"/country/update";
            createAjaxObject(dataUrl,dataObj,"POST",onCreate,onError);
        }
    }
}

function onCreate(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            var obj = {};
            obj.status = "success";
            obj.operation = operation;
            popupClose(obj);
        }else{
            customAlert.error("error", dataObj.response.status.message);
        }
    }
    /*var obj = {};
    obj.status = "success";
    obj.operation = operation;
    popupClose(obj);*/
}

function onClickReset(){
    $("#txtName").val("");
    $("#txtCode").val("");
}
function onError(errObj){
    console.log(errObj);
    customAlert.error("Error","Unable to create Country");
}
function onClickCancel(){
    var obj = {};
    obj.status = "false";
    popupClose(obj);
}
function onClickSearch(){
    var obj = {};
    obj.status = "search";
    popupClose(obj);
}
function popupClose(obj){
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}



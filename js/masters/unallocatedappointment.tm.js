var angularUIgridWrapper, angularPTUIgridWrapper;
var flatAppointments = [];


$(document).ready(function () {

    var dataOptions = {};

    angularUIgridWrapper = new AngularUIGridWrapper("dgridUnAllocatedAppointmentsList", dataOptions);
    angularUIgridWrapper.init();
    buildAppointmentsListGrid([]);

    // angularPTUIgridWrapper = new AngularUIGridWrapper("dgridPatientsList", dataOptions);
    // angularPTUIgridWrapper.init();
    // buildPatientsListGrid([]);

    $('#dgridUnAllocatedAppointmentsList').on('click', 'div.ui-grid-cell', function () {
        var index = $(this).index();

        if (index > 0) {
            var selectedTime = $(this).parent().find('div:eq(0)').text();
            var selectedDate = $('div.ui-grid-header-cell-row .ui-grid-header-cell:eq(' + index + ') span').text();

            showPatients(selectedTime, selectedDate);
        }

    })

});

function onClickSubmit() {

    if($("#txtDate").val() !== ""){



        flatAppointments=[];
        $("#divPatients").css("display", "none");
        $('#dgridUnAllocatedAppointmentsList').css('display', 'none');
        var selectedDate = GetDateTime("txtDate");
        var allDatesInWeek = [];

        for (let i = 1; i <= 7; i++) {
            var first = selectedDate.getDate() - (selectedDate.getDay() + 1) + i;
            var day = new Date(selectedDate.setDate(first));

            allDatesInWeek.push(day);
        }
        var firstDayOfWeek = allDatesInWeek[0];
        var lastDayOfWeek = allDatesInWeek[allDatesInWeek.length - 1];

        var APIResponse = unallocatedAppointments;

        if (APIResponse.response.appointment !== null && APIResponse.response.appointment.length > 0) {
            var appointments = APIResponse.response.appointment;
            var updatedAppointments = $.map(appointments, function (element, index) {
                var duration = parseInt(element.duration);
                var appointmentStartDate = new Date(parseInt(element.dateOfAppointment));
                var appointmentEndDate = dateAdd(appointmentStartDate, 'minute', duration);

                var startHour = appointmentStartDate.getHours(), endHour = appointmentEndDate.getHours();
                var timeSlots = [];
                for (var counter = startHour; counter <= endHour; counter++) {

                    var hours = counter > 12 ? counter - 12 : counter;
                    var am_pm = counter >= 12 ? "PM" : "AM";
                    hours = hours < 10 ? "0" + hours : hours;
                    time = hours + ":00 " + am_pm;

                    timeSlots.push(time);
                }

                element.appointmentStartDate = appointmentStartDate;
                element.appointmentEndDate = appointmentEndDate;
                element.timeSlots = timeSlots;

                return element;
            });


            for (var counter = 0; counter < updatedAppointments.length; counter++) {
                var appointment = updatedAppointments[counter];

                for (var counter2 = 0; counter2 < appointment.timeSlots.length; counter2++) {
                    var time = appointment.timeSlots[counter2];
                    var date = daysOfWeek[appointment.appointmentStartDate.getDay()] + " " + formatDate(appointment.appointmentStartDate);
                    var flatAppointmentObj = {};

                    var index = -1;
                    flatAppointments.find(function (item, i) {
                        if (item.time === time && item.date === date) {
                            index = i;
                        }
                    });

                    if (index > -1) {
                        flatAppointments[index]["patients"].push(appointment.composition.patient);
                    }
                    else {

                        flatAppointmentObj.time = time;
                        flatAppointmentObj.date = date;
                        flatAppointmentObj.patients = [];
                        flatAppointmentObj.patients.push(appointment.composition.patient);
                        flatAppointments.push(flatAppointmentObj);
                    }
                }
            }


            var hoursArray = [];
            hoursArray = $.map(APIResponse.response.appointment, function (e, i) {
                return new Date(e.dateOfAppointment).getHours();
            });

            var minHour = Math.min(...hoursArray);
            var maxHour = Math.max(...hoursArray);

            var allTimeSlots = [];

            for (var counter3 = minHour; counter3 <= maxHour; counter3++) {

                var hours = counter3 > 12 ? counter3 - 12 : counter3;
                var am_pm = counter3 >= 12 ? "PM" : "AM";
                hours = hours < 10 ? "0" + hours : hours;
                time = hours + ":00 " + am_pm;

                allTimeSlots.push(time);
            }

            var allDaysInWeek = [];
            for (var counter4 = 0; counter4 < allDatesInWeek.length; counter4++) {
                allDaysInWeek.push(daysOfWeek[allDatesInWeek[counter4].getDay()] + " " + formatDate(allDatesInWeek[counter4]));
            }

            var dataSource = [];

            for (var counter5 = 0; counter5 < allTimeSlots.length; counter5++) {
                var timeSlot = allTimeSlots[counter5];
                var dataSourceObj = {};
                dataSourceObj.Time = timeSlot;

                for (var counter6 = 0; counter6 < allDaysInWeek.length; counter6++) {
                    var tempDate = allDaysInWeek[counter6];
                    var noOfAppointments = 0;

                    flatAppointments.find(function (item, index) {
                        if (item.time === timeSlot && item.date === tempDate) {
                            noOfAppointments = item.patients.length;
                        }
                    });

                    dataSourceObj[allDaysInWeek[counter6]] = noOfAppointments.toString();
                }

                dataSource.push(dataSourceObj);
            }
            if (dataSource != null && dataSource.length > 0) {
                $('#dgridUnAllocatedAppointmentsList').css('display', 'block');
                buildAppointmentsListGrid(dataSource);
            }

        }
    }
    else{
        alert("Select date");
    }
}

function GetDateTime(Id) {
    var dt = document.getElementById('' + Id + '').value;
    var dob = null;

    var dtArray = dt.split("/");
    dob = (dtArray[2] + "/" + dtArray[1] + "/" + dtArray[0]);

    var returnValue = new Date(dob);

    return returnValue;

}

function formatDate(date) {

    var dd = date.getDate();

    var mm = date.getMonth() + 1;
    var yyyy = date.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }

    if (mm < 10) {
        mm = '0' + mm;
    }

    return dd + '/' + mm + '/' + yyyy;
}

function dateAdd(date, interval, units) {
    var ret = new Date(date); //don't change original date
    var checkRollover = function () { if (ret.getDate() != date.getDate()) ret.setDate(0); };
    switch (interval.toLowerCase()) {
        case 'year': ret.setFullYear(ret.getFullYear() + units); checkRollover(); break;
        case 'quarter': ret.setMonth(ret.getMonth() + 3 * units); checkRollover(); break;
        case 'month': ret.setMonth(ret.getMonth() + units); checkRollover(); break;
        case 'week': ret.setDate(ret.getDate() + 7 * units); break;
        case 'day': ret.setDate(ret.getDate() + units); break;
        case 'hour': ret.setTime(ret.getTime() + units * 3600000); break;
        case 'minute': ret.setTime(ret.getTime() + units * 60000); break;
        case 'second': ret.setTime(ret.getTime() + units * 1000); break;
        default: ret = undefined; break;
    }
    return ret;
}

function buildAppointmentsListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;

    var columns = [];
    if (dataSource != null && dataSource.length > 0) {
        var firstObj = dataSource[0];

        for (var key in firstObj) {
            columns.push(key);
        }
    }

    if (columns != null && columns.length > 0) {
        for (var counter = 0; counter < columns.length; counter++) {
            var strGridColumn = '{';
            strGridColumn = strGridColumn + '"title":"' + columns[counter] + '","field":"' + columns[counter] + '"}';

            var jsonGridColumn = JSON.parse(strGridColumn);
            gridColumns.push(jsonGridColumn);

        }
    }




    angularUIgridWrapper.creategrid(dataSource, gridColumns, otoptions);
    adjustHeight();


    //onIndividualRowClick();
}


function buildPatientsListGrid(dataSource) {
    $('#ulPatientsList').empty();
    if (dataSource != null && dataSource.length > 0) {
        for (var counter = 0; counter < dataSource.length; counter++) {
            var id = "patient-" + counter;
            var $item = $('<li id="' + id + '" class="list-group-item"> <div> <img src="images/patient_101.png" class="rounded-circle" alt="Cinque Terre"> <div>' + dataSource[counter].id + ' - ' + dataSource[counter].firstName + ' ' + dataSource[counter].lastName + '</div> <div>' + dataSource[counter].dateOfBirth + '</div> </div> </li>');
            $item.droppable({
                accept: '.ui-draggable',
                drop: function (e, ui) {

                    $(this).find('.staff').remove();
                    var $staffDiv = $('<div class="staff">Staff : ' + $(ui.draggable).text() + '</div>');
                    $staffDiv.appendTo($(this));
                }
            });
            $item.appendTo('#ulPatientsList');


        }

    } else {
        $('#ulPatientsList').empty();
    }




    // var gridColumns = [];
    // var otoptions = {};
    // otoptions.noUnselect = false;
    // otoptions.rowHeight = 100;
    // gridColumns.push({
    //     "title": "Image",
    //     "field": "id",
    //     "enableColumnMenu": false,
    //     "width": "20%",
    //     "cellTemplate":showPtImage()
    // });
    // gridColumns.push({
    //     "title": "Patient Details",
    //     "field": "firstName",
    //     "enableColumnMenu": false,
    //     "cellTemplate":showPtDetails()
    // });

    // angularPTUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    // adjustHeight();


    // angularPTUIgridWrapper.creategrid(dataSource, gridColumns, otoptions);
}

function buildProvidersListGrid(dataSource) {
    $('#ulStaffList').empty();
    if (dataSource != null && dataSource.length > 0) {
        for (var counter = 0; counter < dataSource.length; counter++) {
            var id = "provider-" + counter;
            var $item = $('<li id="' + id + '"class="list-group-item"> <div> <div>' + dataSource[counter].id + ' - ' + dataSource[counter].firstName + ' ' + dataSource[counter].lastName + '</div> </div> </li>');
            $item.draggable({
                helper: "clone"
            });
            $item.appendTo('#ulStaffList');


        }


    }
    else {
        $('#ulStaffList').empty();
    }
}

function adjustHeight() {
    var defHeight = 230;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    angularUIgridWrapper.adjustGridHeight(cmpHeight);
}

function showPatients(time, date) {
    var patients = null;
    flatAppointments.find(function (item, index) {
        if (item.time.trim() === time.trim() && item.date.trim() === date.trim()) {
            patients = item.patients;
        }
    });

    if (patients !== null && patients.length > 0) {
        $("#dgridUnAllocatedAppointmentsList").css("display", "none");
        $("#divPatients").css("display", "block");


        buildPatientsListGrid(patients);
        buildProvidersListGrid(providerDetails);
    }
    else {
        alert('There are no appointments at this time.');
    }
}

function showPtDetails() {
    var node = '<div style="padding-top:5px"><spn class="pt">{{row.entity.id}}</spn><spn class="pt"> - {{row.entity.firstName}} &nbsp;&nbsp;{{row.entity.middleName}} &nbsp;&nbsp;{{row.entity.lastName}}</spn><br>';
    node = node + '<span class="avlClass">Available Now</span></div>';
    return node;
}
function showPtImage() {
    var node;

    node = '<div ng-show="((row.entity.dnr == \'1\'))"><img src="{{row.entity.photo}}"  onerror="onImgError(event)" style="width:90%;height:100%;border: 1px solid red;" gender="{{row.entity.GR}}"></img></div>';
    node = node + '<div ng-show="((row.entity.dnr == \'0\'))"><img src="{{row.entity.photo}}"  onerror="onImgError(event)" style="width:90%;height:100%;border: 1px solid;" gender="{{row.entity.GR}}"></img></div>';

    return node;
}

function onImgError(e) {
    //console.log(e);
}


function onClickSave() {
    alert('Successfully saved.');
    $("#dgridUnAllocatedAppointmentsList").css("display", "none");
    $("#divPatients").css("display", "none");
    buildAppointmentsListGrid([]);
    $('#ulStaffList').empty();
    $('#ulPatientsList').empty();
    $("#txtDate").val("");
}
var daysOfWeek = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
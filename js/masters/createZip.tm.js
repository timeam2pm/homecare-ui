var parentRef = null;
var operation = "";
var selItem = null;
var ADD = "add";
var UPDATE = "update";
var VIEW = "view";
var DELETE = "delete";
var searchZip = false;

var statusArr = [{Key:'Active',Value:'Active'},{Key:'InActive',Value:'InActive'}];
var IsPostalCodeManual = sessionStorage.IsPostalCodeManual;
var IsPostalFlag = "0";

$(document).ready(function(){
    parentRef = parent.frames['iframe'].window;
});

function adjustHeight(){
    var defHeight = 45;
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 40;
    }
}

$(window).load(function(){
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
    init();
});

function init(){
    var strPC = "";
    var strState= "";
    var cntry = sessionStorage.countryName;
    if(cntry.indexOf("India")>=0){
        strPC = "Postal Code";
        strState = "State";
    }else if(cntry.indexOf("United Kingdom")>=0){
        if(IsPostalCodeManual == "1"){
            IsPostalFlag = "1";
            $("#divZip").css("display","none");
            $("#divAreaCode").css("display","none");
        }
        strPC = "Postal Code";
        strState = "County";
    }else if(cntry.indexOf("United State")>=0){
        strPC = "Zip";
        strState = "State";
    }else{
        strPC = "Zip";
        strState = "State";
    }
    $("#lblZip").html(""+strPC+" :" + "<span class='mandatoryClass noPadding' style='margin-left: -4px;'>*</span>");
    $("#lblState").html(""+strState+" :" + "<span class='mandatoryClass noPadding'>*</span>");

    //allowAlphabets("txtCity");
    allowNumerics("txtAreaCode");
    //allowNumerics("txtZip");
    allowNumerics("txtZip4");
    if(parentRef.searchZip){
        searchZip = parentRef.searchZip;
    }
    $('#txtCity, #txtAreaCode,#txtZip,#txtZip4').bind('keyup', function() {
        if(allFilled()){
            //$('#btnSave').removeAttr('disabled');
        }else{
            //$('#btnSave').attr("disabled", "disabled");
        }

        if(searchZip){
            $("#btnSearch").hide();
            $("#btnReset").hide();
        }

    });

    operation = parentRef.operation;
    selItem = parentRef.selItem;

    $("#cmbState").kendoComboBox();
    $("#cmbCountry").kendoComboBox();
//	$("#cmbCounty").kendoComboBox();
    $("#cmbStatus").kendoComboBox();

    setDataForSelection(statusArr, "cmbStatus", onStatusChange, ["Value", "Key"], 0, "");
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if(cmbStatus){
        cmbStatus.select(0);
        cmbStatus.enable(false);
    }
    getAjaxObject(ipAddress+"/state/list/","GET",getCountryList,onError);

    buttonEvents();
}
function allFilled() {
    var filled = true;
    $('body input').each(function() {
        if($(this).val() == '') filled = false;
    });
    return filled;
}
function Validate(event) {
    var regex = new RegExp("^[0-9?]");
    var key = String.fromCharCode(event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
}
function onlyAlphabets(e, t) {
    try {
        if (window.event) {
            var charCode = window.event.keyCode;
        }
        else if (e) {
            var charCode = e.which;
        }
        else { return true; }
        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))
            return true;
        else
            return false;
    }
    catch (err) {
        alert(err.Description);
    }
}
function onError(errorObj){
    console.log(errorObj);
}
function getCountryList(dataObj){
    console.log(dataObj);
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.stateExt){
        if($.isArray(dataObj.response.stateExt)){
            dataArray = dataObj.response.stateExt;
        }else{
            dataArray.push(dataObj.response.stateExt);
        }
    }
    var tempDataArry = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
    }
    console.log(dataArray);
    setDataForSelection(dataArray, "cmbState", onStateChange, ["state", "idk"], 0, "");

    // getAjaxObject(ipAddress+"/county/list/","GET",getCountyList,onError);
    if(operation == UPDATE && selItem){
        console.log(selItem);
        $("#btnReset").hide();
        $("#txtCity").val(selItem.city);
        $("#txtZip").val(selItem.zip);
        $("#txtZip4").val(selItem.zipFour);
        $("#txtAreaCode").val(selItem.areaCode);
        var state = selItem.state;
        if(state){
            var stateIdx = getComboListIndex("cmbState","state",state);
            setComboBoxIndex("cmbState",stateIdx);
            onStateChange();
        }
        var cmbStatus = $("#cmbStatus").data("kendoComboBox");
        if(cmbStatus){
            if(selItem.isActive == 1){
                cmbStatus.select(0);
            }else{
                cmbStatus.select(1);
            }
            cmbStatus.enable(true);
        }
    }else{
        var cmbStatus = $("#cmbStatus").data("kendoComboBox");
        if(cmbStatus && cmbStatus.selectedIndex<0){
            cmbStatus.select(0);
            cmbStatus.enable(false);
        }
        onStateChange();
    }

}

function onStateChange(){
    var cmbState = $("#cmbState").data("kendoComboBox");
    if(cmbState && cmbState.selectedIndex<0){
        cmbState.select(0);
    }
    getCountryByState();
}
function getCountryByState(){
    $("#txtCountry").val("");
    var cmbState = $("#cmbState").data("kendoComboBox");
    if(cmbState){
        var dataItem = cmbState.dataItem();
        var country = dataItem.country;
        $("#txtCountry").val(country);
    }

}
function getCountyList(dataObj){
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.countyext){
        if($.isArray(dataObj.response.countyext)){
            dataArray = dataObj.response.countyext;
        }else{
            dataArray.push(dataObj.response.countyext);
        }
    }
    var tempDataArry = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
    }
    console.log(dataArray);
    var obj = {};
    obj.county = "";
    obj.idk = "";
    dataArray.unshift(obj);
    //setDataForSelection(dataArray, "cmbCounty", onCountyChange, ["county", "idk"], 0, "");


}
function onCountryChange(){
    var cmbCountry = $("#cmbCountry").data("kendoComboBox");
    if(cmbCountry && cmbCountry.selectedIndex<0){
        cmbCountry.select(0);
    }
}
function onStatusChange(){
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if(cmbStatus && cmbStatus.selectedIndex<0){
        cmbStatus.select(0);
    }
}
function onCountyChange(){
    var cmbCounty = $("#cmbCounty").data("kendoComboBox");
    if(cmbCounty && cmbCounty.selectedIndex<0){
        cmbCounty.select(0);
    }
}
function buttonEvents(){
    $("#btnCancel").off("click",onClickCancel);
    $("#btnCancel").on("click",onClickCancel);

    $("#btnSave").off("click",onClickSave);
    $("#btnSave").on("click",onClickSave);

    $("#btnSearch").off("click",onClickSearch);
    $("#btnSearch").on("click",onClickSearch);

    $("#btnReset").off("click",onClickReset);
    $("#btnReset").on("click",onClickReset);

}
function onClickReset(){
    operation = ADD;
    selItem = null;
    $("#txtCity").val("");
    $("#txtZip").val("");
    $("#txtZip4").val("");
    //$("#txtZip4").val("");
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if(cmbStatus){
        cmbStatus.select(0);
        cmbStatus.enable(false);
    }
    /*var cmbCountry = $("#cmbCountry").data("kendoComboBox");
    if(cmbCountry){
        cmbCountry.select(0);
    }
    var cmbCounty = $("#cmbCounty").data("kendoComboBox");
    if(cmbCounty){
        cmbCounty.select(0);
    }*/

    setComboDefault("cmbCountry");
    //setComboDefault("cmbCounty");
    setComboDefault("cmbState");
    onStateChange()
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if(cmbStatus){
        cmbStatus.select(0);
        cmbStatus.enable(false);
    }
}
function setComboDefault(cmbId){
    var cmbState = $("#"+cmbId).data("kendoComboBox");
    if(cmbState){
        cmbState.select(0);
    }
}
function validation(){
    var flag = true;
    var strCity = $("#txtCity").val();
    strCity = $.trim(strCity);
    if(strCity == ""){
        customAlert.error("Error","Enter City");
        flag = false;
        return false;
    }
    var strZip = $("#txtZip").val();
    strZip = $.trim(strZip);
    if((IsPostalFlag != "1" && strZip == "")){
        customAlert.error("Error","Enter Zip");
        flag = false;
        return false;
    }
    /*var strZip4 = $("#txtZip4").val();
    strZip4 = $.trim(strZip4);
    if(strZip4 == ""){
        customAlert.error("Error","Enter Zip4");
        flag = false;
        return false;
    }*/
    return flag;
}

function onClickSave(){
    if(validation()){
        var strCity = $("#txtCity").val();
        strCity = $.trim(strCity);

        var strZip = $("#txtZip").val();
        strZip = $.trim(strZip);

        var strZip4 = $("#txtZip4").val();
        strZip4 = $.trim(strZip4);

        var cmbState = $("#cmbState").data("kendoComboBox");
        var cmbCountry = $("#cmbCountry").data("kendoComboBox");
        //var cmbCounty = $("#cmbCounty").data("kendoComboBox");
        var isActive = 0;
        var cmbStatus = $("#cmbStatus").data("kendoComboBox");
        if(cmbStatus){
            if(cmbStatus.selectedIndex == 0){
                isActive = 1;
            }
        }

        var stateItem = cmbState.dataItem();
        var dataObj = {};
        dataObj.city = strCity;
        dataObj.zip = strZip;
        dataObj.zipFour = strZip4;

        dataObj.stateId = cmbState.value();
        dataObj.countyId = null;//cmbCounty.value();
        dataObj.areaCode = $("#txtAreaCode").val();
        //dataObj.state = cmbState.text();

        dataObj.isActive = isActive;
        dataObj.createdBy = sessionStorage.userId;//"101";//sessionStorage.uName;
        dataObj.isDeleted = "0";

        if(operation == ADD){
            var dataUrl = ipAddress+"/city/create";
            createAjaxObject(dataUrl,dataObj,"POST",onCreate,onError);
        }else{
            dataObj.id = selItem.idk;
            var dataUrl = ipAddress+"/city/update";
            createAjaxObject(dataUrl,dataObj,"POST",onCreate,onError);
        }

    }
}

function onCreate(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            var obj = {};
            obj.status = "success";
            obj.operation = operation;
            popupClose(obj);
        }else{
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}

function onClickZipDetails(dataObj){
    var obj = {};
    obj.status = "success";
    obj.item = dataObj.response.city;
    popupClose(obj);
}
function onClickSearch(){
    var obj = {};
    obj.status = "search";
    popupClose(obj);
}

function onError(errObj){
    console.log(errObj);
    customAlert.error("Error","Unable to create Zip");
}
function onClickCancel(){
    var obj = {};
    obj.status = "false";
    popupClose(obj);
}
function popupClose(obj){
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}



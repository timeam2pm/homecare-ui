var angularUIgridWrapper = null;
var angularUISelgridWrapper = null;

var angularUIPTgridWrapper = null;

var parentRef = null;
var operation = "";
var selItem = null;
var ADD = "add";
var UPDATE = "update";
var VIEW = "view";
var DELETE = "delete";
var IsradioValue = 1;
var IsFlag = 0;
var selUserId;
var typeArr = [{ Key: '100', Value: 'Doctor' }, { Key: '200', Value: 'Nurse' }, { Key: '201', Value: 'Caregiver' }];

$(document).ready(function(){
    parentRef = parent.frames['iframe'].window;
    sessionStorage.setItem("IsSearchPanel", "1");
    var pnlHeight = window.innerHeight;
    var imgHeight = pnlHeight-50;
    $("#divTop").height(imgHeight);

});
$(document).ready(function(){
    parentRef = parent.frames['iframe'].window;
    patientId = parentRef.patientId
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("dgSelUserIdList1", dataOptions);
    angularUIgridWrapper.init();
    buildFileResourceListGrid([]);

    var dataOptions1 = {
        pagination: false,
        changeCallBack: onChange1
    }
    angularUISelgridWrapper = new AngularUIGridWrapper("dgAvlUserIdList2", dataOptions1);
    angularUISelgridWrapper.init();
    buildFileResourceSelListGrid([]);

    var dataPtOptions = {
        pagination: false,
        changeCallBack: onPtChange
    }
    angularUIPTgridWrapper = new AngularUIGridWrapper("dgridUserPatient", dataPtOptions);
    angularUIPTgridWrapper.init();
    buildPatientGrid([]);
});

function onClickActive() {
    $(".btnInActive").removeClass("radioButton-active");
    $(".btnActive").addClass("radioButton-active");
}

function onClickInActive() {
    $(".btnActive").removeClass("radioButton-active");
    $(".btnInActive").addClass("radioButton-active");
}

function onClickrdActive() {
    $(".btnrdInActive").removeClass("radioButton-active");
    $(".btnrdActive").addClass("radioButton-active");
}

function onClickrdInActive() {
    $(".btnrdActive").removeClass("radioButton-active");
    $(".btnrdInActive").addClass("radioButton-active");
}

$(window).load(function(){
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
    init();
    buttonEvents();
    adjustHeight();
}
function adjustHeight(){
    var defHeight = 140;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    cmpHeight = cmpHeight/2;
    if(angularUIgridWrapper){
        angularUIgridWrapper.adjustGridHeight(cmpHeight);
    }
    if(angularUISelgridWrapper){
        angularUISelgridWrapper.adjustGridHeight(cmpHeight);
    }

    if(angularUIPTgridWrapper){
        angularUIPTgridWrapper.adjustGridHeight(cmpHeight);
    }
}
function buildFileResourceListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    // gridColumns.push({
    //     "title": "Select",
    //     "field": "SEL",
    //     "cellTemplate": showCheckBoxTemplate(),
    //     "width":"15%"
    // });
    gridColumns.push({
        "title": "User ID",
        "field": "userId",
        "width":"12%",
        "cellTemplate": "<div  style='text-decoration:underline;cursor:pointer' class='ui-grid-cell-contents' onclick='onClickRPLink(1)'><a style='font-size: 13px;color: #2f98d29c'>{{row.entity.userId}}</a></div>",
    });
    gridColumns.push({
        "title": "Last Name",
        "field": "lastName",
    });
    gridColumns.push({
        "title": "First Name",
        "field": "firstName",
    });

    gridColumns.push({
        "title": "User Type",
        "field": "userType",
        "width":"20%"
    });

    gridColumns.push({
        "title": "Access Type",
        "field": "accessTypes"
    });

    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}
var prevSelectedItem =[];
function onChange(){

}
function buildFileResourceSelListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    // gridColumns.push({
    //     "title": "Select",
    //     "field": "SEL",
    //     "cellTemplate": showCheckBoxTemplate(),
    //     "width":"15%"
    // });
    gridColumns.push({
        "title": "User ID",
        "field": "userId",
        "width":"12%",
        "cellTemplate": "<div  style='text-decoration:underline;cursor:pointer' class='ui-grid-cell-contents' onclick='onClickRPLink(2)'><a style='font-size: 13px;color: #2f98d29c'>{{row.entity.userId}}</a></div>",

    });
    gridColumns.push({
        "title": "Last Name",
        "field": "lastName",
    });
    gridColumns.push({
        "title": "First Name",
        "field": "firstName",
    });

    gridColumns.push({
        "title": "User Type",
        "field": "userType",
        "width":"20%"
    });

    gridColumns.push({
        "title": "Access Type",
        "field": "accessTypes"
    });

    angularUISelgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}
var prevSelectedItem =[];
function onChange1(){

}

function showCheckBoxTemplate(){
    var node = '<div style="text-align:center;padding-top: 6px;"><input type="checkbox" class="check1" id="chkbox" ng-model="row.entity.SEL" style="width:20px;height:20px;margin:0px;"   onclick="onSelect(event);"></input></div>';
    return node;
}
function buildPatientGrid(dataSource){
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Staff Id",
        "field": "idk",
        "enableColumnMenu": false
    });
    gridColumns.push({
        "title": "Last Name",
        "field": "lastName",
        "enableColumnMenu": false,
    });

    gridColumns.push({
        "title": "First Name",
        "field": "firstName",
        "enableColumnMenu": false,
    });

    gridColumns.push({
        "title": "Middle Name",
        "field": "middleName",
        "enableColumnMenu": false,
    });
    gridColumns.push({
        "title": "Date of Birth",
        "field": "DOB",
        "enableColumnMenu": false,
    });
    gridColumns.push({
        "title": "Gender",
        "field": "gender",
        "enableColumnMenu": false,
    });
    gridColumns.push({
        "title": "User Type",
        "field": "userType",
        "enableColumnMenu": false,
    });

    gridColumns.push({
        "title": "Access Type",
        "field": "accessTypes"
    });
    angularUIPTgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
}
var selPatientId = null;
var selStaffId = null;
function onPtChange(){
    setTimeout(function(){
        console.log(angularUIPTgridWrapper.getSelectedRows());
        var selRows = angularUIPTgridWrapper.getSelectedRows();
        if(selRows && selRows.length>0){
            var selItem = selRows[0];
            if(selItem){
                selPatientId = selItem.userId;
                selStaffId = selItem.idk;
                // buildFileResourceListGrid([]);
                buildFileResourceSelListGrid([]);
                getPatientMappedList();
                //getUserList();
            }
        }
    },50)

}
// $(window).load(function(){
//     if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
//         $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
//     }
//     init();
// });
function onClickSubRight() {
//	alert("right");
    var selGridData = angularUISelgridWrapper.getSelectedRows();
    var selList = [];
    for (var i = 0; i < selGridData.length; i++) {
        var dataRow = selGridData[i].entity;

        angularUISelgridWrapper.deleteItem(dataRow);
        //dataRow.SEL = false;
        angularUIgridWrapper.insert(dataRow);
    }
}

function onClickSubLeft(){
    //alert("click");
    var selGridData = angularUIgridWrapper.getSelectedRows();
    var selList = [];
    for (var i = 0; i < selGridData.length; i++) {
        var dataRow = selGridData[i].entity;
        if(dataRow.SEL){
            angularUIgridWrapper.deleteItem(dataRow);
            angularUISelgridWrapper.insert(dataRow);
        }
    }
}

function init(){
    operation = parentRef.operation;
    selItem = parentRef.selItem;
    // getPatientList();
    getUserList();

    buttonEvents();

    $("#cmbLang").igCombo();
    setMultipleDataForSelection(typeArr, "cmbLang", onLanChange, ["Value", "Key"], 0, "");
    $("#cmbLang").val("Caregiver");
    onClickFilterBtn();
}
function onStatusChange(){
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if(cmbStatus && cmbStatus.selectedIndex<0){
        cmbStatus.select(0);
    }
}

function onLanChange() {
    onComboChange("cmbLang");
}

function buttonEvents(){
    $("#btnCancel").off("click",onClickCancel);
    $("#btnCancel").on("click",onClickCancel);

    $("#btnSave").off("click",onClickSave);
    $("#btnSave").on("click",onClickSave);

    $("#btnSubRight").off("click");
    $("#btnSubRight").on("click",onClickRight);

    $("#btnSubLeft").off("click");
    $("#btnSubLeft").on("click",onClickLeft);

    // $("input[name=Comm]").on( "change", function() {
    //     radioValue = $(this).val();
    //     if(radioValue == "1"){
    //         IsradioValue = 1;
    //         buildPatientGrid([]);
    //         getPatientList();
    //
    //     }else{
    //         IsradioValue = 2;
    //         buildPatientGrid([]);
    //         getPatientList();
    //     }
    //
    // });

    $("#rdUnAttached").on("click", function(e) {
        e.preventDefault();
        IsradioValue = 1;
        buildPatientGrid([]);
        onClickFilterBtn();
        onClickrdInActive();

    });
    $("#rdAttached").on("click", function(e) {
        e.preventDefault();
        IsradioValue = 2;
        buildPatientGrid([]);
        onClickFilterBtn();
        onClickrdActive();
    });

    $(".btnActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('active');
        onClickActive();
    });
    $(".btnInActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('inactive');
        onClickInActive();
    });

    $("#providerFilter-btn").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        buildPatientGrid([]);
        onClickFilterBtn();

    });

}

function onClickFilterBtn() {
    var seldata = $("#cmbLang").val();
    var selKey = '';
    for (var key in typeArr) {
        if (seldata.includes(typeArr[key].Value)) {
            selKey = selKey + typeArr[key].Key + ",";
        }
    }
    selKey = selKey.trimEnd(',');
    if (selKey != '') {
        if (userTypeStatus == "active") {
            urlExtn = ipAddress + "/provider/list?is-active=1&is-deleted=0&type=" + selKey;
        }
        else if (userTypeStatus == "inactive") {
            urlExtn = ipAddress + "/provider/list?is-active=0&is-deleted=1&type=" + selKey;
        }
        getAjaxObject(urlExtn, "GET", onStaffListData, onErrorMedication);
    }

    else {
        if (userTypeStatus == "active") {
            getPatientList();
        }
        else if (userTypeStatus == "inactive") {
            getAjaxObject(ipAddress + "/provider/list?is-active=0&is-deleted=1", "GET", onStaffListData, onErrorMedication);
        }

    }

}
function getPatientList(){
    getAjaxObject(ipAddress+"/provider/list?is-active=1","GET",onStaffListData,onErrorMedication);
}
var userTypeStatus= "active";
function searchOnLoad(status) {
    buildPatientGrid([]);
    var urlExtn;
    userTypeStatus = status;
    // if(status == "active") {
    //     urlExtn = ipAddress + "/provider/list?is-active=1&is-deleted=0";
    // }
    // else if(status == "inactive") {
    //     urlExtn = ipAddress + "/provider/list?is-active=0&is-deleted=1";
    // }
    // getAjaxObject(urlExtn,"GET",onStaffListData,onErrorMedication);
    onClickFilterBtn();
}


function onStaffListData(dataObj) {
    var userdataArray = [];
    var unuserdataArray = [];
    var dataArray = [];
    //var tempDataArry = [];
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if ($.isArray(dataObj.response.provider)) {
            dataArray = dataObj.response.provider;
        } else {
            dataArray.push(dataObj.response.provider);
        }
    } else {
        //customAlert.error("Error",dataObj.response.status.message);
    }
    for (var i = 0; i < dataArray.length; i++) {

        dataArray[i].idk = dataArray[i].id;
        dataArray[i].PID = dataArray[i].id;
        dataArray[i].Status = "InActive";
        dataArray[i].photo = ipAddress + "/homecare/download/providers/photo/?" + Math.round(Math.random() * 1000000) + "&access_token=" + sessionStorage.access_token + "&tenant=" + sessionStorage.tenant + "&id=" + dataArray[i].idk;
        // dataArray[i].language1 = dataArray[i].language
        if (dataArray[i].isActive == 1) {
            dataArray[i].Status = "Active";
        }
        var createdDate = new Date(dataArray[i].createdDate);
        var createdDateString = createdDate.toLocaleDateString();
        var modifiedDate = new Date(dataArray[i].modifiedDate);
        var modifiedDateString = modifiedDate.toLocaleDateString();
        dataArray[i].createdDateString = createdDateString;
        dataArray[i].modifiedDateString = modifiedDateString;
        dataArray[i].DOB = kendo.toString(new Date(dataArray[i].dateOfBirth), "dd/MM/yyyy");
        if (dataArray[i].type == 100) {
            dataArray[i].userType = "Doctor";
        }
        if (dataArray[i].type == 200) {
            dataArray[i].userType = "Nurse";
        }
        if (dataArray[i].type == 201) {
            dataArray[i].userType = "Caregiver";
        }


        if (dataArray[i].userId != null && dataArray[i].userId != " " && dataArray[i].userId != 0) {
            userdataArray.push(dataArray[i]);
        }
        else {
            unuserdataArray.push(dataArray[i]);
        }
    }

    if (IsradioValue == 1) {

        buildPatientGrid(unuserdataArray);
    }
    else {

        buildPatientGrid(userdataArray);
    }
}

function getPatientMappedList(){
    // getAjaxObject(ipAddress+"/user-patient/list","GET",onGetUserPatientListData,onErrorMedication);
    // getAjaxObject(ipAddress+"/user/by-patient/"+selPatientId,"GET",onGetUserPatientListData,onErrorMedication);
    if(selPatientId != null) {
        getAjaxObject(ipAddress + "/homecare/tenant-users/?id=" + selPatientId, "GET", onGetUserPatientListData, onErrorMedication);
    }
}
var userPatientListArray = [];
function onGetUserPatientListData(dataObj){
    userPatientListArray = [];
    var userPtList = [];
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            if($.isArray(dataObj.response.tenantUsers)){
                userPtList = dataObj.response.tenantUsers;
            }else{
                userPtList.push(dataObj.response.tenantUsers);
            }
        }else{
            customAlert.info("Error", dataObj.response.status.message);
        }
    }
    for(var y=0;y<userPtList.length;y++){
        var ptItem = userPtList[y];
        //if(ptItem && ptItem.patientId == selPatientId){
        ptItem.pkid = ptItem.id;
        ptItem.userId = ptItem.id;
        ptItem.idk = ptItem.id;
        ptItem.fullName = ptItem.lastName +" "+ ptItem.firstName;
        if (ptItem.userTypeId == 100) {
            ptItem.userType = "Doctor";
        }
        if (ptItem.userTypeId == 200) {
            ptItem.userType = "Nurse";
        }
        if (ptItem.userTypeId == 201) {
            ptItem.userType = "Caregiver";
        }
        userPatientListArray.push(ptItem);
        //}
    }
    buildFileResourceSelListGrid(userPatientListArray);

}
function getUserList(){
    // getAjaxObject(ipAddress+"/user/list/500,600,700","GET",onGetUserListData,onErrorMedication);
    // getAjaxObject(ipAddress + "/homecare/tenant-users/?is-active=1&is-deleted=0&userTypeId=:in:200,201,100", "GET", onGetUserListData, onErrorMedication);
    getAjaxObject(ipAddress + "/homecare/provider/tenant-users/filter/?is-active=1&is-deleted=0&userTypeId=:in:200,201,100", "GET", onGetUserListData, onErrorMedication);
}
var userList = [];
var userListArray = [];
function onGetUserListData(dataObj){
    console.log(dataObj);
    userListArray = [];
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            if($.isArray(dataObj.response.tenantUsers)){
                userListArray = dataObj.response.tenantUsers;
            }else{
                userListArray.push(dataObj.response.tenantUsers);
            }
        }else{
            customAlert.info("Error", dataObj.response.status.message);
        }
    }

    userList = [];
    for(var i=0;i<userListArray.length;i++){
        var item = userListArray[i];
        item.idk = item.id;
        item.userId = item.id;
        if (item.userTypeId == 100) {
            item.userType = "Doctor";
        }
        if (item.userTypeId == 200) {
            item.userType = "Nurse";
        }
        if (item.userTypeId == 201) {
            item.userType = "Caregiver";
        }

        // if(!isPatientUserExist(item.idk)){
        userList.push(item);
        // }
    }
    buildFileResourceListGrid(userList);
}

function isPatientUserExist(userId){
    for(var x=0;x<userPatientListArray.length;x++){
        var userItem = userPatientListArray[x];
        if(userItem && userItem.userId == userId){
            return true;
        }
    }
    return false;
}



function onClickRight(){
    var selGridData = angularUIgridWrapper.getSelectedRows();
    var mappedGridData = angularUISelgridWrapper.getAllRows();
    if(!(mappedGridData.length > 0)) {
        if(selGridData.length > 0) {
            var selList = [];
            for (var i = 0; i < selGridData.length; i++) {
                var dataRow = selGridData[i];
                selUserId = selGridData[i].idk;
                angularUIgridWrapper.deleteItem(dataRow);
                angularUISelgridWrapper.insert(dataRow);
                // buildFileResourceSelListGrid(dataRow);
            }
        }
        else{
            customAlert.error("Error", "Select user in User List");
        }
    }
    else{
        customAlert.error("Error", "Make sure first remove user in Mapped user List");
    }
}
function onClickLeft(){
    var selStGridData = angularUIPTgridWrapper.getSelectedRows();
    if(selStGridData.length > 0) {
        var selGridData = angularUISelgridWrapper.getSelectedRows();
        if (selGridData.length > 0) {
            customAlert.confirm("Confirm", "Are you sure delete?", function (response) {
                if (response.button == "Yes") {
                    IsFlag = 0;
                    var selList = [];
                    for (var i = 0; i < selGridData.length; i++) {
                        var dataRow = selGridData[i];
                        angularUISelgridWrapper.deleteItem(dataRow);
                        angularUIgridWrapper.insert(dataRow);
                        // buildFileResourceSelListGrid([]);
                        staffUserUpdate();
                    }
                }
            });
        }
        else {
            customAlert.error("Error", "Select user in Mapped User List");
        }
    }
    else{
        customAlert.error("Error", "Select Staff");
    }
}

function onErrorMedication(errobj){
    console.log(errobj);
}


function validation(){
    var flag = true;
    /*var strAbbr = $("#txtAbbrevation").val();
        strAbbr = $.trim(strAbbr);
        if(strAbbr == ""){
            customAlert.error("Error","Enter Abbrevation");
            flag = false;
            return false;
        }
        var strCode = $("#txtCode").val();
        strCode = $.trim(strCode);
        if(strCode == ""){
            customAlert.error("Error","Enter Code");
            flag = false;
            return false;
        }
        var strName = $("#txtName").val();
        strName = $.trim(strName);
        if(strName == ""){
            customAlert.error("Error","Enter Country");
            flag = false;
            return false;
        }*/

    return flag;
}

function onClickSave(){
    IsFlag = 1;
    staffUserUpdate();
    // if(validation()){
    //
    //     var delUserList = angularUIgridWrapper.getAllRows();
    //     var gridUserList = angularUISelgridWrapper.getAllRows();
    //
    //     var mapUserList = [];
    //     for(var u=0;u<gridUserList.length;u++){
    //         var uItem = gridUserList[u].entity;
    //         if(uItem && !uItem.pkid){
    //             var reqObj = {};
    //             reqObj.isActive = "1";
    //             reqObj.isDeleted = "0";
    //             reqObj.userId = uItem.userId;
    //             reqObj.patientId = selPatientId;
    //             reqObj.sendAlert = "1";
    //             reqObj.id = "";
    //             reqObj.createdBy = sessionStorage.userId;
    //             mapUserList.push(reqObj);
    //         }
    //     }
    //     for(var d=0;d<delUserList.length;d++){
    //         var uItem = delUserList[d].entity;
    //         if(uItem){
    //             if(uItem.pkid){
    //                 var reqObj = {};
    //                 reqObj.isActive = "1";
    //                 reqObj.isDeleted = "1";
    //                 reqObj.userId = uItem.userId;
    //                 reqObj.patientId = selPatientId;
    //                 reqObj.sendAlert = "1";
    //                 reqObj.id = uItem.pkid;
    //                 reqObj.modifiedBy = sessionStorage.userId;
    //                 mapUserList.push(reqObj);
    //             }
    //         }
    //     }
    //     console.log(mapUserList);
    //
    //     var dataObj = {};
    //     dataObj = mapUserList;
    //     var dataUrl = ipAddress+"/user-patient/add-or-remove";
    //     createAjaxObject(dataUrl,dataObj,"POST",onCreate,onError);
    // }
}

function onCreate(dataObj){
    console.log(dataObj);
    if(dataObj &&dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            customAlert.error("Info", "Patient User Mapped Successfully");
            setTimeout(function(){
                onPtChange();
            },100)
        }else{
            customAlert.error("error", dataObj.status.message);
        }
    }
    /*var obj = {};
    obj.status = "success";
    obj.operation = operation;
    popupClose(obj);*/
}

function onError(errObj){
    console.log(errObj);
    customAlert.error("Error","Unable to create UserMapping");
}
function onClickCancel(){
    var obj = {};
    obj.status = "false";
    popupClose(obj);
}
function onClickSearch(){
    var obj = {};
    obj.status = "search";
    popupClose(obj);
}
function popupClose(obj){
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}


function staffUserUpdate() {
    var dataObj = {};
    var data = angularUIPTgridWrapper.getSelectedRows();
    if(IsFlag == 0) {
        data[0].userId = null;
    }
    else{
        data[0].userId = selUserId;
    }

    // var data = angularUIPTgridWrapper.getSelectedRows();
    data[0].modifiedBy = sessionStorage.userId;
    data[0].communications[0].modifiedBy = sessionStorage.userId;
    data[0].billableDoctor = data[0].billableDoctor;
    // data[0].dateOfBirth = "2019-03-31";
    if(data[0].employmentTypeId) {
        data[0].employmentTypeId = data[0].employmentTypeId.toString();
    }
    data[0].id = data[0].idk;
    delete data[0].createdDate;
    delete data[0].createdBy;
    delete data[0].PID;
    delete data[0].idk;
    delete data[0].modifiedDate;
    delete data[0].modifiedDateString;
    delete data[0].DOB;
    delete data[0].Status;
    delete data[0].userType;
    delete data[0].photo;
    delete data[0].facilityName;
    delete data[0].billable;
    delete data[0].createdDateString;
    delete data[0].communications[0].createdBy;
    delete data[0].communications[0].createdDate;
    delete data[0].communications[0].modifiedDate;
    delete data[0].communications[0].areaCode;
    delete data[0].communications[0].city;
    delete data[0].communications[0].country;
    delete data[0].communications[0].countryId;
    delete data[0].communications[0].fax;
    delete data[0].communications[0].homePhoneExt;
    delete data[0].communications[0].state;
    delete data[0].communications[0].stateId;
    delete data[0].communications[0].zip;
    delete data[0].communications[0].zipFour;



    // dataObj.push(data[0]);
    var dataUrl = ipAddress + "/provider/update";
    createAjaxObject(dataUrl, data[0], "POST", onUpdate, onErrorMedication);

}

function onUpdate(dataObj){
    console.log(dataObj);
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            var response= dataObj.response.status.message;
            if(IsFlag != 0) {
                customAlert.error("info", response.replace("Provider", "Staff"));
            }
            else{
                customAlert.error("info", "User is removed to the Staff");
            }
            buildPatientGrid([]);
            // init();
            getPatientList();
            // onPtChange();
        } else {
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}

function onClickRPLink(id){
    if(id == 1) {
        var rows = angularUIgridWrapper.getSelectedRows();
        if (rows && rows.length > 0) {
            var row = rows[0];
            getUserName(row.userId);
        }
    }
    else{
        var rows = angularUISelgridWrapper.getSelectedRows();
        if (rows && rows.length > 0) {
            var row = rows[0];
            getUserName(row.userId);
        }
    }
}

function getUserName(userId){
    getAjaxObject(ipAddress+"/homecare/tenant-users/?fields=username&id="+userId,"GET",onGetUserName,onErrorMedication);
}

function onGetUserName(dataObj){
    var userPtList = [];
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            if($.isArray(dataObj.response.tenantUsers)){
                userPtList = dataObj.response.tenantUsers;
            }else{
                userPtList.push(dataObj.response.tenantUsers);
            }
            customAlert.info("UserName", userPtList[0].username);
        }else{
            customAlert.info("Info", dataObj.response.status.message);
        }
    }

}
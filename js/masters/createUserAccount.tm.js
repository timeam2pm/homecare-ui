$(document).ready(function() {
    function createUserObj() {
        var self = this,
            $pwdElem = $("#cmp-acc-password"),
            uId = '',
            tzArray = [],
            operation = "",
            ADD = "add",
            UPDATE = "update",
            finalCheck = false;
        this.init = function() {
            this.bindEvents();
            allowSplAlphaNumericWithoutSpace('cmp-acc-password');
            allowSplAlphaNumericWithoutSpace('cmp-acc-confirm-password');
            allowSpaceAlphaNumeric("cmp-acc-first-name");
            allowSpaceAlphaNumeric("cmp-acc-last-name");
            allowNumerics("cmp-acc-mobile-phone");
            $("#cmp-acc-dob").kendoDatePicker();
            this.timezoneResults();
        };
        this.timezoneResults = function() {
            tzArray = [{ Key: "(GMT-12:00) International Date Line West", Value: 'Etc/GMT+12' }];
            tzArray.push({ Key: "(GMT-11:00) Midway Island, Samoa", Value: 'Pacific/Midway', Offset: 660 });
            tzArray.push({ Key: "(GMT-10:00) Hawaii", Value: 'Pacific/Honolulu', Offset: 600 });
            tzArray.push({ Key: "(GMT-09:00) Alaska", Value: 'US/Alaska', Offset: 540 });
            tzArray.push({ Key: "(GMT-08:00) Pacific Time (US & Canada)", Value: 'America/Los_Angeles', Offset: 480 });
            tzArray.push({ Key: "(GMT-08:00) Tijuana, Baja California", Value: 'America/Tijuana', Offset: 480 });
            tzArray.push({ Key: "(GMT-07:00) Arizona", Value: 'US/Arizona', Offset: 420 });
            tzArray.push({ Key: "(GMT-07:00) Chihuahua, La Paz, Mazatlan", Value: 'America/Chihuahua', Offset: 420 });
            tzArray.push({ Key: "(GMT-07:00) Mountain Time (US & Canada)", Value: 'US/Mountain', Offset: 420 });
            tzArray.push({ Key: "(GMT-06:00) Central America", Value: 'America/Managua', Offset: 360 });
            tzArray.push({ Key: "(GMT-06:00) Central Time (US & Canada)", Value: 'US/Central', Offset: 360 });
            tzArray.push({ Key: "(GMT-06:00) Guadalajara, Mexico City, Monterrey", Value: 'America/Mexico_City', Offset: 360 });
            tzArray.push({ Key: "(GMT-06:00) Saskatchewan", Value: 'Canada/Saskatchewan', Offset: 360 });
            tzArray.push({ Key: "(GMT-05:00) Bogota, Lima, Quito, Rio Branco", Value: 'America/Bogota', Offset: 300 });
            tzArray.push({ Key: "(GMT-05:00) Eastern Time (US & Canada)", Value: 'US/Eastern', Offset: 300 });
            tzArray.push({ Key: "(GMT-05:00) Indiana (East)", Value: 'US/East-Indiana', Offset: 300 });
            tzArray.push({ Key: "(GMT-04:00) Atlantic Time (Canada)", Value: 'Canada/Atlantic', Offset: 240 });
            tzArray.push({ Key: "(GMT-04:00) Caracas, La Paz", Value: 'America/Caracas', Offset: 240 });
            tzArray.push({ Key: "(GMT-04:00) Manaus", Value: 'America/Manaus', Offset: 240 });
            tzArray.push({ Key: "(GMT-04:00) Santiago", Value: 'America/Santiago', Offset: 240 });
            tzArray.push({ Key: "(GMT-03:30) Newfoundland", Value: 'Canada/Newfoundland', Offset: 180 });
            tzArray.push({ Key: "(GMT-03:00) Brasilia", Value: 'America/Sao_Paulo', Offset: 180 });
            tzArray.push({ Key: "(GMT-03:00) Buenos Aires, Georgetown", Value: 'America/Argentina/Buenos_Aires', Offset: 180 });
            tzArray.push({ Key: "(GMT-03:00) Greenland", Value: 'America/Godthab', Offset: 180 });
            tzArray.push({ Key: "(GMT-03:00) Montevideo", Value: 'America/Montevideo', Offset: 180 });
            tzArray.push({ Key: "(GMT-02:00) Mid-Atlantic", Value: 'America/Noronha', Offset: 120 });
            tzArray.push({ Key: "(GMT-01:00) Cape Verde Is", Value: 'Atlantic/Cape_Verde', Offset: 60 });
            tzArray.push({ Key: "(GMT-01:00) Azores", Value: 'Atlantic/Azores', Offset: 60 });
            tzArray.push({ Key: "(GMT+00:00) Casablanca, Monrovia, Reykjavik", Value: 'Africa/Casablanca', Offset: 0 });
            tzArray.push({ Key: "(GMT+00:00) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London", Value: 'Etc/Greenwich', Offset: 0 });
            tzArray.push({ Key: "(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna", Value: 'Europe/Amsterdam', Offset: -60 });
            tzArray.push({ Key: "(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague", Value: 'Europe/Belgrade', Offset: -60 });
            tzArray.push({ Key: "(GMT+01:00) Brussels, Copenhagen, Madrid, Paris", Value: 'Europe/Brussels', Offset: -60 });
            tzArray.push({ Key: "(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb", Value: 'Europe/Sarajevo', Offset: -60 });
            tzArray.push({ Key: "(GMT+01:00) West Central Africa", Value: 'Africa/Lagos', Offset: -120 });
            tzArray.push({ Key: "(GMT+02:00) Amman", Value: 'Asia/Amman', Offset: -120 });
            tzArray.push({ Key: "(GMT+02:00) Athens, Bucharest, Istanbul", Value: 'Europe/Athens', Offset: -120 });
            tzArray.push({ Key: "(GMT+02:00) Beirut", Value: 'Asia/Beirut', Offset: -120 });
            tzArray.push({ Key: "(GMT+02:00) Cairo", Value: 'Africa/Cairo', Offset: -120 });
            tzArray.push({ Key: "(GMT+02:00) Harare, Pretoria", Value: 'Africa/Harare', Offset: -120 });
            tzArray.push({ Key: "(GMT+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius", Value: 'Europe/Helsinki', Offset: -120 });
            tzArray.push({ Key: "(GMT+02:00) Jerusalem", Value: 'Asia/Jerusalem', Offset: -120 });
            tzArray.push({ Key: "(GMT+02:00) Minsk", Value: 'Europe/Minsk', Offset: -120 });
            tzArray.push({ Key: "(GMT+02:00) Windhoek", Value: 'Africa/Windhoek', Offset: -120 });
            tzArray.push({ Key: "(GMT+03:00) Kuwait, Riyadh, Baghdad", Value: 'Asia/Kuwait', Offset: -180 });
            tzArray.push({ Key: "(GMT+03:00) Moscow, St. Petersburg, Volgograd", Value: 'Europe/Moscow', Offset: -180 });
            tzArray.push({ Key: "(GMT+03:00) Nairobi", Value: 'Africa/Nairobi', Offset: -180 });
            tzArray.push({ Key: "(GMT+03:00) Tbilisi", Value: 'Asia/Tbilisi', Offset: -180 });
            tzArray.push({ Key: "(GMT+03:30) Tehran", Value: 'Asia/Tehran', Offset: -210 });
            tzArray.push({ Key: "(GMT+04:00) Abu Dhabi, Muscat", Value: 'Asia/Muscat', Offset: -240 });
            tzArray.push({ Key: "(GMT+04:00) Baku", Value: 'Asia/Baku', Offset: -240 });
            tzArray.push({ Key: "(GMT+04:00) Yerevan", Value: 'Asia/Yerevan', Offset: -240 });
            tzArray.push({ Key: "(GMT+04:30) Kabul", Value: 'Asia/Kabul', Offset: -270 });
            tzArray.push({ Key: "(GMT+05:00) Yekaterinburg", Value: 'Asia/Yekaterinburg', Offset: -300 });
            tzArray.push({ Key: "(GMT+05:00) Islamabad, Karachi, Tashkent", Value: 'Asia/Karachi', Offset: -300 });
            tzArray.push({ Key: "(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi", Value: 'Asia/Calcutta', Offset: -330 });
            tzArray.push({ Key: "(GMT+05:30) Sri Jayawardenapura", Value: 'Asia/Calcutta', Offset: -330 });
            tzArray.push({ Key: "(GMT+05:45) Kathmandu", Value: 'Asia/Katmandu', Offset: -345 });
            setDataForSelection(tzArray, "cmp-acc-timezone", onTimezoneChange, ["Key", "Value"], 0, "");
            function onTimezoneChange() {
                var timezoneVal = $("#cmp-acc-timezone").data("kendoComboBox");
                if (timezoneVal && timezoneVal.selectedIndex < 0) {
                    timezoneVal.select(0);
                }
            }
        };
        this.bindEvents = function() {
            var errortypeobj, successtypeobj;
            var $timezoneElem = $('#cmp-acc-timezone');
            errortypeobj = {
                'background': "url(../../img/create-account/warning@44.png) no-repeat"
            }
            successtypeobj = {
                'background': 'url(../../img/create-account/check@44.png) no-repeat'
            }
            setTimeout(function() {
                var date = new Date();
                if($timezoneElem.val() == "") {
                    $timezoneElem.closest('.cmp-acc-text-input-container').find('.cmp-acc-status').css(errortypeobj);
                    $timezoneElem.addClass('cmp-acc-input-error');
                } else {
                    $timezoneElem.closest('.cmp-acc-text-input-container').find('.cmp-acc-status').css(successtypeobj);
                    $timezoneElem.removeClass('cmp-acc-input-error');
                }
                var timezoneVal = $("#cmp-acc-timezone").data("kendoComboBox");
                if (timezoneVal) {
                    for (var i = 0; i < tzArray.length; i++) {
                        if(Number(tzArray[i].Offset) == date.getTimezoneOffset()) {
                            timezoneVal.select(i);
                        }
                    }
                }
            }, 750);
            $('.cmp-acc-text-input-container').find('input.cmp-acc-text-input[required="required"], input.required-input').on('focusout blur keyup', function() {
                var $this = $(this);
                $this.closest('.cmp-acc-text-input-container').find('.cmp-acc-info').removeClass('cmp-acc-banner cmp-acc-defaultposition').addClass('cmp-acc-hide');
                if($this.attr('id') !== 'cmp-acc-password') {
                    errortypeobj = {
                        'background': "url(../../img/create-account/warning@44.png) no-repeat"
                    }
                    successtypeobj = {
                        'background': 'url(../../img/create-account/check@44.png) no-repeat'
                    }
                    if($this.val().length == 0) {
                        $this.closest('.cmp-acc-text-input-container').find('.cmp-acc-status').css(errortypeobj);
                        $this.addClass('cmp-acc-input-error');
                    } else if($this.attr('type') !== 'password') {
                        $this.closest('.cmp-acc-text-input-container').find('.cmp-acc-status').css(successtypeobj);
                        $this.removeClass('cmp-acc-input-error');
                    }
                } else if ($this.attr('id') === 'cmp-acc-password') {
                    errortypeobj = {
                        'background': "url(../../img/create-account/lock-left-grey@3x.png) no-repeat"
                    }
                    successtypeobj = {
                        'background': 'url(../../img/create-account/lock-right-grey@3x.png) no-repeat'
                    }
                    if($this.val().length == 0) {
                        $this.closest('.cmp-acc-text-input-container').find('.cmp-acc-status').css(errortypeobj);
                        $this.addClass('cmp-acc-input-error');
                    } else {
                        $this.closest('.cmp-acc-text-input-container').find('.cmp-acc-status').css(successtypeobj);
                        $this.removeClass('cmp-acc-input-error');
                    }
                }
            }).on('focus', function() {
                var $this = $(this);
                $this.closest('.cmp-acc-text-input-container').find('.cmp-acc-info').removeClass('cmp-acc-hide').addClass('cmp-acc-banner cmp-acc-defaultposition');
            });
            $('.cmp-acc-select-security').on('change', function() {
                $('.cmp-acc-select-security').find('option').show();
                $('.cmp-acc-select-security').each(function() {
                    var $this = $(this);
                    var selfValue = $this.val();
                    if(selfValue) {
                        // $this.find('option').show();
                        $('.cmp-acc-select-security').not(this).find('option[value="'+selfValue+'"]').hide();
                        //$this.find('option[value="'+selfValue+'"]').show();
                    }
                });
            });
            var pwrValidObj = {
                confirmField: "#cmp-acc-confirm-password",
                minLength: 8,
                minUpperCase: 1,
                minLowerCase: 1,
                minDigits: 1,
                minSpecial: 1
            };
            var numberRegex = /\d/;
            var splRegex = /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;
            $pwdElem.passwordValidation(pwrValidObj, function(element, valid, match, failedCases) {
                $("#errors").html("<pre>" + failedCases.join("\n") + "</pre>");
                if(valid) {
                    $('#cmp-acc-password-validator-message-success').removeClass('cmp-acc-hide');
                    $('#cmp-acc-password-validator-message-fail').addClass('cmp-acc-hide');
                    $pwdElem.removeClass('cmp-acc-input-error');
                    $pwdElem.closest('.cmp-acc-text-input-container').find('.cmp-acc-status').css({'background': 'url(../../img/create-account/lock-success@3x.png) no-repeat'});
                }
                if(!valid) {
                    $('#cmp-acc-password-validator-message-success').addClass('cmp-acc-hide');
                    $('#cmp-acc-password-validator-message-fail').removeClass('cmp-acc-hide');
                    $pwdElem.addClass('cmp-acc-input-error');
                    $pwdElem.closest('.cmp-acc-text-input-container').find('.cmp-acc-status').css({'background': 'url(../../img/create-account/lock-right-grey@3x.png) no-repeat'});
                }
                if(valid && match) {
                    $('#cmp-acc-error-confirm-password').addClass('cmp-acc-hide');
                    $('#cmp-acc-confirm-password').removeClass('cmp-acc-input-error');
                    $('#cmp-acc-confirm-password').closest('.cmp-acc-text-input-container').find('.cmp-acc-status').css({'background': 'url(../../img/create-account/check@44.png) no-repeat'});
                }
                if(!valid || !match) {
                    $('#cmp-acc-error-confirm-password').removeClass('cmp-acc-hide');
                    $('#cmp-acc-confirm-password').addClass('cmp-acc-input-error');
                    $('#cmp-acc-confirm-password').closest('.cmp-acc-text-input-container').find('.cmp-acc-status').css({'background': 'url(../../img/create-account/warning@44.png) no-repeat'});
                }
                if($pwdElem.val().length >= 8) {
                    $('#cmp-acc-password-min-num-of-chars').addClass('message-success');
                    $pwdElem.removeClass('cmp-acc-input-error');
                } else {
                    $('#cmp-acc-password-min-num-of-chars').removeClass('message-success');
                    $pwdElem.addClass('cmp-acc-input-error');
                }
                if($pwdElem.val().match(/[a-z]/) && $pwdElem.val().match(/[A-Z]/)) {
                    $('#cmp-acc-password-lower-upper-chars').addClass('message-success');
                    $pwdElem.removeClass('cmp-acc-input-error');
                } else {
                    $('#cmp-acc-password-lower-upper-chars').removeClass('message-success');
                    $pwdElem.addClass('cmp-acc-input-error');
                }
                if(numberRegex.test($pwdElem.val())) {
                    $('#cmp-acc-password-use-number').addClass('message-success');
                    $pwdElem.removeClass('cmp-acc-input-error');
                } else {
                    $('#cmp-acc-password-use-number').removeClass('message-success');
                    $pwdElem.addClass('cmp-acc-input-error');
                }
                if(splRegex.test($pwdElem.val())) {
                    $('#cmp-acc-password-use-symbol').addClass('message-success');
                    $pwdElem.removeClass('cmp-acc-input-error');
                } else {
                    $('#cmp-acc-password-use-symbol').removeClass('message-success');
                    $pwdElem.addClass('cmp-acc-input-error');
                }
            });
            $('.cmp-acc-user-available').on('click', function(e) {
                e.preventDefault();
                var strUN = $("#cmp-acc-email").val();
                $('.user-valid-alert').remove();
                if (strUN != "" && validateEmail(strUN)) {
                    var dataUrl = ipAddress + "/user/is-available/" + strUN + "/";
                    getAjaxObject(dataUrl, "GET", getUserAvailable, onError);
                } else if(strUN != "" && !validateEmail(strUN)) {

                }
            });
            var checkFlag = true;
            $('#cmp-acc-sign-up-submit-btn').on('click', function(e) {
                e.preventDefault();
                checkFlag = true;
                $('.cmp-acc-text-input-container:visible').find('input[required="required"], input.required-input, select[required="required"]').each(function() {
                    if($(this).val() === '' || $(this).hasClass('cmp-acc-input-error')) {
                        checkFlag = false;
                    }
                });
                if (checkFlag) {
                    hideShowContent();
                }
            });
            $('.cmp-acc-text-input-container').find('input[required="required"], input.required-input, select[required="required"]').on('change', function() {
                if($(this).val() === '' || $(this).hasClass('cmp-acc-input-error')) {
                    checkFlag = false;
                }
            });
            $('#nextBtn, #nextBtnAfterDevices,  #nextBtnAfterVerifyCode').on('click', function(e) {
                e.preventDefault();
                var strUN = $("#cmp-acc-email").val();
                checkFlag = true;
                $('.cmp-acc-text-input-container:visible').find('input[required="required"], input.required-input, select[required="required"]').each(function() {
                    if($(this).val() === '' || $(this).hasClass('cmp-acc-input-error')) {
                        checkFlag = false;
                    }
                });
                if (checkFlag) {
                    if($(this).attr('id') == 'nextBtnAfterVerifyCode') {
                        finalCheck = true;
                        var dataUrl = ipAddress + "/user/is-available/" + strUN + "/";
                        getAjaxObject(dataUrl, "GET", getUserCreate, onError);
                    } else {
                        hideShowContent();
                    }
                }
            });
            $('#finishBtn').on('click', function(e) {
                e.preventDefault();
                $('.cmp-acc-text-input-container:visible').find('input[required="required"], select[required="required"]').each(function() {
                    if($(this).val() === '' || $(this).hasClass('cmp-acc-input-error')) {
                        checkFlag = false;
                    }
                });
                if (checkFlag) {
                    var strOTP = $("#cmp-acc-otp").val();
                    if (strOTP != "") {
                        getAjaxObject(ipAddress + "/user/is-valid-otp/" + uId + "/" + strOTP, "GET", onIsValidOTP, onError);
                    }
                }
            });
            $('.flag-container').on('click', function() {
                $('.country-list').toggleClass('hide');
            });
            $('.country-list .country').on('click', function() {
                var country = $(this).attr('data-country-code');
                var countryCode = $(this).find('.dial-code').text();
                var attributeText = $(this).find('.dial-code').text() + " " + $(this).find('.country-name').text();
                $('.selected-flag').find('.iti-flag').addClass(country);
                $('.selected-flag').attr({'title': attributeText, 'data-attr-countrycode': countryCode});
                setTimeout(function() {
                    $('.country-list').addClass('hide');
                }, 200);
            });
            getAjaxObject(ipAddress + '/homecare/user-types/?fields=id,value', "GET", getUserType, onError);
            getAjaxObject(ipAddress + '/master/access_type/list', "GET", getAccessType, onError);
            getAjaxObject(ipAddress + "/security-question/list/", "GET", getSecurityQuestionList, onError);
            getAjaxObject(ipAddress + "/master/Gender/list/?is-active=1", "GET", getGenderValueList, onError);
            var countryName = getCountryName();
            if(countryName) {
                if(countryName.indexOf('India') >= 0) {
                    $('.selected-flag').find('.iti-flag').addClass('in');
                    $('.selected-flag').attr({'title': '+91 India', 'data-attr-countrycode': '+91'});
                } else if(countryName.indexOf("United State") >= 0) {
                    $('.selected-flag').find('.iti-flag').addClass('usa');
                    $('.selected-flag').attr({'title': '+91 India', 'data-attr-countrycode': '+1'});
                } else if(countryName.indexOf("United Kingdom") >= 0) {
                    $('.selected-flag').find('.iti-flag').addClass('uk');
                    $('.selected-flag').attr({'title': '+91 India', 'data-attr-countrycode': '+10'});
                }
            }
        };
        this.init();
        function getUserAvailable(dataObj) {
            $('.user-valid-alert').remove();
            if (dataObj && dataObj.response && dataObj.response.status) {
                if (dataObj.response.status.code == "1") {
                    $("#part-two-div").removeClass("hide");
                    $("#cmp-acc-fieldset-email").append('<span class="user-valid-alert message-success">Username is Available</span>');
                    $("#cmp-acc-email").attr('readonly', 'true');
                } else {
                    $("#part-two-div").addClass("hide");
                    $("#cmp-acc-fieldset-email").append('<span class="user-valid-alert message-error">Username is not available</span>');
                    $("#cmp-acc-email").removeAttr('readonly');
                }
            }
        }
        function getUserCreate(dataObj) {
            $('.user-valid-alert').remove();
            if (dataObj && dataObj.response && dataObj.response.status) {
                if (dataObj.response.status.code == "1") {
                    if(!finalCheck) {
                        $("#part-two-div").removeClass("hide");
                        $("#cmp-acc-fieldset-email").append('<span class="user-valid-alert message-success">Username is Available</span>');
                    }
                    createUserFunctionality();
                } else {
                    if(!finalCheck) {
                        $("#part-two-div").addClass("hide");
                        $("#cmp-acc-fieldset-email").append('<span class="user-valid-alert message-error">Username is not available</span>');
                    }
                }
            }
        }
        function createUserFunctionality() {
            var strUN = $.trim($("#cmp-acc-email").val());
            var strPass = $.trim($("#cmp-acc-password").val());
            var strFN = $.trim($("#cmp-acc-first-name").val());
            var strLN = $.trim($("#cmp-acc-last-name").val());
            var strMobileNo = $('.selected-flag').attr('data-attr-countrycode') ? $.trim($('.selected-flag').attr('data-attr-countrycode') + "" + $("#cmp-acc-mobile-phone").val()) : $.trim($("#cmp-acc-mobile-phone").val());
            var email = $.trim($("#cmp-acc-email").val());
            var reqObj = {
                username: strUN,
                password: strPass,
                firstName: strFN,
                lastName: strLN,
                mobile: strMobileNo,
                email: email,
                userTypeId: $('#cmp-acc-account-type').val(),
                userrole: $('#cmp-acc-access-type').val(),
                isActive: "1",
                isDeleted: "0"
            };
            var dataUrl = ipAddress + "/user/create";
            var dtItem = $("#cmp-acc-dob").data("kendoDatePicker");
            var strDate = "";
            if (dtItem && dtItem.value()) {
                strDate = kendo.toString(dtItem.value(), "yyyy-MM-dd");
                reqObj.dateOfBirth = strDate;
            }
            reqObj.gender = $("#cmp-acc-gender").val();

            reqObj.timezone = $.trim($("#cmp-acc-timezone").val());

            var sQuesions = [];

            var sObj = {};
            sObj.securityQuestionId = $("#cmp-acc-security-qs-1").val();
            sObj.answer = $("#cmp-acc-security-ans-1").val();

            sQuesions.push(sObj);

            var sObj1 = {};
            sObj1.securityQuestionId = $("#cmp-acc-security-qs-2").val();
            sObj1.answer = $("#cmp-acc-security-ans-2").val();

            sQuesions.push(sObj1);

            var sObj2 = {};
            sObj2.securityQuestionId = $("#cmp-acc-security-qs-3").val();
            sObj2.answer = $("#cmp-acc-security-ans-3").val();

            sQuesions.push(sObj2);
            reqObj.securityAnswers = sQuesions;
            reqObj.createdBy = sessionStorage.userId;
            console.log(reqObj);
            createAjaxObject(dataUrl, reqObj, "POST", createUserDetails, onError);
        }
        function getSecurityQuestionList(dataObj) {
            console.log(dataObj);
            $('#cmp-acc-security-qs-1').html('<option selected="true" disabled="disabled">Please Select</option>');
            $('#cmp-acc-security-qs-2').html('<option selected="true" disabled="disabled">Please Select</option>');
            $('#cmp-acc-security-qs-3').html('<option selected="true" disabled="disabled">Please Select</option>');
            if (dataObj && dataObj.length) {
                for (var i = 0; i < dataObj.length; i++) {
                    $('#cmp-acc-security-qs-1').append('<option value='+dataObj[i].id+'>'+dataObj[i].question+'</option>');
                    $('#cmp-acc-security-qs-2').append('<option value='+dataObj[i].id+'>'+dataObj[i].question+'</option>');
                    $('#cmp-acc-security-qs-3').append('<option value='+dataObj[i].id+'>'+dataObj[i].question+'</option>');
                }
            }
        }
        function getGenderValueList(resp) {
            $('#cmp-acc-gender').html('<option selected="true" disabled="disabled">Please Select</option>');
            var dataArray = [];
            if(resp && resp.response && resp.response.codeTable){
                if($.isArray(resp.response.codeTable)){
                    dataArray = resp.response.codeTable;
                }else{
                    dataArray.push(resp.response.codeTable);
                }
            }
            if(dataArray.length) {
                for (var i = 0; i < dataArray.length; i++) {
                    $('#cmp-acc-gender').append('<option value='+dataArray[i].id+'>'+dataArray[i].desc+'</option>');
                }
            }
        }
        function createUserDetails(dataObj) {
            console.log(dataObj);
            if (dataObj && dataObj.response && dataObj.response.status) {
                if (dataObj.response.status.code == "1") {
                    uId = dataObj.response.user.id;
                    $('#part-five-div').addClass('hide');
                    $('#part-six-div').removeClass('hide');
                } else {
                    customAlert.error("error", dataObj.response.status.message);
                }
            }
        }
        function hideShowContent() {
            if(!$('#part-one-div').hasClass('hide')) {
                $('#part-three-div').removeClass('hide');
                $('#part-one-div, #part-two-div').addClass('hide');
            } else if(!$('#part-three-div').hasClass('hide') && $('#nextBtn').attr('data-attr-application') != 'web') {
                $('#part-four-div').removeClass('hide');
                $('#part-three-div').addClass('hide');
            } else if(!$('#part-three-div').hasClass('hide') && $('#nextBtn').attr('data-attr-application') == 'web') {
                $('#part-five-div').removeClass('hide');
                $('#part-three-div').addClass('hide');
                $('#part-four-div').addClass('hide');
            }
            else if(!$('#part-four-div').hasClass('hide')) {
                $('#part-five-div').removeClass('hide');
                $('#part-four-div').addClass('hide');
            } else if(!$('#part-five-div').hasClass('hide')) {
                $('#part-six-div').removeClass('hide');
                $('#part-five-div').addClass('hide');
            }
            if($('#cmp-acc-access-type').val() === 'Web') {
                $('#nextBtn').attr('data-attr-application','web');
            } else {
                $('#nextBtn').attr('data-attr-application','not-web');
            }
        }
        function onIsValidOTP(dataObj) {
            if (dataObj && dataObj.response && dataObj.response.status) {
                if (dataObj.response.status.code == "1") {
                    $('#part-seven-div').removeClass('hide');
                    $('#part-six-div').addClass('hide');
                } else {
                    customAlert.info("error", "OPT is wrong");
                }
            }
        }
        function getUserType(resp) {
            var dataArray = [];
            if(resp && resp.response && resp.response.userTypes){
                if($.isArray(resp.response.userTypes)){
                    dataArray = resp.response.userTypes;
                }else{
                    dataArray.push(resp.response.userTypes);
                }
            }
            if(dataArray.length) {
                $('#cmp-acc-account-type').html('<option selected="true" disabled="disabled">Please Select</option>');
                for (var i = dataArray.length - 1; i >= 0; i--) {
                    $('#cmp-acc-account-type').append('<option value='+dataArray[i].id+'>'+dataArray[i].value+'</option>');
                }
            }
        }
        function getAccessType(resp) {
            var dataArray = [];
            if(resp && resp.response && resp.response.codeTable){
                if($.isArray(resp.response.codeTable)){
                    dataArray = resp.response.codeTable;
                }else{
                    dataArray.push(resp.response.codeTable);
                }
            }
            if(dataArray.length) {
                $('#cmp-acc-access-type').html('<option selected="true" disabled="disabled">Please Select</option>');
                for (var i = dataArray.length - 1; i >= 0; i--) {
                    $('#cmp-acc-access-type').append('<option value='+dataArray[i].value+'>'+dataArray[i].desc+'</option>');
                }
            }
        }
        function onError(errObj) {
            console.log(errObj);
            customAlert.error("Error", "Error");
        }
    };
    createUserObj();
});
(function($) {
    $.fn.extend({
        passwordValidation: function(_options, _callback, _confirmcallback) {
            //var _unicodeSpecialSet = "^\\x00-\\x1F\\x7F\\x80-\\x9F0-9A-Za-z"; //All chars other than above (and C0/C1)
            var CHARSETS = {
                upperCaseSet: "A-Z", 	//All UpperCase (Acii/Unicode)
                lowerCaseSet: "a-z", 	//All LowerCase (Acii/Unicode)
                digitSet: "0-9", 		//All digits (Acii/Unicode)
                specialSet: "\\x20-\\x2F\\x3A-\\x40\\x5B-\\x60\\x7B-\\x7E\\x80-\\xFF", //All Other printable Ascii
            }
            var _defaults = {
                minLength: 12,		  //Minimum Length of password
                minUpperCase: 2,	  //Minimum number of Upper Case Letters characters in password
                minLowerCase: 2,	  //Minimum number of Lower Case Letters characters in password
                minDigits: 2,		  //Minimum number of digits characters in password
                minSpecial: 2,		  //Minimum number of special characters in password
                maxRepeats: 5,		  //Maximum number of repeated alphanumeric characters in password dhgurAAAfjewd <- 3 A's
                maxConsecutive: 3,	  //Maximum number of alphanumeric characters from one set back to back
                noUpper: false,		  //Disallow Upper Case Lettera
                noLower: false,		  //Disallow Lower Case Letters
                noDigit: false,		  //Disallow Digits
                noSpecial: false,	  //Disallow Special Characters
                //NOT IMPLEMENTED YET allowUnicode: false,  //Switches Ascii Special Set out for Unicode Special Set
                failRepeats: true,    //Disallow user to have x number of repeated alphanumeric characters ex.. ..A..a..A.. <- fails if maxRepeats <= 3 CASE INSENSITIVE
                failConsecutive: true,//Disallow user to have x number of consecutive alphanumeric characters from any set ex.. abc <- fails if maxConsecutive <= 3
                confirmField: undefined
            };

            //Ensure parameters are correctly defined
            if($.isFunction(_options)) {
                if($.isFunction(_callback)) {
                    if($.isFunction(_confirmcallback)) {
                        console.log("Warning in passValidate: 3 or more callbacks were defined... First two will be used.");
                    }
                    _confirmcallback = _callback;
                }
                _callback = _options;
                _options = {};
            }

            //concatenate user options with _defaults
            _options = $.extend(_defaults, _options);
            if(_options.maxRepeats < 2) _options.maxRepeats = 2;

            function charsetToString() {
                return CHARSETS.upperCaseSet + CHARSETS.lowerCaseSet + CHARSETS.digitSet + CHARSETS.specialSet;
            }

            //GENERATE ALL REGEXs FOR EVERY CASE
            function buildPasswordRegex() {
                var cases = [];

                //if(_options.allowUnicode) CHARSETS.specialSet = _unicodeSpecialSet;
                if(_options.noUpper) 	cases.push({"regex": "(?=" + CHARSETS.upperCaseSet + ")",  																				"message": "Password can't contain an Upper Case Letter"});
                else 					cases.push({"regex": "(?=" + ("[" + CHARSETS.upperCaseSet + "][^" + CHARSETS.upperCaseSet + "]*").repeat(_options.minUpperCase) + ")", 	"message": "Password must contain at least " + _options.minUpperCase + " Upper Case Letters."});
                if(_options.noLower) 	cases.push({"regex": "(?=" + CHARSETS.lowerCaseSet + ")",  																				"message": "Password can't contain a Lower Case Letter"});
                else 					cases.push({"regex": "(?=" + ("[" + CHARSETS.lowerCaseSet + "][^" + CHARSETS.lowerCaseSet + "]*").repeat(_options.minLowerCase) + ")", 	"message": "Password must contain at least " + _options.minLowerCase + " Lower Case Letters."});
                if(_options.noDigit) 	cases.push({"regex": "(?=" + CHARSETS.digitSet + ")", 																					"message": "Password can't contain a Number"});
                else 					cases.push({"regex": "(?=" + ("[" + CHARSETS.digitSet + "][^" + CHARSETS.digitSet + "]*").repeat(_options.minDigits) + ")", 			"message": "Password must contain at least " + _options.minDigits + " Digits."});
                if(_options.noSpecial) 	cases.push({"regex": "(?=" + CHARSETS.specialSet + ")", 																				"message": "Password can't contain a Special Character"});
                else 					cases.push({"regex": "(?=" + ("[" + CHARSETS.specialSet + "][^" + CHARSETS.specialSet + "]*").repeat(_options.minSpecial) + ")", 		"message": "Password must contain at least " + _options.minSpecial + " Special Characters."});

                cases.push({"regex":"[" + charsetToString() + "]{" + _options.minLength + ",}", "message":"Password must contain at least " + _options.minLength + " characters."});

                return cases;
            }
            var _cases = buildPasswordRegex();

            var _element = this;
            var $confirmField = (_options.confirmField != undefined)? $(_options.confirmField): undefined;

            //Field validation on every captured event
            function validateField() {
                var failedCases = [];

                //Evaluate all verbose cases
                $.each(_cases, function(i, _case) {
                    if($(_element).val().search(new RegExp(_case.regex, "g")) == -1) {
                        failedCases.push(_case.message);
                    }
                });
                if(_options.failRepeats && $(_element).val().search(new RegExp("(.)" + (".*\\1").repeat(_options.maxRepeats - 1), "gi")) != -1) {
                    failedCases.push("Password can not contain " + _options.maxRepeats + " of the same character case insensitive.");
                }
                if(_options.failConsecutive && $(_element).val().search(new RegExp("(?=(.)" + ("\\1").repeat(_options.maxConsecutive) + ")", "g")) != -1) {
                    failedCases.push("Password can't contain the same character more than " + _options.maxConsecutive + " times in a row.");
                }

                //Determine if valid
                var validPassword = (failedCases.length == 0) && ($(_element).val().length >= _options.minLength);
                var fieldsMatch = true;
                if($confirmField != undefined) {
                    fieldsMatch = ($confirmField.val() == $(_element).val());
                }

                _callback(_element, validPassword, validPassword && fieldsMatch, failedCases);
            }

            //Add custom classes to fields
            this.each(function() {
                //Validate field if it is already filled
                if($(this).val()) {
                    validateField().apply(this);
                }

                $(this).toggleClass("jqPassField", true);
                if($confirmField != undefined) {
                    $confirmField.toggleClass("jqPassConfirmField", true);
                }
            });

            //Add event bindings to the password fields
            return this.each(function() {
                $(this).bind('keyup focus input proprtychange mouseup', validateField);
                if($confirmField != undefined) {
                    $confirmField.bind('keyup focus input proprtychange mouseup', validateField);
                }
            });
        }
    });
})(jQuery);
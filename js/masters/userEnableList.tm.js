var angularUIgridWrapper;
var parentRef = null;
var recordType = "1";
var dataArray = [];
var ADD = "add";
var UPDATE = "edit";
var VIEW = "view";
var DELETE = "delete";
var operation = "add";
var atID = "";
var ds  = "";
var isFlag = 0;
var universaluserId = "";
$(document).ready(function(){
    $("#pnlPatient",parent.document).css("display","none");
    sessionStorage.setItem("IsSearchPanel", "1");
    themeAPIChange();
    parentRef = parent.frames['iframe'].window;
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("dgridUserList2", dataOptions);
    angularUIgridWrapper.init();
    buildUserListGrid([]);

});


$(window).load(function(){
    loading = false;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
    init();
    buttonEvents();
    adjustHeight();
}
var userStatus = "1";
function init(){
    $("#btnSave").prop("disabled", true);
    $("#btnDelete").prop("disabled", true);
    $("#btnUpdateEmail").prop("disabled", true);
    $("#btnSave").hide();
    buildUserListGrid([]);
    var strUrl = "";
    if(userStatus == "1"){
        strUrl = ipAddress+"/homecare/tenant-users/?is-active=1";
    }else{
        strUrl = ipAddress+"/homecare/tenant-users/?is-active=:in:0,2,3";
    }
    getAjaxObject(strUrl,"GET",getUserList,onError);
}
var tempDataArry = [];
function getUserList(dataObj){
    // console.log(dataObj);
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            if($.isArray(dataObj.response.tenantUsers)){
                dataArray = dataObj.response.tenantUsers;
            }else{
                dataArray.push(dataObj.response.tenantUsers);
            }
        }
    }
    tempDataArry = [];
    for(var i=0;i<dataArray.length;i++){
        if (dataArray[i].userTypeId != 1000) {
            dataArray[i].idk = dataArray[i].id;
            dataArray[i].Status = "InActive";
            if (dataArray[i].isActive == 1) {
                dataArray[i].Status = "Active";
            }
            if (dataArray[i].userTypeId == 100) {
                dataArray[i].userType = "Doctor";
            } else if (dataArray[i].userTypeId == 200) {
                dataArray[i].userType = "Nurse";
            } else if (dataArray[i].userTypeId == 201) {
                dataArray[i].userType = "Caregiver";
            } else if (dataArray[i].userTypeId == 500) {
                dataArray[i].userType = "Service User";
            } else if (dataArray[i].userTypeId == 600) {
                dataArray[i].userType = "Relative";
            } else if (dataArray[i].userTypeId == 700) {
                dataArray[i].userType = "Care Manager";
            } else if (dataArray[i].userTypeId == 800) {
                dataArray[i].userType = "Carehome Carer";
            }
            tempDataArry.push(dataArray[i]);
        }
    }
    if(userStatus == 0){
        $("#btnSave").show();
    }
    else{
        $("#btnSave").hide();
    }
    buildUserListGrid(tempDataArry);
}
function onError(errorObj){
    console.log(errorObj);
}

function buttonEvents(){
    $("#btnSave").off("click",onClickOK);
    $("#btnSave").on("click",onClickOK);

    $("#btnDelete").off("click",onClickDelete);
    $("#btnDelete").on("click",onClickDelete);

    $("#btnCancelDet").off("click",onClickCancel);
    $("#btnCancelDet").on("click",onClickCancel);

    $("#btnUpdate").off("click",validatePassword);
    $("#btnUpdate").on("click",validatePassword);

    $("#btnActive").off("click");
    $("#btnActive").on("click",onClickActiveUsers);

    $("#btnInActive").off("click");
    $("#btnInActive").on("click",onClickInActiveUsers);

    $("#btnReset").off("click",onClickReset);
    $("#btnReset").on("click",onClickReset);

    $("#btnUpdateEmail").off("click",onClickUpdateEmail);
    $("#btnUpdateEmail").on("click",onClickUpdateEmail);

    $("#btnCancel").off("click");
    $("#btnCancel").on("click",onClickBack);

    $(".popupClose").off("click", onClickBack);
    $(".popupClose").on("click", onClickBack);


    $('#txtPassword + .glyphicon').on('click', function() {
        $(this).toggleClass('glyphicon-eye-close').toggleClass('glyphicon-eye-open'); // toggle our classes for the eye icon
        var passwordInput = document.getElementById('txtPassword');
        if (passwordInput.type == 'password')
        {
            passwordInput.type='text';
            /*passStatus.className='fa fa-eye-slash';*/
        }
        else
        {
            passwordInput.type='password';
            /*passStatus.className='fa fa-eye';*/
        }
    });

    $("#selUserFields").change(function(){
        $(".filter-heading").html("Update User Details");
        var selUserVal = $("#selUserFields").val();
        if(selUserVal == 1){
            onClickReset();
            $("#dvPassword").show();
            $("#dvAcessType").hide();
            $("#dvAccountType").hide();
        }
        else if(selUserVal == 2){
            onClickUpdateEmail();
            $("#dvAcessType").hide();
            $("#dvAccountType").hide();
        }
        else if(selUserVal == 3) {
            $('#selAccessType').multipleSelect('refresh');
            isFlag = 2;
            $("#dvAcessType").show();
            $("#dvPassword").hide();
            $("#dvEmail").hide();
            $("#dvAccountType").hide();
            $("#lblAccessType").empty();
            $("#lblAccessType").html("Present Access Type <span style='margin-left: 13px;'>" + selAccessType+"</span>");
            $("#selAccessType").multipleSelect({
                selectAll: false,
                placeholder: 'Select Access Type'
            });

            getAjaxObject(ipAddress + '/homecare/user-access-types/?user-id='+atID, "GET", getUserAccessType, onError);
        }
        else if(selUserVal == 4){
            getAjaxObject(ipAddress + '/homecare/user-types/?fields=id,value', "GET", getUserType, onError);
            isFlag =3;
            $("#dvAcessType").hide();
            $("#dvPassword").hide();
            $("#dvEmail").hide();
            $("#dvAccountType").show();
            $("#lblAccountType").empty();
            $("#lblAccountType").html("Present Account Type  <span style='margin-left: 4px;'> "+ selAccountType+"</span>");
        }
    });

}

function getUserType(resp) {
    var dataArray = [];
    $('#selAccountType').empty();
    if(resp && resp.response && resp.response.userTypes){
        if($.isArray(resp.response.userTypes)){
            dataArray = resp.response.userTypes;
        }else{
            dataArray.push(resp.response.userTypes);
        }
    }
    if(dataArray.length) {
        $('#selAccountType').html('<option selected="true" disabled="disabled">Please Select</option>');
        for (var i = dataArray.length - 1; i >= 0; i--) {
            if(dataArray[i].value.toLowerCase() != "admin"){
                if(dataArray[i].value.toLowerCase() == "patient") {
                    $('#selAccountType').append('<option value=' + dataArray[i].id + '>' + 'Service User' + '</option>');
                }
                else{
                    $('#selAccountType').append('<option value=' + dataArray[i].id + '>' + dataArray[i].value + '</option>');
                }
            }

        }
    }
}

var objselUserAccessType;

function getUserAccessType(resp) {
    var dataArray = [];
    $('#selAccountType').empty();
    if(resp && resp.response && resp.response.userAccessTypes){
        if($.isArray(resp.response.userAccessTypes)){
            dataArray = resp.response.userAccessTypes;
        }else{
            dataArray.push(resp.response.userAccessTypes);
        }
    }
    objselUserAccessType = dataArray;
}


function onClickOK(){
    customAlert.confirm("Confirm", "Are you sure you want Enable?",function(response){
        if(response.button == "Yes"){
            var selectedItems = angularUIgridWrapper.getSelectedRows();
            console.log(selectedItems);
            if(selectedItems && selectedItems.length>0){
                var selItem = selectedItems[0];
                if(selItem){
                    var dataUrl = ipAddress+"/user/enable/";
                    var reqObj = {};
                    var idk = selItem.idk
                    reqObj.id = Number(idk);
                    reqObj.enabled = true;
                    createAjaxObject(dataUrl,reqObj,"POST",onDeleteCountryt1,onError);
                }
            }
        }
    });
}
function onDeleteCountryt1(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            customAlert.info("info", "User successfully enabled");
            init();
        }else{
            customAlert.error("error", response.status.message);
        }
    }
}

function onClickAdd(){
    $("#addPopup").show();
    $('#viewDivBlock').hide();
    $("#txtID").hide();
    parentRef.operation = "add";
    onClickReset();
}



function onClickActive() {
    $(".btnInActive").removeClass("radioButton-active");
    $(".btnActive").addClass("radioButton-active");
}

function onClickInActive() {
    $(".btnActive").removeClass("radioButton-active");
    $(".btnInActive").addClass("radioButton-active");
}

function onClickDelete(){
    customAlert.confirm("Confirm", "Are you sure you want Disable?",function(response){
        if(response.button == "Yes"){
            var selectedItems = angularUIgridWrapper.getSelectedRows();
            console.log(selectedItems);
            if(selectedItems && selectedItems.length>0){
                var selItem = selectedItems[0];
                if(selItem){
                    var dataUrl = ipAddress+"/user/enable/";
                    var reqObj = {};
                    var idk = selItem.idk
                    reqObj.id = Number(idk);
                    reqObj.enabled = false;
                    createAjaxObject(dataUrl,reqObj,"POST",onDeleteCountryt,onError);
                }
            }
        }
    });
}
function onDeleteCountryt(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            customAlert.info("info", "User successfully disabled");
            init();
        }else{
            customAlert.error("error", response.status.message);
        }
    }
}
function onClickSave(){
    var strAbb = $("#txtAbbreviation").val();
    strAbb = $.trim(strAbb);

    var strDescription = $("#txtDescription").val();
    strDescription = $.trim(strDescription);

    var dataObj = {};
    dataObj.createdBy = Number(sessionStorage.userId);
    if(parseInt($("#cmbStatus").val()) == 1){
        dataObj.isDeleted = 0;
    }
    else{
        dataObj.isDeleted = 1;
    }
    dataObj.isActive = parseInt($("#cmbStatus").val());
    dataObj.code = strAbb;
    dataObj.value = strDescription;
    var dataUrl = ipAddress +"/homecare/billing-rate-types/";
    var method = "POST";
    if(operation == UPDATE){
        method = "PUT";
        dataObj.id = atID;
    }
    createAjaxObject(dataUrl, dataObj, method, onCreate, onError);
}

function onCreate(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "Billing Rate Type created successfully";
            if(operation == UPDATE){
                msg = "Billing Rate Type updated successfully"
            }
            $("#divID").css("display","none");
            $("#billTitle").html("View Rate Type");
            displaySessionErrorPopUp("Info", msg, function(res) {
                // $("#txtAT").val("");
                operation = ADD;
                onClickCancel();
                init();
                onClickActive();
            })
        }else{
            customAlert.error("Error", dataObj.response.status.message);
        }
    }else{

    }
    onClickRest();
}
function onClickCancel(){
    var obj = {};
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}

function adjustHeight(){
    var defHeight = 230;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    angularUIgridWrapper.adjustGridHeight(cmpHeight);
}


function buildUserListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "ID",
        "field": "idk",
        "cellTemplate": "<div  style='text-decoration:underline;cursor:pointer' class='ui-grid-cell-contents' onclick='onClickRPLink(2)'><a style='font-size: 13px;color: #2f98d29c'>{{row.entity.idk}}</a></div>",
    });
    gridColumns.push({
        "title": "Last Name",
        "field": "lastName"
    });
    gridColumns.push({
        "title": "First Name",
        "field": "firstName"
    });
    // gridColumns.push({
    //     "title": "Middle Name",
    //     "field": "middleName",
    //     "width": "20%",
    // });
    gridColumns.push({
        "title": "User Type",
        "field": "userType"
    });

    gridColumns.push({
        "title": "Access Type",
        "field": "accessTypes"
    });



    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}

var selAccessType = "", selAccountType = "";
function onChange(){
    setTimeout(function() {
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        // console.log(selectedItems);
        if (selectedItems && selectedItems.length > 0) {
            var obj = selectedItems[0];
            atID = obj.idk;
            if(obj.accessTypes && obj.accessTypes != undefined) {
                selAccessType = obj.accessTypes;
            }

            $("#txtID").html("ID : " + atID + ",  Name : " + obj.lastName + " " + obj.firstName);

            selAccountType = obj.userTypeValue;
            universaluserId = obj.universalUserId;
            $("#btnSave").prop("disabled", false);
            $("#btnDelete").prop("disabled", false);
            $("#btnUpdateEmail").prop("disabled", false);
            $("#btnReset").prop("disabled", false);
        } else {
            $("#btnSave").prop("disabled", true);
            $("#btnDelete").prop("disabled", true);
            $("#btnUpdateEmail").prop("disabled", true);
            $("#btnReset").prop("disabled", false);
        }
    });
}

function onClickEdit() {
    $("#txtID").show();
    $(".filter-heading").html("Edit billing Rate Type");
    parentRef.operation = "edit";
    $("#viewDivBlock").hide();
    $("#addPopup").show();
    var selectedItems = angularUIgridWrapper.getSelectedRows();
    if (selectedItems && selectedItems.length > 0) {
        var obj = selectedItems[0];
        atID = obj.idk;
        $("#txtAbbreviation").val(obj.code);
        $("#txtDescription").val(obj.value);
        $("#txtID").html("ID : " + obj.idk);

        $("#cmbStatus").val(obj.isActive);
    }
}

var operation = "add";
function onStatusChange(){
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if(cmbStatus && cmbStatus.selectedIndex<0){
        cmbStatus.select(0);
    }
}

function onClickReset() {
    if(operation == ADD) {
        operation = ADD;
    }
    else {
        operation = UPDATE;
    }
    $("#txtAbbreviation").val("");
    $("#txtDescription").val("");
    $("#cmbStatus").val(1);


}

function themeAPIChange(){
    getAjaxObject(ipAddress+"/homecare/settings/?id=2","GET",getThemeValue,onError);
}

function getThemeValue(dataObj){
    console.log(dataObj);
    var tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.activityTypes){
            if($.isArray(dataObj.response.settings)){
                tempCompType = dataObj.response.settings;
            }else{
                tempCompType.push(dataObj.response.settings);
            }
        }
        if(tempCompType.length > 0){
            themesChange(tempCompType[0].value);
        }
    }
}

function themesChange(themeValue){
    if(themeValue == 2){
        loadAPi("../../Theme2/Theme02.css");
    }
    else if(themeValue == 3){
        loadAPi("../../Theme3/Theme03.css");
    }
}



function validatePassword() {

    if(isFlag == 0) {
        var pwd = $("#txtPassword").val();
        var cpwd = $("#txtConfirmPassword").val();

        if (pwd == "" || cpwd == "") {
            customAlert.error("error", "Please enter mandatory fields");
        } else if (pwd != cpwd) {
            customAlert.error("error", "Enter confirm password same as password");
        } else {
            onClickUpdate();
        }
    }
    else if(isFlag == 1){
        var email = $("#txtEmail").val();
        if(email == ""){
            customAlert.error("error", "Please enter mandatory field");
        }
        else{
            onClickUpdate();
        }
    }
    else if(isFlag == 2 || isFlag == 3){
        onClickUpdate();
    }


}

function onClickUpdate(){
    var dataUrl;
    var reqObj = {};
    if(isFlag == 0){
        dataUrl = ipAddress+"/user/password/update/";
        var pwd = $("#txtPassword").val();
        reqObj.password = pwd;
        var idk = universaluserId;
        reqObj.universalUserId = Number(idk);
        createAjaxObject(dataUrl,reqObj,"POST",onUpdateSuccess,onError);
    }
    else if(isFlag == 1){
        dataUrl = ipAddress+"/homecare/tenant-users/";
        reqObj.email = $("#txtEmail").val();
        reqObj.id = atID;
        createAjaxObject(dataUrl,reqObj,"PUT",onUpdateSuccess,onError);
    }
    else if(isFlag == 2) {
        dataUrl = ipAddress+"/homecare/user-access-types/batch/";
        var accessTypeIds = $('#selAccessType').multipleSelect('getSelects');
        var userAccessTypes = [];
        if(accessTypeIds && accessTypeIds.length > 0) {
            for(var a=0;a<accessTypeIds.length;a++){
                var obj = {};
                var response = _.where(objselUserAccessType, {accessTypeId: Number(accessTypeIds[a])});
                if(response && response.length > 0) {
                    obj.accessTypeId = response[0].accessTypeId;
                    obj.id = response[0].id;
                    obj.modifiedBy = Number(sessionStorage.userId);
                }
                else{
                    obj.accessTypeId = Number(accessTypeIds[a]);
                    obj.createdBy = Number(sessionStorage.userId);
                }
                obj.userId = atID;
                obj.isDeleted = 0;
                obj.isActive = 1;
                userAccessTypes.push(obj);
            }

            createAjaxObject(dataUrl, userAccessTypes, "PUT", onUpdateSuccess, onError);
        }
        else{
            customAlert.error("error", "Please select mandatory field");
        }
    }
    else if(isFlag == 3) {
        dataUrl = ipAddress+"/homecare/tenant-users/";
        reqObj.userTypeId = $("#selAccountType").val();
        reqObj.id = atID;
        createAjaxObject(dataUrl,reqObj,"PUT",onUpdateSuccess,onError);
    }
}




function onUpdateSuccess(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            var msg;
            if(isFlag == 0){
                msg = "User password updated successfully";
            }
            else if(isFlag == 1){
                msg = "User email updated successfully";
            }
            else if(isFlag == 2) {
                msg = "User access type updated successfully";
            }
            else if(isFlag == 3){
                msg = "User account type updated successfully";
            }

            displaySessionErrorPopUp("Info", msg, function(res) {
                onClickBack();
                onClickActiveUsers();
            });

        }else{
            customAlert.error("error", response.status.message);
        }
    }
}


function onClickActiveUsers(){
    userStatus = "1";
    $("#btnSave").hide();
    $("#btnDelete").show();
    $(".btnInActive").removeClass("radioButton-active");
    $(".btnActive").addClass("radioButton-active");
    //init();

    buildUserListGrid([]);
    var strUrl = "";

    strUrl = ipAddress+"/homecare/tenant-users/?is-active=1";
    getAjaxObject(strUrl,"GET",getUserList,onError);
    angularUIgridWrapper.refreshGrid();
}
function onClickInActiveUsers(){
    $("#btnDelete").hide();
    $("#btnSave").show();
    $(".btnInActive").addClass("radioButton-active");
    $(".btnActive").removeClass("radioButton-active");
    userStatus = "0";
    //init();
    buildUserListGrid([]);
    var strUrl = "";

    strUrl = ipAddress+"/homecare/tenant-users/?is-active=:in:0,2,3";

    getAjaxObject(strUrl,"GET",getUserList,onError);
    angularUIgridWrapper.refreshGrid();
}

function onClickReset() {
    isFlag =0;
    createUserObj();
    // var strUrl = ipAddress+"/homecare/tenant-user-emails/?user-id="+Number(parentRef.selItem.idk);
    // getAjaxObject(strUrl,"GET",getUserEmailList,onError);
    $(".addtaskgroupContainer").show();
    $("#viewDivBlock").hide();
    $("#addPopup").show();
    $("#selUserFields").val(1);
    $("#dvPassword").show();
    $("#dvEmail").hide();
    $("#dvAcessType").hide();
    $("#dvAccountType").hide();
    // $(".filter-heading").html("Reset User Password");
}



(function($) {
    $.fn.extend({
        passwordValidation: function(_options, _callback, _confirmcallback) {
            //var _unicodeSpecialSet = "^\\x00-\\x1F\\x7F\\x80-\\x9F0-9A-Za-z"; //All chars other than above (and C0/C1)
            var CHARSETS = {
                upperCaseSet: "A-Z", 	//All UpperCase (Acii/Unicode)
                lowerCaseSet: "a-z", 	//All LowerCase (Acii/Unicode)
                digitSet: "0-9", 		//All digits (Acii/Unicode)
                specialSet: "\\x20-\\x2F\\x3A-\\x40\\x5B-\\x60\\x7B-\\x7E\\x80-\\xFF", //All Other printable Ascii
            }
            var _defaults = {
                minLength: 12,		  //Minimum Length of password
                minUpperCase: 2,	  //Minimum number of Upper Case Letters characters in password
                minLowerCase: 2,	  //Minimum number of Lower Case Letters characters in password
                minDigits: 2,		  //Minimum number of digits characters in password
                minSpecial: 2,		  //Minimum number of special characters in password
                maxRepeats: 5,		  //Maximum number of repeated alphanumeric characters in password dhgurAAAfjewd <- 3 A's
                maxConsecutive: 3,	  //Maximum number of alphanumeric characters from one set back to back
                noUpper: false,		  //Disallow Upper Case Lettera
                noLower: false,		  //Disallow Lower Case Letters
                noDigit: false,		  //Disallow Digits
                noSpecial: false,	  //Disallow Special Characters
                //NOT IMPLEMENTED YET allowUnicode: false,  //Switches Ascii Special Set out for Unicode Special Set
                failRepeats: true,    //Disallow user to have x number of repeated alphanumeric characters ex.. ..A..a..A.. <- fails if maxRepeats <= 3 CASE INSENSITIVE
                failConsecutive: true,//Disallow user to have x number of consecutive alphanumeric characters from any set ex.. abc <- fails if maxConsecutive <= 3
                confirmField: undefined
            };

            //Ensure parameters are correctly defined
            if($.isFunction(_options)) {
                if($.isFunction(_callback)) {
                    if($.isFunction(_confirmcallback)) {
                        console.log("Warning in passValidate: 3 or more callbacks were defined... First two will be used.");
                    }
                    _confirmcallback = _callback;
                }
                _callback = _options;
                _options = {};
            }

            //concatenate user options with _defaults
            _options = $.extend(_defaults, _options);
            if(_options.maxRepeats < 2) _options.maxRepeats = 2;

            function charsetToString() {
                return CHARSETS.upperCaseSet + CHARSETS.lowerCaseSet + CHARSETS.digitSet + CHARSETS.specialSet;
            }

            //GENERATE ALL REGEXs FOR EVERY CASE
            function buildPasswordRegex() {
                var cases = [];

                //if(_options.allowUnicode) CHARSETS.specialSet = _unicodeSpecialSet;
                if(_options.noUpper) 	cases.push({"regex": "(?=" + CHARSETS.upperCaseSet + ")",  																				"message": "Password can't contain an Upper Case Letter"});
                else 					cases.push({"regex": "(?=" + ("[" + CHARSETS.upperCaseSet + "][^" + CHARSETS.upperCaseSet + "]*").repeat(_options.minUpperCase) + ")", 	"message": "Password must contain at least " + _options.minUpperCase + " Upper Case Letters."});
                if(_options.noLower) 	cases.push({"regex": "(?=" + CHARSETS.lowerCaseSet + ")",  																				"message": "Password can't contain a Lower Case Letter"});
                else 					cases.push({"regex": "(?=" + ("[" + CHARSETS.lowerCaseSet + "][^" + CHARSETS.lowerCaseSet + "]*").repeat(_options.minLowerCase) + ")", 	"message": "Password must contain at least " + _options.minLowerCase + " Lower Case Letters."});
                if(_options.noDigit) 	cases.push({"regex": "(?=" + CHARSETS.digitSet + ")", 																					"message": "Password can't contain a Number"});
                else 					cases.push({"regex": "(?=" + ("[" + CHARSETS.digitSet + "][^" + CHARSETS.digitSet + "]*").repeat(_options.minDigits) + ")", 			"message": "Password must contain at least " + _options.minDigits + " Digits."});
                if(_options.noSpecial) 	cases.push({"regex": "(?=" + CHARSETS.specialSet + ")", 																				"message": "Password can't contain a Special Character"});
                else 					cases.push({"regex": "(?=" + ("[" + CHARSETS.specialSet + "][^" + CHARSETS.specialSet + "]*").repeat(_options.minSpecial) + ")", 		"message": "Password must contain at least " + _options.minSpecial + " Special Characters."});

                cases.push({"regex":"[" + charsetToString() + "]{" + _options.minLength + ",}", "message":"Password must contain at least " + _options.minLength + " characters."});

                return cases;
            }
            var _cases = buildPasswordRegex();

            var _element = this;
            var $confirmField = (_options.confirmField != undefined)? $(_options.confirmField): undefined;

            //Field validation on every captured event
            function validateField() {
                var failedCases = [];

                //Evaluate all verbose cases
                $.each(_cases, function(i, _case) {
                    if($(_element).val().search(new RegExp(_case.regex, "g")) == -1) {
                        failedCases.push(_case.message);
                    }
                });
                if(_options.failRepeats && $(_element).val().search(new RegExp("(.)" + (".*\\1").repeat(_options.maxRepeats - 1), "gi")) != -1) {
                    failedCases.push("Password can not contain " + _options.maxRepeats + " of the same character case insensitive.");
                }
                if(_options.failConsecutive && $(_element).val().search(new RegExp("(?=(.)" + ("\\1").repeat(_options.maxConsecutive) + ")", "g")) != -1) {
                    failedCases.push("Password can't contain the same character more than " + _options.maxConsecutive + " times in a row.");
                }

                //Determine if valid
                var validPassword = (failedCases.length == 0) && ($(_element).val().length >= _options.minLength);
                var fieldsMatch = true;
                if($confirmField != undefined) {
                    fieldsMatch = ($confirmField.val() == $(_element).val());
                }

                _callback(_element, validPassword, validPassword && fieldsMatch, failedCases);
            }

            //Add custom classes to fields
            this.each(function() {
                //Validate field if it is already filled
                if($(this).val()) {
                    //validateField().apply(this);
                }

                $(this).toggleClass("jqPassField", true);
                if($confirmField != undefined) {
                    $confirmField.toggleClass("jqPassConfirmField", true);
                }
            });

            //Add event bindings to the password fields
            return this.each(function() {
                $(this).bind('keyup focus input proprtychange mouseup', validateField);
                if($confirmField != undefined) {
                    $confirmField.bind('keyup focus input proprtychange mouseup', validateField);
                }
            });
        }
    });
})(jQuery);

function createUserObj() {
    var self = this,
        $pwdElem = $("#txtPassword"),
        uId = '',
        tzArray = [],
        operation = "",
        ADD = "add",
        UPDATE = "update",
        finalCheck = false;
    this.init = function() {
        this.bindEvents();
        allowSplAlphaNumericWithoutSpace('txtPassword');
        allowSplAlphaNumericWithoutSpace('txtConfirmPassword');

    };

    this.bindEvents = function() {

        // $('.cmp-acc-text-input-container').find('input.cmp-acc-text-input[required="required"], input.required-input').on('focusout blur keyup', function() {
        // 	var $this = $(this);
        // 	$this.closest('.cmp-acc-text-input-container').find('.cmp-acc-info').removeClass('cmp-acc-banner cmp-acc-defaultposition').addClass('cmp-acc-hide');
        // 	if($this.attr('id') !== 'cmp-acc-password') {
        // 		errortypeobj = {
        // 			'background': "url(../../img/create-account/warning@44.png) no-repeat"
        // 		}
        // 		successtypeobj = {
        // 			'background': 'url(../../img/create-account/check@44.png) no-repeat'
        // 		}
        // 		if($this.val().length == 0) {
        // 			$this.closest('.cmp-acc-text-input-container').find('.cmp-acc-status').css(errortypeobj);
        // 			$this.addClass('cmp-acc-input-error');
        // 		} else if($this.attr('type') !== 'password') {
        // 			$this.closest('.cmp-acc-text-input-container').find('.cmp-acc-status').css(successtypeobj);
        // 			$this.removeClass('cmp-acc-input-error');
        // 		}
        // 	} else if ($this.attr('id') === 'cmp-acc-password') {
        // 		errortypeobj = {
        // 			'background': "url(../../img/create-account/lock-left-grey@3x.png) no-repeat"
        // 		}
        // 		successtypeobj = {
        // 			'background': 'url(../../img/create-account/lock-right-grey@3x.png) no-repeat'
        // 		}
        // 		if($this.val().length == 0) {
        // 			$this.closest('.cmp-acc-text-input-container').find('.cmp-acc-status').css(errortypeobj);
        // 			$this.addClass('cmp-acc-input-error');
        // 		} else {
        // 			$this.closest('.cmp-acc-text-input-container').find('.cmp-acc-status').css(successtypeobj);
        // 			$this.removeClass('cmp-acc-input-error');
        // 		}
        // 	}
        // }).on('focus', function() {
        // 	var $this = $(this);
        // 	$this.closest('.cmp-acc-text-input-container').find('.cmp-acc-info').removeClass('cmp-acc-hide').addClass('cmp-acc-banner cmp-acc-defaultposition');
        // });
        /*$('.cmp-acc-select-security').on('change', function() {
            $('.cmp-acc-select-security').find('option').show();
            $('.cmp-acc-select-security').each(function() {
                var $this = $(this);
                var selfValue = $this.val();
                if(selfValue) {
                    // $this.find('option').show();
                    $('.cmp-acc-select-security').not(this).find('option[value="'+selfValue+'"]').hide();
                    //$this.find('option[value="'+selfValue+'"]').show();
                }
            });
        });*/
        var pwrValidObj = {
            confirmField: "#txtConfirmPassword",
            minLength: 8,
            minUpperCase: 1,
            minLowerCase: 1,
            minDigits: 1,
            minSpecial: 1
        };
        var numberRegex = /\d/;
        var splRegex = /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;
        $pwdElem.passwordValidation(pwrValidObj, function(element, valid, match, failedCases) {
            $("#errors").html("<pre>" + failedCases.join("\n") + "</pre>");
            if(valid) {
                $('#cmp-acc-password-validator-message-success').removeClass('cmp-acc-hide');
                $('#cmp-acc-password-validator-message-fail').addClass('cmp-acc-hide');
                $pwdElem.removeClass('cmp-acc-input-error');
                $pwdElem.closest('.cmp-acc-text-input-container').find('.cmp-acc-status').css({'background': 'url(../../img/create-account/lock-success@3x.png) no-repeat'});
            }
            if(!valid) {
                $('#cmp-acc-password-validator-message-success').addClass('cmp-acc-hide');
                $('#cmp-acc-password-validator-message-fail').removeClass('cmp-acc-hide');
                $pwdElem.addClass('cmp-acc-input-error');
                $pwdElem.closest('.cmp-acc-text-input-container').find('.cmp-acc-status').css({'background': 'url(../../img/create-account/lock-right-grey@3x.png) no-repeat'});
            }
            if(valid && match) {
                $('#cmp-acc-error-confirm-password').addClass('cmp-acc-hide');
                $('#cmp-acc-confirm-password').removeClass('cmp-acc-input-error');
                $('#cmp-acc-confirm-password').closest('.cmp-acc-text-input-container').find('.cmp-acc-status').css({'background': 'url(../../img/create-account/check@44.png) no-repeat'});
            }
            if(!valid || !match) {
                $('#cmp-acc-error-confirm-password').removeClass('cmp-acc-hide');
                $('#cmp-acc-confirm-password').addClass('cmp-acc-input-error');
                $('#cmp-acc-confirm-password').closest('.cmp-acc-text-input-container').find('.cmp-acc-status').css({'background': 'url(../../img/create-account/warning@44.png) no-repeat'});
            }
            if($pwdElem.val().length >= 8) {
                $('#cmp-acc-password-min-num-of-chars').addClass('message-success');
                $pwdElem.removeClass('cmp-acc-input-error');
            } else {
                $('#cmp-acc-password-min-num-of-chars').removeClass('message-success');
                $pwdElem.addClass('cmp-acc-input-error');
            }
            if($pwdElem.val().match(/[a-z]/) && $pwdElem.val().match(/[A-Z]/)) {
                $('#cmp-acc-password-lower-upper-chars').addClass('message-success');
                $pwdElem.removeClass('cmp-acc-input-error');
            } else {
                $('#cmp-acc-password-lower-upper-chars').removeClass('message-success');
                $pwdElem.addClass('cmp-acc-input-error');
            }
            if(numberRegex.test($pwdElem.val())) {
                $('#cmp-acc-password-use-number').addClass('message-success');
                $pwdElem.removeClass('cmp-acc-input-error');
            } else {
                $('#cmp-acc-password-use-number').removeClass('message-success');
                $pwdElem.addClass('cmp-acc-input-error');
            }
            if(splRegex.test($pwdElem.val())) {
                $('#cmp-acc-password-use-symbol').addClass('message-success');
                $pwdElem.removeClass('cmp-acc-input-error');
            } else {
                $('#cmp-acc-password-use-symbol').removeClass('message-success');
                $pwdElem.addClass('cmp-acc-input-error');
            }
        });
        /*$('.cmp-acc-user-available').on('click', function(e) {
            e.preventDefault();
            var strUN = $("#cmp-acc-email").val();
            $('.user-valid-alert').remove();
            if (strUN != "" && validateEmail(strUN)) {
                var dataUrl = ipAddress + "/user/is-available/" + strUN + "/";
                getAjaxObject(dataUrl, "GET", getUserAvailable, onError);
            } else if(strUN != "" && !validateEmail(strUN)) {

            }
        });
        var checkFlag = true;
        $('#cmp-acc-sign-up-submit-btn').on('click', function(e) {
            e.preventDefault();
            checkFlag = true;
            $('.cmp-acc-text-input-container:visible').find('input[required="required"], input.required-input, select[required="required"]').each(function() {
                if($(this).val() === '' || $(this).hasClass('cmp-acc-input-error')) {
                    checkFlag = false;
                }
            });
            if (checkFlag) {
                hideShowContent();
            }
        });
        $('.cmp-acc-text-input-container').find('input[required="required"], input.required-input, select[required="required"]').on('change', function() {
            if($(this).val() === '' || $(this).hasClass('cmp-acc-input-error')) {
                checkFlag = false;
            }
        });
        $('#nextBtn, #nextBtnAfterDevices,  #nextBtnAfterVerifyCode').on('click', function(e) {
            e.preventDefault();
            var strUN = $("#cmp-acc-email").val();
            checkFlag = true;
            $('.cmp-acc-text-input-container:visible').find('input[required="required"], input.required-input, select[required="required"]').each(function() {
                if($(this).val() === '' || $(this).hasClass('cmp-acc-input-error')) {
                    checkFlag = false;
                }
            });
            if (checkFlag) {
                if($(this).attr('id') == 'nextBtnAfterVerifyCode') {
                    finalCheck = true;
                    var dataUrl = ipAddress + "/user/is-available/" + strUN + "/";
                    getAjaxObject(dataUrl, "GET", getUserCreate, onError);
                } else {
                    hideShowContent();
                }
            }
        });*/

    };
    this.init();
};

function onClickUpdateEmail(){
    isFlag =1;
    $("#txtEmail").val("");
    $(".addtaskgroupContainer").show();
    $("#addPopup").show();
    $("#viewDivBlock").hide();
    $("#dvPassword").hide();
    $("#dvEmail").show();
    var strUrl = ipAddress+"/homecare/tenant-user-emails/?user-id="+atID;
    getAjaxObject(strUrl,"GET",getUserEmailList,onError);
}


function getUserEmailList(dataObj){
    // console.log(dataObj);
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            if($.isArray(dataObj.response.userEmails)){
                dataArray = dataObj.response.userEmails;
            }else{
                dataArray.push(dataObj.response.userEmails);
            }
        }
    }
    var tempDataArry = [];
    $("#lblEmail").val("");
    for(var i=0;i<dataArray.length;i++){
        $("#lblEmail").html("Present Email  <span style='margin-left: 58px;'>"+ dataArray[i].email+"</span>");
    }

}

function onClickBack(){

    $(".filter-heading").html("Users List");
    $("#viewDivBlock").show();
    $(".addtaskgroupContainer").hide();
}

function onClickRPLink(id) {
    var rows = angularUIgridWrapper.getSelectedRows();
    if (rows && rows.length > 0) {
        var row = rows[0];
        getUserName(row.idk);
    }
}

function getUserName(userId){
    getAjaxObject(ipAddress+"/homecare/tenant-users/?fields=username&id="+userId,"GET",onGetUserName,onError);
}

function onGetUserName(dataObj){
    var userPtList = [];
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            if($.isArray(dataObj.response.tenantUsers)){
                userPtList = dataObj.response.tenantUsers;
            }else{
                userPtList.push(dataObj.response.tenantUsers);
            }
            customAlert.info("UserName", userPtList[0].username);
        }else{
            customAlert.info("Info", dataObj.response.status.message);
        }
    }

}
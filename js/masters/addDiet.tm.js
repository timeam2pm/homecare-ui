var angularUIgridWrapper = null;
var windowLoad = false;
var selectedItems;
var activeListFlag = true;
function bindEvents() {
    $('.sidebar-nav-link').on('click',function() {
        $(this).closest('li').addClass('activeList').siblings().removeClass('activeList');
        $('.medicationForm-form')[0].reset();
        $('.hasError').hide();
        $('.alert').remove();
        $('#medication-save').show();
        $('#medication-reset').show();
        $('#medication-update').hide();
        $('#medication-cancel').hide();
        $('[data-medicate-list="1"]').find('.tablist-link').text("Add");
        $('.tableListbtn-wrapper').find('.medication-btn-link').attr('disabled','disabled');
        $('.medicationForm-form').find('option').each(function() {
            if($(this).attr('data-medicate-status') == 1) {
                $(this).attr('selected','selected');
            }
            else {
                $(this).removeAttr('selected');
            }
        });
        $('.hasError-rxNorm').remove();
        $('.non-rxNorm-form').show();
        if(windowLoad && $('.tab-list.active-tabList').attr('data-medicate-list') == "2") {
            searchOnLoad();
        }
    });
    $('.tablist-link').on('click',function(e) {
        e.preventDefault();
        $('.hasError').hide();
        $('.alert').remove();
        $('.tableListbtn-wrapper').find('.medication-btn-link').attr('disabled','disabled');
        $('.tab-list').removeClass('active-tabList');
        var tablinkNum = $(this).closest('.tab-list').addClass('active-tabList').attr('data-medicate-list');
        $('[data-medicate-content]').hide();
        $('[data-medicate-content='+tablinkNum+']').show();
    });
    $('#medication-save').on('click',function(e) {
        e.preventDefault();
        $('.hasError').hide();
        $('.alert').remove();
        var urlExtn = getMedicationForm() + '/';
        var codeVal = $('.medicationForm-form:visible').find('input[name="codeValue"]').val();
        var activeStatusVal = $('.medicationForm-form:visible').find('option:selected').attr('data-medicate-status');
        var abbreviationVal = $('.medicationForm-form:visible').find('input[name="abbreviation"]').val();
        var descriptionVal = $('.medicationForm-form:visible').find('input[name="description"]').val();
        var dataObj = {
            "code": codeVal,
            "createdBy": sessionStorage.userId,
            "isActive": activeStatusVal,
            "value": abbreviationVal,
            "notes": descriptionVal,
            "isDeleted": 0
        }
        if(codeVal != "" && abbreviationVal != "" && activeStatusVal != "" && descriptionVal != "") {
            var dataUrl = ipAddress+urlExtn;
            createAjaxObject(dataUrl,dataObj,"POST",onCreate,onError);
        }
        else {
            $('.hasError').show();
        }
    });
    $("#medication-reset").on('click',function(e) {
        e.preventDefault();
        $('.medicationForm-form')[0].reset();
    });
    $("#medication-list-search").on('click',function(e) {
        e.preventDefault();
        if($(".btnActive").hasClass("selectButtonBarClass")) {
            $(".btnActive").trigger('click');
        }
        else if($(".btnInActive").hasClass("selectButtonBarClass")) {
            $(".btnInActive").trigger('click');
        }
    });
    $(".medication-delete").on('click',function(e) {
        e.preventDefault();
        $('.alert').remove();
        var deleteItemObj = {
            "id": selectedItems[0].idDuplicate,
            "isActive": 0,
            "isDeleted": 1,
            "modifiedBy": sessionStorage.userId
        }
        var urlExtn = getMedicationForm() + '/';
        var dataUrl = ipAddress+urlExtn;
        createAjaxObject(dataUrl,deleteItemObj,"DELETE",onDeleteItem,onDeleteError);
    });
    $(".medication-edit-tableList").on('click',function(e) {
        e.preventDefault();
        $('[data-medicate-list="1"]').find('.tablist-link').trigger('click');
        $('[data-medicate-list="1"]').find('.tablist-link').text("Edit");
        $('.medicationForm-form').find('option').each(function() {
            if($(this).attr('data-medicate-status') == selectedItems[0].isActive) {
                $(this).attr('selected','selected');
            }
            else {
                $(this).removeAttr('selected');
            }
        });
        $('.medicationForm-form').find('input[name="codeValue"]').val(selectedItems[0].code);
        $('.medicationForm-form').find('input[name="abbreviation"]').val(selectedItems[0].value);
        $('.medicationForm-form').find('input[name="description"]').val(selectedItems[0].notes);
        $('#medication-save').hide();
        $('#medication-reset').hide();
        $('#medication-update').show();
        $('#medication-cancel').show();
    });
    $("#medication-update").on('click',function(e) {
        e.preventDefault();
        $('.alert').remove();
        var codeVal = $('.medicationForm-form').find('input[name="codeValue"]').val();
        var activeStatusVal = $('.statusUser:visible').find('option:selected').attr('data-medicate-status');
        var abbreviationVal = $('.medicationForm-form').find('input[name="abbreviation"]').val();
        var descriptionVal = $('.medicationForm-form').find('input[name="description"]').val();
        var updateItemObj = {
            "id": selectedItems[0].idDuplicate,
            "code": codeVal,
            "modifiedBy": sessionStorage.userId,
            "isActive": activeStatusVal,
            "value": abbreviationVal,
            "notes": descriptionVal,
            "isDeleted": 0
        }
        var urlExtn = getMedicationForm() + '/';
        var dataUrl = ipAddress+urlExtn;
        createAjaxObject(dataUrl,updateItemObj,"PUT",onUpdateItem,onUpdateError);
    });
    $('#medication-cancel').on('click',function(e) {
        e.preventDefault();
        $('.medicationForm-form')[0].reset();
        $('#medication-save').show();
        $('#medication-reset').show();
        $('#medication-update').hide();
        $('#medication-cancel').hide();
        $('[data-medicate-list="2"]').find('.tablist-link').trigger('click');
    });
    $(".btnActive").on("click", function(e) {
        e.preventDefault();
        activeListFlag = true;
        $('.alert').remove();
        $('.hasError-rxNorm').remove();
        $('.medication-delete').show();
        searchOnLoad();
        onClickActive();
    });
    $(".btnInActive").on("click", function(e) {
        e.preventDefault();
        activeListFlag = false;
        $('.alert').remove();
        $('.hasError-rxNorm').remove();
        $('.medication-delete').hide();
        searchOnLoad();
        onClickInActive();
    });
}
function getMedicationForm() {
    var formUrl = '/homecare/diet-types';
    return formUrl;
}
function searchOnLoad() {
    buildMedicationListGrid([]);
    var urlExtn = getMedicationForm() + '/?is-active=1';
    getAjaxObject(ipAddress+urlExtn,"GET",getSearchList,onSearchError);
}
function onCreate(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1") {
            $('.medicationForm-form')[0].reset();
            $('.medicationForm-form').prepend('<div class="alert alert-success">success</div>');
        }
        else {
            $('.medicationForm-form')[0].reset();
            $('.medicationForm-form').prepend('<div class="alert alert-danger">error</div>');
        }
    }
}
function onError(errObj){
    console.log(errObj);
    customAlert.error("Error","Error");
}
function onDeleteItem(respObj) {
    console.log(respObj);
    if(respObj && respObj.response && respObj.response.status){
        if(respObj.response.status.code == "1") {
            if($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name').toLowerCase() == "rxnorm") {
                rxNormDataOnLoad();
                $('#medicationNormData').prepend('<div class="alert alert-success">Deleted Successfully</div>');
            }
            if($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name').toLowerCase() == "tradename") {
                //tradeNameDataOnLoad();
                if($('.tradeName-filter-input').val() != "") {
                    $('.medication-rxNorm-search').trigger('click');
                }
                $('#medicationTradeData').prepend('<div class="alert alert-success">Deleted Successfully</div>');
            }
            else {
                searchOnLoad();
                $('#medicationList').prepend('<div class="alert alert-success">Deleted Successfully</div>');
            }
            $('.tableListbtn-wrapper').find('.medication-btn-link').each(function() {
                if(selectedItems && selectedItems.length>0){
                    $(this).attr('disabled','disabled');
                }
                else {
                    $(this).removeAttr('disabled');
                }
            });
        }
        else {
            $('#medicationList').prepend('<div class="alert alert-danger">Error</div>');
        }
    }
}
function onDeleteError(errObj){
    console.log(errObj);
    customAlert.error("Error","Error");
}
function onUpdateItem(respObj) {
    console.log(respObj);
    if(respObj && respObj.response && respObj.response.status){
        if(respObj.response.status.code == "1") {
            $('.medicationForm-form').prepend('<div class="alert alert-success">Updated Successfully</div>');
            $('.medicationForm-form')[0].reset();
            $('#medication-save').show();
            $('#medication-reset').show();
            $('#medication-update').hide();
            $('#medication-cancel').hide();
            if($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name').toLowerCase() == "tradename") {
                $('.tradeFormWrapper').find('.tradeFormWrapper-select').each(function() {
                    $(this).find('option:first-child').attr('selected','selected')
                });
            }
        }
        else {
            $('.medicationForm-form').prepend('<div class="alert alert-danger">'+respObj.response.status.message+'</div>');
        }
    }
}
function onUpdateError(errObj){
    console.log(errObj);
    customAlert.error("Error","Error");
}
function addMedicationData() {
    $('.sidebar-nav-list:first-child').find('.sidebar-nav-link').trigger('click');
}
var dataActiveList=[], dataInActiveList=[], dataListArr = [];
function getSearchList(resp) {
    var dataArray, dataActiveList=[], dataInActiveList=[];
    if(resp && resp.response && resp.response.dietTypes){
        if($.isArray(resp.response.dietTypes)){
            dataArray = resp.response.dietTypes;
        }else{
            dataArray.push(resp.response.dietTypes);
        }
    }
    if(dataArray != undefined) {
        for(var i=0;i<dataArray.length;i++){
            dataArray[i].idDuplicate = dataArray[i].id;
            if(dataArray[i].isActive == 1){
                dataArray[i].Status = "Active";
                dataActiveList.push(dataArray[i]);
            }
            else {
                dataArray[i].Status = "InActive";
                dataInActiveList.push(dataArray[i]);
            }
        }
    }
    if(activeListFlag) {
        buildMedicationListGrid(dataActiveList);
    }
    else if(!activeListFlag) {
        buildMedicationListGrid(dataInActiveList);
    }
}
function onSearchError(err) {
    console.log(err);
    customAlert.error("Error","Error");
}
function buildMedicationListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Code",
        "field": "code",
    });
    gridColumns.push({
        "title": "Abbreviation",
        "field": "value",
    });
    gridColumns.push({
        "title": "Description",
        "field": "notes",
    });
    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
}
function adjustHeight(){
    var defHeight = 220;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    angularUIgridWrapper.adjustGridHeight(cmpHeight);
}

function onChange(){
    setTimeout(function() {
        selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        disableButtons();
    });
}
function disableButtons() {
    $('.tableListbtn-wrapper').find('.medication-btn-link').each(function() {
        if(selectedItems && selectedItems.length>0){
            $(this).removeAttr('disabled');
        }
        else {
            $(this).attr('disabled','disabled');
        }
    });
}
function onClickActive() {
    $(".btnInActive").removeClass("selectButtonBarClass");
    $(".btnActive").addClass("selectButtonBarClass");
}
function onClickInActive() {
    $(".btnActive").removeClass("selectButtonBarClass");
    $(".btnInActive").addClass("selectButtonBarClass");
}
function init() {
    bindEvents();
    addMedicationData();
}
$(document).ready(function() {
    $("#divMain",parent.document).css("display","none");
    $("#divPatInfo",parent.document).css("display","");
    $("#lblTitleName",parent.document).html("Diet");
    init();
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("medicationList", dataOptions);
    angularUIgridWrapper.init();
    buildMedicationListGrid([]);
});
$(window).load(function(){
    windowLoad = true;
    searchOnLoad();
});
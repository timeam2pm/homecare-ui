var angularUIgridWrapper = AngularUIGridWrapper();
var parentRef = null;
var dtArray = [];


$(document).ready(function(){
    parentRef = parent.frames['iframe'].window;
});

$(window).load(function(){
    onMessagesLoaded();
});

function onMessagesLoaded() {
    var dataOptions = {
        pagination: false,
        paginationPageSize: 500,
        changeCallBack: onChange
    }

    angularUIgridWrapper = new AngularUIGridWrapper("dgridAppointmentList", dataOptions);
    angularUIgridWrapper.init();

    buttonEvents();
    getAppointmentList();
}
function getAppointmentList(){
    var url = ipAddress+"/appointment/list/?facility-id="+parentRef.staffFacilityId+"&provider-id="+parentRef.staffId+"&to-date="+parentRef.leaveToDate+"&from-date="+parentRef.leaveFromDate+"&is-active=1&is-deleted=0";
    getAjaxObject(url,"GET",onPatientListData,onErrorMedication);
}

function onClickSearch(){
    buildDeviceTypeGrid([]);
    var strSearch = $("#txtSearch").val();
    strSearch = $.trim(strSearch);
    if(strSearch != ""){
        var txtPatientId = $("#txtPatientId").data("kendoComboBox");
        if(txtPatientId){
            var idx = txtPatientId.value();
            if(idx == 1){
                getPatientList();
            }else if(idx == 2){
                patientListURL = ipAddress+"/patient/list/?id="+strSearch;
                getAjaxObject(patientListURL,"GET",onPatientListData,onErrorMedication);
            }else if(idx == 3){
                patientListURL = ipAddress+"/patient/list/?first-name="+strSearch;
                getAjaxObject(patientListURL,"GET",onPatientListData,onErrorMedication);
            }else if(idx == 4){
                patientListURL = ipAddress+"/patient/list/?last-name="+strSearch;
                getAjaxObject(patientListURL,"GET",onPatientListData,onErrorMedication);
            }else if(idx == 5){
                var pArray = strSearch.split(",");
                if(pArray){
                    if(pArray.length == 2){
                        var fName = pArray[0];
                        var lName = pArray[1];
                        if(fName && lName){
                            patientListURL = ipAddress+"/patient/list/?first-name="+fName+"&last-name="+lName;
                            getAjaxObject(patientListURL,"GET",onPatientListData,onErrorMedication);
                        }else if(fName){
                            patientListURL = ipAddress+"/patient/list/?first-name="+fName;
                            getAjaxObject(patientListURL,"GET",onPatientListData,onErrorMedication);
                        }else if(lName){
                            patientListURL = ipAddress+"/patient/list/?last-name="+lName;
                            getAjaxObject(patientListURL,"GET",onPatientListData,onErrorMedication);
                        }
                    }else if(pArray.length == 1){
                        patientListURL = ipAddress+"/patient/list/?first-name="+strSearch;
                        getAjaxObject(patientListURL,"GET",onPatientListData,onErrorMedication);
                    }
                }
            }
        }
    }else{
        getPatientList();
    }
}
function onPatientListData(dataObj){
    dtArray = [];
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.appointment) {
            if ($.isArray(dataObj.response.appointment)) {
                dtArray = dataObj.response.appointment;
            } else {
                dtArray.push(dataObj.response.appointment);
            }
        }
    }
    for(var i=0;i<dtArray.length;i++) {
        dtArray[i].idk = dtArray[i].id;
        dtArray[i].name = dtArray[i].composition.patient.lastName+" "+dtArray[i].composition.patient.firstName+" "+dtArray[i].composition.patient.middleName;
        var day = new Date(GetDateTimeEdit(dtArray[i].dateOfAppointment)).getDay();
        var dow = getWeekDayName(day);

        dtArray[i].dateTimeOfAppointment = dow + " " +GetDateTimeEdit(dtArray[i].dateOfAppointment);



        dtArray[i].appointmentReasonDesc = dtArray[i].composition.appointmentReason.desc;
        dtArray[i].appointmentTypeDesc = dtArray[i].composition.appointmentType.desc;
        dtArray[i].staffName = dtArray[i].composition.provider.lastName+" "+dtArray[i].composition.provider.firstName+" "+dtArray[i].composition.provider.middleName;
    }

    buildDeviceTypeGrid(dtArray);
}
function onErrorMedication(errobj){
    console.log(errobj);
}
function buttonEvents(){
    $("#btnSave").off("click");
    $("#btnSave").on("click",onClickUnassignStaff);

    $("#btnDelete").off("click");
    $("#btnDelete").on("click",onClickDelete);

    $("#btnCancel").off("click");
    $("#btnCancel").on("click",onClickCancel);
}

$(window).resize(adjustHeight);
function adjustHeight(){
    var defHeight = 120;
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 120;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    try{angularUIgridWrapper.adjustGridHeight(cmpHeight);}catch(e){};

    var cmpHeight = window.innerHeight - 180;
    $("#divPtInfo").height(cmpHeight);

}
function buildDeviceTypeGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    otoptions.rowHeight = 100;
    gridColumns.push({
        "title": "Date Time",
        "field": "dateTimeOfAppointment",
        "enableColumnnMenu": false,
        "width": "25%"
    });
    gridColumns.push({
        "title": "Status",
        "field": "appointmentTypeDesc",
        "enableColumnMenu": false,
    });
    gridColumns.push({
        "title": "Reason",
        "field": "appointmentReasonDesc",
        "enableColumnMenu": false,
    });
    gridColumns.push({
        "title": "Service User",
        "field": "name",
        "enableColumnMenu": false,
    });
    gridColumns.push({
        "title": "Staff",
        "field": "staffName",
        "enableColumnMenu": false,
    });

    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
}
function onChange(){

}
function onErrorMedication(errobj){
    console.log(errobj);
}
function onGetPatientInfo(dataObj){
    if(dataObj && dataObj.response && dataObj.response.patient){
        //$("#ptDOB",parent.document).html(dataObj.response.patient.dateOfBirth);
        $("#txtID").val(dataObj.response.patient.id);
        $("#txtExtID1").val(dataObj.response.patient.externalId1);
        $("#txtExtID2").val(dataObj.response.patient.externalId2);
        ptExtId2 = dataObj.response.patient.externalId2;
        $("#txtPrefix").val(dataObj.response.patient.prefix);
        $("#txtStatus").val(dataObj.response.patient.status);
        $("#txtNN").val(dataObj.response.patient.nickname);
        $("#txtFN").val(dataObj.response.patient.firstName);
        $("#txtMN").val(dataObj.response.patient.middleName);
        $("#txtLN").val(dataObj.response.patient.lastName);
        var dt = new Date(dataObj.response.patient.dateOfBirth);
        if(dt){
            var strDT = kendo.toString(dt,"MM-dd-yyyy");
            $("#txtDOB").val(strDT);

            /*var currDate = new Date();
            console.log(currDate.getFullYear()-dt.getFullYear());
            var strAge = currDate.getFullYear()-dt.getFullYear();*/
            strAge = getAge(dt);
            $("#txtAge").val(strAge);
        }
        var ssn = showSSNNumver(dataObj.response.patient.ssn);
        $("#txtSSN").val(ssn);
        $("#txtWeight").val(dataObj.response.patient.weight);
        $("#txtHeight").val(dataObj.response.patient.height);
        $("#txtGender").val(dataObj.response.patient.gender);
        $("#txtEthnicity").val(dataObj.response.patient.ethnicity);
        $("#txtRace").val(dataObj.response.patient.race);
        $("#txtLan").val(dataObj.response.patient.language);
    }
    if(dataObj && dataObj.response && dataObj.response.communication){
        var commArray = [];
        if($.isArray(dataObj.response.communication)){
            commArray = dataObj.response.communication;
        }else{
            commArray.push(dataObj.response.communication);
        }
        var comObj = commArray[0];
        $("#txtAddr1").val(comObj.address1);
        $("#txtAddr2").val(comObj.address2);
        $("#txtSMS").val(comObj.sms);
        $("#txtCell").val(comObj.cellPhone);
        $("#txtExtension").val(comObj.workPhoneExt);
        $("#txtWORK").val(comObj.workPhone);
        $("#txtHExt").val(comObj.homePhoneExt);
        $("#txtHM").val(comObj.homePhone);

        $("#txtCity").val(comObj.city);
        $("#txtState").val(comObj.state);
        $("#txtZip").val(comObj.zip);
        $("#txtZip4").val(comObj.zipFour);
        $("#txtCity").val(comObj.city);
    }
}


function onClickDelete(){
    customAlert.confirm("Confirm", "Are you sure you want delete?",function(response){
        if(response.button == "Yes"){
            var selectedItems = angularUIgridWrapper.getSelectedRows();
            console.log(selectedItems);
            if(selectedItems && selectedItems.length>0){
                var selItem = selectedItems[0];
                if(selItem){
                    var dataUrl = ipAddress+"/patient/delete/";
                    var reqObj = {};
                    reqObj.id = selItem.PID;
                    reqObj.isDeleted = "1";
                    reqObj.modifiedBy = sessionStorage.userId;
                    createAjaxObject(dataUrl,reqObj,"POST",onDeletePatient,onErrorMedication);
                }
            }
        }
    });
}
function onDeletePatient(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            customAlert.info("info", "Patient Deleted Successfully");
            getPatientList();
        }else{
            customAlert.error("error", dataObj.message);
        }
    }
}

function onClickCancel(){
    popupClose(false);
}
function popupClose(st){
    var onCloseData = new Object();
    onCloseData.status = st;
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(onCloseData);
}


function onClickUnassignStaff() {
    var objList = []
    customAlert.confirm("Confirm", "Are you sure want to un allocate appointments?", function (response) {
        if (response.button == "Yes") {
            for(var i=0;i<dtArray.length;i++) {
                var appObj = {};
                appObj = dtArray[i];
                appObj.id = appObj.idk;
                delete appObj.providerId;
                delete appObj.composition;
                delete appObj.createdDate;
                delete appObj.appointmentStartDate;
                delete appObj.appointmentEndDate;
                delete appObj.handoverNotes;
                delete appObj.inTime;
                delete appObj.isSynced;
                delete appObj.modifiedDate;
                delete appObj.notes;
                delete appObj.outTime;
                delete appObj.readBy;
                delete appObj.syncTime;
                delete appObj.timeSlots;
                appObj.modifiedBy = Number(sessionStorage.userId);
                appObj.providerId = 0;
                objList.push(appObj);
            }

            var dataUrl = ipAddress + "/appointment/update";
            var arr = [];
            arr.push(appObj);
            createAjaxObject(dataUrl, objList, "POST", onUnAssignCreate, onErrorMedication);
        }
    });
}

function onUnAssignCreate(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            var msg = "appointments unallocated successfully";
            displaySessionErrorPopUp("Info", msg, function(res) {
                onClickCancel();
            })
        }else{
            customAlert.error("Error",dataObj.response.status.message);
        }
    }else{
        customAlert.error("Error",dataObj.response.status.message);
    }
}

function getWeekDayName(wk){
    var wn = "";
    if(wk == 1){
        return "Mon";
    }else if(wk == 2){
        return "Tue";
    }else if(wk == 3){
        return "Wed";
    }else if(wk == 4){
        return "Thu";
    }else if(wk == 5){
        return "Fri";
    }else if(wk == 6){
        return "Sat";
    }else if(wk == 0){
        return "Sun";
    }
    return wn;
}
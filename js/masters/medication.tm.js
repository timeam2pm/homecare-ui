var angularUIgridWrapper = null;
var normUIgrid = null;
var tradeNameUIgrid = null;
var windowLoad = false;
var selectedItems;
var activeListFlag = true;
var fetchTradeDetails = false;
var sessionMedicationValue = sessionStorage.sessionMedicationValue;
var ADD = "add";
var UPDATE = "edit";
var VIEW = "view";
var DELETE = "delete";
var operation = "add";
var atID = "";
var parentRef = null;


function bindEvents() {
    $("#btnAdd").off("click");
    $("#btnAdd").on("click",onClickAdd);

    $("#btnCancel").off("click",onClickCancel);
    $("#btnCancel").on("click",onClickCancel);

    $("#btnReset").off("click",onClickReset);
    $("#btnReset").on("click",onClickReset);

    $(".popupClose").off("click", onClickCancel);
    $(".popupClose").on("click", onClickCancel);

    $('#btnSave').on('click',function(e) {
        e.preventDefault();
        var urlExtn = getMedicationForm() + '/create';
        if(sessionMedicationValue.toLowerCase() == "rxnorm") {
            var drugIdVal = $('.medicationForm-form').find('input[name="drugId"]').val();
            var drugInfoVal = $('.medicationForm-form').find('input[name="drugInfo"]').val();
            var rxcuiVal = $('.medicationForm-form').find('input[name="rxcui"]').val();
            var externalIdVal = $('.medicationForm-form').find('input[name="externalId"]').val();
            var notesVal = $('.medicationForm-form').find('input[name="notes"]').val();
            var dataObj = {
                "createdBy": sessionStorage.userId,
                "isActive": 1,
                "isDeleted": 0,
                "drugId": drugIdVal,
                "drugInfo": drugInfoVal,
                "rxcui": rxcuiVal,
                "externalId": externalIdVal,
                "notes": notesVal
            }



            if(drugIdVal != "" && drugInfoVal != "" && rxcuiVal != "") {
                var dataUrl = ipAddress+urlExtn;
                createAjaxObject(dataUrl,dataObj,"POST",onCreate,onError);
            }
            else {
                customAlert.error("Error","Please fill the Required Fields");
            }
        }
        else if(sessionMedicationValue.toLowerCase() == "tradename") {
            var tradeNameVal = $('.medicationForm-form').find('input[name="tradeName"]').val() || "";
            var tradeDrugVal = $("#tradeDrugNameValue").val() || "";
            var tradeFormVal = $('#tradeName-form-select').find('option:selected').attr('data-tradeName-val') || "";
            var tradeStrengthVal = $('#tradeName-strength-select').find('option:selected').attr('data-tradeName-val') || "";
            var tradeRouteVal = $('#tradeName-route-select').find('option:selected').attr('data-tradeName-val') || "";
            var tradeFrequencyVal = $('#tradeName-frequency-select').find('option:selected').attr('data-tradeName-val') || "";
            var tradeInstructionsVal = $('#tradeName-instructions-select').find('option:selected').attr('data-tradeName-val') || "";
            if(tradeNameVal != "" && tradeDrugVal != "" && tradeFormVal != "" && tradeStrengthVal != "" && tradeRouteVal != "") {
                var dataObj = {
                    "name": tradeNameVal,
                    "createdBy": sessionStorage.userId,
                    "isActive": 1,
                    "isDeleted": 0,
                    "rxNormId": tradeDrugVal,
                    "medicationFormId": tradeFormVal,
                    "medicationStrengthId": tradeStrengthVal,
                    "medicationRouteId": tradeRouteVal,
                    "medicationFrequencyId": tradeFrequencyVal,
                    "medicationInstructionsId": tradeInstructionsVal
                }
                var dataUrl = ipAddress+urlExtn;
                createAjaxObject(dataUrl,dataObj,"POST",onCreate,onError);
            }
            else {
                customAlert.error("Error","Please fill the Required Fields");
            }
        }
        else {
            var activeStatusVal = parseInt($("#cmbStatus").val());
            var abbreviationVal = $("#txtAbbreviation").val();
            var descriptionVal = $("#txtDescription").val();
            var IsDeleted = 0;
            if(activeStatusVal == 1){
                IsDeleted = 0;
            }
            else{
                IsDeleted = 1;
            }
            var dataObj = {
                "isActive": activeStatusVal,
                "value": abbreviationVal,
                "desc": descriptionVal,
                "isDeleted": IsDeleted,
                "note": null
            }

            if(parentRef.operation == "edit"){
                dataObj.id = atID;
                dataObj.modifiedBy = sessionStorage.userId;
                urlExtn = getMedicationForm() + '/update';
            }
            else{
                dataObj.createdBy= sessionStorage.userId;
            }

            if(abbreviationVal != "" && activeStatusVal != "" && descriptionVal != "") {
                var dataUrl = ipAddress+urlExtn;
                createAjaxObject(dataUrl,dataObj,"POST",onCreate,onError);
            }
            else {
                customAlert.error("Error","Please fill the Required Fields");
            }
        }
    });

    $("#btnDelete").on('click',function(e) {
        e.preventDefault();
        $('.alert').remove();
        var deleteItemObj = {
            "id": selectedItems[0].idDuplicate,
            "isActive": 0,
            "isDeleted": 1,
            "modifiedBy": sessionStorage.userId
        }
        var urlExtn = getMedicationForm() + '/delete';
        var dataUrl = ipAddress+urlExtn;
        createAjaxObject(dataUrl,deleteItemObj,"POST",onDeleteItem,onDeleteError);
    });

    $("#btnEdit").on('click',function(e) {
        e.preventDefault();
        operation = UPDATE;

        if(sessionMedicationValue.toLowerCase() == "rxnorm") {
            $('.medicationForm-form').find('input[name="drugId"]').val(selectedItems[0].drugId);
            $('.medicationForm-form').find('input[name="drugInfo"]').val(selectedItems[0].drugInfo);
            $('.medicationForm-form').find('input[name="rxcui"]').val(selectedItems[0].rxcui);
            $('.medicationForm-form').find('input[name="externalId"]').val(selectedItems[0].externalId);
            $('.medicationForm-form').find('input[name="notes"]').val(selectedItems[0].notes);
        }
        if(sessionMedicationValue == "tradename") {
            $('.close-popup').trigger('click');
            $('.medicationForm-form').find('input[name="tradeName"]').val(selectedItems[0].name);
            $('.medicationForm-form').find('input[name="drugName"]').val(selectedItems[0].drugInfo);
            $('#tradeName-form-select').find('option').each(function() {
                if($(this).attr('data-tradeName-val') == selectedItems[0].medicationFormId) {
                    $(this).attr('selected','selected');
                    return false;
                }
                else {
                    $(this).removeAttr('selected');
                }
            });
            $('#tradeName-strength-select').find('option').each(function() {
                if($(this).attr('data-tradeName-val') == selectedItems[0].medicationStrengthId) {
                    $(this).attr('selected','selected');
                    return false;
                }
                else {
                    $(this).removeAttr('selected');
                }
            });
            $('#tradeName-route-select').find('option').each(function() {
                if($(this).attr('data-tradeName-val') == selectedItems[0].medicationRouteId) {
                    $(this).attr('selected','selected');
                    return false;
                }
                else {
                    $(this).removeAttr('selected');
                }
            });
            $('#tradeName-frequency-select').find('option').each(function() {
                if($(this).attr('data-tradeName-val') == selectedItems[0].medicationFrequencyId) {
                    $(this).attr('selected','selected');
                    return false;
                }
                else {
                    $(this).removeAttr('selected');
                }
            });
            $('#tradeName-instructions-select').find('option').each(function() {
                if($(this).attr('data-tradeName-val') == selectedItems[0].medicationInstructionsId) {
                    $(this).attr('selected','selected');
                    return false;
                }
                else {
                    $(this).removeAttr('selected');
                }
            });
        }
        else {
            $("#txtID").show();
            parentRef.operation = "edit";
            getMedicationForm();
            $("#viewDivBlock").hide();
            $("#addPopup").show();
            $("#txtAbbreviation").val(selectedItems[0].value);
            $("#txtDescription").val(selectedItems[0].desc);
            $("#txtID").html("ID: "+ atID);
        }
        medicationForm();
    });

    $(".btnActive").on("click", function(e) {
        e.preventDefault();
        activeListFlag = true;
        searchOnLoad();
        onClickActive();
    });
    $(".btnInActive").on("click", function(e) {
        e.preventDefault();
        activeListFlag = false;
        searchOnLoad();
        onClickInActive();
    });
}

function getMedicationForm() {
    var formUrl;
    if(sessionMedicationValue.toLowerCase() == "form") {
        formUrl = '/master/medication_form';
        $(".filter-heading").html("View Form");
    }
    else if(sessionMedicationValue.toLowerCase() == "strength") {
        formUrl = '/master/medication_strength';
        $(".filter-heading").html("View Strength");
    }
    else if(sessionMedicationValue.toLowerCase() == "route") {
        formUrl = '/master/medication_route';
        $(".filter-heading").html("View Route");
    }
    else if(sessionMedicationValue.toLowerCase() == "frequency") {
        formUrl = '/master/medication_frequency';
        $(".filter-heading").html("View Frequency");
    }
    else if(sessionMedicationValue.toLowerCase() == "instruction") {
        formUrl = '/master/medication_instructions';
        $(".filter-heading").html("View Instructions");
    }
    else if(sessionMedicationValue.toLowerCase() == "action") {
        formUrl = '/master/medication_action';
        $(".filter-heading").html("View Action");
    }
    else if(sessionMedicationValue.toLowerCase() == "status") {
        formUrl = '/master/medication_status';
        $(".filter-heading").html("View Status");
    }
    else if(sessionMedicationValue.toLowerCase() == "transmission") {
        formUrl = '/master/medication_transmission';
        $(".filter-heading").html("View Transmission");
    }
    else if(sessionMedicationValue.toLowerCase() == "rxnorm") {
        formUrl = '/rx-norm';
        $(".filter-heading").html("View Rx Norm");
    }
    else if(sessionMedicationValue.toLowerCase() == "tradename") {
        formUrl = '/medication-trade-name';
        $(".filter-heading").html("View Trade Name");
    }
    return formUrl;
}
function searchOnLoad() {
    buildMedicationListGrid([]);
    var urlExtn = getMedicationForm() + '/list';
    getAjaxObject(ipAddress+urlExtn,"GET",getSearchList,onSearchError);
}
function rxNormDataOnLoad() {
    buildMedicationNormListGrid([]);
    var urlExtn = getMedicationForm() + '/list?is-active=1';
    getAjaxObject(ipAddress+urlExtn,"GET",getRxNormData,onRxNormError);
}
function tradeNameDataOnLoad() {
    buildMedicationTradeListViewGrid([]);
    var urlExtn = getMedicationForm() + '/list?is-active=1';
    getAjaxObject(ipAddress+urlExtn,"GET",getTradeListData,onTradeListError);
}
function onCreate(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1") {
            var msg = "Created Successfully";
            if(operation == UPDATE){
                msg = "Updated Successfully"
            }
            displaySessionErrorPopUp("Info", msg, function(res) {
                operation = ADD;
                init();
                searchOnLoad();
                onClickCancel();
                onClickActive();
            })
        }
        else {
            customAlert.error("Error", dataObj.response.status.message);
        }
    }
}
function onError(errObj){
    console.log(errObj);
    customAlert.error("Error","Error");
}
function onDeleteItem(respObj) {
    console.log(respObj);
    if(respObj && respObj.response && respObj.response.status){
        if(respObj.response.status.code == "1") {
            if(sessionMedicationValue.toLowerCase() == "rxnorm") {
                rxNormDataOnLoad();
                $('#medicationNormData').prepend('<div class="alert alert-success">Deleted Successfully</div>');
            }
            if(sessionMedicationValue.toLowerCase() == "tradename") {
                //tradeNameDataOnLoad();
                if($('.tradeName-filter-input').val() != "") {
                    $('.medication-rxNorm-search').trigger('click');
                }
                $('#medicationTradeData').prepend('<div class="alert alert-success">Deleted Successfully</div>');
            }
            else {
                var msg = "Deleted successfully";
                displaySessionErrorPopUp("Info", msg, function(res) {
                    init();
                    searchOnLoad();
                })
            }
        }
        else {
            $('#medicationList').prepend('<div class="alert alert-danger">Error</div>');
        }
    }
}
function onDeleteError(errObj){
    console.log(errObj);
    customAlert.error("Error","Error");
}
function onUpdateItem(respObj) {
    console.log(respObj);
    if(respObj && respObj.response && respObj.response.status){
        if(respObj.response.status.code == "1") {
            $('.medicationForm-form').prepend('<div class="alert alert-success">Updated Successfully</div>');
            $('.medicationForm-form')[0].reset();
            $('#medication-save').show();
            $('#medication-reset').show();
            $('#medication-update').hide();
            /*$('#medication-cancel').hide();*/
            if($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name').toLowerCase() == "tradename") {
                $('.tradeFormWrapper').find('.tradeFormWrapper-select').each(function() {
                    $(this).find('option:first-child').attr('selected','selected')
                });
            }
        }
        else {
            $('.medicationForm-form').prepend('<div class="alert alert-danger">'+respObj.response.status.message+'</div>');
        }
    }
}
function onUpdateError(errObj){
    console.log(errObj);
    customAlert.error("Error","Error");
}
function addMedicationData() {
    $('.sidebar-nav-list:first-child').find('.sidebar-nav-link').trigger('click');
}
var dataActiveList=[], dataInActiveList=[], dataListArr = [];
function getSearchList(resp) {
    var dataArray, dataActiveList=[], dataInActiveList=[];
    if(resp && resp.response && resp.response.codeTable){
        if($.isArray(resp.response.codeTable)){
            dataArray = resp.response.codeTable;
        }else{
            dataArray.push(resp.response.codeTable);
        }
    }
    if(dataArray != undefined) {
        for(var i=0;i<dataArray.length;i++){
            dataArray[i].idDuplicate = dataArray[i].id;
            if(dataArray[i].isActive == 1){
                dataArray[i].Status = "Active";
                dataActiveList.push(dataArray[i]);
            }
            else {
                dataArray[i].Status = "InActive";
                dataInActiveList.push(dataArray[i]);
            }
        }
    }
    if(activeListFlag) {
        buildMedicationListGrid(dataActiveList);
    }
    else if(!activeListFlag) {
        buildMedicationListGrid(dataInActiveList);
    }
}
function onSearchError(err) {
    console.log(err);
    customAlert.error("Error","Error");
}
function getRxNormData(resp) {
    var dataArray, dataActiveList=[], dataInActiveList=[];
    if(resp && resp.response && resp.response.rxNorm){
        if($.isArray(resp.response.rxNorm)){
            dataArray = resp.response.rxNorm;
        }
        else{
            dataArray.push(resp.response.rxNorm);
        }
    }
    if(dataArray != undefined) {
        for(var i=0;i<dataArray.length;i++){
            dataArray[i].idDuplicate = dataArray[i].id;
            if(dataArray[i].isActive == 1){
                dataArray[i].Status = "Active";
                dataActiveList.push(dataArray[i]);
            }
            else {
                dataArray[i].Status = "InActive";
                dataInActiveList.push(dataArray[i]);
            }
        }
    }
    if(activeListFlag && $('#medication-list-search').closest('li').hasClass('active-tabList')) {
        buildMedicationNormListGrid(dataActiveList);
        if(dataActiveList && dataActiveList.length == 0) {
            if($('.medication-data-content').find('.hasError-rxNorm') && $('.medication-data-content').find('.hasError-rxNorm').length > 0) {
                $('.medication-data-content .hasError-rxNorm').text('o Results Found for Selected Filters');
            } else {
                $('.medication-data-content').prepend('<div class="hasError-rxNorm">No Results Found for Selected Filters</div>');
            }
            $('.hasError-rxNorm').show();
        }
    }
    else if(!activeListFlag && $('#medication-list-search').closest('li').hasClass('active-tabList')) {
        buildMedicationNormListGrid(dataInActiveList);
    }
}
function onRxNormError(err) {
    console.log(err);
    customAlert.error("Error","Error");
}
function retrieveTradeInfo(url,formName) {
    $('.tradeFormWrapper').find('select.tradeFormWrapper-select').each(function() {
        $(this).html('<option>Select</option>');
    });
    getAjaxObject(ipAddress+url+'?is-active=1&is-deleted=0',"GET",onRetreiveTradeInfoSuccess,onError);
    function onRetreiveTradeInfoSuccess(resp) {
        var tradeNameData = [], dataArr = [];
        if(resp && resp.response && resp.response.codeTable){
            if($.isArray(resp.response.codeTable)){
                dataArr = resp.response.codeTable;
            }
            else{
                dataArr.push(resp.response.codeTable);
            }
        }
        if(dataArr && dataArr.length > 0) {
            for(var i=0;i<dataArr.length;i++){
                if(dataArr[i].isActive == 1) {
                    tradeNameData.push(dataArr[i]);
                }
            }
        }
        $.each(tradeNameData, function(i,val) {
            if(formName == "/master/medication_form") {
                $('#tradeName-form-select').append('<option data-tradeName-val="'+val.id+'">'+val.desc+'</option>')
            }
            else if(formName == "/master/medication_strength") {
                $('#tradeName-strength-select').append('<option data-tradeName-val="'+val.id+'">'+val.desc+'</option>')
            }
            else if(formName == "/master/medication_route") {
                $('#tradeName-route-select').append('<option data-tradeName-val="'+val.id+'">'+val.desc+'</option>')
            }
            else if(formName == "/master/medication_frequency") {
                $('#tradeName-frequency-select').append('<option data-tradeName-val="'+val.id+'">'+val.desc+'</option>')
            }
            else if(formName == "/master/medication_instructions") {
                $('#tradeName-instructions-select').append('<option data-tradeName-val="'+val.id+'">'+val.desc+'</option>')
            }
        });
    }
}
function getRxNormTradeData(resp) {
    var dataListArr = [], dataArray;
    if(resp && resp.response && resp.response.rxNorm){
        if($.isArray(resp.response.rxNorm)){
            dataArray = resp.response.rxNorm;
        }
        else{
            dataArray.push(resp.response.rxNorm);
        }
    }
    if(dataArray != undefined) {
        for(var i=0;i<dataArray.length;i++){
            dataArray[i].idDuplicate = dataArray[i].id;
            if(dataArray[i].isActive == 1){
                dataListArr.push(dataArray[i]);
            }
        }
    }
    if(dataListArr && dataListArr.length == 0) {
        $('#medicationNormData').prepend('<div class="hasError-rxNorm">No Results Found for Selected Filters</div>');
        $('.hasError-rxNorm').show();
    }
    buildMedicationTradeListGrid(dataListArr);
}
function onRxNormTradeError(err) {
    console.log(err);
    customAlert.error("Error","Error");
}
function getTradeListData(resp) {
    var dataListArr = [], dataArray;
    if(resp && resp.response && resp.response.medicationTradeName){
        if($.isArray(resp.response.medicationTradeName)){
            dataArray = resp.response.medicationTradeName;
        }
        else{
            dataArray.push(resp.response.medicationTradeName);
        }
    }
    if(dataArray != undefined) {
        for(var i=0;i<dataArray.length;i++){
            dataArray[i].idDuplicate = dataArray[i].id;
            if(dataArray[i].isActive == 1){
                dataListArr.push(dataArray[i]);
            }
        }
    }
    if(dataListArr && dataListArr.length == 0) {
        $('#medicationTradeData').prepend('<div class="hasError-rxNorm">No Results Found for Required Name</div>');
        $('.hasError-rxNorm').show();
    }
    buildMedicationTradeListViewGrid(dataListArr);
}
function onTradeListError(err) {
    console.log(err);
    customAlert.error("Error","Error");
}
function buildMedicationListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Abbreviation",
        "field": "value",
    });
    gridColumns.push({
        "title": "Description",
        "field": "desc",
    });
    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
}
function buildMedicationNormListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Drug Id",
        "field": "drugId",
        "width": "*"
    });
    gridColumns.push({
        "title": "Drug Info",
        "field": "drugInfo",
        "width": 230
    });
    gridColumns.push({
        "title": "Rxcui",
        "field": "rxcui",
        "width": "*"
    });
    gridColumns.push({
        "title": "External Id",
        "field": "externalId",
        "width": "*"
    });
    gridColumns.push({
        "title": "Notes",
        "field": "notes",
        "width": 140
    });
    normUIgrid.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
}
function buildMedicationTradeListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Drug Id",
        "field": "drugId",
        "width": "*"
    });
    gridColumns.push({
        "title": "Drug Info",
        "field": "drugInfo",
        "width": 230
    });
    gridColumns.push({
        "title": "Rxcui",
        "field": "rxcui",
        "width": "*"
    });
    gridColumns.push({
        "title": "External Id",
        "field": "externalId",
        "width": "*"
    });
    gridColumns.push({
        "title": "Notes",
        "field": "notes",
        "width": 140
    });
    tradeNameUIgrid.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
}
function buildMedicationTradeListViewGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    if(dataSource.length > 0) {
        var k;
        for (k = 0; k < dataSource.length; k++) {
            dataSource[k].drugInfo = dataSource[k].composition.rxNorm != undefined ? dataSource[k].composition.rxNorm.drugInfo : "";
            dataSource[k].formVal = dataSource[k].composition.medicationForm != undefined ? dataSource[k].composition.medicationForm.desc : "";
            dataSource[k].strengthVal = dataSource[k].composition.medicationStrength != undefined ? dataSource[k].composition.medicationStrength.desc : "";
            dataSource[k].routeVal = dataSource[k].composition.medicationRoute != undefined ? dataSource[k].composition.medicationRoute.desc : "";
            dataSource[k].frequencyVal = dataSource[k].composition.medicationFrequency != undefined ? dataSource[k].composition.medicationFrequency.desc : "";
            dataSource[k].instructionsVal = dataSource[k].composition.medicationInstructions != undefined ? dataSource[k].composition.medicationInstructions.desc : "";
        }
    }
    gridColumns.push({
        "title": "Name",
        "field": "name",
        "width": "*"
    });
    gridColumns.push({
        "title": "Drug Info",
        "field": "drugInfo",
        "width": "*"
    });
    gridColumns.push({
        "title": "Form Id",
        "field": "formVal",
        "width": "*"
    });
    gridColumns.push({
        "title": "Strength Id",
        "field": "strengthVal",
        "width": "*"
    });
    gridColumns.push({
        "title": "Route Id",
        "field": "routeVal",
        "width": "*"
    });
    gridColumns.push({
        "title": "Frequency Id",
        "field": "frequencyVal",
        "width": "*"
    });
    gridColumns.push({
        "title": "Instructions Id",
        "field": "instructionsVal",
        "width": "*"
    });
    tradeNameListUIgrid.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
}
function adjustHeight(){
    var defHeight = 220;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    angularUIgridWrapper.adjustGridHeight(cmpHeight);
    // normUIgrid.adjustGridHeight(cmpHeight);
}

function onChange(){
    setTimeout(function() {
        selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        disableButtons();
    });
}
function onNormChange(){
    setTimeout(function() {
        selectedItems = normUIgrid.getSelectedRows();
        console.log(selectedItems);
        disableButtons();
    });
}
function onTradeNameChange(){
    setTimeout(function() {
        selectedItems = tradeNameUIgrid.getSelectedRows();
        console.log(selectedItems);
        disableButtons();
    });
}
function onTradeNameListViewChange(){
    setTimeout(function() {
        selectedItems = tradeNameListUIgrid.getSelectedRows();
        console.log(selectedItems);
        disableButtons();
    });
}

function onClickActive() {
    $(".btnInActive").removeClass("radioButton-active");
    $(".btnActive").addClass("radioButton-active");
}

function onClickInActive() {
    $(".btnActive").removeClass("radioButton-active");
    $(".btnInActive").addClass("radioButton-active");
}
function init() {
    bindEvents();

}
$(document).ready(function() {
    $("#pnlPatient",parent.document).css("display","none");
    themeAPIChange();
    parentRef = parent.frames['iframe'].window;
    init();
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    // var dataNormOptions = {
    // 	pagination: false,
    // 	changeCallBack: onNormChange
    // }
    // var dataTradeNameOptions = {
    // 	pagination: false,
    // 	changeCallBack: onTradeNameChange
    // }
    // var dataTradeNameListViewOptions = {
    // 	pagination: false,
    // 	changeCallBack: onTradeNameListViewChange
    // }
    angularUIgridWrapper = new AngularUIGridWrapper("medicationList", dataOptions);
    angularUIgridWrapper.init();
    // normUIgrid = new AngularUIGridWrapper("medicationList", dataNormOptions);
    // normUIgrid.init();
    // tradeNameUIgrid = new AngularUIGridWrapper("medicationNormData", dataTradeNameOptions);
    // tradeNameUIgrid.init();
    // tradeNameListUIgrid = new AngularUIGridWrapper("medicationTradeData", dataTradeNameListViewOptions);
    // tradeNameListUIgrid.init();
    buildMedicationListGrid([]);
    // buildMedicationNormListGrid([]);
    // buildMedicationTradeListGrid([]);
    // buildMedicationTradeListViewGrid([]);
    searchOnLoad();
});
$(window).load(function(){
    windowLoad = true;
    searchOnLoad();
});


function themeAPIChange(){
    getAjaxObject(ipAddress+"/homecare/settings/?id=2","GET",getThemeValue,onError);
}


function getThemeValue(dataObj){
    console.log(dataObj);
    var tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.activityTypes){
            if($.isArray(dataObj.response.settings)){
                tempCompType = dataObj.response.settings;
            }else{
                tempCompType.push(dataObj.response.settings);
            }
        }
        if(tempCompType.length > 0){
            themesChange(tempCompType[0].value);
        }
    }
}

function themesChange(themeValue){
    if(themeValue == 2){
        loadAPi("../../Theme2/Theme02.css");
    }
    else if(themeValue == 3){
        loadAPi("../../Theme3/Theme03.css");
    }
}

function onClickAdd(){
    $("#addPopup").show();
    $('#viewDivBlock').hide();
    $("#txtID").hide();
    onClickReset();
    parentRef.operation = "add";
    operation = ADD;
    medicationForm();
}

function onClickReset() {
    if(operation == ADD) {
        operation = ADD;
    }
    else {
        operation = UPDATE;
    }
    $("#txtAbbreviation").val("");
    $("#txtDescription").val("");
    $("#cmbStatus").val(1);

}

function onClickCancel(){
    getMedicationForm();
    $("#viewDivBlock").show();
    $("#addPopup").hide();
}

function disableButtons() {
    if (selectedItems && selectedItems.length > 0) {
        var obj = selectedItems[0];
        atID = obj.idDuplicate;
        $("#btnEdit").prop("disabled", false);
        $("#btnDelete").prop("disabled", false);
    }else{
        $("#btnEdit").prop("disabled", true);
        $("#btnDelete").prop("disabled", true);
    }
}


function medicationForm() {
    if(sessionMedicationValue.toLowerCase() == "form") {
        if(operation == ADD){
            $(".filter-heading").html("Add Form");
        }
        else{
            $(".filter-heading").html("Edit Form");
        }
    }
    else if(sessionMedicationValue.toLowerCase() == "strength") {
        if(operation == ADD){
            $(".filter-heading").html("Add Strength");
        }
        else{
            $(".filter-heading").html("Edit Strength");
        }
    }
    else if(sessionMedicationValue.toLowerCase() == "route") {
        if(operation == ADD){
            $(".filter-heading").html("Add Route");
        }
        else{
            $(".filter-heading").html("Edit Route");
        }
    }
    else if(sessionMedicationValue.toLowerCase() == "frequency") {
        if(operation == ADD){
            $(".filter-heading").html("Add Frequency");
        }
        else{
            $(".filter-heading").html("Edit Frequency");
        }
    }
    else if(sessionMedicationValue.toLowerCase() == "instruction") {
        if(operation == ADD){
            $(".filter-heading").html("Add Instruction");
        }
        else{
            $(".filter-heading").html("Edit Instruction");
        }
    }
    else if(sessionMedicationValue.toLowerCase() == "action") {
        if(operation == ADD){
            $(".filter-heading").html("Add Action");
        }
        else{
            $(".filter-heading").html("Edit Action");
        }
    }
    else if(sessionMedicationValue.toLowerCase() == "status") {
        if(operation == ADD){
            $(".filter-heading").html("Add Status");
        }
        else{
            $(".filter-heading").html("Edit Status");
        }
    }
    else if(sessionMedicationValue.toLowerCase() == "transmission") {
        if(operation == ADD){
            $(".filter-heading").html("Add Transmission");
        }
        else{
            $(".filter-heading").html("Edit Transmission");
        }
    }
    else if(sessionMedicationValue.toLowerCase() == "rxnorm") {
        if(operation == ADD){
            $(".filter-heading").html("Add Rx Norm");
        }
        else{
            $(".filter-heading").html("Edit Rx Norm");
        }
    }
    else if(sessionMedicationValue.toLowerCase() == "tradename") {
        if(operation == ADD){
            $(".filter-heading").html("Add Trade Name");
        }
        else{
            $(".filter-heading").html("Edit Trade Name");
        }
    }
}
var angularUIgridWrapper;
var parentRef = null;
var recordType = "1";
var dataArray = [];
var ADD = "add";
var UPDATE = "edit";
var VIEW = "view";
var DELETE = "delete";
var operation = "add";
var ds  = "";
var IsFlag = 1;
var cntry = sessionStorage.countryName;
var selectPatientId;
var dtFMT = "dd/MM/yyyy";
var allDatesInWeek = [];

$(document).ready(function(){
    $("#pnlPatient",parent.document).css("display","none");
    var cntry = sessionStorage.countryName;
    if((cntry.indexOf("India")>=0) || (cntry.indexOf("United Kingdom")>=0)){
        $("#lblFacility").text("Location");
    }else if(cntry.indexOf("United State")>=0) {
        $("#lblFacility").text("Facility");
    }
    sessionStorage.setItem("IsSearchPanel", "1");
    themeAPIChange();
    parentRef = parent.frames['iframe'].window;
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("dgridCopyAppointmentsList", dataOptions);
    angularUIgridWrapper.init();
    buildDeviceListGrid([]);

});


$(window).load(function(){
    loading = false;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){

    init();
    buttonEvents();
    adjustHeight();
}

function init(){
    $("#btnEdit").prop("disabled", true);
    $("#btnDelete").prop("disabled", true);

    getAjaxObject(ipAddress+"/facility/list/?is-active=1","GET",getFacilityList,onError);

    // if(cntry.indexOf("India")>=0){
    //     $("#txtFDate").datepick({
    //         changeMonth: true,
    //         changeYear: true,
    //         dateFormat: 'dd/mm/yyyy',
    //         yearRange: "-200:+200"
    //     });
    // }else if(cntry.indexOf("United Kingdom")>=0){
    //     $("#txtFDate").datepick({
    //         changeMonth: true,
    //         changeYear: true,
    //         dateFormat: 'dd/mm/yyyy',
    //         yearRange: "-200:+200"
    //     });
    // }else{
    //     $("#txtFDate").datepick({
    //         changeMonth: true,
    //         changeYear: true,
    //         dateFormat: 'mm/dd/yyyy',
    //         yearRange: "-200:+200"
    //     });
    // }

    $("#txtFDate").kendoDatePicker({format:dtFMT,value:new Date()});
}



function onError(errorObj){
    console.log(errorObj);
}

function buttonEvents(){

    $("#btnSubmit").off("click",onClickSubmit);
    $("#btnSubmit").on("click",onClickSubmit);

    $("#btnEdit").off("click",onClickEdit);
    $("#btnEdit").on("click",onClickEdit);

    $("#btnDelete").off("click",onClickDelete);
    $("#btnDelete").on("click",onClickDelete);

    $("#btnCopy").off("click");
    $("#btnCopy").on("click",onClickCopy);

    $("#btnCancel").off("click",onClickCancel);
    $("#btnCancel").on("click",onClickCancel);

    $("#btnReset").off("click",onClickReset);
    $("#btnReset").on("click",onClickReset);

    $(".popupClose").off("click", onClickCancel);
    $(".popupClose").on("click", onClickCancel);

    $("#btnServiceUser").off("click");
    $("#btnServiceUser").on("click",onClickServiceUser);


    $(".btnActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('active');
        onClickActive();
    });
    $(".btnInActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('inactive');
        onClickInActive();
    });

}

function searchOnLoad(status) {
    buildDeviceListGrid([]);
    if(status == "active") {
        var urlExtn = ipAddress+"/homecare/vacations/?fields=*&parentTypeId="+Number(parentRef.type)+"&parentId="+Number(parentRef.patientId)+"&is-active=1&is-deleted=0";

    }
    else if(status == "inactive") {
        var urlExtn =  ipAddress+"/homecare/vacations/?fields=*&parentTypeId="+Number(parentRef.type)+"&parentId="+Number(parentRef.patientId)+"&is-active=0&is-deleted=1"
    }
    getAjaxObject(urlExtn,"GET",handleGetVacationList,onError);
}


function onClickAdd(){
    $("#addPopup").show();
    $('#viewDivBlock').hide();
    $("#txtID").hide();
    $(".filter-heading").html("Add Staff Leave Request");
    parentRef.operation = "add";
    onClickReset();
}

function addReportMaster(opr){
    var popW = 500;
    var popH = "43%";

    //$('.listDataWrapper').hide();
    //$('.addOrRemoveWrapper').show();
    $('.alert').remove();
    var profileLbl = "Add Task Type";
    parentRef.operation = opr;
    operation = opr;
    if(opr == "add"){
        $('#btnSave').html('<span><img src="../../img/medication/Save.png" class="providerLink-btn-img" />Save</span>');
        $('.tabContentTitle').text('Add Task Type');
        $('#btnReset').show();
        //var billActName = $("#txtBAN").val();
        $('#btnReset').trigger('click');
        //$("#txtBAN").val(billActName);
    }else{

        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if (selectedItems && selectedItems.length > 0) {
            var obj = selectedItems[0];
            parentRef.id = obj.idk;
            parentRef.displayOrder  = obj.displayOrder;
            parentRef.type = obj.type;
            parentRef.DIO = obj.DIO;
            parentRef.activityGroup = obj.activityGroup;
            parentRef.isActive = obj.isActive;
            operation = UPDATE;
        }

        profileLbl = "Edit Task Type";
        $('.tabContentTitle').text('Edit Task Type');
        $('#btnReset').hide();
        $('#btnSave').html('<span><img src="../../img/medication/Save.png" class="providerLink-btn-img" />Update</span>');
        $('#btnReset').trigger('click');
    }
    var devModelWindowWrapper = new kendoWindowWrapper();
    devModelWindowWrapper.openPageWindow("../../html/masters/createactivityTypeList.html", profileLbl, popW, popH, true, closeaddReportMastertAction);
}
function closeaddReportMastertAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        var opr = returnData.operation;
        if(opr == "add"){
            customAlert.info("info", "Task Type created successfully");
        }else{
            customAlert.info("info", "Task Type updated successfully");
        }
        init();
    }
}
function onClickActive() {
    $(".btnInActive").removeClass("radioButton-active");
    $(".btnActive").addClass("radioButton-active");
}

function onClickInActive() {
    $(".btnActive").removeClass("radioButton-active");
    $(".btnInActive").addClass("radioButton-active");
}

function onClickDelete(){
    customAlert.confirm("Confirm", "Are you sure to delete?",function(response){
        if(response.button == "Yes"){
            var dataObj = {};
            dataObj.createdBy = Number(sessionStorage.userId);
            dataObj.isDeleted = 1;
            dataObj.isActive = 0;
            var dataUrl = ipAddress +"/homecare/vacations/";
            var method = "DELETE";
            var selectedItems = angularUIgridWrapper.getSelectedRows();
            dataObj.id = selectedItems[0].idk;
            createAjaxObject(dataUrl, dataObj, method, onCreateDelete, onError);
        }
    });
}

function onCreateDelete(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "Vacation deleted successfully";
            customAlert.error("Info", msg);
            operation = ADD;
            init();
        }
    }
}


function onClickCancel(){
    $(".filter-heading").html("View Appointments");
    $("#viewDivBlock").show();
    $("#addPopup").hide();
}

function adjustHeight(){
    var defHeight = 280;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    angularUIgridWrapper.adjustGridHeight(cmpHeight);
}

function buildDeviceListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    otoptions.rowHeight = 100;
    gridColumns.push({
        "title": "Start Date",
        "field": "dateTimeOfAppointment",
        "enableColumnnMenu": false,
        "width": "18%"
    });
    gridColumns.push({
        "title": "End Date",
        "field": "dateTimeOfAppointmentED",
        "enableColumnnMenu": false,
        "width": "18%"
    });
    gridColumns.push({
        "title": "Status",
        "field": "appointmentTypeDesc",
        "enableColumnMenu": false,
    });
    gridColumns.push({
        "title": "Reason",
        "field": "appointmentReasonDesc",
        "enableColumnMenu": false,
    });
    gridColumns.push({
        "title": "Service User",
        "field": "name",
        "enableColumnMenu": false,
    });
    gridColumns.push({
        "title": "Staff",
        "field": "staffName",
        "enableColumnMenu": false,
    });

    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
}

function onChange(){
    setTimeout(function() {
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        if (selectedItems && selectedItems.length > 0) {
            if(selectedItems[0].approvedBy != '') {
                $("#btnEdit").prop("disabled", false);
                $("#btnDelete").prop("disabled", false);
            }
            else {
                $("#btnEdit").prop("disabled", true);
                $("#btnDelete").prop("disabled", true);
            }
        }
    });
}

function onClickEdit(){
    $("#txtID").show();
    $(".filter-heading").html("Edit Staff Leave Request");
    parentRef.operation = "edit";
    operation =  "edit";
    $("#viewDivBlock").hide();
    $("#addPopup").show();
    var selectedItems = angularUIgridWrapper.getSelectedRows();
    if (selectedItems && selectedItems.length > 0) {
        var obj = selectedItems[0];
        $("#txtLeaveID").html("ID : " + obj.idk);
        $("#cmbManager").val(obj.managerIds);
        $("#cmbLeaveType").val(obj.leaveTypeId);
        $("#txtReason").val(obj.reason);

        // $("#txtFDate").datepick();
        // $("#txtFDate").datepick("setDate", GetDateTimeEdit(obj.fromDate));

        $("#txtFDate").val(GetDateTimeEdit(obj.fromDate));
        $("#txtTDate").val(GetDateTimeEdit(obj.toDate));

        // $("#txtTDate").datepick();
        // $("#txtTDate").datepick("setDate", GetDateTimeEdit(obj.toDate));
    }
}


var operation = "add";
var selFileResourceDataItem = null;

function onStatusChange(){
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if(cmbStatus && cmbStatus.selectedIndex<0){
        cmbStatus.select(0);
    }
}

function onClickReset() {
    $("#txtReason").val("");
    $("#txtFDate").val("");
    $("#txtTDate").val("");
    operation = ADD;
}

function themeAPIChange(){
    getAjaxObject(ipAddress+"/homecare/settings/?id=2","GET",getThemeValue,onError);
}

function getThemeValue(dataObj){
    console.log(dataObj);
    var tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.activityTypes){
            if($.isArray(dataObj.response.settings)){
                tempCompType = dataObj.response.settings;
            }else{
                tempCompType.push(dataObj.response.settings);
            }
        }
        if(tempCompType.length > 0){
            themesChange(tempCompType[0].value);
        }
    }
}

function themesChange(themeValue){
    if(themeValue == 2){
        loadAPi("../../Theme2/Theme02.css");
    }
    else if(themeValue == 3){
        loadAPi("../../Theme3/Theme03.css");
    }
}

function getFacilityList(dataObj) {
    var dataArray = [];
    if (dataObj) {
        if ($.isArray(dataObj.response.facility)) {
            dataArray = dataObj.response.facility;
        } else {
            dataArray.push(dataObj.response.facility);
        }
    }
    var tempDataArry = [];
    for (var i = 0; i < dataArray.length; i++) {
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if (dataArray[i].isActive == 1) {
            dataArray[i].Status = "Active";
        }
        $("#cmbFacility").append('<option value="' + dataArray[i].idk + '">' + dataArray[i].name + '</option>');
    }
}

function onClickServiceUser(){
    var popW = 800;
    var popH = 450;

    if(parentRef)
        parentRef.searchZip = true;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Service User";
    devModelWindowWrapper.openPageWindow("../../html/patients/searchPatient.html", profileLbl, popW, popH, true, onCloseServiceUserAction);
}
function onCloseServiceUserAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        selectPatientId = returnData.selItem.PID;
        $('#txtSU').val(returnData.selItem.LN + " "+ returnData.selItem.FN+" "+returnData.selItem.MN);
    }
}

function onClickSubmit() {
    if($("#txtSU").val() != "") {
        if ($("#txtFDate").val() !== "") {
            var txtAppPTDate = $("#txtFDate").data("kendoDatePicker");
            var selectedDate = txtAppPTDate.value();
            var selDate = new Date(selectedDate);


            // var selectedDate = GetDateTime("txtDate");


            for (let i = 1; i <= 7; i++) {
                var first = selectedDate.getDate() - (selectedDate.getDay() + 1) + i;
                var day = new Date(selectedDate.setDate(first));

                allDatesInWeek.push(day);
            }
            var day = selDate.getDate();
            var month = selDate.getMonth();
            month = month + 1;
            var year = selDate.getFullYear();

            var stDate = month + "/" + day + "/" + year;
            stDate = stDate + " 00:00:00";

            var startDate = new Date(stDate);
            var stDateTime = startDate.getTime();

            var etDate = month + "/" + day + "/" + year;
            etDate = etDate + " 23:59:59";

            var endtDate = new Date(etDate);
            var edDateTime = endtDate.getTime();

            var startDate = new Date(stDate);
            var endtDate = new Date(etDate);

            var day = startDate.getDate() - startDate.getDay();
            var pDate = new Date(startDate);
            pDate.setDate(day);

            endtDate.setDate(day + 6);
            var stDateTime = pDate.getTime();
            var edDateTime = endtDate.getTime();
            var txtAppPTFacility = $("#cmbFacility").val();
            parentRef.facilityId = txtAppPTFacility;
            parentRef.patientId = selectPatientId;
            parentRef.SUName = $("#txtSU").val();
            parentRef.facilityName = $("#cmbFacility option:selected").text();
            parentRef.selDate = pDate;
            parentRef.copyDate = txtAppPTDate.value();

            buildDeviceListGrid([]);

            var patientListURL = ipAddress + "/appointment/list/?facility-id=" + txtAppPTFacility + "&patient-id=" + selectPatientId + "&to-date=" + edDateTime + "&from-date=" + stDateTime + "&is-active=1";

            getAjaxObject(patientListURL, "GET", onPatientListData, onError);

        }
        else {
            customAlert.error("Error", "Please select date");
        }
    }
    else{
        customAlert.error("Error", "Please select service user");
    }
}


function onPatientListData(dataObj){
    dtArray = [];
    buildDeviceListGrid([]);
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.appointment) {
            if ($.isArray(dataObj.response.appointment)) {
                dtArray = dataObj.response.appointment;
            } else {
                dtArray.push(dataObj.response.appointment);
            }
        }
    }
    dtArray.sort(function (a, b) {
        var nameA = a.dateOfAppointment; // ignore upper and lowercase
        var nameB = b.dateOfAppointment; // ignore upper and lowercase
        if (nameA < nameB) {
            return -1;
        }
        if (nameA > nameB) {
            return 1;
        }
        // names must be equal
        return 0;
    });

    if(dtArray.length > 0) {
        for (var i = 0; i < dtArray.length; i++) {
            // $("#btnCopy").css("display", "");
            dtArray[i].idk = dtArray[i].id;
            dtArray[i].name = dtArray[i].composition.patient.lastName + " " + dtArray[i].composition.patient.firstName + " " + dtArray[i].composition.patient.middleName;


            var date = new Date(GetDateTimeEditDay(dtArray[i].dateOfAppointment));
            var day = date.getDay();
            var dow = getWeekDayName(day);

            var dtEnd= dtArray[i].dateOfAppointment + (Number(dtArray[i].duration) * 60000);

            dtArray[i].dateTimeOfAppointment = dow + " " + GetDateTimeEdit(dtArray[i].dateOfAppointment);
            dtArray[i].DW = dow;

            var dateED = new Date(GetDateTimeEditDay(dtEnd));
            var dayED = dateED.getDay();
            var dowED = getWeekDayName(dayED);

            dtArray[i].dateTimeOfAppointmentED = dowED + " " + GetDateTimeEdit(dtEnd);


            var dt = new Date(dtArray[i].dateOfAppointment);
            var strDT = "";
            if (dt) {
                strDT = kendo.toString(dt, "MM/dd/yyyy h:mm tt");
            }

            dtArray[i].dateTimeOfAppointment1 = strDT;


            dtArray[i].appointmentReasonDesc = dtArray[i].composition.appointmentReason.desc;
            dtArray[i].appointmentTypeDesc = dtArray[i].composition.appointmentType.desc;
            if(dtArray[i].providerId != 0) {
                dtArray[i].staffName = dtArray[i].composition.provider.lastName + " " + dtArray[i].composition.provider.firstName + " " + (dtArray[i].composition.provider.middleName != null ? dtArray[i].composition.provider.middleName : " ");
            }else{
                dtArray[i].staffName = "";
            }
        }


        buildDeviceListGrid(dtArray);
    }
    else{
        // $("#btnCopy").css("display", "none");
        customAlert.info("Info","No appointments");
    }
}

function onClickCopy(){
    var selGridData = angularUIgridWrapper.getAllRows();
    var selList = [];
    for (var i = 0; i < selGridData.length; i++) {

        var dataRow = selGridData[i].entity;
        dataRow.appointmentType = "Con";
        dataRow.appointmentTypeDesc = "Confirmed";
        dataRow.composition.appointmentType.desc="Confirmed";
        dataRow.composition.appointmentType.id=7;
        dataRow.composition.appointmentType.value="Con";
        selList.push(dataRow);
    }

    // if(selList.length>0){
    sessionStorage.setItem("appselList", JSON.stringify(selList));
    parentRef.appselList = selList;
    var popW = "56%";
    var popH = "80%";
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Copy To Appointments";
    parentRef.selValue = allDatesInWeek;
    devModelWindowWrapper.openPageWindow("../../html/masters/createCopyAppointment.html", profileLbl, popW, popH, true, copyAppointment);
    parent.setPopupWIndowHeaderColor("0bc56f","FFF");
    // }
}
function copyAppointment(dataObj){
    parentRef.selValue= null;
    parentRef.appselList=null;
    onClickSubmit();
}


function getWeekDayName(wk){
    var wn = "";
    if(wk == 1){
        return "Mon";
    }else if(wk == 2){
        return "Tue";
    }else if(wk == 3){
        return "Wed";
    }else if(wk == 4){
        return "Thu";
    }else if(wk == 5){
        return "Fri";
    }else if(wk == 6){
        return "Sat";
    }else if(wk == 0){
        return "Sun";
    }
    return wn;
}
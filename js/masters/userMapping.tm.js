var angularUIgridWrapper = null;
var angularUISelgridWrapper = null;

var angularUIPTgridWrapper = null;

var parentRef = null;
var operation = "";
var selItem = null;
var ADD = "add";
var UPDATE = "update";
var VIEW = "view";
var DELETE = "delete";
var statusArr = [{Key:'Active',Value:'Active'},{Key:'InActive',Value:'InActive'}];

$(document).ready(function(){
    parentRef = parent.frames['iframe'].window;
    var pnlHeight = window.innerHeight;
    var imgHeight = pnlHeight-50;
    $("#divTop").height(imgHeight);
});
$(document).ready(function(){
    parentRef = parent.frames['iframe'].window;
    $("#pnlPatient",parent.document).css("display","none");
    sessionStorage.setItem("IsSearchPanel", "1");
    patientId = parentRef.patientId
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("dgSelUserIdList1", dataOptions);
    angularUIgridWrapper.init();
    buildFileResourceListGrid([]);

    var dataOptions1 = {
        pagination: false,
        changeCallBack: onChange1
    }
    angularUISelgridWrapper = new AngularUIGridWrapper("dgAvlUserIdList2", dataOptions1);
    angularUISelgridWrapper.init();
    buildFileResourceSelListGrid([]);

    var dataPtOptions = {
        pagination: false,
        changeCallBack: onPtChange
    }
    angularUIPTgridWrapper = new AngularUIGridWrapper("dgridUserPatient", dataPtOptions);
    angularUIPTgridWrapper.init();
    buildPatientGrid([]);
});


$(window).load(function(){
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
    init();
    buttonEvents();
    adjustHeight();
}
function adjustHeight(){
    var defHeight = 140;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    cmpHeight = cmpHeight/2;
    if(angularUIgridWrapper){
        angularUIgridWrapper.adjustGridHeight(cmpHeight);
    }
    if(angularUISelgridWrapper){
        angularUISelgridWrapper.adjustGridHeight(cmpHeight);
    }

    if(angularUIPTgridWrapper){
        angularUIPTgridWrapper.adjustGridHeight(cmpHeight);
    }
}
function buildFileResourceListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Select",
        "field": "SEL",
        "cellTemplate": showCheckBoxTemplate(),
        "width":"15%"

    });
    gridColumns.push({
        "title": "ID",
        "field": "idk",
        "width":"10%",
        "cellTemplate": "<div  style='text-decoration:underline;color: deepskyblue;cursor:pointer' class='ui-grid-cell-contents' onclick='onClickRPLink(1)'><a style='font-size: 13px;color: #2f98d29c'>{{row.entity.idk}}</a></div>",
    });
    gridColumns.push({
        "title": "Last Name",
        "field": "lastName",
    });
    gridColumns.push({
        "title": "First Name",
        "field": "firstName",
    });

    gridColumns.push({
        "title": "User Type",
        "field": "userType",
        "width":"20%"
    });

    gridColumns.push({
        "title": "Access Type",
        "field": "accessTypes"
    });

    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}
var prevSelectedItem =[];
function onChange(){

}
function buildFileResourceSelListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Select",
        "field": "SEL",
        "cellTemplate": showCheckBoxTemplate(),
        "width":"15%"
    });
    gridColumns.push({
        "title": "ID",
        "field": "userId",
        "width":"12%",
        "cellTemplate": "<div  style='text-decoration:underline;cursor:pointer' class='ui-grid-cell-contents' onclick='onClickRPLink(2)'><a style='font-size: 13px;color: #2f98d29c'>{{row.entity.userId}}</a></div>",

    });
    gridColumns.push({
        "title": "Last Name",
        "field": "lastName",
    });
    gridColumns.push({
        "title": "First Name",
        "field": "firstName",
    });

    gridColumns.push({
        "title": "User Type",
        "field": "userType",
        "width":"20%"
    });

    gridColumns.push({
        "title": "Access Type",
        "field": "accessTypes"
    });
    angularUISelgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}
var prevSelectedItem =[];
function onChange1(){

}

function showCheckBoxTemplate(){
    var node = '<div style="text-align:center;"><input type="checkbox" class="check1" id="chkbox" ng-model="row.entity.SEL" style="width:20px;height:20px;margin:0px;"   onclick="onSelect(event);"></input></div>';
    return node;
}
function buildPatientGrid(dataSource){
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Service User Id",
        "field": "PID",
        "enableColumnMenu": false,
    });
    gridColumns.push({
        "title": "Last Name",
        "field": "LN",
        "enableColumnMenu": false,
    });
    gridColumns.push({
        "title": "First Name",
        "field": "FN",
        "enableColumnMenu": false,
    });
    gridColumns.push({
        "title": "Middle Name",
        "field": "MN",
        "enableColumnMenu": false,
    });
    gridColumns.push({
        "title": "Date of Birth",
        "field": "DOB",
        "enableColumnMenu": false,
    });
    gridColumns.push({
        "title": "Gender",
        "field": "GR",
        "enableColumnMenu": false,
    });
    gridColumns.push({
        "title": "Status",
        "field": "ST",
        "enableColumnMenu": false,
    });
    angularUIPTgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
}
var selPatientId = null;
function onPtChange(){
    setTimeout(function(){
        console.log(angularUIPTgridWrapper.getSelectedRows());
        var selRows = angularUIPTgridWrapper.getSelectedRows();
        if(selRows && selRows.length>0){
            var selItem = selRows[0];
            if(selItem){
                selPatientId = selItem.PID;
                buildFileResourceListGrid([]);
                buildFileResourceSelListGrid([]);
                getPatientMappedList();
                //getUserList();
            }
        }
    },50)

}
$(window).load(function(){
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
    init();
});
function onClickSubRight(){
//	alert("right");
    var selGridData = angularUISelgridWrapper.getAllRows();
    var selList = [];
    for (var i = 0; i < selGridData.length; i++) {
        var dataRow = selGridData[i].entity;
        if(dataRow.SEL){
            angularUISelgridWrapper.deleteItem(dataRow);
            //dataRow.SEL = false;
            angularUIgridWrapper.insert(dataRow);
        }
    }
}
function onClickSubLeft(){
    //alert("click");
    var selGridData = angularUIgridWrapper.getAllRows();
    var selList = [];
    for (var i = 0; i < selGridData.length; i++) {
        var dataRow = selGridData[i].entity;
        if(dataRow.SEL){
            angularUIgridWrapper.deleteItem(dataRow);
            angularUISelgridWrapper.insert(dataRow);
        }
    }
}

function init(){
    operation = parentRef.operation;
    selItem = parentRef.selItem;
    getPatientList();
    //getUserList();

    buttonEvents();
}
function onStatusChange(){
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if(cmbStatus && cmbStatus.selectedIndex<0){
        cmbStatus.select(0);
    }
}
function buttonEvents(){
    $("#btnCancel").off("click",onClickCancel);
    $("#btnCancel").on("click",onClickCancel);

    $("#btnSave").off("click",onClickSave);
    $("#btnSave").on("click",onClickSave);

    $("#btnSubRight").off("click");
    $("#btnSubRight").on("click",onClickRight);

    $("#btnSubLeft").off("click");
    $("#btnSubLeft").on("click",onClickLeft);
    $(".btnActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('active');
        onClickActive();
    });
    $(".btnInActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('inactive');
        onClickInActive();
    });

}

function searchOnLoad(status) {
    buildPatientGrid([]);
    var urlExtn;
    if(status == "active") {
        urlExtn = ipAddress + "/patient/list/?is-active=1&is-deleted=0";
    }
    else if(status == "inactive") {
        urlExtn = ipAddress + "/patient/list/?is-active=0&is-deleted=1";
    }
    getAjaxObject(urlExtn,"GET",onPatientListData,onErrorMedication);
}

function getPatientList(){
    getAjaxObject(ipAddress+"/patient/list/?is-active=1&is-deleted=0","GET",onPatientListData,onErrorMedication);
}

function onClickActive() {
    $(".btnInActive").removeClass("radioButton-active");
    $(".btnActive").addClass("radioButton-active");
}

function onClickInActive() {
    $(".btnActive").removeClass("radioButton-active");
    $(".btnInActive").addClass("radioButton-active");
}

function onPatientListData(dataObj){
    console.log(dataObj);
    var tempArray = [];
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.patient){
            if($.isArray(dataObj.response.patient)){
                tempArray = dataObj.response.patient;
            }else{
                tempArray.push(dataObj.response.patient);
            }
        }

    }

    for(var i=0;i<tempArray.length;i++){
        var obj = tempArray[i];
        if(obj){
            var dataItemObj = {};
            dataItemObj.PID = obj.id;
            dataItemObj.FN = obj.firstName;
            dataItemObj.LN = obj.lastName;
            dataItemObj.WD = obj.weight;
            dataItemObj.HD = obj.height;
            if(obj.middleName){
                dataItemObj.MN = obj.middleName;
            }else{
                dataItemObj.MN =  "";
            }
            dataItemObj.GR = obj.gender;
            if(obj.status){
                dataItemObj.ST = obj.status;
            }else{
                dataItemObj.ST = "ACTIVE";
            }
            dataItemObj.DOB = kendo.toString(new Date(obj.dateOfBirth),"dd/MM/yyyy");

            dataArray.push(dataItemObj);
        }
    }
    buildPatientGrid(dataArray);
}
function getPatientMappedList(){
    // getAjaxObject(ipAddress+"/user-patient/list","GET",onGetUserPatientListData,onErrorMedication);
    getAjaxObject(ipAddress+"/user/by-patient/"+selPatientId,"GET",onGetUserPatientListData,onErrorMedication);
}
var userPatientListArray = [];
function onGetUserPatientListData(dataObj){
    userPatientListArray = [];
    var userPtList = [];
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            if($.isArray(dataObj.response.tenantUsers)){
                userPtList = dataObj.response.tenantUsers;
            }else{
                userPtList.push(dataObj.response.tenantUsers);
            }
        }else{
            customAlert.info("Info", dataObj.response.status.message);
        }
    }
    for(var y=0;y<userPtList.length;y++){
        var ptItem = userPtList[y];
        //if(ptItem && ptItem.patientId == selPatientId){
        ptItem.pkid = ptItem.id;
        ptItem.userId = ptItem.id;
        if (ptItem.userTypeId == 500) {
            ptItem.userType = "Service User";
        }
        if (ptItem.userTypeId == 600) {
            ptItem.userType = "Relative";
        }

        userPatientListArray.push(ptItem);
        //}
    }
    buildFileResourceSelListGrid(userPatientListArray);
    getUserList();
}
function getUserList(){
    // getAjaxObject(ipAddress+"/user/list/500,600,700","GET",onGetUserListData,onErrorMedication);
    getAjaxObject(ipAddress + "/homecare/tenant-users/?is-active=1&is-deleted=0&userTypeId=:in:500,600", "GET", onGetUserListData, onErrorMedication);
}
var userList = [];
var userListArray = [];
function onGetUserListData(dataObj){
    console.log(dataObj);
    userListArray = [];
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            if($.isArray(dataObj.response.tenantUsers)){
                userListArray = dataObj.response.tenantUsers;
            }else{
                userListArray.push(dataObj.response.tenantUsers);
            }
        }else{
            customAlert.info("Error", dataObj.response.status.message);
        }
    }

    userList = [];
    for(var i=0;i<userListArray.length;i++){
        var item = userListArray[i];
        item.idk = item.id;
        item.userId = item.id;
        if (item.userTypeId == 500) {
            item.userType = "Service User";
        }

        if (item.userTypeId == 600) {
            item.userType = "Relative";
        }

        if(!isPatientUserExist(item.idk)){
            userList.push(item);
        }
    }
    buildFileResourceListGrid(userList);
}

function isPatientUserExist(userId){
    for(var x=0;x<userPatientListArray.length;x++){
        var userItem = userPatientListArray[x];
        if(userItem && userItem.userId == userId){
            return true;
        }
    }
    return false;
}



function onClickRight(){
    var selGridData = angularUIgridWrapper.getAllRows();
    var selList = [];
    for (var i = 0; i < selGridData.length; i++) {
        var dataRow = selGridData[i].entity;
        if(dataRow.SEL){
            angularUIgridWrapper.deleteItem(dataRow);
            angularUISelgridWrapper.insert(dataRow);
        }
    }
}
function onClickLeft(){
    var selGridData = angularUISelgridWrapper.getAllRows();
    var selList = [];
    for (var i = 0; i < selGridData.length; i++) {
        var dataRow = selGridData[i].entity;
        if(dataRow.SEL){
            angularUISelgridWrapper.deleteItem(dataRow);
            angularUIgridWrapper.insert(dataRow);
        }
    }
}
function onErrorMedication(errobj){
    console.log(errobj);
}


function validation(){
    var flag = true;
    /*var strAbbr = $("#txtAbbrevation").val();
        strAbbr = $.trim(strAbbr);
        if(strAbbr == ""){
            customAlert.error("Error","Enter Abbrevation");
            flag = false;
            return false;
        }
        var strCode = $("#txtCode").val();
        strCode = $.trim(strCode);
        if(strCode == ""){
            customAlert.error("Error","Enter Code");
            flag = false;
            return false;
        }
        var strName = $("#txtName").val();
        strName = $.trim(strName);
        if(strName == ""){
            customAlert.error("Error","Enter Country");
            flag = false;
            return false;
        }*/

    return flag;
}

function onClickSave(){
    if(validation()){

        var delUserList = angularUIgridWrapper.getAllRows();
        var gridUserList = angularUISelgridWrapper.getAllRows();

        var mapUserList = [];
        for(var u=0;u<gridUserList.length;u++){
            var uItem = gridUserList[u].entity;
            if(uItem && !uItem.pkid){
                var reqObj = {};
                reqObj.isActive = "1";
                reqObj.isDeleted = "0";
                reqObj.userId = uItem.userId;
                reqObj.patientId = selPatientId;
                reqObj.sendAlert = "1";
                reqObj.id = "";
                reqObj.createdBy = sessionStorage.userId;
                mapUserList.push(reqObj);
            }
        }
        for(var d=0;d<delUserList.length;d++){
            var uItem = delUserList[d].entity;
            if(uItem){
                if(uItem.pkid){
                    var reqObj = {};
                    reqObj.isActive = "1";
                    reqObj.isDeleted = "1";
                    reqObj.userId = uItem.userId;
                    reqObj.patientId = selPatientId;
                    reqObj.sendAlert = "1";
                    reqObj.id = uItem.pkid;
                    reqObj.modifiedBy = sessionStorage.userId;
                    mapUserList.push(reqObj);
                }
            }
        }
        console.log(mapUserList);

        var dataObj = {};
        dataObj = mapUserList;
        var dataUrl = ipAddress+"/user-patient/add-or-remove";
        createAjaxObject(dataUrl,dataObj,"POST",onCreate,onError);
    }
}

function onCreate(dataObj){
    console.log(dataObj);
    if(dataObj &&dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            customAlert.error("Info", "Patient User Mapped Successfully");
            setTimeout(function(){
                onPtChange();
            },100)
        }else{
            customAlert.error("error", dataObj.status.message);
        }
    }
    /*var obj = {};
    obj.status = "success";
    obj.operation = operation;
    popupClose(obj);*/
}

function onError(errObj){
    console.log(errObj);
    customAlert.error("Error","Unable to create UserMapping");
}
function onClickCancel(){
    var obj = {};
    obj.status = "false";
    popupClose(obj);
}
function onClickSearch(){
    var obj = {};
    obj.status = "search";
    popupClose(obj);
}
function popupClose(obj){
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}


function getUserName(userId){
    getAjaxObject(ipAddress+"/homecare/tenant-users/?fields=username&id="+userId,"GET",onGetUserName,onErrorMedication);
}

function onGetUserName(dataObj){
    var userPtList = [];
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            if($.isArray(dataObj.response.tenantUsers)){
                userPtList = dataObj.response.tenantUsers;
            }else{
                userPtList.push(dataObj.response.tenantUsers);
            }
            customAlert.info("UserName", userPtList[0].username);
        }else{
            customAlert.info("Info", dataObj.response.status.message);
        }
    }

}

function onClickRPLink(id){
    if(id == 1) {
        var rows = angularUIgridWrapper.getSelectedRows();
        if (rows && rows.length > 0) {
            var row = rows[0];
            getUserName(row.idk);
        }
    }
    else{
        var rows = angularUISelgridWrapper.getSelectedRows();
        if (rows && rows.length > 0) {
            var row = rows[0];
            getUserName(row.userId);
        }
    }
}
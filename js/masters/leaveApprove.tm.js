var angularUIgridWrapper;
var parentRef = null;
var recordType = "1";
var dataArray = [];
var ADD = "add";
var UPDATE = "edit";
var VIEW = "view";
var DELETE = "delete";
var operation = "add";
var ds  = "";
var IsFlag = 1;
var cntry = sessionStorage.countryName;
var leaveTypes = [];
var staffArray = [];
var leaveStatusId;
var staffFacilityId;
var staffId;
var leaveFromDate;
var leaveToDate;
var leaveFDate;
var leaveTDate;
var appointmentdtFMT = "dd/MM/yyyy hh:mm tt";
var dtFMT = "dd/MM/yyyy";
$(document).ready(function () {
    $("#pnlPatient", parent.document).css("display", "none");
    sessionStorage.setItem("IsSearchPanel", "1");
    //themeAPIChange();
    parentRef = parent.frames['iframe'].window;
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("dgridLeaveApproveList", dataOptions);
    angularUIgridWrapper.init();
    buildDeviceListGrid([]);


});


$(window).load(function(){
    loading = false;
    $(window).resize(adjustHeight);
    getAjaxObject(ipAddress+"/provider/list?is-active=1&is-deleted=0","GET",getAccountList,onError);
    getLeaveTypes();
    getLeaveStatus();
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
    getManagers();
    getEmails();
    init();
    buttonEvents();
    adjustHeight();
}

function init(){
    $("#btnEdit").prop("disabled", true);
    $("#btnDelete").prop("disabled", true);

    // if(cntry.indexOf("India")>=0){
    //     $("#txtFDate").datepick({
    //         changeMonth: true,
    //         changeYear: true,
    //         dateFormat: 'dd/mm/yyyy 00:00',
    //         yearRange: "-200:+200"
    //     });
    //     $("#txtTDate").datepick({
    //         changeMonth: true,
    //         changeYear: true,
    //         dateFormat: 'dd/mm/yyyy 23:59',
    //         yearRange: "-200:+200"
    //     });
    // }else if(cntry.indexOf("United Kingdom")>=0){
    //     $("#txtFDate").datepick({
    //         changeMonth: true,
    //         changeYear: true,
    //         dateFormat: 'dd/mm/yyyy 00:00',
    //         yearRange: "-200:+200"
    //     });
    //     $("#txtTDate").datepick({
    //         changeMonth: true,
    //         changeYear: true,
    //         dateFormat: 'dd/mm/yyyy 23:59',
    //         yearRange: "-200:+200"
    //     });
    // }else{
    //     $("#txtFDate").datepick({
    //         changeMonth: true,
    //         changeYear: true,
    //         dateFormat: 'mm/dd/yyyy 00:00',
    //         yearRange: "-200:+200"
    //     });
    //     $("#txtTDate").datepick({
    //         changeMonth: true,
    //         changeYear: true,
    //         dateFormat: 'mm/dd/yyyy 23:59',
    //         yearRange: "-200:+200"
    //     });
    // }

    // getAjaxObject(ipAddress+"/homecare/vacations/?fields=*&managerIds=295&is-active=1&is-deleted=0","GET",handleGetVacationList,onError);


    $("#txtFDate").kendoDateTimePicker({ format: appointmentdtFMT, interval: 15 });
    $("#txtTDate").kendoDateTimePicker({ format: appointmentdtFMT, interval: 15 });

    $("#txtStartDate").kendoDatePicker({format:dtFMT,value: new Date()});
    $("#txtEndDate").kendoDatePicker({format:dtFMT,value: new Date()});
}

function handleGetVacationList(dataObj) {
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            var vacation = [];
            if (dataObj.response.vacations) {
                if ($.isArray(dataObj.response.vacations)) {
                    vacation = dataObj.response.vacations;
                } else {
                    vacation.push(dataObj.response.vacations);
                }
            }

            if (vacation !== null && vacation.length > 0) {

                for (var j = 0; j < vacation.length; j++) {
                    // vacation[j].FD = kendo.toString(new Date(vacation[j].fromDate),"dd/MM/yyyy h:mm tt");
                    // vacation[j].TD = kendo.toString(new Date(vacation[j].toDate),"dd/MM/yyyy h:mm tt");
                    vacation[j].FD = GetDateTimeEdit(vacation[j].fromDate);
                    vacation[j].TD = GetDateTimeEdit(vacation[j].toDate);

                    if (vacation[j].acceptedFromDate != null) {
                        vacation[j].approvedFrom = GetDateTimeEdit(vacation[j].acceptedFromDate);
                    }
                    if (vacation[j].acceptedToDate != null) {
                        vacation[j].approvedTo = GetDateTimeEdit(vacation[j].acceptedToDate);
                    }


                    vacation[j].idk = vacation[j].id;
                    vacation[j].photo = ipAddress + "/homecare/download/providers/photo/?" + Math.round(Math.random() * 1000000) + "&access_token=" + sessionStorage.access_token + "&tenant=" + sessionStorage.tenant + "&id=" + vacation[j].parentId;
                    vacation[j].leaveType = fncGetLeaveType(vacation[j].leaveTypeId);
                    vacation[j].name = fncGetName(vacation[j].parentId);
                }

                var cmbLeaveStatus = parseInt($("#cmbLeaveStatus option:selected").val());

                if (cmbLeaveStatus === 1) {
                    vacation.sort(sortByDateAsc);
                } else if (cmbLeaveStatus === 2 || cmbLeaveStatus === 3) {
                    vacation.sort(sortByDateDec);
                }
                buildDeviceListGrid(vacation);
            } else {
                buildDeviceListGrid([]);
                customAlert.info("Info", "No record(s) are exist.");
            }
        }
    }
}

function onError(errorObj) {
    console.log(errorObj);
}

function buttonEvents() {

    $("#btnSave").off("click", onClickSave);
    $("#btnSave").on("click", onClickSave);

    $("#btnEdit").off("click", onClickEdit);
    $("#btnEdit").on("click", onClickEdit);

    $("#btnDelete").off("click", onClickDelete);
    $("#btnDelete").on("click", onClickDelete);

    $("#btnCancel").off("click", onClickCancel);
    $("#btnCancel").on("click", onClickCancel);
    $("#btnReset").off("click", onClickReset);
    $("#btnReset").on("click", onClickReset);


    $(".popupClose").off("click", onClickCancel);
    $(".popupClose").on("click", onClickCancel);

    $("#cmbLeaveStatus").on("change", onLeaveChange);

    $(".btnActive").on("click", function (e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('active');
        onClickActive();
    });
    $(".btnInActive").on("click", function (e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('inactive');
        onClickInActive();
    });

    $("#btnSubmit").off("click", onClickSubmit);
    $("#btnSubmit").on("click", onClickSubmit);

    $("#btnResetControls").off("click", onClickResetControls);
    $("#btnResetControls").on("click", onClickResetControls);

}

function onLeaveChange() {
    var cmbLeaveStatus = $("#cmbLeaveStatus").val() || 0;
    buildDeviceListGrid([]);
    if (cmbLeaveStatus > 0) {
        getLeavesList('active');
    }
}

function onClickResetControls() {
    $("#cmbLeaveStatus").val("1");

    $("#txtStartDate").kendoDatePicker({format: dtFMT, value: new Date()});
    $("#txtEndDate").kendoDatePicker({format: dtFMT, value: new Date()});

    $("input[name='rbnLeaveStatus']").prop('checked', false);

}

function onClickSubmit() {
    var cmbLeaveStatus = $("#cmbLeaveStatus option:selected").val();

    var searchTypeId = parseInt($("input[name='rbnLeaveStatus']:checked").val());

    if (searchTypeId !== null && parseInt(searchTypeId) > 0) {


        var url = ipAddress + "/homecare/vacations/?fields=*&managerIds=" + Number(sessionStorage.userId) + "&is-active=1&is-deleted=0&leaveStatusId=" + cmbLeaveStatus;

        var startDT = $("#txtStartDate").data("kendoDatePicker");
        var endDT = $("#txtEndDate").data("kendoDatePicker");

        var sDate = getFromDate(new Date(startDT.value()));
        var eDate = getToDate(new Date(endDT.value()));

        if (searchTypeId === 1) {

            url = url + "&from-date=" + sDate + "&to-date=" + eDate;

        } else if (searchTypeId === 2) {
            url = url + "&accepted-from-date=" + sDate + "&accepted-to-date=" + eDate;
        }

        getAjaxObject(url, "GET", handleGetVacationList, onError);
    } else {
        customAlert.error("Info", "Please select requested or approved.");
    }


}

function getLeavesList(status) {
    var cmbLeaveStatus = $("#cmbLeaveStatus option:selected").val();
    buildDeviceListGrid([]);
    if (cmbLeaveStatus && cmbLeaveStatus != "") {
        if (status == "active") {
            getAjaxObject(ipAddress + "/homecare/vacations/?fields=*&managerIds=" + Number(sessionStorage.userId) + "&is-active=1&is-deleted=0&leaveStatusId=" + cmbLeaveStatus, "GET", handleGetVacationList, onError);
        } else if (status == "inactive") {
            getAjaxObject(ipAddress + "/homecare/vacations/?fields=*&managerIds=" + Number(sessionStorage.userId) + "&is-active=0&is-deleted=1&leaveStatusId=" + cmbLeaveStatus, "GET", handleGetVacationList, onError);
        }
    }
}


function searchOnLoad(status) {
    buildDeviceListGrid([]);
    getLeavesList(status);
}

function onClickActive() {
    $(".btnInActive").removeClass("radioButton-active");
    $(".btnActive").addClass("radioButton-active");
}

function onClickInActive() {
    $(".btnActive").removeClass("radioButton-active");
    $(".btnInActive").addClass("radioButton-active");
}

function onClickDelete() {
    customAlert.confirm("Confirm", "Are you sure to delete?", function (response) {
        if (response.button == "Yes") {
            var dataObj = {};
            dataObj.createdBy = Number(sessionStorage.userId);
            dataObj.isDeleted = 1;
            dataObj.isActive = 0;
            var dataUrl = ipAddress + "/homecare/vacations/";
            var method = "DELETE";
            var selectedItems = angularUIgridWrapper.getSelectedRows();
            dataObj.id = selectedItems[0].idk;
            createAjaxObject(dataUrl, dataObj, method, onCreateDelete, onError);
        }
    });
}

function onCreateDelete(dataObj) {
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            var msg = "Vacation deleted successfully";
            customAlert.error("Info", msg);
            operation = ADD;
            init();
        }
    }
}

function onClickSave() {
    //var txtFDate = $("#txtFDate").val();
    //var txtTDate = $("#txtTDate").val();

    var txtFDate = $("#txtFDate").data("kendoDateTimePicker");
    var fDate = new Date(txtFDate.value());

    var txtTDate = $("#txtTDate").data("kendoDateTimePicker");
    var tDate = new Date(txtTDate.value());

    var cmbLStaus = $("#cmbLStatus").val();
    var IsFlag = false;

    if (txtFDate != "" && txtTDate != "" && cmbLStaus != "") {
        leaveFromDate = fDate.getTime(); //GetDateTimeLeave("txtFDate");
        leaveToDate = tDate.getTime();//GetDateTimeLeave("txtTDate");
        if (!(leaveFromDate >= leaveFDate)) {
            IsFlag = true;
            customAlert.error("Error", "Approved From Date should be between Requested From Date and To Date Time");
        } else if (leaveToDate > leaveTDate) {
            IsFlag = true;
            customAlert.error("Error", "Approved To Date Time should be between Approved From Date and Requested To Date Time");
        }
        if ((!IsFlag)) {
            var reqData = {};
            reqData.remarks = $("#txtRemarks").val();
            reqData.acceptedToDate = leaveToDate;
            reqData.acceptedFromDate = leaveFromDate;
            reqData.approvedBy = Number(sessionStorage.userId);
            reqData.leaveStatusId = $("#cmbLStatus").val();
            reqData.isActive = 1;
            reqData.isDeleted = 0;

            var method = "PUT";

            var selectedItems = angularUIgridWrapper.getSelectedRows();
            reqData.id = selectedItems[0].idk;
            reqData.createdBy = selectedItems[0].createdBy;
            reqData.modifiedBy = Number(sessionStorage.userId);
            if (selectedItems[0].reason != "") {
                reqData.reason = selectedItems[0].reason;
            } else {
                reqData.reason = null;
            }

            reqData.subject = selectedItems[0].subject;

            reqData.fromEmail = selectedItems[0].fromEmail;
            reqData.toEmail = selectedItems[0].toEmail;
            reqData.leaveTypeId = selectedItems[0].leaveTypeId;
            reqData.parentTypeId = selectedItems[0].parentTypeId;
            staffId = selectedItems[0].parentId;
            reqData.parentId = selectedItems[0].parentId;
            reqData.managerIds = selectedItems[0].managerIds;

            dataUrl = ipAddress + "/homecare/vacations/";
            createAjaxObject(dataUrl, reqData, method, onCreate, onError);
            // onGetAppointments();
        }
    } else {
        customAlert.error("error", "Please fill the Required Fields");
    }
}

function onCreate(dataObj) {
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            var msg;
            if (operation == UPDATE) {
                var status = $("#cmbLStatus option:selected").text().toLowerCase();
                var statusVal = $("#cmbLStatus").val();
                msg = "Leave " + status + " successfully";

            }
            displaySessionErrorPopUp("Info", msg, function (res) {
                onClickReset();
                operation = ADD;
                $(".btnActive").click();
                if (statusVal == "2") {
                    onGetAppointments();
                } else {
                    onClickCancel();
                }
            });
        } else {
            customAlert.error("Error", dataObj.response.status.message);
        }
    }
    // onClickReset();
}


function onClickCancel() {
    $(".filter-heading").html("View Staff Leave Request");
    $("#viewDivBlock").show();
    $("#addPopup").hide();
    $(".taskgroupContainer").css("margin", "12px auto 0");
}

function adjustHeight() {
    var defHeight = 270;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 260;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    angularUIgridWrapper.adjustGridHeight(cmpHeight);
}



function onChange() {
    setTimeout(function () {
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if (selectedItems && selectedItems.length > 0) {
            leaveStatusId = selectedItems[0].leaveStatusId
            if (leaveStatusId != 1) {
                $("#txtFDate").prop("disabled", true);
                $("#txtTDate").prop("disabled", true);
                $("#cmbLStatus").prop("disabled", true);
                $("#txtRemarks").prop("disabled", true);
                $("#btnSave").prop("disabled", true);
            } else {
                $("#txtFDate").prop("disabled", false);
                $("#txtTDate").prop("disabled", false);
                $("#cmbLStatus").prop("disabled", false);
                $("#txtRemarks").prop("disabled", false);
                $("#btnSave").prop("disabled", false);
            }
            $("#btnEdit").prop("disabled", false);
            $("#btnDelete").prop("disabled", false);
        } else {
            $("#btnEdit").prop("disabled", true);
            $("#btnDelete").prop("disabled", true);
        }
    });
}

function onClickEdit() {
    $("#txtID").show();
    $("#addPopup").css("padding", "0 0 5px;");
    $(".filter-heading").html("Edit Staff Leave Request");
    $(".taskgroupContainer").css("margin", "-2px auto 0");
    $(".iboxpopup-title").css("padding-bottom", "5px");
    $(".iboxpopup-content-inner").css("padding-bottom", "0px");
    parentRef.operation = "edit";
    operation = "edit";
    $("#viewDivBlock").hide();
    $("#addPopup").show();

    var selectedItems = angularUIgridWrapper.getSelectedRows();
    if (selectedItems && selectedItems.length > 0) {
        var obj = selectedItems[0];
        $("#txtID").html("ID : " + obj.idk);
        $("#lblFMail").text(obj.fromEmail);
        $("#lblTMail").text(obj.toEmail);
        $("#lblReason").text(obj.reason);
        $("#lblSubject").text(obj.subject);
        $("#lblLeaveType").text(obj.leaveType);

        $("#txtRemarks").val(obj.remarks);

        $("#lblLeaveFrom").html(GetDateTimeEdit(obj.fromDate));
        $("#lblLeaveTo").html(GetDateTimeEdit(obj.toDate));

        var txtFDate = $("#txtFDate").data("kendoDateTimePicker");
        var txtTDate = $("#txtTDate").data("kendoDateTimePicker");

        if (obj.leaveStatusId == 1) {
            $("#cmbLStatus").val(2);
            if (obj.fromDate != null) {

                if (txtFDate) {
                    txtFDate.value(GetDateTimeEdit(obj.fromDate));
                }
                //$("#txtFDate").val(GetDateTimeEditLeave(obj.fromDate));
                //var startDate = $("#txtFDate").data("kendoDateTimePicker");
                //var sDate = new Date(startDate.value());
                leaveFDate = obj.fromDate;
            }
            if (obj.toDate != null) {
                if (txtTDate) {
                    txtTDate.value(GetDateTimeEdit(obj.toDate));
                }
                //$("#txtTDate").val(GetDateTimeEditLeave(obj.toDate));
                //var endDate = $("#txtEndDate").data("kendoDateTimePicker");
                //var eDate = new Date(endDate.value());
                leaveTDate = obj.toDate;
            }

        } else {
            $("#cmbLStatus").val(obj.leaveStatusId);
            if (obj.acceptedFromDate != null) {
                //$("#txtFDate").val(GetDateTimeEditLeave(obj.acceptedFromDate));
                txtFDate.value(GetDateTimeEdit(obj.acceptedFromDate));
            }
            if (obj.acceptedToDate != null) {
                //$("#txtTDate").val(GetDateTimeEditLeave(obj.acceptedToDate));
                txtTDate.value(GetDateTimeEdit(obj.acceptedToDate));
            }
        }

        // $("#txtFDate").datepick();
        // $("#txtFDate").datepick("setDate", GetDateEdit(obj.fromDate));

        // $("#txtTDate").datepick();
        // $("#txtTDate").datepick("setDate", GetDateEdit(obj.toDate));


    }
}


var operation = "add";
var selFileResourceDataItem = null;

function onStatusChange() {
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if (cmbStatus && cmbStatus.selectedIndex < 0) {
        cmbStatus.select(0);
    }
}


function onClickReset() {
    $("#txtReason").val("");
    $("#txtFDate").val("");
    $("#txtTDate").val("");
    $("#cmbLStatus").val("");
    $("#txtRemarks").val("");
}

function themeAPIChange() {
    getAjaxObject(ipAddress + "/homecare/settings/?id=2", "GET", getThemeValue, onError);
}

function getThemeValue(dataObj) {
    console.log(dataObj);
    var tempCompType = [];
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.activityTypes) {
            if ($.isArray(dataObj.response.settings)) {
                tempCompType = dataObj.response.settings;
            } else {
                tempCompType.push(dataObj.response.settings);
            }
        }
        if (tempCompType.length > 0) {
            themesChange(tempCompType[0].value);
        }
    }
}

function themesChange(themeValue) {
    if (themeValue == 2) {
        loadAPi("../../Theme2/Theme02.css");
    } else if (themeValue == 3) {
        loadAPi("../../Theme3/Theme03.css");
    }
}


function getManagers() {
    getAjaxObject(ipAddress + "/homecare/tenant-users/?is-active=1&is-deleted=0&userTypeId=700", "GET", getUserList, onError);
}

function getUserList(dataObj) {
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            if ($.isArray(dataObj.response.tenantUsers)) {
                dataArray = dataObj.response.tenantUsers;
            } else {
                dataArray.push(dataObj.response.tenantUsers);
            }
        }
    }
    var tempDataArry = [];
    for (var i = 0; i < dataArray.length; i++) {
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if (dataArray[i].isActive == 1) {
            dataArray[i].Status = "Active";
        }

        $("#cmbManager").append('<option value="' + dataArray[i].idk + '">' + dataArray[i].lastName + ' ' + dataArray[i].firstName + '</option>');
    }

}

function getLeaveStatus() {
    getAjaxObject(ipAddress + "/homecare/leave-status/?fields=id,value", "GET", handleGetLeaveStatus, onError);
}

function getLeaveTypes() {
    getAjaxObject(ipAddress + "/homecare/leave-types/?fields=id,value", "GET", handleGetLeaveTypes, onError);
}

function handleGetLeaveTypes(dataObj) {
    leaveTypes = [];
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            if ($.isArray(dataObj.response.leaveTypes)) {
                leaveTypes = dataObj.response.leaveTypes;
            } else {
                leaveTypes.push(dataObj.response.leaveTypes);
            }
            for (var i = 0; i < leaveTypes.length; i++) {
                leaveTypes[i].idk = leaveTypes[i].id;
            }
        }
    }
}

function handleGetLeaveStatus(dataObj) {
    var leaveArray = [];
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            if ($.isArray(dataObj.response.leaveStatus)) {
                leaveArray = dataObj.response.leaveStatus;
            } else {
                leaveArray.push(dataObj.response.leaveStatus);
            }
            for (var i = 0; i < leaveArray.length; i++) {
                leaveArray[i].idk = leaveArray[i].id;
                if (leaveArray[i].idk != 1) {
                    $("#cmbLStatus").append('<option value="' + leaveArray[i].idk + '">' + leaveArray[i].value + '</option>');
                }
                $("#cmbLeaveStatus").append('<option value="' + leaveArray[i].idk + '">' + leaveArray[i].value + '</option>');
            }
        }
        $("#cmbLeaveStatus").val("1");
    }
}

function getEmails() {
    getAjaxObject(ipAddress + "/homecare/tenant-user-emails/?user-id=" + Number(parentRef.patientId), "GET", handleGetEmails, onError);
}

function handleGetEmails(dataObj) {
    var userEmailArray = [];
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            if ($.isArray(dataObj.response.userEmails)) {
                userEmailArray = dataObj.response.userEmails;
            } else {
                userEmailArray.push(dataObj.response.userEmails);
            }
            for (var i = 0; i < userEmailArray.length; i++) {
                if (userEmailArray[i].id == Number(parentRef.patientId)) {
                    $("#txtFMail").val(userEmailArray[i].email);
                } else {
                    $("#txtTMail").val(userEmailArray[i].email);
                }
            }
        }
    }
}

function showPtImage() {
    var node = '<div ng-show="((row.entity.photo))"><img src="{{row.entity.photo}}" onerror="this.src=\'../../img/AppImg/HosImages/blank.png\'" style="width:50px"><spn>';//this.src=\'../../img/AppImg/HosImages/patient1.png\'
    node = node + "</div>";
    return node;
}

function fncGetLeaveType(leaveTypeId) {
    for (var i = 0; i < leaveTypes.length; i++) {
        if (leaveTypes[i].idk == leaveTypeId) {
            return leaveTypes[i].value;
        }
    }
}

function fncGetName(staffId) {
    for (var i = 0; i < staffArray.length; i++) {
        if (staffArray[i].idk == staffId) {
            return staffArray[i].lastName + ' ' + staffArray[i].firstName;
        }
    }
}

function getAccountList(dataObj) {
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if ($.isArray(dataObj.response.provider)) {
            staffArray = dataObj.response.provider;
        } else {
            staffArray.push(dataObj.response.provider);
        }
    }

    for (var i = 0; i < staffArray.length; i++) {
        staffArray[i].idk = staffArray[i].id;
        staffFacilityId = staffArray[i].facilityId;
    }
    buildDeviceListGrid([]);
    getAjaxObject(ipAddress + "/homecare/vacations/?fields=*&managerIds=" + Number(sessionStorage.userId) + "&is-active=1&is-deleted=0&leaveStatusId=1", "GET", handleGetVacationList, onError);
}

function onGetLeaveAppointments(leaveApprovedFromTime, leaveApprovedToTime, staffId, facilityId) {
    var url = ipAddress + "/appointment/list/?facility-id=" + facilityId + "&provider-id=" + staffId + "&to-date=" + leaveApprovedToTime + "&from-date=" + leaveApprovedFromTime + "&is-active=1&is-deleted=0";
    getAjaxObject(url, "GET", onPatientListData, onError);
}

function onPatientListData(dataObj) {
    var dtArray = [];
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.appointment) {
            if ($.isArray(dataObj.response.appointment)) {
                dtArray = dataObj.response.appointment;
            } else {
                dtArray.push(dataObj.response.appointment);
            }
        }
    }
}


function buildDeviceListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Staff",
        "field": "parentId",
        "width": "8%",
        "cellTemplate": showPtImage()
    });
    gridColumns.push({
        "title": "Name",
        "field": "name",
        "width": "15%"
    });
    gridColumns.push({
        "title": "Req From",
        "field": "FD",
        "width": "15%"
    });
    gridColumns.push({
        "title": "Req To",
        "field": "TD",
        "width": "15%"
    });
    gridColumns.push({
        "title": "Approved From",
        "field": "approvedFrom",

    });
    gridColumns.push({
        "title": "Approved To",
        "field": "approvedTo",

    });
    gridColumns.push({
        "title": "Leave Type",
        "field": "leaveType",
        "width": "10%"
    });
    gridColumns.push({
        "title": "Reason",
        "field": "reason",
        "width": "15%"
    });
    angularUIgridWrapper.creategrid(dataSource, gridColumns, otoptions);
    adjustHeight();
    //onIndividualRowClick();
}

function onGetAppointments() {
    var url = ipAddress + "/appointment/list/?facility-id=" + staffFacilityId + "&provider-id=" + staffId + "&to-date=" + leaveToDate + "&from-date=" + leaveFromDate + "&is-active=1&is-deleted=0";
    getAjaxObject(url, "GET", onGetAppointmentsList, onError);

}

function closePtRAddAction(evt, returnData) {
    onClickCancel();
}

function onGetAppointmentsList(dataObj) {
    var dtArray = [];
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.appointment) {
            if ($.isArray(dataObj.response.appointment)) {
                dtArray = dataObj.response.appointment;
            } else {
                dtArray.push(dataObj.response.appointment);
            }
        }
    }
    if (dtArray.length > 0) {
        var popW = "60%";
        var popH = 500;

        var profileLbl;
        var devModelWindowWrapper = new kendoWindowWrapper();
        profileLbl = "Staff Appointments";
        parentRef.staffFacilityId = staffFacilityId;
        parentRef.staffId = staffId;
        parentRef.leaveFromDate = leaveFromDate;
        parentRef.leaveToDate = leaveToDate;


        devModelWindowWrapper.openPageWindow("../../html/masters/staffAppointment.html", profileLbl, popW, popH, true, closePtRAddAction);
    } else {
        customAlert.info("Info", "No appointments exist during leave days");
    }
}


function getFromDate(fromDate) {


    var day = fromDate.getDate();
    var month = fromDate.getMonth();
    month = month + 1;
    var year = fromDate.getFullYear();

    var strDate = month + "/" + day + "/" + year;
    strDate = strDate + " 00:00:00";

    var date = new Date(strDate);
    date.setMilliseconds(0);
    var strDateTime = date.getTime();

    return strDateTime;
}

function getToDate(toDate) {
    var day = toDate.getDate();
    var month = toDate.getMonth();
    month = month + 1;
    var year = toDate.getFullYear();

    var strDate = month + "/" + day + "/" + year;
    strDate = strDate + " 23:59:59";

    var date = new Date(strDate);
    date.setMilliseconds(0);
    var strDateTime = date.getTime();

    return strDateTime;
}




var sortByDateDec = function (x, y) {
    // This is a comparison function that will result in dates being sorted in
    // DESCENDING order.
    var date1 = new Date(x.fromDate);
    var date2 = new Date(y.fromDate);
    if (date1 > date2) return -1;
    if (date1 < date2) return 1;
    return 0;
};



var sortByDateAsc = function (x, y) {
    // This is a comparison function that will result in dates being sorted in
    // ASCENDING order. As you can see, JavaScript's native comparison operators
    // can be used to compare dates. This was news to me.

    date1 = new Date(x.fromDate);
    date2 = new Date(y.fromDate);

    if (date1 > date2) return 1;
    if (date1 < date2) return -1;
    return 0;
};

var angularUIgridWrapper;
var parentRef = null;

var selItem = null;
var ADD = "add";
var UPDATE = "update";
var VIEW = "view";
var DELETE = "delete";
var patientId = "";
var communicationId = "";
var contactComunicationId = "";
var statusArr = [{ Key: 'Active', Value: 'Active' }, { Key: 'InActive', Value: 'InActive' }];
var filterArr = [{ Key: '', Value: 'All' }, { Key: 'id', Value: 'Contract ID' }, { Key: 'billing-account-id', Value: 'Billing Account ID' }, { Key: 'abbr', Value: 'Abbrevation' }, { Key: 'name', Value: 'Name' }, { Key: 'display-name', Value: 'Display Name' }]

var patientInfoObject = null;
var commId = "";
var operation;

$(document).ready(function() {
    parentRef = parent.frames['iframe'].window;
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("dgridcontractList", dataOptions);
    angularUIgridWrapper.init();
    buildcontractListGrid([]);
    getCountryZoneName();
});

$(window).load(function() {
    loading = false;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded() {
    $("#btnEdit").prop("disabled", true);
    $("#btnDelete").prop("disabled", true);
    init();
    buttonEvents();
    adjustHeight();
    $('.btnActive').trigger('click');
}

function init() {
    //getAjaxObject(ipAddress + "/contract/list/", "GET", getcontractList, onError);
    allowAlphaNumeric('txtAbbreviation');
    allowSplAlphaNumeric('txtN');
    allowSplAlphaNumeric('txtDN');
    allowSpaceAlphaNumeric('txtAdd1');
    allowSpaceAlphaNumeric('txtAdd2');
    allowSpaceAlphaNumeric('AddtxtAdd1');
    allowSpaceAlphaNumeric('AddtxtAdd2');
    allowNumerics('txtWPhone');
    allowNumerics('txtExtension');
    allowNumerics('txtFax');
    allowNumerics('txtCell');
    allowNumerics('txtHPhone');
    allowNumerics('txtCell1');
    allowNumerics('txtHPhone1');
    allowAlphaNumeric('txtExtID1');
    allowAlphaNumeric('txtExtID2');
    $("#cmbFTIT").kendoComboBox();
    $("#cmbPrefix").kendoComboBox();
    $("#cmbStatus").kendoComboBox();
    $("#cmbSuffix").kendoComboBox();
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if (cmbStatus) {
        cmbStatus.enable(true);
    }
    $("#cmbZip1").kendoComboBox();
    $("#cmbSMS").kendoComboBox();
    $("#AddcmbSMS").kendoComboBox();
    $("#cmbBilling").kendoComboBox();
    $('[data-toggle="tab"]').on('click', function(e) {
        e.preventDefault();
        $('.alert').remove();
    });
    getZip();
    setDataForSelection(filterArr, "cmbFilterByName", onFilterChange, ["Value", "Key"], 0, "");
}

function onError(errorObj) {
    console.log(errorObj);
}

function getcontractList(dataObj) {
    console.log(dataObj);
    var dataArray = [];
    if (dataObj && dataObj.response.status && dataObj.response.status.code == "1") {
        if ($.isArray(dataObj.response.contract)) {
            dataArray = dataObj.response.contract;
        } else {
            dataArray.push(dataObj.response.contract);
        }
    }
    var tempDataArry = [];
    for (var i = 0; i < dataArray.length; i++) {
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if (dataArray[i].isActive == 1) {
            dataArray[i].Status = "Active";
            var createdDate = new Date(dataArray[i].createdDate);
            var createdDateString = createdDate.toLocaleDateString();
            var modifiedDate = new Date(dataArray[i].modifiedDate);
            var modifiedDateString = modifiedDate.toLocaleDateString();
            dataArray[i].createdDateString = createdDateString;
            dataArray[i].modifiedDateString = modifiedDateString;
        }

    }
    if(dataArray.length == 0 && dataObj.response.status.code == "0") {
        $('.customAlert').append('<div class="alert alert-danger">'+dataObj.response.status.message+'</div>');
    }
    buildcontractListGrid(dataArray);
}

function buttonEvents() {
    $("#btnEdit").off("click", onClickOK);
    $("#btnEdit").on("click", onClickOK);

    $("#btnCancelDet").off("click", onClickCancel);
    $("#btnCancelDet").on("click", onClickCancel);

    $("#btnCancel").off("click",onClickClose);
    $("#btnCancel").on("click",onClickClose);

    $("#btnDelete").off("click", onClickDelete);
    $("#btnDelete").on("click", onClickDelete);

    $("#btnAdd").off("click");
    $("#btnAdd").on("click", onClickAdd);

    $("#btnReset").off("click", onClickReset);
    $("#btnReset").on("click", onClickReset);

    $("#btnSave").off("click", onClickSave);
    $("#btnSave").on("click", onClickSave);

    $("#btnSearch").off("click", onClickSearch);
    $("#btnSearch").on("click", onClickSearch);

    $("#btnZipSearch").off("click");
    $("#btnZipSearch").on("click", onClickZipSearch);

    $("#AddbtnZipSearch").off("click");
    $("#AddbtnZipSearch").on("click", onClickAdditionalZipSearch);

    $("#accountFilter-btn").off("click");
    $("#accountFilter-btn").on("click", onClickFilterBtn);

    $(".btnActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('active');
        onClickActive();
    });
    $(".btnInActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('inactive');
        onClickInActive();
    });

    $("#txtWPhone").on('keyup', function(e) {
        if($(this).val().length > 0) {
            $("#txtExtension").removeAttr("disabled");
        }
        else {
            $("#txtExtension").attr("disabled","disabled");
        }
    });
    $("#cmbFilterByValue").on('keyup', function(e) {
        if($(this).val().length > 0) {
            $("#accountFilter-btn").removeAttr("disabled");
        }
        else {
            $("#accountFilter-btn").attr("disabled","disabled");
        }
    });
    $("#txtCell").on('keyup', function(e) {
        if($(this).val().length > 0) {
            $("#cmbSMS").closest('.k-dropdown-wrap').removeClass('k-state-disabled').find('.k-input').removeAttr("disabled");
        }
        else {
            $("#cmbSMS").closest('.k-dropdown-wrap').addClass('k-state-disabled').find('.k-input').attr("disabled","disabled");
        }
    });
    $("#txtCell1").on('keyup', function(e) {
        if($(this).val().length > 0) {
            $("#AddcmbSMS").closest('.k-dropdown-wrap').removeClass('k-state-disabled').find('.k-input').removeAttr("disabled");
        }
        else {
            $("#AddcmbSMS").closest('.k-dropdown-wrap').addClass('k-state-disabled').find('.k-input').attr("disabled","disabled");
        }
    });
}
function searchOnLoad(status) {
    buildcontractListGrid([]);
    if(status == "active") {
        var urlExtn = '/contract/list?is-active=1&is-deleted=0';
    }
    else if(status == "inactive") {
        var urlExtn = '/contract/list?is-active=0';
    }
    getAjaxObject(ipAddress+urlExtn,"GET",getcontractList,onError);
}
function getCountryZoneName() {
    $.getJSON('//freegeoip.net/json/?callback=', function(dataObj) {
        var dataUrl = ipAddress + "/user/location";
        console.log(dataObj);
        sessionStorage.country = dataObj.country_code;
        dataObj.createdBy = sessionStorage.userId;
        dataObj.countryCode = dataObj.country_code;
        dataObj.countryName = dataObj.country_name;
        dataObj.regionCode = dataObj.region_code;
        dataObj.regionName = dataObj.region_name;
        dataObj.timeZone = dataObj.time_zone;
        dataObj.metroCode = dataObj.metro_code;
        dataObj.zipCode = dataObj.zip_code;
        sessionStorage.countryName = dataObj.country_name;
        var countryName = sessionStorage.countryName;
        if (countryName.indexOf("India") >= 0) {
            $('.postalCode').html('Postal Code :<span class="mandatoryField">*</span>');
            $('.postalCodeAdditional').html('Postal Code');
            $('.stateLabel').text("State");
            $('.zipFourWrapper').addClass('hideZipFourWrapper');
        } else if (countryName.indexOf("United Kingdom")) {
            $('.postalCode').html('Postal Code :<span class="mandatoryField">*</span>');
            $('.postalCodeAdditional').html('Postal Code');
            $('.stateLabel').text("County");
            $('.zipFourWrapper').addClass('hideZipFourWrapper');
        } else {
            $('.postalCode').html('Zip :<span class="mandatoryField">*</span>');
            $('.postalCodeAdditional').html('Zip');
            $('.stateLabel').text("State");
            $('.zipFourWrapper').removeClass('hideZipFourWrapper');
        }
    });
}

function adjustHeight() {
    if($(window).width() > 767) {
        var defHeight = 120; //+window.top.getAvailPageHeight();
        if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
            defHeight = 80;
        }
        var cmpHeight = window.innerHeight - defHeight;
        cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
        angularUIgridWrapper.adjustGridHeight(cmpHeight);
    }
}
function onClickActive() {
    $(".btnInActive").removeClass("selectButtonBarClass");
    $(".btnActive").addClass("selectButtonBarClass");
}
function onClickInActive() {
    $(".btnActive").removeClass("selectButtonBarClass");
    $(".btnInActive").addClass("selectButtonBarClass");
}
function onClickFilterBtn(e) {
    e.preventDefault();
    $('.alert').remove();
    var filterName = $("#cmbFilterByName").val();
    var filterValue = $("#cmbFilterByValue").val();
    buildcontractListGrid([]);
    /*if($(".btnActive").hasClass("selectButtonBarClass")) {
        var urlExtn = '/contract/list?is-active=1&is-deleted=0';
    }
    else {
        var urlExtn = '/contract/list?is-active=0&is-deleted=1';
    }*/
    getAjaxObject(ipAddress+"/contract/list?is-active=1&is-deleted=0&"+filterName+"="+filterValue,"GET",getcontractList,onError);
}

function buildcontractListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "ID",
        "field": "idk"
    });
    gridColumns.push({
        "title": "Abbrevation",
        "field": "abbreviation"
    });
    gridColumns.push({
        "title": "Display Name",
        "field": "displayName"
    });
    gridColumns.push({
        "title": "Name",
        "field": "name"
    });
    gridColumns.push({
        "title": "External ID1",
        "field": "externalId1"
    });
    gridColumns.push({
        "title": "External ID2",
        "field": "externalId2"
    });
    /*if($(window).width() > 767) {
	    gridColumns.push({
	        "title": "External ID1",
	        "field": "externalId1"
	    });
	    gridColumns.push({
	        "title": "External ID2",
	        "field": "externalId2"
	    });
	    gridColumns.push({
	        "title": "Created By",
	        "field": "createdBy",
		});
		gridColumns.push({
	        "title": "Created Date",
	        "field": "createdDateString",
		});
		gridColumns.push({
	        "title": "Modified By",
	        "field": "modifiedBy",
		});
		gridColumns.push({
	        "title": "Modified Date",
	        "field": "modifiedDateString",
		});
	}*/
    angularUIgridWrapper.creategrid(dataSource, gridColumns, otoptions);
    adjustHeight();
    //onIndividualRowClick();
}
var prevSelectedItem = [];

function onChange() {
    setTimeout(function() {
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if (selectedItems && selectedItems.length > 0) {
            $("#btnEdit").prop("disabled", false);
            $("#btnDelete").prop("disabled", false);
        } else {
            $("#btnEdit").prop("disabled", true);
            $("#btnDelete").prop("disabled", true);
        }
    });

}
var selAccountItem = null;

function onClickOK() {
    setTimeout(function() {
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if (selectedItems && selectedItems.length > 0) {
            var obj = {};
            obj.selItem = selectedItems[0];
            selAccountItem = selectedItems[0];
            addAccount("update");
            // var onCloseData = new Object();
            /* obj.status = "success";
             obj.operation = "ok";
            	var windowWrapper = new kendoWindowWrapper();
            	windowWrapper.closePageWindow(obj);*/
        }
    })
}
function onClickDelete() {
    customAlert.confirm("Confirm", "Are you sure you want delete?", function(response) {
        if (response.button == "Yes") {
            var selectedItems = angularUIgridWrapper.getSelectedRows();
            console.log(selectedItems);
            if (selectedItems && selectedItems.length > 0) {
                var selItem = selectedItems[0];
                if (selItem) {
                    var dataUrl = ipAddress + "/contract/delete";
                    var reqObj = {};
                    reqObj.id = selItem.idk;
                    reqObj.isDeleted = "1";
                    reqObj.isActive = "0";
                    reqObj.modifiedBy = sessionStorage.userId;
                    createAjaxObject(dataUrl, reqObj, "POST", onDeleteCountryt, onError);
                }
            }
        }
    });
}

function onDeleteCountryt(dataObj) {
    console.log(dataObj);
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            customAlert.info("info", "Billing Deleted Successfully");
            /*getAjaxObject(ipAddress + "/contract/list/", "GET", getcontractList, onError);*/
            buildcontractListGrid([]);
            init();
            getAjaxObject(ipAddress + "/contract/list/", "GET", getcontractList, onError);
        } else {
            customAlert.error("error", dataObj.message);
        }
    }
}

function onClickAdd() {
    /*var obj = {};
     obj.status = "Add";
     obj.operation = "ok";
    	var windowWrapper = new kendoWindowWrapper();
    	windowWrapper.closePageWindow(obj);*/
    addAccount("add");

}

function addAccount(opr) {
    var popW = 850;
    var popH = 600;

    var profileLbl;
    // var operation = "ADD";
    //var devModelWindowWrapper = new kendoWindowWrapper();
    $('.listDataWrapper').hide();
    $('.addOrRemoveWrapper').show();
    $('.contractListSidebar').find('li').each(function() {
        $(this).removeClass('active');
    });
    $('.contractListSidebar').find('li:first-child a').trigger('click');
    $('.alert').remove();
    parentRef.operation = opr;
    operation = opr;
    if (opr == "add") {
        $('#btnSave').html('<span><img src="../../img/medication/Save.png" class="contractList-btn-img" />Save</span>');
        $('.tabContentTitle').text('Add Contract');
        $('#btnReset').show();
        $('#btnReset').trigger('click');
        /*profileLbl = "Add Billing Account";
        parentRef.selItem = null;*/
    } else {
        /*profileLbl = "Edit Billing Account";
        parentRef.selItem = selAccountItem;*/
        $('#btnReset').trigger('click');
        $('.tabContentTitle').text('Edit Contract');
        $('#btnReset').hide();
        $('#btnSave').html('<span><img src="../../img/medication/Save.png" class="contractList-btn-img" />Update</span>');
        console.log(selAccountItem);
        var communicationLen = selAccountItem.communications != null ? selAccountItem.communications.length : 0;
        var cityIndexVal = "";
        var dupCityIndexVal = "";
        for(var i=0; i<communicationLen; i++) {
            if(selAccountItem.communications[i].parentTypeId == 600) {
                cityIndexVal = i;
            }
            if(selAccountItem.communications[i].parentTypeId == 601) {
                dupCityIndexVal = i;
            }
        }
        if(cityIndexVal != "" || (cityIndexVal == 0 && selAccountItem.communications != null)) {
            $("#cityIdHiddenValue").val(selAccountItem.communications[cityIndexVal].cityId);
            $("#txtCountry").val(selAccountItem.communications[cityIndexVal].country);
            $("#cmbZip").val(selAccountItem.communications[cityIndexVal].zip);
            $("#txtZip4").val(selAccountItem.communications[cityIndexVal].zipFour);
            $("#txtState").val(selAccountItem.communications[cityIndexVal].state);
            $("#txtCity").val(selAccountItem.communications[cityIndexVal].city);
            $("#txtWPhone").val(selAccountItem.communications[cityIndexVal].workPhone);
            $("#txtExtension").val(selAccountItem.communications[cityIndexVal].workPhoneExt);
            $("#txtHPhone").val(selAccountItem.communications[cityIndexVal].homePhone);
            $("#txtFax").val(selAccountItem.communications[cityIndexVal].fax);
            $("#txtCell").val(selAccountItem.communications[cityIndexVal].cellPhone);
            getComboListIndex("cmbSMS", "desc", selAccountItem.communications[cityIndexVal].sms);
            $("#txtEmail").val(selAccountItem.communications[cityIndexVal].email);
            $("#txtAdd1").val(selAccountItem.communications[cityIndexVal].address1);
            $("#txtAdd2").val(selAccountItem.communications[cityIndexVal].address2);
            getComboListIndex("cmbZip", "idk", selAccountItem.communications[cityIndexVal].cityId);
            zipSelItem = selAccountItem.communications[cityIndexVal];
            zipSelItem.idk = selAccountItem.communications[cityIndexVal].id;
            //getAjaxObject(ipAddress+"/city/list/?id="+selAccountItem.communications[cityIndexVal].cityId,"GET",onCityList,onError);
        }
        if(dupCityIndexVal != "") {
            $("#AddcityIdHiddenValue").val(selAccountItem.communications[dupCityIndexVal].cityId);
            $("#AddtxtCountry").val(selAccountItem.communications[dupCityIndexVal].country);
            $("#AddcmbZip").val(selAccountItem.communications[dupCityIndexVal].zip);
            $("#AddtxtZip4").val(selAccountItem.communications[dupCityIndexVal].zipFour);
            $("#AddtxtState").val(selAccountItem.communications[dupCityIndexVal].state);
            $("#AddtxtCity").val(selAccountItem.communications[dupCityIndexVal].city);
            AddzipSelItem = selAccountItem.communications[dupCityIndexVal];
            AddzipSelItem.idk = selAccountItem.communications[dupCityIndexVal].id;
            if(selAccountItem.communications[dupCityIndexVal].homePhoneExt) {
                $("#txtExtension1").val(selAccountItem.communications[dupCityIndexVal].homePhoneExt);
            }
            if(selAccountItem.communications[dupCityIndexVal].homePhone) {
                $("#txtHPhone1").val(selAccountItem.communications[dupCityIndexVal].homePhone);
            }
            if(selAccountItem.communications[dupCityIndexVal].address1) {
                $("#AddtxtAdd1").val(selAccountItem.communications[dupCityIndexVal].address1);
            }
            if(selAccountItem.communications[dupCityIndexVal].address2) {
                $("#AddtxtAdd2").val(selAccountItem.communications[dupCityIndexVal].address2);
            }
            if(selAccountItem.communications[dupCityIndexVal].cellPhone) {
                $("#txtCell1").val(selAccountItem.communications[dupCityIndexVal].cellPhone);
            }
            if(selAccountItem.communications[dupCityIndexVal].sms) {
                //$("#AddcmbSMS").val(selAccountItem.communications[dupCityIndexVal].sms);
                getComboListIndex("AddcmbSMS", "desc", selAccountItem.communications[dupCityIndexVal].sms);
            }
            if(selAccountItem.communications[dupCityIndexVal].email) {
                $("#txtEmail1").val(selAccountItem.communications[dupCityIndexVal].email);
            }
            //getAjaxObject(ipAddress+"/city/list/?id="+selAccountItem.communications[dupCityIndexVal].cityId,"GET",onAdditionalCityList,onError);
        }
        $("#txtID").val(selAccountItem.idk);
        $("#txtExtID1").val(selAccountItem.externalId1);
        $("#txtExtID2").val(selAccountItem.externalId2);
        getComboListIndex("cmbPrefix", "value", selAccountItem.contactPrefix);
        getComboListIndex("cmbSuffix", "value", selAccountItem.contactSuffix);
        $("#txtFN").val(selAccountItem.contactFirstName);
        $("#txtLN").val(selAccountItem.contactLastName);
        $("#txtMN").val(selAccountItem.contactMiddleName);
        //$("#txtWeight").val(dataObj.response.patient.weight);
        //$("#txtHeight").val(dataObj.response.patient.height);
        $("#txtNN").val(selAccountItem.contactNickName);
        $("#txtAbbreviation").val(selAccountItem.abbreviation);
        $("#txtN").val(selAccountItem.name);
        $("#txtDN").val(selAccountItem.displayName);
        /*$("#txtFTI").val(selAccountItem.federalTaxId);
        $("#txtFTEIN").val(selAccountItem.federalTaxEin);
        $("#txtSTI").val(selAccountItem.stateTaxId);
        $("#txtNOTT").val(selAccountItem.taxName);
        $("#txtNPI").val(selAccountItem.npi);
        $("#txtTI").val(selAccountItem.taxonomyId);
        getComboListIndex("cmbFTIT", "desc", selAccountItem.federalTaxIdType);*/
        getComboListIndex("cmbStatus", "Value", selAccountItem.Status);
        getComboListIndex("cmbBilling", "idk", selAccountItem.billingAccountId);
        /*getComboListIndex("cmbPrefix", "desc", selAccountItem.contactPrefix);*/

        //	$("#txtSMS").val(comObj.sms);
        /*getComboListIndex("cmbSMS", "desc", selAccountItem.sms);*/
        /*$("#txtCountry").val(selAccountItem.country);
        $("#cmbZip").val(selAccountItem.zip);
        $("#txtZip4").val(selAccountItem.zipFour);
        $("#txtState").val(selAccountItem.state);
        $("#txtCity").val(selAccountItem.city);*/
    }

    //devModelWindowWrapper.openPageWindow("../../html/masters/createAccount.html", profileLbl, popW, popH, true, closeAddAccountAction);
}
/*function onCityList(dataObj) {
	console.log(dataObj);
    var dataArray = [];
    if (dataObj && dataObj.response.status && dataObj.response.status.code == "1") {
        if ($.isArray(dataObj.response.cityExt)) {
            dataArray = dataObj.response.cityExt;
        } else {
            dataArray.push(dataObj.response.cityExt);
        }
        $("#txtCountry").val(dataArray[0].country);
        $("#cmbZip").val(dataArray[0].zip);
        $("#txtZip4").val(dataArray[0].zipFour);
        $("#txtState").val(dataArray[0].state);
        $("#txtCity").val(dataArray[0].city);
        zipSelItem = dataArray[0];
        zipSelItem.idk = dataArray[0].id;
    }
}
function onAdditionalCityList(dataObj) {
	console.log(dataObj);
    var dataArray = [];
    if (dataObj && dataObj.response.status && dataObj.response.status.code == "1") {
        if ($.isArray(dataObj.response.cityExt)) {
            dataArray = dataObj.response.cityExt;
        } else {
            dataArray.push(dataObj.response.cityExt);
        }
        $("#AddtxtCountry").val(dataArray[0].country);
        $("#AddcmbZip").val(dataArray[0].zip);
        $("#AddtxtZip4").val(dataArray[0].zipFour);
        $("#AddtxtState").val(dataArray[0].state);
        $("#AddtxtCity").val(dataArray[0].city);
        AddzipSelItem = dataArray[0];
        AddzipSelItem.idk = dataArray[0].id;
    }
}*/

function closeAddAccountAction(evt, returnData) {
    if (returnData && returnData.status == "success") {
        var operation = returnData.operation;
        if (operation == "add") {
            customAlert.info("info", "Contract Created Successfully");
        } else {
            customAlert.info("info", "Contract  Updated Successfully");
        }
        init();
    }
}

function onClickCancel() {
    var obj = {};
    var windowWrapper = new kendoWindowWrapper();
    //windowWrapper.closePageWindow(obj);
}
function onClickClose() {
    $('.listDataWrapper').show();
    $('.addOrRemoveWrapper').hide();
    $("#btnEdit").prop("disabled", true);
    $("#btnDelete").prop("disabled", true);
    /*buildcontractListGrid([]);
    getAjaxObject(ipAddress+"/contract/list?is-active=1&is-deleted=0","GET",getcontractList,onError);*/
    $('.selectButtonBarClass').trigger('click');
}



function onClickZipSearch() {
    var popW = 600;
    var popH = 500;

    parentRef.searchZip = true;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search Zip";
    devModelWindowWrapper.openPageWindow("../../html/masters/zipList.html", profileLbl, popW, popH, true, onCloseSearchZipAction);
}
function onClickAdditionalZipSearch() {
    var popW = 600;
    var popH = 500;

    parentRef.searchZip = true;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search Zip";
    devModelWindowWrapper.openPageWindow("../../html/masters/zipList.html", profileLbl, popW, popH, true, onCloseSearchAdditionalZipAction);
}
var zipSelItem = null;
var AddzipSelItem = null;

function onCloseSearchZipAction(evt, returnData) {
    console.log(returnData);
    if (returnData && returnData.status == "success") {
        //console.log();
        $("#cmbZip").val("");
        $("#txtZip4").val("");
        $("#txtState").val("");
        $("#txtCountry").val("");
        $("#txtCity").val("");
        var selItem = returnData.selItem;
        if (selItem) {
            zipSelItem = selItem;
            if (zipSelItem) {
                cityId = zipSelItem.idk;
                if (zipSelItem.zip) {
                    $("#cmbZip").val(zipSelItem.zip);
                }
                if (zipSelItem.zipFour) {
                    $("#txtZip4").val(zipSelItem.zipFour);
                }
                if (zipSelItem.state) {
                    $("#txtState").val(zipSelItem.state);
                }
                if (zipSelItem.country) {
                    $("#txtCountry").val(zipSelItem.country);
                }
                if (zipSelItem.city) {
                    $("#txtCity").val(zipSelItem.city);
                }
            }
        }
    }
}
function onCloseSearchAdditionalZipAction(evt, returnData) {
    console.log(returnData);
    if (returnData && returnData.status == "success") {
        //console.log();
        $("#AddcmbZip").val("");
        $("#AddtxtZip4").val("");
        $("#AddtxtState").val("");
        $("#AddtxtCountry").val("");
        $("#AddtxtCity").val("");
        var selItem = returnData.selItem;
        if (selItem) {
            AddzipSelItem = selItem;
            if (AddzipSelItem) {
                cityId = AddzipSelItem.idk;
                if (AddzipSelItem.zip) {
                    $("#AddcmbZip").val(AddzipSelItem.zip);
                }
                if (AddzipSelItem.zipFour) {
                    $("#AddtxtZip4").val(AddzipSelItem.zipFour);
                }
                if (AddzipSelItem.state) {
                    $("#AddtxtState").val(AddzipSelItem.state);
                }
                if (AddzipSelItem.country) {
                    $("#AddtxtCountry").val(AddzipSelItem.country);
                }
                if (AddzipSelItem.city) {
                    $("#AddtxtCity").val(AddzipSelItem.city);
                }
            }
        }
    }
}

function getZip() {
    getAjaxObject(ipAddress + "/city/list?is-active=1", "GET", getZipList, onError);
}

function getZipList(dataObj) {
    //var dArray = getTableListArray(dataObj);
    var dArray = [];
    if (dataObj && dataObj.response && dataObj.response.cityExt) {
        if ($.isArray(dataObj.response.cityExt)) {
            dArray = dataObj.response.cityExt;
        } else {
            dArray.push(dataObj.response.cityExt);
        }
    }
    if (dArray && dArray.length > 0) {
        /*setDataForSelection(dArray, "cmbZip", onZipChange, ["zip", "id"], 0, "");
        onZipChange();*/
    }
    getPrefix();
}

function getPrefix() {
    getAjaxObject(ipAddress + "/Prefix/list?is-active=1", "GET", getCodeTableValueList, onError);
}

function onComboChange(cmbId) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb && cmb.selectedIndex < 0) {
        cmb.select(0);
    }
}

function onPrefixChange() {
    onComboChange("cmbPrefix");
}
function onSuffixChange() {
    onComboChange("cmbSuffix");
}

function onPtChange() {
    onComboChange("cmbStatus");
}

function onGenderChange() {
    onComboChange("cmbGender");
}
function onSMSChange() {
    onComboChange("cmbSMS");
}
function onAddSMSChange() {
    onComboChange("AddcmbSMS");
}

function onLanChange() {
    onComboChange("cmbLan");
}

function onRaceChange() {
    onComboChange("cmbRace");
}

function onEthnicityChange() {
    onComboChange("cmbEthicity");
}

function onZipChange() {
    onComboChange("cmbZip");
    var cmbZip = $("#cmbZip").data("kendoComboBox");
    if (cmbZip) {
        var dataItem = cmbZip.dataItem();
        if (dataItem) {
            $("#txtZip4").val(dataItem.zipFour);
            $("#txtCountry").val(dataItem.country);
            $("#txtState").val(dataItem.state);
            $("#txtCity").val(dataItem.city);
        }
    }
}

function getCodeTableValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbPrefix", onPrefixChange, ["value", "idk"], 0, "");
    }
    getAjaxObject(ipAddress + "/Suffix/list?is-active=1", "GET", getCodeTableSuffixValueList, onError);
}
function getCodeTableSuffixValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbSuffix", onSuffixChange, ["value", "idk"], 0, "");
    }
    getAjaxObject(ipAddress+"/Status/list?is-active=1","GET",getPatientStatusValueList,onError);
}


function getPatientStatusValueList(dataObj) {
    var dArray = getTableFirstListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(statusArr, "cmbStatus", onStatusChange, ["Value", "Key"], 0, "");
    }
    getAjaxObject(ipAddress+"/billing-account/list?is-active=1","GET",getAccountList,onError);
    //getAjaxObject(ipAddress + "/Gender/list?is-active=1", "GET", getGenderValueList, onError);
}

function getGenderValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    /*if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbGender", onGenderChange, ["desc", "idk"], 0, "");
    }*/
}
function getAccountList(dataObj){
    var dataArray = [];
    if(dataObj && dataObj.response.status && dataObj.response.status.code == "1"){
        if($.isArray(dataObj.response.billingAccount)){
            dataArray = dataObj.response.billingAccount;
        }else{
            dataArray.push(dataObj.response.billingAccount);
        }
    }
    var tempDataArry = [];
    var obj = {};
    obj.name = "";
    obj.idk = "";
    tempDataArry.push(obj);
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
            var obj = dataArray[i];
            obj.idk = dataArray[i].id;
            obj.status = dataArray[i].Status;
            tempDataArry.push(obj);
        }
    }
    dataArray = tempDataArry;
    setDataForSelection(dataArray, "cmbBilling", onBillAccountChange, ["name", "idk"], 0, "");

    if(operation == UPDATE && facilityId != ""){
        getFacilityInfo();
    }
    getAjaxObject(ipAddress + "/SMS/list?is-active=1", "GET", getSMSValueList, onError);
}
function onBillAccountChange(){
    var cmbBilling = $("#cmbBilling").data("kendoComboBox");
    if(cmbBilling && cmbBilling.selectedIndex<0){
        cmbBilling.select(0);
    }
}

function getSMSValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbSMS", onSMSChange, ["desc", "idk"], 0, "");
        setDataForSelection(dArray, "AddcmbSMS", onAddSMSChange, ["desc", "idk"], 0, "");
    }
    if (operation == UPDATE && selItem) {
        $("#btnReset").hide();
        getContractInfo();
        var cmbStatus = $("#cmbStatus").data("kendoComboBox");
        if (cmbStatus) {
            if (selItem.isActive == 1) {
                cmbStatus.select(0);
            } else {
                cmbStatus.select(1);
            }
        }
    } else {
        var cmbStatus = $("#cmbStatus").data("kendoComboBox");
        if (cmbStatus) {
            cmbStatus.enable(false);
        }
    }
    //$("#btnSave").removeAttr('disabled',false);
    //$("#btnZipSearch").kendoWindowWrapper("close");
    //getAjaxObject(ipAddress+"/Language/list/","GET",getLanguageValueList,onError);
}
/*function getLanguageValueList(dataObj){
	var dArray = getTableListArray(dataObj);
	if(dArray && dArray.length>0){
		setDataForSelection(dArray, "cmbLan", onLanChange, ["desc", "idk"], 0, "");
	}
	getAjaxObject(ipAddress+"/Race/list/","GET",getRaceValueList,onError);
}
function getRaceValueList(dataObj){
	var dArray = getTableListArray(dataObj);
	if(dArray && dArray.length>0){
		setDataForSelection(dArray, "cmbRace", onRaceChange, ["desc", "idk"], 0, "");
	}
	getAjaxObject(ipAddress+"/Ethnicity/list/","GET",getEthnicityValueList,onError);
}
function getEthnicityValueList(dataObj){
	var dArray = getTableListArray(dataObj);
	if(dArray && dArray.length>0){
		setDataForSelection(dArray, "cmbEthicity", onEthnicityChange, ["desc", "idk"], 0, "");
	}

	if(operation == UPDATE && patientId != ""){
		getPatientInfo();
	}
}*/
function getContractInfo() {
    getAjaxObject(ipAddress + "/contract/" + selItem.idk, "GET", onGetContractInfo, onError);
}

function getComboListIndex(cmbId, attr, attrVal) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb) {
        var ds = cmb.dataSource;
        var totalRec = ds.total();
        for (var i = 0; i < totalRec; i++) {
            var dtItem = ds.at(i);
            if (dtItem && dtItem[attr] == attrVal) {
                cmb.select(i);
                return i;
            }
        }
    }
    return -1;
}

function onGetContractInfo(dataObj) {
    //patientInfoObject = dataObj;
    if (dataObj && dataObj.response && dataObj.response.contract) {
        communicationId = dataObj.response.contract.communicationId;
        contactComunicationId = dataObj.response.contract.contactComunicationId;
        $("#txtID").val(dataObj.response.contract.id);
        $("#txtExtID1").val(dataObj.response.contract.externalId1);
        $("#txtExtID2").val(dataObj.response.contract.externalId2);
        getComboListIndex("cmbPrefix", "value", dataObj.response.contract.prefix);
        $("#txtFN").val(dataObj.response.contract.contactFirstName);
        $("#txtLN").val(dataObj.response.contract.contactLastName);
        $("#txtMN").val(dataObj.response.contract.contactMiddleName);
        //$("#txtWeight").val(dataObj.response.patient.weight);
        //$("#txtHeight").val(dataObj.response.patient.height);
        $("#txtNN").val(dataObj.response.contract.contactNickName);
        $("#txtAbbreviation").val(dataObj.response.contract.abbreviation);
        $("#txtN").val(dataObj.response.contract.name);
        $("#txtDN").val(dataObj.response.contract.displayName);
        /* $("#txtFTI").val(dataObj.response.contract.federalTaxId);
         $("#txtFTEIN").val(dataObj.response.contract.federalTaxEin);
         $("#txtSTI").val(dataObj.response.contract.stateTaxId);
         $("#txtNOTT").val(dataObj.response.contract.taxName);
         $("#txtNPI").val(dataObj.response.contract.npi);
         $("#txtTI").val(dataObj.response.contract.taxonomyId);*/
        $("#txtAdd1").val(dataObj.response.contract.address1);
        $("#txtWPhone").val(dataObj.response.contract.workPhone);
        $("#txtExtension").val(dataObj.response.contract.workPhoneExt);
        $("#txtFax").val(dataObj.response.contract.fax);
        $("#txtCell").val(dataObj.response.contract.cellPhone);
        $("#txtAdd2").val(dataObj.response.contract.address2);
        $("#txtEmail").val(dataObj.response.contract.email);
        /*$("#txtEmail1").val(dataObj.response.contract.email);*/
        /*var dt = new Date(dataObj.response.patient.dateOfBirth);
        if(dt){
        	var strDT = kendo.toString(dt,"MM/dd/yyyy");
        	var dtDOB = $("#dtDOB").data("kendoDatePicker");
        	if(dtDOB){
        		dtDOB.value(strDT);
        	}
        }*/
        //$("#txtSSN").val(dataObj.response.patient.ssn);

        /*getComboListIndex("cmbFTIT", "desc", dataObj.response.contract.federalTaxIdType);*/
        getComboListIndex("cmbStatus", "Value", dataObj.response.contract.status);
        getComboListIndex("cmbPrefix", "desc", dataObj.response.contract.contactPrefix);
        getComboListIndex("cmbBilling","idk",dataObj.response.contract.billingAccountId);

        //getComboListIndex("cmbGender","desc",dataObj.response.patient.gender);
        //getComboListIndex("cmbEthicity","desc",dataObj.response.patient.ethnicity);
        //getComboListIndex("cmbRace","desc",dataObj.response.patient.race);
        //getComboListIndex("cmbLan","desc",dataObj.response.patient.language);

        if (dataObj && dataObj.response && dataObj.response.communication) {

            var commArray = [];
            if ($.isArray(dataObj.response.communication)) {
                commArray = dataObj.response.communication;
            } else {
                commArray.push(dataObj.response.communication);
            }
            var comObj = commArray[0];
            commId = comObj.id;
            $("#txtAdd1").val(comObj.address1);
            $("#txtAdd2").val(comObj.address2);
            //	$("#txtSMS").val(comObj.sms);
            getComboListIndex("cmbSMS", "desc", comObj.sms);
            getComboListIndex("cmbZip", "idk", comObj.cityId);
            onZipChange();
            $("#txtCell").val(comObj.cellPhone);
            $("#txtExtension").val(comObj.workPhoneExt);
            $("#txtWPhone").val(comObj.workPhone);
            $("#txtExtension1").val(comObj.homePhoneExt);
            $("#txtHPhone").val(comObj.homePhone);
            $("#txtEmail").val(comObj.email);
            $("#txtCountry").val(comObj.country);
            $("#cmbZip").val(comObj.zip);
            $("#txtZip4").val(comObj.zipFour);
            $("#txtState").val(comObj.state);
            $("#txtCity").val(comObj.city);

            /*if(comObj.defaultCommunication == "1"){
            	$("#rdHome").prop("checked",true);
            }else if(comObj.defaultCommunication == "2"){
            	$("#rdWork").prop("checked",true);
            }else if(comObj.defaultCommunication == "3"){
            	$("#rdCell").prop("checked",true);
            }else if(comObj.defaultCommunication == "4"){
            	$("#rdEmail").prop("checked",true);
            }*/
        }

    }
}

function getTableListArray(dataObj) {
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.codeTable) {
        if ($.isArray(dataObj.response.codeTable)) {
            dataArray = dataObj.response.codeTable;
        } else {
            dataArray.push(dataObj.response.codeTable);
        }
    }
    var tempDataArry = [];
    var obj = {};
    obj.desc = "";
    obj.zip = "";
    obj.value = "";
    obj.idk = "";
    tempDataArry.push(obj);
    for (var i = 0; i < dataArray.length; i++) {
        if (dataArray[i].isActive == 1) {
            var obj = dataArray[i];
            obj.idk = dataArray[i].id;
            obj.status = dataArray[i].Status;
            tempDataArry.push(obj);
        }
    }
    return tempDataArry;
}
function getTableFirstListArray(dataObj) {
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.codeTable) {
        if ($.isArray(dataObj.response.codeTable)) {
            dataArray = dataObj.response.codeTable;
        } else {
            dataArray.push(dataObj.response.codeTable);
        }
    }
    var tempDataArry = [];
    for (var i = 0; i < dataArray.length; i++) {
        if (dataArray[i].isActive == 1) {
            var obj = dataArray[i];
            obj.idk = dataArray[i].id;
            obj.status = dataArray[i].Status;
            tempDataArry.push(obj);
        }
    }
    return tempDataArry;
}

function onClickReset() {
    if(operation == ADD) {
        operation = ADD;
    }
    else {
        operation = UPDATE;
    }
    //patientId = "";
    $("#txtID").val("");
    $("#txtExtID1").val("");
    $("#txtExtID2").val("");
    $("#txtAbbreviation").val("");
    $("#txtN").val("");
    $("#txtDN").val("");
    $("#txtFTI").val("");
    $("#txtFTEIN").val("");
    $("#txtSTI").val("");
    $("#txtNOTT").val("");
    $("#txtNPI").val("");
    $("#txtTI").val("");
    $("#txtNPI").val("");
    setComboReset("cmbPrefix");
    setComboReset("cmbSuffix");
    $("#txtFN").val("");
    $("#txtLN").val("");
    $("#txtMN").val("");
    /*$("#txtWeight").val("");
    $("#txtHeight").val("");
    $("#txtNN").val("");
    	var dtDOB = $("#dtDOB").data("kendoDatePicker");
    	if(dtDOB){
    		dtDOB.value("");
    	}*/
    $("#txtCity").val("");
    $("#txtState").val("");
    $("#txtCountry").val("");
    $("#txtZip4").val("");


    setComboReset("cmbStatus");
    //setComboReset("cmbZip");
    setComboReset("cmbFTIT");
    setComboReset("cmbBilling");
    /*setComboReset("cmbEthicity");
    setComboReset("cmbRace");
    setComboReset("cmbLan");*/
    $("#txtAdd1").val("");
    $("#txtAdd2").val("");
    setComboReset("cmbSMS");
    setComboReset("AddcmbSMS");
    $("#cmbZip").val("");
    $("#txtCell").val("");
    $("#txtNN").val("");
    $("#txtExtension").val("");
    $("#txtWPhone").val("");
    $("#txtExtension1").val("");
    $("#txtHPhone").val("");
    $("#txtHPhone1").val("");
    $("#txtEmail").val("");
    $("#txtFax").val("");
    $("#txtCell1").val("");
    $("#txtEmail1").val("");
    $("#AddtxtAdd1").val("");
    $("#AddtxtAdd2").val("");
    $("#AddcmbZip").val("");
    $("#AddtxtCity").val("");
    $("#AddtxtState").val("");
    $("#AddtxtCountry").val("");
    $("#AddtxtZip4").val("");

    $("#rdHome").prop("checked", true);

}

function setComboReset(cmbId) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb) {
        cmb.select(0);
    }
}

function validation() {
    var flag = true;
    var strAbbr = $("#txtAbbreviation").val();
    strAbbr = $.trim(strAbbr);
    if (strAbbr == "") {
        customAlert.error("Error", "Enter Abbrevation");
        flag = false;
        return false;
    }
    var strName = $("#txtN").val();
    strName = $.trim(strName);
    if (strName == "") {
        customAlert.error("Error", "Enter Name");
        flag = false;
        return false;
    }
    var strDName = $("#txtDN").val();
    strDName = $.trim(strDName);
    if (strDName == "") {
        customAlert.error("Error", "Enter Display Name");
        flag = false;
        return false;
    }

    return flag;
}

function getComboDataItem(cmbId) {
    var dItem = null;
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb && cmb.dataItem()) {
        dItem = cmb.dataItem();
        return cmb.text();
    }
    return "";
}

function getComboDataItemValue(cmbId) {
    var dItem = null;
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb && cmb.dataItem()) {
        dItem = cmb.dataItem();
        return cmb.value();
    }
    return "";
}

function onClickSave(e) {
    /*if (validation()) {*/
    e.preventDefault();
    $('.alert').remove();
    var strId = $("#txtID").val();
    var strStatus = getComboDataItem("cmbStatus");

    var strSSN = $("#txtSSN").val();
    var strGender = getComboDataItem("cmbGender");
    var strAdd1 = $("#txtAdd1").val();
    var strAdd2 = $("#txtAdd2").val();
    var strCity = $("#txtCity").val();
    var strState = $("#txtState").val();
    var strCountry = $("#txtCountry").val();
    var strFax = $("#txtFax").val();

    //var strZip = getComboDataItemValue("cmbZip");
    var strZip4 = $("#txtZip4").val();
    var strHPhone = $("#txtHPhone").val();
    var strExt1 = $("#txtExtension1").val();
    var strWp = $("#txtWPhone").val();
    var strWpExt = $("#txtExtension").val();
    var strCell = $("#txtCell").val();

    /*var strSMS = getComboDataItem("cmbSMS");*/

    var strEmail = $("#txtEmail").val();

    var dataObj = {};
    //dataObj.id = strId;
    dataObj.createdBy = sessionStorage.userId;
    /*dataObj.isActive = 1;
    if (strStatus && statusArr == "ACTIVE") {
        dataObj.isActive = 1;
    }*/
    if(strStatus == "InActive") {
        dataObj.isActive = 0;
    }
    else if(strStatus == "Active") {
        dataObj.isActive = 1;
    }
    dataObj.isDeleted = 0;
    //dataObj.id = strId;
    var strExtId1 = $("#txtExtID1").val();
    var strExtId2 = $("#txtExtID2").val();

    dataObj.externalId1 = strExtId1;
    dataObj.externalId2 = strExtId2;

    var strAbbr = $("#txtAbbreviation").val();
    dataObj.abbreviation = strAbbr;

    var strName = $("#txtN").val();
    var strDName = $("#txtDN").val();

    dataObj.name = strName;
    dataObj.displayName = strDName;

    var cmbBilling = $("#cmbBilling").data("kendoComboBox");

    var billIdk = cmbBilling.value();
    billIdk = Number(billIdk);
    dataObj.billingAccountId = billIdk;

    /*var strFTIT = getComboDataItem("cmbFTIT");
    var cmbFTIT = $("#cmbFTIT").data("kendoComboBox");
    if (cmbFTIT) {
        strFTIT = cmbFTIT.text();
    }
    dataObj.federalTaxIdType = strFTIT; //strFTIT;

    var strFTI = $("#txtFTI").val();
    dataObj.federalTaxId = strFTI;

    var strFTEIN = $("#txtFTEIN").val();
    dataObj.federalTaxEin = strFTEIN;

    var strSTI = $("#txtSTI").val();
    dataObj.stateTaxId = strSTI;

    var strNOTT = $("#txtNOTT").val();
    dataObj.taxName = strNOTT;

    var strNPI = $("#txtNPI").val();
    dataObj.npi = strNPI;

    var strTI = $("#txtTI").val();
    dataObj.taxonomyId = strTI;*/

    var strPrefix = getComboDataItem("cmbPrefix");
    dataObj.contactPrefix = strPrefix;//$("#txtEmail").val();;
    var strSuffix = getComboDataItem("cmbSuffix");
    dataObj.contactSuffix = strSuffix;
    //dataObj.communicationId = null; //communicationId;

    var strNickName = $("#txtNN").val();
    dataObj.contactNickName = strNickName;

    var strFN = $("#txtFN").val();
    var strMN = $("#txtMN").val();
    var strLN = $("#txtLN").val();

    dataObj.contactFirstName = strFN;
    dataObj.contactMiddleName = strMN;
    dataObj.contactLastName = strLN;
    /*dataObj.sms = getComboDataItem("cmbSMS");*/

    //dataObj.contactComunicationId = null; //contactComunicationId;

    var comm1 = {};
    var comm2 = {};
    /*comm1.country = $("#txtCountry").val();
    comm1.city = $("#txtCity").val();
    comm1.state = $("#txtState").val();*/
    /*var cmbZip = $("#cmbZip").data("kendoComboBox");*/
    var cmbZip = $("#cmbZip").val();
    var cityId = "";
    var zip = "";
    if (cmbZip) {
        cityId = zipSelItem.cityId || zipSelItem.idk;
    }
    /*comm1.zip = cmbZip;*/
    comm1.defaultCommunication = 1;
    comm1.cityId = cityId;
    if(strStatus == "InActive") {
        comm1.isActive = 0;
    }
    else if(strStatus == "Active") {
        comm1.isActive = 1;
    }
    comm1.isDeleted = 0;
    /*comm1.countryId = 1;*/
    /*comm1.zipFour = $("#txtZip4").val();*/
    comm1.address1 = $("#txtAdd1").val();
    comm1.address2 = $("#txtAdd2").val();
    /*comm1.state = $("#txtState").val();*/

    comm1.sms = getComboDataItem("cmbSMS");

    comm1.workPhone = $("#txtWPhone").val();
    comm1.workPhoneExt = strWpExt; //$("#txtExtension").val();
    comm1.fax = strFax;
    comm1.cellPhone = $("#txtCell").val();
    comm1.email = $("#txtEmail").val();
    comm1.homePhone = $("#txtHPhone").val();
    comm1.parentTypeId = 600;
    /*comm1.homePhoneExt = null;*/
    /*comm1.stateId = 1;
    comm1.areaCode = null;*/
    /*if (operation == UPDATE) {
        comm1.modifiedBy = sessionStorage.userId;
        //comm1.modifiedDate = new Date().getTime();
    } else {
        comm1.createdBy = Number(sessionStorage.userId);
        //comm1.createdDate = new Date().getTime();
    }*/
    comm2.defaultCommunication = 1;
    var cmbZip = $("#AddcmbZip").data("kendoComboBox");
    /* comm2.zipFour = $("#AddtxtZip4").val();
     comm2.country = $("#AddcmbZip").val();*/
    if($("#AddcmbZip").val() != "") {
        $("#AddcmbZip").data("kendoComboBox");
        comm2.cityId = AddzipSelItem.cityId || AddzipSelItem.idk;
    }
    comm2.address1 = $("#AddtxtAdd1").val();
    comm2.address2 = $("#AddtxtAdd2").val();
    /*comm2.state = $("#AddtxtState").val();*/
    comm2.isDeleted = 0;
    if(strStatus == "InActive") {
        comm2.isActive = 0;
    }
    else if(strStatus == "Active") {
        comm2.isActive = 1;
    }
    /*comm2.countryId = 12;
    comm2.sms = "yes";
    comm2.defaultCommunication = 1;*/
    /*comm2.workPhone = "121212"; // $("#txtWPhone").val();
    comm2.workPhoneExt = "12333"; //$("#txtExtension").val();*/
    //comm2.fax = ""
    comm2.cellPhone = $("#txtCell1").val();
    comm2.email = $("#txtEmail1").val();
    //comm2.createdDate = new Date().getTime();
    comm2.homePhone = $("#txtHPhone1").val();
    /*comm2.homePhoneExt = $("#txtExtension1").val();*/
    /*comm2.stateId = 1;*/
    /*comm2.areaCode = null;*/
    comm2.parentTypeId = 601;
    comm2.sms = getComboDataItem("AddcmbSMS");

    /*if (operation == UPDATE) {
        comm2.modifiedBy = sessionStorage.userId;
        comm2.parentId = selAccountItem.idk;
        if(selAccountItem.communications.id) {
            comm2.id = selAccountItem.communications.id;
        }
        //comm2.modifiedDate = new Date().getTime();
    } else {
        comm2.createdBy = sessionStorage.userId;
        //comm2.createdDate = new Date().getTime();
    }*/

    if (operation == UPDATE) {
        var communicationLen = selAccountItem.communications != null ? selAccountItem.communications.length : 0;
        var cityIndexVal = "";
        var dupCityIndexVal = "";
        for(var i=0; i<communicationLen; i++) {
            if(selAccountItem.communications[i].parentTypeId == 600) {
                cityIndexVal = i;
            }
            if(selAccountItem.communications[i].parentTypeId == 601) {
                dupCityIndexVal = i;
            }
        }
        if(cityIndexVal != "" || (cityIndexVal == 0 && selAccountItem.communications != null)) {
            comm1.id = selAccountItem.communications[cityIndexVal].id;
            comm1.modifiedBy = sessionStorage.userId;
            comm1.parentId = selAccountItem.idk;
        }
        if(dupCityIndexVal != "") {
            comm2.id = selAccountItem.communications[dupCityIndexVal].id;
            comm2.modifiedBy = sessionStorage.userId;
            comm2.parentId = selAccountItem.idk;
        }
        //comm1.modifiedDate = new Date().getTime();
    }
    else {
        comm1.createdBy = Number(sessionStorage.userId);
    }

    var comm = [];
    comm.push(comm1);
    if($("#txtEmail1").val() != "" || $("#txtCell1").val() != "" || $("#txtHPhone1").val() != "") {
        if($("#AddcmbZip").val() != "" && $("#AddtxtAdd1").val() != "") {
            if($("#txtEmail1").val() != "" && !validateEmail($("#txtEmail1").val())) {
                $("body").animate({
                    scrollTop: 0
                }, 500);
                $('.customAlert').append('<div class="alert alert-danger">Please enter valid email address of Contact Person</div>');
                return false;
            }
            else {
                comm2.createdBy = Number(sessionStorage.userId);
                comm.push(comm2);
            }
        }
        else {
            $("body").animate({
                scrollTop: 0
            }, 500);
            if(sessionStorage.countryName.indexOf("India") >= 0 || sessionStorage.countryName.indexOf("United Kingdom") >= 0) {
                $('.customAlert').append('<div class="alert alert-danger">Please fill Address 1 and Postal Code of Contact Person</div>');
            }
            else {
                $('.customAlert').append('<div class="alert alert-danger">Please fill Address 1 and Zip of Contact Person</div>');
            }
            return false;
        }
    }
    else if($("#AddcmbZip").val() != "" || $("#AddtxtAdd1").val() != "") {
        if($("#txtEmail1").val() == "" || $("#txtCell1").val() == "" || $("#txtHPhone1").val() == "") {
            $("body").animate({
                scrollTop: 0
            }, 500);
            $('.customAlert').append('<div class="alert alert-danger">Please enter Email or Homephone or Mobile Phone of Contact Person</div>');
            return false;
        }
    }

    dataObj.communications = comm;

    if (operation == UPDATE) {

        //dataObj.id = commId;
        //dataObj.isDeleted = "0";
        dataObj.id = strId;
        dataObj.modifiedBy = sessionStorage.userId;
        //dataObj.modifiedDate = new Date().getTime();
    }

    console.log(dataObj);
    if(strStatus != "" && strAbbr != "" && strName != "" && strDName != "" && $("#cmbZip").val() != "" && strWp != "" && strEmail != "" && strAdd1 != "" && cmbBilling.value() != "") {
        if(!validateEmail(strEmail)) {
            $("body").animate({
                scrollTop: 0
            }, 500);
            $('.customAlert').append('<div class="alert alert-danger">Please enter valid Email Address</div>');
        }
        else {
            if (operation == ADD) {
                var dataUrl = ipAddress + "/contract/create";
                createAjaxObject(dataUrl, dataObj, "POST", onCreate, onError);
            } else if (operation == UPDATE) {
                var dataUrl = ipAddress + "/contract/update";
                createAjaxObject(dataUrl, dataObj, "POST", onUpdate, onError);
            }
        }
    }
    else {
        $("body").animate({
            scrollTop: 0
        }, 500);
        $('.customAlert').append('<div class="alert alert-danger">Please fill all the Required Fields</div>');
    }
    /*}*/
}
function onStatusChange() {
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if (cmbStatus && cmbStatus.selectedIndex < 0) {
        cmbStatus.select(0);
    }
}
function onFilterChange() {
    var cmbFilterByName = $("#cmbFilterByName").data("kendoComboBox");
    if (cmbFilterByName && cmbFilterByName.selectedIndex < 0) {
        cmbFilterByName.select(0);
    }
    if($("#cmbFilterByName").val() == "id" || $("#cmbFilterByName").val() == "billing-account-id") {
        $("#cmbFilterByValue").removeAttr("disabled");
        $("#cmbFilterByValue").attr("type","number");
    }
    else if($("#cmbFilterByName").val() == "All") {
        $("#cmbFilterByValue").attr("disabled","disabled");
        $("#accountFilter-btn").attr("disabled","disabled");
        $("#cmbFilterByValue").val('');
    }
    else {
        $("#cmbFilterByValue").removeAttr("disabled");
        $("#cmbFilterByValue").attr("type","text");
    }
}

function onCreate(dataObj) {
    console.log(dataObj);
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            /*var obj = {};
            obj.status = "success";
            obj.operation = operation;
            popupClose(obj);*/
            $('.customAlert').append('<div class="alert alert-success">'+dataObj.response.status.message+'</div>');
            $('#btnReset').trigger('click');
            $('.tabContentTitle').text('Add Contract');
        } else {
            $('.customAlert').append('<div class="alert alert-danger">'+dataObj.response.status.message+'</div>');
            /*customAlert.error("error", dataObj.response.status.message);*/
        }
    }
}
function onUpdate(dataObj) {
    console.log(dataObj);
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            /*var obj = {};
            obj.status = "success";
            obj.operation = operation;
            popupClose(obj);*/
            $('.customAlert').append('<div class="alert alert-success">'+dataObj.response.status.message+'</div>');
            $('.tabContentTitle').text('Update Contract');
            var id = $('#txtID').val();
            getAjaxObject(ipAddress+"/contract/list?id="+id,"GET",updateList,onError);
        } else {
            $('.customAlert').append('<div class="alert alert-danger">'+dataObj.response.status.message+'</div>');
            /*customAlert.error("error", dataObj.response.status.message);*/
        }
    }
}
function updateList(dataObj) {
    console.log(dataObj);
    selAccountItem = dataObj.response.contract[0];
    selAccountItem.idk = selAccountItem.id;
    addAccount("update");
}

function onError(errObj) {
    console.log(errObj);
    customAlert.error("Error", "Error");
}
/*function onClickCancel() {
    $('.listDataWrapper').show();
    $('.addOrRemoveWrapper').hide();
    var obj = {};
    obj.status = "false";
    popupClose(obj);
    //window.close();
}*/

function onClickSearch() {
    /*var obj = {};
    obj.status = "search";
    popupClose(obj);*/
}
function validateEmail(sEmail) {
    console.log(sEmail);
    var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    if (filter.test(sEmail)) {
        return true;
    }
    else {
        return false;
    }
}

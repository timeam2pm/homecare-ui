var angularUIgridWrapper = null;
var parentRef = null;
var IsPostalCodeManual = sessionStorage.IsPostalCodeManual;

var  stArray = [{key:'ACTIVE',text:'ACTIVE'},{key:'INACTIVE',text:'INACTIVE'},{key:'BOTH',text:'BOTH'}];
var searchData = [{key:'1',text:'All'},{key:'2',text:'Staff Id'},{key:'3',text:'First Name'},{key:'4',text:'Last Name'},{key:'5',text:'First Name, Last Name'}, {key:'6',text:'Last Name, First Name'}];

var ADD = "add";
var operation = "";
var patientListURL = "";
var pageName;

$(document).ready(function(){
    sessionStorage.setItem("IsSearchPanel", "1");
    pageName = localStorage.getItem("pageName");
    if(pageName != "form") {
        $("#btnSUAdd").css("display","none");
    }
    else{
        $("#btnSUAdd").css("display","");
    }
    themeAPIChange();
    $("#cmbStatus").kendoComboBox();
    $("#txtPatientId").kendoComboBox();

    $("#cmbStatus").data("kendoComboBox").input.attr("placeholder", "Status");
    $("#txtPatientId").data("kendoComboBox").input.attr("placeholder", "Search Parameters");

    setDataForSelection(stArray, "cmbStatus", onStatusChange, ["text", "key"], 0, "");
    setDataForSelection(searchData, "txtPatientId", onPTChange, ["text", "key"], 0, "");

    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("dgridZipList", dataOptions);
    angularUIgridWrapper.init();
    buildZipListGrid([]);
});

function onStatusChange(){
    var cmbZip = $("#cmbZip").data("kendoComboBox");
    if(cmbZip && cmbZip.selectedIndex<0){
        cmbZip.select(0);
    }
}
function onPTChange(){
    var txtPatientId = $("#txtPatientId").data("kendoComboBox");
    if(txtPatientId && txtPatientId.selectedIndex<0){
        txtPatientId.select(0);
    }
}

$(window).load(function(){
    loading = false;
    if(parent.frames['iframe'] != undefined)
        parentRef = parent.frames['iframe'].window;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
    // if(parentRef && parentRef.searchZip == true){
    $("#btnDelete").hide();
    //$("#btnAdd").hide();
    $("#btnSave").text("OK");
    $("#btnInActive").hide();
    $("#btnActive").hide();
    // }
    init();
    buttonEvents();
    adjustHeight();
}

function init(){
    $("#btnSave").prop("disabled", true);
    $("#btnDelete").prop("disabled", true);
    buildZipListGrid([]);
    https://stage.timeam.com/provider/list/?is-active=1&access_token=2d701d52-7c68-4acc-9884-5dfa302da75f&_=1547657354283
        getAjaxObject(ipAddress+"/provider/list/?is-active=1&is-deleted=0","GET",getStaffList,onError);
}
function onError(errorObj){
    console.log(errorObj);
}
function getStaffList(dataObj){
    console.log(dataObj);

    var tempArray = [];
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.provider){
            if($.isArray(dataObj.response.provider)){
                tempArray = dataObj.response.provider;
            }else{
                tempArray.push(dataObj.response.provider);
            }
        }
    }else{
        customAlert.error("Error",dataObj.response.status.message);
    }

    for(var i=0;i<tempArray.length;i++){
        var obj = tempArray[i];
        if(obj){
            var dataItemObj = obj;
            dataItemObj.PID = obj.id;
            dataItemObj.firstName = obj.firstName;
            dataItemObj.lastName = obj.lastName;
            dataItemObj.weight = obj.weight;
            dataItemObj.height = obj.height;
            var dt = new Date(obj.dateOfBirth);
            dt = kendo.toString(dt, "dd-MM-yyyy");
            dataItemObj.dateOfBirth = dt;
            if(obj.middleName){
                dataItemObj.middleName = obj.middleName;
            }else{
                dataItemObj.middleName =  "";
            }
            dataItemObj.gender = obj.gender;
            if(obj.status){
                dataItemObj.status = obj.status;
            }else{
                dataItemObj.status = "ACTIVE";
            }
            dataArray.push(dataItemObj);
        }
    }
    //setDataForSelection(dataArray, "cmbPatients", function(){}, ["FN", "PID"], 0, "");

    buildZipListGrid(dataArray);
}
function buttonEvents(){

    $("#btnSave").off("click",onClickOK);
    $("#btnSave").on("click",onClickOK);

    $("#btnCancelDet").off("click",onClickCancel);
    $("#btnCancelDet").on("click",onClickCancel);

    $("#btnDelete").off("click",onClickDelete);
    $("#btnDelete").on("click",onClickDelete);

    $("#btnActive").off("click");
    $("#btnActive").on("click",onClickActive);

    $("#btnInActive").off("click");
    $("#btnInActive").on("click",onClickInActive);

    $("#btnZipSearch").off("click");
    $("#btnZipSearch").on("click",onClickSearch);

}

function onClickAddPatient() {
    var myIframe = parent.frames["ptframe"];
    if (myIframe && myIframe.contentWindow) {
        myIframe.contentWindow.onClickAddPatient();
    }
}

function onClickAddPatient(){
    operation = ADD;
    addPatient();
}


function addPatient(){
    var popW = "75%";
    var popH = "72%";

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Add Staff";
    // parentRef.operation = operation;
    devModelWindowWrapper.openPageWindow("../../html/masters/createDoctor.html", profileLbl, popW, popH, true, closeAddPatientAction);
}

function closeAddPatientAction(evt,returnData) {
    if (returnData && returnData.status == "success") {
        getPatientList();
    }
}

function adjustHeight(){
    var defHeight = 220;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    angularUIgridWrapper.adjustGridHeight(cmpHeight);
}

var isActive = 1;
function onClickActive(){
    isActive = 1;
    $("#btnInActive").removeClass("selectButtonBarClass");
    $("#btnActive").addClass("selectButtonBarClass");
    init();
}
function onClickInActive(){
    isActive = 0;
    $("#btnActive").removeClass("selectButtonBarClass");
    $("#btnInActive").addClass("selectButtonBarClass");
    init();
}
function buildZipListGrid(dataSource) {
    console.log(dataSource);
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    var cntry = sessionStorage.countryName;

    gridColumns.push({
        "title": "Id",
        "field": "PID",
    });

    gridColumns.push({
        "title": "First Name",
        "field": "firstName",
    });


    gridColumns.push({
        "title": "Last Name",
        "field": "lastName",
    });
    gridColumns.push({
        "title": "DOB",
        "field": "dateOfBirth",
    });
    gridColumns.push({
        "title": "Gender",
        "field": "gender",
    });
    // gridColumns.push({
    //    "title": "Status",
    //    "field": "Status",
    // });
    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}
var prevSelectedItem =[];
function onChange(){
    setTimeout(function(){
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if(selectedItems && selectedItems.length>0){
            $("#btnSave").prop("disabled", false);
            $("#btnDelete").prop("disabled", false);
        }else{
            $("#btnSave").prop("disabled", true);
            $("#btnDelete").prop("disabled", true);
        }
    });
}

function onClickOK(){
    setTimeout(function(){
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if(selectedItems && selectedItems.length>0){
            var obj = {};
            obj.selItem = selectedItems[0];
            if($("#btnSave").text() ==  "OK"){
                obj.status = "success";
                var windowWrapper = new kendoWindowWrapper();
                windowWrapper.closePageWindow(obj);
            }else{
                operation = "update";
                parentRef.operation = operation;
                parentRef.selItem = selectedItems[0];
                addZip();
            }

        }
    })
}
function onClickDelete(){
    customAlert.confirm("Confirm", "Are you sure you want delete?",function(response){
        if(response.button == "Yes"){
            var selectedItems = angularUIgridWrapper.getSelectedRows();
            console.log(selectedItems);
            if(selectedItems && selectedItems.length>0){
                var selItem = selectedItems[0];
                if(selItem){
                    var dataUrl = ipAddress+"/city/delete/";
                    var reqObj = {};
                    reqObj.id = selItem.idk;
                    reqObj.isDeleted = "0";
                    reqObj.isActive = "0";
                    reqObj.modifiedBy = sessionStorage.userId;//"101";
                    createAjaxObject(dataUrl,reqObj,"POST",onDeleteCountryt,onError);
                }
            }
        }
    });
}
function onDeleteCountryt(dataObj){
    if(dataObj && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            customAlert.info("info", "Deleted Successfully");
            init();
        }else{
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}

var operation = "";

function onClickCancel(){
    var obj = {};
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}


function onClickSearch(){
    var patientListURL;
    buildZipListGrid([]);
    var strSearch = $("#txtSearch").val();
    strSearch = $.trim(strSearch);
    if(strSearch != ""){
        var txtPatientId = $("#txtPatientId").data("kendoComboBox");
        if(txtPatientId){
            var idx = txtPatientId.value();
            if(idx == 1){
                getPatientList();
            }else if(idx == 2){
                patientListURL = ipAddress+"/provider/list/?id="+strSearch;
                getAjaxObject(patientListURL,"GET",getStaffList,onErrorMedication);
            }else if(idx == 3){
                patientListURL = ipAddress+"/provider/list/?first-name="+strSearch;
                getAjaxObject(patientListURL,"GET",getStaffList,onErrorMedication);
            }else if(idx == 4){
                patientListURL = ipAddress+"/provider/list/?last-name="+strSearch;
                getAjaxObject(patientListURL,"GET",getStaffList,onErrorMedication);
            }else if(idx == 5){
                var pArray = strSearch.split(",");
                if(pArray){
                    if(pArray.length == 2){
                        var fName = pArray[0];
                        var lName = pArray[1];
                        if(fName && lName){
                            patientListURL = ipAddress+"/provider/list/?first-name="+fName+"&last-name="+lName;
                            getAjaxObject(patientListURL,"GET",getStaffList,onErrorMedication);
                        }else if(fName){
                            patientListURL = ipAddress+"/provider/list/?first-name="+fName;
                            getAjaxObject(patientListURL,"GET",getStaffList,onErrorMedication);
                        }else if(lName){
                            patientListURL = ipAddress+"/provider/list/?last-name="+lName;
                            getAjaxObject(patientListURL,"GET",getStaffList,onErrorMedication);
                        }
                    }else if(pArray.length == 1){
                        patientListURL = ipAddress+"/provider/list/?first-name="+strSearch;
                        getAjaxObject(patientListURL,"GET",getStaffList,onErrorMedication);
                    }
                }
            }
            else if(idx == 6){
                var pArray = strSearch.split(",");
                if(pArray){
                    if(pArray.length == 2){
                        var lName = pArray[0];
                        var fName = pArray[1];
                        if(fName && lName){
                            patientListURL = ipAddress+"/provider/list/?first-name="+fName+"&last-name="+lName;
                            getAjaxObject(patientListURL,"GET",getStaffList,onErrorMedication);
                        }else if(fName){
                            patientListURL = ipAddress+"/provider/list/?first-name="+fName;
                            getAjaxObject(patientListURL,"GET",getStaffList,onErrorMedication);
                        }else if(lName){
                            patientListURL = ipAddress+"/provider/list/?last-name="+lName;
                            getAjaxObject(patientListURL,"GET",getStaffList,onErrorMedication);
                        }
                    }else if(pArray.length == 1){
                        patientListURL = ipAddress+"/provider/list/?last-name="+strSearch;
                        getAjaxObject(patientListURL,"GET",getStaffList,onErrorMedication);
                    }
                }
            }
        }
    }else{
        getPatientList();
    }
}

function onErrorMedication(errobj){
    console.log(errobj);
}


function themeAPIChange(){
    getAjaxObject(ipAddress+"/homecare/settings/?id=2","GET",getThemeValue,onError);
}

function getThemeValue(dataObj){
    console.log(dataObj);
    var tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.activityTypes){
            if($.isArray(dataObj.response.settings)){
                tempCompType = dataObj.response.settings;
            }else{
                tempCompType.push(dataObj.response.settings);
            }
        }
        if(tempCompType.length > 0){
            themesChange(tempCompType[0].value);
        }
    }
}

function themesChange(themeValue){
    if(themeValue == 2){
        loadAPi("../../Theme2/Theme02.css");
    }
    else if(themeValue == 3){
        loadAPi("../../Theme3/Theme03.css");
    }
}
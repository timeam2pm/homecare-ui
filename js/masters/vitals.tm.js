var angularUIgridWrapper = null;
var normUIgrid = null;
var tradeNameUIgrid = null;
var windowLoad = false;
var selectedItems;
var activeListFlag = true;
var fetchTradeDetails = false;
function bindEvents() {
    $('.sidebar-nav-link').on('click',function() {
        $(this).closest('li').addClass('activeList').siblings().removeClass('activeList');
        $('.vitalsForm-form')[0].reset();
        $('.hasError').hide();
        $('.alert').remove();
        $('#vitals-save').show();
        $('#vitals-reset').show();
        $('#vitals-update').hide();
        $('#vitals-cancel').hide();
        $('[data-medicate-list="1"]').find('.tablist-link').text("Add");
        $('.tableListbtn-wrapper').find('.vitals-btn-link').attr('disabled','disabled');
        $('.vitalsForm-form').find('option').each(function() {
            if($(this).attr('data-medicate-status') == 1) {
                $(this).attr('selected','selected');
            }
            else {
                $(this).removeAttr('selected');
            }
        });
        $('.non-rxNorm-form').show();
        $('.non-group-forms').hide();
        $('.group-forms').show();
        $('.rxNormWrapperTable').show();
        $('.rxNorm-form').hide();
        $('.rxNorm-filter-tab-content').hide();
        // $('.vitals-data-content').removeClass('hidevitalsData');
        if(windowLoad && $('.tab-list.active-tabList').attr('data-medicate-list') == "2") {
            searchOnLoad();
        }
    });
    $('.tablist-link').on('click',function(e) {
        e.preventDefault();
        $('.hasError').hide();
        $('.alert').remove();
        $('.tableListbtn-wrapper').find('.vitals-btn-link').attr('disabled','disabled');
        $('.tab-list').removeClass('active-tabList');
        var tablinkNum = $(this).closest('.tab-list').addClass('active-tabList').attr('data-medicate-list');
        $('[data-medicate-content]').hide();
        $('[data-medicate-content='+tablinkNum+']').show();
        $('.rxNormWrapperTable').show();
    });
    $('#vitals-save').on('click',function(e) {
        e.preventDefault();
        $('.hasError').hide();
        $('.alert').remove();
        var urlExtn = getvitalsForm() + '/create';
        var activeStatusVal = $('.vitalsForm-form:visible').find('option:selected').attr('data-medicate-status');
        var abbreviationVal = $('.vitalsForm-form:visible').find('input[name="abbreviation"]').val();
        var descriptionVal = $('.vitalsForm-form:visible').find('input[name="description"]').val();
        var dataObj = {
            "createdBy": sessionStorage.userId,
            "isActive": activeStatusVal,
            "value": abbreviationVal,
            "desc": descriptionVal,
            "note": null
        }
        if(abbreviationVal != "" && activeStatusVal != "" && descriptionVal != "") {
            var dataUrl = ipAddress+urlExtn;
            createAjaxObject(dataUrl,dataObj,"POST",onCreate,onError);
        }
        else {
            $('.hasError').show();
        }
    });
    $("#vitals-reset").on('click',function(e) {
        e.preventDefault();
        $('.vitalsForm-form')[0].reset();
    });
    $("#vitals-list-search").on('click',function(e) {
        e.preventDefault();
        if($(".btnActive").hasClass("selectButtonBarClass")) {
            $(".btnActive").trigger('click');
        }
        else if($(".btnInActive").hasClass("selectButtonBarClass")) {
            $(".btnInActive").trigger('click');
        }
    });
    $(".vitals-delete").on('click',function(e) {
        e.preventDefault();
        $('.alert').remove();
        var deleteItemObj = {
            "id": selectedItems[0].idDuplicate,
            "isActive": 0,
            "isDeleted": 1,
            "modifiedBy": sessionStorage.userId
        }
        var urlExtn = getvitalsForm() + '/delete';
        var dataUrl = ipAddress+urlExtn;
        createAjaxObject(dataUrl,deleteItemObj,"POST",onDeleteItem,onDeleteError);
    });
    $(".vitals-edit-tableList").on('click',function(e) {
        e.preventDefault();
        $('[data-medicate-list="1"]').find('.tablist-link').trigger('click');
        $('[data-medicate-list="1"]').find('.tablist-link').text("Edit");
        $('.vitalsForm-form').find('option').each(function() {
            if($(this).attr('data-medicate-status') == selectedItems[0].isActive) {
                $(this).attr('selected','selected');
            }
            else {
                $(this).removeAttr('selected');
            }
        });
        $('.vitalsForm-form').find('input[name="abbreviation"]').val(selectedItems[0].value);
        $('.vitalsForm-form').find('input[name="description"]').val(selectedItems[0].desc);
        $('#vitals-save').hide();
        $('#vitals-reset').hide();
        $('#vitals-update').show();
        $('#vitals-cancel').show();
    });
    $("#vitals-update").on('click',function(e) {
        e.preventDefault();
        $('.alert').remove();
        var activeStatusVal = $('.statusUser:visible').find('option:selected').attr('data-medicate-status');
        var abbreviationVal = $('.vitalsForm-form').find('input[name="abbreviation"]').val();
        var descriptionVal = $('.vitalsForm-form').find('input[name="description"]').val();
        var updateItemObj = {
            "id": selectedItems[0].idDuplicate,
            "modifiedBy": sessionStorage.userId,
            "isActive": activeStatusVal,
            "value": abbreviationVal,
            "desc": descriptionVal,
            "note": null
        }
        var urlExtn = getvitalsForm() + '/update';
        var dataUrl = ipAddress+urlExtn;
        createAjaxObject(dataUrl,updateItemObj,"POST",onUpdateItem,onUpdateError);
    });
    $('#vitals-cancel').on('click',function(e) {
        e.preventDefault();
        $('.vitalsForm-form')[0].reset();
        $('#vitals-save').show();
        $('#vitals-reset').show();
        $('#vitals-update').hide();
        $('#vitals-cancel').hide();
        $('[data-medicate-list="2"]').find('.tablist-link').trigger('click');
    });
    $(".btnActive").on("click", function(e) {
        e.preventDefault();
        activeListFlag = true;
        $('.alert').remove();
        $('.hasError-rxNorm').remove();
        $('.vitals-delete').show();
        var urlExtn = getvitalsForm() + '/list?is-active=1';
        searchOnLoad();
        onClickActive();
    });
    $(".btnInActive").on("click", function(e) {
        e.preventDefault();
        activeListFlag = false;
        $('.alert').remove();
        $('.hasError-rxNorm').remove();
        $('.vitals-delete').hide();
        var urlExtn = getvitalsForm() + '/list?is-active=0';
        buildvitalsListGrid([]);
        getAjaxObject(ipAddress+urlExtn,"GET",getSearchList,onSearchError);
        onClickInActive();
    });
}
function getvitalsForm() {
    var formUrl;
    formUrl = '/master/vitals';
    return formUrl;
}
function searchOnLoad() {
    buildvitalsListGrid([]);
    var urlExtn = getvitalsForm() + '/list?is-active=1';
    getAjaxObject(ipAddress+urlExtn,"GET",getSearchList,onSearchError);
}
function onCreate(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1") {
            $('.vitalsForm-form')[0].reset();
            $('.vitalsForm-form').prepend('<div class="alert alert-success">success</div>');
        }
        else {
            $('.vitalsForm-form')[0].reset();
            $('.vitalsForm-form').prepend('<div class="alert alert-danger">error</div>');
        }
    }
}
function onError(errObj){
    console.log(errObj);
    customAlert.error("Error","Error");
}
function onDeleteItem(respObj) {
    console.log(respObj);
    if(respObj && respObj.response && respObj.response.status){
        if(respObj.response.status.code == "1") {
            searchOnLoad();
            $('#vitalsList').prepend('<div class="alert alert-success">Deleted Successfully</div>');
            $('.tableListbtn-wrapper').find('.vitals-btn-link').each(function() {
                if(selectedItems && selectedItems.length>0){
                    $(this).attr('disabled','disabled');
                }
                else {
                    $(this).removeAttr('disabled');
                }
            });
        }
        else {
            $('#vitalsList').prepend('<div class="alert alert-danger">Error</div>');
        }
    }
}
function onDeleteError(errObj){
    console.log(errObj);
    customAlert.error("Error","Error");
}
function onUpdateItem(respObj) {
    console.log(respObj);
    if(respObj && respObj.response && respObj.response.status){
        if(respObj.response.status.code == "1") {
            $('.vitalsForm-form').prepend('<div class="alert alert-success">Updated Successfully</div>');
            $('.vitalsForm-form')[0].reset();
            $('#vitals-save').show();
            $('#vitals-reset').show();
            $('#vitals-update').hide();
            $('#vitals-cancel').hide();
            $('[data-medicate-list="1"]').find('.tablist-link').text("Add");
        }
        else {
            $('.vitalsForm-form').prepend('<div class="alert alert-danger">'+respObj.response.status.message+'</div>');
        }
    }
}
function onUpdateError(errObj){
    console.log(errObj);
    customAlert.error("Error","Error");
}
function addvitalsData() {
    $('.sidebar-nav-list:first-child').find('.sidebar-nav-link').trigger('click');
}
var dataActiveList=[], dataInActiveList=[], dataListArr = [];
function getSearchList(resp) {
    var dataArray, dataActiveList=[], dataInActiveList=[];
    if(resp && resp.response && resp.response.codeTable){
        if($.isArray(resp.response.codeTable)){
            dataArray = resp.response.codeTable;
        }else{
            dataArray.push(resp.response.codeTable);
        }
    }
    if(dataArray != undefined) {
        for(var i=0;i<dataArray.length;i++){
            dataArray[i].idDuplicate = dataArray[i].id;
            if(dataArray[i].isActive == 1){
                dataArray[i].Status = "Active";
                dataActiveList.push(dataArray[i]);
            }
            else {
                dataArray[i].Status = "InActive";
                dataInActiveList.push(dataArray[i]);
            }
        }
    }
    if(activeListFlag) {
        buildvitalsListGrid(dataActiveList);
    }
    else if(!activeListFlag) {
        buildvitalsListGrid(dataInActiveList);
    }
}
function onSearchError(err) {
    console.log(err);
    customAlert.error("Error","Error");
}
function buildvitalsListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Abbreviation",
        "field": "value",
    });
    gridColumns.push({
        "title": "Description",
        "field": "desc",
    });
    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
}
function adjustHeight(){
    var defHeight = 220;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
}

function onChange(){
    setTimeout(function() {
        selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        disableButtons();
    });
}
function disableButtons() {
    $('.tableListbtn-wrapper').find('.vitals-btn-link').each(function() {
        if(selectedItems && selectedItems.length>0){
            $(this).removeAttr('disabled');
        }
        else {
            $(this).attr('disabled','disabled');
        }
    });
}
function onClickActive() {
    $(".btnInActive").removeClass("selectButtonBarClass");
    $(".btnActive").addClass("selectButtonBarClass");
}
function onClickInActive() {
    $(".btnActive").removeClass("selectButtonBarClass");
    $(".btnInActive").addClass("selectButtonBarClass");
}
function init() {
    bindEvents();
    addvitalsData();
}
$(document).ready(function() {
    init();
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("vitalsList", dataOptions);
    angularUIgridWrapper.init();
    buildvitalsListGrid([]);
});
$(window).load(function(){
    windowLoad = true;
    searchOnLoad();
});
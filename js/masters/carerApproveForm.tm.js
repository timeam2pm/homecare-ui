var parentRef = null;
var recordType = "1";
var dataArray = [];
var selRow = null;
$(document).ready(function(){
    parentRef = parent.frames['iframe'].window;
    selRow = parentRef.selRow;
    console.log(selRow);
    //recordType = parentRef.dietType;
});

$(window).load(function(){
    loading = false;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
    $("#cmbLeaveType").kendoComboBox();
    $("#cmbRequest").kendoComboBox();
    $("#txtAFromDate").kendoDatePicker();
    $("#txtAToDate").kendoDatePicker();
    getLeaveTypes();
    buttonEvents();
}

function getLeaveTypes(){
    getAjaxObject(ipAddress+"/homecare/leave-types/?fields=id,value","GET",handleGetLeaveTypes,onError);
}

function handleGetLeaveTypes(dataObj){
    console.log(dataObj);
    var leaveArray = [];
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            if($.isArray(dataObj.response.leaveTypes)){
                leaveArray = dataObj.response.leaveTypes;
            }else{
                leaveArray.push(dataObj.response.leaveTypes);
            }
            for(var i=0;i<leaveArray.length;i++){
                leaveArray[i].idk = leaveArray[i].id;
            }

            setDataForSelection(leaveArray, "cmbLeaveType", function(){}, ["value", "idk"], 0, "");
        }
    }

    getLeaveRequest();
}

function getLeaveRequest(){
    getAjaxObject(ipAddress+"/master/leave_status/list?is-active=1","GET",handleGetLeaveRequest,onError);
}
function handleGetLeaveRequest(dataObj){
    console.log(dataObj);
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbRequest", function(){}, ["desc", "idk"], 1, "");
    }
    setFormData();
}
function setFormData(){
    if(selRow){
        $("#txtFEmail").val(selRow.fromEmail);
        $("#txtTEmail").val(selRow.toEmail);
        $("#txtSubject").val(selRow.subject);
        $("#txtFromDate").val(selRow.FD);
        $("#txtToDate").val(selRow.TD);
        $("#txtNotes").val(selRow.reason);
        var cmbLeaveType = $("#cmbLeaveType").data("kendoComboBox");
        if(cmbLeaveType){
            var idx = getComboListIndex("cmbLeaveType","idk",selRow.leaveTypeId);
            if(idx>=0){
                cmbLeaveType.select(idx);
                cmbLeaveType.enable(false);
            }
        }
        var obj = selRow;
        var afdt = "";
        if(obj.acceptedFromDate){
            afdt = new Date(obj.acceptedFromDate);
        }else{
            afdt = new Date(obj.fromDate);
        }

        var atdt = "";
        if(obj.acceptedToDate){
            atdt = new Date(obj.acceptedToDate);
        }else{
            atdt = new Date(obj.toDate);
        }

        $("#txtANotes").val(selRow.remarks);

        var txtAFromDate = $("#txtAFromDate").data("kendoDatePicker");
        var txtAToDate = $("#txtAToDate").data("kendoDatePicker");

        if(afdt){
            txtAFromDate.value(afdt);
        }else{
        }
        if(atdt){
            txtAToDate.value(atdt);
        }
    }
}
function getComboListIndex(cmbId, attr, attrVal) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb) {
        var ds = cmb.dataSource;
        var totalRec = ds.total();
        for (var i = 0; i < totalRec; i++) {
            var dtItem = ds.at(i);
            if (dtItem && dtItem[attr] == attrVal) {
                cmb.select(i);
                return i;
            }
        }
    }
    return -1;
}
function getTableListArray(dataObj) {
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.codeTable) {
        if ($.isArray(dataObj.response.codeTable)) {
            dataArray = dataObj.response.codeTable;
        } else {
            dataArray.push(dataObj.response.codeTable);
        }
    }
    var tempDataArry = [];
    var obj = {};
    obj.desc = "";
    obj.zip = "";
    obj.value = "";
    obj.idk = "";
    // tempDataArry.push(obj);
    for (var i = 0; i < dataArray.length; i++) {
        if (dataArray[i].isActive == 1) {
            var obj = dataArray[i];
            obj.idk = dataArray[i].id;
            obj.status = dataArray[i].Status;
            tempDataArry.push(obj);
        }
    }
    return tempDataArry;
}
function onClickView(){
    var txtFDate = $("#txtPFDate").data("kendoDatePicker");
    var txtTDate = $("#txtPTDate").data("kendoDatePicker");
}

function init(){
    $("#btnEdit").prop("disabled", true);
    $("#btnDelete").prop("disabled", true);
    //getAjaxObject(ipAddress+"/homecare/activity-types/?is-active=1&is-deleted=0&sort=display-order","GET",getActivityTypes,onError);
}


function onError(errorObj){
    console.log(errorObj);
}

function buttonEvents(){

    //$("#btnView").off("click",onClickView);
    //$("#btnView").on("click",onClickView);

    $("#btnApprove").off("click",onClickSave);
    $("#btnApprove").on("click",onClickSave);

    //$("#btnSaveDO").off("click",onClickSaveDO);
    //$("#btnSaveDO").on("click",onClickSaveDO);

    $("#btnCancel").off("click",onClickCancel);
    $("#btnCancel").on("click",onClickCancel);

    //$("#btnDelete").off("click",onClickDelete);
    //$("#btnDelete").on("click",onClickDelete);

    //$("#btnEdit").off("click",onClickEdit);
    //$("#btnEdit").on("click",onClickEdit);

    //$("#btnDelete").off("click",onClickDelete);
    //$("#btnDelete").on("click",onClickDelete);

    //$("#btnAdd").off("click");
    //$("#btnAdd").on("click",onClickAdd);

    //$("#btnDiet").off("click");
    //$("#btnDiet").on("click",onClickDiet);

    //$("#btnExcersize").off("click");
    //$("#btnExcersize").on("click",onClickExcersize);

    //$("#btnillness").off("click");
    //$("#btnillness").on("click",onClickillness);
}
function getLeaveTypes(){
    getAjaxObject(ipAddress+"/homecare/leave-types/?fields=id,value","GET",handleGetLeaveTypes,onError);
}



function onCreateDelete(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "Vacation deleted successfully";
            customAlert.error("Info", msg);
            operation = ADD;
            init();
        }
    }
}
function onClickSave(){
    try{
        var txtAFromDate = $("#txtAFromDate").data("kendoDatePicker");
        var txtAToDate = $("#txtAToDate").data("kendoDatePicker");

        var reqData = {};
        reqData.acceptedToDate = txtAToDate.value().getTime();
        reqData.acceptedFromDate = txtAFromDate.value().getTime();
        reqData.remarks = $("#txtANotes").val();
        reqData.isActive = 1;
        reqData.isDeleted = 0;
        reqData.createdBy = 101;
        reqData.approvedBy = 101;
        reqData.modifiedBy = 101;
        reqData.id = selRow.idk;
        console.log(reqData);

        var method = "PUT";
        dataUrl = ipAddress +"/homecare/vacations/";
        createAjaxObject(dataUrl, reqData, method, onCreate, onError);
    }catch(ex){
        console.log(ex);
    }
}
function onCreate(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "Vacation Approved  successfully";
            displaySessionErrorPopUp("Info", msg, function(res) {
                //resetData();
                operation = ADD;
                onClickCancel();
            })
        }else{
            customAlert.error("Error", dataObj.response.status.message);
        }
    }else{

    }
}

function resetData(){
    $("#txtFEmail").val("");
    $("#txtTEmail").val("");
    $("#txtSubject").val("");
    $("#txtReason").val("");

    var txtFDate = $("#txtFDate").data("kendoDatePicker");
    var txtTDate = $("#txtTDate").data("kendoDatePicker");

    txtFDate.value("");
    txtTDate.value("");
}
function onClickDiet(){
    onDiet();
    recordType = "1";
    init();
}
function onClickExcersize(){
    onExcersize();
    recordType = "2";
    init();
}
function onClickillness(){
    onIllness();
    recordType = "5";
    init();
}
function onDiet(){
    $("#btnDiet").addClass("selectButtonBarClass");
    $("#btnExcersize").removeClass("selectButtonBarClass");
    $("#btnillness").removeClass("selectButtonBarClass");
}
function onExcersize(){
    $("#btnDiet").removeClass("selectButtonBarClass");
    $("#btnExcersize").addClass("selectButtonBarClass");
    $("#btnillness").removeClass("selectButtonBarClass");
}
function onIllness(){
    $("#btnDiet").removeClass("selectButtonBarClass");
    $("#btnExcersize").removeClass("selectButtonBarClass");
    $("#btnillness").addClass("selectButtonBarClass");
}
function adjustHeight(){
    var defHeight = 280;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    if(angularUIgridWrapper){
        angularUIgridWrapper.adjustGridHeight(cmpHeight);
    }
}

function buildDeviceListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "From Date",
        "field": "FD",
    });
    gridColumns.push({
        "title": "To Date",
        "field": "TD",
    });
    gridColumns.push({
        "title": "Accepted From Date",
        "field": "AFD",
    });
    gridColumns.push({
        "title": "Accepted To Date",
        "field": "ATD",
    });
    gridColumns.push({
        "title": "Remarks",
        "field": "remarks",
    });
    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}

var selRow = null;
function onClickAction(){

}

var prevSelectedItem =[];
function onChange(){
    setTimeout(function() {
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if (selectedItems && selectedItems.length > 0) {
            var obj = selectedItems[0];
            atID = obj.idk;
            $("#btnEdit").prop("disabled", false);
            //$("#btnDelete").prop("disabled", false);
            //onClickEdit();
        } else {
            $("#btnEdit").prop("disabled", true);
            //$("#btnDelete").prop("disabled", true);
            //resetData();
        }
    });

}
var operation = "ADD";
var ADD = "ADD";
var UPDATE = "UPDATE";
var atID = "";
var ds  = "";

function onClickEdit(){
    var selectedItems = angularUIgridWrapper.getSelectedRows();
    console.log(selectedItems);
    if (selectedItems && selectedItems.length > 0) {
        var obj = selectedItems[0];
        operation = UPDATE;
        /*$("#txtFEmail").val(obj.fromEmail);
        $("#txtTEmail").val(obj.toEmail);
        $("#txtSubject").val(obj.subject);
        $("#txtReason").val(obj.remarks);

        var fdt = new Date(obj.fromDate);
        var tdt = new Date(obj.toDate);

        var afdt = "";
        if(obj.acceptedFromDate){
            afdt = new Date(obj.acceptedFromDate);
        }

        var atdt = "";
        if(obj.acceptedToDate){
            atdt = new Date(obj.acceptedToDate);
        }


        var txtFDate = $("#txtPFDate").data("kendoDatePicker");
        var txtTDate = $("#txtPTDate").data("kendoDatePicker");

        txtFDate.value(fdt);
        txtTDate.value(tdt);

        txtFDate.enable(false);
        txtTDate.enable(false);

        var txtFDate = $("#txtFDate").data("kendoDatePicker");
        var txtTDate = $("#txtTDate").data("kendoDatePicker");
        if(afdt){
            txtFDate.value(afdt);
        }else{
            txtFDate.value(fdt);
        }
        if(atdt){
            txtTDate.value(atdt);
        }else{
            txtTDate.value(tdt);
        }*/

    }
}


function closeVideoScreen(evt,returnData){

}
var operation = "add";
var selFileResourceDataItem = null;
function onClickCancel(){
    var obj = {};
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}

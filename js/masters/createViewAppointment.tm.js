var angularUIgridWrapper;
var parentRef = null;
var recordType = "1";
var dataArray = [];
var ADD = "add";
var UPDATE = "edit";
var VIEW = "view";
var DELETE = "delete";
var operation = "add";
var ds  = "";
var IsFlag = 1;
var cntry = sessionStorage.countryName;
var selectPatientId, selectProviderId;
var dtFMT = "dd/MM/yyyy";
var appointmentdtFMT = "dd/MM/yyyy hh:mm tt";
var appointmentId;
var selItem;
var patientBillArray = [];
var typeWeek;

$(document).ready(function(){
    $("#pnlPatient",parent.document).css("display","none");
    sessionStorage.setItem("IsSearchPanel", "1");
    themeAPIChange();
    parentRef = parent.frames['iframe'].window;

    $("#btnCancel").off("click",onClickCancel);
    $("#btnCancel").on("click",onClickCancel);
    //
    // $(".popupClose").off("click", onClickCancel);
    // $(".popupClose").on("click", onClickCancel);

});


$(window).load(function(){
    loading = false;
    //$(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
    init();
    buttonEvents();
    callMastersData();
}

function init() {

    $("#txtStartDate").kendoDateTimePicker({
        change: startChange,
        format: appointmentdtFMT, interval: 15
    });
    $("#txtEndDate").kendoDateTimePicker({ format: appointmentdtFMT, interval: 15 });
    console.log(parentRef.selObj);

    if (parentRef != null && parentRef.selObj != null) {
        if (parentRef.selObj.facilityName != null && parentRef.selObj.facilityName != "") {
            $("#txtFacility").val(parentRef.selObj.facilityName);
        }

        if (parentRef.selObj.pLastName != null && parentRef.selObj.pLastName != "" && parentRef.selObj.pFirstName != null && parentRef.selObj.pFirstName != "") {
            $("#txtStaff").val(parentRef.selObj.pLastName + " " + parentRef.selObj.pFirstName);
        }

        var txtStartDate = $("#txtStartDate").data("kendoDateTimePicker");
        console.log(new Date(parentRef.selObj.dateOfAppointment));

        if (txtStartDate) {
            txtStartDate.value(GetDateTimeEdit(parentRef.selObj.dateOfAppointment));
        }

        var endappoinment = (parentRef.selObj.dateOfAppointment + (15 * 60000));
        var tDate = new Date(endappoinment);
        var minDate = new Date(tDate.getFullYear(), tDate.getMonth(), tDate.getDate());
        var maxDate = new Date(tDate.getFullYear(), tDate.getMonth(), tDate.getDate(), 23, 59, 0);

        console.log(minDate);
        console.log(maxDate);

        $("#txtEndDate").kendoDateTimePicker(
            {
                format: appointmentdtFMT,
                interval: 15,
                min: minDate,
                max: maxDate
            })
        var txtEndDate = $("#txtEndDate").data("kendoDateTimePicker");
        if (txtEndDate) {
            txtEndDate.value(GetDateTimeEdit(endappoinment));
        }


    }



}



function onError(errorObj){
    console.log(errorObj);
}

function buttonEvents(){

    $("#btnCancel").off("click",onClickCancel);
    $("#btnCancel").on("click",onClickCancel);

    $("#btnReset").off("click",onClickReset);
    $("#btnReset").on("click",onClickReset);

    // $(".popupClose").off("click", onClickCancel);
    // $(".popupClose").on("click", onClickCancel);

    $("#btnStaff").off("click");
    $("#btnStaff").on("click",onClickStaff);

    $("#btnServiceUser").off("click");
    $("#btnServiceUser").on("click", onClickServiceUser);

    $("#btnSave").off("click");
    $("#btnSave").on("click",function(){
        appointmentSave();
    });


    $("#cmbTypeAppointment").change(function() {
        $("#divAppsection").show();
        var cmbTypeAppointment = $("#cmbTypeAppointment").val();

        if(cmbTypeAppointment == 3){
            $("#divAppsection").hide();
        }
        else if(cmbTypeAppointment == 1){
            $("#txtAppPTDate").kendoDatePicker({format:dtFMT});
            var txtAppPTDate = $("#txtAppPTDate").data("kendoDatePicker");
            if(parentRef.selDate) {
                txtAppPTDate.setOptions({
                    min: new Date(date)
                });
            }
        }
        else{
            $("#txtAppPTDate").kendoDatePicker({format:dtFMT});
            var txtAppPTDate = $("#txtAppPTDate").data("kendoDatePicker");
            txtAppPTDate.setOptions({
                min: new Date()
            });
        }
    });

}

function searchOnLoad(status) {
    buildDeviceListGrid([]);
    if(status == "active") {
        var urlExtn = ipAddress+"/homecare/vacations/?fields=*&parentTypeId="+Number(parentRef.type)+"&parentId="+Number(parentRef.patientId)+"&is-active=1&is-deleted=0";

    }
    else if(status == "inactive") {
        var urlExtn =  ipAddress+"/homecare/vacations/?fields=*&parentTypeId="+Number(parentRef.type)+"&parentId="+Number(parentRef.patientId)+"&is-active=0&is-deleted=1"
    }
    getAjaxObject(urlExtn,"GET",handleGetVacationList,onError);
}


function onClickAdd(e){
    $("#addPopup").show();
    $('#viewDivBlock').hide();
    $("#txtID").hide();
    $(".filter-heading").html("Add Appointment");
    parentRef.operation = "add";
    onClickReset();
}


function onClickActive() {
    $(".btnInActive").removeClass("radioButton-active");
    $(".btnActive").addClass("radioButton-active");
}

function onClickInActive() {
    $(".btnActive").removeClass("radioButton-active");
    $(".btnInActive").addClass("radioButton-active");
}



function onCreateDelete(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "Vacation deleted successfully";
            customAlert.error("Info", msg);
            operation = ADD;
            init();
        }
    }
}


function onCreate(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "Appointment created successfully";
            displaySessionErrorPopUp("Info", msg, function(res) {
                if(parentRef.appselList.length > 0 ) {
                    delete parentRef.appselList[gridId];
                }
                onClickReset();
                init();
                popupClose(false);
            })
        }else{
            customAlert.error("Error", dataObj.response.status.message);
        }
    }else{

    }
    onClickReset();
}


function onClickCancel(){
    var onCloseData = new Object();
    // onCloseData.status = st;
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(onCloseData);
}

function adjustHeight(){
    var defHeight = 230;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    angularUIgridWrapper.adjustGridHeight(cmpHeight);
}

function buildDeviceListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    otoptions.rowHeight = 100;
    gridColumns.push({
        "title": "Select",
        "field": "SEL",
        "cellTemplate": showCheckBoxTemplate(),
        "headerCellTemplate": "<input type='checkbox' class='check1' ng-model='TCSELECT' onclick='toggleSelectAll(event);' style='margin: 6px 0 0 12px !important; display: inline-block;'></input>",
        "width":"5%"
    });
    gridColumns.push({
        "title": "Start Date",
        "field": "dateTimeOfAppointment1",
        "enableColumnnMenu": false,
        "width": "22%"
    });
    gridColumns.push({
        "title": "End Date",
        "field": "dateTimeOfAppointmentED",
        "enableColumnnMenu": false,
        "width": "22%"
    });
    gridColumns.push({
        "title": "Status",
        "field": "appointmentTypeDesc",
        "enableColumnMenu": false,
    });
    gridColumns.push({
        "title": "Reason",
        "field": "appointmentReasonDesc",
        "enableColumnMenu": false,
    });
    gridColumns.push({
        "title": "Service User",
        "field": "name",
        "enableColumnMenu": false,
    });
    gridColumns.push({
        "title": "Staff",
        "field": "staffName",
        "enableColumnMenu": false,
    });

    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
}

function onChange(){
    setTimeout(function() {
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        if (selectedItems && selectedItems.length > 0) {
            selItem = selectedItems[0];
            $("#btnEdit").prop("disabled", false);
            // if(selItem.SEL) {
            $("#btnDelete").prop("disabled", false);
            // }
            // else{
            //     $("#btnDelete").prop("disabled", true);
            // }
        }
        else {
            $("#btnEdit").prop("disabled", true);

            // if(selItem.SEL) {
            //     $("#btnDelete").prop("disabled", false);
            // }else{
            $("#btnDelete").prop("disabled", true);
            // }
        }
    });
}
var editbilloName;
function onClickEdit(){
    $(".filter-heading").html("Edit Appointment");
    parentRef.operation = "edit";
    operation =  "edit";
    $("#viewDivBlock").hide();
    $("#addPopup").show();
    var selectedItems = angularUIgridWrapper.getSelectedRows();
    if (selectedItems && selectedItems.length > 0) {
        var obj = selectedItems[0];
        if(obj.idk){
            appointmentId = obj.idk;
        }
        else{
            appointmentId = obj.id;
        }

        if(obj.composition && obj.composition.appointmentReason.value) {
            $("#cmbReason").val(obj.composition.appointmentReason.value);
        }else{
            $("#cmbReason").val(obj.appointmentReason);
        }
        if(obj.composition && obj.composition.appointmentType){
            $("#cmbStatus").val(obj.composition.appointmentType.value);
        }
        else{
            $("#cmbStatus").val(obj.appointmentType);
        }

        selectPatientId = obj.patientId;
        selectProviderId = obj.providerId;
        if(selectProviderId != 0) {
            if(obj.composition && obj.composition.provider){
                $("#txtStaff").val(obj.composition.provider.lastName + " " + obj.composition.provider.firstName + " " + (obj.composition.provider.middleName != null ? obj.composition.provider.middleName : " "));
            }else{
                $("#txtStaff").val(obj.staffName);
            }

        }
        else{
            $("#txtStaff").val("");
        }
        $("#txtFacility").val(parentRef.facilityName);
        var txtStartDate = $("#txtStartDate").data("kendoDateTimePicker");
        if(txtStartDate){
            // txtStartDate.val(GetDateTimeEdit(obj.dateTimeOfAppointment));
            txtStartDate.value(GetDateTimeEdit(obj.dateTimeOfAppointment));
        }
        var endappoinment = (obj.dateTimeOfAppointment + (Number(obj.duration)*60000));
        var txtEndDate = $("#txtEndDate").data("kendoDateTimePicker");
        if(txtEndDate){
            // txtEndDate.val(GetDateTimeEdit(endappoinment));
            txtEndDate.value(GetDateTimeEdit(endappoinment));
        }
        $("#txtPayout").val(obj.payoutHourlyRate);
        $("#cmbBilling").val(obj.billToName);
    }
}


var operation = "add";
var selFileResourceDataItem = null;


function onClickReset() {
    $("#cmbReason").val("");
    $("#cmbStatus").val("Con");
    $("#txtDateofAppointment").val("");
    $("#txtStaff").val("");
    operation = ADD;
    selectProviderId = "";
    var startDate = $("#txtStartDate").data("kendoDateTimePicker");
    if(startDate){
        startDate.value("");
    }
    var endDate = $("#txtEndDate").data("kendoDateTimePicker");
    if(endDate){
        endDate.value("");
    }
    $("#cmbBilling").val("");
    $("#txtPayout").val("");
    appointmentId = null;


}

function themeAPIChange(){
    getAjaxObject(ipAddress+"/homecare/settings/?id=2","GET",getThemeValue,onError);
}

function getThemeValue(dataObj){
    console.log(dataObj);
    var tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.activityTypes){
            if($.isArray(dataObj.response.settings)){
                tempCompType = dataObj.response.settings;
            }else{
                tempCompType.push(dataObj.response.settings);
            }
        }
        if(tempCompType.length > 0){
            themesChange(tempCompType[0].value);
        }
    }
}

function themesChange(themeValue){
    if(themeValue == 2){
        loadAPi("../../Theme2/Theme02.css");
    }
    else if(themeValue == 3){
        loadAPi("../../Theme3/Theme03.css");
    }
}


function onClickSubmit() {
    buildDeviceListGrid([]);
    typeWeek = $("#cmbTypeAppointment").val();
    angularUIgridWrapper.refreshGrid();
    parentRef = parent.frames['iframe'].window;
    var appselList;


    var txtAppPTDate = $("#txtAppPTDate").data("kendoDatePicker");
    if (txtAppPTDate.value() !== "" && txtAppPTDate.value() != null) {
        // alert(GetDateTime("txtAppPTDate"));
        // alert(GetDateTime(parentRef.selValue));
        // alert(GetDateTimeEdit(parentRef.selValue).getTime());

        var diffDays;
        var day = txtAppPTDate.value().getDay();
        for (var i = 0; parentRef.selValue.length > 0; i++) {
            if (parentRef.selValue[i].getDay() == day) {
                // alert(parentRef.selValue[i]);
                diffDays = showDays(GetFormattedDate(txtAppPTDate.value()), GetFormattedDate(GetFormattedDate(parentRef.selValue[i])));
                break;
            }

        }
        if(typeWeek == "1") {
            appselList = JSON.parse(sessionStorage.appselList);
            for (var i = 0; i < (appselList.length); i++) {
                var date = new Date(appselList[i].dateTimeOfAppointment1);
                appselList[i].dateTimeOfAppointment = date.setDate(date.getDate() + diffDays);
                // parentRef.appselList[i].dateOfAppointment = date.setDate(date.getDate() + diffDays);
                appselList[i].rowId = i;

                // if(parentRef.appselList[i] && parentRef.appselList[i].dateTimeOfAppointment) {
                var day = new Date(GetDateTimeEditDay(appselList[i].dateTimeOfAppointment)).getDay();
                var dow = getWeekDayName(day);

                appselList[i].dateTimeOfAppointment1 = dow + " " + GetDateTimeEdit(appselList[i].dateTimeOfAppointment);
                // }
                var dtEnd = appselList[i].dateTimeOfAppointment + (Number(appselList[i].duration) * 60000);


                var dateED = new Date(GetDateTimeEditDay(dtEnd));
                var dayED = dateED.getDay();
                var dowED = getWeekDayName(dayED);

                appselList[i].dateTimeOfAppointmentED = dowED + " " + GetDateTimeEdit(dtEnd);


            }
        }
        else if(typeWeek == "2") {
            appselList = JSON.parse(sessionStorage.appselList);
            var copyDate = new Date(txtAppPTDate.value());
            var copyDay = copyDate.getDay();
            var copyDow = getWeekDayName(copyDay);


            var daylist = [];
            for (var i = 0; i < (appselList.length); i++) {
                if(appselList[i].DW == copyDow) {

                    var date = new Date(appselList[i].dateTimeOfAppointment1);
                    var dateOfAppointment = kendo.toString(appselList[i].dateTimeOfAppointment1, "MM/dd/yyyy");
                    dateOfAppointment = new Date(dateOfAppointment);

                    var day = dateOfAppointment.getDate();
                    var month = dateOfAppointment.getMonth();
                    month = month + 1;
                    var year = dateOfAppointment.getFullYear();

                    var stDate = month + "/" + day + "/" + year;
                    dateOfAppointment = new Date(stDate).getTime();

                    var copyDate = kendo.toString(parentRef.copyDate, "MM/dd/yyyy");
                    copyDate = new Date(copyDate).getTime();
                    // if (copyDate == dateOfAppointment) {
                    appselList[i].dateTimeOfAppointment = date.setDate(date.getDate() + diffDays);
                    // parentRef.appselList[i].dateOfAppointment = date.setDate(date.getDate() + diffDays);
                    appselList[i].rowId = i;

                    // if(parentRef.appselList[i] && parentRef.appselList[i].dateTimeOfAppointment) {
                    var day = new Date(GetDateTimeEditDay(appselList[i].dateTimeOfAppointment)).getDay();
                    var dow = getWeekDayName(day);

                    appselList[i].dateTimeOfAppointment1 = dow + " " + GetDateTimeEdit(appselList[i].dateTimeOfAppointment);
                    // }
                    var dtEnd = appselList[i].dateTimeOfAppointment + (Number(appselList[i].duration) * 60000);


                    var dateED = new Date(GetDateTimeEditDay(dtEnd));
                    var dayED = dateED.getDay();
                    var dowED = getWeekDayName(dayED);

                    appselList[i].dateTimeOfAppointmentED = dowED + " " + GetDateTimeEdit(dtEnd);

                    daylist.push(appselList[i]);
                    // }
                }

            }
            if(!(daylist.length > 0)){
                customAlert.info("info","No record(s) to copy");
            }
        }

        // var copyDay = new Date(parentRef.copyDate).getDay();
        // var copydow = getWeekDayName(copyDay);

        buildDeviceListGrid([]);
        // buildDeviceListGrid(appselList);
        // appselList = null;
        // var startDate = GetDateEdit(txtAppPTDate.value());
        // var resultProductData = appselList.filter(
        //     function (a)
        //     {
        //         return (a.dateTimeOfAppointment) > startDate && (a.ProductHits) < endDate;
        //     });

        if(typeWeek == "1"){
            parentRef.appselList = appselList;
            angularUIgridWrapper.refreshGrid();
            buildDeviceListGrid([]);
            setTimeout(function() {
                buildDeviceListGrid(parentRef.appselList);
            },1000);
            angularUIgridWrapper.refreshGrid();
        }
        else if(typeWeek == "2"){
            daylist.sort(function(a,b){
                // Turn your strings into dates, and then subtract them
                // to get a value that is either negative, positive, or zero.
                return new Date(a.dateTimeOfAppointment) - new Date(b.dateTimeOfAppointment);
            });
            parentRef.appselDayList = daylist;
            angularUIgridWrapper.refreshGrid();
            buildDeviceListGrid([]);
            setTimeout(function() {
                buildDeviceListGrid(daylist);
            },1000);
            angularUIgridWrapper.refreshGrid();
        }else{
            buildDeviceListGrid([]);
        }
        // onbindGrid(appselList);
    }
    else {
        customAlert.error("Error", "Please select date");
    }
}

// function onbindGrid(appselList) {
//     buildDeviceListGrid([]);
//     parentRef.appselList = appselList;
//
//     buildDeviceListGrid(parentRef.appselList);
// }

function getAppointmentList(dataObj){
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.codeTable){
        if($.isArray(dataObj.response.codeTable)){
            dataArray = dataObj.response.codeTable;
        }else{
            dataArray.push(dataObj.response.codeTable);
        }
    }

    for(var i=0;i<dataArray.length;i++){
        if(dataArray[i].isActive == 1){
            $("#cmbStatus").append('<option value="' + dataArray[i].value + '">' + dataArray[i].desc + '</option>');
        }
    }
    $("#cmbStatus").val("Con");
}

function getReasonList(dataObj){
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.codeTable){
        if($.isArray(dataObj.response.codeTable)){
            dataArray = dataObj.response.codeTable;
        }else{
            dataArray.push(dataObj.response.codeTable);
        }
    }

    for(var i=0;i<dataArray.length;i++){
        if(dataArray[i].isActive == 1){
            $("#cmbReason").append('<option value="' + dataArray[i].value + '">' + dataArray[i].desc + '</option>');
        }
    }
}
function callMastersData() {
    getAjaxObject(ipAddress + "/master/appointment_type/list/?is-active=1", "GET", getAppointmentList, onError);
    getAjaxObject(ipAddress + "/master/appointment_reason/list/?is-active=1", "GET", getReasonList, onError);
    //getAjaxObject(ipAddress + "/carehome/patient-billing/?is-active=1&patientId=" + selectPatientId + "&fields=*,shift.*,travelMode.*,billingType.*,billingPeriod.*,billTo.name,billingRateType.code", "GET", getPatientData, onError);
}

function appointmentSave(){
    //buildDeviceListGrid([]);

    if(validation()) {
        if(!($("#txtStaff").val() != "")) {
            var msg = "You have not selected any staff still you want to add?";
            displayConfirmSessionErrorPopUp("Info", msg, function (res) {
                onClickappoinmentsave(0);
            });
        }else{
            onClickappoinmentsave(1);
        }

    }
    else{
        customAlert.error("Error","Please select required fields");
    }

}
function onClickappoinmentsave(IsProvider) {
    // $("#txtStartDate").kendoDateTimePicker({change: startChange,
    //     min: new Date(parentRef.selDate),
    //     format: appointmentdtFMT,interval: 15});
    // $("#txtEndDate").kendoDateTimePicker({format: appointmentdtFMT ,interval: 15,min:new Date()});

    var arr = [];
    var obj = {};

    if (parentRef.selObj != null) {

        var txtEndDate = $("#txtEndDate").data("kendoDateTimePicker");

        var appointmentsArr = parentRef.appointmentsArr;

        var isExist = false;

        if (appointmentsArr && appointmentsArr.length > 0) {

            var dStaffAppointments = _.where(appointmentsArr, { providerId: parentRef.selObj.providerId });

            if (dStaffAppointments != null && dStaffAppointments.length > 0) {
                for (var m = 0; m < dStaffAppointments.length; m++) {
                    var currAppSD = parentRef.selObj.dateOfAppointment;
                    var eDate = new Date(txtEndDate.value());
                    var currAppED = eDate.getTime();

                    var dAppSD = dStaffAppointments[m].dateOfAppointment;
                    var dAppED = dStaffAppointments[m].dateOfAppointment + (dStaffAppointments[m].duration * 60 * 1000);

                    if ((currAppSD >= dAppSD && currAppSD < dAppED)) {
                        isExist = true;
                        break;
                    }

                }
            }
        }

        if (!isExist) {


            obj.facilityId = parentRef.selObj.facilityId;
            obj.providerId = parentRef.selObj.providerId;
            obj.createdBy = Number(sessionStorage.userId);
            obj.isActive = 1;
            obj.isDeleted = 0;

            var txtStartDate = $("#txtStartDate").data("kendoDateTimePicker");
            var sDate = new Date(txtStartDate.value());
            obj.dateOfAppointment = sDate.getTime();

            txtEndDate = $("#txtEndDate").data("kendoDateTimePicker");
            eDate = new Date(txtEndDate.value());

            var diff = eDate.getTime() - sDate.getTime();
            diff = diff / 1000;
            diff = diff / 60;
            obj.duration = Math.round(diff);
            obj.patientId = selectPatientId;

            obj.notes = null;
            obj.billToId = $("#cmbBilling").val();
            obj.payoutHourlyRate = $("#txtPayout").val();
            obj.appointmentType = $("#cmbStatus").val();
            obj.notes = "";
            var pBillItem = getPatientBillingDetails(obj.billToId);
            obj.shiftValue = pBillItem.shiftValue;
            obj.billingRateType = pBillItem.billingRateTypeCode;
            obj.patientBillingRate = pBillItem.rate;
            obj.billingRateTypeId = pBillItem.billingRateTypeId;
            obj.swiftBillingId = pBillItem.swiftBillingId;
            obj.payoutHourlyRate = Number($("#txtPayout").val());
            obj.billToName = pBillItem.billToname;
            obj.billToId = pBillItem.billToId;

            obj.appointmentReason = $("#cmbReason").val();


            arr.push(obj);

            var dataUrl = ipAddress + "/appointment/create";
            createAjaxObject(dataUrl, arr, "POST", onCreateAppointment, onError);

        }
        else {
            customAlert.info("Info", "Already appointment occupied this slot");
        }


    }
}

function getPatientBillingDetails(pBid){
    for(var p=0;p<patientBillArray.length;p++){
        var pItem = patientBillArray[p];
        if(pItem && pItem.billToname == pBid){
            return pItem;
        }
    }
    return null;
}

function onCreateAppointment(dataObj) {

    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            var msg = "Appointment created successfully";
            displaySessionErrorPopUp("Info", msg, function (res) {
                onClickCancel();
            })
        } else {
            customAlert.error("Error", dataObj.response.status.message);
        }
    } else {
        customAlert.error("Error", dataObj.response.status.message);
    }

}

function getPatientData(dataObj) {
    patientBillArray = [];

    var tempCompType = [];
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.patientBilling) {
            if ($.isArray(dataObj.response.patientBilling)) {
                tempCompType = dataObj.response.patientBilling;
            } else {
                tempCompType.push(dataObj.response.patientBilling);
            }
        }
    }
    for (var q = 0; q < tempCompType.length; q++) {
        var qItem = tempCompType[q];
        if (qItem) {
            qItem.value = qItem.billToname;
            qItem.text = qItem.billToname;
            patientBillArray.push(qItem);
            $("#cmbBilling").append('<option value="' + qItem.value + '">' + qItem.value + '</option>');
        }
    }


    $("#cmbBilling").removeAttr("disabled");
}

function onGetSUBillingDetails(name){
    for (var i=0;patientBillArray.length >= 0;i++){
        if(patientBillArray[i].billToname.toLowerCase() == name.toLowerCase()){
            return patientBillArray[i];
        }
    }

}


function onGetAllSUBillingDetails(name,data){
    for (var i=0;patientBillArray.length >= 0;i++){
        if(data[i].billToname.toLowerCase() == name.toLowerCase()){
            return data[i];
        }
    }

}

function showDays(firstDate,secondDate){
    var startDay = new Date(firstDate);
    var endDay = new Date(secondDate);
    var millisecondsPerDay = 1000 * 60 * 60 * 24;

    var millisBetween = startDay.getTime() - endDay.getTime();
    var days = millisBetween / millisecondsPerDay;
    return Math.floor(days);
}

function onClickStaff(){
    var popW = 800;
    var popH = 450;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Staff";

    devModelWindowWrapper.openPageWindow("../../html/masters/staffList.html", profileLbl, popW, popH, true, onCloseStaffAction);
}

function onCloseStaffAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        selectProviderId = returnData.selItem.PID;
        $("#txtStaff").val(returnData.selItem.lastName + ' '+ returnData.selItem.firstName+' '+ (returnData.selItem.middleName != null ? returnData.selItem.middleName : " "));
    }
}

var gridId;
function onCreateAppointments(){
    var dataList = []
    var selectedItems = angularUIgridWrapper.getAllRows();
    if(selectedItems.length > 0) {
        for (var i = 0; i < selectedItems.length; i++) {

            var obj = {};
            obj.createdBy = Number(sessionStorage.userId);
            obj.isActive = 1;
            obj.isDeleted = 0;
            gridId = selectedItems[i].entity.id;
            obj.dateOfAppointment = selectedItems[i].entity.dateTimeOfAppointment;
            obj.duration = selectedItems[i].entity.duration;
            obj.providerId = selectedItems[i].entity.providerId;
            obj.facilityId = selectedItems[i].entity.facilityId;
            obj.patientId = selectedItems[i].entity.patientId;
            if (selectedItems[i].entity.composition) {
                obj.appointmentType = selectedItems[i].entity.composition.appointmentType.value;
                obj.appointmentReason = selectedItems[i].entity.composition.appointmentReason.value;

            }
            else {
                obj.appointmentType = selectedItems[i].entity.appointmentType;
                obj.appointmentReason = selectedItems[i].entity.appointmentReason;
            }
            obj.notes = selectedItems[i].entity.notes;

            var response = _.where(patientAllBillArray, {patientId: selectedItems[i].entity.patientId})


            var selSUItem = onGetAllSUBillingDetails(selectedItems[i].entity.billToName, response);
            obj.shiftValue = selSUItem.shiftValue;
            obj.billingRateType = selSUItem.billingRateTypeCode;
            obj.patientBillingRate = selSUItem.rate;
            obj.billingRateTypeId = selSUItem.billingRateTypeId;
            obj.swiftBillingId = selSUItem.swiftBillingId;
            obj.payoutHourlyRate = selectedItems[i].entity.payoutHourlyRate;
            obj.billToName = selectedItems[i].entity.billToName;
            obj.billToId = selSUItem.billToId;

            dataList.push(obj);
        }
        // console.log(dataList);
        var dataUrl = ipAddress + "/appointment/create";
        createAjaxObject(dataUrl, dataList, "POST", onCreate, onError);
    }
    else{
        customAlert.info("info","No record(s) in the list");
    }

}


function onSUBilliing(){
    getAjaxObject(ipAddress + "/carehome/patient-billing/?is-active=1&fields=*,shift.*,travelMode.*,billingType.*,billingPeriod.*,billTo.name,billingRateType.code", "GET", getAllPatientData, onError);
}

var patientAllBillArray;
function getAllPatientData(dataObj) {
    patientAllBillArray = [];
    var tempCompType = [];
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.patientBilling) {
            if ($.isArray(dataObj.response.patientBilling)) {
                tempCompType = dataObj.response.patientBilling;
            } else {
                tempCompType.push(dataObj.response.patientBilling);
            }
        }
    }
    for (var q = 0; q < tempCompType.length; q++) {
        var qItem = tempCompType[q];
        if (qItem) {
            qItem.value = qItem.billToname;
            qItem.text = qItem.billToname;
            patientAllBillArray.push(qItem);
        }
    }
}

function validation() {
    var IsFlag = true;
    var txtStartDate = $("#txtStartDate").data("kendoDateTimePicker");
    var txtEndDate = $("#txtEndDate").data("kendoDateTimePicker");
    if(!(txtStartDate.value() != null)){
        IsFlag = false;
    }
    else if(!(txtEndDate.value() != null)){
        IsFlag = false;
    }
    if(!($("#cmbReason").val() != "")){
        IsFlag = false;
    }
    else if(!($("#cmbStatus").val() != "")){
        IsFlag = false;
    }
    else if(!($("#cmbBilling").val() != "")){
        IsFlag = false;
    }
    else if(!($("#txtPayout").val() != "")){
        IsFlag = false;
    }
    return IsFlag;
}



function startChange(){
    var txtStartTime = $("#txtStartDate").data("kendoDateTimePicker");
    var txtEndTime = $("#txtEndDate").data("kendoDateTimePicker");
    var startTime = txtStartTime.value();



    if (startTime) {
        var sTime = new Date(startTime);
        var tTime = sTime.getTime();
        tTime = tTime+(15*60*1000);
        var strTime = new Date(tTime);
        txtEndTime.min(strTime);
        txtEndTime.value(strTime);
        // txtEndTime.max(today1);
        //end.value(startTime);
    }

}

function showCheckBoxTemplate(){
    var node = '<div style="text-align:center;padding-top: 0;"><input type="checkbox" class="check1" id="chkbox" ng-model="row.entity.SEL" style="width:15px;height:15px;margin:0px;"   onclick="onSelect(event);"></input></div>';
    return node;
}
function onSelect(evt){
    //console.log(evt);
}
function toggleSelectAll(e) {
    var checked = e.currentTarget.checked;
    var rows = angularUIgridWrapper.getScope().gridApi.core.getVisibleRows();
    for (var i = 0; i < rows.length; i++) {
        rows[i].entity["SEL"] = checked;
    }
}



function onClickDeleteRoster(){
    var selectedItems = angularUIgridWrapper.getSelectedRows();
    if(selectedItems && selectedItems.length>0){
        customAlert.confirm("Confirm", "Are you sure you want delete?",function(response){
            if(response.button == "Yes"){
                var selGridData = angularUIgridWrapper.getAllRows();
                var selList = [];
                for (var i = 0; i < selGridData.length; i++) {
                    var dataRow = selGridData[i].entity;
                    if(dataRow.SEL){
                        selList.push(dataRow);
                    }
                }
                angularUIgridWrapper.deleteItems(selList);
            }
        });
    }
}
function popupClose(st){
    var onCloseData = new Object();
    onCloseData.status = st;
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(onCloseData);
}


function GetFormattedDate(date) {
    var todayTime = new Date(date);
    var month = (todayTime .getMonth() + 1);
    var day =(todayTime .getDate());
    var year = (todayTime .getFullYear());
    return month + "/" + day + "/" + year;
}

function bindData(){
    if(typeWeek == "1"){
        buildDeviceListGrid(parentRef.appselList);
    }else if(typeWeek == "2"){
        buildDeviceListGrid(parentRef.appselDayList);
    }else{
        buildDeviceListGrid(parentRef.appselEmptyList);
    }
}

function onClickServiceUser() {
    var popW = 800;
    var popH = 450;

    if (parentRef)
        parentRef.searchZip = true;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Service User";
    devModelWindowWrapper.openPageWindow("../../html/patients/searchPatient.html", profileLbl, popW, popH, true, onCloseServiceUserAction);
}
function onCloseServiceUserAction(evt, returnData) {
    if (returnData && returnData.status == "success") {
        selectPatientId = returnData.selItem.PID;
        $('#txtSU').val(returnData.selItem.LN + " " + returnData.selItem.FN + " " + returnData.selItem.MN);
        getAjaxObject(ipAddress + "/carehome/patient-billing/?is-active=1&patientId=" + selectPatientId + "&fields=*,shift.*,travelMode.*,billingType.*,billingPeriod.*,billTo.name,billingRateType.code", "GET", getPatientData, onError);
    }
}
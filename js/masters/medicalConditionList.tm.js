var angularUIgridWrapper = null;
var parentRef = null;

$(document).ready(function() {
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("dgridZipList", dataOptions);
    angularUIgridWrapper.init();
    buildZipListGrid([]);
});


$(window).load(function() {
    loading = false;
    parentRef = parent.frames['iframe'].window;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded() {
    if (parentRef && parentRef.searchZip == true) {
        $("#btnDelete").hide();
        //$("#btnAdd").hide();
        $("#btnSave").text("OK");
        $("#btnInActive").hide();
        $("#btnActive").hide();
    }
    init();
    buttonEvents();
    adjustHeight();
}

function init() {
    $("#btnSave").prop("disabled", true);
    $("#btnDelete").prop("disabled", true);
    buildZipListGrid([]);
    getAjaxObject(ipAddress + "/homecare/medical-condition-types/", "GET", getZipList, onError);
}

function onError(errorObj) {
    console.log(errorObj);
}

function getZipList(dataObj) {
    console.log(dataObj);
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.medicalConditionTypes) {
        if ($.isArray(dataObj.response.medicalConditionTypes)) {
            dataArray = dataObj.response.medicalConditionTypes;
        } else {
            dataArray.push(dataObj.response.medicalConditionTypes);
        }
    }
    var tempDataArry = [];
    for (var i = 0; i < dataArray.length; i++) {
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if (dataArray[i].isActive == 1) {
            dataArray[i].Status = "Active";
        }
    }
    buildZipListGrid(dataArray);
}

function buttonEvents() {

    $("#btnSave").off("click", onClickOK);
    $("#btnSave").on("click", onClickOK);

    $("#btnCancel").off("click", onClickCancel);
    $("#btnCancel").on("click", onClickCancel);

    $("#btnDelete").off("click", onClickDelete);
    $("#btnDelete").on("click", onClickDelete);

    $("#btnAdd").off("click");
    $("#btnAdd").on("click", onClickAdd);

    $("#btnActive").off("click");
    $("#btnActive").on("click", onClickActive);

    $("#btnInActive").off("click");
    $("#btnInActive").on("click", onClickInActive);
}

function adjustHeight() {
    var defHeight = 220; //+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    angularUIgridWrapper.adjustGridHeight(cmpHeight);
}

var isActive = 1;

function onClickActive() {
    isActive = 1;
    $("#btnInActive").removeClass("selectButtonBarClass");
    $("#btnActive").addClass("selectButtonBarClass");
    init();
}

function onClickInActive() {
    isActive = 0;
    $("#btnActive").removeClass("selectButtonBarClass");
    $("#btnInActive").addClass("selectButtonBarClass");
    init();
}

function buildZipListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    // gridColumns.push({
    //     "title": "Code",
    //     "field": "code",
    // });
    gridColumns.push({
        "title": "Abbreviation",
        "field": "value",
    });
    gridColumns.push({
        "title": "Description",
        "field": "notes",
    });
    angularUIgridWrapper.creategrid(dataSource, gridColumns, otoptions);
    adjustHeight();
    //onIndividualRowClick();
}
var prevSelectedItem = [];

function onChange() {
    setTimeout(function() {
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if (selectedItems && selectedItems.length > 0) {
            $("#btnSave").prop("disabled", false);
            $("#btnDelete").prop("disabled", false);
        } else {
            $("#btnSave").prop("disabled", true);
            $("#btnDelete").prop("disabled", true);
        }
    });
}

function onClickOK() {
    setTimeout(function() {
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if (selectedItems && selectedItems.length > 0) {
            var obj = {};
            obj.selItem = selectedItems[0];
            if ($("#btnSave").attr('data-attr-select')) {
                obj.status = "success";
                var windowWrapper = new kendoWindowWrapper();
                windowWrapper.closePageWindow(obj);
            } else {
                operation = "update";
                parentRef.operation = operation;
                parentRef.selItem = selectedItems[0];
                addMedicalCondition();
            }

        }
    })
}

function onClickDelete() {
    customAlert.confirm("Confirm", "Are you sure you want delete?", function(response) {
        if (response.button == "Yes") {
            var selectedItems = angularUIgridWrapper.getSelectedRows();
            console.log(selectedItems);
            if (selectedItems && selectedItems.length > 0) {
                var selItem = selectedItems[0];
                if (selItem) {
                    var dataUrl = ipAddress + "/homecare/medical-condition-types/";
                    var reqObj = {};
                    reqObj.id = selItem.idk;
                    reqObj.isDeleted = "0";
                    reqObj.isActive = "0";
                    reqObj.modifiedBy = sessionStorage.userId;
                    createAjaxObject(dataUrl, reqObj, "DELETE", onDeleteCountryt, onError);
                }
            }
        }
    });
}

function onDeleteCountryt(dataObj) {
    if (dataObj && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            customAlert.info("info", "Medical Condition Deleted Successfully");
            init();
        } else {
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}

function onClickAdd() {

    operation = "add";
    parentRef.operation = operation;
    parentRef.selItem = null;
    addMedicalCondition();

}
var operation = "";

function addMedicalCondition() {
    var popW = 800;
    var popH = 500;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    if (operation == "add") {
        profileLbl = "Add Medical Condition";
    } else {
        profileLbl = "Edit Medical Condition";
    }
    devModelWindowWrapper.openPageWindow("../../html/masters/medicalCondition.html", profileLbl, popW, popH, true, closeAddMedicalConditionAction);
}

function closeAddMedicalConditionAction(evt, returnData) {
    if (returnData && returnData.status == "success") {
        if (returnData.operation == "add") {
            customAlert.info("Info", "Medical Condition Created Successfully.")
        } else if (returnData.operation == "update") {
            customAlert.info("Info", "Medical Condition Updated Successfully.")
        }
    }
    init();
}

function onClickCancel() {
    var obj = {};
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}
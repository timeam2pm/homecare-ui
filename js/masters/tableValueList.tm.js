var angularUIgridWrapper;
var operation = "";
var parentRef = null;
var atID;
var selItem = null;
var ADD = "add";
var UPDATE = "update";
var VIEW = "view";
var DELETE = "delete";

$(document).ready(function(){
    $("#pnlPatient",parent.document).css("display","none");
    // themeAPIChange();
    // sessionStorage.setItem("IsSearchPanel", "1");
    parentRef = parent.frames['iframe'].window;
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("dgridCountryList", dataOptions);
    angularUIgridWrapper.init();
    buildDeviceListGrid([]);
    // init();
})


$(window).load(function(){
    loading = false;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
    init();
    buttonEvents();
    adjustHeight();
}

function init(){
    buildDeviceListGrid([]);
    getAjaxObject(ipAddress+"/codetable/list/","GET",getCodeTableList,onError);
//    searchOnLoad('active');
}
function onError(errorObj){
    console.log(errorObj);
}
function getCodeTableList(dataObj){
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.codeTableName){
        if($.isArray(dataObj.response.codeTableName)){
            dataArray = dataObj.response.codeTableName;
        }else{
            dataArray.push(dataObj.response.codeTableName);
        }
    }

    dataArray.sort(function (a, b) {
        var nameA = a.tableName.toUpperCase(); // ignore upper and lowercase
        var nameB = b.tableName.toUpperCase(); // ignore upper and lowercase
        if (nameA < nameB) {
            return -1;
        }
        if (nameA > nameB) {
            return 1;
        }
        // names must be equal
        return 0;
    });

    var tempDataArry = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
        }
        if(dataArray[i].tableName.toLowerCase().includes('patient')){
            dataArray[i].tableName = dataArray[i].tableName.toLowerCase().replace("patient","Serviceuser");
        }

        $("#txtTableName").append('<option value="'+dataArray[i].id+'">'+dataArray[i].tableName+'</option>');
        $("#cmbTableName").append('<option value="'+dataArray[i].id+'">'+dataArray[i].tableName+'</option>');
    }
    onTableChange();
}
function onTableChange(){
    var txtTableName = $("#txtTableName").val() || 0;
    buildDeviceListGrid([]);
    if(txtTableName>0){
        getTableValueList('active');
    }
}

function getTableValueList(status){
    var txtTableName = $("#txtTableName option:selected").text();
    buildDeviceListGrid([]);
    if(txtTableName && txtTableName != ""){
        if(txtTableName.includes('Serviceuser')){
            txtTableName = txtTableName.replace("Serviceuser","patient");
        }
        if(status == "active") {
            getAjaxObject(ipAddress+"/master/"+txtTableName+"/list/?is-active=1&is-deleted=0","GET",getCodeTableValueList,onError);
        }
        else if(status == "inactive"){
            getAjaxObject(ipAddress+"/master/"+txtTableName+"/list/?is-active=0&is-deleted=1","GET",getCodeTableValueList,onError);
        }

    }
}
function getCodeTableValueList(dataObj){
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.codeTable){
        if($.isArray(dataObj.response.codeTable)){
            dataArray = dataObj.response.codeTable;
        }else{
            dataArray.push(dataObj.response.codeTable);
        }
    }
    var tempDataArry = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
        }
    }
    buildDeviceListGrid(dataArray);
}
function buttonEvents(){
    $("#btnSave").off("click",onClickSave);
    $("#btnSave").on("click",onClickSave);

    $("#btnEdit").off("click",onClickEdit);
    $("#btnEdit").on("click",onClickEdit);

    $("#btnCancel").off("click",onClickCancel);
    $("#btnCancel").on("click",onClickCancel);

    $("#btnDelete").off("click",onClickDelete);
    $("#btnDelete").on("click",onClickDelete);

    $("#btnAdd").off("click");
    $("#btnAdd").on("click",onClickAdd);

    $("#btnReset").off("click",onClickReset);
    $("#btnReset").on("click",onClickReset);

    $(".popupClose").off("click", onClickCancel);
    $(".popupClose").on("click", onClickCancel);

    $("#txtTableName").on("change",onTableChange);

    $(".btnActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        getTableValueList('active');
        onClickActive();
    });
    $(".btnInActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        getTableValueList('inactive');
        onClickInActive();
    });
}

function adjustHeight(){
    var defHeight = 250;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    angularUIgridWrapper.adjustGridHeight(cmpHeight);
}

function buildDeviceListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Value",
        "field": "value"
    });
    gridColumns.push({
        "title": "Description",
        "field": "desc"
    });
    /*gridColumns.push({
        "title": "Status",
        "field": "Status",
    });	*/
    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}
var prevSelectedItem =[];
function onChange(){
    setTimeout(function(){
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        if(selectedItems && selectedItems.length>0){
            $("#btnEdit").prop("disabled", false);
            $("#btnDelete").prop("disabled", false);
        }else{
            $("#btnEdit").prop("disabled", true);
            $("#btnDelete").prop("disabled", true);
        }
    },100);
}

function onClickOK(){
    setTimeout(function(){
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        if(selectedItems && selectedItems.length>0){
            var txtTableName = $("#txtTableName").data("kendoComboBox");
            var obj = {};
            obj.selItem = selectedItems[0];
            parentRef.selItem = selectedItems[0];
            parentRef.selItem.tbk = txtTableName.value();
            parentRef.operation = "update";
            addValues();
        }
    })
}
function onClickDelete(){
    customAlert.confirm("Confirm", "Are you sure you want delete?",function(response){
        if(response.button == "Yes"){
            var selectedItems = angularUIgridWrapper.getSelectedRows();
            if(selectedItems && selectedItems.length>0){
                var selItem = selectedItems[0];
                if(selItem){
                    var txtTableName = $("#txtTableName").val();


                    var tableName = $("#txtTableName option:selected").text();

                    if(tableName.includes('Serviceuser')){
                        tableName = tableName.replace("Serviceuser","patient");
                    }

                    var dataUrl = ipAddress+"/master/"+tableName+"/delete/";
                    var reqObj = {};
                    reqObj.id = selItem.idk;
                    reqObj.abbr = selItem.abbr;
                    reqObj.country = selItem.country;
                    reqObj.code = selItem.code;
                    reqObj.isDeleted = 1;
                    reqObj.isActive = 0;
                    reqObj.modifiedBy = sessionStorage.userId;//"101";
                    createAjaxObject(dataUrl,reqObj,"POST",onDeleteCountryt,onError);
                }
            }
        }
    });
}
function onDeleteCountryt(dataObj){
    if(dataObj && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            customAlert.info("info", "Table value deleted successfully");
            //init();
            onTableChange();
        }else{
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}
function onClickAdd(){
    onClickReset();
    parentRef.selItem = null;
    $(".filter-heading").html("Add Data");
    var txtTableName = $("#txtTableName").val();
    parentRef.tbk = txtTableName;
    $("#cmbTableName").val(parentRef.tbk);
    parentRef.operation = "add";
    $("#addPopup").show();
    $('#viewDivBlock').hide();
    $("#txtID").hide();
    $("#btnSave").html("Save");

    // addValues();
}

function onClickReset(){
    operation = ADD;
    selItem = null;
    $("#cmbTableName").val("");
    $("#txtValue").val("");
    $("#txtDesc").val("");
    $("#cmbStatus").val(1);
}

function onCloseCodeTableValueAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        if(returnData.operation == "add"){
            customAlert.info("Info","Data created successfully.");
        }else if(returnData.operation == "update"){
            customAlert.info("Info","Data updated successfully.");
        }
        onTableChange();
    }
}
function onClickCancel(){
    $(".filter-heading").html("View Data");
    $("#viewDivBlock").show();
    $("#addPopup").hide();
}

function onClickActive() {
    $(".btnInActive").removeClass("radioButton-active");
    $(".btnActive").addClass("radioButton-active");
}

function onClickInActive() {
    $(".btnActive").removeClass("radioButton-active");
    $(".btnInActive").addClass("radioButton-active");
}

function themeAPIChange(){
    getAjaxObject(ipAddress+"/homecare/settings/?id=2","GET",getThemeValue,onError);
}


function getThemeValue(dataObj){
    var tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.activityTypes){
            if($.isArray(dataObj.response.settings)){
                tempCompType = dataObj.response.settings;
            }else{
                tempCompType.push(dataObj.response.settings);
            }
        }
        if(tempCompType.length > 0){
            themesChange(tempCompType[0].value);
        }
    }
}

function themesChange(themeValue){
    if(themeValue == 2){
        loadAPi("../../Theme2/Theme02.css");
    }
    else if(themeValue == 3){
        loadAPi("../../Theme3/Theme03.css");
    }
}


function onClickEdit(){
    $("#txtID").show();
    $(".filter-heading").html("Edit Data");
    parentRef.operation = "edit";
    $("#viewDivBlock").hide();
    $("#addPopup").show();
    var selectedItems = angularUIgridWrapper.getSelectedRows();
    if (selectedItems && selectedItems.length > 0) {
        var obj = selectedItems[0];
        atID = obj.idk;
        $("#txtID").html("ID : " + obj.idk);
        $("#cmbStatus").val(obj.isActive);
        $("#txtValue").val(obj.value);
        $("#txtDesc").val(obj.desc);
        var txtTableName = $("#txtTableName").val();
        parentRef.tbk = txtTableName;
        $("#cmbTableName").val(parentRef.tbk);
    }
}


function onClickSave(){
    if(validation()){
        var txtTableName = $("#cmbTableName").val();
        var selTableName = $("#cmbTableName option:selected").text();
        if(selTableName.includes('Serviceuser')){
            selTableName = selTableName.replace("Serviceuser","patient");
        }
        var strValue = $("#txtValue").val();
        strValue = $.trim(strValue);
        var strDesc = $("#txtDesc").val();
        strDesc = $.trim(strDesc);
        var isActive = 0;
        isActive = $("#cmbStatus").val();

        var dataObj = {};
        dataObj.value = strValue;
        dataObj.note = strDesc;
        dataObj.desc = strDesc;
        dataObj.isActive = isActive;
        dataObj.createdBy = sessionStorage.userId;//"101";//sessionStorage.uName;
        if(isActive == 1){
            dataObj.isDeleted = "0";
        }
        else{
            dataObj.isDeleted = "1";
        }

        if(operation == ADD){
            var dataUrl = ipAddress+"/master/"+selTableName+"/create";
            createAjaxObject(dataUrl,dataObj,"POST",onCreate,onError);
        }else {
            dataObj.id = atID;
            var dataUrl = ipAddress+"/master/"+selTableName+"/update";
            createAjaxObject(dataUrl,dataObj,"POST",onCreate,onError);
        }

    }
}

function onCreate(dataObj){
    var rows ="";
    if(dataObj && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            var msg;
            if(operation == ADD){
                msg = "Data Created Successfully.";
            }
            else{
                msg = "Data Updated Successfully.";
            }
            displaySessionErrorPopUp("Info", msg, function(res) {
                operation = ADD;
                // init();
                // buildDeviceListGrid([]);
                $("#btnCancel").click();
                // $("#txtTableName").val(parseInt($("cmbTableName").val()));
                // onClickActive();
                $(".btnActive").click();
            })
        }else{
            customAlert.error("error", dataObj.response.status.message);
        }
    }

}


function validation(){
    var flag = true;
    var txtTableName = $("#cmbTableName option:selected").text();
    if(txtTableName && txtTableName == ""){
        customAlert.error("Error","Select Table Name");
        flag = false;
        return false;
    }
    var strValue = $("#txtValue").val();
    strValue = $.trim(strValue);
    if(strValue == ""){
        customAlert.error("Error","Please Enter Abbrevation");
        flag = false;
        return false;
    }

    var strDesc = $("#txtDesc").val();
    strDesc = $.trim(strDesc);
    if(strDesc == ""){
        customAlert.error("Error","Please Enter Description");
        flag = false;
        return false;
    }

    return flag;
}

var angularUIgridWrapper;
var parentRef = null;
var recordType = "1";
var dataArray = [];
var ADD = "add";
var UPDATE = "edit";
var VIEW = "view";
var DELETE = "delete";
var operation = "add";
var atID = "";
var ds  = "";

var IsFlag = 1;


$(document).ready(function(){
    $("#pnlPatient",parent.document).css("display","none");
    themeAPIChange();
    parentRef = parent.frames['iframe'].window;
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("dgridBDYList", dataOptions);
    angularUIgridWrapper.init();
    buildDeviceListGrid([]);

});


$(window).load(function(){
    loading = false;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
    init();
    buttonEvents();
    adjustHeight();
}

function init(){
    $("#btnEdit").prop("disabled", true);
    $("#btnDelete").prop("disabled", true);
    buildDeviceListGrid([]);
    var strTableName = "body_parts";
    getAjaxObject(ipAddress+"/master/"+strTableName+"/list/?is-active=1","GET",getCodeTableValueList,onError);
}


function buttonEvents(){

    $("#btnSave").off("click",onClickSave);
    $("#btnSave").on("click",onClickSave);

    $("#btnEdit").off("click",onClickEdit);
    $("#btnEdit").on("click",onClickEdit);

    $("#btnDelete").off("click",onClickDelete);
    $("#btnDelete").on("click",onClickDelete);

    $("#btnAdd").off("click");
    $("#btnAdd").on("click",onClickAdd);

    $("#btnCancel").off("click",onClickCancel);
    $("#btnCancel").on("click",onClickCancel);

    $("#btnReset").off("click",onClickReset);
    $("#btnReset").on("click",onClickReset);

    $(".popupClose").off("click", onClickCancel);
    $(".popupClose").on("click", onClickCancel);

    $(".btnActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('active');
        onClickActive();
    });
    $(".btnInActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('inactive');
        onClickInActive();
    });

    allowAlphabets("txtAbbreviation");
    allowAlphaNumericwithSapce("txtDescription");
}


function searchOnLoad(status) {
    buildDeviceListGrid([]);
    var strTableName = "body_parts";
    if(status == "active") {
        var urlExtn = ipAddress+"/master/"+strTableName+"/list/?is-active=1&is-deleted=0";

    }
    else if(status == "inactive") {
        var urlExtn =  ipAddress+"/master/"+strTableName+"/list/?is-active=0&is-deleted=1";
    }

    getAjaxObject(urlExtn,"GET",getCodeTableValueList,onError);

}


function onClickSave(){
    if(validation()){
        var txtTableName = "body_parts";//$("#txtTableName").data("kendoComboBox");
        var strValue = $("#txtAbbreviation").val();
        strValue = $.trim(strValue);
        var strDesc = $("#txtDescription").val();
        strDesc = $.trim(strDesc);
        // var isActive = 0;
        // var cmbStatus = $("#cmbStatus").data("kendoComboBox");
        // if(cmbStatus){
        //     if(cmbStatus.selectedIndex == 0){
        //         isActive = 1;
        //     }
        // }
        var dataObj = {};
        dataObj.value = strValue;
        dataObj.note = strDesc;
        dataObj.desc = strDesc;
        dataObj.isActive = parseInt($("#cmbStatus").val());
        dataObj.createdBy = sessionStorage.userId;//"101";//sessionStorage.uName;
        if(parseInt($("#cmbStatus").val()) == 1){
            dataObj.isDeleted = "0";
        }
        else{
            dataObj.isDeleted = "1";
        }

        if(operation == ADD){
            var dataUrl = ipAddress+"/master/"+txtTableName+"/create";
            createAjaxObject(dataUrl,dataObj,"POST",onCreate,onError);
        }else{
            dataObj.id = atID;
            var dataUrl = ipAddress+"/master/"+txtTableName+"/update";
            createAjaxObject(dataUrl,dataObj,"POST",onCreate,onError);
        }

    }
}

function validation(){
    var flag = true;

    var strValue = $("#txtAbbreviation").val();
    strValue = $.trim(strValue);
    if(strValue == ""){
        customAlert.error("Error","Please Enter Abbreviation");
        flag = false;
        return false;
    }

    var strDesc = $("#txtDescription").val();
    strDesc = $.trim(strDesc);
    if(strDesc == ""){
        customAlert.error("Error","Please Enter Description");
        flag = false;
        return false;
    }

    return flag;
}

function onCreate(dataObj){

    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "Body Part created successfully";
            if(operation == UPDATE){
                msg = "Body Part updated successfully"
            }else{

            }
            displaySessionErrorPopUp("Info", msg, function(res) {
                operation = ADD;
                init();
                $("#btnCancel").click();
                onClickActive();
            })
        }else{
            customAlert.error("Error", dataObj.response.status.message);
        }
    }else{

    }
    onClickReset();
}

function adjustHeight(){
    var defHeight = 230;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    angularUIgridWrapper.adjustGridHeight(cmpHeight);
}

function onClickEdit(){
    $("#txtID").show();
    $(".filter-heading").html("Edit Body Part");
    parentRef.operation = "edit";
    $("#viewDivBlock").hide();
    $("#addPopup").show();
    var selectedItems = angularUIgridWrapper.getSelectedRows();
    if (selectedItems && selectedItems.length > 0) {
        var obj = selectedItems[0];
        atID = obj.idk;
        $("#txtAbbreviation").val(obj.value);
        $("#txtDescription").val(obj.desc);
        $("#txtID").html("ID : " + obj.idk);
        $("#cmbStatus").val(obj.isActive);
    }
}

var operation = "add";
var selFileResourceDataItem = null;

function onStatusChange(){
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if(cmbStatus && cmbStatus.selectedIndex<0){
        cmbStatus.select(0);
    }
}


function onClickReset() {
    if(operation == ADD) {
        operation = ADD;
    }
    else {
        operation = UPDATE;
    }
    $("#txtAbbreviation").val("");
    $("#txtDescription").val("");
    $("#cmbStatus").val(1);

}


function onError(errorObj){
    console.log(errorObj);
}
function getCodeTableList(dataObj){
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.codeTableName){
        if($.isArray(dataObj.response.codeTableName)){
            dataArray = dataObj.response.codeTableName;
        }else{
            dataArray.push(dataObj.response.codeTableName);
        }
    }
    var tempDataArry = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
        }
    }
    var obj = {};
    obj.tableName = "";
    obj.idk = "";
    dataArray.unshift(obj);
    setDataForSelection(dataArray, "txtTableName", onTableChange, ["tableName", "idk"], 0, "");
    onTableChange();
}
function onTableChange(){
    var txtTableName = $("#txtTableName").data("kendoComboBox");
    if(txtTableName && txtTableName.selectedIndex<0){
        txtTableName.select(0);
    }
    buildDeviceListGrid([]);
    if(txtTableName.selectedIndex>0){
        getTableValueList();
    }

}

function getCodeTableValueList(dataObj){
    console.log(dataObj);
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.codeTable){
        if($.isArray(dataObj.response.codeTable)){
            dataArray = dataObj.response.codeTable;
        }else{
            dataArray.push(dataObj.response.codeTable);
        }
    }
    var tempDataArry = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
        }
    }
    buildDeviceListGrid(dataArray);
}


function adjustHeight(){
    var defHeight = 200;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    angularUIgridWrapper.adjustGridHeight(cmpHeight);
}

function buildDeviceListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Abbreviation",
        "field": "value"
    });
    gridColumns.push({
        "title": "Description",
        "field": "desc"
    });
    /*gridColumns.push({
     "title": "Status",
     "field": "Status"
     });	*/
    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}
var prevSelectedItem =[];
function onChange(){
    setTimeout(function(){
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if(selectedItems && selectedItems.length>0){
            $("#btnEdit").prop("disabled", false);
            $("#btnDelete").prop("disabled", false);
        }else{
            $("#btnEdit").prop("disabled", true);
            $("#btnDelete").prop("disabled", true);
        }
    },100);
}

function onClickOK(){
    setTimeout(function(){
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if(selectedItems && selectedItems.length>0){
            // var txtTableName = $("#txtTableName").data("kendoComboBox");
            var obj = {};
            obj.selItem = selectedItems[0];
            parentRef.selItem = selectedItems[0];
            // parentRef.selItem.tbk = txtTableName.value();
            parentRef.operation = "update";
            addValues();
        }
    })
}
function onClickDelete(){
    customAlert.confirm("Confirm", "Are you sure you want delete?",function(response){
        if(response.button == "Yes"){
            var selectedItems = angularUIgridWrapper.getSelectedRows();
            console.log(selectedItems);
            if(selectedItems && selectedItems.length>0){
                var selItem = selectedItems[0];
                if(selItem){
                    // var txtTableName = $("#txtTableName").data("kendoComboBox");
                    var tableName = "body_parts";//txtTableName.text();
                    var dataUrl = ipAddress+"/master/"+tableName+"/delete/";
                    var reqObj = {};
                    reqObj.id = selItem.idk;
                    reqObj.abbr = selItem.abbr;
                    reqObj.country = selItem.country;
                    reqObj.code = selItem.code;
                    reqObj.isDeleted = 1;
                    reqObj.isActive = 0;
                    reqObj.modifiedBy = sessionStorage.userId;//"101";
                    createAjaxObject(dataUrl,reqObj,"POST",onDeleteCountryt,onError);
                }
            }
        }
    });
}
function onDeleteCountryt(dataObj){
    if(dataObj && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            msg = "Body Part deleted successfully";
            displaySessionErrorPopUp("Info", msg, function(res) {
                init();
            })
        }else{
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}
function onClickAdd(){
    parentRef.selItem = null;
    $("#addPopup").show();
    $('#viewDivBlock').hide();
    $("#txtID").hide();
    $(".filter-heading").html("Add Body Part");
    onClickReset();
    parentRef.operation = "add";
}

function onClickCancel(){
    $(".filter-heading").html("View Body Parts");
    $("#viewDivBlock").show();
    $("#addTaskGroupPopup").hide();
}

function onClickActive() {
    $(".btnInActive").removeClass("radioButton-active");
    $(".btnActive").addClass("radioButton-active");
}

function onClickInActive() {
    $(".btnActive").removeClass("radioButton-active");
    $(".btnInActive").addClass("radioButton-active");
}

function themeAPIChange(){
    getAjaxObject(ipAddress+"/homecare/settings/?id=2","GET",getThemeValue,onError);
}


function getThemeValue(dataObj){
    console.log(dataObj);
    var tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.activityTypes){
            if($.isArray(dataObj.response.settings)){
                tempCompType = dataObj.response.settings;
            }else{
                tempCompType.push(dataObj.response.settings);
            }
        }
        if(tempCompType.length > 0){
            themesChange(tempCompType[0].value);
        }
    }
}

function themesChange(themeValue){
    if(themeValue == 2){
        loadAPi("../../Theme2/Theme02.css");
    }
    else if(themeValue == 3){
        loadAPi("../../Theme3/Theme03.css");
    }
}
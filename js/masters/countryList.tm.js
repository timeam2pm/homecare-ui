var angularUIgridWrapper;
var parentRef = null;

$(document).ready(function(){
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("dgridCountryList", dataOptions);
    angularUIgridWrapper.init();
    buildDeviceListGrid([]);
});


$(window).load(function(){
    loading = false;
    parentRef = parent.frames['iframe'].window;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
    init();
    buttonEvents();
    adjustHeight();
}

function init(){
    $("#btnSave").prop("disabled", true);
    $("#btnDelete").prop("disabled", true);
    buildDeviceListGrid([]);
    getAjaxObject(ipAddress+"/country/list/","GET",getCountryList,onError);
}
function onError(errorObj){
    console.log(errorObj);
}
function getCountryList(dataObj){
    console.log(dataObj);
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.country){
        if($.isArray(dataObj.response.country)){
            dataArray = dataObj.response.country;
        }else{
            dataArray.push(dataObj.response.country);
        }
    }
    var tempDataArry = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
        }
    }
    buildDeviceListGrid(dataArray);
}
function buttonEvents(){
    $("#btnSave").off("click",onClickOK);
    $("#btnSave").on("click",onClickOK);

    $("#btnCancelDet").off("click",onClickCancel);
    $("#btnCancelDet").on("click",onClickCancel);

    $("#btnDelete").off("click",onClickDelete);
    $("#btnDelete").on("click",onClickDelete);

    $("#btnAdd").off("click");
    $("#btnAdd").on("click",onClickAdd);
}

function adjustHeight(){
    var defHeight = 150;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    angularUIgridWrapper.adjustGridHeight(cmpHeight);
}

function buildDeviceListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Abbreviation",
        "field": "abbr",
        "width":"25%"
    });
    gridColumns.push({
        "title": "Calling Code",
        "field": "code",
        "width":"25%"
    });
    gridColumns.push({
        "title": "Country",
        "field": "country",
        "width":"25%"
    });
    gridColumns.push({
        "title": "Status",
        "field": "Status",
        "width":"25%"
    });
    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}
var prevSelectedItem =[];
function onChange(){
    setTimeout(function(){
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        if(selectedItems && selectedItems.length>0){
            $("#btnSave").prop("disabled", false);
            $("#btnDelete").prop("disabled", false);
        }else{
            $("#btnSave").prop("disabled", true);
            $("#btnDelete").prop("disabled", true);
        }
    })
}

function onClickOK(){
    setTimeout(function(){
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if(selectedItems && selectedItems.length>0){
            var obj = {};
            obj.selItem = selectedItems[0];
            operation = "update";
            parentRef.operation = "update";
            parentRef.selItem = selectedItems[0];;
            addCountry();
        }
    })
}
function onClickDelete(){
    customAlert.confirm("Confirm", "Are you sure you want delete?",function(response){
        if(response.button == "Yes"){
            var selectedItems = angularUIgridWrapper.getSelectedRows();
            console.log(selectedItems);
            if(selectedItems && selectedItems.length>0){
                var selItem = selectedItems[0];
                if(selItem){
                    var dataUrl = ipAddress+"/country/delete/";
                    var reqObj = {};
                    reqObj.id = selItem.idk;
                    reqObj.abbr = selItem.abbr;
                    reqObj.country = selItem.country;
                    reqObj.code = selItem.code;
                    reqObj.isDeleted = "1";
                    reqObj.modifiedBy = sessionStorage.userId
                    createAjaxObject(dataUrl,reqObj,"POST",onDeleteCountryt,onError);
                }
            }
        }
    });
}
function onDeleteCountryt(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            customAlert.info("info", "Country Deleted Successfully");
            init();
        }else{
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}
var operation = "add";
function onClickAdd(){
    operation = "add";
    parentRef.operation = "add";
    parentRef.selItem = null;
    addCountry();
}
function addCountry(){
    var popW = 600;
    var popH = 500;

    var profileLbl;
    if(operation == "add"){
        profileLbl = "Add Country";
    }else{
        profileLbl = "Edit Country";
    }

    var devModelWindowWrapper = new kendoWindowWrapper();

    devModelWindowWrapper.openPageWindow("../../html/masters/createCountry.html", profileLbl, popW, popH, true, closeAddAction);
}
function closeAddAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        if(returnData.operation == "add"){
            customAlert.info("Info","Country Created Successfully.");
        }else if(returnData.operation == "update"){
            customAlert.info("Info","Country Updated Successfully.");
        }
        init();
    }
}
function onClickCancel(){
    var obj = {};
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}

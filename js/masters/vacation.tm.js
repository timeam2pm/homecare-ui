var parentRef = null;
$(document).ready(function() {
    parentRef = parent.frames['iframe'].window;
    var patientID = parentRef.patientId;
    var operation = parentRef.operation;
    init();
    function init() {
        bindEvents();
    };
    function bindEvents() {
        $(".datepicker").kendoDatePicker();
        $('.contentWrapper-link').on('click', function() {
            $('.contentWrapper-link').removeClass('activeLink');
            $(this).addClass('activeLink');
            $('.tabcontent').hide();
            if($(this).attr('data-attr-link') == "1") {
                $('[data-attr-content="1"]').show();
                $('#updateVacation').hide();
                $('#applyVacation').show();
                $('.vacation-table-content').find('input, textarea').val('');
                getAjaxObject(ipAddress+"/homecare/vacations/?parent-id="+patientID+"&parent-type-id=500&is-active=1","GET",getVacationList,onError);
            } else if($(this).attr('data-attr-link') == "2") {
                $('[data-attr-content="2"]').show();
            }
        });
        $(document).on('click', '#addVacationList ul', function() {
            if($(this).hasClass('selectedList')) {
                $('#addVacationList ul').removeClass('selectedList');
                $('.updateVacationWrapper').hide();
            } else {
                $('#addVacationList ul').removeClass('selectedList');
                $(this).addClass('selectedList');
                $('.updateVacationWrapper').show();
            }
        });
        $('#applyVacation').off("click", onClickApply);
        $('#applyVacation').on("click", onClickApply);
        $('#updateVacation').off("click", onClickUpdate);
        $('#updateVacation').on("click", onClickUpdate);
        $('#showVacation').off("click", onClickShowVacation);
        $('#showVacation').on("click", onClickShowVacation);
        getAjaxObject(ipAddress+"/homecare/vacations/?parent-id="+patientID+"&parent-type-id=500&is-active=1","GET",getVacationList,onError);
    };
    function getVacationList(dataObj) {
        console.log(dataObj);
        var dataArray = [];
        if (dataObj && dataObj.response && dataObj.response.vacations) {
            if($.isArray(dataObj.response.vacations)){
                dataArray = dataObj.response.vacations;
            } else{
                dataArray.push(dataObj.response.vacations);
            }
            $('#addVacationList').empty();
            for(var i=0;i<dataArray.length;i++){
                dataArray[i].idk = dataArray[i].id;
                dataArray[i].Status = "InActive";
                if(dataArray[i].isActive == 1){
                    dataArray[i].Status = "Active";
                    var createdDate = new Date(dataArray[i].createdDate);
                    var createdDateString = createdDate.toLocaleDateString();
                    var modifiedDate = new Date(dataArray[i].modifiedDate);
                    var modifiedDateString = modifiedDate.toLocaleDateString();
                    dataArray[i].createdDateString = createdDateString;
                    dataArray[i].modifiedDateString = modifiedDateString;
                }
                var fromDateVal = dataArray[i].fromDate != "" ? new Date(dataArray[i].fromDate).toLocaleDateString().split('/').join('-') : '';
                var toDateVal = dataArray[i].toDate != "" ? new Date(dataArray[i].toDate).toLocaleDateString().split('/').join('-') : '';
                var reasonVal = dataArray[i].reason != null ? dataArray[i].reason : '';
                var approvedByVal = dataArray[i].approvedBy != null ? dataArray[i].approvedBy : '';
                $('#addVacationList').append('<ul data-attr-vacationid="'+dataArray[i].idk+'"><li data-attr-fromdate="'+fromDateVal+'">'+fromDateVal+'</li><li data-attr-todate="'+toDateVal+'">'+toDateVal+'</li><li data-attr-reason="'+reasonVal+'">'+reasonVal+'</li><li data-attr-approvedby="'+approvedByVal+'">'+approvedByVal+'</li><li></li></ul>');
            }
        } else {
            // customAlert.error("error", dataObj.response.status.message);
        }
    }
    function onClickApply() {
        onAddUpApply('add');
    };
    function onClickUpdate() {
        onAddUpApply('update');
    };
    function onClickShowVacation() {
        $('[data-attr-link="2"]').trigger('click');
        $('[name="vacation-fromdate"]').val($('#addVacationList').find('.selectedList').find('[data-attr-fromdate]').attr('data-attr-fromdate'));
        $('[name="vacation-todate"]').val($('#addVacationList').find('.selectedList').find('[data-attr-todate]').attr('data-attr-todate'));
        $('[name="vacation-reason"]').val($('#addVacationList').find('.selectedList').find('[data-attr-reason]').attr('data-attr-reason'));
        $('#updateVacation').show();
        $('#applyVacation').hide();
    }
    function onAddUpApply(testString) {
        var toDateVal = '';
        var fromDateVal = '';
        if($('[name="vacation-todate"]').val() != "" && new Date($('[name="vacation-todate"]').val().split('/').join('-')) != "Invalid Date") {
            toDateVal = new Date($('[name="vacation-todate"]').val().split('/').join('-')).getTime();
        }
        if($('[name="vacation-fromdate"]').val() != "" && new Date($('[name="vacation-fromdate"]').val().split('/').join('-')) != "Invalid Date") {
            fromDateVal = new Date($('[name="vacation-fromdate"]').val().split('/').join('-')).getTime();
        }
        var dataUrl = ipAddress + '/homecare/vacations/';
        var vacObj = {
            "reason": $('[name="vacation-reason"]').val(),
            "parentTypeId": 500,
            "toDate": toDateVal,
            "isActive": 1,
            "parentId": patientID,
            "fromDate": fromDateVal,
            "isDeleted": 0
        };
        if(testString == "add") {
            vacObj.createdDate = new Date().getTime();
            vacObj.createdBy = Number(sessionStorage.userId);
            createAjaxObject(dataUrl, vacObj, "POST", onCreate, onError);
        } else if(testString == "update") {
            vacObj.modifiedDate = new Date().getTime();
            vacObj.modifiedBy = Number(sessionStorage.userId);
            vacObj.id = Number($('#addVacationList').find('.selectedList').attr('data-attr-vacationId'));
            createAjaxObject(dataUrl, vacObj, "PUT", onUpdate, onError);
        }
        /*"vacation": $('[name="vacation-subject"]').val()
        "approvedBy": Number(sessionStorage.userId),*/
    }
    function onCreate(dataObj) {
        console.log(dataObj);
        if (dataObj && dataObj.response && dataObj.response.status) {
            if (dataObj.response.status.code == "1") {
                var obj = {};
                obj.status = "success";
                obj.operation = operation;
                popupClose(obj);
            } else {
                customAlert.error("error", dataObj.response.status.message);
            }
        }
    }
    function onUpdate(dataObj) {
        console.log(dataObj);
        if (dataObj && dataObj.response && dataObj.response.status) {
            if (dataObj.response.status.code == "1") {
                var obj = {};
                obj.status = "success";
                obj.operation = operation;
                //popupClose(obj);
            } else {
                customAlert.error("error", dataObj.response.status.message);
            }
        }
    }
    function onError(errObj) {
        console.log(errObj);
        customAlert.error("Error", "Error");
    }
});
var angularUIgridWrapper;

var selGroup = "Group";

$(document).ready(function(){

    setTimeout(function() {
        $('.txtBlue').parent().find('.k-in').addClass('abc');
    }, 5000);
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("dgridUserList", dataOptions);
    angularUIgridWrapper.init();
    buildScreenUserList([]);

    setDataForSelection([], "txtGroup", function(){}, ["", ""], 0, "");

    $(".treeview-sprites-menu").kendoContextMenu({
        // listen to right-clicks on treeview container
        target: ".treeview-sprites",

        // show when node text is clicked
        filter: ".k-in",

        // handle item clicks
        change: function(e) {
            /*$("#DivGroup").hide();
            $("#divOK").hide();
            $("#DivItem").hide();*/
            console.log(e);
        },
        open: function(e) {
            /* $("#DivGroup").hide();
             $("#divOK").hide();
             $("#DivItem").hide();*/
            console.log(e);
            var tv = $('.treeview-sprites').data('kendoTreeView'),
                selected = tv.select(),
                item = tv.dataItem(selected);
            node = $(e.target);
            console.log(node.text());
            var str1 = $(node).html();
            if (str1.indexOf("rootfolder") < 0) {
                setTimeout(function() {
                    var mn = $(".treeview-sprites-menu").data("kendoContextMenu");
                    mn.close();
                }, 1);
            }
        },
        select: function(e) {
            var button = $(e.item);
            node = $(e.target);
            // selGroup = button.text();
            /*  $("#DivGroup").hide();
              $("#divOK").hide();
              $("#DivItem").hide();*/
            if (button.text() == "Delete") {
                console.log("Delete");
                //customAlert.confirm("Confirm", "Are you sure you want delete?",function(response){
                var popW = "500";
                var popH = "150";

                var profileLbl;
                var devModelWindowWrapper = new kendoWindowWrapper();
                profileLbl = "Confirm";
                devModelWindowWrapper.openPageWindow("../../html/masters/customConfirmPopup.html", profileLbl, popW, popH, true, function(evt,re){
                    if(re.status == "true"){
                        var treeView = $("#treeview").data("kendoTreeView");
                        if(treeView){
                            var parentNode = getTreeParentNode();
                            var selectedNode = treeView.select();
                            if(selectedNode.text() != "Root"){
                                console.log(selectedNode);
                                var dItem = treeView.dataItem(selectedNode);
                                if(dItem && dItem.items && dItem.items.length == 0){
                                    var arr = [];
                                    selTreeNodeArray = [];
                                    var arrItem = {};
                                    arrItem.id = dItem.idk;
                                    arrItem.parentId = null;
                                    arrItem.modifiedBy = Number(sessionStorage.userId);
                                    arr.push(arrItem);
                                    var me = "PUT";
                                    var dObj = arr;//selTreeNodeArray;
                                    console.log(dObj);
                                    var dataUrl = ipAddress + "/homecare/tenant-users/batch/";
                                    createAjaxObject(dataUrl, dObj, me, onManagerCreate, onError);
                                }
                                //treeView.remove(selectedNode);
                                //var selItem = treeView.findByText(parentNode.text);
                                //treeView.select(selItem);
                            }
                        }
                    }
                });

                //});


                /*$("#DivGroup").show();
                $("#divOK").show();*/
                wrapperElem = node.closest('.master-party-content').attr('data-content-data-type');
                $('[data-content-data-type="'+wrapperElem+'"]').find('.box.wide').show();
            } else {
                /*$("#DivItem").show();
                $("#divOK").show();*/
                //getSelectItemUsers();
                $('#invoice-add').trigger('click');
            }
        }
    });
});
var selTreeNodeArray = [];
function getSelectedTreeItems(dItem){
    if(dItem.type == "Item"){
        var obj = {};
        obj.id = dItem.id;
        obj.parentId = null;
        obj.modifiedBy = Number(sessionStorage.userId);
        selTreeNodeArray.push(obj);
    }else if(dItem.type == "Group"){
        var items = dItem.items;
        if(items.length>0){
            for(var i=0;i<items.length;i++){
                var aItem = items[i];
                if(aItem && aItem.type == "Item"){
                    var obj = {};
                    obj.id = dItem.id;
                    obj.parentId = null;
                    obj.modifiedBy = Number(sessionStorage.userId);
                    selTreeNodeArray.push(obj);
                }
            }
        }
    }
}
function getTreeParentNode(){
    var treeView = $("#treeview").data("kendoTreeView");
    var treeDataItem = treeView.dataItem(treeView.select());
    var parentNode = treeView.dataItem(treeView.parent(treeView.select()));
    return parentNode;
}

$(window).load(function(){
    loading = false;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
    init();
    buttonEvents();
    adjustHeight();
}

function init(){
    //getAjaxObject(ipAddress+"/codetable/list/","GET",getCountryList,onError);
    var panelHeight = window.innerHeight-100;
    $("#divScreenList").height(panelHeight);
    $("#divScreenTreeList").height(panelHeight);
    $("#treeview").height(panelHeight-15);

    var userList = [];
    userList[0] = {uName:'Nurse1',Create:true,Delete:false,View:true};
    userList[1] = {uName:'Krishna',Create:true,Delete:true,View:true};
    userList[2] = {uName:'Nurse2',Create:false,Delete:false,View:true};
    //buildScreenUserList(userList);


    getUserTypes();
}
function getUserTypes(){
    getAjaxObject(ipAddress+"/homecare/user-types/?is-active=1&is-deleted=0","GET",onGetUserTypes,onError);
}
var userTypeArray = [];
var localuserTypeArray = [];
function onGetUserTypes(dataObj){
    // console.log(dataObj);
    userTypeArray = [];
    localuserTypeArray = [];

    var array = {};
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1" && dataObj.response.userTypes){
        if($.isArray(dataObj.response.userTypes)){
            userTypeArray = dataObj.response.userTypes;
        }else{
            userTypeArray.push(dataObj.response.userTypes);
        }
    }
    for(var i=0;i<userTypeArray.length;i++) {
        if(userTypeArray[i].id != 500 && userTypeArray[i].id != 600) {
            array = userTypeArray[i];
            array.value = userTypeArray[i].value.toUpperCase();
            // if (userTypeArray[i].value == "PATIENT") {
            // 	userTypeArray[i].value = "SERVICE USER";
            // }
            if (userTypeArray[i].value.toLowerCase() == "admin") {

            }
            else{
                if (userTypeArray[i].value.toLowerCase() == "care giver") {
                    array.value = "CAREGIVER";
                }

                localuserTypeArray.push(array);
            }
        }
    }
    setDataForSelection(localuserTypeArray, "txtGroup", function(){}, ["value", "id"], 0, "");
    getTreeList();
}
var userTypeId;
function getSelectItemUsers(){
    $(".createManagerRight").css("display","");
    $(".btnCenterAlign").css("display","");
    buildScreenUserList([]);
    var selItem = getSelectTreeNode("treeview");
    // console.log(selItem);
    if(selItem){
        var idk = getUserTypeIdByValue(selItem.text);
        userTypeId = idk;
        getAjaxObject(ipAddress+"/homecare/tenant-users/?user-type-id="+idk+"&is-active=1&is-deleted=0","GET",onGetSelUsers,onError);
    }
}
function onGetSelUsers(dataObj){
    console.log(dataObj);
    var userTypeUsersArray = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1" && dataObj.response.tenantUsers){
        if($.isArray(dataObj.response.tenantUsers)){
            userTypeUsersArray = dataObj.response.tenantUsers;
        }else{
            userTypeUsersArray.push(dataObj.response.tenantUsers);
        }
    }
    var LtreeUserArray = _.where(treeUserArray, {userTypeId: userTypeId})
    ///var val1 = _.difference(userTypeUsersArray, LtreeUserArray);

    for(var i=0;i<userTypeUsersArray.length;i++){
        userTypeUsersArray[i].idk = userTypeUsersArray[i].id;
        var fName = "";
        var mName = "";
        var lName = "";
        if(userTypeUsersArray[i].firstName){
            fName = userTypeUsersArray[i].firstName;
        }
        if(userTypeUsersArray[i].middleName){
            mName = userTypeUsersArray[i].middleName;
        }
        if(userTypeUsersArray[i].lastName){
            lName = userTypeUsersArray[i].lastName;
        }
        userTypeUsersArray[i].uName = fName+" "+mName+" "+lName;
    }

    var val1 =  userTypeUsersArray.filter(o => !LtreeUserArray.find(o2 => o.id === o2.id))

    buildScreenUserList(val1);
}
function getTreeList(){
    getAjaxObject(ipAddress+"/homecare/tenant-users/?parent-id=:ge:0&is-active=1&is-deleted=0","GET",getTenantUsers,onError);
}
var treeUserArray = [];
var treeDataSource = [{ text: "Root", menu:"true",type:"ROOT",expanded: true,spriteCssClass: "rootfolder",items:[]}];
function getTenantUsers(dataObj){
    console.log(dataObj);
    treeUserArray = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1" && dataObj.response.tenantUsers){
        if($.isArray(dataObj.response.tenantUsers)){
            treeUserArray = dataObj.response.tenantUsers;
        }else{
            treeUserArray.push(dataObj.response.tenantUsers);
        }
    }

    var treeArray = [];
    var duplicate = 0;
    var uduplicate = 0;
    var duplicate200 = 0;
    var duplicate201 = 0;
    var duplicate500 = 0;
    var duplicate600 = 0;
    var duplicate701 = 0;
    var duplicate800 = 0;
    for(var i=0;i<treeUserArray.length;i++){

        var uItem = treeUserArray[i];
        if(uItem && uItem.parentId == 0){
            var pItem = {};
            if(uItem.userTypeId == 700 && duplicate == 0) {
                /* i = i++;*/
                duplicate = duplicate + 1;
                var response = _.where(treeArray, {id: uItem.id, userTypeId: 700});
                // if(response && response.length > 0 && duplicate == 0){
                //
                // }
                // else{
                duplicate = duplicate + 1;
                var pId = getUserTypeName(uItem.userTypeId);
                pItem.text = pId;
                pItem.items = getParentChildItems(uItem.parentId, uItem.userTypeId);
                pItem.idk = uItem.id;
                pItem.spriteCssClass = "rootfolder txtBlue";
                pItem.pidk = uItem.parentId;
                pItem.universalUserId = uItem.universalUserId;
                pItem.userTypeId = uItem.userTypeId;
                pItem.type = "Group";
                treeArray.push(pItem);
                // }

            }
            else if(uItem.userTypeId == 100 && uduplicate == 0) {
                /* i = i++;*/
                uduplicate = uduplicate + 1;
                var pId = getUserTypeName(uItem.userTypeId);
                pItem.text = pId;
                pItem.items = getParentChildItems(uItem.parentId, uItem.userTypeId);
                pItem.idk = uItem.id;
                pItem.spriteCssClass = "rootfolder txtBlue";
                pItem.pidk = uItem.parentId;
                pItem.universalUserId = uItem.universalUserId;
                pItem.userTypeId = uItem.userTypeId;
                pItem.type = "Group";
                treeArray.push(pItem);
            }
            else if(uItem.userTypeId == 200 && duplicate200 == 0) {
                /* i = i++;*/
                duplicate200 = duplicate200 + 1;
                var pId = getUserTypeName(uItem.userTypeId);
                pItem.text = pId;
                pItem.items = getParentChildItems(uItem.parentId, uItem.userTypeId);
                pItem.idk = uItem.id;
                pItem.spriteCssClass = "rootfolder txtBlue";
                pItem.pidk = uItem.parentId;
                pItem.universalUserId = uItem.universalUserId;
                pItem.userTypeId = uItem.userTypeId;
                pItem.type = "Group";
                treeArray.push(pItem);
            }
            else if(uItem.userTypeId == 201) {

                var response = _.where(treeArray, {id: uItem.id, userTypeId: 201});
                if(response && response.length > 0 && duplicate201 == 0){

                }
                else {
                    duplicate201 = duplicate201 + 1;
                    var pId = getUserTypeName(uItem.userTypeId);
                    pItem.text = pId;
                    pItem.items = getParentChildItems(uItem.parentId, uItem.userTypeId);
                    pItem.idk = uItem.id;
                    pItem.spriteCssClass = "rootfolder txtBlue";
                    pItem.pidk = uItem.parentId;
                    pItem.universalUserId = uItem.universalUserId;
                    pItem.userTypeId = uItem.userTypeId;
                    pItem.type = "Group";
                    treeArray.push(pItem);
                }
            }
            else if(uItem.userTypeId == 500 && duplicate500 == 0) {
                /* i = i++;*/
                duplicate500 = duplicate500 + 1;
                var pId = getUserTypeName(uItem.userTypeId);
                pItem.text = pId;
                pItem.items = getParentChildItems(uItem.parentId, uItem.userTypeId);
                pItem.idk = uItem.id;
                pItem.spriteCssClass = "rootfolder txtBlue";
                pItem.pidk = uItem.parentId;
                pItem.universalUserId = uItem.universalUserId;
                pItem.userTypeId = uItem.userTypeId;
                pItem.type = "Group";
                treeArray.push(pItem);
            }
            else if(uItem.userTypeId == 600 && duplicate600 == 0) {
                /* i = i++;*/
                duplicate600 = duplicate600 + 1;
                var pId = getUserTypeName(uItem.userTypeId);
                pItem.text = pId;
                pItem.items = getParentChildItems(uItem.parentId, uItem.userTypeId);
                pItem.idk = uItem.id;
                pItem.spriteCssClass = "rootfolder txtBlue";
                pItem.pidk = uItem.parentId;
                pItem.universalUserId = uItem.universalUserId;
                pItem.userTypeId = uItem.userTypeId;
                pItem.type = "Group";
                treeArray.push(pItem);
            }
            else if(uItem.userTypeId == 701 && duplicate701 == 0) {
                /* i = i++;*/
                duplicate701 = duplicate701 + 1;
                var pId = getUserTypeName(uItem.userTypeId);
                pItem.text = pId;
                pItem.items = getParentChildItems(uItem.parentId, uItem.userTypeId);
                pItem.idk = uItem.id;
                pItem.spriteCssClass = "rootfolder txtBlue";
                pItem.pidk = uItem.parentId;
                pItem.universalUserId = uItem.universalUserId;
                pItem.userTypeId = uItem.userTypeId;
                pItem.type = "Group";
                treeArray.push(pItem);
            }
            else if(uItem.userTypeId == 800 && duplicate800 == 0) {
                /* i = i++;*/
                duplicate800 = duplicate800 + 1;
                var pId = getUserTypeName(uItem.userTypeId);
                pItem.text = pId;
                pItem.items = getParentChildItems(uItem.parentId, uItem.userTypeId);
                pItem.idk = uItem.id;
                pItem.spriteCssClass = "rootfolder txtBlue";
                pItem.pidk = uItem.parentId;
                pItem.universalUserId = uItem.universalUserId;
                pItem.userTypeId = uItem.userTypeId;
                pItem.type = "Group";
                treeArray.push(pItem);
            }

        }
    }

    /*var tempD = $.grep(treeArray,function(e){

        return userTypeIds[m] === e.userTypeId;
    });


    if(tempD != null && tempD.length > 0){
        filteredData.push(tempD[0]);
    }*/

    treeDataSource[0].items = treeArray
    // console.log(treeArray);
    getScreenListTree();
}
function getParentChildItems(pid,uid){
    var uArray = [];
    for(var i=0;i<treeUserArray.length;i++){
        var uItem = treeUserArray[i];
        if(uItem && uItem.parentId == pid && uItem.userTypeId == uid){
            uItem.items = getParentItem(uItem.id);
            uItem.text = (uItem.lastName+" "+uItem.firstName);
            uItem.idk = uItem.id;
            uItem.spriteCssClass = "rootfolder";
            uItem.pidk = uItem.parentId;
            uItem.universalUserId = uItem.universalUserId;
            uItem.userTypeId = uItem.userTypeId;
            uItem.type = "Item";
            uArray.push(uItem);
        }
    }
    return uArray;
}

function getParentItem(idk){
    var uArray = [];
    var userTypeIdArray = [];

    for(var i=0;i<treeUserArray.length;i++){
        var uItem = treeUserArray[i];
        if(uItem && uItem.parentId == idk){
            if(userTypeIdArray.indexOf(uItem.userTypeId) == -1){
                userTypeIdArray.push(uItem.userTypeId);
                var pItem = {};
                var pId = getUserTypeName(uItem.userTypeId);
                pItem.text = pId;
                pItem.idk = uItem.id;
                pItem.spriteCssClass = "rootfolder txtBlue";
                pItem.pidk = uItem.parentId;
                pItem.universalUserId = uItem.universalUserId;
                pItem.userTypeId = uItem.userTypeId;
                pItem.type = "Group";
                pItem.items = getParentChildItems(uItem.parentId,uItem.userTypeId);
                uArray.push(pItem);
            }
            //break;
        }
    }
    return uArray;
}
function getUserTypeName(idk){
    for(var i=0;i<userTypeArray.length;i++){
        var uItem = userTypeArray[i];
        if(uItem && uItem.id == idk){
            return uItem.value.toUpperCase();
        }
    }
    return "";
}

function getUserTypeIdByValue(vl){
    for(var i=0;i<userTypeArray.length;i++){
        var uItem = userTypeArray[i];
        var strTest = uItem.value;
        var strTest1 = vl;
        strTest = strTest.toLowerCase();
        strTest1 = strTest1.toLowerCase();
        if(uItem && strTest == strTest1){
            return uItem.id;
        }
    }
    return "";
}
function getOrgUserTypes(idk){
    var pArray = [];
    for(var i=0;i<userTypeArray.length;i++){
        var uItem = userTypeArray[i];
        if(uItem && uItem.parentId == idk){
            var fName = uItem.firstName+" "+uItem.middleName+" "+uItem.lastName;
            var ftItem = {};
            ftItem.text = fName;//getUserTypeName(tItem.userTypeId);
            ftItem.idk = (tItem.id);
            ftItem.spriteCssClass = "rootfolder";
            ftItem.pidk = uItem.parentId;
            ftItem.type = "Item";
            pArray.push(ftItem);
            // item.items = tItems;
        }
    }
}
function getScreenListTree(){
    var tree = $("#treeview").data("kendoTreeView");
    if(tree){
        tree.destroy();
    }
    $("#treeview").kendoTreeView({
        change:onTreeItemSelect,
        dataSource: treeDataSource
    });
    setTimeout(function(){
        var treeView = $("#treeview").data("kendoTreeView");
        treeView.expand(".k-item");
    },1000);

}
function onError(errorObj) {
    console.log(errorObj);
}
function onTreeItemSelect(e){
    console.log(e);
    var selItem = getSelectTreeNode("treeview");
    console.log(selItem);
    $("#DivGroup").hide();
    $("#divOK").hide();
    $("#DivItem").hide();
    if(selItem){
        if(selItem.text == "Root" && selItem.type == "ROOT"){
            $("#DivGroup").show();
            $(".createManagerRight").show();
            $("#divOK").show();
            selGroup = "Group";
        }else if(selItem.type == "Item"){
            $("#DivGroup").show();
            $(".createManagerRight").show();
            $("#divOK").show();
            selGroup = "Group";

            var selItem = getSelectTreeNode("treeview");
            // console.log(selItem);
            if(selItem) {


                onGroup(selItem.id);
            }

        }else if(selItem.type == "Group"){
            $("#DivItem").show();
            $("#divOK").hide();
            selGroup = "Item";
            getSelectItemUsers();
            $("#divOK").css("display","none");
        }

    }else{

    }
}
function getSelectTreeNode(treeId){
    var tv = $('#'+treeId).data('kendoTreeView');
    if(tv){
        var selTreeItem = tv.dataItem(tv.select());
        if(selTreeItem){
            return selTreeItem;
        }
    }
    return null;
}
function onError(errorObj){
    console.log(errorObj);
}
function getCountryList(dataObj){
    console.log(dataObj);
    var dataArray = [];
    if(dataObj){
        if($.isArray(dataObj.response)){
            dataArray = dataObj.response;
        }else{
            dataArray.push(dataObj.response);
        }
    }
    var tempDataArry = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
        }
    }
    buildCodeTableListGrid(dataArray);
}
function buttonEvents(){
    $("#btnSave").off("click",onClickSave);
    $("#btnSave").on("click",onClickSave);

    $("#btnOK").off("click",onClickOK);
    $("#btnOK").on("click",onClickOK);

    $("#btnSubLeft").off("click",onClickOK);
    $("#btnSubLeft").on("click",onClickOK);

    $("#btnCancel").off("click",onClickCancel);
    $("#btnCancel").on("click",onClickCancel);

}

function adjustHeight(){
    var defHeight = 200;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    angularUIgridWrapper.adjustGridHeight(cmpHeight);
}

function buildScreenUserList(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;

    var txtGroup = $("#txtGroup").data("kendoComboBox");
    if(txtGroup) {
        if (txtGroup.text() == "NURSE") {
            gridColumns.push({
                "title": "Staff Type",
                "field": "uName",
            });
        } else if (txtGroup.text() == "CARE GIVER") {
            gridColumns.push({
                "title": "Staff Type",
                "field": "uName",
            });
        } else if (txtGroup.text() == "SERVICE USER") {
            gridColumns.push({
                "title": "Staff Type",
                "field": "uName",
            });
        } else if (txtGroup.text() == "RELATIVE") {
            gridColumns.push({
                "title": "Staff Type",
                "field": "uName",
            });
        } else if (txtGroup.text() == "CARE MANAGER") {
            gridColumns.push({
                "title": "Staff Type",
                "field": "uName",
            });
        } else if (txtGroup.text() == "FORMS") {
            gridColumns.push({
                "title": "Staff Type",
                "field": "uName",
            });
        } else if (txtGroup.text() == "COUNCIL-CARER") {
            gridColumns.push({
                "title": "Council Carer Name",
                "field": "uName",
            });
        }
        else if (txtGroup.text().toLowerCase() == "staff") {
            gridColumns.push({
                "title": "Manager Name",
                "field": "uName",
            });
        }

        else {
            gridColumns.push({
                "title": "Staff Name",
                "field": "uName",
            });
        }
    }
    else{
        gridColumns.push({
            "title": "Staff Name",
            "field": "uName",
        });
    }


    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}
function showCreateCheckBoxTemplate(){
    var node = '<div style="text-align:center;padding-top: 6px;"><input type="checkbox" class="check1" id="chkbox" ng-model="row.entity.Create" style="width:20px;height:20px;margin:0px"></input></div>';
    return node;
}
function showDeleteCheckBoxTemplate(){
    var node = '<div style="text-align:center;padding-top: 6px;"><input type="checkbox" class="check1" id="chkbox" ng-model="row.entity.Delete" style="width:20px;height:20px;margin:0px"></input></div>';
    return node;
}
function showViewCheckBoxTemplate(){
    var node = '<div style="text-align:center;padding-top: 6px;"><input type="checkbox" class="check1" id="chkbox" ng-model="row.entity.View" style="width:20px;height:20px;margin:0px"></input></div>';
    return node;
}
var prevSelectedItem =[];
function onChange(){

}

function onClickOK(){
    var treeView = $("#treeview").data("kendoTreeView");
    var txtGroup = $("#txtGroup").data("kendoComboBox");
    var selTreeItem = treeView.dataItem(treeView.select());
    var treeParentId = null;
    try{
        treeParentId = treeView.dataItem(treeView.parent(treeView.select()));
    }catch(ex){}
    //selItem && selItem.text == "Billing Account" && selItem.type == "ROOT"
    if(treeView &&  (selGroup == "Group" || selTreeItem.text == "Root")){
        var treeNode = {};
        treeNode.text = txtGroup.text();
        treeNode.idk = txtGroup.value();
        treeNode.type = "Group";
        treeNode.spriteCssClass = "rootfolder";
        if(treeParentId){
            treeNode.pidk = treeParentId.idk;
        }
        if(selTreeItem.text == "Root"){
            treeNode.pidk = "0";
        }
        treeView.append(treeNode, treeView.select());
    }else if(treeView &&  selGroup == "Item"){
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        if(selectedItems){
            var treeNode = {};
            treeNode.text = selectedItems[0].uName.toUpperCase();
            treeNode.idk = selectedItems[0].idk;
            treeNode.type = "Item";
            if(treeParentId){
                treeNode.pidk = treeParentId.idk;
            }
            treeNode.spriteCssClass = "rootfolder";
            treeView.append(treeNode, treeView.select());
        }
    }
}
var saveDataArray = [];
function onClickSave(){
    /*setTimeout(function(){
         var selectedItems = angularUIgridWrapper.getSelectedRows();
         console.log(selectedItems);
         if(selectedItems && selectedItems.length>0){
             var obj = {};
             obj.selItem = selectedItems[0];
            // var onCloseData = new Object();
             obj.status = "success";
             obj.operation = "ok";
                var windowWrapper = new kendoWindowWrapper();
                windowWrapper.closePageWindow(obj);
         }
    })*/
    saveDataArray = [];
    var treeView = $("#treeview").data("kendoTreeView");
    var blDataArray = treeView.dataSource.data();
    var nodeArray = getTreeNodeArray(blDataArray);
    var tempArray = [];
    if(saveDataArray && saveDataArray.length>0){
        for(var j=0;j<saveDataArray.length;j++){
            var sItem = saveDataArray[j];
            var obj = {};
            obj.id = Number(sItem.id);
            obj.parentId = Number(sItem.parentId);
            obj.modifiedBy = Number(sItem.modifiedBy);
            tempArray.push(obj);
        }
    }
    console.log(tempArray);
    var me = "PUT";
    var dObj = tempArray;
    var dataUrl = ipAddress + "/homecare/tenant-users/batch/";
    createAjaxObject(dataUrl, dObj, me, onManagerCreate, onError);
    //saveTreeData();
    //console.log(nodeArray);
}
function onManagerCreate(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1" && dataObj.response["tenant-users"]){
        //alert("Heirarchy Saved Successfully");
        saveTreeData();
    }
}
function saveTreeData(){
    var popW = "500";
    var popH = "150";

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Save";
    devModelWindowWrapper.openPageWindow("../../html/masters/customAlertPopup.html", profileLbl, popW, popH, true, onCloseAvailability);
}
function onCloseAvailability(evt,re){
    setTimeout(function(){
        var obj = {};
        var windowWrapper = new kendoWindowWrapper();
        windowWrapper.closePageWindow(obj);
    },1000);

}
function onError(errObj) {
    console.log(errObj);
    //customAlert.error("Error", "Error");
}
function getTreeNodeArray(treeNodeArr){
    var nodeArray = [];
    $.each(treeNodeArr,function(idx,item){
        var itemNodeArray = [];
        var itemObj = {};
        if(item.items){
            itemNodeArray = item.items;
            itemNodeArray = getNodeStatusArray(itemNodeArray);
            //console.log(itemNodeArray);
        }
        if(itemNodeArray.length>0){
            itemObj.Node = itemNodeArray;
        }
        itemObj.id = item.idk;
        itemObj.parentId = item.pidk;
        itemObj.text = item.text;
        itemObj.modifiedBy = Number(sessionStorage.userId);
        if(item.type == "Item"){
            saveDataArray.push(itemObj);
        }
        //itemObj['-Name'] = ((item.text == undefined)?"":item.text);
        //item['State'] = ((item.checked == true)?TRUE:FALSE);
        //console.log($("#checkTree"+item.InstanceKey).is(":checked"));
        /*itemObj['-Name'] = ((item.Name == undefined)?"":item.Name);
        if(item.InstanceKey != "All"){
            itemObj['-InstanceKey'] = ((item.InstanceKey == undefined)?"":item.InstanceKey);
        }
        if(item.InstanceKey == "All_1"){
            itemObj['-InstanceKey'] = "All";//((item.InstanceKey == undefined)?"":item.InstanceKey);
        }
        if(item.Root){
            itemObj.Root = item.Root;
        }
        itemObj['-ObjectKey'] = ((item.ObjectKey == undefined)?"":item.ObjectKey) ;
        itemObj['-Source'] = ((item.Source == undefined)?"":item.Source);
        if(flag){
            itemObj['-State'] = (item.checked == true)?TRUE:FALSE;
        }else{
            itemObj['-State'] = $("#checkTree"+item.InstanceKey).is(":checked")?TRUE:FALSE;//((item.checked == true)?TRUE:FALSE);
        }*/
        nodeArray.push(itemObj);
    });
    return nodeArray;
}
function getNodeStatusArray(nodeArr){
    var itemNodeStatusArray = [];
    $.each(nodeArr,function(idx,item){
        var itemNodeArray = [];
        var itemObj = {};
        if(item.items){
            itemNodeArray = item.items;
            itemNodeArray = getNodeStatusArray(itemNodeArray);
            //console.log(itemNodeArray);
        }
        if(itemNodeArray.length>0){
            itemObj.Node = itemNodeArray;
        }
        itemObj.id = item.idk;
        itemObj.parentId = item.pidk;
        itemObj.text = item.text;
        itemObj.modifiedBy = Number(sessionStorage.userId);
        if(item.type == "Item"){
            if(!item.pidk){
                itemObj.parentId = "0";
            }
            saveDataArray.push(itemObj);
        }
        /*itemObj['-InstanceKey'] = item.InstanceKey;
        itemObj['-ObjectKey'] = item.ObjectKey;
        itemObj['-Source'] = item.Source;
        if(flag){
            itemObj['-State'] = (item.checked == true)?TRUE:FALSE;
        }else{
            itemObj['-State'] = $("#checkTree"+item.InstanceKey).is(":checked")?TRUE:FALSE;//((item.checked == true)?TRUE:FALSE);
        }
        if(item.Root){
            itemObj.Root = item.Root;
        }*/
        itemNodeStatusArray.push(itemObj);
    });
    return itemNodeStatusArray;
}
function onClickCancel(){
    var obj = {};
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}

function onGroup(id){
    setDataForSelection([], "txtGroup", function(){}, ["", ""], 0, "");

    var LtreeUserArray = _.where(treeUserArray, {id: id})
    var obj = LtreeUserArray[0].items;
    var val1 =  localuserTypeArray.filter(o => !obj.find(o2 => o.id === o2.userTypeId) && o.id != 700 )

    // var idk = getUserTypeIdByValue(selItem.text);


    setDataForSelection(val1, "txtGroup", function(){}, ["value", "id"], 0, "");

}
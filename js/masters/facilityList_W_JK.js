var angularUIgridWrapper;
var parentRef = null;

$(document).ready(function(){
	var dataOptions = {
        pagination: false,
        changeCallBack: onChange
	}
	angularUIgridWrapper = new AngularUIGridWrapper("dgridFacilityList", dataOptions);
	angularUIgridWrapper.init();
	buildAccountListGrid([]);
});


$(window).load(function(){
	parentRef = parent.frames['iframe'].window;
	loading = false;
	$(window).resize(adjustHeight);
onLoaded();
	if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
	init();
    buttonEvents();
	adjustHeight();
}

function init(){
	buildAccountListGrid([]);
	getAjaxObject(ipAddress+"/facility/list","GET",getAccountList,onError);
}
function onError(errorObj){
	console.log(errorObj);
}
function getAccountList(dataObj){
	console.log(dataObj);
	var dataArray = [];
	if(dataObj){
		if($.isArray(dataObj.response.facility)){
			dataArray = dataObj.response.facility;
		}else{
			dataArray.push(dataObj.response.facility);
		}
	}
	var tempDataArry = [];
	for(var i=0;i<dataArray.length;i++){
		dataArray[i].idk = dataArray[i].id; 
		dataArray[i].Status = "InActive";
		if(dataArray[i].isActive == 1){
			dataArray[i].Status = "Active";
		}
	}
	buildAccountListGrid(dataArray);
}
function buttonEvents(){
	$("#btnSave").off("click",onClickOK);
	$("#btnSave").on("click",onClickOK);
	
	$("#btnCancelDet").off("click",onClickCancel);
	$("#btnCancelDet").on("click",onClickCancel);
	
	$("#btnDelete").off("click",onClickDelete);
	$("#btnDelete").on("click",onClickDelete);
	
	$("#btnAdd").off("click");
	$("#btnAdd").on("click",onClickAdd);
}

function adjustHeight(){
	var defHeight = 120;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
	angularUIgridWrapper.adjustGridHeight(cmpHeight);
}

function buildAccountListGrid(dataSource) {
	var gridColumns = [];
	var otoptions = {};
	otoptions.noUnselect = false;
	gridColumns.push({
        "title": "ID",
        "field": "idk",
	});
	gridColumns.push({
        "title": "ExternalID1",
        "field": "externalId1",
	});
	gridColumns.push({
        "title": "ExternalID2",
        "field": "externalId2",
	});
    gridColumns.push({
        "title": "Abbrevation",
        "field": "abbreviation",
	});
    /*gridColumns.push({
        "title": "FirstName",
        "field": "firstname",
	});
	gridColumns.push({
        "title": "MiddleName",
        "field": "middlename",
        "width":"25%"
	});	
	gridColumns.push({
        "title": "LastName",
        "field": "lastname",
	});*/
	gridColumns.push({
        "title": "Name",
        "field": "name",
	});
	gridColumns.push({
        "title": "Display Name",
        "field": "displayName",
	});
	gridColumns.push({
        "title": "Status",
        "field": "Status",
	});
   angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions); 
	adjustHeight();
	//onIndividualRowClick();
}
var prevSelectedItem =[];
function onChange(){
	setTimeout(function(){
		var selectedItems = angularUIgridWrapper.getSelectedRows();
		 console.log(selectedItems);
		 if(selectedItems && selectedItems.length>0){
			 $("#btnSave").prop("disabled", false);
			 $("#btnDelete").prop("disabled", false);
		 }else{
			 $("#btnSave").prop("disabled", true);
			 $("#btnDelete").prop("disabled", true);
		 }
	},100)
}

function onClickOK(){
	setTimeout(function(){
		 var selectedItems = angularUIgridWrapper.getSelectedRows();
		 console.log(selectedItems);
		 if(selectedItems && selectedItems.length>0){
			 var obj = {};
			 obj.selItem = selectedItems[0];
			 selAccountItem = selectedItems[0];
			 addFacility("update");
			// var onCloseData = new Object();
			 /*obj.status = "success";
			 obj.operation = "ok";
				var windowWrapper = new kendoWindowWrapper();
				windowWrapper.closePageWindow(obj);*/
		 }
	})
}
function onClickDelete(){
	customAlert.confirm("Confirm", "Are you sure you want delete?",function(response){
		if(response.button == "Yes"){
			var selectedItems = angularUIgridWrapper.getSelectedRows();
			 console.log(selectedItems);
			 if(selectedItems && selectedItems.length>0){
				 var selItem = selectedItems[0];
				 if(selItem){
					 var dataUrl = ipAddress+"/facility/delete/";
					 var reqObj = {};
					 reqObj.id = selItem.idk;
					 reqObj.abbr = selItem.abbr;
					 reqObj.isDeleted = "1";
					 reqObj.modifiedBy = "101";
					 createAjaxObject(dataUrl,reqObj,"POST",onDeleteCountryt,onError);
				 }
			 }
		}
	});
}
function onDeleteCountryt(dataObj){
	console.log(dataObj);
	if(dataObj && dataObj.response.status){
		if(dataObj.response.status.code == "1"){
			customAlert.info("info", "Facility Deleted Successfully");
			init();
		}else{
			customAlert.error("error", dataObj.message);
		}
	}
}
function onClickAdd(){
	/*var obj = {};
	 obj.status = "Add";
	 obj.operation = "ok";
		var windowWrapper = new kendoWindowWrapper();
		windowWrapper.closePageWindow(obj);*/
	addFacility("add")
	
}
var selAccountItem = null;
function addFacility(opr){
	var popW = 850;
    var popH = 550;

    var profileLbl = "Add Facility";
    var devModelWindowWrapper = new kendoWindowWrapper();
   
    	parentRef.operation = opr;
    	parentRef.selItem = selAccountItem;
    
    devModelWindowWrapper.openPageWindow("../../html/masters/createFacility.html", profileLbl, popW, popH, true, closeAddFacilitytAction);
}
function closeAddFacilitytAction(evt,returnData){
	if(returnData && returnData.status == "success"){
		var opr = returnData.operation;
		if(opr == "add"){
			customAlert.info("info", "Facility Created Successfully");
		}else{
			customAlert.info("info", "Facility Updated Successfully");
		}
	}
	init();
}

function onClickCancel(){
	var obj = {};
	var windowWrapper = new kendoWindowWrapper();
	windowWrapper.closePageWindow(obj);
}

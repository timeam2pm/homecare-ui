var parentRef = null;
var recordType = "1";
var dataArray = [];
$(document).ready(function(){
    parentRef = parent.frames['iframe'].window;
});

$(window).load(function(){
    loading = false;
    //$(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
    init();
    buttonEvents();
}

function init(){
    var urlsrc = ipAddress+"/homecare/download/img/logo/?access_token="+sessionStorage.access_token+"&tenant="+sessionStorage.tenant+"&"+Math.round(Math.random()*1000000);
    $("#imgPhoto").attr("src",urlsrc);

    var urlsrc = ipAddress+"/homecare/download/img/logowithtext/?access_token="+sessionStorage.access_token+"&tenant="+sessionStorage.tenant+"&"+Math.round(Math.random()*1000000);
    $("#imgPhoto1").attr("src",urlsrc);

    var urlsrc = ipAddress+"/homecare/download/img/inverted/?access_token="+sessionStorage.access_token+"&tenant="+sessionStorage.tenant+"&"+Math.round(Math.random()*1000000);
    $("#imgPhoto2").attr("src",urlsrc);

    var urlsrc = ipAddress+"/homecare/download/img/invertedwithtext/?access_token="+sessionStorage.access_token+"&tenant="+sessionStorage.tenant+"&"+Math.round(Math.random()*1000000);
    $("#imgPhoto3").attr("src",urlsrc);

    var urlsrc = ipAddress+"/homecare/download/img/applogo/?access_token="+sessionStorage.access_token+"&tenant="+sessionStorage.tenant+"&"+Math.round(Math.random()*1000000);
    $("#imgPhoto4").attr("src",urlsrc);

    var urlsrc = ipAddress+"/homecare/download/img/applogowithtext/?access_token="+sessionStorage.access_token+"&tenant="+sessionStorage.tenant+"&"+Math.round(Math.random()*1000000);
    $("#imgPhoto5").attr("src",urlsrc);
}

function onError(errorObj){
    console.log(errorObj);
}

function buttonEvents(){
    $("#btnBrowse").off("click", onClickBrowse);
    $("#btnBrowse").on("click", onClickBrowse);

    $("#fileElem").off("change", onSelectionFiles);
    $("#fileElem").on("change", onSelectionFiles);

    $("#btnUpload").off("click", onClickUploadPhoto);
    $("#btnUpload").on("click", onClickUploadPhoto);

    $("#btnBrowse1").off("click", onClickBrowse1);
    $("#btnBrowse1").on("click", onClickBrowse1);

    $("#fileElem1").off("change", onSelectionFiles1);
    $("#fileElem1").on("change", onSelectionFiles1);

    $("#btnUpload1").off("click", onClickUploadPhoto1);
    $("#btnUpload1").on("click", onClickUploadPhoto1);

    $("#btnBrowse2").off("click", onClickBrowse2);
    $("#btnBrowse2").on("click", onClickBrowse2);

    $("#fileElem2").off("change", onSelectionFiles2);
    $("#fileElem2").on("change", onSelectionFiles2);

    $("#btnUpload2").off("click", onClickUploadPhoto2);
    $("#btnUpload2").on("click", onClickUploadPhoto2);

    $("#btnBrowse3").off("click", onClickBrowse3);
    $("#btnBrowse3").on("click", onClickBrowse3);

    $("#fileElem3").off("change", onSelectionFiles3);
    $("#fileElem3").on("change", onSelectionFiles3);

    $("#btnUpload3").off("click", onClickUploadPhoto3);
    $("#btnUpload3").on("click", onClickUploadPhoto3);

    $("#btnBrowse4").off("click", onClickBrowse4);
    $("#btnBrowse4").on("click", onClickBrowse4);

    $("#fileElem4").off("change", onSelectionFiles4);
    $("#fileElem4").on("change", onSelectionFiles4);

    $("#btnUpload4").off("click", onClickUploadPhoto4);
    $("#btnUpload4").on("click", onClickUploadPhoto4);

    $("#btnBrowse5").off("click", onClickBrowse5);
    $("#btnBrowse5").on("click", onClickBrowse5);

    $("#fileElem5").off("change", onSelectionFiles5);
    $("#fileElem5").on("change", onSelectionFiles5);

    $("#btnUpload5").off("click", onClickUploadPhoto5);
    $("#btnUpload5").on("click", onClickUploadPhoto5);

    $("#btnCancelDet").off("click");
    $("#btnCancelDet").on("click",onClickCancel);
}
function onClickCancel() {
    var obj = {};
    obj.status = "false";
    popupClose(obj);
}
function popupClose(obj) {
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}
function onClickBrowse(e) {
    if (e.currentTarget && e.currentTarget.id) {
        var btnId = e.currentTarget.id;
        var fileTagId = ("fileElem" + btnId);
        $("#fileElem").click();
    }
}
function onClickBrowse1(e) {
    if (e.currentTarget && e.currentTarget.id) {
        var btnId = e.currentTarget.id;
        var fileTagId = ("fileElem" + btnId);
        $("#fileElem1").click();
    }
}
function onClickBrowse2(e) {
    if (e.currentTarget && e.currentTarget.id) {
        var btnId = e.currentTarget.id;
        var fileTagId = ("fileElem" + btnId);
        $("#fileElem2").click();
    }
}

function onClickBrowse3(e) {
    if (e.currentTarget && e.currentTarget.id) {
        var btnId = e.currentTarget.id;
        var fileTagId = ("fileElem" + btnId);
        $("#fileElem3").click();
    }
}
function onClickBrowse4(e) {
    if (e.currentTarget && e.currentTarget.id) {
        var btnId = e.currentTarget.id;
        var fileTagId = ("fileElem" + btnId);
        $("#fileElem4").click();
    }
}
function onClickBrowse5(e) {
    if (e.currentTarget && e.currentTarget.id) {
        var btnId = e.currentTarget.id;
        var fileTagId = ("fileElem" + btnId);
        $("#fileElem5").click();
    }
}
var fileName = "";
var files = null;
function onSelectionFiles(event) {
    console.log(event);
    files = event.target.files;
    fileName = "";
    //$('#txtFU').val("");
    if (files) {
        if (files.length > 0) {
            fileName = files[0].name;
            // if (patientId != "") {
            var oFReader = new FileReader();
            oFReader.readAsDataURL(files[0]);
            oFReader.onload = function(oFREvent) {
                document.getElementById("imgPhoto").src = oFREvent.target.result;
            }
            //}
        }
    }
}
var fileName1 = "";
var files1 = null;
function onSelectionFiles1(event) {
    console.log(event);
    files1 = event.target.files;
    fileName1 = "";
    //$('#txtFU').val("");
    if (files1) {
        if (files1.length > 0) {
            fileName1 = files1[0].name;
            // if (patientId != "") {
            var oFReader = new FileReader();
            oFReader.readAsDataURL(files1[0]);
            oFReader.onload = function(oFREvent) {
                document.getElementById("imgPhoto1").src = oFREvent.target.result;
            }
            //}
        }
    }
}

var fileName2 = "";
var files2 = null;
function onSelectionFiles2(event) {
    console.log(event);
    files2 = event.target.files;
    fileName2 = "";
    //$('#txtFU').val("");
    if (files2) {
        if (files2.length > 0) {
            fileName2 = files2[0].name;
            // if (patientId != "") {
            var oFReader = new FileReader();
            oFReader.readAsDataURL(files2[0]);
            oFReader.onload = function(oFREvent) {
                document.getElementById("imgPhoto2").src = oFREvent.target.result;
            }
            //}
        }
    }
}

var fileName3 = "";
var files3 = null;
function onSelectionFiles3(event) {
    console.log(event);
    files3 = event.target.files;
    fileName3 = "";
    //$('#txtFU').val("");
    if (files3) {
        if (files3.length > 0) {
            fileName3 = files3[0].name;
            // if (patientId != "") {
            var oFReader = new FileReader();
            oFReader.readAsDataURL(files3[0]);
            oFReader.onload = function(oFREvent) {
                document.getElementById("imgPhoto3").src = oFREvent.target.result;
            }
            //}
        }
    }
}
var fileName4 = "";
var files4 = null;
function onSelectionFiles4(event) {
    console.log(event);
    files4 = event.target.files;
    fileName4 = "";
    //$('#txtFU').val("");
    if (files4) {
        if (files4.length > 0) {
            fileName4 = files4[0].name;
            // if (patientId != "") {
            var oFReader = new FileReader();
            oFReader.readAsDataURL(files4[0]);
            oFReader.onload = function(oFREvent) {
                document.getElementById("imgPhoto4").src = oFREvent.target.result;
            }
            //}
        }
    }
}
var fileName5 = "";
var files5 = null;
function onSelectionFiles5(event) {
    console.log(event);
    files5 = event.target.files;
    fileName5 = "";
    //$('#txtFU').val("");
    if (files5) {
        if (files5.length > 0) {
            fileName5 = files5[0].name;
            // if (patientId != "") {
            var oFReader = new FileReader();
            oFReader.readAsDataURL(files5[0]);
            oFReader.onload = function(oFREvent) {
                document.getElementById("imgPhoto5").src = oFREvent.target.result;
            }
            //}
        }
    }
}
function onClickUploadPhoto(){
    imageUpload(0);
}
function onClickUploadPhoto1(){
    imageUpload(1);
}
function onClickUploadPhoto2(){
    imageUpload(2);
}
function onClickUploadPhoto3(){
    imageUpload(3);
}
function onClickUploadPhoto4(){
    imageUpload(4);
}
function onClickUploadPhoto5(){
    imageUpload(5);
}
function imageUpload(st){
    var reqUrl = "";
    var formData = new FormData();
    if(st == 0){
        reqUrl = ipAddress + "/homecare/upload/img/logo/?access_token=" + sessionStorage.access_token+"&tenant="+sessionStorage.tenant;
        formData.append("desc", fileName);
        formData.append("file", files[0]);
    }else if(st == 1){
        reqUrl = ipAddress + "/homecare/upload/img/logowithtext/?access_token=" + sessionStorage.access_token+"&tenant="+sessionStorage.tenant;
        formData.append("desc", fileName1);
        formData.append("file", files1[0]);
    }else if(st == 2){
        reqUrl = ipAddress + "/homecare/upload/img/inverted/?access_token=" + sessionStorage.access_token+"&tenant="+sessionStorage.tenant;
        formData.append("desc", fileName2);
        formData.append("file", files2[0]);
    }else if(st == 3){
        reqUrl = ipAddress + "/homecare/upload/img/invertedwithtext/?access_token=" + sessionStorage.access_token+"&tenant="+sessionStorage.tenant;
        formData.append("desc", fileName3);
        formData.append("file", files3[0]);
    }else if(st == 4){
        reqUrl = ipAddress + "/homecare/upload/img/applogo/?access_token=" + sessionStorage.access_token+"&tenant="+sessionStorage.tenant;
        formData.append("desc", fileName4);
        formData.append("file", files4[0]);
    }else if(st == 5){
        reqUrl = ipAddress + "/homecare/upload/img/applogowithtext/?access_token=" + sessionStorage.access_token+"&tenant="+sessionStorage.tenant;
        formData.append("desc", fileName5);
        formData.append("file", files5[0]);
    }

    formData.append("icon-file-extension", "png");

    Loader.showLoader();
    $.ajax({
        url: reqUrl,
        type: 'POST',
        data: formData,
        processData: false,
        contentType: false, // "multipart/form-data",
        success: function(data, textStatus, jqXHR) {
            if (typeof data.error === 'undefined') {
                Loader.hideLoader();
                submitForm(data);
            } else {
                Loader.hideLoader();
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            Loader.hideLoader();
        }
    });

}


function submitForm(dataObj) {
    if(dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        customAlert.error("Info", "Image uploaded successfully");
    }else{
        customAlert.error("Error", dataObj.response.status.message);
    }

    //init();
}
var selRow = null;
function onClickAction(){

}
function closeVideoScreen(evt,returnData){

}
var operation = "add";
var selFileResourceDataItem = null;
function onClickCancel(){
    var obj = {};
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}

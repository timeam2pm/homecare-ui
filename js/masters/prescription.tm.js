var tradeNameListUIgrid = null;
var parentRef = null;
$(document).ready(function() {
    $('#prescription-form-select').val();
    var medNameData;
    var todayDate;
    var todayDateString;
    var listFullData;
    var getIndex;
    var patientListFlag = false;
    var globalFacilityData;
    init();
    function init() {
        parentRef = parent.frames['iframe'].window;

        var cntry = sessionStorage.countryName;
        if (cntry.indexOf("India") >= 0) {
            $(".datepicker").kendoDatePicker({format: "dd/MM/yyyy"});
        } else if (cntry.indexOf("United Kingdom") >= 0) {
            $(".datepicker").kendoDatePicker({format: "dd/MM/yyyy"});
        } else {
            $(".datepicker").kendoDatePicker();
        }
        $('.datepicker').on('keyup keypress', function(e) {
            e.preventDefault();
        });
        var dataTradeNameListViewOptions = {
            pagination: false,
            changeCallBack: onTradeNameListViewChange
        }
        function onTradeNameListViewChange(){
            setTimeout(function() {
                selectedItems = tradeNameListUIgrid.getSelectedRows();
                console.log(selectedItems);
                disableButtons();
            });
        }
        tradeNameListUIgrid = new AngularUIGridWrapper("medicationTradeData", dataTradeNameListViewOptions);
        tradeNameListUIgrid.init();
        buildMedicationTradeListViewGrid([]);
        onClickGo();
        $("#btnCancelDet").off("click");
        $("#btnCancelDet").on("click",onClickCancel);
        //$("#pres-ok-link").trigger('click');
    }
    function onClickCancel() {
        var obj = {};
        obj.status = "false";
        popupClose(obj);
    }
    function popupClose(obj) {
        var windowWrapper = new kendoWindowWrapper();
        windowWrapper.closePageWindow(obj);
    }
    function onClickGo(){
        $('.alert').remove();
        var facilityIdVal = $('#medication-facilityName-list').find('option:selected').attr('data-prescription-val') || "";
        var prescribedByVal = $("#medication-prescribedBy-list").find('option:selected').attr('data-prescription-val') || "";
        var statusVal = $("#prescription-status-select-list").find('option:selected').attr('data-prescription-val') || "";
        //var startDateList = $('input[name="medication-startDate-list"]').val() || "";
        //var endDateList = $('input[name="medication-endDate-list"]').val() || "";
        var dataObj = "&patient-id="+ parentRef.patientId;
        if(facilityIdVal != "") {
            dataObj = dataObj + "&facility-id=" + facilityIdVal;
        }
        if(prescribedByVal != "") {
            dataObj = dataObj + "&provider-id=" + prescribedByVal;
        }
        if(statusVal != "") {
            dataObj = dataObj + "&medication-status-id=" + statusVal;
        }
        /*if(startDateList != "") {
            startDateListTime = startDateList.split('/').join('-');
            dataObj = dataObj + "&from-date=" + getDateTime(startDateListTime);
        }
        if(endDateList != "") {
            endDateListTime = endDateList.split('/').join('-');
            dataObj = dataObj + "&to-date=" + getDateTime(endDateListTime);
        }*/
        getAjaxObject(ipAddress+"/patient-medication/list?is-active=1"+dataObj,"GET",getPrescriptionListData1,function(){});
    }
    function getPrescriptionListData1(resp) {
        var dataListArr = [], dataArray;
        $('.prescription-list-data').find('#listDataWrapper').empty();
        if(resp && resp.response && resp.response.patientMedication){
            if($.isArray(resp.response.patientMedication)){
                dataArray = resp.response.patientMedication;
            }
            else{
                dataArray.push(resp.response.patientMedication);
            }
        }
        if((resp && resp.response && resp.response.status.code == "0") || (resp && resp.response && resp.response.status.code == "1" && !resp.response.patientMedication)) {
            $('.prescription-list-data').prepend('<div class="alert alert-danger">'+resp.response.status.message+'</div>');
        }
        /*else if(resp && resp.response && resp.response.status.code == "1" && !resp.response.patientMedication) {

        }*/
        if(dataArray != undefined) {
            for(var i=0;i<dataArray.length;i++){
                dataArray[i].idDuplicate = dataArray[i].id;
                if(dataArray[i].isActive == 1){
                    dataListArr.push(dataArray[i]);
                }
            }
            listFullData = dataListArr;
        }
        for (var i = 0; i < dataListArr.length; i++) {
            dataListArr[i].drugInfo = dataListArr[i].composition.rxNorm != undefined ? dataListArr[i].composition.rxNorm.drugInfo : "";
            dataListArr[i].tradeNameVal = dataListArr[i].medicationTradeNameId != undefined ? dataListArr[i].composition.tradeName.name : "";
            dataListArr[i].formVal = dataListArr[i].composition.medicationForm != undefined ? dataListArr[i].composition.medicationForm.desc : "";
            dataListArr[i].strengthVal = dataListArr[i].composition.medicationStrength != undefined ? dataListArr[i].composition.medicationStrength.desc : "";
            dataListArr[i].routeVal = dataListArr[i].composition.medicationRoute != undefined ? dataListArr[i].composition.medicationRoute.desc : "";
            dataListArr[i].frequencyVal = dataListArr[i].composition.medicationFrequency != undefined ? dataListArr[i].composition.medicationFrequency.desc : "";
            dataListArr[i].instructionsVal = dataListArr[i].composition.medicationInstructions != undefined ? dataListArr[i].composition.medicationInstructions.desc : "";
            dataListArr[i].transmissionVal = dataListArr[i].composition.medicationTransmission != undefined ? dataListArr[i].composition.medicationTransmission.desc : "";
            dataListArr[i].dispenceQuantyVal = dataListArr[i].dispenceQuanty != null ? dataListArr[i].dispenceQuanty : "";
            dataListArr[i].facilityIdVal = dataListArr[i].facilityId != null ? dataListArr[i].composition.facility.name : "";
            dataListArr[i].prescribedByVal = dataListArr[i].prescribedBy != null ? (dataListArr[i].composition.prescribedBy.firstName + ", " + dataListArr[i].composition.prescribedBy.lastName) : "";
            dataListArr[i].pharmacyIdVal = dataListArr[i].pharmacyId != null ? dataListArr[i].composition.pharmacy.name : "";
            dataListArr[i].medicationDateVal = dataListArr[i].medicationDate != null ? new Date(dataListArr[i].medicationDate).toLocaleDateString() : "";
            dataListArr[i].refillDateVal = dataListArr[i].refillDate != null ? new Date(dataListArr[i].refillDate).toLocaleDateString() : "";
            dataListArr[i].medicationStartDateVal = dataListArr[i].medicationStartDate != null ? new Date(dataListArr[i].medicationStartDate).toLocaleDateString() : "";
            dataListArr[i].medicationEndDateVal = dataListArr[i].medicationEndDate != null ? new Date(dataListArr[i].medicationEndDate).toLocaleDateString() : "LongTerm";
            $('.prescription-list-data').find('#listDataWrapper').append('<ul data-list-arr='+i+'><li class="reduceWidth"><div><label>'+onGetDate(dataListArr[i].medicationDateVal)+'</label><label>'+dataListArr[i].transmissionVal+'</label></div></li>'+
                '<li><div><label>'+dataListArr[i].tradeNameVal+'</label></div></li>'+
                '<li class="reduceWidth"><div><label>'+dataListArr[i].dispenceQuantyVal+'</label></div></li>'+
                '<li class="reduceWidth"><div><label>'+onGetDate(dataListArr[i].medicationStartDateVal)+'<label>to</label>'+onGetDate(dataListArr[i].medicationEndDateVal)+'</label></div></li>'+
                '<li><div><label>'+dataListArr[i].formVal+'</label><label>'+dataListArr[i].routeVal+'</label><label>'+dataListArr[i].frequencyVal+'</label></div></li>'+
                '<li><div><label>'+dataListArr[i].instructionsVal+'</label></div></li>'+
                '<li class="reduceWidth"><div><label>'+onGetDate(dataListArr[i].refillDateVal)+'</label></div></li>'+
                '<li><div><label>'+dataListArr[i].sideEffects+'</label></div></li>'+
                '<li><div><label>'+dataListArr[i].facilityIdVal+'</label><label>'+dataListArr[i].prescribedByVal+'</label></div></li>'+
                '<li><div><label>'+dataListArr[i].pharmacyIdVal+'</label><label>'+dataListArr[i].pharamacistMessage+'</label></div></li>'+
                '</ul>');
        }
        /*if(!patientListFlag) {
            getListSelectedItems();
            patientListFlag = true;
        }*/
    }
    var presObj = {
        bindFunctions: function() {
            presObj.setMedicateValues();
            presObj.setDates();
            presObj.bindEvents();
        },
        setDates: function() {
            todayDate = new Date();
            todayDateString = todayDate.toLocaleDateString();
            todayDateString = onGetDate(todayDateString);
            $('.pres-medicate').find('input[name="medication-currentDate"]').val(todayDateString);
            $('.pres-medicate').find('input[name="medication-startDate"]').val(todayDateString);
            $('.pres-medicate').find('input[name="medication-startDate-list"]').val(todayDateString);
            $('.presListDataWrapper').find('input[name="medication-startDate-list"]').val(todayDateString);
            $('.presListDataWrapper').find('input[name="medication-endDate-list"]').val(todayDateString);
        },
        bodyScrollFn: function() {
            $('body, html').stop().animate({ scrollTop: 0 }, 500);
        },
        setMedicateValues: function() {
            var dataMedicateList = ['/master/medication-trade-name','/master/medication_form','/master/medication_strength','/master/medication_route','/master/medication_frequency','/master/medication_instructions','/master/medication_status','/master/medication_action','/master/medication_transmission','/provider','/facility'];
            $.each(dataMedicateList,function(i,value) {
                var urlExtn = value + '/list/';
                retrieveSelectData(value,urlExtn);
            });
            function retrieveSelectData(formName,url) {
                // $('.medicationPrescription-select').each(function() {
                // 	var selfVal = $(this).attr('medicate-pres-select');
                // 	var $selectVal = $(this).attr('medicate-pres-select');
                // 	if($selectVal != "Action" && $selectVal != "Status" && $selectVal != "Transmission" && $selectVal != "Facility Name" && $selectVal != "Prescribed By") {
                // 		$(this).html('<option value="" disabled selected>'+selfVal+'</option>');
                // 	} else {
                // 		$(this).html('');
                // 	}
                // });
                getAjaxObject(ipAddress+url+'?is-active=1',"GET", onRetreiveSelectDataSuccess, onError);
                function onRetreiveSelectDataSuccess(resp) {
                    var medicationPresData = [], dataArr = [];
                    if(formName == '/provider' && resp && resp.response && resp.response.provider) {
                        var providersData = resp.response.provider;
                        for (var i = 0; i < providersData.length; i++) {
                            if(providersData[i].type == 100) {
                                var provideObj = {
                                    "id": providersData[i].id,
                                    "abbreviation": providersData[i].abbreviation,
                                    "firstName": providersData[i].firstName,
                                    "lastName": providersData[i].lastName
                                }
                                $('#medication-prescribedBy').append('<option data-prescription-val="'+provideObj.id+'">'+provideObj.abbreviation+' '+provideObj.firstName+','+provideObj.lastName+'</option>');
                            }
                        }
                    }
                    else if(formName == '/facility' && resp && resp.response && resp.response.facility) {
                        var facilityData = resp.response.facility;
                        globalFacilityData = resp.response.facility;
                        for (var i = 0; i < facilityData.length; i++) {
                            var facilityObj = {
                                "id": facilityData[i].id,
                                "displayName": facilityData[i].displayName
                            }
                            $('#medication-facilityName').append('<option data-prescription-val="'+facilityObj.id+'">'+facilityObj.displayName+'</option>');
                        }
                    }
                    else if(formName == '/medication-trade-name' && resp && resp.response && resp.response.medicationTradeName) {
                        var medicationTradeNameData = resp.response.medicationTradeName;
                        medNameData = medicationTradeNameData;
                        for (var i = 0; i < medicationTradeNameData.length; i++) {
                            var medicationTradeNameObj = {
                                "id": medicationTradeNameData[i].id,
                                "name": medicationTradeNameData[i].name
                            }
                            $('#medicationName').append('<option data-prescription-val="'+medicationTradeNameObj.id+'">'+medicationTradeNameObj.name+'</option>');
                        }
                    }
                    else {
                        if(resp && resp.response && resp.response.codeTable){
                            if($.isArray(resp.response.codeTable)){
                                dataArr = resp.response.codeTable;
                            }
                            else{
                                dataArr.push(resp.response.codeTable);
                            }
                        }
                        if(dataArr && dataArr.length > 0) {
                            for(var i=0;i<dataArr.length;i++){
                                if(dataArr[i].isActive == 1) {
                                    medicationPresData.push(dataArr[i]);
                                }
                            }
                        }
                        $.each(medicationPresData, function(i,val) {
                            if(formName == "/master/medication_form") {
                                $('#prescription-form-select').append('<option data-prescription-val="'+val.id+'">'+val.desc+'</option>')
                            }
                            else if(formName == "/master/medication_strength") {
                                $('#prescription-strength-select').append('<option data-prescription-val="'+val.id+'">'+val.desc+'</option>')
                            }
                            else if(formName == "/master/medication_route") {
                                $('#prescription-route-select').append('<option data-prescription-val="'+val.id+'">'+val.desc+'</option>')
                            }
                            else if(formName == "/master/medication_frequency") {
                                $('#prescription-frequency-select').append('<option data-prescription-val="'+val.id+'">'+val.desc+'</option>')
                            }
                            else if(formName == "/master/medication_instructions") {
                                $('#prescription-instructions-select').append('<option data-prescription-val="'+val.id+'">'+val.desc+'</option>')
                            }
                            else if(formName == "/master/medication_action") {
                                $('#prescription-action-select').append('<option data-prescription-val="'+val.id+'">'+val.desc+'</option>')
                            }
                            else if(formName == "/master/medication_status") {
                                $('#prescription-status-select').append('<option data-prescription-val="'+val.id+'">'+val.desc+'</option>')
                            }
                            else if(formName == "/master/medication_transmission") {
                                $('#prescription-transmission-select').append('<option data-prescription-val="'+val.id+'">'+val.desc+'</option>')
                            }
                        });
                    }
                }
                function onError(errObj){
                    console.log(errObj);
                    customAlert.error("Error","Error");
                }
            }
        },
        bindEvents: function() {
            $("#prescription-save-btn").on('click', function(e) {
                e.preventDefault();
                $('.alert').remove();
                $('.borderColor').removeClass("borderColor");
                var dataUrl = ipAddress+'/patient-medication/create';
                var dataUpdateUrl = ipAddress+'/patient-medication/update';
                var medicationTradeNameIdVal = $('#tradeDrugNameValue').val() || "";
                var medicationFormIdVal = $('#prescription-form-select').find('option:selected').attr('data-prescription-val') || "";
                var takeVal = $('.pres-medicate').find('input[name="formMedicineTake"]').val() || "";
                var strengthVal = $('.pres-medicate').find('input[name="medication-formQty"]').val() || "";
                var medicationStrengthIdVal = $('#prescription-strength-select').find('option:selected').attr('data-prescription-val') || "";
                var medicationRouteIdVal = $('#prescription-route-select').find('option:selected').attr('data-prescription-val') || "";
                var medicationFrequencyIdVal = $('#prescription-frequency-select').find('option:selected').attr('data-prescription-val') || "";
                var medicationInstructionsIdVal = $('#prescription-instructions-select').find('option:selected').attr('data-prescription-val') || "";
                var dispenceQuantyVal = $('.pres-medicate').find('input[name="medication-dosageQuantity"]').val() || "";
                var longTermVal = $('#longterm-switch').is(':checked') ? 1 : 0;
                var instructionVal = $('.pres-medicate').find('textarea[name="medicate-dosageInstructions"]').val() || "";
                var medicationActionIdVal = $('#prescription-action-select').find('option:selected').attr('data-prescription-val') || "";
                var medicationStatusIdVal = $('#prescription-status-select').find('option:selected').attr('data-prescription-val') || "";
                var pharmacyIdVal = $('#medicationPharmacy').find('option:selected').attr('data-prescription-val') || "";
                var pharamacistMessageVal = $('.pres-medicate').find('input[name="medication-pharmacy-message"]').val() || "";
                var transmissionIdVal = $('#prescription-transmission-select').find('option:selected').attr('data-prescription-val') || "";
                var medicineTakenForVal = $('.pres-medicate').find('input[name="medication-takenFor"]').val() || "";
                var sideEffectsVal = $('.pres-medicate').find('input[name="medication-additionalNotes"]').val() || "";
                var notesVal = $('.pres-medicate').find('input[name="medication-sideEffects"]').val() || "";
                var prescribedByVal = $('#medication-prescribedBy').find('option:selected').attr('data-prescription-val') || "";
                var facilityIdVal = $('#medication-facilityName').find('option:selected').attr('data-prescription-val') || "";
                var currDateVal = new Date().getTime();
                var medicationStartDate = $('.pres-medicate').find('input[name="medication-startDate"]').val() || "";
                var medicationEndDate = $('.pres-medicate').find('input[name="medication-endDate"]').val() || "";
                /*var endDate = $('.pres-medicate').find('input[name="medication-endDate"]').val();*/
                var refillDate = $('.pres-medicate').find('input[name="medication-refillDate"]').val() || "";
                var medicationEndDateVal, medicationStartDateVal, refillDateVal;
                if(medicationStartDate != "") {
                    medicationStartDate = medicationStartDate.split('/').join('-');
                    medicationStartDateVal = getDateTime(medicationStartDate);
                }
                else {
                    medicationStartDate = null;
                }
                if(medicationEndDate != "") {
                    if(longTermVal === 1) {
                        medicationEndDateVal = null;
                    }
                    else {
                        medicationEndDate = medicationEndDate.split('/').join('-');
                        medicationEndDateVal = getDateTime(medicationEndDate);
                    }
                }
                else {
                    medicationEndDateVal = null;
                }
                if(refillDate != "") {
                    refillDate = refillDate.split('/').join('-');
                    refillDateVal = getDateTime(refillDate);
                }
                else {
                    refillDateVal = null;
                }
                var prescriptionObj = {
                    "createdBy": sessionStorage.userId,
                    "createdDate": currDateVal,
                    "isActive": 1,
                    "isDeleted": 0,
                    "patientId": parentRef.patientId,
                    "medicationDate": currDateVal,
                    "medicationTradeNameId": medicationTradeNameIdVal,
                    "take": takeVal,
                    "medicationFormId": medicationFormIdVal,
                    "strength": strengthVal,
                    "medicationStrengthId": medicationStrengthIdVal,
                    "medicationRouteId": medicationRouteIdVal,
                    "medicationFrequencyId": medicationFrequencyIdVal,
                    "medicationInstructionsId": medicationInstructionsIdVal,
                    "dispenceQuanty": dispenceQuantyVal,
                    "medicationStartDate": currDateVal,
                    "medicationEndDate": medicationEndDateVal,
                    "longTerm": longTermVal,
                    "instruction": instructionVal,
                    "medicationActionId": medicationActionIdVal,
                    "medicationStatusId": medicationStatusIdVal,
                    "refillDate": refillDateVal,
                    "pharmacyId": pharmacyIdVal,
                    "pharamacistMessage": pharamacistMessageVal,
                    "medicationTransmissionId": transmissionIdVal,
                    "medicineTakenFor": medicineTakenForVal,
                    "sideEffects": sideEffectsVal,
                    "prescribedBy": prescribedByVal,
                    "facilityId": facilityIdVal,
                    "notes": notesVal
                }
                if(takeVal != "" && strengthVal != "" && medicationFormIdVal != "" && medicationFrequencyIdVal != "" && medicationStrengthIdVal != "" && medicationRouteIdVal != "" && medicationInstructionsIdVal != "" && dispenceQuantyVal != "" && medicationActionIdVal != "" && medicationStatusIdVal != "" && facilityIdVal != "" && prescribedByVal != "") {
                    if($(this).attr('data-savebtn-link') == "true") {
                        prescriptionObj.id = listFullData[getIndex].idDuplicate;
                        prescriptionObj.modifiedBy = sessionStorage.userId;
                        prescriptionObj.modifiedDate = currDateVal;
                        createAjaxObject(dataUpdateUrl,prescriptionObj,"POST",onUpdateItem,onError);
                    }
                    else {
                        createAjaxObject(dataUrl,prescriptionObj,"POST",onCreate,onError);
                    }
                }
                else {
                    $('.customAlert').append('<div class="alert alert-danger">Please fill all the Required Fields</div>');
                    if(facilityIdVal == ""){
                        $("#medication-facilityName").addClass("borderColor");
                    }
                    if(strengthVal == ""){
                        $('.pres-medicate').find('input[name="medication-formQty"]').addClass("borderColor");
                    }
                    if(medicationStrengthIdVal == ""){
                        $('#prescription-strength-select').addClass("borderColor");
                    }
                    if(takeVal == ""){
                        $('.pres-medicate').find('input[name="formMedicineTake"]').addClass("borderColor");
                    }
                    if(medicationFormIdVal == ""){
                        $('#prescription-form-select').addClass("borderColor");
                    }
                    if(medicationFrequencyIdVal == ""){
                        $('#prescription-frequency-select').addClass("borderColor");
                    }
                    if(medicationRouteIdVal == ""){
                        $('#prescription-route-select').addClass("borderColor");
                    }
                    if(medicationInstructionsIdVal == ""){
                        $('#prescription-instructions-select').addClass("borderColor");
                    }
                    if(dispenceQuantyVal == ""){
                        $('.pres-medicate').find('input[name="medication-dosageQuantity"]').addClass("borderColor");
                    }
                    if(medicationActionIdVal == ""){
                        $('#prescription-action-select').addClass("borderColor");
                    }
                    if(medicationStatusIdVal == ""){
                        $('#prescription-status-select').addClass("borderColor");
                    }
                    if(prescribedByVal == ""){
                        $('#medication-prescribedBy').addClass("borderColor");
                    }

                    presObj.bodyScrollFn();
                }
                console.log(prescriptionObj);
            });
            $("#prescription-reset-btn").on('click',function(e) {
                e.preventDefault();
                $('.alert').remove();
                $('#prescriptionForm')[0].reset();
                $('#longterm-switch').removeAttr('checked');
                //presObj.setMedicateValues();
                presObj.setDates();
            });
            $(".medication-rxNorm-search").on("click", function(e) {
                e.preventDefault();
                var urlExtn = '/medication-trade-name/list/';
                var rxNormSelectVal = $(".rxNorm-filter-select:visible").find('option:selected').attr('data-rxNorm-option');
                var rxNormInputVal = $(".rxNorm-filter-input:visible").val();
                var tradeNameInputVal = $(".tradeName-filter-input").val();
                $('.hasError-rxNorm').remove();
                if($(this).closest('.tradeName-data-wrapper').hasClass('showTradeData')) {
                    if(tradeNameInputVal != "") {
                        buildMedicationTradeListViewGrid([]);
                        getAjaxObject(ipAddress+urlExtn+'?name='+tradeNameInputVal,"GET",getTradeListData,onTradeListError);
                    }
                    else {
                        $('#medicationTradeData').prepend('<div class="hasError-rxNorm">Please Fill the Required Field</div>');
                        $('.hasError-rxNorm').show();
                    }
                }
            });
            $(".medicationNameSearch").on('click', function(e) {
                e.preventDefault();
                buildMedicationTradeListViewGrid([]);
                $('.tradeName-data-wrapper').addClass('showTradeData');
                $('.overlay-back').show();
            });
            $("#tradeName-ok-btn").on("click", function(e) {
                e.preventDefault();
                if(selectedItems[0] && selectedItems[0].name) {
                    $(".pres-medicate").find('input[name="medication-name"]').val(selectedItems[0].name);
                    $("#tradeDrugNameValue").val(selectedItems[0].idDuplicate);
                    $(".close-popup").trigger('click');
                }
                var medNameFormId = selectedItems[0].composition.medicationForm ? selectedItems[0].composition.medicationForm.id : "";
                var medNameStrengthId = selectedItems[0].composition.medicationStrength ? selectedItems[0].composition.medicationStrength.id : "";
                var medNameRouteId = selectedItems[0].composition.medicationRoute ? selectedItems[0].composition.medicationRoute.id : "";
                var medNameFrequencyId = selectedItems[0].composition.medicationFrequency ? selectedItems[0].composition.medicationFrequency.id : "";
                var medNameInstructionsId = selectedItems[0].composition.medicationInstructions ? selectedItems[0].composition.medicationInstructions.id : "";
                $('#prescription-form-select').find('option').each(function(){
                    if($(this).attr('data-prescription-val') == medNameFormId) {
                        $(this).attr('selected','selected');
                        return false;
                    }
                });
                $('#prescription-strength-select').find('option').each(function(){
                    if($(this).attr('data-prescription-val') == medNameStrengthId) {
                        $(this).attr('selected','selected');
                        return false;
                    }
                });
                $('#prescription-route-select').find('option').each(function(){
                    if($(this).attr('data-prescription-val') == medNameRouteId) {
                        $(this).attr('selected','selected');
                        return false;
                    }
                });
                $('#prescription-frequency-select').find('option').each(function(){
                    if($(this).attr('data-prescription-val') == medNameFrequencyId) {
                        $(this).attr('selected','selected');
                        return false;
                    }
                });
                $('#prescription-instructions-select').find('option').each(function(){
                    if($(this).attr('data-prescription-val') == medNameInstructionsId) {
                        $(this).attr('selected','selected');
                        return false;
                    }
                });
            });
            $(".close-popup").on("click", function(e) {
                e.preventDefault();
                $('.tradeName-data-wrapper').removeClass('showTradeData');
                $('.overlay-back').hide();
            });
            $('#pres-list-link').on('click', function(e) {
                e.preventDefault();
                $('.alert').remove();
                $('[data-pres-content="form"], #prescription-reset-btn, #prescription-save-btn, #pres-list-link').hide();
                $('[data-pres-content="list"]').show();
                $('#prescription-save-btn').removeAttr('data-savebtn-link');
                patientListFlag = false;
                getListSelectedItems();
                //getAjaxObject(ipAddress+"/patient-medication/list?patient-id="+parentRef.patientId,"GET",getPrescriptionListData,onError);
                $('.prescription-list-data').find('#listDataWrapper').empty();
                $('#pres-edit-link').attr('disabled','disabled');
                //$("#pres-ok-link").trigger('click');
            });
            $('#pres-back-link').on('click', function(e) {
                e.preventDefault();
                $('.alert').remove();
                $('[data-pres-content="list"]').hide();
                $('[data-pres-content="form"], #prescription-reset-btn, #prescription-save-btn, #pres-list-link').show();
                $('#prescription-save-btn').html('<span><img src="../../img/medication/Save.png" class="prescription-btn-img">Save</span>');
                $('#prescriptionForm')[0].reset();
                $('#longterm-switch').removeAttr('checked');
                // presObj.setMedicateValues();
                presObj.setDates();
            });
            $("#listDataWrapper").on('click','li', function(e) {
                e.preventDefault();
                $('#pres-edit-link').removeAttr('disabled');
                $(this).closest('ul').addClass('activeList').siblings('ul').removeClass('activeList');
                getIndex = $(this).closest('ul').attr('data-list-arr');
            });
            $("#pres-ok-link").on('click', function(e) {
                e.preventDefault();
                $('.alert').remove();
                var facilityIdVal = $('#medication-facilityName-list').find('option:selected').attr('data-prescription-val') || "";
                var prescribedByVal = $("#medication-prescribedBy-list").find('option:selected').attr('data-prescription-val') || "";
                var statusVal = $("#prescription-status-select-list").find('option:selected').attr('data-prescription-val') || "";
                //var startDateList = $('input[name="medication-startDate-list"]').val() || "";
                //var endDateList = $('input[name="medication-endDate-list"]').val() || "";
                var dataObj = "&patient-id="+ parentRef.patientId;
                if(facilityIdVal != "") {
                    dataObj = dataObj + "&facility-id=" + facilityIdVal;
                }
                if(prescribedByVal != "") {
                    dataObj = dataObj + "&provider-id=" + prescribedByVal;
                }
                if(statusVal != "") {
                    dataObj = dataObj + "&medication-status-id=" + statusVal;
                }
                /*if(startDateList != "") {
                    startDateListTime = startDateList.split('/').join('-');
                    dataObj = dataObj + "&from-date=" + getDateTime(startDateListTime);
                }
                if(endDateList != "") {
                    endDateListTime = endDateList.split('/').join('-');
                    dataObj = dataObj + "&to-date=" + getDateTime(endDateListTime);
                }*/
                getAjaxObject(ipAddress+"/patient-medication/list?is-active=1"+dataObj,"GET",getPrescriptionListData,onError);
            });
            function getDateTime(sTime){
                var now;
                var cntry = sessionStorage.countryName;
                if (cntry.indexOf("India") >= 0) {
                    now = new Date(sTime.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
                } else if (cntry.indexOf("United Kingdom") >= 0) {
                    now = new Date(sTime.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
                } else {
                    now = new Date(sTime);
                }
                var utc = now.getTime();
                return utc;
            }
            function getLocalTimeFromGMT(sTime){
                var dte = new Date(sTime);
                var dt = new Date(dte.getTime() - dte.getTimezoneOffset()*60*1000);
                return dt.getTime();
            }
            $('#pres-print-link').on('click', function(e) {
                e.preventDefault();
                $('.alert').remove();
                if($('#listDataWrapper').find('ul').length > 0) {
                    var facilityVal = $('#medication-facilityName-list').find('option:selected').val();
                    var facilityIdVal = $('#medication-facilityName-list').find('option:selected').attr('data-prescription-val');
                    var prescribedVal = $("#medication-prescribedBy-list").find('option:selected').val();
                    var prescribedIdVal = $("#medication-prescribedBy-list").find('option:selected').attr('data-prescription-val');
                    getAjaxObjectAsync(ipAddress + '/facility/list?id=' + facilityIdVal, "GET", onFacilityDataSuccess, onError);
                    function onFacilityDataSuccess(resp) {
                        var resp = resp.response.facility[0].communications[0];
                        $('.mediaPrintInfo').find(".mediaPrintInfo-facility").html('<h3>'+facilityVal+'</h3><p>'+resp.address1+', '+resp.address2+', '+resp.city+' - '+resp.zip+', '+resp.country+'</p><p><label>'+resp.workPhone+' - '+resp.workPhoneExt+'</label></p>');
                        getAjaxObjectAsync(ipAddress + '/patient/list?id=' + parentRef.patientId, "GET", onPatientDataSuccess, onError);
                        function onPatientDataSuccess(resp) {
                            var resp = resp.response.patient[0];
                            var respCommunication = resp.communications.length > 0 ? resp.communications[0] : {};
                            $('.mediaPrintInfo').find(".mediaPrintInfo-patient").html('<h3>'+resp.firstName+' '+resp.middleName+' '+resp.lastName+'</h3><p>'+respCommunication.address1+', '+respCommunication.address2+', '+respCommunication.city+' - '+respCommunication.zip+', '+respCommunication.country+'</p><p><label>'+respCommunication.workPhone+' - '+respCommunication.workPhoneExt+'</label></p>');
                            getAjaxObjectAsync(ipAddress + '/provider/list?id=' + prescribedIdVal, "GET", onProviderDataSuccess, onError);
                            function onProviderDataSuccess(resp) {
                                var resp = resp.response.provider[0];
                                var respCommunication = resp.communications.length > 0 ? resp.communications[0] : {};
                                $('.mediaPrintInfo').find(".mediaPrintInfo-provider").html('<h3>'+resp.firstName+' '+resp.middleName+' '+resp.lastName+'</h3><p>'+respCommunication.address1+', '+respCommunication.address2+', '+respCommunication.city+' - '+respCommunication.zip+', '+respCommunication.country+'</p><p><label>'+respCommunication.workPhone+' - '+(respCommunication.workPhoneExt != null ? respCommunication.workPhoneExt : "-")+'</label></p>');
                                window.print();
                            }
                        }
                    }
                }
                else {
                    $('.prescription-list-data').prepend('<div class="alert alert-danger">No data to Print</div>');
                }
            });
            $('#pres-edit-link').on('click', function(e) {
                e.preventDefault();
                setTimeout(function() {
                    presObj.setMedicateValues();
                });
                $('#pres-back-link').trigger('click');
                $('#prescription-save-btn').html('<span><img src="../../img/medication/Save.png" class="prescription-btn-img">Update</span>');
                $('#prescription-save-btn').attr('data-savebtn-link','true');
                $('#tradeDrugNameValue').val(listFullData[getIndex].medicationTradeNameId);
                $('#prescription-form-select').find('option').each(function() {
                    if($(this).attr('data-prescription-val') == listFullData[getIndex].medicationFormId) {
                        $(this).attr('selected','selected');
                        return false;
                    }
                    else {
                        $(this).removeAttr('selected');
                    }
                });
                $('#prescription-strength-select').find('option').each(function() {
                    if($(this).attr('data-prescription-val') == listFullData[getIndex].medicationStrengthId) {
                        $(this).attr('selected','selected');
                        return false;
                    }
                    else {
                        $(this).removeAttr('selected');
                    }
                });
                $('#prescription-route-select').find('option').each(function() {
                    if($(this).attr('data-prescription-val') == listFullData[getIndex].medicationRouteId) {
                        $(this).attr('selected','selected');
                        return false;
                    }
                    else {
                        $(this).removeAttr('selected');
                    }
                });
                $('#prescription-frequency-select').find('option').each(function() {
                    if($(this).attr('data-prescription-val') == listFullData[getIndex].medicationFrequencyId) {
                        $(this).attr('selected','selected');
                        return false;
                    }
                    else {
                        $(this).removeAttr('selected');
                    }
                });
                $('#prescription-instructions-select').find('option').each(function() {
                    if($(this).attr('data-prescription-val') == listFullData[getIndex].medicationInstructionsId) {
                        $(this).attr('selected','selected');
                        return false;
                    }
                    else {
                        $(this).removeAttr('selected');
                    }
                });
                $('#prescription-action-select').find('option').each(function() {
                    if($(this).attr('data-prescription-val') == listFullData[getIndex].medicationActionId) {
                        $(this).attr('selected','selected');
                        return false;
                    }
                    else {
                        $(this).removeAttr('selected');
                    }
                });
                $('#prescription-status-select').find('option').each(function() {
                    if($(this).attr('data-prescription-val') == listFullData[getIndex].medicationStatusId) {
                        $(this).attr('selected','selected');
                        return false;
                    }
                    else {
                        $(this).removeAttr('selected');
                    }
                });
                $('#medicationPharmacy').find('option').each(function() {
                    if($(this).attr('data-prescription-val') == listFullData[getIndex].pharmacyId) {
                        $(this).attr('selected','selected');
                        return false;
                    }
                    else {
                        $(this).removeAttr('selected');
                    }
                });
                $('#prescription-transmission-select').find('option').each(function() {
                    if($(this).attr('data-prescription-val') == listFullData[getIndex].medicationTransmissionId) {
                        $(this).attr('selected','selected');
                        return false;
                    }
                    else {
                        $(this).removeAttr('selected');
                    }
                });
                $('#medication-prescribedBy').find('option').each(function() {
                    if($(this).attr('data-prescription-val') == listFullData[getIndex].prescribedBy) {
                        $(this).attr('selected','selected');
                        return false;
                    }
                    else {
                        $(this).removeAttr('selected');
                    }
                });
                $('#medication-facilityName').find('option').each(function() {
                    if($(this).attr('data-prescription-val') == listFullData[getIndex].facilityId) {
                        $(this).attr('selected','selected');
                        return false;
                    }
                    else {
                        $(this).removeAttr('selected');
                    }
                });


                if(listFullData[getIndex].longTerm == 1) {
                    listFullData[getIndex].medicationEndDateVal = "";
                }
                $('.pres-medicate').find('input[name="medication-currentDate"]').val(onGetDate(listFullData[getIndex].medicationDateVal));
                if((listFullData[getIndex].composition.tradeName)){
                    $('.pres-medicate').find('input[name="medication-name"]').val(listFullData[getIndex].composition.tradeName.name);
                }
                $('.pres-medicate').find('input[name="medication-formQty"]').val(listFullData[getIndex].strength);
                $('.pres-medicate').find('input[name="medication-dosageQuantity"]').val(listFullData[getIndex].dispenceQuanty);
                $('.pres-medicate').find('input[name="medication-startDate"]').val(onGetDate(listFullData[getIndex].medicationStartDateVal));
                $('.pres-medicate').find('input[name="medication-endDate"]').val(onGetDate(listFullData[getIndex].medicationEndDateVal));
                if(listFullData[getIndex].longTerm == 1) {
                    $('#longterm-switch').attr('checked','checked');
                }
                else {
                    $('#longterm-switch').removeAttr('checked');
                }
                $('.pres-medicate').find('textarea[name="medicate-dosageInstructions"]').val(listFullData[getIndex].instruction);
                $('.pres-medicate').find('input[name="medication-pharmacy-message"]').val(listFullData[getIndex].pharamacistMessage);
                $('.pres-medicate').find('input[name="medication-takenFor"]').val(listFullData[getIndex].medicineTakenFor);
                $('.pres-medicate').find('input[name="medication-additionalNotes"]').val(listFullData[getIndex].notes);
                $('.pres-medicate').find('input[name="medication-sideEffects"]').val(listFullData[getIndex].notes);
                $('.pres-medicate').find('input[name="medication-refillDate"]').val(onGetDate(listFullData[getIndex].refillDateVal));
                $('.pres-medicate').find('input[name="formMedicineTake"]').val(listFullData[getIndex].take);
                console.log(listFullData[$(this).closest('ul').attr('data-list-arr')]);
            });
            function onCreate(dataObj){
                console.log(dataObj);
                if(dataObj && dataObj.response && dataObj.response.status){
                    if(dataObj.response.status.code == "1") {
                        //$('.prescription-wrapperForm')[0].reset();
                        $('.alert').remove();
                        $('.customAlert').append('<div class="alert alert-success">success</div>');
                        $('#prescriptionForm')[0].reset();
                        $('#longterm-switch').removeAttr('checked');
                        //presObj.setMedicateValues();
                        presObj.setDates();
                        presObj.bodyScrollFn();
                    }
                    else {
                        //$('.prescription-wrapperForm')[0].reset();
                        $('.customAlert').append('<div class="alert alert-danger">error</div>');
                        presObj.bodyScrollFn();
                    }
                }
            }
            function onUpdateItem(respObj) {
                console.log(respObj);
                if(respObj && respObj.response && respObj.response.status){
                    if(respObj.response.status.code == "1") {
                        $('.alert').remove();
                        $('.customAlert').append('<div class="alert alert-success">Updated Successfully</div>');
                        $('#prescriptionForm')[0].reset();
                        $('#longterm-switch').removeAttr('checked');
                        //presObj.setMedicateValues();
                        presObj.setDates();
                        presObj.bodyScrollFn();
                    }
                    else {
                        $('.pres-medicate').find('input[name="medication-currentDate"]').val(onGetDate(listFullData[getIndex].medicationDateVal));
                        $('.pres-medicate').find('input[name="medication-startDate"]').val(onGetDate(listFullData[getIndex].medicationStartDateVal));
                        $('.pres-medicate').find('input[name="medication-endDate"]').val(onGetDate(listFullData[getIndex].medicationEndDateVal));
                        $('.pres-medicate').find('input[name="medication-refillDate"]').val(onGetDate(listFullData[getIndex].refillDateVal));
                        $('.customAlert').append('<div class="alert alert-danger">'+respObj.response.status.message+'</div>');
                        presObj.bodyScrollFn();
                    }
                }
            }
            function onError(errObj){
                console.log(errObj);
                customAlert.error("Error","Error");
            }
            function getListSelectedItems() {
                var dataMedicateList = ['/master/medication_status','/provider','/facility'];
                $.each(dataMedicateList,function(i,value) {
                    var urlExtn = value + '/list';
                    retrieveSelectData(value,urlExtn);
                });
                function retrieveSelectData(formName,url) {
                    $('.medicationPrescription-select').each(function() {
                        var selfVal = $(this).attr('medicate-pres-select');
                        //$(this).html('<option value="" disabled selected>'+selfVal+'</option>');
                    });
                    getAjaxObjectAsync(ipAddress+url+'?is-active=1',"GET",onGetListSelectedItemsSuccess, onError);
                    function onGetListSelectedItemsSuccess(resp) {
                        var medicationPresData = [], dataArr = [];
                        if(formName == '/provider' && resp && resp.response && resp.response.provider) {
                            var providersData = resp.response.provider;
                            $('#medication-prescribedBy-list').empty();
                            var providerFlag = true;
                            for (var i = 0; i < providersData.length; i++) {
                                if(providersData[i].type == 100) {
                                    if(providerFlag) {
                                        $('#medication-prescribedBy-list').append('<option data-prescription-val="">All</option>');
                                        providerFlag = false;
                                    }
                                    var provideObj = {
                                        "id": providersData[i].id,
                                        "abbreviation": providersData[i].abbreviation,
                                        "firstName": providersData[i].firstName,
                                        "lastName": providersData[i].lastName
                                    }
                                    $('#medication-prescribedBy-list').append('<option data-prescription-val="'+provideObj.id+'">'+provideObj.abbreviation+' '+provideObj.firstName+','+provideObj.lastName+'</option>');
                                }
                            }
                        }
                        else if(formName == '/facility' && resp && resp.response && resp.response.facility) {
                            var facilityData = resp.response.facility;
                            $('#medication-facilityName-list').empty();
                            for (var i = 0; i < facilityData.length; i++) {
                                var facilityObj = {
                                    "id": facilityData[i].id,
                                    "displayName": facilityData[i].displayName
                                }
                                $('#medication-facilityName-list').append('<option data-prescription-val="'+facilityObj.id+'">'+facilityObj.displayName+'</option>');
                            }
                        }
                        else if(formName == '/master/medication_status' && resp && resp.response && resp.response.codeTable){
                            $('#prescription-status-select-list').empty();
                            if($.isArray(resp.response.codeTable)){
                                dataArr = resp.response.codeTable;
                            }
                            else{
                                dataArr.push(resp.response.codeTable);
                            }
                            if(dataArr && dataArr.length > 0) {
                                for(var i=0;i<dataArr.length;i++){
                                    if(dataArr[i].isActive == 1) {
                                        medicationPresData.push(dataArr[i]);
                                    }
                                }
                            }
                            $.each(medicationPresData, function(i,val) {
                                $('#prescription-status-select-list').append('<option data-prescription-val="'+val.id+'">'+val.desc+'</option>');
                            });
                        }
                        if($('#medication-prescribedBy-list').find('option').length > 1 && $('#medication-facilityName-list').find('option').length  > 1 && $('#prescription-status-select-list').find('option').length > 1) {
                            $("#pres-ok-link").trigger('click');
                        }
                    }
                }
            }
            function getPrescriptionListData(resp) {
                var dataListArr = [], dataArray;
                $('.prescription-list-data').find('#listDataWrapper').empty();
                if(resp && resp.response && resp.response.patientMedication){
                    if($.isArray(resp.response.patientMedication)){
                        dataArray = resp.response.patientMedication;
                    }
                    else{
                        dataArray.push(resp.response.patientMedication);
                    }
                }
                if((resp && resp.response && resp.response.status.code == "0") || (resp && resp.response && resp.response.status.code == "1" && !resp.response.patientMedication)) {
                    $('.prescription-list-data').prepend('<div class="alert alert-danger">'+resp.response.status.message+'</div>');
                }
                /*else if(resp && resp.response && resp.response.status.code == "1" && !resp.response.patientMedication) {

                }*/
                if(dataArray != undefined) {
                    for(var i=0;i<dataArray.length;i++){
                        dataArray[i].idDuplicate = dataArray[i].id;
                        if(dataArray[i].isActive == 1){
                            dataListArr.push(dataArray[i]);
                        }
                    }
                    listFullData = dataListArr;
                }
                for (var i = 0; i < dataListArr.length; i++) {
                    dataListArr[i].drugInfo = dataListArr[i].composition.rxNorm != undefined ? dataListArr[i].composition.rxNorm.drugInfo : "";
                    dataListArr[i].tradeNameVal = dataListArr[i].medicationTradeNameId != undefined ? dataListArr[i].composition.tradeName.name : "";
                    dataListArr[i].formVal = dataListArr[i].composition.medicationForm != undefined ? dataListArr[i].composition.medicationForm.desc : "";
                    dataListArr[i].strengthVal = dataListArr[i].composition.medicationStrength != undefined ? dataListArr[i].composition.medicationStrength.desc : "";
                    dataListArr[i].routeVal = dataListArr[i].composition.medicationRoute != undefined ? dataListArr[i].composition.medicationRoute.desc : "";
                    dataListArr[i].frequencyVal = dataListArr[i].composition.medicationFrequency != undefined ? dataListArr[i].composition.medicationFrequency.desc : "";
                    dataListArr[i].instructionsVal = dataListArr[i].composition.medicationInstructions != undefined ? dataListArr[i].composition.medicationInstructions.desc : "";
                    dataListArr[i].transmissionVal = dataListArr[i].composition.medicationTransmission != undefined ? dataListArr[i].composition.medicationTransmission.desc : "";
                    dataListArr[i].dispenceQuantyVal = dataListArr[i].dispenceQuanty != null ? dataListArr[i].dispenceQuanty : "";
                    dataListArr[i].facilityIdVal = dataListArr[i].facilityId != null ? dataListArr[i].composition.facility.name : "";
                    dataListArr[i].prescribedByVal = dataListArr[i].prescribedBy != null ? (dataListArr[i].composition.prescribedBy.firstName + ", " + dataListArr[i].composition.prescribedBy.lastName) : "";
                    dataListArr[i].pharmacyIdVal = dataListArr[i].pharmacyId != null ? dataListArr[i].composition.pharmacy.name : "";
                    dataListArr[i].medicationDateVal = dataListArr[i].medicationDate != null ? new Date(dataListArr[i].medicationDate).toLocaleDateString() : "";
                    dataListArr[i].refillDateVal = dataListArr[i].refillDate != null ? new Date(dataListArr[i].refillDate).toLocaleDateString() : "";
                    dataListArr[i].medicationStartDateVal = dataListArr[i].medicationStartDate != null ? new Date(dataListArr[i].medicationStartDate).toLocaleDateString() : "";
                    dataListArr[i].medicationEndDateVal = dataListArr[i].medicationEndDate != null ? new Date(dataListArr[i].medicationEndDate).toLocaleDateString() : "LongTerm";
                    $('.prescription-list-data').find('#listDataWrapper').append('<ul data-list-arr='+i+'><li class="reduceWidth"><div><label>'+onGetDate(dataListArr[i].medicationDateVal)+'</label><label>'+dataListArr[i].transmissionVal+'</label></div></li>'+
                        '<li><div><label>'+dataListArr[i].tradeNameVal+'</label></div></li>'+
                        '<li class="reduceWidth"><div><label>'+dataListArr[i].dispenceQuantyVal+'</label></div></li>'+
                        '<li class="reduceWidth"><div><label>'+onGetDate(dataListArr[i].medicationStartDateVal)+'<label>to</label>'+onGetDate(dataListArr[i].medicationEndDateVal)+'</label></div></li>'+
                        '<li><div><label>'+dataListArr[i].formVal+'</label><label>'+dataListArr[i].routeVal+'</label><label>'+dataListArr[i].frequencyVal+'</label></div></li>'+
                        '<li><div><label>'+dataListArr[i].instructionsVal+'</label></div></li>'+
                        '<li class="reduceWidth"><div><label>'+onGetDate(dataListArr[i].refillDateVal)+'</label></div></li>'+
                        '<li><div><label>'+dataListArr[i].sideEffects+'</label></div></li>'+
                        '<li><div><label>'+dataListArr[i].facilityIdVal+'</label><label>'+dataListArr[i].prescribedByVal+'</label></div></li>'+
                        '<li><div><label>'+dataListArr[i].pharmacyIdVal+'</label><label>'+dataListArr[i].pharamacistMessage+'</label></div></li>'+
                        '</ul>');
                }
                /*if(!patientListFlag) {
                    getListSelectedItems();
                    patientListFlag = true;
                }*/
            }
            $('#pres-list-link').trigger('click');
        }
    };
    function buildMedicationTradeListViewGrid(dataSource) {
        var gridColumns = [];
        var otoptions = {};
        otoptions.noUnselect = false;
        if(dataSource.length > 0) {
            var k;
            for (k = 0; k < dataSource.length; k++) {
                dataSource[k].drugInfo = dataSource[k].composition.rxNorm != undefined ? dataSource[k].composition.rxNorm.drugInfo : "";
                dataSource[k].formVal = dataSource[k].composition.medicationForm != undefined ? dataSource[k].composition.medicationForm.desc : "";
                dataSource[k].strengthVal = dataSource[k].composition.medicationStrength != undefined ? dataSource[k].composition.medicationStrength.desc : "";
                dataSource[k].routeVal = dataSource[k].composition.medicationRoute != undefined ? dataSource[k].composition.medicationRoute.desc : "";
                dataSource[k].frequencyVal = dataSource[k].composition.medicationFrequency != undefined ? dataSource[k].composition.medicationFrequency.desc : "";
                dataSource[k].instructionsVal = dataSource[k].composition.medicationInstructions != undefined ? dataSource[k].composition.medicationInstructions.desc : "";
            }
        }
        gridColumns.push({
            "title": "Name",
            "field": "name",
            "width": "*"
        });
        gridColumns.push({
            "title": "Drug Info",
            "field": "drugInfo",
            "width": "*"
        });
        gridColumns.push({
            "title": "Form Id",
            "field": "formVal",
            "width": "*"
        });
        gridColumns.push({
            "title": "Strength Id",
            "field": "strengthVal",
            "width": "*"
        });
        gridColumns.push({
            "title": "Route Id",
            "field": "routeVal",
            "width": "*"
        });
        gridColumns.push({
            "title": "Frequency Id",
            "field": "frequencyVal",
            "width": "*"
        });
        gridColumns.push({
            "title": "Instructions Id",
            "field": "instructionsVal",
            "width": "*"
        });
        tradeNameListUIgrid.creategrid(dataSource, gridColumns,otoptions);
        adjustHeight();
    }
    function getTradeListData(resp) {
        var dataListArr = [], dataArray;
        if(resp && resp.response && resp.response.medicationTradeName){
            if($.isArray(resp.response.medicationTradeName)){
                dataArray = resp.response.medicationTradeName;
            }
            else{
                dataArray.push(resp.response.medicationTradeName);
            }
        }
        if(dataArray != undefined) {
            for(var i=0;i<dataArray.length;i++){
                dataArray[i].idDuplicate = dataArray[i].id;
                if(dataArray[i].isActive == 1){
                    dataListArr.push(dataArray[i]);
                }
            }
        }
        if(dataListArr && dataListArr.length == 0) {
            $('#medicationTradeData').prepend('<div class="hasError-rxNorm">No Results Found for Required Name</div>');
            $('.hasError-rxNorm').show();
        }
        buildMedicationTradeListViewGrid(dataListArr);
    }
    function onTradeListError(err) {
        console.log(err);
        customAlert.error("Error","Error");
    }
    function adjustHeight(){
        var defHeight = 220;//+window.top.getAvailPageHeight();
        if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
            defHeight = 80;
        }
        var cmpHeight = window.innerHeight - defHeight;
        cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
        tradeNameListUIgrid.adjustGridHeight(cmpHeight);
    }
    function disableButtons() {
        $('.tableListbtn-wrapper').find('.medication-btn-link').each(function() {
            if(selectedItems && selectedItems.length>0){
                $(this).removeAttr('disabled');
            }
            else {
                $(this).attr('disabled','disabled');
            }
        });
    }
    function getCountryZoneName() {
        $.getJSON('//freegeoip.net/json/?callback=', function(dataObj) {
            var dataUrl = ipAddress + "/user/location";
            console.log(dataObj);
            sessionStorage.country = dataObj.country_code;
            dataObj.createdBy = sessionStorage.userId;
            dataObj.countryCode = dataObj.country_code;
            dataObj.countryName = dataObj.country_name;
            dataObj.regionCode = dataObj.region_code;
            dataObj.regionName = dataObj.region_name;
            dataObj.timeZone = dataObj.time_zone;
            dataObj.metroCode = dataObj.metro_code;
            dataObj.zipCode = dataObj.zip_code;
            sessionStorage.countryName = dataObj.country_name;
            /*if (sessionStorage.countryName.indexOf("United Kingdom") >= 0) {
                $(".datepicker").kendoDatePicker({format: "MM/dd/yyyy"});
            } else {
                //$(".datepicker").kendoDatePicker({format: "dd/MM/yyyy"});
                $(".datepicker").kendoDatePicker({format: "MM/dd/yyyy"});
            }*/
        });
    }
    //getCountryZoneName();
    presObj.bindFunctions();
});
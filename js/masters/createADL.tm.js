var parentRef = null;
var operation = "";
var selItem = null;
var ADD = "add";
var UPDATE = "update";
var VIEW = "view";
var DELETE = "delete";

var files = null;
var fileName = "";
var compressorSettings = {
    toWidth: 30,
    toHeight: 42,
    mimeType: 'image/png',
    mode: 'strict',
    quality: 0.2,
    grayScale: false,
    sepia: false,
    threshold: false,
    vReverse: false,
    hReverse: false,
    speed: 'low'
};
var isBrowseFlag = false;

var imagePhotoData = null;
var componentId;

var IsThumbnailPresent =0;


var statusArr = [{Key:'Active',Value:'Active'},{Key:'InActive',Value:'InActive'}];
var typeArr = [{Key:'ADL',Value:'1'},{Key:'iADL',Value:'2'},{Key:'Care',Value:'3'},{Key:'Fluid',Value:'4'},{Key:'Food',Value:'5'}];
var compType = [{Key:'Text',Value:'1'},{Key:'Text Area',Value:'2'},{Key:'Boolean',Value:'3'},{Key:'Checkbox',Value:'4'},{Key:'Radio Button',Value:'5'}];

$(document).ready(function(){
    parentRef = parent.frames['iframe'].window;
    $("#divDuration").hide();
    $("#divCharge").hide();
    document.getElementById('txtDisplayOrder').value = sessionStorage.getItem("displayOrderMaxlength");
});

function adjustHeight(){
    var defHeight = 30;
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 40;
    }
}

$(window).load(function(){
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
    init();
});

function init(){
    operation = parentRef.operation;
    selItem = parentRef.selitem;
    //$("#cmbCountry").kendoComboBox();
    $("#cmbType").kendoComboBox();
    $("#cmbStatus").kendoComboBox();
    setDataForSelection(statusArr, "cmbStatus", onStatusChange, ["Value", "Key"], 0, "");

    getAjaxObject(ipAddress+"/master/type/list/?name=component","GET",getComponentTypes,onError);
    getAjaxObject(ipAddress+"/activity/list?is-active=1&is-deleted=0&sort=activity","GET",getStateList,onError);

    //setDataForSelection(compType, "cmbType", onCompTypeChange, ["Key", "Value"], 0, "");

    //setDataForSelection(typeArr, "txtID", onTYpeChange, ["Key", "Value"], 0, "");



    buttonEvents();
}

function getComponentTypes(dataObj){
    var tempCompType = [];
    compType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.typeMaster){
            if($.isArray(dataObj.response.typeMaster)){
                tempCompType = dataObj.response.typeMaster;
            }else{
                tempCompType.push(dataObj.response.typeMaster);
            }
        }
    }
    for(var i=0;i<tempCompType.length;i++){
        var obj = {};
        obj.Key = tempCompType[i].type;
        obj.Value = tempCompType[i].id;
        compType.push(obj);
    }
    setDataForSelection(compType, "cmbType", onCompTypeChange, ["Key", "Value"], 0, "");

    getAjaxObject(ipAddress+"/master/type/list/?name=activity","GET",getActivityTypes,onError);
}

function getActivityTypes(dataObj){
    console.log('init');
    var tempCompType = [];
    typeArr = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.typeMaster){
            if($.isArray(dataObj.response.typeMaster)){
                tempCompType = dataObj.response.typeMaster;
            }else{
                tempCompType.push(dataObj.response.typeMaster);
            }
        }
    }
    tempCompType.sort(function(a, b) {
        var nameA = a.type.toUpperCase(); // ignore upper and lowercase
        var nameB = b.type.toUpperCase(); // ignore upper and lowercase
        if (nameA < nameB) {
            return -1;
        }
        if (nameA > nameB) {
            return 1;
        }
        // names must be equal
        return 0;
    });

    for(var i=0;i<tempCompType.length;i++){
        var obj = {};
        obj.Key = tempCompType[i].type;
        obj.Value = tempCompType[i].id;
        typeArr.push(obj);

    }

    setDataForSelection(typeArr, "txtID", onTYpeChange, ["Key", "Value"], 0, "");

    if(operation == UPDATE && selItem){
        console.log(selItem);

        componentId = selItem.idk;
        $("#txtActivityName").val(selItem.activity);
        $("#txtAbbrevation").val(selItem.abbr);
        $("#taDesc").val(selItem.description);
        $("#txtDuration").val(selItem.duration);
        $("#txtCharge").val(selItem.charge);
        $("#txtDisplayOrder").val(selItem.displayOrder);

        if(selItem.remarksRequired == 1){
            $("#chkRemarks").prop("checked",true);
        }
        else{
            $("#chkRemarks").prop("checked",false);
        }
        if(selItem.camera == 1){
            $("#chkCamera").prop("checked",true);
        }
        else{
            $("#chkCamera").prop("checked",false);
        }

        onShowImage();
        // if (componentId != "") {
        //     var imageServletUrl = ipAddress + "/homecare/download/components/thumbnail/?id=" + componentId + "&access_token=" + sessionStorage.access_token + "&tenant=" + sessionStorage.tenant;
        //     $("#imgPhoto").attr("src", imageServletUrl);
        // }




        var cmbStatus = $("#cmbStatus").data("kendoComboBox");
        if(cmbStatus){
            if(selItem.isActive == 1){
                cmbStatus.select(0);
            }else{
                cmbStatus.select(1);
            }
        }
        var txtID = $("#txtID").data("kendoComboBox");
        if(txtID){
            var aid = getActivityNameById(selItem.activityTypeId);
            txtID.select(aid);
        }
        var cmbType = $("#cmbType").data("kendoComboBox");
        if(cmbType){
            var aid = getTypeNameById(selItem.componentId);
            cmbType.select(aid);
        }
        onCompTypeChange();
        onTYpeChange();
    }
    else{
        var txtID = $("#txtID").data("kendoComboBox");
        var strVal = txtID.value();
        var displayOrder = 0;
        displayOrder = _.where(actComponents, {activityTypeId: parseInt(strVal)}).length + 1;
        $("#txtDisplayOrder").val(displayOrder);
    }
}
function getActivityNameById(aId){
    for(var i=0;i<typeArr.length;i++){
        var item = typeArr[i];
        if(item && item.Value == aId){
            return i;
        }
    }
    return 0;
}
function getTypeNameById(aId){
    for(var i=0;i<compType.length;i++){
        var item = compType[i];
        if(item && item.Value == aId){
            return i;
        }
    }
    return 0;
}
function onError(errorObj){
    console.log(errorObj);
}
function getCountryList(dataObj){
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.country){
        if($.isArray(dataObj.response.country)){
            dataArray = dataObj.response.country;
        }else{
            dataArray.push(dataObj.response.country);
        }
    }
    var tempDataArry = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
    }
    console.log(dataArray);
    setDataForSelection(dataArray, "cmbCountry", onCountryChange, ["country", "idk"], 0, "");

    if(operation == UPDATE && selItem){
        console.log(selItem);
        $("#txtAbbrevation").val(selItem.abbr);
        $("#txtName").val(selItem.state);
        $("#txtCode").val(selItem.code);
        $("#btnReset").hide();
        /*var county = selItem.county;
        if(county){
            var countyIdx = getComboListIndex("cmbCounty","county",county);
            if(countyIdx>=0){
                var cmbCounty = $("#cmbCounty").data("kendoComboBox");
                if(cmbCounty){
                    cmbCounty.select(countyIdx);
                }
            }
        }*/
        var country = selItem.country;
        if(country){
            var countryIdx = getComboListIndex("cmbCountry","country",country);
            if(countryIdx>=0){
                var cmbCountry = $("#cmbCountry").data("kendoComboBox");
                if(cmbCountry){
                    cmbCountry.select(countryIdx);
                }
            }
        }
    }else{
        var cmbStatus = $("#cmbStatus").data("kendoComboBox");
        if(cmbStatus){
            cmbStatus.select(0);
            cmbStatus.enable(false);
        }
    }

    //getAjaxObject(ipAddress+"/county/list/","GET",getCountyList,onError);


}
function getCountyList(dataObj){
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.countyext){
        if($.isArray(dataObj.response.countyext)){
            dataArray = dataObj.response.countyext;
        }else{
            dataArray.push(dataObj.response.countyext);
        }
    }
    var tempDataArry = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
    }
    console.log(dataArray);
    var obj = {};
    obj.county = "";
    obj.idk = "";
    dataArray.unshift(obj);
    //setDataForSelection(dataArray, "cmbCounty", onCountyChange, ["county", "idk"], 0, "");

    if(operation == UPDATE && selItem){
        console.log(selItem);
        $("#btnReset").hide();
        $("#txtAbbrevation").val(selItem.abbr);
        $("#txtName").val(selItem.state);
        $("#txtCode").val(selItem.code);
        /*var county = selItem.county;
        if(county){
            var countyIdx = getComboListIndex("cmbCounty","county",county);
            if(countyIdx>=0){
                var cmbCounty = $("#cmbCounty").data("kendoComboBox");
                if(cmbCounty){
                    cmbCounty.select(countyIdx);
                }
            }
        }*/
        var country = selItem.country;
        if(country){
            var countryIdx = getComboListIndex("cmbCountry","country",country);
            if(countryIdx>=0){
                var cmbCountry = $("#cmbCountry").data("kendoComboBox");
                if(cmbCountry){
                    cmbCountry.select(countryIdx);
                }
            }
        }
    }else{
        var cmbStatus = $("#cmbStatus").data("kendoComboBox");
        if(cmbStatus){
            cmbStatus.select(0);
            cmbStatus.enable(false);
        }
    }
}
function onCountryChange(){
    var cmbCountry = $("#cmbCountry").data("kendoComboBox");
    if(cmbCountry && cmbCountry.selectedIndex<0){
        cmbCountry.select(0);
    }
}
function onStatusChange(){
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if(cmbStatus && cmbStatus.selectedIndex<0){
        cmbStatus.select(0);
    }
}
function onTYpeChange(){
    var txtID = $("#txtID").data("kendoComboBox");
    if(txtID && txtID.selectedIndex<0){
        txtID.select(0);
    }
    if(txtID){
        var strVal = txtID.value();
        if(strVal  == '1' || strVal == '2' ){
            /*$("#divDuration").show();*/
            /*$("#divCharge").show();*/
            $("#txtDuration").attr("readonly",false);
            $("#txtCharge").attr("readonly",false);
            $("#spnDuration").hide();
            $("#spnCharge").hide();
        }else{
            $("#divDuration").hide();
            $("#divCharge").hide();
            //$("#txtDuration").attr("readonly",true);
            //$("#txtCharge").attr("readonly",true);
            $("#spnDuration").hide();
            $("#spnCharge").hide();
        }

    }
    if(operation == ADD) {
        var displayOrder = 0;
        displayOrder = _.where(actComponents, {activityTypeId: parseInt(strVal)}).length + 1;
        $("#txtDisplayOrder").val(displayOrder);
    }
}
function onCompTypeChange(){
    var cmbType = $("#cmbType").data("kendoComboBox");
    if(cmbType && cmbType.selectedIndex<0){
        cmbType.select(0);
    }
    $("#taDesc").val("");
    $("#taDesc").attr("readonly",false);
    if(cmbType.text() == "check box"){
        $("#taDesc").attr("readonly",true);
    }
    if(operation == UPDATE && selItem && cmbType.text() != "check box"){
        $("#taDesc").val(selItem.description);
    }
}
function buttonEvents(){
    $("#btnCancel").off("click",onClickCancel);
    $("#btnCancel").on("click",onClickCancel);

    $("#btnSave").off("click",onClickSave);
    $("#btnSave").on("click",onClickSave);

    $("#btnSearch").off("click",onClickSearch);
    $("#btnSearch").on("click",onClickSearch);

    $("#btnReset").off("click");
    $("#btnReset").on("click",onClickReset);

    $("#btnBrowse").off("click", onClickBrowse);
    $("#btnBrowse").on("click", onClickBrowse);

    $("#btnBrowse").off("mouseover", onClickBrowseOver);
    $("#btnBrowse").on("mouseover", onClickBrowseOver);

    $("#btnBrowse").off("mouseout", onClickBrowseOut);
    $("#btnBrowse").on("mouseout", onClickBrowseOut);

    $("#fileElem").off("change", onSelectionFiles);
    $("#fileElem").on("change", onSelectionFiles);

    $("#fileElem").off("click", onSelectionFiles);
    $("#fileElem").on("click", onSelectionFiles);

    // allowAlphaNumericwithSapce("txtActivityName");

}

function onClickReset(){
    operation = ADD;
    selItem = null;
    $("#txtAbbrevation").val("");
    $("#txtCode").val("");
    $("#txtName").val("");
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if(cmbStatus){
        cmbStatus.select(0);
        cmbStatus.enable(false);
    }
    var cmbCountry = $("#cmbCountry").data("kendoComboBox");
    if(cmbCountry){
        cmbCountry.select(0);
    }
    $("#chkRemarks").prop("checked",false);
    $("#chkCamera").prop("checked",false);
    /*var cmbCounty = $("#cmbCounty").data("kendoComboBox");
    if(cmbCounty){
        cmbCounty.select(0);
    }*/
}
function validation(){
    var flag = true;
    var strName = $("#txtActivityName").val();
    strName = $.trim(strName);
    if(strName == ""){
        customAlert.error("Error","Enter task name");
        flag = false;
        return false;
    }
    var txtID = $("#txtID").data("kendoComboBox");
    if(txtID){
        var strVal = txtID.value();
        if(strVal == '1' || strVal == '2' ){
            var txtDuration = $("#txtDuration").val();
            txtDuration = $.trim(txtDuration);
            if(txtDuration == ""){
                customAlert.error("Error","Enter duration");
                flag = false;
                return false;
            }
            var txtCharge = $("#txtCharge").val();
            txtCharge = $.trim(txtCharge);
            if(txtCharge == ""){
                customAlert.error("Error","Enter charge");
                flag = false;
                return false;
            }
        }
    }
    return flag;
}

function onClickSave(){
    if(validation()) {
        var strName = $("#txtActivityName").val();
        strName = $.trim(strName);
        var txtID = $("#txtID").data("kendoComboBox");
        var cmbType = $("#cmbType").data("kendoComboBox");
        var taDesc = $("#taDesc").val();
        taDesc = $.trim(taDesc);
        var txtDuration = $("#txtDuration").val();
        txtDuration = $.trim(txtDuration);

        var txtCharge = $("#txtCharge").val();

        var isActive = 0;
        var cmbStatus = $("#cmbStatus").data("kendoComboBox");
        if (cmbStatus) {
            if (cmbStatus.selectedIndex == 0) {
                isActive = 1;
            }
        }

        var IsRemarksChecked = 0;
        var IsCameraChecked = 0;

        if ($("#chkRemarks").is(':checked')) {
            IsRemarksChecked = 1;
        }

        if ($("#chkCamera").is(':checked')) {
            IsCameraChecked = 1;
        }


        var dataObj = {};
        dataObj.activity = strName;
        dataObj.description = taDesc;
        dataObj.duration = Number(txtDuration);
        dataObj.activityTypeId = Number(txtID.value());
        dataObj.componentId = Number(cmbType.value());
        dataObj.charge = Number(txtCharge);
        dataObj.displayOrder = $("#txtDisplayOrder").val();
        dataObj.remarksRequired = IsRemarksChecked;
        dataObj.camera = IsCameraChecked;

        dataObj.isActive = isActive;
        dataObj.createdBy = Number(sessionStorage.userId);//"101";//sessionStorage.uName;
        dataObj.isDeleted = 0;
        dataObj.thumbnailPresent = IsThumbnailPresent;

        if (operation == ADD) {
            var dataUrl = ipAddress + "/activity/create";
            createAjaxObject(dataUrl, dataObj, "POST", onCreate, onError);
        } else {
            dataObj.id = selItem.idk;
            var dataUrl = ipAddress + "/activity/update";
            createAjaxObject(dataUrl, dataObj, "POST", onCreate, onError);
        }
    }
}

function onCreate(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            if(isBrowseFlag) {
                onClickUploadPhoto();
            }
            else{
                onSuccess(dataObj);
            }
        }else{
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}

function onClickSearch(){
    var obj = {};
    obj.status = "search";
    popupClose(obj);
}

function onError(errObj){
    console.log(errObj);
    customAlert.error("Error","Unable to create Country");
}
function onClickCancel(){
    var obj = {};
    obj.status = "false";
    popupClose(obj);
}
function popupClose(obj){
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}

function onClickReset() {
    if(operation == ADD) {
        operation = ADD;
    }
    else {
        operation = UPDATE;
    }
    $("#txtActivityName").val("");
    // $("#txtID").val("");
    $("#txtDuration").val("0");
    $("#txtCharge").val("0");
    $("#taDesc").val("");
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if(cmbStatus){
        cmbStatus.select(0);
        cmbStatus.enable(false);
    }
    var cmbType = $("#cmbType").data("kendoComboBox");
    if(cmbType){
        cmbType.select(0);
    }
}

var actComponents;
function getStateList(dataObj){
    console.log(dataObj);
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.activity){
        if($.isArray(dataObj.response.activity)){
            dataArray = dataObj.response.activity;
        }else{
            dataArray.push(dataObj.response.activity);
        }

    }
    var tempDataArry = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].displayOrder1 = dataArray[i].displayOrder;
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
        }
        dataArray[i].activityID = getActivityNameById(dataArray[i].activityTypeId);
        dataArray[i].Type = getTypeNameById(dataArray[i].componentId);
    }
    actComponents = dataArray;
    //buildStateListGrid(dataArray);
}

function onSelectionFiles(event) {
    var imageCompressor = new ImageCompressor();
    console.log(event);
    files = event.target.files;
    fileName = "";
    //$('#txtFU').val("");
    if (files) {
        if (files.length > 0) {
            fileName = files[0].name;
            // if (patientId != "") {
            var oFReader = new FileReader();
            oFReader.readAsDataURL(files[0]);
            oFReader.onload = function(oFREvent) {
                document.getElementById("imgPhoto").src = oFREvent.target.result;
                if (imageCompressor) {
                    imageCompressor.run(oFREvent.target.result, compressorSettings, function(small) {
                        document.getElementById("imgPhoto").src = small;
                        //isBrowseFlag = false;
                        imagePhotoData = small;
                        return small;
                    });
                }

            }
            // }
        }
    }
}

function onClickBrowse(e) {
    removeSelectedButtons();
    $("#btnBrowse").addClass("selClass");
    isBrowseFlag = true;
    $("#imgPhoto").show();
    console.log(e);
    if (e.currentTarget && e.currentTarget.id) {
        var btnId = e.currentTarget.id;
        var fileTagId = ("fileElem" + btnId);
        IsThumbnailPresent = 1;
        $("#fileElem").click();
    }
}

function removeSelectedButtons() {
    $("#btnBrowse").removeClass("selClass");
}

function onClickBrowseOver() {
    removeOverSelection();
    $("#btnBrowse").addClass("borderClass");
}

function onClickBrowseOut() {
    removeOverSelection();
}

function removeOverSelection() {
    $("#btnBrowse").removeClass("borderClass");
}

function onClickUploadPhoto() {
    if (componentId != "") {
        removeSelectedButtons();
        $("#btnUpload").addClass("selClass");
        if (isBrowseFlag) {
            //imagePhotoData
            var reqUrl;

            reqUrl = ipAddress+"/homecare/upload/activities/thumbnail/";

            reqUrl = reqUrl+"?access_token="+sessionStorage.access_token + "&id="+componentId;

            // var reqUrl = ipAddress + "/upload/patient/photo/stream/?patient-id=" + patientId + "&photo-ext=png&access_token=" + sessionStorage.access_token + "&tenant=" + sessionStorage.tenant
            // var xmlhttp = new XMLHttpRequest();
            // xmlhttp.open("POST", reqUrl, true);
            // //xmlhttp.open("POST", "/ROOT/upload/patient/photo/?patient-id=101&photo-ext=png", true);
            // xmlhttp.send(imagePhotoData);
            // xmlhttp.onreadystatechange = function() { //Call a function when the state changes.
            //     if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            //         customAlert.error("Info", "Image uploaded successfully");
            //     }
            // }


            var tagFileId = ("fileElem");
            var file = document.getElementById(tagFileId);

            if(file && file.files[0] && file.files[0].name){
                fileName = file.files[0].name;
            }

            var form = new FormData();
            if(operation != UPDATE){
                form.append("file",file.files[0]);
            }else{
                form.append("file",file.files[0]);
            }


            // var settings = {
            //     "async": true,
            //     "crossDomain": true,
            //     "url": reqUrl,
            //     "method": "POST",
            //     "processData": false,
            //     "contentType": false,
            //     "data": form,
            //     "headers": {
            //         "tenant": sessionStorage.tenant,
            //         "contentType": "multipart/form-data",
            //         "cache-control": "no-cache",
            //     },
            // }
            //
            // $.ajax(settings).done(function (response) {
            //     console.log(response);
            // });

            Loader.showLoader();
            $.ajax({
                url: reqUrl,
                type: 'POST',
                data: form,
                processData: false,
                contentType:false,// "multipart/form-data",
                // contentType: "application/json",
                headers: {
                    tenant: sessionStorage.tenant
                },
                success: function(data, textStatus, jqXHR){
                    if(typeof data.error === 'undefined'){
                        Loader.hideLoader();
                        onSuccess(data);
                    }else{
                        Loader.hideLoader();
                    }
                },
                error: function(jqXHR, textStatus, errorThrown){
                    Loader.hideLoader();
                }
            });

        }
    }
}


function onSuccess(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            if(operation == ADD){
                customAlert.info("info", "Task Component Created Successfully");
            }else{
                customAlert.info("info", "Task Component Updated Successfully");
            }
            var obj = {};
            obj.status = "success";
            obj.operation = operation;
            popupClose(obj);
        }else{
            customAlert.error("Error", dataObj.response.status.message);
        }
    }else{

    }
    onClickReset();
}

function onShowImage() {
    if (selItem && selItem.thumbnailPresent == 1) {
        var imageServletUrl = ipAddress+"/homecare/download/activities/thumbnail/?access_token="+sessionStorage.access_token+"&id="+selItem.idk+"&tenant="+sessionStorage.tenant;
        // $("#imgPhoto").attr("src", imageServletUrl);
        document.getElementById("imgPhoto").src =imageServletUrl;
    } else {
        $("#imgPhoto").attr("src", "../../img/AppImg/HosImages/blank.png");
    }
}


var angularUIgridWrapper;
var parentRef = null;
var operation = "";
var ADD = "add";

$(document).ready(function(){
    $("#pnlPatient",parent.document).css("display","none");
    // themeAPIChange();
    parentRef = parent.frames['iframe'].window;
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("dgridTableList", dataOptions);
    angularUIgridWrapper.init();
    buildCodeTableListGrid([]);
});

$(window).load(function(){
    loading = false;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
    init();
    buttonEvents();
    adjustHeight();
}

function init(){
    searchOnLoad('active');
}
function onError(errorObj){
    console.log(errorObj);
}
function getCountryList(dataObj){
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.codeTableName){
        if($.isArray(dataObj.response.codeTableName)){
            dataArray = dataObj.response.codeTableName;
        }else{
            dataArray.push(dataObj.response.codeTableName);
        }
    }
    var tempDataArry = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
        }
        if(dataArray[i].tableName.toLowerCase().includes('patient')){
            dataArray[i].tableName = dataArray[i].tableName.toLowerCase().replace("patient","Serviceuser");
        }
        if(dataArray[i].codeName.toLowerCase().includes('patient')){
            dataArray[i].codeName = dataArray[i].codeName.toLowerCase().replace("patient","Serviceuser");
        }
    }
    buildCodeTableListGrid(dataArray);
}
function buttonEvents(){
    $("#btnSave").off("click",onClickSave);
    $("#btnSave").on("click",onClickSave);

    $("#btnCancel").off("click",onClickCancel);
    $("#btnCancel").on("click",onClickCancel);

    $("#btnDelete").off("click",onClickDelete);
    $("#btnDelete").on("click",onClickDelete);

    $("#btnAdd").off("click");
    $("#btnAdd").on("click",onClickAdd);

    $(".popupClose").off("click", onClickCancel);
    $(".popupClose").on("click", onClickCancel);

    $("#btnReset").off("click",onClickReset);
    $("#btnReset").on("click",onClickReset);

    $(".btnActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('active');
        onClickActive();
    });
    $(".btnInActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('inactive');
        onClickInActive();
    });
}

function adjustHeight(){
    var defHeight = 220;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    angularUIgridWrapper.adjustGridHeight(cmpHeight);
}

function buildCodeTableListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Table",
        "field": "tableName",
    });
    gridColumns.push({
        "title": "Code",
        "field": "codeValue",
    });
    gridColumns.push({
        "title": "Abbreviation",
        "field": "codeName",
    });
    // gridColumns.push({
    //    "title": "Status",
    //    "field": "Status",
    // });
    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}
var prevSelectedItem =[];
function onChange(){

}

function onClickOK(){
    setTimeout(function(){
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        if(selectedItems && selectedItems.length>0){
            var obj = {};
            obj.selItem = selectedItems[0];
            // var onCloseData = new Object();
            obj.status = "success";
            obj.operation = "ok";
            var windowWrapper = new kendoWindowWrapper();
            windowWrapper.closePageWindow(obj);
        }
    })
}
function onClickDelete(){
    customAlert.confirm("Confirm", "Are you sure you want delete?",function(response){
        if(response.button == "Yes"){
            var selectedItems = angularUIgridWrapper.getSelectedRows();
            if(selectedItems && selectedItems.length>0){
                var selItem = selectedItems[0];
                if(selItem){
                    var dataUrl = ipAddress+"/codetable/delete/";
                    var reqObj = {};
                    reqObj.id = selItem.idk;
                    //reqObj.abbr = selItem.abbr;
                    //reqObj.country = selItem.country;
                    reqObj.code = selItem.code;
                    reqObj.isDeleted = "1";
                    reqObj.modifiedBy = "101";
                    createAjaxObject(dataUrl,reqObj,"POST",onDeleteCodeTables,onError);
                    // getAjaxObject(ipAddress+"/codetable/delete/"+selItem.id,"GET",onDeleteCodeTables,onError);
                }
            }
        }
    });
}
function onDeleteCodeTables(dataObj){
    if(dataObj && dataObj.status){
        if(dataObj.status.code == "1"){
            customAlert.info("info", "Country Deleted Successfully");
            init();
        }else{
            customAlert.error("error", dataObj.message);
        }
    }

}
function onClickAdd(){
    $("#addPopup").show();
    $('#viewDivBlock').hide();
    $("#txtID").hide();
    operation = "add";
    $(".filter-heading").html("Add Code Table");
    onClickReset();
}
function closeAddCreateTableAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        var operation = returnData.operation;
        if(operation == "add"){
            customAlert.info("Info", "Table created successfully");
        }
        init();
    }
}

function onClickCancel(){
    $(".filter-heading").html("View Code Tables");
    $("#viewDivBlock").show();
    $("#addPopup").hide();
}

function searchOnLoad(status) {
    buildCodeTableListGrid([]);
    if(status == "active") {
        var urlExtn = ipAddress + "/codetable/list/?is-active=1&is-deleted=0";
    }
    else if(status == "inactive") {
        var urlExtn =  ipAddress + "/codetable/list/?is-active=0&is-deleted=1";
    }
    getAjaxObject(urlExtn,"GET",getCountryList,onError);
}

function onClickActive() {
    $(".btnInActive").removeClass("radioButton-active");
    $(".btnActive").addClass("radioButton-active");
}

function onClickInActive() {
    $(".btnActive").removeClass("radioButton-active");
    $(".btnInActive").addClass("radioButton-active");
}

function validation(){
    var flag = true;
    var strName = $("#txtName").val();
    strName = $.trim(strName);
    if(strName == ""){
        customAlert.error("Error","Enter Table Name");
        flag = false;
        return false;
    }
    var strCode = $("#txtCode").val();
    strCode = $.trim(strCode);
    /*if(strCode == ""){
     customAlert.error("Error","Enter Code");
     flag = false;
     return false;
     }*/
    return flag;
}


function onClickSave(){
    if(validation()){
        var strName = $("#txtName").val();
        strName = $.trim(strName);
        var strCode = $("#txtCode").val();
        strCode = $.trim(strCode);

        var strValue = $("#txtValue").val();
        strValue = $.trim(strValue);

        var isActive = parseInt($("#cmbStatus").val());

        var dataObj = {};
        dataObj.tableName = strName;
        dataObj.codeValue = strCode;
        dataObj.isActive = isActive;
        dataObj.createdBy = sessionStorage.userId;//"101";//sessionStorage.uName;
        if(isActive == 1){
            dataObj.isDeleted = "0";
        }
        else{
            dataObj.isDeleted = "1";
        }

        dataObj.codeName =  strValue;

        if(operation == ADD){
            var dataUrl = ipAddress+"/codetable/create";
            createAjaxObject(dataUrl,dataObj,"POST",onCreate,onError);
        }else{
            dataObj.id = selItem.idk;
            var dataUrl = ipAddress+"/country/update";
            createAjaxObject(dataUrl,dataObj,"POST",onCreate,onError);
        }
    }
}

function onCreate(dataObj){
    if(dataObj && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            var msg = "Table created successfully";
            displaySessionErrorPopUp("Info", msg, function(res) {
                operation = ADD;
                init();
                onClickCancel();
                onClickActive();
            })
        }else{
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}

function onClickReset(){
    $("#txtName").val("");
    $("#txtCode").val("");
    $("#cmbStatus").val(1);
}
function onError(errObj){
    customAlert.error("Error","Unable to create Country");
}

function themeAPIChange(){
    getAjaxObject(ipAddress+"/homecare/settings/?id=2","GET",getThemeValue,onError);
}


function getThemeValue(dataObj){
    var tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.activityTypes){
            if($.isArray(dataObj.response.settings)){
                tempCompType = dataObj.response.settings;
            }else{
                tempCompType.push(dataObj.response.settings);
            }
        }
        if(tempCompType.length > 0){
            themesChange(tempCompType[0].value);
        }
    }
}

function themesChange(themeValue){
    if(themeValue == 2){
        loadAPi("../../Theme2/Theme02.css");
    }
    else if(themeValue == 3){
        loadAPi("../../Theme3/Theme03.css");
    }
}
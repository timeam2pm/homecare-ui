var angularUIgridWrapper, angularPTUIgridWrapper;
var flatAppointments = [];
var allDatesInWeek = [];
var APIResponse = [];


$(document).ready(function () {

    var dataOptions = {};

    angularUIgridWrapper = new AngularUIGridWrapper("dgridUnAllocatedAppointmentsList", dataOptions);
    angularUIgridWrapper.init();
    buildAppointmentsListGrid([]);

    // angularPTUIgridWrapper = new AngularUIGridWrapper("dgridPatientsList", dataOptions);
    // angularPTUIgridWrapper.init();
    // buildPatientsListGrid([]);

    getAjaxObject(ipAddress+"/facility/list/?is-active=1&is-deleted=0","GET",getFacilityList,onError);

    $('#dgridUnAllocatedAppointmentsList').on('click', 'div.ui-grid-cell', function () {
        var index = $(this).index();

        if (index > 0) {
            var selectedTime = $(this).parent().find('div:eq(0)').text();
            var selectedDate = $('div.ui-grid-header-cell-row .ui-grid-header-cell:eq(' + index + ') span').text();

            showPatients(selectedTime, selectedDate);
        }

    })
    buttonEvents();
});

function buttonEvents() {
    $('#btnSubmit').on('click', function(e) {
        e.preventDefault();
        buildAppointmentsListGrid([]);
        onClickSubmit();
    });
}

function onClickSubmit() {
    if($("#txtDate").val() !== ""){

        flatAppointments=[];
        $("#divPatients").css("display", "none");
        $('#dgridUnAllocatedAppointmentsList').css('display', 'none');
        var selectedDate = GetDateTime("txtDate");

        allDatesInWeek = [];
        APIResponse = [];

        for (let i = 1; i <= 7; i++) {
            var first = selectedDate.getDate() - (selectedDate.getDay() + 1) + i;
            var day = new Date(selectedDate.setDate(first));

            allDatesInWeek.push(day);
        }
        var firstDayOfWeek = allDatesInWeek[0];
        var lastDayOfWeek = allDatesInWeek[allDatesInWeek.length - 1];

        // var selectedDate = GetDateTime("txtDate");
        var selDate = new Date(selectedDate);

        var day = selDate.getDate();
        var month = selDate.getMonth();
        month = month+1;
        var year = selDate.getFullYear();

        var stDate = month+"/"+day+"/"+year;
        stDate = stDate+" 00:00:00";

        var startDate = new Date(stDate);
        var stDateTime = startDate.getTime();

        var etDate = month+"/"+day+"/"+year;
        etDate = etDate+" 23:59:59";

        var endtDate = new Date(etDate);
        var edDateTime = endtDate.getTime();

        var startDate = new Date(stDate);
        var endtDate = new Date(etDate);

        var day = startDate.getDate()-startDate.getDay();
        var pDate = new Date(startDate);
        pDate.setDate(day);

        endtDate.setDate(day+6);
        var stDateTime = pDate.getTime();
        var edDateTime = endtDate.getTime();

        // alert(firstDayOfWeek);
        var patientListURL = ipAddress+"/appointment/list/?facility-id="+$("#cmbFacility").val()+"&provider-id=0&to-date="+edDateTime+"&from-date="+stDateTime+"&is-active=1";;

        getAjaxObject(patientListURL,"GET",onPatientAppListData,onErrorMedication);


        // var APIResponse = unallocatedAppointments;


    }
    else{
        alert("Select date");
    }
}

function GetDateTime(Id) {
    var dt = document.getElementById('' + Id + '').value;
    var dob = null;

    var dtArray = dt.split("/");
    dob = (dtArray[2] + "/" + dtArray[1] + "/" + dtArray[0]);

    var returnValue = new Date(dob);

    return returnValue;

}

function formatDate(date) {

    var dd = date.getDate();

    var mm = date.getMonth() + 1;
    var yyyy = date.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }

    if (mm < 10) {
        mm = '0' + mm;
    }

    return dd + '/' + mm + '/' + yyyy;
}

function dateAdd(date, interval, units) {
    var ret = new Date(date); //don't change original date
    var checkRollover = function () { if (ret.getDate() != date.getDate()) ret.setDate(0); };
    switch (interval.toLowerCase()) {
        case 'year': ret.setFullYear(ret.getFullYear() + units); checkRollover(); break;
        case 'quarter': ret.setMonth(ret.getMonth() + 3 * units); checkRollover(); break;
        case 'month': ret.setMonth(ret.getMonth() + units); checkRollover(); break;
        case 'week': ret.setDate(ret.getDate() + 7 * units); break;
        case 'day': ret.setDate(ret.getDate() + units); break;
        case 'hour': ret.setTime(ret.getTime() + units * 3600000); break;
        case 'minute': ret.setTime(ret.getTime() + units * 60000); break;
        case 'second': ret.setTime(ret.getTime() + units * 1000); break;
        default: ret = undefined; break;
    }
    return ret;
}

function buildAppointmentsListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;

    var columns = [];
    if (dataSource != null && dataSource.length > 0) {
        var firstObj = dataSource[0];

        for (var key in firstObj) {
            columns.push(key);
        }
    }

    if (columns != null && columns.length > 0) {
        for (var counter = 0; counter < columns.length; counter++) {
            var strGridColumn = '{';
            strGridColumn = strGridColumn + '"title":"' + columns[counter] + '","field":"' + columns[counter] + '"}';

            var jsonGridColumn = JSON.parse(strGridColumn);
            gridColumns.push(jsonGridColumn);

        }
    }




    angularUIgridWrapper.creategrid(dataSource, gridColumns, otoptions);
    adjustHeight();


    //onIndividualRowClick();
}

var staffId;

function buildPatientsListGrid(dataSource) {
    $('#ulPatientsList').empty();
    if (dataSource != null && dataSource.length > 0) {
        for (var counter = 0; counter < dataSource.length; counter++) {
            var id = "patient-" + counter;
            var $item = $('<li id="' + id + '" class="list-group-item"> <br> <img src="'+ipAddress+"/download/patient/photo/"+dataSource[counter].id+"/?"+Math.round(Math.random()*1000000)+"&access_token="+sessionStorage.access_token+"&tenant="+sessionStorage.tenant +'"> ' + dataSource[counter].id + ' - ' + dataSource[counter].firstName + ' ' + dataSource[counter].lastName + ", "+ kendo.toString(new Date(dataSource[counter].dateOfBirth),"dd/MM/yyyy") + ', Appointment Date: '+dataSource.dateOfAppointment+' </div> </li>');
            $item.appendTo('#ulPatientsList');
            $item.droppable({
                accept: '.ui-draggable',
                drop: function (e, ui) {

                    $(this).find('.staff').remove();
                    var $staffDiv = $('<div class="staff">' + $(ui.draggable).html() + '</div>');
                    var text = $(ui.draggable).text();
                    staffId = text.substr(0, text.indexOf('-')).trim();
                    $staffDiv.appendTo($(this));
                }
            });


        }

    } else {
        $('#ulPatientsList').empty();
    }




    // var gridColumns = [];
    // var otoptions = {};
    // otoptions.noUnselect = false;
    // otoptions.rowHeight = 100;
    // gridColumns.push({
    //     "title": "Image",
    //     "field": "id",
    //     "enableColumnMenu": false,
    //     "width": "20%",
    //     "cellTemplate":showPtImage()
    // });
    // gridColumns.push({
    //     "title": "Patient Details",
    //     "field": "firstName",
    //     "enableColumnMenu": false,
    //     "cellTemplate":showPtDetails()
    // });

    // angularPTUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    // adjustHeight();


    // angularPTUIgridWrapper.creategrid(dataSource, gridColumns, otoptions);
}

function buildProvidersListGrid(dataSource) {
    $('#ulStaffList').empty();
    if (dataSource != null && dataSource.length > 0) {
        for (var counter = 0; counter < dataSource.length; counter++) {
            var id = "provider-" + counter;
            var $item = $('<li id="' + id + '"class="list-group-item"> <br> <img class="imgStaff" src="'+ipAddress+"/homecare/download/providers/photo/?"+Math.round(Math.random()*1000000)+"&access_token="+sessionStorage.access_token+"&tenant="+sessionStorage.tenant+"&id="+dataSource[counter].id+'">' + '  ' + dataSource[counter].id + ' - ' + dataSource[counter].lastName + ' ' + dataSource[counter].firstName + '      ('+ SecondsTohhmmss(dataSource[counter].startTime)+ ' - '+ SecondsTohhmmss(dataSource[counter].endTime) +')'+'</li>');
            $item.draggable({
                helper: "clone"
            });
            $item.appendTo('#ulStaffList');
        }
    }
    else {
        $('#ulStaffList').empty();
    }
}

function adjustHeight() {
    var defHeight = 230;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    angularUIgridWrapper.adjustGridHeight(cmpHeight);
}
var appointid ;
function showPatients(time, date) {
    var patients = null;
    // alert(time+","+date);
    var selectedDate = date.trim().substring(4);

    flatAppointments.find(function (item, index) {
        if (item.time.trim() === time.trim() && item.date.trim() === date.trim()) {
            patients = item.patients;
            appointid = item.idk;
        }
    });

    if (patients !== null && patients.length > 0) {
        $("#dgridUnAllocatedAppointmentsList").css("display", "none");
        $("#divPatients").css("display", "block");
        $(".ibox").css("display", "none");


        buildPatientsListGrid(patients);

        selectedDate = selectedDate.split("/");


        selectedDate = (selectedDate[1] + "/" + selectedDate[0] + "/" + selectedDate[2]);

        var selDate = new Date(selectedDate);
        // var selTime;
        // if(time.trim().includes("AM")){
        //     selTime = time.substring(0,2);
        //     selTime = selTime*3600;
        // }
        // else{
        //     selTime = time.substring(0,2);
        //     selTime = (selTime*3600)+(43200);
        // }
        // var endTime = selTime+3599;



        var day = selDate.getDate();
        var month = selDate.getMonth();
        month = month+1;
        var year = selDate.getFullYear();

        var stDate = month+"/"+day+"/"+year;
        stDate = stDate+" 00:00:00";

        var startDate = new Date(stDate);
        var stDateTime = startDate.getTime();

        var etDate = month+"/"+day+"/"+year;
        etDate = etDate+" 23:59:59";

        // var endtDate = new Date(etDate);
        // var edDateTime = endtDate.getTime();

        var startDate = new Date(stDate);
        var endtDate = new Date(etDate);

        var day = startDate.getDate()-startDate.getDay();
        var pDate = new Date(startDate);
        pDate.setDate(day);

        endtDate.setDate(day+6);
        var stDateTime = pDate.getTime()/1000;
        var edDateTime = endtDate.getTime()/1000;


        var url = ipAddress+"/homecare/providers/filter-by/appointments/?start-time="+stDateTime+"&end-time="+edDateTime+"&type=201";
        getAjaxObject(url, "GET", onStaffListData, onErrorMedication);

        // buildProvidersListGrid(providerDetails);
    }
    else {
        alert('There are no appointments at this time.');
    }
}

function showPtDetails() {
    var node = '<div style="padding-top:5px"><spn class="pt">{{row.entity.id}}</spn><spn class="pt"> - {{row.entity.firstName}} &nbsp;&nbsp;{{row.entity.middleName}} &nbsp;&nbsp;{{row.entity.lastName}}</spn><br>';
    node = node + '<span class="avlClass">Available Now</span></div>';
    return node;
}
function showPtImage() {
    var node;

    node = '<div ng-show="((row.entity.dnr == \'1\'))"><img src="{{row.entity.photo}}"  onerror="onImgError(event)" style="width:90%;height:100%;border: 1px solid red;" gender="{{row.entity.GR}}"></img></div>';
    node = node + '<div ng-show="((row.entity.dnr == \'0\'))"><img src="{{row.entity.photo}}"  onerror="onImgError(event)" style="width:90%;height:100%;border: 1px solid;" gender="{{row.entity.GR}}"></img></div>';

    return node;
}

function onImgError(e) {
    //console.log(e);
}


function onClickSave() {
    var response = _.where(APIResponse, {id: appointid})
    var appObj = {};
    appObj = response[0];
    appObj.id = appointid;
    delete appObj.providerId;
    delete appObj.composition;
    delete appObj.createdDate;
    delete appObj.appointmentStartDate;
    delete appObj.appointmentEndDate;
    delete appObj.handoverNotes;
    delete appObj.inTime;
    delete appObj.isSynced;
    delete appObj.modifiedDate;
    delete appObj.notes;
    delete appObj.outTime;
    delete appObj.readBy;
    delete appObj.syncTime;
    delete appObj.timeSlots;
    appObj.modifiedBy = Number(sessionStorage.userId);
    appObj.providerId = parseInt(staffId);

    var dataUrl = ipAddress+"/appointment/update";
    var arr = [];
    arr.push(appObj);
    createAjaxObject(dataUrl,arr,"POST",onAvlAppCreate,onErrorMedication);
}

function onAvlAppCreate(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            var msg = "Appointment Updated Successfully";
            displaySessionErrorPopUp("Info", msg, function(res) {
                onClickCancel();

            })
        }else{
            customAlert.error("Error",dataObj.response.status.message);
        }
    }else{
        customAlert.error("Error",dataObj.response.status.message);
    }
}
var daysOfWeek = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
//
// function  onGetAppointments() {
//     var url = ipAddress+"/appointment/list/?facility-id=8&provider-id=0&to-date="+edDateTime+"&from-date="+stDateTime+"&is-active=1";//+"&is-active=1&is-deleted=0"
//     getAjaxObject(url, "GET", getIllnessData, onError);
//
// }


function getFacilityList(dataObj) {
    var dataArray = [];
    if (dataObj) {
        if ($.isArray(dataObj.response.facility)) {
            dataArray = dataObj.response.facility;
        } else {
            dataArray.push(dataObj.response.facility);
        }
    }
    var tempDataArry = [];
    for (var i = 0; i < dataArray.length; i++) {
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if (dataArray[i].isActive == 1) {
            dataArray[i].Status = "Active";
        }

        $("#cmbFacility").append('<option value="'+dataArray[i].idk+'">'+dataArray[i].name+'</option>');
    }
}

function onPatientAppListData(dataObj) {
    // console.log(dataObj);
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.appointment) {
            if ($.isArray(dataObj.response.appointment)) {
                APIResponse = dataObj.response.appointment;
            } else {
                APIResponse.push(dataObj.response.appointment);
            }
        }
    }

    if (APIResponse !== null && APIResponse.length > 0) {
        var appointments = APIResponse;
        var updatedAppointments = $.map(appointments, function (element, index) {
            var duration = parseInt(element.duration);
            var appointmentStartDate = new Date(parseInt(element.dateOfAppointment));
            var appointmentEndDate = dateAdd(appointmentStartDate, 'minute', duration);

            var startHour = appointmentStartDate.getHours(), endHour = appointmentEndDate.getHours();
            var timeSlots = [];
            for (var counter = startHour; counter <= endHour; counter++) {

                var hours = counter > 12 ? counter - 12 : counter;
                var am_pm = counter >= 12 ? "PM" : "AM";
                hours = hours < 10 ? "0" + hours : hours;
                time = hours + ":00 " + am_pm;

                timeSlots.push(time);
            }

            element.appointmentStartDate = appointmentStartDate;
            element.appointmentEndDate = appointmentEndDate;
            element.timeSlots = timeSlots;

            return element;
        });


        for (var counter = 0; counter < updatedAppointments.length; counter++) {
            var appointment = updatedAppointments[counter];

            for (var counter2 = 0; counter2 < appointment.timeSlots.length; counter2++) {
                var time = appointment.timeSlots[counter2];
                var date = daysOfWeek[appointment.appointmentStartDate.getDay()] + " " + formatDate(appointment.appointmentStartDate);
                var flatAppointmentObj = {};

                var index = -1;
                flatAppointments.find(function (item, i) {
                    if (item.time === time && item.date === date) {
                        index = i;
                    }
                });

                if (index > -1) {
                    flatAppointments[index]["patients"].push(appointment.composition.patient);
                }
                else {

                    flatAppointmentObj.time = time;
                    flatAppointmentObj.date = date;
                    flatAppointmentObj.idk = appointment.id;
                    flatAppointmentObj.patients = [];
                    flatAppointmentObj.patients.push(appointment.composition.patient);
                    flatAppointmentObj.patients.dateOfAppointment =GetDateEdit(appointment.dateOfAppointment);
                    flatAppointments.push(flatAppointmentObj);
                }
            }
        }


        var hoursArray = [];
        hoursArray = $.map(APIResponse, function (e, i) {
            return new Date(e.dateOfAppointment).getHours();
        });

        var minHour = Math.min(...hoursArray);
        var maxHour = Math.max(...hoursArray);

        var allTimeSlots = [];

        for (var counter3 = minHour; counter3 <= maxHour; counter3++) {

            var hours = counter3 > 12 ? counter3 - 12 : counter3;
            var am_pm = counter3 >= 12 ? "PM" : "AM";
            hours = hours < 10 ? "0" + hours : hours;
            time = hours + ":00 " + am_pm;

            allTimeSlots.push(time);
        }

        var allDaysInWeek = [];
        for (var counter4 = 0; counter4 < allDatesInWeek.length; counter4++) {
            allDaysInWeek.push(daysOfWeek[allDatesInWeek[counter4].getDay()] + " " + formatDate(allDatesInWeek[counter4]));
        }

        var dataSource = [];

        for (var counter5 = 0; counter5 < allTimeSlots.length; counter5++) {
            var timeSlot = allTimeSlots[counter5];
            var dataSourceObj = {};
            dataSourceObj.Time = timeSlot;

            for (var counter6 = 0; counter6 < allDaysInWeek.length; counter6++) {
                var tempDate = allDaysInWeek[counter6];
                var noOfAppointments = '';

                flatAppointments.find(function (item, index) {
                    if (item.time === timeSlot && item.date === tempDate) {
                        noOfAppointments = item.patients.length;
                    }
                });

                dataSourceObj[allDaysInWeek[counter6]] = noOfAppointments;
            }

            dataSource.push(dataSourceObj);
        }
        if (dataSource != null && dataSource.length > 0) {
            $('#dgridUnAllocatedAppointmentsList').css('display', 'block');
            buildAppointmentsListGrid(dataSource);
        }

    }
    else{
        customAlert.info("Info","No appointments found in this week")
    }

}

function onErrorMedication(errobj){
    //console.log(errobj);
}

function onStaffListData(dataObj) {
    // console.log(dataObj);
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.providers) {
            if ($.isArray(dataObj.response.providers)) {
                dataArray = dataObj.response.providers;
            } else {
                dataArray.push(dataObj.response.providers);
            }
        }
    }
    buildProvidersListGrid(dataArray);
}

function onClickCancel(){
    $(".ibox").css("display","block");
    $("#dgridUnAllocatedAppointmentsList").css("display", "none");
    $("#divPatients").css("display", "none");
    buildAppointmentsListGrid([]);
    $('#ulStaffList').empty();
    $('#ulPatientsList').empty();
    $("#txtDate").val("");
}

var SecondsTohhmmss = function(totalSeconds) {
    var hours   = Math.floor(totalSeconds / 3600);
    var minutes = Math.floor((totalSeconds - (hours * 3600)) / 60);
    var seconds = totalSeconds - (hours * 3600) - (minutes * 60);

    // round seconds
    seconds = Math.round(seconds * 100) / 100

    var am_pm;
    var result = (hours < 10 ? "0" + hours : hours);
    am_pm = result >= 12 ? "PM" : "AM";
    result = result > 12 ? result - 12 : result;
    result += ":" + (minutes < 10 ? "0" + minutes : minutes);
    result += ":" + (seconds  < 10 ? "0" + seconds : seconds);
    result += " " + am_pm;
    return result;
}

var parentRef = null;
var operation = "";
var selItem = null;
var ADD = "add";
var UPDATE = "edit";
var VIEW = "view";
var DELETE = "delete";
var patientId = "";
var rs = "";
var selRSItem = null;

var towArr = [{Key:'0',Value:'Vacation1'},{Key:'1',Value:'Vacation2'},{Key:'2',Value:'Vacation3'},{Key:'3',Value:'Vacation4'},{Key:'4',Value:'Vacation5'},{Key:'5',Value:'Vacation6'},{Key:'6',Value:'Vacation7'}];

var photoExt = "";
var pid = "";
var pname = "";

var fid = "";
var fname = "";


$(document).ready(function() {
    parentRef = parent.frames['iframe'].window;
    var pnlHeight = window.innerHeight;
});
 
$(window).load(function(){
	parentRef = parent.frames['iframe'].window;
	rs = parentRef.rs;
	
	pid = parentRef.pid;
	pname = parentRef.pname;
	
	fid = parentRef.fid;
	fname = parentRef.fname;
	
	selRSItem = parentRef.dtItem;
	
	init();
	buttonEvents();
});

function buttonEvents(){
	$("#lblAvl").off("click");
	$("#lblAvl").on("click",onClickAvl);
	
	$("#btnSave").off("click");
	$("#btnSave").on("click",onClickOK);
	
	$("#btnCancelDet").off("click");
	$("#btnCancelDet").on("click",onClickCancel);
}
var cntry = "";
var dtFMT = "dd/MM/yyyy";
function init(){
	cntry = sessionStorage.countryName;
	 if(cntry.indexOf("India")>=0){
		 dtFMT = INDIA_DATE_FMT;
	 }else  if(cntry.indexOf("United Kingdom")>=0){
		 dtFMT = ENG_DATE_FMT;
	 }else  if(cntry.indexOf("United State")>=0){
		 dtFMT = US_DATE_FMT;
	 }
	//$("#txtRPatient").kendoComboBox();
	//$("#txtRFecility").kendoComboBox();
	$("#txtStartTime").kendoDateTimePicker({change: startChange,interval: 15});
	$("#txtEndTime").kendoDateTimePicker({interval: 15});
	$("#txtVType").kendoComboBox();
	setDataForSelection(towArr, "txtVType", function(){getProviderAvlTimes()}, ["Value", "Key"], 0, "");
	
	var sDate =  $("#txtStartTime").data("kendoDateTimePicker");
	var eDate = $("#txtEndTime").data("kendoDateTimePicker");
	
	var sDateValue = new Date();
	sDateValue.setHours(0);
	sDateValue.setMinutes(0);
	sDateValue.setSeconds(0);
	
	var eDateValue = new Date();
	eDateValue.setHours(23);
	eDateValue.setMinutes(59);
	eDateValue.setSeconds(59);
	
	sDate.value(sDateValue);
	sDate.min(sDateValue);
	eDate.value(eDateValue);
	eDate.min(eDateValue);
	
	if(selRSItem){
		getComboListIndex("txtVType","value",selRSItem.dayOfWeek);
		var txtStartTime = $("#txtStartTime").data("kendoTimePicker");
		txtStartTime.value(selRSItem.startTimeOfDay);
		var txtEndTime = $("#txtEndTime").data("kendoTimePicker");
		txtEndTime.value(selRSItem.endTimeOfDay);
	}
	onChangeProvider();
	//getAjaxObject(ipAddress+"/master/appointment_type/list/?is-active=1","GET",getAppointmentList,onError);
}
function getProviderAvlTimes(){
	
}
function allowNumerics(ctrlId){
	$("#"+ctrlId).keypress(function (e) {
			 if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
					return false;
				}
   });
}
function onChangeProvider(){
	var prId = parentRef.pid;
	var API_URL = ipAddress + '/homecare/availabilities/';
	API_URL = API_URL + '?parent-id='+prId+'&parent-type-id=201&is-active=1&is-deleted=0';
	//getAjaxObject(API_URL,"GET",onProviderAvailableData,onError);
}
var providerAvailableArray = [];
function onProviderAvailableData(dataObj){
	console.log(dataObj);
	providerAvailableArray = [];
	  if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1" && dataObj.response.availabilities){
	        if($.isArray(dataObj.response.availabilities)){
	        	providerAvailableArray = dataObj.response.availabilities;
	        }else{
	        	providerAvailableArray.push(dataObj.response.availabilities);
	        }
	    }
	  $("#divTable").html("");
	  var txtStartTime = $("#txtStartTime").data("kendoTimePicker");
	  var txtEndTime = $("#txtEndTime").data("kendoTimePicker");
	  if(rs != "edit"){
		  txtStartTime.value("");
		  txtStartTime.enable(false);
		  txtEndTime.value("");
		  txtEndTime.enable(false);
	  }else{
		  
	  }
	  
	  if(providerAvailableArray.length>=0){
		  getProviderAvlTimes();
	  }
}
function startChange(){
	var txtStartTime = $("#txtStartTime").data("kendoDateTimePicker");
	var txtEndTime = $("#txtEndTime").data("kendoDateTimePicker");
	var startTime = txtStartTime.value();
    if (startTime) {
        startTime = new Date(startTime);

        //txtEndTime.max(startTime);

        //startTime.setMinutes(startTime.getMinutes());

        txtEndTime.min(startTime);
        txtEndTime.max(today1);
        //end.value(startTime);
    }
}

var today1 = null;

function onError(errObj) {
    console.log(errObj);
    customAlert.error("Error", "Error");
}
function getWeekDayName(wk){
	var wn = "";
	if(wk == 1){
		return "Mon";
	}else if(wk == 2){
		return "Tuesday";
	}else if(wk == 2){
		return "Tuesday";
	}else if(wk == 3){
		return "Wednesday";
	}else if(wk == 4){
		return "Thursday";
	}else if(wk == 5){
		return "Friday";
	}else if(wk == 6){
		return "Saturday";
	}else if(wk == 0){
		return "Sunday";
	}
	return wn;
}

function onClickOK(){
	 var txtVType = $("#txtVType").data("kendoComboBox");
	var txtStartTime = $("#txtStartTime").data("kendoTimePicker");
	var txtEndTime = $("#txtEndTime").data("kendoTimePicker");
	if(txtVType.value() && txtStartTime.value() && txtEndTime.value()){
		var txtStartTimeIdx = txtStartTime.value();
		var txtEndTimeIdx = txtEndTime.value();
			var fd = new Date(txtStartTimeIdx);
			var fdk = fd.getHours()*60*60+fd.getMinutes()*60+fd.getSeconds();
			
			//var ed = fd;
			//var ed = ed.setMinutes(txtDuration);
			var ed = new Date(txtEndTimeIdx);//txtStartTimeIdx.getTime()+txtDuration*60*1000;
			//ed = new Date(ed);
			var edk = ed.getHours()*60*60+ed.getMinutes()*60+ed.getSeconds();
			if(fdk>=edk){
				customAlert.info("Error","End Time must be greater than Start Time");
				return;
			}
			//console.log(rObj);
			var dObj = {};
			dObj.dayOfWeek = txtVType.value()
			dObj.isDeleted = 0;//Number(objRosterEntiry.isDeleted);
			dObj.isActive = 1;//Number(objRosterEntiry.isActive);
			dObj.parentId = Number(parentRef.pid);//parentRef.patientId;
			dObj.createdBy = Number(sessionStorage.userId);
			dObj.startTime = fdk;//objRosterEntiry.startTime;
			dObj.endTime = edk;//objRosterEntiry.endTime;
			dObj.facilityId = Number( parentRef.fid);
			var me = "POST";
			if(selRSItem && selRSItem.idk){
				dObj.id  = selRSItem.idk;
				me = "PUT";
			}
		
			var dataUrl = ipAddress + "/homecare/availabilities/";
			//createAjaxObject(dataUrl, dObj, me, onPatientTMCreate, onError);
	}else{
		customAlert.info("Error","Enter Mandatory Fields");
	}
	
}

function onPatientTMCreate(dataObj){
	if(dataObj && dataObj.response.status){
		if(dataObj.response.status.code == "1"){
			customAlert.info("Info","Created Successfully");
			 setTimeout(function(){
					onClickCancel();  
				},1000);
		}else{
			customAlert.error("error", dataObj.message);
		}
	}
}
function onError(){
	
}
function onClickCancel() {
    var obj = {};
    obj.status = "false";
    popupClose(obj);
}

function onClickSearch() {
    var obj = {};
    obj.status = "search";
    popupClose(obj);
}

function popupClose(obj) {
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}
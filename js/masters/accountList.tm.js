var angularUIgridWrapper;
var parentRef = null;

var selItem = null;
var ADD = "add";
var UPDATE = "update";
var VIEW = "view";
var DELETE = "delete";
var patientId = "";
var communicationId = "";
var contactComunicationId = "";
var statusArr = [{ Key: 'Active', Value: 'Active' }, { Key: 'InActive', Value: 'InActive' }];
var filterArr = [{ Key: '', Value: 'All' }, { Key: 'id', Value: 'ID' }, { Key: 'abbr', Value: 'Abbreviation' }, { Key: 'name', Value: 'Name' }, { Key: 'display-name', Value: 'Display Name' }]

var patientInfoObject = null;
var commId = "";
var operation;
var showTab;
var groupId;
$(document).ready(function() {
    $("#pnlPatient",parent.document).css("display","none");
    // themeAPIChange();
    parentRef = parent.frames['iframe'].window;
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("dgridAccountList", dataOptions);
    angularUIgridWrapper.init();
    buildAccountListGrid([]);
    getCountryZoneName();
});

$(window).load(function() {
    loading = false;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded() {
    $("#btnEdit").prop("disabled", true);
    $("#btnDelete").prop("disabled", true);
    init();
    buttonEvents();
    adjustHeight();
    $('.btnActive').trigger('click');
}

function init() {
    //getAjaxObject(ipAddress + "/billing-account/list/", "GET", getAccountList, onError);
    allowAlphaNumeric('txtAbbreviation');
    allowAlphaNumeric('txtBankSort');
    allowAlphaNumeric('txtBankAccount');
    validateEmail('txtRemittanceEmail');
    allowSplAlphaNumeric('txtN');
    allowSplAlphaNumeric('txtDN');
    allowSpaceAlphaNumeric('txtAdd1');
    allowSpaceAlphaNumeric('txtAdd2');
    allowSpaceAlphaNumeric('AddtxtAdd1');
    allowSpaceAlphaNumeric('AddtxtAdd2');
    allowNumerics('txtWPhone');
    allowNumerics('txtExtension');
    allowNumerics('txtFax');
    allowNumerics('txtCell');
    allowNumerics('txtHPhone');
    allowNumerics('txtCell1');
    allowAlphaNumeric('txtExtID1');
    allowAlphaNumeric('txtExtID2');
    allowAlphaNumeric('txtNPI');
    allowAlphaNumeric('txtTI');
    allowAlphaNumeric('cmbFTIT');
    allowAlphaNumeric('txtFTI');
    allowAlphaNumeric('txtFTEIN');
    allowAlphaNumeric('txtSTI');
    allowAlphaNumeric('txtNOTT');
    $("#cmbFTIT").kendoComboBox();
    $("#cmbPrefix").kendoComboBox();
    // $("#cmbStatus").kendoComboBox();
    $("#cmbSuffix").kendoComboBox();
    // var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    // if (cmbStatus) {
    //     cmbStatus.enable(true);
    // }
    $("#cmbZip1").kendoComboBox();
    $("#cmbSMS").kendoComboBox();
    $("#AddcmbSMS").kendoComboBox();
    $('[data-toggle="tab"]').on('click', function(e) {
        e.preventDefault();
        $('.alert').remove();
    });
    getZip();
    setDataForSelection(filterArr, "cmbFilterByName", onFilterChange, ["Value", "Key"], 0, "");
}

function onError(errorObj) {
    console.log(errorObj);
}

function getAccountList(dataObj) {
    // console.log(dataObj);
    var dataArray = [];
    if (dataObj && dataObj.response.status && dataObj.response.status.code == "1") {
        if ($.isArray(dataObj.response.billingAccount)) {
            dataArray = dataObj.response.billingAccount;
        } else {
            dataArray.push(dataObj.response.billingAccount);
        }
    }
    var tempDataArry = [];
    for (var i = 0; i < dataArray.length; i++) {
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if (dataArray[i].isActive == 1) {
            dataArray[i].Status = "Active";
            var createdDate = new Date(dataArray[i].createdDate);
            var createdDateString = createdDate.toLocaleDateString();
            var modifiedDate = new Date(dataArray[i].modifiedDate);
            var modifiedDateString = modifiedDate.toLocaleDateString();
            dataArray[i].createdDateString = createdDateString;
            dataArray[i].modifiedDateString = modifiedDateString;
        }

    }
    if(dataArray.length == 0 && dataObj.response.status.code == "0") {
        // $('.customAlert').append('<div class="alert alert-danger">'+dataObj.response.status.message+'</div>');
        customAlert.error("Error",dataObj.response.status.message);
    }
    buildAccountListGrid(dataArray);
}

function buttonEvents() {
    $("#btnEdit").off("click", onClickOK);
    $("#btnEdit").on("click", onClickOK);

    $("#btnCancel").off("click",onClickCancel);
    $("#btnCancel").on("click",onClickCancel);

    $("#btnDelete").off("click", onClickDelete);
    $("#btnDelete").on("click", onClickDelete);

    $("#btnAdd").off("click");
    $("#btnAdd").on("click", onClickAdd);

    $("#btnReset").off("click", onClickReset);
    $("#btnReset").on("click", onClickReset);

    $("#btnSave").off("click", onClickSave);
    $("#btnSave").on("click", onClickSave);

    $("#btnSearch").off("click", onClickSearch);
    $("#btnSearch").on("click", onClickSearch);

    $("#btnCitySearch").off("click");
    $("#btnCitySearch").on("click", onClickZipSearch);

    $("#AddbtnZipSearch").off("click");
    $("#AddbtnZipSearch").on("click", onClickAdditionalZipSearch);

    $("#accountFilter-btn").off("click");
    $("#accountFilter-btn").on("click", onClickFilterBtn);

    $(".popupClose").off("click", onClickCancel);
    $(".popupClose").on("click", onClickCancel);

    $('#tabsUL .boxtablink').on('click', function() {
        $(this).closest('li').addClass('active').siblings('li').removeClass('active');
        // $('.tab-content').hide();

        showTab = $(this).attr('data-toggle');
        // $('#' + showTab).show();
        $('.alert').remove();
        $('.boxtablink-active').removeClass("boxtablink-active");
        if (showTab == "Detailstab") {
            $("#tab1").addClass("tabContent-active");
            $("#tab2").removeClass("tabContent-active");
        }
        else {
            $("#tab2").addClass("tabContent-active");
            $("#tab1").removeClass("tabContent-active");
            // $(this).attr('data-toggle').removeClass("boxtablink-active");
        }
    });


    $(".btnActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('active');
        onClickActive();
    });
    $(".btnInActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('inactive');
        onClickInActive();
    });

    $("#txtWPhone").on('keyup', function(e) {
        if($(this).val().length > 0) {
            $("#txtExtension").removeAttr("disabled");
        }
        else {
            $("#txtExtension").attr("disabled","disabled");
        }
    });
    $("#cmbFilterByValue").on('keyup', function(e) {
        if($(this).val().length > 0) {
            $("#accountFilter-btn").removeAttr("disabled");
        }
        else {
            $("#accountFilter-btn").attr("disabled","disabled");
        }
    });
    $("#txtCell").on('keyup', function(e) {
        if($(this).val().length > 0) {
            $("#cmbSMS").closest('.k-dropdown-wrap').removeClass('k-state-disabled').find('.k-input').removeAttr("disabled");
        }
        else {
            $("#cmbSMS").closest('.k-dropdown-wrap').addClass('k-state-disabled').find('.k-input').attr("disabled","disabled");
        }
    });
    $("#txtCell1").on('keyup', function(e) {
        if($(this).val().length > 0) {
            $("#AddcmbSMS").closest('.k-dropdown-wrap').removeClass('k-state-disabled').find('.k-input').removeAttr("disabled");
        }
        else {
            $("#AddcmbSMS").closest('.k-dropdown-wrap').addClass('k-state-disabled').find('.k-input').attr("disabled","disabled");
        }
    });
}
function searchOnLoad(status) {
    buildAccountListGrid([]);
    if(status == "active") {
        var urlExtn = '/billing-account/list?is-active=1&is-deleted=0';
    }
    else if(status == "inactive") {
        var urlExtn = '/billing-account/list?is-active=0&is-deleted=1';
    }
    getAjaxObject(ipAddress+urlExtn,"GET",getAccountList,onError);
}

function getCountryZoneName() {
    var countryName = sessionStorage.countryName;
    if (countryName.indexOf("India") >= 0) {
        $('#postalCode').html('Postal Code :<span class="mandatory"></span>');
        $('.postalCodeAdditional').html('Postal Code');
        $('.stateLabel').text("State");
        $('.zipFourWrapper').addClass('hideZipFourWrapper');
    } else if (countryName.indexOf("United Kingdom")) {
        $('#postalCode').html('Postal Code');
        $('.postalCodeAdditional').html('Postal Code');
        $('.stateLabel').text("County");
        $('.zipFourWrapper').addClass('hideZipFourWrapper');
        $("#cmbZip").removeClass('input-search');
        $("#lblCity").html('City <span class="mandatory"></span>')
    } else {
        $('#postalCode').html('Zip :<span class="mandatory"></span>');
        $('.postalCodeAdditional').html('Zip');
        $('.stateLabel').text("State");
        $('.zipFourWrapper').removeClass('hideZipFourWrapper');
    }
}

function adjustHeight() {
    if($(window).width() > 767) {
        var defHeight = 240; //+window.top.getAvailPageHeight();
        if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
            defHeight = 80;
        }
        var cmpHeight = window.innerHeight - defHeight;
        cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
        angularUIgridWrapper.adjustGridHeight(cmpHeight);
    }
}
function onClickActive() {
    $(".btnInActive").removeClass("radioButton-active");
    $(".btnActive").addClass("radioButton-active");
}
function onClickInActive() {
    $(".btnActive").removeClass("radioButton-active");
    $(".btnInActive").addClass("radioButton-active");
}
function onClickFilterBtn(e) {
    e.preventDefault();
    $('.alert').remove();
    var filterName = $("#cmbFilterByName").val();
    var filterValue = $("#cmbFilterByValue").val();
    buildAccountListGrid([]);
    /*if($(".btnActive").hasClass("selectButtonBarClass")) {
        var urlExtn = '/billing-account/list?is-active=1&is-deleted=0';
    }
    else {
        var urlExtn = '/billing-account/list?is-active=0&is-deleted=1';
    }*/
    getAjaxObject(ipAddress+"/billing-account/list?is-active=1&is-deleted=0&"+filterName+"="+filterValue,"GET",getAccountList,onError);
}

function buildAccountListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "ID",
        "field": "idk",
        "width":"20%"
    });
    gridColumns.push({
        "title": "Abbreviation",
        "field": "abbreviation"
    });
    // gridColumns.push({
    //     "title": "Display Name",
    //     "field": "displayName"
    // });
    gridColumns.push({
        "title": "Name",
        "field": "name"
    });
    // gridColumns.push({
    //     "title": "External ID1",
    //     "field": "externalId1"
    // });
    // gridColumns.push({
    //     "title": "External ID2",
    //     "field": "externalId2"
    // });
    /*if($(window).width() > 767) {
	    gridColumns.push({
	        "title": "External ID1",
	        "field": "externalId1"
	    });
	    gridColumns.push({
	        "title": "External ID2",
	        "field": "externalId2"
	    });
	    gridColumns.push({
	        "title": "Created By",
	        "field": "createdBy",
		});
		gridColumns.push({
	        "title": "Created Date",
	        "field": "createdDateString",
		});
		gridColumns.push({
	        "title": "Modified By",
	        "field": "modifiedBy",
		});
		gridColumns.push({
	        "title": "Modified Date",
	        "field": "modifiedDateString",
		});
	}*/
    angularUIgridWrapper.creategrid(dataSource, gridColumns, otoptions);
    adjustHeight();
    //onIndividualRowClick();
}
var prevSelectedItem = [];

function onChange() {
    setTimeout(function() {
        var selectedItems = angularUIgridWrapper.getSelectedRows();

        if (selectedItems && selectedItems.length > 0) {
            $("#btnEdit").prop("disabled", false);
            $("#btnDelete").prop("disabled", false);
        } else {
            $("#btnEdit").prop("disabled", true);
            $("#btnDelete").prop("disabled", true);
        }
    });

}
var selAccountItem = null;

function onClickOK() {
    setTimeout(function() {
        $("#txtID").show();
        parentRef.operation = "edit";
        $("#viewDivBlock").hide();
        $("#addTaskGroupAccountPopup").show();
        var selectedItems = angularUIgridWrapper.getSelectedRows();

        if (selectedItems && selectedItems.length > 0) {
            var obj = {};
            obj.selItem = selectedItems[0];
            selAccountItem = selectedItems[0];
            addAccount("update");
        }
    })
}
function onClickDelete() {
    customAlert.confirm("Confirm", "Are you sure you want delete?", function(response) {
        if (response.button == "Yes") {
            var selectedItems = angularUIgridWrapper.getSelectedRows();

            if (selectedItems && selectedItems.length > 0) {
                var selItem = selectedItems[0];
                if (selItem) {
                    var dataUrl = ipAddress + "/billing-account/delete";
                    var reqObj = {};
                    reqObj.id = selItem.idk;
                    reqObj.isDeleted = "1";
                    reqObj.isActive = "0";
                    reqObj.modifiedBy = sessionStorage.userId;
                    createAjaxObject(dataUrl, reqObj, "POST", onDeleteCountryt, onError);
                }
            }
        }
    });
}

function onDeleteCountryt(dataObj) {

    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            customAlert.info("info", "Group Account Deleted Successfully");
            /*getAjaxObject(ipAddress + "/billing-account/list/", "GET", getAccountList, onError);*/
            buildAccountListGrid([]);
            init();
            getAjaxObject(ipAddress + "/billing-account/list/", "GET", getAccountList, onError);
        } else {
            customAlert.error("error", dataObj.message);
        }
    }
}

function onClickAdd() {
    $("#addTaskGroupAccountPopup").show();
    $('#viewDivBlock').hide();
    $("#txtID").hide()
    onClickReset();
    addAccount("add");
}

function addAccount(opr) {
    var popW = 850;
    var popH = 600;

    var profileLbl;
    // var operation = "ADD";
    //var devModelWindowWrapper = new kendoWindowWrapper();
    $('.listDataWrapper').hide();
    $('.addOrRemoveWrapper').show();
    $('.accountListSidebar').find('li').each(function() {
        $(this).removeClass('active');
    });
    $('.accountListSidebar').find('li:first-child a').trigger('click');
    $('.alert').remove();
    parentRef.operation = opr;
    operation = opr;
    if (opr == "add") {
        $('#btnSave').html('Save');
        $('.tabContentTitle').text('Add Group Account');
        $('#btnReset').show();
        $('#btnReset').trigger('click');
        /*profileLbl = "Add Billing Account";
        parentRef.selItem = null;*/
    } else {
        /*profileLbl = "Edit Billing Account";
        parentRef.selItem = selAccountItem;*/
        $('#btnReset').trigger('click');
        $('.tabContentTitle').text('Edit Group Account');
        $('#btnReset').hide();
        // console.log(selAccountItem);
        var communicationLen = selAccountItem.communications != null ? selAccountItem.communications.length : 0;
        var cityIndexVal = "";
        var dupCityIndexVal = "";
        for(var i=0; i<communicationLen; i++) {
            if(selAccountItem.communications[i].parentTypeId == 300) {
                cityIndexVal = i;
            }
            if(selAccountItem.communications[i].parentTypeId == 301) {
                dupCityIndexVal = i;
            }
        }
        if(cityIndexVal != "" || (cityIndexVal == 0 && selAccountItem.communications != null)) {
            $("#cityIdHiddenValue").val(selAccountItem.communications[cityIndexVal].cityId);
            $("#txtCountry").val(selAccountItem.communications[cityIndexVal].country);
            $("#cmbZip").val(selAccountItem.communications[cityIndexVal].houseNumber);
            $("#txtZip4").val(selAccountItem.communications[cityIndexVal].zipFour);
            $("#txtState").val(selAccountItem.communications[cityIndexVal].state);
            $("#txtCity").val(selAccountItem.communications[cityIndexVal].city);
            $("#txtWPhone").val(selAccountItem.communications[cityIndexVal].workPhone);
            $("#txtExtension").val(selAccountItem.communications[cityIndexVal].workPhoneExt);
            $("#txtFax").val(selAccountItem.communications[cityIndexVal].fax);
            $("#txtCell").val(selAccountItem.communications[cityIndexVal].cellPhone);
            $("#cmbSMS").val(selAccountItem.communications[cityIndexVal].sms);
            $("#txtEmail").val(selAccountItem.communications[cityIndexVal].email);
            $("#txtAdd1").val(selAccountItem.communications[cityIndexVal].address1);
            $("#txtAdd2").val(selAccountItem.communications[cityIndexVal].address2);
            getComboListIndex("cmbSMS", "desc", selAccountItem.communications[cityIndexVal].sms);
            zipSelItem = selAccountItem.communications[cityIndexVal];
            zipSelItem.idk = selAccountItem.communications[cityIndexVal].id;
            //getAjaxObject(ipAddress+"/city/list/?id="+selAccountItem.communications[cityIndexVal].cityId,"GET",onCityList,onError);
        }
        if(dupCityIndexVal != "") {
            $("#AddcityIdHiddenValue").val(selAccountItem.communications[dupCityIndexVal].cityId);
            $("#AddtxtCountry").val(selAccountItem.communications[dupCityIndexVal].country);
            $("#AddcmbZip").val(selAccountItem.communications[dupCityIndexVal].houseNumber);
            $("#AddtxtZip4").val(selAccountItem.communications[dupCityIndexVal].zipFour);
            $("#AddtxtState").val(selAccountItem.communications[dupCityIndexVal].state);
            $("#AddtxtCity").val(selAccountItem.communications[dupCityIndexVal].city);
            AddzipSelItem = selAccountItem.communications[dupCityIndexVal];
            AddzipSelItem.idk = selAccountItem.communications[dupCityIndexVal].id;
            if(selAccountItem.communications[dupCityIndexVal].homePhoneExt) {
                $("#txtExtension1").val(selAccountItem.communications[dupCityIndexVal].homePhoneExt);
            }
            if(selAccountItem.communications[dupCityIndexVal].homePhone) {
                $("#txtHPhone").val(selAccountItem.communications[dupCityIndexVal].homePhone);
            }
            if(selAccountItem.communications[dupCityIndexVal].address1) {
                $("#AddtxtAdd1").val(selAccountItem.communications[dupCityIndexVal].address1);
            }
            if(selAccountItem.communications[dupCityIndexVal].address2) {
                $("#AddtxtAdd2").val(selAccountItem.communications[dupCityIndexVal].address2);
            }
            if(selAccountItem.communications[dupCityIndexVal].cellPhone) {
                $("#txtCell1").val(selAccountItem.communications[dupCityIndexVal].cellPhone);
            }
            if(selAccountItem.communications[dupCityIndexVal].sms) {
                getComboListIndex("AddcmbSMS", "desc", selAccountItem.communications[dupCityIndexVal].sms);
            }
            if(selAccountItem.communications[dupCityIndexVal].email) {
                $("#txtEmail1").val(selAccountItem.communications[dupCityIndexVal].email);
            }
            //getAjaxObject(ipAddress+"/city/list/?id="+selAccountItem.communications[dupCityIndexVal].cityId,"GET",onAdditionalCityList,onError);
        }

        groupId =selAccountItem.idk;
        $("#txtID").html("ID :"+selAccountItem.idk);
        $("#txtExtID1").val(selAccountItem.externalId1);
        $("#txtExtID2").val(selAccountItem.externalId2);
        getComboListIndex("cmbPrefix", "value", selAccountItem.contactPrefix);
        getComboListIndex("cmbSuffix", "value", selAccountItem.contactSuffix);
        $("#txtFN").val(selAccountItem.contactFirstName);
        $("#txtLN").val(selAccountItem.contactLastName);
        $("#txtMN").val(selAccountItem.contactMiddleName);
        //$("#txtWeight").val(dataObj.response.patient.weight);
        //$("#txtHeight").val(dataObj.response.patient.height);
        $("#txtNN").val(selAccountItem.contactNickName);
        $("#txtAbbreviation").val(selAccountItem.abbreviation);
        $("#txtN").val(selAccountItem.name);
        $("#txtDN").val(selAccountItem.displayName);
        $("#txtFTI").val(selAccountItem.federalTaxId);
        $("#txtFTEIN").val(selAccountItem.federalTaxEin);
        $("#txtSTI").val(selAccountItem.stateTaxId);
        $("#txtNOTT").val(selAccountItem.taxName);
        $("#txtNPI").val(selAccountItem.npi);
        $("#txtTI").val(selAccountItem.taxonomyId);
        getComboListIndex("cmbFTIT", "desc", selAccountItem.federalTaxIdType);
        // getComboListIndex("cmbStatus", "Value", selAccountItem.Status);
        $("#cmbStatus").val(selAccountItem.isActive);
        if(selAccountItem.bankSortCode != null)
            $("#txtBankSort").val(selAccountItem.bankSortCode);
        if(selAccountItem.bankAccountNumber != null)
            $("#txtBankAccount").val(selAccountItem.bankAccountNumber);
        if(selAccountItem.remittanceEmail != null)
            $("#txtRemittanceEmail").val(selAccountItem.remittanceEmail);

        /*getComboListIndex("cmbPrefix", "desc", selAccountItem.contactPrefix);*/

        //	$("#txtSMS").val(comObj.sms);
        /*getComboListIndex("cmbSMS", "desc", selAccountItem.sms);*/
        /*$("#txtCountry").val(selAccountItem.country);
        $("#cmbZip").val(selAccountItem.zip);
        $("#txtZip4").val(selAccountItem.zipFour);
        $("#txtState").val(selAccountItem.state);
        $("#txtCity").val(selAccountItem.city);*/
    }

    //devModelWindowWrapper.openPageWindow("../../html/masters/createAccount.html", profileLbl, popW, popH, true, closeAddAccountAction);
}


function closeAddAccountAction(evt, returnData) {
    if (returnData && returnData.status == "success") {
        var operation = returnData.operation;
        if (operation == "add") {
            customAlert.info("info", "Group Account Created Successfully");
        } else {
            customAlert.info("info", "Group Account  Updated Successfully");
        }
        init();
    }
}

function onClickCancel(){
    $("#viewDivBlock").show();
    $("#addTaskGroupAccountPopup").hide();
}

function onClickClose() {
    $('.listDataWrapper').show();
    $('.addOrRemoveWrapper').hide();
    $("#btnEdit").prop("disabled", true);
    $("#btnDelete").prop("disabled", true);
    /*buildAccountListGrid([]);
    getAjaxObject(ipAddress+"/billing-account/list?is-active=1&is-deleted=0","GET",getAccountList,onError);*/
    $('.selectButtonBarClass').trigger('click');
}


function onClickZipSearch() {
    var popW = 600;
    var popH = 500;

    parentRef.searchZip = true;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search Zip";
    var cntry = sessionStorage.countryName;
    if(cntry.indexOf("India")>=0){
        profileLbl = "Search Postal Code";
    }else if(cntry.indexOf("United Kingdom")>=0){
        profileLbl = "Search Postal Code";
    }else if(cntry.indexOf("United State")>=0){
        profileLbl = "Search Zip";
    }else{
        profileLbl = "Search Zip";
    }
    devModelWindowWrapper.openPageWindow("../../html/masters/zipList.html", profileLbl, popW, popH, true, onCloseSearchZipAction);
}
function onClickAdditionalZipSearch() {
    var popW = 600;
    var popH = 500;

    parentRef.searchZip = true;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search Zip";
    devModelWindowWrapper.openPageWindow("../../html/masters/zipList.html", profileLbl, popW, popH, true, onCloseSearchAdditionalZipAction);
}
var zipSelItem = null;
var AddzipSelItem = null;

function onCloseSearchZipAction(evt, returnData) {

    if (returnData && returnData.status == "success") {
        //console.log();
        $("#cmbZip").val("");
        $("#txtZip4").val("");
        $("#txtState").val("");
        $("#txtCountry").val("");
        $("#txtCity").val("");
        var selItem = returnData.selItem;
        if (selItem) {
            zipSelItem = selItem;
            if (zipSelItem) {
                cityId = zipSelItem.idk;
                // if (zipSelItem.zip) {
                //     $("#cmbZip").val(zipSelItem.zip);
                // }
                if (zipSelItem.zipFour) {
                    $("#txtZip4").val(zipSelItem.zipFour);
                }
                if (zipSelItem.state) {
                    $("#txtState").val(zipSelItem.state);
                }
                if (zipSelItem.country) {
                    $("#txtCountry").val(zipSelItem.country);
                }
                if (zipSelItem.city) {
                    $("#txtCity").val(zipSelItem.city);
                }
            }
        }
    }
}
function onCloseSearchAdditionalZipAction(evt, returnData) {

    if (returnData && returnData.status == "success") {
        //console.log();
        $("#AddcmbZip").val("");
        $("#AddtxtZip4").val("");
        $("#AddtxtState").val("");
        $("#AddtxtCountry").val("");
        $("#AddtxtCity").val("");
        var selItem = returnData.selItem;
        if (selItem) {
            AddzipSelItem = selItem;
            if (AddzipSelItem) {
                cityId = AddzipSelItem.idk;
                // if (AddzipSelItem.zip) {
                //     $("#AddcmbZip").val(AddzipSelItem.zip);
                // }
                if (AddzipSelItem.zipFour) {
                    $("#AddtxtZip4").val(AddzipSelItem.zipFour);
                }
                if (AddzipSelItem.state) {
                    $("#AddtxtState").val(AddzipSelItem.state);
                }
                if (AddzipSelItem.country) {
                    $("#AddtxtCountry").val(AddzipSelItem.country);
                }
                if (AddzipSelItem.city) {
                    $("#AddtxtCity").val(AddzipSelItem.city);
                }
            }
        }
    }
}

function getZip() {
    getAjaxObject(ipAddress + "/city/list?is-active=1", "GET", getZipList, onError);
}

function getZipList(dataObj) {
    //var dArray = getTableListArray(dataObj);
    var dArray = [];
    if (dataObj && dataObj.response && dataObj.response.cityExt) {
        if ($.isArray(dataObj.response.cityExt)) {
            dArray = dataObj.response.cityExt;
        } else {
            dArray.push(dataObj.response.cityExt);
        }
    }
    if (dArray && dArray.length > 0) {
        /*setDataForSelection(dArray, "cmbZip", onZipChange, ["zip", "id"], 0, "");
        onZipChange();*/
    }
    getPrefix();
}

function getPrefix() {
    getAjaxObject(ipAddress + "/master/Prefix/list?is-active=1", "GET", getCodeTableValueList, onError);
}

function onComboChange(cmbId) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb && cmb.selectedIndex < 0) {
        cmb.select(0);
    }
}

function onPrefixChange() {
    onComboChange("cmbPrefix");
}
function onSuffixChange() {
    onComboChange("cmbSuffix");
}

function onPtChange() {
    onComboChange("cmbStatus");
}

function onGenderChange() {
    onComboChange("cmbGender");
}

function onSMSChange() {
    onComboChange("cmbSMS");
}
function onAddSMSChange() {
    onComboChange("AddcmbSMS");
}

function onLanChange() {
    onComboChange("cmbLan");
}

function onRaceChange() {
    onComboChange("cmbRace");
}

function onEthnicityChange() {
    onComboChange("cmbEthicity");
}

function onZipChange() {
    onComboChange("cmbZip");
    var cmbZip = $("#cmbZip").data("kendoComboBox");
    if (cmbZip) {
        var dataItem = cmbZip.dataItem();
        if (dataItem) {
            $("#txtZip4").val(dataItem.zipFour);
            $("#txtCountry").val(dataItem.country);
            $("#txtState").val(dataItem.state);
            $("#txtCity").val(dataItem.city);
        }
    }
}

function getCodeTableValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbPrefix", onPrefixChange, ["value", "idk"], 0, "");
    }
    getAjaxObject(ipAddress + "/master/Suffix/list?is-active=1", "GET", getCodeTableSuffixValueList, onError);
}
function getCodeTableSuffixValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbSuffix", onSuffixChange, ["value", "idk"], 0, "");
    }
    getAjaxObject(ipAddress+"/master/Status/list?is-active=1","GET",getPatientStatusValueList,onError);
}


function getPatientStatusValueList(dataObj) {
    var dArray = getTableFirstListArray(dataObj);
    if (dArray && dArray.length > 0) {
        // setDataForSelection(statusArr, "cmbStatus", onStatusChange, ["Value", "Key"], 0, "");
    }
    getAjaxObject(ipAddress + "/master/Gender/list?is-active=1", "GET", getGenderValueList, onError);
}

function getGenderValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    /*if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbGender", onGenderChange, ["desc", "idk"], 0, "");
    }*/
    getAjaxObject(ipAddress + "/master/SMS/list?is-active=1", "GET", getSMSValueList, onError);
}

function getSMSValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbSMS", onSMSChange, ["desc", "idk"], 0, "");
        setDataForSelection(dArray, "AddcmbSMS", onAddSMSChange, ["desc", "idk"], 0, "");
    }
    if (operation == UPDATE && selItem) {
        $("#btnReset").hide();
        getBillingAccountInfo();
        // var cmbStatus = $("#cmbStatus").data("kendoComboBox");
        // if (cmbStatus) {
        //     if (selItem.isActive == 1) {
        //         cmbStatus.select(0);
        //     } else {
        //         cmbStatus.select(1);
        //     }
        // }
        $("#cmbStatus").val(selItem.isActive);
    } else {
        // var cmbStatus = $("#cmbStatus").data("kendoComboBox");
        // if (cmbStatus) {
        //     cmbStatus.enable(false);
        // }
        $("#cmbStatus").attr("readonly", false);

    }
    //$("#btnSave").removeAttr('disabled',false);
    //$("#btnZipSearch").kendoWindowWrapper("close");



    //getAjaxObject(ipAddress+"/master/Language/list/","GET",getLanguageValueList,onError);
}

function getBillingAccountInfo() {
    getAjaxObject(ipAddress + "/billing-account/" + selItem.idk, "GET", onGetBillingAccountInfo, onError);
}

function getComboListIndex(cmbId, attr, attrVal) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb) {
        var ds = cmb.dataSource;
        var totalRec = ds.total();
        for (var i = 0; i < totalRec; i++) {
            var dtItem = ds.at(i);
            if (dtItem && dtItem[attr] == attrVal) {
                cmb.select(i);
                return i;
            }
        }
    }
    return -1;
}

function onGetBillingAccountInfo(dataObj) {
    //patientInfoObject = dataObj;
    if (dataObj && dataObj.response && dataObj.response.billingAccount) {
        communicationId = dataObj.response.billingAccount.communicationId;
        contactComunicationId = dataObj.response.billingAccount.contactComunicationId;
        $("#txtID").val(dataObj.response.billingAccount.id);
        $("#txtExtID1").val(dataObj.response.billingAccount.externalId1);
        $("#txtExtID2").val(dataObj.response.billingAccount.externalId2);
        getComboListIndex("cmbPrefix", "value", dataObj.response.billingAccount.prefix);
        $("#txtFN").val(dataObj.response.billingAccount.contactFirstName);
        $("#txtLN").val(dataObj.response.billingAccount.contactLastName);
        $("#txtMN").val(dataObj.response.billingAccount.contactMiddleName);
        //$("#txtWeight").val(dataObj.response.patient.weight);
        //$("#txtHeight").val(dataObj.response.patient.height);
        $("#txtNN").val(dataObj.response.billingAccount.contactNickName);
        $("#txtAbbreviation").val(dataObj.response.billingAccount.abbreviation);
        $("#txtN").val(dataObj.response.billingAccount.name);
        $("#txtDN").val(dataObj.response.billingAccount.displayName);
        $("#txtFTI").val(dataObj.response.billingAccount.federalTaxId);
        $("#txtFTEIN").val(dataObj.response.billingAccount.federalTaxEin);
        $("#txtSTI").val(dataObj.response.billingAccount.stateTaxId);
        $("#txtNOTT").val(dataObj.response.billingAccount.taxName);
        $("#txtNPI").val(dataObj.response.billingAccount.npi);
        $("#txtTI").val(dataObj.response.billingAccount.taxonomyId);
        $("#txtAdd1").val(dataObj.response.billingAccount.address1);
        $("#txtWPhone").val(dataObj.response.billingAccount.workPhone);
        $("#txtExtension").val(dataObj.response.billingAccount.workPhoneExt);
        $("#txtFax").val(dataObj.response.billingAccount.fax);
        $("#txtCell").val(dataObj.response.billingAccount.cellPhone);
        $("#txtAdd2").val(dataObj.response.billingAccount.address2);
        $("#txtEmail").val(dataObj.response.billingAccount.email);
        $("#txtEmail1").val(dataObj.response.billingAccount.email);
        /*var dt = new Date(dataObj.response.patient.dateOfBirth);
        if(dt){
        	var strDT = kendo.toString(dt,"MM/dd/yyyy");
        	var dtDOB = $("#dtDOB").data("kendoDatePicker");
        	if(dtDOB){
        		dtDOB.value(strDT);
        	}
        }*/
        //$("#txtSSN").val(dataObj.response.patient.ssn);

        getComboListIndex("cmbFTIT", "desc", dataObj.response.billingAccount.federalTaxIdType);
        // getComboListIndex("cmbStatus", "Value", dataObj.response.billingAccount.status);
        $("#cmbStatus").val(dataObj.response.billingAccount.status);

        getComboListIndex("cmbPrefix", "desc", dataObj.response.billingAccount.contactPrefix);

        //getComboListIndex("cmbGender","desc",dataObj.response.patient.gender);
        //getComboListIndex("cmbEthicity","desc",dataObj.response.patient.ethnicity);
        //getComboListIndex("cmbRace","desc",dataObj.response.patient.race);
        //getComboListIndex("cmbLan","desc",dataObj.response.patient.language);

        if (dataObj && dataObj.response && dataObj.response.communication) {

            var commArray = [];
            if ($.isArray(dataObj.response.communication)) {
                commArray = dataObj.response.communication;
            } else {
                commArray.push(dataObj.response.communication);
            }
            var comObj = commArray[0];
            commId = comObj.id;
            $("#txtAdd1").val(comObj.address1);
            $("#txtAdd2").val(comObj.address2);
            //	$("#txtSMS").val(comObj.sms);
            getComboListIndex("cmbSMS", "desc", comObj.sms);
            getComboListIndex("cmbZip", "idk", comObj.cityId);
            onZipChange();
            $("#txtCell").val(comObj.cellPhone);
            $("#txtExtension").val(comObj.workPhoneExt);
            $("#txtWPhone").val(comObj.workPhone);
            $("#txtExtension1").val(comObj.homePhoneExt);
            $("#txtHPhone").val(comObj.homePhone);
            $("#txtEmail").val(comObj.email);
            $("#txtCountry").val(comObj.country);
            $("#cmbZip").val(comObj.zip);
            $("#txtZip4").val(comObj.zipFour);
            $("#txtState").val(comObj.state);
            $("#txtCity").val(comObj.city);

            /*if(comObj.defaultCommunication == "1"){
            	$("#rdHome").prop("checked",true);
            }else if(comObj.defaultCommunication == "2"){
            	$("#rdWork").prop("checked",true);
            }else if(comObj.defaultCommunication == "3"){
            	$("#rdCell").prop("checked",true);
            }else if(comObj.defaultCommunication == "4"){
            	$("#rdEmail").prop("checked",true);
            }*/
        }

    }
}

function getTableListArray(dataObj) {
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.codeTable) {
        if ($.isArray(dataObj.response.codeTable)) {
            dataArray = dataObj.response.codeTable;
        } else {
            dataArray.push(dataObj.response.codeTable);
        }
    }
    var tempDataArry = [];
    var obj = {};
    obj.desc = "";
    obj.zip = "";
    obj.value = "";
    obj.idk = "";
    tempDataArry.push(obj);
    for (var i = 0; i < dataArray.length; i++) {
        if (dataArray[i].isActive == 1) {
            var obj = dataArray[i];
            obj.idk = dataArray[i].id;
            obj.status = dataArray[i].Status;
            tempDataArry.push(obj);
        }
    }
    return tempDataArry;
}
function getTableFirstListArray(dataObj) {
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.codeTable) {
        if ($.isArray(dataObj.response.codeTable)) {
            dataArray = dataObj.response.codeTable;
        } else {
            dataArray.push(dataObj.response.codeTable);
        }
    }
    var tempDataArry = [];
    for (var i = 0; i < dataArray.length; i++) {
        if (dataArray[i].isActive == 1) {
            var obj = dataArray[i];
            obj.idk = dataArray[i].id;
            obj.status = dataArray[i].Status;
            tempDataArry.push(obj);
        }
    }
    return tempDataArry;
}

function onClickReset() {
    if(operation == ADD) {
        operation = ADD;
    }
    else {
        operation = UPDATE;
    }
    //patientId = "";
    $("#txtID").val("");
    $("#txtExtID1").val("");
    $("#txtExtID2").val("");
    $("#txtAbbreviation").val("");
    $("#txtN").val("");
    $("#txtDN").val("");
    $("#txtFTI").val("");
    $("#txtFTEIN").val("");
    $("#txtSTI").val("");
    $("#txtNOTT").val("");
    $("#txtNPI").val("");
    $("#txtTI").val("");
    $("#txtNPI").val("");
    setComboReset("cmbPrefix");
    setComboReset("cmbSuffix");
    $("#txtFN").val("");
    $("#txtLN").val("");
    $("#txtMN").val("");
    $("#txtBankSort").val("");
    $("#txtBankAccount").val("");
    $("#txtRemittanceEmail").val("");
    $("#txtMN").val("");
    $("#txtMN").val("");

    /*$("#txtWeight").val("");
    $("#txtHeight").val("");
    $("#txtNN").val("");
    	var dtDOB = $("#dtDOB").data("kendoDatePicker");
    	if(dtDOB){
    		dtDOB.value("");
    	}*/
    $("#txtCity").val("");
    $("#txtState").val("");
    $("#txtCountry").val("");
    $("#txtZip4").val("");

    $("#cmbStatus").val(1);


    // setComboReset("cmbStatus");
    //setComboReset("cmbZip");
    setComboReset("cmbFTIT");
    /*setComboReset("cmbEthicity");
    setComboReset("cmbRace");
    setComboReset("cmbLan");*/
    $("#txtAdd1").val("");
    $("#txtAdd2").val("");
    setComboReset("cmbSMS");
    setComboReset("AddcmbSMS");
    $("#cmbZip").val("");
    $("#txtCell").val("");
    $("#txtNN").val("");
    $("#txtExtension").val("");
    $("#txtWPhone").val("");
    $("#txtExtension1").val("");
    $("#txtHPhone").val("");
    $("#txtEmail").val("");
    $("#txtFax").val("");
    $("#txtCell1").val("");
    $("#txtEmail1").val("");
    $("#AddtxtAdd1").val("");
    $("#AddtxtAdd2").val("");
    $("#AddcmbZip").val("");
    $("#AddtxtCity").val("");
    $("#AddtxtState").val("");
    $("#AddtxtCountry").val("");
    $("#AddtxtZip4").val("");

    $("#rdHome").prop("checked", true);

}

function setComboReset(cmbId) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb) {
        cmb.select(0);
    }
}

function validation() {
    var flag = true;
    var strAbbr = $("#txtAbbreviation").val();
    strAbbr = $.trim(strAbbr);
    // if (strAbbr == "") {
    //     customAlert.error("Error", "Enter Abbrevation");
    //     flag = false;
    //     return false;
    // }
    var strName = $("#txtN").val();
    strName = $.trim(strName);
    if (strName == "") {
        customAlert.error("Error", "Enter Name");
        flag = false;
        return false;
    }
    var strDName = $("#txtDN").val();
    strDName = $.trim(strDName);
    if (strDName == "") {
        customAlert.error("Error", "Enter Display Name");
        flag = false;
        return false;
    }

    return flag;
}

function getComboDataItem(cmbId) {
    var dItem = null;
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb && cmb.dataItem()) {
        dItem = cmb.dataItem();
        return cmb.text();
    }
    return "";
}

function getComboDataItemValue(cmbId) {
    var dItem = null;
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb && cmb.dataItem()) {
        dItem = cmb.dataItem();
        return cmb.value();
    }
    return "";
}

function onClickSave(e) {
    /*if (validation()) {*/
    e.preventDefault();
    $('.alert').remove();
    var isRemittanceEmail = true;
    var remittanceEmail = $("#txtRemittanceEmail").val();
    if (remittanceEmail != "") {
        isRemittanceEmail = validateEmail(remittanceEmail);
    }
    if (isRemittanceEmail) {
        var strId = groupId;
        // var strStatus = getComboDataItem("cmbStatus");
        var strStatus = $("#cmbStatus").val();

        var strSSN = $("#txtSSN").val();
        var strGender = getComboDataItem("cmbGender");
        var strAdd1 = $("#txtAdd1").val();
        var strAdd2 = $("#txtAdd2").val();
        var strCity = $("#txtCity").val();
        var strState = $("#txtState").val();
        var strCountry = $("#txtCountry").val();
        var strFax = $("#txtFax").val();

        //var strZip = getComboDataItemValue("cmbZip");
        var strZip4 = $("#txtZip4").val();
        var strHPhone = $("#txtHPhone").val();
        var strExt1 = $("#txtExtension1").val();
        var strWp = $("#txtWPhone").val();
        var strWpExt = $("#txtExtension").val();
        var strCell = $("#txtCell").val();

        /*var strSMS = getComboDataItem("cmbSMS");*/

        var strEmail = $("#txtEmail").val();

        var dataObj = {};
        //dataObj.id = strId;
        dataObj.createdBy = sessionStorage.userId;
        /*dataObj.isActive = 1;
        if (strStatus && statusArr == "ACTIVE") {
            dataObj.isActive = 1;
        }*/
        if (strStatus == "0") {
            dataObj.isActive = 0;
        } else if (strStatus == "1") {
            dataObj.isActive = 1;
        }
        dataObj.isDeleted = 0;
        //dataObj.id = strId;
        var strExtId1 = $("#txtExtID1").val();
        var strExtId2 = $("#txtExtID2").val();

        dataObj.externalId1 = strExtId1;
        dataObj.externalId2 = strExtId2;

        var strAbbr = $("#txtAbbreviation").val();
        dataObj.abbreviation = strAbbr;

        var strName = $("#txtN").val();
        var strDName = $("#txtDN").val();

        dataObj.name = strName;
        dataObj.displayName = "";

        var strFTIT = getComboDataItem("cmbFTIT");
        var cmbFTIT = $("#cmbFTIT").data("kendoComboBox");
        if (cmbFTIT) {
            strFTIT = cmbFTIT.text();
        }

        var bankSortCode = $("#txtBankSort").val();
        if (bankSortCode != "") {
            dataObj.bankSortCode = bankSortCode;
        } else {
            dataObj.bankSortCode = null;
        }

        var bankAccount = $("#txtBankAccount").val();
        if (bankAccount != "") {
            dataObj.bankAccountNumber = bankAccount;
        } else {
            dataObj.bankAccountNumber = null;
        }

        var remittanceEmail = $("#txtRemittanceEmail").val();
        if (remittanceEmail != "") {
            dataObj.remittanceEmail = remittanceEmail;
        } else {
            dataObj.remittanceEmail = null;
        }

        dataObj.federalTaxIdType = strFTIT; //strFTIT;

        var strFTI = $("#txtFTI").val();
        dataObj.federalTaxId = strFTI;

        var strFTEIN = $("#txtFTEIN").val();
        dataObj.federalTaxEin = strFTEIN;

        var strSTI = $("#txtSTI").val();
        dataObj.stateTaxId = strSTI;

        var strNOTT = $("#txtNOTT").val();
        dataObj.taxName = strNOTT;

        var strNPI = $("#txtNPI").val();
        dataObj.npi = strNPI;

        var strTI = $("#txtTI").val();
        dataObj.taxonomyId = strTI;

        var strPrefix = getComboDataItem("cmbPrefix");
        dataObj.contactPrefix = strPrefix;//$("#txtEmail").val();;
        var strSuffix = getComboDataItem("cmbSuffix");
        dataObj.contactSuffix = strSuffix;
        //dataObj.communicationId = null; //communicationId;

        var strNickName = $("#txtNN").val();
        dataObj.contactNickName = strNickName;

        dataObj.sms = getComboDataItem("cmbSMS");

        var strFN = $("#txtFN").val();
        var strMN = $("#txtMN").val();
        var strLN = $("#txtLN").val();

        dataObj.contactFirstName = strFN;
        dataObj.contactMiddleName = strMN;
        dataObj.contactLastName = strLN;

        //dataObj.contactComunicationId = null; //contactComunicationId;

        var comm1 = {};
        var comm2 = {};
        /*comm1.country = $("#txtCountry").val();
        comm1.city = $("#txtCity").val();
        comm1.state = $("#txtState").val();*/
        /*var cmbZip = $("#cmbZip").data("kendoComboBox");*/
        var cmbZip = $("#cmbZip").val();
        var cityId = "";
        var zip = "";
        // if (cmbZip) {
        cityId = zipSelItem.cityId || zipSelItem.idk;
        // }
        /*comm1.zip = cmbZip;*/
        comm1.defaultCommunication = 1;
        comm1.cityId = cityId;
        if (strStatus == "0") {
            comm1.isActive = 0;
        } else if (strStatus == "1") {
            comm1.isActive = 1;
        }
        comm1.isDeleted = 0;
        /*comm1.countryId = 1;*/
        /*comm1.zipFour = $("#txtZip4").val();*/
        comm1.address1 = $("#txtAdd1").val();
        comm1.address2 = $("#txtAdd2").val();
        /*comm1.state = $("#txtState").val();*/

        /*comm1.sms = getComboDataItem("AddcmbSMS");*/

        comm1.workPhone = $("#txtWPhone").val();
        comm1.workPhoneExt = strWpExt; //$("#txtExtension").val();
        comm1.fax = strFax;
        comm1.cellPhone = $("#txtCell").val();
        comm1.email = $("#txtEmail").val();
        /*comm1.homePhone = $("#txtHPhone").val();
        comm1.homePhoneExt = null;*/
        /*comm1.stateId = 1;
        comm1.areaCode = null;*/
        comm1.parentTypeId = "300";
        comm1.houseNumber = cmbZip;

        /*comm2.country = $("#txtCountry").val();
        comm2.city = $("#txtCity").val();
        comm2.state = $("#txtState").val();*/
        comm2.defaultCommunication = 1;
        var cmbZip = $("#AddcmbZip").data("kendoComboBox");
        /* comm2.zipFour = $("#AddtxtZip4").val();
         comm2.country = $("#AddcmbZip").val();*/
        if ($("#AddcmbZip").val() != "") {
            $("#AddcmbZip").data("kendoComboBox");
            comm2.cityId = AddzipSelItem.cityId || AddzipSelItem.idk;
        }
        comm2.address1 = $("#AddtxtAdd1").val();
        comm2.address2 = $("#AddtxtAdd2").val();
        /*comm2.state = $("#AddtxtState").val();*/
        comm2.isDeleted = 0;
        if (strStatus == "InActive") {
            comm2.isActive = 0;
        } else if (strStatus == "Active") {
            comm2.isActive = 1;
        }
        /*comm2.countryId = 12;
        comm2.sms = "yes";
        comm2.defaultCommunication = 1;*/
        /*comm2.workPhone = "121212"; // $("#txtWPhone").val();
        comm2.workPhoneExt = "12333"; //$("#txtExtension").val();*/
        //comm2.fax = ""
        comm2.cellPhone = $("#txtCell1").val();
        comm2.email = $("#txtEmail1").val();
        //comm2.createdDate = new Date().getTime();
        comm2.homePhone = $("#txtHPhone").val();
        comm2.houseNumber = $("#AddcmbZip").val();
        /*comm2.homePhoneExt = $("#txtExtension1").val();*/
        /*comm2.stateId = 1;*/
        /*comm2.areaCode = null;*/
        comm2.parentTypeId = "301";
        comm2.sms = getComboDataItem("AddcmbSMS");

        /*if (operation == UPDATE) {
            comm2.modifiedBy = sessionStorage.userId;
            comm2.parentId = selAccountItem.idk;
            if(selAccountItem.communications.id) {
                comm2.id = selAccountItem.communications.id;
            }
            //comm2.modifiedDate = new Date().getTime();
        } else {
            comm2.createdBy = sessionStorage.userId;
            //comm2.createdDate = new Date().getTime();
        }*/

        if (operation == UPDATE) {
            var communicationLen = selAccountItem.communications != null ? selAccountItem.communications.length : 0;
            var cityIndexVal = "";
            var dupCityIndexVal = "";
            for (var i = 0; i < communicationLen; i++) {
                if (selAccountItem.communications[i].parentTypeId == 300) {
                    cityIndexVal = i;
                }
                if (selAccountItem.communications[i].parentTypeId == 301) {
                    dupCityIndexVal = i;
                }
            }
            if (cityIndexVal != "" || (cityIndexVal == 0 && selAccountItem.communications != null)) {
                comm1.id = selAccountItem.communications[cityIndexVal].id;
                comm1.modifiedBy = sessionStorage.userId;
                comm1.parentId = selAccountItem.idk;
            }
            if (dupCityIndexVal != "") {
                comm2.id = selAccountItem.communications[dupCityIndexVal].id;
                comm2.modifiedBy = sessionStorage.userId;
                comm2.parentId = selAccountItem.idk;
            }
            //comm1.modifiedDate = new Date().getTime();
        } else {
            comm1.createdBy = Number(sessionStorage.userId);
        }

        var comm = [];
        comm.push(comm1);
        if (showTab != "Detailstab") {
            /*      if($("#txtEmail1").val() != "" || $("#txtCell1").val() != "" || $("#txtHPhone").val() != "") {*/
            if ($("#AddtxtAdd1").val() != "") {
                if ($("#txtEmail1").val() != "" && !validateEmail($("#txtEmail1").val())) {
                    $("body").animate({
                        scrollTop: 0
                    }, 500);
                    // $('.customAlert').append('<div class="alert alert-danger">Please enter valid email address of Contact Person</div>');
                    customAlert.error("Error", "Please enter valid email address of Contact Person");
                    return false;
                } else {
                    comm2.createdBy = Number(sessionStorage.userId);
                    comm.push(comm2);
                }
            } else {
                $("body").animate({
                    scrollTop: 0
                }, 500);
                if (sessionStorage.countryName.indexOf("India") >= 0 || sessionStorage.countryName.indexOf("United Kingdom") >= 0) {
                    // $('.customAlert').append('<div class="alert alert-danger">Please fill Address 1 and Postal Code of Contact Person</div>');
                    customAlert.error("Error", "Please fill Address 1 and Postal Code of Contact Person");
                } else {
                    // $('.customAlert').append('<div class="alert alert-danger">Please fill Address 1 and Zip of Contact Person</div>');
                    customAlert.error("Error", "Please fill Address 1 and Zip of Contact Person");
                }
                return false;
            }
        }
        /*}*/
        /*else if($("#AddcmbZip").val() != "" || $("#AddtxtAdd1").val() != "") {
            if($("#txtEmail1").val() == "" || $("#txtCell1").val() == "" || $("#txtHPhone").val() == "") {
                $("body").animate({
                    scrollTop: 0
                }, 500);
                // $('.customAlert').append('<div class="alert alert-danger">Please enter Email or Homephone or Mobile Phone of Contact Person</div>');
                customAlert.error("Error","Please enter Email or Homephone or Mobile Phone of Contact Person");
                return false;
            }
        }*/

        dataObj.communications = comm;

        if (operation == UPDATE) {

            //dataObj.id = commId;
            //dataObj.isDeleted = "0";
            dataObj.id = strId;
            dataObj.modifiedBy = sessionStorage.userId;
            //dataObj.modifiedDate = new Date().getTime();
        }
        if ((strStatus != "" && strAbbr != "" && strName != "" && strAdd1 != "") || (showTab != "Detailstab")) {
            if (strEmail != "" && !validateEmail(strEmail)) {
                $("body").animate({
                    scrollTop: 0
                }, 500);
                // $('.customAlert').append('<div class="alert alert-danger">Please enter valid Email Address</div>');
                customAlert.error("Error", "Please enter valid email address");

            } else {
                if (operation == ADD) {
                    var dataUrl = ipAddress + "/billing-account/create";
                    createAjaxObject(dataUrl, dataObj, "POST", onCreate, onError);
                } else if (operation == UPDATE) {
                    var dataUrl = ipAddress + "/billing-account/update";
                    createAjaxObject(dataUrl, dataObj, "POST", onUpdate, onError);
                }
            }
        } else {
            $("body").animate({
                scrollTop: 0
            }, 500);
            // $('.customAlert').append('<div class="alert alert-danger">Please fill all the Required Fields</div>');
            customAlert.error("Error", "Please fill all the required fields");
        }
        /*}*/
    } else {
        customAlert.error("Error", "Please enter valid remittance email address");
    }
}
function onStatusChange() {
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if (cmbStatus && cmbStatus.selectedIndex < 0) {
        cmbStatus.select(0);
    }
}
function onFilterChange() {
    var cmbFilterByName = $("#cmbFilterByName").data("kendoComboBox");
    if (cmbFilterByName && cmbFilterByName.selectedIndex < 0) {
        cmbFilterByName.select(0);
    }
    if($("#cmbFilterByName").val() == "id") {
        $("#cmbFilterByValue").removeAttr("disabled");
        $("#cmbFilterByValue").attr("type","number");
    }
    else if($("#cmbFilterByName").val() == "All") {
        $("#cmbFilterByValue").attr("disabled","disabled");
        $("#accountFilter-btn").attr("disabled","disabled");
        $("#cmbFilterByValue").val('');
    }
    else {
        $("#cmbFilterByValue").removeAttr("disabled");
        $("#cmbFilterByValue").attr("type","text");
    }
}

function onCreate(dataObj) {

    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            var msg = "Group Account Created Successfully";
            if(operation == UPDATE){
                msg = " Group Account Updated Successfully"
            }
            displaySessionErrorPopUp("Info", msg, function(res) {
                operation = ADD;
                init();
                onClickCancel();
            })

        } else {
            customAlert.error("error", dataObj.response.status.message.replace("Billing","Group"));
        }
    }

}
function onUpdate(dataObj) {
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            var msg = "Group account updated successfully";
            displaySessionErrorPopUp("Info", msg, function (res) {
                operation = ADD;
                init();
                onClickCancel();
                $('.btnActive').trigger('click');
            });


        }else {
            customAlert.error("error", dataObj.response.status.message.replace("Billing","Group"));
        }
    }
}
function updateList(dataObj) {
    // console.log(dataObj);
    selAccountItem = dataObj.response.billingAccount[0];
    selAccountItem.idk = selAccountItem.id;
    addAccount("update");
}

function onError(errObj) {

    customAlert.error("Error", "Error");
}


function onClickSearch() {
    /*var obj = {};
    obj.status = "search";
    popupClose(obj);*/
}
function validateEmail(sEmail) {
    var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    if (filter.test(sEmail)) {
        return true;
    }
    else {
        return false;
    }
}

function themeAPIChange(){
    getAjaxObject(ipAddress+"/homecare/settings/?id=2","GET",getThemeValue,onError);
}


function getThemeValue(dataObj){

    var tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.activityTypes){
            if($.isArray(dataObj.response.settings)){
                tempCompType = dataObj.response.settings;
            }else{
                tempCompType.push(dataObj.response.settings);
            }
        }
        if(tempCompType.length > 0){
            themesChange(tempCompType[0].value);
        }
    }
}

function themesChange(themeValue){
    if(themeValue == 2){
        loadAPi("../../Theme2/Theme02.css");
    }
    else if(themeValue == 3){
        loadAPi("../../Theme3/Theme03.css");
    }
}
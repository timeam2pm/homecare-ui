var parentRef = null;
var angularUIgridWrapper = null;
var windowLoad = false;
var selectedItems;
var activeListFlag = true;
var sessionMedicalConditionValue = sessionStorage.sessionMedicalConditionValue;
var ADD = "add";
var UPDATE = "edit";
var VIEW = "view";
var DELETE = "delete";
var operation = "add";
var atID = "";
var documentId;

function bindEvents() {
    $('[data-medicate-content=2]').show();
    $('[data-medicate-content=1]').hide();
    $('.sidebar-nav-link').on('click', function () {
        $(this).closest('li').addClass('activeList').siblings().removeClass('activeList');
        $('.medicationForm-form')[0].reset();
        $('.hasError').hide();
        $('.alert').remove();
        $('#medication-save').show();
        $('#medication-reset').show();
        $('#medication-update').hide();
        /*$('#medication-cancel').hide();*/
        $('[data-medicate-list="1"]').find('.tablist-link').text("Add");
        $('.navBar').find('.medication-btn-link').attr('disabled', 'disabled');
        $('.medicationForm-form').find('option').each(function () {
            if ($(this).attr('data-medicate-status') == 1) {
                $(this).attr('selected', 'selected');
            }
            else {
                $(this).removeAttr('selected');
            }
        });
        $('.hasError-rxNorm').remove();
        $('.non-rxNorm-form').show();
        if (windowLoad && $('.tab-list.active-tabList').attr('data-medicate-list') == "2") {
            searchOnLoad();
        }
    });
    $('#btnAdd').on('click', function (e) {
        e.preventDefault();
        $('input[type=radio][name=fileUploadType]').prop('checked', false);
        $("#divVideoURL").hide();
        $("#divFileUpload").hide();
        $("#txtFU").val("");
        $("#imgPlay").removeClass("imgBorder");
        $("#addPopup").show();
        $('#viewDivBlock').hide();
        $("#txtID").hide()
        $("#iVideoFrame").attr("src", "");
        $(".photo-options").hide();
        onClickReset();
        if (sessionMedicalConditionValue.toLowerCase() == "illness") {
            $(".filter-heading").html("Add Diagnosis");
        }
        else if (sessionMedicalConditionValue.toLowerCase() == "diet") {
            $(".filter-heading").html("Add Diet");
        }
        else if (sessionMedicalConditionValue.toLowerCase() == "allergy") {
            $(".filter-heading").html("Add Allergy");
        }
        else if (sessionMedicalConditionValue.toLowerCase() == "exercise") {
            $(".filter-heading").html("Add Exercise");
        }
    });
    $('#btnSave').on('click', function (e) {
        e.preventDefault();
        var method = "POST";
        var urlExtn = getMedicationForm() + '/';
        //var codeVal = $('input[name="codeValue"]').val();
        var activeStatusVal = $("#cmbStatus").val();
        var abbreviationVal = $('input[name="abbreviation"]').val();
        var codeVal = "";


        // if (abbreviationVal !== null && abbreviationVal !== "" && abbreviationVal.length > 0) {
        //     if (abbreviationVal.length > 15) {
        //         codeVal = abbreviationVal.substring(0, 14);
        //     }
        //     // else {
        //     //     codeVal = abbreviationVal;
        //     // }
        // }

        var descriptionVal = $("#description").val();
        var videourlVal = $('input[name="videourl"]').val();
        var fileName = $("#txtFU").val();

        var extension = "";
        var IsDeleted = 0;
        if (activeStatusVal == 1) {
            IsDeleted = 0;
        }
        else {
            IsDeleted = 1;
        }

        var radioValue = $("input[name='fileUploadType']:checked").val();

        if (radioValue === '1') {
            if (videourlVal === '') {
                customAlert.error("Error", "Please fill the required fields");
                return;
            }
        }
        else if (radioValue === '2') {
            if (fileName === '') {
                customAlert.error("Error", "Please fill the required fields");
                return;
            }
            else{
                videourlVal = "";
            }
            // else{
            //     var fNameSplit = fileName.split('.');
            //
            //     if(fNameSplit[1] != undefined && fNameSplit[1] != "") {
            //         extension = fNameSplit[1];
            //     }
            // }
        }

        var dataObj = {
            "code": fileName,
            "isActive": activeStatusVal,
            "value": abbreviationVal,
            "notes": descriptionVal,
            "videoUrl": videourlVal,
            "isDeleted": IsDeleted
        }
        if (operation == "edit") {
            dataObj.id = atID;
            dataObj.modifiedBy = sessionStorage.userId;
            method = "PUT";
        }
        else {
            dataObj.createdBy = sessionStorage.userId;
        }

        if (abbreviationVal != "" && activeStatusVal != "" && descriptionVal != "") {
            var dataUrl = ipAddress + urlExtn;
            createAjaxObject(dataUrl, dataObj, method, onCreate, onError);
        }
        else {
            customAlert.error("Error", "Please fill the Required Fields");
        }
    });
    $("#btnReset").on('click', function (e) {
        e.preventDefault();
        onClickReset();
    });

    $('input[name="videourl"]').on('change', function (e) {
        e.preventDefault();
        var url = $('input[name="videourl"]').val();
        if (url != '') {
            var videoUrl = url + "?access_token=" + sessionStorage.access_token;
            $("#iVideoFrame").attr('src', videoUrl);
            $(".photo-options").show();
        }
        else {
            $(".photo-options").hide();
        }

    });
    $("#medication-list-search").on('click', function (e) {
        e.preventDefault();
        if ($(".btnActive").hasClass("selectButtonBarClass")) {
            $(".btnActive").trigger('click');
        }
        else if ($(".btnInActive").hasClass("selectButtonBarClass")) {
            $(".btnInActive").trigger('click');
        }
    });
    $("#btnDelete").on('click', function (e) {
        e.preventDefault();
        var deleteItemObj = {
            "id": selectedItems[0].idDuplicate,
            "isActive": 0,
            "isDeleted": 1,
            "modifiedBy": sessionStorage.userId
        }
        var urlExtn = getMedicationForm() + '/';
        var dataUrl = ipAddress + urlExtn;
        createAjaxObject(dataUrl, deleteItemObj, "DELETE", onDeleteItem, onDeleteError);
    });
    $("#btnEdit").on('click', function (e) {
        e.preventDefault();
        $("#imgPlay").removeClass("imgBorder");

        // $('.medicationForm-form').find('option').each(function() {
        //     if($(this).attr('data-medicate-status') == selectedItems[0].isActive) {
        //         $(this).attr('selected','selected');
        //     }
        //     else {
        //         $(this).removeAttr('selected');
        //     }
        // });
        $("#txtID").show();
        operation = "edit";
        $("#viewDivBlock").hide();
        $("#addPopup").show();
        $("#txtID").html("ID :" + selectedItems[0].idDuplicate);
        // $('input[name="codeValue"]').val(selectedItems[0].code);
        $('input[name="abbreviation"]').val(selectedItems[0].value);
        $("#description").val(selectedItems[0].notes);
        $('input[name="videourl"]').val(selectedItems[0].videoUrl);
        $('input[name="videourl"]').change();

        if (selectedItems[0].videoUrl !== '') {
            $("#divVideoURL").show();
            $("#divFileUpload").hide();
            $('input[type=radio][value=1]').prop('checked', true);
        }
        else {
            $("#divVideoURL").hide();
            $("#divFileUpload").show();
            $('input[type=radio][value=2]').prop('checked', true);
            documentId = selectedItems[0].id;
            $("#txtFU").val(selectedItems[0].code);
        }


        if (sessionMedicalConditionValue.toLowerCase() == "illness") {
            $(".filter-heading").html("Edit Diagnosis");
        }
        else if (sessionMedicalConditionValue.toLowerCase() == "diet") {
            $(".filter-heading").html("Edit Diet");
        }
        else if (sessionMedicalConditionValue.toLowerCase() == "allergy") {
            $(".filter-heading").html("Edit Allergy");
        }
        else if (sessionMedicalConditionValue.toLowerCase() == "exercise") {
            $(".filter-heading").html("Edit Exercise");
        }
    });
    $("#medication-update").on('click', function (e) {
        e.preventDefault();
        $('.alert').remove();
        var codeVal = $('.medicationForm-form').find('input[name="codeValue"]').val();
        var activeStatusVal = $('.statusUser:visible').find('option:selected').attr('data-medicate-status');
        var abbreviationVal = $('.medicationForm-form').find('input[name="abbreviation"]').val();
        var descriptionVal = $('.medicationForm-form').find('input[name="description"]').val();
        var videourlVal = $('.medicationForm-form:visible').find('input[name="videourl"]').val();
        var updateItemObj = {
            "id": selectedItems[0].idDuplicate,
            "code": codeVal,
            "modifiedBy": sessionStorage.userId,
            "isActive": activeStatusVal,
            "value": abbreviationVal,
            "notes": descriptionVal,
            "videoUrl": videourlVal,
            "isDeleted": 0
        }
        var urlExtn = getMedicationForm() + '/';
        var dataUrl = ipAddress + urlExtn;
        createAjaxObject(dataUrl, updateItemObj, "PUT", onUpdateItem, onUpdateError);
    });
    $('#btnCancel').on('click', function (e) {
        e.preventDefault();
        onClickCancel();
    });

    $('.popupClose').on('click', function (e) {
        e.preventDefault();
        onClickCancel();
    });
    $('#imgPlay').on('click', function (e) {
        e.preventDefault();
        onClickPlay();
    });

    $(".btnActive").on("click", function (e) {
        e.preventDefault();
        activeListFlag = true;
        searchOnLoad();
        onClickActive();
    });
    $(".btnInActive").on("click", function (e) {
        e.preventDefault();
        activeListFlag = false;
        searchOnLoad();
        onClickInActive();
    });
    $('input[type=radio][name=fileUploadType]').change(function () {
        if (this.value == '1') {
            $("#divVideoURL").show();
            $("#divFileUpload").hide();
        }
        else if (this.value == '2') {
            $("#divVideoURL").hide();
            $("#divFileUpload").show();
        }
    });


    $("#fileElem").on("change", onSelectionFiles);

    $("#btnBrowse").off("click", onBrowseClick);
    $("#btnBrowse").on("click", onBrowseClick);
}

function onSelectionFiles(event) {
    files = event.target.files;
    fileName = "";
    var size = files[0].size;
    var type = files[0].type;
    var FUtype = $("#cmbCategory").val();
    if (type.toLowerCase() == "application/pdf") {
        type = "pdf";
    }
    if (type.toLowerCase() == "application/vnd.openxmlformats-officedocument.wordprocessingml.document") {
        type = "doc";
    }
    if (type.toLowerCase() == "image/png") {
        type = "png";
    }
    if (type.toLowerCase() == "image/gif") {
        type = "gif";
    }
    if (type.toLowerCase() == "image/jpeg") {
        type = "jpeg";
    }
    if (type.toLowerCase() == "image/jpg") {
        type = "jpg";
    }

    $('#txtFU').val("");
    var maxSize = 2097152;
    if (files) {
        if (type != undefined && type != "") {
            if (files.length > 0) {
                if (size != undefined && size < maxSize) {
                    fileName = files[0].name;
                    IsFlag = 1;
                    $('#txtFU').val(fileName);
                }
                else {
                    customAlert.error("Error", "File size should be less than 2MB");
                }
            }
        }
    }
}

function onBrowseClick(e) {
    // console.log(e);
    if (e.currentTarget && e.currentTarget.id) {
        var btnId = e.currentTarget.id;
        var fileTagId = ("fileElem" + btnId);
        $("#fileElem").click();
    }
}

function getMedicationForm() {
    var formUrl;
    if (sessionMedicalConditionValue.toLowerCase() == "illness") {
        formUrl = '/homecare/medical-condition-types';
        $(".filter-heading").html("View Diagnosis");
    }
    else if (sessionMedicalConditionValue.toLowerCase() == "diet") {
        formUrl = '/homecare/diet-types';
        $(".filter-heading").html("View Diet");
    }
    else if (sessionMedicalConditionValue.toLowerCase() == "allergy") {
        formUrl = '/homecare/allergy-types';
        $(".filter-heading").html("View Allergy");
    }
    else if (sessionMedicalConditionValue.toLowerCase() == "exercise") {
        formUrl = '/homecare/exercise-types';
        $(".filter-heading").html("View Exercise");
    }
    return formUrl;
}


function getDownloadMedicationForm() {
    var formUrl;
    if (sessionMedicalConditionValue.toLowerCase() == "illness") {
        formUrl = '/homecare/download/medical-condition-types';
        $(".filter-heading").html("View Diagnosis");
    }
    else if (sessionMedicalConditionValue.toLowerCase() == "diet") {
        formUrl = '/homecare/download/diet-types';
        $(".filter-heading").html("View Diet");
    }
    else if (sessionMedicalConditionValue.toLowerCase() == "allergy") {
        formUrl = '/homecare/download/allergy-types';
        $(".filter-heading").html("View Allergy");
    }
    else if (sessionMedicalConditionValue.toLowerCase() == "exercise") {
        formUrl = '/homecare/download/exercise-types';
        $(".filter-heading").html("View Exercise");
    }
    return formUrl;
}


function getMedicationFUPUrl() {
    var formUrl;
    if (sessionMedicalConditionValue.toLowerCase() == "illness") {
        formUrl = '/homecare/upload/medical-condition-types';
    }
    else if (sessionMedicalConditionValue.toLowerCase() == "diet") {
        formUrl = '/homecare/upload/diet-types';
    }
    else if (sessionMedicalConditionValue.toLowerCase() == "allergy") {
        formUrl = '/homecare/upload/allergy-types';
    }
    else if (sessionMedicalConditionValue.toLowerCase() == "exercise") {
        formUrl = '/homecare/upload/exercise-types';
    }
    return formUrl;
}



function searchOnLoad() {
    buildMedicationListGrid([]);
    var urlExtn = getMedicationForm() + '/?';
    getAjaxObject(ipAddress + urlExtn, "GET", getSearchList, onSearchError);
}
function onCreate(dataObj) {
    // console.log("On Create");
    // console.log(dataObj);
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {

            var radioValue = $("input[name='fileUploadType']:checked").val();

            if (radioValue === '2') {

                if (dataObj.response["medical-condition-types"] && dataObj.response["medical-condition-types"].id) {
                    documentId = dataObj.response["medical-condition-types"].id;
                }
                else if (dataObj.response["diet-types"] && dataObj.response["diet-types"].id) {
                    documentId = dataObj.response["diet-types"].id;
                }
                else if (dataObj.response["allergy-types"] && dataObj.response["allergy-types"].id) {
                    documentId = dataObj.response["allergy-types"].id;
                }
                else if (dataObj.response["exercise-types"] && dataObj.response["exercise-types"].id) {
                    documentId = dataObj.response["exercise-types"].id;
                }
                else {
                    documentId = atID;
                }

                fncSaveFile(documentId);

            } else {
                var msg = "Created Successfully";
                if (operation == UPDATE) {
                    msg = "Updated Successfully"
                }
                displaySessionErrorPopUp("Info", msg, function (res) {
                    operation = ADD;
                    searchOnLoad();
                    onClickCancel();
                    onClickActive();
                })
            }
        }
        else {
            customAlert.error("Error", "Error");
        }
    }
}

function onCreateFile(dataObj) {
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            var msg = "Created Successfully";
            if (operation == UPDATE) {
                msg = "Updated Successfully"
            }
            displaySessionErrorPopUp("Info", msg, function (res) {

                operation = ADD;
                searchOnLoad();
                onClickCancel();
                onClickActive();
            })
        } else {
            customAlert.error("Error", dataObj.response.status.message);
        }
    }
}

function fncSaveFile(documentId) {
    var documentId;


    var tagFileId = ("fileElem");
    var file = document.getElementById(tagFileId);
    var fileName = $("#txtFU").val();
    if (fileName != "") {
        if (file && file.files[0] && file.files[0].name) {
            fileName = file.files[0].name;
        }
    }

    var reqUrl = "";

    var formData = new FormData();
    if (operation != UPDATE) {
        formData.append("file", file.files[0]);
    } else {
        formData.append("file", file.files[0]);
    }
    reqUrl = ipAddress + getMedicationFUPUrl() + "/";

    reqUrl = reqUrl + "?access_token=" + sessionStorage.access_token + "&id=" + documentId;

    Loader.showLoader();
    $.ajax({
        url: reqUrl,
        type: 'POST',
        data: formData,
        processData: false,
        contentType: false,// "multipart/form-data",
        // contentType: "application/json",
        headers: {
            tenant: sessionStorage.tenant
        },
        success: function (data, textStatus, jqXHR) {
            if (typeof data.error === 'undefined') {
                Loader.hideLoader();
                onCreateFile(data);
            } else {
                Loader.hideLoader();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            Loader.hideLoader();
        }
    });
}

function onError(errObj) {
    // console.log(errObj);
    customAlert.error("Error", "Error");
}
function onDeleteItem(respObj) {
    // console.log(respObj);
    if (respObj && respObj.response && respObj.response.status) {
        if (respObj.response.status.code == "1") {
            // if($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name').toLowerCase() == "rxnorm") {
            //     rxNormDataOnLoad();
            //     $('#medicationNormData').prepend('<div class="alert alert-success">Deleted Successfully</div>');
            // }
            // if($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name').toLowerCase() == "tradename") {
            //     //tradeNameDataOnLoad();
            //     if($('.tradeName-filter-input').val() != "") {
            //         $('.medication-rxNorm-search').trigger('click');
            //     }
            //     $('#medicationTradeData').prepend('<div class="alert alert-success">Deleted Successfully</div>');
            // }
            // else {
            // searchOnLoad();
            // $('#medicationList').prepend('<div class="alert alert-success">Deleted Successfully</div>');

            var msg = "Deleted successfully";
            displaySessionErrorPopUp("Info", msg, function (res) {
                searchOnLoad();
            });

            // }
            // $('.navBar').find('.medication-btn-link').each(function() {
            //     if(selectedItems && selectedItems.length>0){
            //         $(this).attr('disabled','disabled');
            //     }
            //     else {
            //         $(this).removeAttr('disabled');
            //     }
            // });
        }
        else {
            // $('#medicationList').prepend('<div class="alert alert-danger">Error</div>');
            customAlert.error("Error", "Error");
        }
    }
}
function onDeleteError(errObj) {
    // console.log(errObj);
    customAlert.error("Error", "Error");
}
function onUpdateItem(respObj) {
    // console.log(respObj);
    if (respObj && respObj.response && respObj.response.status) {
        if (respObj.response.status.code == "1") {
            $('.medicationForm-form').prepend('<div class="alert alert-success">Updated Successfully</div>');
            $('.medicationForm-form')[0].reset();
            $('#medication-save').show();
            $('#medication-reset').show();
            $('#medication-update').hide();
            /*$('#medication-cancel').hide();*/
            $('#medication-cancel').click();
            if ($('.sidebar-nav-list.activeList').find('.sidebar-nav-link').attr('data-link-name').toLowerCase() == "tradename") {
                $('.tradeFormWrapper').find('.tradeFormWrapper-select').each(function () {
                    $(this).find('option:first-child').attr('selected', 'selected')
                });
            }
        }
        else {
            $('.medicationForm-form').prepend('<div class="alert alert-danger">' + respObj.response.status.message + '</div>');
        }
    }
}
function onUpdateError(errObj) {
    // console.log(errObj);
    customAlert.error("Error", "Error");
}
function addMedicationData() {
    if (sessionMedicalConditionValue.toLowerCase() == "illness") {
        $(".filter-heading").html("View Diagnosis");
    }
    else if (sessionMedicalConditionValue.toLowerCase() == "diet") {
        $(".filter-heading").html("View Diet");
    }
    else if (sessionMedicalConditionValue.toLowerCase() == "allergy") {
        $(".filter-heading").html("View Allergy");
    }
    else if (sessionMedicalConditionValue.toLowerCase() == "exercise") {
        $(".filter-heading").html("View Exercise");
    }
}

var dataActiveList = [], dataInActiveList = [], dataListArr = [], dataVideoList = [];
function getSearchList(resp) {
    var dataArray, dataActiveList = [], dataInActiveList = [];
    dataVideoList = [];
    if (sessionMedicalConditionValue.toLowerCase() == "illness") {
        if (resp && resp.response && resp.response.medicalConditionTypes) {
            if ($.isArray(resp.response.medicalConditionTypes)) {
                dataArray = resp.response.medicalConditionTypes;
            } else {
                dataArray.push(resp.response.medicalConditionTypes);
            }
        }
    }
    else if (sessionMedicalConditionValue.toLowerCase() == "diet") {
        if (resp && resp.response && resp.response.dietTypes) {
            if ($.isArray(resp.response.dietTypes)) {
                dataArray = resp.response.dietTypes;
            } else {
                dataArray.push(resp.response.dietTypes);
            }
        }
    }
    else if (sessionMedicalConditionValue.toLowerCase() == "allergy") {
        if (resp && resp.response && resp.response.allergyTypes) {
            if ($.isArray(resp.response.allergyTypes)) {
                dataArray = resp.response.allergyTypes;
            } else {
                dataArray.push(resp.response.allergyTypes);
            }
        }
    }
    else if (sessionMedicalConditionValue.toLowerCase() == "exercise") {
        if (resp && resp.response && resp.response.exerciseTypes) {
            if ($.isArray(resp.response.exerciseTypes)) {
                dataArray = resp.response.exerciseTypes;
            } else {
                dataArray.push(resp.response.exerciseTypes);
            }
        }
    }

    if (dataArray != undefined) {
        for (var i = 0; i < dataArray.length; i++) {
            dataArray[i].idDuplicate = dataArray[i].id;

            var fNameSplit = dataArray[i].code.split('.');
            var extension;

            if(fNameSplit[1] != undefined && fNameSplit[1] != "") {
                extension = fNameSplit[1];
            }
            else{
                extension = "";
            }
            dataArray[i].extension = extension;

            if(dataArray[i].videoUrl == "" || dataArray[i].videoUrl == null){
                dataArray[i].imageUrl = ipAddress + "/homecare/download/medical-condition-types/?id="+dataArray[i].id+"&access_token="+sessionStorage.access_token;
                dataArray[i].videoUrl = "";
            }
            else{
                dataArray[i].imageUrl = "";
            }

            if (dataArray[i].isActive == 1) {
                dataArray[i].Status = "Active";
                dataActiveList.push(dataArray[i]);
            }
            else {
                dataArray[i].Status = "InActive";
                dataInActiveList.push(dataArray[i]);
            }
            dataVideoList.push(dataArray[i]);
        }
    }
    if (activeListFlag) {
        buildMedicationListGrid(dataActiveList);
    }
    else if (!activeListFlag) {
        buildMedicationListGrid(dataInActiveList);
    }
}
function onSearchError(err) {
    customAlert.error("Error", "Error");
}
function buildMedicationListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    // gridColumns.push({
    //     "title": "Code",
    //     "field": "code",
    // });
    gridColumns.push({
        "title": "Abbreviation",
        "field": "value",
    });
    gridColumns.push({
        "title": "Description",
        "field": "notes",
    });
    gridColumns.push({
        "title": "Image/ Video / Document",
        "field": "videoUrl",
        "cellTemplate": showVideo()
    });
    angularUIgridWrapper.creategrid(dataSource, gridColumns, otoptions);
    adjustHeight();
}

function showVideo() {
    var node = '<div style="width:200px;padding:0px" ng-show="((row.entity.videoUrl !=  \'\'))" class="textAlign">';
    node += '<img id="{{row.entity.id}}" src="../../img/AppImg/HosImages/video.png"  class="cusrsorStyle videoIcon" onClick="onClickDietYoutube(event)">';
    node += '</div>';
    node += '<div style="width:200px;padding:0px" ng-show="((row.entity.extension !=  \'\'))" class="textAlign">';
    node += '<img id="{{row.entity.id}}" src="../../img/img-icon.png"  class="cusrsorStyle videoIcon" onClick="onClickDietYoutube(event)">';
    node += '</div>';
    return node;
}
function adjustHeight() {
    var defHeight = 220;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    angularUIgridWrapper.adjustGridHeight(cmpHeight);
}

function onChange() {
    setTimeout(function () {
        selectedItems = angularUIgridWrapper.getSelectedRows();
        disableButtons();
    });
}
function disableButtons() {
    if (selectedItems && selectedItems.length > 0) {
        atID = selectedItems[0].idDuplicate;
        $("#btnEdit").prop("disabled", false);
        $("#btnDelete").prop("disabled", false);
    } else {
        $("#btnEdit").prop("disabled", true);
        $("#btnDelete").prop("disabled", true);
    }
}
function onClickActive() {
    $(".btnInActive").removeClass("radioButton-active");
    $(".btnActive").addClass("radioButton-active");
}

function onClickInActive() {
    $(".btnActive").removeClass("radioButton-active");
    $(".btnInActive").addClass("radioButton-active");
}
function init() {
    bindEvents();
    addMedicationData();
    parentRef = parent.frames['iframe'].window;
}
$(document).ready(function () {
    $("#pnlPatient", parent.document).css("display", "none");
    sessionStorage.setItem("IsSearchPanel", "1");
    // themeAPIChange();
    init();
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("medicationList", dataOptions);
    angularUIgridWrapper.init();
    buildMedicationListGrid([]);
});
$(window).load(function () {
    windowLoad = true;
    searchOnLoad();
});
var imgImage = null;
function onClickDietYoutube(event) {
    var dietVideoId, ext = "";
    var urlPath = "";
    if (imgImage) {
        $(imgImage).removeClass("imgBorder");
    }

    if (event.currentTarget) {
        imgImage = event.currentTarget;
        $(imgImage).addClass("imgBorder");
    }
    for (var i = 0; i < dataVideoList.length; i++) {
        var item = dataVideoList[i];
        if (item && item.id == event.currentTarget.id) {
            urlPath = item.videoUrl;
            dietVideoId = item.idDuplicate;
            var fNameSplit = item.code.split('.');

            if(fNameSplit[1] != undefined && fNameSplit[1] != "") {
                ext = fNameSplit[1];
            }
            // iItem = item;
            break;
        }
    }
    // if (urlPath && urlPath.indexOf("www.youtube.com") >= 0) {
    playYouTubeFile(urlPath, dietVideoId, ext);
    // }
}

function onClickPlay() {
    $("#imgPlay").addClass("imgBorder");

    var urlPath = $('input[name="videourl"]').val();

    if (urlPath && urlPath.indexOf("www.youtube.com/embed") >= 0) {
        playYouTubeFile(urlPath);
    }
    else {
        if (!urlPath != '') {
            customAlert.error("Error", "Mention video URL");
        }
        else {
            customAlert.error("Error", "Accepts only youtube embed URL's");
        }
    }
}

function playYouTubeFile(urlPath, fileId, ext) {
    var popW = 900;
    var popH = 580;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Video";
    parentRef.sType = "illness";
    parentRef.fileType = "Video";
    parentRef.extension = ext;
    if(urlPath == ""){
        var urlExtn = getDownloadMedicationForm() + '/?id='+fileId;
        urlPath = ipAddress +urlExtn+"&access_token="+sessionStorage.access_token+"&tenant=" + sessionStorage.tenant;
        parentRef.fileType = "file";
        profileLbl = "Image/Document";
    }
    parentRef.illUrlPath = urlPath;
    devModelWindowWrapper.openPageWindow("../../html/patients/showVideo.html", profileLbl, popW, popH, true, closeVideoScreen);
}

function closeVideoScreen(evt, returnData) {
    //$(imgImage).removeClass("imgBorder");
    //$(imgImage).removeClass("textBorder");
}


function themesChange(themeValue) {
    if (themeValue == 2) {
        loadAPi("../../Theme2/Theme02.css");
    }
    else if (themeValue == 3) {
        loadAPi("../../Theme3/Theme03.css");
    }
}

function themeAPIChange() {
    getAjaxObject(ipAddress + "/homecare/settings/?id=2", "GET", getThemeValue, onError);
}


function getThemeValue(dataObj) {
    var tempCompType = [];
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.activityTypes) {
            if ($.isArray(dataObj.response.settings)) {
                tempCompType = dataObj.response.settings;
            } else {
                tempCompType.push(dataObj.response.settings);
            }
        }
        if (tempCompType.length > 0) {
            themesChange(tempCompType[0].value);
        }
    }
}

function onClickCancel() {
    getMedicationForm();
    $("#viewDivBlock").show();
    $("#addPopup").hide();
}

function onClickReset() {
    $('input[name="codeValue"]').val("");
    $("#cmbStatus").val(1);
    $('input[name="abbreviation"]').val("");
    $("#description").val("");
    $('input[name="videourl"]').val("");
    $('input[type=radio][name=fileUploadType]').prop('checked', false);
    $("#divVideoURL").hide();
    $("#divFileUpload").hide();
    $("#txtFU").val("");
}

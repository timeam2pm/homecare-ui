var parentRef = null;
var operation = "";
var selItem = null;
var ADD = "add";
var UPDATE = "update";
var VIEW = "view";
var DELETE = "delete";


$(document).ready(function(){
	parentRef = parent.frames['iframe'].window;
});

function adjustHeight(){
	var defHeight = 45;
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 40;
    }
}

$(window).load(function(){
	if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
	init();
});

function init(){
	
	buttonEvents();
}

function buttonEvents(){
	$("#btnSave").off("click",onClickSave);
	$("#btnSave").on("click",onClickSave);
	
	$("#btnCancel").off("click",onClickCancel);
	$("#btnCancel").on("click",onClickCancel);
	
}
function onClickSave(){
	var obj = {};
	obj.status = "true";
	popupClose(obj);
}
function onClickCancel(){
	var obj = {};
	obj.status = "false";
	popupClose(obj);
}
function popupClose(obj){
	var windowWrapper = new kendoWindowWrapper();
	windowWrapper.closePageWindow(obj);
}



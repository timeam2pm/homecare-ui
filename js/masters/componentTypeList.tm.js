var angularUIgridWrapper;
var parentRef = null;
var recordType = "1";
var dataArray = [];
var ADD = "add";
var UPDATE = "edit";
var VIEW = "view";
var DELETE = "delete";
var operation = "add";
var atID = "";
var ds  = "";

$(document).ready(function(){
    $("#divMain",parent.document).css("display","none");
    $("#divPatInfo",parent.document).css("display","");
    $("#lblTitleName",parent.document).html("Component Type");
    parentRef = parent.frames['iframe'].window;
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("dgridActivityList", dataOptions);
    angularUIgridWrapper.init();
    buildDeviceListGrid([]);
});

$(window).load(function(){
    loading = false;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
    init();
    buttonEvents();
    adjustHeight();
}

function init(){
    $("#btnEdit").prop("disabled", true);
    $("#btnDelete").prop("disabled", true);
    buildDeviceListGrid([]);
    ///homecare/activity-types/?is-active=1&is-deleted=0&sort=type
    getAjaxObject(ipAddress+"/master/type/list/?name=component","GET",getActivityTypes,onError);

}

function getActivityTypes(dataObj){
    console.log(dataObj);
    var tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.typeMaster){
            if($.isArray(dataObj.response.typeMaster)){
                tempCompType = dataObj.response.typeMaster;
            }else{
                tempCompType.push(dataObj.response.typeMaster);
            }
        }
    }

    buildDeviceListGrid(tempCompType);
}
function onError(errorObj){
    console.log(errorObj);
}
function getFileResourceList(dataObj){
    console.log(dataObj);
    dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.fileResource){
        if($.isArray(dataObj.response.fileResource)){
            dataArray = dataObj.response.fileResource;
        }else{
            dataArray.push(dataObj.response.fileResource);
        }
    }
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        if(!dataArray[i].fileType){
            dataArray[i].fileType = "";
        }
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
        }
    }
    //getDietList();
    buildDeviceListGrid([]);
    setTimeout(function(){
        buildDeviceListGrid(dataArray);
    })
}

function buttonEvents(){
    $("#btnSave").off("click",onClickSave);
    $("#btnSave").on("click",onClickSave);

    $("#btnCancelDet").off("click",onClickCancel);
    $("#btnCancelDet").on("click",onClickCancel);

    $("#btnEdit").off("click",onClickEdit);
    $("#btnEdit").on("click",onClickEdit);

    $("#btnDelete").off("click",onClickDelete);
    $("#btnDelete").on("click",onClickDelete);

    $("#btnAdd").off("click");
    $("#btnAdd").on("click",onClickAdd);

    $("#btnDiet").off("click");
    $("#btnDiet").on("click",onClickDiet);

    $("#btnExcersize").off("click");
    $("#btnExcersize").on("click",onClickExcersize);

    $("#btnillness").off("click");
    $("#btnillness").on("click",onClickillness);

    $(".btnActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('active');
        onClickActive();
    });
    $(".btnInActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('inactive');
        onClickInActive();
    });
}
var allRosterRowCount = 0;
var allRosterRowIndex = 0;
var allRosterRows = [];
var allRosterRowCount = 0;

function searchOnLoad(status) {
    buildDeviceListGrid([]);
    if(status == "active") {
        var urlExtn = ipAddress + "/master/type/list/?name=component/?is-active=1&is-deleted=0";
    }
    else if(status == "inactive") {
        var urlExtn =  ipAddress + "/master/type/list/?name=component/?is-active=0&is-deleted=1";
    }
    getAjaxObject(urlExtn,"GET",getActivityTypes,onError);
}

function onCreateDS(dataObj){
    allRosterRowIndex = allRosterRowIndex+1;
    if(allRosterRowCount == allRosterRowIndex){
        customAlert.error("Info", "Display order/colour flag updated successfully");
        init();
    }else{
        createDisplayOrder();
    }
}
function onClickAdd(){
    parentRef.operation = "add";
    addReportMaster("add");
}
function addReportMaster(opr){
    var popW = 500;
    var popH = "43%";

    //$('.listDataWrapper').hide();
    //$('.addOrRemoveWrapper').show();
    $('.alert').remove();
    var profileLbl = "Add Activity Type";
    parentRef.operation = opr;
    operation = opr;
    if(opr == "add"){
        $('#btnSave').html('<span><img src="../../img/medication/Save.png" class="providerLink-btn-img" />Save</span>');
        $('.tabContentTitle').text('Add Component Type');
        $('#btnReset').show();
        $('#btnReset').trigger('click');

    }else{
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if (selectedItems && selectedItems.length > 0) {
            var obj = selectedItems[0];
            parentRef.id = obj.idk;
            parentRef.displayOrder  = obj.displayOrder;
            parentRef.type = obj.type;
            parentRef.DIO = obj.DIO;
            parentRef.activityGroup = obj.activityGroup;
            parentRef.isActive = obj.isActive;
            operation = UPDATE;
        }

        profileLbl = "Edit Component Type";
        $('.tabContentTitle').text('Edit Component Type');
        $('#btnReset').hide();
        $('#btnSave').html('<span><img src="../../img/medication/Save.png" class="providerLink-btn-img" />Update</span>');
        $('#btnReset').trigger('click');
    }
    var devModelWindowWrapper = new kendoWindowWrapper();
    devModelWindowWrapper.openPageWindow("../../html/masters/createComponentTypeList.html", profileLbl, popW, popH, true, closeaddReportMastertAction);
}
function closeaddReportMastertAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        var opr = returnData.operation;
        if(opr == "add"){
            customAlert.info("info", "Activity Type created successfully");
        }else{
            customAlert.info("info", "Activity Type updated successfully");
        }
        init();
    }
}
function onClickActive() {
    $(".btnInActive").removeClass("selectButtonBarClass");
    $(".btnActive").addClass("selectButtonBarClass");
}

function onClickInActive() {
    $(".btnActive").removeClass("selectButtonBarClass");
    $(".btnInActive").addClass("selectButtonBarClass");
}

function onClickDelete(){
    customAlert.confirm("Confirm", "Are you sure to delete?",function(response){
        if(response.button == "Yes"){
            var dataObj = {};
            dataObj.createdBy = Number(sessionStorage.userId);
            dataObj.isDeleted = 1;
            dataObj.isActive = 0;
            dataObj.id = atID;
            var dataUrl = ipAddress +"/homecare/activity-types/?id="+ atID ;
            var method = "DELETE";
            createAjaxObject(dataUrl, dataObj, method, onCreateDelete, onError);
        }
    });
}

function onCreateDelete(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "Activity Type deleted successfully";
            customAlert.error("Info", msg);
            $("#txtAT").val("");
            operation = ADD;
            init();
        }
    }
}
function onClickSave(){
    var strAT = $("#txtAT").val();
    strAT = $.trim(strAT);
    if(strAT != ""){
        var dataObj = {};
        dataObj.createdBy = Number(sessionStorage.userId);
        dataObj.isDeleted = 0;
        dataObj.isActive = 1;
        dataObj.type = strAT;
        var dataUrl = ipAddress +"/homecare/activity-types/";
        var method = "POST";
        if(operation == UPDATE){
            method = "PUT";
            dataObj.id = atID;
            dataObj.displayOrder = ds;
            dataUrl = ipAddress +"/homecare/activity-types/";
        }else{

        }
        createAjaxObject(dataUrl, dataObj, method, onCreate, onError);
    }
}

function onCreate(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "Activity Type created successfully";
            if(operation == UPDATE){
                msg = "Activity Type updated successfully"
            }else{

            }
            displaySessionErrorPopUp("Info", msg, function(res) {
                $("#txtAT").val("");
                operation = ADD;
                init();
            })
        }else{
            customAlert.error("Error", dataObj.response.status.message);
        }
    }else{

    }
}
function onClickDiet(){
    onDiet();
    recordType = "1";
    init();
}
function onClickExcersize(){
    onExcersize();
    recordType = "2";
    init();
}
function onClickillness(){
    onIllness();
    recordType = "5";
    init();
}
function onDiet(){
    $("#btnDiet").addClass("selectButtonBarClass");
    $("#btnExcersize").removeClass("selectButtonBarClass");
    $("#btnillness").removeClass("selectButtonBarClass");
}
function onExcersize(){
    $("#btnDiet").removeClass("selectButtonBarClass");
    $("#btnExcersize").addClass("selectButtonBarClass");
    $("#btnillness").removeClass("selectButtonBarClass");
}
function onIllness(){
    $("#btnDiet").removeClass("selectButtonBarClass");
    $("#btnExcersize").removeClass("selectButtonBarClass");
    $("#btnillness").addClass("selectButtonBarClass");
}
function adjustHeight(){
    var defHeight = 180;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    angularUIgridWrapper.adjustGridHeight(cmpHeight);
}

function buildDeviceListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Component Type",
        "field": "type",
    });

    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}

var selRow = null;
function onClickAction(){

}
function maxLengthCheck(e){
    console.log(e);
    selRow = angular.element($(e.currentTarget).parent()).scope();
    setTimeout(function(){
        upDateDataSource();
    },100)
}

//begin = selRow.row.entity.DIS;
//end = selRow.row.entity.DIS1;
function upDateDataSource(){
    var selectedItems = angularUIgridWrapper.getSelectedRows();
    console.log(selectedItems);
    var selRowIndex = 0;
    if(selRow){
        var selItem = selRow.row.entity;//selectedItems[0];
        selRowIndex = selItem.idk;
        var idVal = selItem.DIO;
        idVal = Number(idVal);

        var begin = selItem.DIO;
        begin = Number(begin);

        var end = selItem.DIO1;
        end = Number(end);

        //selItem.DIO1 = end;
        //angularUIgridWrapper.refreshGrid();
        var rows = angularUIgridWrapper.getScope().gridApi.core.getVisibleRows();
        console.log(rows);

        for(var i=0;i<rows.length;i++){
            var rowItem = rows[i].entity;
            if(rowItem.id == selRow.row.entity.id){
                continue;
            }
            var rowValue = rowItem.DIO;
            if(end>begin){
                flag = true;
                if(rowValue>=begin && rowValue<end){
                    var rValue = (rowItem.DIO+1);
                    rowItem.DIO = rValue;
                    rowItem.DIO1 = rValue;
                }
            }else{
                flag = false;
                if(rowValue>end){
                    var rValue = (rowItem.DIO+1);
                    rowItem.DIO = rValue;
                    rowItem.DIO1 = rValue;
                }
            }
        }
        if(flag){
            selRow.row.entity.DIS1 = begin;
        }else{
            selRow.row.entity.DIS1 = begin;
        }
        angularUIgridWrapper.refreshGrid();
    }

}
var prevSelectedItem =[];
function onChange(){
    setTimeout(function() {
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if (selectedItems && selectedItems.length > 0) {
            var obj = selectedItems[0];
            atID = obj.idk;
            $("#btnEdit").prop("disabled", false);
            $("#btnDelete").prop("disabled", false);
        } else {
            $("#btnEdit").prop("disabled", true);
            $("#btnDelete").prop("disabled", true);
        }
    });

}

function onClickEdit(){
    parentRef.operation = "edit";
    addReportMaster("edit");
    //var selectedItems = angularUIgridWrapper.getSelectedRows();
    //console.log(selectedItems);
    //if (selectedItems && selectedItems.length > 0) {
    //	var obj = selectedItems[0];
    //	atID = obj.idk;
    //	ds = obj.displayOrder;
    //	$("#txtAT").val(obj.type);
    //	$("#txtDO").val(obj.DIO);
    //	operation = UPDATE;
    //}
}

function closeVideoScreen(evt,returnData){

}
var operation = "add";
var selFileResourceDataItem = null;
function onClickCancel(){
    var obj = {};
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}

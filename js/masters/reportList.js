var angularUIgridWrapper;
var reportRef = null;
var operation = "";
var selItem = null;
var ADD = "add";
var UPDATE = "edit";
var VIEW = "view";
var DELETE = "delete";
var patientId = "";
var statusArr = [{ Key: 'Active', Value: 'Active' }, { Key: 'InActive', Value: 'InActive' }];

var typeArr = [{Key: '', Value: '' }, { Key: '100', Value: 'Doctor' }, { Key: '200', Value: 'Nurse' }, { Key: '201', Value: 'Caregiver' }];
var filterArr = [{ Key: '', Value: 'All' }, { Key: 'id', Value: 'Provider ID' }, { Key: 'abbr', Value: 'Abbreviation' }, { Key: 'first-name', Value: 'First Name' }, { Key: 'middle-name', Value: 'Middle Name' }, { Key: 'last-name', Value: 'Last Name' }, { Key: 'type', Value: 'Type' }, { Key: 'user-id', Value: 'User Id' }, { Key: 'facility-id', Value: 'Facility Id' }];

var patientInfoObject = null;
var commId = "";

$(document).ready(function(){
	var dataOptions = {
        pagination: false,
        changeCallBack: onChange
	}
	angularUIgridWrapper = new AngularUIGridWrapper("dgridDAccountList", dataOptions);
	angularUIgridWrapper.init();
	buildAccountListGrid([]);
	//getCountryZoneName();
});


$(window).load(function(){
	reportRef = parent.frames['iframe'].window;
	loading = false;
	$(window).resize(adjustHeight);
onLoaded();
	if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
	init();
    buttonEvents();
	adjustHeight();
	$('.btnActive').trigger('click');
}

function init(){
	$("#btnEdit").prop("disabled", true);
	$("#btnDelete").prop("disabled", true);
	buildAccountListGrid([]);
	
	$('[data-toggle="tab"]').on('click', function(e) {
    	e.preventDefault();
    	$('.alert').remove();
    });
	//setDataForSelection(typeArr, "txtType", onTypeChange, ["Value", "Key"], 0, "");
	//setDataForSelection(filterArr, "cmbFilterByName", onFilterChange, ["Value", "Key"], 0, "");
	//getZip();
	getAjaxObject(ipAddress+"/homecare/reports/","GET",getAccountList,onError);
}
function onError(errorObj){
	console.log(errorObj);
}
function getAccountList(dataObj){
	console.log(dataObj);
	var dataArray = [];
	//var tempDataArry = [];
	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		if($.isArray(dataObj.response.reports)){
			dataArray = dataObj.response.reports;
		}else{
			dataArray.push(dataObj.response.reports);
		}
	}else{
		//customAlert.error("Error",dataObj.response.status.message);
	}
	
	
	for(var i=0;i<dataArray.length;i++){
		dataArray[i].idk = dataArray[i].id; 
		dataArray[i].Status = "InActive";
		if(dataArray[i].isActive == 1){
			dataArray[i].Status = "Active";
			var createdDate = new Date(dataArray[i].createdDate);
			var createdDateString = createdDate.toLocaleDateString();
			var modifiedDate = new Date(dataArray[i].modifiedDate);
			var modifiedDateString = modifiedDate.toLocaleDateString();
			dataArray[i].createdDateString = createdDateString;
			dataArray[i].modifiedDateString = modifiedDateString;
		}
		else{
			dataArray = [];
		}
	}
	buildAccountListGrid(dataArray);
}
function buttonEvents(){
	$("#btnEdit").off("click",onClickOK);
	$("#btnEdit").on("click",onClickOK);
	
	$("#btnCancelDet").off("click",onClickCancel);
	$("#btnCancelDet").on("click",onClickCancel);
	
	$("#btnDelete").off("click",onClickDelete);
	$("#btnDelete").on("click",onClickDelete);
	
	$("#btnAdd").off("click",onClickAdd);
	$("#btnAdd").on("click",onClickAdd);

	$("#btnCancel").off("click", onClickClose);
    $("#btnCancel").on("click", onClickClose);

    $("#btnSave").off("click", onClickSave);
    $("#btnSave").on("click", onClickSave);

    $("#btnSearch").off("click", onClickSearch);
    $("#btnSearch").on("click", onClickSearch);

    $("#btnReset").off("click", onClickReset);
    $("#btnReset").on("click", onClickReset);

   // $("#btnZipSearch").off("click");
    //$("#btnZipSearch").on("click", onClickZipSearch);

    //$("#providerFilter-btn").off("click");
    //$("#providerFilter-btn").on("click", onClickFilterBtn);

	$(".btnActive").on("click", function(e) {
		e.preventDefault();
		$('.alert').remove();
		searchOnLoad('active');
		onClickActive();
	});
	$(".btnInActive").on("click", function(e) {
		e.preventDefault();
		$('.alert').remove();
		searchOnLoad('inactive');
		onClickInActive();
	});	
}
function searchOnLoad(status) {
	buildAccountListGrid([]);
	if(status == "active") {
		var urlExtn = '/homecare/reports/';
	}
	else if(status == "inactive") {
		var urlExtn = '/homecare/reports/';
	}
	getAjaxObject(ipAddress+urlExtn,"GET",getAccountList,onError);
}
function adjustHeight(){
	var defHeight = 250;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
	angularUIgridWrapper.adjustGridHeight(cmpHeight);
}
function onClickActive() {
	$(".btnInActive").removeClass("selectButtonBarClass");
	$(".btnActive").addClass("selectButtonBarClass");
}
function onClickInActive() {
	$(".btnActive").removeClass("selectButtonBarClass");
	$(".btnInActive").addClass("selectButtonBarClass");
}
function onClickFilterBtn(e) {
	e.preventDefault();
	$('.alert').remove();
	var filterName = $("#cmbFilterByName").val();
	var filterValue = $("#cmbFilterByValue").val();
	buildAccountListGrid([]);
	/*if($(".btnActive").hasClass("selectButtonBarClass")) {
		var urlExtn = '/billing-account/list?is-active=1&is-deleted=0';
	}
	else {
		var urlExtn = '/billing-account/list?is-active=0&is-deleted=1';
	}*/
	//getAjaxObject(ipAddress+"/provider/list?is-active=1&is-deleted=0&"+filterName+"="+filterValue,"GET",getAccountList,onError);
}

function buildAccountListGrid(dataSource) {
	var gridColumns = [];
	var otoptions = {};
	otoptions.noUnselect = false;
	gridColumns.push({
        "title": "ID",
        "field": "idk",
	});
    gridColumns.push({
        "title": "Name",
        "field": "name",
	});
    gridColumns.push({
        "title": "Notes",
        "field": "notes",
	});
	// gridColumns.push({
     //    "title": "API",
     //    "field": "api"
	// });
	angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions); 
	adjustHeight();
	//onIndividualRowClick();
}

function showAction(){
	var node = '<div style="text-align:center">';
	//node += '<input  type="button" id="btn" value="View" onclick="onClickAction(event)"></input>';
	node += '<a href="#" onclick="onClickAction(event)" style="cursor:pointer">View</a>';
	node += '</div>';
	return node;
}

var selRow = null;
function onClickAction(e){
	setTimeout(function(){
		selRow = angular.element($(e.target).parent()).scope();
		onClickGPSReport();
	})
}
function onClickGPSReport(){
	reportRef.screenType = "providers";
	reportRef.providerid = selRow.row.entity.userId;
	openReportPopup("../../html/patients/geoReport.html","GPS Report");
}
function openReportPopup(path,title){
	var popW = "60%";
    var popH = "75%";

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = title;
    devModelWindowWrapper.openPageWindow(path, profileLbl, popW, popH, false, closeCallReport);
}
function closeCallReport(evt,ret){
	
}
function getCountryZoneName() {
	 var countryName = sessionStorage.countryName;
        if (countryName.indexOf("India") >= 0) {
            $('.postalCode').html('Postal Code :<span class="mandatoryField">*</span>');
            $('.stateLabel').text("State");
            $('.zipFourWrapper').addClass('hideZipFourWrapper');
        } else if (countryName.indexOf("United Kingdom")) {
            $('.postalCode').html('Postal Code :<span class="mandatoryField">*</span>');
            $('.stateLabel').text("County");
            $('.zipFourWrapper').addClass('hideZipFourWrapper');
        } else {
        	$('.postalCode').html('Zip :<span class="mandatoryField">*</span>');
            $('.stateLabel').text("State");
            $('.zipFourWrapper').removeClass('hideZipFourWrapper');
        }
    /*$.getJSON('//freegeoip.net/json/?callback=', function(dataObj) {
        var dataUrl = ipAddress + "/user/location";
        console.log(dataObj);
        sessionStorage.country = dataObj.country_code;
        dataObj.createdBy = sessionStorage.userId;
        dataObj.countryCode = dataObj.country_code;
        dataObj.countryName = dataObj.country_name;
        dataObj.regionCode = dataObj.region_code;
        dataObj.regionName = dataObj.region_name;
        dataObj.timeZone = dataObj.time_zone;
        dataObj.metroCode = dataObj.metro_code;
        dataObj.zipCode = dataObj.zip_code;
        sessionStorage.countryName = dataObj.country_name;
        var countryName = sessionStorage.countryName;
        if (countryName.indexOf("India") >= 0) {
            $('.postalCode').html('Postal Code :<span class="mandatoryField">*</span>');
            $('.stateLabel').text("State");
            $('.zipFourWrapper').addClass('hideZipFourWrapper');
        } else if (countryName.indexOf("United Kingdom")) {
            $('.postalCode').html('Postal Code :<span class="mandatoryField">*</span>');
            $('.stateLabel').text("County");
            $('.zipFourWrapper').addClass('hideZipFourWrapper');
        } else {
        	$('.postalCode').html('Zip :<span class="mandatoryField">*</span>');
            $('.stateLabel').text("State");
            $('.zipFourWrapper').removeClass('hideZipFourWrapper');
        }
    });*/
}
var prevSelectedItem =[];
function onChange(){
	setTimeout(function(){
		var selectedItems = angularUIgridWrapper.getSelectedRows();
		 console.log(selectedItems);
		 if(selectedItems && selectedItems.length>0){
			 $("#btnEdit").prop("disabled", false);
			 $("#btnDelete").prop("disabled", false);
		 }else{
			 $("#btnEdit").prop("disabled", true);
			 $("#btnDelete").prop("disabled", true);
		 }
	},100)
}

var selDocItem = null;
function onClickOK(){
	setTimeout(function(){
		 var selectedItems = angularUIgridWrapper.getSelectedRows();
		 console.log(selectedItems);
		 if(selectedItems && selectedItems.length>0){
			var obj = {};
			obj.selItem = selectedItems[0];
			selDocItem = selectedItems[0];
			reportRef.selDocItem = selDocItem;
			reportRef.operation = "update";
			addReportMaster("edit")
			/*var onCloseData = new Object();
			obj.status = "success";
			obj.operation = "ok";
			var windowWrapper = new kendoWindowWrapper();
			windowWrapper.closePageWindow(obj);*/
		 }
	})
}
function onClickDelete(){
	customAlert.confirm("Confirm", "Are you sure you want delete?",function(response){
		if(response.button == "Yes"){
			var selectedItems = angularUIgridWrapper.getSelectedRows();
			 console.log(selectedItems);
			 if(selectedItems && selectedItems.length>0){
				 var selItem = selectedItems[0];
				 if(selItem){
					 var dataUrl = ipAddress+"/homecare/reports/";
					 var reqObj = {};
					 reqObj.id = selItem.id;
					 reqObj.isDeleted = "1";
					 reqObj.isActive = "0";
					 reqObj.modifiedBy = sessionStorage.userId;
					 createAjaxObject(dataUrl,reqObj,"DELETE",onDeleteCountryt,onError);
				 }
			 }
		}
	});
}
function onDeleteCountryt(dataObj){
	console.log(dataObj);
	if(dataObj && dataObj.response.status){
		if(dataObj.response.status.code == "1"){
			customAlert.info("info", "Report Master Deleted Successfully");
			buildAccountListGrid([]);
			init();
		}else{
			customAlert.error("error", dataObj.message);
		}
	}
}
function onClickAdd(){
	/*var obj = {};
	 obj.status = "Add";
	 obj.operation = "ok";
		var windowWrapper = new kendoWindowWrapper();
		windowWrapper.closePageWindow(obj);*/
	reportRef.operation = "add";
	addReportMaster("add");
}

function addReportMaster(opr){
	var popW = "70%";
    var popH = "75%";

    //$('.listDataWrapper').hide();
    //$('.addOrRemoveWrapper').show();
    $('.alert').remove();
    var profileLbl = "Add Report Master";
    reportRef.operation = opr;
    operation = opr;
    if(opr == "add"){
    	$('#btnSave').html('<span><img src="../../img/medication/Save.png" class="providerLink-btn-img" />Save</span>');
    	$('.tabContentTitle').text('Add Report Master');
    	$('#btnReset').show();
    	//var billActName = $("#txtBAN").val();
    	$('#btnReset').trigger('click');
    	//$("#txtBAN").val(billActName);
    }else{
    	reportRef.id = selDocItem.idk;
    	profileLbl = "Edit Report Master";
    	$('.tabContentTitle').text('Edit Report Master');
        $('#btnReset').hide();
        $('#btnSave').html('<span><img src="../../img/medication/Save.png" class="providerLink-btn-img" />Update</span>');
        $('#btnReset').trigger('click');
        //onFacilityChange();
    }
    var devModelWindowWrapper = new kendoWindowWrapper();
    devModelWindowWrapper.openPageWindow("../../html/masters/createReportMaster.html", profileLbl, popW, popH, true, closeaddReportMastertAction);
}
function closeaddReportMastertAction(evt,returnData){
	if(returnData && returnData.status == "success"){
		var opr = returnData.operation;
		if(opr == "add"){
			customAlert.info("info", "Report Master Created Successfully");
		}else{
			customAlert.info("info", "Report Master Updated Successfully");
		}
		init();
	}
}
function onClickCancel(){
	var obj = {};
	var windowWrapper = new kendoWindowWrapper();
	windowWrapper.closePageWindow(obj);
}
function onClickClose() {
	//$('.listDataWrapper').show();
    //$('.addOrRemoveWrapper').hide();
    $("#btnEdit").prop("disabled", true);
	$("#btnDelete").prop("disabled", true);
    //buildAccountListGrid([]);
    init();
    $('.selectButtonBarClass').trigger('click');
}


function onClickZipSearch() {
    var popW = 600;
    var popH = 500;

    reportRef.searchZip = true;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search Zip";
	var cntry = sessionStorage.countryName;
	 if(cntry.indexOf("India")>=0){
		  profileLbl = "Search Postal Code";
	 }else if(cntry.indexOf("United Kingdom")>=0){
		profileLbl = "Search Postal Code";
	 }else if(cntry.indexOf("United State")>=0){
		 profileLbl = "Search Zip";
	 }else{
		 profileLbl = "Search Zip";
	 }
    devModelWindowWrapper.openPageWindow("../../html/masters/zipList.html", profileLbl, popW, popH, true, onCloseSearchZipAction);
}
var zipSelItem = null;
var cityId = "";

function onCloseSearchZipAction(evt, returnData) {
    console.log(returnData);
    if (returnData && returnData.status == "success") {
        //console.log();
        $("#cmbZip").val("");
        $("#txtZip4").val("");
        $("#txtState").val("");
        $("#txtCountry").val("");
        $("#txtCity").val("");
        var selItem = returnData.selItem;
        if (selItem) {
            zipSelItem = selItem;
            if (zipSelItem) {
                cityId = zipSelItem.idk;
                if (zipSelItem.zip) {
                    $("#cmbZip").val(zipSelItem.zip);
                }
                if (zipSelItem.zipFour) {
                    $("#txtZip4").val(zipSelItem.zipFour);
                }
                if (zipSelItem.state) {
                    $("#txtState").val(zipSelItem.state);
                }
                if (zipSelItem.country) {
                    $("#txtCountry").val(zipSelItem.country);
                }
                if (zipSelItem.city) {
                    $("#txtCity").val(zipSelItem.city);
                }
            }
        }
    }
}

function getZip() {
    getAjaxObject(ipAddress + "/city/list?is-active=1", "GET", getZipList, onError);
}

function getZipList(dataObj) {
    //var dArray = getTableListArray(dataObj);
    var dArray = [];
    if (dataObj && dataObj.response && dataObj.response.cityext) {
        if ($.isArray(dataObj.response.cityext)) {
            dArray = dataObj.response.cityext;
        } else {
            dArray.push(dataObj.response.cityext);
        }
    }
    if (dArray && dArray.length > 0) {
        //setDataForSelection(dArray, "cmbZip", onZipChange, ["zip", "id"], 0, "");
        onZipChange();
    }
    getPrefix();
}

function getPrefix() {
    getAjaxObject(ipAddress + "/master/Prefix/list?is-active=1", "GET", getCodeTableValueList, onError);
}

function onComboChange(cmbId) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb && cmb.selectedIndex < 0) {
        cmb.select(0);
    }
}

function onPrefixChange() {
    onComboChange("cmbPrefix");
}
function onSuffixChange() {
    onComboChange("cmbSuffix");
}
function onUserIdChange() {
	onComboChange("cmbUserId");
}

function onPtChange() {
    onComboChange("cmbStatus");
}

function onGenderChange() {
    onComboChange("cmbGender");
}

function onSMSChange() {
    onComboChange("cmbSMS");
}

function onLanChange() {
    onComboChange("cmbLan");
}

function onRaceChange() {
    onComboChange("cmbRace");
}

function onEthnicityChange() {
    onComboChange("cmbEthicity");
}

function onZipChange() {
    onComboChange("cmbZip");
    var cmbZip = $("#cmbZip").data("kendoComboBox");
    if (cmbZip) {
        var dataItem = cmbZip.dataItem();
        if (dataItem) {
            $("#txtZip4").val(dataItem.zipFour);
            $("#txtCountry").val(dataItem.country);
            $("#txtState").val(dataItem.state);
            $("#txtCity").val(dataItem.city);
        }
    }
}

function getCodeTableValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbPrefix", onPrefixChange, ["value", "idk"], 0, "");
    }
    getAjaxObject(ipAddress + "/master/Suffix/list?is-active=1", "GET", getCodeTableSuffixValueList, onError);
}
function getCodeTableSuffixValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbSuffix", onSuffixChange, ["value", "idk"], 0, "");
    }
    getAjaxObject(ipAddress + "/user/list?is-active=1&is-deleted=0&user-type-id=100,200,201", "GET", getUserIdValueList, onError);
}
function getUserIdValueList(dataObj) {
	var dArray = getTableUserListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbUserId", onUserIdChange, ["firstAndLastName", "userIdDup"], 0, "");
    }
    getAjaxObject(ipAddress + "/master/Status/list?is-active=1", "GET", getPatientStatusValueList, onError);
}

function getPatientStatusValueList(dataObj) {
    var dArray = getTableFirstListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbStatus", onPtChange, ["desc", "idk"], 0, "");
    }
    getAjaxObject(ipAddress + "/master/Gender/list?is-active=1", "GET", getGenderValueList, onError);
}

function getGenderValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    /*if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbGender", onGenderChange, ["desc", "idk"], 0, "");
    }*/
    getAjaxObject(ipAddress + "/master/SMS/list?is-active=1", "GET", getSMSValueList, onError);
}

function getSMSValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbSMS", onSMSChange, ["desc", "idk"], 0, "");
    }
    getAjaxObject(ipAddress + "/master/Language/list?is-active=1", "GET", getLanguageValueList, onError);
}

function getLanguageValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    /*if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbLan", onLanChange, ["desc", "idk"], 0, "");
    }*/
    getAjaxObject(ipAddress + "/master/Race/list?is-active=1", "GET", getRaceValueList, onError);
}

function getRaceValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    /*if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbRace", onRaceChange, ["desc", "idk"], 0, "");
    }*/
    getAjaxObject(ipAddress + "/master/Ethnicity/list?is-active=1", "GET", getEthnicityValueList, onError);
}

function getEthnicityValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    /*if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbEthicity", onEthnicityChange, ["desc", "idk"], 0, "");
    }*/
    getAjaxObject(ipAddress + "/facility/list?is-active=1", "GET", getFacilityList, onError);
}

function getFacilityList(dataObj) {
    console.log(dataObj);
    var dataArray = [];
    if (dataObj) {
        if ($.isArray(dataObj.response.facility)) {
            dataArray = dataObj.response.facility;
        } else {
            dataArray.push(dataObj.response.facility);
        }
    }
    var tempDataArry = [];
    var obj = {};
    obj.name = "";
    obj.idk = "";
    tempDataArry.push(obj);
    for (var i = 0; i < dataArray.length; i++) {
        dataArray[i].Status = "InActive";
        if (dataArray[i].isActive == 1) {
            dataArray[i].Status = "Active";
            var obj = dataArray[i];
            obj.idk = dataArray[i].id;
            obj.status = dataArray[i].Status;
            tempDataArry.push(obj);
        }
    }
    dataArray = tempDataArry;

    setDataForSelection(dataArray, "txtFAN", onFacilityChange, ["name", "idk"], 0, "");
    onFacilityChange();
    if (operation == UPDATE && patientId != "") {
        getPatientInfo();
    }
}

function onFacilityChange() {
    var txtFAN = $("#txtFAN").data("kendoComboBox");
    if (txtFAN) {
        var txtFId = txtFAN.value();
        getAjaxObject(ipAddress + "/facility/list?id=" + txtFId, "GET", onGetFacilityInfo, onError);
    }
}
var billActNo = "";

function onGetFacilityInfo(dataObj) {
    billActNo = "";
    if (dataObj && dataObj.response && dataObj.response.facility) {
        $("#txtBAN").val(dataObj.response.facility[0].name);
        billActNo = dataObj.response.facility[0].billingAccountId;
    }
}

function getPatientInfo() {
    getAjaxObject(ipAddress + "/provider/list?id=" + patientId, "GET", onGetPatientInfo, onError);
}

function getComboListIndex(cmbId, attr, attrVal) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb) {
        var ds = cmb.dataSource;
        var totalRec = ds.total();
        for (var i = 0; i < totalRec; i++) {
            var dtItem = ds.at(i);
            if (dtItem && dtItem[attr] == attrVal) {
                cmb.select(i);
                return i;
            }
        }
    }
    return -1;
}



function getTableListArray(dataObj) {
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.codeTable) {
        if ($.isArray(dataObj.response.codeTable)) {
            dataArray = dataObj.response.codeTable;
        } else {
            dataArray.push(dataObj.response.codeTable);
        }
    }
    var tempDataArry = [];
    var obj = {};
    obj.desc = "";
    obj.zip = "";
    obj.value = "";
    obj.idk = "";
    tempDataArry.push(obj);
    for (var i = 0; i < dataArray.length; i++) {
        if (dataArray[i].isActive == 1) {
            var obj = dataArray[i];
            obj.idk = dataArray[i].id;
            obj.status = dataArray[i].Status;
            tempDataArry.push(obj);
        }
    }
    return tempDataArry;
}
function getTableFirstListArray(dataObj) {
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.codeTable) {
        if ($.isArray(dataObj.response.codeTable)) {
            dataArray = dataObj.response.codeTable;
        } else {
            dataArray.push(dataObj.response.codeTable);
        }
    }
    var tempDataArry = [];
    for (var i = 0; i < dataArray.length; i++) {
        if (dataArray[i].isActive == 1) {
            var obj = dataArray[i];
            obj.idk = dataArray[i].id;
            obj.status = dataArray[i].Status;
            tempDataArry.push(obj);
        }
    }
    return tempDataArry;
}
function getTableUserListArray(dataObj) {
	var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.user) {
        if ($.isArray(dataObj.response.user)) {
            dataArray = dataObj.response.user;
        } else {
            dataArray.push(dataObj.response.user);
        }
    }
    var tempDataArry = [];
    var obj = {};
    obj.firstAndLastName = "";
    obj.idk = "";
    obj.userTypeId = "";
    obj.userIdDup = "";
    tempDataArry.push(obj);
    for (var i = 0; i < dataArray.length; i++) {
        if (dataArray[i].isActive == 1) {
            var obj = dataArray[i];
            obj.idk = obj.id;
            obj.status = obj.Status;
            obj.userIdDup = obj.idk;
            if(obj.userTypeId == 100) {
            	obj.firstAndLastName = obj.firstName + ", " + obj.lastName + " - Doctor";
            }
            if(obj.userTypeId == 200) {
            	obj.firstAndLastName = obj.firstName + ", " + obj.lastName + " - Nurse";
            }
            if(obj.userTypeId == 201) {
            	obj.firstAndLastName = obj.firstName + ", " + obj.lastName + " - Caregiver";
            }
            tempDataArry.push(obj);
        }
    }
    return tempDataArry;
}

function onClickReset() {
    if(operation == ADD) {
    	operation = ADD;
    }
    else {
    	operation = UPDATE;
    }
    patientId = "";
    $("#txtID").val("");
    $("#txtRN").val("");
    $("#txtNotes").val("");
    $("#txtAPI").val("");
 }

function setComboReset(cmbId) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb) {
        cmb.select(0);
    }
}

function validation() {
    var flag = true;
    var txtFC = $("#txtFC").val();
    txtFC = $.trim(txtFC);
    if (txtFC == "") {
        customAlert.error("Error", "Enter Finance charges");
        flag = false;
        return false;
    }
    if (cityId == "") {
        customAlert.error("Error", "Select City ");
        flag = false;
        return false;
    }
    /*var strAbbr = $("#txtAbbrevation").val();
    	strAbbr = $.trim(strAbbr);
    	if(strAbbr == ""){
    		customAlert.error("Error","Enter Abbrevation");
    		flag = false;
    		return false;
    	}
    	var strCode = $("#txtCode").val();
    	strCode = $.trim(strCode);
    	if(strCode == ""){
    		customAlert.error("Error","Enter Code");
    		flag = false;
    		return false;
    	}
    	var strName = $("#txtName").val();
    	strName = $.trim(strName);
    	if(strName == ""){
    		customAlert.error("Error","Enter Country");
    		flag = false;
    		return false;
    	}*/

    return flag;
}

function getComboDataItem(cmbId) {
    var dItem = null;
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb && cmb.dataItem()) {
        dItem = cmb.dataItem();
        return cmb.text();
    }
    return "";
}

function getComboDataItemValue(cmbId) {
    var dItem = null;
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb && cmb.dataItem()) {
        dItem = cmb.dataItem();
        return cmb.value();
    }
    return "";
}


function onClickSave() {
    var sEmail = $('#txtEmail').val();
    /*if (validation()) {*/
    	$('.alert').remove();
        var strId = $("#txtID").val();
        var strExtId1 = $("#txtExtID1").val();
        var strExtId2 = $("#txtExtID2").val();
        var strPrefix = getComboDataItem("cmbPrefix");
        var strSuffix = getComboDataItem("cmbSuffix");
        var strNickName = $("#txtNN").val();
        var strStatus = getComboDataItem("cmbStatus");
        var strAbbr = $("#txtAbbreviation").val();
        var strFN = $("#txtFN").val();
        var strMN = $("#txtMN").val();
        var strLN = $("#txtLN").val();

        var dtItem = $("#dtDOB").data("kendoDatePicker");
        var strDate = "";
        if (dtItem && dtItem.value()) {
            strDate = kendo.toString(dtItem.value(), "yyyy-MM-dd");
        }

        var strSSN = $("#txtSSN").val();
        var strGender = getComboDataItem("cmbGender");
        var strAdd1 = $("#txtAdd1").val();
        var strAdd2 = $("#txtAdd2").val();
        var strCity = $("#txtCity").val();
        var strState = $("#txtState").val();
        var strCountry = $("#txtCountry").val();

        var strZip = $("#cmbZip").val();
        var strZip4 = $("#txtZip4").val();
        var strHPhone = $("#txtHPhone").val();
        var strExt = $("#txtExtension").val();
        var strWp = $("#txtWPhone").val();
        var strWpExt = $("#txtWPExt").val();
        var strCell = $("#txtCell").val();

        var strSMS = getComboDataItem("cmbSMS");

        var strEmail = $("#txtEmail").val();
        var strLan = getComboDataItem("cmbLan");
        var strRace = getComboDataItem("cmbRace");
        var strEthinicity = getComboDataItem("cmbEthicity");

        var dataObj = {};
        dataObj.createdBy = sessionStorage.userId;
        dataObj.externalId1 = strExtId1;
        dataObj.externalId2 = strExtId2;
        dataObj.abbreviation = $("#txtAbbreviation").val();
        var txtType = $("#txtType").data("kendoComboBox");
        var strTType = "";
        if (txtType) {
            strTType = Number(txtType.value());
        }
        dataObj.type = strTType; //$("#txtType").val();;
        dataObj.prefix = strPrefix; //$("#txtEmail").val();;
        dataObj.suffix = strSuffix;
        dataObj.firstName = strFN;
        dataObj.middleName = strMN;
        dataObj.lastName = strLN;
        dataObj.nickname = strNickName;
        dataObj.billingAccountName = $("#txtBAN").val();
        dataObj.billingAccountId = Number(billActNo);
        var txtFAN = $("#txtFAN").data("kendoComboBox");
        var fId = "";
        if (txtFAN) {
            fId = txtFAN.value();
        }
        var txtUserId = $("#cmbUserId").data("kendoComboBox");
        var cmbUserId = "";
        if(txtUserId) {
        	cmbUserId = txtUserId.value();
        }
        dataObj.facilityId = Number(fId);
        dataObj.neicSpeciality = $("#txtNEIC").val();
        dataObj.amaSpeciality = $("#txtAS").val();
        dataObj.nuucType = $("#txtNT").val();;
        dataObj.upin = $("#txtUPIN").val();;
        dataObj.dea = $("#txtDEA").val();;
        dataObj.npi = $("#txtNPI").val();
        dataObj.taxonomyId = $("#txtTI").val();
        dataObj.pcp = $("#txtPCP").val();
        dataObj.billableDoctor = $("#txtBD").val();
        var finCharges = $("#txtFC").val() != "" ? $("#txtFC").val() : 0.0;
        dataObj.financeCharges = finCharges;
        if(strStatus == "InActive") {
        	dataObj.isActive = 0;
        }
        else if(strStatus == "Active") {
	        dataObj.isActive = 1;
	    }
        dataObj.userId = Number(cmbUserId);

        var comm = [];
        var comObj = {};
        //comObj.country = $("#txtCountry").val(); //"101";
        //comObj.city = $("#txtCity").val();
        if (strZip) {
            comObj.cityId = zipSelItem.cityId || zipSelItem.idk;
        }
        if(strStatus == "InActive") {
        	comObj.isActive = 0;
        }
        else if(strStatus == "Active") {
	        comObj.isActive = 1;
	    }
        /*comObj.countryId = cityId;*/
        comObj.isDeleted = 0;
        //comObj.zipFour = $("#txtZip4").val(); //"2323";
        comObj.sms = getComboDataItem("cmbSMS");
        //comObj.state = $("#txtState").val();
        comObj.workPhoneExt = $("#txtWExtension").val();
        comObj.email = $("#txtEmail").val();
        //comObj.zip = strZip;
        comObj.parentTypeId = 500;
        comObj.address2 = $("#txtAdd2").val();
        comObj.address1 = $("#txtAdd1").val();
        comObj.homePhone = $("#txtHPhone").val();
        /*comObj.stateId = 1;
        comObj.areaCode = "232";*/
        comObj.homePhoneExt = $("#txtExtension").val();
        comObj.workPhone = $("#txtWPhone").val();
        comObj.cellPhone = $("#txtCell").val();

        comObj.defaultCommunication = 1;
        /*if (operation == UPDATE) {}*/

        if (operation == UPDATE) {
            var communicationLen = selDocItem.communications != null ? selDocItem.communications.length : 0;
	        var cityIndexVal = "";
	        //var dupCityIndexVal = "";
	        for(var i=0; i<communicationLen; i++) {
	        	if(selDocItem.communications[i].parentTypeId == 500) {
	        		cityIndexVal = i;
	        	}
	        	/*if(selDocItem.communications[i].parentTypeId == 501) {
	        		dupCityIndexVal = i;
	        	}*/
	        }
	        if(cityIndexVal != "" || (cityIndexVal == 0 && selDocItem.communications != null)) {
	        	comObj.id = selDocItem.communications[cityIndexVal].id;
	        	comObj.modifiedBy = sessionStorage.userId;
            	comObj.parentId = selDocItem.idk;
	        }
	        /*if(dupCityIndexVal != "") {
        		comObj1.id = selDocItem.communications[dupCityIndexVal].id;
        		comObj1.modifiedBy = sessionStorage.userId;
            	comObj1.parentId = selDocItem.idk;
	        }*/
            //comm1.modifiedDate = new Date().getTime();
        }
        else {
        	comObj.createdBy = Number(sessionStorage.userId);
        }

        comm.push(comObj);
        dataObj.communications = comm;
        if(strStatus && strStatus != "" && strAbbr != "" && strFN != "" && strMN != "" && strLN != "" && strTType != "" && strAdd1 != "" && strZip != "" && strWp != "" && strCell != "" && strEmail != "" && $("#txtFAN").val() != "" && cmbUserId != "") {
        	if(!validateEmail(strEmail)) {
	        	$("body").animate({
				    scrollTop: 0
				}, 500);
		    	$('.customAlert').append('<div class="alert alert-danger">Please enter valid Email Address</div>');
	        }
	        else {
		        if (operation == ADD) {
		            var dataUrl = ipAddress + "/provider/create";
		            createAjaxObject(dataUrl, dataObj, "POST", onCreate, onError);
		        } else if (operation == UPDATE) {
		            dataObj.modifiedBy = sessionStorage.userId;
		            dataObj.id = selDocItem.idk;
		            dataObj.isDeleted = "0";
		            var dataUrl = ipAddress + "/provider/update";
		            createAjaxObject(dataUrl, dataObj, "POST", onUpdate, onError);
		        }
		    }
	    }
	    else {
	    	$("body").animate({
			    scrollTop: 0
			}, 500);
	    	$('.customAlert').append('<div class="alert alert-danger">Please fill all the Required Fields</div>');
	    }
    /*}*/
}
function onTypeChange() {

}
function onFilterChange() {
	var cmbFilterByName = $("#cmbFilterByName").data("kendoComboBox");
    if (cmbFilterByName && cmbFilterByName.selectedIndex < 0) {
        cmbFilterByName.select(0);
    }
    if($("#cmbFilterByName").val() == "id" || $("#cmbFilterByName").val() == "type" || $("#cmbFilterByName").val() == "user-id" || $("#cmbFilterByName").val() == "facility-id") {
    	$("#cmbFilterByValue").removeAttr("disabled");
    	$("#cmbFilterByValue").attr("type","number");
    }
    else if($("#cmbFilterByName").val() == "All") {
    	$("#cmbFilterByValue").attr("disabled","disabled");
    	$("#providerFilter-btn").attr("disabled","disabled");
    	$("#cmbFilterByValue").val('');
    	buildAccountListGrid([]);
		getAjaxObject(ipAddress+"/homecare/reports/","DELETE",getAccountList,onError);
		
    }
    else {
    	$("#cmbFilterByValue").removeAttr("disabled");
    	$("#cmbFilterByValue").attr("type","text");
    }
}

function onCreate(dataObj) {
    console.log(dataObj);
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            var obj = {};
            obj.status = "success";
            obj.operation = operation;
            popupClose(obj);
            // $('.customAlert').append('<div class="alert alert-success">'+dataObj.response.status.message+'</div>');
            // $('#btnReset').trigger('click');
            // $('.tabContentTitle').text('Add Provider');
        } else {
        	// $('.customAlert').append('<div class="alert alert-danger">'+dataObj.response.status.message+'</div>');
            customAlert.error("error", dataObj.response.status.message);
        }
    }
    var obj = {};
    obj.status = "success";
    obj.operation = operation;
    popupClose(obj);
}
function onUpdate(dataObj){
	console.log(dataObj);
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            /*var obj = {};
            obj.status = "success";
            obj.operation = operation;
            popupClose(obj);*/
            $('.customAlert').append('<div class="alert alert-success">'+dataObj.response.status.message+'</div>');
            $('.tabContentTitle').text('Update Provider');
            var id = $('#txtID').val();
            getAjaxObject(ipAddress+"/provider/list?id="+id,"GET",updateList,onError);
        } else {
        	//$('.customAlert').append('<div class="alert alert-danger">'+dataObj.response.status.message+'</div>');
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}
function updateList(dataObj) {
	console.log(dataObj);
	selDocItem = dataObj.response.provider[0];
	selDocItem.idk = selDocItem.id;
	addReportMaster("update");
}

function onError(errObj) {
    console.log(errObj);
    customAlert.error("Error", "Error");
}

function onClickCancel() {
    var obj = {};
    obj.status = "false";
    popupClose(obj);
}

function onClickSearch() {
    /*var obj = {};
    obj.status = "search";
    popupClose(obj);*/
}

function popupClose(obj) {
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}

function validateEmail(sEmail) {
    console.log(sEmail);
    var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    if (filter.test(sEmail)) {
    return true;
    }
    else {
    return false;
    }
}
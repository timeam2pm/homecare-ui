var parentRef = null;
var operation = "";
var selItem = null;
var ADD = "add";
var UPDATE = "update";
var VIEW = "view";
var DELETE = "delete";
var facilityId = "";
var statusArr = [{Key:'Active',Value:'1'},{Key:'InActive',Value:'2'}];

var patientInfoObject = null;
var commId = "";

$(document).ready(function(){
	parentRef = parent.frames['iframe'].window;
	var pnlHeight = window.innerHeight;
	var imgHeight = pnlHeight-100;
	$("#divTop").height(imgHeight);
//	setDataForSelection(statusArr, "cmbStatus", function(){}, ["Value", "Key"], 0, "");
	
});

function adjustHeight(){
	var defHeight = 45;
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 40;
    }
}

$(window).load(function(){
	if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
	init();
});

function init(){
	allowNumerics("txtID");
	allowAlphaNumeric("txtExtID1");
	allowAlphaNumeric("txtExtID2");
	allowAlphabets("txtAbbreviation");
	allowAlphabets("txtN");
	//allowAlphabets("txtDN");
	allowAlphabets("txtType");
	allowAlphabets("txtNUCC");
	allowAlphaNumeric("txtFTI");
	allowAlphaNumeric("txtFTEIN");
	allowAlphaNumeric("txtSTI");
	allowAlphaNumeric("txtNOTT");
	allowAlphabets("txtNPI");
	allowAlphaNumeric("txtTI");
	//allowAlphaNumeric("txtAdd1");
	//allowAlphaNumeric("txtAdd2");
	allowPhoneNumber("txtHPhone");
	allowPhoneNumber("txtExtension");
	allowPhoneNumber("txtWPhone");
	allowPhoneNumber("txtWExtension");
	allowPhoneNumber("txtCell");
	allowNumerics("txtFax");
	validateEmail("txtEmail");
	allowAlphabets("txtNN");
	allowAlphabets("txtFN");
	allowAlphabets("txtMN");
	allowAlphabets("txtLN");
	allowPhoneNumber("txtHPhone1");
	allowPhoneNumber("txtExtension1");
	allowPhoneNumber("txtCell1");
	validateEmail("txtEmail1");
	
	operation = parentRef.operation;
	if(operation == UPDATE){
		facilityId = parentRef.selItem.idk;
		console.log(parentRef.selItem);
		console.log(parentRef.operation);
	}
	
	//selItem = parentRef.selItem;
	$("#cmbBilling").kendoComboBox();
	$("#cmbPrefix").kendoComboBox();
	$("#cmbStatus").kendoComboBox();
	$("#cmbFTIT").kendoComboBox();
	$("#cmbZip1").kendoComboBox();
	getZip();
	buttonEvents();
}
function onStatusChange(){
	var cmbStatus = $("#cmbStatus").data("kendoComboBox");
	if(cmbStatus && cmbStatus.selectedIndex<0){
		cmbStatus.select(0);
	}
}
function buttonEvents(){
	$("#btnCancel").off("click",onClickCancel);
	$("#btnCancel").on("click",onClickCancel);
	
	$("#btnSave").off("click",onClickSave);
	$("#btnSave").on("click",onClickSave);
	
	$("#btnSearch").off("click",onClickSearch);
	$("#btnSearch").on("click",onClickSearch);
	
	$("#btnReset").off("click",onClickReset);
	$("#btnReset").on("click",onClickReset);
	
	$("#btnZipSearch").off("click");
	$("#btnZipSearch").on("click",onClickZipSearch);
	
}
function onClickZipSearch(){
	var popW = 600;
    var popH = 500;
    
    parentRef.searchZip = true;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search Zip";
    devModelWindowWrapper.openPageWindow("../../html/masters/zipList.html", profileLbl, popW, popH, true, onCloseSearchZipAction);
}
var zipSelItem = null;
function onCloseSearchZipAction(evt,returnData){
	console.log(returnData);
	if(returnData && returnData.status == "success"){
		//console.log();
		$("#cmbZip").val("");
		$("#txtZip4").val("");
		$("#txtState").val("");
		$("#txtCountry").val("");
		$("#txtCity").val("");
		var selItem = returnData.selItem;
		if(selItem){
			zipSelItem = selItem;
			if(zipSelItem){
				cityId = zipSelItem.idk;
				if(zipSelItem.zip){
					$("#cmbZip").val(zipSelItem.zip);
				}
				if(zipSelItem.zipFour){
					$("#txtZip4").val(zipSelItem.zipFour);
				}
				if(zipSelItem.state){
					$("#txtState").val(zipSelItem.state);
				}
				if(zipSelItem.country){
					$("#txtCountry").val(zipSelItem.country);
				}
				if(zipSelItem.city){
					$("#txtCity").val(zipSelItem.city);
				}
			}
		}
	}
}
function getZip(){
	getAjaxObject(ipAddress+"/city/list/","GET",getZipList,onError);
}
function getZipList(dataObj){
	//var dArray = getTableListArray(dataObj);
	var dArray = [];
	if(dataObj && dataObj.response && dataObj.response.cityExt){
		if($.isArray(dataObj.response.cityext)){
			dArray = dataObj.response.cityext;
		}else{
			dArray.push(dataObj.response.cityext);
		}
	}
	if(dArray && dArray.length>0){
		//setDataForSelection(dArray, "cmbZip", onZipChange, ["zip", "id"], 0, "");
		onZipChange();
	}
	getPrefix();
	
}

function getPrefix(){
	getAjaxObject(ipAddress+"/Prefix/list/","GET",getCodeTableValueList,onError);
}
function onComboChange(cmbId){
	var cmb = $("#"+cmbId).data("kendoComboBox");
	if(cmb && cmb.selectedIndex<0){
		cmb.select(0);
	}
}
function onPrefixChange(){
	onComboChange("cmbPrefix");
}
function onPtChange(){
	onComboChange("cmbStatus");
}
function onGenderChange(){
	onComboChange("cmbGender");
}
function onSMSChange(){
	onComboChange("cmbSMS");
}
function onLanChange(){
	onComboChange("cmbLan");
}
function onRaceChange(){
	onComboChange("cmbRace");
}
function onEthnicityChange(){
	onComboChange("cmbEthicity");
}
function onZipChange(){
	onComboChange("cmbZip");
	var cmbZip = $("#cmbZip").data("kendoComboBox");
	if(cmbZip){
		var dataItem = cmbZip.dataItem();
		if(dataItem){
			$("#txtZip4").val(dataItem.zipFour);
			$("#txtCountry").val(dataItem.country);
			$("#txtState").val(dataItem.state);
			$("#txtCity").val(dataItem.city);
		}
	}
}
function getCodeTableValueList(dataObj){
	var dArray = getTableListArray(dataObj);
	if(dArray && dArray.length>0){
		setDataForSelection(dArray, "cmbPrefix", onPrefixChange, ["value", "idk"], 0, "");
	}
	getAjaxObject(ipAddress+"/Patient_Status/list/","GET",getPatientStatusValueList,onError);
}

function getPatientStatusValueList(dataObj){
	var dArray = getTableListArray(dataObj);
	if(dArray && dArray.length>0){
		setDataForSelection(dArray, "cmbStatus", onPtChange, ["desc", "idk"], 0, "");
	}
	getAjaxObject(ipAddress+"/Gender/list/","GET",getGenderValueList,onError);
}
function getGenderValueList(dataObj){
	var dArray = getTableListArray(dataObj);
	if(dArray && dArray.length>0){
		setDataForSelection(dArray, "cmbGender", onGenderChange, ["desc", "idk"], 0, "");
	}
	getAjaxObject(ipAddress+"/SMS/list/","GET",getSMSValueList,onError);
}
function getSMSValueList(dataObj){
	var dArray = getTableListArray(dataObj);
	if(dArray && dArray.length>0){
		setDataForSelection(dArray, "cmbSMS", onSMSChange, ["desc", "idk"], 0, "");
	}
	getAjaxObject(ipAddress+"/Language/list/","GET",getLanguageValueList,onError);
}
function getLanguageValueList(dataObj){
	var dArray = getTableListArray(dataObj);
	if(dArray && dArray.length>0){
		setDataForSelection(dArray, "cmbLan", onLanChange, ["desc", "idk"], 0, "");
	}
	getAjaxObject(ipAddress+"/Race/list/","GET",getRaceValueList,onError);
}
function getRaceValueList(dataObj){
	var dArray = getTableListArray(dataObj);
	if(dArray && dArray.length>0){
		setDataForSelection(dArray, "cmbRace", onRaceChange, ["desc", "idk"], 0, "");
	}
	getAjaxObject(ipAddress+"/Ethnicity/list/","GET",getEthnicityValueList,onError);
}
function getEthnicityValueList(dataObj){
	var dArray = getTableListArray(dataObj);
	if(dArray && dArray.length>0){
		setDataForSelection(dArray, "cmbEthicity", onEthnicityChange, ["desc", "idk"], 0, "");
	}
	getAjaxObject(ipAddress+"/billing-account/list/","GET",getAccountList,onError);
}
function getAccountList(dataObj){
	var dataArray = [];
	if(dataObj && dataObj.response.status && dataObj.response.status.code == "1"){
		if($.isArray(dataObj.response.billingAccount)){
			dataArray = dataObj.response.billingAccount;
		}else{
			dataArray.push(dataObj.response.billingAccount);
		}
	}
	var tempDataArry = [];
	for(var i=0;i<dataArray.length;i++){
		dataArray[i].idk = dataArray[i].id; 
		dataArray[i].Status = "InActive";
		if(dataArray[i].isActive == 1){
			dataArray[i].Status = "Active";
		}
	}
	setDataForSelection(dataArray, "cmbBilling", onBillAccountChange, ["name", "idk"], 0, "");
	
	if(operation == UPDATE && facilityId != ""){
		getFacilityInfo();
	}
}
function onBillAccountChange(){
	var cmbBilling = $("#cmbBilling").data("kendoComboBox");
	if(cmbBilling && cmbBilling.selectedIndex<0){
		cmbBilling.select(0);
	}
}
function getFacilityInfo(){
	getAjaxObject(ipAddress+"/facility/list/?id="+facilityId,"GET",onGetFacilityInfo,onError);
}
function getComboListIndex(cmbId,attr,attrVal){
	var cmb = $("#"+cmbId).data("kendoComboBox");
	if(cmb){
		var ds = cmb.dataSource;
		var totalRec = ds.total();
		for(var i=0;i<totalRec;i++){
			var dtItem = ds.at(i);
			if(dtItem && dtItem[attr] == attrVal){
				cmb.select(i);
				return i;
			}
		}
	}
	return -1;
}
function onGetFacilityInfo(dataObj){
	patientInfoObject = dataObj;
	if(dataObj && dataObj.response && dataObj.response.facility[0]){
		$("#txtID").val(dataObj.response.facility[0].id);
		$("#txtExtID1").val(dataObj.response.facility[0].externalId1);
		$("#txtExtID2").val(dataObj.response.facility[0].externalId2);
		$("#txtAbbreviation").val(dataObj.response.facility[0].abbreviation);
		$("#txtN").val(dataObj.response.facility[0].name);
		$("#txtDN").val(dataObj.response.facility[0].displayName);
		$("#txtFTI").val(dataObj.response.facility[0].federalTaxId);
		$("#txtFTEIN").val(dataObj.response.facility[0].federalTaxEin);
		$("#txtSTI").val(dataObj.response.facility[0].stateTaxId);
		$("#txtNOTT").val(dataObj.response.facility[0].taxName);
		$("#txtNPI").val(dataObj.response.facility[0].npi);
		$("#txtTI").val(dataObj.response.facility[0].taxonomyId);
		
		getComboListIndex("cmbPrefix","value",dataObj.response.facility[0].contactPrefix);
		$("#txtFN").val(dataObj.response.facility[0].firstName);
		$("#txtLN").val(dataObj.response.facility[0].lastName);
		$("#txtMN").val(dataObj.response.facility[0].middleName);
		$("#txtWeight").val(dataObj.response.facility[0].weight);
		$("#txtHeight").val(dataObj.response.facility[0].height);
		$("#txtNN").val(dataObj.response.facility[0].nickname);
		/*var dt = new Date(dataObj.response.facility.dateOfBirth);
		if(dt){
			var strDT = kendo.toString(dt,"MM/dd/yyyy");
			var dtDOB = $("#dtDOB").data("kendoDatePicker");
			if(dtDOB){
				dtDOB.value(strDT);
			}
		}
		$("#txtSSN").val(dataObj.response.facility.ssn);*/
		
		getComboListIndex("cmbStatus","idk",dataObj.response.facility[0].isActive);
		getComboListIndex("cmbBilling","idk",dataObj.response.facility[0].billingAccountId);
		
		//getComboListIndex("cmbGender","desc",dataObj.response.facility.gender);
		//getComboListIndex("cmbEthicity","desc",dataObj.response.facility.ethnicity);
		//getComboListIndex("cmbRace","desc",dataObj.response.facility.race);
		//getComboListIndex("cmbLan","desc",dataObj.response.facility.language);
		
		if(dataObj && dataObj.response && dataObj.response.facility[0].communications){
			
			var commArray = [];
			if($.isArray(dataObj.response.facility[0].communications)){
				commArray = dataObj.response.facility[0].communications;
			}else{
				commArray.push(dataObj.response.facility[0].communications);
			}
			var comObj = commArray[0];
			commId = comObj.id;
			cityId = comObj.cityId;
			$("#txtCity").val(comObj.city);
			$("#txtState").val(comObj.state);
			$("#txtCountry").val(comObj.country);
			$("#cmbZip").val(comObj.zip);
			$("#txtZip4").val(comObj.zipFour);
			$("#txtAdd1").val(comObj.address1);
			$("#txtAdd2").val(comObj.address2);
		//	$("#txtSMS").val(comObj.sms);
			getComboListIndex("cmbSMS","desc",comObj.sms);
			getComboListIndex("cmbZip","idk",comObj.cityId);
			onZipChange();
			$("#txtCell").val(comObj.cellPhone);
			$("#txtWPExt").val(comObj.workPhoneExt);
			$("#txtWP").val(comObj.workPhone);
			$("#txtExtension").val(comObj.homePhoneExt);
			$("#txtHPhone").val(comObj.homePhone);
			$("#txtEmail").val(comObj.email);
			
			if(comObj.defaultCommunication == "1"){
				$("#rdHome").prop("checked",true);
			}else if(comObj.defaultCommunication == "2"){
				$("#rdWork").prop("checked",true);
			}else if(comObj.defaultCommunication == "3"){
				$("#rdCell").prop("checked",true);
			}else if(comObj.defaultCommunication == "4"){
				$("#rdEmail").prop("checked",true);
			}
		}
		
	}
}
function getTableListArray(dataObj){
	var dataArray = [];
	if(dataObj && dataObj.response && dataObj.response.codeTable){
		if($.isArray(dataObj.response.codeTable)){
			dataArray = dataObj.response.codeTable;
		}else{
			dataArray.push(dataObj.response.codeTable);
		}
	}
	var tempDataArry = [];
	var obj = {};
	obj.desc = "";
	obj.zip = "";
	obj.value = "";
	obj.idk = "";
	tempDataArry.push(obj);
	for(var i=0;i<dataArray.length;i++){
		if(dataArray[i].isActive == 1){
			var obj = dataArray[i];
			obj.idk = dataArray[i].id;
			obj.status = dataArray[i].Status;
			tempDataArry.push(obj);
		}
	}
	return tempDataArry;
}
function onClickReset(){
	operation = ADD;
	facilityId = "";
	$("#txtID").val("");
	$("#txtExtID1").val("");
	$("#txtExtID2").val("");
	setComboReset("cmbPrefix");
	$("#txtFN").val("");
	$("#txtLN").val("");
	$("#txtMN").val("");
	$("#txtN").val("");
	$("#txtNUCC").val("");
	$("#txtFTIT").val("");
	$("#txtFTI").val("");
	$("#txtFTEIN").val("");
	$("#txtSTI").val("");
	$("#txtNOTT").val("");
	$("#txtNPI").val("");
	$("#txtTI").val("");
	$("#txtDN1").val("");
	/*$("#txtCity").val("");
	$("#txtState").val("");
	$("#txtCountry").val("");
	$("#txtZip4").val("");*/
	$("#txtNN").val("");
	$("#txtDN").val("");
		/*var dtDOB = $("#txtNOTT").data("kendoDatePicker");
		if(dtDOB){
			dtDOB.value("");
		}*/
	//$("#txtSSN").val("");
	setComboReset("cmbStatus");
	//setComboReset("cmbAbbreviation");
	setComboReset("cmbType");
	setComboReset("cmbZip");
	setComboReset("cmbLan");
		$("#txtAdd1").val("");
		$("#txtAdd2").val("");
		setComboReset("cmbSMS");
		$("#txtCell").val("");
		$("#txtType").val("");
		$("#txtAbbreviation").val("");

		$("#txtWExtension").val("");
		$("#txtWPhone").val("");
		$("#txtExtension").val("");
		$("#txtHPhone").val("");
		$("#txtEmail").val("");
		$("#txtFax").val("");
		$("#txtHPhone1").val("");
		$("#txtExtension1").val("");
		$("#txtCell1").val("");
		$("#txtEmail1").val("");
		//$("#txtCell1").val("");
		
		$("#rdHome").prop("checked",true);

}
function setComboReset(cmbId){
	var cmb = $("#"+cmbId).data("kendoComboBox");
	if(cmb){
		cmb.select(0);
	}
}

function validation(){
var flag = true;		
/*var strAbbr = $("#txtAbbrevation").val();
	strAbbr = $.trim(strAbbr);
	if(strAbbr == ""){
		customAlert.error("Error","Enter Abbrevation");
		flag = false;
		return false;
	}
	var strCode = $("#txtCode").val();
	strCode = $.trim(strCode);
	if(strCode == ""){
		customAlert.error("Error","Enter Code");
		flag = false;
		return false;
	}
	var strName = $("#txtName").val();
	strName = $.trim(strName);
	if(strName == ""){
		customAlert.error("Error","Enter Country");
		flag = false;
		return false;
	}*/
	
	return flag;
}

function getComboDataItem(cmbId){
	var dItem = null;
	var cmb = $("#"+cmbId).data("kendoComboBox");
	if(cmb && cmb.dataItem()){
		dItem =  cmb.dataItem();
		return cmb.text();
	}
	return "";
}

function getComboDataItemValue(cmbId){
	var dItem = null;
	var cmb = $("#"+cmbId).data("kendoComboBox");
	if(cmb && cmb.dataItem()){
		dItem =  cmb.dataItem();
		return cmb.value();
	}
	return "";
}
var cityId = "";
function onClickSave(){
	var sEmail = $('#txtEmail').val();
	/*if (validateEmail(sEmail)) {
		}else {
				customAlert.error("Error","your EmailId is invalid,Please enter the valid EmailId");
				return;
		}*/
	if(validation()){
		var strId = $("#txtID").val();
		var strExtId1 = $("#txtExtID1").val();
		var strExtId2 = $("#txtExtID2").val();
		var strAbbreviation = $("#txtAbbreviation").val();//getComboDataItem("txtAbbreviation");
		var strPrefix = getComboDataItem("cmbPrefix");
		var strNickName = $("#txtN").val();
		var strStatus = getComboDataItem("cmbStatus");
		var strFN = $("#txtFN").val();
		var strMN = $("#txtMN").val();
		var strLN = $("#txtLN").val();
		var strName = $("#txtN").val();
		var strDisplayName = $("#txtDN").val();
		
		var strNUCC = $("#txtNUCC").val();
		var strType = getComboDataItem("cmbType");
		var strAdd1 = $("#txtAdd1").val();
		var strAdd2 = $("#txtAdd2").val();
		var strCity = $("#txtCity").val();
		var strState = $("#txtState").val();
		var strCountry = $("#txtCountry").val();
		var strFederalTaxIDType = $("#txtFTIT").val();
		var strFederalTaxID = $("#txtFTI").val();
		var strFederalTaxEIN = $("#txtFTEIN").val();
		var strStateTaxID = $("#txtSTI").val();
		var strNameOfTheTax = $("#txtNOTT").val();
		var strNPI = $("#txtNPI").val();
		var strToxonomyID = $("#txtTI").val();
		var strDisplayName = $("#txtDN1").val();
		var strFax = $("#txtFax").val();
		var strHomephone = $("#txtHPhone1").val();
		var strHExtension1 = $("#txtExtension1").val();
		var strCell1 = $("#txtCell1").val();
		var strEmail1 = $("#txtEm1").val();
		
		var strZip = getComboDataItemValue("cmbZip");
		var strZip4 = $("#txtZip4").val();
		var strHPhone = $("#txtHPhone").val();
		var strExt = $("#txtExtension").val();
		var strWp = $("#txtWP").val();
		var strWpExt = $("#txtWPExt").val();
		var strCell = $("#txtCell").val();
		
		//var strSMS = getComboDataItem("cmbSMS");
		
		var strEmail = $("#txtEmail").val();
		var strLan = getComboDataItem("cmbLan");
		var strRace = getComboDataItem("cmbRace");
		var strEthinicity = getComboDataItem("cmbEthicity");
		
		var dataObj = {};
		dataObj.createdBy = 101;
		dataObj.isActive = 1;
		dataObj.isDeleted =  0;
		dataObj.externalId1 = strExtId1;
		dataObj.externalId2 = strExtId2;
		dataObj.abbreviation = strAbbreviation;
		dataObj.name = strNickName;//"name8";
		dataObj.displayName = $("#txtDN").val();
		dataObj.federalTaxIdType = 1;
		dataObj.federalTaxId = $("#txtFTI").val();//"10101";
		dataObj.federalTaxEin = $("#txtFTEIN").val();//"federalTaxEin";
		dataObj.stateTaxId = $("#txtSTI").val();//"1012";
		dataObj.taxName = $("#txtNOTT").val();//"tax name";
		dataObj.npi= $("#txtNPI").val();//"npi";
		dataObj.taxonomyId= $("#txtTI").val();//"npi";
		dataObj.contactPrefix = 43;//strPrefix;//"1";
		//dataObj.prefix = strPrefix;
		dataObj.contactFirstName = strFN;
		dataObj.contactMiddleName = strMN;
		dataObj.contactLastName = strLN;
		dataObj.contactNickName = strNickName;
		
		var cmbBilling = $("#cmbBilling").data("kendoComboBox");
		
		var billIdk = cmbBilling.value();
		billIdk = Number(billIdk);
		dataObj.billingAccountId = billIdk;
		
		    	  
		var comm = [];
		var comObj = {};
		comObj.defaultCommunication = 1;
		comObj.cityId = cityId;
		comObj.isActive = 1;
		comObj.isDeleted = 0;
		comObj.sms = "yes";
		comObj.fax = $("#txtFax").val();
		comObj.workPhone = $("#txtWPhone").val();
		comObj.workPhoneExt = $("#txtWExtension").val();
		comObj.email = $("#txtEmail").val();
		comObj.parentTypeId = 400;
		comObj.address2 = $("#txtAdd2").val();
		comObj.address1 = $("#txtAdd1").val();
		comObj.homePhone = $("#txtHPhone").val();
		comObj.homePhoneExt = $("#txtExtension").val();
		//comObj.stateId = 1;
		//comObj.areaCode = "232";
		comObj.cellPhone = $("#txtCell").val();
		comObj.createdBy = 101;
		
		
		if(operation == UPDATE){
		}
		comm.push(comObj);
		dataObj.communications = comm;
		console.log(dataObj);
		
		if(operation == ADD){
				var dataUrl = ipAddress+"/facility/create";
				createAjaxObject(dataUrl,dataObj,"POST",onCreate,onError);
			}else if(operation == UPDATE){
				dataObj.modifiedBy = 101;
				dataObj.id = facilityId;
				//dataObj.facility.isDeleted = "0";
				var dataUrl = ipAddress+"/facility/update";
				createAjaxObject(dataUrl,dataObj,"POST",onCreate,onError);
			}
	}
}

function onCreate(dataObj){
	console.log(dataObj);
	if(dataObj && dataObj.response && dataObj.response.status){
		if(dataObj.response.status.code == "1"){
			/*if(operation == ADD){
				customAlert.info("info", "Facility Created Successfully");
			}else{
				customAlert.info("info", "Facility Updated Successfully");
			}*/
			var obj = {};
			obj.status = "success";
			obj.operation = operation;
			popupClose(obj);
			
		}else{
			customAlert.error("error", dataObj.response.status.message);
		}
	}
	/*var obj = {};
	obj.status = "success";
	obj.operation = operation;
	popupClose(obj);*/
}

function onError(errObj){
	console.log(errObj);
	customAlert.error("Error","Error");
}
function onClickCancel(){
	var obj = {};
	obj.status = "false";
	popupClose(obj);
}
function onClickSearch(){
	var obj = {};
	obj.status = "search";
	popupClose(obj);
}
function popupClose(obj){
	var windowWrapper = new kendoWindowWrapper();
	windowWrapper.closePageWindow(obj);
}



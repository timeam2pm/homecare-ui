var angularUIgridWrapper;
var parentRef = null;
var operation = "";
var selItem = null;
var ADD = "add";
var UPDATE = "edit";
var VIEW = "view";
var DELETE = "delete";
var patientId = "";
var radioValue = "";
var statusArr = [{ Key: 'Active', Value: 'Active' }, { Key: 'InActive', Value: 'InActive' }];

var typeArr = [{Key: '', Value: '' }, { Key: '100', Value: 'Doctor' }, { Key: '200', Value: 'Nurse' }, { Key: '201', Value: 'Caregiver' }];
//var filterArr = [{ Key: 'last-name', Value: 'Last Name' },{ Key: 'All', Value: 'All' }, { Key: 'id', Value: 'Staff ID' }, { Key: 'abbr', Value: 'Abbreviation' }, { Key: 'first-name', Value: 'First Name' }, { Key: 'middle-name', Value: 'Middle Name' },  { Key: 'type', Value: 'Type' }, { Key: 'user-id', Value: 'User Id' }, { Key: 'facility-id', Value: 'Facility Id' }];
var filterArr = [{ Key: 'last-name', Value: 'Last Name' },{ Key: 'All', Value: 'All' }, { Key: 'id', Value: 'Id' }, { Key: 'first-name', Value: 'First Name' },{ Key: '5', Value: 'Last Name, First Name' },{ Key: 'type', Value: 'Type' }];

var patientInfoObject = null;
var commId = "";
var IsPostalCodeManual = sessionStorage.IsPostalCodeManual;
var angularLeaveUIgridWrapper;
var cntry = sessionStorage.countryName;
var leaveStatus = [];
var appointmentdtFMT = "dd/MM/yyyy hh:mm tt";
var dtFMT = "dd/MM/yyyy";
var angularPTUIgridWrapper = null;
var angularPTUIgridWrapper1 = null;
var appTypeArr = [{Key:'Notes1',Value:'Notes1'},{Key:'Notes2',Value:'Notes2'},{Key:'Notes3',Value:'Notes3'}];

var typeArr = [{ Key: '100', Value: 'Doctor' }, { Key: '200', Value: 'Nurse' }, { Key: '201', Value: 'Caregiver' }];

var empArr;

var towArr = [{Key:'0',Value:'Sun'},{Key:'1',Value:'Mon'},{Key:'2',Value:'Tue'},{Key:'3',Value:'Wed'},{Key:'4',Value:'Thu'},{Key:'5',Value:'Fri'},{Key:'6',Value:'Sat'}];

var prsViewArray = [];
var prsViewArray = [{idk:'1',h:'0',m:'0',tm:'12:00 AM'},{idk:'2',h:'0',m:'15',tm:''},{idk:'3',h:'0',m:'30',tm:''},{idk:'4',h:'0',m:'45',tm:''},{idk:'5',h:'1',m:'0',tm:'01:00 AM'}];
prsViewArray.push({idk:'6',h:'1',m:'15',tm:''});
prsViewArray.push({idk:'7',h:'1',m:'30',tm:''});
prsViewArray.push({idk:'8',h:'1',m:'45',tm:''});
prsViewArray.push({idk:'9',h:'2',m:'0',tm:'02:00 AM'});
prsViewArray.push({idk:'10',h:'2',m:'15',tm:''});
prsViewArray.push({idk:'11',h:'2',m:'30',tm:''});
prsViewArray.push({idk:'11',h:'2',m:'45',tm:''});
prsViewArray.push({idk:'12',h:'3',m:'0',tm:'03:00 AM'})
prsViewArray.push({idk:'13',h:'3',m:'15',tm:''});
prsViewArray.push({idk:'14',h:'3',m:'30',tm:''});
prsViewArray.push({idk:'15',h:'3',m:'45',tm:''})
prsViewArray.push({idk:'16',h:'4',m:'0',tm:'04:00 AM'});
prsViewArray.push({idk:'17',h:'4',m:'15',tm:''});
prsViewArray.push({idk:'18',h:'4',m:'30',tm:''});
prsViewArray.push({idk:'19',h:'4',m:'45',tm:''});
prsViewArray.push({idk:'20',h:'5',m:'0',tm:'05:00 AM'});
prsViewArray.push({idk:'21',h:'5',m:'15',tm:''});
prsViewArray.push({idk:'22',h:'5',m:'30',tm:''});
prsViewArray.push({idk:'23',h:'5',m:'45',tm:''});
prsViewArray.push({idk:'24',h:'6',m:'0',tm:'06:00 AM'});
prsViewArray.push({idk:'25',h:'6',m:'15',tm:''});
prsViewArray.push({idk:'26',h:'6',m:'30',tm:''});
prsViewArray.push({idk:'27',h:'6',m:'45',tm:''});
prsViewArray.push({idk:'28',h:'7',m:'0',tm:'07:00 AM'});
prsViewArray.push({idk:'29',h:'7',m:'15',tm:''});
prsViewArray.push({idk:'30',h:'7',m:'30',tm:''});
prsViewArray.push({idk:'31',h:'7',m:'45',tm:''});
prsViewArray.push({idk:'32',h:'8',m:'0',tm:'08:00 AM'})
prsViewArray.push({idk:'33',h:'8',m:'15',tm:''});
prsViewArray.push({idk:'34',h:'8',m:'30',tm:''});
prsViewArray.push({idk:'35',h:'8',m:'45',tm:''});
prsViewArray.push({idk:'36',h:'9',m:'0',tm:'09:00 AM'});
prsViewArray.push({idk:'37',h:'9',m:'15',tm:''});
prsViewArray.push({idk:'38',h:'9',m:'30',tm:''});
prsViewArray.push({idk:'39',h:'9',m:'45',tm:''});
prsViewArray.push({idk:'40',h:'10',m:'0',tm:'10:00 AM'});
prsViewArray.push({idk:'41',h:'10',m:'15',tm:''});
prsViewArray.push({idk:'42',h:'10',m:'30',tm:''});
prsViewArray.push({idk:'43',h:'10',m:'45',tm:''});
prsViewArray.push({idk:'44',h:'11',m:'0',tm:'11:00 AM'});
prsViewArray.push({idk:'45',h:'11',m:'15',tm:''});
prsViewArray.push({idk:'46',h:'11',m:'30',tm:''});
prsViewArray.push({idk:'47',h:'11',m:'45',tm:''});
prsViewArray.push({idk:'48',h:'12',m:'0',tm:'12:00 PM'});
prsViewArray.push({idk:'49',h:'12',m:'15',tm:''})
prsViewArray.push({idk:'50',h:'12',m:'30',tm:''});
prsViewArray.push({idk:'51',h:'12',m:'45',tm:''});
prsViewArray.push({idk:'52',h:'13',m:'0',tm:'01:00 PM'});
prsViewArray.push({idk:'53',h:'13',m:'15',tm:''});
prsViewArray.push({idk:'54',h:'13',m:'30',tm:''});
prsViewArray.push({idk:'55',h:'13',m:'45',tm:''});
prsViewArray.push({idk:'56',h:'14',m:'0',tm:'02:00 PM'});
prsViewArray.push({idk:'57',h:'14',m:'15',tm:''});
prsViewArray.push({idk:'58',h:'14',m:'30',tm:''});
prsViewArray.push({idk:'59',h:'14',m:'45',tm:''});
prsViewArray.push({idk:'60',h:'15',m:'0',tm:'03:00 PM'});
prsViewArray.push({idk:'61',h:'15',m:'15',tm:''});
prsViewArray.push({idk:'62',h:'15',m:'30',tm:''});
prsViewArray.push({idk:'63',h:'15',m:'45',tm:''});
prsViewArray.push({idk:'64',h:'16',m:'0',tm:'04:00 PM'});
prsViewArray.push({idk:'65',h:'16',m:'15',tm:''});
prsViewArray.push({idk:'66',h:'16',m:'30',tm:''});
prsViewArray.push({idk:'67',h:'16',m:'45',tm:''});
prsViewArray.push({idk:'68',h:'17',m:'0',tm:'05:00 PM'});
prsViewArray.push({idk:'69',h:'17',m:'15',tm:''});
prsViewArray.push({idk:'70',h:'17',m:'30',tm:''});
prsViewArray.push({idk:'71',h:'17',m:'45',tm:''});
prsViewArray.push({idk:'72',h:'18',m:'0',tm:'06:00 PM'});
prsViewArray.push({idk:'73',h:'18',m:'15',tm:''});
prsViewArray.push({idk:'74',h:'18',m:'30',tm:''});
prsViewArray.push({idk:'75',h:'18',m:'45',tm:''});
prsViewArray.push({idk:'76',h:'19',m:'0',tm:'07:00 PM'});
prsViewArray.push({idk:'77',h:'19',m:'15',tm:''});
prsViewArray.push({idk:'78',h:'19',m:'30',tm:''});
prsViewArray.push({idk:'79',h:'19',m:'45',tm:''});
prsViewArray.push({idk:'80',h:'20',m:'0',tm:'08:00 PM'});
prsViewArray.push({idk:'81',h:'20',m:'15',tm:''});
prsViewArray.push({idk:'82',h:'20',m:'30',tm:''});
prsViewArray.push({idk:'83',h:'20',m:'45',tm:''});
prsViewArray.push({idk:'84',h:'21',m:'0',tm:'09:00 PM'});
prsViewArray.push({idk:'85',h:'21',m:'15',tm:''});
prsViewArray.push({idk:'86',h:'21',m:'30',tm:''});
prsViewArray.push({idk:'87',h:'21',m:'45',tm:''});
prsViewArray.push({idk:'88',h:'22',m:'0',tm:'10:00 PM'});
prsViewArray.push({idk:'89',h:'22',m:'15',tm:''});
prsViewArray.push({idk:'90',h:'22',m:'30',tm:''});
prsViewArray.push({idk:'91',h:'22',m:'45',tm:''});
prsViewArray.push({idk:'92',h:'23',m:'0',tm:'11:00 PM'});
prsViewArray.push({idk:'93',h:'23',m:'15',tm:''});
prsViewArray.push({idk:'94',h:'23',m:'30',tm:''});
prsViewArray.push({idk:'95',h:'23',m:'45',tm:''});
var careerTotal;

var qualIncrement=0;
$(document).ready(function(){
    $("#divMain",parent.document).css("display","none");
    $("#divPatInfo",parent.document).css("display","");
    // $("#lblTitleName",parent.document).html("Staff");
    //$(".ibox-title").css("padding-bottom","5px");
    $(".ibox-content-inner").css("padding-top","8px");

    var cntry = sessionStorage.countryName;
    if (cntry.indexOf("India") >= 0) {
        $("#lblState").html("State");
    } else if (cntry.indexOf("United Kingdom") >= 0) {
        $("#lblState").html("County");
    } else {
        $("#lblState").html("State");
    }

    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("dgridDAccountList", dataOptions);
    angularUIgridWrapper.init();
    buildAccountListGrid([]);
    getCountryZoneName();

    var dataLeaveOptions = {
        pagination: false,
        changeCallBack: onLeaveChange
    }
    angularLeaveUIgridWrapper = new AngularUIGridWrapper("dgridLeaveRequestList", dataLeaveOptions);
    angularLeaveUIgridWrapper.init();
    buildDeviceListGrid([]);

    var dataOptionsPT = {pagination: false,paginationPageSize: 500,changeCallBack: onPTChange};
    angularPTUIgridWrapper = new AngularUIGridWrapper("rPrGrid1", dataOptionsPT);
    angularPTUIgridWrapper.init();
    buildPatientRosterList([]);

    var dataOptionsPT1 = {pagination: false,paginationPageSize: 500,changeCallBack: onPTChange1};
    angularPTUIgridWrapper1 = new AngularUIGridWrapper("rPrGrid2", dataOptionsPT1);
    angularPTUIgridWrapper1.init();
    buildPatientRosterList1([]);

    adjustHeight();

    $('table.contact-tbl.staff-tbl').on('click','.qualAddRemoveLink.addQual', function() {
        // $('.qualAddRemoveLink.addQual').on('click',function() {
        // if(qualIncrement == 0) {
        //     qualIncrement = 1;
        qualIncrement = $('.qualificationTabWrapper table tbody tr').length;
        var $trLen = $('.qualificationTabWrapper table tbody tr').length;
        // if(qualIncrement == ($trLen +1)){

        $('.qualificationTabWrapper table tbody').append('<tr class="qualWrapper_li qualificationTabWrapper-li-' + $trLen + '"><td class="qual-td-addRemove"><div class="tbl-plusminus"><a href="#" class="qualAddRemoveLink addQual btn-sm-plus">+</a> <a href="#" class="qualAddRemoveLink removeQual btn-sm-minus">-</a></div></td><td><div class="tbl-inpdiv"><div class="col-xs-12"><input type="text" class="form-control" name="qualWrap-sno" value="' + $trLen + '" readonly /></div></div></td><td><div class="tbl-inpdiv"><div class="col-xs-12"><input class="form-control" type="text" name="qualWrap-qualification" id="qualWrapQualification" maxlength="100" value="" /></div></div></td><td><div class="tbl-inpdiv"><div class="col-xs-12"><input type="text" name="qualWrap-university" class="form-control" id="qualWrapUniversity" maxlength="100" value="" /></div></div></td><td><div class="tbl-inpdiv"><div class="col-xs-12"><input type="text" name="qualWrap-percentage" class="form-control" id="qualWrapPercentage" value="" /></div></div></td><td><input type="text" name="qualWrap-passedout" id="qualWrapPassedout" value="" style="width: auto !important; padding-left: 0 !important;" /></td><td><input type="text" name="qualWrap-expirydate" value="" style="width: auto !important; padding-left: 0 !important;" /></td></tr>');
        allowOnlyDecimals($('[name="qualWrap-percentage"]'));
        $('.qualificationTabWrapper-li-' + $trLen).find('[name="qualWrap-passedout"]').kendoDatePicker(
            {format: "dd-MM-yyyy"});
        $('.qualificationTabWrapper-li-' + $trLen).find('[name="qualWrap-expirydate"]').kendoDatePicker({format: "dd-MM-yyyy"});

        startDT = $('.qualificationTabWrapper-li-' + $trLen).find('[name="qualWrap-passedout"]').kendoDatePicker({
            change: startChange,format:"dd-MM-yyyy"
        }).data("kendoDatePicker");

        endDT= $('.qualificationTabWrapper-li-' + $trLen).find('[name="qualWrap-expirydate"]').kendoDatePicker({
            format:"dd-MM-yyyy"
        }).data("kendoDatePicker");
        modifySerialNumber();
        // }
        // }
        // break;
    });

    $(".btnActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('active');
        onClickActive();
    });
    $(".btnInActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('inactive');
        onClickInActive();
    });
});


$(window).load(function(){
    parentRef = parent.frames['iframe'].window;
    loading = false;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
    init();
    buttonEvents();
    adjustHeight();
    // $('.btnActive').trigger('click');
}

function init(){
    $("#cmbNotes").kendoComboBox();
    setDataForSelection(appTypeArr, "cmbNotes", onNotesChange, ["Value", "Key"], 0, "");

    $("#txtStartTime").kendoTimePicker({interval: 15});
    $("#txtEndTime").kendoTimePicker({interval: 15});
    $("#txtDateOfWeek").kendoDatePicker({ format: dtFMT, value: new Date() });
    if(sessionStorage.userTypeId == "700"){
        $("#divApprove").hide();
    }else{
        $("#divApprove").hide();
    }

    allowNumerics("txtID");
    allowNumerics("txtFC");
    allowAlphaNumeric("txtExtID1");
    allowAlphaNumeric("txtExtID2");
    allowAlphabets("txtAbbreviation");
    allowAlphabetsSapaceLN("txtFN");
    allowAlphabets("txtMN");
    allowAlphabetsSapaceLN("txtLN");
    allowAlphabets("txtBAN");
    allowAlphabets("txtFAN");
    allowAlphabets("txtNEIC");
    allowAlphabets("txtAS");
    allowAlphabets("txtNT");
    allowAlphabets("txtUPIN");
    allowAlphabets("txtDEA");
    allowAlphabets("txtNPI");
    allowAlphaNumeric("txtTI");
    allowAlphabets("txtPCP");
    allowAlphaNumeric("txtBD");
    allowAlphaNumeric("txtFC");
    // allowAlphaNumeric("txtAdd1");
    //allowAlphaNumeric("txtAdd2");
    allowPhoneNumber("txtHPhone");
    allowPhoneNumber("txtExtension");
    allowPhoneNumber("txtWPhone");
    allowPhoneNumber("txtExtension1");
    allowPhoneNumber("txtCell");
    validateEmail("txtEmail1");
    allowDecimals("txtHeight");
    allowDecimals("txtWeight");

    //txtNEIC
    if(parentRef) {
        operation = parentRef.operation;
        patientId = parentRef.patientId;
    }

    //selItem = parentRef.selItem;
    // $("#txtType").kendoComboBox();
    // $("#cmbStatus").kendoComboBox();
    //$("#cmbAbbreviation").kendoComboBox();
    $("#cmbZip1").kendoComboBox();
    // $("#cmbSMS").kendoComboBox();
    // $("#cmbType").kendoComboBox();
    // $("#cmbPrefix").kendoComboBox();
    // $("#cmbSuffix").kendoComboBox();
    // $("#cmbGender").kendoComboBox();
    // $("#cmbLang").igCombo();
    // $("#cmbRace").kendoComboBox();
    // $("#cmbEthnicity").kendoComboBox();
    // $("#cmbReligion").kendoComboBox();
    $("#cmbEmploymentType").kendoComboBox();
    /*$('#txtHR').kendoMaskedTextBox({
        mask: "00.00"
    });*/
    allowDecimals("txtHR");
    allowDecimals("txtOverTimeHourRate");
    allowDecimals("txtHRperMile");

    $("#btnEdit").prop("disabled", true);
    $("#btnDelete").prop("disabled", true);
    buildAccountListGrid([]);
    allowNumerics("txtID");
    allowNumerics("txtFC");
    /*allowAlphaNumeric("txtExtID1");
    allowAlphaNumeric("txtExtID2");*/
    allowAlphaNumeric("txtAbbreviation");

    allowAlphaNumeric("txtMN");

    allowAlphabets("txtBAN");
    allowAlphabets("txtFAN");
    allowAlphabets("txtNEIC");
    allowAlphabets("txtAS");
    allowAlphabets("txtNT");
    allowAlphabets("txtUPIN");
    allowAlphabets("txtDEA");
    allowAlphabets("txtNPI");
    /*allowAlphaNumeric("txtTI");*/
    allowAlphabets("txtPCP");
    /*allowAlphaNumeric("txtBD");
    allowAlphaNumeric("txtFC");
    allowAlphaNumeric("txtAdd1");
    allowAlphaNumeric("txtAdd2");*/
    allowPhoneNumber("txtHPhone");
    allowPhoneNumber("txtExtension");
    allowPhoneNumber("txtWPhone");
    allowPhoneNumber("txtExtension1");
    allowPhoneNumber("txtCell");
    validateEmail("txtEmail");
    //txtNEIC
    operation = parentRef.operation;
    patientId = parentRef.patientId;
    //selItem = parentRef.selItem;
    // $("#txtType").kendoComboBox();
    // $("#cmbStatus").kendoComboBox();
    //$("#cmbAbbreviation").kendoComboBox();
    $("#cmbZip1").kendoComboBox();
    // $("#cmbSMS").kendoComboBox();
    $("#cmbType").kendoComboBox();
    // $("#cmbPrefix").kendoComboBox();
    // $("#cmbSuffix").kendoComboBox();
    $("#cmbUserId").kendoComboBox();
    // $("#cmbGender").kendoComboBox();
    $("#cmbLang").igCombo();
    // $("#cmbRace").kendoComboBox();
    // $("#cmbEthnicity").kendoComboBox();
    // $("#cmbReligion").kendoComboBox();

    var cntry = sessionStorage.countryName;
    if(cntry.indexOf("India")>=0){
        $("#dtEmpStartDate").kendoDatePicker({format:dtFMT});
        $("#dtEmpEndDate").kendoDatePicker({format:dtFMT});
        $("#dtDOB1").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            yearRange: "-200:+200",
            maxDate: '0',
            onSelect: function(e,e1) {
                // var dob = document.getElementById("dtDOB").value;
                var dob = ((e1.selectedMonth+1)+"-"+e1.selectedDay+"-"+e1.selectedYear);
                var DOB = new Date(dob);
                var today = new Date();
                var age = today.getTime() - DOB.getTime();
                age = Math.floor(age / (1000 * 60 * 60 * 24 * 365.25));
                document.getElementById('txtAge').value = age;
            }
        });
    }else if(cntry.indexOf("United Kingdom")>=0){
        $("#dtDOB1").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            yearRange: "-200:+200",
            maxDate: '0',
            onSelect: function(e,e1) {
                var dob = ((e1.selectedMonth+1)+"-"+e1.selectedDay+"-"+e1.selectedYear);
                //var dob = document.getElementById("dtDOB").value;
                var DOB = new Date(dob);
                var today = new Date();
                var age = today.getTime() - DOB.getTime();
                age = Math.floor(age / (1000 * 60 * 60 * 24 * 365.25));
                document.getElementById('txtAge').value = age;
            }
        });
        $("#dtEmpStartDate").kendoDatePicker({format:dtFMT});
        $("#dtEmpEndDate").kendoDatePicker({format:dtFMT});
        if(IsPostalCodeManual == "1"){
            $("#btnCitySearch").css("display","");
            // $("#lblCity").html("City" +"<span class='mandatoryClass'>*</span>");
            $("#cmbZip").attr("readonly", false);
            $("#btnZipSearch").css("display","none");
            $(".postalCode").html("Postal Code : ");
        }
        else{
            $('.postalCode').html('Postal Code : <span class="mandatoryClass">*</span>');
        }
    }else{
        $("#dtDOB1").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'mm/dd/yy',
            yearRange: "-200:+200",
            maxDate: '0',
            onSelect: function(e,e1) {
                var dob = ((e1.selectedMonth+1)+"-"+e1.selectedDay+"-"+e1.selectedYear);
                // var dob = document.getElementById("dtDOB").value;
                var DOB = new Date(dob);
                var today = new Date();
                var age = today.getTime() - DOB.getTime();
                age = Math.floor(age / (1000 * 60 * 60 * 24 * 365.25));
                document.getElementById('txtAge').value = age;
            }
        });
        $("#dtEmpStartDate").kendoDatePicker({format:dtFMT});
        $("#dtEmpEndDate").kendoDatePicker({format:dtFMT});
    }

    $('[data-toggle="tab"]').on('click', function(e) {
        e.preventDefault();
        $('.alert').remove();
    });


    // setDataForSelection(typeArr, "txtType", onTypeChange, ["Value", "Key"], 0, "");
    setDataForSelection(filterArr, "cmbFilterByName", onFilterChange, ["Value", "Key"], 0, "");
    getZip();
    getAjaxObject(ipAddress+"/homecare/employment-types/?is-active=1&is-deleted=0","GET",getEmployeeList,onError);
    //getAjaxObject(ipAddress+"/provider/list?is-active=1","GET",getAccountList,onError);
    displayLocalNames();
    buttonEvents();

}

function getEmployeeList(dataObj){
    console.log(dataObj);
    var dataArray = [];
    empArr = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.employmentTypes){
            if($.isArray(dataObj.response.employmentTypes)){
                dataArray = dataObj.response.employmentTypes;
            }else{
                dataArray.push(dataObj.response.employmentTypes);
            }
        }
    }
    var obj = {};
    obj.Key = "";
    obj.Value = "";
    empArr.push(obj);
    for(var i=0;i<dataArray.length;i++){
        var obj = {};
        obj.Key = dataArray[i].value;
        obj.Value = dataArray[i].id;
        empArr.push(obj);
    }
    setDataForSelection(empArr, "cmbEmploymentType", onEmploymentTypeChange, ["Key", "Value"], 0, "");

}

function onEmploymentTypeChange(){
    var cmbEmploymentType = $("#cmbEmploymentType").data("kendoComboBox");
    if(cmbEmploymentType && cmbEmploymentType.selectedIndex<0){
        cmbEmploymentType.select(0);
    }
}

function onError(errorObj){
    console.log(errorObj);
}

function getAccountList(dataObj){
    // console.log(dataObj);
    buildAccountListGrid([]);
    var dataArray = [];
    //var tempDataArry = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if($.isArray(dataObj.response.provider)){
            dataArray = dataObj.response.provider;
        }else{
            dataArray.push(dataObj.response.provider);
        }
    }else{
        //customAlert.error("Error",dataObj.response.status.message);
    }
    /*for(var j=0;j<tempDataArry.length;j++){
        var item = tempDataArry[j];
        dataArray.push(item.provider);
    }*/

    for(var i=0;i<dataArray.length;i++) {
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        dataArray[i].photo = ipAddress + "/homecare/download/providers/photo/?" + Math.round(Math.random() * 1000000) + "&access_token=" + sessionStorage.access_token + "&tenant=" + sessionStorage.tenant + "&id=" + dataArray[i].idk;
        // dataArray[i].language1 = dataArray[i].language
        if (dataArray[i].isActive == 1) {
            dataArray[i].Status = "Active";
            var createdDate = new Date(dataArray[i].createdDate);
            var createdDateString = createdDate.toLocaleDateString();
            var modifiedDate = new Date(dataArray[i].modifiedDate);
            var modifiedDateString = modifiedDate.toLocaleDateString();
            dataArray[i].createdDateString = createdDateString;
            dataArray[i].modifiedDateString = modifiedDateString;
            if (dataArray[i].type && dataArray[i].type != null) {
                dataArray[i].typeValue = fncGetTypeValuebyId(dataArray[i].type);
            }
            var dt = new Date(dataArray[i].dateOfBirth);
            var strDate = onGetDate(dataArray[i].dateOfBirth) + " (" + getAge(dt) + " Y)";

            dataArray[i].dob = strDate;
            if (dataArray[i].communications  && dataArray[i].communications[0]) {
                dataArray[i].city = dataArray[i].communications[0].city;
            }
        }
        // dataArray.sort(function(a, b){
        //             return a.lastName - b.lastName;
        //         });
        // dataArray1 = dataArray;
        // if(dataArray1.length > 2) {
        $("#dgridDAccountList").show();
        $("#btnEdit").show();
        $("#btnDelete").show();
    }
    dataArray.sort(function (a, b) {
        var nameA = a.lastName.toUpperCase(); // ignore upper and lowercase
        var nameB = b.lastName.toUpperCase(); // ignore upper and lowercase

        if (nameA > nameB) {
            return 1;
        }
        if (nameA < nameB) {
            return -1;
        }

        // names must be equal
        return 0;

    });
    // }


    // dataArray.sort(GetSortOrder("lastName"));
    // if(dataArray.length > 0) {
    //     dataArray.sort(function(a, b){
    //         return a.lastName - b.lastName;
    //     });
    // }

    // function(a, b) {
    //     if (a[prop] > b[prop]) {
    //         return 1;
    //     } else if (a[prop] < b[prop]) {
    //         return -1;
    //     }
    // }
    buildAccountListGrid(dataArray);
}

function GetSortOrder(prop) {
    return function(a, b) {
        if (a[prop] > b[prop]) {
            return 1;
        } else if (a[prop] < b[prop]) {
            return -1;
        }
        return 0;
    }
}
function buttonEvents(){
    $("#btnEdit").off("click",onClickOK);
    $("#btnEdit").on("click",onClickOK);

    $(".popupClose").off("click", onClickCancel);
    $(".popupClose").on("click", onClickCancel);

    $("#btnCancel").off("click",onClickCancel);
    $("#btnCancel").on("click",onClickCancel);

    $("#btnDelete").off("click",onClickDelete);
    $("#btnDelete").on("click",onClickDelete);

    $("#btnAdd").off("click");
    $("#btnAdd").on("click",onClickAdd);

    $("#btnSave").off("click", onClickSave);
    $("#btnSave").on("click", onClickSave);

    $("#btnSubmit").off("click", onClickView);
    $("#btnSubmit").on("click", onClickView);

    $("#btnSearch").off("click", onClickSearch);
    $("#btnSearch").on("click", onClickSearch);

    // $("#btnReset").off("click", onClickReset);
    $("#btnReset").on("click", function(){
            // $('#cmbLang').closest('.ui-igcombo').find('.ui-igcombo-clearicon').trigger('click');
            $(".ui-igcombo-clearicon").click();
            onClickReset();
        }
    );

    $("input[name=Communication]").on( "change", function() {
        radioValue = $(this).val();
    } )

    $("#btnZipSearch").off("click");
    $("#btnZipSearch").on("click", onClickZipSearch);

    $("#providerFilter-btn").off("click");
    $("#providerFilter-btn").on("click", onClickFilterBtn);

    $("#btnBrowse").off("click", onClickBrowse);
    $("#btnBrowse").on("click", onClickBrowse);

    $("#fileElem").off("change", onSelectionFiles);
    $("#fileElem").on("change", onSelectionFiles);

    $("#btnUpload").off("click", onClickUploadPhoto);
    $("#btnUpload").on("click", onClickUploadPhoto);



    $(".btnLeaveActive").on("click", function(e) {
        e.preventDefault();
        searchOnLoadLeave('active');
        onClickLeaveActive();
    });
    $(".btnLeaveInActive").on("click", function(e) {
        e.preventDefault();
        searchOnLoadLeave('inactive');
        onClickLeaveInActive();
    });

    $("#btnLeaveSave").off("click",onClickLeaveSave);
    $("#btnLeaveSave").on("click",onClickLeaveSave);

    $("#btnLeaveEdit").off("click",onClickLeaveEdit);
    $("#btnLeaveEdit").on("click",onClickLeaveEdit);

    $("#btnLeaveDelete").off("click",onClickLeaveDelete);
    $("#btnLeaveDelete").on("click",onClickLeaveDelete);

    $("#btnLeaveAdd").off("click");
    $("#btnLeaveAdd").on("click",onClickLeaveAdd);

    $("#btnLeaveCancel").off("click",onClickLeaveCancel);
    $("#btnLeaveCancel").on("click",onClickLeaveCancel);

    $("#btnLeaveReset").off("click",onClickLeaveReset);
    $("#btnLeaveReset").on("click",onClickLeaveReset);

    $("#txtWPhone").on('keyup', function(e) {
        if($(this).val().length > 0) {
            $("#txtExtension1").removeAttr("disabled");
        }
        else {
            $("#txtExtension1").attr("disabled","disabled");
        }
    });

    $("#txtCell").on('keyup', function(e) {
        if($(this).val().length > 0) {
            $("#cmbSMS").removeAttr("disabled");
        }
        else {
            $("#cmbSMS").attr("disabled","disabled");
        }
    });

    $("#txtHPhone").on('keyup', function(e) {
        if($(this).val().length > 0) {
            $("#txtExtension").removeAttr("disabled");
        }
        else {
            $("#txtExtension").attr("disabled","disabled");
        }
    });
    $("#txtHPhone1").on('keyup', function(e) {
        if($(this).val().length > 0) {
            $("#txtExtension1").removeAttr("disabled");
        }
        else {
            $("#txtExtension1").attr("disabled","disabled");
        }
    });
    $("#cmbFilterByValue").on('keyup', function(e) {
        if($(this).val().length > 0) {
            $("#providerFilter-btn").removeAttr("disabled");
        }
        else {
            $("#providerFilter-btn").attr("disabled","disabled");
        }
    });

    $("#txtFAN").on('change', function(e) {
        onFacilityChange();
    });

    $("#tabsUL li a[data-toggle='tab']").off("click");
    $("#tabsUL li a[data-toggle='tab']").on("click",onClickTabs);

    $("#btnRAdd").off("click",onClickAddRoster);
    $("#btnRAdd").on("click",onClickAddRoster);

    $("#btnREdit").off("click",onClickEditRoster);
    $("#btnREdit").on("click",onClickEditRoster);

    $("#btnRDel").off("click",onClickDeleteRoster);
    $("#btnRDel").on("click",onClickDeleteRoster);

    $("#btnRRoster").off("click",onClickCreateRoster);
    $("#btnRRoster").on("click",onClickCreateRoster);

    $("#btnRApp").off("click",onClickCreateRosterAppointment);
    $("#btnRApp").on("click",onClickCreateRosterAppointment);

    $("#btnCancel1").off("click", onClickCancel);
    $("#btnCancel1").on("click", onClickCancel);

    $("#btnSave").off("click", onClickSave);
    $("#btnSave").on("click", onClickSave);

    $("#btnSearch").off("click", onClickSearch);
    $("#btnSearch").on("click", onClickSearch);

    $("#btnZipSearch").off("click");
    $("#btnZipSearch").on("click", onClickZipSearch);

    $("#btnCitySearch").off("click");
    $("#btnCitySearch").on("click", onClickZipSearch);

    $("#btnGPSReport").off("click", onClickGPSReport);
    $("#btnGPSReport").on("click", onClickGPSReport);

    // $("#btnStartVideo").off("click", onClickStartVideo);
    //$("#btnStartVideo").on("click", onClickStartVideo);


    $("#btnBrowse").off("click", onClickBrowse);
    $("#btnBrowse").on("click", onClickBrowse);

    $("#fileElem").off("change", onSelectionFiles);
    $("#fileElem").on("change", onSelectionFiles);

    $("#btnUpload").off("click", onClickUploadPhoto);
    $("#btnUpload").on("click", onClickUploadPhoto);

    $("#btnBrowse1").off("click", onClickBrowse1);
    $("#btnBrowse1").on("click", onClickBrowse1);

    $("#fileElem1").off("change", onSelectionFiles1);
    $("#fileElem1").on("change", onSelectionFiles1);

    $("#fileElem").off("click", onSelectionFiles);
    $("#fileElem").on("click", onSelectionFiles);

    $("#btnUpload1").off("click", onClickUploadPhoto1);
    $("#btnUpload1").on("click", onClickUploadPhoto1);

    $("#btnASave").off("click", onClickTMSave);
    $("#btnASave").on("click", onClickTMSave);

    $("#btnAAdd").off("click", onClickAddAvailable);
    $("#btnAAdd").on("click", onClickAddAvailable);

    $("#btnAEdit").off("click", onClickTMEdit);
    $("#btnAEdit").on("click", onClickTMEdit);

    $("#btnADel").off("click", function(){
        $(".k-window-title",parent.document).css("top","5px");
        $(".k-window-title").css("top","5px");
        onClickTMDel();
    });
    $("#btnADel").on("click", function(){
        $(".k-window-title",parent.document).css("top","5px");
        $(".k-window-title").css("top","5px");
        onClickTMDel();
    });

    $("#btnAReset").off("click", onClickTMReset);
    $("#btnAReset").on("click", onClickTMReset);

    $("#btnView").off("click", onClickResumeView);
    $("#btnView").on("click", onClickResumeView);


    $("#imgPhoto").error(function() {
        onshowImage();
    });
    qualIncrement = $('.qualificationTabWrapper table tbody tr').length;

    $('body').on('click','.qualAddRemoveLink.removeQual', function() {
        $(this).closest('tr').remove();
        /*$('.qualificationTabWrapper table tbody').append('<span><a href="#" class="qualAddRemoveLink addQual">+</a> <a href="#" class="qualAddRemoveLink removeQual">-</a><tr><td><input type="text" name="qualWrap-sno" value="'+$trLen+'" /></td><td><input type="text" name="qualWrap-qualification" value="" /></td><td><input type="text" name="qualWrap-university" value="" /></td><td><input type="text" name="qualWrap-percentage" value="" /></td><td><input type="text" name="qualWrap-passedout" value="" /></td><td><input type="text" name="qualWrap-expirydate" value="" /></td></tr></span>');*/
        modifySerialNumber();
    });

    $('#txtTE').on('change', function() {
        if($(this).is(':checked')) {
            $('#txtHRperMile').removeAttr('disabled');
            /*if($("#txtHRperMile").closest('.wraphourpermile').find('.k-maskedtextbox') && $("#txtHRperMile").closest('.wraphourpermile').find('.k-maskedtextbox').length == 0) {
                $('#txtHRperMile').kendoMaskedTextBox({
                    mask: "00.00"
                });
            }*/
        } else {
            $('#txtHRperMile').attr('disabled','disabled');
        }
    });

    $('.activities-link').off('click', onClickActivities);
    $('.activities-link').on('click', onClickActivities);

    $('.availability-link').off('click', onClickAvailability);
    $('.availability-link').on('click', onClickAvailability);

    $('.block-link').off('click', onClickBlock);
    $('.block-link').on('click', onClickBlock);


    $('.approve-link').off('click', onClickApprove);
    $('.approve-link').on('click', onClickApprove);

    // var txtWeeklyContractHour = document.getElementById('txtWeeklyContractHour');
    //
    // txtWeeklyContractHour.addEventListener('change', function() {
    //     var v = parseFloat(this.value);
    //     if (isNaN(v)) {
    //         this.value = '';
    //     } else {
    //         this.value = v.toFixed(2);
    //     }
    // });

    // $("#txtLeavesPerYear").on('onkeypress', function(e) {
    //     IsDigit(e,this.id)
    // });

}
function searchOnLoad(status) {
    buildAccountListGrid([]);
    if(status == "active") {
        var urlExtn = '/provider/list?is-active=1&is-deleted=0';
    }
    else if(status == "inactive") {
        var urlExtn = '/provider/list?is-active=0&is-deleted=1';
    }
    getAjaxObject(ipAddress+urlExtn,"GET",getAccountList,onError);
}
function adjustHeight(){
    var defHeight = 240;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }

    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    if(angularUIgridWrapper) {
        angularUIgridWrapper.adjustGridHeight(cmpHeight);
    }

    var defPTHeight = 210;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defPTHeight = 210;
    }
    var cmpPTHeight = window.innerHeight - defPTHeight;
    cmpPTHeight = (cmpPTHeight < 200) ? 200 : cmpPTHeight;
    if(angularPTUIgridWrapper){
        angularPTUIgridWrapper.adjustGridHeight(cmpPTHeight);
    }
    if(angularPTUIgridWrapper1){
        angularPTUIgridWrapper1.adjustGridHeight((cmpPTHeight));
    }

    var defLeaveHeight = 260;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defLeaveHeight = 80;
    }

    var cmpLeaveHeight = window.innerHeight - defLeaveHeight;
    cmpLeaveHeight = (cmpLeaveHeight < 200) ? 200 : cmpLeaveHeight;
    if(angularLeaveUIgridWrapper) {
        angularLeaveUIgridWrapper.adjustGridHeight(cmpLeaveHeight);
    }
}
function onClickActive() {
    $(".btnInActive").removeClass("radioButton-active");
    $(".btnActive").addClass("radioButton-active");
}
function onClickInActive() {
    $(".btnActive").removeClass("radioButton-active");
    $(".btnInActive").addClass("radioButton-active");
}

function onClickLeaveActive() {
    $(".btnLeaveInActive").removeClass("radioButton-active");
    $(".btnLeaveActive").addClass("radioButton-active");
}
function onClickLeaveInActive() {
    $(".btnLeaveActive").removeClass("radioButton-active");
    $(".btnLeaveInActive").addClass("radioButton-active");
}


function onClickFilterBtn(e) {
    e.preventDefault();
    $('.alert').remove();

    var filterName = $("#cmbFilterByName").val();
    var filterValue = $("#cmbFilterByValue").val();

    buildAccountListGrid([]);
    /*if($(".btnActive").hasClass("selectButtonBarClass")) {
        var urlExtn = '/billing-account/list?is-active=1&is-deleted=0';
    }
    else {
        var urlExtn = '/billing-account/list?is-active=0&is-deleted=1';
    }*/
    if (filterName != "All" && filterName != "type" && filterName != "5") {
        getAjaxObject(ipAddress + "/provider/list?is-active=1&is-deleted=0&" + filterName + "=" + filterValue, "GET", getAccountList, onError);
    } else if (filterName == "5") {
        if (filterValue != "") {
            var pArray = filterValue.split(",");
            if (pArray) {
                if (pArray.length == 2) {
                    var fName = pArray[1];
                    var lName = pArray[0];
                    if (fName && lName) {
                        getAjaxObject(ipAddress + "/provider/list?is-active=1&is-deleted=0&first-name=" + fName + "&last-name=" + lName, "GET", getAccountList, onError);
                    } else if (fName) {
                        getAjaxObject(ipAddress + "/provider/list?is-active=1&is-deleted=0&first-name=" + fName, "GET", getAccountList, onError);
                    } else if (lName) {
                        getAjaxObject(ipAddress + "/provider/list?is-active=1&is-deleted=0&last-name=" + lName, "GET", getAccountList, onError);
                    }
                } else if (pArray.length == 1) {
                    getAjaxObject(ipAddress + "/provider/list?is-active=1&is-deleted=0&last-name=" + lName, "GET", getAccountList, onError);
                }
            }
        }
    } else if (filterName == "type") {
        var type = Number($("#cmbType option:selected").val());
        if (type !== null && type > 0) {
            getAjaxObject(ipAddress + "/provider/list?is-active=1&is-deleted=0&type=" + type, "GET", getAccountList, onError);
        } else {
            customAlert.error("error", "Please select type.");
        }
    }

}
function showPtImage(){
    console.log("Id2")

    var node = '<div ng-show="((row.entity.photo))"><img src="{{row.entity.photo}}" onerror="this.src=\'../../img/AppImg/HosImages/blank.png\'" style="width:50px"><spn>';//this.src=\'../../img/AppImg/HosImages/patient1.png\'
    node = node + "</div>";

    return node;
}
function onImgError(e){
    //console.log(e);
}

function buildAccountListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    if(dataSource.length > 0) {
        gridColumns.push({
            "title": "Image",
            "field": "PID",
            "enableColumnMenu": false,
            "width": "4%",
            "cellTemplate": showPtImage()
        });
    }
    else{
        gridColumns.push({
            "title": "Image",
            "field": "PID",
            "enableColumnMenu": false,
            "width": "4%"
        });
    }
    gridColumns.push({
        "title": "ID",
        "field": "idk",
        "width": "5%",
    });
    // gridColumns.push({
    //    "title": "Abbrevation",
    //    "field": "abbreviation",
    // });
    gridColumns.push({
        "title": "Last Name",
        "field": "lastName",
        "width": "12%",
    });
    gridColumns.push({
        "title": "First Name",
        "field": "firstName",
        "width": "12%",
    });
    gridColumns.push({
        "title": "Language",
        "field": "language",
        "width": "20%",
    });

    gridColumns.push({
        "title": "DOB(Age)",
        "field": "dob",
        "width": "18%",
    });

    gridColumns.push({
        "title": "Type",
        "field": "typeValue",
        "width": "12%",
    });

    gridColumns.push({
        "title": "Gender",
        "field": "gender",
        "width": "8%",
    });
    gridColumns.push({
        "title": "City",
        "field": "city",
        "width": "9%",
    });
    // if(dataSource.length > 0) {
    //     gridColumns.push({
    //         "title": "GPS",
    //         "field": "report",
    //         "width": "7%",
    //         "cellTemplate": showAction()
    //     });
    // }
    // else{
    //     gridColumns.push({
    //         "title": "GPS",
    //         "field": "report",
    //         "width": "7%"
    //     });
    // }


    /*if($(window).width() > 767) {
        gridColumns.push({
            "title": "External ID1",
            "field": "externalId1",
        });
        gridColumns.push({
            "title": "External ID2",
            "field": "externalId2",
        });
        gridColumns.push({
            "title": "Created By",
            "field": "createdBy",
        });
        gridColumns.push({
            "title": "Created Date",
            "field": "createdDateString",
        });
        gridColumns.push({
            "title": "Modified By",
            "field": "modifiedBy",
        });
        gridColumns.push({
            "title": "Modified Date",
            "field": "modifiedDateString",
        });
    }*/
    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}

function showAction(){
    var node = '<div style="text-align:center">';
    //node += '<input  type="button" id="btn" value="View" onclick="onClickAction(event)"></input>';
    node += '<a href="#" onclick="onClickAction(event)" style="cursor:pointer">View</a>';
    node += '</div>';
    return node;
}

var selRow = null;
function onClickAction(e){
    setTimeout(function(){
        selRow = angular.element($(e.target).parent()).scope();
        onClickGPSReport();
    })
}
function onClickGPSReport(){
    parentRef.screenType = "providers";
    parentRef.providerid = selRow.row.entity.userId;
    openReportPopup("../../html/patients/geoReport.html","GPS Report");
}
function openReportPopup(path,title){
    var popW = "60%";
    var popH = "75%";

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = title;
    devModelWindowWrapper.openPageWindow(path, profileLbl, popW, popH, false, closeCallReport);
}
function closeCallReport(evt,ret){

}
function getCountryZoneName() {
    var countryName = sessionStorage.countryName;
    if (countryName.indexOf("India") >= 0) {
        $('.postalCode').html('Postal Code :<span class="mandatoryField">*</span>');
        $('.stateLabel').text("State");
        $('.zipFourWrapper').addClass('hideZipFourWrapper');
        $("#lblFacility").text("Location Name");

    } else if (countryName.indexOf("United Kingdom")) {
        $('.postalCode').html('Postal Code :<span class="mandatoryField">*</span>');
        $('.stateLabel').text("County");
        $('.zipFourWrapper').addClass('hideZipFourWrapper');
        $("#lblFacility").text("Location Name");
    } else {
        $("#lblFacility").text("Facility Name");
        $('.postalCode').html('Zip :<span class="mandatoryField">*</span>');
        $('.stateLabel').text("State");
        $('.zipFourWrapper').removeClass('hideZipFourWrapper');
    }
    /*$.getJSON('//freegeoip.net/json/?callback=', function(dataObj) {
        var dataUrl = ipAddress + "/user/location";
        console.log(dataObj);
        sessionStorage.country = dataObj.country_code;
        dataObj.createdBy = sessionStorage.userId;
        dataObj.countryCode = dataObj.country_code;
        dataObj.countryName = dataObj.country_name;
        dataObj.regionCode = dataObj.region_code;
        dataObj.regionName = dataObj.region_name;
        dataObj.timeZone = dataObj.time_zone;
        dataObj.metroCode = dataObj.metro_code;
        dataObj.zipCode = dataObj.zip_code;
        sessionStorage.countryName = dataObj.country_name;
        var countryName = sessionStorage.countryName;
        if (countryName.indexOf("India") >= 0) {
            $('.postalCode').html('Postal Code :<span class="mandatoryField">*</span>');
            $('.stateLabel').text("State");
            $('.zipFourWrapper').addClass('hideZipFourWrapper');
        } else if (countryName.indexOf("United Kingdom")) {
            $('.postalCode').html('Postal Code :<span class="mandatoryField">*</span>');
            $('.stateLabel').text("County");
            $('.zipFourWrapper').addClass('hideZipFourWrapper');
        } else {
        	$('.postalCode').html('Zip :<span class="mandatoryField">*</span>');
            $('.stateLabel').text("State");
            $('.zipFourWrapper').removeClass('hideZipFourWrapper');
        }
    });*/
}
var prevSelectedItem =[];
function onChange(){
    setTimeout(function(){
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if(selectedItems && selectedItems.length>0){
            $("#btnEdit").prop("disabled", false);
            $("#btnDelete").prop("disabled", false);
        }else{
            $("#btnEdit").prop("disabled", true);
            $("#btnDelete").prop("disabled", true);
        }
    },100)
}

var selDocItem = null;
function onClickOK(){
    setTimeout(function(){
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if(selectedItems && selectedItems.length>0){
            var obj = {};
            obj.selItem = selectedItems[0];
            selDocItem = selectedItems[0];
            selItem = selectedItems[0];
            parentRef.selDocItem = selDocItem;
            parentRef.operation = "update";
            buildQualificationListGrid([]);
            $("#aGeneral").click();
            addDoctor("edit")

        }
    })
}
function onClickDelete(){
    customAlert.confirm("Confirm", "Are you sure you want delete?",function(response){
        if(response.button == "Yes"){
            var selectedItems = angularUIgridWrapper.getSelectedRows();
            console.log(selectedItems);
            if(selectedItems && selectedItems.length>0){
                var selItem = selectedItems[0];
                if(selItem){
                    var dataUrl = ipAddress+"/provider/delete/";
                    var reqObj = {};
                    reqObj.id = selItem.idk;
                    reqObj.abbr = selItem.abbr;
                    reqObj.country = selItem.county;
                    reqObj.code = selItem.code;
                    reqObj.isDeleted = "1";
                    reqObj.isActive = "0";
                    reqObj.modifiedBy = sessionStorage.userId;
                    createAjaxObject(dataUrl,reqObj,"POST",onDeleteCountryt,onError);
                }
            }
        }
    });
}
function onDeleteCountryt(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            customAlert.info("info", "Staff Deleted Successfully");
            buildAccountListGrid([]);
            init();
        }else{
            customAlert.error("error", dataObj.message);
        }
    }
}
function onClickAdd(){
    tabLink('tab1','a.boxtablink');
    $("#liAddiional").hide();
    $("#liCarePlan").hide();
    $("#liCareAvl").hide();
    $("#liCareLeave").hide();
    parentRef.operation = "add";
    $('#cmbLang').closest('.ui-igcombo').find('.ui-igcombo-clearicon').trigger('click');
    onClickReset();
    addDoctor("add");
}
function addDoctor(opr){
    var popW = "75%";
    var popH = "78%";

    $("#viewDivBlock").hide();
    $("#addTaskGroupAccountPopup").show();
    var profileLbl = "Add Staff";
    parentRef.operation = opr;
    operation = opr;
    onClickReset();
    if(opr == "add"){
        $('.tabContentTitle').text('Add Staff');
        var billActName = $("#txtBAN").val();

        $("#txtBAN").val(billActName);
    }else{
        if(operation == "edit") {
            $("#liAddiional").show();
            $("#liCarePlan").show();
            $("#liCareAvl").show();
            $("#liCareLeave").show();
            $("#liCareRota").show();
        }
        parentRef.patientId = selDocItem.idk;
        patientId = selDocItem.idk;
        profileLbl = "Edit Staff";
        $('.tabContentTitle').text('Edit Staff');
        onFacilityChange();
        onGetPatientInfo(selDocItem);
        // getAjaxObject(ipAddress + "/provider/list?id=" + selDocItem.idk, "GET", onGetPatientInfo, onError);
    }
    // var devModelWindowWrapper = new kendoWindowWrapper();
    // devModelWindowWrapper.openPageWindow("../../html/masters/createDoctor.html", profileLbl, popW, popH, true, closeAddDoctortAction);
}
function closeAddDoctortAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        var opr = returnData.operation;
        if(opr == "add"){
            customAlert.info("info", "Staff Created Successfully");
        }else{
            customAlert.info("info", "Staff Updated Successfully");
        }
        init();
    }
}
function onClickCancel(){
    $("#viewDivBlock").show();
    $("#addTaskGroupAccountPopup").hide();
    onClickReset();
}
function onClickClose() {
    //$('.listDataWrapper').show();
    //$('.addOrRemoveWrapper').hide();
    $("#btnEdit").prop("disabled", true);
    $("#btnDelete").prop("disabled", true);
    //buildAccountListGrid([]);
    init();
    $('.selectButtonBarClass').trigger('click');
}


function onClickZipSearch() {
    var popW = 600;
    var popH = 500;

    parentRef.searchZip = true;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search Zip";
    var cntry = sessionStorage.countryName;
    if(cntry.indexOf("India")>=0){
        profileLbl = "Search Postal Code";
    }else if(cntry.indexOf("United Kingdom")>=0){
        profileLbl = "Search Postal Code";
    }else if(cntry.indexOf("United State")>=0){
        profileLbl = "Search Zip";
    }else{
        profileLbl = "Search Zip";
    }
    devModelWindowWrapper.openPageWindow("../../html/masters/zipList.html", profileLbl, popW, popH, true, onCloseSearchZipAction);
}
var zipSelItem = null;
var cityId = "";

function onCloseSearchZipAction(evt, returnData) {
    console.log(returnData);
    if (returnData && returnData.status == "success") {
        //console.log();
        $("#cmbZip").val("");
        $("#txtZip4").val("");
        $("#txtState").val("");
        $("#txtCountry").val("");
        $("#txtCity").val("");
        var selItem = returnData.selItem;
        if (selItem) {
            zipSelItem = selItem;
            if (zipSelItem) {
                cityId = zipSelItem.idk;
                if (zipSelItem.zip) {
                    $("#cmbZip").val(zipSelItem.zip);
                }
                if (zipSelItem.zipFour) {
                    $("#txtZip4").val(zipSelItem.zipFour);
                }
                if (zipSelItem.state) {
                    $("#txtState").val(zipSelItem.state);
                }
                if (zipSelItem.country) {
                    $("#txtCountry").val(zipSelItem.country);
                }
                if (zipSelItem.city) {
                    $("#txtCity").val(zipSelItem.city);
                }
            }
        }
    }
}

function getZip() {
    getAjaxObject(ipAddress + "/city/list?is-active=1", "GET", getZipList, onError);
}

function getZipList(dataObj) {
    //var dArray = getTableListArray(dataObj);
    var dArray = [];
    if (dataObj && dataObj.response && dataObj.response.cityext) {
        if ($.isArray(dataObj.response.cityext)) {
            dArray = dataObj.response.cityext;
        } else {
            dArray.push(dataObj.response.cityext);
        }
    }
    if (dArray && dArray.length > 0) {
        //setDataForSelection(dArray, "cmbZip", onZipChange, ["zip", "id"], 0, "");
        onZipChange();
    }
    getPrefix();
}

function getPrefix() {
    getAjaxObject(ipAddress + "/master/Prefix/list?is-active=1", "GET", getCodeTableValueList, onError);
}

function onComboChange(cmbId) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb && cmb.selectedIndex < 0) {
        cmb.select(0);
    }
}

function onPrefixChange() {
    onComboChange("cmbPrefix");
}
function onSuffixChange() {
    onComboChange("cmbSuffix");
}
function onUserIdChange() {
    onComboChange("cmbUserId");
}

function onPtChange() {
    onComboChange("cmbStatus");
}

function onSMSChange() {
    onComboChange("cmbSMS");
}

function onLanChange() {
    onComboChange("cmbLang");
}

function onRaceChange() {
    onComboChange("cmbRace");
}

function onEthnicityChange() {
    onComboChange("cmbEthicity");
}

function onZipChange() {
    onComboChange("cmbZip");
    var cmbZip = $("#cmbZip").data("kendoComboBox");
    if (cmbZip) {
        var dataItem = cmbZip.dataItem();
        if (dataItem) {
            $("#txtZip4").val(dataItem.zipFour);
            $("#txtCountry").val(dataItem.country);
            $("#txtState").val(dataItem.state);
            $("#txtCity").val(dataItem.city);
        }
    }
}
var prefixData;
function getCodeTableValueList(dataObj) {
    prefixData = getTableListArray(dataObj,"cmbPrefix");
    // if (dArray && dArray.length > 0) {
    //     // setDataForSelection(dArray, "cmbPrefix", onPrefixChange, ["value", "idk"], 0, "");
    // }
    getAjaxObject(ipAddress + "/master/Suffix/list?is-active=1", "GET", getCodeTableSuffixValueList, onError);
}

var suffixData;
function getCodeTableSuffixValueList(dataObj) {
    suffixData = getTableListArray(dataObj,"cmbSuffix");
    // if (dArray && dArray.length > 0) {
    //     setDataForSelection(dArray, "cmbSuffix", onSuffixChange, ["value", "idk"], 0, "");
    // }
    getAjaxObject(ipAddress + "/master/Status/list?is-active=1", "GET", getPatientStatusValueList, onError);
}

function getPatientStatusValueList(dataObj) {
    var dArray = getTableFirstListArray(dataObj,"cmbStatus");
    // if (dArray && dArray.length > 0) {
    //     setDataForSelection(dArray, "cmbStatus", onPtChange, ["desc", "idk"], 0, "");
    // }
    getAjaxObject(ipAddress + "/master/Gender/list?is-active=1", "GET", getGenderValueList, onError);
}

function getGenderValueList(dataObj) {
    var dArray = getTableListArray(dataObj,"cmbGender");
    /*if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbGender", onGenderChange, ["desc", "idk"], 0, "");
    }*/
    getAjaxObject(ipAddress + "/master/SMS/list?is-active=1", "GET", getSMSValueList, onError);
}

function getSMSValueList(dataObj) {
    var dArray = getTableListArray(dataObj,"cmbSMS");
    // if (dArray && dArray.length > 0) {
    //     setDataForSelection(dArray, "cmbSMS", onSMSChange, ["desc", "idk"], 0, "");
    // }
    getAjaxObject(ipAddress + "/master/Language/list?is-active=1", "GET", getLanguageValueList, onError);
}

function getLanguageValueList(dataObj) {
    var dArray = getTableFirstListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setMultipleDataForSelection(dArray, "cmbLang", onLanChange, ["desc", "idk"], 0, "");
    }
    getAjaxObject(ipAddress + "/master/Race/list?is-active=1", "GET", getRaceValueList, onError);
}
var raceData;
function getRaceValueList(dataObj) {
    raceData = getTableListArray(dataObj,"cmbRace");
    /*if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbRace", onRaceChange, ["desc", "idk"], 0, "");
    }*/
    getAjaxObject(ipAddress + "/master/Ethnicity/list?is-active=1", "GET", getEthnicityValueList, onError);
}
var ethnicityData;
function getEthnicityValueList(dataObj) {
    ethnicityData = getTableListArray(dataObj,"cmbEthnicity");
    /*if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbEthicity", onEthnicityChange, ["desc", "idk"], 0, "");
    }*/
    getAjaxObject(ipAddress + "/master/Religion/list?is-active=1", "GET", getReligionValueList, onError);
}
var religionData;
function getReligionValueList(dataObj) {
    religionData = getTableListArray(dataObj,"cmbReligion");
    // if (dArray && dArray.length > 0) {
    //     setDataForSelection(dArray, "cmbReligion", onReligionChange, ["desc", "idk"], 0, "");
    // }
    getAjaxObject(ipAddress + "/facility/list?is-active=1", "GET", getFacilityList, onError);
}

function getFacilityList(dataObj) {
    console.log(dataObj);
    var dataArray = [];
    if (dataObj) {
        if ($.isArray(dataObj.response.facility)) {
            dataArray = dataObj.response.facility;
        } else {
            dataArray.push(dataObj.response.facility);
        }
    }
    var tempDataArry = [];
    var obj = {};
    obj.name = "";
    obj.idk = "";
    tempDataArry.push(obj);
    for (var i = 0; i < dataArray.length; i++) {
        dataArray[i].Status = "InActive";
        if (dataArray[i].isActive == 1) {
            dataArray[i].Status = "Active";
            var obj = dataArray[i];
            obj.idk = dataArray[i].id;
            obj.status = dataArray[i].Status;
            tempDataArry.push(obj);
            $("#txtFAN").append('<option value="' + dataArray[i].idk + '">' + dataArray[i].name + '</option>');
        }
    }
    dataArray = tempDataArry;

    // setDataForSelection(dataArray, "txtFAN", onFacilityChange, ["name", "idk"], 0, "");
    onFacilityChange();
    if (operation == UPDATE && patientId != "") {
        getPatientInfo();
    }
}

function onFacilityChange() {
    var txtFAN = $("#txtFAN").val();
    if (txtFAN) {
        var txtFId = txtFAN;
        getAjaxObject(ipAddress + "/facility/list?id=" + txtFId, "GET", onGetFacilityInfo, onError);
    }
}
var billActNo = "";

function onGetFacilityInfo(dataObj) {
    billActNo = "";
    if (dataObj && dataObj.response && dataObj.response.facility) {
        $("#txtBAN").val(dataObj.response.facility[0].name);
        billActNo = dataObj.response.facility[0].billingAccountId;
    }
}

function getPatientInfo() {
    getAjaxObject(ipAddress + "/provider/list?id=" + patientId, "GET", onGetPatientInfo, onError);
}

function getComboListIndex(cmbId, attr, attrVal) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb) {
        var ds = cmb.dataSource;
        var totalRec = ds.total();
        for (var i = 0; i < totalRec; i++) {
            var dtItem = ds.at(i);
            if (dtItem && dtItem[attr] == attrVal) {
                cmb.select(i);
                return i;
            }
        }
    }
    return -1;
}


function getTableListArray(dataObj,cmbId) {
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.codeTable) {
        if ($.isArray(dataObj.response.codeTable)) {
            dataArray = dataObj.response.codeTable;
        } else {
            dataArray.push(dataObj.response.codeTable);
        }
    }
    var tempDataArry = [];
    var obj = {};
    obj.desc = "";
    obj.zip = "";
    obj.value = "";
    obj.idk = "";
    tempDataArry.push(obj);
    for (var i = 0; i < dataArray.length; i++) {
        if (dataArray[i].isActive == 1) {
            var obj = dataArray[i];
            obj.idk = dataArray[i].id;
            obj.status = dataArray[i].Status;
            tempDataArry.push(obj);
            if(cmbId != '') {
                if (cmbId != "cmbGender" && cmbId != "cmbSMS" && cmbId != "cmbStatus" && cmbId != "cmbRace"
                    && cmbId != "cmbReligion" && cmbId != "cmbEthnicity") {
                    $("#" + cmbId).append('<option value="' + dataArray[i].idk + '">' + dataArray[i].value + '</option>');
                }
                else {
                    $("#" + cmbId).append('<option value="' + dataArray[i].idk + '">' + dataArray[i].desc + '</option>');
                }
            }
        }
    }

    return tempDataArry;
}
function getTableFirstListArray(dataObj,cmbId) {
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.codeTable) {
        if ($.isArray(dataObj.response.codeTable)) {
            dataArray = dataObj.response.codeTable;
        } else {
            dataArray.push(dataObj.response.codeTable);
        }
    }
    var tempDataArry = [];
    for (var i = 0; i < dataArray.length; i++) {
        if (dataArray[i].isActive == 1) {
            var obj = dataArray[i];
            obj.idk = dataArray[i].id;
            obj.status = dataArray[i].Status;
            tempDataArry.push(obj);
            if(cmbId != '') {
                $("#" + cmbId).append('<option value="' + dataArray[i].idk + '">' + dataArray[i].desc + '</option>');
            }

        }
    }
    return tempDataArry;

}
function getTableUserListArray(dataObj) {
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.user) {
        if ($.isArray(dataObj.response.user)) {
            dataArray = dataObj.response.user;
        } else {
            dataArray.push(dataObj.response.user);
        }
    }
    var tempDataArry = [];
    var obj = {};
    obj.firstAndLastName = "";
    obj.idk = "";
    obj.userTypeId = "";
    obj.userIdDup = "";
    tempDataArry.push(obj);
    for (var i = 0; i < dataArray.length; i++) {
        if (dataArray[i].isActive == 1) {
            var obj = dataArray[i];
            obj.idk = obj.id;
            obj.status = obj.Status;
            obj.userIdDup = obj.idk;
            if(obj.userTypeId == 100) {
                obj.firstAndLastName = obj.firstName + ", " + obj.lastName + " - Doctor";
            }
            if(obj.userTypeId == 200) {
                obj.firstAndLastName = obj.firstName + ", " + obj.lastName + " - Nurse";
            }
            if(obj.userTypeId == 201) {
                obj.firstAndLastName = obj.firstName + ", " + obj.lastName + " - Caregiver";
            }
            tempDataArry.push(obj);
        }
    }
    return tempDataArry;
}

function onClickReset() {
    if(operation == ADD) {
        operation = ADD;
    }
    else {
        operation = UPDATE;
    }

    $("#cmbGender option").each(function() {
        $(this).removeAttr("selected");
    });
    patientId = "";
    $("#txtID").html("");
    $("#txtExtID1").val("");
    $("#txtExtID2").val("");
    // setComboReset("cmbPrefix");
    // setComboReset("cmbSuffix");
    $("#cmbPrefix").val("");
    $("#cmbSuffix").val("");
    // $("#cmbSuffix option").each(function() {
    //     $(this).removeAttr("selected");
    // });
    setComboReset("cmbUserId");
    setComboReset("cmbEmploymentType");
    $("#txtFN").val("");
    $("#txtLN").val("");
    $("#txtMN").val("");
    $("#txtWeight").val("");
    $("#txtHeight").val("");
    $("#txtNN").val("");
    $("#txtAbbreviation").val("");
    $("#txtType").val("");
    $("#txtBAN").val("");
    $("#txtFAN").val("");
    $("#txtNEIC").val("");
    $("#txtAS").val("");
    $("#txtNT").val("");
    $("#txtUPIN").val("");
    $("#txtDEA").val("");
    $("#txtNPI").val("");
    $("#txtTI").val("");
    $("#txtPCP").val("");
    $("#txtBD").val("");
    $("#txtFC").val("");
    $("#cmbReligion").val("");
    // $("#cmbReligion option").each(function() {
    //     $(this).removeAttr("selected");
    // });
    // $('#cmbLang').closest('.ui-igcombo').find('.ui-igcombo-clearicon').trigger('click');
    $("#imgPhoto").attr("src", "");
    //$("#txtZip4").val("");


    // var dtDOB = $("#dtDOB1").data("kendoDatePicker");
    // if(dtDOB){
    // 	dtDOB.value("");
    // }
    $("#dtDOB1").val("");
    $("#txtSSN").val("");

    $("#cmbGender").val("");
    // $("#cmbEthnicity option").each(function() {
    //     $(this).removeAttr("selected");
    // });
    $("#cmbEthnicity").val("");
    // $("#cmbRace option").each(function() {
    //     $(this).removeAttr("selected");
    // });
    $("#cmbRace").val("");
    // setComboReset("cmbLang");
    $("#txtAdd1").val("");
    $("#txtAdd2").val("");
    $("#txtCity").val("");
    $("#txtState").val("");
    $("#txtCountry").val("");
    $("#txtZip4").val("");
    $("#cmbSMS").val("");
    $("#txtCell").val("");
    $("#txtExtension1").val("");
    $("#txtWPhone").val("");
    $("#txtExtension").val("");
    $("#txtHPhone").val("");
    $("#txtEmail").val("");
    $("#txtAge").val("");

    $("#rdHome").prop("checked", true);
    $("#cmbZip").val("");
    radioValue="";


    // if(operation == ADD) {
    //     operation = ADD;
    // }
    // else {
    //     operation = UPDATE;
    // }
    // patientId = "";
    // $("#txtID").val("");
    // $("#txtExtID1").val("");
    // $("#txtExtID2").val("");
    // setComboReset("cmbPrefix");
    // setComboReset("cmbSuffix");
    // $("#txtFN").val("");
    // $("#txtLN").val("");
    // $("#txtMN").val("");
    // $("#txtWeight").val("");
    // $("#txtHeight").val("");
    // $("#txtNN").val("");
    // $("#txtAbbreviation").val("");
    // $("#txtType").val("");
    // $("#txtBAN").val("");
    // $("#txtFAN").val("");
    // $("#txtNEIC").val("");
    // $("#txtAS").val("");
    // $("#txtNT").val("");
    // $("#txtUPIN").val("");
    // $("#txtDEA").val("");
    // $("#txtNPI").val("");
    // $("#txtTI").val("");
    // $("#txtPCP").val("");
    // $("#txtBD").val("");
    // $("#txtFC").val("");
    // $("#txtBAN").val("");
    // //$("#txtZip4").val("");
    //
    //
    // /*var dtDOB = $("#dtDOB").data("kendoDatePicker");
    // if(dtDOB){
    //     dtDOB.value("");
    // }*/
    // $("#txtSSN").val("");
    // setComboReset("cmbStatus");
    // setComboReset("cmbGender");
    // setComboReset("cmbEthnicity");
    // setComboReset("cmbRace");
    // /*setComboReset("cmbLang");*/
    // $('#cmbLang').closest('.ui-igcombo').find('.ui-igcombo-clearicon').trigger('click');
    // $("#txtAdd1").val("");
    // $("#txtAdd2").val("");
    // $("#txtCity").val("");
    // $("#txtState").val("");
    // $("#txtCountry").val("");
    // $("#cmbZip").val("");
    // $("#txtZip4").val("");
    // setComboReset("cmbSMS");
    // setComboReset("cmbReligion");
    // setComboReset("txtType");
    // $("#txtCell").val("");
    // $("#txtExtension1").val("");
    // $("#txtWPhone").val("");
    // $("#txtExtension").val("");
    // $("#txtHPhone").val("");
    // $("#txtEmail").val("");
    // $("#rdHome").prop("checked", true);

    $('#txtLikes').val("");
    $('#txtDisLikes').val("");
    $('#txtSkills').val("");
    $('#txtHR').val("");
    $('#txtTE').removeAttr("checked");
    $('#txtHRperMile').val("");
    $('#txtWeeklyContractHour').val("");
    $('#txtLeavesPerYear').val("");
    $('#txtOverTimeHourRate').val("");
    var dtEmpStartDate = $("#dtEmpStartDate").data("kendoDatePicker");
    if(dtEmpStartDate){
        dtEmpStartDate.value("");
    }

    var dtEmpEndDate = $("#dtEmpEndDate").data("kendoDatePicker");
    if(dtEmpEndDate){
        dtEmpEndDate.value("");
    }


}

function setComboReset(cmbId) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb) {
        cmb.select(0);
    }
}

function validation() {
    var flag = true;
    var txtFC = $("#txtFC").val();
    txtFC = $.trim(txtFC);
    if (txtFC == "") {
        customAlert.error("Error", "Enter Finance charges");
        flag = false;
        return false;
    }
    if (cityId == "") {
        customAlert.error("Error", "Select City ");
        flag = false;
        return false;
    }
    /*var strAbbr = $("#txtAbbrevation").val();
    	strAbbr = $.trim(strAbbr);
    	if(strAbbr == ""){
    		customAlert.error("Error","Enter Abbrevation");
    		flag = false;
    		return false;
    	}
    	var strCode = $("#txtCode").val();
    	strCode = $.trim(strCode);
    	if(strCode == ""){
    		customAlert.error("Error","Enter Code");
    		flag = false;
    		return false;
    	}
    	var strName = $("#txtName").val();
    	strName = $.trim(strName);
    	if(strName == ""){
    		customAlert.error("Error","Enter Country");
    		flag = false;
    		return false;
    	}*/

    return flag;
}

function getComboDataItem(cmbId) {
    var dItem = null;
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb && cmb.dataItem()) {
        dItem = cmb.dataItem();
        return cmb.text();
    }
    return "";
}

function getComboDataItemValue(cmbId) {
    var dItem = null;
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb && cmb.dataItem()) {
        dItem = cmb.dataItem();
        return cmb.value();
    }
    return "";
}


function onClickSave() {
    var sEmail = $('#txtEmail').val();
    /*if (validation()) {*/
    $('.alert').remove();
    var strId = $("#txtID").val();
    var strExtId1 = $("#txtExtID1").val();
    var strExtId2 = $("#txtExtID2").val();
    var strPrefix = $("#cmbPrefix option:selected").text();

    var strSuffix = $("#cmbSuffix option:selected").text();

    var strNickName = $("#txtNN").val();
    var strStatus = $("#cmbStatus").val();
    var strAbbr = $("#txtAbbreviation").val();
    var strFN = $("#txtFN").val();
    var strMN = $("#txtMN").val();
    var strLN = $("#txtLN").val();

    var dtItem = $("#dtDOB").data("kendoDatePicker");
    var strDate = "";
    if (dtItem && dtItem.value()) {
        strDate = kendo.toString(dtItem.value(), "yyyy-MM-dd");
    }
    var cntry = sessionStorage.countryName;
    var strhouseNumber = "";
    var dt = document.getElementById("dtDOB1").value;
    if(dt){
        if (cntry.indexOf("India") >= 0) {
            var dtArray = dt.split("/");
            dob = (dtArray[1] + "/" + dtArray[0] + "/" + dtArray[2]);
        } else if (cntry.indexOf("United Kingdom") >= 0) {
            var dtArray = dt.split("/");
            dob = (dtArray[1] + "/" + dtArray[0] + "/" + dtArray[2]);
            if(IsPostalCodeManual =="1"){
                strhouseNumber = $("#cmbZip").val();
            }
        } else {
            var dtArray = dt.split("/");
            dob = (dtArray[0] + "/" + dtArray[1] + "/" + dtArray[2]);
        }
        if (dob) {
            var DOB = new Date(dob);
            strDate = kendo.toString(DOB, "yyyy-MM-dd");
        }
    }

    if(strDate){
        var stDate = new Date(strDate);
        var currDate = new Date();
        if(currDate.getTime()<stDate.getTime()){
            var strMsg = "Invalid Date of Birth ";
            // $('.customAlert').append('<div class="alert alert-danger">'+strMsg+'</div>');
            customAlert.error("error", strMsg);
            return false;
        }
    }
    var dt = document.getElementById("dtDOB1").value;
    var strSSN = $("#txtSSN").val();
    var strGender = $("#cmbGender option:selected").text();
    var strAdd1 = $("#txtAdd1").val();
    var strAdd2 = $("#txtAdd2").val();
    var strCity = $("#txtCity").val();
    var strState = $("#txtState").val();
    var strCountry = $("#txtCountry").val();

    var strZip = $("#cmbZip").val();
    var strZip4 = $("#txtZip4").val();
    var strHPhone = $("#txtHPhone").val();
    var strExt = $("#txtExtension").val();
    var strWp = $("#txtWPhone").val();
    var strWpExt = $("#txtWPExt").val();
    var strCell = $("#txtCell").val();

    var strSMS = $("#cmbSMS").val();

    var strEmail = $("#txtEmail").val();
    var strLan;
    if(operation == UPDATE){
        strLan = $("#cmbLang").val();
        if(strLan == ""){
            strLan = selDocItem.language;
        }

    }else{
        strLan = $("#cmbLang").val();
    }

    var strRace = $("#cmbRace option:selected").text();
    var strEthinicity = $("#cmbEthnicity option:selected").text();
    var strReligion = $("#cmbReligion option:selected").text();



    var dtEmpStartDate = $("#dtEmpStartDate").data("kendoDatePicker");
    var dtEmpEndDate = $("#dtEmpEndDate").data("kendoDatePicker");

    var fDate = dtEmpStartDate.value();
    var tDate = dtEmpEndDate.value();

    var selFDate, selTDate, stDateTime, edDateTime;
    if(fDate != null){
        selFDate = new Date(fDate);
        stDateTime = getFromDate(selFDate);
    }
    else{
        stDateTime = null;
    }

    if(tDate != null){
        selTDate = new Date(tDate);
        edDateTime = getToDate(selTDate);
    }
    else{
        edDateTime = null;
    }



    var IsValid = true;
    var errorMessage = "";
    if (dtEmpStartDate && dtEmpStartDate.value() && dtEmpEndDate && dtEmpEndDate.value() && stDateTime && edDateTime) {
        if(stDateTime > edDateTime){
            IsValid = false;
            errorMessage = "Employment end should be greater than the start date";
        }
    }


    if (IsValid) {
        var dataObj = {};
        dataObj.createdBy = sessionStorage.userId;
        dataObj.externalId1 = strExtId1;
        dataObj.externalId2 = strExtId2;
        dataObj.abbreviation = $("#txtAbbreviation").val();
        // var txtType = $("#txtType").data("kendoComboBox");
        var strTType = "";
        // if (txtType) {
        //     strTType = Number(txtType.value());
        // }
        strTType = $("#txtType").val();
        dataObj.type = strTType; //$("#txtType").val();;
        dataObj.prefix = strPrefix; //$("#txtEmail").val();;
        dataObj.suffix = strSuffix;
        dataObj.firstName = strFN;
        dataObj.middleName = strMN;
        dataObj.lastName = strLN;
        dataObj.nickname = strNickName;
        dataObj.billingAccountName = $("#txtBAN").val();
        dataObj.billingAccountId = Number(billActNo);
        var fId = $("#txtFAN").val();
        dataObj.facilityId = Number(fId);
        dataObj.neicSpeciality = $("#txtNEIC").val();
        dataObj.amaSpeciality = $("#txtAS").val();
        dataObj.nuucType = $("#txtNT").val();;
        dataObj.upin = $("#txtUPIN").val();;
        dataObj.dea = $("#txtDEA").val();;
        dataObj.npi = $("#txtNPI").val();
        dataObj.taxonomyId = $("#txtTI").val();
        dataObj.pcp = $("#txtPCP").val();
        dataObj.billableDoctor = $("#txtBD").val();
        dataObj.dateOfBirth = strDate;
        var finCharges = $("#txtFC").val() != "" ? $("#txtFC").val() : 0.0;
        dataObj.financeCharges = finCharges;
        // if(strStatus == "InActive") {
        dataObj.isActive = strStatus;
        // // }
        // else if(strStatus == "Active") {
        //     dataObj.isActive = 1;
        // }
        // dataObj.userId = null;
        dataObj.height = $('#txtHeight').val();
        dataObj.weight = $('#txtWeight').val();
        dataObj.gender = strGender;
        dataObj.ethnicity = strEthinicity;
        dataObj.race = strRace;
        dataObj.language = strLan;
        dataObj.religion = strReligion;
        dataObj.likes = $('#txtLikes').val();
        dataObj.dislikes = $('#txtDisLikes').val();
        dataObj.skills = $('#txtSkills').val();
        dataObj.hourlyRate = $('#txtHR').val();
        dataObj.travelExpenses = $('#txtTE').is(':checked') ? "1" : "0";
        dataObj.ratePerMile = $('#txtHRperMile').val();
        dataObj.weekelyContractHours = $('#txtWeeklyContractHour').val();
        dataObj.leavesPerYear = $('#txtLeavesPerYear').val();
        dataObj.overtimeHourlyRate = $('#txtOverTimeHourRate').val();
        var cmbEmploymentType = $("#cmbEmploymentType").data("kendoComboBox");
        dataObj.employmentTypeId = Number(cmbEmploymentType.value());


        dataObj.employmentStartDate = stDateTime;
        dataObj.employmentEndDate = edDateTime;

        var comm = [];
        var comObj = {};
        if (cntry.indexOf("United Kingdom") >= 0) {
            if (IsPostalCodeManual == "1") {
                if(zipSelItem.cityId) {
                    comObj.cityId = zipSelItem.cityId;
                }
                else if(zipSelItem.idk){
                    comObj.cityId = zipSelItem.idk;
                }
            }
            else{
                if (strZip) {
                    if(zipSelItem.cityId) {
                        comObj.cityId = zipSelItem.cityId;
                    }
                    else if(zipSelItem.idk){
                        comObj.cityId = zipSelItem.idk;
                    }
                }
            }
        }
        else {
            if (strZip) {
                if(zipSelItem.cityId) {
                    comObj.cityId = zipSelItem.cityId;
                }
                else if(zipSelItem.idk){
                    comObj.cityId = zipSelItem.idk;
                }
            }
        }
        if(strStatus == "InActive") {
            comObj.isActive = 0;
        }
        else if(strStatus == "Active") {
            comObj.isActive = 1;
        }
        /*comObj.countryId = cityId;*/
        comObj.isDeleted = 0;
        //comObj.zipFour = $("#txtZip4").val(); //"2323";
        comObj.sms = $("#cmbSMS option:selected").text();;

        //comObj.state = $("#txtState").val();
        comObj.workPhoneExt = $("#txtExtension1").val();
        comObj.email = $("#txtEmail").val();
        //comObj.zip = strZip;
        comObj.parentTypeId = 500;
        comObj.address2 = $("#txtAdd2").val();
        comObj.address1 = $("#txtAdd1").val();
        /*comObj.stateId = 1;
        comObj.areaCode = "232";*/
        //comObj.homePhoneExt = $("#txtExtension").val();
        comObj.homePhone = $("#txtHPhone").val();
        comObj.houseNumber = strhouseNumber;
        comObj.workPhone = $("#txtWPhone").val();
        comObj.cellPhone = $("#txtCell").val();
        if(radioValue != ""){
            comObj.defaultCommunication = radioValue;
        }
        if($("#txtCell").val() != ""){
            comObj.defaultCommunication = 3;
        }
        else if($("#txtHPhone").val() != ""){
            comObj.defaultCommunication = 1;
        }
        else if($("#txtWPhone").val() != ""){
            comObj.defaultCommunication = 2;
        }
        else if($("#txtEmail").val() != ""){
            comObj.defaultCommunication = 4;
        }
        /*if (operation == UPDATE) {}*/

        if (operation == UPDATE) {
            var communicationLen = selDocItem.communications != null ? selDocItem.communications.length : 0;
            var cityIndexVal = "";
            //var dupCityIndexVal = "";
            for(var i=0; i<communicationLen; i++) {
                if(selDocItem.communications[i].parentTypeId == 500) {
                    cityIndexVal = i;
                }
                /*if(selItem.communications[i].parentTypeId == 501) {
                    dupCityIndexVal = i;
                }*/
            }
            if(cityIndexVal != "" || (cityIndexVal == 0 && selDocItem.communications != null)) {
                comObj.id = selDocItem.communications[cityIndexVal].id;
                comObj.modifiedBy = sessionStorage.userId;
                comObj.parentId = selDocItem.idk;
            }
            /*if(dupCityIndexVal != "") {
                comObj1.id = selItem.communications[dupCityIndexVal].id;
                comObj1.modifiedBy = sessionStorage.userId;
                comObj1.parentId = selItem.idk;
            }*/
            //comm1.modifiedDate = new Date().getTime();
        }
        else {
            comObj.createdBy = Number(sessionStorage.userId);
        }

        comm.push(comObj);
        dataObj.communications = comm;
        if(strStatus && strDate!= "" &&  strStatus != "" && strFN != "" && strLN != "" && strGender != "" && strLan != "" && strTType != "" && strAdd1 != "" && strAbbr != "" && $("#txtFAN").val() != "") {
            // if(!validateEmail(strEmail)) {
            //     $("html,body").animate({
            //         scrollTop: 0
            //     }, 500);
            //     $('.customAlert').append('<div class="alert alert-danger">Please enter valid Email Address</div>');
            // }
            // else {


            if (cntry.indexOf("United Kingdom") >= 0){
                if(IsPostalCodeManual=="1"){

                    ABBREVIATION_Patient = dataObj.abbreviation;
                    if (operation == ADD) {
                        var dataUrl = ipAddress + "/provider/create";
                        createAjaxObject(dataUrl, dataObj, "POST", onCreate, onError);
                    } else if (operation == UPDATE) {
                        dataObj.modifiedBy = sessionStorage.userId;
                        dataObj.id = selDocItem.idk;
                        dataObj.isDeleted = "0";
                        dataObj.userId = selDocItem.userId;
                        var dataUrl = ipAddress + "/provider/update";
                        createAjaxObject(dataUrl, dataObj, "POST", onUpdate, onError);
                    }

                }
                else{
                    if(strZip != ""){
                        ABBREVIATION_Patient = dataObj.abbreviation;
                        if (operation == ADD) {
                            var dataUrl = ipAddress + "/provider/create";
                            createAjaxObject(dataUrl, dataObj, "POST", onCreate, onError);
                        } else if (operation == UPDATE) {
                            dataObj.modifiedBy = sessionStorage.userId;
                            dataObj.id = selDocItem.idk;
                            dataObj.isDeleted = "0";
                            dataObj.userId = selDocItem.userId;
                            var dataUrl = ipAddress + "/provider/update";
                            createAjaxObject(dataUrl, dataObj, "POST", onUpdate, onError);
                        }
                    }
                    else{
                        $("html,body").animate({
                            scrollTop: 0
                        }, 500);
                        // $('.customAlert').append('<div class="alert alert-danger">Please fill all the Required Fields</div>');
                        customAlert.error("Error","Please fill all the Required Fields");
                    }
                }

            }else{
                if(strZip != ""){
                    ABBREVIATION_Patient = dataObj.abbreviation;
                    if (operation == ADD) {
                        var dataUrl = ipAddress + "/provider/create";
                        createAjaxObject(dataUrl, dataObj, "POST", onCreate, onError);
                    } else if (operation == UPDATE) {
                        dataObj.modifiedBy = sessionStorage.userId;
                        dataObj.id = selDocItem.idk;
                        dataObj.isDeleted = "0";
                        var dataUrl = ipAddress + "/provider/update";
                        createAjaxObject(dataUrl, dataObj, "POST", onUpdate, onError);
                    }
                }
                else{
                    // $("html,body").animate({
                    //     scrollTop: 0
                    // }, 500);
                    // $('.customAlert').append('<div class="alert alert-danger">Please fill all the Required Fields</div>');
                    customAlert.error("Error","Please fill all the Required Fields");
                }
            }

            // }
        }
        else {
            // $("html,body").animate({
            //     scrollTop: 0
            // }, 500);
            // $('.customAlert').append('<div class="alert alert-danger">Please fill all the Required Fields</div>');
            customAlert.error("Error","Please fill all the Required Fields");
        }
    }
    else{
        customAlert.error("Error",errorMessage);
    }


}
/*}*/
function onTypeChange() {

}
function onFilterChange() {
    $("#divType").hide();
    $("#divSearch").show();
    var cmbFilterByName = $("#cmbFilterByName").data("kendoComboBox");
    if (cmbFilterByName && cmbFilterByName.selectedIndex < 0) {
        cmbFilterByName.select(0);
    }
    if($("#cmbFilterByName").val() == "id" || $("#cmbFilterByName").val() == "user-id" || $("#cmbFilterByName").val() == "facility-id") {
        $("#cmbFilterByValue").removeAttr("disabled");
        $("#cmbFilterByValue").attr("type","number");
    }
    else if($("#cmbFilterByName").val() == "type"){
        $("#divType").show();
        $("#divSearch").hide();
    }
    else if($("#cmbFilterByName").val() == "All") {
        $("#dgridDAccountList").show();

        $("#cmbFilterByValue").attr("disabled","disabled");
        $("#providerFilter-btn").attr("disabled","disabled");
        $("#cmbFilterByValue").val('');
        buildAccountListGrid([]);
        getAjaxObject(ipAddress+"/provider/list?is-active=1&is-deleted=0","GET",getAccountList,onError);
    }
    else {
        $("#cmbFilterByValue").removeAttr("disabled");
        $("#cmbFilterByValue").attr("type","text");
    }
}

function onUpdate(dataObj){
    console.log(dataObj);
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            /*var obj = {};
            obj.status = "success";
            obj.operation = operation;
            popupClose(obj);*/
            // $('.customAlert').append('<div class="alert alert-success">'+dataObj.response.status.message+'</div>');
            onClickUploadPhoto();
            customAlert.error("Info", dataObj.response.status.message);
            $('.tabContentTitle').text('Update Provider');
            var id = selDocItem.idk;
            getAjaxObject(ipAddress+"/provider/list?id="+id,"GET",updateList,onError);
        } else {
            //$('.customAlert').append('<div class="alert alert-danger">'+dataObj.response.status.message+'</div>');
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}
function updateList(dataObj) {
    console.log(dataObj);
    if(dataObj.response.provider) {
        selDocItem = dataObj.response.provider[0];
        selDocItem.idk = selDocItem.id;
    }
    addDoctor("update");
}

function onError(errObj) {
    console.log(errObj);
    customAlert.error("Error", "Error");
}

function onClickSearch() {
    /*var obj = {};
    obj.status = "search";
    popupClose(obj);*/
}

function popupClose(obj) {
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}

function validateEmail(sEmail) {
    console.log(sEmail);
    var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    if (filter.test(sEmail)) {
        return true;
    }
    else {
        return false;
    }
}

function fncGetTypeValuebyId(Id){
    for (var i = 0; i < typeArr.length; i++) {
        var item = typeArr[i];
        if (item && item.Key == Id) {
            return item.Value;
        }
    }
    return "";
}


function onGetDate(DateOfBirth) {
    var dt = new Date(DateOfBirth);
    var strDT = "";
    if (dt) {

        var cntry = sessionStorage.countryName;
        if (cntry.indexOf("India") >= 0) {
            strDT = kendo.toString(dt, "dd/MM/yyyy");
        } else if (cntry.indexOf("United Kingdom") >= 0) {
            strDT = kendo.toString(dt, "dd/MM/yyyy");
        } else {
            strDT = kendo.toString(dt, "MM/dd/yyyy");
        }
    }
    return strDT;
}
var tabId;
function tabLink(id, current){
    tabId = id;
    //debugger;
    $("#divSelectDate").css("display", "none");
    $('.boxtablink').removeClass('boxtablink-active');
    //$(current).parent().removeClass('active');
    $('.tabContent').removeClass('tabContent-active');

    $('#' + id).addClass('tabContent-active');

    $(current).addClass('boxtablink-active');
    if(operation == "edit") {
        selItem = parentRef.selDocItem;
        // $("#divGPSReport").show();
        // $("#liCarePlan").show();
        // $("#liCareAvl").show();

        // onClickRPSearch();
        getPatientRosterAppointments();

        //getProviderAvailableTimings();
    }

    if(id == "tab6"){
        $("#divGeneral").css("display","none");
        //$(".tabContent").css("padding", "0");
        $(".iboxpopup-title").css("padding-bottom","5px");
        $(".groupAccountContainer").css("margin","5px auto 0");
        getLeaveStatus();
        // if(cntry.indexOf("India")>=0){
        //     $("#txtFDate").datepick({
        //         changeMonth: true,
        //         changeYear: true,
        //         dateFormat: 'dd/mm/yyyy 00:00',
        //         yearRange: "-200:+200"
        //     });
        //     $("#txtTDate").datepick({
        //         changeMonth: true,
        //         changeYear: true,
        //         dateFormat: 'dd/mm/yyyy 23:59',
        //         yearRange: "-200:+200"
        //     });
        // }else if(cntry.indexOf("United Kingdom")>=0){
        //     $("#txtFDate").datepick({
        //         changeMonth: true,
        //         changeYear: true,
        //         dateFormat: 'dd/mm/yyyy 00:00',
        //         yearRange: "-200:+200"
        //     });
        //     $("#txtTDate").datepick({
        //         changeMonth: true,
        //         changeYear: true,
        //         dateFormat: 'dd/mm/yyyy 23:59',
        //         yearRange: "-200:+200"
        //     });
        // }else{
        //     $("#txtFDate").datepick({
        //         changeMonth: true,
        //         changeYear: true,
        //         dateFormat: 'mm/dd/yyyy 00:00',
        //         yearRange: "-200:+200"
        //     });
        //     $("#txtTDate").datepick({
        //         changeMonth: true,
        //         changeYear: true,
        //         dateFormat: 'mm/dd/yyyy 23:59',
        //         yearRange: "-200:+200"
        //     });
        // }
        getLeaveTypes();
        getManagers();
        getEmails();
        buildDeviceListGrid([]);

    }
    else if(id == "tab5" ){
        $("#divGeneral").css("display","none");
    }
    else if (id == "tab4") {

        onClickRPSearch();
        $(".rosterappointmentTableBlock").css("display", "block");
        $(".rosterappointmentReportTableBlock").css("display", "block");
        $("#divGeneral").css("display","block");
        $("#btnSave").hide();
        $("#btnReset").hide();
    }
    else if(id=="tab1"){
        $("#divGeneral").css("display","block");
        $("#btnSave").show();
        $("#btnReset").show();
    }
    else if(id=="tab2"){
        $("#btnSave").show();
        $("#btnReset").show();
        $("#divGeneral").css("display","block");
    }
    else if (id == "tab7") {
        $(".rosterappointmentTableBlock").css("display", "none");
        $(".rosterappointmentReportTableBlock").css("display", "none");
        $("#divSelectDate").css("display", "block");
        $("#divTable").empty();
        $("#divCareerTable").empty();
        $('#tab4').addClass('tabContent-active');
        //onClickRotaSearch();
        $("#divGeneral").css("display", "block");
        $("#btnSave").hide();
        $("#btnReset").hide();
    }
    else{
        $("#btnSave").show();
        $("#btnReset").show();
        $("#divGeneral").css("display","block");
    }
}

function handleGetVacationList(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var vacation = [];
            if(dataObj.response.vacations){
                if($.isArray(dataObj.response.vacations)){
                    vacation = dataObj.response.vacations;
                }else{
                    vacation.push(dataObj.response.vacations);
                }
            }
            for(var j=0;j<vacation.length;j++){
                // vacation[j].FD = kendo.toString(new Date(vacation[j].fromDate),"dd/MM/yyyy h:mm tt");
                // vacation[j].TD = kendo.toString(new Date(vacation[j].toDate),"dd/MM/yyyy h:mm tt");
                vacation[j].FD = GetDateTimeEdit(vacation[j].fromDate);
                vacation[j].TD = GetDateTimeEdit(vacation[j].toDate);
                if(vacation[j].acceptedFromDate != null) {
                    // vacation[j].approvedFrom = kendo.toString(new Date(vacation[j].acceptedFromDate), "dd/MM/yyyy h:mm tt");
                    vacation[j].approvedFrom = GetDateTimeEdit(vacation[j].acceptedFromDate);

                }
                if(vacation[j].acceptedToDate != null) {
                    // vacation[j].approvedTo = kendo.toString(new Date(vacation[j].acceptedToDate), "dd/MM/yyyy h:mm tt");
                    vacation[j].approvedTo = GetDateTimeEdit(vacation[j].acceptedToDate);
                }
                vacation[j].leaveStatus = onGetLeaveStatus(vacation[j].leaveStatusId);
                vacation[j].leaveType = onGetLeaveType(vacation[j].leaveTypeId);
                vacation[j].idk = vacation[j].id;
            }
            buildDeviceListGrid(vacation);
        }
    }
}

var isBrowseFlag = false;
function onClickBrowse(e) {
    $("#btnBrowse").addClass("selClass");
    isBrowseFlag = true;
    $("#video").hide();
    $("#canvas").hide();
    $("#imgPhoto").show();
    if (e.currentTarget && e.currentTarget.id) {
        var btnId = e.currentTarget.id;
        var fileTagId = ("fileElem" + btnId);
        $("#fileElem").click();
    }
}

var fileName = "";
var files = null;
function onSelectionFiles(event) {
    //var imageCompressor = new ImageCompressor();
    console.log(event);
    files = event.target.files;
    fileName = "";
    if (files) {
        if (files.length > 0) {
            fileName = files[0].name;
            var oFReader = new FileReader();
            oFReader.readAsDataURL(files[0]);
            oFReader.onload = function(oFREvent) {
                document.getElementById("imgPhoto").src = oFREvent.target.result;

            }
        }
    }
}
function onClickUploadPhoto() {
    // if (operation == "edit") {
    removeSelectedButtons();
    $("#btnUpload").addClass("selClass");
    if (isBrowseFlag) {
        //imagePhotoData
        var reqUrl = ipAddress + "/homecare/upload/providers/photo/?id=" + selDocItem.idk + "&access_token=" + sessionStorage.access_token+"&tenant="+sessionStorage.tenant;
        // var reqUrl = ipAddress + "/upload/providers/photo/stream/?patient-id=" + patientId + "&photo-ext=png&access_token=" + sessionStorage.access_token+"&tenant="+sessionStorage.tenant
        /* var xmlhttp = new XMLHttpRequest();
         xmlhttp.open("POST", reqUrl, true);
         //xmlhttp.open("POST", "/ROOT/upload/patient/photo/?patient-id=101&photo-ext=png", true);
         xmlhttp.send(imagePhotoData);
         xmlhttp.onreadystatechange = function() {//Call a function when the state changes.
             if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                 customAlert.error("Info", "Image uploaded successfully");
             }
         }*/

        var formData = new FormData();
        formData.append("desc", fileName);
        formData.append("file", files[0]);

        Loader.showLoader();
        $.ajax({
            url: reqUrl,
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false, // "multipart/form-data",
            success: function(data, textStatus, jqXHR) {
                if (typeof data.error === 'undefined') {
                    Loader.hideLoader();
                    submitForm(data);
                } else {
                    Loader.hideLoader();
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                Loader.hideLoader();
            }
        });
    } else {
        var canvas = document.getElementById('canvas');
        var dataURL = canvas.toDataURL("image/png");

        var reqUrl = ipAddress + "/upload/patient/photo/stream/?patient-id=" + patientId + "&photo-ext=png&access_token=" + sessionStorage.access_token+"&tenant="+sessionStorage.tenant;
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open("POST", reqUrl, true);
        //xmlhttp.open("POST", "/ROOT/upload/patient/photo/?patient-id=101&photo-ext=png", true);
        xmlhttp.send(dataURL);
        xmlhttp.onreadystatechange = function() {//Call a function when the state changes.
            if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                // customAlert.error("Info", "Image uploaded successfully");
            }
        }
    }
    // }
}
function submitForm(dataObj) {
    if(dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        // customAlert.error("Info", "Image uploaded successfully");
    }else{
        customAlert.error("Error", dataObj.response.status.message);
    }
}

function onGetPatientInfo(dataObj) {
    var cntry = sessionStorage.countryName;
    patientInfoObject = selDocItem;
    // patientInfoObject = dataObj.response.provider[0];
    parentRef.type = patientInfoObject.type;
    parentRef.userId = patientInfoObject.userId;
    var communicationLen = patientInfoObject.communications != null ? patientInfoObject.communications.length : 0;
    for(var i=0; i<communicationLen; i++) {
        if(patientInfoObject.communications[i].parentTypeId == 500) {
            cityIndexVal = i;
        }
    }
    $("#txtID").html("<b>ID: </b>"+patientInfoObject.idk+'    '+patientInfoObject.lastName+'  '+patientInfoObject.firstName);
    $("#txtExtID1").val(patientInfoObject.externalId1);
    $("#txtExtID2").val(patientInfoObject.externalId2);
    // $("#cmbPrefix").val(patientInfoObject.prefix);
    if(patientInfoObject.prefix != ""){
        // $("#cmbPrefix option:contains(" + patientInfoObject.prefix + ")").attr('selected', 'selected');
        var cmbPrefix = onGetPrefix(patientInfoObject.prefix);
        $("#cmbPrefix").val(cmbPrefix);
    }
    parentRef.firstName = patientInfoObject.firstName;
    parentRef.lastName = patientInfoObject.lastName;
    parentRef.middleName = patientInfoObject.middleName;
    $("#txtFN").val(patientInfoObject.firstName);
    $("#txtLN").val(patientInfoObject.lastName);
    $("#txtMN").val(patientInfoObject.middleName);
    $("#txtWeight").val(patientInfoObject.weight);
    $("#txtHeight").val(patientInfoObject.height);
    // $("#txtNN").val(patientInfoObject.contactNickName);
    $("#txtAbbreviation").val(patientInfoObject.abbreviation);
    /*$("#txtN").val(patientInfoObject.name);
    $("#txtDN").val(patientInfoObject.displayName);
    $("#txtFTI").val(patientInfoObject.federalTaxId);
    $("#txtFTEIN").val(patientInfoObject.federalTaxEin);
    $("#txtSTI").val(patientInfoObject.stateTaxId);
    $("#txtNOTT").val(patientInfoObject.taxName);*/
    /*getComboListIndex("cmbFTIT", "desc", patientInfoObject.federalTaxIdType);*/
    $("#cmbStatus").val(patientInfoObject.isActive);
    // getComboListIndex("cmbStatus", "desc", patientInfoObject.Status);
    // $("#cmbSuffix").val(patientInfoObject.suffix);
    if(patientInfoObject.suffix != "" ){
        // $("#cmbSuffix option:contains(" + patientInfoObject.suffix + ")").attr('selected', 'selected');

        var cmbSuffix = onGetSuffix(patientInfoObject.suffix);
        $("#cmbSuffix").val(cmbSuffix);
    }

    // getComboListIndex("cmbSuffix", "value", patientInfoObject.suffix);
    $("#txtType").val(patientInfoObject.type);
    // getComboListIndex("txtType", "Key", patientInfoObject.type);
    if(operation == "edit") {
        getProviderAvailableTimings();
    }
    //  $("#txtSMS").val(comObj.sms);
    //getComboListIndex("cmbSMS", "desc", patientInfoObject.sms);


    if(patientInfoObject.communications != null) {
        $("#cityIdHiddenValue").val(patientInfoObject.communications[cityIndexVal].cityId);
        $("#txtCountry").val(patientInfoObject.communications[cityIndexVal].country);

        $("#txtZip4").val(patientInfoObject.communications[cityIndexVal].zipFour);
        $("#txtState").val(patientInfoObject.communications[cityIndexVal].state);
        if (cntry.indexOf("United Kingdom") >= 0) {
            if(IsPostalCodeManual == "1"){
                $("#cmbZip").val(patientInfoObject.communications[cityIndexVal].houseNumber);
            }
            else{
                $("#cmbZip").val(patientInfoObject.communications[cityIndexVal].zip);
            }
        }
        else{
            $("#cmbZip").val(patientInfoObject.communications[cityIndexVal].zip);
        }
        $("#txtCity").val(patientInfoObject.communications[cityIndexVal].city);
        $("#txtHPhone").val(patientInfoObject.communications[cityIndexVal].homePhone);
        $("#txtExtension1").val(patientInfoObject.communications[cityIndexVal].workPhoneExt);
        $("#txtAdd1").val(patientInfoObject.communications[cityIndexVal].address1);
        $("#txtAdd2").val(patientInfoObject.communications[cityIndexVal].address2);
        $("#txtWPhone").val(patientInfoObject.communications[cityIndexVal].workPhone);
        // $("#txtExtension1").val(patientInfoObject.communications[cityIndexVal].homePhoneExt);
        $("#txtFax").val(patientInfoObject.communications[cityIndexVal].fax);
        $("#txtCell").val(patientInfoObject.communications[cityIndexVal].cellPhone);
        // $("#cmbSMS").val(patientInfoObject.communications[cityIndexVal].sms);
        if(patientInfoObject.communications[cityIndexVal].sms != "") {
            // $("#cmbSMS option:contains(" + patientInfoObject.sms + ")").attr('selected', 'selected');
            if(patientInfoObject.communications[cityIndexVal].sms == "Yes"){
                $("#cmbSMS").val(1);
            }else if(patientInfoObject.communications[cityIndexVal].sms == "No"){
                $("#cmbSMS").val(2);
            }
        }

        // getComboListIndex("cmbSMS", "desc", patientInfoObject.communications[cityIndexVal].sms);
        $("#txtEmail").val(patientInfoObject.communications[cityIndexVal].email);
        getComboListIndex("cmbZip", "idk", patientInfoObject.communications[cityIndexVal].cityId);
        zipSelItem = patientInfoObject.communications[cityIndexVal];
        zipSelItem.idk = patientInfoObject.communications[cityIndexVal].id;
        radioValue = patientInfoObject.communications[cityIndexVal].defaultCommunication;
        if (patientInfoObject.communications[cityIndexVal].defaultCommunication == "1") {
            $("#rdHome").prop("checked", true);
        } else if (patientInfoObject.communications[cityIndexVal].defaultCommunication == "2") {
            $("#rdWork").prop("checked", true);
        } else if (patientInfoObject.communications[cityIndexVal].defaultCommunication == "3") {
            $("#rdMobile").prop("checked", true);
        } else if (patientInfoObject.communications[cityIndexVal].defaultCommunication == "4") {
            $("#rdEmail").prop("checked", true);
        }

    }

    $("#txtFC").val(patientInfoObject.financeCharges);
    $("#txtBD").val(patientInfoObject.billableDoctor);
    $("#txtPCP").val(patientInfoObject.pcp);
    $("#txtTI").val(patientInfoObject.taxonomyId);
    $("#txtNPI").val(patientInfoObject.npi);
    $("#txtDEA").val(patientInfoObject.dea);
    $("#txtUPIN").val(patientInfoObject.upin);
    $("#txtNT").val(patientInfoObject.nuucType);
    $("#txtAS").val(patientInfoObject.amaSpeciality);
    $("#txtNEIC").val(patientInfoObject.neicSpeciality);
    $("#txtBAN").val(patientInfoObject.billingAccountName);

    // $("#cmbGender").text(patientInfoObject.gender);

    if(patientInfoObject.gender == "Male"){
        $("#cmbGender").val(1);
    }else if(patientInfoObject.gender == "Female"){
        $("#cmbGender").val(2);
    }else if(patientInfoObject.gender == "Transgender"){
        $("#cmbGender").val(42);
    }

    // $("#cmbGender option:contains(" + patientInfoObject.gender + ")").attr('selected', 'selected');

    // $("#cmbGender option").each(function() {
    //     if($(this).text().trim() == patientInfoObject.gender.trim()) {
    //         $(this).attr('selected', 'selected');
    //     }
    // });
    // getComboListIndex("cmbGender", "desc", patientInfoObject.gender);
    // onGenderChange();
    var imageServletUrl = ipAddress + "/homecare/download/providers/photo/?" + Math.round(Math.random() * 1000000)+"&access_token="+sessionStorage.access_token+"&tenant="+sessionStorage.tenant+"&id="+patientInfoObject.idk;
    $("#imgPhoto").attr("src", imageServletUrl);
    // $("#cmbEthnicity").val(patientInfoObject.ethnicity);
    if(patientInfoObject.ethnicity != "") {
        // $("#cmbEthnicity option:contains(" + patientInfoObject.ethnicity + ")").attr('selected', 'selected');

        var cmbEthnicity = onGetEthnicity(patientInfoObject.ethnicity);
        $("#cmbEthnicity").val(cmbEthnicity);
    }
    // getComboListIndex("cmbEthnicity", "desc", patientInfoObject.ethnicity);
    // $("#cmbRace").txt = patientInfoObject.race;
    if(patientInfoObject.race != "") {
        // $("#cmbRace option:contains(" + patientInfoObject.race + ")").attr('selected', 'selected');

        var cmbRace = onGetrace(patientInfoObject.race);
        $("#cmbRace").val(cmbRace);
    }
    // getComboListIndex("cmbRace", "desc", patientInfoObject.race);
    getComboListMultipleIndex("cmbLang", "desc", patientInfoObject.language);
    // $("#cmbReligion").val(patientInfoObject.religion);
    if(patientInfoObject.religion != "") {
        // $("#cmbReligion option:contains(" + patientInfoObject.religion + ")").attr('selected', 'selected');

        var cmbReligion = onGetreligion(patientInfoObject.religion);
        // cmbReligion = $('#cmbReligion option:selected').val();
        $("#cmbReligion").val(cmbReligion);
    }
    // getComboListIndex("cmbReligion", "desc", patientInfoObject.religion);
    $("#txtLikes").val(patientInfoObject.likes);
    $("#txtDisLikes").val(patientInfoObject.dislikes);
    $("#txtSkills").val(patientInfoObject.skills);
    $("#txtHR").val(patientInfoObject.hourlyRate);
    $("#txtLeavesPerYear").val(patientInfoObject.leavesPerYear);
    $("#txtOverTimeHourRate").val(patientInfoObject.overtimeHourlyRate);
    $("#txtWeeklyContractHour").val(patientInfoObject.weekelyContractHours);
    // $("#cmbEmploymentType").val(patientInfoObject.employmentTypeId);
    getComboListIndex("cmbEmploymentType", "Value", patientInfoObject.employmentTypeId);


    var dtEmpStartDate = $("#dtEmpStartDate").data("kendoDatePicker");
    if (dtEmpStartDate) {
        if(patientInfoObject.employmentStartDate != null) {
            dtEmpStartDate.value(GetDateTimeEdit(patientInfoObject.employmentStartDate));
        }
        else{
            dtEmpStartDate.value("");
        }
    }


    var dtEmpEndDate = $("#dtEmpEndDate").data("kendoDatePicker");
    if (dtEmpEndDate) {
        if(patientInfoObject.employmentEndDate != null) {
            dtEmpEndDate.value(GetDateTimeEdit(patientInfoObject.employmentEndDate));
        }
        else{
            dtEmpEndDate.value("");
        }
    }

    // patientInfoObject.travelExpenses == "1" ? $('#txtTE').attr('checked','true') : null;
    if(patientInfoObject.travelExpenses == "1" ){
        $('#txtTE').prop('checked',true);
    }
    else{
        $('#txtTE').prop('checked',false);
    }
    $('#txtHRperMile').val(patientInfoObject.ratePerMile);
    if (patientInfoObject.dateOfBirth) {
        var dt = new Date(patientInfoObject.dateOfBirth);
        if (dt) {
            var strDT = "";
            var cntry = sessionStorage.countryName;
            if (cntry.indexOf("India") >= 0) {
                strDT = kendo.toString(dt, "dd/MM/yyyy");
            } else if (cntry.indexOf("United Kingdom") >= 0) {
                strDT = kendo.toString(dt, "dd/MM/yyyy");
            } else {
                strDT = kendo.toString(dt, "MM/dd/yyyy");
            }

            $("#dtDOB1").datepicker();
            $("#dtDOB1").datepicker("setDate", strDT);
            var strAge = getAge(dt);
            $("#txtAge").val(strAge);
        }
    }
    $("#txtFAN").val(patientInfoObject.facilityId);
    // getComboListIndex("txtFAN", "idk", patientInfoObject.facilityId);
    onFacilityChange();
    getAjaxObject(ipAddress+"/homecare/qualifications/?parentId="+ patientInfoObject.idk + "&parentTypeId="+patientInfoObject.type,"GET",getQualificationListData,onError);
}

function getProviderAvailableTimings(){
    onClickTMReset();
    // buildPatientRosterList1([]);
    var txtType = $("#txtType").data("kendoComboBox");
    if (txtType) {
        var txtId = txtType.value();
        parentRef.cid = txtId;

        var patientRosterURL = ipAddress+"/homecare/availabilities/?parent-id="+selItem.idk+"&parent-type-id="+txtId+"&is-active=1&is-deleted=0";
        getAjaxObject(patientRosterURL,"GET",onPatientAvalableTimings,onError);
    }

}

// function onClickTMReset(){
//     opr = "add";
//     $("#btnAEdit").prop("disabled", true);
//     $("#btnADel").prop("disabled", true);
// }


function onLeaveChange(){
    setTimeout(function() {
        var selectedItems = angularLeaveUIgridWrapper.getSelectedRows();
        if (selectedItems && selectedItems.length > 0) {
            if (selectedItems[0].approvedBy != null) {
                $("#btnLeaveEdit").prop("disabled", true);
                $("#btnLeaveDelete").prop("disabled", true);
            }
            else {
                $("#btnLeaveEdit").prop("disabled", false);
                $("#btnLeaveDelete").prop("disabled", false);
            }
        }

    });
}


function getLeaveTypes(){
    getAjaxObject(ipAddress+"/homecare/leave-types/?fields=id,value","GET",handleGetLeaveTypes,onError);
}

var leaveTypes = [];
function handleGetLeaveTypes(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            if($.isArray(dataObj.response.leaveTypes)){
                leaveTypes = dataObj.response.leaveTypes;
            }else{
                leaveTypes.push(dataObj.response.leaveTypes);
            }
            for(var i=0;i<leaveTypes.length;i++){
                leaveTypes[i].idk = leaveTypes[i].id;
                $("#cmbLType").append('<option value="'+leaveTypes[i].idk+'">'+leaveTypes[i].value+'</option>');
            }
        }
    }
}

function getEmails(){
    getAjaxObject(ipAddress+"/homecare/tenant-user-emails/?user-id="+ Number(parentRef.userId),"GET",handleGetEmails,onError);
}

function handleGetEmails(dataObj){
    var userEmailArray = [];
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            if($.isArray(dataObj.response.userEmails)){
                userEmailArray = dataObj.response.userEmails;
            }else{
                userEmailArray.push(dataObj.response.userEmails);
            }
            for(var i=0;i<userEmailArray.length;i++){
                if(userEmailArray[i].id == Number(parentRef.userId)){
                    $("#txtFMail").val(userEmailArray[i].email);
                }
                else{
                    $("#txtTMail").val(userEmailArray[i].email);
                }
            }
        }
    }
}



function getManagers(){
    getAjaxObject(ipAddress+"/homecare/tenant-users/?is-active=1&is-deleted=0&id="+Number(parentRef.userId),"GET",getUserList,onError);
}
function getUserList(dataObj){
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            if($.isArray(dataObj.response.tenantUsers)){
                dataArray = dataObj.response.tenantUsers;
            }else{
                dataArray.push(dataObj.response.tenantUsers);
            }
        }
    }

    if(dataArray.length > 0 && dataArray[0] != undefined) {
        for (var i = 0; i < dataArray.length; i++) {
            dataArray[i].idk = dataArray[i].id;
            dataArray[i].Status = "InActive";
            if (dataArray[i].isActive == 1) {
                dataArray[i].Status = "Active";
            }
        }
        if (dataArray.length > 0) {
            getAjaxObject(ipAddress + "/homecare/tenant-users/?is-active=1&is-deleted=0&id=" + Number(dataArray[0].parentId), "GET", getManagersList, onError);
        }
    }
    else{
        $("#btnLeaveAdd").prop("disabled", true);
        $("#btnLeaveEdit").prop("disabled", true);
        $("#btnLeaveDelete").prop("disabled", true);
        customAlert.info("Info","User is not assigned please contact administrator");
    }
}

function getManagersList(dataObj){
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            if($.isArray(dataObj.response.tenantUsers)){
                dataArray = dataObj.response.tenantUsers;
            }else{
                dataArray.push(dataObj.response.tenantUsers);
            }
        }
    }

    if(dataArray[0] != undefined && dataArray.length > 0) {
        for (var i = 0; i < dataArray.length; i++) {
            dataArray[i].idk = dataArray[i].id;
            dataArray[i].Status = "InActive";
            if (dataArray[i].isActive == 1) {
                dataArray[i].Status = "Active";
            }
            $("#cmbManager").append('<option value="' + dataArray[i].idk + '">' + dataArray[i].lastName + ' ' + dataArray[i].firstName + '</option>');
        }
    }
    else{
        $("#btnLeaveAdd").prop("disabled", true);
        $("#btnLeaveEdit").prop("disabled", true);
        $("#btnLeaveDelete").prop("disabled", true);
        customAlert.info("Info","Manager is not assigned please contact administrator");
    }
}

function searchOnLoadLeave(status) {
    buildDeviceListGrid([]);
    if(status == "active") {
        var urlExtn = ipAddress+"/homecare/vacations/?fields=*&parentTypeId="+Number(parentRef.type)+"&parentId="+Number(selDocItem.idk)+"&is-active=1&is-deleted=0";

    }
    else if(status == "inactive") {
        var urlExtn =  ipAddress+"/homecare/vacations/?fields=*&parentTypeId="+Number(parentRef.type)+"&parentId="+Number(selDocItem.idk)+"&is-active=0&is-deleted=1"
    }
    getAjaxObject(urlExtn,"GET",handleGetVacationList,onError);
}

function buildDeviceListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "From Date Time",
        "field": "FD",
    });
    gridColumns.push({
        "title": "To Date Time",
        "field": "TD",
    });
    gridColumns.push({
        "title": "Leave Type",
        "field": "leaveType",
        "width": "12%"
    });
    gridColumns.push({
        "title": "Reason",
        "field": "reason",
        "width": "12%"
    });
    gridColumns.push({
        "title": "Status",
        "field": "leaveStatus",
        "width": "10%"
    });
    gridColumns.push({
        "title": "Approved From Date Time",
        "field": "approvedFrom",
        "width": "18%"
    });
    gridColumns.push({
        "title": "Approved To Date Time",
        "field": "approvedTo",
        "width": "18%"
    });
    angularLeaveUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
}

function onClickLeaveSave(){
    //var txtFDate = $("#txtFDate").val();
    //var txtTDate = $("#txtTDate").val();
    var cmbLType = $("#cmbLType").val();

    var txtFDate = $("#txtFDate").data("kendoDateTimePicker");
    var fDate = new Date(txtFDate.value());

    var txtTDate = $("#txtTDate").data("kendoDateTimePicker");
    var tDate = new Date(txtTDate.value());
    var IsFlag = false;

    if (txtFDate != "" && txtTDate != "" && cmbLType != "") {

        if (fDate.getTime() > tDate.getTime()) {
            IsFlag = true;
            customAlert.error("Error", "From Date should be less than To Date.");
        }

        if ((!IsFlag)) {

            var reqData = {};
            var subject = selDocItem.idk + ' ' + selDocItem.lastName + ' ' + selDocItem.firstName + ': ' + $("#txtFDate").val() + '  - ' + $("#txtTDate").val();
            if ($("#txtReason").val() != "") {
                reqData.reason = $("#txtReason").val();
            }
            else {
                reqData.reason = null;
            }

            reqData.subject = subject;
            reqData.subject = subject;

            reqData.toDate = tDate.getTime(); //GetDateTimeLeave("txtTDate");
            reqData.fromDate = fDate.getTime(); //GetDateTimeLeave("txtFDate");
            reqData.fromEmail = $("#txtFMail").val();
            reqData.toEmail = $("#txtTMail").val();
            reqData.leaveTypeId = $("#cmbLType").val();
            reqData.leaveStatusId = 1;
            reqData.parentTypeId = selDocItem.type;
            reqData.parentId = selDocItem.idk;
            reqData.managerIds = $("#cmbManager").val();
            reqData.isActive = 1;
            reqData.isDeleted = 0;
            var method = "POST";
            if (operation == UPDATE) {
                var selectedItems = angularLeaveUIgridWrapper.getSelectedRows();
                reqData.id = selectedItems[0].idk;
                reqData.createdBy = selectedItems[0].createdBy;
                reqData.modifiedBy = Number(sessionStorage.userId);
                method = "PUT";
            }
            else {
                reqData.createdBy = Number(sessionStorage.userId);
            }
            dataUrl = ipAddress + "/homecare/vacations/";
            createAjaxObject(dataUrl, reqData, method, onLeaveCreate, onError);
        }

    }
    else{
        customAlert.error("error", "Please fill the Required Fields");
    }
}

function onLeaveCreate(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "Leave created successfully";
            if(operation == UPDATE){
                msg = "Leave updated successfully"
            }
            displaySessionErrorPopUp("Info", msg, function(res) {
                onClickLeaveReset();
                buildDeviceListGrid([]);
                $(".btnLeaveActive").click();
                onClickLeaveCancel();
            })
        }else{
            customAlert.error("Error", dataObj.response.status.message);
        }
    }
}

function onClickLeaveReset() {
    $("#txtReason").val("");
    $("#txtFDate").val("");
    $("#txtTDate").val("");
    $("#txtLeaveID").html("");
    operation = ADD;
}



function onClickLeaveEdit() {

    $("#txtFDate").kendoDateTimePicker({ format: appointmentdtFMT, interval: 15 });
    $("#txtTDate").kendoDateTimePicker({ format: appointmentdtFMT, interval: 15 });

    $("#txtID").show();
    $(".filter-heading").html("Edit Staff Leave Request");
    parentRef.operation = "edit";
    operation =  "edit";
    $("#viewLeaveDivBlock").hide();
    $("#addPopup").show();
    var selectedItems = angularLeaveUIgridWrapper.getSelectedRows();
    if (selectedItems && selectedItems.length > 0) {
        var obj = selectedItems[0];
        $("#txtLeaveID").html("ID : " + obj.idk);
        $("#cmbManager").val(obj.managerIds);
        $("#cmbLeaveType").val(obj.leaveTypeId);
        $("#txtReason").val(obj.reason);

        // $("#txtFDate").datepick();
        // $("#txtFDate").datepick("setDate", GetDateTimeEdit(obj.fromDate));

        var txtFDate = $("#txtFDate").data("kendoDateTimePicker");
        var txtTDate = $("#txtTDate").data("kendoDateTimePicker");

        txtFDate.value(GetDateTimeEdit(obj.fromDate));
        txtTDate.value(GetDateTimeEdit(obj.toDate));

        //$("#txtFDate").val(GetDateTimeEditLeave(obj.fromDate));
        //$("#txtTDate").val(GetDateTimeEditLeave(obj.toDate));

        // $("#txtTDate").datepick();
        // $("#txtTDate").datepick("setDate", GetDateTimeEdit(obj.toDate));
    }
}

function onClickLeaveDelete(){
    customAlert.confirm("Confirm", "Are you sure to delete?",function(response){
        if(response.button == "Yes"){
            var dataObj = {};
            dataObj.createdBy = Number(sessionStorage.userId);;
            dataObj.isDeleted = 1;
            dataObj.isActive = 0;
            var dataUrl = ipAddress +"/homecare/vacations/";
            var method = "DELETE";
            var selectedItems = angularLeaveUIgridWrapper.getSelectedRows();
            dataObj.id = selectedItems[0].idk;
            createAjaxObject(dataUrl, dataObj, method, onCreateDelete, onError);
        }
    });
}

function onClickLeaveAdd() {
    $("#txtFDate").kendoDateTimePicker({ format: appointmentdtFMT, interval: 15 });
    $("#txtTDate").kendoDateTimePicker({ format: appointmentdtFMT, interval: 15 });
    $("#addPopup").show();
    $('#viewLeaveDivBlock').hide();
    $("#txtID").hide();
    $(".filter-heading").html("Add Staff Leave Request");
    parentRef.operation = "add";
    onClickLeaveReset();
}

function onClickLeaveCancel(){
    $(".filter-heading").html("View Staff Leave Request");
    $("#viewLeaveDivBlock").show();
    $("#addPopup").hide();
    $(".groupAccountContainer").css("margin","12px auto 0");
    $("#btnLeaveReset").click();
}

function onNotesChange(){
    var cmbNotes = $("#cmbNotes").data("kendoComboBox");
    if(cmbNotes){
        if(cmbNotes.selectedIndex<0){
            cmbNotes.select(0);
        }
    }
}

function displayLocalNames(){
    var cntry = sessionStorage.countryName;
    if(cntry.indexOf("India")>=0){
        $('.postalCode').html('Postal Code : <span class="mandatoryClass">*</span>');
        $('.stateLabel').text("State:");
        $('.zipFourWrapper').hide();
    }else if(cntry.indexOf("United Kingdom")>=0){
        $('.postalCode').html('Postal Code : <span class="mandatoryClass">*</span>');
        $('.stateLabel').text("County:");
        $('.zipFourWrapper').hide();
    }else if(cntry.indexOf("United State")>=0){
        $('.postalCode').html('Zip : <span class="mandatoryClass">*</span>');
        $('.stateLabel').text("State:");
        $('.zipFourWrapper').show();
    }else{
        $('.postalCode').html('Zip : <span class="mandatoryClass">*</span>');
        $('.stateLabel').text("State:");
        $('.zipFourWrapper').show();
    }
}

function getWeekDayName(wk){
    var wn = "";
    if(wk == 1){
        return "Mon";
    }else if(wk == 2){
        return "Tue";
        // }else if(wk == 2){
        //     return "Tue";
    }else if(wk == 3){
        return "Wed";
    }else if(wk == 4){
        return "Thu";
    }else if(wk == 5){
        return "Fri";
    }else if(wk == 6){
        return "Sat";
    }else if(wk == 0){
        return "Sun";
    }
    return wn;
}
function buildPatientRosterList(dataSource){
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    otoptions.rowHeight = 100;
    gridColumns.push({
        "title": "Contract",
        "field": "contract",
        "width": "15%",
    });
    gridColumns.push({
        "title": "DOW",
        "field": "dowK",
        "width": "10%",
        "cellTemplate": '<div class="ui-grid-cell-contents" >{{row.entity.dow}}</div>'
    });
    gridColumns.push({
        "title": "Start Time",
        "field": "fromTime",
        "width": "10%",
    });
    gridColumns.push({
        "title": "End Time",
        "field": "toTime",
        "width": "10%",
    });
    gridColumns.push({
        "title": "Duration",
        "field": "duration",
        "width": "10%",
    });
    gridColumns.push({
        "title": "Reason",
        "field": "reason",
        "width": "30%",
    });
    gridColumns.push({
        "title": "Client",
        "field": "patient",
        "width": "20%",
    });
    gridColumns.push({
        "title": "Facility",
        "field": "facility",
        "width": "15%",
    });
    gridColumns.push({
        "title": "Notes",
        "field": "notes",
        "width": "30%",
    });

    angularPTUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
}
function buildPatientRosterList1(dataSource){
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    otoptions.rowHeight = 100;
    gridColumns.push({
        "title": "Day of Week",
        "field": "dayName",
    });
    gridColumns.push({
        "title": "Start Time",
        "field": "ST",
    });
    gridColumns.push({
        "title": "End Time",
        "field": "ET",
    });
    angularPTUIgridWrapper1.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();

}
function onPTChange1(){
    setTimeout(function(){
        var selectedItems = angularPTUIgridWrapper1.getSelectedRows();
        console.log(selectedItems);
        if(selectedItems && selectedItems.length>0){
            $("#btnAEdit").prop("disabled", false);
            $("#btnADel").prop("disabled", false);
        }else{
            $("#btnAEdit").prop("disabled", true);
            $("#btnADel").prop("disabled", true);
        }
    },100)
}
function onPTChange(){
    setTimeout(function(){
        var selectedItems = angularPTUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if(selectedItems && selectedItems.length>0){
            $("#btnREdit").prop("disabled", false);
            $("#btnRDel").prop("disabled", false);
        }else{
            $("#btnREdit").prop("disabled", true);
            $("#btnRDel").prop("disabled", true);
        }
    },100)
}
function showOperations(){
    var node = '<div style="text-align:center"><span style="cursor:pointer;font-weight:bold;font-size:15px">Edit</span>&nbsp;&nbsp;<span style="cursor:pointer;font-weight:bold;font-size:15px">Delete</span>';
    node = node+"</div>";
    return node;
}
function showPatientRoster(){
    var txtFAN = $("#txtFAN").data("kendoComboBox");
    if(txtFAN){
        parentRef.fid = txtFAN.value();
        parentRef.fname = txtFAN.text();
    }
    parentRef.txtID = $("#txtID").val();

}

function onClickGPSReport(){
    parentRef.screenType = "providers";
    parentRef.providerid = selDocItem.idk;
    openReportPopup("../../html/patients/geoReport.html","GPS Report");
}
function openReportPopup(path,title){
    var popW = "90%";
    var popH = "85%";

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = title;
    devModelWindowWrapper.openPageWindow(path, profileLbl, popW, popH, false, closeCallReport);
}
function closeCallReport(evt,re){

}
function onClickAddRoster(){
    //angularPTUIgridWrapper.insert({},0);
    var popW = "60%";
    var popH = "60%";
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Create Roster";
    parentRef.RS = "create";
    parentRef.selRSItem = null;
    showPatientRoster();
    devModelWindowWrapper.openPageWindow("../../html/patients/createRoster.html", profileLbl, popW, popH, true, closePatientRoster);
}
function getDataGridArray(){
    var allRows = angularPTUIgridWrapper.getAllRows();
    var items = [];
    for(var a=0;a<allRows.length;a++){
        var row = allRows[a].entity;
        items.push(row);
    }
    return items;
}
function closePatientRoster(evt,returnData){
    console.log(returnData);
    if(returnData && returnData.status == "success"){
        var obj = returnData;
        var st = returnData.rs;
        if(st == "create"){
            var strDOW = obj.dow1;
            var strDOWK = obj.dowK1;
            var dArray = [];
            for(var s=0;s<strDOW.length;s++){
                //var item = strDOW[s];
                obj.dow = strDOW[s];
                obj.dowK = strDOWK[s];
                var rdItem =   JSON.parse(JSON.stringify(obj));
                dArray.push(rdItem);
            }

            var dItems = getDataGridArray();
            dArray = dArray.concat(dItems);
            buildPatientRosterList([]);
            setTimeout(function(){
                buildPatientRosterList(dArray);
                angularPTUIgridWrapper.refreshGrid();
            },100);
        }else{
            var selectedItems = angularPTUIgridWrapper.getSelectedRows();
            var dgItem = selectedItems[0];
            dgItem.contract = obj.contract;
            dgItem.contractK = obj.contractK;

            dgItem.facility = obj.facility;
            dgItem.facilityK = obj.facilityK;

            dgItem.provider = obj.provider;
            dgItem.providerK = obj.providerK;

            dgItem.fromTime = obj.fromTime;
            dgItem.fromTimeK = obj.fromTimeK;

            dgItem.toTime = obj.toTime;
            dgItem.toTimeK = obj.toTimeK;

            dgItem.duration = obj.duration;

            dgItem.AppType = obj.AppType;
            dgItem.AppTypeK = obj.AppTypeK;

            dgItem.reason = obj.reason;
            dgItem.reasonK = obj.reasonK;

            dgItem.notes = obj.notes;

            dgItem.patient = obj.patient;
            dgItem.patientK = obj.patientK;

            dgItem.dow = obj.dow;
            dgItem.dowK = obj.dowK;

            angularPTUIgridWrapper.refreshGrid();
        }
    }
}
function onClickEditRoster(){
    var selectedItems = angularPTUIgridWrapper.getSelectedRows();
    if(selectedItems && selectedItems.length>0){
        var popW = "60%";
        var popH = "60%";
        var profileLbl;
        var devModelWindowWrapper = new kendoWindowWrapper();
        profileLbl = "Edit Roster";
        parentRef.RS = "edit";
        parentRef.selRSItem = selectedItems[0];
        showPatientRoster()
        devModelWindowWrapper.openPageWindow("../../html/patients/createRoster.html", profileLbl, popW, popH, true, closePatientRoster);
    }
}
function onClickDeleteRoster(){
    var selectedItems = angularPTUIgridWrapper.getSelectedRows();
    if(selectedItems && selectedItems.length>0){
        customAlert.confirm("Confirm", "Are you sure you want delete?",function(response){
            if(response.button == "Yes"){
                var obj = selectedItems[0];
                if(obj.idk){
                    var rosterObj = {};
                    rosterObj.id = obj.idk;
                    rosterObj.modifiedBy = Number(sessionStorage.userId);;
                    rosterObj.isActive = 0;
                    rosterObj.isDeleted = 1;
                    var dataUrl = ipAddress+"/patient/roster/delete";
                    createAjaxObject(dataUrl,rosterObj,"POST",onPatientRosterDelete,onError);
                }else{
                    angularPTUIgridWrapper.deleteItem(selectedItems[0]);
                }
            }
        });
    }
}
function onPatientRosterDelete(dataObj){
    onClickRPSearch();
}
function onClickRPSearch(){
    if(selItem.idk != ""){
        var patientRosterURL = ipAddress+"/patient/roster/list/?provider-id="+selItem.idk+"&is-active=1";
        buildPatientRosterList([]);
        getAjaxObject(patientRosterURL,"GET",onGetRosterList,onError);
    }
}
var allDatesInWeek = [];

function onClickRotaSearch() {
    if (selItem.idk != "") {

        if ($("#txtDateOfWeek").val() !== "") {
            var txtDateOfWeek = $("#txtDateOfWeek").data("kendoDatePicker");
            var selDate = txtDateOfWeek.value();
            var selectedDate = new Date(selDate);

            allDatesInWeek = [];

            for (let i = 1; i <= 7; i++) {
                var first = selectedDate.getDate() - (selectedDate.getDay() + 1) + i;
                var day = new Date(selectedDate.setDate(first));

                allDatesInWeek.push(day);
            }
            var firstDayOfWeek = allDatesInWeek[0];
            var lastDayOfWeek = allDatesInWeek[allDatesInWeek.length - 1];

            var fromDate = getFromDate(firstDayOfWeek);
            var toDate = getToDate(lastDayOfWeek);

            var patientRosterURL = ipAddress + "/appointment/list/?provider-id=" + selItem.idk + "&is-active=1&from-date=" + fromDate + "&to-date=" + toDate;
            buildPatientRosterList([]);
            getAjaxObject(patientRosterURL, "GET", onGetAppointmentRosterList, onError);
        }


    }
}

function getAllServiceUsers(){
    var arr = [];
    for(var p=0;p<providerRosterArray.length;p++){
        var pItem = providerRosterArray[p];
        if (pItem) {
            var SUName;
            if (pItem && pItem.patient) {
                SUName = pItem.patient;
            } else if (pItem && pItem.composition && pItem.composition.patient) {
                var tempPatient = pItem.composition.patient;
                SUName = tempPatient.firstName + " " + tempPatient.lastName + " " + (tempPatient.middleName != null ? tempPatient.middleName : "");
                SUName = SUName.trim();
            }

            if (arr.length == 0) {
                arr.push(SUName);
            }
            if (arr.indexOf(SUName) == -1) {
                arr.push(SUName);
            }
        }
    }
    return arr;
}
function getProviderHours(day,pr){
    var ds = providerRosterArray
    var strData = "";
    var arrProvider = [];
    var total = 0;
    for(var p=0;p<ds.length;p++){
        var pItem = ds[p];
        if(pItem  && pItem.patient == pr && pItem.weekDayStart == day){
            var ft = pItem.fromTime;
            var tt = pItem.toTime;
            var diff = tt-ft;
            total = total+diff;
        }
    }
    return total;
}
function getServiceUsersByProviders(){
    var suArray = getAllServiceUsers();
    var strTable = "<table>";
    strTable = strTable+"<tr>";
    strTable = strTable+"<th>Service User</th>";
    for(var i=0;i<towArr.length;i++){
        strTable = strTable+"<th>"+towArr[i].Value+"</thcla>";
    }
    strTable = strTable+"<th>Total</th>";
    strTable = strTable+"</tr>";
    var providerList = suArray;
    careerTotal = 0;
    if(providerList.length>0){
        for(var j=0;j<providerList.length;j++){
            strTable = strTable+"<tr>";
            strTable = strTable+"<td>"+providerList[j]+"</td>";
            var total = 0;
            for(var k=0;k<towArr.length;k++){
                var hours = getProviderHours(towArr[k].Key,providerList[j]);
                total = total+Number(hours);
                strTable = strTable+"<td >"+convertMinsToHrsMins(hours)+"</td>";
            }
            careerTotal =  careerTotal + total;
            strTable = strTable+"<td>"+convertMinsToHrsMins(total)+"</td>";
            strTable = strTable+"</tr>";
        }
    }
    $("#sncareerTotal").html("Week Totals : " + convertMinsToHrsMins(careerTotal));


    strTable = strTable+"</table>";
    $("#divCareerTable").html(strTable);
}



var providerRosterArray = [];
function onGetRosterList(dataObj){
    console.log(dataObj);
    var rosterArray = [];
    if(dataObj && dataObj.response &&  dataObj.response.status && dataObj.response.status.code == 1){
        if($.isArray(dataObj.response.patientRoster)){
            rosterArray = dataObj.response.patientRoster;
        }else{
            rosterArray.push(dataObj.response.patientRoster);
        }
    }
    providerRosterArray = rosterArray;
    $("#divTable").html("");
    $("#divCareerTable").html("");
    getServiceUsersByProviders();
    console.log(providerRosterArray);

    var strTable = "<table class='fixed-first-column'>";
    strTable = strTable+"<tr>";
    strTable = strTable+"<th class='column-head'>Day</th>";
    for(var i=0;i<prsViewArray.length;i++){
        if(i%4 == 0){
            strTable = strTable+"<th colspan='4'>"+prsViewArray[i].tm+"</th>";
        }
    }
    strTable = strTable+"</tr>";
    for(var k=0;k<=6;k++){
        var selectedDate = new Date();

        allDatesInWeek = [];

        for (let i = 1; i <= 7; i++) {
            var first = selectedDate.getDate() - (selectedDate.getDay() + 1) + i;
            var day = new Date(selectedDate.setDate(first));

            allDatesInWeek.push(day);
        }

        var date = onGetHeadDate(k);
        strTable = strTable+"<tr>";
        strTable = strTable+"<td class='column-head' style='font-weight: 900;'>"+getWeekDayName(k)+"</td>";
        for(var j=0;j<prsViewArray.length;j++){
            var itemData = getItemData(prsViewArray[j],k);
            if(itemData){
                if(itemData.color == "green"){
                    sdt = "";
                    var tle = itemData.provider;
                    tle = tle.substring(0, tle.length-1);
                    tle = tle+"\n";
                    //tle = tle+st+"-"+et;
                    var countArr = tle.split(",");
                    var count = countArr.length;
                    // if(count == 1){
                    //     count = "";
                    // }
                    if(count>1){
                        strTable = strTable+"<td style='background-color: #44b1f7; font-weight: bold; color:#fff;' title='"+tle+"'>"+count+"</td>";
                    }else{
                        strTable = strTable+"<td style='background-color: #63c67c; font-weight: bold; color:#fff;' title='"+tle+"'>"+count+"</td>";
                    }

                }else{
                    var tItem = prsViewArray[j];
                    var hours = Number(tItem.h);
                    hours = hours*3600;

                    var minutes = Number(tItem.m);
                    minutes = minutes*60;

                    var currMS = (hours+minutes);

                    if(sdt == ""){
                        sdt = currMS;
                        currClass = Math.floor(Math.random()*10000000);
                        currClass = "C"+currClass;
                        if(posClass == ""){
                            posClass = currClass;
                        }
                    }
                    classArray[currClass] = currMS;
                    //edt = getProviderEndDateTime(tItem.tm,currMS,prId,txtId,et);
                    strTable = strTable+"<td st="+sdt+" et="+et+" class='"+currClass+"'></td>";
                }
            }else{
                sdt = "";
                strTable = strTable+"<td></td>";
            }
        }
        strTable = strTable+"</tr>";
    }
    strTable = strTable+"</table>";
    $("#divTable").html(strTable);


}


function onGetAppointmentRosterList(dataObj) {
    // debugger;
    // console.log("onGetAppointmentRosterList");
    // console.log(dataObj);
    var rosterArray = [];
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == 1) {
        if ($.isArray(dataObj.response.appointment)) {
            rosterArray = dataObj.response.appointment;
        } else {
            rosterArray.push(dataObj.response.appointment);
        }
    }
    var tempRosterArray = [];
    for (var counter = 0; counter < rosterArray.length; counter++) {
        if (rosterArray[counter] && rosterArray[counter].dateOfAppointment !== 0 && rosterArray[counter].duration !== 0) {

            var dateOfAppointment = new Date(parseInt(rosterArray[counter].dateOfAppointment));
            var hours = dateOfAppointment.getHours();
            var minutes = dateOfAppointment.getMinutes();

            var tempTime = (Number(hours) * 60) + Number(minutes);

            rosterArray[counter].fromTime = tempTime;
            rosterArray[counter].toTime = tempTime + Number(rosterArray[counter].duration);
            rosterArray[counter].weekDayEnd = dateOfAppointment.getDay();
            rosterArray[counter].weekDayStart = dateOfAppointment.getDay();

            if (rosterArray[counter].composition && rosterArray[counter].composition.patient) {
                var tempPatient = rosterArray[counter].composition.patient;
                var SUName = tempPatient.firstName + " " + tempPatient.lastName + " " + (tempPatient.middleName != null ? tempPatient.middleName : "");
                rosterArray[counter].patient = SUName.trim();
            }

            tempRosterArray.push(rosterArray[counter]);
        }
    }
    rosterArray = tempRosterArray;
    providerRosterArray = rosterArray;
    $("#divTable").html("");
    $("#divCareerTable").html("");
    getServiceUsersByProviders();
    console.log(providerRosterArray);

    var strTable = "<table class='fixed-first-column'>";
    strTable = strTable + "<tr>";
    strTable = strTable + "<th class='column-head'>Day</th>";
    for (var i = 0; i < prsViewArray.length; i++) {
        if (i % 4 == 0) {
            strTable = strTable + "<th colspan='4'>" + prsViewArray[i].tm + "</th>";
        }
    }
    strTable = strTable + "</tr>";
    for (var k = 0; k <= 6; k++) {
        var date = onGetHeadDate(k);
        strTable = strTable + "<tr>";
        strTable = strTable + "<td class='column-head' style=''>"+date +" ("+ getWeekDayName(k) + ")</td>";
        for (var j = 0; j < prsViewArray.length; j++) {
            var itemData = getItemData(prsViewArray[j], k);
            if (itemData) {
                if (itemData.color == "green") {
                    sdt = "";
                    var tle = itemData.provider;
                    tle = tle.substring(0, tle.length - 1);
                    tle = tle + "\n";
                    //tle = tle+st+"-"+et;
                    var countArr = tle.split(",");
                    var count = countArr.length;
                    // if(count == 1){
                    //     count = "";
                    // }
                    if (count > 1) {
                        strTable = strTable + "<td style='background-color: #44b1f7; font-weight: bold; color:#fff;' title='" + tle + "'>" + count + "</td>";
                    } else {
                        strTable = strTable + "<td style='background-color: #63c67c; font-weight: bold; color:#fff;' title='" + tle + "'>" + count + "</td>";
                    }

                } else {
                    var tItem = prsViewArray[j];
                    var hours = Number(tItem.h);
                    hours = hours * 3600;

                    var minutes = Number(tItem.m);
                    minutes = minutes * 60;

                    var currMS = (hours + minutes);

                    if (sdt == "") {
                        sdt = currMS;
                        currClass = Math.floor(Math.random() * 10000000);
                        currClass = "C" + currClass;
                        if (posClass == "") {
                            posClass = currClass;
                        }
                    }
                    classArray[currClass] = currMS;
                    //edt = getProviderEndDateTime(tItem.tm,currMS,prId,txtId,et);
                    strTable = strTable + "<td st=" + sdt + " et=" + et + " class='" + currClass + "'></td>";
                }
            } else {
                sdt = "";
                strTable = strTable + "<td></td>";
            }
        }
        strTable = strTable + "</tr>";
    }
    strTable = strTable + "</table>";
    $("#divTable").html(strTable);


}

function getItemData(tItem,idVal){
    var prItemData = getProviderItemData(tItem,idVal);
    if(prItemData){
        //console.log(prItemData);
        prItemData.color = "green";
        return prItemData;
    }
    return null;
}
function getProviderItemData(tItem,idVal){
    var data = "";
    var ds = providerRosterArray;
    for(var p=0;p<ds.length;p++){
        var pItem = ds[p];
        if(pItem  &&  (pItem.weekDayStart == idVal || pItem.weekDayEnd == idVal)){
            var hours = Number(tItem.h);
            hours = hours*3600;

            var minutes = Number(tItem.m);
            minutes = minutes*60;

            var currMS = (hours+minutes);
            var stTime = Number(pItem.fromTime)*60;
            stTime = Number(stTime);

            var etTime = Number(pItem.toTime)*60;
            etTime = Number(etTime);

            if(pItem.weekDayEnd!= idVal){
                etTime = (23*3600)+(45*60);//-stTime;//(2*3600)+30*60);
            }

            if(pItem.weekDayStart != idVal){
                stTime = 0;//stTime-2000;//(2*3600)+30*60);
            }

            etTime = etTime-(15*60);

            if(currMS>=stTime && currMS<=etTime){ //&& tItem.h<=et.getHours()
                var sDate = new Date();//item.startDateTime);
                var eDate = new Date();
                var strData = {};
                sDate.setHours(0);
                sDate.setMinutes(0);
                sDate.setSeconds(0);
                sDate.setSeconds(pItem.fromTime*60);

                eDate.setHours(0,0,0,0);
                eDate.setSeconds(pItem.toTime*60);
                var st = new Date(sDate);
                var et = new Date(eDate);
                strData.st = kendo.toString(st,"hh:mm tt");//pItem.fromTimeK;
                strData.et = kendo.toString(et,"hh:mm tt");;//pItem.toTimeK;
                var provider = getProviderDetails(tItem,idVal);
                strData.provider = provider;
                return strData;
            }
        }
    }
    return null;
}
function getProviderDetails(tItem,idVal){
    var data = "";
    var ds = providerRosterArray;
    var strData = "";
    for(var p=0;p<ds.length;p++){
        var pItem = ds[p];
        if(pItem  && (pItem.weekDayStart == idVal || pItem.weekDayEnd == idVal)){
            var hours = Number(tItem.h);
            hours = hours*3600;

            var minutes = Number(tItem.m);
            minutes = minutes*60;

            var currMS = (hours+minutes);
            var stTime = Number(pItem.fromTime)*60;
            stTime = Number(stTime);

            var etTime = Number(pItem.toTime)*60;
            etTime = Number(etTime);

            if(currMS>=stTime && currMS<=etTime){ //&& tItem.h<=et.getHours()
                strData = strData+pItem.patient+",";
            }
        }
    }
    return strData;
}
var allRosterRows = [];
var allRosterRowCount = 0;
var allRosterRowIndex = 0;
function onClickCreateRoster(){
    allRosterRowCount = 0;
    allRosterRowIndex = 0;
    allRosterRows = angularPTUIgridWrapper.getAllRows();
    allRosterRowCount = allRosterRows.length;
    if(allRosterRowCount>0){
        saveRoster();
    }
}
function saveRoster(){
    if(allRosterRowCount>allRosterRowIndex){
        var objRosterEntiry = allRosterRows[allRosterRowIndex];
        var rosterObj = objRosterEntiry.entity;
        rosterObj.createdBy = Number(sessionStorage.userId);
        rosterObj.isActive = 1;
        rosterObj.isDeleted = 0;

        rosterObj.patientId = rosterObj.patientK;
        rosterObj.facilityId = rosterObj.facilityK;
        rosterObj.contractId = rosterObj.contractK;
        rosterObj.providerId = rosterObj.providerK;
        rosterObj.weekDay = rosterObj.dowK;
        rosterObj.fromTime = rosterObj.fromTimeK;
        rosterObj.toTime = rosterObj.toTimeK;
        rosterObj.careTypeId = 2;
        rosterObj.duration = rosterObj.duration;
        rosterObj.reason = rosterObj.reason;
        rosterObj.reasonId = rosterObj.reasonK;
        rosterObj.appointmentReason = rosterObj.reason;
        rosterObj.appointmentType = rosterObj.AppType;
        rosterObj.appointmentReasonId = rosterObj.reasonK;
        rosterObj.appointmentTypeId = rosterObj.AppTypeK;
        rosterObj.notes = rosterObj.notes;
        var dataUrl = "";
        if(rosterObj.idk){
            var rObj = {};
            rObj.id = rosterObj.idk;
            rObj.modifiedBy = Number(sessionStorage.userId);
            rObj.isActive = 1;
            rObj.isDeleted = 0;
            rObj.patientId = rosterObj.patientK;
            rObj.contractId = rosterObj.contractK;
            rObj.providerId = rosterObj.providerK;
            rObj.weekDay = rosterObj.dowK;
            rObj.fromTime = rosterObj.fromTimeK;
            rObj.toTime = rosterObj.toTimeK;
            rObj.careTypeId = rosterObj.careTypeId;
            rObj.duration = rosterObj.duration;
            rObj.reason = rosterObj.reason;
            rObj.reasonId = rosterObj.reasonK;
            rObj.notes = rosterObj.notes;
            rObj.facilityId = rosterObj.facilityK;
            rObj.appointmentReason = rosterObj.reason;
            rObj.appointmentType = rosterObj.AppType;
            rObj.appointmentReasonId = rosterObj.reasonK;
            rObj.appointmentTypeId = rosterObj.AppTypeK;

            dataUrl = ipAddress+"/patient/roster/update";
            createAjaxObject(dataUrl,rObj,"POST",onPatientRosterCreate,onError);
        }else{
            dataUrl = ipAddress+"/patient/roster/create";
            createAjaxObject(dataUrl,rosterObj,"POST",onPatientRosterCreate,onError);
        }
    }
}
function onPatientRosterCreate(dataObj){
    allRosterRowIndex = allRosterRowIndex+1;
    if(allRosterRowCount == allRosterRowIndex){
        customAlert.error("Info", "Roster created successfully");
        buildPatientRosterList([]);
        onClickRPSearch();
        //getPatientRosterList();
    }else{
        saveRoster();
    }
}
function onClickCreateRosterAppointment(){
    if(selItem.idk != ""){
        var popW = "400px";
        var popH = "500px";
        var profileLbl;
        var devModelWindowWrapper = new kendoWindowWrapper();
        profileLbl = "Create Roster Appointment";
        parentRef.pid = selItem.idk;
        var txtFAN = $("#txtFAN").data("kendoComboBox");
        if(txtFAN){
            parentRef.fid = txtFAN.value();
            parentRef.fname = txtFAN.text();
        }
        devModelWindowWrapper.openPageWindow("../../html/patients/createRosterAppointment.html", profileLbl, popW, popH, true, closeRosterAppointment);
    }
}
function closeRosterAppointment(evt,returnData){
    getPatientRosterAppointments();
}
function getPatientRosterAppointments(){
    var patientRosterURL = ipAddress+"/patient/roster/assignment/list/?provider-id="+selItem.idk;
    getAjaxObject(patientRosterURL,"GET",onGetRosterAppList,onError);
}
function onGetRosterAppList(dataObj){
    var ptArray = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.patientRosterAssignment){
            if($.isArray(dataObj.response.patientRosterAssignment)){
                ptArray = dataObj.response.patientRosterAssignment;
            }else{
                ptArray.push(dataObj.response.patientRosterAssignment);
            }
        }
    }

    if(ptArray.length>0){
        var pItem = ptArray[0];
        if(pItem){
            var dtFMT = null;
            var cntry = sessionStorage.countryName;
            if(cntry.indexOf("India")>=0){
                dtFMT = INDIA_DATE_FMT;
            }else  if(cntry.indexOf("United Kingdom")>=0){
                dtFMT = ENG_DATE_FMT;
            }else  if(cntry.indexOf("United State")>=0){
                dtFMT = US_DATE_FMT;
            }
            $("#txtRCR").val(pItem.createdByUser);
            var dt = kendo.toString(new Date(pItem.createdDate),dtFMT);
            $("#txtRCD").val(dt);
            var st = kendo.toString(new Date(pItem.fromDate),dtFMT);
            var ed = kendo.toString(new Date(pItem.toDate),dtFMT);
            $("#txtRCA").val(st);
            $("#txtRCE").val(ed);
        }
    }
}
function getProviderAvailableTimings(){
    onClickTMReset();
    buildPatientRosterList1([]);
    var txtId = $("#txtType").val();
    if (txtId) {
        parentRef.cid = txtId;

        var patientRosterURL = ipAddress+"/homecare/availabilities/?parent-id="+selDocItem.idk+"&parent-type-id="+txtId+"&is-active=1&is-deleted=0";
        getAjaxObject(patientRosterURL,"GET",onPatientAvalableTimings,onError);
    }

}
function onPatientAvalableTimings(dataObj){
    console.log(dataObj);
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1" ){
        if(dataObj.response.availabilities){
            if($.isArray(dataObj.response.availabilities)){
                dataArray = dataObj.response.availabilities;
            }else{
                dataArray.push(dataObj.response.availabilities);
            }
        }

    }

    /*for(var i=0;i<arrAvl.length;i++){
        var item = arrAvl[i];
        if(item){
            var st = item.startTime;
            var sdt = new Date();
            sdt.setHours(0, 0, 0, 0);
            sdt.setSeconds(Number(st));
            item.ST = kendo.toString(sdt,"t");

            var et = item.endTime;
            var edt = new Date();
            edt.setHours(0, 0, 0, 0);
            edt.setSeconds(Number(et));
            item.ET = kendo.toString(edt,"t");
            item.idk = item.id;
        }
    }*/
    for(var i=0 ; i <dataArray.length ; i++){
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        dataArray[i].dayOfWeek == "1" ? dataArray[i].dayName = "Monday" : "";
        dataArray[i].dayOfWeek == "2" ? dataArray[i].dayName = "Tuesday" : "";
        dataArray[i].dayOfWeek == "3" ? dataArray[i].dayName = "Wednesday" : "";
        dataArray[i].dayOfWeek == "4" ? dataArray[i].dayName = "Thursday" : "";
        dataArray[i].dayOfWeek == "5" ? dataArray[i].dayName = "Friday" : "";
        dataArray[i].dayOfWeek == "6" ? dataArray[i].dayName = "Saturday" : "";
        dataArray[i].dayOfWeek == "0" ? dataArray[i].dayName = "Sunday" : "";
        dataArray[i].parentId = selItem.idk;//parentRef.patientId;
        dataArray[i].parentTypeId = parentRef.cid;

        var sTime = dataArray[i].startTime;
        var eTime = dataArray[i].endTime;
        var sec_num_start = parseInt(sTime, 10);

        var st = sTime;
        var sdt = new Date();
        sdt.setHours(0, 0, 0, 0);
        sdt.setSeconds(Number(st));
        dataArray[i].ST = kendo.toString(sdt,"t");

        var et = eTime;
        var edt = new Date();
        edt.setHours(0, 0, 0, 0);
        edt.setSeconds(Number(et));
        dataArray[i].ET = kendo.toString(edt,"t");
        dataArray[i].idk = dataArray[i].id;

        var hours_start   = Math.floor(sec_num_start / 3600);
        var minutes_start = Math.floor((sec_num_start - (hours_start * 3600)) / 60);
        var meridian_start = "AM";
        if(hours_start > 12) {
            hours_start = hours_start - 12;
            meridian_start = "PM";
        }
        dataArray[i].startTimeOfDay = hours_start + ':' + minutes_start + ' ' + meridian_start;
        var sec_num_end = parseInt(eTime, 10);
        var hours_end   = Math.floor(sec_num_end / 3600);
        var minutes_end = Math.floor((sec_num_end - (hours_end * 3600)) / 60);
        var meridian_end = "AM";
        if(hours_end > 12) {
            hours_end = hours_end - 12;
            meridian_end = "PM";
        }
        dataArray[i].endTimeOfDay = hours_end + ':' + minutes_end + ' ' + meridian_end;
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
            var createdDate = new Date(dataArray[i].createdDate);
            var createdDateString = createdDate.toLocaleDateString();
            var modifiedDate = new Date(dataArray[i].modifiedDate);
            var modifiedDateString = modifiedDate.toLocaleDateString();
            dataArray[i].createdDateString = createdDateString;
            dataArray[i].modifiedDateString = modifiedDateString;
        }
    }
    if(dataArray.length > 0) {
        dataArray.sort(function(a, b){
            return a.dayOfWeek - b.dayOfWeek;
        });
    }


    buildPatientRosterList1(dataArray);
}

function onClickTMSave(){
    var st = $("#txtStartTime").data("kendoTimePicker");
    var et = $("#txtEndTime").data("kendoTimePicker");
    var cmbNotes = $("#cmbNotes").data("kendoComboBox");
    if(cmbNotes){
        note = cmbNotes.text();
    }
    var chkMon = $("#chkMon").is(":checked");
    var chkTue = $("#chkTue").is(":checked");
    var chkWed = $("#chkWed").is(":checked");
    var chkThurs = $("#chkThurs").is(":checked");
    var chkFri = $("#chkFri").is(":checked");
    var chkSat = $("#chkSat").is(":checked");
    var chkSun = $("#chkSun").is(":checked");
    var day = 0;
    if(chkMon){
        day = 1;
    }
    if(chkTue){
        day = 2;
    }
    if(chkWed){
        day = 3;
    }
    if(chkThurs){
        day = 4;
    }
    if(chkFri){
        day = 5;
    }
    if(chkSat){
        day = 6;
    }
    if(chkSun){
        day = 7;
    }

    if(st.value() && et.value()){
        var stValue = new Date(st.value());
        var etValue = new Date(et.value());
        var stv = stValue.getTime();
        var etv = etValue.getTime();
        var dt = new Date();
        dt.setHours(0);
        dt.setMinutes(0);
        dt.setSeconds(0);
        etv = etv-dt;
        stv = stv-dt;
        etv = etv/1000;
        stv = stv/1000;
        if(etv>stv){
            var dObj = {};
            dObj.parentTypeId = 500;
            dObj.dayOfWeek = day;
            dObj.isDeleted = 0;
            dObj.isActive = 1;
            dObj.parentId = selItem.idk;
            dObj.createdBy = Number(sessionStorage.userId);
            dObj.startTime = Number(stv.toFixed());
            dObj.endTime = Number(etv.toFixed());;
            var me1 = "POST";
            if(opr == "edit"){
                me1 = "PUT";
                var selectedItems = angularPTUIgridWrapper1.getSelectedRows();
                dObj.id = selectedItems[0].idk;
            }

            var dataUrl = ipAddress + "/homecare/availabilities/";
            createAjaxObject(dataUrl, dObj, me1, onPatientTMCreate, onError);

        }else{
            customAlert.error("Error", "End Time should be greater than  Start Time");
        }
    }else{
        customAlert.error("Error", "Enter Proper Time");
    }
}
function onPatientTMCreate(dataObj){
    console.log(dataObj);
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            if(opr == "add"){
                customAlert.error("Info", "Created");
            }else{
                customAlert.error("Info", "Updated");
            }
            $("#btnAEdit").prop("disabled", true);
            $("#btnADel").prop("disabled", true);
            getProviderAvailableTimings();
        } else {
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}
var opr = "add";
function onClickTMEdit(){
    var selectedItems = angularPTUIgridWrapper1.getSelectedRows();
    console.log(selectedItems);
    if(selectedItems.length>0){
        opr = "edit";
        var dtItem = selectedItems[0];
        if(dtItem){
            // if(operation == UPDATE) {
            var popW = "75%";
            var popH = "58%";

            var profileLbl;
            var devModelWindowWrapper = new kendoWindowWrapper();
            profileLbl = "Staff Availability";
            parentRef.pid = selItem.idk;
            parentRef.ds = getDataGridArray();
            parentRef.TYPE = "AVL";
            var txtFAN = $("#txtFAN").data("kendoComboBox");
            if (txtFAN) {
                var txtFId = txtFAN.value();
                parentRef.fid = txtFId;
            }

            var txtType = $("#txtType").data("kendoComboBox");
            if (txtType) {
                var txtId = txtType.value();
                parentRef.cid = txtId;
            }

            parentRef.rs = "edit";
            parentRef.dtItem = dtItem;
            devModelWindowWrapper.openPageWindow("../../html/masters/createProviderAvailable.html", profileLbl, popW, popH, true, onCloseProviderAvailability);
            parent.setPopupWIndowHeaderColor("0bc56f","FFF");
            // }
            /*var txtStartTime = $("#txtStartTime").data("kendoTimePicker");
           txtStartTime.value(dtItem.ST);

            var txtEndTime = $("#txtEndTime").data("kendoTimePicker");
            txtEndTime.value(dtItem.ET);*/
        }
    }
}
function getDataGridArray(){
    var allRows = angularPTUIgridWrapper1.getAllRows();
    var items = [];
    for(var a=0;a<allRows.length;a++){
        var row = allRows[a].entity;
        items.push(row);
    }
    return items;
}
function onClickAddAvailable(){
    console.log("available add");
    var popW = "75%";
    var popH = "58%";

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Staff Availability";
    parentRef.pid = selItem.idk;
    parentRef.ds = getDataGridArray();
    parentRef.TYPE = "AVL";
    var txtFAN = $("#txtFAN").data("kendoComboBox");
    if (txtFAN) {
        var txtFId = txtFAN.value();
        parentRef.fid = txtFId;
    }

    var txtType = $("#txtType").data("kendoComboBox");
    if (txtType) {
        var txtId = txtType.value();
        parentRef.cid = txtId;
    }

    parentRef.rs = "add";
    parentRef.dtItem = null;

    devModelWindowWrapper.openPageWindow("../../html/masters/createProviderAvailable.html", profileLbl, popW, popH, true, onCloseProviderAvailability);
    parent.setPopupWIndowHeaderColor("0bc56f","FFF");
}
function onCloseProviderAvailability(evt){
    getProviderAvailableTimings();
}
function onClickTMDel(){
    customAlert.confirm("Confirm", "Are you sure you want delete?",function(response){
        if(response.button == "Yes"){
            var selectedItems = angularPTUIgridWrapper1.getSelectedRows();
            console.log(selectedItems);
            if(selectedItems && selectedItems.length>0){
                var selItem = selectedItems[0];
                if(selItem){
                    var dataUrl = ipAddress + "/homecare/availabilities/";
                    var reqObj = {};
                    reqObj.id = selItem.idk;
                    reqObj.isDeleted = "1";
                    reqObj.isActive = "0";
                    reqObj.modifiedBy = sessionStorage.userId;
                    createAjaxObject(dataUrl, reqObj, "PUT", onPatientTMCreate, onError);
                }
            }
        }
    });
}
function onPatientTMCreate(dataObj){
    if(dataObj && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            customAlert.info("Info","Availability deleted successfully");
            getProviderAvailableTimings();
        }else{
            customAlert.error("error", dataObj.message);
        }
    }
}
function onClickTMReset(){
    opr = "add";
    $("#btnAEdit").prop("disabled", true);
    $("#btnADel").prop("disabled", true);
}
var isBrowseFlag = false;
function onClickBrowse(e) {
    removeSelectedButtons();
    // if(operation == "edit"){
    $("#btnBrowse").addClass("selClass");
    isBrowseFlag = true;
    $("#video").hide();
    $("#canvas").hide();
    $("#imgPhoto").show();
    console.log(e);
    if (e.currentTarget && e.currentTarget.id) {
        var btnId = e.currentTarget.id;
        var fileTagId = ("fileElem" + btnId);
        $("#fileElem").click();
    }
    // }
}

function onClickBrowse1(e) {
    //removeSelectedButtons();
    if(operation == "edit"){
        console.log(e);
        if (e.currentTarget && e.currentTarget.id) {
            var btnId = e.currentTarget.id;
            var fileTagId = ("fileElem" + btnId);
            $("#fileElem1").click();
        }
    }
}
function onClickResumeView(){
    if (operation == "edit") {
        //imagePhotoData
        var reqUrl = ipAddress + "/homecare/download/providers/resume/?id=" + selItem.idk + "&access_token=" + sessionStorage.access_token+"&tenant="+sessionStorage.tenant;

        var popW = "50%";
        var popH = "50%";

        var profileLbl;
        var devModelWindowWrapper = new kendoWindowWrapper();
        profileLbl = "View Resume";
        parentRef.rUrl = reqUrl;
        devModelWindowWrapper.openPageWindow("../../html/masters/showResume.html", profileLbl, popW, popH, true, onCloseResume);
    }
}
function onCloseResume(evt,returnData){

}
function onClickUploadPhoto1() {
    if (operation == "edit") {
        //imagePhotoData
        var reqUrl = ipAddress + "/homecare/upload/providers/resume/?id=" + selItem.idk + "&access_token=" + sessionStorage.access_token+"&tenant="+sessionStorage.tenant;

        var formData = new FormData();
        formData.append("desc", fileName1);
        formData.append("file", files1[0]);

        Loader.showLoader();
        $.ajax({
            url: reqUrl,
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false, // "multipart/form-data",
            success: function(data, textStatus, jqXHR) {
                if (typeof data.error === 'undefined') {
                    Loader.hideLoader();
                    submitForm1(data);
                } else {
                    Loader.hideLoader();
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                Loader.hideLoader();
            }
        });
    }
}
function removeSelectedButtons(){

}
function onClickUploadPhoto() {
    // if (operation == "edit") {
    removeSelectedButtons();
    $("#btnUpload").addClass("selClass");
    if (isBrowseFlag) {
        //imagePhotoData
        var reqUrl = ipAddress + "/homecare/upload/providers/photo/?id=" + selItem.idk + "&access_token=" + sessionStorage.access_token+"&tenant="+sessionStorage.tenant;
        // var reqUrl = ipAddress + "/upload/providers/photo/stream/?patient-id=" + patientId + "&photo-ext=png&access_token=" + sessionStorage.access_token+"&tenant="+sessionStorage.tenant
        /* var xmlhttp = new XMLHttpRequest();
         xmlhttp.open("POST", reqUrl, true);
         //xmlhttp.open("POST", "/ROOT/upload/patient/photo/?patient-id=101&photo-ext=png", true);
         xmlhttp.send(imagePhotoData);
         xmlhttp.onreadystatechange = function() {//Call a function when the state changes.
             if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                 customAlert.error("Info", "Image uploaded successfully");
             }
         }*/

        var formData = new FormData();
        formData.append("desc", fileName);
        formData.append("file", files[0]);

        Loader.showLoader();
        $.ajax({
            url: reqUrl,
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false, // "multipart/form-data",
            success: function(data, textStatus, jqXHR) {
                if (typeof data.error === 'undefined') {
                    Loader.hideLoader();
                    submitForm(data);
                } else {
                    Loader.hideLoader();
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                Loader.hideLoader();
            }
        });
    } else {
        var canvas = document.getElementById('canvas');
        var dataURL = canvas.toDataURL("image/png");

        var reqUrl = ipAddress + "/upload/patient/photo/stream/?patient-id=" + patientId + "&photo-ext=png&access_token=" + sessionStorage.access_token+"&tenant="+sessionStorage.tenant;
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open("POST", reqUrl, true);
        //xmlhttp.open("POST", "/ROOT/upload/patient/photo/?patient-id=101&photo-ext=png", true);
        xmlhttp.send(dataURL);
        xmlhttp.onreadystatechange = function() {//Call a function when the state changes.
            if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                // customAlert.error("Info", "Image uploaded successfully");
            }
        }
    }
    // }
}
function submitForm(dataObj) {
    if(dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        // customAlert.error("Info", "Image uploaded successfully");
    }else{
        customAlert.error("Error", dataObj.response.status.message);
    }

    //init();
}
function submitForm1(dataObj) {
    if(dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        customAlert.error("Info", "Resume uploaded successfully");
    }else{
        customAlert.error("Error", dataObj.response.status.message);
    }

    //init();
}
var fileName = "";
var files = null;
function onSelectionFiles(event) {
    //var imageCompressor = new ImageCompressor();
    console.log(event);
    files = event.target.files;
    fileName = "";
    //$('#txtFU').val("");
    if (files) {
        if (files.length > 0) {
            fileName = files[0].name;
            // if (operation == "edit") {
            var oFReader = new FileReader();
            oFReader.readAsDataURL(files[0]);
            oFReader.onload = function(oFREvent) {
                document.getElementById("imgPhoto").src = oFREvent.target.result;
                /*  if (imageCompressor) {
                      imageCompressor.run(oFREvent.target.result, compressorSettings, function(small){
                          document.getElementById("imgPhoto").src = small;
                          //isBrowseFlag = false;
                          imagePhotoData = small;
                          return small;
                      });*/
            }

            // }
        }
    }
}

var fileName1 = "";
var files1 = null;
function onSelectionFiles1(event) {
    //var imageCompressor = new ImageCompressor();
    console.log(event);
    files1 = event.target.files;
    fileName1 = "";
    //$('#txtFU').val("");
    if (files1) {
        if (files1.length > 0) {
            fileName1 = files1[0].name;
            var type = files1[0].type;
            if (type.toLowerCase() == "application/pdf") {
                type = "pdf";
            }
            if (type == "pdf") {
                if (operation == "edit") {
                    $("#txtCV").val(fileName1);
                    //onClickUploadPhoto1();
                    var oFReader = new FileReader();
                    // oFReader.readAsDataURL(files1[0]);
                    oFReader.onload = function (oFREvent) {
                        //document.getElementById("imgPhoto").src = oFREvent.target.result;
                        /*  if (imageCompressor) {
                              imageCompressor.run(oFREvent.target.result, compressorSettings, function(small){
                                  document.getElementById("imgPhoto").src = small;
                                  //isBrowseFlag = false;
                                  imagePhotoData = small;
                                  return small;
                              });*/
                    }

                }
            }
            else{
                customAlert.error("Error", "Accept only PDF files");
            }
        }
    }
}
var selectedTab = "General";
function onClickTabs(e){
    var tabHref = $(e.target).attr("id");
    console.log($(e.target).text());
    $("#btnSave").show();
    $("#btnReset").show();
    $("#btnCancel").show();
    if($(e.target).text() == "Roster" || $(e.target).text() == "Availability"){
        $("#btnSave").hide();
        $("#btnReset").hide();
        $("#btnCancel").hide();
    }
}
function modifySerialNumber() {
    $('.qualificationTabWrapper [name="qualWrap-sno"]').each(function(i) {
        $(this).val(i + 1);
    });
}

function onClickActivities() {
    var popW = "72%";
    var popH = "70%";

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Staff Activities";
    devModelWindowWrapper.openPageWindow("../../html/masters/providerActivityList.html", profileLbl, popW, popH, true, onCloseActivities);
}
function onClickAvailability() {
    if(operation == UPDATE) {
        var popW = "75%";
        var popH = "70%";

        var profileLbl;
        var devModelWindowWrapper = new kendoWindowWrapper();
        profileLbl = "Staff Availability";
        parentRef.pid = selItem.idk;
        parentRef.TYPE = "AVL";
        var txtFAN = $("#txtFAN").data("kendoComboBox");
        if (txtFAN) {
            var txtFId = txtFAN.value();
            parentRef.fid = txtFId;
        }

        var txtType = $("#txtType").data("kendoComboBox");
        if (txtType) {
            var txtId = txtType.value();
            parentRef.cid = txtId;
        }


        devModelWindowWrapper.openPageWindow("../../html/masters/providerAppointment.html", profileLbl, popW, popH, true, onCloseAvailability);
        parent.setPopupWIndowHeaderColor("0bc56f","FFF");
    }
}
function onClickBlock(){
    if(operation == UPDATE) {
        var popW = "75%";
        var popH = "70%";

        var profileLbl;
        var devModelWindowWrapper = new kendoWindowWrapper();
        profileLbl = "Staff Block";
        parentRef.pid = selItem.idk;
        parentRef.TYPE = "BLOCK";
        devModelWindowWrapper.openPageWindow("../../html/masters/providerAppointment.html", profileLbl, popW, popH, true, onCloseAvailability);
    }
}
// function onClickVacation() {
//     if(operation == UPDATE) {
//         var popW = "52%";
//         var popH = "65%";
//
//         var profileLbl;
//         var devModelWindowWrapper = new kendoWindowWrapper();
//         profileLbl = "Staff Leave";
//         devModelWindowWrapper.openPageWindow("../../html/masters/staffVacationList.html", profileLbl, popW, popH, true, onCloseVacation);
//     }
// }
function onClickApprove(){
    if(operation == UPDATE) {
        var popW = "70%";
        var popH = "70%";

        var profileLbl;
        var devModelWindowWrapper = new kendoWindowWrapper();
        profileLbl = "Staff Approve";
        devModelWindowWrapper.openPageWindow("../../html/masters/carerApproveList.html", profileLbl, popW, popH, true, onCloseVacation);
    }
}
function onCloseActivities() {

}
function onCloseAvailability() {

}
function onCloseVacation() {

}

function onClickZipSearch() {
    var popW = 600;
    var popH = 500;
    if(parentRef)
        parentRef.searchZip = true;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search Zip";
    var cntry = sessionStorage.countryName;
    if(cntry.indexOf("India")>=0){
        profileLbl = "Search Postal Code";
    }else if(cntry.indexOf("United Kingdom")>=0){
        if(IsPostalCodeManual == "1"){
            profileLbl = "Search City";
        }
        else{
            profileLbl = "Search Postal Code";
        }
    }else if(cntry.indexOf("United State")>=0){
        profileLbl = "Search Zip";
    }else{
        profileLbl = "Search Zip";
    }
    devModelWindowWrapper.openPageWindow("../../html/masters/zipList.html", profileLbl, popW, popH, true, onCloseSearchZipAction);
}
var zipSelItem = null;
var cityId = "";

function onCloseSearchZipAction(evt, returnData) {
    console.log(returnData);
    if (returnData && returnData.status == "success") {
        //console.log();
        $("#cmbZip").val("");
        $("#txtZip4").val("");
        $("#txtState").val("");
        $("#txtCountry").val("");
        $("#txtCity").val("");
        var selItem = returnData.selItem;
        if (selItem) {
            zipSelItem = selItem;
            if (zipSelItem) {
                cityId = zipSelItem.idk;
                var cntry = sessionStorage.countryName;
                if (cntry.indexOf("United Kingdom") >= 0) {
                    if(IsPostalCodeManual != "1") {
                        if (zipSelItem.zip) {
                            $("#cmbZip").val(zipSelItem.zip);
                        }
                    }
                }
                else {
                    if (zipSelItem.zip) {
                        $("#cmbZip").val(zipSelItem.zip);
                    }
                }
                if (zipSelItem.zipFour) {
                    $("#txtZip4").val(zipSelItem.zipFour);
                }
                if (zipSelItem.state) {
                    $("#txtState").val(zipSelItem.state);
                }
                if (zipSelItem.country) {
                    $("#txtCountry").val(zipSelItem.country);
                }
                if (zipSelItem.city) {
                    $("#txtCity").val(zipSelItem.city);
                }
            }
        }
    }
}





function onComboChange(cmbId) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb && cmb.selectedIndex < 0) {
        cmb.select(0);
    }
}

function onPrefixChange() {
    onComboChange("cmbPrefix");
}
function onSuffixChange() {
    onComboChange("cmbSuffix");
}
function onPtChange() {
    onComboChange("cmbStatus");
}

function onGenderChange() {
    onComboChange("cmbGender");
    if (selItem && selItem.idk != "") {
        var imageServletUrl = ipAddress + "/homecare/download/providers/photo/?" + Math.round(Math.random() * 1000000)+"&access_token="+sessionStorage.access_token+"&tenant="+sessionStorage.tenant+"&id="+selItem.idk;
        $("#imgPhoto").attr("src", imageServletUrl);
    } else {
        onshowImage();
    }
}









function onshowImage() {
    var cmbGender = $("#cmbGender").val();
    if (cmbGender) {
        if (cmbGender == "1") {
            $("#imgPhoto").attr("src", "../../img/AppImg/HosImages/male_profile.png");
        } else if (cmbGender== "2") {
            $("#imgPhoto").attr("src", "../../img/AppImg/HosImages/profile_female.png");
        } else {
            $("#imgPhoto").attr("src", "../../img/AppImg/HosImages/blank.png");
        }
    }
}

function setComboReset(cmbId) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb) {
        cmb.select(0);
    }
}

function validation() {
    var flag = true;
    var txtFC = $("#txtFC").val();
    txtFC = $.trim(txtFC);
    if (txtFC == "") {
        customAlert.error("Error", "Enter Finance charges");
        flag = false;
        return false;
    }
    if (cityId == "") {
        customAlert.error("Error", "Select City ");
        flag = false;
        return false;
    }

    return flag;
}

function getComboDataItem(cmbId) {
    var dItem = null;
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb && cmb.dataItem()) {
        dItem = cmb.dataItem();
        return cmb.text();
    }
    return "";
}

function getComboDataItemValue(cmbId) {
    var dItem = null;
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb && cmb.dataItem()) {
        dItem = cmb.dataItem();
        return cmb.value();
    }
    return "";
}


function onTypeChange() {

}
function buildQualificationListGrid(dataSource) {
    $('.qualWrapper_li').html("");
    if(dataSource.length > 0) {
        for (var i = 0; i < dataSource.length; i++) {
            if(dataSource[i]) {
                var $trLen = i;
                // var passDateString = new Date(dataSource[i].dateOfPass) || "";
                // passDateString = passDateString.replace("/","-");
                // var passDateString = "";
                // passDateString = passDateString.split("-");
                // passDateString = (passDateString[1] + "-" + passDateString[0] + "-" + passDateString[2]);
                var passDateString = GetDateEdit(dataSource[i].dateOfPass);
                passDateString = passDateString.replace('/','-');
                passDateString = passDateString.replace('/','-');
                // var expiryDateString = new Date(dataSource[i].dateOfExpiry).toLocaleDateString().split('/').join('-') || "";
                // var dtArray = expiryDateString.split("-");
                // expiryDateString = (dtArray[1] + "-" + dtArray[0] + "-" + dtArray[2]);
                var expiryDateString = GetDateEdit(dataSource[i].dateOfExpiry);
                expiryDateString = expiryDateString.replace('/','-');
                expiryDateString = expiryDateString.replace('/','-');
                $('.qualificationTabWrapper table tbody').append('<tr class="qualWrapper_li qualificationTabWrapper-li-' + $trLen + '" data-attr-qualId="' + dataSource[i].idk + '"><td class="qual-td-addRemove"><div class="tbl-plusminus"><a href="#" class="qualAddRemoveLink addQual btn-sm-plus">+</a> <a href="#" class="qualAddRemoveLink removeQual btn-sm-minus">-</a></div></td><td><div class="tbl-inpdiv"><div class="col-xs-12"><input type="text" class="form-control" name="qualWrap-sno" value="' + $trLen + '" readonly /></div></div></td><td><div class="tbl-inpdiv"><div class="col-xs-12"><input type="text" class="form-control" name="qualWrap-qualification" id="qualWrapQualification" maxlength="100" value="' + dataSource[i].qualification + '" /></div></div></td><td><div class="tbl-inpdiv"><div class="col-xs-12"><input type="text" name="qualWrap-university" class="form-control" id="qualWrapUniversity" maxlength="100" value="' + dataSource[i].university + '" /></div></div></td><td><div class="tbl-inpdiv"><div class="col-xs-12"><input type="text" name="qualWrap-percentage" class="form-control" id="qualWrapPercentage" value="' + dataSource[i].percentage + '" /></div></div></td><td><input type="text" name="qualWrap-passedout" id="qualWrapPassedout" value="' + passDateString + '" style="width: auto !important; padding-left: 0 !important;" /></td><td><input type="text" name="qualWrap-expirydate" value="' + expiryDateString + '" style="width: auto !important; padding-left: 0 !important;" /></td></tr>');
                allowOnlyDecimals($('[name="qualWrap-percentage"]'));
                $('.qualificationTabWrapper-li-' + $trLen).find('[name="qualWrap-passedout"]').kendoDatePicker({
                    format: "dd-MM-yyyy",
                    change: function (e) {
                        selectedDate = $('.qualificationTabWrapper-li-' + $trLen).find('[name="qualWrap-passedout"]').data("kendoDatePicker").value();
                    }
                });
                //$('.qualificationTabWrapper-li-'+$trLen).find('[name="qualWrap-passedout"]').data("kendoDatePicker").value(selectedDate);
                $('.qualificationTabWrapper-li-' + $trLen).find('[name="qualWrap-expirydate"]').kendoDatePicker({
                    format: "dd-MM-yyyy",
                    change: function (e) {

                        selectedDate = $('.qualificationTabWrapper-li-' + $trLen).find('[name="qualWrap-expirydate"]').data("kendoDatePicker").value();
                    }
                });
                //$('.qualificationTabWrapper-li-'+$trLen).find('[name="qualWrap-expirydate"]').data("kendoDatePicker").value(selectedDate);
                modifySerialNumber();
            }
        }
    }
    else{
        $('.qualificationTabWrapper table tbody').append("");
    }
}

function onCreate(dataObj) {
    console.log(dataObj);
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            selItem = dataObj.response.provider;
            selItem.idk = selItem.id;
            $("#btnUpload").click();
            // var obj = {};
            // obj.status = "success";
            // obj.operation = operation;
            //popupClose(obj);
            $(".btnActive").click();
            customAlert.error("Info", "Staff created successfully.");
            buttonEvents();
            onClickCancel();
            if(ABBREVIATION_Patient != "") {
                getAjaxObject(ipAddress+"/provider/list?is-active=1","GET",getProviderListData,onError);
            }
        } else {
            if(dataObj.response.status.message.includes("Duplicate entry")){
                customAlert.error("error", "Abbreviation is already Exist, please type another.");
            }else{
                customAlert.error("error", dataObj.response.status.message);
            }
        }
    }
    /*var obj = {};
    obj.status = "success";
    obj.operation = operation;
    popupClose(obj);*/
}
function getProviderListData(dataObj) {
    console.log(dataObj);
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if($.isArray(dataObj.response.provider)){
            dataArray = dataObj.response.provider;
        }else{
            dataArray.push(dataObj.response.provider);
        }
    }
    for(var i=dataArray.length - 1; i >= 0; i--){
        if(ABBREVIATION_Patient == dataArray[i].abbreviation) {
            qualifyFunction(dataArray[i].id, "create");
            break;
        }
    }
    // var obj = {};
    // obj.status = "success";
    // obj.operation = operation;
    // popupClose(obj);
}

function getQualificationListData(dataObj) {
    console.log(dataObj);
    var dataArray = [];
    buildQualificationListGrid([]);
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if($.isArray(dataObj.response.qualifications)){
            dataArray = dataObj.response.qualifications;
        }else{
            dataArray.push(dataObj.response.qualifications);
        }
    }
    if(dataArray.length > 0) {
        for (var i = 0; i < dataArray.length; i++) {
            if(dataArray[i] && dataArray[i].id != undefined) {
                dataArray[i].idk = dataArray[i].id;
                dataArray[i].Status = "InActive";
                if (dataArray[i].isActive == 1) {
                    dataArray[i].Status = "Active";
                    var createdDate = new Date(dataArray[i].createdDate);
                    var createdDateString = createdDate.toLocaleDateString();
                    var modifiedDate = new Date(dataArray[i].modifiedDate);
                    var modifiedDateString = modifiedDate.toLocaleDateString();
                    dataArray[i].createdDateString = createdDateString;
                    dataArray[i].modifiedDateString = modifiedDateString;
                }
            }
        }
    }
    buildQualificationListGrid(dataArray);
}
function qualifyFunction(id, testString) {
    $('.qualificationTabWrapper .qualWrapper_li').each(function() {
        var expiryDate = $(this).find('[name="qualWrap-expirydate"]').val();
        var passDate = $(this).find('[name="qualWrap-passedout"]').val();
        // var expiryDateVal = new Date(expiryDate).getTime();
        // var passDateVal = new Date(passDate).getTime();
        var expiryDateVal = getDateTime(expiryDate);
        var passDateVal = getDateTime(passDate);

        if(expiryDateVal && passDateVal && expiryDateVal>passDateVal){
            var obj = {
                "qualification": $(this).find('[name="qualWrap-qualification"]').val(),
                "dateOfExpiry": expiryDateVal,
                "parentTypeId": $("#txtType").val(),
                "university": $(this).find('[name="qualWrap-university"]').val(),
                "isActive": 1,
                "isDeleted": 0,
                "parentId": id,
                "createdBy": Number(sessionStorage.userId),
                "percentage": Number($(this).find('[name="qualWrap-percentage"]').val()),
                "dateOfPass": passDateVal
            }

            var dataUrl = ipAddress + "/homecare/qualifications/";
            if(testString == "create") {
                createAjaxObjectAsync(dataUrl, obj, "POST", onCreateQualification, onError);
            } else if(testString == "update") {
                if($(this).attr('data-attr-qualId') != undefined) {
                    obj.modifiedBy = Number(sessionStorage.userId);
                    obj.id = Number($(this).attr('data-attr-qualId'));
                    createAjaxObjectAsync(dataUrl, obj, "PUT", onUpdateQualification, onError);
                } else {
                    createAjaxObjectAsync(dataUrl, obj, "POST", onCreateQualification, onError);
                }
            }
        }else{
            customAlert.error("Error","Expiry Date is greater than Start Date");
        }
    });
}
function onCreateQualification(dataObj) {
    console.log(dataObj);
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            if($('.alert-qualify') && $('.alert-qualify').length == 0) {
                // $('.customAlert').append('<div class="alert alert-success alert-qualify">'+dataObj.response.status.message+'</div>');
                // if(tabId != "tab1") {
                //     customAlert.error("Info", dataObj.response.status.message);
                // }
                // else{
                customAlert.error("Info", "Staff updated successfully.");
                $(".btnActive").click();
                // }
            } else {
                $('.alert.alert-alert-qualify').text(dataObj.response.status.message);
            }
        } else {
            //$('.customAlert').append('<div class="alert alert-danger">'+dataObj.response.status.message+'</div>');
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}
function onUpdateQualification(dataObj) {
    console.log(dataObj);
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            if($('.alert-qualify') && $('.alert-qualify').length == 0) {
                // $('.customAlert').append('<div class="alert alert-success alert-qualify">'+dataObj.response.status.message+'</div>');
                // if(tabId != "tab1") {
                //     customAlert.error("Info", dataObj.response.status.message);
                // }
                // else{
                customAlert.error("Info", "Staff updated successfully.");
                // }
            } else {
                $('.alert.alert-qualify').text(dataObj.response.status.message);
            }
        } else {
            //$('.customAlert').append('<div class="alert alert-danger">'+dataObj.response.status.message+'</div>');
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}

function onUpdate(dataObj){
    console.log(dataObj);
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            $("#btnUpload").click();
            /*var obj = {};
            obj.status = "success";
            obj.operation = operation;
            popupClose(obj);*/
            var response= dataObj.response.status.message;
            // $('.customAlert').append('<div class="alert alert-success">'+response.replace("Provider","Staff")+'</div>');
            customAlert.error("Info", response.replace("Provider","Staff"));
            $(".btnActive").click();
            $('.tabContentTitle').text('Update Staff');
            var id = selDocItem.idk;
            if(ABBREVIATION_Patient != "") {
                qualifyFunction(id, "update");
            }
            // getAjaxObject(ipAddress+"/provider/list?id="+id,"GET",updateList,onError);
        } else {
            //$('.customAlert').append('<div class="alert alert-danger">'+dataObj.response.status.message+'</div>');
            customAlert.error("error", dataObj.response.status.message.replace('abbr','abbreviation'));
        }
    }
}
function updateList(dataObj) {
    console.log(dataObj);
    selItem = dataObj.response.provider[0];
    selItem.idk = selItem.id;
    getAjaxObject(ipAddress+"/provider/list?is-active=1","GET",getAccountList,onError);
}


function onClickSearch() {
    var obj = {};
    obj.status = "search";
    popupClose(obj);
}
function validateEmail(sEmail) {
    console.log(sEmail);
    var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    if (filter.test(sEmail)) {
        return true;
    }
    else {
        return false;
    }
}

function allowOnlyDecimals(selector) {
    selector.keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 95 && e.which != 46) {
            return false;
        }
    });
}

function getDateTime(sTime){
    if(sTime != "") {
        var now;
        var cntry = sessionStorage.countryName;
        if (cntry.indexOf("India") >= 0) {
            now = new Date(sTime.replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
        } else if (cntry.indexOf("United Kingdom") >= 0) {
            now = new Date(sTime.replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
        } else {
            now = new Date(sTime);
        }
        var utc = now.getTime();
        return utc;
    }
}

function getLeaveStatus(){
    getAjaxObject(ipAddress+"/homecare/leave-status/?fields=id,value","GET",handleGetLeaveStatus,onError);
}

function handleGetLeaveStatus(dataObj){
    leaveStatus = [];
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            if($.isArray(dataObj.response.leaveStatus)){
                leaveStatus = dataObj.response.leaveStatus;
            }else{
                leaveStatus.push(dataObj.response.leaveStatus);
            }
            for(var i=0;i<leaveStatus.length;i++){
                leaveStatus[i].idk = leaveStatus[i].id;
            }
            getAjaxObject(ipAddress+"/homecare/vacations/?fields=*&parentTypeId="+Number(parentRef.type)+"&parentId="+Number(selDocItem.idk)+"&is-active=1&is-deleted=0","GET",handleGetVacationList,onError);
        }
    }
}

function onGetLeaveStatus(id){
    for (var i=0;i<leaveStatus.length;i++){
        if(leaveStatus[i].idk == id){
            return leaveStatus[i].value;
        }
    }
}

function onGetLeaveType(id){
    for (var i=0;i<leaveTypes.length;i++){
        if(leaveTypes[i].idk == id){
            return leaveTypes[i].value;
        }
    }
}

function IsDigit(evt, elementId)
{
    var charCode = evt.charCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46 && charCode != 0)
    {
        return false;
    }
    else
    {
        var len = document.getElementById(elementId).value.length;
        var index = document.getElementById(elementId).value.indexOf('.');
        if (charCode == 0)
        {
            return true;
        }
        if (index > 0 && charCode == 46)
        {
            return false;
        }
        if (index > 0)
        {
            var CharAfterdot = (len + 1) - index;
            if (CharAfterdot > 3)
            {
                return false;
            }
        }
    }
    return true;
}

function onGetPrefix(txt){
    for(var i = 0; i< prefixData.length; i++){
        if(prefixData[i].value == txt){
            return prefixData[i].idk;
        }
    }
}

function onGetSuffix(txt){
    for(var i = 0; i< suffixData.length; i++){
        if(suffixData[i].value == txt){
            return suffixData[i].idk;
        }
    }
}

function onGetEthnicity(txt){
    for(var i = 0; i< ethnicityData.length; i++){
        if(ethnicityData[i].desc == txt){
            return ethnicityData[i].idk;
        }
    }
}


function onGetrace(txt){
    for(var i = 0; i< raceData.length; i++){
        if(raceData[i].desc == txt){
            return raceData[i].idk;
        }
    }
}

function onGetreligion(txt){
    for(var i = 0; i< religionData.length; i++){
        if(religionData[i].desc == txt){
            return religionData[i].idk;
        }
    }
}

function startChange() {
    var startDate = startDT.value(),
        endDate = endDT.value();

    if (startDate) {
        startDate = new Date(startDate);
        startDate.setDate(startDate.getDate() + 1);
        endDT.min(startDate);
    } else if (endDate) {
        startDT.max(new Date(endDate));
    } else {
        endDate = new Date();
        startDT.max(endDate);
        endDT.min(endDate);
    }
}

function onClickView() {
    // $("#divTable").html("");
    onClickRotaSearch();
    $(".rosterappointmentTableBlock").css("display", "block");
    $(".rosterappointmentReportTableBlock").css("display", "block");
}

function onGetHeadDate(k){
    if(allDatesInWeek.length > 0) {
        var day = allDatesInWeek[k].getDate();
        var month = allDatesInWeek[k].getMonth();
        month = month + 1;
        var date;
        if (cntry.indexOf("India") >= 0) {
            date = day + "/" + month;
        } else if (cntry.indexOf("United Kingdom") >= 0) {
            date = day + "/" + month;
        } else {
            date = month + "/" + day;
        }
        return date;
    }
}


function getFromDate(fromDate) {


    var day = fromDate.getDate();
    var month = fromDate.getMonth();
    month = month + 1;
    var year = fromDate.getFullYear();

    var strDate = month + "/" + day + "/" + year;
    strDate = strDate + " 00:00:00";

    var date = new Date(strDate);
    var strDateTime = date.getTime();

    return strDateTime;
}

function getToDate(toDate) {


    var day = toDate.getDate();
    var month = toDate.getMonth();
    month = month + 1;
    var year = toDate.getFullYear();

    var strDate = month + "/" + day + "/" + year;
    strDate = strDate + " 23:59:59";

    var date = new Date(strDate);
    var strDateTime = date.getTime();

    return strDateTime;
}
var angularUIgridWrapper;

$(document).ready(function(){
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("dgridTableList", dataOptions);
    angularUIgridWrapper.init();
    buildDeviceListGrid([]);
});


$(window).load(function(){
    loading = false;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
    init();
    buttonEvents();
    adjustHeight();
}

function init(){
    getAjaxObject(ipAddress+"/country/list/","GET",getCountryList,onError);
}
function onError(errorObj){
    console.log(errorObj);
}
function getCountryList(dataObj){
    console.log(dataObj);
    var dataArray = [];
    if(dataObj){
        if($.isArray(dataObj)){
            dataArray = dataObj;
        }else{
            dataArray.push(dataObj);
        }
    }
    buildDeviceListGrid(dataArray);
}
function buttonEvents(){
    $("#btnSave").off("click",onClickOK);
    $("#btnSave").on("click",onClickOK);

    $("#btnCancelDet").off("click",onClickCancel);
    $("#btnCancelDet").on("click",onClickCancel);

    $("#btnDelete").off("click",onClickDelete);
    $("#btnDelete").on("click",onClickDelete);

    $("#btnAdd").off("click");
    $("#btnAdd").on("click",onClickAdd);
}

function adjustHeight(){
    var defHeight = 80;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    angularUIgridWrapper.adjustGridHeight(cmpHeight);
}

function buildDeviceListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Abbrevation",
        "field": "abbr",
    });
    gridColumns.push({
        "title": "Code",
        "field": "countryCode",
    });
    gridColumns.push({
        "title": "Country",
        "field": "country",
    });
    gridColumns.push({
        "title": "Status",
        "field": "status",
    });
    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}
var prevSelectedItem =[];
function onChange(){

}

function onClickOK(){
    setTimeout(function(){
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if(selectedItems && selectedItems.length>0){
            var obj = {};
            obj.selItem = selectedItems[0];
            // var onCloseData = new Object();
            obj.status = "success";
            obj.operation = "ok";
            var windowWrapper = new kendoWindowWrapper();
            windowWrapper.closePageWindow(obj);
        }
    })
}
function onClickDelete(){
    customAlert.confirm("Confirm", "Are you sure want to delete?",function(response){
        if(response.button == "Yes"){
            var selectedItems = angularUIgridWrapper.getSelectedRows();
            console.log(selectedItems);
            if(selectedItems && selectedItems.length>0){
                var selItem = selectedItems[0];
                if(selItem){
                    getAjaxObject(ipAddress+"/country/delete/"+selItem.id,"GET",onDeleteCountryt,onError);
                }
            }
        }
    });
}
function onDeleteCountryt(dataObj){
    if(dataObj.code == "1"){
        customAlert.info("info", "Country Deleted Successfully");
        init();
    }else{
        customAlert.error("error", dataObj.message);
    }
}
function onClickAdd(){
    var obj = {};
    obj.status = "Add";
    obj.operation = "ok";
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}
function onClickCancel(){
    var obj = {};
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}

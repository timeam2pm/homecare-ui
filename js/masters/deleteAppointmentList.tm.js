var parentRef = null;
var staffArray = null;

var patientId = "";
var angularUIgridWrapper = null;
var angularUIgridWrapperH = null;
var appointmentList = [];

$(document).ready(function () {
    $(".titleToolbar", parent.document).html("Current and Future Appointments");
    $(".titleToolbar").html("Current and Future Appointments");
    var cntry = sessionStorage.countryName;
    if ((cntry.indexOf("India") >= 0) || (cntry.indexOf("United Kingdom") >= 0)) {
        $("#lblFacility").text("Location :");
    } else if (cntry.indexOf("United State") >= 0) {
        $("#lblFacility").text("Facility :");
    }
});


$(window).load(function () {
    loading = false;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded() {
    parentRef = parent.frames['iframe'].window;
    init();
    buttonEvents();
    adjustHeight();
    getAjaxObject(ipAddress + "/carehome/patient-billing/?is-active=1&fields=*,shift.*,travelMode.*,billingType.*,billingPeriod.*,billTo.name,billingRateType.code", "GET", getPatientData, onError);
}

var startDT = null;
var endDT = null;

function init() {
    var fmt = getCountryDateFormat();
    $("#cmbFecility").kendoComboBox();
    $("#cmbProvider").igCombo();
    //$("#cmbProvider").kendoComboBox();
    startDT = $("#txtStartDate").kendoDatePicker({
        max: new Date(),
        change: startChange, format: fmt
    }).data("kendoDatePicker");

    endDT = $("#txtEndDate").kendoDatePicker({
        min: new Date(),
        change: endChange, format: fmt
    }).data("kendoDatePicker");
    patientId = parentRef.patientId;

    getAjaxObject(ipAddress + "/facility/list/?is-active=1", "GET", getFacilityList, onError);

    var dataOptions = {pagination: false, paginationPageSize: 500, changeCallBack: onChange};
    angularUIgridWrapper = new AngularUIGridWrapper("dgridMProviderAppList", dataOptions);
    angularUIgridWrapper.init();
    buildProviderAppList([]);


    //&from-date=05-NOV-2017&days=1
    var dt = new Date();
    var stDate = new Date();
    stDate.setDate(stDate.getDate());
    // startDT.value(stDate);

    var endDate = new Date();
    endDate.setDate(endDate.getDate());
    //endDT.value(endDate);

}

function getFacilityList(dataObj) {
    var dataArray = [];
    if (dataObj) {
        if ($.isArray(dataObj.response.facility)) {
            dataArray = dataObj.response.facility;
        } else {
            dataArray.push(dataObj.response.facility);
        }
    }
    var tempDataArry = [];
    for (var i = 0; i < dataArray.length; i++) {
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if (dataArray[i].isActive == 1) {
            dataArray[i].Status = "Active";
        }
    }
    var obj = {};
    obj.name = "";
    obj.idk = "";
    //dataArray.unshift(obj);

    setDataForSelection(dataArray, "cmbFecility", onFacilityChange, ["name", "idk"], 0, "");

    var cmbFecility = $("#cmbFecility").data("kendoComboBox");
    var facilityId = cmbFecility.value();
    var patientListURL = ipAddress + "/provider/list/?facility-id=" + facilityId + "&is-active=1&is-deleted=0";

    getAjaxObject(patientListURL, "GET", onGetProviderList, onError);
}

function onFacilityChange() {
    var cmbFecility = $("#cmbFecility").data("kendoComboBox");
    if (cmbFecility) {
        if (cmbFecility.selectedIndex < 0) {
            cmbFecility.select(0);
        }
    }
    if (cmbFecility.selectedIndex > 0) {
        getProviderList();
    } else {
        //setDataForSelection([], "cmbProvider", onProviderChange, ["", ""], 0, "");
        /*var cmbProvider = $("#cmbProvider").data("kendoComboBox");
        if(cmbProvider){
            cmbProvider.text("");
            cmbProvider.value("");
        }*/
        getAjaxObject(ipAddress + "/provider/list/?is-active=1", "GET", onGetProviderList, onError);
    }
}

function getProviderList() {
    $("#cmbProvider").html("");
    var cmbFecility = $("#cmbFecility").data("kendoComboBox");
    var facilityId = cmbFecility.value();
    var patientListURL = ipAddress + "/provider/list/?facility-id=" + facilityId + "&is-active=1&is-deleted=0";
    getAjaxObject(patientListURL, "GET", onGetProviderList, onError);
}

function onGetProviderList(dataObj) {
    // console.log(dataObj);
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if ($.isArray(dataObj.response.provider)) {
            dataArray = dataObj.response.provider;
        } else {
            dataArray.push(dataObj.response.provider);
        }
    }
    dataArray.sort(function (a, b) {
        var p1 = a.lastName;
        var p2 = b.lastName;

        if (p1 < p2) return -1;
        if (p1 > p2) return 1;
        return 0;
    });

    var tempDataArry = [];
    for (var i = 0; i < dataArray.length; i++) {
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if (dataArray[i].isActive == 1) {
            dataArray[i].Status = "Active";
        }
        dataArray[i].name = dataArray[i].lastName + " " + dataArray[i].firstName;

        // $("#cmbProvider").append(GetOption(dataArray[i].firstName, dataArray[i].idk));
    }

    staffArray = dataArray;


    var obj = {};
    obj.name = "Un Allocated";
    obj.idk = "0";
    dataArray.unshift(obj);

    setMultipleDataForSelection(dataArray, "cmbProvider", onProviderChange, ["name", "idk"], 0, "");

}

function GetOption(text, value) {
    return "<option value = '" + value + "'>" + text + "</option>"
}

function onProviderChange() {
    onComboChange("cmbProvider");
}

function startChange() {
    var startDate = startDT.value(),
        endDate = endDT.value();

    if (startDate) {
        startDate = new Date(startDate);
        startDate.setDate(startDate.getDate());
        endDT.min(startDate);
    } else if (endDate) {
        // startDT.max(new Date(endDate));
    } else {
        endDate = new Date();
        // startDT.max(endDate);
        endDT.min(endDate);
    }
}

function endChange() {
    var endDate = endDT.value(),
        startDate = startDT.value();

    if (endDate) {
        endDate = new Date(endDate);
        endDate.setDate(endDate.getDate());
        // startDT.max(endDate);
    } else if (startDate) {
        endDT.min(new Date(startDate));
    } else {
        endDate = new Date();
        // startDT.max(endDate);
        endDT.min(endDate);
    }
}

function onPatientListData1(dataObj) {
    console.log(dataObj);
    var dtArray = [];
    if (dataObj && dataObj.response && dataObj.response.appointment) {
        if ($.isArray(dataObj.response.appointment)) {
            dtArray = dataObj.response.appointment;
        } else {
            dtArray.push(dataObj.response.appointment);
        }
    }
    if (dtArray.length > 0) {
        var dArray = [];
        dtArray.sort(function (a, b) {
            var p1 = a.dateOfAppointment;
            var p2 = b.dateOfAppointment;

            if (p1 < p2) return -1;
            if (p1 > p2) return 1;
            return 0;
        });

        for (var j = 0; j < dtArray.length; j++) {
            var item = dtArray[j];
            item.idk = item.id;
            var fDateMS = item.dateOfAppointment;
            var fDate = new Date(fDateMS);
            var tmFmt = getCountryDateFormat();//getCountryDateTimeFormat();
            fDate = kendo.toString(new Date(fDateMS), tmFmt);
            var fTM = kendo.toString(new Date(fDateMS), "hh:mm tt");
            item.fromDate = fDate;
            item.TM = fTM;
            var day = new Date(fDateMS).getDay();
            var dow = getWeekDayName(day);
            item.DOW = dow;
            if (item.createdDate) {
                var fmt = getCountryDateFormat();
                item.CD = kendo.toString(new Date(item.createdDate), fmt);
            }
            item.CB = item.createdBy;
            if (item.composition && item.composition.appointmentType && item.composition.appointmentType.desc) {
                item.AppType = item.composition.appointmentType.desc;
            } else {
                item.AppType = "";
            }
            if (item.composition && item.composition.appointmentReason && item.composition.appointmentReason.desc) {
                item.Reason = item.composition.appointmentReason.desc;
            } else {
                item.Reason = "";
            }

            if (item.providerId != 0 && item.composition && item.composition.provider && item.composition.provider.firstName) {
                item.Provider = item.composition.provider.lastName + " " + item.composition.provider.firstName;
            } else {
                item.Provider = "Un Allocated";
            }
            if (item.composition && item.composition.facility && item.composition.facility.displayName) {
                item.Fecility = item.composition.facility.displayName;
            } else {
                item.Fecility = "";
            }
            if (item.composition && item.composition.patient && item.composition.patient) {
                item.patient = item.composition.patient.lastName + " " + item.composition.patient.firstName;
            } else {
                item.patient = "";
            }
            if (item.outTime && item.inTime) {
                item.RP = "View";
            } else {
                item.RP = "";
            }
            item.idk = item.id;
            item.shortduration = item.duration;
            if (item.duration) {
                var totalMinutes = item.duration;
                var hours = Math.floor(totalMinutes / 60);
                var minutes = Math.round(totalMinutes % 60);
                if (hours.toString().length == 1) {
                    hours = "0" + hours;
                }
                if (minutes.toString().length == 1) {
                    minutes = "0" + minutes;
                }


                item.duration = hours + ":" + minutes;
            }

            dArray.push(item);
        }
        buildProviderAppList(dArray);
    } else {
        customAlert.info("Info", "No appointments");
    }
}

function getWeekDayName(wk) {
    var wn = "";
    if (wk == 1) {
        return "Monday";
    } else if (wk == 2) {
        return "Tuesday";
    } else if (wk == 3) {
        return "Wednesday";
    } else if (wk == 4) {
        return "Thursday";
    } else if (wk == 5) {
        return "Friday";
    } else if (wk == 6) {
        return "Saturday";
    } else if (wk == 0) {
        return "Sunday";
    }
    return wn;
}

function buildProviderAppList(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Select",
        "field": "SEL",
        "cellTemplate": showCheckBoxTemplate(),
        "headerCellTemplate": "<input type='checkbox' class='check1' ng-model='TCSELECT' onclick='toggleSelectAll1(event);' style='width:18px;height:18px;padding: 8px 0 0px 42px;'></input>",
        "width": "8%"
    });
    gridColumns.push({
        "title": "Date",
        "field": "fromDate",
        "enableColumnMenu": false,
        "width": "8%"
    });
    gridColumns.push({
        "title": "Time",
        "field": "TM",
        "enableColumnMenu": false,
        "width": "8%"
    });
    gridColumns.push({
        "title": "DOW",
        "field": "DOW",
        "enableColumnMenu": false,
        "width": "8%"
    });
    gridColumns.push({
        "title": "Dur (hh:mm)",
        "field": "duration",
        "enableColumnMenu": false,
        "width": "12%"
    });
    gridColumns.push({
        "title": "Status",
        "field": "AppType",
        "enableColumnMenu": false,
        "width": "8%"
    });
    gridColumns.push({
        "title": "Reason",
        "field": "Reason",
        "enableColumnMenu": false,
    });
    gridColumns.push({
        "title": "Staff",
        "field": "Provider",
        "enableColumnMenu": false,
        "width": "12%"
    });
    gridColumns.push({
        "title": "Service User",
        "field": "patient",
        "enableColumnMenu": false,
        "width": "12%"
    });
    angularUIgridWrapper.creategrid(dataSource, gridColumns, otoptions);
    adjustHeight();
}

function showCheckBoxTemplate() {
    var node = '<div style="text-align:center;padding-top: 6px;"><input type="checkbox" class="check1" id="chkbox" ng-model="row.entity.SEL" style="width:20px;height:20px;margin:0px;"   onclick="onSelect(event);"></input></div>';
    return node;
}

function toggleSelectAll1(e) {
    var checked = e.currentTarget.checked;
    var rows = angularUIgridWrapper.getScope().gridApi.core.getVisibleRows();
    for (var i = 0; i < rows.length; i++) {
        rows[i].entity["SEL"] = checked;
    }
}

function onClickDelete() {
    var arr = [];
    var rows = angularUIgridWrapper.getScope().gridApi.core.getVisibleRows();
    for (var i = 0; i < rows.length; i++) {
        var row = rows[i].entity["SEL"];
        if (row == true) {
            arr.push(rows[i].entity);
        }
    }

    // console.log(arr);
    if (arr.length > 0) {
        customAlert.confirm("Confirm", "Are you sure you want delete?", function (response) {
            if (response.button == "Yes") {
                var strArray = [];
                for (var i = 0; i < arr.length; i++) {
                    var iItem = arr[i];
                    if (iItem) {
                        var obj = {};
                        // obj.createdBy = iItem.createdBy;
                        obj.modifiedBy = Number(sessionStorage.userId);
                        obj.isActive = 0;
                        obj.isDeleted = 1;
                        obj.dateOfAppointment = iItem.dateOfAppointment;
                        obj.duration = iItem.shortduration;
                        obj.providerId = Number(iItem.providerId);
                        obj.facilityId = Number(iItem.facilityId);
                        obj.patientId = iItem.patientId;//parentRef.patientId;
                        obj.appointmentType = iItem.appointmentType;
                        obj.notes = iItem.notes;//$("#taArea").val();

                        var selSUItem = onGetSUBillingDetails(iItem.billToName);
                        if (selSUItem) {
                            obj.billingRateType = selSUItem.billingRateTypeCode;
                            obj.patientBillingRate = selSUItem.rate;
                            obj.billingRateTypeId = selSUItem.billingRateTypeId;
                            obj.swiftBillingId = selSUItem.swiftBillingId;
                            obj.shiftValue = selSUItem.shiftValue;
                            // obj.billingRateType = iItem.billingRateTypeCode;
                            // obj.patientBillingRate = iItem.rate;
                            // obj.billingRateTypeId = iItem.billingRateTypeId;
                            // obj.swiftBillingId = iItem.swiftBillingId;
                            obj.billToName = iItem.billToName;
                            obj.billToId = selSUItem.billToId;
                        }
                        obj.payoutHourlyRate = iItem.payoutHourlyRate;

                        obj.id = iItem.idk;
                        obj.appointmentReason = iItem.appointmentReason;


                        // // var selSUItem = onGetSUBillingDetails($("#cmbBilling").val());
                        // // obj.shiftValue = selSUItem.shiftValue;
                        // // obj.billingRateType = selSUItem.billingRateTypeCode;
                        // // obj.patientBillingRate = selSUItem.rate;
                        // // obj.billingRateTypeId = selSUItem.billingRateTypeId;
                        // // obj.swiftBillingId = selSUItem.swiftBillingId;
                        // // obj.payoutHourlyRate = $("#txtPayout").val();
                        // // obj.billToName = $("#cmbBilling option:selected").text();
                        // //
                        // // obj.billToId = selSUItem.billToId;
                        // obj.appointmentReason = $("#cmbReason").val();
                        strArray.push(obj);
                    }
                }

                var dataUrl = ipAddress + "/appointment/update";
                createAjaxObject(dataUrl, strArray, "POST", onCreate, onError);

            }
        });
    }
}

function onCreate(dataObj) {
    //console.log(dataObj);
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            customAlert.error("Info", "Appointment deleted successfully");
            onClickSearch();
        }
    }
}

function onSelect(evt) {
    // console.log(evt);
}

function onClickLink() {
    setTimeout(function () {
        // console.log("click");
        var rows = angularUIgridWrapper.getSelectedRows();
        //console.log(rows);
        if (rows && rows.length > 0) {
            var row = rows[0];
            if (row) {
                // console.log(row.idk);
                var dataUrl = ipAddress + "/appointment/history/list/?id=" + row.idk;
                getAjaxObject(dataUrl, "GET", getAppHistoryList, onError);

            }
        }
    }, 100)
}

function onClickRPLink() {
    setTimeout(function () {
        var rows = angularUIgridWrapper.getSelectedRows();
        //console.log(rows);
        if (rows && rows.length > 0) {
            var row = rows[0];
            if (row) {
                // console.log(row.idk);
                var popW = "60%";
                var popH = "60%";
                var profileLbl;
                var devModelWindowWrapper = new kendoWindowWrapper();
                profileLbl = "Service User Activities";
                parentRef.idk = row.idk;
                devModelWindowWrapper.openPageWindow("../../html/reports/patientActivity.html", profileLbl, popW, popH, true, onCloseSearchZipAction);
            }
        }
    }, 100);
}

function onCloseSearchZipAction(evt, returnData) {

}

function getAppHistoryList(dataObj) {
    // console.log(dataObj);
    var dtArray = [];
    if (dataObj && dataObj.response && dataObj.response.appointmentHistory) {
        if ($.isArray(dataObj.response.appointmentHistory)) {
            dtArray = dataObj.response.appointmentHistory;
        } else {
            dtArray.push(dataObj.response.appointmentHistory);
        }
    }
    var dArray = [];
    for (var j = 0; j < dtArray.length; j++) {
        var item = dtArray[j];
        var fDate = item.dateOfAppointment;
        fDate = new Date(fDate);
        fDate = kendo.toString(new Date(fDate), "dd-MMM-yyyy h:mm:ss tt");

        item.fromDate = fDate;
        if (item.modifiedDate) {
            item.CD = kendo.toString(new Date(item.modifiedDate), "dd-MMM-yyyy");
        } else if (item.createdDate) {
            item.CD = kendo.toString(new Date(item.createdDate), "dd-MMM-yyyy");
        }
        if (item.modifiedBy) {
            item.CB = item.modifiedBy;
        } else {
            item.CB = item.createdBy;
        }

        item.AppType = item.appointmentTypeDesc;
        item.Reason = item.appointmentReasonDesc;
        item.Provider = item.providerFirstName;
        item.Fecility = item.facilityName;
        item.idk = item.id;

        dArray.push(item);
    }
    buildProviderAppListH(dArray);
}

function onChange() {

}

function onChange1() {

}

function buildProviderAppListH(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Appointment Date",
        "field": "fromDate",
        "enableColumnMenu": false,
        "width": "25%"
    });
    gridColumns.push({
        "title": "Dur",
        "field": "duration",
        "enableColumnMenu": false,
    });
    gridColumns.push({
        "title": "Appointment Type",
        "field": "AppType",
        "enableColumnMenu": false,
    });
    gridColumns.push({
        "title": "Reason",
        "field": "Reason",
        "enableColumnMenu": false,
    });
    gridColumns.push({
        "title": "Facility",
        "field": "Fecility",
        "enableColumnMenu": false,
    });
    gridColumns.push({
        "title": "Provider",
        "field": "Provider",
        "enableColumnMenu": false,
    });

    angularUIgridWrapperH.creategrid(dataSource, gridColumns, otoptions);
    adjustHeight();
}

function adjustHeight() {
    var defHeight = 200;
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 300;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    try {
        angularUIgridWrapper.adjustGridHeight(cmpHeight);
        //angularUIgridWrapperH.adjustGridHeight(cmpHeight);
    } catch (e) {
    }
}

function onError(errorObj) {
    // console.log(errorObj);
}

function buttonEvents() {
    $("#btnClose").off("click", onClickCancel);
    $("#btnClose").on("click", onClickCancel);

    $("#btnSearch").off("click", onClickSearch);
    $("#btnSearch").on("click", onClickSearch);

    $("#btnAdd").off("click", onClickAdd);
    $("#btnAdd").on("click", onClickAdd);

    $("#btnDelete").off("click", onClickDelete);
    $("#btnDelete").on("click", onClickDelete);
}

var providerId = "";
var fromDate = "";
var toDate = "";

function onClickSearch() {
    buildProviderAppList([]);
    var cmbFecility = $("#cmbFecility").data("kendoComboBox");
    //var cmbProvider = $("#cmbProvider").data("kendoComboBox");
    var txtStartDate = $("#txtStartDate").data("kendoDatePicker");
    var txtEndDate = $("#txtEndDate").data("kendoDatePicker");

    patientId = parentRef.patientId;

    var patientListURL = "";//

    var sensorArray = [];
    var pr = "";
    $('#cmbProvider').each(function (i, selected) {
        var obj = {};
        //obj.Sensor = $(selected).val();
        //sensorArray.push(obj);
        // pr = pr+$(selected).val()+",";
        var sLength = $(selected).val().split(',');
        for (var i = 0; i < sLength.length; i++) {
            pr = pr + onGetStaffId(sLength[i]) + ",";
        }
    });
    pr = pr.replace(/,\s*$/, "");

    //var pr = sensorArray.toString();
    if (txtStartDate.value() && txtEndDate.value()) {
        var endDate = new Date(txtEndDate.value());
        var stDate = new Date(txtStartDate.value());
        stDate.setHours(0, 0, 0, 0);
        endDate.setHours(23, 59, 59, 0);
        var edTime = endDate.getTime();

        var patientListURL = ipAddress + "/appointment/list/?facility-id=" + cmbFecility.value() + "&provider-id=" + pr + "&to-date=" + edTime + "&from-date=" + stDate.getTime() + "&is-active=1";
        //edTime = edTime+86400000;

        /*patientListURL = ipAddress+"/appointment/list/?patient-id="+patientId+"&to-date="+edTime+"&from-date="+stDate.getTime()+"&is-active=1";
        if(cmbFecility && cmbFecility.value()){
            patientListURL = patientListURL+"&facility-id="+cmbFecility.value();
        }
        if(cmbProvider && cmbProvider.value()){
            patientListURL = patientListURL+"&provider-id="+cmbProvider.value();
        }*/
        getAjaxObject(patientListURL, "GET", onPatientListData1, onError);
    } else {
        customAlert.error("Error", "Select Dates");
    }
}

function onClickAdd() {
}

function addAppointment() {
}

function closeAppointment(evt, returnData) {
}

function onClickCancel() {
    var obj = {};
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}

function onGetStaffId(name) {
    for (var i = 0; i < staffArray.length; i++) {
        if ($.trim(staffArray[i].name) == $.trim(name)) {
            return staffArray[i].idk;
        }
    }
}

function onGetAppointmentList() {
    var urlExtn = "/master/appointment_type/";
    getAjaxObject(ipAddress + urlExtn, "GET", getSearchList, onSearchError);
}

function getSearchList(resp) {
    if (resp && resp.response && resp.response.codeTable) {
        if ($.isArray(resp.response.codeTable)) {
            appointmentList = resp.response.codeTable;
        } else {
            appointmentList.push(resp.response.codeTable);
        }
    }
    if (appointmentList != undefined) {
        for (var i = 0; i < appointmentList.length; i++) {
            appointmentList[i].idk = appointmentList[i].id;
        }
    }

}

function onGetSUBillingDetails(name) {
    for (var i = 0; i < patientBillArray.length; i++) {
        if (patientBillArray[i].billToname && patientBillArray[i].billToname != null && patientBillArray[i].billToname.toLowerCase() == name.toLowerCase()) {
            return patientBillArray[i];
        }
    }
}


function getPatientData(dataObj) {
    patientBillArray = [];
    var tempCompType = [];
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.patientBilling) {
            if ($.isArray(dataObj.response.patientBilling)) {
                tempCompType = dataObj.response.patientBilling;
            } else {
                tempCompType.push(dataObj.response.patientBilling);
            }
        }
    }
    for (var q = 0; q < tempCompType.length; q++) {
        var qItem = tempCompType[q];
        if (qItem) {
            qItem.value = qItem.billToname;
            qItem.text = qItem.billToname;
            patientBillArray.push(qItem);
        }
    }
}
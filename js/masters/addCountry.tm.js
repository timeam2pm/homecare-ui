var countryUIGridWrapper;
var stateUIGridWrapper = null;
var zipUIGridWrapper = null;
var parentRef = null;
var recordType = "1";
var dataArray = [];
var ADD = "add";
var UPDATE = "edit";
var VIEW = "view";
var DELETE = "delete";
var operation = "add";
var atID = "";
var ds  = "";
var activeListFlag = true;

var IsCountryFlag = 0;
var IsPostalCodeManual = sessionStorage.IsPostalCodeManual;
var IsPostalFlag = "0";
var sessionMasterCommValue = sessionStorage.sessionMasterCommValue;

var statusArr = [{Key:'Active',Value:'Active'},{Key:'InActive',Value:'InActive'}];

$(document).ready(function(){
    $("#pnlPatient",parent.document).css("display","none");
    sessionStorage.setItem("IsSearchPanel", "1");
    themeAPIChange();
    parentRef = parent.frames['iframe'].window;
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    countryUIGridWrapper = new AngularUIGridWrapper("communicationCountryList", dataOptions);
    countryUIGridWrapper.init();
    stateUIGridWrapper = new AngularUIGridWrapper("communicationStateList", dataOptions);
    stateUIGridWrapper.init();
    zipUIGridWrapper = new AngularUIGridWrapper("communicationZipList", dataOptions);
    zipUIGridWrapper.init();
    if(sessionMasterCommValue.toLowerCase() == "country"){
        buildCountryListGrid([]);
    }
    else if(sessionMasterCommValue.toLowerCase() == "state"){
        buildStateListGrid([]);
    }
    else if(sessionMasterCommValue.toLowerCase() == "city"){
        buildZipListGrid([]);
    }
    getAjaxObject(ipAddress + "/country/list/?is-active=1", "GET", getCountryList, onError);
    getAjaxObject(ipAddress + "/state/list/", "GET", getStateList, onError);
    getCountryZoneName();
    getCommunicationTypes();
});


$(window).load(function(){
    loading = false;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
    init();
    buttonEvents();
    adjustHeight();
}

function init(){
    $("#btnEdit").prop("disabled", true);
    $("#btnDelete").prop("disabled", true);
    buildCountryListGrid([]);
    var urlExtn = getcommunicationForm() + '/list/';
    getAjaxObject(ipAddress + urlExtn, "GET", getSearchList, onError);
}

function getSearchList(resp) {
    var dataArray, dataActiveList = [],
        dataInActiveList = [];
    if (sessionMasterCommValue.toLowerCase() == "country") {
        if (resp && resp.response && resp.response.country) {
            if ($.isArray(resp.response.country)) {
                dataArray = resp.response.country;
            } else {
                dataArray.push(resp.response.country);
            }
        }
        dataArray.sort(function (a, b) {
            var nameA = a.country.toUpperCase(); // ignore upper and lowercase
            var nameB = b.country.toUpperCase(); // ignore upper and lowercase
            if (nameA < nameB) {
                return -1;
            }
            if (nameA > nameB) {
                return 1;
            }
            // names must be equal
            return 0;
        });
    } else if (sessionMasterCommValue.toLowerCase() == "state") {
        if (resp && resp.response && resp.response.stateExt) {
            if ($.isArray(resp.response.stateExt)) {
                dataArray = resp.response.stateExt;
            } else {
                dataArray.push(resp.response.stateExt);
            }
        }
        dataArray.sort(function (a, b) {
            var nameA = a.state.toUpperCase(); // ignore upper and lowercase
            var nameB = b.state.toUpperCase(); // ignore upper and lowercase
            if (nameA < nameB) {
                return -1;
            }
            if (nameA > nameB) {
                return 1;
            }
            // names must be equal
            return 0;
        });
    } else if (sessionMasterCommValue.toLowerCase() == "city") {
        if (resp && resp.response && resp.response.cityExt) {
            if ($.isArray(resp.response.cityExt)) {
                dataArray = resp.response.cityExt;
            } else {
                dataArray.push(resp.response.cityExt);
            }
        }
        dataArray.sort(function (a, b) {
            var nameA = a.city.toUpperCase(); // ignore upper and lowercase
            var nameB = b.city.toUpperCase(); // ignore upper and lowercase
            if (nameA < nameB) {
                return -1;
            }
            if (nameA > nameB) {
                return 1;
            }
            // names must be equal
            return 0;
        });
    }
    if (dataArray != undefined) {
        for (var i = 0; i < dataArray.length; i++) {
            dataArray[i].idDuplicate = dataArray[i].id;
            if (dataArray[i].isActive == 1) {
                dataArray[i].Status = "Active";
                dataActiveList.push(dataArray[i]);
            } else {
                dataArray[i].Status = "InActive";
                dataInActiveList.push(dataArray[i]);
            }
        }
    }
    if (activeListFlag) {
        if (sessionMasterCommValue.toLowerCase() == "country") {
            buildCountryListGrid(dataActiveList);
        } else if (sessionMasterCommValue.toLowerCase() == "state") {
            buildStateListGrid(dataActiveList);
        } else if (sessionMasterCommValue.toLowerCase() == "city") {
            buildZipListGrid(dataActiveList);
        }
    } else if (!activeListFlag) {
        if (sessionMasterCommValue.toLowerCase() == "country") {
            buildCountryListGrid(dataInActiveList);
        } else if (sessionMasterCommValue.toLowerCase() == "state") {
            buildStateListGrid(dataInActiveList);
        } else if (sessionMasterCommValue.toLowerCase() == "city") {
            buildZipListGrid(dataInActiveList);
        }
    }
}


function buttonEvents(){
    $('#btnSave').on('click', function(e) {
        e.preventDefault();
        var urlExtn = getcommunicationForm() + '/create';
        if (sessionMasterCommValue.toLowerCase() == "country") {
            var strAbbr = $("#txtAbbreviation").val();
            strAbbr = $.trim(strAbbr);
            var strCode = $("#txtCode").val();
            strCode = $.trim(strCode);
            var strName = $("#txtName").val();
            strName = $.trim(strName);
            var isActive = parseInt($("#cmbStatusCountry").val());
            var IsDeleted;
            if(isActive == 1){
                IsDeleted = 0;
            }
            else{
                IsDeleted = 1;
            }

            var dataObj = {
                "abbr": strAbbr,
                "country": strName,
                "code": strCode,
                "isActive": isActive,
                "isDeleted": 0
            };

            if(parentRef.operation == "edit"){
                dataObj.id = atID;
                dataObj.modifiedBy = sessionStorage.userId;
                urlExtn = getcommunicationForm() + '/update';
            }
            else{
                dataObj.createdBy= sessionStorage.userId;
            }

            if (strAbbr != "" && ((strCode != "" && IsPostalFlag != "1") || IsPostalFlag == "1") && strName != "") {
                var dataUrl = ipAddress + urlExtn;
                createAjaxObject(dataUrl, dataObj, "POST", onCreate, onError);
            } else {
                customAlert.error("Error","Please fill the Required Fields");
            }
        } else if (sessionMasterCommValue.toLowerCase() == "state") {
            var strAbbr = $("#txtAbbreviations").val();
            strAbbr = $.trim(strAbbr);
            var strCode = $("#txtCodes").val();
            strCode = $.trim(strCode);
            var strName = $("#txtNames").val();
            strName = $.trim(strName);

            var isActive = parseInt($("#cmbStatusCountrys").val());

            var cmbCountry = parseInt($("#cmbCountry").val());
            var dataObj = {
                "abbr": strAbbr,
                "state": strName,
                "code": strCode,
                "countryId": cmbCountry,
                "isActive": isActive,
                "isDeleted": 0
            };

            if(parentRef.operation == "edit"){
                dataObj.id = atID;
                dataObj.modifiedBy = sessionStorage.userId;
                urlExtn = getcommunicationForm() + '/update';
            }
            else{
                dataObj.createdBy= sessionStorage.userId;
            }

            if (strAbbr != "" && strName != "" && cmbCountry != "") {
                var dataUrl = ipAddress + urlExtn;
                createAjaxObject(dataUrl, dataObj, "POST", onCreate, onError);
            } else {
                customAlert.error("Error","Please fill the Required Fields");
            }
        } else if (sessionMasterCommValue.toLowerCase() == "city") {
            var strCity = $("#txtCity").val();
            strCity = $.trim(strCity);
            var strZip = $("#txtZip").val();
            strZip = $.trim(strZip);
            var strZip4 = $("#txtZip4").val();
            strZip4 = $.trim(strZip4);

            var isActive = parseInt($("#cmbStatusZip").val());
            var strStateId = $("#cmbState").val();
            var strAreaCode = $("#txtAreaCode").val();
            var dataObj = {
                "city": strCity,
                "zip": strZip,
                "zipFour": strZip4,
                "stateId": strStateId,
                "countyId": null,
                "areaCode": strAreaCode,
                "isActive": isActive,
                "isDeleted": 0
            };


            if(parentRef.operation == "edit"){
                dataObj.id = atID;
                dataObj.modifiedBy = sessionStorage.userId;
                urlExtn = getcommunicationForm() + '/update';
            }
            else{
                dataObj.createdBy= sessionStorage.userId;
            }

            if (strCity != "" && ((strZip != "" && IsPostalFlag != "1") || IsPostalFlag == 1) && strStateId != "" && isActive != "") {
                var dataUrl = ipAddress + urlExtn;
                createAjaxObject(dataUrl, dataObj, "POST", onCreate, onError);
            } else {
                customAlert.error("Error","Please fill the Required Fields");
            }
        }
    });

    $("#cmbState").change(function() {
        onStateChange();
    });



    $("#btnEdit").off("click",onClickEdit);
    $("#btnEdit").on("click",onClickEdit);

    $("#btnDelete").on('click', function(e) {
        customAlert.confirm("Confirm", "Are you sure to delete?",function(response){
            if(response.button == "Yes"){
                e.preventDefault();
                var deleteItemObj = {
                    "id": atID,
                    "isActive": 0,
                    "isDeleted": 1,
                    "modifiedBy": sessionStorage.userId
                }
                var urlExtn = getcommunicationForm() + '/delete';
                var dataUrl = ipAddress + urlExtn;
                createAjaxObject(dataUrl, deleteItemObj, "POST", onDeleteCountryt, onError);
            }
        });
    });

    $("#btnAdd").off("click");
    $("#btnAdd").on("click",onClickAdd);

    $("#btnCancel").off("click",onClickCancel);
    $("#btnCancel").on("click",onClickCancel);

    $("#btnReset").off("click",onClickReset);
    $("#btnReset").on("click",onClickReset);

    $(".popupClose").off("click", onClickCancel);
    $(".popupClose").on("click", onClickCancel);

    $(".btnActive").on("click", function(e) {
        activeListFlag = true;
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('active');
        onClickActive();
    });
    $(".btnInActive").on("click", function(e) {
        activeListFlag = false;
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('inactive');
        onClickInActive();
    });

    allowAlphabets("txtAbbreviation");
    allowAlphaNumericwithSapce("txtDescription");
}

function onStateChange(){
    var selectId = $("#cmbState option:selected").val();
    for(var i=0;i<tempCountryDataArry.length;i++){
        if(tempCountryDataArry[i].idk == selectId) {
            $("#txtCountry").val(tempCountryDataArry[i].country);
        }
    }
}


function searchOnLoad(status) {
    buildCountryListGrid([]);
    buildStateListGrid([]);
    buildZipListGrid([]);
    var urlExtn = getcommunicationForm() + '/list/';
    getAjaxObject(ipAddress + urlExtn, "GET", getSearchList, onError);
}

function onCreate(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "Created Successfully";
            if(operation == UPDATE){
                msg = "Updated Successfully"
            }
            displaySessionErrorPopUp("Info", msg, function(res) {
                operation = ADD;
                // init();
                $("#btnCancel").click();
                $(".btnActive").click();
            })
        }else{
            customAlert.error("Error", dataObj.response.status.message);
        }
    }
    onClickReset();
}

function onClickCancel(){
    getCommunicationTypes();
    $("#viewDivBlock").show();
    $("#addPopup").hide();
}

function onClickEdit(){
    $("#txtID").show();

    parentRef.operation = "edit";
    $("#viewDivBlock").hide();
    $("#addPopup").show();
    if(sessionMasterCommValue.toLowerCase() == "country") {
        var selectedItems = countryUIGridWrapper.getSelectedRows();
        if (selectedItems && selectedItems.length > 0) {
            $(".filter-heading").html("Edit Country");
            var obj = selectedItems[0];
            atID = obj.idDuplicate;
            $("#txtAbbreviation").val(obj.abbr);
            $("#txtCode").val(obj.code);
            $("#txtName").val(obj.country);
            $("#txtID").html("ID : " + obj.idDuplicate);
            $("#cmbStatusCountry").val(obj.isActive);
        }
    }
    else if(sessionMasterCommValue.toLowerCase() == "state"){
        var selectedItems = stateUIGridWrapper.getSelectedRows();
        if (selectedItems && selectedItems.length > 0) {
            $(".filter-heading").html("Edit County");
            var obj = selectedItems[0];
            $("#txtAbbreviations").val(obj.abbr);
            $("#txtCodes").val(obj.code);
            $("#txtNames").val(obj.state);
            $("#txtID").html("ID : " + obj.idDuplicate);
            $("#cmbStatusStates").val(obj.isActive);
            $("#cmbCountry").val(obj.countryId);
        }
    }
    else if(sessionMasterCommValue.toLowerCase() == "city"){
        var selectedItems = zipUIGridWrapper.getSelectedRows();
        if (selectedItems && selectedItems.length > 0) {
            $(".filter-heading").html("Edit City");
            var obj = selectedItems[0];
            $("#txtCity").val(obj.city);
            $("#txtZip").val(obj.zip);
            $("#txtZip4").val(obj.zipFour);
            $("#txtID").html("ID : " + obj.idDuplicate);
            $("#cmbStatusZip").val(obj.isActive);
            $("#txtAreaCode").val(obj.areaCode);
            $("#cmbState").val(obj.stateId);
            onStateChange();
        }
    }
}

var operation = "add";
var selFileResourceDataItem = null;

function onStatusChange(){
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if(cmbStatus && cmbStatus.selectedIndex<0){
        cmbStatus.select(0);
    }
}

function onClickReset() {
    if(operation == ADD) {
        operation = ADD;
    }
    else {
        operation = UPDATE;
    }
    $("#txtAbbreviation").val("");
    $("#txtAbbreviations").val("");

    $("#txtDescription").val("");
    $("#cmbStatus").val(1);
    $("#txtCode").val("");
    $("#txtCodes").val("");
    $("#txtNames").val("");
    $("#txtName").val();
    $("#cmbStatusCountry").val(1);
    $("#cmbStatusCountrys").val(1);
    $("#txtCity").val("");
    $("#txtAreaCode").val("");
    $("#txtZip").val("");
    $("#txtZip4").val("");
    $("#cmbState").val("");
    $("#txtCountry").val("");
    $("#cmbStatusZip").val("1");
}


function onError(errorObj){
    console.log(errorObj);
}
function getCodeTableList(dataObj){
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.codeTableName){
        if($.isArray(dataObj.response.codeTableName)){
            dataArray = dataObj.response.codeTableName;
        }else{
            dataArray.push(dataObj.response.codeTableName);
        }
    }
    var tempDataArry = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
        }
    }
    var obj = {};
    obj.tableName = "";
    obj.idk = "";
    dataArray.unshift(obj);
    setDataForSelection(dataArray, "txtTableName", onTableChange, ["tableName", "idk"], 0, "");
    onTableChange();
}
function onTableChange(){
    var txtTableName = $("#txtTableName").data("kendoComboBox");
    if(txtTableName && txtTableName.selectedIndex<0){
        txtTableName.select(0);
    }
    buildCountryListGrid([]);
    if(txtTableName.selectedIndex>0){
        getTableValueList();
    }

}

function getCodeTableValueList(dataObj){
    console.log(dataObj);
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.codeTable){
        if($.isArray(dataObj.response.codeTable)){
            dataArray = dataObj.response.codeTable;
        }else{
            dataArray.push(dataObj.response.codeTable);
        }
    }
    var tempDataArry = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
        }
    }
    buildCountryListGrid(dataArray);
}


function adjustHeight(){
    var defHeight = 200;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;

    if (sessionMasterCommValue.toLowerCase() == "country") {
        countryUIGridWrapper.adjustGridHeight(cmpHeight);
    } else if (sessionMasterCommValue.toLowerCase() == "state") {
        stateUIGridWrapper.adjustGridHeight(cmpHeight);
    } else if (sessionMasterCommValue.toLowerCase() == "city") {
        zipUIGridWrapper.adjustGridHeight(cmpHeight);
    }
}

function buildCountryListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Abbreviation",
        "field": "abbr",
    });
    if(sessionStorage.countryName.indexOf("United Kingdom") >= 0){
        if(IsPostalCodeManual != "1"){
            gridColumns.push({
                "title": "Calling Code",
                "field": "code",
            });
        }
    }
    gridColumns.push({
        "title": "Country",
        "field": "country",
    });

    countryUIGridWrapper.creategrid(dataSource, gridColumns, otoptions);
    adjustHeight();
}

var prevSelectedItem =[];
function onChange(){
    setTimeout(function(){
        if (sessionMasterCommValue.toLowerCase() == "country") {
            selectedItems = countryUIGridWrapper.getSelectedRows();
        } else if (sessionMasterCommValue.toLowerCase() == "state") {
            selectedItems = stateUIGridWrapper.getSelectedRows();
        } else if (sessionMasterCommValue.toLowerCase() == "city") {
            selectedItems = zipUIGridWrapper.getSelectedRows();
        }
        if(selectedItems && selectedItems.length>0){
            var obj = selectedItems[0];
            atID = obj.idDuplicate;
            $("#btnEdit").prop("disabled", false);
            $("#btnDelete").prop("disabled", false);
        }else{
            $("#btnEdit").prop("disabled", true);
            $("#btnDelete").prop("disabled", true);
        }
    },100);
}

function onClickOK(){
    setTimeout(function(){
        var selectedItems = countryUIGridWrapper.getSelectedRows();
        console.log(selectedItems);
        if(selectedItems && selectedItems.length>0){
            // var txtTableName = $("#txtTableName").data("kendoComboBox");
            var obj = {};
            obj.selItem = selectedItems[0];
            parentRef.selItem = selectedItems[0];
            // parentRef.selItem.tbk = txtTableName.value();
            parentRef.operation = "update";
            addValues();
        }
    })
}

function onDeleteCountryt(dataObj){
    if(dataObj && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            msg = "Deleted successfully";
            displaySessionErrorPopUp("Info", msg, function(res) {
                init();
            })
        }else{
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}
function onClickAdd(){
    parentRef.selItem = null;
    $("#addPopup").show();
    $('#viewDivBlock').hide();
    $("#txtID").hide();
    onClickReset();
    parentRef.operation = "add";
    if ((sessionMasterCommValue.toLowerCase()) == "country") {
        $(".filter-heading").html("Add Country");
    } else if ((sessionMasterCommValue.toLowerCase()) == "state") {
        $(".filter-heading").html("Add State");
        if(sessionStorage.countryName.indexOf("United Kingdom") >= 0) {
            $(".filter-heading").html("Add County");
        }

    } else if ((sessionMasterCommValue.toLowerCase()) == "city") {
        $(".filter-heading").html("Add City");
    }

}


function onClickActive() {
    $(".btnInActive").removeClass("radioButton-active");
    $(".btnActive").addClass("radioButton-active");
}

function onClickInActive() {
    $(".btnActive").removeClass("radioButton-active");
    $(".btnInActive").addClass("radioButton-active");
}

function themeAPIChange(){
    getAjaxObject(ipAddress+"/homecare/settings/?id=2","GET",getThemeValue,onError);
}


function getThemeValue(dataObj){
    console.log(dataObj);
    var tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.activityTypes){
            if($.isArray(dataObj.response.settings)){
                tempCompType = dataObj.response.settings;
            }else{
                tempCompType.push(dataObj.response.settings);
            }
        }
        if(tempCompType.length > 0){
            themesChange(tempCompType[0].value);
        }
    }
}

function themesChange(themeValue){
    if(themeValue == 2){
        loadAPi("../../Theme2/Theme02.css");
    }
    else if(themeValue == 3){
        loadAPi("../../Theme3/Theme03.css");
    }
}

function getcommunicationForm() {
    var formUrl;
    if ((sessionMasterCommValue.toLowerCase()) == "country") {
        formUrl = '/country';
    } else if ((sessionMasterCommValue.toLowerCase()) == "state") {
        formUrl = '/state';
    } else if ((sessionMasterCommValue.toLowerCase()) == "city") {
        formUrl = '/city';
    }
    return formUrl;
}


function getCountryZoneName() {
    if (sessionStorage.countryName.indexOf("India") >= 0) {
        // $(".sidebar-nav-link").each(function() {
        //     if ($(this).attr('data-link-name') == 'zipcode') {
        //         $(this).text("Postal Code");
        //     }
        // });
        $('.zipFourWrapper').hide();
        $('.zipWrapper').find('label').html('Postal Code : <span class="mandatoryClass">*</span>');
    } else if (sessionStorage.countryName.indexOf("United Kingdom") >= 0) {
        // $(".sidebar-nav-link").each(function() {
        //     if ($(this).attr('data-link-name') == 'zipcode') {
        //         if(IsPostalCodeManual == "1"){
        //             // $(this).text("City");
        //             IsPostalFlag = "1";
        //             // $('.zipWrapper').css("display","none");
        //         }
        //         else{
        //             $(this).text("Postal Code");
        //         }
        //     }
        //     if ($(this).attr('data-link-name') == 'state') {
        //         $(this).text("County");
        //     }
        //     $('.stateWrapper').find('label').html('County : <span class="mandatoryClass">*</span>');
        // });
        if(IsPostalCodeManual == "1"){
            IsPostalFlag = "1";
            $('.zipWrapper').css("display","none");
        }
        $('.stateWrapper').find('label').html('County :');
        $('.zipFourWrapper').hide();
        $('.zipWrapper').find('label').html('Postal Code : <span class="mandatoryClass">*</span>');

    } else {
        $('.zipFourWrapper').show();
    }
}

function getCommunicationTypes(){
    if(sessionMasterCommValue.toLowerCase() == "country"){
        searchOnComm("country");
        $("#countryForm").css("display","");
        $("#communicationCountryList").css("display","");
        $(".filter-heading").html("View Countries");
    }
    else if(sessionMasterCommValue.toLowerCase() == "state"){
        searchOnComm("state");
        $("#stateForm").css("display","");
        $("#communicationStateList").css("display","");
        $(".filter-heading").html("View Counties");
        $("#countryForm").css("display","none");
        $("#communicationCountryList").css("display","none");
    }
    else if (sessionMasterCommValue.toLowerCase() == "city"){
        searchOnComm("zip");
        $("#zipForm").css("display","");
        $("#communicationZipList").css("display","");
        $(".filter-heading").html("View Cities");
    }
}


function searchOnComm(getName) {
    if (getName == "country") {
        if(IsPostalFlag == "1") {
            $("#divCallingCode").css("display", "none");
        }
        $(".filter-heading").html("View Countries");
        // $('#communicationCountryList').addClass('showOnTop');
        // $('#communicationStateList').removeClass('showOnTop');
        // $('#communicationZipList').removeClass('showOnTop');
        // onStatusChangeCountry();
    } else if (getName == "state") {
        if(IsPostalFlag == "1"){
            $("#divAreaCode").css("display","none");
        }
        $(".filter-heading").html("View Counties");

        $('#communicationCountryList').removeClass('showOnTop');
        $('#communicationStateList').addClass('showOnTop');
        $('#communicationZipList').removeClass('showOnTop');

    } else if (getName == "zip") {
        //buildZipListGrid([]);
        if(IsPostalFlag == "1"){
            $(".filter-heading").html("View Cities");
            $("#divCityAreaCode").css("display","none");
            $("#divCityZip").css("display","none");
        }
        else{
            $(".filter-heading").html("View Postal Codes");
        }

        // $('#communicationCountryList').removeClass('showOnTop');
        // $('#communicationStateList').removeClass('showOnTop');
        // $('#communicationZipList').addClass('showOnTop');
    }
}
function onlyAlphabets(e) {
    try {
        if (window.event) {
            var charCode = window.event.keyCode;
        } else if (e) {
            var charCode = e.which;
        } else { return true; }
        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || (charCode == 32))
            return true;
        else
            return false;
    } catch (err) {
        alert(err.Description);
    }
}


function buildStateListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Abbreviation",
        "field": "abbr",
    });
    if(IsPostalFlag != "1") {
        gridColumns.push({
            "title": "Area Code",
            "field": "code",
        });
    }
    if (sessionStorage.countryName.indexOf("United Kingdom") >= 0) {

        if (sessionMasterCommValue.toLowerCase()== 'state') {
            gridColumns.push({
                "title": "County",
                "field": "state",
            });
        }

    } else {
        gridColumns.push({
            "title": "County",
            "field": "state",
        });
    }
    gridColumns.push({
        "title": "Country",
        "field": "country",
    });
    // gridColumns.push({
    //     "title": "Status",
    //     "field": "Status",
    // });
    stateUIGridWrapper.creategrid(dataSource, gridColumns, otoptions);
    adjustHeight();
}


function buildZipListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "City",
        "field": "city",
    });
    if (sessionStorage.countryName.indexOf("India") >= 0) {
        if (sessionMasterCommValue.toLoweCase() == 'city') {
            gridColumns.push({
                "title": "Postal Code",
                "field": "zip",
            });
        }
        $('.zipFourWrapper').hide();
    }else if(sessionStorage.countryName.indexOf("United Kingdom") >= 0){
        if(IsPostalCodeManual != "1"){
            if (sessionMasterCommValue.toLowerCase() == 'city') {
                gridColumns.push({
                    "title": "Postal Code",
                    "field": "zip",
                });
            }
            $('.zipFourWrapper').hide();
        }
    }
    else {
        gridColumns.push({
            "title": "Zip",
            "field": "zip",
        });
        gridColumns.push({
            "title": "Zip4",
            "field": "zipFour",
        });
        $('.zipFourWrapper').show();
    }
    if (sessionStorage.countryName.indexOf("United Kingdom") >= 0) {
        if (sessionMasterCommValue.toLowerCase() == 'city') {
            gridColumns.push({
                "title": "County",
                "field": "state",
            });
        }
    } else {
        gridColumns.push({
            "title": "County",
            "field": "state",
        });
    }
    gridColumns.push({
        "title": "Country",
        "field": "country",
    });
    // gridColumns.push({
    //     "title": "Status",
    //     "field": "Status",
    // });
    zipUIGridWrapper.creategrid(dataSource, gridColumns, otoptions);
    adjustHeight();
}


function getCountryList(dataObj) {
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.country) {
        if ($.isArray(dataObj.response.country)) {
            dataArray = dataObj.response.country;
        } else {
            dataArray.push(dataObj.response.country);
        }
    }
    var tempDataArry = [];
    for (var i = 0; i < dataArray.length; i++) {
        dataArray[i].idk = dataArray[i].id;
        $("#cmbCountry").append('<option value="'+dataArray[i].idk+'">'+dataArray[i].country+'</option>');
    }
    // setDataForSelection(dataArray, "cmbCountry", onCountryChange, ["country", "idk"], 0, "");
}

var tempCountryDataArry = [];

function getStateList(dataObj) {
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.stateExt) {
        if ($.isArray(dataObj.response.stateExt)) {
            dataArray = dataObj.response.stateExt;
        } else {
            dataArray.push(dataObj.response.stateExt);
        }
    }

    dataArray.sort(function (a, b) {
        var nameA = a.state.toUpperCase(); // ignore upper and lowercase
        var nameB = b.state.toUpperCase(); // ignore upper and lowercase
        if (nameA < nameB) {
            return -1;
        }
        if (nameA > nameB) {
            return 1;
        }
        // names must be equal
        return 0;
    });

    tempCountryDataArry = dataArray;
    for (var i = 0; i < dataArray.length; i++) {
        dataArray[i].idk = dataArray[i].id;
        $("#cmbState").append('<option value="'+dataArray[i].idk+'">'+dataArray[i].state+'</option>');
    }

    // console.log(dataArray);
    // setDataForSelection(dataArray, "cmbState", onStateChange, ["state", "idk"], 0, "");
}

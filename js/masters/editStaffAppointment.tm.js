var angularUIgridWrapper;
var parentRef = null;
var recordType = "1";
var dataArray = [], facilityListArray = [];
var ADD = "add";
var UPDATE = "edit";
var VIEW = "view";
var DELETE = "delete";
var operation = "add";
var ds  = "";
var IsFlag = 1;
var cntry = sessionStorage.countryName;
var selectPatientId, selectProviderId;
var dtFMT = "dd/MM/yyyy";
var appointmentdtFMT = "dd/MM/yyyy hh:mm tt";
var appointmentId;
var selItem;
var patientBillArray = [];
var typeWeek;
var pateintId, apptId, facilityId;
var appointmentDetails;

$(document).ready(function () {

    sessionStorage.setItem("IsSearchPanel", "1");
    themeAPIChange();
    parentRef = parent.frames['iframe'].window;

    $("#btnCancel").off("click",onClickCancel);
    $("#btnCancel").on("click",onClickCancel);

    $(".popupClose").off("click", onClickCancel);
    $(".popupClose").on("click", onClickCancel);

});


$(window).load(function(){
    loading = false;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
    init();
    buttonEvents();
    adjustHeight();
    if (parentRef.selectedItems != null && parentRef.selectedItems.length> 0) {
        // selectPatientId = parentRef.selectedItems[0].pateintId;
        apptId = parentRef.selectedItems[0].id;

    }

    getPatientDetails();


    callMastersData();



}

function getPatientDetails() {
    getAjaxObjectAsync(ipAddress + "/appointment/list/?is-active=1&is-deleted=0&id=" + apptId, "GET", buildAppointmentList, onError);
}

function buildAppointmentList(dataObj) {
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1" && dataObj.response.appointment){
        if($.isArray(dataObj.response.appointment)){
            dataArray = dataObj.response.appointment;
        }else{
            dataArray.push(dataObj.response.appointment);
        }
    }
    if(dataArray.length > 0) {
        parentRef.selectedApptItems = dataObj.response.appointment;
        appointmentDetails = dataObj.response.appointment;
        if (dataObj.response.appointment != null && dataObj.response.appointment.length > 0 && dataObj.response.appointment[0].facilityId) {
            facilityId = dataObj.response.appointment[0].facilityId;
        }
        var patientObj = dataArray[0].composition.patient ? dataArray[0].composition.patient : '';
        if(patientObj != "") {
            selectPatientId =  patientObj.id;
            if(selectPatientId != 0) {
                $("#txtSU").val(patientObj.lastName + " " + patientObj.firstName + " " + (patientObj.middleName != null ? patientObj.middleName : " "));
            }
            else{
                $("#txtSU").val("");
            }
        }


        var providerObj = dataArray[0].composition.provider ? dataArray[0].composition.provider : '';
        selectProviderId = providerObj.id;
        if (selectProviderId != 0) {
            $("#txtStaff").val(providerObj.lastName + " " + providerObj.firstName + " " + (providerObj.middleName != null ? providerObj.middleName : " "));

        }
        else {
            $("#txtStaff").val("");
        }
    }

}

function init(){
    $("#txtStartDate").kendoDateTimePicker({format: appointmentdtFMT });
    $("#txtEndDate").kendoDateTimePicker({format: appointmentdtFMT });
}



function onError(errorObj){
    console.log(errorObj);
}

function buttonEvents(){

    $("#btnCancel").off("click",onClickCancel);
    $("#btnCancel").on("click",onClickCancel);

    $("#btnStaff").off("click");
    $("#btnStaff").on("click",onClickStaff);

    $("#btnAdd").off("click");
    $("#btnAdd").on("click",onClickAdd);

    $("#btnSave").off("click");
    $("#btnSave").on("click",function(){
        appointmentSave();
    });

}

function searchOnLoad(status) {
    buildDeviceListGrid([]);
    if(status == "active") {
        var urlExtn = ipAddress+"/homecare/vacations/?fields=*&parentTypeId="+Number(parentRef.type)+"&parentId="+Number(parentRef.patientId)+"&is-active=1&is-deleted=0";

    }
    else if(status == "inactive") {
        var urlExtn =  ipAddress+"/homecare/vacations/?fields=*&parentTypeId="+Number(parentRef.type)+"&parentId="+Number(parentRef.patientId)+"&is-active=0&is-deleted=1"
    }
    getAjaxObject(urlExtn,"GET",handleGetVacationList,onError);
}


function onClickAdd(e){
    $("#addPopup").show();
    $('#viewDivBlock').hide();
    $("#txtID").hide();
    $(".filter-heading").html("Add Appointment");
    parentRef.operation = "add";
    onClickReset();
}


function onClickActive() {
    $(".btnInActive").removeClass("radioButton-active");
    $(".btnActive").addClass("radioButton-active");
}

function onClickInActive() {
    $(".btnActive").removeClass("radioButton-active");
    $(".btnInActive").addClass("radioButton-active");
}

function onClickDelete(){
    customAlert.confirm("Confirm", "Are you sure to delete?",function(response){
        if(response.button == "Yes") {

            var selectedItems = angularUIgridWrapper.getSelectedRows();
            // dataObj.id = selectedItems[0].id;
            // parentRef.appselList.removeAt(0);
            if (parentRef.appselList.length > 0) {
                // delete parentRef.appselList[selectedItems[0].id];
                // var response = _.where(parentRef.appselList, {id: 1})

                var selGridData = angularUIgridWrapper.getAllRows();
                var dataList = [];
                var selList = [];
                for (var i = 0; i < selGridData.length; i++) {
                    var dataRow = selGridData[i].entity;
                    if(!dataRow.SEL){
                        var dataList1;
                        selList.push(dataRow);
                        dataList1 = parentRef.appselList.filter(function (item){
                            return item.idk == dataRow.idk;});
                        dataList.push(dataRow);
                        if(dataList1.length == 0) {
                            dataList1 = parentRef.appselList.filter(function (item) {
                                return item.id == dataRow.id;
                            });
                        }
                    }
                }
                buildDeviceListGrid([]);
                var msg =  "Deleted successfully from copy appointments list";
                if(typeWeek == "1") {
                    parentRef.appselList = dataList;
                    parentRef.appselList.sort(function(a,b){
                        return new Date(a.dateTimeOfAppointment) - new Date(b.dateTimeOfAppointment);
                    });
                    displaySessionErrorPopUp("Info", msg, function(res) {
                        buildDeviceListGrid(parentRef.appselList);
                    })
                }else if(typeWeek=="2"){
                    parentRef.appselDayList = dataList;
                    parentRef.appselDayList.sort(function(a,b){
                        return new Date(a.dateTimeOfAppointment) - new Date(b.dateTimeOfAppointment);
                    });
                    displaySessionErrorPopUp("Info", msg, function(res) {
                        buildDeviceListGrid(parentRef.appselDayList);
                    })
                }
                else{
                    parentRef.appselEmptyList = dataList;
                    parentRef.appselEmptyList.sort(function(a,b){
                        return new Date(a.dateTimeOfAppointment) - new Date(b.dateTimeOfAppointment);
                    });
                    displaySessionErrorPopUp("Info", msg, function(res) {
                        buildDeviceListGrid(parentRef.appselEmptyList);
                    })
                }
            } else {
                customAlert.info("info", "Records are not exist");
            }
        }
    });
}

function onCreateDelete(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "Vacation deleted successfully";
            customAlert.error("Info", msg);
            operation = ADD;
            init();
        }
    }
}


function onCreate(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "Appointment created successfully";
            displaySessionErrorPopUp("Info", msg, function(res) {
                if(parentRef.appselList.length > 0 ) {
                    delete parentRef.appselList[gridId];
                }
                onClickReset();
                init();
                popupClose(false);
            })
        }else{
            customAlert.error("Error", dataObj.response.status.message);
        }
    }else{

    }
    onClickReset();
}


function onClickCancel(){

    // $("a.k-window-action.k-link").trigger("click");
    var onCloseData = new Object();
    // onCloseData.status = st;
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(onCloseData);
    //bindData();
}

function adjustHeight(){
    var defHeight = 230;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    //angularUIgridWrapper.adjustGridHeight(cmpHeight);
}

function buildDeviceListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    otoptions.rowHeight = 100;
    gridColumns.push({
        "title": "Select",
        "field": "SEL",
        "cellTemplate": showCheckBoxTemplate(),
        "headerCellTemplate": "<input type='checkbox' class='check1' ng-model='TCSELECT' onclick='toggleSelectAll(event);' style='margin: 6px 0 0 12px !important; display: inline-block;'></input>",
        "width":"5%"
    });
    gridColumns.push({
        "title": "Start Date",
        "field": "dateTimeOfAppointment1",
        "enableColumnnMenu": false,
        "width": "22%"
    });
    gridColumns.push({
        "title": "End Date",
        "field": "dateTimeOfAppointmentED",
        "enableColumnnMenu": false,
        "width": "22%"
    });
    gridColumns.push({
        "title": "Status",
        "field": "appointmentTypeDesc",
        "enableColumnMenu": false,
    });
    gridColumns.push({
        "title": "Reason",
        "field": "appointmentReasonDesc",
        "enableColumnMenu": false,
    });
    gridColumns.push({
        "title": "Service User",
        "field": "name",
        "enableColumnMenu": false,
    });
    gridColumns.push({
        "title": "Staff",
        "field": "staffName",
        "enableColumnMenu": false,
    });

    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
}

function onChange(){
    setTimeout(function() {
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        if (selectedItems && selectedItems.length > 0) {
            selItem = selectedItems[0];
            $("#btnEdit").prop("disabled", false);
            // if(selItem.SEL) {
            $("#btnDelete").prop("disabled", false);
            // }
            // else{
            //     $("#btnDelete").prop("disabled", true);
            // }
        }
        else {
            $("#btnEdit").prop("disabled", true);

            // if(selItem.SEL) {
            //     $("#btnDelete").prop("disabled", false);
            // }else{
            $("#btnDelete").prop("disabled", true);
            // }
        }
    });
}
var editbilloName;
function onClickEdit(){
    init();
    $(".filter-heading").html("Edit Appointment");
    parentRef.operation = "edit";
    operation =  "edit";
    $("#viewDivBlock").hide();
    $("#addPopup").show();
    var selectedItems = parentRef.selectedItems;
    if (selectedItems && selectedItems.length > 0) {
        var obj = selectedItems[0];
        if(obj.idk){
            appointmentId = obj.idk;
        }
        else{
            appointmentId = obj.id;
        }


        //
        // selectPatientId = obj.patientId;
        selectProviderId = obj.providerId;
        if(selectProviderId != 0) {
            $("#txtStaff").val(obj.lastName + " " + obj.firstName + " " + (obj.middleName != null ? obj.middleName : " "));

        }
        else{
            $("#txtStaff").val("");
        }



        $("#txtFacility").val(parentRef.facilityName);
        var txtStartDate = $("#txtStartDate").data("kendoDateTimePicker");
        if(txtStartDate){
            txtStartDate.value(GetDateTimeEdit(obj.dateOfAppointment));
        }
        var endappoinment = (obj.dateOfAppointment + (Number(obj.duration)*60000));
        var txtEndDate = $("#txtEndDate").data("kendoDateTimePicker");
        if(txtEndDate){
            txtEndDate.value(GetDateTimeEdit(endappoinment));
        }
        $("#txtPayout").val(obj.hourlyRate);

    }
}


var operation = "add";
var selFileResourceDataItem = null;


function onClickReset() {
    $("#cmbReason").val("");
    $("#cmbStatus").val("Con");
    $("#txtDateofAppointment").val("");
    $("#txtStaff").val("");
    operation = ADD;
    selectProviderId = "";
    var startDate = $("#txtStartDate").data("kendoDateTimePicker");
    if(startDate){
        startDate.value("");
    }
    var endDate = $("#txtEndDate").data("kendoDateTimePicker");
    if(endDate){
        endDate.value("");
    }
    $("#cmbBilling").val("");
    $("#txtPayout").val("");
    appointmentId = null;


}

function themeAPIChange(){
    getAjaxObject(ipAddress+"/homecare/settings/?id=2","GET",getThemeValue,onError);
}

function getThemeValue(dataObj){
    console.log(dataObj);
    var tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.activityTypes){
            if($.isArray(dataObj.response.settings)){
                tempCompType = dataObj.response.settings;
            }else{
                tempCompType.push(dataObj.response.settings);
            }
        }
        if(tempCompType.length > 0){
            themesChange(tempCompType[0].value);
        }
    }
}

function themesChange(themeValue){
    if(themeValue == 2){
        loadAPi("../../Theme2/Theme02.css");
    }
    else if(themeValue == 3){
        loadAPi("../../Theme3/Theme03.css");
    }
}


function onClickSubmit() {
    buildDeviceListGrid([]);
    typeWeek = $("#cmbTypeAppointment").val();
    angularUIgridWrapper.refreshGrid();
    parentRef = parent.frames['iframe'].window;
    var appselList;


    var txtAppPTDate = $("#txtAppPTDate").data("kendoDatePicker");
    if (txtAppPTDate.value() !== "" && txtAppPTDate.value() != null) {
        // alert(GetDateTime("txtAppPTDate"));
        // alert(GetDateTime(parentRef.selValue));
        // alert(GetDateTimeEdit(parentRef.selValue).getTime());

        var diffDays;
        var day = txtAppPTDate.value().getDay();
        for (var i = 0; parentRef.selValue.length > 0; i++) {
            if (parentRef.selValue[i].getDay() == day) {
                // alert(parentRef.selValue[i]);
                diffDays = showDays(GetFormattedDate(txtAppPTDate.value()), GetFormattedDate(GetFormattedDate(parentRef.selValue[i])));
                break;
            }

        }
        if(typeWeek == "1") {
            appselList = JSON.parse(sessionStorage.appselList);
            for (var i = 0; i < (appselList.length); i++) {
                var date = new Date(appselList[i].dateTimeOfAppointment1);
                appselList[i].dateTimeOfAppointment = date.setDate(date.getDate() + diffDays);
                // parentRef.appselList[i].dateOfAppointment = date.setDate(date.getDate() + diffDays);
                appselList[i].rowId = i;

                // if(parentRef.appselList[i] && parentRef.appselList[i].dateTimeOfAppointment) {
                var day = new Date(GetDateTimeEditDay(appselList[i].dateTimeOfAppointment)).getDay();
                var dow = getWeekDayName(day);

                appselList[i].dateTimeOfAppointment1 = dow + " " + GetDateTimeEdit(appselList[i].dateTimeOfAppointment);
                // }
                var dtEnd = appselList[i].dateTimeOfAppointment + (Number(appselList[i].duration) * 60000);


                var dateED = new Date(GetDateTimeEditDay(dtEnd));
                var dayED = dateED.getDay();
                var dowED = getWeekDayName(dayED);

                appselList[i].dateTimeOfAppointmentED = dowED + " " + GetDateTimeEdit(dtEnd);


            }
        }
        else if(typeWeek == "2") {
            appselList = JSON.parse(sessionStorage.appselList);
            var copyDate = new Date(txtAppPTDate.value());
            var copyDay = copyDate.getDay();
            var copyDow = getWeekDayName(copyDay);


            var daylist = [];
            for (var i = 0; i < (appselList.length); i++) {
                if(appselList[i].DW == copyDow) {

                    var date = new Date(appselList[i].dateTimeOfAppointment1);
                    var dateOfAppointment = kendo.toString(appselList[i].dateTimeOfAppointment1, "MM/dd/yyyy");
                    dateOfAppointment = new Date(dateOfAppointment);

                    var day = dateOfAppointment.getDate();
                    var month = dateOfAppointment.getMonth();
                    month = month + 1;
                    var year = dateOfAppointment.getFullYear();

                    var stDate = month + "/" + day + "/" + year;
                    dateOfAppointment = new Date(stDate).getTime();

                    var copyDate = kendo.toString(parentRef.copyDate, "MM/dd/yyyy");
                    copyDate = new Date(copyDate).getTime();
                    // if (copyDate == dateOfAppointment) {
                    appselList[i].dateTimeOfAppointment = date.setDate(date.getDate() + diffDays);
                    // parentRef.appselList[i].dateOfAppointment = date.setDate(date.getDate() + diffDays);
                    appselList[i].rowId = i;

                    // if(parentRef.appselList[i] && parentRef.appselList[i].dateTimeOfAppointment) {
                    var day = new Date(GetDateTimeEditDay(appselList[i].dateTimeOfAppointment)).getDay();
                    var dow = getWeekDayName(day);

                    appselList[i].dateTimeOfAppointment1 = dow + " " + GetDateTimeEdit(appselList[i].dateTimeOfAppointment);
                    // }
                    var dtEnd = appselList[i].dateTimeOfAppointment + (Number(appselList[i].duration) * 60000);


                    var dateED = new Date(GetDateTimeEditDay(dtEnd));
                    var dayED = dateED.getDay();
                    var dowED = getWeekDayName(dayED);

                    appselList[i].dateTimeOfAppointmentED = dowED + " " + GetDateTimeEdit(dtEnd);

                    daylist.push(appselList[i]);
                    // }
                }

            }
            if(!(daylist.length > 0)){
                customAlert.info("info","No record(s) to copy");
            }
        }

        // var copyDay = new Date(parentRef.copyDate).getDay();
        // var copydow = getWeekDayName(copyDay);

        buildDeviceListGrid([]);
        // buildDeviceListGrid(appselList);
        // appselList = null;
        // var startDate = GetDateEdit(txtAppPTDate.value());
        // var resultProductData = appselList.filter(
        //     function (a)
        //     {
        //         return (a.dateTimeOfAppointment) > startDate && (a.ProductHits) < endDate;
        //     });

        if(typeWeek == "1"){
            parentRef.appselList = appselList;
            angularUIgridWrapper.refreshGrid();
            buildDeviceListGrid([]);
            setTimeout(function() {
                buildDeviceListGrid(parentRef.appselList);
            },1000);
            angularUIgridWrapper.refreshGrid();
        }
        else if(typeWeek == "2"){
            daylist.sort(function(a,b){
                // Turn your strings into dates, and then subtract them
                // to get a value that is either negative, positive, or zero.
                return new Date(a.dateTimeOfAppointment) - new Date(b.dateTimeOfAppointment);
            });
            parentRef.appselDayList = daylist;
            angularUIgridWrapper.refreshGrid();
            buildDeviceListGrid([]);
            setTimeout(function() {
                buildDeviceListGrid(daylist);
            },1000);
            angularUIgridWrapper.refreshGrid();
        }else{
            buildDeviceListGrid([]);
        }
        // onbindGrid(appselList);
    }
    else {
        customAlert.error("Error", "Please select date");
    }
}

// function onbindGrid(appselList) {
//     buildDeviceListGrid([]);
//     parentRef.appselList = appselList;
//
//     buildDeviceListGrid(parentRef.appselList);
// }

function getAppointmentList(dataObj){
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.codeTable){
        if($.isArray(dataObj.response.codeTable)){
            dataArray = dataObj.response.codeTable;
        }else{
            dataArray.push(dataObj.response.codeTable);
        }
    }

    for(var i=0;i<dataArray.length;i++){
        if(dataArray[i].isActive == 1){
            $("#cmbStatus").append('<option value="' + dataArray[i].value + '">' + dataArray[i].desc + '</option>');
        }
    }
    $("#cmbStatus").val("Con");

    getAppointmentReasons();
}

function getReasonList(dataObj){
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.codeTable){
        if($.isArray(dataObj.response.codeTable)){
            dataArray = dataObj.response.codeTable;
        }else{
            dataArray.push(dataObj.response.codeTable);
        }
    }

    for(var i=0;i<dataArray.length;i++){
        // if(dataArray[i].isActive == 1){
        $("#cmbReason").append('<option value="' + dataArray[i].value + '">' + dataArray[i].desc + '</option>');
        //
    }

    getFacilityMasterList();
}
function callMastersData() {

    getAppointmentTypes();
}

function getAppointmentTypes() {
    getAjaxObject(ipAddress + "/master/appointment_type/list/?is-active=1", "GET", getAppointmentList, onError);
}

function getAppointmentReasons() {
    getAjaxObject(ipAddress + "/master/appointment_reason/list/", "GET", getReasonList, onError);
}

function getFacilityMasterList() {
    getAjaxObject(ipAddress + "/facility/list/?is-active=1", "GET", getFacilityList, onError);
}

function getPatientBills() {
    getAjaxObject(ipAddress + "/carehome/patient-billing/?is-active=1&patientId=" + selectPatientId + "&fields=*,shift.*,travelMode.*,billingType.*,billingPeriod.*,billTo.name,billingRateType.code", "GET", getPatientData, onError);
}

function bindDropDownLists() {

    if (facilityListArray != null && facilityListArray.length > 0) {
        var tempFacilityArr = _.where(facilityListArray, { id: facilityId });

        if (tempFacilityArr != null && tempFacilityArr.length > 0) {
            $("#txtFacility").val(tempFacilityArr[0].name);
        }
    }
    if (appointmentDetails != null && appointmentDetails.length > 0 && appointmentDetails[0].dateOfAppointment && appointmentDetails[0].duration) {
        var txtStartDate = $("#txtStartDate").data("kendoDateTimePicker");
        if (txtStartDate) {
            txtStartDate.value(GetDateTimeEdit(appointmentDetails[0].dateOfAppointment));
        }
        var endappoinment = (appointmentDetails[0].dateOfAppointment + (Number(appointmentDetails[0].duration) * 60000));
        var txtEndDate = $("#txtEndDate").data("kendoDateTimePicker");
        if (txtEndDate) {
            txtEndDate.value(GetDateTimeEdit(endappoinment));
        }

        if (appointmentDetails[0].composition && appointmentDetails[0].composition.appointmentReason.value) {
            $("#cmbReason").val(appointmentDetails[0].composition.appointmentReason.value);
        } else {
            $("#cmbReason").val(appointmentDetails[0].appointmentReason);
        }
        if (appointmentDetails[0].composition && appointmentDetails[0].composition.appointmentType) {
            $("#cmbStatus").val(appointmentDetails[0].composition.appointmentType.value);
        }
        else {
            $("#cmbStatus").val(appointmentDetails[0].appointmentType);
        }

        if (appointmentDetails[0].billToName) {
            $("#cmbBilling").val(appointmentDetails[0].billToName);
        }

        if (appointmentDetails[0].payoutHourlyRate) {
            $("#txtPayout").val(appointmentDetails[0].payoutHourlyRate);
        }
    }


}

function appointmentSave() {
    var arr = [];
    var obj = {};

    if (appointmentDetails != null && appointmentDetails.length > 0) {
        obj = appointmentDetails[0];

        delete obj.providerId;
        delete obj.composition;
        delete obj.createdDate;
        delete obj.appointmentStartDate;
        delete obj.appointmentEndDate;
        delete obj.handoverNotes;
        delete obj.inTime;
        delete obj.isSynced;
        delete obj.modifiedDate;
        delete obj.notes;
        delete obj.outTime;
        delete obj.readBy;
        delete obj.syncTime;
        delete obj.timeSlots;
        obj.modifiedBy = Number(sessionStorage.userId);

        obj.providerId = selectProviderId

    }
    arr.push(obj);

    var dataUrl = ipAddress + "/appointment/update";
    createAjaxObject(dataUrl, arr, "POST", onUpdateAppointment, onError);
}

function onUpdateAppointment(dataObj) {

    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            var msg = "Appointment updated successfully";
            displaySessionErrorPopUp("Info", msg, function (res) {
                onClickCancel();
            })
        } else {
            customAlert.error("Error", dataObj.response.status.message);
        }
    } else {
        customAlert.error("Error", dataObj.response.status.message);
    }

}

function onClickappoinmentsave(IsProvider){
    var txtStartDate = $("#txtStartDate").data("kendoDateTimePicker");
    var txtEndDate = $("#txtEndDate").data("kendoDateTimePicker");
    var sDate = new Date(txtStartDate.value());
    var eDate = new Date(txtEndDate.value());
    var dataList;
    if(sDate.getTime() > eDate.getTime()){
        customAlert.error("Error","End date should be greater than the start date");
    }else {

        if (appointmentId != null) {
            if(typeWeek == "2"){
                var response = _.where(parentRef.appselDayList, {idk: appointmentId})

                if(response.length == 0){
                    response = _.where(parentRef.appselDayList, {id: appointmentId})
                    dataList = parentRef.appselDayList.filter(function (item) {
                        return item.id != appointmentId;
                    });
                }else{
                    dataList = parentRef.appselDayList.filter(function (item) {
                        return item.idk != appointmentId;
                    });
                }


                parentRef.appselDayList = dataList;
            }else if(typeWeek == "1") {
                var response = _.where(parentRef.appselList, {idk: appointmentId})

                if(response.length == 0){
                    response = _.where(parentRef.appselList, {id: appointmentId})
                    dataList = parentRef.appselList.filter(function (item) {
                        return item.id != appointmentId;
                    });
                }else{
                    dataList = parentRef.appselList.filter(function (item) {
                        return item.idk != appointmentId;
                    });
                }

                parentRef.appselList = dataList;
            }else if(typeWeek == "3"){
                var response = _.where(parentRef.appselEmptyList, {idk: appointmentId})

                if(response.length == 0){
                    response = _.where(parentRef.appselEmptyList, {id: appointmentId})
                    dataList = parentRef.appselEmptyList.filter(function (item) {
                        return item.id != appointmentId;
                    });
                }else{
                    dataList = parentRef.appselEmptyList.filter(function (item) {
                        return item.idk != appointmentId;
                    });
                }

                parentRef.appselEmptyList = dataList;
            }
            // var gridList = parentRef.appselList;

            var obj = {};
            obj.createdBy = Number(sessionStorage.userId);
            obj.isActive = 1;
            obj.isDeleted = 0;
            var startDate = $("#txtStartDate").data("kendoDateTimePicker");
            var endDate = $("#txtEndDate").data("kendoDateTimePicker");
            var sDate = new Date(startDate.value());
            var eDate = new Date(endDate.value());
            var diff = eDate.getTime() - sDate.getTime();
            diff = diff / 1000;
            diff = diff / 60;
            var nSec = sDate.getTime();
            // var uSec = getGMTDateFromLocaleDate(nSec);

            obj.dateOfAppointment = nSec;
            obj.dateTimeOfAppointment = nSec;
            obj.duration = diff;
            if(IsProvider == 0){
                obj.providerId = 0;
            }else{
                obj.providerId = Number(selectProviderId);
            }


            obj.facilityId = parentRef.facilityId;
            obj.patientId = Number(selectPatientId);
            obj.appointmentType = $("#cmbStatus").val();
            obj.notes = null;//$("#taArea").val();
            var selSUItem = onGetSUBillingDetails($("#cmbBilling").val());
            obj.shiftValue = selSUItem.shiftValue;
            obj.billingRateType = selSUItem.billingRateTypeCode;
            obj.patientBillingRate = selSUItem.rate;
            obj.billingRateTypeId = selSUItem.billingRateTypeId;
            obj.swiftBillingId = selSUItem.swiftBillingId;
            obj.payoutHourlyRate = $("#txtPayout").val();
            obj.billToName = $("#cmbBilling option:selected").text();

            obj.billToId = selSUItem.billToId;
            obj.appointmentReason = $("#cmbReason").val();

            obj.name = $("#txtSU").val();
            var day = new Date(GetDateTimeEditDay(obj.dateOfAppointment)).getDay();
            var dow = getWeekDayName(day);

            obj.dateTimeOfAppointment1 = dow + " " + GetDateTimeEdit(obj.dateOfAppointment);
            obj.dateTimeOfAppointment = obj.dateOfAppointment;

            obj.appointmentReasonDesc = $("#cmbReason option:selected").text();
            obj.appointmentTypeDesc = $("#cmbStatus option:selected").text();
            obj.staffName = $("#txtStaff").val();

            var dtEnd = obj.dateOfAppointment + (Number(obj.duration) * 60000);


            var dateED = new Date(GetDateTimeEditDay(dtEnd));
            var dayED = dateED.getDay();
            var dowED = getWeekDayName(dayED);

            obj.dateTimeOfAppointmentED = dowED + " " + GetDateTimeEdit(dtEnd);



            var arr = [];
            arr.push(obj);


            var msg = "Updated successfully of copy appointments list";
            displaySessionErrorPopUp("Info", msg, function (res) {
                if(typeWeek == "2"){
                    parentRef.appselDayList.push(obj);
                    parentRef.appselDayList.sort(function(a,b){
                        return new Date(a.dateTimeOfAppointment) - new Date(b.dateTimeOfAppointment);
                    });
                    buildDeviceListGrid(parentRef.appselDayList);
                }else if(typeWeek == "1"){
                    parentRef.appselList.push(obj);
                    parentRef.appselList.sort(function(a,b){
                        return new Date(a.dateTimeOfAppointment) - new Date(b.dateTimeOfAppointment);
                    });
                    buildDeviceListGrid(parentRef.appselList);
                }else if(typeWeek == "3"){
                    buildDeviceListGrid([]);
                }
                // onClickReset();

                $(".filter-heading").html("View Copy Appointments");
                $("#viewDivBlock").show();
                $("#addPopup").hide();
            });

        } else {
            var gridList = parentRef.appselList;
            var obj = {};
            obj.createdBy = Number(sessionStorage.userId);
            obj.isActive = 1;
            obj.isDeleted = 0;
            var startDate = $("#txtStartDate").data("kendoDateTimePicker");
            var endDate = $("#txtEndDate").data("kendoDateTimePicker");
            var sDate = new Date(startDate.value());
            var eDate = new Date(endDate.value());
            var diff = eDate.getTime() - sDate.getTime();
            diff = diff / 1000;
            diff = diff / 60;
            var nSec = sDate.getTime();
            // var uSec = getGMTDateFromLocaleDate(nSec);

            obj.dateOfAppointment = nSec;
            obj.dateTimeOfAppointment = nSec;
            obj.duration = diff;

            obj.providerId = Number(selectProviderId);
            obj.facilityId = parentRef.facilityId;
            obj.patientId = Number(selectPatientId);
            obj.appointmentType = $("#cmbStatus").val();
            obj.notes = null;//$("#taArea").val();


            var selSUItem = onGetSUBillingDetails($("#cmbBilling").val());
            obj.shiftValue = selSUItem.shiftValue;
            obj.billingRateType = selSUItem.billingRateTypeCode;
            obj.patientBillingRate = selSUItem.rate;
            obj.billingRateTypeId = selSUItem.billingRateTypeId;
            obj.swiftBillingId = selSUItem.swiftBillingId;
            obj.payoutHourlyRate = $("#txtPayout").val();
            obj.billToName = $("#cmbBilling option:selected").text();
            obj.billToId = selSUItem.billToId;
            obj.appointmentReason = $("#cmbReason").val();

            obj.name = $("#txtSU").val();
            var day = new Date(GetDateTimeEditDay(obj.dateOfAppointment)).getDay();
            var dow = getWeekDayName(day);

            obj.dateTimeOfAppointment1 = dow + " " + GetDateTimeEdit(obj.dateOfAppointment);

            obj.appointmentReasonDesc = $("#cmbReason option:selected").text();
            obj.appointmentTypeDesc = $("#cmbStatus option:selected").text();
            obj.staffName = $("#txtStaff").val();

            var dtEnd = obj.dateOfAppointment + (Number(obj.duration) * 60000);


            var dateED = new Date(GetDateTimeEditDay(dtEnd));
            var dayED = dateED.getDay();
            var dowED = getWeekDayName(dayED);

            obj.dateTimeOfAppointmentED = dowED + " " + GetDateTimeEdit(dtEnd);


            var arr = [];
            arr.push(obj);


            if(typeWeek == "2"){
                parentRef.appselDayList.push(obj);
                // parentRef.appselDayList.sort(function(a,b){
                //     return new Date(a.dateTimeOfAppointment) - new Date(b.dateTimeOfAppointment);
                // });
                buildDeviceListGrid(parentRef.appselDayList);
            }else if(typeWeek == "1"){
                parentRef.appselList.push(obj);
                parentRef.appselList.sort(function(a,b){
                    return new Date(a.dateTimeOfAppointment) - new Date(b.dateTimeOfAppointment);
                });
                buildDeviceListGrid(parentRef.appselList);
            }else if(typeWeek == "3"){
                // parentRef.appselEmptyList = parentRef.appselList;
                if(parentRef.appselEmptyList && parentRef.appselEmptyList.length > 0) {
                    parentRef.appselEmptyList.push(obj);
                }
                else{
                    parentRef.appselEmptyList = arr
                }

                // parentRef.appselEmptyList.sort(function(a,b){
                //     return new Date(a.dateTimeOfAppointment) - new Date(b.dateTimeOfAppointment);
                // });
                buildDeviceListGrid(parentRef.appselEmptyList);
            }

            var msg = "Added successfully to copy appointments list";
            displaySessionErrorPopUp("Info", msg, function (res) {

                // onClickReset();
                // onClickCancel();
                $(".filter-heading").html("View Copy Appointments");
                $("#viewDivBlock").show();
                $("#addPopup").hide();
            });
        }
    }
}

function getPatientData(dataObj) {
    patientBillArray = [];

    var tempCompType = [];
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.patientBilling) {
            if ($.isArray(dataObj.response.patientBilling)) {
                tempCompType = dataObj.response.patientBilling;
            } else {
                tempCompType.push(dataObj.response.patientBilling);
            }
        }
    }
    for (var q = 0; q < tempCompType.length; q++) {
        var qItem = tempCompType[q];
        if (qItem) {
            qItem.value = qItem.billToname;
            qItem.text = qItem.billToname;
            patientBillArray.push(qItem);
            $("#cmbBilling").append('<option value="' + qItem.value + '">' + qItem.value + '</option>');
        }
    }


    bindDropDownLists();
}

function onGetSUBillingDetails(name){
    for (var i=0;patientBillArray.length >= 0;i++){
        if(patientBillArray[i].billToname.toLowerCase() == name.toLowerCase()){
            return patientBillArray[i];
        }
    }

}


function onGetAllSUBillingDetails(name,data){
    for (var i=0;patientBillArray.length >= 0;i++){
        if(data[i].billToname.toLowerCase() == name.toLowerCase()){
            return data[i];
        }
    }

}

function showDays(firstDate,secondDate){
    var startDay = new Date(firstDate);
    var endDay = new Date(secondDate);
    var millisecondsPerDay = 1000 * 60 * 60 * 24;

    var millisBetween = startDay.getTime() - endDay.getTime();
    var days = millisBetween / millisecondsPerDay;
    return Math.floor(days);
}

function onClickStaff(){
    var popW = 800;
    var popH = 450;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Staff";

    devModelWindowWrapper.openPageWindow("../../html/masters/staffList.html", profileLbl, popW, popH, true, onCloseStaffAction);
}

function onCloseStaffAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        selectProviderId = returnData.selItem.PID;
        $("#txtStaff").val(returnData.selItem.lastName + ' '+ returnData.selItem.firstName+' '+ (returnData.selItem.middleName != null ? returnData.selItem.middleName : " "));
    }
}

var gridId;
function onCreateAppointments(){
    var dataList = []
    var selectedItems = angularUIgridWrapper.getAllRows();
    if(selectedItems.length > 0) {
        for (var i = 0; i < selectedItems.length; i++) {

            var obj = {};
            obj.createdBy = Number(sessionStorage.userId);
            obj.isActive = 1;
            obj.isDeleted = 0;
            gridId = selectedItems[i].entity.id;
            obj.dateOfAppointment = selectedItems[i].entity.dateTimeOfAppointment;
            obj.duration = selectedItems[i].entity.duration;
            obj.providerId = selectedItems[i].entity.providerId;
            obj.facilityId = selectedItems[i].entity.facilityId;
            obj.patientId = selectedItems[i].entity.patientId;
            if (selectedItems[i].entity.composition) {
                obj.appointmentType = selectedItems[i].entity.composition.appointmentType.value;
                obj.appointmentReason = selectedItems[i].entity.composition.appointmentReason.value;

            }
            else {
                obj.appointmentType = selectedItems[i].entity.appointmentType;
                obj.appointmentReason = selectedItems[i].entity.appointmentReason;
            }
            obj.notes = selectedItems[i].entity.notes;

            var response = _.where(patientAllBillArray, {patientId: selectedItems[i].entity.patientId})


            var selSUItem = onGetAllSUBillingDetails(selectedItems[i].entity.billToName, response);
            obj.shiftValue = selSUItem.shiftValue;
            obj.billingRateType = selSUItem.billingRateTypeCode;
            obj.patientBillingRate = selSUItem.rate;
            obj.billingRateTypeId = selSUItem.billingRateTypeId;
            obj.swiftBillingId = selSUItem.swiftBillingId;
            obj.payoutHourlyRate = selectedItems[i].entity.payoutHourlyRate;
            obj.billToName = selectedItems[i].entity.billToName;
            obj.billToId = selSUItem.billToId;

            dataList.push(obj);
        }
        // console.log(dataList);
        var dataUrl = ipAddress + "/appointment/create";
        createAjaxObject(dataUrl, dataList, "POST", onCreate, onError);
    }
    else{
        customAlert.info("info","No record(s) in the list");
    }

}


function onSUBilliing(){
    getAjaxObject(ipAddress + "/carehome/patient-billing/?is-active=1&fields=*,shift.*,travelMode.*,billingType.*,billingPeriod.*,billTo.name,billingRateType.code", "GET", getAllPatientData, onError);
}

var patientAllBillArray;
function getAllPatientData(dataObj) {
    patientAllBillArray = [];
    var tempCompType = [];
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.patientBilling) {
            if ($.isArray(dataObj.response.patientBilling)) {
                tempCompType = dataObj.response.patientBilling;
            } else {
                tempCompType.push(dataObj.response.patientBilling);
            }
        }
    }
    for (var q = 0; q < tempCompType.length; q++) {
        var qItem = tempCompType[q];
        if (qItem) {
            qItem.value = qItem.billToname;
            qItem.text = qItem.billToname;
            patientAllBillArray.push(qItem);
        }
    }
}

function validation() {
    var IsFlag = true;
    var txtStartDate = $("#txtStartDate").data("kendoDateTimePicker");
    var txtEndDate = $("#txtEndDate").data("kendoDateTimePicker");

    if(!(txtStartDate.value() != null)){
        IsFlag = false;
    }
    else if(!(txtEndDate.value() != null)){
        IsFlag = false;
    }
    else if(!($("#cmbReason").val() != "")){
        IsFlag = false;
    }
    else if(!($("#cmbStatus").val() != "")){
        IsFlag = false;
    }
    else if(!($("#cmbBilling").val() != "")){
        IsFlag = false;
    }
    else if(!($("#txtPayout").val() != "")){
        IsFlag = false;
    }
    return IsFlag;
}



function startChange(){
    var txtStartTime = $("#txtStartDate").data("kendoDateTimePicker");
    var txtEndTime = $("#txtEndDate").data("kendoDateTimePicker");
    var startTime = txtStartTime.value();



    if (startTime) {
        var sTime = new Date(startTime);
        var tTime = sTime.getTime();
        tTime = tTime+(15*60*1000);
        var strTime = new Date(tTime);
        txtEndTime.min(strTime);
        txtEndTime.value(strTime);
        // txtEndTime.max(today1);
        //end.value(startTime);
    }

}

function showCheckBoxTemplate(){
    var node = '<div style="text-align:center;padding-top: 0;"><input type="checkbox" class="check1" id="chkbox" ng-model="row.entity.SEL" style="width:15px;height:15px;margin:0px;"   onclick="onSelect(event);"></input></div>';
    return node;
}
function onSelect(evt){
    //console.log(evt);
}
function toggleSelectAll(e) {
    var checked = e.currentTarget.checked;
    var rows = angularUIgridWrapper.getScope().gridApi.core.getVisibleRows();
    for (var i = 0; i < rows.length; i++) {
        rows[i].entity["SEL"] = checked;
    }
}



function onClickDeleteRoster(){
    var selectedItems = angularUIgridWrapper.getSelectedRows();
    if(selectedItems && selectedItems.length>0){
        customAlert.confirm("Confirm", "Are you sure you want delete?",function(response){
            if(response.button == "Yes"){
                var selGridData = angularUIgridWrapper.getAllRows();
                var selList = [];
                for (var i = 0; i < selGridData.length; i++) {
                    var dataRow = selGridData[i].entity;
                    if(dataRow.SEL){
                        selList.push(dataRow);
                    }
                }
                angularUIgridWrapper.deleteItems(selList);
            }
        });
    }
}
function popupClose(st){
    var onCloseData = new Object();
    onCloseData.status = st;
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(onCloseData);
}


function GetFormattedDate(date) {
    var todayTime = new Date(date);
    var month = (todayTime .getMonth() + 1);
    var day =(todayTime .getDate());
    var year = (todayTime .getFullYear());
    return month + "/" + day + "/" + year;
}

function bindData(){
    if(typeWeek == "1"){
        buildDeviceListGrid(parentRef.appselList);
    }else if(typeWeek == "2"){
        buildDeviceListGrid(parentRef.appselDayList);
    }else{
        buildDeviceListGrid(parentRef.appselEmptyList);
    }
}


function getFacilityList(dataObj) {
    facilityListArray = [];
    if (dataObj) {
        if ($.isArray(dataObj.response.facility)) {
            facilityListArray = dataObj.response.facility;
        } else {
            facilityListArray.push(dataObj.response.facility);
        }
    }

    getPatientBills();
}
var parentRef = null;
var operation = "";
var selItem = null;
var ADD = "add";
var UPDATE = "update";
var VIEW = "view";
var DELETE = "delete";
var statusArr = [{Key:'Active',Value:'Active'},{Key:'InActive',Value:'InActive'}];

var fileTypeArray = [{Key:'',Value:''},{Key:'PDF',Value:'PDF'},{Key:'MP3',Value:'MP3'},{Key:'MP4',Value:'MP4'}];

var dietType = "1";
$(document).ready(function(){
    parentRef = parent.frames['iframe'].window;
});

function adjustHeight(){
    var defHeight = 45;
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 40;
    }
}

$(window).load(function(){
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
    init();
});
//var updateItem = null;
function init(){
    operation = parentRef.operation;
    selItem = parentRef.selItem;
    $("#cmbStatus").kendoComboBox();
    $("#cmbFileList").kendoComboBox();
    setDataForSelection(statusArr, "cmbStatus", onStatusChange, ["Value", "Key"], 0, "");
    setDataForSelection(fileTypeArray, "cmbFT", onFileTypeChange, ["Value", "Key"], 0, "");
    onSelectButton();
    fileTypeReset(1);
    if(operation == UPDATE && selItem){
        console.log(selItem);
        $("#txtFN").val(selItem.fileName);
        var fileType = selItem.fileType;
        if(fileType){
            fileType = fileType.toUpperCase();
            var idx = 0;
            if(fileType == "MP3"){
                idx = 1;
            }else if(fileType == "MP4"){
                idx = 2;
            }
            setComboBoxIndex("cmbFT",idx);
        }
        $("#txtYouTube").val(selItem.youtubeLink);
        $("#txtIOS").val(selItem.iosPath);
        $("#txtAndroid").val(selItem.androidPath);

        if(selItem.recordType == "1"){
            onClickDiet();
        }else if(selItem.recordType == "2"){
            onClickExcersize();
        }else if(selItem.recordType == "5"){
            onClickillness();
        }

        var cmbStatus = $("#cmbStatus").data("kendoComboBox");
        if(cmbStatus){
            if(selItem.isActive == 1){
                cmbStatus.select(0);
            }else{
                cmbStatus.select(1);
            }
        }
    }else{
        var cmbStatus = $("#cmbStatus").data("kendoComboBox");
        if(cmbStatus){
            cmbStatus.enable(false);
        }
    }
    buttonEvents();
}

function onStatusChange(){
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if(cmbStatus && cmbStatus.selectedIndex<0){
        cmbStatus.select(0);
    }
}
function onFileTypeChange(){
    var cmbFT = $("#cmbFT").data("kendoComboBox");
    if(cmbFT && cmbFT.selectedIndex<0){
        cmbFT.select(0);
    }
}
function buttonEvents(){
    $("#btnCancel").off("click",onClickCancel);
    $("#btnCancel").on("click",onClickCancel);

    $("#btnSave").off("click",onClickSave);
    $("#btnSave").on("click",onClickSave);

    $("#btnSearch").off("click",onClickSearch);
    $("#btnSearch").on("click",onClickSearch);

    $("#btnReset").off("click",onClickReset);
    $("#btnReset").on("click",onClickReset);

    $("#btnBrowse").off("click", onBrowseClick);
    $("#btnBrowse").on("click", onBrowseClick);

    $("#fileElem").on("change", onSelectionFiles);

    $("#btnThumbBrowse").off("click", onBrowseClick1);
    $("#btnThumbBrowse").on("click", onBrowseClick1);

    $("#fileElem1").on("change", onSelectionFiles1);

    $("#btnDiet").off("click");
    $("#btnDiet").on("click",onClickDiet);

    $("#btnExcersize").off("click");
    $("#btnExcersize").on("click",onClickExcersize);

    $("#btnillness").off("click");
    $("#btnillness").on("click",onClickillness);

    $('input:radio[name="File"]').change(function(){
        console.log($("input:radio[name='File']:checked").val());
        var dataVal = $(this).val();
        fileTypeReset(dataVal);
    });
}
function textFieldReadOnly(txtId,flag){
    $('#'+txtId).val("");
    $('#'+txtId).prop('readonly', flag);
}
function fileTypeReset(dataVal){
    var cmbFileList = $("#cmbFileList").data("kendoComboBox");
    if(dataVal == "1"){
        $("#btnBrowse").show();
        if(cmbFileList){
            cmbFileList.enable(false);
        }
        //textFieldReadOnly("txtYouTube",true);
    }else if(dataVal == "2"){
        $("#txtFU").val("");
        $("#btnBrowse").hide();
        if(cmbFileList){
            cmbFileList.select(0);
            cmbFileList.enable(true);
        }
        //textFieldReadOnly("txtYouTube",true);
    }else{
        $("#txtFU").val("");
        $("#btnBrowse").hide();
        if(cmbFileList){
            cmbFileList.enable(false);
        }
        //textFieldReadOnly("txtYouTube",false);
    }
}
function onClickDiet(){
    dietType = "1";
    $("#btnDiet").addClass("selectButtonBarClass");
    $("#btnExcersize").removeClass("selectButtonBarClass");
    $("#btnillness").removeClass("selectButtonBarClass");
}
function onClickExcersize(){
    dietType = "2";
    $("#btnDiet").removeClass("selectButtonBarClass");
    $("#btnExcersize").addClass("selectButtonBarClass");
    $("#btnillness").removeClass("selectButtonBarClass");
}
function onClickillness(){
    dietType = "5";
    $("#btnDiet").removeClass("selectButtonBarClass");
    $("#btnExcersize").removeClass("selectButtonBarClass");
    $("#btnillness").addClass("selectButtonBarClass");
}

function onBrowseClick(e){
    console.log(e);
    if(e.currentTarget && e.currentTarget.id){
        var btnId = e.currentTarget.id;
        var fileTagId = ("fileElem"+btnId);
        $("#fileElem").click();
    }
}
function onSelectionFiles(event){
    console.log(event);
    files = event.target.files;
    fileName = "";
    $('#txtFU').val("");
    if(files){
        if(files.length > 0){
            fileName = files[0].name;
            $('#txtFU').val(fileName);
        }
    }
}
function onBrowseClick1(e){
    console.log(e);
    if(e.currentTarget && e.currentTarget.id){
        var btnId = e.currentTarget.id;
        var fileTagId = ("fileElem1"+btnId);
        $("#fileElem1").click();
    }
}
function onSelectionFiles1(event){
    console.log(event);
    files = event.target.files;
    fileName = "";
    $('#txtThumb').val("");
    if(files){
        if(files.length > 0){
            fileName = files[0].name;
            $('#txtThumb').val(fileName);
        }
    }
}
function onClickReset(){
    operation = ADD;
    selItem = null;
    dietType = "1";
    onClickDiet();
    $("#txtFN").val("");
    $("#txtFU").val("");
    $("#txtThumb").val("");
    $("#txtYouTube").val("");
    $("#txtIOS").val("");
    $("#txtAndroid").val("");
    setComboBoxIndex("cmbStatus",0);
    setComboBoxIndex("cmbFT",0);
    setComboBoxIndex("cmbFileList",0);
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if(cmbStatus){
        cmbStatus.select(0);
        cmbStatus.enable(false);
    }
}

function validation(){
    var flag = true;
    var strFN = $("#txtFN").val();
    strFN = $.trim(strFN);
    if(strFN == ""){
        customAlert.error("Error","Please enter file name");
        return false;
    }
    if(operation != UPDATE){
        var isFileUpload = $("input:radio[name='File']:checked").val();
        var strYouTube = $("#txtYouTube").val();
        if(isFileUpload == "1"){
            var fileName = $("#txtFU").val();
            if(fileName == "" && strYouTube == ""){
                customAlert.error("Error","Please upload a file (or) enter you tube URL");
                return false;
            }
        }else if(isFileUpload == "2"){
            var cmbFileList = $("#cmbFileList").data("kendoComboBox");
            var strFileListName = cmbFileList.text();
            if(strFileListName == "" && strYouTube == ""){
                customAlert.error("Error","Please Select file from list (or) enter you tube URL");
                return false;
            }
        }
    }

    return flag;
}

function onClickSave(){
    if(validation()){
        var strFN = $("#txtFN").val();
        strFN = $.trim(strFN);
        if(operation == UPDATE && selItem && selItem.fileName == strFN){
            onSaveFileResource();
        }else{
            var dataUrl = ipAddress+"/file-resource/is-available/"+strFN;
            getAjaxObject(dataUrl,"GET",onDietAvailable,onError);
        }
    }
}
function onSaveFileResource(){
    var strFN = $("#txtFN").val();
    strFN = $.trim(strFN);
    var cmbFT = $("#cmbFT").data("kendoComboBox");
    var strFT = cmbFT.text();
    var isFileUpload = $("input:radio[name='File']:checked").val();

    var tagFileId = ("fileElem");
    var file = document.getElementById(tagFileId);
    var fileName = $("#txtFU").val();
    if(fileName != ""){
        if(file && file.files[0] && file.files[0].name){
            fileName = file.files[0].name;
        }
    }
    var thumbTagFileId = ("fileElem1");
    var thumbFile = document.getElementById(thumbTagFileId);
    var tFileName = $("#txtThumb").val();
    if(tFileName != ""){
        if(thumbFile && thumbFile.files[0] && thumbFile.files[0].name){
            tFileName = thumbFile.files[0].name;
        }
    }

    var cmbFileList = $("#cmbFileList").data("kendoComboBox");
    var strFileListName = cmbFileList.text();

    var strYouTube = $("#txtYouTube").val();
    var strIOS = $("#txtIOS").val();
    var strAndroid = $("#txtAndroid").val();

    isActive = 0;
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if(cmbStatus){
        if(cmbStatus.selectedIndex == 0){
            isActive = 1;
        }
    }

    var dataObj = {};
    dataObj.isActive = isActive;
    dataObj.fileName = strFN;
    dataObj.fileType = strFT;
    dataObj.filePath = $("#txtFU").val();
    dataObj.youtubeLink = $("#txtYouTube").val();
    dataObj.iosPath = $("#txtIOS").val();
    dataObj.androidPath = $("#txtAndroid").val();
    dataObj.createdBy = sessionStorage.userId;
    dataObj.isDeleted = "0";
    if(operation == UPDATE && selItem){
        dataObj.id = selItem.idk;
    }
    if(tFileName != ""){
        dataObj.thumbnailType = "jpg";
    }

    dataObj.recordType = dietType;

    var reqUrl = "";
    var fileOperation = "create";
    var uploadFileOperation = "upload-file";
    var thumbNailOperation = "upload-files";

    var strDataObj = JSON.stringify(dataObj);
    var formData = new FormData();
    if(operation != UPDATE){
        if(fileName == ""){
            formData.append("fileResource",strDataObj);
        }else if(fileName != "" && tFileName == ""){
            formData.append("fileResource",strDataObj);
            formData.append("file",file.files[0]);
        }else if(tFileName != ""  && tFileName != ""){
            formData.append("fileResource",strDataObj);
            formData.append("file",file.files[0]);
            formData.append("thumbnail",thumbFile.files[0]);
        }else if(tFileName != ""){
            formData.append("fileResource",strDataObj);
            formData.append("thumbnail",thumbFile.files[0]);
        }
    }else{
        if(fileName == ""  && tFileName == "" ){
            formData.append("fileResource",strDataObj);
        }else if(fileName != "" && tFileName == ""){
            formData.append("fileResource",strDataObj);
            formData.append("file",file.files[0]);
        }else if(tFileName != ""  && tFileName != ""){
            formData.append("fileResource",strDataObj);
            formData.append("file",file.files[0]);
            formData.append("thumbnail",thumbFile.files[0]);
        }else if(tFileName != ""){
            formData.append("fileResource",strDataObj);
            formData.append("thumbnail",thumbFile.files[0]);
        }
    }
    if(operation == UPDATE){
        fileOperation = "update";
        uploadFileOperation = "upload-file";
        thumbNailOperation = "upload-files";
    }
    if(fileName == "" && tFileName == ""){
        reqUrl = ipAddress+"/file-resource/"+fileOperation;
    }else if(fileName != "" && tFileName == ""){
        reqUrl = ipAddress+"/file-resource/"+uploadFileOperation;
    }else if(tFileName != ""  && tFileName != "" ){
        reqUrl = ipAddress+"/file-resource/"+thumbNailOperation;
    }

    reqUrl = reqUrl+"?access_token="+sessionStorage.access_token;

    Loader.showLoader();
    $.ajax({
        url: reqUrl,
        type: 'POST',
        data: formData,
        processData: false,
        contentType:false,// "multipart/form-data",
        // contentType: "application/json",
        success: function(data, textStatus, jqXHR){
            if(typeof data.error === 'undefined'){
                Loader.hideLoader();
                submitForm(data);
            }else{
                Loader.hideLoader();
            }
        },
        error: function(jqXHR, textStatus, errorThrown){
            Loader.hideLoader();
        }
    });


}
function onDietAvailable(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        onSaveFileResource();
    }else{
        customAlert.error("Error","File is not available. Please use different file name");
    }
}
function submitForm(dataObj){
    if(dataObj && dataObj.response){
        if(dataObj.response.status){
            if(dataObj.response.status.code == "1"){
                if(dietType == "1"){
                    customAlert.error("Info","Diet file uploaded successfully");
                }else if(dietType == "2"){
                    customAlert.error("Info","Excersize file uploaded successfully");
                }else if(dietType == "3"){
                    customAlert.error("Info","Illness file uploaded successfully");
                }

            }else{
                customAlert.error("Error",dataObj.response.status.message);
            }
        }else{
            customAlert.error("Error","Error");
        }
    }

}
/*if(operation == ADD){
    var dataUrl = ipAddress+"/file-resource/create";
    createAjaxObject(dataUrl,dataObj,"POST",onCreate,onError);
}else if(selItem){
    dataObj.id = selItem.idk;
    var dataUrl = ipAddress+"/file-resource/update";
    createAjaxObject(dataUrl,dataObj,"POST",onCreate,onError);
}*/

function onCreate(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.status){
        if(dataObj.status.code == "1"){
            if(operation == ADD){
                customAlert.info("info", "File Created Successfully");
            }else{
                customAlert.info("info", "File Updated Successfully");
            }

        }else{
            customAlert.error("error", dataObj.status.message);
        }
    }
    /*var obj = {};
    obj.status = "success";
    obj.operation = operation;
    popupClose(obj);*/
}

function onError(errObj){
    console.log(errObj);
    customAlert.error("Error","Unable to create Country");
}
function onClickCancel(){
    var obj = {};
    obj.status = "false";
    popupClose(obj);
}
function onClickSearch(){
    var obj = {};
    obj.status = "search";
    obj.dietType = dietType;
    popupClose(obj);
}
function popupClose(obj){
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}

function onSelectButton() {
    var type = $("#lblTitleName",parent.document)[0].innerHTML.toLowerCase();
    if (type == "diet" ) {
        onClickDiet();
    }
    else if(type == "exercise"){
        onClickExcersize();
    }
    else if( type == "illness"){
        onClickillness();
    }
}

var angularUIgridWrapper;
var angularUIgridWrapper1;
var angularUIgridWrapper3;
var parentRef = null;
var recordType = "1";
var dataArray = [];
var selMasterId = null;
var gridColumns = [];
var selObj = null;
var entityMaster = null,groupMaster = null,userGroupMaster = null,userListMaster = null;
var selGroupId = null,selGroupName=null;
var treeUserArray = [];
var IsBindTreeView = false;
var treeview, handleTextBox;
var selUserId,tmpUserGroupMaster;

var treeDataSource = [{ text: "Root", menu:"true",type:"ROOT",expanded: true,spriteCssClass: "rootfolder",items:[]}];
var treeDataSourcepopup = [{ text: "Add User",type:"popup",expanded: true,spriteCssClass: "rootfolder",items:[],idk:0},{ text: "View Groups",type:"popup",expanded: true,spriteCssClass: "rootfolder",items:[],idk:1}];
$(document).ready(function(){
    $("#pnlPatient",parent.document).css("display","none");
    sessionStorage.setItem("IsSearchPanel", "1");
    getManagers();
    //themeAPIChange();
    if(parent.frames['iframe'])
        parentRef = parent.frames['iframe'].window;
    //recordType = parentRef.dietType;

    var dataOptions1 = {
        pagination: false,
        changeCallBack: onChange1
    }
    angularUIgridWrapper1 = new AngularUIGridWrapper("dgridSecurityMasters", dataOptions1);
    angularUIgridWrapper1.init();


    buildDeviceListGrid1([]);

    handleTextBox = function(callback) {
        return function(e) {
            if (e.type != "keypress" || kendo.keys.ENTER == e.keyCode) {
                callback(e);
            }
        };
    };
});

$(window).load(function(){
    loading = false;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){


    bindSecurityMasters();
    initControls();

    buttonEvents();
    adjustHeight();
}


function bindSecurityMasters(){
    var securityMasters=[];
    var userId = sessionStorage.userId;
    if(userId == 298) {
        // if(userId > 0) {
        securityMasters = [
            {
                idk: 1,
                name: "Type"
            },
            {
                idk: 2,
                name: "Create Group"
            },
            {
                idk: 3,
                name: "Group Permissions"
            },
            //  {
            //      idk: 4,
            //      name: "UserGroups"
            //  },
            //  {
            //      idk: 5,
            //      name: "UserGroupEntities"
            //  },
            {
                idk: 6,
                name: "Profiles"
            }
        ];
    }
    else{
        // $("#divGrid").hide();
        securityMasters = [
            {
                idk: 2,
                name: "Create Group"
            },
            {
                idk: 3,
                name: "Group Permissions"
            },
            {
                idk: 6,
                name: "Profiles"
            }
        ];
    }
    //  else{
    //      securityMasters = [
    //          {
    //              idk: 4,
    //              name: "UserGroups"
    //          },
    //          {
    //              idk: 5,
    //              name: "UserGroupEntities"
    //          }
    //      ];
    //  }

    buildDeviceListGrid1(securityMasters);
}

function handleGetTemplateList(dataObj){
    var vacation = [];
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            if(dataObj.response.templates){
                if($.isArray(dataObj.response.templates)){
                    vacation = dataObj.response.templates;
                }else{
                    vacation.push(dataObj.response.templates);
                }
            }
        }
    }
    for(var j=0;j<vacation.length;j++){
        vacation[j].idk = vacation[j].id;
    }
    buildDeviceListGrid1(vacation);
}

function init(){

    buildDeviceListGrid([]);
    //buildDeviceListGrid1([]);
    var selectedItems = angularUIgridWrapper1.getSelectedRows();

    buildDeviceListGrid([]);
    if(selectedItems && selectedItems.length>0 && selectedItems[0].idk){
        var ipaddress = ipAddress+"/homecare/mini-templates/?is-active=1&is-deleted=0&templateId="+selectedItems[0].idk;
        getAjaxObject(ipaddress,"GET",handleGetVacationList,onError);
    }
}

function handleGetVacationList(dataObj){
    var vacation = [];
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            if(dataObj.response.miniTemplates){
                if($.isArray(dataObj.response.miniTemplates)){
                    vacation = dataObj.response.miniTemplates;
                }else{
                    vacation.push(dataObj.response.miniTemplates);
                }
            }
        }
    }
    for(var j=0;j<vacation.length;j++){
        vacation[j].idk = vacation[j].id;
    }
    buildDeviceListGrid(vacation);
}
function onError(errorObj){
    console.log(errorObj);
}

function buttonEvents(){
    $("#btnSave").off("click",onClickSave);
    $("#btnSave").on("click",onClickSave);

    $("#btnView").off("click",onClickView);
    $("#btnView").on("click",onClickView);

    $("#btnCancelDet").off("click",onClickCancel);
    $("#btnCancelDet").on("click",onClickCancel);

    $("#btnBackToList").off("click",onClickBackToList);
    $("#btnBackToList").on("click",onClickBackToList);

    $("#btnEdit").off("click",onClickEdit);
    $("#btnEdit").on("click",onClickEdit);

    $("#btnDelete").off("click",onClickDelete);
    $("#btnDelete").on("click",onClickDelete);

    $('#btnAdd').off('click', onClickAdd);
    $('#btnAdd').on('click', onClickAdd);

    $(".popupClose").off("click",onClickBackToList);
    $(".popupClose").on("click",onClickBackToList);

    $("#btnSearch1").off("click",onClickBlockDays);
    $("#btnSearch1").on("click",onClickBlockDays);

    $("#cmbUserType").change(function() {
        var selectValue = $("#cmbUserType").val();
        onGetUserDetails(selectValue);
    });

    $("#btnSaveUserGroup").off("click",onClickSaveUserGroup);
    $("#btnSaveUserGroup").on("click",onClickSaveUserGroup);


    $("#btnGo").off("click",onClickGo);
    $("#btnGo").on("click",onClickGo);

    $("#cmbTenantUserType").off("change",onChangeTenantUserType);
    $("#cmbTenantUserType").on("change",onChangeTenantUserType);

    $("#ulGroupMaster").off("click","li",onClickGroup);
    $("#ulGroupMaster").on("click","li",onClickGroup);

    $("#ulGroupUser").off("click","li:first-child span",onClickAddUser);
    $("#ulGroupUser").on("click","li:first-child span",onClickAddUser);

    /* $("input[name=userGroupEntities]").on( "change", function() {
         radioValue = $(this).val();

         $("#divUserblockGroup1").hide();
         $("#divScreenTreeList").hide();

         if(radioValue == "1"){
             $("#divScreenTreeList").show();
         }
         else {
             $("#divUserblockGroup1").show();
             $("#divScreenTreeList").hide();

         }

     } )
 */


}

function onClickGroup(){
    debugger;

    var groupId = Number($(this).attr("idk"));
    selGroupId = groupId;
    selGroupName = $(this).text();


    $("#ulGroupMaster li").css('background','white');
    $(this).css('background','#b2eaf3');
    bindGroupUsers(groupId);
}

function bindGroupUsers(groupId) {
    $("#ulGroupUser").empty();
    if (userGroupMaster !== null && userGroupMaster.length > 0) {
        var tmpUsers = $.grep(userGroupMaster, function (e) {
            return e.groupId === groupId;
        });

        if (tmpUsers !== null && tmpUsers.length > 0) {
            for (var i = 0; i < tmpUsers.length; i++) {
                if (userListMaster !== null && userListMaster.length > 0) {
                    var tmpUserDetailsArr = _.where(userListMaster, { id: tmpUsers[i].userId });

                    if (tmpUserDetailsArr !== null && tmpUserDetailsArr.length > 0) {
                        var username = tmpUserDetailsArr[0].lastName + " " + tmpUserDetailsArr[0].firstName;
                        tmpUsers[i].username = username;
                        //$("#ulGroupUser").append('<li class="list-group-item draggable-item" idk="' + tmpUsers[i].userId + '">' + username + '</li>');
                    }
                }
            }

            debugger;

            tmpUsers.sort(sortByUsername);

            if (tmpUsers !== null && tmpUsers.length > 0) {
                $.each(tmpUsers,function(i,e){
                    $("#ulGroupUser").append('<li class="list-group-item draggable-item" style="text-align: center" idk="' + e.userId + '"><span style="float:left;cursor:pointer;" id="spMinus"><img src="../../img/Default/minus.svg" title="Minus" style="width:10px" onclick="onDeleteUser('+groupId+','+ e.userId+')" /></span><span>' + e.username + '</span></li>');
                })
            }
        }


    }

    if ($("#ulGroupUser li").length > 0) {
        $("#ulGroupUser li:first-child").append('<span style="float:right;cursor:pointer;"><img src="../../img/Default/add-icon.png" title="Add" style="width:10px"/></span>');
    }
    else{
        $("#ulGroupUser").append('<li type="none"><span style="float:right;cursor:pointer;"><img src="../../img/Default/add-icon.png" title="Add" style="width:10px"/></span></li>');
    }

    $("#ulGroupUser").show();
    initDragAndDrop();


}



function initDragAndDrop() {
    $("#ulGroupUser li.draggable-item").draggable({
        helper: 'clone'
    });
    $("#ulGroupMaster li").droppable({
        accept: '.draggable-item',
        drop: function (e, ui) {

            var sourceUserId = parseInt($(ui.draggable).attr("idk"));
            var destinationGroupId = Number($(this).attr("idk"));

            if (sourceUserId > 0 && destinationGroupId > 0) {

                if (userGroupMaster !== null && userGroupMaster.length > 0) {
                    var existingGroupUser = $.grep(userGroupMaster, function (e) {
                        return e.userId === sourceUserId && e.isActive === 1 && e.isDeleted === 0;
                    });

                    if (existingGroupUser !== null && existingGroupUser.length > 0) {
                        sourceGroupId = existingGroupUser[0].groupId;
                        sourceMasterId = existingGroupUser[0].id;
                    }


                    if (sourceGroupId !== destinationGroupId) {
                        var reqData = {};
                        reqData.groupId = Number(sourceGroupId);
                        reqData.userId = Number(sourceUserId);
                        reqData.modifiedBy = Number(sessionStorage.getItem("userId"));
                        reqData.id = sourceMasterId;
                        reqData.isActive = 0;
                        reqData.isDeleted = 1;

                        createAjaxObject(ipAddress + "/homecare/security/user-groups/", reqData, "PUT", onDeleteUser2, onError);

                        var reqData2 = {};
                        reqData2.groupId = Number(destinationGroupId);
                        reqData2.userId = Number(sourceUserId);
                        reqData2.createdBy = Number(sessionStorage.getItem("userId"));

                        reqData2.isActive = 1;
                        reqData2.isDeleted = 0;

                        createAjaxObject(ipAddress + "/homecare/security/user-groups/", reqData2, "POST", onSaveUserGroup2, onError);
                    }

                    // setTimeout(function () {

                    //     getUserGroups();

                    //     $("#divEntityPermission").hide();
                    //     $("#divAddUserToGroup").hide();
                    //     $("#linkGroupUserPopup").hide();
                    //     $("#tblEntityPermission tbody tr").remove();

                    //     $("#cmbUserList").empty();
                    // }, 1000);
                }
            }
        }
    });
}


var userIdsMaster = [];
function onClickSaveUserGroup(){

    debugger;

    var userIds = $('#cmbUserList').multipleSelect('getSelects');


    if (userIds !== null && userIds.length > 0) {

        userIdsMaster = userIds;
        var obj = [];

        for (var i = 0; i < userIds.length; i++) {
            var reqData = {};
            reqData.groupId = Number(selGroupId);
            reqData.userId = userIds[i];
            reqData.createdBy = Number(sessionStorage.getItem("userId"));
            reqData.isActive = 1;
            reqData.isDeleted = 0;
            obj.push(reqData);
        }

        createAjaxObject(ipAddress + "/homecare/security/user-groups/batch/", obj, "POST", onSaveUserGroup, onError);

    }





}


function onClickGo(){
    var userVal = $("#txtUser").val();
    if(userVal != "0") {
        var groupIds = $('#cmbGroupList').multipleSelect('getSelects');
        selUserId = Number($('#txtUser').val());
        if (groupIds !== null && groupIds.length > 0) {
            if (userGroupMaster !== null && userGroupMaster.length > 0) {
                for (var i = 0; i < groupIds.length; i++) {
                    tmpUserGroupMaster = $.grep(userGroupMaster, function (e) {
                        return ((e.groupId === Number(groupIds[i])) && (e.userId === selUserId) && e.isActive === 1 && e.isDeleted === 0);
                    });

                    if (tmpUserGroupMaster !== null && tmpUserGroupMaster.length > 0) {
                        break;
                    }
                }

                if (tmpUserGroupMaster !== null && tmpUserGroupMaster.length > 0) {
                    setTimeout(function () {
                        $("#ulGroupMaster").empty();
                        IsBindTreeView = true;

                        //getUserGroups(1);
                        bindGroupEntityTreeView2();
                        // initControls();
                        // onClickBackToList();
                        //onClickSecurityMaster(selMasterId);

                        $("#divEntityPermission").hide();
                        $("#divAddUserToGroup").hide();
                        $("#linkGroupUserPopup").hide();
                        $("#tblEntityPermission tbody tr").remove();


                        $("#cmbUserList").empty();
                    }, 1000);
                } else {
                    customAlert.error("Error", "No matches found.");
                    //$('#cmbGroupList').empty();
                }
            }
        }
    }
    else{
        customAlert.error("Error","Please select user")
    }
}

function onSaveUserGroup(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "";
            msg = "User added to role successfully";

            displaySessionErrorPopUp("Info", msg, function(res) {

                //$("#divTreeView").empty();
                IsBindTreeView = true;
                getUserGroups();
                // initControls();
                // onClickBackToList();
                //onClickSecurityMaster(selMasterId);

                $("#divEntityPermission").hide();
                $("#divAddUserToGroup").hide();
                $("#linkGroupUserPopup").hide();
                $("#tblEntityPermission tbody tr").remove();




                selGroupId = null;
                selGroupName = null;

                // var selectedNode = treeview.select();
                $("#divUserList").hide();
                $("#cmbUserList").empty();
                $("#cmbTenantUserType").val(0);
                $("#ulGroupUser").empty();
                $("#ulGroupUser").hide();

            })
        }else{
            customAlert.error("Error", dataObj.response.status.message);
        }
    }else{

    }
}

function onClickAddUserToGroup(){
    $("#divEntityPermission").hide();

    getUserTypes();

    $("#divAddUserToGroup").show();
}

var IsDeleteUser = 0;
function onClickAddUser() {
    if (IsDeleteUser == 0) {
        $("#divPopupTreeView").empty();

        getUserTypes();
        $("#dvDetail").show();
        $("#dvAdd").show();
        $("#spRole").html("Role - " + selGroupName);
        $("#linkGroupUserPopup").show();

        $("#divPopupTreeView").append('<div id="treeviewpop"></div>');

        $("#treeviewpop").kendoTreeView({
            select: onSelectpopup,
            dataSource: treeDataSourcepopup
        });
    }

}


function onChangeTenantUserType(){
    bindTenantUsers();
}


function bindTenantUsers(){
    $("#cmbUserList").empty();
    var selectedUserTypeId = null;
    if($("#cmbTenantUserType option:selected").val() !== null){
        selectedUserTypeId = Number($("#cmbTenantUserType option:selected").val());
    }

    if (selectedUserTypeId !== null && selectedUserTypeId > 0) {
        var usersToBind = [];

        if (userGroupMaster !== null && userGroupMaster.length > 0) {
            if (userListMaster !== null && userListMaster.length > 0) {
                for(var i = 0; i < userListMaster.length; i++){

                    var tmpUsers = $.grep(userGroupMaster,function(e){
                        return e.userId === userListMaster[i].id;
                    });

                    if (tmpUsers === null || (tmpUsers !== null && tmpUsers.length === 0)) {

                        if (userListMaster[i].userTypeId === selectedUserTypeId) {
                            usersToBind.push(userListMaster[i]);
                        }
                    }
                }
            }
        }
        else{
            usersToBind = userListMaster;
        }

        if(usersToBind !== null && usersToBind.length > 0){
            $.each(usersToBind,function(i,e){
                $("#cmbUserList").append('<option value="'+e.id+'">'+e.lastName+' '+e.firstName+'</option>');
            })

            $("#cmbUserList").multipleSelect({
                selectAll: true,
                width: 400,
                height:500,
                dropWidth: 400,
                multipleWidth: 400,
                placeholder: 'Select User',
                selectAllText: 'All'

            });

            $("#divUserList").show();
        }
        else{
            customAlert.error("Info", "No users found with this selection.");
            $("#divUserList").hide();
            $("#cmbUserList").empty();
        }
    }
    else{
        customAlert.error("Info", "Please select valid User Type.");
        $("#divUserList").hide();
        $("#cmbUserList").empty();
    }

}

function onClickShowDetail(){
    $("#divAddUserToGroup").hide();
    var dtArray = [];
    dtArray = _.where(groupEntities, {groupId: selGroupId});
    if (dtArray !== null && dtArray.length > 0) {
        $("#tblEntityPermission tbody tr").remove();
        for(var c=0; c < dtArray.length;c++){

            if (entityMaster !== null && entityMaster.length > 0) {
                var entitynameArr = $.map(entityMaster,function(e){
                    if (e.id === dtArray[c].entityId) {
                        return e.name;
                    }
                    else{
                        return null;
                    }
                });

                dtArray[c].entityName = entitynameArr !== null ? entitynameArr[0] : null;

                var groups,grpType;
                groups = _.where(groupMaster, {id: dtArray[c].groupId});

                if(groups.type == 0){
                    grpType = "ALL";
                }
                else{
                    grpType = "SELF";
                }
                var permissions = getPermissionsById(dtArray[c].permission,1);

                var row = '<tr><td>'+dtArray[c].entityName+'</td><td>'+grpType+'</td>'+permissions+'</tr>';
                $("#tblEntityPermission tbody").append(row);
            }
        }

        $("#divEntityPermission").show();
    }

}

function getPermissionsById(permissionId,id){
    if(id==0){
        $("#chkR").prop('checked', false);
        $("#chkC").prop('checked', false);
        $("#chkU").prop('checked', false);
        $("#chkD").prop('checked', false);
    }
    var permissions = "";
    if (permissionId === 1) {
        if(id==1) {
            // permissions = "<div><span style='color: green'>"+"View"+"</span><span>,Add,Edit,Delete</span></div>";
            permissions = "<td><input type='checkbox' checked disabled='true'></td>";
        }
        else if(id==2){
            permissions = "View";
        }
        else{
            $("#chkR").prop('checked', true);
        }
    }
    else if (permissionId === 2) {
        if(id==1) {
            // permissions = "<div><span style='color: green'>"+"Add"+"</span><span>,View,Edit,Delete</span></div>";
            permissions = "<td></td><td><input type='checkbox' checked disabled='true'></td>";
        }
        else if(id==2){
            permissions = "Add";
        }
        else{
            $("#chkC").prop('checked', true);
        }
    }
    else if (permissionId === 3) {
        if(id==1) {
            // permissions = "<div><span style='color: green'>"+"View,Add"+"</span><span>,Edit,Delete</span></div>";
            permissions = "<td><input type='checkbox' checked disabled='true'></td><td><input type='checkbox' checked disabled='true'></td>";
        }
        else if(id==2){
            permissions = "View,Add";
        }
        else{
            $("#chkR").prop('checked', true);
            $("#chkC").prop('checked', true);
        }
    }
    else if (permissionId === 4) {
        if(id==1) {
            //permissions = "<div><span style='color: green'>"+"Edit"+"</span><span>,View,Add,Delete</span></div>";
            permissions = "<td></td><td></td><td><input type='checkbox' checked disabled='true'></td>";
        }
        else if(id==2){
            permissions = "Edit";
        }
        else{
            $("#chkU").prop('checked', true);
        }
    }
    else if (permissionId === 5) {
        if(id==1) {
            //permissions = "<div><span style='color: green'>"+"View,Edit"+"</span><span>,Add,Delete</span></div>";
            permissions = "<td><input type='checkbox' checked disabled='true'></td><td></td><td><input type='checkbox' checked disabled='true'></td>";
        }
        else if(id==2){
            permissions = "View,Edit";
        }
        else{
            $("#chkR").prop('checked', true);
            $("#chkU").prop('checked', true);
        }
    }
    else if (permissionId === 6) {
        if(id==1) {
            //permissions = "<div><span style='color: green'>"+"Add,Edit"+"</span><span>,View,Delete</span></div>";
            permissions = "<td></td><td><input type='checkbox' checked disabled='true'></td><td><input type='checkbox' checked disabled='true'></td>";
        }
        else if(id==2){
            permissions = "Add,Edit";
        }
        else{
            $("#chkC").prop('checked', true);
            $("#chkU").prop('checked', true);
        }
    }
    else if (permissionId === 7) {
        if(id==1) {
            //permissions = "<div><span style='color: green'>"+"Add,View,Edit"+"</span><span>,Delete</span></div>";
            permissions = "<td><input type='checkbox' checked disabled='true'></td><td><input type='checkbox' checked disabled='true'></td><td><input type='checkbox' checked disabled='true'></td>";
        }
        else if(id==2){
            permissions = "Add,View,Edit";
        }
        else{
            $("#chkC").prop('checked', true);
            $("#chkU").prop('checked', true);
            $("#chkR").prop('checked', true);
        }
    }
    else if (permissionId === 8) {
        if(id==1) {
            //permissions = "<div><span style='color: green'>"+"Delete"+"</span><span>,View,Add,Edit</span></div>";
            permissions = "<td></td><td></td><td></td><td><input type='checkbox' checked disabled='true'></td>";
        }
        else if(id==2){
            permissions = "Delete";
        }
        else{
            $("#chkD").prop('checked', true);
        }

    }
    else if (permissionId === 9) {
        if(id==1) {
            //permissions = "<div><span style='color: green'>"+"View,Delete"+"</span><span>,Add,Edit</span></div>";
            permissions = "<td><input type='checkbox' checked disabled='true'></td><td></td><td></td><td><input type='checkbox' checked disabled='true'></td>";
        }
        else if(id==2){
            permissions = "View,Delete";
        }
        else{
            $("#chkD").prop('checked', true);
            $("#chkR").prop('checked', true);
        }
    }
    else if (permissionId === 10) {
        if(id==1) {
            //permissions = "<div><span style='color: green'>"+"Add,Delete"+"</span><span>,View,Edit</span></div>";
            permissions = "<td></td><td><input type='checkbox' checked disabled='true'></td><td></td><td><input type='checkbox' checked disabled='true'></td>";
        }
        else if(id==2){
            permissions = "Add,Delete";
        }
        else{
            $("#chkD").prop('checked', true);
            $("#chkC").prop('checked', true);
        }
    }
    else if (permissionId === 11) {
        if(id==1) {
            //permissions = "<div><span style='color: green'>"+"Add,View,Delete"+"</span><span>,Edit</span></div>";
            permissions = "<td><input type='checkbox' checked disabled='true'></td><td><input type='checkbox' checked disabled='true'></td><td></td><td><input type='checkbox' checked disabled='true'></td>";
        }
        else if(id==2){
            permissions = "Add,View,Delete";
        }
        else{
            $("#chkD").prop('checked', true);
            $("#chkC").prop('checked', true);
            $("#chkR").prop('checked', true);
        }
    }
    else if (permissionId === 12) {
        if(id==1) {
            //permissions = "<div><span style='color: green'>"+"Edit,Delete"+"</span><span>,Add,View</span></div>";
            permissions = "<td></td><td></td><td><input type='checkbox' checked disabled='true'></td><td><input type='checkbox' checked disabled='true'></td>";
        }
        else if(id==2){
            permissions = "Edit,Delete";
        }
        else{
            $("#chkD").prop('checked', true);
            $("#chkU").prop('checked', true);
        }
    }
    else if (permissionId === 13) {
        if(id==1) {
            //permissions = "<div><span style='color: green'>"+"View,Edit,Delete"+"</span><span>,Add</span></div>";
            permissions = "<td><input type='checkbox' checked disabled='true'></td><td></td><td><input type='checkbox' checked disabled='true'></td><td><input type='checkbox' checked disabled='true'></td>";
        }
        else if(id==2){
            permissions = "View,Edit,Delete";
        }
        else{
            $("#chkD").prop('checked', true);
            $("#chkU").prop('checked', true);
            $("#chkR").prop('checked', true);
        }
    }
    else if (permissionId === 14) {
        if(id==1) {
            //permissions = "<div><span style='color: green'>"+"Add,Edit,Delete"+"</span><span>,View</span></div>";
            permissions = "<td></td><td><input type='checkbox' checked disabled='true'></td><td><input type='checkbox' checked disabled='true'></td><td><input type='checkbox' checked disabled='true'></td>";
        }
        else if(id==2){
            permissions = "Add,Edit,Delete";
        }
        else{
            $("#chkD").prop('checked', true);
            $("#chkU").prop('checked', true);
            $("#chkC").prop('checked', true);
        }
    }
    else if (permissionId === 15) {
        if(id==1) {
            //permissions = "<div style='color: green'>"+"View,Add,Edit,Delete"+"</div>";
            permissions = "<td><input type='checkbox' checked disabled='true'></td><td><input type='checkbox' checked disabled='true'></td><td><input type='checkbox' checked disabled='true'></td><td><input type='checkbox' checked disabled='true'></td>";
        }
        else if(id==2){
            permissions = "View,Add,Edit,Delete";
        }
        else{
            $("#chkR").prop('checked', true);
            $("#chkC").prop('checked', true);
            $("#chkU").prop('checked', true);
            $("#chkD").prop('checked', true);
        }
    }
    return permissions;
}

function onChangeShowDetail(){

}

function buildGroupEntityListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Entity",
        "field": "name",
        "width":"100%"
    });
    gridColumns.push({
        "title": "Permissions",
        "field": "name",
        "width":"100%"
    });
    angularUIgridWrapper3.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}

function onClickDelete(){
    customAlert.confirm("Confirm", "Are you sure want to delete?",function(response){
        if(response.button == "Yes"){
            var apiURL = "";
            var dataObj = {};
            if (selMasterId !== null && selMasterId !== 0) {
                if (selMasterId === 1) {
                    apiURL = ipAddress + "/homecare/security/entities/";
                }
                else if (selMasterId === 2) {
                    apiURL = ipAddress + "/homecare/security/groups/";
                }
                else if (selMasterId === 3) {
                    apiURL = ipAddress + "/homecare/security/group-entities/";
                }
                else if (selMasterId === 4) {
                    apiURL = ipAddress + "/homecare/security/user-groups/";
                }
                else if (selMasterId === 5) {
                    apiURL = ipAddress + "/homecare/security/user-group-entities/";
                }
            }

            /* dataObj = selObj;*/
            //dataObj.deletedBy = Number(sessionStorage.userId);;
            dataObj.isDeleted = 1;
            dataObj.isActive = 0;
            dataObj.id = selObj.idk;
            createAjaxObject(apiURL, dataObj, "DELETE", onCreateDelete, onError);
        }
    });
}

function onCreateDelete(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "Deleted successfully";
            customAlert.error("Info", msg);

            initControls();
            onClickSecurityMaster(selMasterId);

        }
    }
}
function onClickSave(){

    var apiURL = "";
    var reqData = {};
    var IsGroupEntityExist = false;
    var IsValid = true;
    try{

        IsValid = ValidateMandatoryFields(selMasterId);

        if (IsValid) {

            if (selMasterId !== 4) {
                reqData.isActive = 1;
                reqData.isDeleted = 0;
            }

            if (selMasterId !== null && selMasterId !== 0) {
                if (selMasterId === 1) {
                    apiURL = ipAddress + "/homecare/security/entities/";
                    reqData.name = $("#txtEntity").val();
                }
                else if (selMasterId === 2) {
                    apiURL = ipAddress + "/homecare/security/groups/";
                    reqData.Name = $("#txtGroup").val();
                    reqData.Type = Number($("#cmbType option:selected").val());
                }
                else if (selMasterId === 3) {
                    apiURL = ipAddress + "/homecare/security/group-entities/";
                    var cmbEntity = $("#cmbEntity").data("kendoComboBox");
                    reqData.entityId = Number(cmbEntity.value());
                    var cmbGroup = $("#cmbGroup").data("kendoComboBox");
                    reqData.groupId = Number(cmbGroup.value());
                    /*alert($("#chkR").prop('checked'));*/
                    if($("#chkR").prop('checked')  && (!$("#chkC").prop('checked')) && (!$("#chkU").prop('checked')) && (!($("#chkD").prop('checked')))) {
                        reqData.permission = 1;
                    }
                    else if(!($("#chkR").prop('checked')) && ($("#chkC").prop('checked')) && (!$("#chkU").prop('checked')) && (!($("#chkD").prop('checked')))) {
                        reqData.permission = 2;
                    }
                    else if(!($("#chkR").prop('checked')) && !($("#chkC").prop('checked')) && ($("#chkU").prop('checked')) && (!($("#chkD").prop('checked')))) {
                        reqData.permission = 4;
                    }
                    else if(!($("#chkR").prop('checked')) && !($("#chkC").prop('checked')) && !($("#chkU").prop('checked')) && (($("#chkD").prop('checked')))) {
                        reqData.permission = 8;
                    }
                    else if($("#chkR").prop('checked') && ($("#chkC").prop('checked')) && (!$("#chkU").prop('checked')) && (!($("#chkD").prop('checked')))) {
                        reqData.permission = 3;
                    }
                    else if(($("#chkR").prop('checked')) && (!$("#chkC").prop('checked')) && ($("#chkU").prop('checked')) && (!($("#chkD").prop('checked')))) {
                        reqData.permission = 5;
                    }
                    else if(($("#chkR").prop('checked')) && !($("#chkC").prop('checked')) && (!$("#chkU").prop('checked')) && (($("#chkD").prop('checked')))) {
                        reqData.permission = 9;
                    }
                    else if($("#chkR").prop('checked') && ($("#chkC").prop('checked')) && ($("#chkU").prop('checked')) && !(($("#chkD").prop('checked')))) {
                        reqData.permission = 7;
                    }
                    else if($("#chkR").prop('checked') && $("#chkC").prop('checked') && $("#chkU").prop('checked') && $("#chkD").prop('checked')) {
                        reqData.permission = 15;
                    }
                    else if(!($("#chkR").prop('checked')) && ($("#chkC").prop('checked')) && ($("#chkU").prop('checked')) && (!($("#chkD").prop('checked')))) {
                        reqData.permission = 6;
                    }
                    else if(!($("#chkR").prop('checked')) && ($("#chkC").prop('checked')) && (!$("#chkU").prop('checked')) && (($("#chkD").prop('checked')))) {
                        reqData.permission = 10;
                    }
                    else if(($("#chkR").prop('checked')) && ($("#chkC").prop('checked')) && (!$("#chkU").prop('checked')) && (($("#chkD").prop('checked')))) {
                        reqData.permission = 11;
                    }
                    else if(!($("#chkR").prop('checked')) && (!$("#chkC").prop('checked')) && ($("#chkU").prop('checked')) && (($("#chkD").prop('checked')))) {
                        reqData.permission = 12;
                    }
                    else if(($("#chkR").prop('checked')) && (!$("#chkC").prop('checked')) && ($("#chkU").prop('checked')) && (($("#chkD").prop('checked')))) {
                        reqData.permission = 13;
                    }
                    else if(!($("#chkR").prop('checked')) && ($("#chkC").prop('checked')) && ($("#chkU").prop('checked')) && (($("#chkD").prop('checked')))) {
                        reqData.permission = 14;
                    }


                    IsGroupEntityExist = CheckGroupEntityExistence(reqData.groupId,reqData.entityId);

                }
                else if (selMasterId === 4) {
                    var userIds = [];
                    var obj = [];
                    var cmbGroup = $("#cmbGroup").data("kendoComboBox");
                    userIds = $('#cmbUsers').multipleSelect('getSelects');
                    for (var i = 0; i < userIds.length; i++) {
                        var reqData = {};
                        reqData.groupId = Number(cmbGroup.value());
                        reqData.userId = userIds[i];
                        reqData.createdBy = Number(sessionStorage.getItem("userId"));
                        reqData.isActive = 1;
                        reqData.isDeleted = 0;
                        obj.push(reqData);
                    }
                    apiURL = ipAddress + "/homecare/security/user-groups/batch/";


                }
                else if (selMasterId === 5) {
                    apiURL = ipAddress + "/homecare/security/user-group-entities" +
                        "/";
                    var cmbEntity = $("#cmbEntity").data("kendoComboBox");
                    reqData.entityId = Number(cmbEntity.value());
                    var cmbGroup = $("#cmbGroup").data("kendoComboBox");
                    reqData.groupId = Number(cmbGroup.value());
                }
            }

            var method = "POST";
            if(operation == UPDATE){
                var selectedItems1 = angularUIgridWrapper.getSelectedRows();
                reqData.id = selectedItems1[0].idk;
                method = "PUT";
            }

            if(method == "POST" && selMasterId != 4){
                reqData.createdBy = Number(sessionStorage.getItem("userId"));
            }
            else if (method == "PUT") {
                reqData.modifiedBy = Number(sessionStorage.getItem("userId"));
                if (selObj != null && selObj.idk > 0) {
                    reqData.id = selObj.idk;
                }
            }

            if((!IsGroupEntityExist && method == "POST") ||(method == "PUT")){
                if(selMasterId != 4){
                    createAjaxObject(apiURL, reqData, method, onCreate, onError);
                }
                else{
                    createAjaxObject(apiURL, obj, method, onCreate, onError);
                }
            }
            else{
                customAlert.error("Error", "Group permission already exist.");
            }

        }
        else{
            customAlert.error("Error", "Please enter all mandatory fields.");
        }
    }catch(ex){
        console.log(ex);
    }
}


function ValidateMandatoryFields(selMasterId){
    var IsValid = true;
    if (selMasterId === 1) {
        if($("#txtEntity").val().trim().length === 0){
            IsValid = false;
        }
    }
    else if (selMasterId === 2) {
        if($("#txtGroup").val().trim().length === 0){
            IsValid = false;
        }
    }

    return IsValid;
}

function CheckGroupEntityExistence(groupId, entityId){
    var IsGroupEntityExist = false;
    if(groupEntities !== null && groupEntities.length > 0){
        var tempGroupEntity = $.grep(groupEntities,function(e){
            return e.groupId === groupId && e.entityId === entityId;
        });

        if(tempGroupEntity !== null && tempGroupEntity.length > 0){
            IsGroupEntityExist = true;
        }

    }

    return IsGroupEntityExist;
}

function onCreate(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "";
            var temp = "";
            msg = "User added to role successfully";
            if(operation == UPDATE){
                msg = "Updated Successfully";
            }

            if (selMasterId === 1) {
                temp = "Entity";
            }
            else if (selMasterId === 2) {
                temp = "Group";
            }
            else if(selMasterId === 3){
                temp = "Group Permission";
            }

            if (operation === ADD) {
                msg = temp+" added successfully.";
            }
            else if(operation === UPDATE){
                msg = temp +" updated successfully.";
            }

            displaySessionErrorPopUp("Info", msg, function(res) {
                // resetData();
                // operation = ADD;
                // //onChange1();
                // init();
                initControls();
                onClickBackToList();
                onClickSecurityMaster(selMasterId);
                $("#divUserList").hide();
                $("#cmbUserList").empty();
                $("#cmbTenantUserType").val(0);
            })
        }else{
            customAlert.error("Error", dataObj.response.status.message);
        }
    }else{

    }
}

function resetData(){
    $("#txtNotes").val("");
    $("#txtMiniTemplateName").val("");
}
function adjustHeight(){
    var defHeight = 260;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    if(angularUIgridWrapper){
        angularUIgridWrapper.adjustGridHeight(cmpHeight+8);
    }
    if(angularUIgridWrapper1){
        angularUIgridWrapper1.adjustGridHeight((cmpHeight+160));
    }
    if(angularUIgridWrapper3){
        angularUIgridWrapper3.adjustGridHeight((cmpHeight+160));
    }
}

function buildDeviceListGrid(dataSource,gridColumns) {

    var otoptions = {};
    otoptions.noUnselect = false;

    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();

}
function buildDeviceListGrid1(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "App Permissions",
        "field": "name",
        "width":"100%"
    });
    angularUIgridWrapper1.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}

var selRow = null;
function onClickAction(){

}

var prevSelectedItem =[];
function onChange(){
    setTimeout(function() {
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        if (selectedItems && selectedItems.length > 0) {
            selObj = selectedItems[0];
            $("#btnEdit").prop("disabled", false);
            $("#btnDelete").prop("disabled", false);

        } else {
            $("#btnEdit").prop("disabled", true);
            $("#btnDelete").prop("disabled", true);

        }
    });
}
function onChange1(){
    setTimeout(function() {
        onClickBackToList();

        var selectedItems = angularUIgridWrapper1.getSelectedRows();

        if(selectedItems && selectedItems.length>0){

            $("#divEvents").removeClass("addEvents");

            if(selectedItems != null && selectedItems.length > 0){

                selMasterId = selectedItems[0].idk;

                onClickSecurityMaster(selMasterId);



            }


        }else{
            $("#divEvents").addClass("addEvents");
        }
    });
}
var operation = "ADD";
var ADD = "ADD";
var UPDATE = "UPDATE";
var atID = "";
var ds  = "";
function onClickEdit(){

    var selectedItems = angularUIgridWrapper.getSelectedRows();
    if (selectedItems && selectedItems.length > 0) {
        selObj = selectedItems[0];
        operation = UPDATE;
        userId = null;
        $("#txtUserName").val("");
        $("#divScreenTreeList").hide();
        $("#divCmbCRUD").css("display","none");
        if (selMasterId !== null && selMasterId !== 0 && selObj !== null && selObj.idk > 0) {
            fncHideAllInputs();
            if (selMasterId === 1) {
                $("#hTitle").text("Edit Type");
                $("#txtEntity").val(selObj.name);
                $("#divInputEntity").css("display","block");
            }
            else if (selMasterId === 2) {
                $("#hTitle").text("Edit Group");
                $("#txtGroup").val(selObj.Name);
                $("#divInputGroup").css("display","block");
                $("#divCmbType").css("display","block");
                $("#cmbType").val(selObj.Type);
            }
            else if (selMasterId === 3) {
                $("#hTitle").text("Edit Group Permission");
                var cmbEntity = $("#cmbEntity").data("kendoComboBox");
                if (cmbEntity) {
                    cmbEntity.value(selObj.entityId);
                }
                var cmbGroup = $("#cmbGroup").data("kendoComboBox");
                if (cmbGroup) {
                    cmbGroup.value(selObj.groupId);
                }
                $("#divCmbEntity").css("display","block");
                $("#divCmbGroup").css("display","block");
                $("#divCmbCRUD").css("display","block");
                getPermissionsById(selObj.permission,0);
            }
            else if (selMasterId === 4) {
                $("#hTitle").text("Edit User Group");
                var cmbGroup = $("#cmbGroup").data("kendoComboBox");
                if (cmbGroup) {
                    cmbGroup.value(selObj.groupId);
                }
                var uName = getUserName(selObj.userId);
                userId = selObj.userId;
                $("#txtUserName").val(uName);
                $("#divUserGroup").css("display","block");
                $("#divCmbGroup").css("display","block");
            }

            $("#divSecurityMastersList").css("display","none");
            $("#addSecurityMasterPopup").css("display","block");
        }
    }
}

function getComboListIndex(cmbId, attr, attrVal) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb) {
        var ds = cmb.dataSource;
        var totalRec = ds.total();
        for (var i = 0; i < totalRec; i++) {
            var dtItem = ds.at(i);
            if (dtItem && dtItem[attr] == attrVal) {
                cmb.select(i);
                return i;
            }
        }
    }
    return -1;
}
function closeVideoScreen(evt,returnData){

}
var operation = "add";
var selFileResourceDataItem = null;
function onClickCancel(){
    var obj = {};
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}


function themeAPIChange(){
    getAjaxObject(ipAddress+"/homecare/settings/?id=2","GET",getThemeValue,onError);
}

function getThemeValue(dataObj){
    var tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.activityTypes){
            if($.isArray(dataObj.response.settings)){
                tempCompType = dataObj.response.settings;
            }else{
                tempCompType.push(dataObj.response.settings);
            }
        }
        if(tempCompType.length > 0){
            themesChange(tempCompType[0].value);
        }
    }
}

function themesChange(themeValue){
    if(themeValue == 2){
        loadAPi("../../Theme2/Theme02.css");
    }
    else if(themeValue == 3){
        loadAPi("../../Theme3/Theme03.css");
    }
}
var groupEntities = [];
function onGetSecurityPermissions(dataObj){
    var dtArray = [];

    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.entities){
            if($.isArray(dataObj.response.entities)){
                dtArray = dataObj.response.entities;
            }else{
                dtArray.push(dataObj.response.entities);
            }
        }
        if(dataObj.response.groups){
            if($.isArray(dataObj.response.groups)){
                dtArray = dataObj.response.groups;
            }else{
                dtArray.push(dataObj.response.groups);
            }
        }
        if(dataObj.response.groupEntities){
            if($.isArray(dataObj.response.groupEntities)){
                dtArray = dataObj.response.groupEntities;
            }else{
                dtArray.push(dataObj.response.groupEntities);
            }

            if (dtArray !== null && dtArray.length > 0) {
                groupEntities = dtArray;
                for (var j = 0; j < dtArray.length; j++) {
                    if(dtArray[j].groupId){
                        var tempArr = $.grep(groupMaster,function(e){
                            return e.id === dtArray[j].groupId;
                        });

                        if (tempArr !== null && tempArr.length > 0) {
                            dtArray[j].group = tempArr[0].Name;
                        }
                    }
                    if(dtArray[j].entityId){
                        var tempArr2 = $.grep(entityMaster,function(e){
                            return e.id === dtArray[j].entityId;
                        });

                        if (tempArr2 !== null && tempArr2.length > 0) {
                            dtArray[j].entity = tempArr2[0].name;
                        }
                    }
                    dtArray[j].groupPermission = getPermissionsById(dtArray[j].permission,2);
                }
            }
            groupEntities = dtArray;
        }
        if(dataObj.response.userGroups){
            if($.isArray(dataObj.response.userGroups)){
                dtArray = dataObj.response.userGroups;
            }else{
                dtArray.push(dataObj.response.userGroups);
            }

            if (dtArray !== null && dtArray.length > 0) {
                for (var j = 0; j < dtArray.length; j++) {
                    if(dtArray[j].groupId){
                        var tempArr = $.grep(groupMaster,function(e){
                            return e.id === dtArray[j].groupId;
                        });

                        if (tempArr !== null && tempArr.length > 0) {
                            dtArray[j].group = tempArr[0].Name;
                        }
                    }

                    if(dtArray[j].userId){
                        var tempArr = $.grep(userList,function(e){
                            return e.id === dtArray[j].userId;
                        });

                        if (tempArr !== null && tempArr.length > 0) {
                            dtArray[j].user = tempArr[0].lastName + " "+ tempArr[0].firstName;
                        }
                    }

                }
            }
        }
        if(dataObj.response.userGroupEntities){
            if($.isArray(dataObj.response.userGroupEntities)){
                dtArray = dataObj.response.userGroupEntities;
            }else{
                dtArray.push(dataObj.response.userGroupEntities);
            }
        }
        if (dtArray !== null && dtArray.length > 0) {
            for (var i = 0; i < dtArray.length; i++) {
                dtArray[i].idk = dtArray[i].id;
            }
        }

        buildDeviceListGrid(dtArray,gridColumns);

    }

}

function onClickAdd(){
    fncClearFields();
    userId = null;
    $("#txtUserName").val("");
    if (selMasterId !== null && selMasterId !== 0) {
        operation = ADD;
        operation = "ADD";
        $("#divCmbCRUD").css("display","none");
        $("#divSecurityMastersList").css("display","none");
        $("#addSecurityMasterPopup").css("display","block");

        if (selMasterId !== null && selMasterId !== 0) {
            fncHideAllInputs();

            if (selMasterId === 1) {
                $("#divInputEntity").css("display","block");
                $("#hTitle").text("Add Type");
            }
            else if (selMasterId === 2) {
                $("#divInputGroup").css("display","block");
                $("#divCmbType").css("display","block");
                $("#hTitle").text("Add Group");

            }
            else if (selMasterId === 3) {
                $("#divCmbEntity").css("display","block");
                $("#divCmbGroup").css("display","block");
                $("#divCmbCRUD").css("display","block");
                $("#hTitle").text("Add Group Permission");

            }
            else if (selMasterId === 4) {
                $("#divUserGroup").css("display","block");
                $("#divCmbGroup").css("display","block");


            }
            else if (selMasterId === 5) {
                $("#divCmbGroup").css("display","block");
                $("#divCmbEntity").css("display","block");

            }
        }
    }
    else {
        alert("Please select security master.");
    }
}

function onClickBackToList(){
    $("#divSecurityMastersList").css("display","block");
    $("#addSecurityMasterPopup").css("display","none");

    fncClearFields();
}


function fncHideAllInputs(){
    $("#divInputEntity").css("display","none");
    $("#divInputGroup").css("display","none");
    $("#divCmbEntity").css("display","none");
    $("#divCmbGroup").css("display","none");
    $("#divCmbType").css("display","none");

}


function initControls(){
    $("cmbEntity").kendoComboBox();
    $("cmbGroup").kendoComboBox();

    getAjaxObject(ipAddress + "/homecare/security/groups/","GET",onGetGroups,onError);
    getAjaxObject(ipAddress + "/homecare/security/entities/","GET",onGetEntities,onError);

    getAjaxObject(ipAddress + "/homecare/security/group-entities/?is-active=1&is-deleted=0","GET",function(dataObj){

        var dtArray = [];
        if($.isArray(dataObj.response.groupEntities)){
            dtArray = dataObj.response.groupEntities;
        }else{
            dtArray.push(dataObj.response.groupEntities);
        }

        if (dtArray !== null && dtArray.length > 0) {
            groupEntities = dtArray;
        }
        getAjaxObject(ipAddress + "/homecare/security/entities/","GET",onGetEntities,onError);

    },onError);
    getUserGroups();

}

function getUserGroups(val = 0){
    if(val == 0) {
        getAjaxObject(ipAddress + "/homecare/security/user-groups/?is-active=1&is-deleted=0", "GET", onGetUserGroups, onError);
    }
    else{
        var userId = $("#txtUser").val();
        var groupIds = $('#cmbGroupList').multipleSelect('getSelects');
        getAjaxObject(ipAddress + "/homecare/security/user-groups/?user-id="+Number(userId)+"&group-id="+groupIds+"&is-active=1&is-deleted=0", "GET", onGetUserGroups, onError);
    }

}



function onGetEntities(dataObj){
    var dtArray = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.entities){
            if($.isArray(dataObj.response.entities)){
                dtArray = dataObj.response.entities;
            }else{
                dtArray.push(dataObj.response.entities);
            }
        }

        entityMaster = dtArray;

        var treeArray = [];
        var typeArr = [];
        for (var i = 0; i < dtArray.length; i++) {
            var obj = {};
            obj.Key = dtArray[i].name;
            obj.Value = dtArray[i].id;

            var pItem = {};

            pItem.text = dtArray[i].name;
            pItem.items = getParentChildItems(dtArray[i].id);
            pItem.idk = dtArray[i].id;
            pItem.spriteCssClass = "rootfolder txtBlue";
            pItem.type = "Entity";
            treeArray.push(pItem);

            typeArr.push(obj);

        }
        treeDataSource[0].items = treeArray;

        setDataForSelection(typeArr, "cmbEntity", onComboBoxChange, ["Key", "Value"], 0, "");

    }
}

function onGetGroups(dataObj){
    var dtArray = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.groups){
            if($.isArray(dataObj.response.groups)){
                dtArray = dataObj.response.groups;
            }else{
                dtArray.push(dataObj.response.groups);
            }
        }

        groupMaster = dtArray;

        var typeArr = [];
        for (var i = 0; i < dtArray.length; i++) {
            var obj = {};
            obj.Key = dtArray[i].Name;
            obj.Value = dtArray[i].id;
            typeArr.push(obj);

        }
        setDataForSelection(typeArr, "cmbUserGroup", onComboBoxChange, ["Key", "Value"], 0, "");
        /*setDataForSelection(typeArr, "cmbUserGroup1", onComboBoxChange, ["Key", "Value"], 0, "");*/
        setDataForSelection(typeArr, "cmbGroup", onComboBoxChange, ["Key", "Value"], 0, "");


        for (var i = 0; i < typeArr.length; i++) {

            $("#cmbGroupList").append('<option value="' + typeArr[i].Value + '">' + typeArr[i].Key + '</option>');

        }

        $("#cmbGroupList").multipleSelect({
            selectAll: true,
            width: 200,
            dropWidth: 200,
            multipleWidth: 200,
            placeholder: 'Select Group',
            selectAllText: 'All'

        });
    }
}

function onGetUserGroups(dataObj){
    var dtArray = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){

        if(dataObj.response.userGroups){
            if($.isArray(dataObj.response.userGroups)){
                dtArray = dataObj.response.userGroups;
            }else{
                dtArray.push(dataObj.response.userGroups);
            }
        }

        userGroupMaster = dtArray;
        bindGroupEntityTreeView2();
    }
}

function onComboBoxChange(){

}

function onClickSecurityMaster(id,url) {
    $("#divScreenTreeList").hide();
    $("#dvGrid").show();
    $("#dvUrGroup").hide();
    $("#dvUserGroupView").hide();
    $("#divUserblockGroup1").hide();
    $("#dvUserEntity").hide();
    if (id) {
        gridColumns = [];
        var apiURL = "";
        if (id === 1) {
            // $(".formDatabtns").show();
            $("#dvUserGroupView").hide();
            $("#hTitle").text("Add Type");
            apiURL = ipAddress + "/homecare/security/entities/?is-active=1&is-deleted=0";
            gridColumns.push({
                "title": "Type",
                "field": "name"
            });
        }
        else if (id === 2) {
            $("#hTitle").text("Add Group");
            // $(".formDatabtns").show();
            $("#dvUserGroupView").hide();
            apiURL = ipAddress + "/homecare/security/groups/?is-active=1&is-deleted=0";
            gridColumns.push({
                "title": "Group",
                "field": "Name"
            });
        }
        else if (id === 3) {
            $("#hTitle").text("Add Group Permission");
            $(".formDatabtns").show();
            $("#dvUserGroupView").hide();
            apiURL = ipAddress + "/homecare/security/group-entities/?is-active=1&is-deleted=0";
            gridColumns.push({
                "title": "Group",
                "field": "group"
            });
            gridColumns.push({
                "title": "Type",
                "field": "entity"
            });

            gridColumns.push({
                "title": "Permissions",
                "field": "groupPermission"
            });
        }
        else if (id === 4) {
            $("#hTitle").text("Add User Group");
            // $(".formDatabtns").show();
            $("#dvUserGroupView").hide();
            apiURL = ipAddress + "/homecare/security/user-groups/?is-active=1&is-deleted=0";
            gridColumns.push({
                "title": "User",
                "field": "user"
            });
            gridColumns.push({
                "title": "Group",
                "field": "group"
            });
        }
        else if (id === 5) {
            $("#hTitle").text("Add User Group Entity");
            $(".formDatabtns").hide();
            $("#divScreenTreeList").show();
            // $("#dvGrid").show();*/
            /* $("#dvUrGroup").show();*/
            $("#dvUserGroupView").show();
            $("#divUserblockGroup1").show();

            gridColumns.push({
                "title": "Type",
                "field": "entityName"
            });

            gridColumns.push({
                "title": "Group",
                "field": "groupName"
            });
            apiURL = url;
            /*apiURL = ipAddress + "/homecare/security/user-group-entities/?user-id=101&entity-id=1";
            gridColumns.push({
                "title": "User",
                "field": "user"
            });
            gridColumns.push({
                "title": "Group",
                "field": "group"
            });
            gridColumns.push({
                "title": "Entity",
                "field": "entity"
            });*/

        }
        else if (id === 6){
            $("#dvGrid").hide();
            $("#dvUserEntity").show();
            getUserTypes();
            //getScreenListTree();
            bindGroupEntityTreeView2();
        }
        if (id != 6){
            var dataOptions = {
                pagination: false,
                changeCallBack: onChange
            }
            angularUIgridWrapper = new AngularUIGridWrapper("dgridSecurityMastersList", dataOptions);
            angularUIgridWrapper.init();
            buildDeviceListGrid([], gridColumns);
            getAjaxObject(apiURL, "GET", onGetSecurityPermissions, onError);
        }
    }
}


function fncClearFields(){
    $("txtEntity").val("");
    $("txtGroup").val("");


    if ($("#cmbEntity").data("kendoComboBox")) {
        $("#cmbEntity").data("kendoComboBox").select(0);
    }
    if ($("#cmbGroup").data("kendoComboBox")) {
        $("#cmbGroup").data("kendoComboBox").select(0);
    }

    $("#cmbType").val(0);
    $("#chkR").prop('checked', false);
    $("#chkC").prop('checked', false);
    $("#chkU").prop('checked', false);
    $("#chkD").prop('checked', false);

}

function onClickBlockDays(){
    var popW = "65%";
    var popH = "70%";

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search User";
    devModelWindowWrapper.openPageWindow("../../html/reports/searchUser.html", profileLbl, popW, popH, true, closeUserAction);
    //var requ
    ired = $("#required").data("kendoMultiSelect");
}

function closeUserAction(dataObj,returnData){
    userId = returnData.selItem.PID;
    var name = returnData.selItem.LN + " "+ returnData.selItem.FN;
    $("#txtUserName").val(name);
}

function getGroupName(idk, val){

    if (groupMaster !== null) {
        for(var i=0;i<groupMaster.length;i++){
            var uItem = groupMaster[i];
            if(uItem && uItem.id == idk){
                if(val == 1) {
                    var notesArray = uItem.Name.split("~");
                    return notesArray[0];
                }
                else if(val == 2){
                    var notesArray = uItem.Name.split("~");
                    return notesArray[1];
                }
                else if(val == 3){
                    var notesArray = uItem.Name.split("~");
                    return notesArray[2];
                }
            }
        }
    }
    return "";
}

function getUserName(idk){
    for(var i=0;i<userList.length;i++){
        var uItem = userList[i];
        if(uItem && uItem.id == idk){
            return uItem.lastName + "  "+ uItem.firstName;
        }
    }
    return "";
}

function getManagers(){
    getAjaxObject(ipAddress+"/homecare/tenant-users/?is-active=1&is-deleted=0","GET",getUserList,onError);
}
var userList = [];

var seluserList = [];
function getUserList(dataObj) {
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            if ($.isArray(dataObj.response.tenantUsers)) {
                userList = dataObj.response.tenantUsers;
            } else {
                userList.push(dataObj.response.tenantUsers);
            }
        }
    }



    if (userList && userList.length > 0) {
        userList.sort(function (a, b) {
            var p1 = a.lastName + " " + a.firstName;
            var p2 = b.lastName + " " + b.firstName;

            if (p1 < p2) return -1;
            if (p1 > p2) return 1;
            return 0;
        });
    }

    userListMaster = userList;

    for (var i = 0; i < userList.length; i++) {
        var staffName = userList[i].lastName + " " + userList[i].firstName;
        $("#cmbUsers").append('<option value="' + userList[i].id + '">' + staffName + '</option>');
        $("#cmbUsers1").append('<option value="' + userList[i].id + '">' + staffName + '</option>');
        $("#txtUser").append('<option value="' + userList[i].id + '">' + staffName + '</option>');

    }

    $("#cmbUsers").multipleSelect({
        selectAll: true,
        width: 200,
        dropWidth: 200,
        multipleWidth: 200,
        placeholder: 'Select User',
        selectAllText: 'All'

    });

    $("#cmbUsers1").multipleSelect({
        selectAll: true,
        width: 200,
        dropWidth: 200,
        multipleWidth: 200,
        placeholder: 'Select User',
        selectAllText: 'All'

    });

}

function getScreenListTree(){
    var tree = $("#treeview").data("kendoTreeView");
    if(tree){
        tree.destroy();
    }
    $("#treeview").kendoTreeView({
        change:onTreeItemSelect,
        dataSource: treeDataSource
    });
    setTimeout(function(){
        var treeView = $("#treeview").data("kendoTreeView");
        treeView.expand(".k-item");
    },1000);

}

var platforms = [{idk:1,name:"WEB"},{idk:2,name:"IOS"},{idk:3,name:"ANDROID"}]
var userTypeArr = [{idk:1,name:"SELF"},{idk:2,name:"ALL"}]

function bindGroupEntityTreeView(){
    var treeViewDataSource = [];

    if (groupEntities !== null && groupEntities.length > 0) {

        for (var k = 0; k < groupEntities.length; k++){
            var tmpGroupId = groupEntities[k].groupId;
            var tmpEntityId = groupEntities[k].entityId;

            if (groupMaster !== null && groupMaster.length > 0) {
                var tmpArr = $.grep(groupMaster,function(e){
                    return parseInt(e.id) === parseInt(tmpGroupId)
                });

                if (tmpArr !== null && tmpArr.length > 0) {
                    groupEntities[k].groupName = tmpArr[0].Name;
                    var tmpplatform = {};
                    splitGroup(tmpArr[0].Name,tmpplatform);

                    if (tmpplatform.platformId) {
                        groupEntities[k].platformId = tmpplatform.platformId;
                    }
                    if (tmpplatform.platformName) {
                        groupEntities[k].platformName = tmpplatform.platformName;
                    }
                    if (tmpplatform.userTypeId) {
                        groupEntities[k].userTypeId = tmpplatform.userTypeId;
                    }
                    if (tmpplatform.userTypeName) {
                        groupEntities[k].userTypeName = tmpplatform.userTypeName;
                    }
                    if (tmpplatform.permission) {
                        groupEntities[k].permission = tmpplatform.permission;
                    }
                }
            }


            if (entityMaster !== null && entityMaster.length > 0) {
                var tmpArr = $.grep(entityMaster,function(e){
                    return parseInt(e.id) === parseInt(tmpEntityId)
                });

                if (tmpArr !== null && tmpArr.length > 0) {
                    groupEntities[k].entityName = tmpArr[0].name;
                }
            }

        }

        var uniqueEntityIdArr = [];

        var lookup = {};

        for (var item, i = 0; item = groupEntities[i++];) {
            var entityId = item.entityId;

            if (!(entityId in lookup)) {
                lookup[entityId] = 1;
                uniqueEntityIdArr.push(item);
            }
        }

        if (uniqueEntityIdArr !== null && uniqueEntityIdArr.length > 0) {



            for (var i = 0 ;i < uniqueEntityIdArr.length ; i++) {
                var tmpObj = {};

                tmpObj.id = uniqueEntityIdArr[i].entityId;
                tmpObj.text = uniqueEntityIdArr[i].entityName;

                var tmpGroupEntities = $.grep(groupEntities,function(e){
                    return e.entityId === uniqueEntityIdArr[i].entityId;
                });

                if (tmpGroupEntities !== null && tmpGroupEntities.length > 0) {

                    tmpObj.expanded = true;

                    var entityPlatformItems = [];

                    if (platforms !== null && platforms.length > 0) {

                        for (var c = 0 ;c < platforms.length ; c++){

                            var epfitem = {};

                            epfitem.id = platforms[c].idk;
                            epfitem.text = platforms[c].name;

                            var tmpEPFArr = $.grep(tmpGroupEntities,function(e){
                                return e.platformId === platforms[c].idk && e.entityId === uniqueEntityIdArr[i].entityId;
                            });

                            if (tmpEPFArr !== null && tmpEPFArr.length > 0) {

                                epfitem.expanded = true;

                                var epfitems = [];


                                if (userTypeArr !== null && userTypeArr.length > 0) {
                                    for (var u = 0; u < userTypeArr.length; u++) {

                                        var epfuitem = {};

                                        epfuitem.id = userTypeArr[u].idk;
                                        epfuitem.text = userTypeArr[u].name;

                                        var tmpEPUArr=$.grep(tmpEPFArr,function(e){
                                            return e.userTypeId === userTypeArr[u].idk;
                                        });

                                        if (tmpEPUArr !== null && tmpEPUArr.length > 0) {
                                            epfuitem.expanded = true;
                                            var epfuitems = [];

                                            for (var p = 0; p < tmpEPUArr.length; p++) {

                                                var epfupitem = {};

                                                epfupitem.id = tmpEPUArr[p].groupId;
                                                epfupitem.text = tmpEPUArr[p].permission;

                                                epfuitems.push(epfupitem);
                                            }
                                            epfuitem.items = epfuitems;
                                        }
                                        epfitems.push(epfuitem);
                                    }
                                }
                                epfitem.items = epfitems;
                            }
                            entityPlatformItems.push(epfitem);
                        }
                    }
                    tmpObj.items = entityPlatformItems;
                }
                treeViewDataSource.push(tmpObj);
            }

        }

        if (treeViewDataSource !== null && treeViewDataSource.length > 0) {
            $("#treeview").kendoTreeView({
                checkboxes: {
                    checkChildren: true
                },
                check: onCheck,
                dataSource: treeViewDataSource
            });

        }

    }
}

function bindGroupEntityTreeView2() {

    $("#ulGroupMaster").empty();
    $("#ulGroupUser").empty();
    $("#ulGroupUser").hide();


    if (groupMaster !== null && groupMaster.length > 0) {
        for (var i = 0; i < groupMaster.length; i++) {

            if (tmpUserGroupMaster != null && tmpUserGroupMaster.length > 0) {
                var groups = _.where(tmpUserGroupMaster, {groupId: groupMaster[i].id});
                if (groups && groups != null && groups.length > 0) {
                    $("#ulGroupMaster").append('<li class="list-group-item" style="cursor: pointer;" idk="' + groupMaster[i].id + '">' + groupMaster[i].Name.toUpperCase() + '</li>');
                }
            } else {
                $("#ulGroupMaster").append('<li class="list-group-item" style="cursor: pointer;" idk="' + groupMaster[i].id + '">' + groupMaster[i].Name.toUpperCase() + '</li>');
            }
        }
    }
}

function onDragStart(e) {
    console.log("Started dragging " + this.text(e.sourceNode));
}

function onDrag(e) {
    console.log("Dragging " + this.text(e.sourceNode));
}

function onDrop(e) {
    debugger;

    var sourceUid = e.sourceNode.dataset.uid;
    var destinationUid = e.destinationNode.dataset.uid;
    var sourceNode, destinationNode;
    var sourceGroupId, destinationGroupId, sourceUserId, sourceMasterId;

    var dataSource = e.sender.dataSource.data();

    if (dataSource !== null && dataSource.length > 0) {

        for (var i = 0; i < dataSource.length; i++) {
            if (dataSource[i].children && dataSource[i].children.data() && dataSource[i].children.data().length > 0) {

                var childDataSource = dataSource[i].children.data();

                sourceNode = $.grep(childDataSource, function (e) {
                    return e.uid === sourceUid;
                });

                if (sourceNode !== null && sourceNode.length > 0) {
                    break;
                }
            }
        }


        destinationNode = $.grep(dataSource, function (e) {
            return e.uid === destinationUid;
        });
    }

    if (sourceNode !== null && sourceNode.length > 0 && destinationNode !== null && destinationNode.length > 0) {
        sourceUserId = sourceNode[0].id;

        if (userGroupMaster !== null && userGroupMaster.length > 0) {
            var existingGroupUser = $.grep(userGroupMaster, function (e) {
                return e.userId === sourceUserId && e.isActive === 1 && e.isDeleted === 0;
            });

            if (existingGroupUser !== null && existingGroupUser.length > 0) {
                sourceGroupId = existingGroupUser[0].groupId;
                sourceMasterId = existingGroupUser[0].id;
            }

            destinationGroupId = destinationNode[0].id;


            var reqData = {};
            reqData.groupId = Number(sourceGroupId);
            reqData.userId = Number(sourceUserId);
            reqData.modifiedBy = Number(sessionStorage.getItem("userId"));
            reqData.id = sourceMasterId;
            reqData.isActive = 0;
            reqData.isDeleted = 1;

            createAjaxObject(ipAddress + "/homecare/security/user-groups/", reqData, "PUT", onDeleteUser2, onError);

            var reqData2 = {};
            reqData2.groupId = Number(destinationGroupId);
            reqData2.userId = Number(sourceUserId);
            reqData2.createdBy = Number(sessionStorage.getItem("userId"));

            reqData2.isActive = 1;
            reqData2.isDeleted = 0;

            createAjaxObject(ipAddress + "/homecare/security/user-groups/", reqData2, "POST", onSaveUserGroup2, onError);

        }
    } else {
        //return false;
    }
}

function onDragEnd(e) {
    console.log("Finished dragging " + this.text(e.sourceNode));
}

function onSelect(e){
    $("#divPopupTreeView").empty();

    var uid = e.node.attributes["data-uid"].value;
    var dataSource = e.sender.dataSource.data();

    if (dataSource !== null && dataSource.length > 0) {
        var selectedNode = $.grep(dataSource,function(e){
            return e.uid === uid;
        });

        if (selectedNode !== null && selectedNode.length > 0) {
            selGroupId = selectedNode[0].id;
            selGroupName = selectedNode[0].text;
            $("#dvDetail").show();
            $("#dvAdd").show();
            $("#spRole").html("Role - " + selGroupName);
            $("#linkGroupUserPopup").show();

            $("#divPopupTreeView").append('<div id="treeviewpop"></div>');

            $("#treeviewpop").kendoTreeView({
                select:onSelectpopup,
                dataSource: treeDataSourcepopup
            });
        }
        else{
            var childNode,tmpGroupId;
            for(var i = 0;i < dataSource.length; i++){
                if(dataSource[i].children && ((isFunction(dataSource[i].children.data) && dataSource[i].children.data() && dataSource[i].children.data().length > 0) || (dataSource[i].children.data && dataSource[i].children.data.length > 0))){
                    var childDataSource;
                    if (isFunction(dataSource[i].children.data)) {
                        childDataSource = dataSource[i].children.data();
                    }
                    else{
                        childDataSource = dataSource[i].children.data;
                    }



                    childNode = $.grep(childDataSource,function(e){
                        return e.uid === uid;
                    });

                    if (childNode !== null && childNode.length > 0) {
                        tmpGroupId =dataSource[i].id;
                        break;
                    }
                }
            }

            if (childNode !== null && childNode.length > 0) {
                var msg = "Are you sure want to delete?";
                displayConfirmSessionErrorPopUp("Info", msg, function (res) {

                    var objUserGroup;
                    if (userGroupMaster !== null && userGroupMaster.length > 0) {
                        objUserGroup = $.grep(userGroupMaster,function(e){
                            return e.userId === Number(childNode[0].id) && e.groupId === Number(tmpGroupId) && e.isActive === 1 && e.isDeleted === 0;
                        })
                    }

                    var reqData = {};
                    reqData.groupId = Number(tmpGroupId);
                    reqData.userId = Number(childNode[0].id);
                    reqData.modifiedBy = Number(sessionStorage.getItem("userId"));
                    if (objUserGroup !== null && objUserGroup.length > 0) {
                        reqData.id = objUserGroup[0].id;
                    }
                    reqData.isActive = 0;
                    reqData.isDeleted = 1;


                    createAjaxObject(ipAddress + "/homecare/security/user-groups/", reqData, "PUT", onDeleteUser, onError);

                });
            }
        }
    }
}

function isFunction(functionToCheck) {
    return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
}

function onSelectpopup(e) {
    var selId;
    var uid = e.node.attributes["data-uid"].value;
    var dataSource = e.sender.dataSource.data();

    if (dataSource !== null && dataSource.length > 0) {
        var selectedNode = $.grep(dataSource, function (e) {
            return e.uid === uid;
        });

        if (selectedNode !== null && selectedNode.length > 0) {
            selId = selectedNode[0].idk;
            if(selId == 0){
                onClickAddUserToGroup();
            }
            else{
                onClickShowDetail();
            }

        }
    }
}

function onDeleteUser(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "";
            msg = "User deleted successfully.";

            displaySessionErrorPopUp("Info", msg, function(res) {

                $("#ulGroupMaster").empty();
                IsBindTreeView = true;
                getUserGroups();
                // initControls();
                // onClickBackToList();
                //onClickSecurityMaster(selMasterId);

                $("#divEntityPermission").hide();
                $("#divAddUserToGroup").hide();
                $("#linkGroupUserPopup").hide();
                $("#tblEntityPermission tbody tr").remove();


                $("#cmbUserList").empty();

            })
        }else{
            customAlert.error("Error", dataObj.response.status.message);
        }
    }else{

    }
}

var IsDeleteSuccess = false;
var IsUpdateSuccess = false;

function onDeleteUser2(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            IsDeleteSuccess = true;
            if(IsDeleteSuccess && IsUpdateSuccess){

                msg = "User updated to role successfully";

                displaySessionErrorPopUp("Info", msg, function(res) {


                    getUserGroups();

                    $("#divEntityPermission").hide();
                    $("#divAddUserToGroup").hide();
                    $("#linkGroupUserPopup").hide();
                    $("#tblEntityPermission tbody tr").remove();


                    $("#cmbUserList").empty();
                })
            }
        }
    }
}

function onSaveUserGroup2(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            IsUpdateSuccess = true;
            if(IsDeleteSuccess && IsUpdateSuccess){

                msg = "User updated to role successfully";

                displaySessionErrorPopUp("Info", msg, function(res) {


                    getUserGroups();

                    $("#divEntityPermission").hide();
                    $("#divAddUserToGroup").hide();
                    $("#linkGroupUserPopup").hide();
                    $("#tblEntityPermission tbody tr").remove();


                    $("#cmbUserList").empty();
                })
            }
        }
    }
}

// function that gathers IDs of checked nodes
function checkedNodeIds(nodes, checkedNodes) {
    for (var i = 0; i < nodes.length; i++) {
        if (nodes[i].checked) {
            checkedNodes.push(nodes[i].id);
        }

        if (nodes[i].hasChildren) {
            checkedNodeIds(nodes[i].children.view(), checkedNodes);
        }
    }
}
var message;
// show checked node IDs on datasource change
function onCheck() {
    $("#linkGroupUserPopup").show();
    var checkedNodes = [],
        treeView = $("#treeviewnew").data("kendoTreeView"),
        message;

    checkedNodeIds(treeView.dataSource.view(), checkedNodes);

    if (checkedNodes.length > 0) {
        message = "IDs of checked nodes: " + checkedNodes.join(",");
        selGroupId = Number(checkedNodes[0]);
    } else {
        message = "No nodes checked.";
    }

}

function splitGroup(group,platform){

    if (group && group !== "") {
        var groupArr = group.split('~');
        if (groupArr && groupArr.length > 0) {
            platform.platformId = getPlatformId(groupArr[0]);
            platform.platformName = groupArr[0];
            if (groupArr.length > 1) {
                platform.userTypeId = getUserTypeId(groupArr[1]);
                platform.userTypeName = groupArr[1];
            }
            if (groupArr.length > 2) {
                platform.permission = groupArr[2];
            }

        }
    }
}

function getPlatformId(name){
    if (name && name !== "") {
        if (platforms !== null && platforms.length > 0) {
            var tmpArr = $.grep(platforms,function(e){
                return e.name.toLowerCase() === name.toLowerCase();
            });

            if (tmpArr !== null && tmpArr.length > 0) {
                return tmpArr[0].idk;
            }
        }
    }
}

function getUserTypeId(name){
    if (name && name !== "") {
        if (userTypeArr !== null && userTypeArr.length > 0) {
            var tmpArr = $.grep(userTypeArr,function(e){
                return e.name.toLowerCase() === name.toLowerCase();
            });

            if (tmpArr !== null && tmpArr.length > 0) {
                return tmpArr[0].idk;
            }
        }
    }
}



function getParentChildItems(id){
    var uArray = [];
    var dtArray = _.where(groupEntities, {entityId: id})
    for (var i = 0; i < dtArray.length; i++) {
        var pItem = {};
        pItem.text = getGroupName(dtArray[i].groupId, 1);
        pItem.items = getParentChildItems1(dtArray[i].id);
        pItem.entityId = dtArray[i].entityId;
        pItem.idk = dtArray[i].groupId;
        pItem.spriteCssClass = "rootfolder txtBlue";
        pItem.type = "Group";
        uArray.push(pItem);
    }


    return uArray;
}

function getParentChildItems1(id){
    var uArray = [];
    var dtArray = _.where(groupEntities, {entityId: id})
    for (var i = 0; i < dtArray.length; i++) {
        var pItem = {};
        pItem.text = getGroupName(dtArray[i].groupId, 2);
        pItem.items = getParentChildItems2(dtArray[i].id);
        pItem.entityId = dtArray[i].entityId;
        pItem.idk = dtArray[i].groupId;
        pItem.spriteCssClass = "rootfolder txtBlue";
        pItem.type = "Group";
        uArray.push(pItem);
    }



    return uArray;
}

function getParentChildItems2(id){
    var uArray = [];
    var dtArray = _.where(groupEntities, {entityId: id})
    for (var i = 0; i < dtArray.length; i++) {
        var pItem = {};
        pItem.text = getGroupName(dtArray[i].groupId, 3);
        /* pItem.items = getParentChildItems2(dtArray[i].id);*/
        pItem.entityId = dtArray[i].entityId;
        pItem.idk = dtArray[i].groupId;
        pItem.spriteCssClass = "rootfolder txtBlue";
        pItem.type = "Group";

    }
    uArray.push(pItem);


    return uArray;
}
function onTreeItemSelect(e) {
    var selItem = getSelectTreeNode("treeview");
    onClickUserEntity();

}

function onClickView(){
    var selItem = getSelectTreeNode("treeview");
    var entityId;
    var groupId;
    var apiURL;
    var userIds;
    userIds = $('#cmbUsers1').multipleSelect('getSelects');
    if(selItem && selItem.type == "Entity") {
        entityId = selItem.idk;
        apiURL = ipAddress + "/homecare/security/user-group-entities/?user-id="+userIds.join(',')+"&entity-id="+entityId;
    }

    if(selItem && selItem.type == "Group") {
        groupId = selItem.idk;
        entityId = selItem.entityId;
        apiURL = ipAddress + "/homecare/security/user-group-entities/?user-id="+userIds.join(',')+"&entity-id="+entityId+"&group-id="+groupId;
    }
    onClickSecurityMaster(5,apiURL);
    /*  getAjaxObject(apiURL, "GET", userGroupEntities, onError);*/
}

function userGroupEntities(dataObj){
    var dtArray = [];
    /*    buildDeviceListGrid([],gridColumns);*/
    if(dataObj.response.userGroupEntities){
        if($.isArray(dataObj.response.userGroupEntities)){
            dtArray = dataObj.response.userGroupEntities;
        }else{
            dtArray.push(dataObj.response.userGroupEntities);
        }
    }
    if (dtArray !== null && dtArray.length > 0) {
        $("#dvGrid").show();
        for (var i = 0; i < dtArray.length; i++) {
            dtArray[i].idk = dtArray[i].id;
        }
    }

}

function getSelectTreeNode(treeId){
    var tv = $('#'+treeId).data('kendoTreeView');
    if(tv){
        var selTreeItem = tv.dataItem(tv.select());
        if(selTreeItem){
            return selTreeItem;
        }
    }
    return null;
}

function getUserTypes(){
    getAjaxObject(ipAddress+"/homecare/user-types/?is-active=1&is-deleted=0","GET",onGetUserTypes,onError);
}
var userTypeArray = [];
function onGetUserTypes(dataObj){
    $("#cmbUserType").empty();
    $("#cmbTenantUserType").empty();
    userTypeArray = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1" && dataObj.response.userTypes){
        if($.isArray(dataObj.response.userTypes)){
            userTypeArray = dataObj.response.userTypes;
        }else{
            userTypeArray.push(dataObj.response.userTypes);
        }
    }
    for(var i=0;i<userTypeArray.length;i++) {
        if(userTypeArray[i].id != 1000) {
            userTypeArray[i].value = userTypeArray[i].value.toUpperCase();
            if (userTypeArray[i].value == "PATIENT") {
                userTypeArray[i].value = "SERVICE USER";
            }
            if (userTypeArray[i].value == "DOCTOR") {
                userTypeArray[i].value = "STAFF";
            }
            $("#cmbUserType").append('<option value="' + userTypeArray[i].id + '">' + userTypeArray[i].value + '</option>');
            $("#cmbTenantUserType").append('<option value="' + userTypeArray[i].id + '">' + userTypeArray[i].value + '</option>');
        }

    }
    /*setDataForSelection(userTypeArray, "txtGroup", function(){}, ["value", "id"], 0, "");*/

}

function onGetUserDetails(id){
    var usersList = _.where(userList, {userTypeId: Number(id)})
    for (var i = 0; i < usersList.length; i++) {
        var Name = usersList[i].lastName + " " + usersList[i].firstName;
        $("#cmbUserName").append('<option value="' + usersList[i].id + '">' + Name + '</option>');

    }

}


function onClickUserEntity() {
    var popW = 600;
    var popH = 500;
    parentRef.userList = userList;
    parentRef.userId = $("#cmbUserName").val();
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Users Entity";
    devModelWindowWrapper.openPageWindow("../../html/masters/securityPermissions.html", profileLbl, popW, popH, true, onClose);
}

function onClose(evt, returnData){

}

function closePopup(id) {
    $('#' + id).hide();
    $("#divEntityPermission").hide();
    $("#divAddUserToGroup").hide();
    $("#linkGroupUserPopup").hide();
    $("#tblEntityPermission tbody tr").remove();


    $("#cmbUserList").empty();
    $("#divUserList").hide();
    $("#cmbUserList").empty();
    $("#cmbTenantUserType").val(0);

}


function sortByUsername (a, b) {

    var usernameA = a.username.toUpperCase();
    var usernameB = b.username.toUpperCase();

    var comparison = 0;
    if (usernameA > usernameB) {
        comparison = 1;
    } else if (usernameA < usernameB) {
        comparison = -1;
    }
    return comparison;

}


function onDeleteUser(groupId, userId) {

    IsDeleteUser = 1;
    var msg = "Are you sure want to delete the user ?"
    displaySessionErrorPopUp("Info", msg, function (res) {
        // var sourceUserId = parseInt($("#ulGroupUser li").attr("idk"));
        var sourceMasterId, sourceGroupId;

        if (userGroupMaster !== null && userGroupMaster.length > 0) {
            var existingGroupUser = $.grep(userGroupMaster, function (e) {
                return e.userId === userId && e.isActive === 1 && e.isDeleted === 0;
            });

            if (existingGroupUser !== null && existingGroupUser.length > 0) {
                sourceGroupId = existingGroupUser[0].groupId;
                sourceMasterId = existingGroupUser[0].id;
            }


            var reqData = {};
            reqData.groupId = Number(groupId);
            reqData.userId = Number(userId);
            reqData.modifiedBy = Number(sessionStorage.getItem("userId"));
            reqData.id = sourceMasterId;
            reqData.isActive = 0;
            reqData.isDeleted = 1;

            createAjaxObject(ipAddress + "/homecare/security/user-groups/", reqData, "PUT", onDeleteUser3, onError);


        }

    });


}

function onDeleteUser3(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1") {
            var msg = "User removed from group successfully";
            displaySessionErrorPopUp("Info", msg, function (res) {
                getUserGroups();
                $("#divEntityPermission").hide();
                $("#divAddUserToGroup").hide();
                $("#linkGroupUserPopup").hide();
                $("#tblEntityPermission tbody tr").remove();


                $("#cmbUserList").empty();
            })

        }
    }
}
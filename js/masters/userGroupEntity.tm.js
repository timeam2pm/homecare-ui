var parentRef = null;
var userList = [];

$(document).ready(function(){
    /*sessionStorage.setItem("IsSearchPanel", "1");
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("dgridZipList", dataOptions);
    angularUIgridWrapper.init();
    buildZipListGrid([]);*/
});


$(window).load(function(){
    loading = false;
    if(parent.frames['iframe'] != undefined)
        parentRef = parent.frames['iframe'].window;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
    init();
    buttonEvents();
}

function init(){
    getAjaxObject(ipAddress+"/city/by-status/"+isActive,"GET",getZipList,onError);
}
function onError(errorObj){
    console.log(errorObj);
}
function getZipList(dataObj){
    console.log(dataObj);
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.cityExt){
        if($.isArray(dataObj.response.cityExt)){
            dataArray = dataObj.response.cityExt;
        }else{
            dataArray.push(dataObj.response.cityExt);
        }
    }

    dataArray.sort(function(a, b) {
        var nameA = a.city.toUpperCase(); // ignore upper and lowercase
        var nameB = b.city.toUpperCase(); // ignore upper and lowercase
        if (nameA < nameB) {
            return -1;
        }
        if (nameA > nameB) {
            return 1;
        }
        // names must be equal
        return 0;
    })



    var tempDataArry = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
        }
    }
    buildZipListGrid(dataArray);
}
function buttonEvents(){

    $("#btnSave").off("click",onClickOK);
    $("#btnSave").on("click",onClickOK);

    $("#btnCancelDet").off("click",onClickCancel);
    $("#btnCancelDet").on("click",onClickCancel);

    $("#btnDelete").off("click",onClickDelete);
    $("#btnDelete").on("click",onClickDelete);

    $("#btnAdd").off("click");
    $("#btnAdd").on("click",onClickAdd);

    $("#btnActive").off("click");
    $("#btnActive").on("click",onClickActive);

    $("#btnInActive").off("click");
    $("#btnInActive").on("click",onClickInActive);
}

function adjustHeight(){
    var defHeight = 220;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    angularUIgridWrapper.adjustGridHeight(cmpHeight);
}

var isActive = 1;
function onClickActive(){
    isActive = 1;
    $("#btnInActive").removeClass("selectButtonBarClass");
    $("#btnActive").addClass("selectButtonBarClass");
    init();
}
function onClickInActive(){
    isActive = 0;
    $("#btnActive").removeClass("selectButtonBarClass");
    $("#btnInActive").addClass("selectButtonBarClass");
    init();
}
function buildZipListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    var cntry = sessionStorage.countryName;
    var strPC = "";
    var strState= "";
    if(cntry.indexOf("India")>=0){
        strPC = "Postal Code";
        strState = "State";
    }else if(cntry.indexOf("United Kingdom")>=0){
        strPC = "Postal Code";
        strState = "County";
    }else if(cntry.indexOf("United State")>=0){
        strPC = "Zip";
        strState = "State";
    }else{
        strPC = "Zip";
        strState = "State";
    }
    gridColumns.push({
        "title": "City",
        "field": "city",
    });
    if(cntry.indexOf("United Kingdom")>=0) {
        if(!IsPostalCodeManual){
            gridColumns.push({
                "title": ""+strPC+"",
                "field": "zip",
            });
        }
    }
    else{
        gridColumns.push({
            "title": ""+strPC+"",
            "field": "zip",
        });
    }

    /* gridColumns.push({
         "title": "Zip4",
         "field": "zipFour",
     });*/
    gridColumns.push({
        "title": ""+strState+"",
        "field": "state",
    });
    gridColumns.push({
        "title": "Country",
        "field": "country",
    });
    // gridColumns.push({
    //    "title": "Status",
    //    "field": "Status",
    // });
    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}
var prevSelectedItem =[];
function onChange(){
    setTimeout(function(){
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if(selectedItems && selectedItems.length>0){
            $("#btnSave").prop("disabled", false);
            $("#btnDelete").prop("disabled", false);
        }else{
            $("#btnSave").prop("disabled", true);
            $("#btnDelete").prop("disabled", true);
        }
    });
}

function onClickOK(){
    setTimeout(function(){
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if(selectedItems && selectedItems.length>0){
            var obj = {};
            obj.selItem = selectedItems[0];
            if($("#btnSave").text() ==  "OK"){
                obj.status = "success";
                var windowWrapper = new kendoWindowWrapper();
                windowWrapper.closePageWindow(obj);
            }else{
                operation = "update";
                parentRef.operation = operation;
                parentRef.selItem = selectedItems[0];
                addZip();
            }

        }
    })
}
function onClickDelete(){
    customAlert.confirm("Confirm", "Are you sure you want delete?",function(response){
        if(response.button == "Yes"){
            var selectedItems = angularUIgridWrapper.getSelectedRows();
            console.log(selectedItems);
            if(selectedItems && selectedItems.length>0){
                var selItem = selectedItems[0];
                if(selItem){
                    var dataUrl = ipAddress+"/city/delete/";
                    var reqObj = {};
                    reqObj.id = selItem.idk;
                    reqObj.isDeleted = "0";
                    reqObj.isActive = "0";
                    reqObj.modifiedBy = sessionStorage.userId;//"101";
                    createAjaxObject(dataUrl,reqObj,"POST",onDeleteCountryt,onError);
                }
            }
        }
    });
}
function onDeleteCountryt(dataObj){
    if(dataObj && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            customAlert.info("info", "Deleted Successfully");
            init();
        }else{
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}
function onClickAdd(){

    operation = "add";
    parentRef.operation = operation;
    parentRef.selItem = null;
    addZip();

}
var operation = "";

function addZip(){
    var popW = 600;
    var popH = 500;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    if(operation == "add"){
        profileLbl = "Add Zip";
    }else{
        profileLbl = "Edit Zip";
    }
    var cntry = sessionStorage.countryName;
    if(cntry.indexOf("India")>=0){
        if(operation == "add"){
            profileLbl = "Add Postal Code";
        }else{
            profileLbl = "Edit Postal Code";
        }
    }else if(cntry.indexOf("United Kingdom")>=0){
        if(IsPostalCodeManual != "1"){
            if(operation == "add"){
                profileLbl = "Add Postal Code";
            }else{
                profileLbl = "Edit Postal Code";
            }
        }
        else{
            if(operation == "add"){
                profileLbl = "Add City";
            }else{
                profileLbl = "Edit City";
            }
        }

    }else if(cntry.indexOf("United State")>=0){
        if(operation == "add"){
            profileLbl = "Add Zip";
        }else{
            profileLbl = "Edit Zip";
        }
    }else{
        if(operation == "add"){
            profileLbl = "Add Zip";
        }else{
            profileLbl = "Edit Zip";
        }
    }
    devModelWindowWrapper.openPageWindow("../../html/masters/createZip.html", profileLbl, popW, popH, true, closeAddZipAction);
}
function closeAddZipAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        if(returnData.operation == "add"){
            customAlert.info("Info","Created Successfully.")
        }else if(returnData.operation == "update"){
            customAlert.info("Info","Updated Successfully.")
        }
    }
    init();
}
function onClickCancel(){
    var obj = {};
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}


function getUserList(dataObj) {
    for (var i = 0; i < userList.length; i++) {
        var staffName = userList[i].lastName + " " + userList[i].firstName;
        $("#cmbUsers").append('<option value="' + userList[i].id + '">' + staffName + '</option>');

    }
}
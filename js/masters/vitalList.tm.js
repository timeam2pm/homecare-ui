var angularUIgridWrapper;
var parentRef = null;
var recordType = "1";
var dataArray = [];
var statusArr = [{Key:'Active',Value:'Active'},{Key:'InActive',Value:'InActive'}];

var ADD = "add";
var VIEW = "view";
var DELETE = "delete";
var patientId = "";

$(document).ready(function(){
    $("#pnlPatient",parent.document).css("display","none");
    themeAPIChange();
    sessionStorage.setItem("IsSearchPanel", "1");
    parentRef = parent.frames['iframe'].window;
    //recordType = parentRef.dietType;
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("dgridVitalList", dataOptions);
    angularUIgridWrapper.init();
    buildDeviceListGrid([]);
});

$(window).load(function(){
    loading = false;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
    init();
    buttonEvents();
    adjustHeight();
}

function onStatusChange(){
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if(cmbStatus && cmbStatus.selectedIndex<0){
        cmbStatus.select(0);
    }
}
function init(){
    $("#btnEdit").prop("disabled", true);
    $("#btnDelete").prop("disabled", true);

    allowNumerics("txtRF");
    allowNumerics("txtRT");

    allowNumerics("txtMRF");
    allowNumerics("txtMRT");

    allowNumerics("txtFRF");
    allowNumerics("txtFRT");

    allowNumerics("txtPRF");
    allowNumerics("txtPRT");

    $("#cmbUnit").kendoComboBox();
    // $("#cmbStatus").kendoComboBox();

    // setDataForSelection(statusArr, "cmbStatus", onStatusChange, ["Value", "Key"], 0, "");
    getAjaxObject(ipAddress + "/master/vital_units/list/", "GET", getVitalListUnits, onError);

    buildDeviceListGrid([]);
    //getAjaxObject(ipAddress+"/file-resource/list/"+recordType,"GET",getFileResourceList,onError);
    getActiveVitals();
}
function getActiveVitals(){
    buildDeviceListGrid([]);
    $(".btnInActive").removeClass("radioButton-active");
    $(".btnActive").addClass("radioButton-active");
    getAjaxObject(ipAddress+"/homecare/vitals/?isActive=1","GET",getActivityTypes,onError);
}
function getInActiveVitals(){
    buildDeviceListGrid([]);
    $(".btnActive").removeClass("radioButton-active");
    $(".btnInActive").addClass("radioButton-active");
    getAjaxObject(ipAddress+"/homecare/vitals/?isActive=0","GET",getActivityTypes,onError);
}
function getActivityTypes(dataObj){
    console.log(dataObj);
    var tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.vitals){
            if($.isArray(dataObj.response.vitals)){
                tempCompType = dataObj.response.vitals;
            }else{
                tempCompType.push(dataObj.response.vitals);
            }
        }
    }
    for(var i=0;i<tempCompType.length;i++){
        tempCompType[i].idk = tempCompType[i].id;
        tempCompType[i].DIO = tempCompType[i].displayOrder;
        tempCompType[i].DIO1 = tempCompType[i].displayOrder;
        tempCompType[i].ABR = tempCompType[i].abbreviation;
        tempCompType[i].DESC = tempCompType[i].description;
        tempCompType[i].RF = tempCompType[i].rangeFrom;
        tempCompType[i].RT = tempCompType[i].rangeTo;
        if(tempCompType[i].isActive == 1){
            tempCompType[i].ST = "Active";
        }else{
            tempCompType[i].ST = "InActive";
        }
        if(tempCompType[i].iconFileExtension){
            tempCompType[i].photo = ipAddress+"/homecare/download/vitals/photo/?id="+tempCompType[i].id+"&access_token="+sessionStorage.access_token+"&tenant="+sessionStorage.tenant+"&icon-file-extension=png&"+Math.round(Math.random()*1000000);
        }
    }
    buildDeviceListGrid(tempCompType);
}
function onError(errorObj){
    console.log(errorObj);
}
function getFileResourceList(dataObj){
    console.log(dataObj);
    dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.fileResource){
        if($.isArray(dataObj.response.fileResource)){
            dataArray = dataObj.response.fileResource;
        }else{
            dataArray.push(dataObj.response.fileResource);
        }
    }
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        if(!dataArray[i].fileType){
            dataArray[i].fileType = "";
        }
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
        }
    }
    //getDietList();
    buildDeviceListGrid([]);
    setTimeout(function(){
        buildDeviceListGrid(dataArray);
    })
}

function getDietList(){
    var tempArray = [];
    for(var i=0;i<dataArray.length;i++){
        if(dataArray[i].recordType == recordType){
            tempArray.push(dataArray[i]);
        }
    }
    buildDeviceListGrid(tempArray);
}
function buttonEvents(){
    $("#btnSave").off("click",onClickSave);
    $("#btnSave").on("click",onClickSave);

    $("#btnDisOrder").off("click",onClickSaveDO);
    $("#btnDisOrder").on("click",onClickSaveDO);

    $("#btnCancel").off("click",onClickCancel);
    $("#btnCancel").on("click",onClickCancel);

    $("#btnEdit").off("click",onClickEdit);
    $("#btnEdit").on("click",onClickEdit);

    $("#btnReset").off("click",onClickReset);
    $("#btnReset").on("click",onClickReset);

    $("#btnDelete").off("click",onClickDelete);
    $("#btnDelete").on("click",onClickDelete);

    $("#btnAdd").off("click");
    $("#btnAdd").on("click",onClickAdd);

    $("#btnBrowse").off("click", onClickBrowse);
    $("#btnBrowse").on("click", onClickBrowse);

    $("#fileElem").off("change", onSelectionFiles);
    $("#fileElem").on("change", onSelectionFiles);

    $("#btnUpload").off("click", onClickUploadPhoto);
    $("#btnUpload").on("click", onClickUploadPhoto);

    //$("#btnDisOrder").off("click", onClickDisplayOrder);
    //$("#btnDisOrder").on("click",  onClickDisplayOrder);

    $(".btnActive").off("click", getActiveVitals);
    $(".btnActive").on("click", getActiveVitals);

    $(".btnInActive").off("click", getInActiveVitals);
    $(".btnInActive").on("click", getInActiveVitals);

    $(".popupClose").off("click", onClickCancel);
    $(".popupClose").on("click", onClickCancel);

}

function onClickBrowse(e) {
    if (e.currentTarget && e.currentTarget.id) {
        var btnId = e.currentTarget.id;
        var fileTagId = ("fileElem" + btnId);
        $("#fileElem").click();
    }
}
function onSelectionFiles(event) {
    console.log(event);
    files = event.target.files;
    fileName = "";
    //$('#txtFU').val("");
    if (files) {
        if (files.length > 0) {
            fileName = files[0].name;
            // if (patientId != "") {
            var oFReader = new FileReader();
            oFReader.readAsDataURL(files[0]);
            oFReader.onload = function(oFREvent) {
                document.getElementById("imgPhoto").src = oFREvent.target.result;
                /* if (imageCompressor) {
                     imageCompressor.run(oFREvent.target.result, compressorSettings, function(small){
                         document.getElementById("imgPhoto").src = small;
                         //isBrowseFlag = false;
                         imagePhotoData = small;
                         return small;
                     });
                 } */

            }
            //}
        }
    }
}
function onClickUploadPhoto(){
    if(operation == UPDATE){
        var reqUrl = ipAddress + "/homecare/upload/vitals/photo/?id=" + atID + "&icon-file-extension=png&access_token=" + sessionStorage.access_token+"&tenant="+sessionStorage.tenant;
        var formData = new FormData();
        formData.append("desc", fileName);
        formData.append("file", files[0]);
        formData.append("icon-file-extension", "png");
        formData.append("id", atID);

        Loader.showLoader();
        $.ajax({
            url: reqUrl,
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false, // "multipart/form-data",
            success: function(data, textStatus, jqXHR) {
                if (typeof data.error === 'undefined') {
                    Loader.hideLoader();
                    submitForm(data);
                } else {
                    Loader.hideLoader();
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                Loader.hideLoader();
            }
        });

    }
}

function submitForm(dataObj) {
    //console.log(dataObj);
    customAlert.error("Info", "Image uploaded successfully");
    init();
    onClickActive();
}
var allRosterRowCount = 0;
var allRosterRowIndex = 0;
var allRosterRows = [];
var allRosterRowCount = 0;
function onClickSaveDO(){
    allRosterRowCount = 0;
    allRosterRowIndex = 0;
    allRosterRows = angularUIgridWrapper.getAllRows();
    allRosterRowCount = allRosterRows.length;
    if(allRosterRowCount>0){
        createDisplayOrder();
    }
    /*var rows = angularUIgridWrapper.getScope().gridApi.core.getVisibleRows();
    var row = rows[0].entity;
    var dataObj = {};
    dataObj.createdBy = Number(sessionStorage.userId);;
    dataObj.isDeleted = 0;
    dataObj.isActive = 1;
    dataObj.type = row.type;
    dataObj.displayOrder = row.DIO;
    var dataUrl = ipAddress +"/homecare/activity-types/";
        method = "PUT";
         dataObj.id = row.idk;
    createAjaxObject(dataUrl, dataObj, method, onCreate, onError);*/
}
function createDisplayOrder(){
    if(allRosterRowCount>allRosterRowIndex){
        var objRosterEntiry = allRosterRows[allRosterRowIndex];
        var rosterObj = objRosterEntiry.entity;
        var dataObj = rosterObj;
        /* dataObj.createdBy = Number(sessionStorage.userId);;
         dataObj.isDeleted = 0;
         dataObj.isActive = 1;
         dataObj.type = rosterObj.type; */
        //dataObj.id = dataObj.idk;
        //dataObj.displayOrder = rosterObj.DIO;

        var dataObj = {};
        dataObj.createdBy = Number(sessionStorage.userId);;
        dataObj.isDeleted =0;
        dataObj.isActive = rosterObj.isActive;
        dataObj.unitOfMeasurement = rosterObj.unitOfMeasurement;
        dataObj.femaleRangeFrom = rosterObj.femaleRangeFrom;;
        dataObj.femaleRangeTo = rosterObj.femaleRangeTo;;
        dataObj.maleRangeFrom = rosterObj.maleRangeFrom;;
        dataObj.maleRangeTo = rosterObj.maleRangeTo;;
        dataObj.pediatricRangeFrom = rosterObj.pediatricRangeFrom;;
        dataObj.pediatricRangeTo = rosterObj.pediatricRangeTo;;
        dataObj.displayOrder =  rosterObj.DIO;
        dataObj.description = rosterObj.description;;
        dataObj.abbreviation = rosterObj.abbreviation;;
        dataObj.id = rosterObj.idk;
        dataObj.iconFileExtension = rosterObj.iconFileExtension;

        var dataUrl = ipAddress +"/homecare/vitals/";
        method = "PUT";
        //	 dataObj.id = rosterObj.idk;
        createAjaxObject(dataUrl, dataObj, method, onCreateDS, onError);
    }
}
function onCreateDS(dataObj){
    allRosterRowIndex = allRosterRowIndex+1;
    if(allRosterRowCount == allRosterRowIndex){
        customAlert.error("Info", "Display order updated successfully");
        init();
    }else{
        createDisplayOrder();
    }
}
function onClickDisplayOrder(){
    var rows = angularUIgridWrapper.getScope().gridApi.core.getVisibleRows();
    var arr = [];
    for(var r=0;r<rows.length;r++){
        var rowItem = rows[r];
        if(rowItem){
            var dataItem = rowItem.entity;
            var dataObj = {};
            dataObj = dataItem;
            dataObj.id = dataItem.idk;
            dataObj.displayOrder = dataItem.DIO;
            arr.push(dataObj);
        }
    }
    if(arr.length>0){
        dataUrl = ipAddress +"/homecare/vital/batch";
        var method = "PUT";
        createAjaxObject(dataUrl, arr, method, onBatchCreate, onError);
    }
    /* var dataObj = {};
        dataObj.createdBy = Number(sessionStorage.userId);;
        dataObj.isDeleted =0;
        dataObj.isActive = 1;
        dataObj.unitOfMeasurement = cmbUnit.text();
        dataObj.femaleRangeFrom = txtFRF;
        dataObj.femaleRangeTo = txtFRT;
        dataObj.maleRangeFrom = txtMRF;
        dataObj.maleRangeTo = txtMRT;
        dataObj.pediatricRangeFrom = txtPRF;
        dataObj.pediatricRangeTo = txtPRT;
        dataObj.displayOrder = 1;
        dataObj.description = txtDesc;
        dataObj.abbreviation = txtABR;
        var dataUrl = ipAddress +"/homecare/vitals/";
        var method = "POST";
        dataObj.id = parentRef.updateItem.idk;
        dataUrl = ipAddress +"/homecare/vitals/";
}//iconFileExtension
createAjaxObject(dataUrl, dataObj, method, onCreate, onError);*/
}
function onBatchCreate(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "Display Order are updated successfully";
            displaySessionErrorPopUp("Info", msg, function(res) {
                //onClickCancel();
            })
        }else{
            customAlert.error("Error", dataObj.response.status.message);
        }
    }
}
var  profileLbl = "Create Vital";
function onClickAdd(){
    $("#addPopup").show();
    $('#viewDivBlock').hide();
    $("#txtVID").hide();
    $(".tabContentTitle").html("Add Vital");
    onClickReset();
    document.getElementById("imgPhoto").src = "";
    operation = ADD;
    parentRef.updateItem = null;
    // createVitals();
}
function createVitals(){
    var popW = "65%";
    var popH = "58%";
    parentRef.operation = operation;
    var devModelWindowWrapper = new kendoWindowWrapper();
    devModelWindowWrapper.openPageWindow("../../html/masters/createVital.html", profileLbl, popW, popH, true, closeAddCreateTableAction);
}
function closeAddCreateTableAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        var operation = returnData.operation;
        if(operation == "add"){
            customAlert.info("Info", "Vitals created successfully");
        }
    }
    init();
}
function onClickDelete(){
    customAlert.confirm("Confirm", "Are you sure to delete?",function(response){
        if(response.button == "Yes"){
            var selectedItems = angularUIgridWrapper.getSelectedRows();
            if(selectedItems.length>0){
                var dataObj = {};
                dataObj.createdBy = Number(sessionStorage.userId);;
                dataObj.isDeleted = 1;
                dataObj.isActive = 0;
                dataObj.id = selectedItems[0].idk;
                var dataUrl = ipAddress +"/homecare/vitals/";
                var method = "DELETE";
                createAjaxObject(dataUrl, dataObj, method, onCreateDelete, onError);
            }
        }
    });

}
function onCreateDelete(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "Vital is  deleted successfully";
            customAlert.error("Info", msg);
            onReset();
            operation = ADD;
            init();
        }
    }
}
function onClickReset(){
    onReset();
}
function onReset(){
    $("#txtRF").val("");
    $("#txtRT").val("");
    $("#txtABR").val("");
    $("#txtDesc").val("");
    document.getElementById("imgPhoto").src = "";
    $("#cmbStatus").val(1);
    isPhotoExt = "";
    operation = ADD;
}
// function onClickSave(){
// 	var txtRF = $("#txtRF").val();
// 	txtRF = $.trim(txtRF);
//
// 	var txtRT = $("#txtRT").val();
// 	txtRT = $.trim(txtRT);
//
// 	var txtABR = $("#txtABR").val();
// 	txtABR = $.trim(txtABR);
//
// 	var txtDesc = $("#txtDesc").val();
// 	txtDesc = $.trim(txtDesc);
//
// 	txtRT = Number(txtRT);
// 	txtRF = Number(txtRF);
// 	if(txtRF != "" && txtRT != ""){
// 		if(txtRT>=txtRF){
// 			var dataObj = {};
// 		    dataObj.createdBy = Number(sessionStorage.userId);;
// 		    dataObj.isDeleted = 0;
// 		    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
// 		    if(cmbStatus.selectedIndex == 0){
// 		    	 dataObj.isActive = 1;
// 		    }else{
// 		    	 dataObj.isActive = 0;
// 		    }
//
// 		    dataObj.rangeTo = txtRT;
// 		    dataObj.rangeFrom = txtRF;
// 		    dataObj.description = txtDesc;
// 		    dataObj.abbreviation = txtABR;
// 		    var dataUrl = ipAddress +"/homecare/vitals/";
// 		    var method = "POST";
// 		    if(operation == UPDATE){
// 		    	method = "PUT";
// 		    	 dataObj.id = atID;
// 		    	 if(isPhotoExt){
// 		    		 dataObj.iconFileExtension = isPhotoExt;
// 		    	 }
// 		    	 dataUrl = ipAddress +"/homecare/vitals/";
// 		    }
// 		    createAjaxObject(dataUrl, dataObj, method, onCreate, onError);
// 		}else{
// 		    	var msg = "Range To is always greater than Range From";
// 				customAlert.error("Error", msg);
// 		}
// 	}
// }
// function onCreate(dataObj){
// 	console.log(dataObj);
// 	if(dataObj && dataObj.response && dataObj.response.status ){
// 		if(dataObj.response.status.code == "1"){
// 			var msg = "Vitals are created successfully";
// 			if(operation == UPDATE){
// 				msg = "Vitals are  updated successfully"
// 			}else{
//
// 			}
// 			displaySessionErrorPopUp("Info", msg, function(res) {
// 				onReset();
// 				operation = ADD;
// 				init();
// 	        })
// 		}else{
// 			customAlert.error("Error", dataObj.response.status.message);
// 		}
// 	}else{
//
// 	}
// }
function onClickDiet(){
    onDiet();
    recordType = "1";
    init();
}
function onClickExcersize(){
    onExcersize();
    recordType = "2";
    init();
}
function onClickillness(){
    onIllness();
    recordType = "5";
    init();
}
function onDiet(){
    $("#btnDiet").addClass("selectButtonBarClass");
    $("#btnExcersize").removeClass("selectButtonBarClass");
    $("#btnillness").removeClass("selectButtonBarClass");
}
function onExcersize(){
    $("#btnDiet").removeClass("selectButtonBarClass");
    $("#btnExcersize").addClass("selectButtonBarClass");
    $("#btnillness").removeClass("selectButtonBarClass");
}
function onIllness(){
    $("#btnDiet").removeClass("selectButtonBarClass");
    $("#btnExcersize").removeClass("selectButtonBarClass");
    $("#btnillness").addClass("selectButtonBarClass");
}
function adjustHeight(){
    var defHeight = 200;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    angularUIgridWrapper.adjustGridHeight(cmpHeight);
}

function buildDeviceListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Image",
        "field": "PID",
        "enableColumnMenu": false,
        "width": "8%",
        "cellTemplate":showPtImage()
    });

    gridColumns.push({
        "title": "Abbreviation",
        "field": "ABR",
        "width": "10%",
    });
    gridColumns.push({
        "title": "Description",
        "field": "DESC",
        "width": "10%"
    });
    gridColumns.push({
        "title": "Unit",
        "field": "unitOfMeasurement",
        "width": "6%",
    });
    gridColumns.push({
        "title": "Male From",
        "field": "maleRangeFrom",
        "width": "9%",
    });
    gridColumns.push({
        "title": "Male To",
        "field": "maleRangeTo",
        "width": "9%",
    });
    gridColumns.push({
        "title": "Female From",
        "field": "femaleRangeFrom",
        "width": "9%",
    });
    gridColumns.push({
        "title": "Female To",
        "field": "femaleRangeTo",
        "width": "10%",
    });
    gridColumns.push({
        "title": "Child From",
        "field": "pediatricRangeFrom",
        "width": "10%",
    });
    gridColumns.push({
        "title": "Child To",
        "field": "pediatricRangeTo",
        "width": "10%",
    });
    gridColumns.push({
        "title": "Display Order",
        "field": "DIO",
        "width": "9%",
        "cellTemplate":showDefaultValue()
    });

    /*  gridColumns.push({
          "title": "Action",
          "cellTemplate":showAction()
      });*/
    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}
function showDefaultValue(){
    var node = '<div>';
    node += '<input  type="number" id="txtI2Val"    ng-model="row.entity.DIO" value="{{row.entity.DIO}}"     class="txtField" min="1"  max="1000" maxlength="5" onchange="maxLengthCheck(event)" style="height:25px;width:100px"></input>';
    node += '</div>';
    return node;
}
function showPtImage(){
    var node = '<div><img src="{{row.entity.photo}}" onerror="onImgError(event)" style="width:50px"><spn>';//this.src=\'../../img/AppImg/HosImages/patient1.png\'
    node = node+"</div>";
    return node;
}
var selRow = null;
function maxLengthCheck(e){
    console.log(e);
    selRow = angular.element($(e.currentTarget).parent()).scope();
    setTimeout(function(){
        upDateDataSource();
    },100)
}
function onImgError(e){
    //console.log(e);
}
/*function showDefaultValue(){
	var node = '<div>';
	node += '<input  type="number" id="txtI2Val"    ng-model="row.entity.DIO" value="{{row.entity.DIO}}"     class="txtField" min="1"  max="1000" maxlength="5" onchange="maxLengthCheck(event)" style="height:25px;width:100px"></input>';
	node += '</div>';
	return node;
}*/
function showAction(){
    var node = '<div>';
    node += '<input  type="button" id="btn" value="Action" onclick="onClickAction()"></input>';
    node += '</div>';
    return node;
}
var selRow = null;
function onClickAction(){

}
function maxLengthCheck(e){
    console.log(e);
    selRow = angular.element($(e.currentTarget).parent()).scope();
    setTimeout(function(){
        upDateDataSource();
    },100)
}

//begin = selRow.row.entity.DIS;
//end = selRow.row.entity.DIS1;
function upDateDataSource(){
    var selectedItems = angularUIgridWrapper.getSelectedRows();
    console.log(selectedItems);
    var selRowIndex = 0;
    if(selRow){
        var selItem = selRow.row.entity;//selectedItems[0];
        selRowIndex = selItem.idk;
        var idVal = selItem.DIO;
        idVal = Number(idVal);

        var begin = selItem.DIO;
        begin = Number(begin);

        var end = selItem.DIO1;
        end = Number(end);

        //selItem.DIO1 = end;
        //angularUIgridWrapper.refreshGrid();
        var rows = angularUIgridWrapper.getScope().gridApi.core.getVisibleRows();
        console.log(rows);

        for(var i=0;i<rows.length;i++){
            var rowItem = rows[i].entity;
            if(rowItem.id == selRow.row.entity.id){
                continue;
            }
            var rowValue = rowItem.DIO;
            if(end>begin){
                flag = true;
                if(rowValue>=begin && rowValue<end){
                    var rValue = (rowItem.DIO+1);
                    rowItem.DIO = rValue;
                    rowItem.DIO1 = rValue;
                }
            }else{
                flag = false;
                if(rowValue>end){
                    var rValue = (rowItem.DIO+1);
                    rowItem.DIO = rValue;
                    rowItem.DIO1 = rValue;
                }
            }
        }
        if(flag){
            selRow.row.entity.DIS1 = begin;
        }else{
            selRow.row.entity.DIS1 = begin;
        }
        angularUIgridWrapper.refreshGrid();
    }

}
var prevSelectedItem =[];
function onChange(){
    setTimeout(function() {
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if (selectedItems && selectedItems.length > 0) {
            var obj = selectedItems[0];
            atID = obj.idk;
            $("#btnEdit").prop("disabled", false);
            $("#btnDelete").prop("disabled", false);
        } else {
            $("#btnEdit").prop("disabled", true);
            $("#btnDelete").prop("disabled", true);
        }
    });

}
var operation = "ADD";
var ADD = "ADD";
var UPDATE = "UPDATE";
var atID = "";
var ds  = "";
var isPhotoExt = "";
function onClickEdit(){
    $("#txtID").show();
    $(".tabContentTitle").html("Edit Vital");
    parentRef.operation = "edit";
    $("#viewDivBlock").hide();
    $("#addPopup").show();
    var selectedItems = angularUIgridWrapper.getSelectedRows();
    if (selectedItems && selectedItems.length > 0) {
        var obj = selectedItems[0];
        atID = obj.idk;
        operation = UPDATE;
        parentRef.updateItem = obj;
        if(parentRef.operation == UPDATE){
            var uItem = obj;
            if(uItem){
                var cmbStatus = $("#cmbStatus").data("kendoComboBox");
                // $("#txtVN").val(uItem.unitOfMeasurement);
                $("#txtVID").html("ID :"+uItem.idk);
                $("#txtABR1").val(uItem.abbreviation);
                $("#txtDesc1").val(uItem.description);
                $("#txtMRF").val(uItem.maleRangeFrom);
                $("#txtMRT").val(uItem.maleRangeTo);
                $("#txtFRF").val(uItem.femaleRangeFrom);
                $("#txtFRT").val(uItem.femaleRangeTo);
                $("#txtPRF").val(uItem.pediatricRangeFrom);
                $("#txtPRT").val(uItem.pediatricRangeTo);

                getComboListIndex("cmbUnit", "value", uItem.unitOfMeasurement);

                if(uItem.iconFileExtension){
                    var urlsrc = ipAddress+"/homecare/download/vitals/photo/?id="+uItem.idk+"&access_token="+sessionStorage.access_token+"&tenant="+sessionStorage.tenant+"&icon-file-extension=png&"+Math.round(Math.random()*1000000);
                    $("#imgPhoto").attr("src",urlsrc);
                }

                if(uItem.isActive == 1){
                    $("#cmbStatus").val(1);
                }else{
                    $("#cmbStatus").val(0);
                }
            }
        }
    }
}


function closeVideoScreen(evt,returnData){

}
var operation = "add";
var selFileResourceDataItem = null;
function onClickCancel(){
    $("#viewDivBlock").show();
    $("#addPopup").hide();
}


function themeAPIChange(){
    getAjaxObject(ipAddress+"/homecare/settings/?id=2","GET",getThemeValue,onError);
}


function getThemeValue(dataObj){
    console.log(dataObj);
    var tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.activityTypes){
            if($.isArray(dataObj.response.settings)){
                tempCompType = dataObj.response.settings;
            }else{
                tempCompType.push(dataObj.response.settings);
            }
        }
        if(tempCompType.length > 0){
            themesChange(tempCompType[0].value);
        }
    }
}

function themesChange(themeValue){
    if(themeValue == 2){
        loadAPi("../../Theme2/Theme02.css");
    }
    else if(themeValue == 3){
        loadAPi("../../Theme3/Theme03.css");
    }
}


function onClickSave(){
    var txtMRF = $("#txtMRF").val();
    txtMRF = $.trim(txtMRF);

    var txtMRT = $("#txtMRT").val();
    txtMRT = $.trim(txtMRT);

    var txtFRT = $("#txtFRT").val();
    txtFRT = $.trim(txtFRT);

    var  txtFRF = $("#txtFRF").val();
    txtFRF = $.trim(txtFRF);

    var  txtPRF = $("#txtPRF").val();
    txtPRF = $.trim(txtPRF);

    var  txtPRT = $("#txtPRT").val();
    txtPRT = $.trim(txtPRT);

    var txtABR = $("#txtABR1").val();
    txtABR = $.trim(txtABR);

    var txtDesc = $("#txtDesc1").val();
    txtDesc = $.trim(txtDesc);

    txtMRT = Number(txtMRT);
    txtMRF = Number(txtMRF);
    txtFRT = Number(txtFRT);
    txtFRF = Number(txtFRF);
    txtPRF = Number(txtPRF);
    txtPRT = Number(txtPRT);

    // var txtVN = $("#txtVN").val();


    if(txtPRT != "" && txtPRF != "" && txtFRF != "" && txtMRF != "" && txtMRT != ""  && txtABR != "" && txtFRT != ""){
        if(txtMRT>=txtMRF){
            var dataObj = {};
            dataObj.createdBy = Number(sessionStorage.userId);
            var IsActive = $("#cmbStatus").val();
            if(IsActive == 1){
                dataObj.isDeleted =0;
            }
            else{
                dataObj.isDeleted =0;
            }
            dataObj.isActive = IsActive;
            var cmbUnit = $("#cmbUnit").data("kendoComboBox");
            dataObj.unitOfMeasurement = cmbUnit.text();
            dataObj.femaleRangeFrom = txtFRF;
            dataObj.femaleRangeTo = txtFRT;
            dataObj.maleRangeFrom = txtMRF;
            dataObj.maleRangeTo = txtMRT;
            dataObj.pediatricRangeFrom = txtPRF;
            dataObj.pediatricRangeTo = txtPRT;
            dataObj.displayOrder = 1;
            dataObj.description = txtDesc;
            dataObj.abbreviation = txtABR;
            var dataUrl = ipAddress +"/homecare/vitals/";
            var method = "POST";
            if(parentRef.operation == UPDATE){
                method = "PUT";
                dataObj.id = parentRef.updateItem.idk;
                if(parentRef.updateItem.iconFileExtension){
                    dataObj.iconFileExtension = parentRef.updateItem.iconFileExtension;
                }
                dataUrl = ipAddress +"/homecare/vitals/";
            }//iconFileExtension
            createAjaxObject(dataUrl, dataObj, method, onCreate, onError);
        }else{
            var msg = "Range To is always greater than Range From";
            customAlert.error("Error", msg);
        }
    }else{
        var msg = "Please enter mandatory fields";
        customAlert.error("Error", msg);
    }
}
function onCreate(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "Vitals are created successfully";
            if(parentRef.operation == UPDATE){
                msg = "Vitals are  updated successfully"
            }else{
                $("#txtVID").val(dataObj.response.vitals.id);
                parentRef.updateItem = {};
                parentRef.updateItem.idk = dataObj.response.vitals.id;
                operation = UPDATE;
                parentRef.operation = UPDATE;
            }

            if(fileName){
                onClickUploadPhoto();
                onClickCancel();
            }else{
                displaySessionErrorPopUp("Info", msg, function(res) {
                    //onClickCancel();
                })
            }

        }else{
            customAlert.error("Error", dataObj.response.status.message);
        }
    }else{

    }
}


function getVitalListUnits(dataObj){
    console.log(dataObj);
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbUnit", onPrefixChange, ["value", "desc"], 0, "");
    }

}

function getTableListArray(dataObj) {
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.codeTable) {
        if ($.isArray(dataObj.response.codeTable)) {
            dataArray = dataObj.response.codeTable;
        } else {
            dataArray.push(dataObj.response.codeTable);
        }
    }
    var tempDataArry = [];
    var obj = {};
    obj.desc = "";
    obj.zip = "";
    obj.value = "";
    obj.idk = "";
    //tempDataArry.push(obj);
    for (var i = 0; i < dataArray.length; i++) {
        if (dataArray[i].isActive == 1) {
            var obj = dataArray[i];
            obj.idk = dataArray[i].id;
            obj.status = dataArray[i].Status;
            tempDataArry.push(obj);
        }
    }
    return tempDataArry;
}
function onPrefixChange(){

}
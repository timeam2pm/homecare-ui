var angularPTUIgridWrapper = null;
var parentRef = null;
var operation = "";
var selItem = null;
var ADD = "add";
var UPDATE = "edit";
var VIEW = "view";
var DELETE = "delete";

var statusArr = [{Key:'Active',Value:'Active'},{Key:'InActive',Value:'InActive'}];
var groupArr = [{Key:'true',Value:'1'},{Key:'false',Value:'0'}];
$(document).ready(function() {
    parentRef = parent.frames['iframe'].window;
    parentRef.screenType = "Activity Type List";
    var pnlHeight = window.innerHeight;
    var imgHeight = pnlHeight - 100;
    $("#divTop").height(imgHeight);
    document.getElementById('txtDisplayOrder').value = sessionStorage.getItem("displayOrderMaxlength");

});


$(window).load(function() {
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
    init();
});

function init() {
    adjustHeight();
    allowNumerics("txtID");
    operation = parentRef.operation;
    $("#cmbStatus").kendoComboBox();
    setDataForSelection(statusArr, "cmbStatus", onStatusChange, ["Key", "Value"], 0, "");
    setDataForSelection(groupArr, "cmbGroup", onGroupChange, ["Key", "Value"], 0, "");

    if(operation == UPDATE) {
        selItem = parentRef;
        $("#liCarePlan").show();
        onClickRPSearch();
    }
    buttonEvents();
}

function showOperations(){
    var node = '<div style="text-align:center"><span style="cursor:pointer;font-weight:bold;font-size:15px">Edit</span>&nbsp;&nbsp;<span style="cursor:pointer;font-weight:bold;font-size:15px">Delete</span>';
    node = node+"</div>";
    return node;
}
function closeCallReport(evt,re){

}
function onClickRPSearch(){
    if(selItem.id != ""){
        $("#txtID").val(selItem.id);
        $("#txtAT").val(selItem.type);
        $("#txtDisplayOrder").val(selItem.displayOrder);
        //$("#selGroup").val(selItem.activityGroup);

        var cmbStatus = $("#cmbStatus").data("kendoComboBox");
        if(cmbStatus){
            if(selItem.isActive == 1){
                cmbStatus.select(0);
            }else{
                cmbStatus.select(1);
            }
        }

        var cmbGroup = $("#cmbGroup").data("kendoComboBox");
        if(cmbGroup){
            var aid = getActivityNameById(selItem.activityGroup);
            cmbGroup.select(aid);
        }
    }
}

var allRosterRows = [];
var allRosterRowCount = 0;
var allRosterRowIndex = 0;

function buttonEvents() {
    $("#tabsUL li a[data-toggle='tab'").off("click");
    $("#tabsUL li a[data-toggle='tab'").on("click",onClickTabs);

    $("#btnCancel").off("click", onClickCancel);
    $("#btnCancel").on("click", onClickCancel);

    $("#btnSave").off("click", onClickSave);
    $("#btnSave").on("click", onClickSave);

    $("#btnSearch").off("click", onClickSearch);
    $("#btnSearch").on("click", onClickSearch);

    $("#btnReset").off("click", onClickReset);
    $("#btnReset").on("click", onClickReset);

    $('body').on('click','.qualAddRemoveLink.addQual', function() {
        var $trLen = $('.qualificationTabWrapper table tbody tr').length;
        $('.qualificationTabWrapper table tbody').append('<tr class="qualWrapper_li qualificationTabWrapper-li-'+$trLen+'"><td class="qual-td-addRemove"><span><a href="#" class="qualAddRemoveLink addQual">+</a> <a href="#" class="qualAddRemoveLink removeQual">-</a></span></td><td><input type="text" name="qualWrap-sno" value="'+$trLen+'" readonly /></td><td><input type="text" name="qualWrap-qualification" id="qualWrapQualification" maxlength="100" value="" /></td><td><input type="text" name="qualWrap-university" id="qualWrapUniversity" maxlength="100" value="" /></td><td><input type="text" name="qualWrap-percentage" id="qualWrapPercentage" value="" /></td><td><input type="text" name="qualWrap-passedout" id="qualWrapPassedout" value="" style="width: auto !important; padding-left: 0 !important;" /></td><td><input type="text" name="qualWrap-expirydate" value="" style="width: auto !important; padding-left: 0 !important;" /></td></tr>');
        allowOnlyDecimals($('[name="qualWrap-percentage"]'));
        $('.qualificationTabWrapper-li-'+$trLen).find('[name="qualWrap-passedout"]').kendoDatePicker();
        $('.qualificationTabWrapper-li-'+$trLen).find('[name="qualWrap-expirydate"]').kendoDatePicker();
        modifySerialNumber();
    });
    $('body').on('click','.qualAddRemoveLink.removeQual', function() {
        $(this).closest('tr').remove();
        /*$('.qualificationTabWrapper table tbody').append('<span><a href="#" class="qualAddRemoveLink addQual">+</a> <a href="#" class="qualAddRemoveLink removeQual">-</a><tr><td><input type="text" name="qualWrap-sno" value="'+$trLen+'" /></td><td><input type="text" name="qualWrap-qualification" value="" /></td><td><input type="text" name="qualWrap-university" value="" /></td><td><input type="text" name="qualWrap-percentage" value="" /></td><td><input type="text" name="qualWrap-passedout" value="" /></td><td><input type="text" name="qualWrap-expirydate" value="" /></td></tr></span>');*/
        modifySerialNumber();
    });

}

function onClickTabs(e){
    var tabHref = $(e.target).attr("id");
    console.log($(e.target).text());
    $("#btnSave").show();
    $("#btnReset").show();
    if($(e.target).text() == "Roster"){
        $("#btnSave").hide();
        $("#btnReset").hide();
    }
}
function modifySerialNumber() {
    $('.qualificationTabWrapper [name="qualWrap-sno"]').each(function(i) {
        $(this).val(i + 1);
    });
}
function adjustHeight(){
    var defHeight = 200;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    if(angularPTUIgridWrapper){
        angularPTUIgridWrapper.adjustGridHeight(cmpHeight);
    }
}

function onClickReset() {
    if(operation == ADD) {
        operation = ADD;
    }
    else {
        operation = UPDATE;
    }
    $("#txtID").val("");
    $("#txtAT").val("");
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if(cmbStatus){
        cmbStatus.select(0);
        cmbStatus.enable(false);
    }
    var cmbGroup = $("#cmbGroup").data("kendoComboBox");
    if(cmbGroup){
        cmbGroup.select(0);
        //cmbGroup.enable(false);
    }
}

function onClickSave(){

    var strAT = $("#txtAT").val();
    strAT = $.trim(strAT);
    var strDisplayOrder = $("#txtDisplayOrder").val();
    var isActive = 0;
    var isDelete = 0;
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");

    if(cmbStatus){
        if(cmbStatus.selectedIndex == 0){
            isActive = 1;
        }
    }
    var cmbGroup = $("#cmbGroup").data("kendoComboBox");

    if(isActive == 0){
        isDelete = 1;
    }


    if(strAT != ""){
        var dataObj = {};
        dataObj.createdBy = Number(sessionStorage.userId);
        dataObj.isDeleted = isDelete;
        dataObj.isActive = isActive;
        dataObj.type = strAT;
        dataObj.activityGroup = cmbGroup.value();
        var dataUrl = ipAddress +"/homecare/activity-types/";
        var method = "POST";
        if(operation == UPDATE){
            method = "PUT";
            dataObj.id = selItem.id;
            dataObj.displayOrder = strDisplayOrder;
            dataUrl = ipAddress +"/homecare/activity-types/";
        }else{
            dataObj.displayOrder = strDisplayOrder;
        }
        createAjaxObject(dataUrl, dataObj, method, onCreate, onError);
    }
    else{
        customAlert.error("error", "Please fill     the Required Fields");
    }
}

function onCreate(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            if(operation == ADD){
                customAlert.info("info", "Activity Type created successfully");
            }else{
                customAlert.info("info", "Activity Type updated successfully");
            }
            var obj = {};
            obj.status = "success";
            obj.operation = operation;
            popupClose(obj);
        }else{
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}

function onError(errObj) {
    console.log(errObj);
    customAlert.error("Error", "Error");
}

function onClickCancel() {
    var obj = {};
    obj.status = "false";
    popupClose(obj);
}

function onClickSearch() {
    var obj = {};
    obj.status = "search";
    popupClose(obj);
}

function popupClose(obj) {
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}

function allowOnlyDecimals(selector) {
    selector.keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 95 && e.which != 46) {
            return false;
        }
    });
}

function onStatusChange(){
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if(cmbStatus && cmbStatus.selectedIndex<0){
        cmbStatus.select(0);
    }
}

function onGroupChange(){
    var cmbGroup = $("#cmbGroup").data("kendoComboBox");
    if(cmbGroup && cmbGroup.selectedIndex<0){
        cmbGroup.select(0);
    }
}

function getActivityNameById(aId){
    for(var i=0;i<groupArr.length;i++){
        var item = groupArr[i];
        if(item && item.Value == aId){
            return i;
        }
    }
    return 0;
}
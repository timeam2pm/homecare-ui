var parentRef = null;
var operation = "";
var selItem = null;
var ADD = "add";
var UPDATE = "update";
var VIEW = "view";
var DELETE = "delete";
var statusArr = [{Key:'Active',Value:'Active'},{Key:'InActive',Value:'InActive'}];


var angularUIgridWrapper;

$(document).ready(function(){
    parentRef = parent.frames['iframe'].window;
    var pnlHeight = window.innerHeight;
    var imgHeight = pnlHeight-120;
    $("#divTop").height(imgHeight);

    var dataOptions = {
        pagination: false,
        changeCallBack: onChange

    }
    angularUIgridWrapper = new AngularUIGridWrapper("dgridMedicationList", dataOptions);
    angularUIgridWrapper.init();
    buildDeviceListGrid([]);

});


$(window).load(function(){
    loading = false;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
    init();
    buttonEvents();
    adjustHeight();
}

function init(){

    allowAlphabets("txtAbbrevation");
    allowAlphabets("txtName");

    /*$('#txtAbbrevation,#txtName').bind('keyup', function() {
    if(allFilled()) {
        $('#btnSave').removeAttr('disabled');
    }else{
        $('#btnSave').attr("disabled" , "disabled");
    }
});

function allFilled() {
    var filled = true;
    $('body input').each(function() {
        if($(this).val() == '')
        filled = false;
    });
    return filled;
}*/
    buildDeviceListGrid([]);
    operation = parentRef.operation;
    selItem = parentRef.selItem;
    $("#cmbStatus").kendoComboBox();
    setDataForSelection(statusArr, "cmbStatus", onStatusChange, ["Value", "Key"], 0, "");
    if(operation == UPDATE && selItem){
        console.log(selItem);
        //$("#btnReset").hide();
        $("#txtAbbrevation").val(selItem.abbr);
        $("#txtName").val(selItem.country);


        var cmbStatus = $("#cmbStatus").data("kendoComboBox");
        if(cmbStatus){
            if(selItem.isActive == 1){
                cmbStatus.select(0);
            }else{
                cmbStatus.select(1);
            }
        }
    }else{
        var cmbStatus = $("#cmbStatus").data("kendoComboBox");
        if(cmbStatus){
            cmbStatus.enable(false);
        }
    }
    getAjaxObject(ipAddress+"/country/list/","GET",getCountryList,onError);
}

function onStatusChange(){
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if(cmbStatus && cmbStatus.selectedIndex<0){
        cmbStatus.select(0);
    }
}
function onError(errorObj){
    console.log(errorObj);
}
function getCountryList(dataObj){
    console.log(dataObj);
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.country){
        if($.isArray(dataObj.response.country)){
            dataArray = dataObj.response.country;
        }else{
            dataArray.push(dataObj.response.country);
        }
    }
    var tempDataArry = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
        }
    }
    buildDeviceListGrid(dataArray);
}
function buttonEvents(){
    $("#btnEdit").off("click",onClickOK);
    $("#btnEdit").on("click",onClickOK);

    $("#btnCancelDet").off("click",onClickCancel);
    $("#btnCancelDet").on("click",onClickCancel);

    $("#btnDelete").off("click",onClickDelete);
    $("#btnDelete").on("click",onClickDelete);

    $("#btnAdd").off("click");
    $("#btnAdd").on("click",onClickAdd);

    $("#btnCancel").off("click",onClickCancel);
    $("#btnCancel").on("click",onClickCancel);

    $("#btnSave").off("click",onClickSave);
    $("#btnSave").on("click",onClickSave);

    $("#btnSearch").off("click",onClickSearch);
    $("#btnSearch").on("click",onClickSearch);

    $("#btnReset").off("click",onClickReset);
    $("#btnReset").on("click",onClickReset);
}

function onClickReset(){
    operation = ADD;
    selItem = null;
    $("#txtAbbrevation").val("");
    $("#txtCode").val("");
    $("#txtName").val("");
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if(cmbStatus){
        cmbStatus.select(0);
        cmbStatus.enable(false);
    }
}

function validation(){
    var flag = true;
    var strAbbr = $("#txtAbbrevation").val();
    strAbbr = $.trim(strAbbr);
    if(strAbbr == ""){
        customAlert.error("Error","Enter abbrevation");
        flag = false;
        return false;
    }

    var strName = $("#txtName").val();
    strName = $.trim(strName);
    if(strName == ""){
        customAlert.error("Error","Enter country name");
        flag = false;
        return false;
    }

    return flag;
}

function onClickSave(){
    if(validation()){
        var strAbbr = $("#txtAbbrevation").val();
        strAbbr = $.trim(strAbbr);
        var strCode = $("#txtCode").val();
        strCode = $.trim(strCode);
        var strName = $("#txtName").val();
        strName = $.trim(strName);
        var isActive = 0;
        var cmbStatus = $("#cmbStatus").data("kendoComboBox");
        if(cmbStatus){
            if(cmbStatus.selectedIndex == 0){
                isActive = 1;
            }
        }
        var dataObj = {};
        dataObj.abbr = strAbbr;
        dataObj.country = strName;
        dataObj.code = strCode;
        dataObj.isActive = isActive;
        dataObj.createdBy = sessionStorage.userId;//"101";//sessionStorage.uName;
        dataObj.isDeleted = "0";
        if(operation == ADD){
            var dataUrl = ipAddress+"/country/create";
            createAjaxObject(dataUrl,dataObj,"POST",onCreate,onError);
        }else if(selItem){
            dataObj.id = selItem.idk;
            var dataUrl = ipAddress+"/country/update";
            createAjaxObject(dataUrl,dataObj,"POST",onCreate,onError);
        }
    }
}

function onCreate(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            if(operation == ADD){
                customAlert.info("info", "Country Created Successfully");
                onClickReset();
            }else{
                customAlert.info("info", "Country Updated Successfully");
            }
        }else{
            customAlert.error("error", dataObj.response.status.message);
        }
    }
    /*var obj = {};
    obj.status = "success";
    obj.operation = operation;
    popupClose(obj);*/
}
function adjustHeight(){
    var defHeight = 80;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    cmpHeight = cmpHeight/2;
    angularUIgridWrapper.adjustGridHeight(cmpHeight);
}

function buildDeviceListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Abbreviation",
        "field": "abbr"

    });
    gridColumns.push({
        "title": "Name",
        "field": "code"

    });
    gridColumns.push({
        "title": "Status",
        "field": "Status"

    });
    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}
var prevSelectedItem =[];
function onChange(){
    setTimeout(function(){
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        /*if(selectedItems && selectedItems.length>0){
            $("#btnEdit").prop("disabled", false);
            $("#btnDelete").prop("disabled", false);
        }else{
            $("#btnEdit").prop("disabled", true);
            $("#btnDelete").prop("disabled", true);
        }*/
    })
}

function onClickOK(){
    setTimeout(function(){
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if(selectedItems && selectedItems.length>0){
            var obj = {};
            obj.selItem = selectedItems[0];
            // var onCloseData = new Object();
            obj.status = "success";
            obj.operation = "ok";
            var windowWrapper = new kendoWindowWrapper();
            windowWrapper.closePageWindow(obj);
        }
    })
}
function onClickDelete(){
    customAlert.confirm("Confirm", "Are you sure you want delete?",function(response){
        if(response.button == "Yes"){
            var selectedItems = angularUIgridWrapper.getSelectedRows();
            console.log(selectedItems);
            if(selectedItems && selectedItems.length>0){
                var selItem = selectedItems[0];
                if(selItem){
                    var dataUrl = ipAddress+"/country/delete/";
                    var reqObj = {};
                    reqObj.id = selItem.idk;
                    reqObj.abbr = selItem.abbr;
                    reqObj.country = selItem.country;
                    reqObj.code = selItem.code;
                    reqObj.isDeleted = "1";
                    reqObj.modifiedBy = sessionStorage.userId
                    createAjaxObject(dataUrl,reqObj,"POST",onDeleteCountryt,onError);
                }
            }
        }
    });
}
function onDeleteCountryt(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            customAlert.info("info", "Country Deleted Successfully");
            init();
        }else{
            customAlert.error("error", dataObj.response.status.message);
        }
    }

}

function onError(errObj){
    console.log(errObj);
    customAlert.error("Error","Unable to create Country");
}

function onClickSearch(){
    var obj = {};
    obj.status = "search";
    popupClose(obj);
}
function onClickCancel(){
    var obj = {};
    obj.status = "false";
    popupClose(obj);
}

function onClickAdd(){
    var obj = {};
    obj.status = "Add";
    obj.operation = "ok";
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}
function onClickCancel(){
    var obj = {};
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}



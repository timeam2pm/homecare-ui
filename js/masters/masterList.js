var operation = "";
var ADD = "add";
var UPDATE = "update";
var VIEW = "view";
var DELETE = "delete";

var parentRef = null;

$(document).ready(function(){
	parentRef = parent.frames['iframe'].window;
});

$(window).load(function(){
	var pnlHeight = window.innerHeight;
	pnlHeight = pnlHeight-30;
	$("#leftPanel").height(pnlHeight);
	$("#centerPanel").height(pnlHeight);
	$("#rightPanel").height(pnlHeight);
	$("#leftButtonBar").height(pnlHeight);

	init()
});

function init(){
    buttonEvents();
}
function buttonEvents(){
    $("#panelMovLeft").off("click",onClickLeftButton);
    $("#panelMovLeft").on("click",onClickLeftButton);
    
    $("#panelMoveRight").off("click",onClickRightButton);
    $("#panelMoveRight").on("click",onClickRightButton);
    
    $("#addPt").off("click",onClickAddPatient);
    $("#addPt").on("click",onClickAddPatient);
    
//    $("#searchPt").off("click",onClickSearchPatient);
   // $("#searchPt").on("click",onClickSearchPatient);
    
    $("#addCountry").off("click",onClickAddCountry);
    $("#addCountry").on("click",onClickAddCountry);
    
    $("#searchCountry").off("click",onClickSearchCountry);
    $("#searchCountry").on("click",onClickSearchCountry);
    
    $("#addCounty").off("click",onClickAddCounty);
    $("#addCounty").on("click",onClickAddCounty);
    
    $("#searchCounty").off("click",onClickSearchCounty);
    $("#searchCounty").on("click",onClickSearchCounty);
    
    $("#addState").off("click",onClickAddState);
    $("#addState").on("click",onClickAddState);
    
    $("#searchState").off("click",onClickSearchState);
    $("#searchState").on("click",onClickSearchState);
    
    
    $("#addZip").off("click",onClickAddZip);
    $("#addZip").on("click",onClickAddZip);
    
    $("#searchZip").off("click",onClickSearchZip);
    $("#searchZip").on("click",onClickSearchZip);
    
    $("#createTable").off("click",onClickCreateTable);
    $("#createTable").on("click",onClickCreateTable);
    
    $("#addValues").off("click",onClickAddValues);
    $("#addValues").on("click",onClickAddValues);
    
    $("#searchValues").off("click",onClickSearchValues);
    $("#searchValues").on("click",onClickSearchValues);
    
    $("#searchTable").off("click",onClickSearchCodeTable);
    $("#searchTable").on("click",onClickSearchCodeTable);
}

function adjustHeight(){
}

function onClickLeftButton(){
	console.log("left");
	if($("#leftPanel").is(":visible")){
		$("#leftPanel").hide();
		$("#centerPanel").removeClass("col-sm-7 col-md-7 col-lg-7 col-xs-12 panelBorderStyle");
		$("#centerPanel").addClass("col-sm-9 col-md-9 col-lg-9 col-xs-12 panelBorderStyle");
	}else{
		$("#leftPanel").show();
		$("#centerPanel").removeClass("col-sm-9 col-md-9 col-lg-9 col-xs-12 panelBorderStyle");
		$("#centerPanel").addClass("col-sm-7 col-md-7 col-lg-7 col-xs-12 panelBorderStyle");
	}
}

function onClickRightButton(){
	console.log("right");
	if($("#rightPanel").is(":visible")){
		$("#rightPanel").hide();
		$("#centerPanel").removeClass("col-sm-7 col-md-7 col-lg-7 col-xs-12 panelBorderStyle");
		$("#centerPanel").addClass("col-sm-10 col-md-10 col-lg-10 col-xs-12 panelBorderStyle");
	}else{
		$("#rightPanel").show();
		$("#centerPanel").removeClass("col-sm-10 col-md-10 col-lg-10 col-xs-12 panelBorderStyle");
		$("#centerPanel").addClass("col-sm-7 col-md-7 col-lg-7 col-xs-12 panelBorderStyle");
	}
}

function onClickAddPatient(){
	console.log("Add");
	$("#centerPanel").css("visibility","visible");
	//$(html).show();
}
function onClickAddCountry(){
	operation = ADD;
	addCountry();
}
function addCountry(){
	var popW = 600;
    var popH = 450;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    if(operation == ADD){
    	 profileLbl = "Add Country";
    }else{
    	 profileLbl = "Edit Country";
    }
   
    parentRef.operation = operation;
    parentRef.selItem = selCountryItem;
    
    devModelWindowWrapper.openPageWindow("../../html/masters/createCountry.html", profileLbl, popW, popH, true, closeAddAction);
}
function closeAddAction(evt,returnData){
	if(returnData && returnData.status == "success"){
		if(returnData.operation == ADD){
			customAlert.info("Info","Country Created Successfully.")
		}else if(returnData.operation == UPDATE){
			customAlert.info("Info","Country Updated Successfully.")
		}else if(returnData.operation == DELETE){
			customAlert.info("Info","Country deleted Successfully.")
		}
	}else if(returnData && returnData.status == "search"){
		onClickSearchCountry();
	}
}
function onClickSearchCountry(){
	var popW = 800;
    var popH = 600;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search Country";
    devModelWindowWrapper.openPageWindow("../../html/masters/countryList.html", profileLbl, popW, popH, true, onCloseSearchCountryAction);
}
var selCountryItem = null;
function onCloseSearchCountryAction(evt,returnData){
	if(returnData && returnData.status == "success"){
		operation = UPDATE;
		selCountryItem = returnData.selItem;
		addCountry();
	}else if(returnData && returnData.status == "Add"){
		operation = ADD;
		addCountry();
	}
}
function onClickAddCounty(){
	operation = ADD;
	addCounty();
}
function addCounty(){
	var popW = 550;
    var popH = 450;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    if(operation == ADD){
    	 profileLbl = "Add County";
    }else{
    	 profileLbl = "Edit County";
    }
   
    parentRef.operation = operation;
    parentRef.selItem = selCountyItem;
    
    devModelWindowWrapper.openPageWindow("../../html/masters/createCounty.html", profileLbl, popW, popH, true, closeAddCountyAction);
}
function closeAddCountyAction(evt,returnData){
	if(returnData && returnData.status == "success"){
		if(returnData.operation == ADD){
			customAlert.info("Info","County Created Successfully.")
		}else if(returnData.operation == UPDATE){
			customAlert.info("Info","County Updated Successfully.")
		}else if(returnData.operation == DELETE){
			customAlert.info("Info","County deleted Successfully.")
		}
	}else if(returnData && returnData.status == "search"){
		onClickSearchCounty();
	}
}

function onClickSearchCounty(){
	var popW = 800;
    var popH = 600;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search County";
    devModelWindowWrapper.openPageWindow("../../html/masters/countyList.html", profileLbl, popW, popH, true, onCloseSearchCountyAction);
}
var selCountyItem = null;
function onCloseSearchCountyAction(evt,returnData){
	if(returnData && returnData.status == "success"){
		operation = UPDATE;
		selCountyItem = returnData.selItem;
		addCounty();
	}else if(returnData && returnData.status == "Add"){
		operation = ADD;
		addCounty();
	}
}

function onClickAddState(){
	operation = ADD;
	addState();
}
var selStateItem = null;
function addState(){
	var popW = 550;
    var popH = 550;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    if(operation == ADD){
    	 profileLbl = "Add State";
    }else{
    	 profileLbl = "Edit State";
    }
   
    parentRef.operation = operation;
    parentRef.selItem = selStateItem;
    
    devModelWindowWrapper.openPageWindow("../../html/masters/createState.html", profileLbl, popW, popH, true, closeAddStateAction);
}
function closeAddStateAction(evt,returnData){
	if(returnData && returnData.status == "success"){
		if(returnData.operation == ADD){
			customAlert.info("Info","State Created Successfully.")
		}else if(returnData.operation == UPDATE){
			customAlert.info("Info","State Updated Successfully.")
		}else if(returnData.operation == DELETE){
			customAlert.info("Info","State deleted Successfully.")
		}
	}else if(returnData && returnData.status == "search"){
		onClickSearchState();
	}
}
function onClickSearchState(){
	var popW = 800;
    var popH = 600;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search State";
    devModelWindowWrapper.openPageWindow("../../html/masters/stateList.html", profileLbl, popW, popH, true, onCloseSearchStateAction);
}
function onCloseSearchStateAction(evt,returnData){
	if(returnData && returnData.status == "success"){
		operation = UPDATE;
		selStateItem= returnData.selItem;
		addState();
	}else if(returnData && returnData.status == "Add"){
		operation = ADD;
		addState();
	}
}
function onClickSearchZip(){
	var popW = 800;
    var popH = 500;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search Zip";
    devModelWindowWrapper.openPageWindow("../../html/masters/zipList.html", profileLbl, popW, popH, true, onCloseSearchZipAction);
}
function onCloseSearchZipAction(evt,returnData){
	if(returnData && returnData.status == "success"){
		operation = UPDATE;
		selZipItem= returnData.selItem;
		addZip();
	}else if(returnData && returnData.status == "Add"){
		operation = ADD;
		addZip();
	}
}
function onClickAddZip(){
	operation = ADD;
	addZip();
}
var selZipItem = null;
function addZip(){
	var popW = 550;
    var popH = 550;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    if(operation == ADD){
    	 profileLbl = "Add Zip";
    }else{
    	 profileLbl = "Edit Zip";
    }
   
    parentRef.operation = operation;
    parentRef.selItem = selZipItem;
    
    devModelWindowWrapper.openPageWindow("../../html/masters/createZip.html", profileLbl, popW, popH, true, closeAddZipAction);
}
function closeAddZipAction(evt,returnData){
	if(returnData && returnData.status == "success"){
		if(returnData.operation == ADD){
			customAlert.info("Info","Zip Created Successfully.")
		}else if(returnData.operation == UPDATE){
			customAlert.info("Info","Zip Updated Successfully.")
		}else if(returnData.operation == DELETE){
			customAlert.info("Info","Zip deleted Successfully.")
		}
	}else if(returnData && returnData.status == "search"){
		onClickSearchZip();
	}
}
var selCodeTableItem = null;
function onClickCreateTable(){
	operation = ADD;
	addCreateTable();
}
function addCreateTable(){
	var popW = 550;
    var popH = 400;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    if(operation == ADD){
    	 profileLbl = "Add Code Table";
    }else{
    	 profileLbl = "Edit Code Table";
    }
   
    parentRef.operation = operation;
    parentRef.selItem = selCodeTableItem;
    
    devModelWindowWrapper.openPageWindow("../../html/masters/createCodeTable.html", profileLbl, popW, popH, true, closeAddCreateTableAction);
}
function closeAddCreateTableAction(evt,returnData){
	if(returnData && returnData.status == "success"){
		if(returnData.operation == ADD){
			customAlert.info("Info","Code Table Created Successfully.")
		}else if(returnData.operation == UPDATE){
			customAlert.info("Info","Code Table Updated Successfully.")
		}else if(returnData.operation == DELETE){
			customAlert.info("Info","Code Table deleted Successfully.")
		}
	}else if(returnData && returnData.status == "search"){
		onClickSearchCodeTable();
	}
}
function onClickSearchCodeTable(){
	var popW = 800;
    var popH = 500;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search Code Table";
    devModelWindowWrapper.openPageWindow("../../html/masters/codeTableList.html", profileLbl, popW, popH, true, onCloseSearchCodeTableAction);
}
function onCloseSearchCodeTableAction(evt,returnData){
	if(returnData && returnData.status == "success"){
		operation = UPDATE;
		selCodeTableItem= returnData.selItem;
		addCreateTable();
	}else if(returnData && returnData.status == "Add"){
		operation = ADD;
		addCreateTable();
	}
}
function onClickAddValues(){
	operation = ADD;
	addValuesToTable();
}
function addValuesToTable(){
	var popW = 600;
    var popH = 500;
    
    parentRef.operation = operation;
    parentRef.selItem = selCodeTableDataItem;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Code Table Values";
    devModelWindowWrapper.openPageWindow("../../html/masters/createTableValue.html", profileLbl, popW, popH, true, onCloseCodeTableValueAction);
}
function onCloseCodeTableValueAction(evt,returnData){
	if(returnData && returnData.status == "success"){
		if(returnData.operation == ADD){
			customAlert.info("Info","Code Table Values are Created Successfully.")
		}else if(returnData.operation == UPDATE){
			customAlert.info("Info","Code Table values are Updated Successfully.")
		}else if(returnData.operation == DELETE){
			customAlert.info("Info","Code Table values are deleted Successfully.")
		}
	}else if(returnData && returnData.status == "search"){
		onClickSearchValues();
	}
}
function onClickSearchValues(){
	var popW = 800;
    var popH = 500;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Table Values";
    devModelWindowWrapper.openPageWindow("../../html/masters/tableValueList.html", profileLbl, popW, popH, true, onCloseTableValueListAction);
}
var selCodeTableDataItem = null;
function onCloseTableValueListAction(evt,returnData){
	if(returnData && returnData.status == "success"){
		operation = UPDATE;
		selCodeTableDataItem= returnData.selItem;
		addValuesToTable();
	}else if(returnData && returnData.status == "Add"){
		operation = ADD;
		addValuesToTable();
	}
}
var angularPTUIgridWrapper = null;
var parentRef = null;
var operation = "";
var selItem = null;
var ADD = "add";
var UPDATE = "edit";
var VIEW = "view";
var DELETE = "delete";
var patientId = "";
var statusArr = [{ Key: 'Active', Value: 'Active' }, { Key: 'InActive', Value: 'InActive' }];

var typeArr = [{ Key: '100', Value: 'Doctor' }, { Key: '200', Value: 'Nurse' }, { Key: '201', Value: 'Caregiver' }];
/*var filterArr = [{ Key: '', Value: 'All' }, { Key: 'id', Value: 'Provider ID' }, { Key: 'abbr', Value: 'Abbreviation' }, { Key: 'first-name', Value: 'First Name' }, { Key: 'middle-name', Value: 'Middle Name' }, { Key: 'last-name', Value: 'Last Name' }, { Key: 'type', Value: 'Type' }, { Key: 'user-id', Value: 'User Id' }, { Key: 'facility-id', Value: 'Facility Id' }];*/

var patientInfoObject = null;
var commId = "";
var ABBREVIATION_Patient = "";

$(document).ready(function() {
    parentRef = parent.frames['iframe'].window;
    parentRef.screenType = "provider";
    var pnlHeight = window.innerHeight;
    var imgHeight = pnlHeight - 100;
    $("#divTop").height(imgHeight);

});


$(window).load(function() {
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
    init();
});

function init() {

    var dataOptionsPT = {pagination: false,paginationPageSize: 500,changeCallBack: onPTChange};

    //angularPTUIgridWrapper = new AngularUIGridWrapper("rPrGrid1", dataOptionsPT);
    //angularPTUIgridWrapper.init();
    //buildPatientRosterList([]);
    adjustHeight();

    allowNumerics("txtID");
    //txtNEIC
    operation = parentRef.operation;
    patientId = parentRef.patientId;
    if(operation == "edit") {
        selItem = parentRef.selDocItem;
        // $("#divGPSReport").show();
        $("#liCarePlan").show();
        onClickRPSearch();
        getPatientRosterAppointments();
    }
    buttonEvents();
}

function getWeekDayName(wk){
    var wn = "";
    if(wk == 1){
        return "Monday";
    }else if(wk == 2){
        return "Tuesday";
    }else if(wk == 2){
        return "Tuesday";
    }else if(wk == 3){
        return "Wednesday";
    }else if(wk == 4){
        return "Thursday";
    }else if(wk == 5){
        return "Friday";
    }else if(wk == 6){
        return "Saturday";
    }else if(wk == 0){
        return "Sunday";
    }
    return wn;
}

function onPTChange(){
    setTimeout(function(){
        var selectedItems = angularPTUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if(selectedItems && selectedItems.length>0){
            $("#btnREdit").prop("disabled", false);
            $("#btnRDel").prop("disabled", false);
        }else{
            $("#btnREdit").prop("disabled", true);
            $("#btnRDel").prop("disabled", true);
        }
    },100)
}
function showOperations(){
    var node = '<div style="text-align:center"><span style="cursor:pointer;font-weight:bold;font-size:15px">Edit</span>&nbsp;&nbsp;<span style="cursor:pointer;font-weight:bold;font-size:15px">Delete</span>';
    node = node+"</div>";
    return node;
}
function showPatientRoster(){
    var txtFAN = $("#txtFAN").data("kendoComboBox");
    if(txtFAN){
        parentRef.fid = txtFAN.value();
        parentRef.fname = txtFAN.text();
    }
    parentRef.txtID = $("#txtID").val();

}

function onClickGPSReport(){
    parentRef.screenType = "providers";
    parentRef.providerid = selItem.idk;
    openReportPopup("../../html/patients/geoReport.html","GPS Report");
}
function openReportPopup(path,title){
    var popW = "90%";
    var popH = "85%";

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = title;
    devModelWindowWrapper.openPageWindow(path, profileLbl, popW, popH, false, closeCallReport);
}
function closeCallReport(evt,re){

}

function getDataGridArray(){
    var allRows = angularPTUIgridWrapper.getAllRows();
    var items = [];
    for(var a=0;a<allRows.length;a++){
        var row = allRows[a].entity;
        items.push(row);
    }
    return items;
}
function closePatientRoster(evt,returnData){
    console.log(returnData);
    if(returnData && returnData.status == "success"){
        var obj = returnData;
        var st = returnData.rs;
        if(st == "create"){
            var strDOW = obj.dow1;
            var strDOWK = obj.dowK1;
            var dArray = [];
            for(var s=0;s<strDOW.length;s++){
                //var item = strDOW[s];
                obj.dow = strDOW[s];
                obj.dowK = strDOWK[s];
                var rdItem =   JSON.parse(JSON.stringify(obj));
                dArray.push(rdItem);
            }

            var dItems = getDataGridArray();
            dArray = dArray.concat(dItems);
            //buildPatientRosterList([]);
            setTimeout(function(){
                //buildPatientRosterList(dArray);
                angularPTUIgridWrapper.refreshGrid();
            },100);
        }else{
            var selectedItems = angularPTUIgridWrapper.getSelectedRows();
            var dgItem = selectedItems[0];
            dgItem.contract = obj.contract;
            dgItem.contractK = obj.contractK;

            dgItem.facility = obj.facility;
            dgItem.facilityK = obj.facilityK;

            dgItem.provider = obj.provider;
            dgItem.providerK = obj.providerK;

            dgItem.fromTime = obj.fromTime;
            dgItem.fromTimeK = obj.fromTimeK;

            dgItem.toTime = obj.toTime;
            dgItem.toTimeK = obj.toTimeK;

            dgItem.duration = obj.duration;

            dgItem.AppType = obj.AppType;
            dgItem.AppTypeK = obj.AppTypeK;

            dgItem.reason = obj.reason;
            dgItem.reasonK = obj.reasonK;

            dgItem.notes = obj.notes;

            dgItem.patient = obj.patient;
            dgItem.patientK = obj.patientK;

            dgItem.dow = obj.dow;
            dgItem.dowK = obj.dowK;

            angularPTUIgridWrapper.refreshGrid();
        }
    }
}
function onClickEditRoster(){
    var selectedItems = angularPTUIgridWrapper.getSelectedRows();
    if(selectedItems && selectedItems.length>0){
        var popW = "60%";
        var popH = "60%";
        var profileLbl;
        var devModelWindowWrapper = new kendoWindowWrapper();
        profileLbl = "Edit Roster";
        parentRef.RS = "edit";
        parentRef.selRSItem = selectedItems[0];
        showPatientRoster()
        devModelWindowWrapper.openPageWindow("../../html/patients/createRoster.html", profileLbl, popW, popH, true, closePatientRoster);
    }
}
function onClickDeleteRoster(){
    var selectedItems = angularPTUIgridWrapper.getSelectedRows();
    if(selectedItems && selectedItems.length>0){
        customAlert.confirm("Confirm", "Are you sure you want delete?",function(response){
            if(response.button == "Yes"){
                var obj = selectedItems[0];
                if(obj.idk){
                    var rosterObj = {};
                    rosterObj.id = obj.idk;
                    rosterObj.modifiedBy = Number(sessionStorage.userId);;
                    rosterObj.isActive = 0;
                    rosterObj.isDeleted = 1;
                    var dataUrl = ipAddress+"/patient/roster/delete";
                    createAjaxObject(dataUrl,rosterObj,"POST",onPatientRosterDelete,onError);
                }else{
                    angularPTUIgridWrapper.deleteItem(selectedItems[0]);
                }
            }
        });
    }
}
function onPatientRosterDelete(dataObj){
    onClickRPSearch();
}
function onClickRPSearch(){
    if(selItem.idk != ""){
        var patientRosterURL = ipAddress+"/homecare/reports/";
        buildPatientRosterList([]);
        getAjaxObject(patientRosterURL,"GET",onGetRosterList,onError);
    }
}
function onGetRosterList(dataObj){
    console.log(dataObj);
    var reportMasterArray = [];
    if(dataObj && dataObj.response &&  dataObj.response.status && dataObj.response.status.code == 1){
        if($.isArray(dataObj.response.reports)){
            reportMasterArray = dataObj.response.reports;
        }else{
            reportMasterArray.push(dataObj.response.reports);
        }
    }
    for(var i=0;i<reportMasterArray.length;i++){
        if(reportMasterArray[i].id == selItem.idk)
        {
            reportMasterArray[i].idk = reportMasterArray[i].id;
            reportMasterArray[i].name = reportMasterArray[i].name;
            reportMasterArray[i].api = reportMasterArray[i].api;
            reportMasterArray[i].notes = reportMasterArray[i].notes;

        }
    }
    buildPatientRosterList(reportMasterArray);
}
var allRosterRows = [];
var allRosterRowCount = 0;
var allRosterRowIndex = 0;
function onClickCreateRoster(){
    allRosterRowCount = 0;
    allRosterRowIndex = 0;
    allRosterRows = angularPTUIgridWrapper.getAllRows();
    allRosterRowCount = allRosterRows.length;
    if(allRosterRowCount>0){
        saveRoster();
    }
}
function saveRoster(){
    if(allRosterRowCount>allRosterRowIndex){
        var objRosterEntiry = allRosterRows[allRosterRowIndex];
        var rosterObj = objRosterEntiry.entity;
        rosterObj.createdBy = Number(sessionStorage.userId);
        rosterObj.isActive = 1;
        rosterObj.isDeleted = 0;

        rosterObj.patientId = rosterObj.patientK;
        rosterObj.facilityId = rosterObj.facilityK;
        rosterObj.contractId = rosterObj.contractK;
        rosterObj.providerId = rosterObj.providerK;
        rosterObj.weekDay = rosterObj.dowK;
        rosterObj.fromTime = rosterObj.fromTimeK;
        rosterObj.toTime = rosterObj.toTimeK;
        rosterObj.careTypeId = 2;
        rosterObj.duration = rosterObj.duration;
        rosterObj.reason = rosterObj.reason;
        rosterObj.reasonId = rosterObj.reasonK;
        rosterObj.appointmentReason = rosterObj.reason;
        rosterObj.appointmentType = rosterObj.AppType;
        rosterObj.appointmentReasonId = rosterObj.reasonK;
        rosterObj.appointmentTypeId = rosterObj.AppTypeK;
        rosterObj.notes = rosterObj.notes;
        var dataUrl = "";
        if(rosterObj.idk){
            var rObj = {};
            rObj.id = rosterObj.idk;
            rObj.modifiedBy = Number(sessionStorage.userId);
            rObj.isActive = 1;
            rObj.isDeleted = 0;
            rObj.patientId = rosterObj.patientK;
            rObj.contractId = rosterObj.contractK;
            rObj.providerId = rosterObj.providerK;
            rObj.weekDay = rosterObj.dowK;
            rObj.fromTime = rosterObj.fromTimeK;
            rObj.toTime = rosterObj.toTimeK;
            rObj.careTypeId = rosterObj.careTypeId;
            rObj.duration = rosterObj.duration;
            rObj.reason = rosterObj.reason;
            rObj.reasonId = rosterObj.reasonK;
            rObj.notes = rosterObj.notes;
            rObj.facilityId = rosterObj.facilityK;
            rObj.appointmentReason = rosterObj.reason;
            rObj.appointmentType = rosterObj.AppType;
            rObj.appointmentReasonId = rosterObj.reasonK;
            rObj.appointmentTypeId = rosterObj.AppTypeK;

            dataUrl = ipAddress+"/patient/roster/update";
            createAjaxObject(dataUrl,rObj,"POST",onPatientRosterCreate,onError);
        }else{
            dataUrl = ipAddress+"/patient/roster/create";
            createAjaxObject(dataUrl,rosterObj,"POST",onPatientRosterCreate,onError);
        }
    }
}
function onPatientRosterCreate(dataObj){
    allRosterRowIndex = allRosterRowIndex+1;
    if(allRosterRowCount == allRosterRowIndex){
        customAlert.error("Info", "Roster created successfully");
        buildPatientRosterList([]);
        onClickRPSearch();
        //getPatientRosterList();
    }else{
        saveRoster();
    }
}
function onClickCreateRosterAppointment(){
    if(selItem.idk != ""){
        var popW = "400px";
        var popH = "500px";
        var profileLbl;
        var devModelWindowWrapper = new kendoWindowWrapper();
        profileLbl = "Create Roster Appointment";
        parentRef.pid = selItem.idk;
        var txtFAN = $("#txtFAN").data("kendoComboBox");
        if(txtFAN){
            parentRef.fid = txtFAN.value();
            parentRef.fname = txtFAN.text();
        }
        devModelWindowWrapper.openPageWindow("../../html/patients/createRosterAppointment.html", profileLbl, popW, popH, true, closeRosterAppointment);
    }
}
function closeRosterAppointment(evt,returnData){
    getPatientRosterAppointments();
}
function getPatientRosterAppointments(){
    var patientRosterURL = ipAddress+"/homecare/reports/";
    getAjaxObject(patientRosterURL,"GET",onGetRosterAppList,onError);
}
function onGetRosterAppList(dataObj){
    var ptArray = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.reports){
            if($.isArray(dataObj.response.reports)){
                ptArray = dataObj.response.reports;
            }else{
                ptArray.push(dataObj.response.reports);
            }
        }
    }
    for(var i=0;i<ptArray.length;i++){
        if(ptArray[i].id == selItem.idk)
        {
            var pItem = ptArray[0];
            if(pItem){
                $("#txtID").val(pItem.id);
                $("#txtRN").val(pItem.name);
                $("#txtNotes").val(pItem.notes);
                $("#txtAPI").val(pItem.api);
            }
        }
    }
}
function getAccountList(dataObj){
    console.log(dataObj);
    var dataArray = [];
    //var tempDataArry = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if($.isArray(dataObj.response.provider)){
            dataArray = dataObj.response.provider;
        }else{
            dataArray.push(dataObj.response.provider);
        }
    }else{
        //customAlert.error("Error",dataObj.response.status.message);
    }
    /*for(var j=0;j<tempDataArry.length;j++){
        var item = tempDataArry[j];
        dataArray.push(item.provider);
    }*/

    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
            var createdDate = new Date(dataArray[i].createdDate);
            var createdDateString = createdDate.toLocaleDateString();
            var modifiedDate = new Date(dataArray[i].modifiedDate);
            var modifiedDateString = modifiedDate.toLocaleDateString();
            dataArray[i].createdDateString = createdDateString;
            dataArray[i].modifiedDateString = modifiedDateString;
        }
    }
    //buildAccountListGrid(dataArray);
    if(operation == "add"){
        $('#btnSave').html('<span><img src="../../img/medication/Save.png" class="providerLink-btn-img" />Save</span>');
        $('.tabContentTitle').text('Add Provider');
        $('#btnReset').show();
        var billActName = $("#txtBAN").val();
        $("#txtBAN").val(billActName);
    }else{
        parentRef.patientId = selItem.idk;
        profileLbl = "Edit Provider";
        $('.tabContentTitle').text('Edit Provider');
        $('#btnReset').hide();
        $('#btnSave').html('<span><img src="../../img/medication/Save.png" class="providerLink-btn-img" />Update</span>');
    }
}
function onStatusChange() {
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if (cmbStatus && cmbStatus.selectedIndex < 0) {
        cmbStatus.select(0);
    }
}

function buttonEvents() {
    $("#tabsUL li a[data-toggle='tab'").off("click");
    $("#tabsUL li a[data-toggle='tab'").on("click",onClickTabs);

    $("#btnCancel").off("click", onClickCancel);
    $("#btnCancel").on("click", onClickCancel);

    $("#btnSave").off("click", onClickSave);
    $("#btnSave").on("click", onClickSave);

    $("#btnSearch").off("click", onClickSearch);
    $("#btnSearch").on("click", onClickSearch);

    $("#btnReset").off("click", onClickReset);
    $("#btnReset").on("click", onClickReset);

    $('body').on('click','.qualAddRemoveLink.addQual', function() {
        var $trLen = $('.qualificationTabWrapper table tbody tr').length;
        $('.qualificationTabWrapper table tbody').append('<tr class="qualWrapper_li qualificationTabWrapper-li-'+$trLen+'"><td class="qual-td-addRemove"><span><a href="#" class="qualAddRemoveLink addQual">+</a> <a href="#" class="qualAddRemoveLink removeQual">-</a></span></td><td><input type="text" name="qualWrap-sno" value="'+$trLen+'" readonly /></td><td><input type="text" name="qualWrap-qualification" id="qualWrapQualification" maxlength="100" value="" /></td><td><input type="text" name="qualWrap-university" id="qualWrapUniversity" maxlength="100" value="" /></td><td><input type="text" name="qualWrap-percentage" id="qualWrapPercentage" value="" /></td><td><input type="text" name="qualWrap-passedout" id="qualWrapPassedout" value="" style="width: auto !important; padding-left: 0 !important;" /></td><td><input type="text" name="qualWrap-expirydate" value="" style="width: auto !important; padding-left: 0 !important;" /></td></tr>');
        allowOnlyDecimals($('[name="qualWrap-percentage"]'));
        $('.qualificationTabWrapper-li-'+$trLen).find('[name="qualWrap-passedout"]').kendoDatePicker();
        $('.qualificationTabWrapper-li-'+$trLen).find('[name="qualWrap-expirydate"]').kendoDatePicker();
        modifySerialNumber();
    });
    $('body').on('click','.qualAddRemoveLink.removeQual', function() {
        $(this).closest('tr').remove();
        /*$('.qualificationTabWrapper table tbody').append('<span><a href="#" class="qualAddRemoveLink addQual">+</a> <a href="#" class="qualAddRemoveLink removeQual">-</a><tr><td><input type="text" name="qualWrap-sno" value="'+$trLen+'" /></td><td><input type="text" name="qualWrap-qualification" value="" /></td><td><input type="text" name="qualWrap-university" value="" /></td><td><input type="text" name="qualWrap-percentage" value="" /></td><td><input type="text" name="qualWrap-passedout" value="" /></td><td><input type="text" name="qualWrap-expirydate" value="" /></td></tr></span>');*/
        modifySerialNumber();
    });

}

function onClickTabs(e){
    var tabHref = $(e.target).attr("id");
    console.log($(e.target).text());
    $("#btnSave").show();
    $("#btnReset").show();
    if($(e.target).text() == "Roster"){
        $("#btnSave").hide();
        $("#btnReset").hide();
    }
}
function modifySerialNumber() {
    $('.qualificationTabWrapper [name="qualWrap-sno"]').each(function(i) {
        $(this).val(i + 1);
    });
}
function adjustHeight(){
    var defHeight = 200;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    if(angularPTUIgridWrapper){
        angularPTUIgridWrapper.adjustGridHeight(cmpHeight);
    }

}

function onClickActivities() {
    var popW = "72%";
    var popH = "70%";

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Provider Activities";
    devModelWindowWrapper.openPageWindow("../../html/masters/providerActivityList.html", profileLbl, popW, popH, true, onCloseActivities);
}
function onClickAvailability() {
    if(operation == UPDATE) {
        var popW = "62%";
        var popH = "70%";

        var profileLbl;
        var devModelWindowWrapper = new kendoWindowWrapper();
        profileLbl = "Provider Availability";
        devModelWindowWrapper.openPageWindow("../../html/masters/providerAppointment.html", profileLbl, popW, popH, true, onCloseAvailability);
    }
}
function onClickVacation() {
    if(operation == UPDATE) {
        var popW = "55%";
        var popH = "70%";

        var profileLbl;
        var devModelWindowWrapper = new kendoWindowWrapper();
        profileLbl = "Provider Vacation";
        devModelWindowWrapper.openPageWindow("../../html/masters/vacation.html", profileLbl, popW, popH, true, onCloseVacation);
    }
}
function onCloseActivities() {

}
function onCloseAvailability() {

}
function onCloseVacation() {

}

function onClickZipSearch() {
    var popW = 600;
    var popH = 500;

    parentRef.searchZip = true;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search Zip";
    var cntry = sessionStorage.countryName;
    if(cntry.indexOf("India")>=0){
        profileLbl = "Search Postal Code";
    }else if(cntry.indexOf("United Kingdom")>=0){
        profileLbl = "Search Postal Code";
    }else if(cntry.indexOf("United State")>=0){
        profileLbl = "Search Zip";
    }else{
        profileLbl = "Search Zip";
    }
    devModelWindowWrapper.openPageWindow("../../html/masters/zipList.html", profileLbl, popW, popH, true, onCloseSearchZipAction);
}
var zipSelItem = null;
var cityId = "";

function onCloseSearchZipAction(evt, returnData) {
    console.log(returnData);
    if (returnData && returnData.status == "success") {
        //console.log();
        $("#cmbZip").val("");
        $("#txtZip4").val("");
        $("#txtState").val("");
        $("#txtCountry").val("");
        $("#txtCity").val("");
        var selItem = returnData.selItem;
        if (selItem) {
            zipSelItem = selItem;
            if (zipSelItem) {
                cityId = zipSelItem.idk;
                if (zipSelItem.zip) {
                    $("#cmbZip").val(zipSelItem.zip);
                }
                if (zipSelItem.zipFour) {
                    $("#txtZip4").val(zipSelItem.zipFour);
                }
                if (zipSelItem.state) {
                    $("#txtState").val(zipSelItem.state);
                }
                if (zipSelItem.country) {
                    $("#txtCountry").val(zipSelItem.country);
                }
                if (zipSelItem.city) {
                    $("#txtCity").val(zipSelItem.city);
                }
            }
        }
    }
}

function getZip() {
    getAjaxObject(ipAddress + "/city/list?is-active=1", "GET", getZipList, onError);
}

function getZipList(dataObj) {
    //var dArray = getTableListArray(dataObj);
    var dArray = [];
    if (dataObj && dataObj.response && dataObj.response.cityext) {
        if ($.isArray(dataObj.response.cityext)) {
            dArray = dataObj.response.cityext;
        } else {
            dArray.push(dataObj.response.cityext);
        }
    }
    if (dArray && dArray.length > 0) {
        //setDataForSelection(dArray, "cmbZip", onZipChange, ["zip", "id"], 0, "");
        onZipChange();
    }
    getPrefix();
}

function getPrefix() {
    getAjaxObject(ipAddress + "/master/Prefix/list?is-active=1", "GET", getCodeTableValueList, onError);
}

function onComboChange(cmbId) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb && cmb.selectedIndex < 0) {
        cmb.select(0);
    }
}

function onPrefixChange() {
    onComboChange("cmbPrefix");
}
function onSuffixChange() {
    onComboChange("cmbSuffix");
}
function onUserIdChange() {
    onComboChange("cmbUserId");
}

function onPtChange() {
    onComboChange("cmbStatus");
}

function onGenderChange() {
    onComboChange("cmbGender");
}

function onSMSChange() {
    onComboChange("cmbSMS");
}

function onLanChange() {
    onComboChange("cmbLang");
}

function onRaceChange() {
    onComboChange("cmbRace");
}

function onEthnicityChange() {
    onComboChange("cmbEthnicity");
}
function onReligionChange() {
    onComboChange("cmbReligion");
}

function onZipChange() {
    onComboChange("cmbZip");
    var cmbZip = $("#cmbZip").data("kendoComboBox");
    if (cmbZip) {
        var dataItem = cmbZip.dataItem();
        if (dataItem) {
            $("#txtZip4").val(dataItem.zipFour);
            $("#txtCountry").val(dataItem.country);
            $("#txtState").val(dataItem.state);
            $("#txtCity").val(dataItem.city);
        }
    }
}

function getCodeTableValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbPrefix", onPrefixChange, ["value", "idk"], 0, "");
    }
    getAjaxObject(ipAddress + "/master/Suffix/list?is-active=1", "GET", getCodeTableSuffixValueList, onError);
}
function getCodeTableSuffixValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbSuffix", onSuffixChange, ["value", "idk"], 0, "");
    }
    getAjaxObject(ipAddress + "/user/list?is-active=1&is-deleted=0&user-type-id=100,200,201", "GET", getUserIdValueList, onError);
}
function getUserIdValueList(dataObj) {
    var dArray = getTableUserListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbUserId", onUserIdChange, ["firstAndLastName", "userIdDup"], 0, "");
    }
    getAjaxObject(ipAddress + "/master/Status/list?is-active=1", "GET", getPatientStatusValueList, onError);
}

function getPatientStatusValueList(dataObj) {
    var dArray = getTableFirstListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbStatus", onPtChange, ["desc", "idk"], 0, "");
    }
    getAjaxObject(ipAddress + "/master/Gender/list?is-active=1", "GET", getGenderValueList, onError);
}

function getGenderValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbGender", onGenderChange, ["desc", "idk"], 0, "");
    }
    getAjaxObject(ipAddress + "/master/SMS/list?is-active=1", "GET", getSMSValueList, onError);
}

function getSMSValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbSMS", onSMSChange, ["desc", "idk"], 0, "");
    }
    getAjaxObject(ipAddress + "/master/Language/list?is-active=1", "GET", getLanguageValueList, onError);
}

function getLanguageValueList(dataObj) {
    var dArray = getTableFirstListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setMultipleDataForSelection(dArray, "cmbLang", onLanChange, ["desc", "idk"], 0, "");
    }
    getAjaxObject(ipAddress + "/master/Race/list?is-active=1", "GET", getRaceValueList, onError);
}

function getRaceValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbRace", onRaceChange, ["desc", "idk"], 0, "");
    }
    getAjaxObject(ipAddress + "/master/Ethnicity/list?is-active=1", "GET", getEthnicityValueList, onError);
}

function getEthnicityValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbEthnicity", onEthnicityChange, ["desc", "idk"], 0, "");
    }
    getAjaxObject(ipAddress + "/master/Religion/list?is-active=1", "GET", getReligionValueList, onError);
}
function getReligionValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbReligion", onReligionChange, ["desc", "idk"], 0, "");
    }
    getAjaxObject(ipAddress + "/facility/list?is-active=1", "GET", getFacilityList, onError);
}

function getFacilityList(dataObj) {
    console.log(dataObj);
    var dataArray = [];
    if (dataObj) {
        if ($.isArray(dataObj.response.facility)) {
            dataArray = dataObj.response.facility;
        } else {
            dataArray.push(dataObj.response.facility);
        }
    }
    var tempDataArry = [];
    var obj = {};
    obj.name = "";
    obj.idk = "";
    tempDataArry.push(obj);
    for (var i = 0; i < dataArray.length; i++) {
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if (dataArray[i].isActive == 1) {
            dataArray[i].Status = "Active";
            var obj = dataArray[i];
            obj.idk = dataArray[i].id;
            obj.status = dataArray[i].Status;
            tempDataArry.push(obj);
        }
    }
    dataArray = tempDataArry;

    setDataForSelection(dataArray, "txtFAN", onFacilityChange, ["name", "idk"], 0, "");
    onFacilityChange();
    if (operation == UPDATE && patientId != "") {
        getPatientInfo();
        // getEditInfo();
    }
}

function onFacilityChange() {
    var txtFAN = $("#txtFAN").data("kendoComboBox");
    if (txtFAN) {
        var txtFId = txtFAN.value();
        getAjaxObject(ipAddress + "/facility/list?id=" + txtFId, "GET", onGetFacilityInfo, onError);
    }
}
var billActNo = "";

function onGetFacilityInfo(dataObj) {
    billActNo = "";
    if (dataObj && dataObj.response && dataObj.response.facility) {
        $("#txtBAN").val(dataObj.response.facility[0].name);
        billActNo = dataObj.response.facility[0].billingAccountId;
    }
}

function getPatientInfo() {
    getAjaxObject(ipAddress + "/provider/list?id=" + patientId, "GET", onGetPatientInfo, onError);
}

function getComboListIndex(cmbId, attr, attrVal) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb) {
        var ds = cmb.dataSource;
        var totalRec = ds.total();
        for (var i = 0; i < totalRec; i++) {
            var dtItem = ds.at(i);
            if (dtItem && dtItem[attr] == attrVal) {
                cmb.select(i);
                return i;
            }
        }
    }
    return -1;
}


function onGetPatientInfo(dataObj) {

    patientInfoObject = dataObj.response.provider[0];
    var communicationLen = patientInfoObject.communications != null ? patientInfoObject.communications.length : 0;
    var cityIndexVal = "";
    for(var i=0; i<communicationLen; i++) {
        if(patientInfoObject.communications[i].parentTypeId == 500) {
            cityIndexVal = i;
        }
    }
    $("#txtID").val(patientInfoObject.id);
    $("#txtExtID1").val(patientInfoObject.externalId1);
    $("#txtExtID2").val(patientInfoObject.externalId2);
    getComboListIndex("cmbPrefix", "value", patientInfoObject.prefix);
    $("#txtFN").val(patientInfoObject.firstName);
    $("#txtLN").val(patientInfoObject.lastName);
    $("#txtMN").val(patientInfoObject.middleName);
    $("#txtWeight").val(patientInfoObject.weight);
    $("#txtHeight").val(patientInfoObject.height);
    // $("#txtNN").val(patientInfoObject.contactNickName);
    $("#txtAbbreviation").val(patientInfoObject.abbreviation);
    /*$("#txtN").val(patientInfoObject.name);
    $("#txtDN").val(patientInfoObject.displayName);
    $("#txtFTI").val(patientInfoObject.federalTaxId);
    $("#txtFTEIN").val(patientInfoObject.federalTaxEin);
    $("#txtSTI").val(patientInfoObject.stateTaxId);
    $("#txtNOTT").val(patientInfoObject.taxName);*/
    /*getComboListIndex("cmbFTIT", "desc", patientInfoObject.federalTaxIdType);*/
    getComboListIndex("cmbStatus", "desc", patientInfoObject.Status);
    getComboListIndex("cmbSuffix", "value", patientInfoObject.suffix);
    getComboListIndex("cmbUserId", "userIdDup", patientInfoObject.userId);
    getComboListIndex("txtType", "Key", patientInfoObject.type)
    //  $("#txtSMS").val(comObj.sms);
    //getComboListIndex("cmbSMS", "desc", patientInfoObject.sms);

    if(patientInfoObject.communications != null) {
        $("#cityIdHiddenValue").val(patientInfoObject.communications[cityIndexVal].cityId);
        $("#txtCountry").val(patientInfoObject.communications[cityIndexVal].country);
        $("#cmbZip").val(patientInfoObject.communications[cityIndexVal].zip);
        $("#txtZip4").val(patientInfoObject.communications[cityIndexVal].zipFour);
        $("#txtState").val(patientInfoObject.communications[cityIndexVal].state);
        $("#txtCity").val(patientInfoObject.communications[cityIndexVal].city);
        $("#txtHPhone").val(patientInfoObject.communications[cityIndexVal].homePhone);
        $("#txtExtension").val(patientInfoObject.communications[cityIndexVal].workPhoneExt);
        $("#txtAdd1").val(patientInfoObject.communications[cityIndexVal].address1);
        $("#txtAdd2").val(patientInfoObject.communications[cityIndexVal].address2);
        $("#txtWPhone").val(patientInfoObject.communications[cityIndexVal].workPhone);
        $("#txtExtension1").val(patientInfoObject.communications[cityIndexVal].homePhoneExt);
        $("#txtFax").val(patientInfoObject.communications[cityIndexVal].fax);
        $("#txtCell").val(patientInfoObject.communications[cityIndexVal].cellPhone);
        /*$("#cmbSMS").val(patientInfoObject.communications[cityIndexVal].sms);*/
        getComboListIndex("cmbSMS", "desc", patientInfoObject.communications[cityIndexVal].sms);
        $("#txtEmail").val(patientInfoObject.communications[cityIndexVal].email);
        getComboListIndex("cmbZip", "idk", patientInfoObject.communications[cityIndexVal].cityId);
        zipSelItem = patientInfoObject.communications[cityIndexVal];
        zipSelItem.idk = patientInfoObject.communications[cityIndexVal].id;
    }

    $("#txtFC").val(patientInfoObject.financeCharges);
    $("#txtBD").val(patientInfoObject.billableDoctor);
    $("#txtPCP").val(patientInfoObject.pcp);
    $("#txtTI").val(patientInfoObject.taxonomyId);
    $("#txtNPI").val(patientInfoObject.npi);
    $("#txtDEA").val(patientInfoObject.dea);
    $("#txtUPIN").val(patientInfoObject.upin);
    $("#txtNT").val(patientInfoObject.nuucType);
    $("#txtAS").val(patientInfoObject.amaSpeciality);
    $("#txtNEIC").val(patientInfoObject.neicSpeciality);
    $("#txtBAN").val(patientInfoObject.billingAccountName);

    getComboListIndex("cmbGender", "desc", patientInfoObject.gender);
    getComboListIndex("cmbEthnicity", "desc", patientInfoObject.ethnicity);
    getComboListIndex("cmbRace", "desc", patientInfoObject.race);
    getComboListMultipleIndex("cmbLang", "desc", patientInfoObject.language);
    getComboListIndex("cmbReligion", "desc", patientInfoObject.religion);
    $('#txtLikes').val(patientInfoObject.likes),
        $('#txtDisLikes').val(patientInfoObject.dislikes),
        $('#txtSkills').val(patientInfoObject.skills),
        $('#txtHR').val(patientInfoObject.hourlyRate),
        patientInfoObject.travelExpenses == "1" ? $('#txtTE').attr('checked','true') : null;
    $('#txtHRperMile').val(patientInfoObject.ratePerMile),
        getComboListIndex("txtFAN", "idk", patientInfoObject.facilityId);
    onFacilityChange();
    getAjaxObject(ipAddress+"/homecare/qualifications/?parentId="+ patientInfoObject.id + "&parentTypeId=500","GET",getQualificationListData,onError);
}

function getTableListArray(dataObj) {
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.codeTable) {
        if ($.isArray(dataObj.response.codeTable)) {
            dataArray = dataObj.response.codeTable;
        } else {
            dataArray.push(dataObj.response.codeTable);
        }
    }
    var tempDataArry = [];
    var obj = {};
    obj.desc = "";
    obj.zip = "";
    obj.value = "";
    obj.idk = "";
    tempDataArry.push(obj);
    for (var i = 0; i < dataArray.length; i++) {
        if (dataArray[i].isActive == 1) {
            var obj = dataArray[i];
            obj.idk = dataArray[i].id;
            obj.status = dataArray[i].Status;
            tempDataArry.push(obj);
        }
    }
    return tempDataArry;
}
function getTableFirstListArray(dataObj) {
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.codeTable) {
        if ($.isArray(dataObj.response.codeTable)) {
            dataArray = dataObj.response.codeTable;
        } else {
            dataArray.push(dataObj.response.codeTable);
        }
    }
    var tempDataArry = [];
    for (var i = 0; i < dataArray.length; i++) {
        if (dataArray[i].isActive == 1) {
            var obj = dataArray[i];
            obj.idk = dataArray[i].id;
            obj.status = dataArray[i].Status;
            tempDataArry.push(obj);
        }
    }
    return tempDataArry;
}
function getTableUserListArray(dataObj) {
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.user) {
        if ($.isArray(dataObj.response.user)) {
            dataArray = dataObj.response.user;
        } else {
            dataArray.push(dataObj.response.user);
        }
    }
    var tempDataArry = [];
    var obj = {};
    obj.firstAndLastName = "";
    obj.idk = "";
    obj.userTypeId = "";
    obj.userIdDup = "";
    tempDataArry.push(obj);
    for (var i = 0; i < dataArray.length; i++) {
        if (dataArray[i].isActive == 1) {
            var obj = dataArray[i];
            obj.idk = obj.id;
            obj.status = obj.Status;
            obj.userIdDup = obj.idk;
            if(obj.userTypeId == 100) {
                obj.firstAndLastName = obj.firstName + ", " + obj.lastName + " - Doctor";
            }
            if(obj.userTypeId == 200) {
                obj.firstAndLastName = obj.firstName + ", " + obj.lastName + " - Nurse";
            }
            if(obj.userTypeId == 201) {
                obj.firstAndLastName = obj.firstName + ", " + obj.lastName + " - Caregiver";
            }
            tempDataArry.push(obj);
        }
    }
    return tempDataArry;
}

function onClickReset() {
    if(operation == ADD) {
        operation = ADD;
    }
    else {
        operation = UPDATE;
    }
    patientId = "";
    $("#txtID").val("");
    $("#txtRN").val("");
    $("#txtNotes").val("");
    $("#txtAPI").val("");
}

function setComboReset(cmbId) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb) {
        cmb.select(0);
    }
}

function validation() {
    var flag = true;
    var txtFC = $("#txtFC").val();
    txtFC = $.trim(txtFC);
    if (txtFC == "") {
        customAlert.error("Error", "Enter Finance charges");
        flag = false;
        return false;
    }
    if (cityId == "") {
        customAlert.error("Error", "Select City ");
        flag = false;
        return false;
    }
    /*var strAbbr = $("#txtAbbrevation").val();
    	strAbbr = $.trim(strAbbr);
    	if(strAbbr == ""){
    		customAlert.error("Error","Enter Abbrevation");
    		flag = false;
    		return false;
    	}
    	var strCode = $("#txtCode").val();
    	strCode = $.trim(strCode);
    	if(strCode == ""){
    		customAlert.error("Error","Enter Code");
    		flag = false;
    		return false;
    	}
    	var strName = $("#txtName").val();
    	strName = $.trim(strName);
    	if(strName == ""){
    		customAlert.error("Error","Enter Country");
    		flag = false;
    		return false;
    	}*/

    return flag;
}

function getComboDataItem(cmbId) {
    var dItem = null;
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb && cmb.dataItem()) {
        dItem = cmb.dataItem();
        return cmb.text();
    }
    return "";
}

function getComboDataItemValue(cmbId) {
    var dItem = null;
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb && cmb.dataItem()) {
        dItem = cmb.dataItem();
        return cmb.value();
    }
    return "";
}

function onClickSave() {

    $('.alert').remove();
    var strId = $("#txtID").val();
    var strReportName = $("#txtRN").val();
    var strNotes = $("#txtNotes").val();
    var strAPI = $("#txtAPI").val();


    var dataObj = {};
    dataObj.createdBy = sessionStorage.userId;
    dataObj.name = strReportName;
    dataObj.api = strAPI;
    dataObj.notes = strNotes;


    /*if (operation == UPDATE) {}*/

    if (operation == UPDATE) {
        var communicationLen = selItem.communications != null ? selItem.communications.length : 0;
        var cityIndexVal = "";
        //var dupCityIndexVal = "";
        for(var i=0; i<communicationLen; i++) {
            if(selItem.communications[i].parentTypeId == 500) {
                cityIndexVal = i;
            }
            /*if(selItem.communications[i].parentTypeId == 501) {
                dupCityIndexVal = i;
            }*/
        }
        if(cityIndexVal != "" || (cityIndexVal == 0 && selItem.communications != null)) {
            comObj.id = selItem.communications[cityIndexVal].id;
            comObj.modifiedBy = sessionStorage.userId;
            comObj.parentId = selItem.idk;
        }
        /*if(dupCityIndexVal != "") {
            comObj1.id = selItem.communications[dupCityIndexVal].id;
            comObj1.modifiedBy = sessionStorage.userId;
            comObj1.parentId = selItem.idk;
        }*/
        //comm1.modifiedDate = new Date().getTime();
    }
    else {
        dataObj.createdBy = Number(sessionStorage.userId);
    }

    //comm.push(comObj);
    //dataObj.communications = comm;
    if(strReportName != "" && strNotes != "" && strAPI != "") {
        if (operation == ADD) {
            dataObj.isDeleted = "0";
            dataObj.isActive = "1";
            var dataUrl = ipAddress + "/homecare/reports/";
            createAjaxObject(dataUrl, dataObj, "POST", onCreate, onError);
        } else if (operation == UPDATE) {
            dataObj.modifiedBy = sessionStorage.userId;
            dataObj.id = selItem.idk;
            dataObj.isDeleted = "0";
            var dataUrl = ipAddress + "/homecare/reports/";
            createAjaxObject(dataUrl, dataObj, "PUT", onUpdate, onError);
        }

    }
    else {
        $("html,body").animate({
            scrollTop: 0
        }, 500);
        $('.customAlert').append('<div class="alert alert-danger">Please fill all the Required Fields</div>');
    }
    /*}*/
}
function onTypeChange() {

}
function buildQualificationListGrid(dataSource) {
    for(var i = 0; i < dataSource.length; i++) {
        var $trLen = i;
        var passDateString = new Date(dataSource[i].dateOfPass).toLocaleDateString().split('/').join('-') || "";
        var expiryDateString = new Date(dataSource[i].dateOfExpiry).toLocaleDateString().split('/').join('-') || "";
        $('.qualificationTabWrapper table tbody').append('<tr class="qualWrapper_li qualificationTabWrapper-li-'+$trLen+'" data-attr-qualId="'+dataSource[i].idk+'"><td class="qual-td-addRemove"><span><a href="#" class="qualAddRemoveLink addQual">+</a> <a href="#" class="qualAddRemoveLink removeQual">-</a></span></td><td><input type="text" name="qualWrap-sno" value="'+$trLen+'" readonly /></td><td><input type="text" name="qualWrap-qualification" id="qualWrapQualification" maxlength="100" value="'+dataSource[i].qualification+'" /></td><td><input type="text" name="qualWrap-university" id="qualWrapUniversity" maxlength="100" value="'+dataSource[i].university+'" /></td><td><input type="text" name="qualWrap-percentage" id="qualWrapPercentage" value="'+dataSource[i].percentage+'" /></td><td><input type="text" name="qualWrap-passedout" id="qualWrapPassedout" value="'+passDateString+'" style="width: auto !important; padding-left: 0 !important;" /></td><td><input type="text" name="qualWrap-expirydate" value="'+expiryDateString+'" style="width: auto !important; padding-left: 0 !important;" /></td></tr>');
        allowOnlyDecimals($('[name="qualWrap-percentage"]'));
        $('.qualificationTabWrapper-li-'+$trLen).find('[name="qualWrap-passedout"]').kendoDatePicker({
            change: function (e) {
                selectedDate = $('.qualificationTabWrapper-li-'+$trLen).find('[name="qualWrap-passedout"]').data("kendoDatePicker").value();
            }
        });
        //$('.qualificationTabWrapper-li-'+$trLen).find('[name="qualWrap-passedout"]').data("kendoDatePicker").value(selectedDate);
        $('.qualificationTabWrapper-li-'+$trLen).find('[name="qualWrap-expirydate"]').kendoDatePicker({
            change: function (e) {
                selectedDate = $('.qualificationTabWrapper-li-'+$trLen).find('[name="qualWrap-expirydate"]').data("kendoDatePicker").value();
            }
        });
        //$('.qualificationTabWrapper-li-'+$trLen).find('[name="qualWrap-expirydate"]').data("kendoDatePicker").value(selectedDate);
        modifySerialNumber();
    }
}

function onCreate(dataObj) {
    console.log(dataObj);
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            var obj = {};
            obj.status = "success";
            obj.operation = operation;
            //popupClose(obj);
            if(ABBREVIATION_Patient != "") {
                getAjaxObject(ipAddress+"/provider/list?is-active=1","GET",getProviderListData,onError);
            }
        } else {
            customAlert.error("error", dataObj.response.status.message);
        }
    }
    /*var obj = {};
    obj.status = "success";
    obj.operation = operation;
    popupClose(obj);*/
}
function getProviderListData(dataObj) {
    console.log(dataObj);
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if($.isArray(dataObj.response.provider)){
            dataArray = dataObj.response.provider;
        }else{
            dataArray.push(dataObj.response.provider);
        }
    }
    for(var i=dataArray.length - 1; i >= 0; i--){
        if(ABBREVIATION_Patient == dataArray[i].abbreviation) {
            qualifyFunction(dataArray[i].id, "create");
            break;
        }
    }
    var obj = {};
    obj.status = "success";
    obj.operation = operation;
    popupClose(obj);
}
function getQualificationListData(dataObj) {
    console.log(dataObj);
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if($.isArray(dataObj.response.qualifications)){
            dataArray = dataObj.response.qualifications;
        }else{
            dataArray.push(dataObj.response.qualifications);
        }
    }
    for(var i=0 ; i < dataArray.length ; i++){
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
            var createdDate = new Date(dataArray[i].createdDate);
            var createdDateString = createdDate.toLocaleDateString();
            var modifiedDate = new Date(dataArray[i].modifiedDate);
            var modifiedDateString = modifiedDate.toLocaleDateString();
            dataArray[i].createdDateString = createdDateString;
            dataArray[i].modifiedDateString = modifiedDateString;
        }
    }
    buildQualificationListGrid(dataArray);
}
function qualifyFunction(id, testString) {
    $('.qualificationTabWrapper .qualWrapper_li').each(function() {
        var expiryDate = $(this).find('[name="qualWrap-expirydate"]').val();
        var passDate = $(this).find('[name="qualWrap-passedout"]').val();
        var expiryDateVal = new Date(expiryDate).getTime();
        var passDateVal = new Date(passDate).getTime();
        var obj = {
            "qualification": $(this).find('[name="qualWrap-qualification"]').val(),
            "dateOfExpiry": expiryDateVal,
            "parentTypeId": 500,
            "university": $(this).find('[name="qualWrap-university"]').val(),
            "isActive": 1,
            "isDeleted": 0,
            "parentId": id,
            "createdBy": Number(sessionStorage.userId),
            "percentage": Number($(this).find('[name="qualWrap-percentage"]').val()),
            "dateOfPass": passDateVal
        }
        var dataUrl = ipAddress + "/homecare/qualifications/";
        if(testString == "create") {
            createAjaxObjectAsync(dataUrl, obj, "POST", onCreateQualification, onError);
        } else if(testString == "update") {
            if($(this).attr('data-attr-qualId') != undefined) {
                obj.modifiedBy = Number(sessionStorage.userId);
                obj.id = Number($(this).attr('data-attr-qualId'));
                createAjaxObjectAsync(dataUrl, obj, "PUT", onUpdateQualification, onError);
            } else {
                createAjaxObjectAsync(dataUrl, obj, "POST", onCreateQualification, onError);
            }
        }
    });
}
function onCreateQualification(dataObj) {
    console.log(dataObj);
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            if($('.alert-qualify') && $('.alert-qualify').length == 0) {
                $('.customAlert').append('<div class="alert alert-success alert-qualify">'+dataObj.response.status.message+'</div>');
            } else {
                $('.alert.alert-alert-qualify').text(dataObj.response.status.message);
            }
        } else {
            //$('.customAlert').append('<div class="alert alert-danger">'+dataObj.response.status.message+'</div>');
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}
function onUpdateQualification(dataObj) {
    console.log(dataObj);
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            if($('.alert-qualify') && $('.alert-qualify').length == 0) {
                $('.customAlert').append('<div class="alert alert-success alert-qualify">'+dataObj.response.status.message+'</div>');
            } else {
                $('.alert.alert-qualify').text(dataObj.response.status.message);
            }
        } else {
            //$('.customAlert').append('<div class="alert alert-danger">'+dataObj.response.status.message+'</div>');
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}
function onUpdate(dataObj){
    console.log(dataObj);
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            /*var obj = {};
            obj.status = "success";
            obj.operation = operation;
            popupClose(obj);*/
            $('.customAlert').append('<div class="alert alert-success">'+dataObj.response.status.message+'</div>');
            $('.tabContentTitle').text('Update Provider');
            var id = $('#txtID').val();
            if(ABBREVIATION_Patient != "") {
                qualifyFunction(id, "update");
            }
            getAjaxObject(ipAddress+"/provider/list?id="+id,"GET",updateList,onError);
        } else {
            //$('.customAlert').append('<div class="alert alert-danger">'+dataObj.response.status.message+'</div>');
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}
function updateList(dataObj) {
    console.log(dataObj);
    selItem = dataObj.response.provider[0];
    selItem.idk = selItem.id;
    getAjaxObject(ipAddress+"/provider/list?is-active=1","GET",getAccountList,onError);
}

function onError(errObj) {
    console.log(errObj);
    customAlert.error("Error", "Error");
}

function onClickCancel() {
    var obj = {};
    obj.status = "false";
    popupClose(obj);
}

function onClickSearch() {
    var obj = {};
    obj.status = "search";
    popupClose(obj);
}

function popupClose(obj) {
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}
function validateEmail(sEmail) {
    console.log(sEmail);
    var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    if (filter.test(sEmail)) {
        return true;
    }
    else {
        return false;
    }
}

function allowOnlyDecimals(selector) {
    selector.keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 95 && e.which != 46) {
            return false;
        }
    });
}

function buildPatientRosterList(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "ID",
        "field": "idk",
    });
    gridColumns.push({
        "title": "Name",
        "field": "name",
    });
    gridColumns.push({
        "title": "Notes",
        "field": "notes",
    });
    gridColumns.push({
        "title": "API",
        "field": "api"
    });
    angularPTUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}
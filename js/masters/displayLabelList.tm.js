var angularUIgridWrapper;
var parentRef = null;
var recordType = "1";
var dataArray = [];
$(document).ready(function(){
    $("#divMain",parent.document).css("display","none");
    $("#divPatInfo",parent.document).css("display","");
    $("#lblTitleName",parent.document).html("Display Name");
    parentRef = parent.frames['iframe'].window;
    //recordType = parentRef.dietType;
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("dgridDLabelList", dataOptions);
    angularUIgridWrapper.init();
    buildDeviceListGrid([]);
});

$(window).load(function(){
    loading = false;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
    init();
    buttonEvents();
    adjustHeight();
}

function init(){
    // getAjaxObject(ipAddress+"/file-resource/list/"+recordType,"GET",getFileResourceList,onError);
}
function onError(errorObj){
    console.log(errorObj);
}
function getFileResourceList(dataObj){
    console.log(dataObj);
    dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.fileResource){
        if($.isArray(dataObj.response.fileResource)){
            dataArray = dataObj.response.fileResource;
        }else{
            dataArray.push(dataObj.response.fileResource);
        }
    }
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        if(!dataArray[i].fileType){
            dataArray[i].fileType = "";
        }
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
        }
    }
    //getDietList();
    buildDeviceListGrid([]);
    setTimeout(function(){
        buildDeviceListGrid(dataArray);
    })
}

function getDietList(){
    var tempArray = [];
    for(var i=0;i<dataArray.length;i++){
        if(dataArray[i].recordType == recordType){
            tempArray.push(dataArray[i]);
        }
    }
    buildDeviceListGrid(tempArray);
}
function buttonEvents(){
    $("#btnSave").off("click",onClickOK);
    $("#btnSave").on("click",onClickOK);

    $("#btnCancelDet").off("click",onClickCancel);
    $("#btnCancelDet").on("click",onClickCancel);

    $("#btnDelete").off("click",onClickDelete);
    $("#btnDelete").on("click",onClickDelete);

    $("#btnAdd").off("click");
    $("#btnAdd").on("click",onClickAdd);

    $("#btnDiet").off("click");
    $("#btnDiet").on("click",onClickDiet);

    $("#btnExcersize").off("click");
    $("#btnExcersize").on("click",onClickExcersize);

    $("#btnillness").off("click");
    $("#btnillness").on("click",onClickillness);
}
function onClickDiet(){
    onDiet();
    recordType = "1";
    init();
}
function onClickExcersize(){
    onExcersize();
    recordType = "2";
    init();
}
function onClickillness(){
    onIllness();
    recordType = "5";
    init();
}
function onDiet(){
    $("#btnDiet").addClass("selectButtonBarClass");
    $("#btnExcersize").removeClass("selectButtonBarClass");
    $("#btnillness").removeClass("selectButtonBarClass");
}
function onExcersize(){
    $("#btnDiet").removeClass("selectButtonBarClass");
    $("#btnExcersize").addClass("selectButtonBarClass");
    $("#btnillness").removeClass("selectButtonBarClass");
}
function onIllness(){
    $("#btnDiet").removeClass("selectButtonBarClass");
    $("#btnExcersize").removeClass("selectButtonBarClass");
    $("#btnillness").addClass("selectButtonBarClass");
}
function adjustHeight(){
    var defHeight = 160;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    angularUIgridWrapper.adjustGridHeight(cmpHeight);
}

function buildDeviceListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "ID",
        "field": "idk",
    });
    gridColumns.push({
        "title": "Screen Name",
        "field": "sn",
    });
    gridColumns.push({
        "title": "Field Name",
        "field": "fd",
    });
    gridColumns.push({
        "title": "Display Name",
        "field": "dn",
    });
    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}
var prevSelectedItem =[];
function onChange(){

}

function showUTubeTemplateVideo(){
    var node = '<div style="text-align:center;">';
    node = node+'<label  ng-hide="((row.entity.youtubeLink ==\'\'))" class="cusrsorStyle" style="color:#000;font-size:15px;padding-top:5px" onClick="onClickUTube()">{{row.entity.youtubeLink}}</label>';
    node = node+"</div>";
    return node;
}
function showVideoTemplate(){
    var node = '<div style="text-align:center;">';
    node = node+'<img  ng-hide="((row.entity.fileType ==\'\' || row.entity.fileType ==\'pdf\' || row.entity.fileType == \'PDF\'))" src="../../img/AppImg/HosImages/video.png" class="videoIcon cusrsorStyle" onClick="onClickVideo()"></img>';
    node = node+'<img  ng-hide="((row.entity.fileType ==\'\' || row.entity.fileType ==\'mp3\' || row.entity.fileType == \'MP3\' || row.entity.fileType == \'MP4\' || row.entity.fileType == \'mp4\'))" src="../../img/AppImg/HosImages/pdf.png" class="videoIcon cusrsorStyle" onClick="onClickVideo()"></img>';
    node = node+"</div>";
    return node;
}

function onClickUTube(){
    var selGridRow = angular.element($(event.currentTarget).parent()).scope();
    console.log(selGridRow);
    if(selGridRow && selGridRow.row && selGridRow.row.entity){
        var popW =900;
        var popH = 580;
        var profileLbl;
        var devModelWindowWrapper = new kendoWindowWrapper();

        profileLbl = "Video";
        parentRef.sType = "illness";
        parentRef.illUrlPath = selGridRow.row.entity.youtubeLink;
        devModelWindowWrapper.openPageWindow("../../html/patients/showVideo.html", profileLbl, popW, popH, true, closeVideoScreen);
    }
}
function onClickVideo(){
    var selGridRow = angular.element($(event.currentTarget).parent()).scope();
    console.log(selGridRow);

    if(selGridRow && selGridRow.row && selGridRow.row.entity){
        /*if(recordType == "1" || recordType == "2"){
            parentRef.sType = "diet";
            parentRef.dietId = selGridRow.row.entity.idk;
        }*/
        var popW =900;
        var popH = 580;
        var profileLbl;
        var devModelWindowWrapper = new kendoWindowWrapper();
        if(selGridRow.row.entity.fileType && selGridRow.row.entity.fileType.toLowerCase() == "pdf"){
            profileLbl = "Document";
            popW = 1100;
            parentRef.sType = "diet";
            parentRef.dietId = selGridRow.row.entity.idk;
            devModelWindowWrapper.openPageWindow("../../html/patients/showPdf.html", profileLbl, popW, popH, true, closeVideoScreen);
        }else if(selGridRow.row.entity.fileType && selGridRow.row.entity.fileType.toLowerCase() == "mp4"){
            profileLbl = "Video";
            parentRef.sType = "diet";
            parentRef.dietId = selGridRow.row.entity.idk;
            devModelWindowWrapper.openPageWindow("../../html/patients/showVideo.html", profileLbl, popW, popH, true, closeVideoScreen);
        }else if(selGridRow.row.entity.fileType && selGridRow.row.entity.fileType.toLowerCase() == "mp3"){
            parentRef.audioId = selGridRow.row.entity.idk;
            var popW = 600;
            var popH = 200;
            var profileLbl;
            var devModelWindowWrapper = new kendoWindowWrapper();
            profileLbl = "Audio";
            devModelWindowWrapper.openPageWindow("../../html/patients/showAudio.html", profileLbl, popW, popH, true, closeVideoScreen);
        }
    }

    //selGridRow.row.entity
}

function closeVideoScreen(evt,returnData){

}
function onClickOK(){
    setTimeout(function(){
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if(selectedItems && selectedItems.length>0){
            var obj = {};
            obj.selItem = selectedItems[0];
            selFileResourceDataItem = selectedItems[0];;
            operation = "update";
            addFileResource();
        }
    })
}
function onClickDelete(){
    customAlert.confirm("Confirm", "Are you sure to delete?",function(response){
        if(response.button == "Yes"){
            var selectedItems = angularUIgridWrapper.getSelectedRows();
            console.log(selectedItems);
            if(selectedItems && selectedItems.length>0){
                var selItem = selectedItems[0];
                if(selItem){
                    var dataUrl = ipAddress+"/file-resource/delete/";
                    var reqObj = {};
                    reqObj.id = selItem.idk;
                    reqObj.isDeleted = "1";
                    reqObj.modifiedBy = "101";

                    var strDataObj = JSON.stringify(reqObj);
                    var formData = new FormData();
                    formData.append("fileResource",strDataObj);
                    Loader.showLoader();
                    $.ajax({
                        url: dataUrl,
                        type: 'POST',
                        data: formData,
                        processData: false,
                        contentType:false,// "multipart/form-data",
                        // contentType: "application/json",
                        success: function(data, textStatus, jqXHR){
                            if(typeof data.error === 'undefined'){
                                Loader.hideLoader();
                                submitForm(data);
                            }else{
                                Loader.hideLoader();
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown){
                            Loader.hideLoader();
                        }
                    });
                    //createAjaxObject(dataUrl,reqObj,"POST",onDeleteFileResource,onError);
                }
            }
        }
    });
}
function submitForm(dataObj){
    if(dataObj && dataObj.response){
        if(dataObj.response.status){
            if(dataObj.response.status.code == "1"){
                customAlert.error("Info","File deleted successfully");
                init();
            }else{
                customAlert.error("Error",dataObj.response.status.message);
            }
        }else{
            customAlert.error("Error","Error");
        }
    }
}
var operation = "add";
function onClickAdd(){
    var popW = "44%";
    var popH = 320;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Add Display Name";
    devModelWindowWrapper.openPageWindow("../../html/masters/createDisplayName.html", profileLbl, popW, popH, true, onCloseCreateDietAction);
}
var selFileResourceDataItem = null;
function addFileResource(){
    var popW = "50%";
    var popH = 400;

    parentRef.operation = operation;
    parentRef.selItem = selFileResourceDataItem;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Add File";
    devModelWindowWrapper.openPageWindow("../../html/masters/createDiet.html", profileLbl, popW, popH, true, onCloseCreateDietAction);
}
function onCloseCreateDietAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        if(returnData.operation == "add"){
            customAlert.info("Info","File uploaded successfully.")
        }else if(returnData.operation == "update"){
            customAlert.info("Info","File modified successfully.")
        }
        init();
    }
}
function onClickCancel(){
    var obj = {};
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}

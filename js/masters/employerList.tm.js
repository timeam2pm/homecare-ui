var angularUIgridWrapper;

$(document).ready(function(){
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("dgridEmployerList", dataOptions);
    angularUIgridWrapper.init();
    buildDeviceListGrid([]);
});


$(window).load(function(){
    loading = false;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
    init();
    buttonEvents();
    adjustHeight();
}

function init(){
    getAjaxObject(ipAddress+"/employer/list/","GET",getCountryList,onError);
}
function onError(errorObj){
    console.log(errorObj);
}
function getCountryList(dataObj){
    console.log(dataObj);
    var dataArray = [];
    if(dataObj){
        if($.isArray(dataObj.response)){
            dataArray = dataObj.response;
        }else{
            dataArray.push(dataObj.response);
        }
    }
    var tempDataArry = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
        }
    }
    buildDeviceListGrid(dataArray);
}
function buttonEvents(){
    $("#btnSave").off("click",onClickOK);
    $("#btnSave").on("click",onClickOK);

    $("#btnCancelDet").off("click",onClickCancel);
    $("#btnCancelDet").on("click",onClickCancel);

    $("#btnDelete").off("click",onClickDelete);
    $("#btnDelete").on("click",onClickDelete);

    $("#btnAdd").off("click");
    $("#btnAdd").on("click",onClickAdd);
}

function adjustHeight(){
    var defHeight = 120;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    angularUIgridWrapper.adjustGridHeight(cmpHeight);
}

function buildDeviceListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Abbrevation",
        "field": "abbr",
        "width":"25%"
    });
    gridColumns.push({
        "title": "Code",
        "field": "code",
        "width":"25%"
    });
    gridColumns.push({
        "title": "Country",
        "field": "country",
        "width":"25%"
    });
    gridColumns.push({
        "title": "Status",
        "field": "Status",
        "width":"25%"
    });
    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}
var prevSelectedItem =[];
function onChange(){

}

function onClickOK(){
    setTimeout(function(){
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if(selectedItems && selectedItems.length>0){
            var obj = {};
            obj.selItem = selectedItems[0];
            // var onCloseData = new Object();
            obj.status = "success";
            obj.operation = "ok";
            var windowWrapper = new kendoWindowWrapper();
            windowWrapper.closePageWindow(obj);
        }
    })
}
function onClickDelete(){
    customAlert.confirm("Confirm", "Are you sure you want delete?",function(response){
        if(response.button == "Yes"){
            var selectedItems = angularUIgridWrapper.getSelectedRows();
            console.log(selectedItems);
            if(selectedItems && selectedItems.length>0){
                var selItem = selectedItems[0];
                if(selItem){
                    var dataUrl = ipAddress+"/country/delete/";
                    var reqObj = {};
                    reqObj.id = selItem.id;
                    reqObj.abbr = selItem.abbr;
                    reqObj.country = selItem.country;
                    reqObj.code = selItem.code;
                    reqObj.isDeleted = "1";
                    reqObj.modifiedBy = "101";
                    createAjaxObject(dataUrl,reqObj,"POST",onDeleteCountryt,onError);
                }
            }
        }
    });
}
function onDeleteCountryt(dataObj){
    if(dataObj && dataObj.status){
        if(dataObj.status.code == "1"){
            customAlert.info("info", "Country Deleted Successfully");
            init();
        }else{
            customAlert.error("error", dataObj.message);
        }
    }

}
function onClickAdd(){
    /*var obj = {};
     obj.status = "Add";
     obj.operation = "ok";
        var windowWrapper = new kendoWindowWrapper();
        windowWrapper.closePageWindow(obj);*/
    var popW = 600;
    var popH = 500;

    var profileLbl = "Add Employer";
    var devModelWindowWrapper = new kendoWindowWrapper();

    devModelWindowWrapper.openPageWindow("../../html/masters/createEmployer.html", profileLbl, popW, popH, true, closeAddEmployerAction);
}
function closeAddEmployerAction(){

}
function onClickCancel(){
    var obj = {};
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}

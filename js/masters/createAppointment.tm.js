var angularPTUIgridWrapper = null;
var facilityArr = [{Key:'Facility1',Value:'Facility1'},{Key:'Facility2',Value:'Facility2'}];

var doctorArr = [{Key:'Doctor1',Value:'Doctor1'},{Key:'Doctor2',Value:'Doctor2'}];

var appTypeArr = [{Key:'Type1',Value:'Type1'},{Key:'Type2',Value:'Type2'}];

var towArr = [{Key:'1',Value:'1'},{Key:'2',Value:'2'},{Key:'3',Value:'3'},{Key:'4',Value:'4'},{Key:'5',Value:'5'},{Key:'6',Value:'6'},{Key:'7',Value:'7'}];
var colors = ['red', 'blue', 'green', 'teal', 'rosybrown', 'tan', 'plum', 'saddlebrown'];

var DAY_VIEW = "DayView";
var appTypeArray = [];
var reasonArray = [];

var dArray = [];
var parentRef = null;

var startDT  = null;
var endDT = null;

$(document).ready(function(){
});

$(window).load(function(){
    if(sessionStorage.clientTypeId == "2"){
        $("#lblAppByPatient").text("Appointment By Client");
        $("#lblPTT").text("Client :");
        $("#lblPTR").text("Client :");
    }
    parentRef = parent.frames['iframe'].window;
    onMessagesLoaded();
});

function onFacilityChange(){

}

function getColor(data) {
    if (data.title === "Mark A Henry") {
        return "Pink";
    } else {
        return "orange";
    }
}
function adjustHeight(){
    var defHeight = 260;
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 100;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    try{angularPTUIgridWrapper.adjustGridHeight(cmpHeight);}catch(e){};

    $("#prscheduler1").css({height: (cmpHeight+90) + 'px' });
    $("#prscheduler").css({height: (cmpHeight+80) + 'px' });
    $("#prschedulerH").css({height: (cmpHeight+80) + 'px' });
}
function onMessagesLoaded() {
    buttonEvents();
    init();
}

var stDate = "";
var endDate = "";
var strDoc = "";

function scrollToHour(hour) {
    var time = new Date();
    time.setHours(hour);
    time.setMinutes(0);
    time.setSeconds(0);
    time.setMilliseconds(0);

    var scheduler = $("#scheduler").data("kendoScheduler");
    var contentDiv = scheduler.element.find("div.k-scheduler-content");
    var rows = contentDiv.find("tr");

    for (var i = 0; i < rows.length; i++) {
        var slot = scheduler.slotByElement(rows[i]);

        var slotTime = kendo.toString(slot.startDate, "HH:mm");
        var targetTime = kendo.toString(time, "HH:mm");

        if (targetTime === slotTime) {
            scheduler.view()._scrollTo($(rows[i]).find("td:first")[0], contentDiv[0]);
        }
    };
}

function getColorBasedOnHour(date) {
    /*var difference = date.getTime() - kendo.date.getDate(date);
    var hours = difference / kendo.date.MS_PER_HOUR;

    if (hours >= 0 && hours <3) {
      return "#e8e0e0";
    } else if (hours >= 13 && hours < 17) {
    } */
    return "#FFF";
}
var cntry = "";
var dtFMT = "dd/MM/yyyy";
function init(){
    //patientId = parentRef.patientId;
    //$("#dtFromDate").kendoDatePicker({value:new Date()});
    //$("#dtToDate").kendoDatePicker({value:new Date()});
    cntry = sessionStorage.countryName;
    if(cntry.indexOf("India")>=0){
        dtFMT = INDIA_DATE_FMT;
    }else  if(cntry.indexOf("United Kingdom")>=0){
        dtFMT = ENG_DATE_FMT;
    }else  if(cntry.indexOf("United State")>=0){
        dtFMT = US_DATE_FMT;
    }
    if(cntry.indexOf("India")>=0){

    }else{
        $("#liAppByPatient").hide();
        $("#liRoster").hide();
    }
    $("#txtStartDate").kendoDatePicker({format:dtFMT});
    $("#txtEndDate").kendoDatePicker({format:dtFMT});
    $("#txtPRDate").kendoDatePicker({format:dtFMT,value:new Date()});

    startDT = $("#txtStartDate").kendoDatePicker({
        change: startChange,format:dtFMT
    }).data("kendoDatePicker");

    endDT= $("#txtEndDate").kendoDatePicker({
        change: endChange,format:dtFMT
    }).data("kendoDatePicker");


    //  startDT.min(new Date());
    endDT.min(new Date());

    $('label[for="recurrenceRule"]').parent().hide();
    $("#cmbFacility").kendoComboBox();
    $("#cmbPtFacility").kendoComboBox();
    $("#cmbDoctor").kendoComboBox();
    $("#cmbPtDoctor").kendoComboBox();
    $("#cmbPatient").kendoComboBox();
    $("#cmbAppTypes").kendoComboBox();
    $("#cmbReason").kendoComboBox();
    $("#txtRFacility").kendoComboBox();
    //$("#txtRPatient").kendoComboBox();
    $("#txtRFecility").kendoComboBox();
    $("#txtRProvider").kendoComboBox();
    $("#txtRDOW").kendoComboBox();
    //$("#txtDuration").kendoComboBox();
    $("#txtRAppType").kendoComboBox();
    $("#txtRReason").kendoComboBox();
    $("#txtStartTime").kendoTimePicker();
    allowNumerics("txtDuration");
    //$("#cmbAppType").kendoComboBox();

    //setDataForSelection(facilityArr, "cmbFacility", onFacilityChange, ["Value", "Key"], 0, "");
    //setDataForSelection(doctorArr, "cmbDoctor", onFacilityChange, ["Value", "Key"], 0, "");
    setDataForSelection(towArr, "txtRDOW", onFacilityChange, ["Value", "Key"], 0, "");

    getAjaxObject(ipAddress+"/master/appointment_type/list/?is-active=1","GET",getAppointmentList,onError);
    getAjaxObject(ipAddress+"/master/appointment_reason/list/?is-active=1","GET",getReasonList,onError);
    getAjaxObject(ipAddress+"/facility/list/?is-active=1","GET",getFacilityList,onError);

    getPatientList();


    var defHeight = 200;
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 100;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;

    var dataOptionsPT = {
        pagination: false,
        paginationPageSize: 500,
        changeCallBack: onPTChange
    }

    angularPTUIgridWrapper = new AngularUIGridWrapper("rGrid", dataOptionsPT);
    angularPTUIgridWrapper.init();
    buildPatientRosterList([]);

    if(angularPTUIgridWrapper){
        angularPTUIgridWrapper.adjustGridHeight(cmpHeight);
    }

    adjustHeight();
    $("#scheduler").kendoScheduler({
        height: cmpHeight,
        slotTemplate: "<div style='background:#=getColorBasedOnHour(date)#; height: 100%;width: 100%;opacity:0.3'></div>",
        footer: false,
        date: new Date(),
        allDaySlot:false,
        currentTimeMarker: false,
        minorTickCount: 1, // display one time slot per major tick
        minorTick: 15,
        majorTick:15,
        messages: {
            deleteWindowTitle: "Delete Appointment",
            editable: {
                confirmation: "Are you sure you want to delete this Appointment?"
            },
            editor: {
                editorTitle: "Create Appointment"
            }
        },
        views: [
            { type: "day", selected: true },
            { type: "week" },
            { type: "month" },
        ],
        dataBound: function(e) {
            //console.log(e.sender._selectedView.options.name);
            DAY_VIEW = e.sender._selectedView.options.name;
            if(DAY_VIEW == "DayView"){
                $($(".k-scheduler-times")[0]).parent().parent().hide();
            }else{
                $($(".k-scheduler-times")[0]).parent().parent().show();
            }

        },
        editable: {
            template: $("#customEditorTemplate").html(),
        },
        schema: {
            model: {
                id: "taskId",
                fields: {
                    taskId: { from: "TaskID", type: "number" },
                    title: { from: "Title", defaultValue: "No title", validation: { required: true } },
                    start: { type: "date", from: "Start" },
                    end: { type: "date", from: "End" },
                    startTimezone: { from: "StartTimezone" },
                    doctor: { type:'string',from: "Doctor" },
                    dob: { type:'string',from: "dob" },
                    endTimezone: { from: "EndTimezone" },
                    description1: { from: "Description1" },
                    recurrenceId: { from: "RecurrenceID" },
                    recurrenceRule: { from: "RecurrenceRule" },
                    recurrenceException: { from: "RecurrenceException" },
                    ownerId: { from: "OwnerID", defaultValue: 'Confirmed' },
                    image: { from: "image", defaultValue: "../../img/AppImg/HosImages/patient1.png" },
                    patientID: { from: "patientID", defaultValue: 1 },
                    isAllDay: { type: "boolean", from: "IsAllDay" }
                }
            }
        },
        remove: scheduler_remove,
        navigate:scheduler_navigate,
        save: scheduler_save,
        cancel: scheduler_cancel,
        eventTemplate: $("#event-template").html(),
        resizeStart: function(e){
            console.log(e.event);
            if(e.event.room){
                e.preventDefault();
            }
        },
        resizeEnd: function(e){
            console.log(e);
        },
        edit: function(e) {// "week","month",
            console.log(e.event);
            if(e.event.room){
                e.preventDefault();
            }
            var required = $("#required").data("kendoMultiSelect");
            if(required){
                dArray = [];
                var arr = required.listView._dataItems;
                for(var i=0;i<arr.length;i++){
                    var item = arr[i];
                    if(item){
                        dArray.push(item.firstName);
                    }
                }
            }
            if(dArray.length == 0){
                customAlert.error("Error", "Please select Providers");
                e.preventDefault();
            }

            var dt = new Date(e.event.start);
            var cnt = getAppointmentStartCount(dt.getTime());
            //e.sender.select().events[0].id
            if(DAY_VIEW == "DayView"){
                if(e.sender && e.sender.select() && e.sender.select().events ){
                    //e.preventDefault();
                    var sEvents = e.sender.select().events;
                    stDate = e.sender.select().start;
                    endDate = e.sender.select().end;
                    if((sEvents.length>0  && sEvents[0].id != "") || patientId != ""){
                        patientId = "";
                        //addAppointment();
                        //return false;
                    }else if(sEvents && (sEvents.length == 0 || sEvents[0].id == "") && patientId == "" && dArray.length>0){
                        e.preventDefault();
                        //patientId = "101";
                        var popW = "60%";
                        var popH = 500;

                        var profileLbl;
                        var devModelWindowWrapper = new kendoWindowWrapper();
                        profileLbl = "Search Patient";
                        if(sessionStorage.clientTypeId == "2"){
                            profileLbl = "Search Client";
                        }
                        devModelWindowWrapper.openPageWindow("../../html/patients/searchPatient.html", profileLbl, popW, popH, true, closeAddAction);
                    }
                }
            }else{
                e.preventDefault();
            }
        },
        selectable: true,
    });
    var scheduler = $("#scheduler").data("kendoScheduler");
    if(scheduler){
        scheduler.setOptions({
            minorTick: 15,
            majorTick:15,
            minorTickCount:1
        });
    }
    setTimeout(function(){
        scheduler.resize(true);
        scrollToHour(9);
        // scheduler.options.selectable = false;
        //scheduler.options.editable = false;
    },2000);

    // var arr = {id: 1, start: new Date("11/5/2017 08:00 AM"), end: new Date("11/5/2017 10:00 AM"), title: "Interview",ownerId:"strDoc",facility:"strFacility",doctor:"strDoc"};
    // scheduler.dataSource.add(arr);

    var operation = parentRef.operation;
    if(operation == "view"){
        //$("#btnSave").hide();
        /*var providerId = parentRef.providerId;
        var fromDate = parentRef.fromDate;
        provider = parentRef.provider;
        facility = parentRef.facility;
        var strDT = kendo.toString(new Date(fromDate), "dd-MMM-yyyy");

        var patientListURL = ipAddress+"/appointment/by-provider/"+providerId+"/"+strDT+"/1";*/
        //getAjaxObject(patientListURL,"GET",onPatientListData,onErrorMedication);

        //https://stage.timeam.com/appointment/by-provider/9/04-nov-2017/5
    }else{

    }
    $(".k-scheduler-times-all-day").each(function () {
        //  $(this).hide();
        // $(this).parent().closest('tr').next('tr').hide();
        //$(this).closest('th').prev('th').attr("rowspan", "1")
    });

    // initPatientSchedule();
}
var scheduleFlag = false;
function onClickTabs(e){
    console.log(e);
    setTimeout(function(){
        if(!scheduleFlag){
            initPatientSchedule();
            scheduleFlag = true;
        }
    })
}
function startChange(){

}
function endChange(){

}
function getPatientList(){
    var patientListURL = ipAddress+"/patient/list/"+sessionStorage.userId;
    if(sessionStorage.userTypeId == "200" || sessionStorage.userTypeId == "100"){
        patientListURL = ipAddress+"/patient/list/?is-active=1";
    }else{

    }
    getAjaxObject(patientListURL,"GET",onPTPatientListData,onErrorMedication);
}
function onPTPatientListData(dataObj){
    var tempArray = [];
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.patient){
            if($.isArray(dataObj.response.patient)){
                tempArray = dataObj.response.patient;
            }else{
                tempArray.push(dataObj.response.patient);
            }
        }
    }else{
        customAlert.error("Error",dataObj.response.status.message);
    }

    for(var i=0;i<tempArray.length;i++){
        var obj = tempArray[i];
        if(obj){
            var dataItemObj = {};
            dataItemObj.PID = obj.id;
            dataItemObj.FN = obj.firstName;
            dataItemObj.LN = obj.lastName;
            dataItemObj.WD = obj.weight;
            dataItemObj.HD = obj.height;

            if(obj.middleName){
                dataItemObj.MN = obj.middleName;
            }else{
                dataItemObj.MN =  "";
            }
            dataItemObj.firstName = dataItemObj.FN+" "+ dataItemObj.MN+" "+dataItemObj.LN;
            dataItemObj.GR = obj.gender;
            if(obj.status){
                dataItemObj.ST = obj.status;
            }else{
                dataItemObj.ST = "ACTIVE";
            }
            dataItemObj.DOB = kendo.toString(new Date(obj.dateOfBirth),"MM/dd/yyyy");

            dataArray.push(dataItemObj);
        }
    }
    var obj = {};
    obj.firstName = "";
    obj.PID = "";
    //dataArray.unshift(obj);
    //setDataForSelection(dataArray, "txtRPatient", onFacilityChange, ["firstName", "PID"], 0, "");
    //getPatientRosterList();
    //buildDeviceTypeGrid(dataArray);
}
var appPatientArray = [];
function onClickPtViews(){
    var txtStartDate = $("#txtStartDate").data("kendoDatePicker");
    if(txtStartDate != ""){
        /*var scheduler = $("#ptscheduler").data("kendoScheduler");
        scheduler.dataSource.data([]);
        var dt = new Date(txtStartDate.value())
        var currentWeekDay = dt.getDay();
        var lessDays = currentWeekDay == 0 ? 6 : currentWeekDay-1
        var wkStart = new Date(new Date(dt).setDate(dt.getDate()- lessDays));
        var wkEnd = new Date(new Date(wkStart).setDate(wkStart.getDate()+6));

        var startDateView = kendo.toString(new Date(wkStart),"dd-MMM-yyyy").toUpperCase();

        var patientListURL = ipAddress+"/appointment/by-patient/"+viewPatientId+"/"+startDateView+"/7"
        getAjaxObject(patientListURL,"GET",function(dataObj){
            var ptArray = [];
            if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
                if(dataObj.response.appointment){
                    if($.isArray(dataObj.response.appointment)){
                        ptArray = dataObj.response.appointment;
                    }else{
                        ptArray.push(dataObj.response.appointment);
                    }
                }
                var pm = 0;
                var am = 0;
                for(var i=0;i<ptArray.length;i++){
                    var item = ptArray[i];
                    var ms = item.dateOfAppointment;
                    var d1 = new Date(ms);
                    var est = item.dateOfAppointment;
                    var st = kendo.toString(new Date(est), "g");
                    var eet = est+(item.duration*60*1000);
                    var et = kendo.toString(new Date(eet), "g");
                    if(d1.getHours()>12){
                        pm++;
                    }else{
                        am++;
                    }
                    var appIdk = item.id;
                    var est = item.dateOfAppointment;
                    var st = kendo.toString(new Date(est), "g");
                    var eet = est+(item.duration*60*1000);
                    var et = kendo.toString(new Date(eet), "g");


                    var strPatient =  viewPatientId+", "+viewpName;

                    var pDOB = viewpDT;//item.composition.patient.dateOfBirth;

                    var appTYpe = item.appointmentType;
                    var appTYpeV = getAppTypeValue(appTYpe, appTypeArray);
                    var note = item.notes;
                    var desc = item.appointmentReason;
                    var descV = getAppTypeValue(desc, reasonArray);
                    var dt = pDOB;//new Date(pDOB);
                    var strAge = viewPTAge;//getAge(dt);
                    var provider = getProviderName(item.providerId);
                     var arr = {description1V:""+desc+"",ownerIdV:""+appTYpe+"",taskId: item.id,notes:note,dob1:pDOB,age:strAge,image:'../../img/AppImg/HosImages/patient1.png',patientID:viewPatientId,id: appIdk, description:appIdk,start: new Date(st), end: new Date(et), title: ""+strPatient+"",ownerId:""+appTYpe+"",facility:""+facility+"",doctor:""+provider+"",description1:""+desc+""};
                      scheduler.dataSource.add(arr);

                }
            }
        },onErrorMedication);*/

        var dt = new Date(txtStartDate.value())  //current date of week
        var currentWeekDay = dt.getDay();
        var lessDays = currentWeekDay == 0 ? 6 : currentWeekDay-1
        var wkStart = new Date(new Date(dt).setDate(dt.getDate()- lessDays));
        var wkEnd = new Date(new Date(wkStart).setDate(wkStart.getDate()+6));

        var startDateView = kendo.toString(new Date(wkStart),"dd-MMM-yyyy").toUpperCase();
        //https://stage.timeam.com/appointment/by-patient/101/22-JAN-2018/7
        var patientListURL = ipAddress+"/appointment/by-patient/"+viewPatientId+"/"+startDateView+"/7"
        getAjaxObject(patientListURL,"GET",function(dataObj){
            var sdt = wkStart;
            var first = kendo.toString(new Date(sdt),"MM/dd/yyyy");
            var fDay = wkStart.getDay();
            $("#ptscheduler").text("");
            var strTable = '<table class="table">';
            strTable = strTable+'<thead class="fillsHeader">';
            strTable = strTable+'<tr>';
            strTable = strTable+'<th class="textAlign whiteColor">'+first +" (Mon) "+'</th>';
            sdt = new Date(new Date(wkStart).setDate(wkStart.getDate()+1));
            var sDay = sdt.getDay();
            var second = kendo.toString(new Date(sdt),"MM/dd/yyyy");
            strTable = strTable+'<th class="textAlign whiteColor">'+second+" (Tue) "+'</th>';
            sdt = new Date(new Date(wkStart).setDate(wkStart.getDate()+2));
            var tDay = sdt.getDay();
            var third = kendo.toString(new Date(sdt),"MM/dd/yyyy");
            strTable = strTable+'<th class="textAlign whiteColor">'+third +" (Wed) "+'</th>';
            sdt = new Date(new Date(wkStart).setDate(wkStart.getDate()+3));
            var foDay = sdt.getDay();
            var four = kendo.toString(new Date(sdt),"MM/dd/yyyy");
            strTable = strTable+'<th class="textAlign whiteColor">'+four +" (Thu) "+'</th>';
            sdt = new Date(new Date(wkStart).setDate(wkStart.getDate()+4));
            var fiDay = sdt.getDay();
            var five = kendo.toString(new Date(sdt),"MM/dd/yyyy");
            strTable = strTable+'<th class="textAlign whiteColor">'+five +" (Fri) "+'</th>';
            sdt = new Date(new Date(wkStart).setDate(wkStart.getDate()+5));
            var sixDay = sdt.getDay();
            var six = kendo.toString(new Date(sdt),"MM/dd/yyyy");
            strTable = strTable+'<th class="textAlign whiteColor">'+six +" (Sat) "+'</th>';
            sdt = new Date(new Date(wkStart).setDate(wkStart.getDate()+6));
            var sevenDay = sdt.getDay();
            var seven = kendo.toString(new Date(sdt),"MM/dd/yyyy");
            strTable = strTable+'<th class="textAlign whiteColor">'+seven +" (Sun) "+'</th>';
            strTable = strTable+'</tr>';
            strTable = strTable+'</thead>';
            strTable = strTable+'<tbody>';
            console.log(dataObj);
            var ptArray = [];
            if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
                if(dataObj.response.appointment){
                    if($.isArray(dataObj.response.appointment)){
                        ptArray = dataObj.response.appointment;
                    }else{
                        ptArray.push(dataObj.response.appointment);
                    }
                }
            }
            appPatientArray = ptArray;
            for(var p=0;p<ptArray.length;p++){
                var item = ptArray[p];
                item.ST = "0";
                item.PR = "0";
            }
            var fDayArr = [];
            var sDayArr = [];
            var tDayArr = [];
            var foDayArr = [];
            var fiDayArr = [];
            var sixDayArr = [];
            var seventhDayArr = [];
            var col = columnSequenceSort(ptArray);
            var rowArray = [];
            for(var p=0;p<ptArray.length;p++){
                //console.log(ptArray[p]);
                var item = ptArray[p];
                if(item){
                    var ms = item.dateOfAppointment;
                    if(rowArray.toString().indexOf(ms) == -1){
                        rowArray.push(ms);
                    }
                    /*var dt = new Date(item.dateOfAppointment);
                    if(dt.getDay() == fDay){
                        fDayArr.push(item);
                    }else if(dt.getDay() == sDay){
                        sDayArr.push(item);
                    }else if(dt.getDay() == tDay){
                        tDayArr.push(item);
                    }else if(dt.getDay() == foDay){
                        foDayArr.push(item);
                    }else if(dt.getDay() == fiDay){
                        fiDayArr.push(item);
                    }else if(dt.getDay() == sixDay){
                        sixDayArr.push(item);
                    }else if(dt.getDay() == sevenDay){
                        seventhDayArr.push(item);
                    }*/
                }
            }
            console.log(rowArray.length);
            var rows = 0;
            /*if(fDayArr.length>rows){
                rows = fDayArr.length;
            }
            if(sDayArr.length>rows){
                rows = sDayArr.length;
            }
            if(tDayArr.length>rows){
                rows = tDayArr.length;
            }
            if(foDayArr.length>rows){
                rows = foDayArr.length;
            }
            if(fiDayArr.length>rows){
                rows = fiDayArr.length;
            }
            if(sixDayArr.length>rows){
                rows = sixDayArr.length;
            }
            if(seventhDayArr.length>rows){
                rows = seventhDayArr.length;
            }*/
            console.log(rows);
            rows = 0;
            for(var r=0;r<rowArray.length;r++){
                strTable = strTable+'<tr>';
                strTable = strTable+'<td style="text-align:center">'+getPatientDayInfo(rowArray[r],fDay,ptArray)+'</td>';
                strTable = strTable+'<td style="text-align:center">'+getPatientDayInfo(rowArray[r],sDay,ptArray)+'</td>';
                strTable = strTable+'<td style="text-align:center">'+getPatientDayInfo(rowArray[r],tDay,ptArray)+'</td>';
                strTable = strTable+'<td style="text-align:center">'+getPatientDayInfo(rowArray[r],foDay,ptArray)+'</td>';
                strTable = strTable+'<td style="text-align:center">'+getPatientDayInfo(rowArray[r],fiDay,ptArray)+'</td>';
                strTable = strTable+'<td style="text-align:center">'+getPatientDayInfo(rowArray[r],sixDay,ptArray)+'</td>';
                strTable = strTable+'<td style="text-align:center">'+getPatientDayInfo(rowArray[r],sevenDay,ptArray)+'</td>';
                strTable = strTable+'</tr>';
            }

            strTable = strTable+'</tbody>';
            strTable = strTable+'</table>';
            $("#ptscheduler").append(strTable);
        },onErrorMedication);

    }
}
function allowNumerics(ctrlId){
    $("#"+ctrlId).keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
}
function getPatientDayInfo(ms,pDay,ptArray){
    var strData = "";
    for(var p=0;p<ptArray.length;p++){
        //console.log(ptArray[p]);
        var item = ptArray[p];
        if(item && item.ST == "0"){
            var dt = new Date(item.dateOfAppointment);
            if(dt.getDay() == pDay && ms == item.dateOfAppointment){
                item.ST = "1";
                //strData = "1";
                var st1 = "";
                var ms = item.dateOfAppointment;
                var d1 = new Date(ms);
                var est = item.dateOfAppointment;
                var st = kendo.toString(new Date(est), "g");
                var eet = est+(item.duration*60*1000);
                var et = kendo.toString(new Date(eet), "g");
                /*if(d1.getHours()>12){
                    //pm++;
                    st1 = "Evening Visit";
                }else{
                    //am++;
                    st1 = "Moring Visit";
                }*/
                var appIdk = item.id;
                var est = item.dateOfAppointment;
                var st = kendo.toString(new Date(est), "t");
                var eet = est+(item.duration*60*1000);
                var et = kendo.toString(new Date(eet), "t");

                var appTYpe = item.appointmentType;
                var appTYpeV = getAppTypeValue(appTYpe, appTypeArray);
                var note = item.notes;
                var desc = item.appointmentReason;
                var descV = getAppTypeValue(desc, reasonArray);
                //var dt = pDOB;//new Date(pDOB);
                //var strAge = viewPTAge;//getAge(dt);
                var provider = getProviderNames(ms,pDay,ptArray,item.providerId);//getProviderName(item.providerId);
                strData = provider+"<br>";
                strData = strData+st+" - "+et+" - "+item.duration+"<br>";
                //strData = strData+""+item.duration+"<br>";
                strData = strData+""+appTYpeV+" - "+descV+"<br>";
                //strData = strData+""+descV+"<br>";
                //strData = strData+""+st1+"<br>";
                strData = strData+"<a href='#' id="+appIdk+" onclick=onPAppEdit(event) style=font-weight:bold>View</a>&nbsp;&nbsp;&nbsp;";

                return strData;
            }
        }
    }
    return "";
}
//appPatientArray
var appPatientItem =  null;
function onPAppEdit(event){
    console.log(event);
    var prArray = [];
    var appIDK = event.currentTarget.id;
    for(var a=0;a<appPatientArray.length;a++){
        var item = appPatientArray[a];
        if(item && item.id == appIDK){
            appPatientItem = item;
            prArray.push(item.providerId);
            break;
        }
    }
    console.log(appPatientItem);
    var ms = appPatientItem.dateOfAppointment;
    var dt = new Date(ms);
    if(dt){
        $("#liAppByProvider").addClass("active");
        $("#tab1").addClass("active");

        $("#liAppByPatient").removeClass("active");
        $("#tab2").removeClass("active");

        var scheduler = $("#scheduler").data("kendoScheduler");
        if(scheduler){
            //scheduler.setOptions({ date: dt });
            //scheduler.view(scheduler.view().name);
            scheduler.date(dt);
            scrollToHour(dt.getHours());
        }
    }
    var required = $("#required").data("kendoMultiSelect");
    if(required){
        required.value(prArray);
    }
    onClickViews();
}
function getProviderNames(ms,pDay,ptArray,prID){
    var pNames = "";
    for(var p=0;p<ptArray.length;p++){
        var item = ptArray[p];
        if(item && item.PR == "0"){
            var ms = item.dateOfAppointment;
            var d1 = new Date(ms);
            if(ms == ms && d1.getDay() == pDay ){//&& prID == item.providerId
                var provider = getProviderName( item.providerId);
                pNames = provider;
                item.PR = "1";
                return pNames;
            }
        }
    }
    if(pNames.length>0){
        pNames = pNames.substring(0, pNames.length-1);
    }
    return pNames;
}
function initPatientSchedule(){
    var defHeight = 200;
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 100;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;



    $("#divPT").css({height: cmpHeight + 'px' });
    $("#ptscheduler1").css({height: cmpHeight + 'px' });
    $("#ptscheduler").css({height: cmpHeight + 'px' });
    /*$("#ptscheduler").kendoScheduler({
         height: cmpHeight,
         footer: false,
          date: new Date(),
          allDaySlot:false,
          currentTimeMarker: false,
          minorTickCount: 1, // display one time slot per major tick
           minorTick: 60,
           majorTick:60,
           messages: {
                deleteWindowTitle: "Delete Appointment",
                editable: {
                    confirmation: "Are you sure you want to delete this Appointment?"
                  },
                  editor: {
                      editorTitle: "Create Appointment"
                  }
              },
          views: [
                    { type: "week" , selected: true},
                ],
                dataBound: function(e) {
                    //console.log(e.sender._selectedView.options.name);
                    //DAY_VIEW = e.sender._selectedView.options.name;
                    if(DAY_VIEW == "DayView"){
                        //$($(".k-scheduler-times")[0]).parent().parent().hide();
                    }else{
                        //$($(".k-scheduler-times")[0]).parent().parent().show();
                    }

                },
                editable: {
                    template: $("#customEditorTemplate").html(),
                  },
                  schema: {
                      model: {
                        id: "taskId",
                        fields: {
                          taskId: { from: "TaskID", type: "number" },
                          title: { from: "Title", defaultValue: "No title", validation: { required: true } },
                          start: { type: "date", from: "Start" },
                          end: { type: "date", from: "End" },
                          startTimezone: { from: "StartTimezone" },
                          doctor: { type:'string',from: "Doctor" },
                          dob: { type:'string',from: "dob" },
                          endTimezone: { from: "EndTimezone" },
                          description1: { from: "Description1" },
                          recurrenceId: { from: "RecurrenceID" },
                          recurrenceRule: { from: "RecurrenceRule" },
                          recurrenceException: { from: "RecurrenceException" },
                          ownerId: { from: "OwnerID", defaultValue: 1 },
                          image: { from: "image", defaultValue: "../../img/AppImg/HosImages/patient1.png" },
                          patientID: { from: "patientID", defaultValue: 1 },
                          isAllDay: { type: "boolean", from: "IsAllDay" }
                        }
                      }
                    },
                    remove: scheduler_remove1,
                    navigate:scheduler_navigate1,
                    save: scheduler_save1,
                    cancel: scheduler_cancel1,
                    eventTemplate: $("#event-template").html(),
                    resizeStart: function(e){
                      console.log(e.event);
                      e.preventDefault();
                    },
                    resizeEnd: function(e){
                        console.log(e);
                    },
         edit: function(e) {// "week","month",
            console.log(e.event);
            if(e.event.room){
                e.preventDefault();
            }
         }

    });*/
}
function scheduler_remove1(e){

}
function scheduler_save1(e){

}
function scheduler_navigate1(e){

}
function scheduler_cancel1(e){

}
function getAppointmentList(dataObj){
    var dArray = getTableListArray(dataObj);
    if(dArray && dArray.length>0){
        appTypeArray = dArray;
        setDataForSelection(dArray, "cmbAppTypes", onAppTypeChange, ["desc", "value"], 0, "");
        setDataForSelection(dArray, "txtRAppType", onAppTypeChange, ["desc", "value"], 0, "");
        var cmbAppTypes = $("#cmbAppTypes").data("kendoComboBox");
        if(cmbAppTypes){
            cmbAppTypes.select(1);
        }
    }
}
function getReasonList(dataObj){
    var dArray = getTableListArray(dataObj);
    if(dArray && dArray.length>0){
        reasonArray = dArray;
        setDataForSelection(dArray, "cmbReason", onReasonChange, ["desc", "value"], 0, "");
        setDataForSelection(dArray, "txtRReason", onReasonChange, ["desc", "value"], 0, "");
    }
}
function onAppTypeChange(){
    var cmbAppTypes = $("#cmbAppTypes").data("kendoComboBox");
    if(cmbAppTypes && cmbAppTypes.selectedIndex<0){
        cmbAppTypes.select(0)
    }
}
function onReasonChange(){
    var cmbReason = $("#cmbReason").data("kendoComboBox");
    if(cmbReason && cmbReason.selectedIndex<0){
        cmbReason.select(0)
    }
}
function getTableListArray(dataObj){
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.codeTable){
        if($.isArray(dataObj.response.codeTable)){
            dataArray = dataObj.response.codeTable;
        }else{
            dataArray.push(dataObj.response.codeTable);
        }
    }
    var tempDataArry = [];
    var obj = {};
    obj.desc = "";
    obj.value = "";
    //tempDataArry.push(obj);
    for(var i=0;i<dataArray.length;i++){
        if(dataArray[i].isActive == 1){
            var obj = dataArray[i];
            //obj.idk = dataArray[i].id;
            //obj.status = dataArray[i].Status;
            tempDataArry.push(obj);
        }
    }
    return tempDataArry;
}
function onClickViews(){
    var ms1 = $("#required").getKendoMultiSelect();
    var values = ms1.value();

    if(values.length == 0){
        customAlert.error("Error", "Please select Providers");
        return;
    }else{
        onGetBlockDays();
    }

}
function onClickBlockDays(){
    var popW = "65%";
    var popH = "70%";

    var cmbDoctor = $("#cmbDoctor").data("kendoComboBox");
    var cmbFacility = $("#cmbFacility").data("kendoComboBox");
    parentRef.providerId = cmbDoctor.value();
    parentRef.facilityId = cmbFacility.value();
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Appointment Block Timings";
    var scheduler = $("#scheduler").data("kendoScheduler");
    devModelWindowWrapper.openPageWindow("../../html/patients/appointmentBlock.html", profileLbl, popW, popH, true, closeBlockAddAction);
    //var required = $("#required").data("kendoMultiSelect");
    //console.log(required);
}
var stTime = "";
var endTime = "";
var stDate = "";
var endDate = "";
function closeBlockAddAction(evt,re){
    console.log(re);
    if(re.status == "success"){
        /*stTime = re.ST;
        endTime = re.ET;
        var scheduler = $("#scheduler").data("kendoScheduler");
        stDate = scheduler.date();
        navigateDateTime(stDate,stTime,endTime);*/
    }
    onGetBlockDays();
}
function isDateAreEqual(edt){
    //var scheduler = $("#scheduler").data("kendoScheduler");
    var dt1 = new Date(stDate);
    var dt2 = new Date(edt);
    if(dt1.getTime() == dt2.getTime()){
        return true;
    }
    return false;
}
function navigateDateTime(eDate,stTm,etTm){
    var scheduler = $("#scheduler").data("kendoScheduler");
    //console.log(scheduler.date());
    var dt = eDate;//scheduler.date();
    dt = kendo.toString(dt,"yyyy/MM/dd");
    if(isDateAreEqual(eDate)){
        var st = dt+" "+stTime;
        var et = dt+" "+endTime;
    }else{
        var st = dt+" "+stTm;
        var et = dt+" "+etTm;
    }

    console.log(new Date(st)+","+new Date(et));
    if(scheduler){
        scheduler.setOptions({
            minorTick: 15,
            majorTick:15,
            minorTickCount:1,
            startTime: new Date(st),
            endTime: new Date(et)
        });
        scheduler.view(scheduler.view().name);
    }
}
function scheduler_add(e){
    try{
        console.log(scheduler);
        //var scheduler = $("#scheduler").data("kendoScheduler");
        //scheduler.addEvent({dob:''});
    }catch(ex){}

}
var provider = "";
var facility = "";
function scheduler_navigate(e){
    console.log(e);
    var cmbDoctor = $("#cmbDoctor").data("kendoComboBox");
    var cmbFacility = $("#cmbFacility").data("kendoComboBox");
    var scheduler = $("#scheduler").data("kendoScheduler");
    var dtFromDate = e.date;//new Date();//scheduler.date();//new Date();//$("#dtFromDate").data("kendoDatePicker");
    //var dtFromDate = $("#dtFromDate").data("kendoDatePicker");

    var providerId = cmbDoctor.value();
    var fromDate = dtFromDate;//dtFromDate.value();
    //fromDate = fromDate.getTime();
    //fromDate = getGMTDateFromLocaleDate(fromDate);


    var strDT = kendo.toString(new Date(fromDate), "dd-MMM-yyyy");

    var scheduler = $("#scheduler").data("kendoScheduler");
    scheduler.dataSource.data([]);
    //onGetBlockDays();
    //navigateDateTime(e.date,"12:00 AM", "11:45 PM");
    DAY_VIEW = e.view;
    onGetBlockDays();
    //getBlockDaysByDate(fromDate);
    //onClickAppSearch(fromDate);
    /*if(DAY_VIEW == "month" || DAY_VIEW == "week"){
        $('.k-event').css('cssText', 'width:20% !important;');
        var strDT = "01-DEC-2017";//kendo.toString(new Date(""), "dd-MMM-yyyy");
        var patientListURL = ipAddress+"/appointment/by-provider/"+providerId+"/"+strDT+"/30";
         getAjaxObject(patientListURL,"GET",onPatientListData,onErrorMedication);
    }else{
        $('.k-event').css('cssText', 'width:99% !important;');
        var patientListURL = ipAddress+"/appointment/by-provider/"+providerId+"/"+strDT+"/1";
         getAjaxObject(patientListURL,"GET",onPatientListData,onErrorMedication);
    }*/

}

function getGMTDateFromLocaleDate(sTime){
    var now = new Date(sTime);
    var utc = new Date(now.getTime() + now.getTimezoneOffset() * 60000);
    return utc.getTime();
}
function getLocalTimeFromGMT(sTime){
    var dte = new Date(sTime);
    var dt = new Date(dte.getTime() - dte.getTimezoneOffset()*60*1000);
    return dt.getTime();
}
function getAppTypeValue(strV,arr){
    for(var i=0;i<arr.length;i++){
        var item = arr[i];
        if(item && item["value"] == strV){
            return item["desc"];
        }
    }
    return "";
}
function isObjectExist(am,flag){
    for(var c=0;c<appViewDays.length;c++){
        var item = appViewDays[c];
        if(item){
            if(flag){
                if(item.AM == am){
                    var num = Number(item.AMT);
                    num = num+1;
                    item.AMT = num;
                }
            }else{
                if(item.PM == am){
                    var num = Number(item.PMT);
                    num = num+1;
                    item.PMT = num;
                }
            }
        }
    }
}
function getCountAppViews(am,flag){
    for(var c=0;c<appViewDays.length;c++){
        var item = appViewDays[c];
        if(flag){
            if(item.AM == am){
                return item.AMT;
            }
        }else{
            if(item.PM == am){
                return item.PMT;
            }
        }
    }
    return "";
}
function columnSequenceSort(arrToSort){
    arrToSort.sort(function (item1,item2) {
        //console.log(a,b);
        var seq1 = Number(item1.dateOfAppointment);
        var seq2 = Number(item2.dateOfAppointment);
        return (seq1-seq2);
        //return a[strObjParamToSortBy] > b[strObjParamToSortBy];
    });
}
function isAppointmentExist(pdId,appTime){
    for(var c=0;c<dtArray.length;c++){
        var item = dtArray[c];
        if(item.dateOfAppointment == appTime && pdId == item.providerId){
            return true;
        }
    }
    return false;
}
function getAppointmentIDK(pdId,appTime){
    for(var c=0;c<dtArray.length;c++){
        var item = dtArray[c];
        if(item.dateOfAppointment == appTime && pdId == item.providerId){
            return item;
        }
    }
    return "";
}

function getAppointmentStartCount(ms){
    var count = 0;
    for(var c=0;c<dtArray.length;c++){
        var item = dtArray[c];
        if(item.dateOfAppointment == ms){
            count = count+1;
        }
    }
    return count;
}
var dtArray = [];
function onPatientListData(dataObj){
    console.log(dataObj);
    Loader.showLoader();
    dtArray = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.appointment){
            if($.isArray(dataObj.response.appointment)){
                dtArray = dataObj.response.appointment;
            }else{
                dtArray.push(dataObj.response.appointment);
            }
        }
    }
    //appViewDays
    for(var a=0;a<appViewDays.length;a++){
        var item = appViewDays[a];
        if(item){
            item.AMT = 0;
            item.PMT = 0;
        }
    }
    var scheduler = $("#scheduler").data("kendoScheduler");
    scheduler.dataSource.data([]);
    var cmbFacility = $("#cmbFacility").data("kendoComboBox");
    //var cmbDoctor = $("#cmbDoctor").data("kendoComboBox");

    for(var i=0;i<blArray.length;i++){
        //	var st = kendo.toString(new Date(blArray[i].startDateTime), "g");
        //var et = kendo.toString(new Date(blArray[i].endDateTime), "g");
        var sdt = blArray[i].startDateTime;
        //sdt = getLocalTimeFromGMT(sdt);

        var edt = blArray[i].endDateTime;
        //edt = getLocalTimeFromGMT(edt);

        var st = kendo.toString(new Date(sdt), "g");
        var et = kendo.toString(new Date(edt), "g");
        var arr = {title:"block times",id:blArray[i].id,room:1,start: new Date(st), end: new Date(et)};
        scheduler.dataSource.add(arr);
    }

    var facility  = cmbFacility.text();
    //var provider  = cmbDoctor.text();
    var am = 0;
    var pm = 0;

    /*var ms1 = $("#required").getKendoMultiSelect();
    var values = ms1.value();*/
    var col = columnSequenceSort(dtArray);
    //console.log(dtArray);
    var dtAppArry = [];
    var msArray = [];
    for(var d=0;d<dtArray.length;d++){
        var item = dtArray[d];
        var ms = item.dateOfAppointment;
        if(dtAppArry.length == 0){
            dtAppArry.push(item);
            msArray.push(ms);
        }else if(dtAppArry.length>0){
            var strArray = msArray.toString();
            if(strArray.indexOf(ms) == -1){
                msArray.push(ms);
                dtAppArry.push(item);
            }
        }
    }

    var pdArray = [];
    var required = $("#required").data("kendoMultiSelect");
    if(required){
        //var pdArray = [];
        var arr = required.listView._dataItems;
        for(var i=0;i<arr.length;i++){
            var item = arr[i];
            if(item){
                pdArray.push(item.idk);
            }
        }
    }
    console.log(pdArray);
    console.log(dtAppArry);
    for(var j=0;j<dtAppArry.length;j++){
        for(var i=0;i<pdArray.length;i++){
            var item = dtAppArry[j];
            var ms = item.dateOfAppointment;//getLocalTimeFromGMT(dtArray[i].dateOfAppointment);
            var d1 = new Date(ms);
            var pdId = pdArray[i];
            var est = item.dateOfAppointment;
            var st = kendo.toString(new Date(est), "g");//new Date(dtArray[i].dateOfAppointment);
            var eet = est+(item.duration*60*1000);
            var et = kendo.toString(new Date(eet), "g");//ms+(dtArray[i].duration*60*1000)

            if(isAppointmentExist(pdId,ms)){
                var provider = getProviderName(pdId);
                console.log(provider);
                if(d1.getHours()>=12){
                    pm++;
                    var spnPM = "spnPM"+pdId;
                    isObjectExist(spnPM, false);
                    var cnt = getCountAppViews(spnPM, false);
                    $("#divProviders").find("#"+spnPM).text(cnt);
                }else{
                    am++;
                    var spnAM = "spnAM"+pdId;//item.providerId;
                    isObjectExist(spnAM, true);
                    var cnt = getCountAppViews(spnAM, true);
                    $("#divProviders").find("#"+spnAM).text(cnt);
                }
                var item = getAppointmentIDK(pdId,ms);//dtAppArry[j];
                var appIdk = item.id;//getAppointmentIDK(pdId,ms);
                //var ampm = (hours >= 12) ? "PM" : "AM";
                var est = item.dateOfAppointment;
                //console.log(dtArray[i].id+" "+"Local SEC: "+est+" Local Date:"+new Date(est));
                //est = getLocalTimeFromGMT(est);
                //console.log("UTC SEC: "+est+" UTC:"+new Date(est));
                var st = kendo.toString(new Date(est), "g");//new Date(dtArray[i].dateOfAppointment);

                var eet = est+(item.duration*60*1000);
                //eet = getLocalTimeFromGMT(eet);

                var et = kendo.toString(new Date(eet), "g");//ms+(dtArray[i].duration*60*1000)
                var strPatient = item.composition.patient.id+", "+item.composition.patient.firstName+" "+item.composition.patient.middleName+" "+item.composition.patient.lastName;

                var pDOB = item.composition.patient.dateOfBirth;

                var appTYpe = item.appointmentType;
                var appTYpeV = getAppTypeValue(appTYpe, appTypeArray);
                var note = item.notes;
                var desc = item.appointmentReason;
                var descV = getAppTypeValue(desc, reasonArray);
                var dt = new Date(pDOB);
                var strAge = getAge(dt);
                var provider = getProviderName(pdId);//item.providerId);
                var arr = {description1V:""+desc+"",ownerIdV:""+appTYpe+"",taskId: item.id,notes:note,dob1:pDOB,age:strAge,image:'../../img/AppImg/HosImages/patient1.png',patientID:item.composition.patient.id,id: appIdk, description:appIdk,start: new Date(st), end: new Date(et), title: ""+strPatient+"",ownerId:""+appTYpe+"",facility:""+facility+"",doctor:""+provider+"",description1:""+desc+""};
                //var arr = {id: 1, start: new Date("11/5/2017 08:00 AM"), end: new Date("11/5/2017 11:00 AM"), title: "Interview",ownerId:"strDoc",facility:"strFacility",doctor:"strDoc"};
                scheduler.dataSource.add(arr);
            }else{
                //var arr = {start: new Date(st), end: new Date(et)};
                var arr = {title:"Not created",id:"",room:"",start: new Date(st), end: new Date(et),description:"A1"};
                scheduler.dataSource.add(arr);
            }
        }
    }
    //	console.log(appViewDays);
    $("#spnAM").text(am);
    $("#spnPM").text(pm);

    try{
        setTimeout(function(){
            var events = $(".k-event");
            var cmbDoctor = $("#cmbDoctor").data("kendoComboBox");
            /* setTimeout(function(){
                  //  scheduler.resize(true);
                    if(DAY_VIEW == "DayView"){
                          scheduler.options.selectable = true;
                          scheduler.options.editable = true;
                  }else{
                      scheduler.options.selectable = false;
                      scheduler.options.editable = false;
                  }
                  },2000);*/
            if(DAY_VIEW == "DayView"){
                //$('.k-event').css('width', '25%');
            }else{
                $('.k-event').css('width', '140px');
            }
            var provider  = cmbDoctor.text();
            for(var e=0;e<events.length;e++){
                var pName = $(events[e]).find("#desc1").text();
                pName = $.trim(pName);
                if(pName != ""){
                    var pItem = getPatientAppDetails(pName,dtArray);
                    if(pItem){
                        if(DAY_VIEW == "DayView"){
                            //$('.k-event').css('cssText', 'width:99% !important;');
                            //$(events[e]).find("#imgId").show();
                            //$('.k-event').css('width', '25%');
                            $(events[e]).css('width', '22%');
                            $(events[e]).find("#eventId").show();
                            var imageServletUrl = ipAddress+"/download/patient/photo/"+pItem.patientId+ "/?" + Math.round(Math.random() * 1000000)+"&access_token="+sessionStorage.access_token+"&tenant="+sessionStorage.tenant;
                            $(events[e]).find("#imgId").attr("src",imageServletUrl);
                            $(events[e]).css("background-color",getRandomColor());
                            var pDOB = pItem.composition.patient.dateOfBirth;
                            var dt = new Date(pDOB);
                            var sDT = kendo.toString(dt,"MM/dd/yyyy");
                            var strAge = getAge(dt);
                            strAge = " ("+strAge+")";
                            var appTYpe = pItem.appointmentType;
                            appTYpe = getAppTypeValue(appTYpe, appTypeArray);
                            var gender = pItem.composition.patient.gender;
                            var note = pItem.notes;
                            $(events[e]).find("#pID").text("");
                            pName = pItem.patientId+" - "+pItem.composition.patient.firstName+" "+pItem.composition.patient.middleName+" "+pItem.composition.patient.lastName;
                            var strId = pName;//+" -  "+sDT+" "+strAge+" , "+gender;
                            $(events[e]).find("#pName").text(strId);
                            if(pItem && pItem.providerId){
                                provider = getProviderName(pItem.providerId);
                            }
                            $(events[e]).find("#provider").text(provider);
                            if(appTYpe){
                                $(events[e]).find("#appId").text(appTYpe);
                            }
                            if(note){
                                $(events[e]).find("#notes").text(note);
                            }
                            var tip = "Id: "+pItem.patientId+"\n";
                            tip = tip+"Name: "+pItem.composition.patient.firstName+" "+pItem.composition.patient.middleName+" "+pItem.composition.patient.lastName+"\n";
                            tip = tip+"DOB: "+sDT + strAge+"\n"
                            tip = tip+"Provider : "+provider+"\n";
                            tip = tip+"Appointment Type : "+appTYpe+"\n";
                            tip = tip+"Note : "+note+"\n";

                            //$('.k-event').attr('title', tip);
                            $(events[e]).attr("title",tip);
                            $("#scheduler").find(" .k-event-delete").show();
                            //$("#scheduler").off("dblclick");
                        }else{
                            $('.k-event').css('width', '140px');
                            buttonEvents();
                            $("#scheduler").find(" .k-event-delete").hide();
                            $(events[e]).find("#eventId").hide();
                            //$(events[e]).find("#pID").text("");
                            //$(events[e]).find("#imgId").hide();
                            $(events[e]).css("background-color","green");
                        }
                    }else{
                        //$("#scheduler").find(" .k-event-delete").hide();
                        $(events[e]).find(" .k-event-delete").hide();
                        $(events[e]).css("background-color","white");
                    }
                }else{
                    $(events[e]).css("background-color","#e2e4e3");
                    $(events[e]).find("#eventBId").show();
                }
            }
            Loader.hideLoader();
        },2000);
    }catch(ex){}
    //$('.k-event').css('width', '99%');

    //$(".k-event").css("background-color","red")
}
function getPatientAppDetails(pName,pArray){
    var pNameArray = pName.split(" ");
    var fn = "";
    var mn = "";
    var ln = "";
    if(pNameArray[0]){
        fn = pNameArray[0];
    }
    if(pNameArray[1]){
        mn = pNameArray[1];
    }
    if(pNameArray[2]){
        ln = pNameArray[2];
    }
    for(var i=0;i<pArray.length;i++){
        var pItem = pArray[i];
        var fName = pItem.composition.patient.firstName;
        var mName = pItem.composition.patient.middleName;
        var lName = pItem.composition.patient.lastName;
        if(pItem.id == pName){ // == fn && mName == mn && lName == ln){
            return pItem;
        }
        //var strPatient = dtArray[i].composition.patient.firstName+" "+dtArray[i].composition.patient.middleName+" "+dtArray[i].composition.patient.lastName;
    }
    return null;
}
function convertHex(hex,opacity){
    hex = hex.replace('#','');
    r = parseInt(hex.substring(0,2), 16);
    g = parseInt(hex.substring(2,4), 16);
    b = parseInt(hex.substring(4,6), 16);

    // Add Opacity to RGB to obtain RGBA
    result = 'rgba('+r+','+g+','+b+','+opacity/100+')';
    return result;
}
function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return convertHex(color,30);
}
function getBGRandomColor() {
    var color = '#999999';
    return convertHex(color,30);
}
function onErrorMedication(errobj){
    console.log(errobj);
}
function onError(errorObj){
    console.log(errorObj);
}
function scheduler_change(e){
    //onClickSearch();
    getBlockDaysByDate(e.start);
    //onClickAppSearch(e.start);
}
var isBlockDay = false;
function scheduler_remove(e){
    console.log(e);
    var reqObj = {};
    reqObj.id = Number(e.event.id);
    reqObj.modifiedBy = 101;
    reqObj.isActive = 0;
    reqObj.isDeleted = 1;

    var dataUrl = ipAddress+"/appointment/delete";
    isBlockDay = false;
    if(e.event.room){
        isBlockDay = true;
        dataUrl = ipAddress+"/appointment-block/delete";
    }
    var arr = [];
    arr.push(reqObj);
    createAjaxObject(dataUrl,reqObj,"POST",onDelete,onError);
}
function onDelete(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == 1){
        if(!isBlockDay){
            customAlert.error("Info", "Appointment is deleted successfully");
            var scheduler = $("#scheduler").data("kendoScheduler");
            onClickAppSearch(scheduler.date());
        }else{
            customAlert.error("Info", "Appointment block day deleted successfully");
            onGetBlockDays();
        }
    }else{
        customAlert.error("error", "error");
    }
}
function scheduler_cancel(e){
    console.log(e);
    var dt = e.event.start;
    //getBlockDaysByDate(dt);
    onClickAppSearch(dt);
}
var saveDate = "";
var updateFlag = false;
var appointmentSaveItem = null;
var appProviderArr = [];
var appCount = 0;
var appProviderLength = 0;
function scheduler_save(e){
    console.log(e);
    appCount = 0;
    var cmbDoctor = $("#cmbDoctor").data("kendoComboBox");
    appointmentSaveItem = e.event;
    var doc = appointmentSaveItem.doctor;
    if(doc){
        if(doc.indexOf(",")>=0){
            appProviderArr = doc.split(",");
            appProviderLength = appProviderArr.length;
        }else{
            appProviderArr[0] = doc;
            appProviderLength = 1;
        }
        appointmentSave();
    }
}

function getProviderId(pName){
    var idx = "";
    for(var p=0;p<providerArray.length;p++){
        var item = providerArray[p];
        if(item && item.firstName == pName){
            return item.idk;
        }
    }
    return "";
}
function getProviderName(pk){
    var idx = "";
    for(var p=0;p<providerArray.length;p++){
        var item = providerArray[p];
        if(item && item.idk == pk){
            return item.firstName;
        }
    }
    return "";
}
function appointmentSave(){
    var cmbFacility = $("#cmbFacility").data("kendoComboBox");
    var obj = {};
    obj.createdBy = 101;
    obj.isActive = 1;
    obj.isDeleted = 0;
    var dsItem = appointmentSaveItem;//e.event;
    saveDate = dsItem.start;
    var sDate = new Date(dsItem.start);
    var eDate = new Date(dsItem.end);
    var diff = eDate.getTime() - sDate.getTime();
    diff = diff/1000;
    diff = diff/60;
    var nSec = sDate.getTime();
    var uSec = getGMTDateFromLocaleDate(nSec);

    //console.log("Local SEC:"+nSec+"Local Date:"+new Date(nSec));
    //console.log("UTC SEC:"+uSec+"UTC:"+new Date(uSec));

    obj.dateOfAppointment = nSec;//uSec;//getGMTDateFromLocaleDate(sDate.getTime());//sDate.getTime();//
    //obj.dateOfAppointment1 = sDate.getTime();//
    obj.duration = diff;
    var providerId = getProviderId(appProviderArr[appCount]);
    obj.providerId = Number(providerId);
    obj.facilityId = Number(cmbFacility.value());
    obj.patientId = dsItem.patientID;//parentRef.patientId;
    obj.appointmentType = dsItem.ownerId;
    obj.notes = dsItem.notes;//$("#taArea").val();
    if( dsItem.description1 &&  dsItem.description1.text){
        //$("#taArea").val();;
    }else{
        //obj.appointmentReason = "";
    }
    obj.appointmentReason = dsItem.description1;

    //var flag = eventDatesStartValid( sDate.getTime(),eDate.getTime());
    //console.log(flag);
    var dataUrl = "";
    if(dsItem.id == 0){
        updateFlag = false;
        dataUrl = ipAddress+"/appointment/create";
    }else{
        updateFlag = true;
        obj.id = dsItem.id;
        dataUrl = ipAddress+"/appointment/update";
    }
    var arr = [];
    arr.push(obj);
    createAjaxObject(dataUrl,arr,"POST",onCreate,onError);
}
function eventDatesStartValid(stDate,etDate){
    for(var i=0;i<blArray.length;i++){
        var est = blArray[i].startDateTime;//kendo.toString(new Date(blArray[i].startDateTime), "g");
        var eet = blArray[i].endDateTime;//kendo.toString(new Date(blArray[i].endDateTime), "g");
        var flag = false;
        console.log("st:"+new Date(stDate));
        console.log("et:"+new Date(etDate));

        console.log(new Date(est));
        console.log(new Date(eet));

        if(stDate>=est){
            if(stDate>=eet){

            }else{
                return false;
            }
        }else if(est<=stDate){
            if(eet<=etDate){

            }else{
                return false;
            }
        }

    }
    return true;
}

function getFacilityList(dataObj){
    var dataArray = [];
    if(dataObj){
        if($.isArray(dataObj.response.facility)){
            dataArray = dataObj.response.facility;
        }else{
            dataArray.push(dataObj.response.facility);
        }
    }
    var tempDataArry = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
        }
    }
    setDataForSelection(dataArray, "cmbFacility", onFacilityChange, ["name", "idk"], 0, "");
    setDataForSelection(dataArray, "cmbPtFacility", onFacilityChange, ["name", "idk"], 0, "");
    setDataForSelection(dataArray, "txtRFacility", onFacilityChange, ["name", "idk"], 0, "");
    setDataForSelection(dataArray, "txtPRFacility", onFacilityChange, ["name", "idk"], 0, "");
    onClickViewProviderApp();
    getAjaxObject(ipAddress+"/provider/list/?is-active=1","GET",getProviderList,onError);
}
var checkInputs = function(elements) {
    elements.each(function() {
        var element = $(this);
        var input = element.children("input");
        input.prop("checked", element.hasClass("k-state-selected"));
    });
};
var providerArray = [];
function getProviderList(dataObj){
    console.log(dataObj);
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if($.isArray(dataObj.response.provider)){
            dataArray = dataObj.response.provider;
        }else{
            dataArray.push(dataObj.response.provider);
        }
    }else{
        //customAlert.error("Error",dataObj.response.status.message);
    }
    var tempDataArry = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
        }
    }
    providerArray = dataArray;
    setDataForSelection(dataArray, "cmbDoctor", onFacilityChange, ["firstName", "idk"], 0, "");
    setDataForSelection(dataArray, "cmbPtDoctor", onFacilityChange, ["firstName", "idk"], 0, "");
    setDataForSelection(dataArray, "txtRProvider", onFacilityChange, ["firstName", "idk"], 0, "");
    //<input type='checkbox'/>
    var required = $("#required").kendoMultiSelect({
        itemTemplate: "<div style='width:100px;float:left'>#:data.firstName# </div>",
        autoClose: false,
        dataSource: dataArray,
        dataTextField: "firstName",
        dataValueField: "idk",
        dataBound: function() {
            var items = this.ul.find("li");
            setTimeout(function() {
                //checkInputs(items);
            });
        },
        change: function() {
            /* var items = this.ul.find("li");
             checkInputs(items);*/
            var ms1 = $("#required").getKendoMultiSelect();
            var values = this.value();//distinctValues(this.value());
            // console.log(values);
        }
    }).data("kendoMultiSelect");

    setTimeout(function(){
        var required = $("#required").data("kendoMultiSelect");
        if(required){
            //required.values(['Thomas']);
        }
    })

    //onGetBlockDays();
    //onClickSearch();
}
var appViewDays = [];
function onGetBlockDays(){
    var required = $("#required").data("kendoMultiSelect");
    if(required){
        var dArr = [];
        var arr = required.listView._dataItems;
        for(var i=0;i<arr.length;i++){
            var item = arr[i];
            if(item){
                dArr.push(item);
            }
        }
    }

    appViewDays = [];
    var strProviders = ""
    for(var d=0;d<dArr.length;d++){
        var spnAM = "spnAM"+dArr[d].idk;
        var spnPM = "spnPM"+dArr[d].idk;
        var strDiv = '<div class="col-sm-3 col-md-3 col-lg-3 col-xs-3" style="padding-left:0px;border:1px solid #999;text-align:center">';
        strDiv = strDiv+'<div class="col-sm-4 col-md-4 col-lg-4 col-xs-4" style="padding-left:5px">';
        strDiv = strDiv+'<label class="col-xs-12 col-sm-12 col-md-12 col-lg-12 control-label input-sm noPadding lblFontBold colorStyle">'+dArr[d].firstName+'</label>';
        strDiv = strDiv+"</div>";
        strDiv = strDiv+'<div class="col-sm-3 col-md-3 col-lg-3 col-xs-3" style="padding-left:5px">';
        strDiv = strDiv+'<a href="#" class="colorStyle">AM <span class="badge" id="'+spnAM+'">0</span></a>';
        strDiv = strDiv+"</div>";
        strDiv = strDiv+'<div class="col-sm-3 col-md-3 col-lg-3 col-xs-3" style="padding-left:5px">';
        strDiv = strDiv+'<a href="#" class="colorStyle">PM <span class="badge" id="'+spnPM+'">0</span></a>';
        strDiv = strDiv+"</div>";
        strDiv = strDiv+"</div>";
        strProviders = strProviders+strDiv;
        var obj = {};
        obj.AM = spnAM;
        obj.AMT = 0;
        appViewDays.push(obj);
        var obj = {};
        obj.PM = spnPM;
        obj.PMT = 0;
        appViewDays.push(obj);
    }
    console.log(appViewDays);
    //$("#divProviders").remove();
    $("#divProviders").html("");
    $("#divProviders").html(strProviders);
    var scheduler = $("#scheduler").data("kendoScheduler");
    var dt = new Date(scheduler.date());
    getBlockDaysByDate(dt);
}
function getBlockDaysByDate(dt){
    var cmbFacility = $("#cmbFacility").data("kendoComboBox");
    var cmbDoctor = $("#cmbDoctor").data("kendoComboBox");
    var providerId = cmbDoctor.value();
    var facilityId = cmbFacility.value();

    var day = dt.getDate();
    var month = dt.getMonth();
    month = month+1;
    var year = dt.getFullYear();

    var stDate = month+"/"+day+"/"+year;
    stDate = stDate+" 00:00:00";

    var startDate = new Date(stDate);
    var stDateTime = startDate.getTime();

    //stDateTime = getGMTDateFromLocaleDate(stDateTime);

    var etDate = month+"/"+day+"/"+year;
    etDate = etDate+" 23:59:59";

    var endtDate = new Date(etDate);
    var edDateTime = endtDate.getTime();
    //edDateTime = getGMTDateFromLocaleDate(edDateTime);
    var patientListURL = "";//
    if(DAY_VIEW == "DayView"){
        patientListURL = ipAddress+"/appointment-block/list/?provider-id="+providerId+"&from-date="+stDateTime+"&to-date="+edDateTime+"&is-active=1";
    }else if(DAY_VIEW == "WeekView"){
        var startDate = new Date(stDate);
        var endtDate = new Date(etDate);

        var day = startDate.getDate()-startDate.getDay();
        startDate.setDate(day);

        endtDate.setDate(day+startDate.getDay());
        var stDateTime = startDate.getTime();
        var edDateTime = endtDate.getTime();
        //var sDate = new Date();
        //sDate.setFullYear(year, month, date)
        patientListURL = ipAddress+"/appointment-block/list/?provider-id="+providerId+"&from-date="+stDateTime+"&to-date="+edDateTime+"&is-active=1";;
    }else{
        var startDate = new Date(stDate);
        var endtDate = new Date(etDate);
        startDate.setDate(1);
        endtDate.setDate(31);
        var stDateTime = startDate.getTime();
        var edDateTime = endtDate.getTime();
        patientListURL = ipAddress+"/appointment-block/list/?provider-id="+providerId+"&from-date="+stDateTime+"&to-date="+edDateTime+"&is-active=1";;
    }
    getAjaxObject(patientListURL,"GET",onPatientBlockDays,onErrorMedication);

}
var blArray = [];
function onPatientBlockDays(dataObj){
    console.log(dataObj);
    blArray = [];
    if(dataObj.response.appointmentBlock){
        if($.isArray(dataObj.response.appointmentBlock)){
            blArray = dataObj.response.appointmentBlock;
        }else{
            blArray.push(dataObj.response.appointmentBlock);
        }
    }

    var scheduler = $("#scheduler").data("kendoScheduler");
    for(var i=0;i<blArray.length;i++){
        var sdt = blArray[i].startDateTime;
        //sdt = getLocalTimeFromGMT(sdt);

        var edt = blArray[i].endDateTime;
        //edt = getLocalTimeFromGMT(edt);

        var st = kendo.toString(new Date(sdt), "g");
        var et = kendo.toString(new Date(edt), "g");
        var arr = {title:"block times",id:blArray[i].id,room:1,start: new Date(st), end: new Date(et)};
        scheduler.dataSource.add(arr);
    }
    setTimeout(function(){
        var events = $(".k-event");
        for(var e=0;e<events.length;e++){
            //console.log(events[e]);
            $(events[e]).css("background","#787b7a");
            $(events[e]).css("color","#FFF");
        }
        onClickAppSearch(scheduler.date());
    },2000);

}
var patientId = "";
function addAppointment(){
    var scheduler = $("#scheduler").data("kendoScheduler");
    var cmbFacility = $("#cmbFacility").data("kendoComboBox");
    var cmbDoctor = $("#cmbDoctor").data("kendoComboBox");

    var strFacility = "";
    var strDoc = "";
    if(cmbFacility){
        strFacility = cmbFacility.text();
    }
    if(cmbDoctor){
        strDoc = cmbDoctor.text();
    }
    //scheduler.addEvent({ownerId:""+strDoc+"",facility:""+strFacility+"",doctor:""+strDoc+"", title: ""+patientId+"",start:stDate,end:endDate});
}
function closeAddAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        console.log(returnData);
        patientId = returnData.selItem.PID;
        var pID = returnData.selItem.PID;
        var pName = pID+","+returnData.selItem.FN+" "+returnData.selItem.MN+" "+returnData.selItem.LN;
        if(pID != ""){
            var scheduler = $("#scheduler").data("kendoScheduler");
            var cmbFacility = $("#cmbFacility").data("kendoComboBox");
            var cmbDoctor = $("#cmbDoctor").data("kendoComboBox");

            var strFacility = "";
            var strDoc = "";
            var strAppType = "";
            var strAppTypeValue = "";

            var strR = "";
            var strRV = "";

            if(cmbFacility){
                strFacility = cmbFacility.text();
            }
            if(cmbDoctor){
                strDoc = cmbDoctor.text();
            }
            strDoc = "";
            if(dArray){
                for(var j=0;j<dArray.length;j++){
                    strDoc = strDoc+dArray[j]+",";
                }
                if(strDoc.length>0){
                    strDoc = strDoc.substring(0,strDoc.length-1);
                }
            }
            var cmbAppTypes = $("#cmbAppTypes").data("kendoComboBox");
            if(cmbAppTypes){
                strAppType = cmbAppTypes.text();
                strAppTypeValue = cmbAppTypes.value();
            }
            var cmbReason = $("#cmbReason").data("kendoComboBox");
            if(cmbReason){
                strR = cmbReason.text();
                strRV = cmbReason.value();
            }

            console.log(stDate+','+endDate);
            scheduler.addEvent({description1V:""+strRV+"",description1:""+strRV+"",ownerIdV:""+strAppTypeValue+"",patientID:""+pID+"",ownerId:""+strAppTypeValue+"",facility:""+strFacility+"",doctor:""+strDoc+"", title: ""+pName+"",start:stDate,end:endDate});
        }
    }
}
function onErrorMedication(errobj){
    console.log(errobj);
}
function onshowImage(){
    $("#imgPhoto").attr( "src", "../../img/AppImg/HosImages/male_profile.png" );
}
function buttonEvents(){
    $("#imgPhoto").error(function() {
        onshowImage();
    });

    $("#btnSave").off("click");
    $("#btnSave").on("click",onClickSave);

    $("#btnCancel").off("click");
    $("#btnCancel").on("click",onClickCancel);

    $("#btnSearch1").off("click",onClickSearch);
    $("#btnSearch1").on("click",onClickSearch);

    $("#btnBlock").off("click",onClickBlockDays);
    $("#btnBlock").on("click",onClickBlockDays);

    $("#btnView").off("click",onClickViews);
    $("#btnView").on("click",onClickViews);

    $("#btnPtView").off("click",onClickPtViews);
    $("#btnPtView").on("click",onClickPtViews);


    $("#btnRAdd").off("click",onClickAddRoster);
    $("#btnRAdd").on("click",onClickAddRoster);

    $("#btnREdit").off("click",onClickEditRoster);
    $("#btnREdit").on("click",onClickEditRoster);

    $("#btnRDel").off("click",onClickDeleteRoster);
    $("#btnRDel").on("click",onClickDeleteRoster);

    $("#btnRRoster").off("click",onClickCreateRoster);
    $("#btnRRoster").on("click",onClickCreateRoster);

    $("#btnRApp").off("click",onClickCreateRosterAppointment);
    $("#btnRApp").on("click",onClickCreateRosterAppointment);

    $("#btnRSearch1").off("click",onClickRSearch);
    $("#btnRSearch1").on("click",onClickRSearch);

    $("#btnRSearch").off("click",onClickRPSearch);
    $("#btnRSearch").on("click",onClickRPSearch);

    $('#prscheduler').scroll(function(e) {
        console.log(e);
    });
    $("#btnCarePlan").off("click",onClickCarePlan);
    $("#btnCarePlan").on("click",onClickCarePlan);

    $("#btnPRView").off("click",onClickViewProviderApp);
    $("#btnPRView").on("click",onClickViewProviderApp);

    $("#divProfileNavs li a[data-toggle='tab'").off("click");
    $("#divProfileNavs li a[data-toggle='tab'").on("click",onClickTabs);

    if(DAY_VIEW != "DayView"){
        $("#scheduler").off("dblclick");
        $("#scheduler").on("dblclick", '.k-event', function (e) {
            if(DAY_VIEW != "DayView"){
                console.log("sss");
                var curr = e.currentTarget;
                var pName = $(curr).find("#desc1").text();
                var pItem = getPatientAppDetails(pName,dtArray);
                if(pItem){
                    console.log(pItem);

                    var scheduler = $("#scheduler").data("kendoScheduler");
                    scheduler.view("day");
                    var dt = new Date(pItem.dateOfAppointment);
                    onClickAppSearch(dt);
                }
            }

        });
    }

}
//$(window).resize(adjustHeight);

var viewPatientId = "";
var viewpName = "";
var viewpDT = "";
var viewPTAge = "";
function closePtAddAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        console.log(returnData);
        var pID = returnData.selItem.PID;
        viewPatientId = pID;
        var pName = returnData.selItem.FN+" "+returnData.selItem.MN+" "+returnData.selItem.LN;
        viewpName = pName;
        if(pID != ""){
            $("#lblName").text(pID+" - "+pName);
            $("#txtPatient").val(pName);
            var imageServletUrl = ipAddress + "/download/patient/photo/" + pID + "/?" + Math.round(Math.random() * 1000000)+"&access_token="+sessionStorage.access_token+"&tenant="+sessionStorage.tenant;
            $("#imgPhoto").attr("src", imageServletUrl);

            getAjaxObject(ipAddress + "/patient/" + pID, "GET", onGetPatientInfo, onError);
        }
    }
}
function onGetPatientInfo(dataObj){
    if (dataObj.response.patient.dateOfBirth) {
        var dt = new Date(dataObj.response.patient.dateOfBirth);
        if (dt) {
            var strDT = "";
            var cntry = sessionStorage.countryName;
            if(cntry.indexOf("India")>=0){
                strDT = kendo.toString(dt, "dd/MM/yyyy");
            }else if(cntry.indexOf("United Kingdom")>=0){
                strDT = kendo.toString(dt, "dd/MM/yyyy");
            }else{
                strDT = kendo.toString(dt, "MM/dd/yyyy");
            }
        }
        viewpDT = strDT;
        var strAge = getAge(dt);
        viewPTAge = strAge;
        var dob = strDT+"( "+strAge+" )"+","+dataObj.response.patient.gender;
        $("#lblDOB").text(dob);
        if (dataObj && dataObj.response && dataObj.response.communication) {
            var commArray = [];
            if ($.isArray(dataObj.response.communication)) {
                commArray = dataObj.response.communication;
            } else {
                commArray.push(dataObj.response.communication);
            }
            var comObj = commArray[0];
            commId = comObj.id;
            var addr = comObj.address1+","+comObj.address2;//+','+comObj.zip+"\n"+comObj.homePhone+","+comObj.cellPhone;
            $("#lblAddr").text(addr);
            var strZip = comObj.city+","+comObj.state+","+comObj.zip;
            $("#lblZip").text(strZip);
            var strPhone = comObj.homePhone+"&nbsp;&nbsp;"+comObj.cellPhone;
            $("#lblhp").html(strPhone);
            //$("#lblcell").text(comObj.cellPhone);
        }
    }
    // $("#txtAge").val(strAge);
}
function onClickRSearch(){
    var popW = "60%";
    var popH = 500;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search Patient";
    if(sessionStorage.clientTypeId == "2"){
        profileLbl = "Search Client";
    }
    devModelWindowWrapper.openPageWindow("../../html/patients/searchPatient.html", profileLbl, popW, popH, true, closePtRAddAction);
}
var roseterPID = "";
var rosterPName = "";
function closePtRAddAction(evt,returnData){
    if(returnData && returnData.selItem && returnData.selItem.PID){
        var pID = returnData.selItem.PID;
        roseterPID = pID;
        var pName = returnData.selItem.FN+" "+returnData.selItem.MN+" "+returnData.selItem.LN;
        rosterPName = pName;
        if(pID != ""){
            $("#txtRPatient").val(pName);
        }
    }
}
function onClickSearch(){
    var popW = "60%";
    var popH = 500;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search Patient";
    if(sessionStorage.clientTypeId == "2"){
        profileLbl = "Search Client";
    }
    devModelWindowWrapper.openPageWindow("../../html/patients/searchPatient.html", profileLbl, popW, popH, true, closePtAddAction);
    /*var scheduler = $("#scheduler").data("kendoScheduler");
    if(scheduler){
        var dt = scheduler.options.date;
        onClickAppSearch(dt);
    }*/

    /*var cmbDoctor = $("#cmbDoctor").data("kendoComboBox");
    var cmbFacility = $("#cmbFacility").data("kendoComboBox");
    var scheduler = $("#scheduler").data("kendoScheduler");
    var dtFromDate = new Date();//scheduler.date();//new Date();//$("#dtFromDate").data("kendoDatePicker");
    //var dtFromDate = $("#dtFromDate").data("kendoDatePicker");

    var providerId = cmbDoctor.value();
    var fromDate = dtFromDate;//dtFromDate.value();

    var strDT = kendo.toString(new Date(fromDate), "dd-MMM-yyyy");

    var scheduler = $("#scheduler").data("kendoScheduler");
    scheduler.dataSource.data([]);
    var patientListURL = ipAddress+"/appointment/by-provider/"+providerId+"/"+strDT+"/1";
     getAjaxObject(patientListURL,"GET",onPatientListData,onErrorMedication);*/
}
function onClickAppSearch(dt){
    //var cmbDoctor = $("#cmbDoctor").data("kendoComboBox");
    //var ms1 = $("#required").getKendoMultiSelect();
    //var values = ms1.value();
    var cmbFacility = $("#cmbFacility").data("kendoComboBox");
    var scheduler = $("#scheduler").data("kendoScheduler");
    var dtFromDate = dt;//new Date();//scheduler.date();//new Date();//$("#dtFromDate").data("kendoDatePicker");
    //var dtFromDate = $("#dtFromDate").data("kendoDatePicker");
    var ms1 = $("#required").getKendoMultiSelect();
    var values = ms1.value();

    if(values.length == 0){
        customAlert.error("Error", "Please select Providers");
        return;
    }
    var providerId = values.toString();//cmbDoctor.value();
    var fromDate = dtFromDate;//dtFromDate.value();

    var day = dt.getDate();
    var month = dt.getMonth();
    month = month+1;
    var year = dt.getFullYear();

    var stDate = month+"/"+day+"/"+year;
    stDate = stDate+" 00:00:00";

    var startDate = new Date(stDate);
    var stDateTime = startDate.getTime();

    var etDate = month+"/"+day+"/"+year;
    etDate = etDate+" 23:59:59";

    var endtDate = new Date(etDate);
    var edDateTime = endtDate.getTime();
    var patientListURL = "";
    scheduler.dataSource.data([]);
    if(DAY_VIEW == "DayView"){
        //edDateTime = getGMTDateFromLocaleDate(edDateTime);
        //stDateTime = getGMTDateFromLocaleDate(stDateTime);
        patientListURL = ipAddress+"/appointment/list/?facility-id="+cmbFacility.value()+"&provider-id="+providerId+"&to-date="+edDateTime+"&from-date="+stDateTime+"&is-active=1";//+"&is-active=1&is-deleted=0"
        console.log(patientListURL);
        console.log(new Date(stDateTime)+","+new Date(edDateTime))
    }else if(DAY_VIEW == "WeekView"){
        var startDate = new Date(stDate);
        var endtDate = new Date(etDate);

        var day = startDate.getDate()-startDate.getDay();
        startDate.setDate(day);

        endtDate.setDate(day+startDate.getDay());
        var stDateTime = startDate.getTime();
        var edDateTime = endtDate.getTime();

        //edDateTime = getGMTDateFromLocaleDate(edDateTime);
        //stDateTime = getGMTDateFromLocaleDate(stDateTime);

        console.log(new Date(stDateTime)+","+new Date(edDateTime));
        patientListURL = ipAddress+"/appointment/list/?facility-id="+cmbFacility.value()+"&provider-id="+providerId+"&to-date="+edDateTime+"&from-date="+stDateTime+"&is-active=1";;
    }else{
        var startDate = new Date(stDate);
        var endtDate = new Date(etDate);
        startDate.setDate(1);
        endtDate.setDate(31);
        var stDateTime = startDate.getTime();
        var edDateTime = endtDate.getTime();

        //edDateTime = getGMTDateFromLocaleDate(edDateTime);
        //stDateTime = getGMTDateFromLocaleDate(stDateTime);

        patientListURL = ipAddress+"/appointment/list/?facility-id="+cmbFacility.value()+"&provider-id="+providerId+"&to-date="+edDateTime+"&from-date="+stDateTime+"&is-active=1";;
    }
    getAjaxObject(patientListURL,"GET",onPatientListData,onErrorMedication);
}
function onClickSave(){
    /*var scheduler = $("#scheduler").data("kendoScheduler");
    if(scheduler){
        scheduler.setOptions({
              minorTick: 15,
              majorTick:15,
              minorTickCount:1,
              startTime: new Date("2017/12/11 12:00 PM"),
              endTime: new Date("2017/12/11 2:00 PM")
          });
          scheduler.view(scheduler.view().name);

          setTimeout(function(){
              scheduler.resize(true);
            },2000);
    }*/
}
function onCreate(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            appCount = appCount+1;
            if(appProviderArr.length == appCount){
                if(!updateFlag){
                    customAlert.error("Info", "Appointment created successfully");
                }else{
                    customAlert.error("Info", "Appointment updated successfully");
                }
                var dt =saveDate;
                onClickAppSearch(dt);
            }else{
                appointmentSave();
            }
            /**/

            /*var obj = {};
            obj.status = "success";
            obj.operation = "add";
            popupClose(obj);*/


        }else{
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}

function onClickDeleteRoster(){
    var selectedItems = angularPTUIgridWrapper.getSelectedRows();
    if(selectedItems && selectedItems.length>0){
        customAlert.confirm("Confirm", "Are you sure you want delete?",function(response){
            if(response.button == "Yes"){
                var obj = selectedItems[0];
                if(obj.idk){
                    var rosterObj = {};
                    rosterObj.id = obj.idk;
                    rosterObj.modifiedBy = Number(sessionStorage.userId);;
                    rosterObj.isActive = 0;
                    rosterObj.isDeleted = 1;
                    var dataUrl = ipAddress+"/patient/roster/delete";
                    createAjaxObject(dataUrl,rosterObj,"POST",onPatientRosterDelete,onError);
                }else{
                    angularPTUIgridWrapper.deleteItem(selectedItems[0]);
                }
            }
        });
    }
}
function onPatientRosterDelete(dataObj){
    onClickRPSearch();
}


var allRosterRows = [];
var allRosterRowCount = 0;
var allRosterRowIndex = 0;
function onClickCreateRoster(){
    allRosterRowCount = 0;
    allRosterRowIndex = 0;
    allRosterRows = angularPTUIgridWrapper.getAllRows();
    allRosterRowCount = allRosterRows.length;
    if(allRosterRowCount>0){
        saveRoster();
    }
}
function saveRoster(){
    if(allRosterRowCount>allRosterRowIndex){
        var objRosterEntiry = allRosterRows[allRosterRowIndex];
        var rosterObj = objRosterEntiry.entity;
        rosterObj.createdBy = Number(sessionStorage.userId);
        rosterObj.isActive = 1;
        rosterObj.isDeleted = 0;

        rosterObj.patientId = rosterObj.patientK;
        rosterObj.facilityId = rosterObj.facilityK;
        rosterObj.contractId = rosterObj.contractK;
        rosterObj.providerId = rosterObj.providerK;
        rosterObj.weekDay = rosterObj.dowK;
        rosterObj.fromTime = rosterObj.fromTimeK;
        rosterObj.toTime = rosterObj.toTimeK;
        rosterObj.careTypeId = 2;
        rosterObj.duration = rosterObj.duration;
        rosterObj.reason = rosterObj.reason;
        rosterObj.notes = rosterObj.notes;
        var dataUrl = "";
        if(rosterObj.idk){
            var rObj = {};
            rObj.id = rosterObj.idk;
            rObj.modifiedBy = Number(sessionStorage.userId);
            rObj.isActive = 1;
            rObj.isDeleted = 0;
            rObj.patientId = rosterObj.patientK;
            rObj.contractId = rosterObj.contractK;
            rObj.providerId = rosterObj.providerK;
            rObj.weekDay = rosterObj.dowK;
            rObj.fromTime = rosterObj.fromTimeK;
            rObj.toTime = rosterObj.toTimeK;
            rObj.careTypeId = rosterObj.careTypeId;
            rObj.duration = rosterObj.duration;
            rObj.reason = rosterObj.reason;
            rObj.notes = rosterObj.notes;
            rObj.facilityId = rosterObj.facilityK;

            dataUrl = ipAddress+"/patient/roster/update";
            createAjaxObject(dataUrl,rObj,"POST",onPatientRosterCreate,onError);
        }else{
            dataUrl = ipAddress+"/patient/roster/create";
            createAjaxObject(dataUrl,rosterObj,"POST",onPatientRosterCreate,onError);
        }
    }
}
function onPatientRosterCreate(dataObj){
    allRosterRowIndex = allRosterRowIndex+1;
    if(allRosterRowCount == allRosterRowIndex){
        customAlert.error("Info", "Roster created successfully");
        buildPatientRosterList([]);
        onClickRPSearch();
        //getPatientRosterList();
    }else{
        saveRoster();
    }
}

function onClickCarePlan(){
    if(roseterPID && rosterPName){
        var popW = "90%";
        var popH = "80%";
        var profileLbl;
        var devModelWindowWrapper = new kendoWindowWrapper();
        profileLbl = "Care Plan";
        showPatientRoster();
        devModelWindowWrapper.openPageWindow("../../html/patients/createPlanRoster.html", profileLbl, popW, popH, false, closeRosterPlan);
    }
}
function closeRosterPlan(evt,returnData){

}
function onClickCreateRosterAppointment(){
    var txtRPatient = $("#txtRPatient").data("kendoComboBox");
    if(txtRPatient.value()){
        var popW = "400px";
        var popH = "500px";
        var profileLbl;
        var devModelWindowWrapper = new kendoWindowWrapper();
        profileLbl = "Create Roster Appointment";
        parentRef.pid = txtRPatient.value();
        devModelWindowWrapper.openPageWindow("../../html/patients/createRosterAppointment.html", profileLbl, popW, popH, false, closeRosterAppointment);
    }

}
function closeRosterAppointment(evt,returnData){

}
function onClickEditRoster(){
    var selectedItems = angularPTUIgridWrapper.getSelectedRows();
    if(selectedItems && selectedItems.length>0){
        var popW = "60%";
        var popH = "60%";
        var profileLbl;
        var devModelWindowWrapper = new kendoWindowWrapper();
        profileLbl = "Edit Roster";
        parentRef.RS = "edit";
        parentRef.selRSItem = selectedItems[0];
        showPatientRoster()
        devModelWindowWrapper.openPageWindow("../../html/patients/createRoster.html", profileLbl, popW, popH, false, closePatientRoster);
    }
}

function showPatientRoster(){
    var txtRFacility = $("#txtRFacility").data("kendoComboBox");
    var txtRPatient = $("#txtRPatient").data("kendoComboBox");

    parentRef.pid = roseterPID;
    parentRef.pname = rosterPName;

    parentRef.fid = txtRFacility.value();
    parentRef.fname = txtRFacility.text();
}
function onClickAddRoster(){
    //angularPTUIgridWrapper.insert({},0);
    var popW = "60%";
    var popH = "60%";
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Create Roster";
    parentRef.RS = "create";
    parentRef.selRSItem = null;
    showPatientRoster();
    devModelWindowWrapper.openPageWindow("../../html/patients/createRoster.html", profileLbl, popW, popH, false, closePatientRoster);
    /*var txtDuration = $("#txtDuration").val();
    txtDuration = $.trim(txtDuration);
    if(txtDuration != ""){
        var txtRPatient = $("#txtRPatient").data("kendoComboBox");
        var txtRFecility = $("#txtRFecility").data("kendoComboBox");
        var txtRProvider = $("#txtRProvider").data("kendoComboBox");
        var txtRDOW = $("#txtRDOW").data("kendoComboBox");
        var txtStartTime = $("#txtStartTime").data("kendoTimePicker");
        var txtRReason = $("#txtRReason").data("kendoComboBox");

        var obj = {};
        obj.createdBy = Number(sessionStorage.userId);
        obj.isActive = 1;
        obj.isDeleted = 0;
        obj.patinetId = Number(txtRPatient.value());
        obj.contractId = Number(txtRFecility.value());
        obj.providerId = Number(txtRProvider.value());
        obj.weekDay = Number(txtRDOW.value());
        var dt = new Date(txtStartTime.value());
        obj.fromTime = dt.getTime();
        obj.toTime = dt.getTime();
        obj.careTypeId = 2;
        obj.duration = Number(txtDuration);
        obj.reason = txtRReason.text();
        obj.notes = $("#taRNotes").val();

        console.log(obj);

        var dataUrl = "";
        dataUrl = ipAddress+"/patient/roster/create";
        createAjaxObject(dataUrl,obj,"POST",onRosterCreate,onError);


    }else{
        customAlert.error("error", "Enter Duration");
    }*/
}
function closePatientRoster(evt,returnData){
    console.log(returnData);
    if(returnData && returnData.status == "success"){
        var obj = returnData;
        var st = returnData.rs;
        if(st == "create"){
            angularPTUIgridWrapper.insert(obj,0);
            /*var dow = obj.dow;
            var dowk = obj.dowK;

            var strDowArray = dow.split(",");
            var strDowKArray = dowk.split(",");*/

            /*for(var s=0;s<strDowArray.length;s++){
                var sObj = obj;
                sObj.dow = strDowArray[s];
                sObj.dowK = strDowKArray[s];
                angularPTUIgridWrapper.insert(sObj,0);
                angularPTUIgridWrapper.refreshGrid();
            }*/
        }else{
            var selectedItems = angularPTUIgridWrapper.getSelectedRows();
            var dgItem = selectedItems[0];
            dgItem.contract = obj.contract;
            dgItem.contractK = obj.contractK;

            dgItem.facility = obj.facility;
            dgItem.facilityK = obj.facilityK;

            dgItem.provider = obj.provider;
            dgItem.providerK = obj.providerK;

            dgItem.fromTime = obj.fromTime;
            dgItem.fromTimeK = obj.fromTimeK;

            dgItem.toTime = obj.toTime;
            dgItem.toTimeK = obj.toTimeK;

            dgItem.duration = obj.duration;

            dgItem.AppType = obj.AppType;
            dgItem.AppTypeK = obj.AppTypeK;

            dgItem.reason = obj.reason;
            dgItem.reasonK = obj.reasonK;

            dgItem.notes = obj.notes;

            dgItem.patient = obj.patient;
            dgItem.patientK = obj.patientK;

            dgItem.dow = obj.dow;
            dgItem.dowK = obj.dowK;
        }

        angularPTUIgridWrapper.refreshGrid();
    }
}
function onRosterCreate(dataObj){
    console.log(dataObj);
}
function onClickRPSearch(){//patient-id
    var txtRPatient = $("#txtRPatient").data("kendoComboBox");
    if(txtRPatient){
        roseterPID = txtRPatient.value();
    }
    if(roseterPID != ""){
        var patientRosterURL = ipAddress+"/patient/roster/list/?patient-id="+roseterPID;
    }else{
        var patientRosterURL = ipAddress+"/patient/roster/list/?is-active=1";
    }
    buildPatientRosterList([]);
    getAjaxObject(patientRosterURL,"GET",onGetRosterList,onErrorMedication);
}
function getPatientRosterList(){
    var patientRosterURL = ipAddress+"/patient/roster/list/?is-active=1";
    getAjaxObject(patientRosterURL,"GET",onGetRosterList,onErrorMedication);
}
function getWeekDayName(wk){
    var wn = "";
    if(wk == 1){
        return "Monday";
    }else if(wk == 2){
        return "Tuesday";
    }else if(wk == 2){
        return "Tuesday";
    }else if(wk == 3){
        return "Wednesday";
    }else if(wk == 4){
        return "Thursday";
    }else if(wk == 5){
        return "Friday";
    }else if(wk == 6){
        return "Saturday";
    }else if(wk == 0){
        return "Sunday";
    }
    return wn;
}
function onGetRosterList(dataObj){
    console.log(dataObj);
    var rosterArray = [];
    if(dataObj && dataObj.response &&  dataObj.response.status && dataObj.response.status.code == 1){
        if($.isArray(dataObj.response.patientRoster)){
            rosterArray = dataObj.response.patientRoster;
        }else{
            rosterArray.push(dataObj.response.patientRoster);
        }
    }
    for(var i=0;i<rosterArray.length;i++){
        rosterArray[i].idk = rosterArray[i].id;
        rosterArray[i].contractK = rosterArray[i].contractId;
        rosterArray[i].contract = rosterArray[i].contract;

        rosterArray[i].facility = rosterArray[i].facility;
        rosterArray[i].facilityK = rosterArray[i].facilityId;

        rosterArray[i].provider = rosterArray[i].provider;
        rosterArray[i].providerK = rosterArray[i].providerId;

        rosterArray[i].dow = getWeekDayName(rosterArray[i].weekDay);
        rosterArray[i].dowK = rosterArray[i].weekDay;

        var sdt = new Date();
        sdt.setHours(0, 0, 0, 0);
        sdt.setMinutes(Number(rosterArray[i].fromTime));
        //var sdms = rosterArray[i].fromTime;
        rosterArray[i].fromTimeK = rosterArray[i].fromTime;
        rosterArray[i].fromTime = kendo.toString(sdt,"t");


        var edt = new Date();
        edt.setHours(0, 0, 0, 0);
        edt.setMinutes(Number(rosterArray[i].toTime));

        rosterArray[i].toTimeK = rosterArray[i].toTime;
        rosterArray[i].toTime = kendo.toString(edt,"t");

        rosterArray[i].duration = rosterArray[i].duration;

        rosterArray[i].AppType = rosterArray[i].AppType;
        rosterArray[i].AppTypeK = rosterArray[i].AppTypeK;

        rosterArray[i].reason = rosterArray[i].reason;
        rosterArray[i].reasonK = rosterArray[i].reasonK;

        rosterArray[i].dow = rosterArray[i].dow;
        rosterArray[i].dowK = rosterArray[i].dowK;

        rosterArray[i].patient = rosterArray[i].patientId;
        rosterArray[i].patientK = rosterArray[i].patientId;
    }
    buildPatientRosterList(rosterArray);
}
function buildPatientRosterList(dataSource){
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    otoptions.rowHeight = 100;
    gridColumns.push({
        "title": "Contact",
        "field": "contract",
        "width": "15%",
    });
    gridColumns.push({
        "title": "DOW",
        "field": "dow",
        "width": "10%",
    });
    gridColumns.push({
        "title": "Start Time",
        "field": "fromTime",
        "width": "10%",
    });
    gridColumns.push({
        "title": "End Time",
        "field": "toTime",
        "width": "10%",
    });
    gridColumns.push({
        "title": "Duration",
        "field": "duration",
        "width": "5%",
    });
    gridColumns.push({
        "title": "Visit/Appointment Type",
        "field": "AppType",
        "width": "20%",
    });
    gridColumns.push({
        "title": "Reason",
        "field": "reason",
        "width": "10%",
    });
    gridColumns.push({
        "title": "Provider",
        "field": "provider",
        "width": "15%",
    });
    gridColumns.push({
        "title": "Facility",
        "field": "facility",
        "width": "15%",
    });
    gridColumns.push({
        "title": "Notes",
        "field": "notes",
    });

    angularPTUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
}
function onPTChange(){
    setTimeout(function(){
        var selectedItems = angularPTUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if(selectedItems && selectedItems.length>0){
            $("#btnREdit").prop("disabled", false);
            $("#btnRDel").prop("disabled", false);
        }else{
            $("#btnREdit").prop("disabled", true);
            $("#btnRDel").prop("disabled", true);
        }
    },100)
}
function showOperations(){
    var node = '<div style="text-align:center"><span style="cursor:pointer;font-weight:bold;font-size:15px">Edit</span>&nbsp;&nbsp;<span style="cursor:pointer;font-weight:bold;font-size:15px">Delete</span>';
    node = node+"</div>";
    return node;
}
function onClickViewProviderApp(){
    var txtPRDate = $("#txtPRDate").data("kendoDatePicker");
    var txtPRFacility = $("#txtPRFacility").data("kendoComboBox");
    if(txtPRDate && txtPRDate.value()){
        $("#prscheduler").text("");
        //https://stage.timeam.com/provider/list/?facility-id=4&is-active=1
        var facilityId = txtPRFacility.value();
        var patientListURL = ipAddress+"/provider/list/?facility-id="+facilityId+"&is-active=1";
        getAjaxObject(patientListURL,"GET",onGetFacilityProviderList,onErrorMedication);
    }else{
        customAlert.error("Error", "Please select Date");
    }
}
var prViewArray = [];
var prViewDateArray = [{idk:'1',h:'0',m:'0',tm:'12:00 AM'},{idk:'2',h:'0',m:'15',tm:'12:15 AM'},{idk:'3',h:'0',m:'30',tm:'12:30 AM'},{idk:'4',h:'0',m:'45',tm:'12:45 AM'},{idk:'5',h:'1',m:'0',tm:'1:00 AM'},{idk:'6',h:'1',m:'15',tm:'1:15 AM'},{idk:'7',h:'1',m:'30',tm:'1:30 AM'},{idk:'8',h:'1',m:'45',tm:'1:45 AM'},{idk:'9',h:'2',m:'0',tm:'2:00 AM'},{idk:'10',h:'2',m:'15',tm:'2:15 AM'},{idk:'11',h:'2',m:'30',tm:'2:30 AM'},{idk:'11',h:'2',m:'45',tm:'2:45 AM'},{idk:'12',h:'3',m:'0',tm:'3:00 AM'},{idk:'13',h:'3',m:'15',tm:'3:15 AM'},{idk:'14',h:'3',m:'30',tm:'3:30 AM'},{idk:'15',h:'3',m:'45',tm:'3:45 AM'},{idk:'15',h:'4',m:'0',tm:'4:00 AM'}];
var obj = {};
obj.idk = "16";
obj.h = "4";
obj.m = "15";
obj.tm = "4:15 AM";
prViewDateArray.push(obj);
var obj = {};
obj.idk = "17";
obj.h = "4";
obj.m = "30";
obj.tm = "4:30 AM";
prViewDateArray.push(obj);

var obj = {};
obj.idk = "18";
obj.h = "4";
obj.m = "45";
obj.tm = "4:45 AM";
prViewDateArray.push(obj);

var obj = {};
obj.idk = "19";
obj.h = "5";
obj.m = "0";
obj.tm = "5:00 AM";
prViewDateArray.push(obj);

var obj = {};
obj.idk = "20";
obj.h = "5";
obj.m = "15";
obj.tm = "5:15 AM";
prViewDateArray.push(obj);

var obj = {};
obj.idk = "20";
obj.h = "5";
obj.m = "30";
obj.tm = "5:30 AM";
prViewDateArray.push(obj);

var obj = {};
obj.idk = "21";
obj.h = "5";
obj.m = "45";
obj.tm = "5:45 AM";
prViewDateArray.push(obj);

var obj = {};
obj.idk = "22";
obj.h = "6";
obj.m = "0";
obj.tm = "6:00 AM";
prViewDateArray.push(obj);

var obj = {};
obj.idk = "23";
obj.h = "6";
obj.m = "15";
obj.tm = "6:15 AM";
prViewDateArray.push(obj);

var obj = {};
obj.idk = "24";
obj.h = "6";
obj.m = "30";
obj.tm = "6:30 AM";
prViewDateArray.push(obj);

var obj = {};
obj.idk = "25";
obj.h = "6";
obj.m = "45";
obj.tm = "6:45 AM";
prViewDateArray.push(obj);

var obj = {};
obj.idk = "26";
obj.h = "7";
obj.m = "0";
obj.tm = "7:00 AM";
prViewDateArray.push(obj);

var obj = {};
obj.idk = "27";
obj.h = "7";
obj.m = "15";
obj.tm = "7:15 AM";
prViewDateArray.push(obj);

var obj = {};
obj.idk = "28";
obj.h = "7";
obj.m = "30";
obj.tm = "7:30 AM";
prViewDateArray.push(obj);

var obj = {};
obj.idk = "29";
obj.h = "7";
obj.m = "45";
obj.tm = "7:45 AM";
prViewDateArray.push(obj);

var obj = {};
obj.idk = "30";
obj.h = "8";
obj.m = "0";
obj.tm = "8:00 AM";
prViewDateArray.push(obj);

var obj = {};
obj.idk = "31";
obj.h = "8";
obj.m = "15";
obj.tm = "8:15 AM";
prViewDateArray.push(obj);

var obj = {};
obj.idk = "31";
obj.h = "8";
obj.m = "30";
obj.tm = "8:30 AM";
prViewDateArray.push(obj);

var obj = {};
obj.idk = "32";
obj.h = "8";
obj.m = "45";
obj.tm = "8:45 AM";
//prViewDateArray.push(obj);

var obj = {};
obj.idk = "33";
obj.h = "8";
obj.m = "45";
obj.tm = "8:45 AM";
prViewDateArray.push(obj);

var obj = {};
obj.idk = "34";
obj.h = "9";
obj.m = "0";
obj.tm = "9:00 AM";
prViewDateArray.push(obj);

var obj = {};
obj.idk = "35";
obj.h = "9";
obj.m = "15";
obj.tm = "9:15 AM";
prViewDateArray.push(obj);

var obj = {};
obj.idk = "36";
obj.h = "9";
obj.m = "30";
obj.tm = "9:30 AM";
prViewDateArray.push(obj);

var obj = {};
obj.idk = "37";
obj.h = "9";
obj.m = "45";
obj.tm = "9:45 AM";
prViewDateArray.push(obj);

var obj = {};
obj.idk = "38";
obj.h = "10";
obj.m = "0";
obj.tm = "10:00 AM";
prViewDateArray.push(obj);

var dts = ['10:15 AM','10:30 AM','10:45 AM','11:00 AM','11:15 AM','11:30 AM','11:45 AM','12:00 PM','12:15 PM','12:30 PM','12:45 PM','1:00 PM','1:15 PM','1:30 PM','1:45 PM','2:00 PM','2:15 PM','2:30 PM','2:45 PM','3:00 PM','3:15 PM','3:30 PM','3:45 PM','4:00 PM','4:15 PM','4:30 PM','4:45 PM','5:00 PM','5:15 PM','5:30 PM','5:45 pM','6:00 PM','6:15 PM','6:30 PM','6:45 PM','7:00 PM','7:15 PM','7:30 PM','7:45 PM','8:00 PM','8:15 PM','8:30 PM','8:45 PM','9:00 PM','9:15 PM','9:30 PM','9:45 PM','10:00 PM','10:15 PM','10:30 PM','10:45 PM','11:00 PM','11:15 PM','11:30 PM','11:45 PM']
var hds = ['10','10','10','11','11','11','11','12','12','12','12','13','13','13','13','14','14','14','14','15','15','15','15','16','16','16','16','17','17','17','17','18','18','18','18','19','19','19','19','20','20','20','20','21','21','21','21','22','22','22','22','23','23','23','23'];
var mds = ['15','30','45','0','15','30','45','0','15','30','45','0','15','30','45','0','15','30','45','0','15','30','45','0','15','30','45','0','15','30','45','0','15','30','45','0','15','30','45','0','15','30','45','0','15','30','45','0','15','30','45','0','15','30','45'];

for(var k=0;k<dts.length;k++){
    var obj = {};
    obj.idk = (k+30);
    obj.h = hds[k];
    obj.m = mds[k];
    obj.tm = dts[k];
    prViewDateArray.push(obj);
}

function onGetFacilityProviderList(dataObj){
    console.log(dataObj);
    prViewArray = [];

    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if($.isArray(dataObj.response.provider)){
            prViewArray = dataObj.response.provider;
        }else{
            prViewArray.push(dataObj.response.provider);
        }
    }
    if(prViewArray.length>0){
        var pdArray = [];
        for(var p=0;p<prViewArray.length;p++){
            pdArray.push(prViewArray[p].id);
        }
        var txtPRDate = $("#txtPRDate").data("kendoDatePicker");
        var dt = txtPRDate.value();

        var day = dt.getDate();
        var month = dt.getMonth();
        month = month+1;
        var year = dt.getFullYear();

        var stDate = month+"/"+day+"/"+year;
        stDate = stDate+" 00:00:00";

        var startDate = new Date(stDate);
        var stDateTime = startDate.getTime();

        var etDate = month+"/"+day+"/"+year;
        etDate = etDate+" 23:59:59";

        var endtDate = new Date(etDate);
        var edDateTime = endtDate.getTime();

        var txtPRFacility = $("#txtPRFacility").data("kendoComboBox");

        var patientListURL = ipAddress+"/appointment/list/?facility-id="+txtPRFacility.value()+"&provider-id="+pdArray.toString()+"&to-date="+edDateTime+"&from-date="+stDateTime+"&is-active=1";//+"&is-active=1&is-deleted=0"

        getAjaxObject(patientListURL,"GET",onProviderPatientListData,onErrorMedication);

    }
}
var providerSArray = [];
function isProviderSExist(ms){
    for(var r=0;r<providerSArray.length;r++){
        var rItem = providerSArray[r];
        if(rItem.dateOfAppointment == ms){
            return true;
        }
    }
    return false;
}
function onProviderPatientListData(dataObj){
    providerSArray = [];
    var pdtArray = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.appointment){
            if($.isArray(dataObj.response.appointment)){
                pdtArray = dataObj.response.appointment;
            }else{
                pdtArray.push(dataObj.response.appointment);
            }
        }
    }
    columnSequenceSort(pdtArray);
    for(var s=0;s<pdtArray.length;s++){
        var item = pdtArray[s];
        if(item){
            if(providerSArray.length == 0){
                providerSArray.push(item);
            }else{
                if(!isProviderSExist(item.dateOfAppointment)){
                    providerSArray.push(item);
                }
            }
        }
    }
    var stDT = new Date(providerSArray[0].dateOfAppointment);
    var etDT = new Date(providerSArray[providerSArray.length-1].dateOfAppointment);

    console.log(stDT+","+etDT);

    var sth = stDT.getHours();
    var eth = etDT.getHours();

    var prDTView = [];
    for(var x=0;x<prViewDateArray.length;x++){
        var xItem = prViewDateArray[x];
        if(xItem){
            if(xItem.h>=sth && xItem.h<=eth){
                prDTView.push(xItem);
            }
        }
    }
    console.log(prDTView);
    //prViewDateArray
    $("#prscheduler").text("");
    $("#prschedulerH").text("");
    var strTable = '<table class="table">';
    strTable = strTable+'<thead class="fillsHeader">';
    strTable = strTable+'<tr>';
    strTable = strTable+'<th class="textAlign whiteColor" style="width:200px;font-size:10px">Provider Name</th>';
    strTable = strTable+'</tr>';
    strTable = strTable+'</thead>';
    strTable = strTable+'<tbody>';
    for(var y1=0;y1<prViewArray.length;y1++){
        var pItem = prViewArray[y1];
        var obj = getProviderStatusCount(pItem.id,pdtArray);
        var strTitle = pItem.firstName+","+pItem.lastName+"<br>"+obj.am+"+"+obj.pm+"="+obj.tot;
        strTable = strTable+'<tr>';
        strTable = strTable+'<td>'+strTitle+'</td>';
        strTable = strTable+'</tr>';
    }

    strTable = strTable+'</tbody>';
    strTable = strTable+'</table>';
    //$("#prschedulerH").append(strTable);

    var strTable = '<table class="table" id="tdh">';
    strTable = strTable+'<thead class="fillsHeader">';
    strTable = strTable+'<tr>';
    strTable = strTable+'<th class="textAlign whiteColor headcol" style="font-size:8px;padding:2px">Provider Name</th>';
    for(var i=0;i<prDTView.length;i++){
        var item = prDTView[i];
        //var strDT = kendo.toString(new Date(item.dateOfAppointment),"t");
        strTable = strTable+'<th class="textAlign whiteColor" style="width:200px;font-size:8px;padding:2px">'+item.tm +'</th>';
    }
    strTable = strTable+'</tr>';
    strTable = strTable+'</thead>';
    strTable = strTable+'<tbody>';
    for(var y=0;y<prViewArray.length;y++){
        var pItem = prViewArray[y];
        var obj = getProviderStatusCount(pItem.id,pdtArray);
        var strTitle = pItem.firstName+","+pItem.lastName+" "+obj.am+"+"+obj.pm+"="+obj.tot;
        strTable = strTable+'<tr>';
        strTable = strTable+'<td class="headcol" style="font-size:8px;padding:2px">'+strTitle+'</td>';
        for(var z=0;z<prDTView.length;z++){
            var item = prDTView[z];
            var strData = getProviderAppDays(item,prViewArray[y],pdtArray);
            if(strData){
                strTable = strTable+'<td title="'+strData+'" style="background:green;border:1px solid green"></td>';
            }else{
                strTable = strTable+'<td></td>';
            }
        }
        strTable = strTable+'</tr>';
    }
    //prViewArray
    /*for(var j=0;j<prViewDateArray.length;j++){
        strTable = strTable+'<tr>';
        strTable = strTable+'<td>'+prViewDateArray[j].tm+'</td>';
        for(var p=0;p<prViewArray.length;p++){
            var strData = getProviderAppDays(prViewDateArray[j],prViewArray[p],pdtArray);
            if(strData){
                strTable = strTable+'<td title="'+strData+'" style="background:green;border:1px solid green"></td>';
            }else{
                strTable = strTable+'<td></td>';
            }

        }
        strTable = strTable+'</tr>';
    }*/
    strTable = strTable+'</tbody>';
    strTable = strTable+'</table>';
    $("#prscheduler").append(strTable);
    //$("#tdh").CongelarFilaColumna({Columnas:1});
}
function getProviderStatusCount(pdId,pdArray){
    var pm = 0;
    var am = 0;
    var tot = 0;
    for(var p=0;p<pdArray.length;p++){
        var pItem = pdArray[p];
        if(pItem && pItem.providerId == pdId){
            var ms = pItem.dateOfAppointment;
            var dt = new Date(ms);
            if(dt.getHours()>=12){
                pm = pm+1;
            }else{
                am = am+1;
            }
        }
    }
    tot = am+pm;
    var obj = {};
    obj.am = am;
    obj.pm = pm;
    obj.tot = tot;
    return obj;
}
function getProviderAppDays(tItem,pItem,appArray){
    var txtPRDate = $("#txtPRDate").data("kendoDatePicker");
    txtPRDate = new Date(txtPRDate.value());
    for(var a=0;a<appArray.length;a++){
        var item = appArray[a];
        if(item){

            var ets = item.dateOfAppointment+(item.duration*60*1000);

            if(pItem.id == item.providerId){//== tItem.h && st.getMinutes() ==
                txtPRDate.setHours(tItem.h);
                var currMS = new Date(txtPRDate);
                currMS.setMinutes(tItem.m);
                currMS = currMS.getTime();
                currMS = currMS+1000;
                if(currMS>=item.dateOfAppointment && currMS<=ets){ //&& tItem.h<=et.getHours()
                    var st = new Date(item.dateOfAppointment);
                    var et = new Date(ets);
                    var dob = new Date(item.composition.patient.dateOfBirth);
                    var strAge = getAge(dob);
                    var strData = "";
                    strData = item.composition.patient.firstName+" "+item.composition.patient.middleName+" "+item.composition.patient.lastName+"\n";
                    strData = strData+kendo.toString(dob,"MM/dd/yyyy")+" ("+strAge+"),"+item.composition.patient.gender+"\n";
                    strData = strData+kendo.toString(st,"MM/dd/yyyy h:mm:ss tt")+" - "+kendo.toString(et,"MM/dd/yyyy h:mm:ss tt")+"\n";
                    strData = strData+ item.composition.appointmentReason.desc+"\n";
                    strData = strData+item.composition.appointmentType.desc+"\n";
                    return strData;
                }

            }
        }
    }
    return "";
}
function onClickCancel(){
    popupClose(false);
}
function popupClose(obj){
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}


var operation = "";
var selItem = null;
var ADD = "add";
var UPDATE = "update";
var VIEW = "view";
var DELETE = "delete";

var files = null;
var fileName = "";
var compressorSettings = {
    // toWidth: 30,
    // toHeight: 42,
    mimeType: 'image/png',
    mode: 'strict',
    quality: 0.2,
    grayScale: false,
    sepia: false,
    threshold: false,
    vReverse: false,
    hReverse: false,
    speed: 'low'
};
var isBrowseFlag = false;

var imagePhotoData = null;
var componentId;

var IsThumbnailPresent =0;
var statusArr = [{Key:'Active',Value:'Active'},{Key:'InActive',Value:'InActive'}];
var compType = [{Key:'Text',Value:'1'},{Key:'Text Area',Value:'2'},{Key:'Boolean',Value:'3'},{Key:'Checkbox',Value:'4'},{Key:'Radio Button',Value:'5'}];
var angularUIgridWrapper;
var parentRef = null;
var typeArr;
// = [{Key:'ADL',Value:'1'},{Key:'iADL',Value:'2'},{Key:'Care',Value:'3'},{Key:'Fluid',Value:'4'},{Key:'Food',Value:'5'}];


$(document).ready(function(){
    sessionStorage.setItem("IsSearchPanel", "1");
    // $("#divPatInfo",parent.document).css("display","");
    // $("#divIdMain",parent.document).css("display","none");
    // $("#divMain",parent.document).css("display","none");
    // $("#lblTitleName",parent.document).html("Task Components");
    // $("#divptName",parent.document).css("display","none");
    // $("#divptDOB",parent.document).css("display","none");
    // $("#divptGender",parent.document).css("display","none");
    // $("#divptStatus",parent.document).css("display","none");
    $("#pnlPatient",parent.document).css("display","none");
    themeAPIChange();
    $("#divDuration").hide();
    $("#divCharge").hide();
    document.getElementById('txtDisplayOrder').value = sessionStorage.getItem("displayOrderMaxlength");
    bindTheGrid();

    $("#btnCancel").on("click", function() {
        // e.preventDefault();
        buildStateListGrid([]);
        onClickCancel();
        // $('.btnActive').trigger('click');
        // onChange1();
    });
});


$(window).load(function(){
    loading = false;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
    parentRef = parent.frames['iframe'].window;
    getAjaxObject(ipAddress+"/master/type/list/?name=activity","GET",getActivityTypes1,onError);
    init();
    getAjaxObject(ipAddress+"/homecare/activity-types/?is-active=1&is-deleted=0&sort=type","GET",getTaskTypes,onError);

    buttonEvents();
    adjustHeight();
    $('.btnActive').trigger('click');
}

function init(){
    // getAjaxObject(ipAddress+"/master/type/list/?name=component","GET",getComponentTypes,onError);
    $("#cmbType").kendoComboBox();
    $("#txtID").kendoComboBox();


    getAjaxObject(ipAddress+"/master/type/list/?name=component","GET",getComponentTypes,onError);
    // getAjaxObject(ipAddress+"/activity/list?is-active=1&is-deleted=0&sort=activity","GET",getStateList1,onError);

    // setDataForSelection(compType, "cmbType", onCompTypeChange, ["Key", "Value"], 0, "");
    // setDataForSelection(typeArr, "txtID", onTYpeChange, ["Key", "Value"], 0, "");
}

function onTYpeChange(){
    var txtID = $("#txtID").data("kendoComboBox");
    if(txtID && txtID.selectedIndex<0){
        txtID.select(0);
    }
    if(txtID){
        var strVal = txtID.value();
        if(strVal  == '1' || strVal == '2' ){
            /*$("#divDuration").show();*/
            /*$("#divCharge").show();*/
            $("#txtDuration").attr("readonly",false);
            $("#txtCharge").attr("readonly",false);
            $("#spnDuration").hide();
            $("#spnCharge").hide();
        }else{
            $("#divDuration").hide();
            $("#divCharge").hide();
            //$("#txtDuration").attr("readonly",true);
            //$("#txtCharge").attr("readonly",true);
            $("#spnDuration").hide();
            $("#spnCharge").hide();
        }

    }
    if(operation == ADD) {
        var displayOrder = 0;
        displayOrder = _.where(actComponents, {activityTypeId: parseInt(strVal)}).length + 1;
        $("#txtDisplayOrder").val(displayOrder);
    }
}
function onCompTypeChange(){
    var cmbType = $("#cmbType").data("kendoComboBox");
    if(cmbType && cmbType.selectedIndex<0){
        cmbType.select(0);
    }
    $("#taDesc").val("");
    $("#taDesc").attr("readonly",false);
    if(cmbType.text().toLowerCase() == "check box" || cmbType.text().toLowerCase() == "diagram" || cmbType.text().toLowerCase() == "image"){
        $("#taDesc").attr("readonly",true);
    }

    if(cmbType.text().toLowerCase() == "diagram"){
        $("#divCamera").css("display","none");
    }
    else{
        $("#divCamera").css("display","");
    }

    if(operation == UPDATE && selItem && cmbType.text() != "check box"){
        $("#taDesc").val(selItem.description);
    }
}

function getComponentTypes(dataObj){
    var tempCompType = [];
    compType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.typeMaster){
            if($.isArray(dataObj.response.typeMaster)){
                tempCompType = dataObj.response.typeMaster;
            }else{
                tempCompType.push(dataObj.response.typeMaster);
            }
        }
    }
    for(var i=0;i<tempCompType.length;i++){
        var obj = {};
        obj.Key = tempCompType[i].type;
        obj.Value = tempCompType[i].id;
        compType.push(obj);
    }
    setDataForSelection(compType, "cmbType", onCompTypeChange, ["Key", "Value"], 0, "");
    getAjaxObject(ipAddress+"/master/type/list/?name=activity","GET",getActivityTypes1,onError);

}


function getActivityTypes1(dataObj){
    // console.log('init');

    var tempCompType = [];
    typeArr = [];

    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.typeMaster) {
            if ($.isArray(dataObj.response.typeMaster)) {
                tempCompType = dataObj.response.typeMaster;
            } else {
                tempCompType.push(dataObj.response.typeMaster);
            }
        }
    }
    tempCompType.sort(function (a, b) {
        var nameA = a.type.toUpperCase(); // ignore upper and lowercase
        var nameB = b.type.toUpperCase(); // ignore upper and lowercase
        if (nameA < nameB) {
            return -1;
        }
        if (nameA > nameB) {
            return 1;
        }
        // names must be equal
        return 0;
    });

    for (var i = 0; i < tempCompType.length; i++) {
        var obj = {};
        obj.Key = tempCompType[i].type;
        obj.Value = tempCompType[i].id;
        typeArr.push(obj);

    }

    setDataForSelection(typeArr, "txtID", onTYpeChange, ["Key", "Value"], 0, "");


    if(operation == UPDATE && selItem){
        console.log(selItem);

        componentId = selItem.idk;
        IsThumbnailPresent = selItem.thumbnailPresent;
        $("#txtAddID").html("ID :"+componentId);
        $("#txtActivityName").val(selItem.activity);
        $("#txtDisplayName").val(selItem.reportDisplayName);
        $("#txtAbbrevation").val(selItem.abbr);
        $("#taDesc").val(selItem.description);
        $("#txtDuration").val(selItem.duration);
        $("#txtCharge").val(selItem.charge);
        $("#txtDisplayOrder").val(selItem.displayOrder);

        if(selItem.remarksRequired == 1){
            $("#chkRemarks").prop("checked",true);
        }
        else{
            $("#chkRemarks").prop("checked",false);
        }
        if(selItem.camera == 1){
            $("#chkCamera").prop("checked",true);
        }
        else{
            $("#chkCamera").prop("checked",false);
        }

        onShowImage();
        // if (componentId != "") {
        //     var imageServletUrl = ipAddress + "/homecare/download/components/thumbnail/?id=" + componentId + "&access_token=" + sessionStorage.access_token + "&tenant=" + sessionStorage.tenant;
        //     $("#imgPhoto").attr("src", imageServletUrl);
        // }

        $("#cmbStatus").val(selItem.isActive);
        var txtID = $("#txtID").data("kendoComboBox");
        if(txtID){
            var aid = getActivityNameById(selItem.activityTypeId);
            txtID.select(aid);
        }
        var cmbType = $("#cmbType").data("kendoComboBox");
        if(cmbType){
            var aid = getTypeNameById(selItem.componentId);
            cmbType.select(aid);
        }
        onTYpeChange();
        onCompTypeChange();
    }
    else{
        var txtID = $("#txtID").data("kendoComboBox");
        var strVal = txtID.value();
        var displayOrder = 0;
        displayOrder = _.where(actComponents, {activityTypeId: parseInt(strVal)}).length + 1;
        $("#txtDisplayOrder").val(displayOrder);
    }

}

function getTaskTypes(dataObj){
    console.log(dataObj);

    var types = [];
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            if(dataObj.response.activityTypes){
                if($.isArray(dataObj.response.activityTypes)){
                    types = dataObj.response.activityTypes;
                }else{
                    types.push(dataObj.response.activityTypes);
                }
            }
            for(var i=0;i<types.length;i++){
                types[i].idk = types[i].id;
            }
        }
    }

    if (types !== null && types.length > 0) {
        types.sort(sortByDisplayOrderAsc);
    }

    buildDeviceListGrid1(types);

}

function getActivityTypes(dataObj){
    var tempCompType = [];
    typeArr = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.typeMaster){
            if($.isArray(dataObj.response.typeMaster)){
                tempCompType = dataObj.response.typeMaster;
            }else{
                tempCompType.push(dataObj.response.typeMaster);
            }
        }
    }
    for(var i=0;i<tempCompType.length;i++){
        var obj = {};
        obj.Key = tempCompType[i].type;
        obj.Value = tempCompType[i].id;
        typeArr.push(obj);
    }

    showGrid();
}
function showGrid(){
    buildStateListGrid([]);
    getAjaxObject(ipAddress+"/activity/list?is-active=1&is-deleted=0&sort=activity","GET",getStateList,onError);

}
function onError(errorObj){
    console.log(errorObj);
}

function getActivityNameById(aId){
    for(var i=0;i<typeArr.length;i++){
        var item = typeArr[i];
        if(item && item.Value == aId){
            return item.Key;
        }
    }
    return "";
}
function getTypeNameById(aId){
    for(var i=0;i<compType.length;i++){
        var item = compType[i];
        if(item && item.Value == aId){
            return item.Key;
        }
    }
    return "";
}
var actComponents;
function getStateList(dataObj){
    console.log(dataObj);
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.activity){
        if($.isArray(dataObj.response.activity)){
            dataArray = dataObj.response.activity;
        }else{
            dataArray.push(dataObj.response.activity);
        }

        var maxDisOrder = getMax(dataObj.response.activity, "displayOrder");
        var displayOrderMaxlength = maxDisOrder.displayOrder;

        sessionStorage.setItem("displayOrderMaxlength", (displayOrderMaxlength + 1));
    }
    var tempDataArry = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].displayOrder1 = dataArray[i].displayOrder;
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
        }
        dataArray[i].activityID = getActivityNameById(dataArray[i].activityTypeId);
        dataArray[i].Type = getTypeNameById(dataArray[i].componentId);

        if(dataArray[i].Type.toLowerCase() == "diagram") {
            dataArray[i].photo = ipAddress + "/homecare/download/activities/?" + Math.round(Math.random() * 1000000) +"&access_token=" + sessionStorage.access_token + "&id=" + dataArray[i].id + "&tenant=" + sessionStorage.tenant;
        }
        else{
            if (dataArray[i].thumbnailPresent == "1") {
                dataArray[i].photo = ipAddress + "/homecare/download/activities/thumbnail/?" + Math.round(Math.random() * 1000000) +"&access_token=" + sessionStorage.access_token + "&id=" + dataArray[i].id + "&tenant=" + sessionStorage.tenant;
            }
        }
        if(dataArray[i].remarksRequired == 1){
            dataArray[i].remarks = "Yes";
        }
        else{
            dataArray[i].remarks = "No";
        }

        if(dataArray[i].camera == 1){
            dataArray[i].addCamera = "Yes";
        }
        else{
            dataArray[i].addCamera = "No";
        }
    }


    actComponents = dataArray;
    //buildStateListGrid(dataArray);
}

function getInactiveDataList(dataObj){
    console.log(dataObj);
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.activity){
        if($.isArray(dataObj.response.activity)){
            dataArray = dataObj.response.activity;
        }else{
            dataArray.push(dataObj.response.activity);
        }
    }
    var tempDataArry = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].displayOrder1 = dataArray[i].displayOrder;
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
        }
        dataArray[i].activityID = getActivityNameById(dataArray[i].activityTypeId);
        dataArray[i].Type = getTypeNameById(dataArray[i].componentId);
        if(dataArray[i].Type.toLowerCase() == "diagram") {
            dataArray[i].photo = ipAddress + "/homecare/download/activities/?" + Math.round(Math.random() * 1000000) +"&access_token=" + sessionStorage.access_token + "&id=" + dataArray[i].id + "&tenant=" + sessionStorage.tenant;
        }
        else{
            if (dataArray[i].thumbnailPresent == "1") {
                dataArray[i].photo = ipAddress + "/homecare/download/activities/thumbnail/?" + Math.round(Math.random() * 1000000) +"&access_token=" + sessionStorage.access_token + "&id=" + dataArray[i].id + "&tenant=" + sessionStorage.tenant;
            }
        }

        if(dataArray[i].remarksRequired == 1){
            dataArray[i].remarks = "Yes";
        }
        else{
            dataArray[i].remarks = "No";
        }

        if(dataArray[i].camera == 1){
            dataArray[i].addCamera = "Yes";
        }
        else{
            dataArray[i].addCamera = "No";
        }
    }
    actComponents = dataArray;
    var selectedItems = angularUIgridWrapper1.getSelectedRows();
    if(selectedItems && selectedItems.length > 0) {
        var filterArray = [];
        filterArray = _.where(actComponents, {activityTypeId: selectedItems[0].idk});
        buildStateListGrid(filterArray);
    }
}

function buttonEvents(){
    $("#btnEdit").off("click",onClickOK);
    $("#btnEdit").on("click",onClickOK);

    $("#btnDelete").off("click",onClickDelete);
    $("#btnDelete").on("click",onClickDelete);

    $("#btnAdd").off("click");
    $("#btnAdd").on("click",onClickAdd);

    $("#btnSaveDO").off("click",onClickSaveDO);
    $("#btnSaveDO").on("click",onClickSaveDO);

    $("#btnReset").off("click", onClickReset);
    $("#btnReset").on("click", onClickReset);

    $(".popupClose").off("click", onClickCancel);
    $(".popupClose").on("click", onClickCancel);

    $("#btnSave").off("click",onClickSave);
    $("#btnSave").on("click",onClickSave);

    // $("#btnSave").off("click",onClickSave);
    // $("#btnSave").on("click", function(e) {
    //     e.preventDefault();
    //     // buildStateListGrid([]);
    //     onClickSave();
    // });


    $("#btnBrowse").on("click", function(e) {
        e.preventDefault();
        if(IsRemove == 1){
            $("#imgPhoto").attr("src","");
        }
        onClickBrowse(e);
    });

    //
    // $("#btnBrowse").off("click", onClickBrowse);
    // $("#btnBrowse").on("click", onClickBrowse);

    $("#btnBrowse").off("mouseover", onClickBrowseOver);
    $("#btnBrowse").on("mouseover", onClickBrowseOver);

    $("#btnBrowse").off("mouseout", onClickBrowseOut);
    $("#btnBrowse").on("mouseout", onClickBrowseOut);

    $("#fileElem").off("change", onSelectionFiles);
    $("#fileElem").on("change", onSelectionFiles);

    $("#fileElem").off("click", onSelectionFiles);
    $("#fileElem").on("click", onSelectionFiles);

    $("#btnRemove").off("click", onClickRemove);
    $("#btnRemove").on("click", onClickRemove);


    $(".btnActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('active');
        onClickActive();
    });
    $(".btnInActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('inactive');
        onClickInActive();
    });

    // $("#appointment-task").off("click");
    // $("#appointment-task").on("click",onClickAddTasks);
}

function onClickActive() {
    $(".btnInActive").removeClass("radioButton-active");
    $(".btnActive").addClass("radioButton-active");
}
function onClickInActive() {
    $(".btnActive").removeClass("radioButton-active");
    $(".btnInActive").addClass("radioButton-active");
}

function onClickRemove(){
    if (validation()) {
        var strName = $("#txtActivityName").val();
        strName = $.trim(strName);
        var txtID = $("#txtID").data("kendoComboBox");
        var cmbType = $("#cmbType").data("kendoComboBox");
        var taDesc = $("#taDesc").val();
        taDesc = $.trim(taDesc);
        var txtDuration = $("#txtDuration").val();
        txtDuration = $.trim(txtDuration);

        var txtCharge = $("#txtCharge").val();

        var isActive = $("#cmbStatus").val();

        var IsRemarksChecked = 0;
        var IsCameraChecked = 0;

        if ($("#chkRemarks").is(':checked')) {
            IsRemarksChecked = 1;
        }

        if ($("#chkCamera").is(':checked')) {
            IsCameraChecked = 1;
        }


        var dataObj = {};
        dataObj.activity = strName;
        dataObj.description = taDesc;
        dataObj.duration = Number(txtDuration);
        dataObj.activityTypeId = Number(txtID.value());
        dataObj.componentId = Number(cmbType.value());
        dataObj.charge = Number(txtCharge);
        dataObj.displayOrder = $("#txtDisplayOrder").val();
        dataObj.remarksRequired = IsRemarksChecked;
        dataObj.camera = IsCameraChecked;


        dataObj.isActive = isActive;
        dataObj.createdBy = Number(sessionStorage.userId);//"101";//sessionStorage.uName;
        if (isActive == 1) {
            dataObj.isDeleted = 0;
        } else {
            dataObj.isDeleted = 1;
        }

        dataObj.thumbnailPresent = 0;
        dataObj.id = selItem.idk;
        var dataUrl = ipAddress + "/activity/update";
        createAjaxObject(dataUrl, dataObj, "POST", onRemove, onError);

    }
}

function searchOnLoad(status) {
    buildStateListGrid([]);
    if(status == "active") {
        var urlExtn = '/activity/list?is-active=1&is-deleted=0&sort=activity';
    }
    else if(status == "inactive") {
        var urlExtn = '/activity/list?is-active=0&is-deleted=1&sort=activity';
    }
    getAjaxObject(ipAddress+urlExtn,"GET",getInactiveDataList,onError);
}

function adjustHeight(){
    var defHeight = 200;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    angularUIgridWrapper.adjustGridHeight(cmpHeight);
}

function buildStateListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Image",
        "field": "idk",
        "enableColumnMenu": false,
        "width": "10%",
        "cellTemplate":showPtImage()
    });
    gridColumns.push({
        "title": "Name",
        "field": "activity",
    });
    // gridColumns.push({
    //    "title": "Task",
    //    "field": "activityID",
    // });

    gridColumns.push({
        "title": "Type",
        "field": "Type",
        "width": "10%",
    });
    gridColumns.push({
        "title": "Rem",
        "field": "remarks",
        "width": "8%"
    });
    gridColumns.push({
        "title": "Cam",
        "field": "addCamera",
        "width": "8%"
    });
    gridColumns.push({
        "title": "Column Name",
        "field": "reportDisplayName",
    });
    gridColumns.push({
        "title": "Disp Order",
        "field": "displayOrder",
        "cellTemplate":showDefaultValue(),
        "width": "14%",
    });
    // gridColumns.push({
    //     "title": "Duration",
    //     "field": "duration",
    // });*/
    /*gridColumns.push({
        "title": "Status",
        "field": "Status",
    });*/
    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}

function showPtImage(){
    var node = '<div><img src="{{row.entity.photo}}" onerror="this.src=\'../../img/AppImg/HosImages/blank.png\'" style="width:50px"><spn>';
    node = node+"</div>";
    return node;
}

function showDefaultValue(){
    var node = '<div>';
    node += '<input  type="number" id="txtI2Val" ng-model="row.entity.displayOrder" value="{{row.entity.displayOrder}}" class="txtField" min="1"  max="1000" maxlength="5" onchange="maxLengthCheck(event)" style="height:25px;width:100px"></input>';
    node += '</div>';
    return node;
}

var selRow = null;
function maxLengthCheck(e){
    console.log(e);
    selRow = angular.element($(e.currentTarget).parent()).scope();
    setTimeout(function(){
        upDateDataSource();
    },100)
}

function upDateDataSource(){
    var selectedItems = angularUIgridWrapper.getSelectedRows();
    console.log(selectedItems);
    var selRowIndex = 0;
    if(selRow){
        var selItem = selRow.row.entity;//selectedItems[0];
        selRowIndex = selItem.idk;
        var idVal = selItem.displayOrder;
        idVal = Number(idVal);

        var begin = selItem.displayOrder;
        begin = Number(begin);

        var end = selItem.displayOrder1;
        end = Number(end);

        //selItem.DIO1 = end;
        //angularUIgridWrapper.refreshGrid();
        var rows = angularUIgridWrapper.getScope().gridApi.core.getVisibleRows();
        console.log(rows);

        for(var i=0;i<rows.length;i++){
            var rowItem = rows[i].entity;
            if(rowItem.id == selRow.row.entity.id){
                continue;
            }
            var rowValue = rowItem.displayOrder;
            if(end>begin){
                flag = true;
                if(rowValue>=begin && rowValue<end){
                    var rValue = (rowItem.displayOrder+1);
                    rowItem.displayOrder = rValue;
                    rowItem.displayOrder1 = rValue;
                }
            }else{
                flag = false;
                if(rowValue>end && rowValue < (begin+1)){
                    var rValue = (rowItem.displayOrder-1);
                    rowItem.displayOrder = rValue;
                    rowItem.displayOrder1 = rValue;
                }
            }
        }
        if(flag){
            selRow.row.entity.displayOrder1 = begin;
        }else{
            selRow.row.entity.displayOrder1 = begin;
        }
        angularUIgridWrapper.refreshGrid();
    }
}

function GetSortOrder(prop) {
    return function(a, b) {
        if (a[prop] > b[prop]) {
            return 1;
        } else if (a[prop] < b[prop]) {
            return -1;
        }
        return 0;
    }
}

var prevSelectedItem =[];
function onChange(){
    setTimeout(function(){
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if(selectedItems && selectedItems.length>0){
            selItem = selectedItems[0];
            $("#btnEdit").prop("disabled", false);
            $("#btnDelete").prop("disabled", false);
        }else{
            $("#btnEdit").prop("disabled", true);
            $("#btnDelete").prop("disabled", true);
        }
    },100)

}
function onChange1(){
    buildStateListGrid([]);
    setTimeout(function(){
        var selectedItems = angularUIgridWrapper1.getSelectedRows();
        // console.log(selectedItems);
        // console.log(actComponents);
        // console.log(_.where(actComponents, {activityTypeId: selectedItems[0].idk}));
        if(selectedItems && selectedItems.length>0){
            // $("#btnEdit").prop("disabled", false);
            // $("#btnDelete").prop("disabled", false);
            var filterArray = [];
            filterArray = _.where(actComponents, {activityTypeId: selectedItems[0].idk});
            buildStateListGrid(filterArray);
        }else{

            // $("#btnDelete").prop("disabled", true);
        }
    },100)

}

function onClickOK(){
    setTimeout(function(){
        $("#txtAddID").show();
        $(".filter-heading").html("Edit Task Components");
        // var tagFileId = ("fileElem");
        // var file = document.getElementById(tagFileId);
        // if(file && file.files[0] && file.files[0].name){
        //     fileName = null;
        //     file.files[0].name = null;
        //     file.files[0] = null;
        // }

        operation = "edit";
        parentRef.operation = "edit";
        isBrowseFlag = false;
        $("#viewDivBlock").hide();
        $(".taskComponentLeft").hide();
        $("#addTaskGroupPopup").show();
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if(selectedItems && selectedItems.length>0){
            parentRef.selitem = selectedItems[0];
            parentRef.operation = "update";
            selItem = selectedItems[0];
            // adlAdd("update");
            // getAjaxObject(ipAddress+"/master/type/list/?name=component","GET",getComponentTypes,onError);
            setDataForSelection(compType, "cmbType", onCompTypeChange, ["Key", "Value"], 0, "");
            setDataForSelection(typeArr, "txtID", onTYpeChange, ["Key", "Value"], 0, "");
            componentId = selItem.idk;
            $("#txtAddID").html("ID :"+componentId);
            $("#txtActivityName").val(selItem.activity);
            $("#txtDisplayName").val(selItem.reportDisplayName);
            $("#txtAbbrevation").val(selItem.abbr);
            $("#taDesc").val(selItem.description);
            $("#txtDuration").val(selItem.duration);
            $("#txtCharge").val(selItem.charge);
            $("#txtDisplayOrder").val(selItem.displayOrder);
            IsThumbnailPresent = selItem.thumbnailPresent;

            if(selItem.remarksRequired == 1){
                $("#chkRemarks").prop("checked",true);
            }
            else{
                $("#chkRemarks").prop("checked",false);
            }
            if(selItem.camera == 1){
                $("#chkCamera").prop("checked",true);
            }
            else{
                $("#chkCamera").prop("checked",false);
            }

            onShowImage();
            // if (componentId != "") {
            //     var imageServletUrl = ipAddress + "/homecare/download/components/thumbnail/?id=" + componentId + "&access_token=" + sessionStorage.access_token + "&tenant=" + sessionStorage.tenant;
            //     $("#imgPhoto").attr("src", imageServletUrl);
            // }


            $("#cmbStatus").val(selItem.isActive);
            var txtID = $("#txtID").data("kendoComboBox");
            if(txtID){
                var aid = getActivityNameById(selItem.activityTypeId);
                txtID.value(selItem.activityTypeId);
            }
            var cmbType = $("#cmbType").data("kendoComboBox");
            if(cmbType){
                var aid = getTypeNameById(selItem.componentId);
                cmbType.value(selItem.componentId);
            }

            onCompTypeChange();
            onTYpeChange();
        }
    })
}
function onClickDelete(){
    customAlert.confirm("Confirm", "Are you sure you want delete?",function(response){
        if(response.button == "Yes"){
            var selectedItems = angularUIgridWrapper.getSelectedRows();
            console.log(selectedItems);
            if(selectedItems && selectedItems.length>0){
                var selItem = selectedItems[0];
                if(selItem){
                    var dataUrl = ipAddress+"/activity/delete/";
                    var reqObj = {};
                    reqObj.id = selItem.idk;
                    reqObj.abbr = selItem.abbr;
                    reqObj.country = selItem.county;
                    reqObj.code = selItem.code;
                    reqObj.isDeleted = "1";
                    reqObj.isActive = "0";
                    reqObj.modifiedBy = sessionStorage.userId;//101
                    createAjaxObject(dataUrl,reqObj,"POST",onDeleteCountryt,onError);
                }
            }
        }
    });
}
function onDeleteCountryt(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            customAlert.info("info", "Task Component Deleted Successfully");
            showGrid();
        }else{
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}
function onClickAdd(){
    isBrowseFlag = false;
    $("#btnRemove").hide();
    $("#addTaskGroupPopup").show();
    $('#viewDivBlock').hide();
    $('.taskComponentLeft').hide();
    $("#txtAddID").hide();
    $(".filter-heading").html("Add Task Component");
    parentRef.selitem = null;
    operation = ADD;
    onCompTypeChange();
    onClickReset();

    var selectedItems = angularUIgridWrapper1.getSelectedRows();
    if(selectedItems && selectedItems.length>0) {
        var txtID = $("#txtID").data("kendoComboBox");
        if(txtID){
            var aid = getActivityNameById(selectedItems[0].idk);
            // txtID.select(aid);
            txtID.value(selectedItems[0].idk);
        }
    }

}

function adlAdd(operation){
    var popW = 800;
    var popH = 400;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    if(operation == "update"){
        profileLbl = "Edit Task Component";
    }
    else {
        profileLbl = "Add Task Component";
    }
    parentRef.operation = operation;//"add";
    devModelWindowWrapper.openPageWindow("../../html/masters/createADL.html", profileLbl, popW, popH, true, closeAddFacilitytAction);
}

function closeAddFacilitytAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        if(returnData.operation == "add"){
            customAlert.info("info", "Task Component Saved Successfully");
        }else{
            customAlert.info("info", "Task Component Updated Successfully");
        }
        onLoaded();
        // $(".btnActive").click();

    }
}

function onClickCancel(){
    // buildDeviceListGrid1([]);
    // buildStateListGrid([]);
    $(".filter-heading").html("View Task Components");
    $("#viewDivBlock").show();
    $(".taskComponentLeft").show();
    $("#addTaskGroupPopup").hide();
    $("#viewDivBlock").css("display","");
    $(".taskComponentLeft").css("display","");
    $("#addTaskGroupPopup").css("display","none");
    // onLoaded();

    showGrid();

    onChange1();

    // angularUIgridWrapper1.refreshGrid();
    // angularUIgridWrapper.refreshGrid();
    onClickReset();
    // onChange1();
    // $(".btnActive").click();
    $("#fileElem").val("");
    var tagFileId = ("fileElem");
    var file = document.getElementById(tagFileId);
    if(file && file.files[0] && file.files[0].name){
        fileName = null;
        file.files[0].name = null;
        file.files[0] = null;
    }

    $("#btnRemove").hide();
    imagePhotoData = null;
    files = null;
    fileName = "";
    IsRemove = 1;
    //
    // $("#imgFieldSet").trigger("update");

}

var allRosterRowCount = 0;
var allRosterRowIndex = 0;
var allRosterRows = [];

function onClickSaveDO(){
    allRosterRowCount = 0;
    allRosterRowIndex = 0;
    allRosterRows = angularUIgridWrapper.getAllRows();
    allRosterRowCount = allRosterRows.length;
    if(allRosterRowCount>0){
        createDisplayOrder();
    }

}
function createDisplayOrder(){
    if(allRosterRowCount>allRosterRowIndex){
        var objRosterEntiry = allRosterRows[allRosterRowIndex];
        var rosterObj = objRosterEntiry.entity;
        var dataObj = {};
        dataObj.modifiedBy = Number(sessionStorage.userId);;
        dataObj.isDeleted = 0;
        dataObj.isActive = 1;
        dataObj.activity = rosterObj.activity;
        dataObj.description = rosterObj.description;
        dataObj.duration = rosterObj.duration;
        dataObj.activityTypeId = rosterObj.activityTypeId;
        dataObj.componentId = rosterObj.componentId;
        dataObj.charge = rosterObj.charge;
        dataObj.displayOrder = rosterObj.displayOrder;
        dataObj.camera = rosterObj.camera;
        dataObj.thumbnailPresent = rosterObj.thumbnailPresent;
        dataObj.remarksRequired = rosterObj.remarksRequired;
        dataObj.reportDisplayName = rosterObj.reportDisplayName;
        dataObj.id = rosterObj.idk;
        var dataUrl = ipAddress+"/activity/update";

        createAjaxObject(dataUrl,dataObj,"POST",onCreateDS,onError);
    }
}

function onCreateDS(dataObj){
    allRosterRowIndex = allRosterRowIndex+1;
    if(allRosterRowCount == allRosterRowIndex){
        customAlert.error("Info", "Display order updated successfully");
        // init();
        $(".btnActive").click();
    }else{
        createDisplayOrder();
    }
}

function getMax(arr, prop) {
    var max;
    var j=-1;
    for (var i=0 ; i<arr.length ; i++) {
        if (!max || parseInt(arr[i][prop]) > parseInt(max[prop]))
            max = arr[i];
    }
    j++;
    return max;
}
function buildDeviceListGrid1(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Task Groups",
        "field": "type"
    });
    gridColumns.push({
        "title": "Display Order",
        "field": "displayOrder"
    });
    angularUIgridWrapper1.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}
function bindTheGrid(){
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("dgridADLList", dataOptions);
    angularUIgridWrapper.init();

    var dataOptions1 = {
        pagination: false,
        changeCallBack: onChange1
    }
    angularUIgridWrapper1 = new AngularUIGridWrapper("dgridActivTypeList", dataOptions1);
    angularUIgridWrapper1.init();

    buildStateListGrid([]);
    buildDeviceListGrid1([]);
}

function onClickAddTasks(){
    var popW = "60%"
    var popH = 520;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Appointment Reason Task List";
    devModelWindowWrapper.openPageWindow("../../html/masters/attachLinkTasks.html", profileLbl, popW, popH, true, closeTasksList);
}

function closeTasksList(evt,returnData){
    if(returnData && returnData.status == "success"){
        customAlert.info("info", "Appointment Reason Updated Successfully");
    }
}

function themesChange(themeValue){
    if(themeValue == 2){
        loadAPi("../../Theme2/Theme02.css");
    }
    else if(themeValue == 3){
        loadAPi("../../Theme3/Theme03.css");
    }
}

function themeAPIChange(){
    getAjaxObject(ipAddress+"/homecare/settings/?id=2","GET",getThemeValue,onError);
}

function getThemeValue(dataObj){
    console.log(dataObj);
    var tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.activityTypes){
            if($.isArray(dataObj.response.settings)){
                tempCompType = dataObj.response.settings;
            }else{
                tempCompType.push(dataObj.response.settings);
            }
        }
        if(tempCompType.length > 0){
            themesChange(tempCompType[0].value);
        }
    }
}


function validation(){
    var flag = true;
    var strName = $("#txtActivityName").val();
    strName = $.trim(strName);
    if(strName == ""){
        customAlert.error("Error","Enter task name");
        flag = false;
        return false;
    }
    var txtID = $("#txtID").data("kendoComboBox");
    if(txtID){
        var strVal = txtID.value();
        if(strVal == '1' || strVal == '2' ){
            var txtDuration = $("#txtDuration").val();
            txtDuration = $.trim(txtDuration);
            if(txtDuration == ""){
                customAlert.error("Error","Enter duration");
                flag = false;
                return false;
            }
            var txtCharge = $("#txtCharge").val();
            txtCharge = $.trim(txtCharge);
            if(txtCharge == ""){
                customAlert.error("Error","Enter charge");
                flag = false;
                return false;
            }
        }
    }
    return flag;
}

function onClickReset() {
    if(operation == ADD) {
        operation = ADD;
    }
    else {
        operation = UPDATE;
    }
    $("#txtActivityName").val("");
    $("#txtDisplayName").val("");

    var txtID = $("#txtID").data("kendoComboBox");
    if(txtID){
        txtID.select(0);
    }

    $("#txtDuration").val("0");
    $("#txtCharge").val("0");
    $("#taDesc").val("");
    $("#cmbStatus").val(1);
    var cmbType = $("#cmbType").data("kendoComboBox");
    if(cmbType){
        cmbType.select(0);
    }

    $("#imgPhoto").attr("src", "");
}


var actComponents;
function getStateList1(dataObj){
    console.log(dataObj);
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.activity){
        if($.isArray(dataObj.response.activity)){
            dataArray = dataObj.response.activity;
        }else{
            dataArray.push(dataObj.response.activity);
        }

    }
    var tempDataArry = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].displayOrder1 = dataArray[i].displayOrder;
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
        }
        dataArray[i].activityID = getActivityNameById(dataArray[i].activityTypeId);
        dataArray[i].Type = getTypeNameById(dataArray[i].componentId);

        if(dataArray[i].remarksRequired == 1){
            dataArray[i].remarks = "Yes";
        }
        else{
            dataArray[i].remarks = "No";
        }

        if(dataArray[i].camera == 1){
            dataArray[i].addCamera = "Yes";
        }
        else{
            dataArray[i].addCamera = "No";
        }
    }
    actComponents = dataArray;
    //buildStateListGrid(dataArray);
}

function onSelectionFiles(event) {
    var imageCompressor = new ImageCompressor();
    console.log(event);
    files = event.target.files;
    fileName = "";
    //$('#txtFU').val("");
    if (files) {
        if (files.length > 0) {
            fileName = files[0].name;
            isBrowseFlag = true;
            // if (patientId != "") {
            var oFReader = new FileReader();
            oFReader.readAsDataURL(files[0]);
            oFReader.onload = function(oFREvent) {
                document.getElementById("imgPhoto").src = oFREvent.target.result;
                if (imageCompressor) {
                    imageCompressor.run(oFREvent.target.result, compressorSettings, function(small) {
                        document.getElementById("imgPhoto").src = small;
                        //isBrowseFlag = false;
                        imagePhotoData = small;
                        return small;
                    });
                }

            }
            // }
        }
    }
}

function onClickBrowse(e) {
    removeSelectedButtons();
    $("#btnBrowse").addClass("selClass");
    // $("#imgPhoto").attr("src", "");
    // isBrowseFlag = true;

    // if(IsRemove == 1){
    //     $("#imgPhoto").attr("src","");
    // }
    $("#imgPhoto").show();
    console.log(e);
    if (e.currentTarget && e.currentTarget.id) {
        var btnId = e.currentTarget.id;
        var fileTagId = ("fileElem" + btnId);
        var cmbType = $("#cmbType").data("kendoComboBox");
        if(cmbType.text().toLowerCase() == "diagram"){
            IsThumbnailPresent = 0;
        }else{
            IsThumbnailPresent = 1;
        }
        $("#fileElem").click();
    }
}

var IsRemove =0;
function removeSelectedButtons() {
    $("#btnBrowse").removeClass("selClass");
}

function onClickBrowseOver() {
    removeOverSelection();
    $("#btnBrowse").addClass("borderClass");
}

function onClickBrowseOut() {
    removeOverSelection();
}

function removeOverSelection() {
    $("#btnBrowse").removeClass("borderClass");
}

function onClickUploadPhoto() {
    if (componentId != "") {
        removeSelectedButtons();
        $("#btnUpload").addClass("selClass");
        if (isBrowseFlag) {
            //imagePhotoData
            var reqUrl;

            var cmbType = $("#cmbType").data("kendoComboBox");
            if(cmbType.text().toLowerCase() == "diagram") {
                reqUrl = ipAddress + "/homecare/upload/activities/";
            }
            else{
                reqUrl = ipAddress + "/homecare/upload/activities/thumbnail/";
            }

            reqUrl = reqUrl+"?access_token="+sessionStorage.access_token + "&id="+componentId;

            // var reqUrl = ipAddress + "/upload/patient/photo/stream/?patient-id=" + patientId + "&photo-ext=png&access_token=" + sessionStorage.access_token + "&tenant=" + sessionStorage.tenant
            // var xmlhttp = new XMLHttpRequest();
            // xmlhttp.open("POST", reqUrl, true);
            // //xmlhttp.open("POST", "/ROOT/upload/patient/photo/?patient-id=101&photo-ext=png", true);
            // xmlhttp.send(imagePhotoData);
            // xmlhttp.onreadystatechange = function() { //Call a function when the state changes.
            //     if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            //         customAlert.error("Info", "Image uploaded successfully");
            //     }
            // }


            var tagFileId = ("fileElem");
            var file = document.getElementById(tagFileId);

            if(file && file.files[0] && file.files[0].name){
                fileName = file.files[0].name;
            }

            var form = new FormData();
            if(operation != UPDATE){
                form.append("file",file.files[0]);
            }else{
                form.append("file",file.files[0]);
            }


            // var settings = {
            //     "async": true,
            //     "crossDomain": true,
            //     "url": reqUrl,
            //     "method": "POST",
            //     "processData": false,
            //     "contentType": false,
            //     "data": form,
            //     "headers": {
            //         "tenant": sessionStorage.tenant,
            //         "contentType": "multipart/form-data",
            //         "cache-control": "no-cache",
            //     },
            // }
            //
            // $.ajax(settings).done(function (response) {
            //     console.log(response);
            // });

            Loader.showLoader();
            $.ajax({
                url: reqUrl,
                type: 'POST',
                data: form,
                processData: false,
                contentType:false,// "multipart/form-data",
                // contentType: "application/json",
                headers: {
                    tenant: sessionStorage.tenant
                },
                success: function(data, textStatus, jqXHR){
                    if(typeof data.error === 'undefined'){
                        Loader.hideLoader();
                        onSuccess(data);
                    }else{
                        Loader.hideLoader();
                    }
                },
                error: function(jqXHR, textStatus, errorThrown){
                    Loader.hideLoader();
                }
            });

        }
    }
}


function onSuccess(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg;
            if(operation == ADD){
                msg = "Task Component Saved Successfully";
            }else{
                msg = "Task Component Updated Successfully";
            }
            var obj = {};
            obj.status = "success";
            obj.operation = operation;
            // displaySessionErrorPopUp("Info", msg, function(res) {

            // onLoaded();
            // onChange1();
            // buildStateListGrid([]);
            // buildDeviceListGrid1([]);
            getAjaxObject(ipAddress + "/activity/list?is-active=1&is-deleted=0&sort=activity", "GET", function(dataObj) {

                if (dataObj && dataObj.response && dataObj.response.activity) {
                    if ($.isArray(dataObj.response.activity)) {
                        dataArray = dataObj.response.activity;
                    } else {
                        dataArray.push(dataObj.response.activity);
                    }

                    var maxDisOrder = getMax(dataObj.response.activity, "displayOrder");
                    var displayOrderMaxlength = maxDisOrder.displayOrder;

                    sessionStorage.setItem("displayOrderMaxlength", (displayOrderMaxlength + 1));
                }
                var tempDataArry = [];
                for (var i = 0; i < dataArray.length; i++) {
                    dataArray[i].idk = dataArray[i].id;
                    dataArray[i].displayOrder1 = dataArray[i].displayOrder;
                    dataArray[i].Status = "InActive";
                    if (dataArray[i].isActive == 1) {
                        dataArray[i].Status = "Active";
                    }
                    dataArray[i].activityID = getActivityNameById(dataArray[i].activityTypeId);
                    dataArray[i].Type = getTypeNameById(dataArray[i].componentId);

                    if (dataArray[i].Type.toLowerCase() == "diagram") {
                        dataArray[i].photo = ipAddress + "/homecare/download/activities/?" + Math.round(Math.random() * 1000000) +"&access_token=" + sessionStorage.access_token + "&id=" + dataArray[i].id + "&tenant=" + sessionStorage.tenant;
                    }
                    else {
                        if (dataArray[i].thumbnailPresent == "1") {
                            dataArray[i].photo = ipAddress + "/homecare/download/activities/thumbnail/?" + Math.round(Math.random() * 1000000) +"&access_token=" + sessionStorage.access_token + "&id=" + dataArray[i].id + "&tenant=" + sessionStorage.tenant;
                        }
                    }

                    if(dataArray[i].remarksRequired == 1){
                        dataArray[i].remarks = "Yes";
                    }
                    else{
                        dataArray[i].remarks = "No";
                    }

                    if(dataArray[i].camera == 1){
                        dataArray[i].addCamera = "Yes";
                    }
                    else{
                        dataArray[i].addCamera = "No";
                    }
                }
                actComponents = dataArray;
            }, function() {});
            //
            //
            //     // bindTheGrid();
            //
            //
            //
            //     // $(".btnActive").click();
            //     // onClickActive();
            //
            //
            // }, function() {});

            // buildStateListGrid([]);
            // buildDeviceListGrid1([]);

            // onLoaded();
            // var isRun = 0;
            // showGrid();
            // angularUIgridWrapper1.refreshGrid();
            // angularUIgridWrapper.refreshGrid();


            // setInterval(function(){
            //     if(isRun==0) {
            //         isRun=1;
            displaySessionErrorPopUp("Info", msg, function(res) {
                // operation = ADD;
                // onClickCancel();
                // onLoaded();
                // onChange1();

                // onClickReset();

                buildStateListGrid([]);
                showGrid();
                onChange1();
                angularUIgridWrapper1.refreshGrid();
                angularUIgridWrapper.refreshGrid();
            });

            // }
            // },1000);
            // onClickCancel();

            // })
        }else{
            customAlert.error("Error", dataObj.response.status.message);
        }
    }else{

    }
    // onClickReset();
}

function onShowImage() {
    $("#btnRemove").hide();
    var imageServletUrl;
    if (selItem) {
        var aid = getTypeNameById(selItem.componentId);
        if(aid.toLowerCase() == "diagram" ){
            imageServletUrl = ipAddress+"/homecare/download/activities/?"+ Math.round(Math.random() * 1000000) +"&access_token="+sessionStorage.access_token+"&id="+selItem.idk+"&tenant="+sessionStorage.tenant;
        }
        else{
            if(selItem.thumbnailPresent == 1){
                imageServletUrl = ipAddress+"/homecare/download/activities/thumbnail/?"+ Math.round(Math.random() * 1000000) +"&access_token="+sessionStorage.access_token+"&id="+selItem.idk+"&tenant="+sessionStorage.tenant;
                $("#btnRemove").show();
            }
        }

        // $("#imgPhoto").attr("src", imageServletUrl);
        document.getElementById("imgPhoto").src =imageServletUrl;
    } else {
        $("#imgPhoto").attr("src", "../../img/AppImg/HosImages/blank.png");
    }
}

function onClickSave(){
    if(validation()) {
        // buildStateListGrid([]);
        var strName = $("#txtActivityName").val();
        strName = $.trim(strName);
        var txtID = $("#txtID").data("kendoComboBox");
        var cmbType = $("#cmbType").data("kendoComboBox");
        var taDesc = $("#taDesc").val();
        taDesc = $.trim(taDesc);
        var txtDuration = $("#txtDuration").val();
        txtDuration = $.trim(txtDuration);

        var txtCharge = $("#txtCharge").val();

        var isActive =$("#cmbStatus").val();

        var IsRemarksChecked = 0;
        var IsCameraChecked = 0;

        if ($("#chkRemarks").is(':checked')) {
            IsRemarksChecked = 1;
        }

        if ($("#chkCamera").is(':checked')) {
            IsCameraChecked = 1;
        }


        var dataObj = {};
        dataObj.activity = strName;
        dataObj.description = taDesc;
        dataObj.duration = Number(txtDuration);
        dataObj.activityTypeId = Number(txtID.value());
        dataObj.componentId = Number(cmbType.value());
        dataObj.charge = Number(txtCharge);
        dataObj.displayOrder = $("#txtDisplayOrder").val();
        dataObj.remarksRequired = IsRemarksChecked;
        dataObj.camera = IsCameraChecked;
        dataObj.reportDisplayName = $("#txtDisplayName").val();

        dataObj.isActive = isActive;
        dataObj.createdBy = Number(sessionStorage.userId);//"101";//sessionStorage.uName;
        if(isActive == 1){
            dataObj.isDeleted = 0;
        }else{
            dataObj.isDeleted = 1;
        }

        dataObj.thumbnailPresent = IsThumbnailPresent;

        if (operation == ADD) {
            var dataUrl = ipAddress + "/activity/create";
            createAjaxObject(dataUrl, dataObj, "POST", onCreate, onError);
        } else {
            dataObj.id = selItem.idk;
            var dataUrl = ipAddress + "/activity/update";
            createAjaxObject(dataUrl, dataObj, "POST", onCreate, onError);
        }
    }
}

function onCreate(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            var cmbType = $("#cmbType").data("kendoComboBox");
            if(operation == ADD) {
                if(dataObj.response.activity){
                    componentId = dataObj.response.activity.id;
                }
                if (cmbType.text().toLowerCase() == "diagram") {
                    if (isBrowseFlag) {
                        var tagFileId = ("fileElem");
                        var file = document.getElementById(tagFileId);

                        if (file && file.files[0] && file.files[0].name) {
                            fileName = file.files[0].name;
                        }

                        onClickUploadPhoto();
                    }
                    else {
                        customAlert.error("error", "Upload Image");
                    }
                }
                else{
                    if (isBrowseFlag) {
                        onClickUploadPhoto();
                    }
                    else {
                        onSuccess(dataObj);
                    }
                }
            }
            else{
                if (isBrowseFlag) {
                    onClickUploadPhoto();
                }
                else {
                    onSuccess(dataObj);
                }
            }
        }else{
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}



function onRemove(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg;
            msg = "Image Removed Successfully";
            var obj = {};
            obj.status = "success";
            obj.operation = operation;
            displaySessionErrorPopUp("Info", msg, function(res) {
                $("#imgPhoto").attr("src", "");
                $("#fileElem").val("");
                var tagFileId = ("fileElem");
                var file = document.getElementById(tagFileId);
                if(file && file.files[0] && file.files[0].name){
                    fileName = null;
                    file.files[0].name = null;
                    file.files[0] = null;
                }

                $("#btnRemove").hide();
                imagePhotoData = null;
                files = null;
                fileName = "";
                IsRemove = 1;
                // location.reload();
                $("#imgFieldSet").trigger("update");
                buildStateListGrid([]);
                showGrid();
                onChange1();
                angularUIgridWrapper1.refreshGrid();
                angularUIgridWrapper.refreshGrid();
                // operation = ADD;
                // onLoaded();
                // onChange1();
                // showGrid();
                // onClickReset();
                // onClickCancel();
                // $(".btnActive").click();
                // onChange1();
            })
        }else{
            customAlert.error("Error", dataObj.response.status.message);
        }
    }else{

    }
    // onClickReset();
}


var sortByDisplayOrderAsc = function (x, y) {
    // This is a comparison function that will result in dates being sorted in
    // ASCENDING order. As you can see, JavaScript's native comparison operators
    // can be used to compare dates. This was news to me.


    return x.displayOrder - y.displayOrder;
};
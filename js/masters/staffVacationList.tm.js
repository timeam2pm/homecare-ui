var angularUIgridWrapper;
var parentRef = null;
var recordType = "1";
var dataArray = [];
var ADD = "add";
var UPDATE = "edit";
var VIEW = "view";
var DELETE = "delete";
var operation = "add";
var ds  = "";
var IsFlag = 1;
var cntry = sessionStorage.countryName;

$(document).ready(function(){
    $("#pnlPatient",parent.document).css("display","none");
    // $("#kendoWindowContainer1_wnd_title",parent.document).css("display","none");


    sessionStorage.setItem("IsSearchPanel", "1");
    themeAPIChange();
    parentRef = parent.frames['iframe'].window;
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("dgridLeaveRequestList", dataOptions);
    angularUIgridWrapper.init();
    buildDeviceListGrid([]);

});


$(window).load(function(){
    loading = false;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
    getLeaveTypes();
    getManagers();
    getEmails();
    init();
    buttonEvents();
    adjustHeight();
}

function init(){
    $("#btnEdit").prop("disabled", true);
    $("#btnDelete").prop("disabled", true);

    if(cntry.indexOf("India")>=0){
        $("#txtFDate").datepick({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yyyy 00:00',
            yearRange: "-200:+200"
        });
        $("#txtTDate").datepick({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yyyy 23:59',
            yearRange: "-200:+200"
        });
    }else if(cntry.indexOf("United Kingdom")>=0){
        $("#txtFDate").datepick({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yyyy 00:00',
            yearRange: "-200:+200"
        });
        $("#txtTDate").datepick({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yyyy 23:59',
            yearRange: "-200:+200"
        });
    }else{
        $("#txtFDate").datepick({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'mm/dd/yyyy 00:00',
            yearRange: "-200:+200"
        });
        $("#txtTDate").datepick({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'mm/dd/yyyy 23:59',
            yearRange: "-200:+200"
        });
    }
    buildDeviceListGrid([]);
    getAjaxObject(ipAddress+"/homecare/vacations/?fields=*&parentTypeId="+Number(parentRef.type)+"&parentId="+Number(parentRef.patientId)+"&is-active=1&is-deleted=0","GET",handleGetVacationList,onError);
}

function handleGetVacationList(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var vacation = [];
            if(dataObj.response.vacations){
                if($.isArray(dataObj.response.vacations)){
                    vacation = dataObj.response.vacations;
                }else{
                    vacation.push(dataObj.response.vacations);
                }
            }
            for(var j=0;j<vacation.length;j++){
                vacation[j].FD = kendo.toString(new Date(vacation[j].fromDate),"dd/MM/yyyy h:mm tt");
                vacation[j].TD = kendo.toString(new Date(vacation[j].toDate),"dd/MM/yyyy h:mm tt");
                vacation[j].idk = vacation[j].id;
            }
            buildDeviceListGrid(vacation);
        }
    }
}

function onError(errorObj){
    console.log(errorObj);
}

function buttonEvents(){

    $("#btnSave").off("click",onClickSave);
    $("#btnSave").on("click",onClickSave);

    $("#btnEdit").off("click",onClickEdit);
    $("#btnEdit").on("click",onClickEdit);

    $("#btnDelete").off("click",onClickDelete);
    $("#btnDelete").on("click",onClickDelete);

    $("#btnAdd").off("click");
    $("#btnAdd").on("click",onClickAdd);

    $("#btnCancel").off("click",onClickCancel);
    $("#btnCancel").on("click",onClickCancel);

    $("#btnReset").off("click",onClickReset);
    $("#btnReset").on("click",onClickReset);

    $(".popupClose").off("click", onClickCancel);
    $(".popupClose").on("click", onClickCancel);


    $(".btnActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('active');
        onClickActive();
    });
    $(".btnInActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('inactive');
        onClickInActive();
    });

}
var allRosterRowCount = 0;
var allRosterRowIndex = 0;
var allRosterRows = [];

function onClickSaveDO(){
    allRosterRowCount = 0;
    allRosterRowIndex = 0;
    allRosterRows = angularUIgridWrapper.getAllRows();
    allRosterRowCount = allRosterRows.length;
    if(allRosterRowCount>0){
        createDisplayOrder();
    }
}
function createDisplayOrder(){
    if(allRosterRowCount>allRosterRowIndex){
        var objRosterEntiry = allRosterRows[allRosterRowIndex];
        var rosterObj = objRosterEntiry.entity;
        var dataObj = {};
        dataObj.createdBy = Number(sessionStorage.userId);;
        dataObj.isDeleted = 0;
        dataObj.isActive = 1;
        dataObj.type = rosterObj.type;
        //  if(rosterObj.activityGroup){
        dataObj.activityGroup = rosterObj.activityGroup;
        // }
        dataObj.displayOrder = rosterObj.DIO;
        var dataUrl = ipAddress +"/homecare/activity-types/";
        method = "PUT";
        dataObj.id = rosterObj.idk;
        createAjaxObject(dataUrl, dataObj, method, onCreateDS, onError);
    }
}

function searchOnLoad(status) {
    buildDeviceListGrid([]);
    if(status == "active") {
        var urlExtn = ipAddress+"/homecare/vacations/?fields=*&parentTypeId="+Number(parentRef.type)+"&parentId="+Number(parentRef.patientId)+"&is-active=1&is-deleted=0";

    }
    else if(status == "inactive") {
        var urlExtn =  ipAddress+"/homecare/vacations/?fields=*&parentTypeId="+Number(parentRef.type)+"&parentId="+Number(parentRef.patientId)+"&is-active=0&is-deleted=1"
    }
    getAjaxObject(urlExtn,"GET",handleGetVacationList,onError);
}


function onClickAdd(){
    $("#addPopup").show();
    $('#viewDivBlock').hide();
    $("#txtID").hide();
    $(".filter-heading").html("Add Staff Leave Request");
    parentRef.operation = "add";
    onClickReset();
}

function addReportMaster(opr){
    var popW = 500;
    var popH = "43%";

    //$('.listDataWrapper').hide();
    //$('.addOrRemoveWrapper').show();
    $('.alert').remove();
    var profileLbl = "Add Task Type";
    parentRef.operation = opr;
    operation = opr;
    if(opr == "add"){
        $('#btnSave').html('<span><img src="../../img/medication/Save.png" class="providerLink-btn-img" />Save</span>');
        $('.tabContentTitle').text('Add Task Type');
        $('#btnReset').show();
        //var billActName = $("#txtBAN").val();
        $('#btnReset').trigger('click');
        //$("#txtBAN").val(billActName);
    }else{

        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if (selectedItems && selectedItems.length > 0) {
            var obj = selectedItems[0];
            parentRef.id = obj.idk;
            parentRef.displayOrder  = obj.displayOrder;
            parentRef.type = obj.type;
            parentRef.DIO = obj.DIO;
            parentRef.activityGroup = obj.activityGroup;
            parentRef.isActive = obj.isActive;
            operation = UPDATE;
        }

        profileLbl = "Edit Task Type";
        $('.tabContentTitle').text('Edit Task Type');
        $('#btnReset').hide();
        $('#btnSave').html('<span><img src="../../img/medication/Save.png" class="providerLink-btn-img" />Update</span>');
        $('#btnReset').trigger('click');
    }
    var devModelWindowWrapper = new kendoWindowWrapper();
    devModelWindowWrapper.openPageWindow("../../html/masters/createactivityTypeList.html", profileLbl, popW, popH, true, closeaddReportMastertAction);
}
function closeaddReportMastertAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        var opr = returnData.operation;
        if(opr == "add"){
            customAlert.info("info", "Task Type created successfully");
        }else{
            customAlert.info("info", "Task Type updated successfully");
        }
        init();
    }
}
function onClickActive() {
    $(".btnInActive").removeClass("radioButton-active");
    $(".btnActive").addClass("radioButton-active");
}

function onClickInActive() {
    $(".btnActive").removeClass("radioButton-active");
    $(".btnInActive").addClass("radioButton-active");
}

function onClickDelete(){
    customAlert.confirm("Confirm", "Are you sure to delete?",function(response){
        if(response.button == "Yes"){
            var dataObj = {};
            dataObj.createdBy = Number(sessionStorage.userId);;
            dataObj.isDeleted = 1;
            dataObj.isActive = 0;
            var dataUrl = ipAddress +"/homecare/vacations/";
            var method = "DELETE";
            var selectedItems = angularUIgridWrapper.getSelectedRows();
            dataObj.id = selectedItems[0].idk;
            createAjaxObject(dataUrl, dataObj, method, onCreateDelete, onError);
        }
    });
}

function onCreateDelete(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "Vacation deleted successfully";
            customAlert.error("Info", msg);
            operation = ADD;
            init();
        }
    }
}

function onClickSave(){
    var reqData = {};
    var subject = parentRef.patientId + ' '+ parentRef.lastName + ' ' + parentRef.firstName + ': ' + $("#txtFDate").val() + '  - ' +  $("#txtTDate").val();
    reqData.reason = $("#txtReason").val();
    reqData.subject =  subject;
    reqData.toDate = GetDateTime("txtTDate");
    reqData.fromDate =GetDateTime("txtFDate");
    reqData.fromEmail = $("#txtFMail").val();
    reqData.toEmail = $("#txtTMail").val();
    reqData.leaveTypeId = $("#cmbLType").val();
    reqData.leaveStatusId = 1;
    reqData.parentTypeId = parentRef.type;
    reqData.parentId = parentRef.patientId;
    reqData.managerIds = $("#cmbManager").val();
    reqData.isActive = 1;
    reqData.isDeleted = 0;


    var method = "POST";
    dataUrl = ipAddress +"/homecare/vacations/";
    createAjaxObject(dataUrl, reqData, method, onCreate, onError);
}
if(operation == UPDATE){
    var selectedItems = angularUIgridWrapper.getSelectedRows();
    reqData.id = selectedItems[0].idk;
    reqData.createdBy = selectedItems[0].createdBy;
    reqData.modifiedBy = Number(sessionStorage.userId);
    method = "PUT";
}
else{
    reqData.createdBy = Number(sessionStorage.userId);
}

function onCreate(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "Leave created successfully";
            if(operation == UPDATE){
                msg = "Leave updated successfully"
            }else{

            }
            displaySessionErrorPopUp("Info", msg, function(res) {
                onClickReset();
                init();
            })
        }else{
            customAlert.error("Error", dataObj.response.status.message);
        }
    }else{

    }
    onClickReset();
}


function onClickCancel(){
    $(".filter-heading").html("View Staff Leave Request");
    $("#viewDivBlock").show();
    $("#addPopup").hide();
}

function adjustHeight(){
    var defHeight = 230;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    angularUIgridWrapper.adjustGridHeight(cmpHeight);
}


function buildDeviceListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "From Date Time",
        "field": "FD",
    });
    gridColumns.push({
        "title": "To Date Time",
        "field": "TD",
    });
    gridColumns.push({
        "title": "Subject",
        "field": "subject",
    });
    gridColumns.push({
        "title": "Reason",
        "field": "reason",
    });
    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}

function onChange(){
    setTimeout(function() {
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        if (selectedItems && selectedItems.length > 0) {
            if(selectedItems[0].approvedBy != '') {
                $("#btnEdit").prop("disabled", false);
                $("#btnDelete").prop("disabled", false);
            }
            else {
                $("#btnEdit").prop("disabled", true);
                $("#btnDelete").prop("disabled", true);
            }
        }
    });
}

function onClickEdit(){
    $("#txtID").show();
    $(".filter-heading").html("Edit Staff Leave Request");
    parentRef.operation = "edit";
    operation =  "edit";
    $("#viewDivBlock").hide();
    $("#addPopup").show();
    var selectedItems = angularUIgridWrapper.getSelectedRows();
    if (selectedItems && selectedItems.length > 0) {
        var obj = selectedItems[0];
        $("#txtLeaveID").html("ID : " + obj.idk);
        $("#cmbManager").val(obj.managerIds);
        $("#cmbLeaveType").val(obj.leaveTypeId);
        $("#txtReason").val(obj.reason);

        // $("#txtFDate").datepick();
        // $("#txtFDate").datepick("setDate", GetDateTimeEdit(obj.fromDate));

        $("#txtFDate").val(GetDateTimeEdit(obj.fromDate));
        $("#txtTDate").val(GetDateTimeEdit(obj.toDate));

        // $("#txtTDate").datepick();
        // $("#txtTDate").datepick("setDate", GetDateTimeEdit(obj.toDate));
    }
}


var operation = "add";
var selFileResourceDataItem = null;

function onStatusChange(){
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if(cmbStatus && cmbStatus.selectedIndex<0){
        cmbStatus.select(0);
    }
}

function onClickReset() {
    $("#txtReason").val("");
    $("#txtFDate").val("");
    $("#txtTDate").val("");
    operation = ADD;
}

function themeAPIChange(){
    getAjaxObject(ipAddress+"/homecare/settings/?id=2","GET",getThemeValue,onError);
}

function getThemeValue(dataObj){
    console.log(dataObj);
    var tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.activityTypes){
            if($.isArray(dataObj.response.settings)){
                tempCompType = dataObj.response.settings;
            }else{
                tempCompType.push(dataObj.response.settings);
            }
        }
        if(tempCompType.length > 0){
            themesChange(tempCompType[0].value);
        }
    }
}

function themesChange(themeValue){
    if(themeValue == 2){
        loadAPi("../../Theme2/Theme02.css");
    }
    else if(themeValue == 3){
        loadAPi("../../Theme3/Theme03.css");
    }
}



function getManagers(){
    getAjaxObject(ipAddress+"/homecare/tenant-users/?is-active=1&is-deleted=0&id="+Number(parentRef.userId),"GET",getUserList,onError);
}

var userParentId;
function getUserList(dataObj){
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            if($.isArray(dataObj.response.tenantUsers)){
                dataArray = dataObj.response.tenantUsers;
            }else{
                dataArray.push(dataObj.response.tenantUsers);
            }
        }
    }

    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
        }
    }

    userParentId = Number(dataArray[0].parentId)
    if(dataArray.length > 0) {
        getAjaxObject(ipAddress + "/homecare/tenant-users/?is-active=1&is-deleted=0&id=" + Number(dataArray[0].parentId), "GET", getManagersList, onError);
    }

}

function getManagersList(dataObj){
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            if($.isArray(dataObj.response.tenantUsers)){
                dataArray = dataObj.response.tenantUsers;
            }else{
                dataArray.push(dataObj.response.tenantUsers);
            }
        }
    }

    if(dataArray[0] != undefined && dataArray.length > 0) {
        for (var i = 0; i < dataArray.length; i++) {
            dataArray[i].idk = dataArray[i].id;
            dataArray[i].Status = "InActive";
            if (dataArray[i].isActive == 1) {
                dataArray[i].Status = "Active";
            }
            $("#cmbManager").append('<option value="' + dataArray[i].idk + '">' + dataArray[i].lastName + ' ' + dataArray[i].firstName + '</option>');
        }
    }
    else{
        $("#btnAdd").hide();
        customAlert.info("Info","Manager is not assigned please contact administrator");
    }
}

function getLeaveTypes(){
    getAjaxObject(ipAddress+"/homecare/leave-types/?fields=id,value","GET",handleGetLeaveTypes,onError);
}

function handleGetLeaveTypes(dataObj){
    console.log(dataObj);
    var leaveArray = [];
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            if($.isArray(dataObj.response.leaveTypes)){
                leaveArray = dataObj.response.leaveTypes;
            }else{
                leaveArray.push(dataObj.response.leaveTypes);
            }
            for(var i=0;i<leaveArray.length;i++){
                leaveArray[i].idk = leaveArray[i].id;
                $("#cmbLType").append('<option value="'+leaveArray[i].idk+'">'+leaveArray[i].value+'</option>');
            }
        }
    }
}

function getEmails(){
    getAjaxObject(ipAddress+"/homecare/tenant-user-emails/?user-id="+userParentId,"GET",handleGetEmails,onError);
}

function handleGetEmails(dataObj){
    var userEmailArray = [];
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            if($.isArray(dataObj.response.userEmails)){
                userEmailArray = dataObj.response.userEmails;
            }else{
                userEmailArray.push(dataObj.response.userEmails);
            }
            for(var i=0;i<userEmailArray.length;i++){
                if(userEmailArray[i].id == Number(parentRef.patientId)){
                    $("#txtFMail").val(userEmailArray[i].email);
                }
                else{
                    $("#txtTMail").val(userEmailArray[i].email);
                }
            }
        }
    }
}


function GetDateTime(Id){
    var strDate = "";
    var dt = document.getElementById(''+ Id +'').value;
    var dob = null;
    var mm = 0;
    var dd = 0;
    var yy = 0;
    var cntry = sessionStorage.countryName;
    if (cntry.indexOf("India") >= 0) {
        var dtArray = dt.split("/");
        dob = (dtArray[1] + "/" + dtArray[0] + "/" + dtArray[2]);
    } else if (cntry.indexOf("United Kingdom") >= 0) {
        var dtArray = dt.split("/");
        dob = (dtArray[1] + "/" + dtArray[0] + "/" + dtArray[2]);
    } else {
        var dtArray = dt.split("/");
        dob = (dtArray[0] + "/" + dtArray[1] + "/" + dtArray[2]);
    }
    // if (dob) {
    //     var DOB = new Date(dob);
    //     strDate = kendo.toString(DOB, "dd/mm/yyyy");
    // }
    var returnValue = new Date(dob).getTime();

    return returnValue;

}
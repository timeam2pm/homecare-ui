var angularPTUIgridWrapper = null;
var angularPTUIgridWrapper1 = null;
var parentRef = null;
var operation = "";
var selItem = null;
var ADD = "add";
var UPDATE = "edit";
var VIEW = "view";
var DELETE = "delete";
var patientId = "";
var statusArr = [{ Key: 'Active', Value: 'Active' }, { Key: 'InActive', Value: 'InActive' }];
var appTypeArr = [{Key:'Notes1',Value:'Notes1'},{Key:'Notes2',Value:'Notes2'},{Key:'Notes3',Value:'Notes3'}];

var typeArr = [{ Key: '100', Value: 'Doctor' }, { Key: '200', Value: 'Nurse' }, { Key: '201', Value: 'Caregiver' }];
/*var filterArr = [{ Key: '', Value: 'All' }, { Key: 'id', Value: 'Provider ID' }, { Key: 'abbr', Value: 'Abbreviation' }, { Key: 'first-name', Value: 'First Name' }, { Key: 'middle-name', Value: 'Middle Name' }, { Key: 'last-name', Value: 'Last Name' }, { Key: 'type', Value: 'Type' }, { Key: 'user-id', Value: 'User Id' }, { Key: 'facility-id', Value: 'Facility Id' }];*/
var empArr;

var towArr = [{Key:'0',Value:'Sun day'},{Key:'1',Value:'Mon day'},{Key:'2',Value:'Tues day'},{Key:'3',Value:'Wedness day'},{Key:'4',Value:'Thurs day'},{Key:'5',Value:'Fri day'},{Key:'6',Value:'Satur day'}];

var prsViewArray = [];
var prsViewArray = [{idk:'1',h:'0',m:'0',tm:'12:00 AM'},{idk:'2',h:'0',m:'15',tm:''},{idk:'3',h:'0',m:'30',tm:''},{idk:'4',h:'0',m:'45',tm:''},{idk:'5',h:'1',m:'0',tm:'01:00 AM'}];
prsViewArray.push({idk:'6',h:'1',m:'15',tm:''});
prsViewArray.push({idk:'7',h:'1',m:'30',tm:''});
prsViewArray.push({idk:'8',h:'1',m:'45',tm:''});
prsViewArray.push({idk:'9',h:'2',m:'0',tm:'02:00 AM'});
prsViewArray.push({idk:'10',h:'2',m:'15',tm:''});
prsViewArray.push({idk:'11',h:'2',m:'30',tm:''});
prsViewArray.push({idk:'11',h:'2',m:'45',tm:''});
prsViewArray.push({idk:'12',h:'3',m:'0',tm:'03:00 AM'})
prsViewArray.push({idk:'13',h:'3',m:'15',tm:''});
prsViewArray.push({idk:'14',h:'3',m:'30',tm:''});
prsViewArray.push({idk:'15',h:'3',m:'45',tm:''})
prsViewArray.push({idk:'16',h:'4',m:'0',tm:'04:00 AM'});
prsViewArray.push({idk:'17',h:'4',m:'15',tm:''});
prsViewArray.push({idk:'18',h:'4',m:'30',tm:''});
prsViewArray.push({idk:'19',h:'4',m:'45',tm:''});
prsViewArray.push({idk:'20',h:'5',m:'0',tm:'05:00 AM'});
prsViewArray.push({idk:'21',h:'5',m:'15',tm:''});
prsViewArray.push({idk:'22',h:'5',m:'30',tm:''});
prsViewArray.push({idk:'23',h:'5',m:'45',tm:''});
prsViewArray.push({idk:'24',h:'6',m:'0',tm:'06:00 AM'});
prsViewArray.push({idk:'25',h:'6',m:'15',tm:''});
prsViewArray.push({idk:'26',h:'6',m:'30',tm:''});
prsViewArray.push({idk:'27',h:'6',m:'45',tm:''});
prsViewArray.push({idk:'28',h:'7',m:'0',tm:'07:00 AM'});
prsViewArray.push({idk:'29',h:'7',m:'15',tm:''});
prsViewArray.push({idk:'30',h:'7',m:'30',tm:''});
prsViewArray.push({idk:'31',h:'7',m:'45',tm:''});
prsViewArray.push({idk:'32',h:'8',m:'0',tm:'08:00 AM'})
prsViewArray.push({idk:'33',h:'8',m:'15',tm:''});
prsViewArray.push({idk:'34',h:'8',m:'30',tm:''});
prsViewArray.push({idk:'35',h:'8',m:'45',tm:''});
prsViewArray.push({idk:'36',h:'9',m:'0',tm:'09:00 AM'});
prsViewArray.push({idk:'37',h:'9',m:'15',tm:''});
prsViewArray.push({idk:'38',h:'9',m:'30',tm:''});
prsViewArray.push({idk:'39',h:'9',m:'45',tm:''});
prsViewArray.push({idk:'40',h:'10',m:'0',tm:'10:00 AM'});
prsViewArray.push({idk:'41',h:'10',m:'15',tm:''});
prsViewArray.push({idk:'42',h:'10',m:'30',tm:''});
prsViewArray.push({idk:'43',h:'10',m:'45',tm:''});
prsViewArray.push({idk:'44',h:'11',m:'0',tm:'11:00 AM'});
prsViewArray.push({idk:'45',h:'11',m:'15',tm:''});
prsViewArray.push({idk:'46',h:'11',m:'30',tm:''});
prsViewArray.push({idk:'47',h:'11',m:'45',tm:''});
prsViewArray.push({idk:'48',h:'12',m:'0',tm:'12:00 PM'});
prsViewArray.push({idk:'49',h:'12',m:'15',tm:''})
prsViewArray.push({idk:'50',h:'12',m:'30',tm:''});
prsViewArray.push({idk:'51',h:'12',m:'45',tm:''});
prsViewArray.push({idk:'52',h:'13',m:'0',tm:'01:00 PM'});
prsViewArray.push({idk:'53',h:'13',m:'15',tm:''});
prsViewArray.push({idk:'54',h:'13',m:'30',tm:''});
prsViewArray.push({idk:'55',h:'13',m:'45',tm:''});
prsViewArray.push({idk:'56',h:'14',m:'0',tm:'02:00 PM'});
prsViewArray.push({idk:'57',h:'14',m:'15',tm:''});
prsViewArray.push({idk:'58',h:'14',m:'30',tm:''});
prsViewArray.push({idk:'59',h:'14',m:'45',tm:''});
prsViewArray.push({idk:'60',h:'15',m:'0',tm:'03:00 PM'});
prsViewArray.push({idk:'61',h:'15',m:'15',tm:''});
prsViewArray.push({idk:'62',h:'15',m:'30',tm:''});
prsViewArray.push({idk:'63',h:'15',m:'45',tm:''});
prsViewArray.push({idk:'64',h:'16',m:'0',tm:'04:00 PM'});
prsViewArray.push({idk:'65',h:'16',m:'15',tm:''});
prsViewArray.push({idk:'66',h:'16',m:'30',tm:''});
prsViewArray.push({idk:'67',h:'16',m:'45',tm:''});
prsViewArray.push({idk:'68',h:'17',m:'0',tm:'05:00 PM'});
prsViewArray.push({idk:'69',h:'17',m:'15',tm:''});
prsViewArray.push({idk:'70',h:'17',m:'30',tm:''});
prsViewArray.push({idk:'71',h:'17',m:'45',tm:''});
prsViewArray.push({idk:'72',h:'18',m:'0',tm:'06:00 PM'});
prsViewArray.push({idk:'73',h:'18',m:'15',tm:''});
prsViewArray.push({idk:'74',h:'18',m:'30',tm:''});
prsViewArray.push({idk:'75',h:'18',m:'45',tm:''});
prsViewArray.push({idk:'76',h:'19',m:'0',tm:'07:00 PM'});
prsViewArray.push({idk:'77',h:'19',m:'15',tm:''});
prsViewArray.push({idk:'78',h:'19',m:'30',tm:''});
prsViewArray.push({idk:'79',h:'19',m:'45',tm:''});
prsViewArray.push({idk:'80',h:'20',m:'0',tm:'08:00 PM'});
prsViewArray.push({idk:'81',h:'20',m:'15',tm:''});
prsViewArray.push({idk:'82',h:'20',m:'30',tm:''});
prsViewArray.push({idk:'83',h:'20',m:'45',tm:''});
prsViewArray.push({idk:'84',h:'21',m:'0',tm:'09:00 PM'});
prsViewArray.push({idk:'85',h:'21',m:'15',tm:''});
prsViewArray.push({idk:'86',h:'21',m:'30',tm:''});
prsViewArray.push({idk:'87',h:'21',m:'45',tm:''});
prsViewArray.push({idk:'88',h:'22',m:'0',tm:'10:00 PM'});
prsViewArray.push({idk:'89',h:'22',m:'15',tm:''});
prsViewArray.push({idk:'90',h:'22',m:'30',tm:''});
prsViewArray.push({idk:'91',h:'22',m:'45',tm:''});
prsViewArray.push({idk:'92',h:'23',m:'0',tm:'11:00 PM'});
prsViewArray.push({idk:'93',h:'23',m:'15',tm:''});
prsViewArray.push({idk:'94',h:'23',m:'30',tm:''});
prsViewArray.push({idk:'95',h:'23',m:'45',tm:''});

var patientInfoObject = null;
var commId = "";
var ABBREVIATION_Patient = "";
var IsPostalCodeManual = sessionStorage.IsPostalCodeManual;

$(document).ready(function() {
    if(parent.frames['iframe'] != undefined) {
        parentRef = parent.frames['iframe'].window;
        parentRef.screenType = "provider";
    }
    var pnlHeight = window.innerHeight;
    var imgHeight = pnlHeight - 100;
    $("#divTop").height(imgHeight);

});


$(window).load(function() {
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
    init();
});

function onNotesChange(){
    var cmbNotes = $("#cmbNotes").data("kendoComboBox");
    if(cmbNotes){
        if(cmbNotes.selectedIndex<0){
            cmbNotes.select(0);
        }
    }
}
function init() {

    var dataOptionsPT = {pagination: false,paginationPageSize: 500,changeCallBack: onPTChange};
    angularPTUIgridWrapper = new AngularUIGridWrapper("rPrGrid1", dataOptionsPT);
    angularPTUIgridWrapper.init();
    buildPatientRosterList([]);

    var dataOptionsPT1 = {pagination: false,paginationPageSize: 500,changeCallBack: onPTChange1};
    angularPTUIgridWrapper1 = new AngularUIGridWrapper("rPrGrid2", dataOptionsPT1);
    angularPTUIgridWrapper1.init();
    buildPatientRosterList1([]);

    adjustHeight();

    $("#cmbNotes").kendoComboBox();
    setDataForSelection(appTypeArr, "cmbNotes", onNotesChange, ["Value", "Key"], 0, "");

    $("#txtStartTime").kendoTimePicker({interval: 15});
    $("#txtEndTime").kendoTimePicker({interval: 15});

    if(sessionStorage.userTypeId == "700"){
        $("#divApprove").hide();
    }else{
        $("#divApprove").hide();
    }

    allowNumerics("txtID");
    allowNumerics("txtFC");
    allowAlphaNumeric("txtExtID1");
    allowAlphaNumeric("txtExtID2");
    allowAlphabets("txtAbbreviation");
    allowAlphabets("txtFN");
    allowAlphabets("txtMN");
    allowAlphabets("txtLN");
    allowAlphabets("txtBAN");
    allowAlphabets("txtFAN");
    allowAlphabets("txtNEIC");
    allowAlphabets("txtAS");
    allowAlphabets("txtNT");
    allowAlphabets("txtUPIN");
    allowAlphabets("txtDEA");
    allowAlphabets("txtNPI");
    allowAlphaNumeric("txtTI");
    allowAlphabets("txtPCP");
    allowAlphaNumeric("txtBD");
    allowAlphaNumeric("txtFC");
    // allowAlphaNumeric("txtAdd1");
    //allowAlphaNumeric("txtAdd2");
    allowPhoneNumber("txtHPhone");
    allowPhoneNumber("txtExtension");
    allowPhoneNumber("txtWPhone");
    allowPhoneNumber("txtExtension1");
    allowPhoneNumber("txtCell");
    validateEmail("txtEmail1");
    allowDecimals("txtHeight");
    allowDecimals("txtWeight");

    //txtNEIC
    if(parentRef) {
        operation = parentRef.operation;
        patientId = parentRef.patientId;
    }
    if(operation == "edit") {
        selItem = parentRef.selDocItem;
        // $("#divGPSReport").show();
        $("#liCarePlan").show();
        $("#liCareAvl").show();
        downloadProviderPhoto();
        onClickRPSearch();
        getPatientRosterAppointments();

        //getProviderAvailableTimings();
    }
    //selItem = parentRef.selItem;
    $("#txtType").kendoComboBox();
    $("#cmbStatus").kendoComboBox();
    //$("#cmbAbbreviation").kendoComboBox();
    $("#cmbZip1").kendoComboBox();
    $("#cmbSMS").kendoComboBox();
    $("#cmbType").kendoComboBox();
    $("#cmbPrefix").kendoComboBox();
    $("#cmbSuffix").kendoComboBox();
    $("#cmbGender").kendoComboBox();
    $("#cmbLang").igCombo();
    $("#cmbRace").kendoComboBox();
    $("#cmbEthnicity").kendoComboBox();
    $("#cmbReligion").kendoComboBox();
    $("#cmbEmploymentType").kendoComboBox();
    /*$('#txtHR').kendoMaskedTextBox({
        mask: "00.00"
    });*/
    allowDecimals("txtHR");
    allowDecimals("txtOverTimeHourRate");
    allowNumerics("txtLeavesPerYear");
    allowNumerics("txtWeeklyContractHour");
    allowDecimals("txtHRperMile");

    setDataForSelection(typeArr, "txtType", onTypeChange, ["Value", "Key"], 0, "");
    /*setDataForSelection(filterArr, "cmbFilterByName", onFilterChange, ["Value", "Key"], 0, "");*/
    getZip();
    getAjaxObject(ipAddress+"/homecare/employment-types/?is-active=1&is-deleted=0","GET",getEmployeeList,onError);
    getAjaxObject(ipAddress+"/provider/list?is-active=1","GET",getAccountList,onError);
    displayLocalNames();
    buttonEvents();

    var cntry = sessionStorage.countryName;
    if(cntry.indexOf("India")>=0){
        $("#dtDOB1").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            yearRange: "-200:+200",
            maxDate: '0',
            onSelect: function(e,e1) {
                // var dob = document.getElementById("dtDOB").value;
                var dob = ((e1.selectedMonth+1)+"-"+e1.selectedDay+"-"+e1.selectedYear);
                var DOB = new Date(dob);
                var today = new Date();
                var age = today.getTime() - DOB.getTime();
                age = Math.floor(age / (1000 * 60 * 60 * 24 * 365.25));
                document.getElementById('txtAge').value = age;
            }
        });
    }else if(cntry.indexOf("United Kingdom")>=0){
        $("#dtDOB1").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            yearRange: "-200:+200",
            maxDate: '0',
            onSelect: function(e,e1) {
                var dob = ((e1.selectedMonth+1)+"-"+e1.selectedDay+"-"+e1.selectedYear);
                //var dob = document.getElementById("dtDOB").value;
                var DOB = new Date(dob);
                var today = new Date();
                var age = today.getTime() - DOB.getTime();
                age = Math.floor(age / (1000 * 60 * 60 * 24 * 365.25));
                document.getElementById('txtAge').value = age;
            }
        });
        if(IsPostalCodeManual == "1"){
            $("#btnCitySearch").css("display","");
            $("#lblCity").html("City" +"<span class='mandatoryClass'>*</span>");
            $("#cmbZip").attr("readonly", false);
            $("#btnZipSearch").css("display","none");
            $(".postalCode").html("Postal Code : ");
        }
        else{
            $('.postalCode').html('Postal Code : <span class="mandatoryClass">*</span>');
        }
    }else{
        $("#dtDOB1").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'mm/dd/yy',
            yearRange: "-200:+200",
            maxDate: '0',
            onSelect: function(e,e1) {
                var dob = ((e1.selectedMonth+1)+"-"+e1.selectedDay+"-"+e1.selectedYear);
                // var dob = document.getElementById("dtDOB").value;
                var DOB = new Date(dob);
                var today = new Date();
                var age = today.getTime() - DOB.getTime();
                age = Math.floor(age / (1000 * 60 * 60 * 24 * 365.25));
                document.getElementById('txtAge').value = age;
            }
        });
    }
}
function downloadProviderPhoto(){

}

function getEmployeeList(dataObj){
    console.log(dataObj);
    var dataArray = [];
    empArr = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.employmentTypes){
            if($.isArray(dataObj.response.employmentTypes)){
                dataArray = dataObj.response.employmentTypes;
            }else{
                dataArray.push(dataObj.response.employmentTypes);
            }
        }
    }


    for(var i=0;i<dataArray.length;i++){
        var obj = {};
        obj.Key = dataArray[i].value;
        obj.Value = dataArray[i].id;
        empArr.push(obj);
    }
    setDataForSelection(empArr, "cmbEmploymentType", onEmploymentTypeChange, ["Key", "Value"], 0, "");

}

function onEmploymentTypeChange(){
    var cmbEmploymentType = $("#cmbEmploymentType").data("kendoComboBox");
    if(cmbEmploymentType && cmbEmploymentType.selectedIndex<0){
        cmbEmploymentType.select(0);
    }
}
function displayLocalNames(){
    var cntry = sessionStorage.countryName;
    if(cntry.indexOf("India")>=0){
        $('.postalCode').html('Postal Code : <span class="mandatoryClass">*</span>');
        $('.stateLabel').text("State:");
        $('.zipFourWrapper').hide();
    }else if(cntry.indexOf("United Kingdom")>=0){
        $('.postalCode').html('Postal Code : <span class="mandatoryClass">*</span>');
        $('.stateLabel').text("County:");
        $('.zipFourWrapper').hide();
    }else if(cntry.indexOf("United State")>=0){
        $('.postalCode').html('Zip : <span class="mandatoryClass">*</span>');
        $('.stateLabel').text("State:");
        $('.zipFourWrapper').show();
    }else{
        $('.postalCode').html('Zip : <span class="mandatoryClass">*</span>');
        $('.stateLabel').text("State:");
        $('.zipFourWrapper').show();
    }
}
function getWeekDayName(wk){
    var wn = "";
    if(wk == 1){
        return "Monday";
    }else if(wk == 2){
        return "Tuesday";
    }else if(wk == 2){
        return "Tuesday";
    }else if(wk == 3){
        return "Wednesday";
    }else if(wk == 4){
        return "Thursday";
    }else if(wk == 5){
        return "Friday";
    }else if(wk == 6){
        return "Saturday";
    }else if(wk == 0){
        return "Sunday";
    }
    return wn;
}
function buildPatientRosterList(dataSource){
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    otoptions.rowHeight = 100;
    gridColumns.push({
        "title": "Contract",
        "field": "contract",
        "width": "15%",
    });
    gridColumns.push({
        "title": "DOW",
        "field": "dowK",
        "width": "10%",
        "cellTemplate": '<div class="ui-grid-cell-contents" >{{row.entity.dow}}</div>'
    });
    gridColumns.push({
        "title": "Start Time",
        "field": "fromTime",
        "width": "10%",
    });
    gridColumns.push({
        "title": "End Time",
        "field": "toTime",
        "width": "10%",
    });
    gridColumns.push({
        "title": "Duration",
        "field": "duration",
        "width": "10%",
    });
    gridColumns.push({
        "title": "Reason",
        "field": "reason",
        "width": "30%",
    });
    gridColumns.push({
        "title": "Client",
        "field": "patient",
        "width": "20%",
    });
    gridColumns.push({
        "title": "Facility",
        "field": "facility",
        "width": "15%",
    });
    gridColumns.push({
        "title": "Notes",
        "field": "notes",
        "width": "30%",
    });

    angularPTUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
}
function buildPatientRosterList1(dataSource){
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    otoptions.rowHeight = 100;
    gridColumns.push({
        "title": "Day of Week",
        "field": "dayName",
    });
    gridColumns.push({
        "title": "Start Time",
        "field": "ST",
    });
    gridColumns.push({
        "title": "End Time",
        "field": "ET",
    });
    angularPTUIgridWrapper1.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();

}
function onPTChange1(){
    setTimeout(function(){
        var selectedItems = angularPTUIgridWrapper1.getSelectedRows();
        console.log(selectedItems);
        if(selectedItems && selectedItems.length>0){
            $("#btnAEdit").prop("disabled", false);
            $("#btnADel").prop("disabled", false);
        }else{
            $("#btnAEdit").prop("disabled", true);
            $("#btnADel").prop("disabled", true);
        }
    },100)
}
function onPTChange(){
    setTimeout(function(){
        var selectedItems = angularPTUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if(selectedItems && selectedItems.length>0){
            $("#btnREdit").prop("disabled", false);
            $("#btnRDel").prop("disabled", false);
        }else{
            $("#btnREdit").prop("disabled", true);
            $("#btnRDel").prop("disabled", true);
        }
    },100)
}
function showOperations(){
    var node = '<div style="text-align:center"><span style="cursor:pointer;font-weight:bold;font-size:15px">Edit</span>&nbsp;&nbsp;<span style="cursor:pointer;font-weight:bold;font-size:15px">Delete</span>';
    node = node+"</div>";
    return node;
}
function showPatientRoster(){
    var txtFAN = $("#txtFAN").data("kendoComboBox");
    if(txtFAN){
        parentRef.fid = txtFAN.value();
        parentRef.fname = txtFAN.text();
    }
    parentRef.txtID = $("#txtID").val();

}

function onClickGPSReport(){
    parentRef.screenType = "providers";
    parentRef.providerid = selItem.idk;
    openReportPopup("../../html/patients/geoReport.html","GPS Report");
}
function openReportPopup(path,title){
    var popW = "90%";
    var popH = "85%";

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = title;
    devModelWindowWrapper.openPageWindow(path, profileLbl, popW, popH, false, closeCallReport);
}
function closeCallReport(evt,re){

}
function onClickAddRoster(){
    //angularPTUIgridWrapper.insert({},0);
    var popW = "75%";
    var popH = "60%";
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Create Roster";
    parentRef.RS = "create";
    parentRef.selRSItem = null;
    showPatientRoster();
    devModelWindowWrapper.openPageWindow("../../html/patients/createRoster.html", profileLbl, popW, popH, true, closePatientRoster);
}
function getDataGridArray(){
    var allRows = angularPTUIgridWrapper.getAllRows();
    var items = [];
    for(var a=0;a<allRows.length;a++){
        var row = allRows[a].entity;
        items.push(row);
    }
    return items;
}
function closePatientRoster(evt,returnData){
    console.log(returnData);
    if(returnData && returnData.status == "success"){
        var obj = returnData;
        var st = returnData.rs;
        if(st == "create"){
            var strDOW = obj.dow1;
            var strDOWK = obj.dowK1;
            var dArray = [];
            for(var s=0;s<strDOW.length;s++){
                //var item = strDOW[s];
                obj.dow = strDOW[s];
                obj.dowK = strDOWK[s];
                var rdItem =   JSON.parse(JSON.stringify(obj));
                dArray.push(rdItem);
            }

            var dItems = getDataGridArray();
            dArray = dArray.concat(dItems);
            buildPatientRosterList([]);
            setTimeout(function(){
                buildPatientRosterList(dArray);
                angularPTUIgridWrapper.refreshGrid();
            },100);
        }else{
            var selectedItems = angularPTUIgridWrapper.getSelectedRows();
            var dgItem = selectedItems[0];
            dgItem.contract = obj.contract;
            dgItem.contractK = obj.contractK;

            dgItem.facility = obj.facility;
            dgItem.facilityK = obj.facilityK;

            dgItem.provider = obj.provider;
            dgItem.providerK = obj.providerK;

            dgItem.fromTime = obj.fromTime;
            dgItem.fromTimeK = obj.fromTimeK;

            dgItem.toTime = obj.toTime;
            dgItem.toTimeK = obj.toTimeK;

            dgItem.duration = obj.duration;

            dgItem.AppType = obj.AppType;
            dgItem.AppTypeK = obj.AppTypeK;

            dgItem.reason = obj.reason;
            dgItem.reasonK = obj.reasonK;

            dgItem.notes = obj.notes;

            dgItem.patient = obj.patient;
            dgItem.patientK = obj.patientK;

            dgItem.dow = obj.dow;
            dgItem.dowK = obj.dowK;

            angularPTUIgridWrapper.refreshGrid();
        }
    }
}
function onClickEditRoster(){
    var selectedItems = angularPTUIgridWrapper.getSelectedRows();
    if(selectedItems && selectedItems.length>0){
        var popW = "75%";
        var popH = "60%";
        var profileLbl;
        var devModelWindowWrapper = new kendoWindowWrapper();
        profileLbl = "Edit Roster";
        parentRef.RS = "edit";
        parentRef.selRSItem = selectedItems[0];
        showPatientRoster()
        devModelWindowWrapper.openPageWindow("../../html/patients/createRoster.html", profileLbl, popW, popH, true, closePatientRoster);
    }
}
function onClickDeleteRoster(){
    var selectedItems = angularPTUIgridWrapper.getSelectedRows();
    if(selectedItems && selectedItems.length>0){
        customAlert.confirm("Confirm", "Are you sure you want delete?",function(response){
            if(response.button == "Yes"){
                var obj = selectedItems[0];
                if(obj.idk){
                    var rosterObj = {};
                    rosterObj.id = obj.idk;
                    rosterObj.modifiedBy = Number(sessionStorage.userId);;
                    rosterObj.isActive = 0;
                    rosterObj.isDeleted = 1;
                    var dataUrl = ipAddress+"/patient/roster/delete";
                    createAjaxObject(dataUrl,rosterObj,"POST",onPatientRosterDelete,onError);
                }else{
                    angularPTUIgridWrapper.deleteItem(selectedItems[0]);
                }
            }
        });
    }
}
function onPatientRosterDelete(dataObj){
    onClickRPSearch();
}
function onClickRPSearch(){
    if(selItem.idk != ""){
        var patientRosterURL = ipAddress+"/patient/roster/list/?provider-id="+selItem.idk+"&is-active=1";
        buildPatientRosterList([]);
        getAjaxObject(patientRosterURL,"GET",onGetRosterList,onError);
    }
}
function getAllServiceUsers(){
    var arr = [];
    for(var p=0;p<providerRosterArray.length;p++){
        var pItem = providerRosterArray[p];
        if(pItem){
            if(arr.length == 0){
                arr.push(pItem.patient);
            }
            if(arr.indexOf(pItem.patient) == -1){
                arr.push(pItem.patient);
            }
        }
    }
    return arr;
}
function getProviderHours(day,pr){
    var ds = providerRosterArray
    var strData = "";
    var arrProvider = [];
    var total = 0;
    for(var p=0;p<ds.length;p++){
        var pItem = ds[p];
        if(pItem  && pItem.patient == pr && pItem.weekDayStart == day){
            var ft = pItem.fromTime;
            var tt = pItem.toTime;
            var diff = tt-ft;
            total = total+diff;
        }
    }
    return total;
}
function getServiceUsersByProviders(){
    var suArray = getAllServiceUsers();
    var strTable = "<table>";
    strTable = strTable+"<tr>";
    strTable = strTable+"<th style='width:100px'>Staff</th>";
    for(var i=0;i<towArr.length;i++){
        strTable = strTable+"<th style='width:100px'>"+towArr[i].Value+"</th>";
    }
    strTable = strTable+"<th style='width:100px'>Total</th>";
    strTable = strTable+"</tr>";
    var providerList = suArray;
    if(providerList.length>0){
        for(var j=0;j<providerList.length;j++){
            strTable = strTable+"<tr>";
            strTable = strTable+"<td style='width:100px'>"+providerList[j]+"</td>";
            var total = 0;
            for(var k=0;k<towArr.length;k++){
                var hours = getProviderHours(towArr[k].Key,providerList[j]);
                total = total+Number(hours);
                strTable = strTable+"<td style='width:100px;font-weight:bold;font-size:15px'>"+convertMinsToHrsMins(hours)+"</td>";
            }
            strTable = strTable+"<td style='width:100px;font-weight:bold;font-size:15px'>"+convertMinsToHrsMins(total)+"</td>";
            strTable = strTable+"</tr>";
        }
    }

    strTable = strTable+"</table>";
    $("#divCareerTable").html(strTable);
}

function convertMinsToHrsMins(minutes) {
    var h = Math.floor(minutes / 60);
    var m = minutes % 60;
    h = h < 10 ? '0' + h : h;
    m = m < 10 ? '0' + m : m;
    return h +" h"+ ' : ' + m+" m";
}

var providerRosterArray = [];
function onGetRosterList(dataObj){
    console.log(dataObj);
    var rosterArray = [];
    if(dataObj && dataObj.response &&  dataObj.response.status && dataObj.response.status.code == 1){
        if($.isArray(dataObj.response.patientRoster)){
            rosterArray = dataObj.response.patientRoster;
        }else{
            rosterArray.push(dataObj.response.patientRoster);
        }
    }
    providerRosterArray = rosterArray;
    $("#divTable").html("");
    $("#divCareerTable").html("");
    getServiceUsersByProviders();
    console.log(providerRosterArray);

    var strTable = "<table>";
    strTable = strTable+"<tr>";
    strTable = strTable+"<th style='width:100px'>Day</th>";
    for(var i=0;i<prsViewArray.length;i++){
        if(i%4 == 0){
            strTable = strTable+"<th style='width:100px' colspan='4'>"+prsViewArray[i].tm+"</th>";
        }
    }
    strTable = strTable+"</tr>";
    for(var k=0;k<=6;k++){
        strTable = strTable+"<tr>";
        strTable = strTable+"<td style='width:100px;font-size:12px;font-weight:bold'>"+getWeekDayName(k)+"</td>";
        for(var j=0;j<prsViewArray.length;j++){
            var itemData = getItemData(prsViewArray[j],k);
            if(itemData){
                if(itemData.color == "green"){
                    sdt = "";
                    var tle = itemData.provider;
                    tle = tle.substring(0, tle.length-1);
                    tle = tle+"\n";
                    //tle = tle+st+"-"+et;
                    var countArr = tle.split(",");
                    var count = countArr.length;
                    if(count == 1){
                        count = "";
                    }
                    if(count>1){
                        strTable = strTable+"<td style='width:100px;background-color:blue;border:1px solid blue;font-weight:bold;font-size:12px;color:white' title='"+tle+"'>"+count+"</td>";
                    }else{
                        strTable = strTable+"<td style='width:100px;background-color:green;border:1px solid green;font-weight:bold;font-size:12px;color:white' title='"+tle+"'>"+count+"</td>";
                    }

                }else{
                    var tItem = prsViewArray[j];
                    var hours = Number(tItem.h);
                    hours = hours*3600;

                    var minutes = Number(tItem.m);
                    minutes = minutes*60;

                    var currMS = (hours+minutes);

                    if(sdt == ""){
                        sdt = currMS;
                        currClass = Math.floor(Math.random()*10000000);
                        currClass = "C"+currClass;
                        if(posClass == ""){
                            posClass = currClass;
                        }
                    }
                    classArray[currClass] = currMS;
                    //edt = getProviderEndDateTime(tItem.tm,currMS,prId,txtId,et);
                    strTable = strTable+"<td st="+sdt+" et="+et+" class='"+currClass+"' style='width:100px;background-color:white;cursor:pointer'></td>";
                }
            }else{
                sdt = "";
                strTable = strTable+"<td style='width:100px;'></td>";
            }
        }
        strTable = strTable+"</tr>";
    }
    strTable = strTable+"</table>";
    $("#divTable").html(strTable);

    /*for(var i=0;i<rosterArray.length;i++){
        rosterArray[i].idk = rosterArray[i].id;
        rosterArray[i].contractK = rosterArray[i].contractId;
        rosterArray[i].contract = rosterArray[i].contract;

        rosterArray[i].facility = rosterArray[i].facility;
        rosterArray[i].facilityK = rosterArray[i].facilityId;

        rosterArray[i].provider = rosterArray[i].provider;
        rosterArray[i].providerK = rosterArray[i].providerId;

        rosterArray[i].dow = getWeekDayName(rosterArray[i].weekDay);
        rosterArray[i].dowK = rosterArray[i].weekDay;

        var sdt = new Date();
        sdt.setHours(0, 0, 0, 0);
        sdt.setMinutes(Number(rosterArray[i].fromTime));
        //var sdms = rosterArray[i].fromTime;
        rosterArray[i].fromTimeK = rosterArray[i].fromTime;
        rosterArray[i].fromTime = kendo.toString(sdt,"t");


        var edt = new Date();
        edt.setHours(0, 0, 0, 0);
        edt.setMinutes(Number(rosterArray[i].toTime));

        rosterArray[i].toTimeK = rosterArray[i].toTime;
        rosterArray[i].toTime = kendo.toString(edt,"t");

        rosterArray[i].duration = rosterArray[i].duration;

        rosterArray[i].AppType = rosterArray[i].appointmentType;
        rosterArray[i].AppTypeK = rosterArray[i].appointmentTypeId;

        rosterArray[i].reason = rosterArray[i].appointmentReason;
        rosterArray[i].reasonK = rosterArray[i].appointmentReasonId;

        rosterArray[i].dow = rosterArray[i].dow;
        rosterArray[i].dowK = rosterArray[i].dowK;

        rosterArray[i].patient = rosterArray[i].patient;
        rosterArray[i].patientK = rosterArray[i].patientId;
    }
    buildPatientRosterList(rosterArray);*/
}
function getItemData(tItem,idVal){
    var prItemData = getProviderItemData(tItem,idVal);
    if(prItemData){
        //console.log(prItemData);
        prItemData.color = "green";
        return prItemData;
    }
    return null;
}
function getProviderItemData(tItem,idVal){
    var data = "";
    var ds = providerRosterArray;
    for(var p=0;p<ds.length;p++){
        var pItem = ds[p];
        if(pItem  &&  (pItem.weekDayStart == idVal || pItem.weekDayEnd == idVal)){
            var hours = Number(tItem.h);
            hours = hours*3600;

            var minutes = Number(tItem.m);
            minutes = minutes*60;

            var currMS = (hours+minutes);
            var stTime = Number(pItem.fromTime)*60;
            stTime = Number(stTime);

            var etTime = Number(pItem.toTime)*60;
            etTime = Number(etTime);

            if(pItem.weekDayEnd!= idVal){
                etTime = (23*3600)+(45*60);//-stTime;//(2*3600)+30*60);
            }

            if(pItem.weekDayStart != idVal){
                stTime = 0;//stTime-2000;//(2*3600)+30*60);
            }

            etTime = etTime-(15*60);

            if(currMS>=stTime && currMS<=etTime){ //&& tItem.h<=et.getHours()
                var sDate = new Date();//item.startDateTime);
                var eDate = new Date();
                var strData = {};
                sDate.setHours(0);
                sDate.setMinutes(0);
                sDate.setSeconds(0);
                sDate.setSeconds(pItem.fromTime*60);

                eDate.setHours(0,0,0,0);
                eDate.setSeconds(pItem.toTime*60);
                var st = new Date(sDate);
                var et = new Date(eDate);
                strData.st = kendo.toString(st,"hh:mm tt");//pItem.fromTimeK;
                strData.et = kendo.toString(et,"hh:mm tt");;//pItem.toTimeK;
                var provider = getProviderDetails(tItem,idVal);
                strData.provider = provider;
                return strData;
            }
        }
    }
    return null;
}
function getProviderDetails(tItem,idVal){
    var data = "";
    var ds = providerRosterArray;
    var strData = "";
    for(var p=0;p<ds.length;p++){
        var pItem = ds[p];
        if(pItem  && (pItem.weekDayStart == idVal || pItem.weekDayEnd == idVal)){
            var hours = Number(tItem.h);
            hours = hours*3600;

            var minutes = Number(tItem.m);
            minutes = minutes*60;

            var currMS = (hours+minutes);
            var stTime = Number(pItem.fromTime)*60;
            stTime = Number(stTime);

            var etTime = Number(pItem.toTime)*60;
            etTime = Number(etTime);

            if(currMS>=stTime && currMS<=etTime){ //&& tItem.h<=et.getHours()
                strData = strData+pItem.patient+",";
            }
        }
    }
    return strData;
}
var allRosterRows = [];
var allRosterRowCount = 0;
var allRosterRowIndex = 0;
function onClickCreateRoster(){
    allRosterRowCount = 0;
    allRosterRowIndex = 0;
    allRosterRows = angularPTUIgridWrapper.getAllRows();
    allRosterRowCount = allRosterRows.length;
    if(allRosterRowCount>0){
        saveRoster();
    }
}
function saveRoster(){
    if(allRosterRowCount>allRosterRowIndex){
        var objRosterEntiry = allRosterRows[allRosterRowIndex];
        var rosterObj = objRosterEntiry.entity;
        rosterObj.createdBy = Number(sessionStorage.userId);
        rosterObj.isActive = 1;
        rosterObj.isDeleted = 0;

        rosterObj.patientId = rosterObj.patientK;
        rosterObj.facilityId = rosterObj.facilityK;
        rosterObj.contractId = rosterObj.contractK;
        rosterObj.providerId = rosterObj.providerK;
        rosterObj.weekDay = rosterObj.dowK;
        rosterObj.fromTime = rosterObj.fromTimeK;
        rosterObj.toTime = rosterObj.toTimeK;
        rosterObj.careTypeId = 2;
        rosterObj.duration = rosterObj.duration;
        rosterObj.reason = rosterObj.reason;
        rosterObj.reasonId = rosterObj.reasonK;
        rosterObj.appointmentReason = rosterObj.reason;
        rosterObj.appointmentType = rosterObj.AppType;
        rosterObj.appointmentReasonId = rosterObj.reasonK;
        rosterObj.appointmentTypeId = rosterObj.AppTypeK;
        rosterObj.notes = rosterObj.notes;
        var dataUrl = "";
        if(rosterObj.idk){
            var rObj = {};
            rObj.id = rosterObj.idk;
            rObj.modifiedBy = Number(sessionStorage.userId);
            rObj.isActive = 1;
            rObj.isDeleted = 0;
            rObj.patientId = rosterObj.patientK;
            rObj.contractId = rosterObj.contractK;
            rObj.providerId = rosterObj.providerK;
            rObj.weekDay = rosterObj.dowK;
            rObj.fromTime = rosterObj.fromTimeK;
            rObj.toTime = rosterObj.toTimeK;
            rObj.careTypeId = rosterObj.careTypeId;
            rObj.duration = rosterObj.duration;
            rObj.reason = rosterObj.reason;
            rObj.reasonId = rosterObj.reasonK;
            rObj.notes = rosterObj.notes;
            rObj.facilityId = rosterObj.facilityK;
            rObj.appointmentReason = rosterObj.reason;
            rObj.appointmentType = rosterObj.AppType;
            rObj.appointmentReasonId = rosterObj.reasonK;
            rObj.appointmentTypeId = rosterObj.AppTypeK;

            dataUrl = ipAddress+"/patient/roster/update";
            createAjaxObject(dataUrl,rObj,"POST",onPatientRosterCreate,onError);
        }else{
            dataUrl = ipAddress+"/patient/roster/create";
            createAjaxObject(dataUrl,rosterObj,"POST",onPatientRosterCreate,onError);
        }
    }
}
function onPatientRosterCreate(dataObj){
    allRosterRowIndex = allRosterRowIndex+1;
    if(allRosterRowCount == allRosterRowIndex){
        customAlert.error("Info", "Roster created successfully");
        buildPatientRosterList([]);
        onClickRPSearch();
        //getPatientRosterList();
    }else{
        saveRoster();
    }
}
function onClickCreateRosterAppointment(){
    if(selItem.idk != ""){
        var popW = "400px";
        var popH = "500px";
        var profileLbl;
        var devModelWindowWrapper = new kendoWindowWrapper();
        profileLbl = "Create Roster Appointment";
        parentRef.pid = selItem.idk;
        var txtFAN = $("#txtFAN").data("kendoComboBox");
        if(txtFAN){
            parentRef.fid = txtFAN.value();
            parentRef.fname = txtFAN.text();
        }
        devModelWindowWrapper.openPageWindow("../../html/patients/createRosterAppointment.html", profileLbl, popW, popH, true, closeRosterAppointment);
    }
}
function closeRosterAppointment(evt,returnData){
    getPatientRosterAppointments();
}
function getPatientRosterAppointments(){
    var patientRosterURL = ipAddress+"/patient/roster/assignment/list/?provider-id="+selItem.idk;
    getAjaxObject(patientRosterURL,"GET",onGetRosterAppList,onError);
}
function onGetRosterAppList(dataObj){
    var ptArray = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.patientRosterAssignment){
            if($.isArray(dataObj.response.patientRosterAssignment)){
                ptArray = dataObj.response.patientRosterAssignment;
            }else{
                ptArray.push(dataObj.response.patientRosterAssignment);
            }
        }
    }

    if(ptArray.length>0){
        var pItem = ptArray[0];
        if(pItem){
            var dtFMT = null;
            var cntry = sessionStorage.countryName;
            if(cntry.indexOf("India")>=0){
                dtFMT = INDIA_DATE_FMT;
            }else  if(cntry.indexOf("United Kingdom")>=0){
                dtFMT = ENG_DATE_FMT;
            }else  if(cntry.indexOf("United State")>=0){
                dtFMT = US_DATE_FMT;
            }
            $("#txtRCR").val(pItem.createdByUser);
            var dt = kendo.toString(new Date(pItem.createdDate),dtFMT);
            $("#txtRCD").val(dt);
            var st = kendo.toString(new Date(pItem.fromDate),dtFMT);
            var ed = kendo.toString(new Date(pItem.toDate),dtFMT);
            $("#txtRCA").val(st);
            $("#txtRCE").val(ed);
        }
    }
}
function getProviderAvailableTimings(){
    onClickTMReset();
    buildPatientRosterList1([]);
    var txtType = $("#txtType").data("kendoComboBox");
    if (txtType) {
        var txtId = txtType.value();
        parentRef.cid = txtId;

        var patientRosterURL = ipAddress+"/homecare/availabilities/?parent-id="+selItem.idk+"&parent-type-id="+txtId+"&is-active=1&is-deleted=0";
        getAjaxObject(patientRosterURL,"GET",onPatientAvalableTimings,onError);
    }

}
function onPatientAvalableTimings(dataObj){
    console.log(dataObj);
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1" ){
        if(dataObj.response.availabilities){
            if($.isArray(dataObj.response.availabilities)){
                dataArray = dataObj.response.availabilities;
            }else{
                dataArray.push(dataObj.response.availabilities);
            }
        }

    }

    /*for(var i=0;i<arrAvl.length;i++){
        var item = arrAvl[i];
        if(item){
            var st = item.startTime;
            var sdt = new Date();
            sdt.setHours(0, 0, 0, 0);
            sdt.setSeconds(Number(st));
            item.ST = kendo.toString(sdt,"t");

            var et = item.endTime;
            var edt = new Date();
            edt.setHours(0, 0, 0, 0);
            edt.setSeconds(Number(et));
            item.ET = kendo.toString(edt,"t");
            item.idk = item.id;
        }
    }*/
    for(var i=0 ; i <dataArray.length ; i++){
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        dataArray[i].dayOfWeek == "1" ? dataArray[i].dayName = "Monday" : "";
        dataArray[i].dayOfWeek == "2" ? dataArray[i].dayName = "Tuesday" : "";
        dataArray[i].dayOfWeek == "3" ? dataArray[i].dayName = "Wednesday" : "";
        dataArray[i].dayOfWeek == "4" ? dataArray[i].dayName = "Thursday" : "";
        dataArray[i].dayOfWeek == "5" ? dataArray[i].dayName = "Friday" : "";
        dataArray[i].dayOfWeek == "6" ? dataArray[i].dayName = "Saturday" : "";
        dataArray[i].dayOfWeek == "0" ? dataArray[i].dayName = "Sunday" : "";
        dataArray[i].parentId = selItem.idk;//parentRef.patientId;
        dataArray[i].parentTypeId = parentRef.cid;

        var sTime = dataArray[i].startTime;
        var eTime = dataArray[i].endTime;
        var sec_num_start = parseInt(sTime, 10);

        var st = sTime;
        var sdt = new Date();
        sdt.setHours(0, 0, 0, 0);
        sdt.setSeconds(Number(st));
        dataArray[i].ST = kendo.toString(sdt,"t");

        var et = eTime;
        var edt = new Date();
        edt.setHours(0, 0, 0, 0);
        edt.setSeconds(Number(et));
        dataArray[i].ET = kendo.toString(edt,"t");
        dataArray[i].idk = dataArray[i].id;

        var hours_start   = Math.floor(sec_num_start / 3600);
        var minutes_start = Math.floor((sec_num_start - (hours_start * 3600)) / 60);
        var meridian_start = "AM";
        if(hours_start > 12) {
            hours_start = hours_start - 12;
            meridian_start = "PM";
        }
        dataArray[i].startTimeOfDay = hours_start + ':' + minutes_start + ' ' + meridian_start;
        var sec_num_end = parseInt(eTime, 10);
        var hours_end   = Math.floor(sec_num_end / 3600);
        var minutes_end = Math.floor((sec_num_end - (hours_end * 3600)) / 60);
        var meridian_end = "AM";
        if(hours_end > 12) {
            hours_end = hours_end - 12;
            meridian_end = "PM";
        }
        dataArray[i].endTimeOfDay = hours_end + ':' + minutes_end + ' ' + meridian_end;
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
            var createdDate = new Date(dataArray[i].createdDate);
            var createdDateString = createdDate.toLocaleDateString();
            var modifiedDate = new Date(dataArray[i].modifiedDate);
            var modifiedDateString = modifiedDate.toLocaleDateString();
            dataArray[i].createdDateString = createdDateString;
            dataArray[i].modifiedDateString = modifiedDateString;
        }
    }
    if(dataArray.length > 0) {
        dataArray.sort(function(a, b){
            return a.dayOfWeek - b.dayOfWeek;
        });
    }


    buildPatientRosterList1(dataArray);
}
function getAccountList(dataObj){
    console.log(dataObj);
    var dataArray = [];
    //var tempDataArry = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if($.isArray(dataObj.response.provider)){
            dataArray = dataObj.response.provider;
        }else{
            dataArray.push(dataObj.response.provider);
        }
    }else{
        //customAlert.error("Error",dataObj.response.status.message);
    }
    /*for(var j=0;j<tempDataArry.length;j++){
        var item = tempDataArry[j];
        dataArray.push(item.provider);
    }*/

    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
            var createdDate = new Date(dataArray[i].createdDate);
            var createdDateString = createdDate.toLocaleDateString();
            var modifiedDate = new Date(dataArray[i].modifiedDate);
            var modifiedDateString = modifiedDate.toLocaleDateString();
            dataArray[i].createdDateString = createdDateString;
            dataArray[i].modifiedDateString = modifiedDateString;
        }
    }
    //buildAccountListGrid(dataArray);
    if(operation == "add"){
        $('#btnSave').html('Save');
        $('.tabContentTitle').text('Add Staff');
        $('#btnReset').show();
        var billActName = $("#txtBAN").val();
        $("#txtBAN").val(billActName);
    }else{
        if(parentRef)
            parentRef.patientId = selItem.idk;
        profileLbl = "Edit Staff";
        $('.tabContentTitle').text('Edit Staff');
        $('#btnReset').hide();
        $('#btnSave').html('Save');
    }
}
function onStatusChange() {
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if (cmbStatus && cmbStatus.selectedIndex < 0) {
        cmbStatus.select(0);
    }
}

function buttonEvents() {
    $("#tabsUL li a[data-toggle='tab']").off("click");
    $("#tabsUL li a[data-toggle='tab']").on("click",onClickTabs);

    $("#btnRAdd").off("click",onClickAddRoster);
    $("#btnRAdd").on("click",onClickAddRoster);

    $("#btnREdit").off("click",onClickEditRoster);
    $("#btnREdit").on("click",onClickEditRoster);

    $("#btnRDel").off("click",onClickDeleteRoster);
    $("#btnRDel").on("click",onClickDeleteRoster);

    $("#btnRRoster").off("click",onClickCreateRoster);
    $("#btnRRoster").on("click",onClickCreateRoster);

    $("#btnRApp").off("click",onClickCreateRosterAppointment);
    $("#btnRApp").on("click",onClickCreateRosterAppointment);

    $("#btnCancel").off("click", onClickCancel);
    $("#btnCancel").on("click", onClickCancel);

    $("#btnCancel1").off("click", onClickCancel);
    $("#btnCancel1").on("click", onClickCancel);

    $("#btnSave").off("click", onClickSave);
    $("#btnSave").on("click", onClickSave);

    $("#btnSearch").off("click", onClickSearch);
    $("#btnSearch").on("click", onClickSearch);

    $("#btnReset").off("click", onClickReset);
    $("#btnReset").on("click", onClickReset);

    $("#btnZipSearch").off("click");
    $("#btnZipSearch").on("click", onClickZipSearch);

    $("#btnCitySearch").off("click");
    $("#btnCitySearch").on("click", onClickZipSearch);

    $("#btnGPSReport").off("click", onClickGPSReport);
    $("#btnGPSReport").on("click", onClickGPSReport);

    // $("#btnStartVideo").off("click", onClickStartVideo);
    //$("#btnStartVideo").on("click", onClickStartVideo);


    $("#btnBrowse").off("click", onClickBrowse);
    $("#btnBrowse").on("click", onClickBrowse);

    $("#fileElem").off("change", onSelectionFiles);
    $("#fileElem").on("change", onSelectionFiles);

    $("#btnUpload").off("click", onClickUploadPhoto);
    $("#btnUpload").on("click", onClickUploadPhoto);

    $("#btnBrowse1").off("click", onClickBrowse1);
    $("#btnBrowse1").on("click", onClickBrowse1);

    $("#fileElem1").off("change", onSelectionFiles1);
    $("#fileElem1").on("change", onSelectionFiles1);

    $("#fileElem").off("click", onSelectionFiles);
    $("#fileElem").on("click", onSelectionFiles);

    $("#btnUpload1").off("click", onClickUploadPhoto1);
    $("#btnUpload1").on("click", onClickUploadPhoto1);

    $("#btnASave").off("click", onClickTMSave);
    $("#btnASave").on("click", onClickTMSave);

    $("#btnAAdd").off("click", onClickAddAvailable);
    $("#btnAAdd").on("click", onClickAddAvailable);

    $("#btnAEdit").off("click", onClickTMEdit);
    $("#btnAEdit").on("click", onClickTMEdit);

    $("#btnADel").off("click", onClickTMDel);
    $("#btnADel").on("click", onClickTMDel);

    $("#btnAReset").off("click", onClickTMReset);
    $("#btnAReset").on("click", onClickTMReset);

    $("#btnView").off("click", onClickResumeView);
    $("#btnView").on("click", onClickResumeView);


    $("#imgPhoto").error(function() {
        onshowImage();
    });

    $('body').on('click','.qualAddRemoveLink.addQual', function() {
        var $trLen = $('.qualificationTabWrapper table tbody tr').length;
        $('.qualificationTabWrapper table tbody').append('<tr class="qualWrapper_li qualificationTabWrapper-li-'+$trLen+'"><td class="qual-td-addRemove"><span><a href="#" class="qualAddRemoveLink addQual">+</a> <a href="#" class="qualAddRemoveLink removeQual">-</a></span></td><td><input type="text" name="qualWrap-sno" value="'+$trLen+'" readonly /></td><td><input type="text" name="qualWrap-qualification" id="qualWrapQualification" maxlength="100" value="" /></td><td><input type="text" name="qualWrap-university" id="qualWrapUniversity" maxlength="100" value="" /></td><td><input type="text" name="qualWrap-percentage" id="qualWrapPercentage" value="" /></td><td><input type="text" name="qualWrap-passedout" id="qualWrapPassedout" value="" style="width: auto !important; padding-left: 0 !important;" /></td><td><input type="text" name="qualWrap-expirydate" value="" style="width: auto !important; padding-left: 0 !important;" /></td></tr>');
        allowOnlyDecimals($('[name="qualWrap-percentage"]'));
        $('.qualificationTabWrapper-li-'+$trLen).find('[name="qualWrap-passedout"]').kendoDatePicker({ format: "dd-MM-yyyy"});
        $('.qualificationTabWrapper-li-'+$trLen).find('[name="qualWrap-expirydate"]').kendoDatePicker({ format: "dd-MM-yyyy"});
        modifySerialNumber();
    });
    $('body').on('click','.qualAddRemoveLink.removeQual', function() {
        $(this).closest('tr').remove();
        /*$('.qualificationTabWrapper table tbody').append('<span><a href="#" class="qualAddRemoveLink addQual">+</a> <a href="#" class="qualAddRemoveLink removeQual">-</a><tr><td><input type="text" name="qualWrap-sno" value="'+$trLen+'" /></td><td><input type="text" name="qualWrap-qualification" value="" /></td><td><input type="text" name="qualWrap-university" value="" /></td><td><input type="text" name="qualWrap-percentage" value="" /></td><td><input type="text" name="qualWrap-passedout" value="" /></td><td><input type="text" name="qualWrap-expirydate" value="" /></td></tr></span>');*/
        modifySerialNumber();
    });

    $('#txtTE').on('change', function() {
        if($(this).is(':checked')) {
            $('#txtHRperMile').removeAttr('disabled');
            /*if($("#txtHRperMile").closest('.wraphourpermile').find('.k-maskedtextbox') && $("#txtHRperMile").closest('.wraphourpermile').find('.k-maskedtextbox').length == 0) {
                $('#txtHRperMile').kendoMaskedTextBox({
                    mask: "00.00"
                });
            }*/
        } else {
            $('#txtHRperMile').attr('disabled','disabled');
        }
    });

    $('.activities-link').off('click', onClickActivities);
    $('.activities-link').on('click', onClickActivities);

    $('.availability-link').off('click', onClickAvailability);
    $('.availability-link').on('click', onClickAvailability);

    $('.block-link').off('click', onClickBlock);
    $('.block-link').on('click', onClickBlock);

    $('.vacation-link').off('click', onClickVacation);
    $('.vacation-link').on('click', onClickVacation);

    $('.approve-link').off('click', onClickApprove);
    $('.approve-link').on('click', onClickApprove);

}

function onClickTMSave(){
    var st = $("#txtStartTime").data("kendoTimePicker");
    var et = $("#txtEndTime").data("kendoTimePicker");
    var cmbNotes = $("#cmbNotes").data("kendoComboBox");
    if(cmbNotes){
        note = cmbNotes.text();
    }
    var chkMon = $("#chkMon").is(":checked");
    var chkTue = $("#chkTue").is(":checked");
    var chkWed = $("#chkWed").is(":checked");
    var chkThurs = $("#chkThurs").is(":checked");
    var chkFri = $("#chkFri").is(":checked");
    var chkSat = $("#chkSat").is(":checked");
    var chkSun = $("#chkSun").is(":checked");
    var day = 0;
    if(chkMon){
        day = 1;
    }
    if(chkTue){
        day = 2;
    }
    if(chkWed){
        day = 3;
    }
    if(chkThurs){
        day = 4;
    }
    if(chkFri){
        day = 5;
    }
    if(chkSat){
        day = 6;
    }
    if(chkSun){
        day = 7;
    }

    if(st.value() && et.value()){
        var stValue = new Date(st.value());
        var etValue = new Date(et.value());
        var stv = stValue.getTime();
        var etv = etValue.getTime();
        var dt = new Date();
        dt.setHours(0);
        dt.setMinutes(0);
        dt.setSeconds(0);
        etv = etv-dt;
        stv = stv-dt;
        etv = etv/1000;
        stv = stv/1000;
        if(etv>stv){
            var dObj = {};
            dObj.parentTypeId = 500;
            dObj.dayOfWeek = day;
            dObj.isDeleted = 0;
            dObj.isActive = 1;
            dObj.parentId = selItem.idk;
            dObj.createdBy = Number(sessionStorage.userId);
            dObj.startTime = Number(stv.toFixed());
            dObj.endTime = Number(etv.toFixed());;
            var me1 = "POST";
            if(opr == "edit"){
                me1 = "PUT";
                var selectedItems = angularPTUIgridWrapper1.getSelectedRows();
                dObj.id = selectedItems[0].idk;
            }

            var dataUrl = ipAddress + "/homecare/availabilities/";
            createAjaxObject(dataUrl, dObj, me1, onPatientTMCreate, onError);

        }else{
            customAlert.error("Error", "End Time should be greater than  Start Time");
        }
    }else{
        customAlert.error("Error", "Enter Proper Time");
    }
}
function onPatientTMCreate(dataObj){
    console.log(dataObj);
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            if(opr == "add"){
                customAlert.error("Info", "Created");
            }else{
                customAlert.error("Info", "Updated");
            }
            $("#btnAEdit").prop("disabled", true);
            $("#btnADel").prop("disabled", true);
            getProviderAvailableTimings();
        } else {
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}
var opr = "add";
function onClickTMEdit(){
    var selectedItems = angularPTUIgridWrapper1.getSelectedRows();
    console.log(selectedItems);
    if(selectedItems.length>0){
        opr = "edit";
        var dtItem = selectedItems[0];
        if(dtItem){
            if(operation == UPDATE) {
                var popW = "75%";
                var popH = "40%";

                var profileLbl;
                var devModelWindowWrapper = new kendoWindowWrapper();
                profileLbl = "Staff Availability";
                parentRef.pid = selItem.idk;
                parentRef.ds = getDataGridArray();
                parentRef.TYPE = "AVL";
                var txtFAN = $("#txtFAN").data("kendoComboBox");
                if (txtFAN) {
                    var txtFId = txtFAN.value();
                    parentRef.fid = txtFId;
                }

                var txtType = $("#txtType").data("kendoComboBox");
                if (txtType) {
                    var txtId = txtType.value();
                    parentRef.cid = txtId;
                }

                parentRef.rs = "edit";
                parentRef.dtItem = dtItem;
                devModelWindowWrapper.openPageWindow("../../html/masters/createProviderAvailable.html", profileLbl, popW, popH, true, onCloseProviderAvailability);
                parent.setPopupWIndowHeaderColor("0bc56f","FFF");
            }
            /*var txtStartTime = $("#txtStartTime").data("kendoTimePicker");
           txtStartTime.value(dtItem.ST);

            var txtEndTime = $("#txtEndTime").data("kendoTimePicker");
            txtEndTime.value(dtItem.ET);*/
        }
    }
}
function getDataGridArray(){
    var allRows = angularPTUIgridWrapper1.getAllRows();
    var items = [];
    for(var a=0;a<allRows.length;a++){
        var row = allRows[a].entity;
        items.push(row);
    }
    return items;
}
function onClickAddAvailable(){
    console.log("available add");
    if(operation == UPDATE) {
        var popW = "75%";
        var popH = "40%";

        var profileLbl;
        var devModelWindowWrapper = new kendoWindowWrapper();
        profileLbl = "Staff Availability";
        parentRef.pid = selItem.idk;
        parentRef.ds = getDataGridArray();
        parentRef.TYPE = "AVL";
        var txtFAN = $("#txtFAN").data("kendoComboBox");
        if (txtFAN) {
            var txtFId = txtFAN.value();
            parentRef.fid = txtFId;
        }

        var txtType = $("#txtType").data("kendoComboBox");
        if (txtType) {
            var txtId = txtType.value();
            parentRef.cid = txtId;
        }

        parentRef.rs = "add";
        parentRef.dtItem = null;

        devModelWindowWrapper.openPageWindow("../../html/masters/createProviderAvailable.html", profileLbl, popW, popH, true, onCloseProviderAvailability);
        parent.setPopupWIndowHeaderColor("0bc56f","FFF");
    }
}
function onCloseProviderAvailability(evt){
    getProviderAvailableTimings();
}
function onClickTMDel(){
    customAlert.confirm("Confirm", "Are you sure you want delete?",function(response){
        if(response.button == "Yes"){
            var selectedItems = angularPTUIgridWrapper1.getSelectedRows();
            console.log(selectedItems);
            if(selectedItems && selectedItems.length>0){
                var selItem = selectedItems[0];
                if(selItem){
                    var dataUrl = ipAddress + "/homecare/availabilities/";
                    var reqObj = {};
                    reqObj.id = selItem.idk;
                    reqObj.isDeleted = "1";
                    reqObj.isActive = "0";
                    reqObj.modifiedBy = sessionStorage.userId;
                    createAjaxObject(dataUrl, reqObj, "PUT", onPatientTMCreate, onError);
                }
            }
        }
    });
}
function onPatientTMCreate(dataObj){
    if(dataObj && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            customAlert.info("Info","Deleted Successfully");
            getProviderAvailableTimings();
        }else{
            customAlert.error("error", dataObj.message);
        }
    }
}
function onClickTMReset(){
    opr = "add";
    $("#btnAEdit").prop("disabled", true);
    $("#btnADel").prop("disabled", true);
}
var isBrowseFlag = false;
function onClickBrowse(e) {
    removeSelectedButtons();
    // if(operation == "edit"){
    $("#btnBrowse").addClass("selClass");
    isBrowseFlag = true;
    $("#video").hide();
    $("#canvas").hide();
    $("#imgPhoto").show();
    console.log(e);
    if (e.currentTarget && e.currentTarget.id) {
        var btnId = e.currentTarget.id;
        var fileTagId = ("fileElem" + btnId);
        $("#fileElem").click();
    }
    // }
}

function onClickBrowse1(e) {
    //removeSelectedButtons();
    if(operation == "edit"){
        console.log(e);
        if (e.currentTarget && e.currentTarget.id) {
            var btnId = e.currentTarget.id;
            var fileTagId = ("fileElem" + btnId);
            $("#fileElem1").click();
        }
    }
}
function onClickResumeView(){
    if (operation == "edit") {
        //imagePhotoData
        var reqUrl = ipAddress + "/homecare/download/providers/resume/?id=" + selItem.idk + "&access_token=" + sessionStorage.access_token+"&tenant="+sessionStorage.tenant;

        var popW = "50%";
        var popH = "50%";

        var profileLbl;
        var devModelWindowWrapper = new kendoWindowWrapper();
        profileLbl = "View Resume";
        parentRef.rUrl = reqUrl;
        devModelWindowWrapper.openPageWindow("../../html/masters/showResume.html", profileLbl, popW, popH, true, onCloseResume);
    }
}
function onCloseResume(evt,returnData){

}
function onClickUploadPhoto1() {
    if (operation == "edit") {
        //imagePhotoData
        var reqUrl = ipAddress + "/homecare/upload/providers/resume/?id=" + selItem.idk + "&access_token=" + sessionStorage.access_token+"&tenant="+sessionStorage.tenant;

        var formData = new FormData();
        formData.append("desc", fileName1);
        formData.append("file", files1[0]);

        Loader.showLoader();
        $.ajax({
            url: reqUrl,
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false, // "multipart/form-data",
            success: function(data, textStatus, jqXHR) {
                if (typeof data.error === 'undefined') {
                    Loader.hideLoader();
                    submitForm1(data);
                } else {
                    Loader.hideLoader();
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                Loader.hideLoader();
            }
        });
    }
}
function removeSelectedButtons(){

}
function onClickUploadPhoto() {
    // if (operation == "edit") {
    removeSelectedButtons();
    $("#btnUpload").addClass("selClass");
    if (isBrowseFlag) {
        //imagePhotoData
        var reqUrl = ipAddress + "/homecare/upload/providers/photo/?id=" + selItem.idk + "&access_token=" + sessionStorage.access_token+"&tenant="+sessionStorage.tenant;
        // var reqUrl = ipAddress + "/upload/providers/photo/stream/?patient-id=" + patientId + "&photo-ext=png&access_token=" + sessionStorage.access_token+"&tenant="+sessionStorage.tenant
        /* var xmlhttp = new XMLHttpRequest();
         xmlhttp.open("POST", reqUrl, true);
         //xmlhttp.open("POST", "/ROOT/upload/patient/photo/?patient-id=101&photo-ext=png", true);
         xmlhttp.send(imagePhotoData);
         xmlhttp.onreadystatechange = function() {//Call a function when the state changes.
             if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                 customAlert.error("Info", "Image uploaded successfully");
             }
         }*/

        var formData = new FormData();
        formData.append("desc", fileName);
        formData.append("file", files[0]);

        Loader.showLoader();
        $.ajax({
            url: reqUrl,
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false, // "multipart/form-data",
            success: function(data, textStatus, jqXHR) {
                if (typeof data.error === 'undefined') {
                    Loader.hideLoader();
                    submitForm(data);
                } else {
                    Loader.hideLoader();
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                Loader.hideLoader();
            }
        });
    } else {
        var canvas = document.getElementById('canvas');
        var dataURL = canvas.toDataURL("image/png");

        var reqUrl = ipAddress + "/upload/patient/photo/stream/?patient-id=" + patientId + "&photo-ext=png&access_token=" + sessionStorage.access_token+"&tenant="+sessionStorage.tenant;
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open("POST", reqUrl, true);
        //xmlhttp.open("POST", "/ROOT/upload/patient/photo/?patient-id=101&photo-ext=png", true);
        xmlhttp.send(dataURL);
        xmlhttp.onreadystatechange = function() {//Call a function when the state changes.
            if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                customAlert.error("Info", "Image uploaded successfully");
            }
        }
    }
    // }
}
function submitForm(dataObj) {
    if(dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        customAlert.error("Info", "Image uploaded successfully");
    }else{
        customAlert.error("Error", dataObj.response.status.message);
    }

    //init();
}
function submitForm1(dataObj) {
    if(dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        customAlert.error("Info", "Resume uploaded successfully");
    }else{
        customAlert.error("Error", dataObj.response.status.message);
    }

    //init();
}
var fileName = "";
var files = null;
function onSelectionFiles(event) {
    //var imageCompressor = new ImageCompressor();
    console.log(event);
    files = event.target.files;
    fileName = "";
    //$('#txtFU').val("");
    if (files) {
        if (files.length > 0) {
            fileName = files[0].name;
            // if (operation == "edit") {
            var oFReader = new FileReader();
            oFReader.readAsDataURL(files[0]);
            oFReader.onload = function(oFREvent) {
                document.getElementById("imgPhoto").src = oFREvent.target.result;
                /*  if (imageCompressor) {
                      imageCompressor.run(oFREvent.target.result, compressorSettings, function(small){
                          document.getElementById("imgPhoto").src = small;
                          //isBrowseFlag = false;
                          imagePhotoData = small;
                          return small;
                      });*/
            }

            // }
        }
    }
}

var fileName1 = "";
var files1 = null;
function onSelectionFiles1(event) {
    //var imageCompressor = new ImageCompressor();
    console.log(event);
    files1 = event.target.files;
    fileName1 = "";
    //$('#txtFU').val("");
    if (files1) {
        if (files1.length > 0) {
            fileName1 = files1[0].name;
            var type = files1[0].type;
            if (type.toLowerCase() == "application/pdf") {
                type = "pdf";
            }
            if (type == "pdf") {
                if (operation == "edit") {
                    $("#txtCV").val(fileName1);
                    //onClickUploadPhoto1();
                    var oFReader = new FileReader();
                    // oFReader.readAsDataURL(files1[0]);
                    oFReader.onload = function (oFREvent) {
                        //document.getElementById("imgPhoto").src = oFREvent.target.result;
                        /*  if (imageCompressor) {
                              imageCompressor.run(oFREvent.target.result, compressorSettings, function(small){
                                  document.getElementById("imgPhoto").src = small;
                                  //isBrowseFlag = false;
                                  imagePhotoData = small;
                                  return small;
                              });*/
                    }

                }
            }
            else{
                customAlert.error("Error", "Accept only PDF files");
            }
        }
    }
}
var selectedTab = "General";
function onClickTabs(e){
    var tabHref = $(e.target).attr("id");
    console.log($(e.target).text());
    $("#btnSave").show();
    $("#btnReset").show();
    $("#btnCancel").show();
    if($(e.target).text() == "Roster" || $(e.target).text() == "Availability"){
        $("#btnSave").hide();
        $("#btnReset").hide();
        $("#btnCancel").hide();
    }
}
function modifySerialNumber() {
    $('.qualificationTabWrapper [name="qualWrap-sno"]').each(function(i) {
        $(this).val(i + 1);
    });
}
function adjustHeight(){
    var defHeight = 200;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    if(angularPTUIgridWrapper){
        angularPTUIgridWrapper.adjustGridHeight(cmpHeight);
    }
    if(angularPTUIgridWrapper1){
        angularPTUIgridWrapper1.adjustGridHeight((cmpHeight));
    }

}

function onClickActivities() {
    var popW = "72%";
    var popH = "70%";

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Staff Activities";
    devModelWindowWrapper.openPageWindow("../../html/masters/providerActivityList.html", profileLbl, popW, popH, true, onCloseActivities);
}
function onClickAvailability() {
    if(operation == UPDATE) {
        var popW = "75%";
        var popH = "70%";

        var profileLbl;
        var devModelWindowWrapper = new kendoWindowWrapper();
        profileLbl = "Staff Availability";
        parentRef.pid = selItem.idk;
        parentRef.TYPE = "AVL";
        var txtFAN = $("#txtFAN").data("kendoComboBox");
        if (txtFAN) {
            var txtFId = txtFAN.value();
            parentRef.fid = txtFId;
        }

        var txtType = $("#txtType").data("kendoComboBox");
        if (txtType) {
            var txtId = txtType.value();
            parentRef.cid = txtId;
        }


        devModelWindowWrapper.openPageWindow("../../html/masters/providerAppointment.html", profileLbl, popW, popH, true, onCloseAvailability);
        parent.setPopupWIndowHeaderColor("0bc56f","FFF");
    }
}
function onClickBlock(){
    if(operation == UPDATE) {
        var popW = "75%";
        var popH = "70%";

        var profileLbl;
        var devModelWindowWrapper = new kendoWindowWrapper();
        profileLbl = "Staff Block";
        parentRef.pid = selItem.idk;
        parentRef.TYPE = "BLOCK";
        devModelWindowWrapper.openPageWindow("../../html/masters/providerAppointment.html", profileLbl, popW, popH, true, onCloseAvailability);
    }
}
function onClickVacation() {
    if(operation == UPDATE) {
        var popW = "52%";
        var popH = "65%";

        var profileLbl;
        var devModelWindowWrapper = new kendoWindowWrapper();
        profileLbl = "Staff Leave";
        devModelWindowWrapper.openPageWindow("../../html/masters/staffVacationList.html", profileLbl, popW, popH, true, onCloseVacation);
    }
}
function onClickApprove(){
    if(operation == UPDATE) {
        var popW = "70%";
        var popH = "70%";

        var profileLbl;
        var devModelWindowWrapper = new kendoWindowWrapper();
        profileLbl = "Staff Approve";
        devModelWindowWrapper.openPageWindow("../../html/masters/carerApproveList.html", profileLbl, popW, popH, true, onCloseVacation);
    }
}
function onCloseActivities() {

}
function onCloseAvailability() {

}
function onCloseVacation() {

}

function onClickZipSearch() {
    var popW = 600;
    var popH = 500;
    if(parentRef)
        parentRef.searchZip = true;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search Zip";
    var cntry = sessionStorage.countryName;
    if(cntry.indexOf("India")>=0){
        profileLbl = "Search Postal Code";
    }else if(cntry.indexOf("United Kingdom")>=0){
        if(IsPostalCodeManual == "1"){
            profileLbl = "Search City";
        }
        else{
            profileLbl = "Search Postal Code";
        }
    }else if(cntry.indexOf("United State")>=0){
        profileLbl = "Search Zip";
    }else{
        profileLbl = "Search Zip";
    }
    devModelWindowWrapper.openPageWindow("../../html/masters/zipList.html", profileLbl, popW, popH, true, onCloseSearchZipAction);
}
var zipSelItem = null;
var cityId = "";

function onCloseSearchZipAction(evt, returnData) {
    console.log(returnData);
    if (returnData && returnData.status == "success") {
        //console.log();
        $("#cmbZip").val("");
        $("#txtZip4").val("");
        $("#txtState").val("");
        $("#txtCountry").val("");
        $("#txtCity").val("");
        var selItem = returnData.selItem;
        if (selItem) {
            zipSelItem = selItem;
            if (zipSelItem) {
                cityId = zipSelItem.idk;
                var cntry = sessionStorage.countryName;
                if (cntry.indexOf("United Kingdom") >= 0) {
                    if(IsPostalCodeManual != "1") {
                        if (zipSelItem.zip) {
                            $("#cmbZip").val(zipSelItem.zip);
                        }
                    }
                }
                else {
                    if (zipSelItem.zip) {
                        $("#cmbZip").val(zipSelItem.zip);
                    }
                }
                if (zipSelItem.zipFour) {
                    $("#txtZip4").val(zipSelItem.zipFour);
                }
                if (zipSelItem.state) {
                    $("#txtState").val(zipSelItem.state);
                }
                if (zipSelItem.country) {
                    $("#txtCountry").val(zipSelItem.country);
                }
                if (zipSelItem.city) {
                    $("#txtCity").val(zipSelItem.city);
                }
            }
        }
    }
}

function getZip() {
    getAjaxObject(ipAddress + "/city/list?is-active=1", "GET", getZipList, onError);
}

function getZipList(dataObj) {
    //var dArray = getTableListArray(dataObj);
    var dArray = [];
    if (dataObj && dataObj.response && dataObj.response.cityext) {
        if ($.isArray(dataObj.response.cityext)) {
            dArray = dataObj.response.cityext;
        } else {
            dArray.push(dataObj.response.cityext);
        }
    }
    if (dArray && dArray.length > 0) {
        //setDataForSelection(dArray, "cmbZip", onZipChange, ["zip", "id"], 0, "");
        onZipChange();
    }
    getPrefix();
}

function getPrefix() {
    getAjaxObject(ipAddress + "/master/Prefix/list?is-active=1", "GET", getCodeTableValueList, onError);
}

function onComboChange(cmbId) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb && cmb.selectedIndex < 0) {
        cmb.select(0);
    }
}

function onPrefixChange() {
    onComboChange("cmbPrefix");
}
function onSuffixChange() {
    onComboChange("cmbSuffix");
}
function onPtChange() {
    onComboChange("cmbStatus");
}

function onGenderChange() {
    onComboChange("cmbGender");
    if (selItem && selItem.idk != "") {
        var imageServletUrl = ipAddress + "/homecare/download/providers/photo/?" + Math.round(Math.random() * 1000000)+"&access_token="+sessionStorage.access_token+"&tenant="+sessionStorage.tenant+"&id="+selItem.idk;
        $("#imgPhoto").attr("src", imageServletUrl);
    } else {
        onshowImage();
    }
}

function onSMSChange() {
    onComboChange("cmbSMS");
}

function onLanChange() {
    onComboChange("cmbLang");
}

function onRaceChange() {
    onComboChange("cmbRace");
}

function onEthnicityChange() {
    onComboChange("cmbEthnicity");
}
function onReligionChange() {
    onComboChange("cmbReligion");
}

function onZipChange() {
    onComboChange("cmbZip");
    var cmbZip = $("#cmbZip").data("kendoComboBox");
    if (cmbZip) {
        var dataItem = cmbZip.dataItem();
        if (dataItem) {
            $("#txtZip4").val(dataItem.zipFour);
            $("#txtCountry").val(dataItem.country);
            $("#txtState").val(dataItem.state);
            $("#txtCity").val(dataItem.city);
        }
    }
}

function getCodeTableValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbPrefix", onPrefixChange, ["value", "idk"], 0, "");
    }
    getAjaxObject(ipAddress + "/master/Suffix/list?is-active=1", "GET", getCodeTableSuffixValueList, onError);
}
function getCodeTableSuffixValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbSuffix", onSuffixChange, ["value", "idk"], 0, "");
    }
    getAjaxObject(ipAddress + "/master/Status/list?is-active=1", "GET", getPatientStatusValueList, onError);
}


function getPatientStatusValueList(dataObj) {
    var dArray = getTableFirstListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbStatus", onPtChange, ["desc", "idk"], 0, "");
    }
    getAjaxObject(ipAddress + "/master/Gender/list?is-active=1", "GET", getGenderValueList, onError);
}

function getGenderValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbGender", onGenderChange, ["desc", "idk"], 0, "");
    }
    getAjaxObject(ipAddress + "/master/SMS/list?is-active=1", "GET", getSMSValueList, onError);
}

function getSMSValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbSMS", onSMSChange, ["desc", "idk"], 1, "");
    }
    getAjaxObject(ipAddress + "/master/Language/list?is-active=1", "GET", getLanguageValueList, onError);
}

function getLanguageValueList(dataObj) {
    var dArray = getTableFirstListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setMultipleDataForSelection(dArray, "cmbLang", onLanChange, ["desc", "idk"], 0, "");
    }
    getAjaxObject(ipAddress + "/master/Race/list?is-active=1", "GET", getRaceValueList, onError);
}

function getRaceValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbRace", onRaceChange, ["desc", "idk"], 0, "");
    }
    getAjaxObject(ipAddress + "/master/Ethnicity/list?is-active=1", "GET", getEthnicityValueList, onError);
}

function getEthnicityValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbEthnicity", onEthnicityChange, ["desc", "idk"], 0, "");
    }
    getAjaxObject(ipAddress + "/master/Religion/list?is-active=1", "GET", getReligionValueList, onError);
}
function getReligionValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbReligion", onReligionChange, ["desc", "idk"], 0, "");
    }
    getAjaxObject(ipAddress + "/facility/list?is-active=1", "GET", getFacilityList, onError);
}

function getFacilityList(dataObj) {
    console.log(dataObj);
    var dataArray = [];
    if (dataObj) {
        if ($.isArray(dataObj.response.facility)) {
            dataArray = dataObj.response.facility;
        } else {
            dataArray.push(dataObj.response.facility);
        }
    }
    var tempDataArry = [];
    var obj = {};
    obj.name = "";
    obj.idk = "";
    tempDataArry.push(obj);
    for (var i = 0; i < dataArray.length; i++) {
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if (dataArray[i].isActive == 1) {
            dataArray[i].Status = "Active";
            var obj = dataArray[i];
            obj.idk = dataArray[i].id;
            obj.status = dataArray[i].Status;
            tempDataArry.push(obj);
        }
    }
    dataArray = tempDataArry;

    setDataForSelection(dataArray, "txtFAN", onFacilityChange, ["name", "idk"], 0, "");
    onFacilityChange();
    if (operation == UPDATE && patientId != "") {
        getPatientInfo();
        // getEditInfo();
    }
}

function onFacilityChange() {
    var txtFAN = $("#txtFAN").data("kendoComboBox");
    if (txtFAN) {
        var txtFId = txtFAN.value();
        getAjaxObject(ipAddress + "/facility/list?id=" + txtFId, "GET", onGetFacilityInfo, onError);
    }
}
var billActNo = "";

function onGetFacilityInfo(dataObj) {
    billActNo = "";
    if (dataObj && dataObj.response && dataObj.response.facility) {
        $("#txtBAN").val(dataObj.response.facility[0].name);
        billActNo = dataObj.response.facility[0].billingAccountId;
    }
}

function getPatientInfo() {
    getAjaxObject(ipAddress + "/provider/list?id=" + patientId, "GET", onGetPatientInfo, onError);
}

function getComboListIndex(cmbId, attr, attrVal) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb) {
        var ds = cmb.dataSource;
        var totalRec = ds.total();
        for (var i = 0; i < totalRec; i++) {
            var dtItem = ds.at(i);
            if (dtItem && dtItem[attr] == attrVal) {
                cmb.select(i);
                return i;
            }
        }
    }
    return -1;
}


function onGetPatientInfo(dataObj) {
    var cntry = sessionStorage.countryName;
    patientInfoObject = dataObj.response.provider[0];
    parentRef.type = patientInfoObject.type;
    parentRef.userId = patientInfoObject.userId;
    var communicationLen = patientInfoObject.communications != null ? patientInfoObject.communications.length : 0;
    var cityIndexVal = "";
    for(var i=0; i<communicationLen; i++) {
        if(patientInfoObject.communications[i].parentTypeId == 500) {
            cityIndexVal = i;
        }
    }
    $("#txtID").val(patientInfoObject.id);
    $("#txtExtID1").val(patientInfoObject.externalId1);
    $("#txtExtID2").val(patientInfoObject.externalId2);
    getComboListIndex("cmbPrefix", "value", patientInfoObject.prefix);
    parentRef.firstName = patientInfoObject.firstName;
    parentRef.lastName = patientInfoObject.lastName;
    $("#txtFN").val(patientInfoObject.firstName);
    $("#txtLN").val(patientInfoObject.lastName);
    $("#txtMN").val(patientInfoObject.middleName);
    $("#txtWeight").val(patientInfoObject.weight);
    $("#txtHeight").val(patientInfoObject.height);
    // $("#txtNN").val(patientInfoObject.contactNickName);
    $("#txtAbbreviation").val(patientInfoObject.abbreviation);
    /*$("#txtN").val(patientInfoObject.name);
    $("#txtDN").val(patientInfoObject.displayName);
    $("#txtFTI").val(patientInfoObject.federalTaxId);
    $("#txtFTEIN").val(patientInfoObject.federalTaxEin);
    $("#txtSTI").val(patientInfoObject.stateTaxId);
    $("#txtNOTT").val(patientInfoObject.taxName);*/
    /*getComboListIndex("cmbFTIT", "desc", patientInfoObject.federalTaxIdType);*/
    getComboListIndex("cmbStatus", "desc", patientInfoObject.Status);
    getComboListIndex("cmbSuffix", "value", patientInfoObject.suffix);
    getComboListIndex("txtType", "Key", patientInfoObject.type);
    if(operation == "edit") {
        getProviderAvailableTimings();
    }
    //  $("#txtSMS").val(comObj.sms);
    //getComboListIndex("cmbSMS", "desc", patientInfoObject.sms);

    if(patientInfoObject.communications != null) {
        $("#cityIdHiddenValue").val(patientInfoObject.communications[cityIndexVal].cityId);
        $("#txtCountry").val(patientInfoObject.communications[cityIndexVal].country);

        $("#txtZip4").val(patientInfoObject.communications[cityIndexVal].zipFour);
        $("#txtState").val(patientInfoObject.communications[cityIndexVal].state);
        if (cntry.indexOf("United Kingdom") >= 0) {
            if(IsPostalCodeManual == "1"){
                $("#cmbZip").val(patientInfoObject.communications[cityIndexVal].houseNumber);
            }
            else{
                $("#cmbZip").val(patientInfoObject.communications[cityIndexVal].zip);
            }
        }
        else{
            $("#cmbZip").val(patientInfoObject.communications[cityIndexVal].zip);
        }
        $("#txtCity").val(patientInfoObject.communications[cityIndexVal].city);
        $("#txtHPhone").val(patientInfoObject.communications[cityIndexVal].homePhone);
        $("#txtExtension").val(patientInfoObject.communications[cityIndexVal].workPhoneExt);
        $("#txtAdd1").val(patientInfoObject.communications[cityIndexVal].address1);
        $("#txtAdd2").val(patientInfoObject.communications[cityIndexVal].address2);
        $("#txtWPhone").val(patientInfoObject.communications[cityIndexVal].workPhone);
        $("#txtExtension1").val(patientInfoObject.communications[cityIndexVal].homePhoneExt);
        $("#txtFax").val(patientInfoObject.communications[cityIndexVal].fax);
        $("#txtCell").val(patientInfoObject.communications[cityIndexVal].cellPhone);
        /*$("#cmbSMS").val(patientInfoObject.communications[cityIndexVal].sms);*/
        getComboListIndex("cmbSMS", "desc", patientInfoObject.communications[cityIndexVal].sms);
        $("#txtEmail").val(patientInfoObject.communications[cityIndexVal].email);
        getComboListIndex("cmbZip", "idk", patientInfoObject.communications[cityIndexVal].cityId);
        zipSelItem = patientInfoObject.communications[cityIndexVal];
        zipSelItem.idk = patientInfoObject.communications[cityIndexVal].id;
    }

    $("#txtFC").val(patientInfoObject.financeCharges);
    $("#txtBD").val(patientInfoObject.billableDoctor);
    $("#txtPCP").val(patientInfoObject.pcp);
    $("#txtTI").val(patientInfoObject.taxonomyId);
    $("#txtNPI").val(patientInfoObject.npi);
    $("#txtDEA").val(patientInfoObject.dea);
    $("#txtUPIN").val(patientInfoObject.upin);
    $("#txtNT").val(patientInfoObject.nuucType);
    $("#txtAS").val(patientInfoObject.amaSpeciality);
    $("#txtNEIC").val(patientInfoObject.neicSpeciality);
    $("#txtBAN").val(patientInfoObject.billingAccountName);

    getComboListIndex("cmbGender", "desc", patientInfoObject.gender);
    //onGenderChange();
    var imageServletUrl = ipAddress + "/homecare/download/providers/photo/?" + Math.round(Math.random() * 1000000)+"&access_token="+sessionStorage.access_token+"&tenant="+sessionStorage.tenant+"&id="+selItem.idk;
    $("#imgPhoto").attr("src", imageServletUrl);
    getComboListIndex("cmbEthnicity", "desc", patientInfoObject.ethnicity);
    getComboListIndex("cmbRace", "desc", patientInfoObject.race);
    getComboListMultipleIndex("cmbLang", "desc", patientInfoObject.language);
    getComboListIndex("cmbReligion", "desc", patientInfoObject.religion);
    $('#txtLikes').val(patientInfoObject.likes),
        $('#txtDisLikes').val(patientInfoObject.dislikes),
        $('#txtSkills').val(patientInfoObject.skills),
        $('#txtHR').val(patientInfoObject.hourlyRate),
        $('#txtLeavesPerYear').val(patientInfoObject.leavesPerYear),
        $('#txtOverTimeHourRate').val(patientInfoObject.overtimeHourlyRate),
        $('#txtWeeklyContractHour').val(patientInfoObject.weekelyContractHours),
        getComboListIndex("cmbEmploymentType", "desc", patientInfoObject.employmentTypeId);
    patientInfoObject.travelExpenses == "1" ? $('#txtTE').attr('checked','checked') : null;
    $('#txtHRperMile').val(patientInfoObject.ratePerMile);
    if (patientInfoObject.dateOfBirth) {
        var dt = new Date(patientInfoObject.dateOfBirth);
        if (dt) {
            var strDT = "";
            var cntry = sessionStorage.countryName;
            if (cntry.indexOf("India") >= 0) {
                strDT = kendo.toString(dt, "dd/MM/yyyy");
            } else if (cntry.indexOf("United Kingdom") >= 0) {
                strDT = kendo.toString(dt, "dd/MM/yyyy");
            } else {
                strDT = kendo.toString(dt, "MM/dd/yyyy");
            }

            $("#dtDOB1").datepicker();
            $("#dtDOB1").datepicker("setDate", strDT);
            var strAge = getAge(dt);
            $("#txtAge").val(strAge);
        }
    }
    getComboListIndex("txtFAN", "idk", patientInfoObject.facilityId);
    onFacilityChange();
    getAjaxObject(ipAddress+"/homecare/qualifications/?parentId="+ patientInfoObject.id + "&parentTypeId=500","GET",getQualificationListData,onError);
}
function onshowImage() {
    var cmbGender = $("#cmbGender").data("kendoComboBox");
    if (cmbGender) {
        if (cmbGender.text() == "Male") {
            $("#imgPhoto").attr("src", "../../img/AppImg/HosImages/male_profile.png");
        } else if (cmbGender.text() == "Female") {
            $("#imgPhoto").attr("src", "../../img/AppImg/HosImages/profile_female.png");
        } else {
            $("#imgPhoto").attr("src", "../../img/AppImg/HosImages/blank.png");
        }
    }
}
function getTableListArray(dataObj) {
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.codeTable) {
        if ($.isArray(dataObj.response.codeTable)) {
            dataArray = dataObj.response.codeTable;
        } else {
            dataArray.push(dataObj.response.codeTable);
        }
    }
    var tempDataArry = [];
    var obj = {};
    obj.desc = "";
    obj.zip = "";
    obj.value = "";
    obj.idk = "";
    tempDataArry.push(obj);
    for (var i = 0; i < dataArray.length; i++) {
        if (dataArray[i].isActive == 1) {
            var obj = dataArray[i];
            obj.idk = dataArray[i].id;
            obj.status = dataArray[i].Status;
            tempDataArry.push(obj);
        }
    }
    return tempDataArry;
}
function getTableFirstListArray(dataObj) {
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.codeTable) {
        if ($.isArray(dataObj.response.codeTable)) {
            dataArray = dataObj.response.codeTable;
        } else {
            dataArray.push(dataObj.response.codeTable);
        }
    }
    var tempDataArry = [];
    for (var i = 0; i < dataArray.length; i++) {
        if (dataArray[i].isActive == 1) {
            var obj = dataArray[i];
            obj.idk = dataArray[i].id;
            obj.status = dataArray[i].Status;
            tempDataArry.push(obj);
        }
    }
    return tempDataArry;
}


function onClickReset() {
    if(operation == ADD) {
        operation = ADD;
    }
    else {
        operation = UPDATE;
    }
    patientId = "";
    $("#txtID").val("");
    $("#txtExtID1").val("");
    $("#txtExtID2").val("");
    setComboReset("cmbPrefix");
    setComboReset("cmbSuffix");
    $("#txtFN").val("");
    $("#txtLN").val("");
    $("#txtMN").val("");
    $("#txtWeight").val("");
    $("#txtHeight").val("");
    $("#txtNN").val("");
    $("#txtAbbreviation").val("");
    $("#txtType").val("");
    $("#txtBAN").val("");
    $("#txtFAN").val("");
    $("#txtNEIC").val("");
    $("#txtAS").val("");
    $("#txtNT").val("");
    $("#txtUPIN").val("");
    $("#txtDEA").val("");
    $("#txtNPI").val("");
    $("#txtTI").val("");
    $("#txtPCP").val("");
    $("#txtBD").val("");
    $("#txtFC").val("");
    $("#txtBAN").val("");
    //$("#txtZip4").val("");


    /*var dtDOB = $("#dtDOB").data("kendoDatePicker");
    if(dtDOB){
        dtDOB.value("");
    }*/
    $("#txtSSN").val("");
    setComboReset("cmbStatus");
    setComboReset("cmbGender");
    setComboReset("cmbEthnicity");
    setComboReset("cmbRace");
    /*setComboReset("cmbLang");*/
    $('#cmbLang').closest('.ui-igcombo').find('.ui-igcombo-clearicon').trigger('click');
    $("#txtAdd1").val("");
    $("#txtAdd2").val("");
    $("#txtCity").val("");
    $("#txtState").val("");
    $("#txtCountry").val("");
    $("#cmbZip").val("");
    $("#txtZip4").val("");
    setComboReset("cmbSMS");
    setComboReset("cmbReligion");
    setComboReset("txtType");
    $("#txtCell").val("");
    $("#txtExtension1").val("");
    $("#txtWPhone").val("");
    $("#txtExtension").val("");
    $("#txtHPhone").val("");
    $("#txtEmail").val("");
    $("#rdHome").prop("checked", true);

    $('#txtLikes').val("");
    $('#txtDisLikes').val("");
    $('#txtSkills').val("");
    $('#txtHR').val("");
    $('#txtTE').removeAttr("checked");
    $('#txtHRperMile').val("");
    $('#txtWeeklyContractHour').val("");
    $('#txtLeavesPerYear').val("");
    $('#txtOverTimeHourRate').val("");
    setComboReset("cmbEmploymentType");

}

function setComboReset(cmbId) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb) {
        cmb.select(0);
    }
}

function validation() {
    var flag = true;
    var txtFC = $("#txtFC").val();
    txtFC = $.trim(txtFC);
    if (txtFC == "") {
        customAlert.error("Error", "Enter Finance charges");
        flag = false;
        return false;
    }
    if (cityId == "") {
        customAlert.error("Error", "Select City ");
        flag = false;
        return false;
    }
    /*var strAbbr = $("#txtAbbrevation").val();
    	strAbbr = $.trim(strAbbr);
    	if(strAbbr == ""){
    		customAlert.error("Error","Enter Abbrevation");
    		flag = false;
    		return false;
    	}
    	var strCode = $("#txtCode").val();
    	strCode = $.trim(strCode);
    	if(strCode == ""){
    		customAlert.error("Error","Enter Code");
    		flag = false;
    		return false;
    	}
    	var strName = $("#txtName").val();
    	strName = $.trim(strName);
    	if(strName == ""){
    		customAlert.error("Error","Enter Country");
    		flag = false;
    		return false;
    	}*/

    return flag;
}

function getComboDataItem(cmbId) {
    var dItem = null;
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb && cmb.dataItem()) {
        dItem = cmb.dataItem();
        return cmb.text();
    }
    return "";
}

function getComboDataItemValue(cmbId) {
    var dItem = null;
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb && cmb.dataItem()) {
        dItem = cmb.dataItem();
        return cmb.value();
    }
    return "";
}

function onClickSave() {
    var sEmail = $('#txtEmail').val();
    /*if (validation()) {*/
    $('.alert').remove();
    var strId = $("#txtID").val();
    var strExtId1 = $("#txtExtID1").val();
    var strExtId2 = $("#txtExtID2").val();
    var strPrefix = getComboDataItem("cmbPrefix");
    var strSuffix = getComboDataItem("cmbSuffix");
    var strNickName = $("#txtNN").val();
    var strStatus = getComboDataItem("cmbStatus");
    var strAbbr = $("#txtAbbreviation").val();
    var strFN = $("#txtFN").val();
    var strMN = $("#txtMN").val();
    var strLN = $("#txtLN").val();

    var dtItem = $("#dtDOB").data("kendoDatePicker");
    var strDate = "";
    if (dtItem && dtItem.value()) {
        strDate = kendo.toString(dtItem.value(), "yyyy-MM-dd");
    }
    var cntry = sessionStorage.countryName;
    var strhouseNumber = "";
    var dt = document.getElementById("dtDOB1").value;
    if(dt){
        if (cntry.indexOf("India") >= 0) {
            var dtArray = dt.split("/");
            dob = (dtArray[1] + "/" + dtArray[0] + "/" + dtArray[2]);
        } else if (cntry.indexOf("United Kingdom") >= 0) {
            var dtArray = dt.split("/");
            dob = (dtArray[1] + "/" + dtArray[0] + "/" + dtArray[2]);
            if(IsPostalCodeManual =="1"){
                strhouseNumber = $("#cmbZip").val();
            }
        } else {
            var dtArray = dt.split("/");
            dob = (dtArray[0] + "/" + dtArray[1] + "/" + dtArray[2]);
        }
        if (dob) {
            var DOB = new Date(dob);
            strDate = kendo.toString(DOB, "yyyy-MM-dd");
        }
    }

    if(strDate){
        var stDate = new Date(strDate);
        var currDate = new Date();
        if(currDate.getTime()<stDate.getTime()){
            var strMsg = "Invalid Date of Birth ";
            $('.customAlert').append('<div class="alert alert-danger">'+strMsg+'</div>');
            return false;
        }
    }
    var dt = document.getElementById("dtDOB1").value;
    var strSSN = $("#txtSSN").val();
    var strGender = getComboDataItem("cmbGender");
    var strAdd1 = $("#txtAdd1").val();
    var strAdd2 = $("#txtAdd2").val();
    var strCity = $("#txtCity").val();
    var strState = $("#txtState").val();
    var strCountry = $("#txtCountry").val();

    var strZip = $("#cmbZip").val();
    var strZip4 = $("#txtZip4").val();
    var strHPhone = $("#txtHPhone").val();
    var strExt = $("#txtExtension").val();
    var strWp = $("#txtWPhone").val();
    var strWpExt = $("#txtWPExt").val();
    var strCell = $("#txtCell").val();

    var strSMS = getComboDataItem("cmbSMS");

    var strEmail = $("#txtEmail").val();
    var strLan = $("#cmbLang").val();
    var strRace = getComboDataItem("cmbRace");
    var strEthinicity = getComboDataItem("cmbEthnicity");
    var strReligion = getComboDataItem("cmbReligion");

    var dataObj = {};
    dataObj.createdBy = sessionStorage.userId;
    dataObj.externalId1 = strExtId1;
    dataObj.externalId2 = strExtId2;
    dataObj.abbreviation = $("#txtAbbreviation").val();
    var txtType = $("#txtType").data("kendoComboBox");
    var strTType = "";
    if (txtType) {
        strTType = Number(txtType.value());
    }
    dataObj.type = strTType; //$("#txtType").val();;
    dataObj.prefix = strPrefix; //$("#txtEmail").val();;
    dataObj.suffix = strSuffix;
    dataObj.firstName = strFN;
    dataObj.middleName = strMN;
    dataObj.lastName = strLN;
    dataObj.nickname = strNickName;
    dataObj.billingAccountName = $("#txtBAN").val();
    dataObj.billingAccountId = Number(billActNo);
    var txtFAN = $("#txtFAN").data("kendoComboBox");
    var fId = "";
    if (txtFAN) {
        fId = txtFAN.value();
    }

    dataObj.facilityId = Number(fId);
    dataObj.neicSpeciality = $("#txtNEIC").val();
    dataObj.amaSpeciality = $("#txtAS").val();
    dataObj.nuucType = $("#txtNT").val();;
    dataObj.upin = $("#txtUPIN").val();;
    dataObj.dea = $("#txtDEA").val();;
    dataObj.npi = $("#txtNPI").val();
    dataObj.taxonomyId = $("#txtTI").val();
    dataObj.pcp = $("#txtPCP").val();
    dataObj.billableDoctor = $("#txtBD").val();
    dataObj.dateOfBirth = strDate;
    var finCharges = $("#txtFC").val() != "" ? $("#txtFC").val() : 0.0;
    dataObj.financeCharges = finCharges;
    if(strStatus == "InActive") {
        dataObj.isActive = 0;
    }
    else if(strStatus == "Active") {
        dataObj.isActive = 1;
    }
    // dataObj.userId = null;
    dataObj.height = $('#txtHeight').val();
    dataObj.weight = $('#txtWeight').val();
    dataObj.gender = strGender;
    dataObj.ethnicity = strEthinicity;
    dataObj.race = strRace;
    dataObj.language = strLan;
    dataObj.religion = strReligion;
    dataObj.likes = $('#txtLikes').val();
    dataObj.dislikes = $('#txtDisLikes').val();
    dataObj.skills = $('#txtSkills').val();
    dataObj.hourlyRate = $('#txtHR').val();
    dataObj.travelExpenses = $('#txtTE').is(':checked') ? "1" : "0";
    dataObj.ratePerMile = $('#txtHRperMile').val();
    dataObj.weekelyContractHours = $('#txtWeeklyContractHour').val();
    dataObj.leavesPerYear = $('#txtLeavesPerYear').val();
    dataObj.overtimeHourlyRate = $('#txtOverTimeHourRate').val();
    var cmbEmploymentType = $("#cmbEmploymentType").data("kendoComboBox");
    dataObj.employmentTypeId = cmbEmploymentType.value();

    var comm = [];
    var comObj = {};
    //comObj.country = $("#txtCountry").val(); //"101";
    //comObj.city = $("#txtCity").val();
    if (cntry.indexOf("United Kingdom") >= 0) {
        if (IsPostalCodeManual == "1") {

            comObj.cityId = zipSelItem.cityId || zipSelItem.idk;

        }
        else{
            if (strZip) {
                comObj.cityId = zipSelItem.cityId || zipSelItem.idk;
            }
        }
    }
    else {
        if (strZip) {
            comObj.cityId = zipSelItem.cityId || zipSelItem.idk;
        }
    }
    if(strStatus == "InActive") {
        comObj.isActive = 0;
    }
    else if(strStatus == "Active") {
        comObj.isActive = 1;
    }
    /*comObj.countryId = cityId;*/
    comObj.isDeleted = 0;
    //comObj.zipFour = $("#txtZip4").val(); //"2323";
    comObj.sms = getComboDataItem("cmbSMS");
    //comObj.state = $("#txtState").val();
    comObj.workPhoneExt = $("#txtExtension1").val();
    comObj.email = $("#txtEmail").val();
    //comObj.zip = strZip;
    comObj.parentTypeId = 500;
    comObj.address2 = $("#txtAdd2").val();
    comObj.address1 = $("#txtAdd1").val();
    /*comObj.stateId = 1;
    comObj.areaCode = "232";*/
    //comObj.homePhoneExt = $("#txtExtension").val();
    comObj.homePhone = $("#txtHPhone").val();
    comObj.houseNumber = strhouseNumber;
    comObj.workPhone = $("#txtWPhone").val();
    comObj.cellPhone = $("#txtCell").val();

    comObj.defaultCommunication = 1;
    /*if (operation == UPDATE) {}*/

    if (operation == UPDATE) {
        var communicationLen = selItem.communications != null ? selItem.communications.length : 0;
        var cityIndexVal = "";
        //var dupCityIndexVal = "";
        for(var i=0; i<communicationLen; i++) {
            if(selItem.communications[i].parentTypeId == 500) {
                cityIndexVal = i;
            }
            /*if(selItem.communications[i].parentTypeId == 501) {
                dupCityIndexVal = i;
            }*/
        }
        if(cityIndexVal != "" || (cityIndexVal == 0 && selItem.communications != null)) {
            comObj.id = selItem.communications[cityIndexVal].id;
            comObj.modifiedBy = sessionStorage.userId;
            comObj.parentId = selItem.idk;
        }
        /*if(dupCityIndexVal != "") {
            comObj1.id = selItem.communications[dupCityIndexVal].id;
            comObj1.modifiedBy = sessionStorage.userId;
            comObj1.parentId = selItem.idk;
        }*/
        //comm1.modifiedDate = new Date().getTime();
    }
    else {
        comObj.createdBy = Number(sessionStorage.userId);
    }

    comm.push(comObj);
    dataObj.communications = comm;
    if(strStatus && strDate!= "" &&  strStatus != "" && strFN != "" && strLN != "" && strGender != "" && strLan != "" && strTType != "" && strAdd1 != "" && strAbbr != "" && $("#txtFAN").val() != "") {
        // if(!validateEmail(strEmail)) {
        //     $("html,body").animate({
        //         scrollTop: 0
        //     }, 500);
        //     $('.customAlert').append('<div class="alert alert-danger">Please enter valid Email Address</div>');
        // }
        // else {


        if (cntry.indexOf("United Kingdom") >= 0){
            if(IsPostalCodeManual=="1"){

                ABBREVIATION_Patient = dataObj.abbreviation;
                if (operation == ADD) {
                    var dataUrl = ipAddress + "/provider/create";
                    createAjaxObject(dataUrl, dataObj, "POST", onCreate, onError);
                } else if (operation == UPDATE) {
                    dataObj.modifiedBy = sessionStorage.userId;
                    dataObj.id = selItem.idk;
                    dataObj.isDeleted = "0";
                    dataObj.userId = selItem.userId;
                    var dataUrl = ipAddress + "/provider/update";
                    createAjaxObject(dataUrl, dataObj, "POST", onUpdate, onError);
                }

            }
            else{
                if(strZip != ""){
                    ABBREVIATION_Patient = dataObj.abbreviation;
                    if (operation == ADD) {
                        var dataUrl = ipAddress + "/provider/create";
                        createAjaxObject(dataUrl, dataObj, "POST", onCreate, onError);
                    } else if (operation == UPDATE) {
                        dataObj.modifiedBy = sessionStorage.userId;
                        dataObj.id = selItem.idk;
                        dataObj.isDeleted = "0";
                        dataObj.userId = selItem.userId;
                        var dataUrl = ipAddress + "/provider/update";
                        createAjaxObject(dataUrl, dataObj, "POST", onUpdate, onError);
                    }
                }
                else{
                    $("html,body").animate({
                        scrollTop: 0
                    }, 500);
                    $('.customAlert').append('<div class="alert alert-danger">Please fill all the Required Fields</div>');
                }
            }

        }else{
            if(strZip != ""){
                ABBREVIATION_Patient = dataObj.abbreviation;
                if (operation == ADD) {
                    var dataUrl = ipAddress + "/provider/create";
                    createAjaxObject(dataUrl, dataObj, "POST", onCreate, onError);
                } else if (operation == UPDATE) {
                    dataObj.modifiedBy = sessionStorage.userId;
                    dataObj.id = selItem.idk;
                    dataObj.isDeleted = "0";
                    var dataUrl = ipAddress + "/provider/update";
                    createAjaxObject(dataUrl, dataObj, "POST", onUpdate, onError);
                }
            }
            else{
                $("html,body").animate({
                    scrollTop: 0
                }, 500);
                $('.customAlert').append('<div class="alert alert-danger">Please fill all the Required Fields</div>');
            }
        }

        // }
    }
    else {
        $("html,body").animate({
            scrollTop: 0
        }, 500);
        $('.customAlert').append('<div class="alert alert-danger">Please fill all the Required Fields</div>');
    }
    /*}*/
}
function onTypeChange() {

}
function buildQualificationListGrid(dataSource) {
    if(dataSource.length > 0) {
        for (var i = 0; i < dataSource.length; i++) {
            var $trLen = i;
            var passDateString = new Date(dataSource[i].dateOfPass).toLocaleDateString().split('/').join('-') || "";
            // var passDateString = "";

            var expiryDateString = new Date(dataSource[i].dateOfExpiry).toLocaleDateString().split('/').join('-') || "";
            $('.qualificationTabWrapper table tbody').append('<tr class="qualWrapper_li qualificationTabWrapper-li-' + $trLen + '" data-attr-qualId="' + dataSource[i].idk + '"><td class="qual-td-addRemove"><span><a href="#" class="qualAddRemoveLink addQual">+</a> <a href="#" class="qualAddRemoveLink removeQual">-</a></span></td><td><input type="text" name="qualWrap-sno" value="' + $trLen + '" readonly /></td><td><input type="text" name="qualWrap-qualification" id="qualWrapQualification" maxlength="100" value="' + dataSource[i].qualification + '" /></td><td><input type="text" name="qualWrap-university" id="qualWrapUniversity" maxlength="100" value="' + dataSource[i].university + '" /></td><td><input type="text" name="qualWrap-percentage" id="qualWrapPercentage" value="' + dataSource[i].percentage + '" /></td><td><input type="text" name="qualWrap-passedout" id="qualWrapPassedout" value="' + passDateString + '" style="width: auto !important; padding-left: 0 !important;" /></td><td><input type="text" name="qualWrap-expirydate" value="' + expiryDateString + '" style="width: auto !important; padding-left: 0 !important;" /></td></tr>');
            allowOnlyDecimals($('[name="qualWrap-percentage"]'));
            $('.qualificationTabWrapper-li-' + $trLen).find('[name="qualWrap-passedout"]').kendoDatePicker({
                format: "dd-MM-yyyy",
                change: function (e) {
                    selectedDate = $('.qualificationTabWrapper-li-' + $trLen).find('[name="qualWrap-passedout"]').data("kendoDatePicker").value();
                }
            });
            //$('.qualificationTabWrapper-li-'+$trLen).find('[name="qualWrap-passedout"]').data("kendoDatePicker").value(selectedDate);
            $('.qualificationTabWrapper-li-' + $trLen).find('[name="qualWrap-expirydate"]').kendoDatePicker({
                format: "dd-MM-yyyy",
                change: function (e) {
                    selectedDate = $('.qualificationTabWrapper-li-' + $trLen).find('[name="qualWrap-expirydate"]').data("kendoDatePicker").value();
                }
            });
            //$('.qualificationTabWrapper-li-'+$trLen).find('[name="qualWrap-expirydate"]').data("kendoDatePicker").value(selectedDate);
            modifySerialNumber();
        }
    }
    else{
        $('.qualificationTabWrapper table tbody').append('');
    }

}

function onCreate(dataObj) {
    console.log(dataObj);
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            selItem = dataObj.response.provider;
            selItem.idk = selItem.id;
            $("#btnUpload").click();
            var obj = {};
            obj.status = "success";
            obj.operation = operation;
            //popupClose(obj);
            if(ABBREVIATION_Patient != "") {
                getAjaxObject(ipAddress+"/provider/list?is-active=1","GET",getProviderListData,onError);
            }
        } else {
            customAlert.error("error", dataObj.response.status.message);
        }
    }
    /*var obj = {};
    obj.status = "success";
    obj.operation = operation;
    popupClose(obj);*/
}
function getProviderListData(dataObj) {
    console.log(dataObj);
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if($.isArray(dataObj.response.provider)){
            dataArray = dataObj.response.provider;
        }else{
            dataArray.push(dataObj.response.provider);
        }
    }
    for(var i=dataArray.length - 1; i >= 0; i--){
        if(ABBREVIATION_Patient == dataArray[i].abbreviation) {
            qualifyFunction(dataArray[i].id, "create");
            break;
        }
    }
    var obj = {};
    obj.status = "success";
    obj.operation = operation;
    popupClose(obj);
}

function getQualificationListData(dataObj) {
    console.log(dataObj);
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if($.isArray(dataObj.response.qualifications)){
            dataArray = dataObj.response.qualifications;
        }else{
            dataArray.push(dataObj.response.qualifications);
        }
    }
    if(dataArray.length > 0) {
        for (var i = 0; i < dataArray.length; i++) {
            if(dataArray[i] && dataArray[i].id != undefined) {
                dataArray[i].idk = dataArray[i].id;
                dataArray[i].Status = "InActive";
                if (dataArray[i].isActive == 1) {
                    dataArray[i].Status = "Active";
                    var createdDate = new Date(dataArray[i].createdDate);
                    var createdDateString = createdDate.toLocaleDateString();
                    var modifiedDate = new Date(dataArray[i].modifiedDate);
                    var modifiedDateString = modifiedDate.toLocaleDateString();
                    dataArray[i].createdDateString = createdDateString;
                    dataArray[i].modifiedDateString = modifiedDateString;
                }
            }
        }
    }
    buildQualificationListGrid(dataArray);
}
function qualifyFunction(id, testString) {
    $('.qualificationTabWrapper .qualWrapper_li').each(function() {
        var expiryDate = $(this).find('[name="qualWrap-expirydate"]').val();
        var passDate = $(this).find('[name="qualWrap-passedout"]').val();
        // var expiryDateVal = new Date(expiryDate).getTime();
        // var passDateVal = new Date(passDate).getTime();
        var expiryDateVal = getDateTime(expiryDate);
        var passDateVal = getDateTime(passDate);

        if(expiryDateVal && passDateVal && expiryDateVal>passDateVal){
            var obj = {
                "qualification": $(this).find('[name="qualWrap-qualification"]').val(),
                "dateOfExpiry": expiryDateVal,
                "parentTypeId": 500,
                "university": $(this).find('[name="qualWrap-university"]').val(),
                "isActive": 1,
                "isDeleted": 0,
                "parentId": id,
                "createdBy": Number(sessionStorage.userId),
                "percentage": Number($(this).find('[name="qualWrap-percentage"]').val()),
                "dateOfPass": passDateVal
            }

            var dataUrl = ipAddress + "/homecare/qualifications/";
            if(testString == "create") {
                createAjaxObjectAsync(dataUrl, obj, "POST", onCreateQualification, onError);
            } else if(testString == "update") {
                if($(this).attr('data-attr-qualId') != undefined) {
                    obj.modifiedBy = Number(sessionStorage.userId);
                    obj.id = Number($(this).attr('data-attr-qualId'));
                    createAjaxObjectAsync(dataUrl, obj, "PUT", onUpdateQualification, onError);
                } else {
                    createAjaxObjectAsync(dataUrl, obj, "POST", onCreateQualification, onError);
                }
            }
        }
    });
}
function onCreateQualification(dataObj) {
    console.log(dataObj);
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            if($('.alert-qualify') && $('.alert-qualify').length == 0) {
                $('.customAlert').append('<div class="alert alert-success alert-qualify">'+dataObj.response.status.message+'</div>');
            } else {
                $('.alert.alert-alert-qualify').text(dataObj.response.status.message);
            }
        } else {
            //$('.customAlert').append('<div class="alert alert-danger">'+dataObj.response.status.message+'</div>');
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}
function onUpdateQualification(dataObj) {
    console.log(dataObj);
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            if($('.alert-qualify') && $('.alert-qualify').length == 0) {
                $('.customAlert').append('<div class="alert alert-success alert-qualify">'+dataObj.response.status.message+'</div>');
            } else {
                $('.alert.alert-qualify').text(dataObj.response.status.message);
            }
        } else {
            //$('.customAlert').append('<div class="alert alert-danger">'+dataObj.response.status.message+'</div>');
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}

function onUpdate(dataObj){
    console.log(dataObj);
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            /*var obj = {};
            obj.status = "success";
            obj.operation = operation;
            popupClose(obj);*/
            var response= dataObj.response.status.message;
            $('.customAlert').append('<div class="alert alert-success">'+response.replace("Provider","Staff")+'</div>');
            $('.tabContentTitle').text('Update Staff');
            var id = $('#txtID').val();
            if(ABBREVIATION_Patient != "") {
                qualifyFunction(id, "update");
            }
            getAjaxObject(ipAddress+"/provider/list?id="+id,"GET",updateList,onError);
        } else {
            //$('.customAlert').append('<div class="alert alert-danger">'+dataObj.response.status.message+'</div>');
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}
function updateList(dataObj) {
    console.log(dataObj);
    selItem = dataObj.response.provider[0];
    selItem.idk = selItem.id;
    getAjaxObject(ipAddress+"/provider/list?is-active=1","GET",getAccountList,onError);
}

function onError(errObj) {
    console.log(errObj);
    customAlert.error("Error", "Error");
}

function onClickCancel() {
    var obj = {};
    obj.status = "false";
    popupClose(obj);
}

function onClickSearch() {
    var obj = {};
    obj.status = "search";
    popupClose(obj);
}

function popupClose(obj) {
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}
function validateEmail(sEmail) {
    console.log(sEmail);
    var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    if (filter.test(sEmail)) {
        return true;
    }
    else {
        return false;
    }
}

function allowOnlyDecimals(selector) {
    selector.keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 95 && e.which != 46) {
            return false;
        }
    });
}

function getDateTime(sTime){
    var now;
    var cntry = sessionStorage.countryName;
    if (cntry.indexOf("India") >= 0) {
        now = new Date(sTime.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
    } else if (cntry.indexOf("United Kingdom") >= 0) {
        now = new Date(sTime.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
    } else {
        now = new Date(sTime);
    }
    var utc = now.getTime();
    return utc;
}
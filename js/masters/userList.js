var angularUIgridWrapper;

$(document).ready(function(){
	parentRef = parent.frames['iframe'].window;
	var dataOptions = {
        pagination: false,
        changeCallBack: onChange
	}
	angularUIgridWrapper = new AngularUIGridWrapper("dgridUserList1", dataOptions);
	angularUIgridWrapper.init();
	buildUserListGrid([]);
});


$(window).load(function(){
	loading = false;
	$(window).resize(adjustHeight);
onLoaded();
	if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
	init(); 
    buttonEvents();
	adjustHeight();
}

var userStatus = "1";
function init(){
	userStatus = "1";
	getUserRecords();
}
function getUserRecords(){
	$("#btnSave").prop("disabled", true);
	 $("#btnDelete").prop("disabled", true);
	 $("#btnResetPass").prop("disabled", true);
	buildUserListGrid([]);
	getAjaxObject(ipAddress+"/user/list/?is-active="+userStatus+"&is-deleted=0","GET",getUserList,onError);
}

function onError(errorObj){
	console.log(errorObj);
}
function getUserList(dataObj){
	console.log(dataObj);
	var dataArray = [];
	if(dataObj && dataObj.response && dataObj.response.status){
		if(dataObj.response.status.code == "1"){
			if($.isArray(dataObj.response.user)){
				dataArray = dataObj.response.user;
			}else{
				dataArray.push(dataObj.response.user);
			}
		}
	}
	var tempDataArry = [];
	for(var i=0;i<dataArray.length;i++){
		dataArray[i].idk = dataArray[i].id; 
		dataArray[i].Status = "InActive";
		if(dataArray[i].isActive == 1){
			dataArray[i].Status = "Active";
		}
	}
	buildUserListGrid(dataArray);
}
function buttonEvents(){
	$("#btnSave").off("click",onClickOK);
	$("#btnSave").on("click",onClickOK);
	
	$("#btnCancelDet").off("click",onClickCancel);
	$("#btnCancelDet").on("click",onClickCancel);
	
	$("#btnDelete").off("click",onClickDelete);
	$("#btnDelete").on("click",onClickDelete);
	
	$("#btnAdd").off("click");
	$("#btnAdd").on("click",onClickAdd);
	
	$("#btnPass").off("click");
	$("#btnPass").on("click",onClickResetPass);
	
	$("#btnResetPass").off("click");
	$("#btnResetPass").on("click",onClickResetPass);
	
	$("#btnActive").off("click");
	$("#btnActive").on("click",onClickActiveUsers);
	
	$("#btnInActive").off("click");
	$("#btnInActive").on("click",onClickInActiveUsers);
	
	$("#btnCreated").off("click");
	$("#btnCreated").on("click",onClickCreatedUsers);
	
	$("#btnValidated").off("click");
	$("#btnValidated").on("click",onClickValidateUsers);
	
}
function onClickActiveUsers(){
	userStatus = "1";
	$("#btnActive").addClass("selectButtonBarClass");
	$("#btnInActive").removeClass("selectButtonBarClass");
	$("#btnCreated").removeClass("selectButtonBarClass");
	$("#btnValidated").removeClass("selectButtonBarClass");
	getUserRecords();
}
function onClickInActiveUsers(){
	userStatus = "0";
	$("#btnActive").removeClass("selectButtonBarClass");
	$("#btnInActive").addClass("selectButtonBarClass");
	$("#btnCreated").removeClass("selectButtonBarClass");
	$("#btnValidated").removeClass("selectButtonBarClass");
	getUserRecords();
}
function onClickCreatedUsers(){
	userStatus = "2";
	$("#btnActive").removeClass("selectButtonBarClass");
	$("#btnInActive").removeClass("selectButtonBarClass");
	$("#btnCreated").addClass("selectButtonBarClass");
	$("#btnValidated").removeClass("selectButtonBarClass");
	getUserRecords();
}
function onClickValidateUsers(){
	userStatus = "3";
	$("#btnActive").removeClass("selectButtonBarClass");
	$("#btnInActive").removeClass("selectButtonBarClass");
	$("#btnCreated").removeClass("selectButtonBarClass");
	$("#btnValidated").addClass("selectButtonBarClass");
	getUserRecords();
}
function adjustHeight(){
	var defHeight = 120;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
	angularUIgridWrapper.adjustGridHeight(cmpHeight);
}

function buildUserListGrid(dataSource) {
	var gridColumns = [];
	var otoptions = {};
	otoptions.noUnselect = false;
	gridColumns.push({
        "title": "User Id",
        "field": "id",
        "width": "20%",
	});
	gridColumns.push({
        "title": "User Name",
        "field": "username",
        "width": "20%",
	});
	gridColumns.push({
        "title": "First Name",
        "field": "firstName",
        "width": "20%",
	});
	gridColumns.push({
        "title": "Middle Name",
        "field": "middleName",
        "width": "20%",
	});
    gridColumns.push({
        "title": "Last Name",
        "field": "lastName",
        "width": "20%",
	}); 
	
	
   angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions); 
	adjustHeight();
	//onIndividualRowClick();
}
var prevSelectedItem =[];
function onChange(){
	setTimeout(function(){
		 var selectedItems = angularUIgridWrapper.getSelectedRows();
		 if(selectedItems && selectedItems.length>0){
			 $("#btnSave").prop("disabled", false);
			 $("#btnPass").prop("disabled", false);
			 $("#btnDelete").prop("disabled", false);
			 $("#btnResetPass").prop("disabled", false);
		 }else{
			 $("#btnSave").prop("disabled", true);
			 $("#btnPass").prop("disabled", true);
			 $("#btnDelete").prop("disabled", true);
			 $("#btnResetPass").prop("disabled", true);
		 }
	})
}

function onClickResetPass(){
	var popW = "50%";
    var popH = "60%";
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Reset Password";
    var selectedItems = angularUIgridWrapper.getSelectedRows();
	 console.log(selectedItems);
	 if(selectedItems && selectedItems.length>0){
		 parentRef.selItem = selectedItems[0];
		  devModelWindowWrapper.openPageWindow("../../html/masters/resetPassword.html", profileLbl, popW, popH, true, onCloseCreateUserAction);
	 }
}
function onCloseCreateUserAction(evt,returnData){
	if(returnData && returnData.status == "success"){
		customAlert.info("info", "Password updated successfully");
	}
}
function onClickOK(){
	setTimeout(function(){
		 var selectedItems = angularUIgridWrapper.getSelectedRows();
		 console.log(selectedItems);
		 if(selectedItems && selectedItems.length>0){
			 var obj = {};
			 obj.selItem = selectedItems[0];
			// var onCloseData = new Object();
			 obj.status = "success";
			 obj.operation = "ok";
				var windowWrapper = new kendoWindowWrapper();
				windowWrapper.closePageWindow(obj);
		 }
	})
}
function onClickDelete(){
	customAlert.confirm("Confirm", "Are you sure you want delete?",function(response){
		if(response.button == "Yes"){
			var selectedItems = angularUIgridWrapper.getSelectedRows();
			 console.log(selectedItems);
			 if(selectedItems && selectedItems.length>0){
				 var selItem = selectedItems[0];
				 if(selItem){
					 var dataUrl = ipAddress+"/user/delete/";
					 var reqObj = {};
					 reqObj.id = selItem.idk;
					 reqObj.abbr = selItem.abbr;
					 reqObj.country = selItem.county;
					 reqObj.code = selItem.code;
					 reqObj.isDeleted = "1";
					 reqObj.modifiedBy = sessionStorage.userId;
					 createAjaxObject(dataUrl,reqObj,"POST",onDeleteCountryt,onError);
				 }
			 }
		}
	});
}
function onDeleteCountryt(dataObj){
	console.log(dataObj);
	if(dataObj && dataObj.response && dataObj.response.status){
		if(dataObj.response.status.code == "1"){
			customAlert.info("info", "County Deleted Successfully");
			init();
		}else{
			customAlert.error("error", response.status.message);
		}
	}
}
function onClickAddPass(){
	
}
function onClickAdd(){
	var obj = {};
	 obj.status = "Add";
	 obj.operation = "ok";
		var windowWrapper = new kendoWindowWrapper();
		windowWrapper.closePageWindow(obj);
}
function onClickCancel(){
	var obj = {};
	var windowWrapper = new kendoWindowWrapper();
	windowWrapper.closePageWindow(obj);
}

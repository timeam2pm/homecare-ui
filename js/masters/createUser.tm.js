var parentRef = null;
var operation = "";
var selItem = null;
var ADD = "add";
var UPDATE = "update";
var VIEW = "view";
var DELETE = "delete";
var statusArr = [{Key:'Active',Value:'Active'},{Key:'InActive',Value:'InActive'}];
var userTpeArray = [{Key:'Patient',Value:'500'},{Key:'Relative',Value:'600'},{Key:'Nurse',Value:'200'},{Key:'Doctor',Value:'100'},{Key:'Care Giver',Value:'201'}];
var tzArray = [{Key:'',Value:''},{Key:"(GMT-12:00) International Date Line West",Value:'Etc/GMT+12'}];


var dietType = "1";
$(document).ready(function(){
    parentRef = parent.frames['iframe'].window;
    tzArray.push({Key:"(GMT-11:00) Midway Island, Samoa",Value:'Pacific/Midway',Offset:660});
    tzArray.push({Key:"(GMT-10:00) Hawaii",Value:'Pacific/Honolulu',Offset:600});
    tzArray.push({Key:"(GMT-09:00) Alaska",Value:'US/Alaska',Offset:540});
    tzArray.push({Key:"(GMT-08:00) Pacific Time (US & Canada)",Value:'America/Los_Angeles',Offset:480});
    tzArray.push({Key:"(GMT-08:00) Tijuana, Baja California",Value:'America/Tijuana',Offset:480});
    tzArray.push({Key:"(GMT-07:00) Arizona",Value:'US/Arizona',Offset:420});
    tzArray.push({Key:"(GMT-07:00) Chihuahua, La Paz, Mazatlan",Value:'America/Chihuahua',Offset:420});
    tzArray.push({Key:"(GMT-07:00) Mountain Time (US & Canada)",Value:'US/Mountain',Offset:420});
    tzArray.push({Key:"(GMT-06:00) Central America",Value:'America/Managua',Offset:360});
    tzArray.push({Key:"(GMT-06:00) Central Time (US & Canada)",Value:'US/Central',Offset:360});
    tzArray.push({Key:"(GMT-06:00) Guadalajara, Mexico City, Monterrey",Value:'America/Mexico_City',Offset:360});
    tzArray.push({Key:"(GMT-06:00) Saskatchewan",Value:'Canada/Saskatchewan',Offset:360});
    tzArray.push({Key:"(GMT-05:00) Bogota, Lima, Quito, Rio Branco",Value:'America/Bogota',Offset:300});
    tzArray.push({Key:"(GMT-05:00) Eastern Time (US & Canada)",Value:'US/Eastern',Offset:300});
    tzArray.push({Key:"(GMT-05:00) Indiana (East)",Value:'US/East-Indiana',Offset:300});
    tzArray.push({Key:"(GMT-04:00) Atlantic Time (Canada)",Value:'Canada/Atlantic',Offset:240});
    tzArray.push({Key:"(GMT-04:00) Caracas, La Paz",Value:'America/Caracas',Offset:240});
    tzArray.push({Key:"(GMT-04:00) Manaus",Value:'America/Manaus',Offset:240});
    tzArray.push({Key:"(GMT-04:00) Santiago",Value:'America/Santiago',Offset:240});
    tzArray.push({Key:"(GMT-03:30) Newfoundland",Value:'Canada/Newfoundland',Offset:180});
    tzArray.push({Key:"(GMT-03:00) Brasilia",Value:'America/Sao_Paulo',Offset:180});
    tzArray.push({Key:"(GMT-03:00) Buenos Aires, Georgetown",Value:'America/Argentina/Buenos_Aires',Offset:180});
    tzArray.push({Key:"(GMT-03:00) Greenland",Value:'America/Godthab',Offset:180});
    tzArray.push({Key:"(GMT-03:00) Montevideo",Value:'America/Montevideo',Offset:180});
    tzArray.push({Key:"(GMT-02:00) Mid-Atlantic",Value:'America/Noronha',Offset:120});
    tzArray.push({Key:"(GMT-01:00) Cape Verde Is",Value:'Atlantic/Cape_Verde',Offset:60});
    tzArray.push({Key:"(GMT-01:00) Azores",Value:'Atlantic/Azores',Offset:60});
    tzArray.push({Key:"(GMT+00:00) Casablanca, Monrovia, Reykjavik",Value:'Africa/Casablanca',Offset:0});
    tzArray.push({Key:"(GMT+00:00) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London",Value:'Etc/Greenwich',Offset:0});
    tzArray.push({Key:"(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna",Value:'Europe/Amsterdam',Offset:-60});
    tzArray.push({Key:"(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague",Value:'Europe/Belgrade',Offset:-60});
    tzArray.push({Key:"(GMT+01:00) Brussels, Copenhagen, Madrid, Paris",Value:'Europe/Brussels',Offset:-60});
    tzArray.push({Key:"(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb",Value:'Europe/Sarajevo',Offset:-60});
    tzArray.push({Key:"(GMT+01:00) West Central Africa",Value:'Africa/Lagos',Offset:-120});
    tzArray.push({Key:"(GMT+02:00) Amman",Value:'Asia/Amman',Offset:-120});
    tzArray.push({Key:"(GMT+02:00) Athens, Bucharest, Istanbul",Value:'Europe/Athens',Offset:-120});
    tzArray.push({Key:"(GMT+02:00) Beirut",Value:'Asia/Beirut',Offset:-120});
    tzArray.push({Key:"(GMT+02:00) Cairo",Value:'Africa/Cairo',Offset:-120});
    tzArray.push({Key:"(GMT+02:00) Harare, Pretoria",Value:'Africa/Harare',Offset:-120});
    tzArray.push({Key:"(GMT+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius",Value:'Europe/Helsinki',Offset:-120});
    tzArray.push({Key:"(GMT+02:00) Jerusalem",Value:'Asia/Jerusalem',Offset:-120});
    tzArray.push({Key:"(GMT+02:00) Minsk",Value:'Europe/Minsk',Offset:-120});
    tzArray.push({Key:"(GMT+02:00) Windhoek",Value:'Africa/Windhoek',Offset:-120});
    tzArray.push({Key:"(GMT+03:00) Kuwait, Riyadh, Baghdad",Value:'Asia/Kuwait',Offset:-180});
    tzArray.push({Key:"(GMT+03:00) Moscow, St. Petersburg, Volgograd",Value:'Europe/Moscow',Offset:-180});
    tzArray.push({Key:"(GMT+03:00) Nairobi",Value:'Africa/Nairobi',Offset:-180});
    tzArray.push({Key:"(GMT+03:00) Tbilisi",Value:'Asia/Tbilisi',Offset:-180});
    tzArray.push({Key:"(GMT+03:30) Tehran",Value:'Asia/Tehran',Offset:-210});
    tzArray.push({Key:"(GMT+04:00) Abu Dhabi, Muscat",Value:'Asia/Muscat',Offset:-240});
    tzArray.push({Key:"(GMT+04:00) Baku",Value:'Asia/Baku',Offset:-240});
    tzArray.push({Key:"(GMT+04:00) Yerevan",Value:'Asia/Yerevan',Offset:-240});
    tzArray.push({Key:"(GMT+04:30) Kabul",Value:'Asia/Kabul',Offset:-270});
    tzArray.push({Key:"(GMT+05:00) Yekaterinburg",Value:'Asia/Yekaterinburg',Offset:-300});
    tzArray.push({Key:"(GMT+05:00) Islamabad, Karachi, Tashkent",Value:'Asia/Karachi',Offset:-300});
    tzArray.push({Key:"(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi",Value:'Asia/Calcutta',Offset:-330});
    tzArray.push({Key:"(GMT+05:30) Sri Jayawardenapura",Value:'Asia/Calcutta',Offset:-330});
    tzArray.push({Key:"(GMT+05:45) Kathmandu",Value:'Asia/Katmandu',Offset:-345});

    $("#txtPass").keyup(function(){
        checkPassword();
    });

    $('.sample').keyup(function() {
        if($('#txtPass').val() != "" && $('#txtCPass').val() != "") {
            $('#txtFN').removeAttr('disabled');
            $('#txtMN').removeAttr('disabled');
            $('#txtLN').removeAttr('disabled');
        } else if($('#txtFN').val() !="" && $('#txtMN').val() !="" && $('#txtLN').val() !="") {
            $('#txtDOB').removeAttr('disabled');
            $('#cmbGender').removeAttr('disabled');

        }else if ($('#txtDOB').val() !="" && $('#cmbGender').val() !=""){
            //$('#txtEmail').removeAttr('disabled');
            //$('#txtTZ').removeAttr('disabled');


        }else {
            /* $('#txtFN').attr('disabled', true);
             $('#txtMN').attr('disabled',true);
             $('#txtLN').attr('disabled',true);
             $('#txtDOB').attr('disabled',true);
             $('#cmbGender').attr('disabled',true);
             $('#txtEmail').attr('disabled',true);
             $('#txtTZ').attr('disabled',true);*/


        }
    });
});

/*$('.sample').keyup(function() {
    if($('#txtPass').val() != "" && $('#txtCPass').val() != "") {
       $('#txtFN').removeAttr('disabled');
    } else {
       $('#txtFN').attr('disabled', true);
    }
});*/
function adjustHeight(){
    var defHeight = 45;
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 40;
    }
}

function  checkPassword(){
    var val=document.getElementById("txtPass").value;
    var meter=document.getElementById("meter");

    var no=0;
    if(val != ""){
        // If the password length is less than or equal to 6
        if(val.length<=6){
            no=1;
        }

        // If the password length is greater than 6 and contain any lowercase alphabet or any number or any special character
        if(val.length>6 && (val.match(/[a-z]/) || val.match(/\d+/) || val.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/))){
            no=2;
        }

        // If the password length is greater than 6 and contain alphabet,number,special character respectively
        if(val.length>6 && ((val.match(/[a-z]/) && val.match(/\d+/)) || (val.match(/\d+/) && val.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/)) || (val.match(/[a-z]/) && val.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/)))){
            no=3;
        }

        // If the password length is greater than 6 and must contain alphabets,numbers and special characters
        if(val.length>6 && val.match(/[a-z]/) && val.match(/\d+/) && val.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/)){
            no=4;
        }
        if(no==1){
            $("#meter").animate({width:'50px'},300);
            meter.style.backgroundColor="red";
            document.getElementById("pass_type").innerHTML="Very Weak";
        }else if(no==2){
            $("#meter").animate({width:'100px'},300);
            meter.style.backgroundColor="#F5BCA9";
            document.getElementById("pass_type").innerHTML="Weak";
        }else if(no==3){
            $("#meter").animate({width:'150px'},300);
            meter.style.backgroundColor="#FF8000";
            document.getElementById("pass_type").innerHTML="Good";
        }else if(no==4) {
            $("#meter").animate({width:'100%'},300);
            meter.style.backgroundColor="#00FF40";
            document.getElementById("pass_type").innerHTML="Strong";
        }
    }else{
        meter.style.backgroundColor="white";
        document.getElementById("pass_type").innerHTML="";
    }
}
$(window).load(function(){
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
    init();
});

function init(){
    //allowAlphaNumeric("txtPass");
    //allowAlphaNumeric("txtCPass");
    allowAlphabets("txtFN");
    allowAlphabets("txtFN");
    allowAlphabets("txtMN");
    allowAlphabets("txtLN");
    allowAlphabets("txtTZ");
    allowAlphabets("txtSAns1");
    allowAlphabets("txtSAns2");
    allowAlphabets("txtSAns3");
    validateEmail("txtEmail");



    /*$('#txtUN, #btnUserAvl').bind('keyup', function() {
         if(allFilled()) {
             $('#txtPass').removeAttr('disabled');
         }else{
             $('#txtPass').attr("disabled","disabled");
         }
     });

     function allFilled() {
         var filled = true;
         $('body input').each(function() {
             if($(this).val() == '') filled = false;
         });
         return filled;
     }*/

    var pnlHeight = window.innerHeight;
    var imgHeight = pnlHeight-90;
    $("#divTop").height(imgHeight);

    operation = parentRef.operation;
    selItem = parentRef.selItem;
    if(operation == UPDATE){
        uid = selItem.idk;
    }
    $("#txtDOB").kendoDatePicker();
    $("#cmbDeviceModel").kendoComboBox();
    $("#txtUserRole").kendoComboBox();
    $("#txtUserType").kendoComboBox();
    $("#txtSQ1").kendoComboBox();
    $("#txtSQ2").kendoComboBox();
    $("#txtSQ3").kendoComboBox();
    $("#cmbGender").kendoComboBox();

    setDataForSelection(userTpeArray, "txtUserType", onGenderChange, ["Key", "Value"], 0, "");
    setDataForSelection(tzArray, "txtTZ", onGenderChange, ["Key", "Value"], 0, "");

    getGenders();
    buttonEvents();
}
/*function onlyAlphabets(e, t) {
     try {
     if (window.event) {
     var charCode = window.event.keyCode;
     }
     else if (e) {
     var charCode = e.which;
     }
     else {
     return true;
     }
     if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))
     return true;
     else
     return false;
     }
     catch (err) {
     alert(err.Description);
     }
}*/
/*function Validate(event) {
        var regex = new RegExp("^[A-Za-z0-9?]");
        var key = String.fromCharCode(event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            event.preventDefault();
            return false;
        }
}


function validateEmail(sEmail) {
    console.log(sEmail);
    var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    if (filter.test(sEmail)) {
    return true;
    }
    else {
    return false;
    }
}*/

function buttonEvents(){
    $("#btnCancel").off("click",onClickCancel);
    $("#btnCancel").on("click",onClickCancel);

    $("#btnSave").off("click",onClickSave);
    $("#btnSave").on("click",onClickSave);

    $("#btnSearch").off("click",onClickSearch);
    $("#btnSearch").on("click",onClickSearch);

    $("#btnReset").off("click",onClickReset);
    $("#btnReset").on("click",onClickReset);

    $("#btnUserAvl").off("click");
    $("#btnUserAvl").on("click",onClickAvailable)

    $("#btnVerify").off("click");
    $("#btnVerify").on("click",onClickVerify);
}
function onClickAvailable(){
    var strUN = $("#txtUN").val();
    if(strUN != ""){
        var dataUrl = ipAddress+"/user/is-available/"+strUN;
        getAjaxObject(dataUrl,"GET",getUserAvailable,onError);
    }

}
function getUserAvailable(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.response && dataObj.response.status){
        $("#divUser").removeClass("removeEvents");
        if(dataObj.response.status.code == "1"){
            $("#divUser").removeClass("removeEvents");
            $("#divUser").show();
            customAlert.error("Info", "User is available");
        }else{
            $("#divUser").addClass("removeEvents");
            customAlert.error("Info", "User is not available");
        }
    }
}
function getGenders(){
    getAjaxObject(ipAddress+"/master/Gender/list/?is-active=1","GET",getGenderValueList,onError);
}
function getGenderValueList(dataObj){
    console.log(dataObj);
    var genderArray = [];
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            if($.isArray(dataObj.response.codeTable)){
                genderArray = dataObj.response.codeTable;
            }else{
                genderArray.push(dataObj.response.codeTable);
            }
        }
    }
    setDataForSelection(genderArray, "cmbGender", onGenderChange, ["desc", "id"], 0, "");
    getSecurityQuestions();
}
function onGenderChange(){
    var cmbGender = $("#cmbGender").data("kendoComboBox");
    if(cmbGender && cmbGender.selectedIndex<0){
        cmbGender.select(0);
    }
}
function getSecurityQuestions(){
    getAjaxObject(ipAddress+"/security-question/list/","GET",getSecurityQuestionList,onError);
}
function getSecurityQuestionList(dataObj){
    console.log(dataObj);
    var arrQue = dataObj;
    setDataForSelection(arrQue, "txtSQ1", onQD1Change, ["question", "id"], 0, "");
    setDataForSelection(arrQue, "txtSQ2", onQD1Change, ["question", "id"], 0, "");
    setDataForSelection(arrQue, "txtSQ3", onQD1Change, ["question", "id"], 0, "");

    if(selItem && operation == UPDATE){
        console.log(selItem);
        $("#txtUN").val(selItem.username);
        $("#txtUN").attr("readonly",true);
        $("#txtFN").val(selItem.firstName);
        $("#txtMN").val(selItem.middleName);
        $("#txtLN").val(selItem.lastName);
        $("#txtCPass").val(selItem.password);
        $("#txtCPass").attr("readonly",true);
        $("#txtPass").val(selItem.password);
        $("#txtPass").attr("readonly",true);

        var dt = new Date(selItem.dateOfBirth);
        if(dt){
            var txtDOB = $("#txtDOB").data("kendoDatePicker");
            if(txtDOB){
                txtDOB.value(dt);
            }
        }

        var txtUserType = $("#txtUserType").data("kendoComboBox");
        if(txtUserType){
            var uType = selItem.userType;
            if(uType == "500"){
                txtUserType.select(0);
            }else if(uType == "600"){
                txtUserType.select(1);
            }else if(uType == "200"){
                txtUserType.select(2);
            }else if(uType == "100"){
                txtUserType.select(3);
            }
        }
        setComboEnable("txtSQ1");
        setComboEnable("txtSQ2");
        setComboEnable("txtSQ3");

        $("#txtSAns1").attr("readonly",true);
        $("#txtSAns2").attr("readonly",true);
        $("#txtSAns3").attr("readonly",true);
        $("#btnUserAvl").hide();

        getAjaxObject(ipAddress+"/user/"+selItem.idk,"GET",onGetUserDetails,onError);

    }
}

var seQArray = [];
function onGetUserDetails(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            $("#txtEmail").val(dataObj.response.user.email);
            $("#txtTZ").val(dataObj.response.user.timezone);

            var seQ = dataObj.response.user.securityAnswers;
            if($.isArray(seQ)){
                seQArray =  seQ;
            }else{
                seQArray.push(seQ);
            }
            seQArray.sort(function(a, b) {
                return parseFloat(a.securityQuestionId) - parseFloat(b.securityQuestionId);
            });
            console.log(seQArray);

            var txtSQ1Idx = getComboListIndex("txtSQ1","id",seQArray[0].securityQuestionId);
            if(txtSQ1Idx>=0){
                var txtSQ1 = $("#txtSQ1").data("kendoComboBox");
                if(txtSQ1){
                    txtSQ1.select(txtSQ1Idx);
                }
            }
            $("#txtSAns1").val(seQArray[0].answer);
            var txtSQ2Idx = getComboListIndex("txtSQ2","id",seQArray[1].securityQuestionId);
            if(txtSQ2Idx>=0){
                var txtSQ2 = $("#txtSQ2").data("kendoComboBox");
                if(txtSQ2){
                    txtSQ2.select(txtSQ2Idx);
                }
            }
            $("#txtSAns2").val(seQArray[1].answer);
            var txtSQ3Idx = getComboListIndex("txtSQ3","id",seQArray[2].securityQuestionId);
            if(txtSQ3Idx>=0){
                var txtSQ3 = $("#txtSQ3").data("kendoComboBox");
                if(txtSQ3){
                    txtSQ3.select(txtSQ3Idx);
                }
            }
            $("#txtSAns3").val(seQArray[2].answer);
        }else{
            customAlert.error("Error", "Unable to get user details");
        }
    }
}

function getSequrityQuestionId(sqId){

}
function setComboEnable(cmbid){
    var cmb = $("#"+cmbid).data("kendoComboBox");
    if(cmb){
        cmb.enable(false);
    }
}
function onQD1Change(){

}
function onClickReset(){
    operation = ADD;
    $("#btnSave").show();
    $("#btnVerify").hide();

    $("#divTop").show();
    $("#divBottom").hide();

    $("#txtSAns3").val("");
    $("#txtSAns2").val("");
    $("#txtSAns1").val("");
    setComboEmpty("txtSQ3");
    setComboEmpty("txtSQ2");
    setComboEmpty("txtSQ1");
    setComboEmpty("cmbDeviceModel");
    $("#txtDeviceUDID").val("");
    $("#txtTZ").val("");
    setComboEmpty("txtUserRole");
    setComboEmpty("txtUserType");
    $("#txtEmail").val("");
    $("#txtLN").val("");
    $("#txtMN").val("");
    $("#txtFN").val("");
    $("#txtCPass").val("");
    $("#txtPass").val("");
    $("#txtUN").val("");
    var txtDOB = $("#txtDOB").data("kendoDatePicker");
    if(txtDOB){
        txtDOB.value("");
    }
    setComboEmpty("cmbGender");
    selItem = null;
}

function setComboEmpty(combo1){
    var cmb = $("#"+combo1).data("kendoComboBox");
    if(cmb){
        cmb.select(0);
    }
}
function validation(){
    var flag = true;
    var strUN = $("#txtUN").val();
    strUN = $.trim(strUN);
    if(strUN == ""){
        customAlert.error("error", "Enter user name");
        return false;
    }
    var strPass = $("#txtPass").val();
    strPass = $.trim(strPass);
    if(strPass == ""){
        customAlert.error("error", "Enter password");
        return false;
    }
    var strCpass = $("#txtCPass").val();
    strCpass = $.trim(strCpass);
    if(strPass != strCpass){
        customAlert.error("error", "Confirm password is not matching");
        return false;
    }
    var sEmail = $('#txtEmail').val();
    sEmail = $.trim(sEmail);
    if(sEmail != ""){
        flag = validateEmail(sEmail);
        if(!flag){
            customAlert.error("Error","your EmailId is invalid,Pleace enter the valid Email");
            return false;
        }
    }else{
        customAlert.error("Error","Please enter email id");
        return false;
    }
    /*if(operation == ADD){
        var txtSQ1 = $("#txtSQ1").data("kendoComboBox")
        var sQuestion1 = txtSQ1.value();

        var txtSQ2 = $("#txtSQ2").data("kendoComboBox")
        var sQuestion2 = txtSQ2.value();

        var txtSQ3 = $("#txtSQ3").data("kendoComboBox")
        var sQuestion3 = txtSQ3.value();

        if((sQuestion1 == sQuestion2) || (sQuestion1 == sQuestion3) || (sQuestion2 == sQuestion3)){
            customAlert.error("Error","Select Different Questions");
            return false;
        }

        var ans1 = $("#txtSAns1").val();
        var ans2 = $("#txtSAns2").val();
        var ans3 = $("#txtSAns3").val();

        ans1 = $.trim(ans1);
        if(ans1 == ""){
            customAlert.error("Error","Enter Question1 answer");
            return false;
        }
        ans2 = $.trim(ans2);
        if(ans2 == ""){
            customAlert.error("Error","Enter Question2 answer");
            return false;
        }
        ans3 = $.trim(ans3);
        if(ans3 == ""){
            customAlert.error("Error","Enter Question3 answer");
            return false;
        }
    }*/
    return flag;
}

function secondValidation(){
    var flag = true;
    var txtSQ1 = $("#txtSQ1").data("kendoComboBox")
    var sQuestion1 = txtSQ1.value();

    var txtSQ2 = $("#txtSQ2").data("kendoComboBox")
    var sQuestion2 = txtSQ2.value();

    var txtSQ3 = $("#txtSQ3").data("kendoComboBox")
    var sQuestion3 = txtSQ3.value();

    if((sQuestion1 == sQuestion2) || (sQuestion1 == sQuestion3) || (sQuestion2 == sQuestion3)){
        customAlert.error("Error","Select Different Questions");
        return false;
    }

    var ans1 = $("#txtSAns1").val();
    var ans2 = $("#txtSAns2").val();
    var ans3 = $("#txtSAns3").val();

    ans1 = $.trim(ans1);
    if(ans1 == ""){
        customAlert.error("Error","Enter Question1 answer");
        return false;
    }
    ans2 = $.trim(ans2);
    if(ans2 == ""){
        customAlert.error("Error","Enter Question2 answer");
        return false;
    }
    ans3 = $.trim(ans3);
    if(ans3 == ""){
        customAlert.error("Error","Enter Question3 answer");
        return false;
    }
    return flag;
}

function onClickSave(){
    if ($('#divBottom').is(':visible')) {
        if(secondValidation()){
            operation = UPDATE;
            createUserDetails();
        }
    }else{
        var sEmail = $('#txtEmail').val();
        if(validation()){
            var strUN = $("#txtUN").val();
            if(strUN != ""){
                if(operation == ADD){
                    var dataUrl = ipAddress+"/user/is-available/"+strUN;
                    getAjaxObject(dataUrl,"GET",getUserCreate,onError);
                }else if(operation == UPDATE && strUN == selItem.username){
                    createUserDetails();
                }else if(operation == UPDATE && strUN != selItem.username){
                    var dataUrl = ipAddress+"/user/is-available/"+strUN;
                    getAjaxObject(dataUrl,"GET",getUserCreate,onError);
                }

            }
        }
    }


    /*$("#divBottom").show();
    $("#divTop").hide();
    $("#btnVerify").show();
    $("#btnSave").hide();*/
}

function getUserCreate(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            createUserDetails();
            //customAlert.error("Info", "User is available");
        }else{
            customAlert.error("Info", "User is not available");
        }
    }
}
function createUserDetails(){
    var strUN = $("#txtUN").val();
    strUN = $.trim(strUN);
    var strPass = $("#txtPass").val();
    strPass = $.trim(strPass);

    var strFN = $.trim($("#txtFN").val());
    var strLN = $.trim($("#txtLN").val());
    var strMN = $.trim($("#txtMN").val());

    var reqObj = {};
    if(operation == ADD){
        reqObj.username = strUN;
        reqObj.password = strPass;
        reqObj.firstName = strFN;
        reqObj.lastName = strLN;
        reqObj.middleName = strMN;
    }else if(operation == UPDATE){
        reqObj.username = strUN;
        reqObj.password = strPass;
        reqObj.firstName = strFN;
        reqObj.lastName = strLN;
        reqObj.middleName = strMN;
    }

//	reqObj.userTypeId = 500;
    if(operation == ADD){
        reqObj.email = $.trim($("#txtEmail").val());
        var txtUserType = $("#txtUserType").data("kendoComboBox");
        reqObj.userTypeId = txtUserType.value();
        var txtUserRole = $("#txtUserRole").data("kendoComboBox");
        reqObj.userrole = txtUserRole.text();
    }
    if(operation == UPDATE){
        reqObj.email = $.trim($("#txtEmail").val());
        var txtUserType = $("#txtUserType").data("kendoComboBox");
        reqObj.userTypeId = txtUserType.value();
        var txtUserRole = $("#txtUserRole").data("kendoComboBox");
        reqObj.userrole = txtUserRole.text();

        var dtItem = $("#txtDOB").data("kendoDatePicker");
        var strDate = "";
        if(dtItem && dtItem.value()){
            strDate = kendo.toString(dtItem.value(),"yyyy-MM-dd");
            reqObj.dateOfBirth = strDate;//"-921218000000";//strDate;
        }
        var cmbGender = $("#cmbGender").data("kendoComboBox");
        reqObj.gender = cmbGender.value();//"1";//cmbGender.value();

        reqObj.timezone = $.trim($("#txtTZ").val());
        reqObj.deviceUDID = $.trim($("#txtDeviceUDID").val());

        var cmbDeviceModel = $("#cmbDeviceModel").data("kendoComboBox");
        //reqObj.deviceModel = cmbDeviceModel.text();

        var sQuesions = [];
        var txtSQ1 = $("#txtSQ1").data("kendoComboBox")
        var sQuestion1 = txtSQ1.value();
        var ans1 = $("#txtSAns1").val();

        var sObj = {};
        sObj.securityQuestionId = sQuestion1;
        sObj.answer = ans1;

        sQuesions.push(sObj);

        var txtSQ2 = $("#txtSQ2").data("kendoComboBox")
        var sQuestion2 = txtSQ2.value();
        var ans2 = $("#txtSAns2").val();

        var sObj1 = {};
        sObj1.securityQuestionId = sQuestion2;
        sObj1.answer = ans2;

        sQuesions.push(sObj1);

        var txtSQ3 = $("#txtSQ3").data("kendoComboBox")
        var sQuestion3 = txtSQ3.value();
        var ans3 = $("#txtSAns3").val();

        var sObj2 = {};
        sObj2.securityQuestionId = sQuestion3;
        sObj2.answer = ans3;

        sQuesions.push(sObj2);
        reqObj.securityAnswers = sQuesions;
    }

    var dataUrl = ipAddress+"/user/create";
    if(operation == UPDATE){
        reqObj.modifiedBy = sessionStorage.userId;
        reqObj.id = uId;
        reqObj.isActive = "1";
        reqObj.isDeleted = "0";
        dataUrl = ipAddress+"/user/update";
    }else{
        reqObj.createdBy = sessionStorage.userId;
    }
    createAjaxObject(dataUrl,reqObj,"POST",onCreate,onError);

}
var uId = "";
function onCreate(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            if(operation == ADD){
                uId = dataObj.response.user.id;
                if ($('#divBottom').is(':visible')) {
                    $("#divBottom").hide();
                    $("#divBottom1").show();
                    $("#btnVerify").show();
                    $("#btnSave").hide();
                }else{
                    $("#divBottom").show();
                    $("#divTop").hide();
                }
                //$("#btnVerify").show();
                //$("#btnSave").hide();
                customAlert.info("info", "User Created Successfully");
            }else{
                customAlert.info("info", "User Updated Successfully");
            }
        }else{
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}

function onClickVerify(){
    var strOTP = $("#txtOPT").val();
    if(strOTP == ""){
        customAlert.info("error", "Enter OTP");
        return;
    }

    getAjaxObject(ipAddress+"/user/is-valid-otp/"+uId+"/"+strOTP,"GET",onIsValidOTP,onError);
}
function onIsValidOTP(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            /*$("#divBottom").hide();
            $("#divTop").show();
            $("#btnVerify").hide();
            $("#btnSave").show();*/
            customAlert.info("info", "OPT Verfied Successfully");
        }else{
            customAlert.info("error", "OPT is wrong");
        }
    }
}
function onError(errObj){
    console.log(errObj);
    customAlert.error("Error","Error");
}
function onClickCancel(){
    var obj = {};
    obj.status = "false";
    popupClose(obj);
}
function onClickSearch(){
    var obj = {};
    obj.status = "search";
    popupClose(obj);
}
function popupClose(obj){
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}



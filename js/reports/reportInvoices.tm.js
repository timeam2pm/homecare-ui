var angularPTUIgridWrapper = null;
var dataArray = [];
var appReport = angular.module('appReport', []);
var allDatesInWeek = [];
var daysOfWeek = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
appReport.controller('reportController', function ($scope) {
	$scope.getData = function () {
		onClickPtViews();
	}
	$scope.printTable = function () {

		// var divToPrint = document.getElementById("carertable");
		//
		// var htmlToPrint = '' +
		// 	'<style type="text/css">' +
		// 	'table , tr, td, th{' +
		// 	'border:1px solid #000' +
		// 	'font-weight:bold' +
		// 	'font-size:13px' +
		// 	'}' +
		// 	'table{' +
		// 	'font-family: arial, sans-serif' +
		// 	'border-collapse: collapse' +
		// 	'width: 100%' +
		// 	';' +
		// 	'padding;0.5em;' +
		// 	'border-collapse:collapse;' +
		// 	'}' +
		// 	'</style>';
		// htmlToPrint += divToPrint.outerHTML;
		//
		// // var newWin = window.open("");
		// // newWin.document.write(htmlToPrint);
		// // newWin.document.close();
		// // newWin.focus();
		// // newWin.print();
		// // newWin.close();
		//
		// // var newWin  = window.open("");
		// // newWin.document.body.innerHTML = htmlToPrint;
		// // newWin.focus();
		// // newWin.print();
		// // newWin.close();
		//
		//
		// window.frames["print_frame"].document.body.innerHTML = htmlToPrint;
		// window.frames["print_frame"].window.focus();
		// window.frames["print_frame"].window.print();

	}
});


$(document).ready(function () {
	$("#divMain", parent.document).css("display", "none");
	$("#divPatInfo", parent.document).css("display", "");
	$("#lblTitleName", parent.document).html("Invoice Report");
});

$(window).load(function () {
	parentRef = parent.frames['iframe'].window;
	onMessagesLoaded();
});
function adjustHeight() {
	var defHeight = 160;
	if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
		defHeight = 100;
	}
	var cmpHeight = window.innerHeight - defHeight;
	cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
	try { angularPTUIgridWrapper.adjustGridHeight(cmpHeight); } catch (e) { };

}
function onMessagesLoaded() {
	buttonEvents();
	init();
}
var cntry = "";
var dtFMT = "dd/MM/yyyy";
var stDate = "";
var endDate = "";

function init() {
	//patientId = parentRef.patientId;
	//$("#dtFromDate").kendoDatePicker({value:new Date()});
	//$("#dtToDate").kendoDatePicker({value:new Date()});

	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth() + 1; //January is 0!
	var yyyy = today.getFullYear();


	getAjaxObject(ipAddress + "/facility/list/?is-active=1", "GET", getFacilityList, onError);

	cntry = sessionStorage.countryName;
	//getFacilityList();
	if (cntry.indexOf("India") >= 0) {
		dtFMT = INDIA_DATE_FMT;
	} else if (cntry.indexOf("United Kingdom") >= 0) {
		dtFMT = ENG_DATE_FMT;
	} else if (cntry.indexOf("United State") >= 0) {
		dtFMT = US_DATE_FMT;
	}

	var currDate = kendo.toString(new Date(), dtFMT);
	$("#txtRunOn").val(currDate);
	$("#txtStartDate").kendoDatePicker({ format: dtFMT });
	$("#txtEndDate").kendoDatePicker({ format: dtFMT });

	startDT = $("#txtStartDate").kendoDatePicker({
		change: startChange, format: dtFMT, value: new Date()
	}).data("kendoDatePicker");

	endDT = $("#txtEndDate").kendoDatePicker({
		change: endChange, format: dtFMT, value: new Date(), max: new Date()
	}).data("kendoDatePicker");

	endDT.min(new Date());

	// $("#txtStartDate").kendoDatePicker({value:new Date()});
	// $("#txtEndDate").kendoDatePicker({value:new Date()});
	// var dataOptionsPT = {
	//         pagination: false,
	//         paginationPageSize: 500,
	// 		changeCallBack: onPTChange
	//     }
	// angularPTUIgridWrapper = new AngularUIGridWrapper("careerGrid1", dataOptionsPT);
	//   angularPTUIgridWrapper.init();
	//  buildPatientRosterList([]);

	// adjustHeight();

	getBillPlans();
	getPatientList();

}

function getPatientList() {

	var facilityId = parseInt($("#cmbFacility option:selected").val());

	if(facilityId > 0){
		var patientListURL = ipAddress + "/patient/list/?is-active=1&facility-id="+facilityId;
		if (sessionStorage.userTypeId == "200" || sessionStorage.userTypeId == "100") {
			patientListURL = ipAddress + "/patient/list/?is-active=1&facility-id="+facilityId;
		}
		getAjaxObject(patientListURL, "GET", onPatientListData, onErrorMedication);
	}
	else{
		var patientListURL = ipAddress + "/patient/list/?is-active=1";
		// patientListURL = ipAddress+"/patient/list/"+sessionStorage.userId;
		if (sessionStorage.userTypeId == "200" || sessionStorage.userTypeId == "100") {
			patientListURL = ipAddress + "/patient/list/?is-active=1";
		}
		//	var patientListURL = ipAddress+"/patient/list/"+sessionStorage.userId;
		getAjaxObject(patientListURL, "GET", onPatientListData, onErrorMedication);
	}

	
}

function onPatientListData(dataObj) {
	$("#cmbServiceUser").empty();
	// console.log(dataObj);
	var tempArray = [];
	var dataArray = [];
	if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
		if (dataObj.response.patient) {
			if ($.isArray(dataObj.response.patient)) {
				tempArray = dataObj.response.patient;
			} else {
				tempArray.push(dataObj.response.patient);
			}
		}
	} else {
		customAlert.error("Error", dataObj.response.status.message.replace('patient', 'service user'));
	}

	if (tempArray !== null && tempArray.length > 0) {


		tempArray.sort(sortBySULastName);
		$("#cmbServiceUser").append('<option value="-1">All</option>')

		for (var i = 0; i < tempArray.length; i++) {
			var obj = tempArray[i];
			if (obj) {
				var dataItemObj = {};
				dataItemObj.PID = obj.id;
				dataItemObj.FN = obj.firstName;
				dataItemObj.LN = obj.lastName;
				dataItemObj.WD = obj.weight;
				dataItemObj.HD = obj.height;
				if (obj.middleName) {
					dataItemObj.MN = obj.middleName;
				} else {
					dataItemObj.MN = "";
				}

				dataArray.push(dataItemObj);

				var SUName = obj.lastName + " " + obj.firstName + " " + obj.middleName;

				$("#cmbServiceUser").append('<option value="' + obj.id + '">' + SUName + '</option>');
			}
		}
	}
}

function onErrorMedication(errobj) {
	console.log(errobj);
}

function getBillPlans() {
	var ipUrl = "";
	ipUrl = ipAddress + "/carehome/bill-to/?is-active=1&is-deleted=0&fields=*,whomToBill.value";
	getAjaxObject(ipUrl, "GET", getBillDataList, onError);
}
function getBillDataList(dataObj) {
	var dataArray = [];
	if (dataObj) {
		if ($.isArray(dataObj.response.billTo)) {
			dataArray = dataObj.response.billTo;
		} else {
			dataArray.push(dataObj.response.billTo);
		}
	}
	if (dataArray.length > 0) {
		dataArray.unshift({ id: "", name: "" });
	}

	dataArray.sort(sortByBillToName);


	for (var i = 0; i < dataArray.length; i++) {
		if (dataArray[i].id && dataArray[i].id > 0 && dataArray[i].name && dataArray[i].name !== "") {
			$("#cmbBillName").append('<option value="' + dataArray[i].id + '">' + dataArray[i].name + '</option>');
		}
	}


	//onClickPtViews();

}
function buildPatientRosterList(dataSource) {
	var gridColumns = [];
	var otoptions = {};
	otoptions.noUnselect = false;
	otoptions.rowHeight = 100;
	gridColumns.push({
		"title": "Carer",
		"field": "carer",
	});
	gridColumns.push({
		"title": "Service User",
		"field": "ServiceUser",
	});
	gridColumns.push({
		"title": "Service User Location",
		"field": "address",
		"width": "25%"
	});
	gridColumns.push({
		"title": "Appointment Time",
		"field": "appTime",
		"width": "15%"
	});
	gridColumns.push({
		"title": "Time In",
		"field": "inTime",
	});
	gridColumns.push({
		"title": "In GPS Location",
		"field": "inLocation",
	});
	gridColumns.push({
		"title": "Time Out",
		"field": "outTime",
	});
	gridColumns.push({
		"title": "Out GPS Location",
		"field": "outLocation",
	});
	gridColumns.push({
		"title": "Actual Visit Time",
		"field": "visitTime",
	});

	angularPTUIgridWrapper.creategrid(dataSource, gridColumns, otoptions);
	adjustHeight();
}
function getReportDataList(dataObj) {
	$("#divPatientAppointments").empty();
	dataArray = [];
	if (dataObj && dataObj.response && dataObj.response.status) {
		if (dataObj.response.status.code == "1") {
			if (dataObj.response.appointment) {
				if ($.isArray(dataObj.response.appointment)) {
					dataArray = dataObj.response.appointment;
				} else {
					dataArray.push(dataObj.response.appointment);
				}
			}
		}
	}
	if (dataArray !== null && dataArray.length > 0) {
		var billTo = $("#cmbBillName option:selected").text();

		dataArray = dataArray.filter(function (itm) {
			if (itm.billToName) {
				return itm.billToName.toLowerCase() === billTo.toLowerCase();
			}
			else {
				return null;
			}
		})
	}

	if (dataArray !== null && dataArray.length > 0) {


		

		var lookup = {};
		var patientIds = [];

		for (var item, c = 0; item = dataArray[c++];) {
			if (item && item.patientId) {

				var patientId = item.patientId;

				if (!(patientId in lookup)) {
					lookup[patientId] = 1;
					patientIds.push(patientId);
				}
			}
		}



		if (patientIds !== null && patientIds.length > 0) {
			var apptArr = [];

			for (var j = 0; j < patientIds.length; j++) {

				var patientAppts = dataArray.filter(function(item){
					return item.patientId === patientIds[j];
				})

				if (patientAppts !== null && patientAppts.length > 0) {
					var pObj = {};
					pObj.patientId = patientIds[j];

					if (patientAppts[0].composition && patientAppts[0].composition.patient && patientAppts[0].composition.patient.firstName && patientAppts[0].composition.patient.lastName) {
						pObj.patientName = patientAppts[0].composition.patient.lastName + " " + patientAppts[0].composition.patient.firstName;
					}

					if (patientAppts[0].composition && patientAppts[0].composition.patient && patientAppts[0].composition.patient.communications && patientAppts[0].composition.patient.communications.length > 0) {
						pObj.address = '';
						if(patientAppts[0].composition.patient.communications[0].address1){
							pObj.address=pObj.address+pObj.address+patientAppts[0].composition.patient.communications[0].address1+', ';
						}
						if(patientAppts[0].composition.patient.communications[0].address2){
							pObj.address=pObj.address+pObj.address+patientAppts[0].composition.patient.communications[0].address2+', ';
						}
						if(patientAppts[0].composition.patient.communications[0].city){
							pObj.address=pObj.address+pObj.address+patientAppts[0].composition.patient.communications[0].city;
						}
						
					}


					var weeklyAppts = [];

					if (allDatesInWeek !== null && allDatesInWeek.length > 0) {
						for (var k = 0; k < allDatesInWeek.length; k++) {
							var apptObj = {};
							var doa = allDatesInWeek[k];
							doa.setHours(0, 0, 0, 0);

							apptObj["DOW Date"] = daysOfWeek[doa.getDay()] + " " + formatDate(doa);

							var filteredappts = patientAppts.filter(function (e) {
								var pdoa = new Date(e.dateOfAppointment);
								pdoa.setHours(0, 0, 0, 0);

								if (doa.getTime() === pdoa.getTime()) {
									return e;
								}
								else {
									return null;
								}
							});

							if (filteredappts !== null && filteredappts.length > 0) {
								var totalDailyHours = 0, hourlyRate = 0, billable = 0;
								var duration = 0;
								for (var x = 0; x < filteredappts.length; x++) {

									if (filteredappts[x].duration) {
										duration = duration + filteredappts[x].duration;
									}

									// if (filteredappts[x].payoutHourlyRate) {
									// 	hourlyRate = hourlyRate + filteredappts[x].payoutHourlyRate;
									// }

									if (filteredappts[x].patientBillingRate) {
                                        hourlyRate = filteredappts[x].patientBillingRate;
										billable = billable + filteredappts[x].patientBillingRate;
									}

									if (duration > 0) {
										totalDailyHours = duration / 60;
									}


								}
								apptObj["Total Day Hours"] = parseFloat(totalDailyHours).toFixed(2);;
								apptObj["Hourly Rate"] = parseFloat(hourlyRate).toFixed(2);;
								apptObj["Billable"] = parseFloat(totalDailyHours*hourlyRate).toFixed(2);
							}
							else {
								apptObj["Total Day Hours"] = 0;
								apptObj["Hourly Rate"] = 0;
								apptObj["Billable"] = 0;
							}

							weeklyAppts.push(apptObj);
						}
					}
					pObj.patientAppts = weeklyAppts;
					apptArr.push(pObj);
				}

			}

			if(apptArr !== null && apptArr.length > 0){

				apptArr.sort(sortByPatientName);

				var billTo = $("#cmbBillName option:selected").text();

				var strBillToHTML = '<table class="tblInvoiceHeader"> <tbody> <tr style="page-break-inside: avoid"> <td> <div class="reportName"> <h2><span style="color:#D5AC57;font-size: 12px;">Bill To</span> <br clear="all"><small style="color:#000000;font-size: 12px;" id="billTo">'+billTo+'</small></h2> </div> </td> </tr> </tbody> </table>';
				$("#divPatientAppointments").append(strBillToHTML);

				for (var y = 0; y < apptArr.length; y++) {

					var strHTML = '';
					
					strHTML = strHTML + '<div class="table-block">';
					strHTML = strHTML + '<div class="timeSheetReportdivHeading">';
	
					strHTML = strHTML + apptArr[y].patientName;
	strHTML = strHTML+'<span class="headingSpn floatRight">'+apptArr[y].address+'</span>';
	
					strHTML = strHTML + '</div></div>';
					strHTML = strHTML + '<div class="col-xs-12 taskReportTblBlock"><div class="table-responsive">';
					strHTML = strHTML + '<table class="tblTaskreport weeklyCareReportTbl"><thead><tr>';


					var cols = ["DOW Date","Total Day Hours","Hourly Rate","Billable"];


					for (var n = 0; n < cols.length; n++) {

						strHTML = strHTML + '<th><span class="spnReportTitle">' + cols[n] + '</th>';
					}
					strHTML = strHTML + '</tr></thead>';
					strHTML = strHTML + '<tbody>';


					var columns = 4;
					var rows = apptArr[y].patientAppts.length;
	
	
					var totalDayHours = 0, totalHourlyRate = 0, totalBillable = 0;


					for (var rownumber = 0; rownumber < rows; rownumber++) {
	
						strHTML = strHTML + '<tr>';

						var tempAppointment = apptArr[y].patientAppts[rownumber];
						strHTML = strHTML + '<td>'+tempAppointment["DOW Date"]+'</td>';
						strHTML = strHTML + '<td>'+tempAppointment["Total Day Hours"]+'</td>';
						strHTML = strHTML + '<td>'+tempAppointment["Hourly Rate"]+'</td>';
						strHTML = strHTML + '<td>'+tempAppointment["Billable"]+'</td>';

						strHTML = strHTML + '</tr>';

						totalDayHours = totalDayHours + tempAppointment["Total Day Hours"];
						totalHourlyRate = totalHourlyRate + tempAppointment["Hourly Rate"];
						totalBillable = totalBillable + tempAppointment["Billable"];

					}

					totalDayHours = parseFloat(totalDayHours).toFixed(2);
					totalHourlyRate = parseFloat(totalHourlyRate).toFixed(2);
					totalBillable = parseFloat(totalBillable).toFixed(2);

					strHTML = strHTML + '<tr class="TotalRow">';
					strHTML = strHTML + '<td>Total</td>';
					strHTML = strHTML + '<td>' + totalDayHours + '</td>';
					strHTML = strHTML + '<td>' + totalHourlyRate + '</td>';
					strHTML = strHTML + '<td>' + totalBillable + '</td>';
					strHTML = strHTML + '</tr>';

					strHTML = strHTML + '</tbody></table></div></div>';

					$("#divPatientAppointments").append(strHTML);
				}
			}
		}
	}
	else{
		customAlert.info("info","Records not found.");
	}

}
function secondsToHms(d) {
	d = Number(d);
	var h = Math.floor(d / 3600);
	var m = Math.floor(d % 3600 / 60);
	var s = Math.floor(d % 3600 % 60);

	var hDisplay = h > 0 ? h + (h == 1 ? " hour, " : " hours, ") : "";
	var mDisplay = m > 0 ? m + (m == 1 ? " minute, " : " minutes, ") : "";
	var sDisplay = s > 0 ? s + (s == 1 ? " second" : " seconds") : "";
	return hDisplay + mDisplay + sDisplay;
}
function onPTChange() {

}
function startChange() {
	var startDate = startDT.value(),
		endDate = endDT.value();

	if (startDate) {
		startDate = new Date(startDate);
		startDate.setDate(startDate.getDate());
		endDT.min(startDate);
	} else if (endDate) {
		startDT.max(new Date(endDate));
	} else {
		endDate = new Date();
		startDT.max(endDate);
		endDT.min(endDate);
	}
}

function endChange() {
	var endDate = endDT.value(),
		startDate = startDT.value();

	if (endDate) {
		endDate = new Date(endDate);
		endDate.setDate(endDate.getDate());
		startDT.max(endDate);
	} else if (startDate) {
		endDT.min(new Date(startDate));
	} else {
		endDate = new Date();
		startDT.max(endDate);
		endDT.min(endDate);
	}
}
function buttonEvents() {
	$("#btnSearch1").off("click", onClickSearch);
	$("#btnSearch1").on("click", onClickSearch);

	$("#btnPtView").off("click", onClickPtViews);
	$("#btnPtView").on("click", onClickPtViews);

	$("#btnPrint").off("click", onClickPrint);
	$("#btnPrint").on("click", onClickPrint);

	$("#cmbFacility").off("change",onChangeFacility);
	$("#cmbFacility").on("change",onChangeFacility);
}
function onClickSearch() {
	var popW = "60%";
	var popH = 500;

	var profileLbl;
	var devModelWindowWrapper = new kendoWindowWrapper();
	profileLbl = "Search Patient";
	if (sessionStorage.clientTypeId == "2") {
		profileLbl = "Search Client";
	}
	devModelWindowWrapper.openPageWindow("../../html/patients/searchPatient.html", profileLbl, popW, popH, true, closePtAddAction);
}
function closePtAddAction(evt, returnData) {
	if (returnData && returnData.status == "success") {
		console.log(returnData);
		var pID = returnData.selItem.PID;
		viewPatientId = pID;
		var pName = returnData.selItem.FN + " " + returnData.selItem.MN + " " + returnData.selItem.LN;
		var viewpName = pName;
		if (pID != "") {
			//$("#lblName").text(pID+" - "+pName);
			$("#txtPatient").val(pName);
			/*var imageServletUrl = ipAddress + "/download/patient/photo/" + pID + "/?" + Math.round(Math.random() * 1000000);
	        $("#imgPhoto").attr("src", imageServletUrl);
	        
	        getAjaxObject(ipAddress + "/patient/" + pID, "GET", onGetPatientInfo, onError);
		}*/
		}
	}
}
function getFacilityList() {
	getAjaxObject(ipAddress + "/facility/list/?is-active=1", "GET", getFacilityList, onError);
}

function onChangeFacility(){
	$("#divPatientAppointments").empty();
	var facilityId = parseInt($("#cmbFacility option:selected").val());

	if(facilityId > 0){
		var patientListURL = ipAddress + "/patient/list/?is-active=1&facility-id="+facilityId;
		if (sessionStorage.userTypeId == "200" || sessionStorage.userTypeId == "100") {
			patientListURL = ipAddress + "/patient/list/?is-active=1&facility-id="+facilityId;
		}
		getAjaxObject(patientListURL, "GET", onPatientListData, onErrorMedication);
	}
	
}

function getFacilityList(dataObj) {
	var dataArray = [];
	if (dataObj) {
		if ($.isArray(dataObj.response.facility)) {
			dataArray = dataObj.response.facility;
		} else {
			dataArray.push(dataObj.response.facility);
		}
	}
	var tempDataArry = [];
	for (var i = 0; i < dataArray.length; i++) {
		dataArray[i].idk = dataArray[i].id;
		dataArray[i].Status = "InActive";
		if (dataArray[i].isActive == 1) {
			dataArray[i].Status = "Active";
		}
		$("#cmbFacility").append('<option value="' + dataArray[i].idk + '">' + dataArray[i].name + '</option>');
	}
}

function onFacilityChange() {

}

function onBillChange() {

}
function onError(err) {

}
function onClickPrint() {
	//angularPTUIgridWrapper.printDataGrid();
	

    if ($.trim($('#divPatientAppointments').html()) !== "") {

        

        var contents = document.getElementById("divPatientAppointments").outerHTML;// $("#divTaskReportsViewer").html();
        var frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({ "position": "absolute", "top": "-1000000px" });
        $("body").append(frame1);
        var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        
        //Create a new HTML document.
        frameDoc.document.write('<html><title>Invoice Week Summary Report</title><head>');
        frameDoc.document.write('<link href="../../css/Theme01.css" rel="stylesheet" type="text/css" />');
        frameDoc.document.write('<link href="../../css/report/invoicestyle.css" rel="stylesheet" type="text/css" />');

		frameDoc.document.write('</head><body style="background-color: #fff;">');
		
		frameDoc.document.write('<div class="container taskComponentContainer"><div class="col-xs-12 timesheetinvoiceBlock"><div class="col-xs-12">');
        //Append the DIV contents.
        frameDoc.document.write(contents);

        frameDoc.document.write('</div></div</div>');
        frameDoc.document.write('</body></html>');


        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();

        }, 500);
    }
    else {
        customAlert.info("info","No data to print.");
    }

}
function onClickPtViews() {
	var startDT = $("#txtStartDate").data("kendoDatePicker");
	var endDT = $("#txtEndDate").data("kendoDatePicker");
	var txtBillName = $("#cmbBillName option:selected");
	var cmbPatientId = Number($("#cmbServiceUser option:selected").val());
	var cmbFacilityId = Number($("#cmbFacility option:selected").val());

	var facilityId = Number($("#cmbFacility option:selected").val());
	var billTo = Number($("#cmbBillName option:selected").text());

	var serviceUserId = Number($("#cmbServiceUser option:selected").val());



	var txtAppPTDate = $("#txtStartDate").data("kendoDatePicker");
	var selectedDate = txtAppPTDate.value();
	var selDate = new Date(selectedDate);
	selDate.setHours(0, 0, 0, 0);



	allDatesInWeek = [];

	for (let i = 1; i <= 7; i++) {
		var first = selDate.getDate() - (selDate.getDay() + 1) + i;
		var day = new Date(selDate.setDate(first));

		allDatesInWeek.push(day);
	}
	var firstDayOfWeek = allDatesInWeek[0];
	var lastDayOfWeek = allDatesInWeek[allDatesInWeek.length - 1];

	var sDate = firstDayOfWeek;
	sDate.setHours(0, 0, 0, 0);
	var eDate = lastDayOfWeek;
	eDate.setHours(23, 59, 59, 999);

	var ipUrl = "";
	// if (txtBillName.text() != "" && cmbPatientId && cmbPatientId > 0 && cmbFacilityId && cmbFacilityId > 0) {
	// 	ipUrl = ipAddress + "/appointment/list/?from-date=" + sDate.getTime() + "&to-date=" + eDate.getTime() + "&patient-id=" + cmbPatientId + "&facility-id=" + cmbFacilityId + "&bill-to-name=" + txtBillName.text();
	// } else {
	// 	ipUrl = ipAddress + "/appointment/list/?from-date=" + sDate.getTime() + "&to-date=" + eDate.getTime();
	// }

	if (facilityId > 0 && serviceUserId > 0) {
		ipUrl = ipAddress + "/appointment/list/?from-date=" + sDate.getTime() + "&to-date=" + eDate.getTime() + "&patient-id=" + serviceUserId + "&facility-id=" + facilityId+"&is-active=1&is-deleted=0";
	} else 	if (facilityId > 0 && serviceUserId === -1) {
		ipUrl = ipAddress + "/appointment/list/?from-date=" + sDate.getTime() + "&to-date=" + eDate.getTime()+"&facility-id=" + facilityId+"&is-active=1&is-deleted=0";
	}


	// buildPatientRosterList([]);
	getAjaxObject(ipUrl, "GET", getReportDataList, onError);
}


function closePopup(id) {
	$('#' + id).hide();
}



function formatDate(date) {

	var dd = date.getDate();

	var mm = date.getMonth() + 1;
	var yyyy = date.getFullYear();
	if (dd < 10) {
		dd = '0' + dd;
	}

	if (mm < 10) {
		mm = '0' + mm;
	}
	btnPrint
	return dd + '/' + mm + '/' + yyyy;
}

function sortByBillToName (a, b) {

    var nameA = a.name.toUpperCase();
    var nameB = b.name.toUpperCase();

    var comparison = 0;
    if (nameA > nameB) {
      comparison = 1;
    } else if (nameA < nameB) {
      comparison = -1;
    }
    return comparison;

}

function sortBySULastName (a, b) {

    var lastNameA = a.lastName.toUpperCase();
    var lastNameB = b.lastName.toUpperCase();

    var comparison = 0;
    if (lastNameA > lastNameB) {
      comparison = 1;
    } else if (lastNameA < lastNameB) {
      comparison = -1;
    }
    return comparison;

}

function sortByPatientName (a, b) {

	var patientNameA = a.patientName.toUpperCase();
    var patientNameB = b.patientName.toUpperCase();

    var comparison = 0;
    if (patientNameA > patientNameB) {
      comparison = 1;
    } else if (patientNameA < patientNameB) {
      comparison = -1;
    }
    return comparison;


}
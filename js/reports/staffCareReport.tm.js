var angularUIgridWrapper;
var parentRef = null;
var recordType = "1";
var dataArray = [];
var ADD = "add";
var UPDATE = "edit";
var VIEW = "view";
var DELETE = "delete";
var operation = "add";
var ds = "";
var IsFlag = 1;
var cntry = sessionStorage.countryName;
var selectPatientId,selProviderId;
var dtFMT = "dd/MM/yyyy";
var allDatesInWeek = [];
var daysOfWeek = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

$(document).ready(function () {
    
    $("#pnlPatient", parent.document).css("display", "none");

    sessionStorage.setItem("IsSearchPanel", "1");
    //themeAPIChange();
    parentRef = parent.frames['iframe'].window;
    // var dataOptions = {
    //     pagination: false,
    //     changeCallBack: onChange
    // }
    $('#cmbStaff').multipleSelect('refresh');
    var cntry = sessionStorage.countryName;
    if((cntry.indexOf("India")>=0) || (cntry.indexOf("United Kingdom")>=0)){
        $("#lblFacility").text("Location");
    }else if(cntry.indexOf("United State")>=0) {
        $("#lblFacility").text("Facility");
    }
    // angularUIgridWrapper = new AngularUIGridWrapper("dgridStaffTimesheets", dataOptions);
    // angularUIgridWrapper.init();
    // buildDeviceListGrid([]);

});


$(window).load(function () {
    loading = false;
    //$(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded() {

    init();
    buttonEvents();
    //adjustHeight();
}

function init() {

    getAjaxObject(ipAddress + "/facility/list/?is-active=1", "GET", getFacilityList, onError);


    $("#txtDateOfWeek").kendoDatePicker({ format: dtFMT, value: new Date() });
}



function onError(errorObj) {
    console.log(errorObj);
}

function buttonEvents() {

    $("#btnSubmit").off("click", onClickViewStaffCareReport);
    $("#btnSubmit").on("click", onClickViewStaffCareReport);

    $("#btnStaffUser").off("click");
    $("#btnStaffUser").on("click", onClickStaff);

    $("#btnPrint").off("click");
    $("#btnPrint").on("click",onClickPrint);

    $("#cmbFacility").change(function() {
        $('#cmbStaff').empty();
        $('#cmbStaff').multipleSelect('refresh');
        var facilityId = $("#cmbFacility").val();
        getAjaxObject(ipAddress + "/provider/list/?facility-id="+facilityId+"&is-active=1&is-deleted=0", "GET", getProvidersList, onError);
    });
}

function onClickPrint(){
    if ($.trim($('#divStaffCareReport').html()) !== "") {

        var facility = $("#cmbFacility option:selected").text();
        var facilityId = $("#cmbFacility").val();
        var communications;
        var communicationsDeatils1, communicationsDeatils2;
        var tempFacilityDataArry = _.where(facilityDataArry, {idk: Number(facilityId)});
        if(tempFacilityDataArry[0].communications && tempFacilityDataArry[0].communications != null) {
            communications = tempFacilityDataArry[0].communications;
            if (communications && communications != null) {
                if (communications[0].address1 != "") {
                    communicationsDeatils1 = communications[0].address1;
                }

                if (communications[0].address2 != "") {
                    communicationsDeatils1 = communicationsDeatils1 + ", " + communications[0].address2;
                }

                if (communications[0].city != "") {
                    communicationsDeatils2 = communications[0].city;
                }

                if (communications[0].state != "") {
                    communicationsDeatils2 = communicationsDeatils2 + ", " + communications[0].state;
                }

                if (communications[0].zip != "") {
                    communicationsDeatils2 = communicationsDeatils2 + ", " + communications[0].zip;
                }
            }
        }

        var contents = document.getElementById("divStaffCareReport").outerHTML;// $("#divTaskReportsViewer").html();
        var frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({ "position": "absolute", "top": "-1000000px" });
        $("body").append(frame1);
        var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        var strDate;
        var DOB = new Date();
        strDate = kendo.toString(DOB, "dd/MM/yyyy hh:mm:ss tt");
        //Create a new HTML document.
        frameDoc.document.write('<html><title>Staff Plan Care Report</title><head>');
        frameDoc.document.write('<link href="../../css/Theme01.css" rel="stylesheet" type="text/css" />');
        frameDoc.document.write('<link href="../../css/report/invoicestyle.css" rel="stylesheet" type="text/css" />');
        frameDoc.document.write('</head><body style="background-color: #fff;">');

        frameDoc.document.write('<table class="tblInvoiceHeader"><tr style="page-break-inside: avoid"><td><div class="reportName"><h2><span style="color:#D5AC57">Staff Plan Care Report</span> <br clear="all"><small style="color:#000000">'+ strDate +'</small></h2></div></td>');
        frameDoc.document.write('<td><div class="reportLocations"><h2><span>'+facility+'<span></span></span></h2><address> '+ communicationsDeatils1 + '<br>'+communicationsDeatils2+'</address></div> </td></tr></table><br clear="all">');

        // frameDoc.document.write('<div class="reportName"><h2><span>Timesheet Report</span> <br clear="all"><small>'+ strDate +'</small></h2></div>');
        // frameDoc.document.write('<div class="reportLocations"><h2><span>'+facility+'<span></span></span></h2><address> '+ communicationsDeatils1 + '<br>'+communicationsDeatils2+'</address></div> <br clear="all">');

        frameDoc.document.write('<div class="searchedLabels">');
        frameDoc.document.write('<div class="divLabel"><span>Location :</span> <label>'+facility+'</label> <span>Staff :</span> <label>'+printStaffName.slice(0,-1)+'</label></div>');
        frameDoc.document.write('</div>');

        frameDoc.document.write('<div class="timesheetinvoiceBlock">');
        
        //Append the DIV contents.
        frameDoc.document.write(contents);

        frameDoc.document.write('</div>');
       //frameDoc.document.write('</div> </div> </div>');
        frameDoc.document.write('</body></html>');


        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();

        }, 500);
    }
    else {
        customAlert.info("info","No data to print.");
    }
}




function adjustHeight() {
    var defHeight = 280;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    if(angularUIgridWrapper) {
        angularUIgridWrapper.adjustGridHeight(cmpHeight);
    }
}

function buildDeviceListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    otoptions.rowHeight = 100;
    gridColumns.push({
        "title": "Staff",
        "field": "Provider"
    });
    // gridColumns.push({
    //     "title": "Weekly Contract Hours",
    //     "field": "weeklyContractHours"
    // });

    gridColumns.push({
        "title": "Appointment Date Time",
        "field": "dateTimeOfAppointment"
    });
    /*gridColumns.push({
        "title": "DOW",
        "field": "dow"
    });*/
    gridColumns.push({
        "title": "Scheduled Hrs",
        "field": "durationTime"
    });

    gridColumns.push({
        "title": "Actual Hrs",
        "field": "actualDurationTime"
    });

    // if ($("#rbtnDetail").is(":checked")) {
    //     gridColumns.push({
    //         "title": "Comments",
    //         "field": "notes"
    //     });
    // }

    angularUIgridWrapper.creategrid(dataSource, gridColumns, otoptions);
    // adjustHeight();
}

function onChange() {
    setTimeout(function () {
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        if (selectedItems && selectedItems.length > 0) {

            if ($("#rbtnDetail").is(":checked")) {
                $("#btnEdit").prop("disabled", false);
            }
            else {
                $("#btnEdit").prop("disabled", true);
            }

            //$("#btnEdit").prop("disabled", false);
        } else {
            $("#btnEdit").prop("disabled", true);
        }
    }, 100);
}
var operation = "add";
var selFileResourceDataItem = null;

function onStatusChange() {
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if (cmbStatus && cmbStatus.selectedIndex < 0) {
        cmbStatus.select(0);
    }
}

function onClickReset() {
    $("#txtReason").val("");
    $("#txtFDate").val("");
    $("#txtTDate").val("");
    operation = ADD;
}

function themeAPIChange() {
    getAjaxObject(ipAddress + "/homecare/settings/?id=2", "GET", getThemeValue, onError);
}

function getThemeValue(dataObj) {
    console.log(dataObj);
    var tempCompType = [];
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.activityTypes) {
            if ($.isArray(dataObj.response.settings)) {
                tempCompType = dataObj.response.settings;
            } else {
                tempCompType.push(dataObj.response.settings);
            }
        }
        if (tempCompType.length > 0) {
            themesChange(tempCompType[0].value);
        }
    }
}

function themesChange(themeValue) {
    if (themeValue == 2) {
        loadAPi("../../Theme2/Theme02.css");
    }
    else if (themeValue == 3) {
        loadAPi("../../Theme3/Theme03.css");
    }
}

var facilityDataArry = [];

function getFacilityList(dataObj) {
    var dataArray = [];
    if (dataObj) {
        if ($.isArray(dataObj.response.facility)) {
            dataArray = dataObj.response.facility;
        } else {
            dataArray.push(dataObj.response.facility);
        }
    }
    facilityDataArry = dataArray;
    for (var i = 0; i < dataArray.length; i++) {
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if (dataArray[i].isActive == 1) {
            dataArray[i].Status = "Active";
        }
        $("#cmbFacility").append('<option value="' + dataArray[i].idk + '">' + dataArray[i].name + '</option>');
    }

    var facilityId = $("#cmbFacility").val();
    getAjaxObject(ipAddress + "/provider/list/?facility-id="+facilityId+"&is-active=1&is-deleted=0", "GET", getProvidersList, onError);
}

function onClickServiceUser() {
    var popW = 800;
    var popH = 450;

    if (parentRef)
        parentRef.searchZip = true;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Service User";
    devModelWindowWrapper.openPageWindow("../../html/patients/searchPatient.html", profileLbl, popW, popH, true, onCloseServiceUserAction);
}
function onCloseServiceUserAction(evt, returnData) {
    if (returnData && returnData.status == "success") {
        selectPatientId = returnData.selItem.PID;
        $('#txtSU').val(returnData.selItem.LN + " " + returnData.selItem.FN + " " + returnData.selItem.MN);
    }
}

function onClickStaff(){
    var popW = 800;
    var popH = 450;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Staff";
    devModelWindowWrapper.openPageWindow("../../html/patients/staffList.html", profileLbl, popW, popH, true, onCloseStaffAction);
}

function onCloseStaffAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        selProviderId = returnData.selItem.PID;
        $('#txtStaffUser').val(returnData.selItem.LN+' '+returnData.selItem.FN);

    }
}

function onClickSubmit() {
    if ($("#txtSU").val() != "") {
        if ($("#txtFDate").val() !== "") {
            var txtAppPTDate = $("#txtFDate").data("kendoDatePicker");
            var selectedDate = txtAppPTDate.value();
            var selDate = new Date(selectedDate);


            // var selectedDate = GetDateTime("txtDate");


            for (let i = 1; i <= 7; i++) {
                var first = selectedDate.getDate() - (selectedDate.getDay() + 1) + i;
                var day = new Date(selectedDate.setDate(first));

                allDatesInWeek.push(day);
            }
            var day = selDate.getDate();
            var month = selDate.getMonth();
            month = month + 1;
            var year = selDate.getFullYear();

            var stDate = month + "/" + day + "/" + year;
            stDate = stDate + " 00:00:00";

            var startDate = new Date(stDate);
            var stDateTime = startDate.getTime();

            var etDate = month + "/" + day + "/" + year;
            etDate = etDate + " 23:59:59";

            var endtDate = new Date(etDate);
            var edDateTime = endtDate.getTime();

            var startDate = new Date(stDate);
            var endtDate = new Date(etDate);

            var day = startDate.getDate() - startDate.getDay();
            var pDate = new Date(startDate);
            pDate.setDate(day);

            endtDate.setDate(day + 6);
            var stDateTime = pDate.getTime();
            var edDateTime = endtDate.getTime();
            var txtAppPTFacility = $("#cmbFacility").val();
            parentRef.facilityId = txtAppPTFacility;
            parentRef.patientId = selectPatientId;
            parentRef.SUName = $("#txtSU").val();
            parentRef.facilityName = $("#cmbFacility option:selected").text();
            parentRef.selDate = pDate;
            parentRef.copyDate = txtAppPTDate.value();

            buildDeviceListGrid([]);

            var patientListURL = ipAddress + "/appointment/list/?facility-id=" + txtAppPTFacility + "&patient-id=" + selectPatientId + "&to-date=" + edDateTime + "&from-date=" + stDateTime + "&is-active=1";

            getAjaxObject(patientListURL, "GET", onPatientListData, onError);

        } else {
            customAlert.error("Error", "Please select date");
        }
    }
    else {
        customAlert.error("Error", "Please select service user");
    }
}

function onClickViewStaffCareReport() {

    $("#divStaffCareReport").html("");

    var strMessage;
    var isValid = true;
    if ($("#cmbFacility").val() === 0) {
        strMessage = "Please select facility.";
        isValid = false;
    }
    var providerIds ;
    var providerIds = $('#cmbStaff').multipleSelect('getSelects');
    if (providerIds == null || providerIds.length == 0) {
        strMessage = "Please select staff.";
        isValid = false;
    }

    if(isValid) {
        var facilityId = $("#cmbFacility").val();

        var apiURL = ipAddress + "/patient/roster/list/?facility-id="+facilityId+"&is-active=1&is-deleted=0&provider-id="+providerIds.join(',');

        // var apiURL = ipAddress + "patient/roster/list/?facility-id=4&provider-id=60&patient-id=485&is-active=1";
        getAjaxObject(apiURL, "GET", onGetStaffCareReport1, onError);


    }
    else{
        customAlert.error("info",strMessage);
    }
}



function onGetStaffCareReport1(dataObj) {
    dtArray = [];
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.patientRoster) {
            if ($.isArray(dataObj.response.patientRoster)) {
                dtArray = dataObj.response.patientRoster;
            } else {
                dtArray.push(dataObj.response.patientRoster);
            }
        }
    }

    if (dtArray !== null && dtArray.length > 0) {

        var txtDateOfWeek = $("#txtDateOfWeek").data("kendoDatePicker");
        var selectedDate = txtDateOfWeek.value();

        var arrDatesInWeek = [];

        for (var i = 1; i <= 7; i++) {
            var first = selectedDate.getDate() - (selectedDate.getDay() + 1) + i;
            var day = new Date(selectedDate.setDate(first));

            arrDatesInWeek.push(day);
        }

        var allDaysInWeek = [];
        for (var counter4 = 0; counter4 < arrDatesInWeek.length; counter4++) {
            allDaysInWeek.push(daysOfWeek[arrDatesInWeek[counter4].getDay()] + " " + formatDate(arrDatesInWeek[counter4]));
        }

        var appointments = [];

        var lookup = {};
        var providerIds = [];

        for (var item, c = 0; item = dtArray[c++];) {
            if (item && item.providerId) {

                var providerId = item.providerId;

                if (!(providerId in lookup)) {
                    lookup[providerId] = 1;
                    providerIds.push(providerId);
                }
            }
        }

        var providerAppointments = [];

        if (providerIds !== null && providerIds.length > 0) {

            for (var j = 0; j < providerIds.length; j++) {

                // var providerObj = {};
                var patientObjpush = [];
                var tmpStaffAppts = $.grep(dtArray, function (e) {
                    return e.providerId === providerIds[j];
                });

                if (tmpStaffAppts !== null && tmpStaffAppts.length > 0) {

                    if (tmpStaffAppts[0] && tmpStaffAppts[0].provider) {
                        patientObjpush.id = tmpStaffAppts[0].providerId;
                        patientObjpush.providerName = tmpStaffAppts[0].provider;
                    }

                    var staffWeekIdAppts = [];

                    for(var w = 1; w < tmpStaffAppts.length; w++) {

                        if (w <= 6) {
                            var tmpStaffWeekIdAppts = $.grep(tmpStaffAppts, function (e) {
                                return e.weekId == w;
                            });

                            if (tmpStaffWeekIdAppts != null && tmpStaffWeekIdAppts.length > 0) {

                                var objWeek = {};
                                var providerObj = {};
                                providerObj.weekId = w;
                                var staffWeeklyAppts = [];

                                for (var counter = 0; counter < arrDatesInWeek.length; counter++) {

                                    var obj = {};

                                    var tmpDate = arrDatesInWeek[counter];
                                    tmpDate.setHours(0, 0, 0, 0);


                                    var tmpAppointments = $.grep(tmpStaffWeekIdAppts, function (e) {
                                        var apptDate = new Date();
                                        e.dateOfAppointment = apptDate.setHours(0, e.fromTime, 0, 0);
                                        return counter == e.weekDayStart;
                                    });

                                    obj.dateOfAppointment = allDaysInWeek[counter];
                                    obj.dayAppointments = [];


                                    if (tmpAppointments != null && tmpAppointments.length > 0) {
                                        obj.dayAppointments = tmpAppointments;

                                    }
                                    staffWeeklyAppts.push(obj);
                                }
                                providerObj.weeklyAppointments = staffWeeklyAppts;
                                patientObjpush.push(providerObj);
                            }
                        }
                    }
                    //providerObj.weeklyAppointments = staffWeeklyAppts;
                }

                providerAppointments.push(patientObjpush);
            }
            bindStaffCareReport(providerAppointments);
        }

    }
    else {
        customAlert.info("Info", "No records found");
    }
}



function onGetStaffCareReport(dataObj) {
    
    dtArray = [];
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.appointment) {
            if ($.isArray(dataObj.response.patientRoster)) {
                dtArray = dataObj.response.patientRoster;
            } else {
                dtArray.push(dataObj.response.patientRoster);
            }
        }
    }

    if (dtArray !== null && dtArray.length > 0) {

        var txtDateOfWeek = $("#txtDateOfWeek").data("kendoDatePicker");
        var selectedDate = txtDateOfWeek.value();

        var arrDatesInWeek = [];

        for (var i = 1; i <= 7; i++) {
            var first = selectedDate.getDate() - (selectedDate.getDay() + 1) + i;
            var day = new Date(selectedDate.setDate(first));

            arrDatesInWeek.push(day);
        }

        var allDaysInWeek = [];
        for (var counter4 = 0; counter4 < arrDatesInWeek.length; counter4++) {
            allDaysInWeek.push(daysOfWeek[arrDatesInWeek[counter4].getDay()] + " " + formatDate(arrDatesInWeek[counter4]));
        }

        var appointments = [];

        var lookup = {};
        var patientIds = [];

        for (var item, c = 0; item = dtArray[c++];) {
            if (item.composition && item.composition.patient && item.composition.patient.id) {

                var patientId = item.composition.patient.id;

                if (!(patientId in lookup)) {
                    lookup[patientId] = 1;
                    patientIds.push(patientId);
                }
            }
        }


        var patientAppointments = [];

        if (patientIds !== null && patientIds.length > 0) {

            for (var j = 0; j < patientIds.length; j++) {

                var patientObj = {};
                var patientObjpush = [];

                var tmpSUAppts = $.grep(dtArray, function (e) {
                    return e.composition.patient.id === patientIds[j];
                });

                if (tmpSUAppts !== null && tmpSUAppts.length > 0) {
                    if (tmpSUAppts[0].composition && tmpSUAppts[0].composition.patient && tmpSUAppts[0].composition.patient.id) {
                        patientObj.id = tmpSUAppts[0].composition.patient.id;
                        patientObj.patientName = tmpSUAppts[0].composition.patient.lastName + " " + tmpSUAppts[0].composition.patient.firstName;
                    }

                    if (tmpSUAppts[0].composition && tmpSUAppts[0].composition.patient && tmpSUAppts[0].composition.patient.dateOfBirth) {

                        var dateArr = tmpSUAppts[0].composition.patient.dateOfBirth.split("-");
                        var dob = parseInt(dateArr[1]) + "-" + parseInt(dateArr[2]) + "-" + parseInt(dateArr[0]);
                        var DOB = new Date(dob);
                        var today = new Date();
                        var age = today.getTime() - DOB.getTime();
                        age = Math.floor(age / (1000 * 60 * 60 * 24 * 365.25));

                        patientObj.age = age;

                    }
                    if (tmpSUAppts[0].composition && tmpSUAppts[0].composition.patient && tmpSUAppts[0].composition.patient.gender) {
                        patientObj.gender = tmpSUAppts[0].composition.patient.gender;
                    }

                    var suWeeklyAppts = [];


                    for (var counter = 0; counter < arrDatesInWeek.length; counter++) {

                        var obj = {};

                        var tmpDate = arrDatesInWeek[counter];
                        tmpDate.setHours(0, 0, 0, 0);


                        var tmpAppointments = $.grep(tmpSUAppts, function (e) {
                            var apptDate = new Date(e.dateOfAppointment);
                            apptDate.setHours(0, 0, 0, 0);
                            return apptDate.getTime();
                        });

                        obj.dateOfAppointment = allDaysInWeek[counter];
                        obj.dayAppointments = [];
                        

                        if (tmpAppointments != null && tmpAppointments.length > 0) {
                            obj.dayAppointments = tmpAppointments;
                        }

                        suWeeklyAppts.push(obj);

                    }
                    patientObj.weeklyAppointments = suWeeklyAppts;
                }

                patientAppointments.push(patientObj);
            }
            console.log(JSON.stringify(patientAppointments));
            bindStaffCareReport(patientAppointments);
        }

        
    }
    else {
        customAlert.info("Info", "No records found");
    }
}

var printStaffName = '';
var providerTotalHrs;
function bindStaffCareReport(providerAppointments) {
    providerTotalHrs = '';
    sessionStorage.providerTotalHrs = '';
    $("#divStaffCareReport").html("");
    if (providerAppointments !== null && providerAppointments.length > 0) {
        var uniques = _.map(_.groupBy(providerAppointments,function(doc){
            return doc.id;
        }),function(grouped){
            return grouped[0];
        });

        for (var m = 0; m < uniques.length; m++) {
            providerTotalHrs = 0;
            var pObj = uniques[m];

            var objStaff = _.where(staffArray, {idk: Number(pObj.id)});
            var dt;
            if (objStaff && objStaff.length > 0) {
                if (objStaff[0].dateOfBirth && objStaff[0].dateOfBirth != null && objStaff[0].dateOfBirth != "") {
                    dt = new Date(objStaff[0].dateOfBirth);
                } else {
                    dt = new Date();
                }

                //var strDate = onGetDate(dataArray[i].dateOfBirth) + " (" + getAge(dt) + " Y)";

                var strHTML = '';

                strHTML = strHTML + '<div class="table-block">';
                strHTML = strHTML + '<div class="timeSheetReportdivHeading">';
                printStaffName = printStaffName + objStaff[0].lastName + " " + objStaff[0].firstName + ",";
                strHTML = strHTML + "#" + pObj.id + " - " + objStaff[0].lastName + " " + objStaff[0].firstName;
                strHTML = strHTML + '<span class="headingSpn floatRight">';
                var gender = (objStaff[0].gender != null) ? objStaff[0].gender.charAt(0) : "";
                strHTML = strHTML + '(' + getAge(dt) + 'Y/' + gender + ') ' + objStaff[0].language + '</span><div class="clearLine"></div>';
                strHTML = strHTML + '<span class="headingSpn floatLeft" id="spWeekly' + pObj.id + '"></span>';
                if (objStaff[0].communications && objStaff[0].communications[0]) {
                    strHTML = strHTML + '<span class="headingSpn floatRight">';
                    var Add;
                    if (objStaff[0].communications[0].address1 && objStaff[0].communications[0].address1 != '') {
                        Add = objStaff[0].communications[0].address1 + ',';
                    }

                    if (objStaff[0].communications[0].address2 && objStaff[0].communications[0].address2 != '') {
                        Add = Add + objStaff[0].communications[0].address2 + ',';
                    }

                    if (objStaff[0].communications[0].city && objStaff[0].communications[0].city != '') {
                        Add = Add + objStaff[0].communications[0].city + ',';
                    }

                    if (objStaff[0].communications[0].zip && objStaff[0].communications[0].zip != '') {
                        Add = Add + objStaff[0].communications[0].zip + ',';
                    }

                    if (objStaff[0].communications[0].homePhone && objStaff[0].communications[0].homePhone != '') {
                        Add = Add + objStaff[0].communications[0].homePhone + ',';
                    }

                    if (objStaff[0].communications[0].workPhone && objStaff[0].communications[0].workPhone != '') {
                        Add = Add + objStaff[0].communications[0].workPhone + ',';
                    }

                    if (objStaff[0].communications[0].email && objStaff[0].communications[0].email != '') {
                        Add = Add + objStaff[0].communications[0].email + ',';
                    }
                    Add = Add.slice(0, -1);
                }

                strHTML = strHTML + Add + '</div></div>';
                strHTML = strHTML + '<div class="col-xs-12 taskReportTblBlock"><div class="table-responsive">';
               //strHTML = strHTML + '<table class="tblTaskreport weeklyCareReportTbl"><thead><tr>';
                var weeklyHTML = "";
                var weeklyDuration = [];
                for (var i = 0; i < pObj.length; i++) {
                    var arrAppointmentsCount = [];
                    weeklyHTML = weeklyHTML + '<table class="tblTaskreport weeklyCareReportTbl"><thead><tr>';
                    for (var j = 0; j < pObj[i].weeklyAppointments.length; j++) {

                        weeklyHTML = weeklyHTML + '<th><span class="spnReportTitle">' + pObj[i].weeklyAppointments[j].dateOfAppointment.split(' ')[0] + '</th>';
                        arrAppointmentsCount.push(pObj[i].weeklyAppointments[j].dayAppointments.length);
                    }
                    weeklyHTML = weeklyHTML + '<span class="headingSubSpn">Week : ' + pObj[i].weekId + '</span><span class="headingSubSpn floatRight" id="spWeekId' +pObj.id+ pObj[i].weekId + '"></span></tr></thead>';
                    weeklyHTML = weeklyHTML + '<tbody>';

                    var columns = 7;
                    var rows = parseInt(Math.max(...arrAppointmentsCount));

                    var dayWiseDuration = {};

                    for (var rownumber = 0; rownumber < rows; rownumber++) {

                        weeklyHTML = weeklyHTML + '<tr>';

                        for (var columnnumber = 0; columnnumber < columns; columnnumber++) {

                            var tempAppointment = pObj[i].weeklyAppointments[columnnumber].dayAppointments[rownumber];

                            var currentDay = pObj[i].weeklyAppointments[columnnumber].dateOfAppointment.split(' ')[0];

                            if (!(currentDay in dayWiseDuration)) {
                                dayWiseDuration[currentDay] = 0;
                            }


                            var apptTimeDuration = "";

                            if (tempAppointment !== null && typeof (tempAppointment) !== "undefined") {

                                var appDate = new Date(tempAppointment.dateOfAppointment);

                                var strH = kendo.toString(appDate, "hh");
                                var strm = kendo.toString(appDate, "mm");
                                var stT = kendo.toString(appDate, "tt");

                                var time = strH;
                                if (strm.length > 0) {
                                    time = time + ":" + strm;
                                }
                                time = time + stT;
                                time = time.toLowerCase();

                                apptTimeDuration = time + '(' + tempAppointment.duration + ')' + '<br/>' + tempAppointment.patient;

                                dayWiseDuration[currentDay] = dayWiseDuration[currentDay] + parseInt(tempAppointment.duration);

                            }


                            weeklyHTML = weeklyHTML + '<td>' + apptTimeDuration + '</td>';

                        }

                        weeklyHTML = weeklyHTML + '</tr>';
                    }
                    var durationInWeek = 0;
                    if (dayWiseDuration) {
                        weeklyHTML = weeklyHTML + '<tr class="TotalRow">';
                        for (key in dayWiseDuration) {
                            var totalHours = convertMinsToHrsMins(dayWiseDuration[key]);
                            totalHours = totalHours === '' ? '00:00' : totalHours;
                            providerTotalHrs = Number(providerTotalHrs) + Number(dayWiseDuration[key]);
                            durationInWeek = Number(durationInWeek) + Number(dayWiseDuration[key]);
                            weeklyHTML = weeklyHTML + '<td>' + totalHours + '</td>';
                        }
                        weeklyHTML = weeklyHTML + '</tr>';

                    }
                    weeklyDuration.push({
                        weekId : pObj[i].weekId,
                        duration : durationInWeek
                    })
                    weeklyHTML = weeklyHTML + '</tbody></table>';

                }
                strHTML = strHTML + weeklyHTML +'</div></div></div>';
                $("#divStaffCareReport").append(strHTML);
                for(var w = 0 ; w < weeklyDuration.length ; w++){
                    $("#spWeekId"+pObj.id+ weeklyDuration[w].weekId).html("Weekly Planned Hours: " + convertMinsToHrsMins(weeklyDuration[w].duration));
                }
                $("#spWeekly" + pObj.id).html("Total Weeks Planned Hours: " + convertMinsToHrsMins(providerTotalHrs));
            }
        }

    }


}

    function onStaffTimesheet(dataObj) {
        dtArray = [];
        if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
            if (dataObj.response.staffTimesheets) {
                if ($.isArray(dataObj.response.staffTimesheets)) {
                    dtArray = dataObj.response.staffTimesheets;
                } else {
                    dtArray.push(dataObj.response.staffTimesheets);
                }
            }
        }
        if (dtArray && dtArray.length > 0) {

            console.log(dtArray);

            if ($("#rbtnOld").is(":checked")) {


                var staffTimeSheetSummary = [];


                dtArray.sort(function (x, y) {
                    var date1 = new Date(x);
                    var date2 = new Date(y);

                    if (date1 > date2) return 1;
                    if (date1 < date2) return -1;
                    return 0;
                });

                var totalScheduledHours = 0, totalActualHours = 0;

                for (var counter = 0; counter <= dtArray.length - 1; counter++) {
                    if (counter == 0 && dtArray[counter].firstName !== null && dtArray[counter].firstName !== "" && dtArray[counter].lastName !== null && dtArray[counter].lastName !== "") {
                        dtArray[counter].ProviderName = dtArray[counter].lastName + " " + dtArray[counter].firstName;
                    }

                    if (dtArray[counter].weeklyContractHours === null) {
                        dtArray[counter].weeklyContractHours = 0;
                    }


                    if (dtArray[counter].hourlyRate === null) {
                        dtArray[counter].hourlyRate = 0;
                    }

                    if (dtArray[counter].dateOfAppointment) {
                        var date = new Date(GetDateTimeEditDay(dtArray[counter].dateOfAppointment));
                        var day = date.getDay();
                        var dow = getWeekDayName(day);

                        dtArray[counter].dateTimeOfAppointment = dow + " " + GetDateTimeEdit(dtArray[counter].dateOfAppointment);
                        var dtEnd = dtArray[counter].dateOfAppointment + (Number(dtArray[counter].duration) * 60000);
                        dtArray[counter].DW = dow;
                        var dateED = new Date(GetDateTimeEditDay(dtEnd));
                        var dayED = dateED.getDay();
                        var dowED = getWeekDayName(dayED);
                        dtArray[counter].dateTimeOfAppointmentED = GetDateTimeEdit(dtEnd);
                        dtArray[counter].dow = dowED;

                    }

                    if (dtArray[counter].duration) {
                        var totalMinutes = dtArray[counter].duration;

                        var hours = Math.floor(totalMinutes / 60);
                        var minutes = Math.round(totalMinutes % 60);
                        if (hours.toString().length == 1) {
                            hours = "0" + hours;
                        }
                        if (minutes.toString().length == 1) {
                            minutes = "0" + minutes;
                        }


                        dtArray[counter].durationTime = hours + ":" + minutes;
                        totalScheduledHours = totalScheduledHours + dtArray[counter].duration;
                    }
                    if (dtArray[counter].actualDuration) {
                        var totalMinutes = dtArray[counter].actualDuration;

                        var hours = Math.floor(totalMinutes / 60);
                        var minutes = Math.round(totalMinutes % 60);
                        if (hours.toString().length == 1) {
                            hours = "0" + hours;
                        }
                        if (minutes.toString().length == 1) {
                            minutes = "0" + minutes;
                        }


                        dtArray[counter].actualDurationTime = hours + ":" + minutes;
                        totalActualHours = totalActualHours + dtArray[counter].actualDuration;
                    }
                    if (counter == 0) {
                        dtArray[counter].Provider = dtArray[counter].ProviderName + '- ' + dtArray[counter].weeklyContractHours;
                    }
                }


                var strTotalScheduledHours = "", strTotalActualHours = "";
                if (totalScheduledHours) {
                    var hours = Math.floor(totalScheduledHours / 60);
                    var minutes = Math.round(totalScheduledHours % 60);
                    if (hours.toString().length == 1) {
                        hours = "0" + hours;
                    }
                    if (minutes.toString().length == 1) {
                        minutes = "0" + minutes;
                    }

                    strTotalScheduledHours = hours + ":" + minutes;
                }

                if (totalActualHours) {
                    var hours = Math.floor(totalActualHours / 60);
                    var minutes = Math.round(totalActualHours % 60);
                    if (hours.toString().length == 1) {
                        hours = "0" + hours;
                    }
                    if (minutes.toString().length == 1) {
                        minutes = "0" + minutes;
                    }

                    strTotalActualHours = hours + ":" + minutes;
                }


                var objTotal = {};
                objTotal.ProviderName = "";
                objTotal.weeklyContractHours = "";
                objTotal.dateTimeOfAppointment = "";
                objTotal.dow = "Total";
                objTotal.durationTime = strTotalScheduledHours;
                objTotal.actualDurationTime = strTotalActualHours;

                dtArray.push(objTotal);
                buildDeviceListGrid(dtArray);
                $("#divOldTimeSheetReportTableBlock").show();
            } else {

                var reportViewerJson = [];
                var lookup = {};
                var providerIds = [];
                var grandTotalScheduledHours = 0, grandTotalActualHours = 0;

                for (var item, c = 0; item = dtArray[c++];) {
                    var providerId = item.providerId;

                    if (!(providerId in lookup)) {
                        lookup[providerId] = 1;
                        providerIds.push(providerId);
                    }
                }

                debugger;

                if (providerIds !== null && providerIds.length > 0) {
                    for (var i = 0; i < providerIds.length; i++) {
                        var report = {};
                        console.log(providerIds[i]);
                        var staffAppts = $.grep(dtArray, function (e, index) {

                            return Number(e.providerId) === Number(providerIds[i]);
                        });


                        if (staffAppts !== null && staffAppts.length > 0) {

                            if (staffAppts[0].providerFirstName !== null && staffAppts[0].providerFirstName !== "" && staffAppts[0].providerLastName !== null && staffAppts[0].providerLastName) {
                                report.ProviderName = staffAppts[0].providerFirstName + " " + staffAppts[0].providerLastName;

                                if (report.ProviderName.toLowerCase() === "u a") {
                                    report.ProviderName = "Unallocated Appointments";
                                }

                            }

                            report.dataSource = [];
                            var duration = 0, actualDuration = 0;
                            for (var j = 0; j < staffAppts.length; j++) {
                                var tmpStaffAppt = {};

                                if (staffAppts[j].patientFirstName !== null && staffAppts[j].patientFirstName !== "" && staffAppts[j].patientLastName !== null && staffAppts[j].patientLastName) {
                                    tmpStaffAppt.ServiceUser = staffAppts[j].patientFirstName + " " + staffAppts[j].patientLastName;
                                }


                                var dateTimeOfAppointment;
                                if (staffAppts[j].dateOfAppointment) {
                                    var date = new Date(GetDateTimeEditDay(staffAppts[j].dateOfAppointment));
                                    var day = date.getDay();
                                    var dow = getWeekDayName(day);

                                    dateTimeOfAppointment = dow + " " + GetDateTimeEdit(staffAppts[j].dateOfAppointment);

                                }
                                tmpStaffAppt.dateTimeOfAppointment = dateTimeOfAppointment;


                                if (staffAppts[j].duration !== null) {
                                    duration = duration + staffAppts[j].duration;
                                }

                                if (staffAppts[j].actualDuration !== null) {
                                    actualDuration = actualDuration + staffAppts[j].actualDuration;
                                }

                                if (staffAppts[j].duration) {

                                    grandTotalScheduledHours = grandTotalScheduledHours + staffAppts[j].duration;

                                    var totalScheduledMinutes = staffAppts[j].duration;

                                    var hoursScheduled = Math.floor(totalScheduledMinutes / 60);
                                    var minutesScheduled = Math.round(totalScheduledMinutes % 60);
                                    if (hoursScheduled.toString().length == 1) {
                                        hoursScheduled = "0" + hoursScheduled;
                                    }
                                    if (minutesScheduled.toString().length == 1) {
                                        minutesScheduled = "0" + minutesScheduled;
                                    }


                                    tmpStaffAppt.ScheduledHours = hoursScheduled + ":" + minutesScheduled;
                                } else {
                                    tmpStaffAppt.ScheduledHours = "00:00";
                                }

                                if (staffAppts[j].actualDuration) {
                                    grandTotalActualHours = grandTotalActualHours + staffAppts[j].actualDuration;

                                    var totalActualMinutes = staffAppts[j].actualDuration;

                                    var actualHours = Math.floor(totalActualMinutes / 60);
                                    var actualMinutes = Math.round(totalActualMinutes % 60);
                                    if (actualHours.toString().length == 1) {
                                        actualHours = "0" + actualHours;
                                    }
                                    if (actualMinutes.toString().length == 1) {
                                        actualMinutes = "0" + actualMinutes;
                                    }


                                    tmpStaffAppt.ActualHours = actualHours + ":" + actualMinutes;
                                } else {
                                    tmpStaffAppt.ActualHours = "00:00";
                                }

                                report.dataSource.push(tmpStaffAppt);
                            }

                            if (duration) {
                                var overallTotalScheduledMinutes = duration;

                                var overallHoursScheduled = Math.floor(overallTotalScheduledMinutes / 60);
                                var overallMinutesScheduled = Math.round(overallTotalScheduledMinutes % 60);
                                if (overallHoursScheduled.toString().length == 1) {
                                    overallHoursScheduled = "0" + overallHoursScheduled;
                                }
                                if (overallMinutesScheduled.toString().length == 1) {
                                    overallMinutesScheduled = "0" + overallMinutesScheduled;
                                }


                                report.totalScheduledHours = overallHoursScheduled + ":" + overallMinutesScheduled;
                            } else {
                                report.totalScheduledHours = "00:00";
                            }

                            if (actualDuration) {
                                var overallTotalActualMinutes = actualDuration;

                                var overallActualHours = Math.floor(overallTotalActualMinutes / 60);
                                var overallActualMinutes = Math.round(overallTotalActualMinutes % 60);
                                if (overallActualHours.toString().length == 1) {
                                    overallActualHours = "0" + overallActualHours;
                                }
                                if (overallActualMinutes.toString().length == 1) {
                                    overallActualMinutes = "0" + overallActualMinutes;
                                }


                                report.totalActualHours = overallActualHours + ":" + overallActualMinutes;
                            } else {
                                report.totalActualHours = "00:00";
                            }

                        }
                        reportViewerJson.push(report);
                    }
                }

                if (reportViewerJson !== null && reportViewerJson.length > 0) {

                    generateReportTables(reportViewerJson);
                    var tmpScheduledHours = "", tmpActualHours = "";
                    if (grandTotalScheduledHours > 0) {

                        var totalScheduledMinutes = grandTotalScheduledHours;

                        var hoursScheduled = Math.floor(totalScheduledMinutes / 60);
                        var minutesScheduled = Math.round(totalScheduledMinutes % 60);
                        if (hoursScheduled.toString().length == 1) {
                            hoursScheduled = "0" + hoursScheduled;
                        }
                        if (minutesScheduled.toString().length == 1) {
                            minutesScheduled = "0" + minutesScheduled;
                        }


                        tmpScheduledHours = hoursScheduled + ":" + minutesScheduled;
                    } else {
                        tmpScheduledHours = "00:00";
                    }


                    if (grandTotalActualHours) {

                        var totalActualMinutes = grandTotalActualHours;

                        var actualHours = Math.floor(totalActualMinutes / 60);
                        var actualMinutes = Math.round(totalActualMinutes % 60);
                        if (actualHours.toString().length == 1) {
                            actualHours = "0" + actualHours;
                        }
                        if (actualMinutes.toString().length == 1) {
                            actualMinutes = "0" + actualMinutes;
                        }


                        tmpActualHours = actualHours + ":" + actualMinutes;
                    } else {
                        tmpActualHours = "00:00";
                    }

                    $("#divGrandTotalHours").show();
                    $("#tdGrandTotaScheduledHours").text(tmpScheduledHours);
                    $("#tdGrandTotaActualHours").text(tmpActualHours);

                }
            }
        } else {
            customAlert.info("Info", "No timesheet details");
        }
    }

    function generateReportTables(reportViewerJson) {
        if (reportViewerJson != null && reportViewerJson.length > 0) {
            var insertAfterElement = "";
            for (var counter = 0; counter < reportViewerJson.length; counter++) {
                var reportDisplayName = reportViewerJson[counter].ProviderName;
                var reportDisplayNameId = "StaffTimeSheet" + (counter + 1);
                var $report = $("<div class=\"col-xs-12 table-block\" id=\"div" + reportDisplayNameId + "\"> <div class=\"col-xs-12 timeSheetReportdivHeading\" style='page-break-before: always !important;'>" + reportDisplayName + " </div><div class=\"table-responsive\"> <table class=\"timesheetmembersTable\" id=\"tbl" + reportDisplayNameId + "\"> </table> </div> </div>");

                $("#divStaffTimeSheetReportViewer").append($report);
                bindTimeSheetReport(reportDisplayNameId, reportViewerJson[counter]);
            }
        }
    }


    function bindTimeSheetReport(reportDisplayNameId, reportData) {
        var dataObj = reportData.dataSource;

        var tableId = "#tbl" + reportDisplayNameId;
        var strHeading = "<thead>";
        var columns = [];
        if (dataObj != null && dataObj.length > 0) {
            var firstObj = dataObj[0];
            var c = 0;
            for (var key in firstObj) {
                if (key.toLowerCase() == "appt date time") {
                    strHeading = strHeading + "<tr style=\"page-break-inside: avoid\"><th class=\"borderbuttomthin\" style='width:12%'>" + key + "</th>";
                } else {
                    if (c === 0) {
                        strHeading = strHeading + "<th></th>";
                    } else {
                        if (key.toLowerCase() == "serviceuser") {
                            strHeading = strHeading + "<th class='borderbuttomthin'>Service User</th>";
                        } else if (key.toLowerCase() == "datetimeofappointment") {
                            strHeading = strHeading + "<th class='borderbuttomthin'>Appointment Date Time</th>";
                        } else if (key.toLowerCase() == "scheduledhours") {
                            strHeading = strHeading + "<th class='borderbuttomthin'>Scheduled Hrs</th>";
                        } else if (key.toLowerCase() == "actualhours") {
                            strHeading = strHeading + "<th class='borderbuttomthin'>Actual Hrs</th></tr>";
                        }
                    }

                }
                columns.push(key);
                c = c + 1;
            }
        }
        strHeading = strHeading + "</thead>";

        $(tableId).append(strHeading);


        var strTableBody = "<tbody>";

        for (var counter = 0; counter < dataObj.length; counter++) {
            var strTableRow = "<tr style=\"page-break-inside: avoid\">";

            for (var counter2 = 0; counter2 < columns.length; counter2++) {
                if (counter2 > 0 && counter === dataObj.length - 1) {
                    strTableRow = strTableRow + "<td class=\"borderbuttomthick\">" + dataObj[counter][columns[counter2]] + "</td>";
                } else {
                    strTableRow = strTableRow + "<td>" + dataObj[counter][columns[counter2]] + "</td>";
                }


            }
            strTableRow = strTableRow + "</tr>";

            strTableBody = strTableBody + strTableRow;
        }

        var totalRow = '<tr class="timesheetmembersTable-total" style="page-break-inside: avoid"><td></td><td align="left">Total Scheduled &amp; Actual Hours</td><td class=\"showgrandtotal\">' + reportData.totalScheduledHours + '</td><td class=\"showgrandtotal\">' + reportData.totalActualHours + '</td></tr>';
        strTableBody = strTableBody + totalRow;
        strTableBody = strTableBody + "</tbody>";
        $(tableId).append(strTableBody);


    }

    function onPatientListData(dataObj) {
        dtArray = [];
        buildDeviceListGrid([]);
        if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
            if (dataObj.response.appointment) {
                if ($.isArray(dataObj.response.appointment)) {
                    dtArray = dataObj.response.appointment;
                } else {
                    dtArray.push(dataObj.response.appointment);
                }
            }
        }
        dtArray.sort(function (a, b) {
            var nameA = a.dateOfAppointment; // ignore upper and lowercase
            var nameB = b.dateOfAppointment; // ignore upper and lowercase
            if (nameA < nameB) {
                return -1;
            }
            if (nameA > nameB) {
                return 1;
            }
            // names must be equal
            return 0;
        });

        if (dtArray.length > 0) {
            for (var i = 0; i < dtArray.length; i++) {
                // $("#btnCopy").css("display", "");
                dtArray[i].idk = dtArray[i].id;
                dtArray[i].name = dtArray[i].composition.patient.lastName + " " + dtArray[i].composition.patient.firstName + " " + dtArray[i].composition.patient.middleName;


                var date = new Date(GetDateTimeEditDay(dtArray[i].dateOfAppointment));
                var day = date.getDay();
                var dow = getWeekDayName(day);

                var dtEnd = dtArray[i].dateOfAppointment + (Number(dtArray[i].duration) * 60000);

                dtArray[i].dateTimeOfAppointment = dow + " " + GetDateTimeEdit(dtArray[i].dateOfAppointment);
                dtArray[i].DW = dow;

                var dateED = new Date(GetDateTimeEditDay(dtEnd));
                var dayED = dateED.getDay();
                var dowED = getWeekDayName(dayED);

                dtArray[i].dateTimeOfAppointmentED = dowED + " " + GetDateTimeEdit(dtEnd);


                var dt = new Date(dtArray[i].dateOfAppointment);
                var strDT = "";
                if (dt) {
                    strDT = kendo.toString(dt, "MM/dd/yyyy h:mm tt");
                }

                dtArray[i].dateTimeOfAppointment1 = strDT;


                dtArray[i].appointmentReasonDesc = dtArray[i].composition.appointmentReason.desc;
                dtArray[i].appointmentTypeDesc = dtArray[i].composition.appointmentType.desc;
                if (dtArray[i].providerId != 0) {
                    dtArray[i].staffName = dtArray[i].composition.provider.lastName + " " + dtArray[i].composition.provider.firstName + " " + (dtArray[i].composition.provider.middleName != null ? dtArray[i].composition.provider.middleName : " ");
                } else {
                    dtArray[i].staffName = "";
                }
            }


            buildDeviceListGrid(dtArray);
        } else {
            // $("#btnCopy").css("display", "none");
            customAlert.info("Info", "No appointments");
        }
    }

    function copyAppointment(dataObj) {
        parentRef.selValue = null;
        parentRef.appselList = null;
        onClickSubmit();
    }

    function onCloseEditAppointment(dataObj) {
        parentRef.selectedItems = null;
        buildDeviceListGrid([]);
        onClickViewStaffCareReport();

    }


    function getWeekDayName(wk) {
        var wn = "";
        if (wk == 1) {
            return "Mon";
        } else if (wk == 2) {
            return "Tue";
        } else if (wk == 3) {
            return "Wed";
        } else if (wk == 4) {
            return "Thu";
        } else if (wk == 5) {
            return "Fri";
        } else if (wk == 6) {
            return "Sat";
        } else if (wk == 0) {
            return "Sun";
        }
        return wn;
    }

    function getFromDate(fromDate) {


        var day = fromDate.getDate();
        var month = fromDate.getMonth();
        month = month + 1;
        var year = fromDate.getFullYear();

        var strDate = month + "/" + day + "/" + year;
        strDate = strDate + " 00:00:00";

        var date = new Date(strDate);
        var strDateTime = date.getTime();

        return strDateTime;
    }

    function getToDate(toDate) {


        var day = toDate.getDate();
        var month = toDate.getMonth();
        month = month + 1;
        var year = toDate.getFullYear();

        var strDate = month + "/" + day + "/" + year;
        strDate = strDate + " 23:59:59";

        var date = new Date(strDate);
        var strDateTime = date.getTime();

        return strDateTime;
    }

    var sortByDateAsc = function (x, y) {
        // This is a comparison function that will result in dates being sorted in
        // ASCENDING order. As you can see, JavaScript's native comparison operators
        // can be used to compare dates. This was news to me.

        var date1 = new Date(x.dateOfAppointment);
        var date2 = new Date(y.dateOfAppointment);

        if (date1 > date2) return 1;
        if (date1 < date2) return -1;
        return 0;
    };

    function formatDate(date) {

        var dd = date.getDate();

        var mm = date.getMonth() + 1;
        var yyyy = date.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }

        if (mm < 10) {
            mm = '0' + mm;
        }

        return dd + '/' + mm + '/' + yyyy;
    }

    var staffArray = [];


    function getProvidersList(dataObj) {
        var dtArray = [];

        if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
            if (dataObj.response.provider) {
                if ($.isArray(dataObj.response.provider)) {
                    dtArray = dataObj.response.provider;
                } else {
                    dtArray.push(dataObj.response.provider);
                }
            }
        }

        if (dtArray && dtArray.length > 0) {
            staffArray = dtArray;
            dtArray.sort(function (a, b) {
                var p1 = a.lastName + " " + a.firstName;
                var p2 = b.lastName + " " + b.firstName;

                if (p1 < p2) return -1;
                if (p1 > p2) return 1;
                return 0;
            });

            for (var i = 0; i < dtArray.length; i++) {
                dtArray[i].idk = dtArray[i].id;
                var staffName = dtArray[i].lastName + " " + dtArray[i].firstName;
                $("#cmbStaff").append('<option value="' + dtArray[i].idk + '">' + staffName + '</option>');
            }

            $("#cmbStaff").multipleSelect({
                selectAll: true,
                width: 200,
                dropWidth: 200,
                multipleWidth: 200,
                placeholder: 'Select Staff',
                selectAllText: 'All'

            });
        }
    }


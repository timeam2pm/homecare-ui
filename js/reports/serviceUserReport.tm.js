var angularUIgridWrapper;
var parentRef = null;
var recordType = "1";
var dataArray = [];
var ADD = "add";
var UPDATE = "edit";
var VIEW = "view";
var DELETE = "delete";
var operation = "add";
var ds = "";
var IsFlag = 1;
var cntry = sessionStorage.countryName;
var selectPatientId,selProviderId;
var dtFMT = "dd/MM/yyyy";
var allDatesInWeek = [];
var daysOfWeek = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

$(document).ready(function () {
    $("#pnlPatient", parent.document).css("display", "none");
    $('#cmbServiceUser').multipleSelect('refresh');
    sessionStorage.setItem("IsSearchPanel", "1");
    //themeAPIChange();
    parentRef = parent.frames['iframe'].window;

    var cntry = sessionStorage.countryName;
    if((cntry.indexOf("India")>=0) || (cntry.indexOf("United Kingdom")>=0)){
        $("#lblFacility").text("Location");
    }else if(cntry.indexOf("United State")>=0) {
        $("#lblFacility").text("Facility");
    }
});


$(window).load(function () {
    loading = false;
    //$(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded() {

    init();
    buttonEvents();
}

function init() {
    allowNumerics("txtAge");
    getAjaxObject(ipAddress + "/patient/list/?is-active=1&is-deleted=0", "GET", getPatientList, onError);
    getAjaxObject(ipAddress + "/master/Language/list?is-active=1&is-deleted=0", "GET", getLanguageValueList, onError);
    getAjaxObject(ipAddress + "/facility/list/?is-active=1&is-deleted=0", "GET", getFacilityList, onError);
    getAjaxObject(ipAddress + "/master/Gender/list/?is-active=1&is-deleted=0", "GET", getGenderValueList, onError);

    $("#txtDateOfWeek").kendoDatePicker({ format: dtFMT, value: new Date() });

}



function onError(errorObj) {
    console.log(errorObj);
}

function buttonEvents() {

    $("#btnSubmit").off("click", onClickViewServiceUserCareReport);
    $("#btnSubmit").on("click", onClickViewServiceUserCareReport);

    $("#btnServiceUser").off("click");
    $("#btnServiceUser").on("click",onClickServiceUser);

    $("#btnStaffUser").off("click");
    $("#btnStaffUser").on("click", onClickStaff);

    $("#btnPrint").off("click");
    $("#btnPrint").on("click",onClickPrint);

}

function onClickPrint(){
    if ($.trim($('#divSUCareReport').html()) !== "") {

        var facility = $("#cmbFacility option:selected").text();
        var facilityId = $("#cmbFacility").val();
        var communications;
        var communicationsDeatils1, communicationsDeatils2;
        var tempFacilityDataArry = _.where(facilityDataArry, {idk: Number(facilityId)});
        if(tempFacilityDataArry[0].communications && tempFacilityDataArry[0].communications != null) {
            communications = tempFacilityDataArry[0].communications;
            if (communications && communications != null) {
                if (communications[0].address1 != "") {
                    communicationsDeatils1 = communications[0].address1;
                }

                if (communications[0].address2 != "") {
                    communicationsDeatils1 = communicationsDeatils1 + ", " + communications[0].address2;
                }

                if (communications[0].city != "") {
                    communicationsDeatils2 = communications[0].city;
                }

                if (communications[0].state != "") {
                    communicationsDeatils2 = communicationsDeatils2 + ", " + communications[0].state;
                }

                if (communications[0].zip != "") {
                    communicationsDeatils2 = communicationsDeatils2 + ", " + communications[0].zip;
                }
            }
        }

        var contents = document.getElementById("divSUCareReport").outerHTML;// $("#divTaskReportsViewer").html();
        var frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({ "position": "absolute", "top": "-1000000px" });
        $("body").append(frame1);
        var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        var strDate;
        var DOB = new Date();
        strDate = kendo.toString(DOB, "dd/MM/yyyy hh:mm:ss tt");

        var age = $("#txtAge").val();
        var langVal = $("#cmbLang").val();
        var lang = "";
        if(langVal != 0){
            lang = $("#cmbLang option:selected").text();
        }

        var genderVal = $("#cmbGender").val();
        var gender = "";
        if(genderVal != 0){
            gender = $("#cmbGender option:selected").text();
        }

        //Create a new HTML document.
        frameDoc.document.write('<html><title>Service User Report</title><head>');
        frameDoc.document.write('<link href="../../css/Theme01.css" rel="stylesheet" type="text/css" />');
        frameDoc.document.write('<link href="../../css/report/invoicestyle.css" rel="stylesheet" type="text/css" />');
        frameDoc.document.write('</head><body style="background-color: #fff;">');

        frameDoc.document.write('<table class="tblInvoiceHeader"><tr style="page-break-inside: avoid"><td><div class="reportName"><h2><span style="color:#D5AC57">Staff Weekly Planned Care Report</span> <br clear="all"><small style="color:#000000">'+ strDate +'</small></h2></div></td>');
        frameDoc.document.write('<td><div class="reportLocations"><h2><span>'+facility+'<span></span></span></h2><address> '+ communicationsDeatils1 + '<br>'+communicationsDeatils2+'</address></div> </td></tr></table><br clear="all">');

        // frameDoc.document.write('<div class="reportName"><h2><span>Timesheet Report</span> <br clear="all"><small>'+ strDate +'</small></h2></div>');
        // frameDoc.document.write('<div class="reportLocations"><h2><span>'+facility+'<span></span></span></h2><address> '+ communicationsDeatils1 + '<br>'+communicationsDeatils2+'</address></div> <br clear="all">');

        frameDoc.document.write('<div class="searchedLabels">');
        frameDoc.document.write('<div class="divLabel"><span>Location :</span> <label>'+facility+'</label>' +" "+'<span>ServiceUser :</span> <label>'+printSUName.slice(0,-1)+'</label>' +" "+'<span>Gender :</span> <label>'+gender+'</label>' +" "+'<span>Age :</span> <label>'+age+'</label>' +" "+'<span>Language :</span> <label>'+lang+'</label></div>');
        frameDoc.document.write('</div>');

        frameDoc.document.write('<div class="timesheetinvoiceBlock">');

        //Append the DIV contents.
        frameDoc.document.write(contents);

        frameDoc.document.write('</div>');
        //frameDoc.document.write('</div> </div> </div>');
        frameDoc.document.write('</body></html>');


        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();

        }, 600);
    }
    else {
        customAlert.info("info","No data to print.");
    }
}

function onClickPrint1(){
    if ($.trim($('#divSUCareReport').html()) !== "") {

        var facility = $("#cmbFacility option:selected").text();
        var facilityId = $("#cmbFacility").val();
        var communications;
        var communicationsDeatils1, communicationsDeatils2;
        var tempFacilityDataArry = _.where(facilityDataArry, {idk: Number(facilityId)});
        if(tempFacilityDataArry[0].communications && tempFacilityDataArry[0].communications != null) {
            communications = tempFacilityDataArry[0].communications;
            if (communications && communications != null) {
                if (communications[0].address1 != "") {
                    communicationsDeatils1 = communications[0].address1;
                }

                if (communications[0].address2 != "") {
                    communicationsDeatils1 = communicationsDeatils1 + ", " + communications[0].address2;
                }

                if (communications[0].city != "") {
                    communicationsDeatils2 = communications[0].city;
                }

                if (communications[0].state != "") {
                    communicationsDeatils2 = communicationsDeatils2 + ", " + communications[0].state;
                }

                if (communications[0].zip != "") {
                    communicationsDeatils2 = communicationsDeatils2 + ", " + communications[0].zip;
                }
            }
        }

        var contents = document.getElementById("divSUCareReport").outerHTML;// $("#divTaskReportsViewer").html();
        var frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({ "position": "absolute", "top": "-1000000px" });
        $("body").append(frame1);
        var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        var strDate;
        var DOB = new Date();
        strDate = kendo.toString(DOB, "dd/MM/yyyy hh:mm:ss tt");

        var age = $("#txtAge").val();
        var langVal = $("#cmbLang").val();
        var lang = "";
        if(langVal != 0){
            lang = $("#cmbLang option:selected").text();
        }

        var genderVal = $("#cmbGender").val();
        var gender = "";
        if(genderVal != 0){
            gender = $("#cmbGender option:selected").text();
        }

        //Create a new HTML document.
        frameDoc.document.write('<html><title>Service User Report</title><head>');
        frameDoc.document.write('<link href="../../css/Theme01.css" rel="stylesheet" type="text/css" />');
        frameDoc.document.write('<link href="../../css/report/invoicestyle.css" rel="stylesheet" type="text/css" />');

        frameDoc.document.write('</head><body style="background-color: #fff;">');

        frameDoc.document.write('<table class="tblInvoiceHeader"><tr style="page-break-inside: avoid"><td><div class="reportName"><h2><span style="color:#D5AC57">Service User Weekly Planned Care Report</span> <br clear="all"><small style="color: #000000 ;">'+ strDate +'</small></h2></div></td>');
        frameDoc.document.write('<td><div class="reportLocations"><h2><span>'+facility+'<span></span></span></h2><address> '+ communicationsDeatils1 + '<br>'+communicationsDeatils2+'</address></div> </td></tr></table><br clear="all">');

        frameDoc.document.write('<div class="searchedLabels">');
        frameDoc.document.write('<div class="divLabel"><span>Location :</span> <label>'+facility+'</label>' +" "+'<span>ServiceUser :</span> <label>'+printSUName.slice(0,-1)+'</label>' +" "+'<span>Gender :</span> <label>'+gender+'</label>' +" "+'<span>Age :</span> <label>'+age+'</label>' +" "+'<span>Language :</span> <label>'+lang+'</label>' +" "+'<span>Gender :</span> <label>'+gender+'</label></div>');
        frameDoc.document.write('</div>');

        frameDoc.document.write('<div class="timesheetinvoiceBlock">');

        //Append the DIV contents.
        frameDoc.document.write(contents);

        frameDoc.document.write('</div>');
        //frameDoc.document.write('</div> </div> </div>');
        frameDoc.document.write('</body></html>');


       // frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();

        }, 0);
    }
    else {
        customAlert.info("info","No data to print.");
    }
}
var operation = "add";
var selFileResourceDataItem = null;


function themeAPIChange() {
    getAjaxObject(ipAddress + "/homecare/settings/?id=2", "GET", getThemeValue, onError);
}

function getThemeValue(dataObj) {
    console.log(dataObj);
    var tempCompType = [];
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.activityTypes) {
            if ($.isArray(dataObj.response.settings)) {
                tempCompType = dataObj.response.settings;
            } else {
                tempCompType.push(dataObj.response.settings);
            }
        }
        if (tempCompType.length > 0) {
            themesChange(tempCompType[0].value);
        }
    }
}

function themesChange(themeValue) {
    if (themeValue == 2) {
        loadAPi("../../Theme2/Theme02.css");
    }
    else if (themeValue == 3) {
        loadAPi("../../Theme3/Theme03.css");
    }
}

var facilityDataArry = [];

function getFacilityList(dataObj) {
    var dataArray = [];
    if (dataObj) {
        if ($.isArray(dataObj.response.facility)) {
            dataArray = dataObj.response.facility;
        } else {
            dataArray.push(dataObj.response.facility);
        }
    }
    facilityDataArry = dataArray;
    for (var i = 0; i < dataArray.length; i++) {
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if (dataArray[i].isActive == 1) {
            dataArray[i].Status = "Active";
        }
        $("#cmbFacility").append('<option value="' + dataArray[i].idk + '">' + dataArray[i].name + '</option>');
    }
}

function onClickServiceUser() {
    var popW = 800;
    var popH = 450;

    if (parentRef)
        parentRef.searchZip = true;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Service User";
    devModelWindowWrapper.openPageWindow("../../html/patients/searchPatient.html", profileLbl, popW, popH, true, onCloseServiceUserAction);
}
function onCloseServiceUserAction(evt, returnData) {
    if (returnData && returnData.status == "success") {
        selectPatientId = returnData.selItem.PID;
        $('#txtSU').val(returnData.selItem.LN + " " + returnData.selItem.FN + " " + returnData.selItem.MN);
    }
}

function onClickStaff(){
    var popW = 800;
    var popH = 450;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Staff";
    devModelWindowWrapper.openPageWindow("../../html/patients/staffList.html", profileLbl, popW, popH, true, onCloseStaffAction);
}

function onCloseStaffAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        selProviderId = returnData.selItem.PID;
        $('#txtStaffUser').val(returnData.selItem.LN+' '+returnData.selItem.FN);

    }
}

function onClickSubmit() {
    if ($("#txtSU").val() != "") {
        if ($("#txtFDate").val() !== "") {
            var txtAppPTDate = $("#txtFDate").data("kendoDatePicker");
            var selectedDate = txtAppPTDate.value();
            var selDate = new Date(selectedDate);


            // var selectedDate = GetDateTime("txtDate");


            for (let i = 1; i <= 7; i++) {
                var first = selectedDate.getDate() - (selectedDate.getDay() + 1) + i;
                var day = new Date(selectedDate.setDate(first));

                allDatesInWeek.push(day);
            }
            var day = selDate.getDate();
            var month = selDate.getMonth();
            month = month + 1;
            var year = selDate.getFullYear();

            var stDate = month + "/" + day + "/" + year;
            stDate = stDate + " 00:00:00";

            var startDate = new Date(stDate);
            var stDateTime = startDate.getTime();

            var etDate = month + "/" + day + "/" + year;
            etDate = etDate + " 23:59:59";

            var endtDate = new Date(etDate);
            var edDateTime = endtDate.getTime();

            var startDate = new Date(stDate);
            var endtDate = new Date(etDate);

            var day = startDate.getDate() - startDate.getDay();
            var pDate = new Date(startDate);
            pDate.setDate(day);

            endtDate.setDate(day + 6);
            var stDateTime = pDate.getTime();
            var edDateTime = endtDate.getTime();
            var txtAppPTFacility = $("#cmbFacility").val();
            parentRef.facilityId = txtAppPTFacility;
            parentRef.patientId = selectPatientId;
            parentRef.SUName = $("#txtSU").val();
            parentRef.facilityName = $("#cmbFacility option:selected").text();
            parentRef.selDate = pDate;
            parentRef.copyDate = txtAppPTDate.value();

            buildDeviceListGrid([]);

            var patientListURL = ipAddress + "/appointment/list/?facility-id=" + txtAppPTFacility + "&patient-id=" + selectPatientId + "&to-date=" + edDateTime + "&from-date=" + stDateTime + "&is-active=1";

            getAjaxObject(patientListURL, "GET", onPatientListData, onError);

        }
        else {
            customAlert.error("Error", "Please select date");
        }
    }
    else {
        customAlert.error("Error", "Please select service user");
    }
}

function onClickViewServiceUserCareReport() {

    $("#divSUCareReport").html("");

    var strMessage;
    var isValid = true;
    if ($("#cmbFacility").val() === 0) {
        strMessage = "Please select facility.";
        isValid = false;
    }

    var age = $("#txtAge").val();
    var lang = $("#cmbLang option:selected").text();
    var gender = $("#cmbGender option:selected").text();

    var langVal = $("#cmbLang").val();
    var genderVal = $("#cmbGender").val();
    var patientIds = 0;
    var patientIds = $('#cmbServiceUser').multipleSelect('getSelects');

    if(patientIds == 0 && age == "" && langVal == "0" && genderVal == "0"){
        strMessage = "Please select any one of the above filter.";
        isValid = false;
    }

    if(isValid) {
        var facilityId = $("#cmbFacility").val();

        var apiURL;
       apiURL = ipAddress + "/carehome/patient-billing/?is-active=1&fields=*,billTo.name";

        if (patientIds != 0) {
            if(patientIds.length > 1) {
                apiURL = apiURL + "&patient-id=:in:" + patientIds.join(',');
            }
            else{
                apiURL = apiURL + "&patient-id=" + patientIds.join(',');
            }
        }

        if (age != ""){
            var date = new Date();
            date.setHours(age);
            apiURL = apiURL + "&patient.date-of-birth=:le:"+date.getTime();
        }

        if (langVal != "0"){
            apiURL = apiURL + "&patient.language=:lk:"+lang.toLowerCase();
        }
        if (genderVal != "0"){
            apiURL = apiURL + "&patient.gender="+gender.toLowerCase();
        }

        getAjaxObject(apiURL, "GET", onGetServiceUserCareReport1, onError);

    }
    else{
        customAlert.error("info",strMessage);
    }
}


function onGetServiceUserCareReport1(dataObj) {
    dtArray = [];
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.patientBilling) {
            if ($.isArray(dataObj.response.patientBilling)) {
                dtArray = dataObj.response.patientBilling;
            } else {
                dtArray.push(dataObj.response.patientBilling);
            }
        }
    }

    if (dtArray !== null && dtArray.length > 0) {

        var txtDateOfWeek = $("#txtDateOfWeek").data("kendoDatePicker");
        var selectedDate = txtDateOfWeek.value();

        // var arrDatesInWeek = [];
        //
        // for (var i = 1; i <= 7; i++) {
        //     var first = selectedDate.getDate() - (selectedDate.getDay() + 1) + i;
        //     var day = new Date(selectedDate.setDate(first));
        //
        //     arrDatesInWeek.push(day);
        // }
        //
        // var allDaysInWeek = [];
        // for (var counter4 = 0; counter4 < arrDatesInWeek.length; counter4++) {
        //     allDaysInWeek.push(daysOfWeek[arrDatesInWeek[counter4].getDay()] + " " + formatDate(arrDatesInWeek[counter4]));
        // }

        var appointments = [];

        var lookup = {};
        var patientIds = [];
        for (var c = 0; c < dtArray.length; c++){
            var item;
            item = dtArray[c];
            if (item && item.patientId) {

                var patientId = item.patientId;

                if (!(patientId in lookup)) {
                    lookup[patientId] = 1;
                    patientIds.push(patientId);
                }
                var objSU = _.where(suArray, {id:  patientId});
                if(objSU && objSU.length > 0) {
                    dtArray[c].patientName = objSU[0].lastName+ " " + objSU[0].firstName;
                }
            }
        }



        var patientAppointments = [];

        if (patientIds !== null && patientIds.length > 0) {

            for (var j = 0; j < patientIds.length; j++) {

                var patientObj = {};


                var tmpSUAppts = $.grep(dtArray, function (e) {
                    return e.patientId === patientIds[j];
                });

                if (tmpSUAppts !== null && tmpSUAppts.length > 0) {
                    if (tmpSUAppts[0]) {
                        patientObj.id = tmpSUAppts[0].patientId;
                        patientObj.patientName = tmpSUAppts[0].patientName;
                    }

                    var suWeeklyAppts = [];

                    // for (var counter = 0; counter < arrDatesInWeek.length; counter++) {
                    //
                    //     var obj = {};
                    //
                    //     var tmpDate = arrDatesInWeek[counter];
                    //     tmpDate.setHours(0, 0, 0, 0);
                    //
                    //
                    //     var tmpAppointments = $.grep(tmpSUAppts, function (e) {
                    //         var apptDate = new Date();
                    //         e.dateOfAppointment = apptDate.setHours(0, 0, e.fromTime, 0);
                    //         return counter == e.weekDayStart ;
                    //     });
                    //
                    //     obj.dateOfAppointment = allDaysInWeek[counter];
                    //     obj.dayAppointments = [];
                    //
                    //
                    //     if (tmpAppointments != null && tmpAppointments.length > 0) {
                    //         obj.dayAppointments = tmpAppointments;
                    //
                    //     }
                    //
                    //     suWeeklyAppts.push(obj);
                    //
                    // }
                    patientObj.weeklyAppointments = tmpSUAppts;
                }

                patientAppointments.push(patientObj);
            }
            // console.log(JSON.stringify(patientAppointments));
            bindStaffCareReport(patientAppointments);
        }

    }
    else {
        customAlert.info("Info", "No records found");
    }
}
var printSUName = '';
var providerTotalHrs;

function bindStaffCareReport(patientAppointments) {
    var facility = $("#cmbFacility option:selected").text();
    sessionStorage.providerTotalHrs = '';
    $("#divSUCareReport").html("");
    if (patientAppointments !== null && patientAppointments.length > 0) {
        patientAppointments.sort(function (a, b) {
            var p1 = a.patientName;
            var p2 = b.patientName;

            if (p1 < p2) return -1;
            if (p1 > p2) return 1;
            return 0;
        });

        for (var i = 0; i < patientAppointments.length; i++) {
            providerTotalHrs = 0;
            var pObj = patientAppointments[i];

            var objSU = _.where(suArray, {id: pObj.id});
            var strHTML = '';
            if(objSU && objSU.length > 0) {
                var dt;
                if(objSU[0].dateOfBirth && objSU[0].dateOfBirth != null && objSU[0].dateOfBirth != ""){
                    dt = new Date(objSU[0].dateOfBirth);
                }
                else{
                    dt = new Date();
                }

                strHTML = strHTML + '<div class="table-block">';
                strHTML = strHTML + '<div class="timeSheetReportdivHeading">';
                printSUName = printSUName + objSU[0].lastName+ " " + objSU[0].firstName + ",";
                strHTML = strHTML + "#"+pObj.id + " - "+objSU[0].lastName +" "+ objSU[0].firstName;
                strHTML = strHTML + '<span class="headingSpn floatRight">';
                var gender = (objSU[0].gender != null) ? objSU[0].gender.charAt(0) : "";
                strHTML = strHTML + GetDateEdit(objSU[0].dateOfBirth) +'(' + getAge(dt) + 'Y/' + gender  + ') '+'</span><div class="clearLine"></div>';
                // strHTML = strHTML + '<span class="headingSpn floatLeft" id="spWeekly'+i+'"></span>';
                // if (objSU.communications) {
                //     strHTML = strHTML + '<span class="headingSpn floatRight">';
                //     var Add;
                //     if (objSU.communications.address1 && objSU.communications.address1 != '') {
                //         Add = objSU.communications.address1 + ',';
                //     }
                //
                //     if (objSU.communications.address2 && objSU.communications.address2 != '') {
                //         Add = Add + objSU.communications.address2 + ',';
                //     }
                //
                //     if (objSU.communications.city && objSU.communications.city != '') {
                //         Add = Add + objSU.communications.city + ',';
                //     }
                //
                //     if (objSU.communications.zip && objSU.communications.zip != '') {
                //         Add = Add + objSU.communications.zip + ',';
                //     }
                //
                //     if (objSU.communications.homePhone && objSU.communications.homePhone != '') {
                //         Add = Add + objSU.communications.homePhone + ',';
                //     }
                //
                //     if (objSU.communications.workPhone && objSU.communications.workPhone != '') {
                //         Add = Add + objSU.communications.workPhone + ',';
                //     }
                //
                //     if (objSU.communications.email && objSU.communications.email != '') {
                //         Add = Add + objSU.communications.email + ',';
                //     }
                //     Add = Add.slice(0, -1);
                //     strHTML = strHTML + Add;
                // }

                strHTML = strHTML + '</div></div>';
                strHTML = strHTML + '<div class="col-xs-12 taskReportTblBlock"><div class="table-responsive">';
                strHTML = strHTML + '<table class="tblTaskreport weeklyCareReportTbl"><thead><tr>';

                var arrAppointmentsCount = [];

                // for (var j = 0; j < pObj.weeklyAppointments.length; j++) {

                strHTML = strHTML + '<th><span class="spnReportTitle">Start</th>';
                strHTML = strHTML + '<th><span class="spnReportTitle">DNAR</th>';
                strHTML = strHTML + '<th><span class="spnReportTitle">Location</th>';
                strHTML = strHTML + '<th><span class="spnReportTitle">Bill To Name</th>';
                strHTML = strHTML + '<th><span class="spnReportTitle">Language</th>';
                // arrAppointmentsCount.push(pObj.weeklyAppointments[j].dayAppointments.length);
                // }
                strHTML = strHTML + '</tr></thead>';
                strHTML = strHTML + '<tbody>';

                // var columns = 7;
                // var rows = parseInt(Math.max(...arrAppointmentsCount));

                var dayWiseDuration = {};

                for (var rownumber = 0; rownumber < pObj.weeklyAppointments.length; rownumber++) {

                    // var date = new Date(GetDateTimeEditDay(pObj.weeklyAppointments[rownumber].createdDate));
                    // var day = date.getDay();
                    // var dow = getWeekDayName(day);

                    var dowDate = "";
                    if(objSU[0].registrationStartDate != null && objSU[0].registrationStartDate != "") {
                        dowDate = GetDateEdit(objSU[0].registrationStartDate);
                    }
                    strHTML = strHTML + '<tr><td>' + dowDate + '</td>';
                    if (objSU[0].dnr == 1) {
                        strHTML = strHTML + '<td>Y</td>';
                    } else {
                        strHTML = strHTML + '<td>N</td>';
                    }
                    var strVal = $.trim(objSU[0].language.slice(0, -1));
                    var lastChar = strVal.slice(-1);
                    if(lastChar == ','){
                        strVal = strVal.slice(0, -1);
                    }
                    strVal = $.trim(strVal);
                    var lastChar = strVal.slice(-1);
                    if(lastChar == ','){
                        strVal = strVal.slice(0, -1);
                    }

                    strHTML = strHTML + '<td>' + facility + '</td>';
                    strHTML = strHTML + '<td>' + pObj.weeklyAppointments[rownumber].billToname + '</td>';
                    strHTML = strHTML + '<td>' +  strVal + '</td></tr>';

                    // for (var columnnumber = 0; columnnumber < columns; columnnumber++) {
                    //
                    //     var tempAppointment = pObj.weeklyAppointments[columnnumber].dayAppointments[rownumber];
                    //
                    //     var currentDay = pObj.weeklyAppointments[columnnumber].dateOfAppointment.split(' ')[0];
                    //
                    //     if (!(currentDay in dayWiseDuration)) {
                    //         dayWiseDuration[currentDay] = 0;
                    //     }
                    //
                    //
                    //     var apptTimeDuration = "";
                    //
                    //     if (tempAppointment !== null && typeof (tempAppointment) !== "undefined") {
                    //
                    //         var appDate = new Date(tempAppointment.dateOfAppointment);
                    //
                    //         var strH = kendo.toString(appDate, "hh");
                    //         var strm = kendo.toString(appDate, "mm");
                    //         var stT = kendo.toString(appDate, "tt");
                    //
                    //         var time = strH;
                    //         if (strm.length > 0) {
                    //             time = time + ":" + strm;
                    //         }
                    //         time = time + stT;
                    //         time = time.toLowerCase();
                    //
                    //         apptTimeDuration = time + '<br/>' + tempAppointment.duration + 'min';
                    //
                    //         dayWiseDuration[currentDay] = dayWiseDuration[currentDay] + parseInt(tempAppointment.duration);
                    //
                    //     }
                    //
                    //
                    //     strHTML = strHTML + '<td>' + apptTimeDuration + '</td>';
                    //
                    // }

                    strHTML = strHTML + '</tr>';
                }

                // if (dayWiseDuration) {
                //     strHTML = strHTML + '<tr class="TotalRow">';
                //     for (key in dayWiseDuration) {
                //         var totalHours = convertMinsToHrsMins(dayWiseDuration[key]);
                //         totalHours = totalHours === '' ? '00:00' : totalHours;
                //         providerTotalHrs = Number(providerTotalHrs) + Number(dayWiseDuration[key]);
                //         strHTML = strHTML + '<td>' + totalHours + 'Hrs</td>';
                //     }
                //     strHTML = strHTML + '</tr>';
                // }


                strHTML = strHTML + '</tbody></table></div></div></div>';

                $("#divSUCareReport").append(strHTML);
            }
        }

    }


}


function onGetServiceUserCareReport(dataObj) {

    dtArray = [];
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.appointment) {
            if ($.isArray(dataObj.response.appointment)) {
                dtArray = dataObj.response.appointment;
            } else {
                dtArray.push(dataObj.response.appointment);
            }
        }
    }

    if (dtArray !== null && dtArray.length > 0) {

        var txtDateOfWeek = $("#txtDateOfWeek").data("kendoDatePicker");
        var selectedDate = txtDateOfWeek.value();

        var arrDatesInWeek = [];

        for (var i = 1; i <= 7; i++) {
            var first = selectedDate.getDate() - (selectedDate.getDay() + 1) + i;
            var day = new Date(selectedDate.setDate(first));

            arrDatesInWeek.push(day);
        }

        var allDaysInWeek = [];
        for (var counter4 = 0; counter4 < arrDatesInWeek.length; counter4++) {
            allDaysInWeek.push(daysOfWeek[arrDatesInWeek[counter4].getDay()] + " " + formatDate(arrDatesInWeek[counter4]));
        }

        var appointments = [];

        var lookup = {};
        var providerIds = [];

        for (var item, c = 0; item = dtArray[c++];) {
            if (item.composition && item.composition.provider && item.composition.provider.id) {

                var providerId = item.composition.provider.id;

                if (!(providerId in lookup)) {
                    lookup[providerId] = 1;
                    providerIds.push(providerId);
                }
            }
        }


        var staffAppointments = [];

        if (providerIds !== null && providerIds.length > 0) {

            for (var j = 0; j < providerIds.length; j++) {

                var providerObj = {};


                var tmpSUAppts = $.grep(dtArray, function (e) {
                    return e.composition.provider.id === providerIds[j];
                });

                if (tmpSUAppts !== null && tmpSUAppts.length > 0) {
                    if (tmpSUAppts[0].composition && tmpSUAppts[0].composition.provider && tmpSUAppts[0].composition.provider.id) {
                        providerObj.id = tmpSUAppts[0].composition.provider.id;
                        providerObj.providerName = tmpSUAppts[0].composition.provider.lastName + " " + tmpSUAppts[0].composition.provider.firstName;
                    }

                    // if (tmpSUAppts[0].composition && tmpSUAppts[0].composition.provider && tmpSUAppts[0].composition.provider.dateOfBirth) {

                    //     var dateArr = tmpSUAppts[0].composition.provider.dateOfBirth.split("-");
                    //     var dob = parseInt(dateArr[1]) + "-" + parseInt(dateArr[2]) + "-" + parseInt(dateArr[0]);
                    //     var DOB = new Date(dob);
                    //     var today = new Date();
                    //     var age = today.getTime() - DOB.getTime();
                    //     age = Math.floor(age / (1000 * 60 * 60 * 24 * 365.25));

                    //     providerObj.age = age;

                    // }
                    // if (tmpSUAppts[0].composition && tmpSUAppts[0].composition.provider && tmpSUAppts[0].composition.provider.gender) {
                    //     providerObj.gender = tmpSUAppts[0].composition.provider.gender;
                    // }

                    var suWeeklyAppts = [];


                    for (var counter = 0; counter < arrDatesInWeek.length; counter++) {

                        var obj = {};

                        var tmpDate = arrDatesInWeek[counter];
                        tmpDate.setHours(0, 0, 0, 0);


                        var tmpAppointments = $.grep(tmpSUAppts, function (e) {
                            var apptDate = new Date(e.dateOfAppointment);
                            apptDate.setHours(0, 0, 0, 0);
                            return apptDate.getTime();
                        });

                        obj.dateOfAppointment = allDaysInWeek[counter];
                        obj.dayAppointments = [];


                        if (tmpAppointments != null && tmpAppointments.length > 0) {
                            obj.dayAppointments = tmpAppointments;

                        }

                        suWeeklyAppts.push(obj);

                    }
                    providerObj.weeklyAppointments = suWeeklyAppts;
                }

                staffAppointments.push(providerObj);
            }
            console.log(JSON.stringify(staffAppointments));
            bindServiceUserCareReport(staffAppointments);
        }


    }
    else {
        customAlert.info("Info", "No records found");
    }
}


function bindServiceUserCareReport(staffAppointments) {

    $("#divSUCareReport").html("");
    if (staffAppointments !== null && staffAppointments.length > 0) {

        for (var i = 0; i < staffAppointments.length; i++) {

            var pObj = staffAppointments[i];

            var strHTML = '';

            strHTML = strHTML + '<div class="table-block">';
            strHTML = strHTML + '<div class="timeSheetReportdivHeading">';
            strHTML = strHTML + pObj.providerName;
            //strHTML = strHTML + '<span class="headingSpn floatRight">';
            //strHTML = strHTML + '(' + pObj.age + '/' + pObj.gender.charAt(0) + ')</span>';
            strHTML = strHTML + '</div>';
            strHTML = strHTML + '<div class="col-xs-12 taskReportTblBlock"><div class="table-responsive">';
            strHTML = strHTML + '<table class="tblTaskreport weeklyCareReportTbl"><thead><tr>';

            var arrAppointmentsCount = [];

            for (var j = 0; j < pObj.weeklyAppointments.length; j++) {

                strHTML = strHTML + '<th><span class="spnReportTitle">' + pObj.weeklyAppointments[j].dateOfAppointment.split(' ')[0] + '</th>';
                arrAppointmentsCount.push(pObj.weeklyAppointments[j].dayAppointments.length);
            }
            strHTML = strHTML + '</tr></thead>';
            strHTML = strHTML + '<tbody>';

            var columns = 7;
            var rows = parseInt(Math.max(...arrAppointmentsCount));

            var dayWiseDuration = {};

            for (var rownumber = 0; rownumber < rows; rownumber++) {

                strHTML = strHTML + '<tr>';

                for (var columnnumber = 0; columnnumber < columns; columnnumber++) {

                    var tempAppointment = pObj.weeklyAppointments[columnnumber].dayAppointments[rownumber];

                    var currentDay = pObj.weeklyAppointments[columnnumber].dateOfAppointment.split(' ')[0];

                    if (!(currentDay in dayWiseDuration)){
                        dayWiseDuration[currentDay] = 0;
                    }


                    var apptTimeDuration = "";

                    if (tempAppointment !== null && typeof (tempAppointment) !== "undefined") {

                        var appDate = new Date(tempAppointment.dateOfAppointment);

                        var strH = kendo.toString(appDate, "hh");
                        var strm = kendo.toString(appDate, "mm");
                        var stT = kendo.toString(appDate, "tt");

                        var time = strH;
                        if (strm.length > 0) {
                            time = time + ":" + strm;
                        }
                        time = time + stT;
                        time = time.toLowerCase();

                        apptTimeDuration = time + '<br/>' + tempAppointment.duration + 'min';

                        dayWiseDuration[currentDay] = dayWiseDuration[currentDay] + parseInt(tempAppointment.duration);

                    }


                    strHTML = strHTML + '<td>' + apptTimeDuration + '</td>';

                }

                strHTML = strHTML + '</tr>';
            }

            if(dayWiseDuration){
                strHTML = strHTML + '<tr class="TotalRow">';
                for(key in dayWiseDuration)   {
                    var totalHours = convertMinsToHrsMins(dayWiseDuration[key]);
                    totalHours = totalHours === '' ? '00:00' : totalHours;
                    strHTML = strHTML + '<td>'+totalHours+'Hrs</td>';
                }
                strHTML = strHTML + '</tr>';
            }


            strHTML = strHTML + '</tbody></table></div></div></div>';

            $("#divSUCareReport").append(strHTML);

        }

    }

}

function onStaffTimesheet(dataObj) {

    dtArray = [];

    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.staffTimesheets) {
            if ($.isArray(dataObj.response.staffTimesheets)) {
                dtArray = dataObj.response.staffTimesheets;
            } else {
                dtArray.push(dataObj.response.staffTimesheets);
            }
        }
    }
    if (dtArray && dtArray.length > 0) {

        console.log(dtArray);

        if ($("#rbtnOld").is(":checked")) {


            var staffTimeSheetSummary = [];


            dtArray.sort(function (x, y) {
                var date1 = new Date(x);
                var date2 = new Date(y);

                if (date1 > date2) return 1;
                if (date1 < date2) return -1;
                return 0;
            });

            var totalScheduledHours = 0, totalActualHours = 0;

            for (var counter = 0; counter <= dtArray.length - 1; counter++) {
                if (counter == 0 && dtArray[counter].firstName !== null && dtArray[counter].firstName !== "" && dtArray[counter].lastName !== null && dtArray[counter].lastName !== "") {
                    dtArray[counter].ProviderName = dtArray[counter].lastName + " " + dtArray[counter].firstName;
                }

                if (dtArray[counter].weeklyContractHours === null) {
                    dtArray[counter].weeklyContractHours = 0;
                }


                if (dtArray[counter].hourlyRate === null) {
                    dtArray[counter].hourlyRate = 0;
                }

                if (dtArray[counter].dateOfAppointment) {
                    var date = new Date(GetDateTimeEditDay(dtArray[counter].dateOfAppointment));
                    var day = date.getDay();
                    var dow = getWeekDayName(day);

                    dtArray[counter].dateTimeOfAppointment = dow + " " + GetDateTimeEdit(dtArray[counter].dateOfAppointment);
                    var dtEnd = dtArray[counter].dateOfAppointment + (Number(dtArray[counter].duration) * 60000);
                    dtArray[counter].DW = dow;
                    var dateED = new Date(GetDateTimeEditDay(dtEnd));
                    var dayED = dateED.getDay();
                    var dowED = getWeekDayName(dayED);
                    dtArray[counter].dateTimeOfAppointmentED = GetDateTimeEdit(dtEnd);
                    dtArray[counter].dow = dowED;

                }

                if (dtArray[counter].duration) {
                    var totalMinutes = dtArray[counter].duration;

                    var hours = Math.floor(totalMinutes / 60);
                    var minutes = Math.round(totalMinutes % 60);
                    if (hours.toString().length == 1) {
                        hours = "0" + hours;
                    }
                    if (minutes.toString().length == 1) {
                        minutes = "0" + minutes;
                    }


                    dtArray[counter].durationTime = hours + ":" + minutes;
                    totalScheduledHours = totalScheduledHours + dtArray[counter].duration;
                }
                if (dtArray[counter].actualDuration) {
                    var totalMinutes = dtArray[counter].actualDuration;

                    var hours = Math.floor(totalMinutes / 60);
                    var minutes = Math.round(totalMinutes % 60);
                    if (hours.toString().length == 1) {
                        hours = "0" + hours;
                    }
                    if (minutes.toString().length == 1) {
                        minutes = "0" + minutes;
                    }


                    dtArray[counter].actualDurationTime = hours + ":" + minutes;
                    totalActualHours = totalActualHours + dtArray[counter].actualDuration;
                }
                if (counter == 0) {
                    dtArray[counter].Provider = dtArray[counter].ProviderName + '- ' + dtArray[counter].weeklyContractHours;
                }
            }



            var strTotalScheduledHours = "", strTotalActualHours = "";
            if (totalScheduledHours) {
                var hours = Math.floor(totalScheduledHours / 60);
                var minutes = Math.round(totalScheduledHours % 60);
                if (hours.toString().length == 1) {
                    hours = "0" + hours;
                }
                if (minutes.toString().length == 1) {
                    minutes = "0" + minutes;
                }

                strTotalScheduledHours = hours + ":" + minutes;
            }

            if (totalActualHours) {
                var hours = Math.floor(totalActualHours / 60);
                var minutes = Math.round(totalActualHours % 60);
                if (hours.toString().length == 1) {
                    hours = "0" + hours;
                }
                if (minutes.toString().length == 1) {
                    minutes = "0" + minutes;
                }

                strTotalActualHours = hours + ":" + minutes;
            }


            var objTotal = {};
            objTotal.ProviderName = "";
            objTotal.weeklyContractHours = "";
            objTotal.dateTimeOfAppointment = "";
            objTotal.dow = "Total";
            objTotal.durationTime = strTotalScheduledHours;
            objTotal.actualDurationTime = strTotalActualHours;

            dtArray.push(objTotal);
            buildDeviceListGrid(dtArray);
            $("#divOldTimeSheetReportTableBlock").show();
        }
        else{

            var reportViewerJson = [];
            var lookup = {};
            var providerIds = [];
            var grandTotalScheduledHours = 0, grandTotalActualHours = 0;

            for (var item, c = 0; item = dtArray[c++];) {
                var providerId = item.providerId;

                if (!(providerId in lookup)) {
                    lookup[providerId] = 1;
                    providerIds.push(providerId);
                }
            }

            debugger;

            if (providerIds !== null && providerIds.length > 0) {
                for (var i = 0; i < providerIds.length; i++) {
                    var report={};
                    console.log(providerIds[i]);
                    var staffAppts = $.grep(dtArray, function (e, index) {

                        return Number(e.providerId) === Number(providerIds[i]);
                    });


                    if (staffAppts !== null && staffAppts.length > 0) {

                        if (staffAppts[0].providerFirstName !== null && staffAppts[0].providerFirstName !== "" && staffAppts[0].providerLastName !== null && staffAppts[0].providerLastName) {
                            report.ProviderName = staffAppts[0].providerFirstName + " " + staffAppts[0].providerLastName;

                            if (report.ProviderName.toLowerCase() === "u a") {
                                report.ProviderName = "Unallocated Appointments";
                            }

                        }

                        report.dataSource = [];
                        var duration = 0, actualDuration = 0;
                        for (var j = 0; j < staffAppts.length; j++) {
                            var tmpStaffAppt = {};

                            if (staffAppts[j].patientFirstName !== null && staffAppts[j].patientFirstName !== "" && staffAppts[j].patientLastName !== null && staffAppts[j].patientLastName) {
                                tmpStaffAppt.ServiceUser = staffAppts[j].patientFirstName + " " + staffAppts[j].patientLastName;
                            }



                            var dateTimeOfAppointment;
                            if (staffAppts[j].dateOfAppointment) {
                                var date = new Date(GetDateTimeEditDay(staffAppts[j].dateOfAppointment));
                                var day = date.getDay();
                                var dow = getWeekDayName(day);

                                dateTimeOfAppointment = dow + " " + GetDateTimeEdit(staffAppts[j].dateOfAppointment);

                            }
                            tmpStaffAppt.dateTimeOfAppointment = dateTimeOfAppointment;


                            if (staffAppts[j].duration !== null) {
                                duration = duration + staffAppts[j].duration;
                            }

                            if (staffAppts[j].actualDuration !== null) {
                                actualDuration = actualDuration + staffAppts[j].actualDuration;
                            }

                            if (staffAppts[j].duration) {

                                grandTotalScheduledHours = grandTotalScheduledHours + staffAppts[j].duration;

                                var totalScheduledMinutes = staffAppts[j].duration;

                                var hoursScheduled = Math.floor(totalScheduledMinutes / 60);
                                var minutesScheduled = Math.round(totalScheduledMinutes % 60);
                                if (hoursScheduled.toString().length == 1) {
                                    hoursScheduled = "0" + hoursScheduled;
                                }
                                if (minutesScheduled.toString().length == 1) {
                                    minutesScheduled = "0" + minutesScheduled;
                                }


                                tmpStaffAppt.ScheduledHours = hoursScheduled + ":" + minutesScheduled;
                            }
                            else{
                                tmpStaffAppt.ScheduledHours = "00:00";
                            }

                            if (staffAppts[j].actualDuration) {
                                grandTotalActualHours = grandTotalActualHours + staffAppts[j].actualDuration;

                                var totalActualMinutes = staffAppts[j].actualDuration;

                                var actualHours = Math.floor(totalActualMinutes / 60);
                                var actualMinutes = Math.round(totalActualMinutes % 60);
                                if (actualHours.toString().length == 1) {
                                    actualHours = "0" + actualHours;
                                }
                                if (actualMinutes.toString().length == 1) {
                                    actualMinutes = "0" + actualMinutes;
                                }


                                tmpStaffAppt.ActualHours = actualHours + ":" + actualMinutes;
                            }
                            else{
                                tmpStaffAppt.ActualHours = "00:00";
                            }

                            report.dataSource.push(tmpStaffAppt);
                        }

                        if (duration) {
                            var overallTotalScheduledMinutes = duration;

                            var overallHoursScheduled = Math.floor(overallTotalScheduledMinutes / 60);
                            var overallMinutesScheduled = Math.round(overallTotalScheduledMinutes % 60);
                            if (overallHoursScheduled.toString().length == 1) {
                                overallHoursScheduled = "0" + overallHoursScheduled;
                            }
                            if (overallMinutesScheduled.toString().length == 1) {
                                overallMinutesScheduled = "0" + overallMinutesScheduled;
                            }


                            report.totalScheduledHours = overallHoursScheduled + ":" + overallMinutesScheduled;
                        }
                        else{
                            report.totalScheduledHours = "00:00";
                        }

                        if (actualDuration) {
                            var overallTotalActualMinutes = actualDuration;

                            var overallActualHours = Math.floor(overallTotalActualMinutes / 60);
                            var overallActualMinutes = Math.round(overallTotalActualMinutes % 60);
                            if (overallActualHours.toString().length == 1) {
                                overallActualHours = "0" + overallActualHours;
                            }
                            if (overallActualMinutes.toString().length == 1) {
                                overallActualMinutes = "0" + overallActualMinutes;
                            }


                            report.totalActualHours = overallActualHours + ":" + overallActualMinutes;
                        }
                        else{
                            report.totalActualHours = "00:00";
                        }

                    }
                    reportViewerJson.push(report);
                }
            }

            if (reportViewerJson !== null && reportViewerJson.length > 0) {

                generateReportTables(reportViewerJson);
                var tmpScheduledHours = "", tmpActualHours="";
                if (grandTotalScheduledHours > 0) {

                    var totalScheduledMinutes = grandTotalScheduledHours;

                    var hoursScheduled = Math.floor(totalScheduledMinutes / 60);
                    var minutesScheduled = Math.round(totalScheduledMinutes % 60);
                    if (hoursScheduled.toString().length == 1) {
                        hoursScheduled = "0" + hoursScheduled;
                    }
                    if (minutesScheduled.toString().length == 1) {
                        minutesScheduled = "0" + minutesScheduled;
                    }


                    tmpScheduledHours = hoursScheduled + ":" + minutesScheduled;
                }
                else{
                    tmpScheduledHours = "00:00";
                }



                if (grandTotalActualHours) {

                    var totalActualMinutes = grandTotalActualHours;

                    var actualHours = Math.floor(totalActualMinutes / 60);
                    var actualMinutes = Math.round(totalActualMinutes % 60);
                    if (actualHours.toString().length == 1) {
                        actualHours = "0" + actualHours;
                    }
                    if (actualMinutes.toString().length == 1) {
                        actualMinutes = "0" + actualMinutes;
                    }


                    tmpActualHours = actualHours + ":" + actualMinutes;
                }
                else{
                    tmpActualHours = "00:00";
                }

                $("#divGrandTotalHours").show();
                $("#tdGrandTotaScheduledHours").text(tmpScheduledHours);
                $("#tdGrandTotaActualHours").text(tmpActualHours);

            }
        }
    }
    else {
        customAlert.info("Info", "No timesheet details");
    }
}

function generateReportTables(reportViewerJson){
    if(reportViewerJson != null && reportViewerJson.length > 0){
        var insertAfterElement="";
        for(var counter = 0;counter < reportViewerJson.length;counter++){
            var reportDisplayName = reportViewerJson[counter].ProviderName;
            var reportDisplayNameId = "StaffTimeSheet" + (counter+1);
            var $report = $("<div class=\"col-xs-12 table-block\" id=\"div"+reportDisplayNameId+"\"> <div class=\"col-xs-12 timeSheetReportdivHeading\" style='page-break-before: always !important;'>"+reportDisplayName+" </div><div class=\"table-responsive\"> <table class=\"timesheetmembersTable\" id=\"tbl"+reportDisplayNameId+"\"> </table> </div> </div>");

            $("#divStaffTimeSheetReportViewer").append($report);
            bindTimeSheetReport(reportDisplayNameId,reportViewerJson[counter]);
        }
    }
}


function bindTimeSheetReport(reportDisplayNameId, reportData) {
    var dataObj = reportData.dataSource;

    var tableId = "#tbl"+reportDisplayNameId;


    var strHeading = "<thead>";
    var columns = [];
    if (dataObj != null && dataObj.length > 0) {
        var firstObj = dataObj[0];
        var c = 0;
        for (var key in firstObj) {
            if(key.toLowerCase() == "appt date time") {
                strHeading = strHeading + "<tr style=\"page-break-inside: avoid\"><th class=\"borderbuttomthin\" style='width:12%'>" + key + "</th>";
            }else{
                if (c === 0) {
                    strHeading = strHeading + "<th></th>";
                }
                else{
                    if(key.toLowerCase() == "serviceuser"){
                        strHeading = strHeading + "<th class='borderbuttomthin'>Service User</th>";
                    }
                    else if(key.toLowerCase() == "datetimeofappointment"){
                        strHeading = strHeading + "<th class='borderbuttomthin'>Appointment Date Time</th>";
                    }
                    else if(key.toLowerCase() == "scheduledhours"){
                        strHeading = strHeading + "<th class='borderbuttomthin'>Scheduled Hrs</th>";
                    }
                    else if(key.toLowerCase() == "actualhours"){
                        strHeading = strHeading + "<th class='borderbuttomthin'>Actual Hrs</th></tr>";
                    }
                }

            }
            columns.push(key);
            c = c + 1;
        }
    }
    strHeading = strHeading+"</thead>";

    $(tableId).append(strHeading);


    var strTableBody = "<tbody>";

    for(var counter = 0;counter<dataObj.length;counter++){
        var strTableRow = "<tr style=\"page-break-inside: avoid\">";

        for(var counter2= 0;counter2<columns.length;counter2++){
            if (counter2 > 0 && counter === dataObj.length-1) {
                strTableRow = strTableRow + "<td class=\"borderbuttomthick\">"+ dataObj[counter][columns[counter2]]+"</td>";
            }
            else{
                strTableRow = strTableRow + "<td>"+ dataObj[counter][columns[counter2]]+"</td>";
            }


        }
        strTableRow = strTableRow+"</tr>";

        strTableBody = strTableBody +strTableRow;
    }

    var totalRow = '<tr class="timesheetmembersTable-total" style="page-break-inside: avoid"><td></td><td align="left">Total Scheduled &amp; Actual Hours</td><td class=\"showgrandtotal\">'+reportData.totalScheduledHours+'</td><td class=\"showgrandtotal\">'+reportData.totalActualHours+'</td></tr>';
    strTableBody = strTableBody+totalRow;
    strTableBody = strTableBody+"</tbody>";
    $(tableId).append(strTableBody);


}

function onPatientListData(dataObj) {
    dtArray = [];
    buildDeviceListGrid([]);
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.appointment) {
            if ($.isArray(dataObj.response.appointment)) {
                dtArray = dataObj.response.appointment;
            } else {
                dtArray.push(dataObj.response.appointment);
            }
        }
    }
    dtArray.sort(function (a, b) {
        var nameA = a.dateOfAppointment; // ignore upper and lowercase
        var nameB = b.dateOfAppointment; // ignore upper and lowercase
        if (nameA < nameB) {
            return -1;
        }
        if (nameA > nameB) {
            return 1;
        }
        // names must be equal
        return 0;
    });

    if (dtArray.length > 0) {
        for (var i = 0; i < dtArray.length; i++) {
            // $("#btnCopy").css("display", "");
            dtArray[i].idk = dtArray[i].id;
            dtArray[i].name = dtArray[i].composition.patient.lastName + " " + dtArray[i].composition.patient.firstName + " " + dtArray[i].composition.patient.middleName;


            var date = new Date(GetDateTimeEditDay(dtArray[i].dateOfAppointment));
            var day = date.getDay();
            var dow = getWeekDayName(day);

            var dtEnd = dtArray[i].dateOfAppointment + (Number(dtArray[i].duration) * 60000);

            dtArray[i].dateTimeOfAppointment = dow + " " + GetDateTimeEdit(dtArray[i].dateOfAppointment);
            dtArray[i].DW = dow;

            var dateED = new Date(GetDateTimeEditDay(dtEnd));
            var dayED = dateED.getDay();
            var dowED = getWeekDayName(dayED);

            dtArray[i].dateTimeOfAppointmentED = dowED + " " + GetDateTimeEdit(dtEnd);


            var dt = new Date(dtArray[i].dateOfAppointment);
            var strDT = "";
            if (dt) {
                strDT = kendo.toString(dt, "MM/dd/yyyy h:mm tt");
            }

            dtArray[i].dateTimeOfAppointment1 = strDT;


            dtArray[i].appointmentReasonDesc = dtArray[i].composition.appointmentReason.desc;
            dtArray[i].appointmentTypeDesc = dtArray[i].composition.appointmentType.desc;
            if (dtArray[i].providerId != 0) {
                dtArray[i].staffName = dtArray[i].composition.provider.lastName + " " + dtArray[i].composition.provider.firstName + " " + (dtArray[i].composition.provider.middleName != null ? dtArray[i].composition.provider.middleName : " ");
            } else {
                dtArray[i].staffName = "";
            }
        }


        buildDeviceListGrid(dtArray);
    }
    else {
        // $("#btnCopy").css("display", "none");
        customAlert.info("Info", "No appointments");
    }
}

function onClickCopy() {
    var selGridData = angularUIgridWrapper.getAllRows();
    var selList = [];
    for (var i = 0; i < selGridData.length; i++) {

        var dataRow = selGridData[i].entity;
        dataRow.appointmentType = "Con";
        dataRow.appointmentTypeDesc = "Confirmed";
        dataRow.composition.appointmentType.desc = "Confirmed";
        dataRow.composition.appointmentType.id = 7;
        dataRow.composition.appointmentType.value = "Con";
        selList.push(dataRow);
    }

    // if(selList.length>0){
    sessionStorage.setItem("appselList", JSON.stringify(selList));
    parentRef.appselList = selList;
    var popW = "60%";
    var popH = "80%";
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Copy To Appointments";
    parentRef.selValue = allDatesInWeek;
    devModelWindowWrapper.openPageWindow("../../html/masters/createCopyAppointment.html", profileLbl, popW, popH, true, copyAppointment);
    parent.setPopupWIndowHeaderColor("0bc56f", "FFF");
    // }
}
function copyAppointment(dataObj) {
    parentRef.selValue = null;
    parentRef.appselList = null;
    onClickSubmit();
}

function onCloseEditAppointment(dataObj) {
    parentRef.selectedItems = null;
    buildDeviceListGrid([]);
    onClickViewServiceUserCareReport();

}


function getWeekDayName(wk) {
    var wn = "";
    if (wk == 1) {
        return "Mon";
    } else if (wk == 2) {
        return "Tue";
    } else if (wk == 3) {
        return "Wed";
    } else if (wk == 4) {
        return "Thu";
    } else if (wk == 5) {
        return "Fri";
    } else if (wk == 6) {
        return "Sat";
    } else if (wk == 0) {
        return "Sun";
    }
    return wn;
}

function getFromDate(fromDate) {


    var day = fromDate.getDate();
    var month = fromDate.getMonth();
    month = month + 1;
    var year = fromDate.getFullYear();

    var strDate = month + "/" + day + "/" + year;
    strDate = strDate + " 00:00:00";

    var date = new Date(strDate);
    var strDateTime = date.getTime();

    return strDateTime;
}

function getToDate(toDate) {


    var day = toDate.getDate();
    var month = toDate.getMonth();
    month = month + 1;
    var year = toDate.getFullYear();

    var strDate = month + "/" + day + "/" + year;
    strDate = strDate + " 23:59:59";

    var date = new Date(strDate);
    var strDateTime = date.getTime();

    return strDateTime;
}

var sortByDateAsc = function (x, y) {
    // This is a comparison function that will result in dates being sorted in
    // ASCENDING order. As you can see, JavaScript's native comparison operators
    // can be used to compare dates. This was news to me.

    var date1 = new Date(x.dateOfAppointment);
    var date2 = new Date(y.dateOfAppointment);

    if (date1 > date2) return 1;
    if (date1 < date2) return -1;
    return 0;
};

function formatDate(date) {

    var dd = date.getDate();

    var mm = date.getMonth() + 1;
    var yyyy = date.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }

    if (mm < 10) {
        mm = '0' + mm;
    }

    return dd + '/' + mm + '/' + yyyy;
}

function getLanguageValueList(dataObj) {
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.codeTable) {
        if ($.isArray(dataObj.response.codeTable)) {
            dataArray = dataObj.response.codeTable;
        } else {
            dataArray.push(dataObj.response.codeTable);
        }
    }
    for (var i = 0; i < dataArray.length; i++) {
        dataArray[i].idk = dataArray[i].id;
        $("#cmbLang").append('<option value="' + dataArray[i].idk + '">' + dataArray[i].desc + '</option>');
    }
}

function getGenderValueList(dataObj) {
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.codeTable) {
        if ($.isArray(dataObj.response.codeTable)) {
            dataArray = dataObj.response.codeTable;
        } else {
            dataArray.push(dataObj.response.codeTable);
        }
    }
    for (var i = 0; i < dataArray.length; i++) {
        dataArray[i].idk = dataArray[i].id;
        $("#cmbGender").append('<option value="' + dataArray[i].idk + '">' + dataArray[i].desc + '</option>');
    }
}


function getPatientList(dataObj){
    var tempArray = [];
    var dataArray = [];
    suArray= [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.patient){
            if($.isArray(dataObj.response.patient)){
                tempArray = dataObj.response.patient;
                suArray = dataObj.response.patient;
            }else{
                tempArray.push(dataObj.response.patient);
            }
        }
    }else{
        var msg = dataObj.response.status.message;
        customAlert.error("Error",msg.replace("patient","service user"));
    }
    suArray = tempArray;
    tempArray.sort(function (a, b) {
        var p1 = a.lastName+ " " + a.firstName;
        var p2 = b.lastName+ " " + b.firstName;

        if (p1 < p2) return -1;
        if (p1 > p2) return 1;
        return 0;
    });

    for(var i=0;i<tempArray.length;i++){
        var obj = tempArray[i];
        if(obj){
            var suName = obj.lastName + " " + obj.firstName;
            if(suName != null && suName != " ") {
                $("#cmbServiceUser").append('<option value="' + obj.id + '">' + suName + '</option>');
            }
        }
    }

    $("#cmbServiceUser").multipleSelect({
        selectAll: true,
        width: 200,
        dropWidth: 200,
        multipleWidth: 200,
        placeholder: 'Select Service User',
        selectAllText: 'All'
    });

}

function onClickServiceUser(){
    var popW = 800;
    var popH = 450;

    if(parentRef)
        parentRef.searchZip = true;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Service User";
    devModelWindowWrapper.openPageWindow("../../html/patients/searchPatient.html", profileLbl, popW, popH, true, onCloseServiceUserAction);
}
var selPatientId = 0;
var suArray = [];
function onCloseServiceUserAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        selPatientId = returnData.selItem.PID;
        $('#txtServiceUser').val(returnData.selItem.LN + " "+ returnData.selItem.FN);
        getAjaxObject(ipAddress+"/patient/"+selPatientId,"GET",onGetPatientInfo,onError);
        //getAjaxObject(ipAddress + "/patient/list/?is-active=1&is-deleted=0&id="+selPatientId, "GET", getPatientList, onError);


    }
}

function onGetPatientInfo(dataObj) {
    suArray= [];
    if (dataObj && dataObj.response && dataObj.response.patient) {
        suArray.push(dataObj.response.patient);
    }
}
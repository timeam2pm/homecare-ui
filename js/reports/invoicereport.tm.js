var angularPTUIgridWrapper = null;
var dataArray = [];
var appReport = angular.module('appReport', []);

appReport.controller('reportController', function($scope) {
	$scope.getData = function () {
        onClickPtViews();
	}
    $scope.printTable = function () {

        var divToPrint = document.getElementById("carertable");

        var htmlToPrint = '' +
            '<style type="text/css">' +
            'table , tr, td, th{'+
    			'border:1px solid #000'+
    			'font-weight:bold'+
    			'font-size:13px'+
    		'}'+
            'table{' +
            'font-family: arial, sans-serif'+
            'border-collapse: collapse'+
            'width: 100%'+
			';' +
            'padding;0.5em;' +
            'border-collapse:collapse;'+
            '}' +
            '</style>';
        htmlToPrint += divToPrint.outerHTML;

        // var newWin = window.open("");
        // newWin.document.write(htmlToPrint);
        // newWin.document.close();
        // newWin.focus();
        // newWin.print();
        // newWin.close();

        // var newWin  = window.open("");
        // newWin.document.body.innerHTML = htmlToPrint;
        // newWin.focus();
        // newWin.print();
        // newWin.close();


        window.frames["print_frame"].document.body.innerHTML = htmlToPrint;
        window.frames["print_frame"].window.focus();
        window.frames["print_frame"].window.print();

    }
});


$(document).ready(function(){
    $("#divMain",parent.document).css("display","none");
    $("#divPatInfo",parent.document).css("display","");
    $("#lblTitleName",parent.document).html("Invoice Report");
});

$(window).load(function(){
	parentRef = parent.frames['iframe'].window;
	onMessagesLoaded();
});
function adjustHeight(){
	var defHeight = 160;
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 100;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    try{angularPTUIgridWrapper.adjustGridHeight(cmpHeight);}catch(e){};
    
}
function onMessagesLoaded() {
	buttonEvents();
	init();
}
var cntry = "";
var dtFMT = "dd/MM/yyyy";
var stDate = "";
var endDate = "";

function init(){
	//patientId = parentRef.patientId;
	//$("#dtFromDate").kendoDatePicker({value:new Date()});
	//$("#dtToDate").kendoDatePicker({value:new Date()});

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();


	getAjaxObject(ipAddress + "/facility/list/?is-active=1", "GET", getFacilityList, onError);
	
	cntry = sessionStorage.countryName;
	//getFacilityList();
	 if(cntry.indexOf("India")>=0){
		 dtFMT = INDIA_DATE_FMT;
	 }else  if(cntry.indexOf("United Kingdom")>=0){
		 dtFMT = ENG_DATE_FMT;
	 }else  if(cntry.indexOf("United State")>=0){
		 dtFMT = US_DATE_FMT;
	 }
	 
	 var currDate = kendo.toString(new Date(), dtFMT);
	 $("#txtRunOn").val(currDate);
	$("#txtStartDate").kendoDatePicker({format:dtFMT});
	$("#txtEndDate").kendoDatePicker({format:dtFMT});
	
	startDT = $("#txtStartDate").kendoDatePicker({
         change: startChange,format:dtFMT,value:new Date()
     }).data("kendoDatePicker");

     endDT= $("#txtEndDate").kendoDatePicker({
         change: endChange,format:dtFMT,value:new Date(),max:new Date()
     }).data("kendoDatePicker");

     endDT.min(new Date());

    // $("#txtStartDate").kendoDatePicker({value:new Date()});
    // $("#txtEndDate").kendoDatePicker({value:new Date()});
     // var dataOptionsPT = {
		//         pagination: false,
		//         paginationPageSize: 500,
		// 		changeCallBack: onPTChange
		//     }
     // angularPTUIgridWrapper = new AngularUIGridWrapper("careerGrid1", dataOptionsPT);
	  //   angularPTUIgridWrapper.init();
		//  buildPatientRosterList([]);
		 
     // adjustHeight();
     
     getBillPlans();
	 getPatientList();
    
}

function getPatientList(){
	var patientListURL = ipAddress+"/patient/list/?is-active=1";
		// patientListURL = ipAddress+"/patient/list/"+sessionStorage.userId;
	if(sessionStorage.userTypeId == "200" || sessionStorage.userTypeId == "100"){
		patientListURL = ipAddress+"/patient/list/?is-active=1";
	}
//	var patientListURL = ipAddress+"/patient/list/"+sessionStorage.userId;
	 getAjaxObject(patientListURL,"GET",onPatientListData,onErrorMedication);
}

function onPatientListData(dataObj){
	console.log(dataObj);
	var tempArray = [];
	var dataArray = [];
	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		if(dataObj.response.patient){
			if($.isArray(dataObj.response.patient)){
				tempArray = dataObj.response.patient;
			}else{
				tempArray.push(dataObj.response.patient);
			}	
		}
	}else{
		customAlert.error("Error",dataObj.response.status.message.replace('patient','service user'));
	}
	
	if (tempArray !== null && tempArray.length > 0) {

		$("#cmbServiceUser").append('<option value="-1">All</option>')

		for(var i=0;i<tempArray.length;i++){
			var obj = tempArray[i];
			if(obj){
				var dataItemObj = {};
				dataItemObj.PID = obj.id;
				dataItemObj.FN = obj.firstName;
				dataItemObj.LN = obj.lastName;
				dataItemObj.WD = obj.weight;
				dataItemObj.HD = obj.height;
				if(obj.middleName){
					dataItemObj.MN = obj.middleName;
				}else{
					dataItemObj.MN =  "";
				}
				
				dataArray.push(dataItemObj);

				var SUName = obj.lastName + " " + obj.firstName + " " + obj.middleName;

				$("#cmbServiceUser").append('<option value="' + obj.id + '">' + SUName + '</option>');
			}
		}
	}
}

function onErrorMedication(errobj){
	console.log(errobj);
}

function getBillPlans(){
	var ipUrl = "";
	ipUrl = ipAddress+"/carehome/bill-to/?is-active=1&is-deleted=0&fields=*,whomToBill.value";
	getAjaxObject(ipUrl,"GET",getBillDataList,onError);
}
function getBillDataList(dataObj){
	var dataArray = [];
	if(dataObj){
		if($.isArray(dataObj.response.billTo)){
			dataArray = dataObj.response.billTo;
		}else{
			dataArray.push(dataObj.response.billTo);
		}
	}
	if(dataArray.length>0){
		dataArray.unshift({id:"",name:""});
	}

	for (var i = 0; i < dataArray.length; i++) {
		if (dataArray[i].id && dataArray[i].id > 0 && dataArray[i].name && dataArray[i].name !== "") {
			$("#cmbBillName").append('<option value="' + dataArray[i].id + '">' + dataArray[i].name + '</option>');	
		}
	}

	
	//onClickPtViews();
	
}
function buildPatientRosterList(dataSource){
	var gridColumns = [];
	var otoptions = {};
	otoptions.noUnselect = false;
	otoptions.rowHeight = 100; 
	gridColumns.push({
        "title": "Carer",
        "field": "carer",
    });
	  gridColumns.push({
	        "title": "Service User",
	        "field": "ServiceUser",
	    });
	  gridColumns.push({
	        "title": "Service User Location",
	        "field": "address",
	        "width":"25%"
	    });
	  gridColumns.push({
	        "title": "Appointment Time",
	        "field": "appTime",
	        "width":"15%"
	    });
	  gridColumns.push({
	        "title": "Time In",
	        "field": "inTime",
	    });
	  gridColumns.push({
	        "title": "In GPS Location",
	        "field": "inLocation",
	    });
	  gridColumns.push({
	        "title": "Time Out",
	        "field": "outTime",
	    });
	  gridColumns.push({
	        "title": "Out GPS Location",
	        "field": "outLocation",
	    });
	   gridColumns.push({
	        "title": "Actual Visit Time",
	        "field": "visitTime",
	    });
 
    angularPTUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
}
function getReportDataList(dataObj){

    dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            if (dataObj.response.invoiceRecords) {
                if ($.isArray(dataObj.response.invoiceRecords)) {
                    dataArray = dataObj.response.invoiceRecords;
                } else {
                    dataArray.push(dataObj.response.invoiceRecords);
                }
            }
        }
    }
    $("#divInvoiceReports").empty();
    if (dataArray.length > 0) {
		  var strTable = '<table class="context-menu-one" style="border:1px solid #000">';
		  var strHtml = '';
        for (var d = 0; d < dataArray.length; d++) {

			var dItem = dataArray[d];
        	if(dItem){


				if(dItem.billTo && dItem.patient){

					var billTo = dItem.billTo;
					var patient = dItem.patient;

					strHtml = strHtml + '<div class="invoiceReportTableContent"> <table class="invoiceTotbl"> <tr> <td> <u><b>Invoice To</b></u>';
					strHtml = strHtml +'<address>';
					strHtml = strHtml +billTo.name+'<br/>';
					strHtml = strHtml +billTo.address1+' '+billTo.address2+'<br/>';
					strHtml = strHtml +billTo.city+'<br/>';
					strHtml = strHtml +billTo.state+'<br/>';
					strHtml = strHtml +billTo.country+'<br/>';
					strHtml = strHtml +'</address></td>';
					strHtml = strHtml +'</tr></table></div>';



					strHtml = strHtml + '<div class="invoiceReportTableContent"> <table class="invoiceTotbl"> <tr> <td>';
					strHtml = strHtml +'<span class="spnServiceUser">Service User Name: '+patient.fristName+' '+patient.middleName+' '+patient.lastName+'</span></td></tr>';

					var suAddress = '';

					if (patient.address1 && patient.address1 !== '') {
						suAddress = suAddress + patient.address1 + ', ';
					}
					if (patient.address2 && patient.address2 !== '') {
						suAddress = suAddress + patient.address2 + ', ';
					}
					if (patient.city && patient.city !== '') {
						suAddress = suAddress + patient.city + ', ';
					}
					if (patient.state && patient.state !== '') {
						suAddress = suAddress + patient.state + ', ';
					}
					if (patient.country && patient.country !== '') {
						suAddress = suAddress + patient.country + ', ';
					}

					if (suAddress && suAddress !== '') {
						strHtml = strHtml +'<tr><td>';
						strHtml = strHtml +'<span  class="spnServiceUser" id="spnServiceUserAddress">Service User Address: '+suAddress+'</span>';
						strHtml = strHtml +'</td></tr>';
					}

					strHtml = strHtml +'</table></div>';

				}

				var dArr = [];
        		if(dItem.appointments){
        			if($.isArray(dItem.appointments)){
        				dArr = dItem.appointments;
        			}else{
        				dArr.push(dItem.appointments);
        			}
				}

				if (dArr && dArr.length > 0) {
					
					var totalNetAmount = 0,vat = 0,grossTotalAmount = 0;

					strHtml = strHtml + '<div class="invoiceReportTableContent"> <table class="invoiceTimeSheetTable"> <tr> <th>Details of Care</th> <th>Actual Hrs</th> <th>Rate</th> <th>Total Charge</th> </tr>';

					for(var j=0;j<dArr.length;j++){
						var jItem = dArr[j];
						if(jItem){
							strHtml = strHtml +'<tr>'
							strHtml = strHtml +'<td>'+jItem.appointmentDate+'</td>';
							var actualHours;
							var total 
							if (jItem.duration) {
								var totalMinutes = jItem.duration;

								var hours = Math.floor(totalMinutes / 60);
								var minutes = Math.round(totalMinutes % 60);
					
								actualHours = hours + "." + minutes;

								strHtml = strHtml +'<td>'+actualHours+'</td>';

							}
							strHtml = strHtml +'<td>'+jItem.patientBillingRate+'</td>';
							strHtml = strHtml +'<td>'+Number(jItem.rate)+'</td>';
							strHtml = strHtml +'</tr>';
							totalNetAmount = totalNetAmount + Number(jItem.rate);
							
						}
					}

					if (totalNetAmount) {
						vat = (totalNetAmount * 20)/100;
					}
					if (totalNetAmount && vat) {
						grossTotalAmount = Number(totalNetAmount) + Number(vat);
					}
					

					strHtml = strHtml + '<tr><td> </td><td> </td><td><b>Total Net Amount:</b></td><td><b>'+totalNetAmount+'</b></td></tr>';
					strHtml = strHtml + '<tr><td> </td><td> </td><td><b>Total VAT Amount:</b></td><td><b>'+vat+'</b></td></tr>';
					strHtml = strHtml + '<tr><td> </td><td> </td><td><b>Gross Total Amount:</b></td><td><b>'+grossTotalAmount+'</b></td></tr>';
					strHtml = strHtml + '</table></div>';
				}
		}

        	
		
		
		$("#divInvoiceReports").html(strHtml);
		$("#invoiceReportPopup").show();
    }


   /* var elem = angular.element(document.querySelector('[ng-app]'));
    var injector = elem.injector();
    var $rootScope = injector.get('$rootScope');
    $rootScope.$apply(function(){
        $rootScope.apidata = dataArray;
    });*/




	// buildPatientRosterList(dataArray);
}
}
function secondsToHms(d) {
    d = Number(d);
    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    var hDisplay = h > 0 ? h + (h == 1 ? " hour, " : " hours, ") : "";
    var mDisplay = m > 0 ? m + (m == 1 ? " minute, " : " minutes, ") : "";
    var sDisplay = s > 0 ? s + (s == 1 ? " second" : " seconds") : "";
    return hDisplay + mDisplay + sDisplay; 
}
function onPTChange(){
	
}
function startChange() {
    var startDate = startDT.value(),
    endDate = endDT.value();

    if (startDate) {
        startDate = new Date(startDate);
        startDate.setDate(startDate.getDate());
        endDT.min(startDate);
    } else if (endDate) {
        startDT.max(new Date(endDate));
    } else {
        endDate = new Date();
        startDT.max(endDate);
        endDT.min(endDate);
    }
}

function endChange() {
    var endDate = endDT.value(),
    startDate = startDT.value();

    if (endDate) {
        endDate = new Date(endDate);
        endDate.setDate(endDate.getDate());
        startDT.max(endDate);
    } else if (startDate) {
        endDT.min(new Date(startDate));
    } else {
        endDate = new Date();
        startDT.max(endDate);
        endDT.min(endDate);
    }
}
function buttonEvents(){
	$("#btnSearch1").off("click",onClickSearch);
	$("#btnSearch1").on("click",onClickSearch);
	
	$("#btnPtView").off("click",onClickPtViews);
	$("#btnPtView").on("click",onClickPtViews);
	
	$("#btnPrint").off("click",onClickPrint);
	$("#btnPrint").on("click",onClickPrint);
}
function onClickSearch(){
	 var popW = "60%";
	    var popH = 500;

	    var profileLbl;
	    var devModelWindowWrapper = new kendoWindowWrapper();
	    profileLbl = "Search Patient";
		if(sessionStorage.clientTypeId == "2"){
			 profileLbl = "Search Client";
		}
	    devModelWindowWrapper.openPageWindow("../../html/patients/searchPatient.html", profileLbl, popW, popH, true, closePtAddAction);
}
function closePtAddAction(evt,returnData){
	if(returnData && returnData.status == "success"){
		console.log(returnData);
		var pID = returnData.selItem.PID;
		viewPatientId = pID;
		var pName = returnData.selItem.FN+" "+returnData.selItem.MN+" "+returnData.selItem.LN;
		var viewpName = pName;
		if(pID != ""){
			//$("#lblName").text(pID+" - "+pName);
			$("#txtPatient").val(pName);
			/*var imageServletUrl = ipAddress + "/download/patient/photo/" + pID + "/?" + Math.round(Math.random() * 1000000);
	        $("#imgPhoto").attr("src", imageServletUrl);
	        
	        getAjaxObject(ipAddress + "/patient/" + pID, "GET", onGetPatientInfo, onError);
		}*/
	}
}
}
function getFacilityList(){
	getAjaxObject(ipAddress+"/facility/list/?is-active=1","GET",getFacilityList,onError);
}
function getFacilityList(dataObj) {
    var dataArray = [];
    if (dataObj) {
        if ($.isArray(dataObj.response.facility)) {
            dataArray = dataObj.response.facility;
        } else {
            dataArray.push(dataObj.response.facility);
        }
    }
    var tempDataArry = [];
    for (var i = 0; i < dataArray.length; i++) {
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if (dataArray[i].isActive == 1) {
            dataArray[i].Status = "Active";
        }
        $("#cmbFacility").append('<option value="' + dataArray[i].idk + '">' + dataArray[i].name + '</option>');
    }
}

function onFacilityChange(){
	
}

function onBillChange(){
	
}
function onError(err){
	
}
function onClickPrint(){
	angularPTUIgridWrapper.printDataGrid();
}
function onClickPtViews(){
	var startDT = $("#txtStartDate").data("kendoDatePicker");
	var endDT = $("#txtEndDate").data("kendoDatePicker");
	var txtBillName = $("#cmbBillName option:selected");
	var cmbPatientId = Number($("#cmbServiceUser option:selected").val());
	var cmbFacilityId = Number($("#cmbFacility option:selected").val());
	
	var sDate = new Date(startDT.value());
	sDate.setHours(0);
	sDate.setMinutes(0);
	sDate.setSeconds(0);
	
	var eDate = new Date(endDT.value())
	eDate.setHours(23);
	eDate.setMinutes(59);
	eDate.setSeconds(59);
	
	var ipUrl = "";
	if(txtBillName.text() != "" && cmbPatientId && cmbPatientId > 0 && cmbFacilityId && cmbFacilityId > 0){
		ipUrl = ipAddress+"/homecare/reports/invoice/?date-of-appointment=:bt:"+sDate.getTime()+","+eDate.getTime()+"&patient-id="+cmbPatientId+"&facility-id="+cmbFacilityId+"&bill-to-name="+txtBillName.text();
	}else{
		ipUrl = ipAddress+"/homecare/reports/invoice/?date-of-appointment=:bt:"+sDate.getTime()+","+eDate.getTime();
	}
	
	// buildPatientRosterList([]);
	getAjaxObject(ipUrl,"GET",getReportDataList,onError);
}


function closePopup(id) {
	$('#' + id).hide();
}
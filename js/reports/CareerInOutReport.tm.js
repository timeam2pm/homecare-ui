var angularPTUIgridWrapper = null;
var dataArray = [];
var appReport = angular.module('appReport', []);

appReport.controller('reportController', function($scope) {
	$scope.getData = function () {
        onClickPtViews();

    }
    $scope.printTable = function () {

        var divToPrint = document.getElementById("carertable");

        var htmlToPrint = '' +
            '<style type="text/css">' +
            'table th, table td, table {' +
            'border:solid 1px #f3f6f9' +
			';' +
            'padding;0.5em;' +
            'border-collapse:collapse;'+
            '}' +
            '</style>';
        htmlToPrint += divToPrint.outerHTML;

        // var newWin = window.open("");
        // newWin.document.write(htmlToPrint);
        // newWin.document.close();
        // newWin.focus();
        // newWin.print();
        // newWin.close();

        // var newWin  = window.open("");
        // newWin.document.body.innerHTML = htmlToPrint;
        // newWin.focus();
        // newWin.print();
        // newWin.close();


        window.frames["print_frame"].document.body.innerHTML = htmlToPrint;
        window.frames["print_frame"].window.focus();
        window.frames["print_frame"].window.print();

    }
});


$(document).ready(function(){
    $("#divMain",parent.document).css("display","none");
    $("#divPatInfo",parent.document).css("display","");
    // $("#lblTitleName",parent.document).html("Carer In Out Report");
    var cntry = sessionStorage.countryName;
    if((cntry.indexOf("India")>=0) || (cntry.indexOf("United Kingdom")>=0)){
        $("#lblFacility").text("Location");
    }else if(cntry.indexOf("United State")>=0) {
        $("#lblFacility").text("Facility");
    }
});

$(window).load(function(){
	parentRef = parent.frames['iframe'].window;
	onMessagesLoaded();
});
function adjustHeight(){
	var defHeight = 160;
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 100;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    try{angularPTUIgridWrapper.adjustGridHeight(cmpHeight);}catch(e){};
    
}
function onMessagesLoaded() {
	buttonEvents();
	init();
}
var cntry = "";
var dtFMT = "dd/MM/yyyy";
var stDate = "";
var endDate = "";

function init(){
	//patientId = parentRef.patientId;
	//$("#dtFromDate").kendoDatePicker({value:new Date()});
	//$("#dtToDate").kendoDatePicker({value:new Date()});

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();


	$("#txtFacility").kendoComboBox();
	cntry = sessionStorage.countryName;
	getFacilityList();
	 if(cntry.indexOf("India")>=0){
		 dtFMT = INDIA_DATE_FMT;
	 }else  if(cntry.indexOf("United Kingdom")>=0){
		 dtFMT = ENG_DATE_FMT;
	 }else  if(cntry.indexOf("United State")>=0){
		 dtFMT = US_DATE_FMT;
	 }
	$("#txtStartDate").kendoDatePicker({format:dtFMT});
	$("#txtEndDate").kendoDatePicker({format:dtFMT});
	
	startDT = $("#txtStartDate").kendoDatePicker({
         change: startChange,format:dtFMT,value:new Date()
     }).data("kendoDatePicker");

     endDT= $("#txtEndDate").kendoDatePicker({
         change: endChange,format:dtFMT,value:new Date()
     }).data("kendoDatePicker");

     endDT.min(new Date());

    // $("#txtStartDate").kendoDatePicker({value:new Date()});
    // $("#txtEndDate").kendoDatePicker({value:new Date()});
     // var dataOptionsPT = {
		//         pagination: false,
		//         paginationPageSize: 500,
		// 		changeCallBack: onPTChange
		//     }
     // angularPTUIgridWrapper = new AngularUIGridWrapper("careerGrid1", dataOptionsPT);
	  //   angularPTUIgridWrapper.init();
		//  buildPatientRosterList([]);
		 
     // adjustHeight();
  /*  onClickPtViews();*/
}
function buildPatientRosterList(dataSource){
	var gridColumns = [];
	var otoptions = {};
	otoptions.noUnselect = false;
	otoptions.rowHeight = 100; 
	gridColumns.push({
        "title": "Carer",
        "field": "carer",
    });
	  gridColumns.push({
	        "title": "Service User",
	        "field": "ServiceUser",
	    });
	  gridColumns.push({
	        "title": "Service User Location",
	        "field": "address",
	        "width":"25%"
	    });
	  gridColumns.push({
	        "title": "Appointment Time",
	        "field": "appTime",
	        "width":"15%"
	    });
	  gridColumns.push({
	        "title": "Time In",
	        "field": "inTime",
	    });
	  gridColumns.push({
	        "title": "In GPS Location",
	        "field": "inLocation",
	    });
	  gridColumns.push({
	        "title": "Time Out",
	        "field": "outTime",
	    });
	  gridColumns.push({
	        "title": "Out GPS Location",
	        "field": "outLocation",
	    });
	   gridColumns.push({
	        "title": "Actual Visit Time",
	        "field": "visitTime",
	    });
 
    angularPTUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
}
function getReportDataList(dataObj){

        // console.log(dataObj);
    dataArray = [];
    var tempdataArray = [];
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            if (dataObj.response.appointmentReports) {
                if ($.isArray(dataObj.response.appointmentReports)) {
                    dataArray = dataObj.response.appointmentReports;
                } else {
                    dataArray.push(dataObj.response.appointmentReports);
                }
            }
        }
    }


   /* var sortOrderValue = $("#cmbSortOrder").val();
    if(sortOrderValue == "1") {
        dataArray.sort(function (a, b) {
            var p1 = a.dateOfAppointment;
            var p2 = b.dateOfAppointment;

            if (p1 < p2) return -1;
            if (p1 > p2) return 1;
            return 0;
        });
    }
    else if(sortOrderValue == "3") {
        dataArray.sort(function (a, b) {
            var p1 = a.dateOfAppointment;
            var p2 = b.dateOfAppointment;
            var o1, o2;

            o1 = a.providerLastName.toLowerCase();
            o2 = b.providerLastName.toLowerCase();


            if (p1 < p2) return -1;
            if (p1 > p2) return 1;

            if (o1 < o2) return -1;
            if (o1 > o2) return 1;
            return 0;
        });
    }else if(sortOrderValue == "2") {
        dataArray.sort(function (a, b) {
            var p1 = a.dateOfAppointment;
            var p2 = b.dateOfAppointment;
            var o1, o2;

            o1 = a.serviceUserLastName.toLowerCase();
            o2 = b.serviceUserLastName.toLowerCase();

            if (p1 < p2) return -1;
            if (p1 > p2) return 1;

            if (o1 < o2) return -1;
            if (o1 > o2) return 1;

            return 0;
        });
    }*/
    // dataArray.sort(function (a, b) {
    //     var nameA = a.dateOfAppointment; // ignore upper and lowercase
    //     var nameB = b.dateOfAppointment; // ignore upper and lowercase
    //     if (nameA < nameB) {
    //         return -1;
    //     }
    //     if (nameA > nameB) {
    //         return 1;
    //     }
    //     // names must be equal
    //     return 0;
    // });

    if (dataArray.length > 0) {
        for (var d = 0; d < dataArray.length; d++) {
        	if(dataArray[d].providerId > 0) {
                dataArray[d].carer = dataArray[d].providerLastName + " " + dataArray[d].providerFirstName;
                dataArray[d].ServiceUser = dataArray[d].serviceUserLastName + " " + dataArray[d].serviceUserFirstName;
                if (dataArray[d].outTime && dataArray[d].outTime != 0) {
                    var outTime = kendo.toString(new Date(dataArray[d].outTime), "hh:mm tt");
                    if(outTime != 0) {
                        dataArray[d].outTime = outTime;
                    }else{
                        dataArray[d].outTime = "";
                    }
                }
                else{
                    dataArray[d].outTime = "";
                }
                if (dataArray[d].inTime && dataArray[d].inTime != 0) {
                    var inTime = kendo.toString(new Date(dataArray[d].inTime), "hh:mm tt");
                    if(inTime != 0) {
                        dataArray[d].inTime = inTime;
                    }
                    else{
                        dataArray[d].inTime = "";
                    }
                }
                else{
                    dataArray[d].inTime = "";
                }

                if (dataArray[d].gpsInTime) {
                    dataArray[d].gpsInTime = kendo.toString(new Date(dataArray[d].gpsInTime), "hh:mm tt");
                }
                if (dataArray[d].gpsOutTime) {
                    dataArray[d].gpsOutTime = kendo.toString(new Date(dataArray[d].gpsOutTime), "hh:mm tt");
                }
                if (dataArray[d].visitTime) {
                    dataArray[d].visitTime = secondsToHms(Number(dataArray[d].visitTime));
                }
                dataArray[d].address = dataArray[d].address1 + "," + dataArray[d].address2 + "," + dataArray[d].zip;
                if (dataArray[d].dateOfAppointment) {
                    dataArray[d].appDate = kendo.toString(new Date(dataArray[d].dateOfAppointment), "dd-MM-yyyy");
                    dataArray[d].appTime = kendo.toString(new Date(dataArray[d].dateOfAppointment), "hh:mm tt");
                }

                var apptDateTime = new Date(dataArray[d].dateOfAppointment);

                var dateOfMonth = apptDateTime.getDate();
                var apptmonth = apptDateTime.getMonth();
                var apptyear = apptDateTime.getFullYear();
                var appthours = apptDateTime.getHours();
                var apptminutes = apptDateTime.getMinutes();
                dataArray[d].appointmentDate = new Date(apptyear, apptmonth, dateOfMonth, appthours, apptminutes, 0, 0);

                tempdataArray.push(dataArray[d]);
            }
        }


        if (tempdataArray != null && tempdataArray.length > 0) {
            var sortOrderValue = $("#cmbSortOrder option:selected").val();
            if (sortOrderValue == "1") {
                tempdataArray.sort(function (a, b) {
                    var p1 = a.dateOfAppointment;
                    var p2 = b.dateOfAppointment;

                    if (p1 < p2) return -1;
                    if (p1 > p2) return 1;
                    return 0;
                });
            }
            else if (sortOrderValue == "2") {
                tempdataArray.sort(sortByAppointmentDateAndServiceUser);
            }
            else if (sortOrderValue == "3") {
                tempdataArray.sort(sortByAppointmentDateAndStaffName);
            }
        }

        var elem = angular.element(document.querySelector('[ng-app]'));
        var injector = elem.injector();
        var $rootScope = injector.get('$rootScope');
        $rootScope.$apply(function(){
            $rootScope.apidata = tempdataArray;
        });

    }
    else{
        customAlert.error("info","No appointments");
    }
	// buildPatientRosterList(dataArray);
}
function secondsToHms(d) {
    d = Number(d);
    var da = Math.floor(d / 86400);
    var h = Math.floor(d / 3600);
    h = da > 0 ? (h-(da*24)):h;
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    var daDisplay = da > 0 ? da + (da == 1 ? " day, " : " days, ") : "";
    var hDisplay = h > 0 ? h + (h == 1 ? " hour, " : " hours, ") : "";
    var mDisplay = m > 0 ? m + (m == 1 ? " minute, " : " minutes, ") : "";
    var sDisplay = s > 0 ? s + (s == 1 ? " second" : " seconds") : "";
    return daDisplay + hDisplay + mDisplay + sDisplay;
}
function onPTChange(){
	
}
function startChange() {
    var startDate = startDT.value(),
    endDate = endDT.value();

    if (startDate) {
        startDate = new Date(startDate);
        startDate.setDate(startDate.getDate());
        endDT.min(startDate);
    } else if (endDate) {
        startDT.max(new Date(endDate));
    } else {
        endDate = new Date();
        startDT.max(endDate);
        endDT.min(endDate);
    }
}

function endChange() {
    var endDate = endDT.value(),
    startDate = startDT.value();

    if (endDate) {
        endDate = new Date(endDate);
        endDate.setDate(endDate.getDate());
        startDT.max(endDate);
    } else if (startDate) {
        endDT.min(new Date(startDate));
    } else {
        endDate = new Date();
        startDT.max(endDate);
        endDT.min(endDate);
    }
}
function buttonEvents(){
	$("#btnSearch1").off("click",onClickSearch);
	$("#btnSearch1").on("click",onClickSearch);
	
	$("#btnPtView").off("click",onClickPtViews);
	$("#btnPtView").on("click",onClickPtViews);
	
	$("#btnPrint").off("click",onClickPrint);
	$("#btnPrint").on("click",onClickPrint);
}
function onClickSearch(){
	 var popW = "60%";
	    var popH = 500;

	    var profileLbl;
	    var devModelWindowWrapper = new kendoWindowWrapper();
	    profileLbl = "Search Patient";
		if(sessionStorage.clientTypeId == "2"){
			 profileLbl = "Search Client";
		}
	    devModelWindowWrapper.openPageWindow("../../html/patients/searchPatient.html", profileLbl, popW, popH, true, closePtAddAction);
}
function closePtAddAction(evt,returnData){
	if(returnData && returnData.status == "success"){
		console.log(returnData);
		var pID = returnData.selItem.PID;
		viewPatientId = pID;
		var pName = returnData.selItem.FN+" "+returnData.selItem.MN+" "+returnData.selItem.LN;
		var viewpName = pName;
		if(pID != ""){
			//$("#lblName").text(pID+" - "+pName);
			$("#txtPatient").val(pName);
			/*var imageServletUrl = ipAddress + "/download/patient/photo/" + pID + "/?" + Math.round(Math.random() * 1000000);
	        $("#imgPhoto").attr("src", imageServletUrl);
	        
	        getAjaxObject(ipAddress + "/patient/" + pID, "GET", onGetPatientInfo, onError);
		}*/
	}
}
}
function getFacilityList(){
	getAjaxObject(ipAddress+"/facility/list/?is-active=1","GET",getFacilityDataList,onError);
}
function getFacilityDataList(dataObj){
	var dataArray = [];
	if(dataObj){
		if($.isArray(dataObj.response.facility)){
			dataArray = dataObj.response.facility;
		}else{
			dataArray.push(dataObj.response.facility);
		}
	}
	var tempDataArry = [];
	for(var i=0;i<dataArray.length;i++){
		dataArray[i].idk = dataArray[i].id; 
		dataArray[i].Status = "InActive";
		if(dataArray[i].isActive == 1){
			dataArray[i].Status = "Active";
		}
	}
	setDataForSelection(dataArray, "txtFacility", onFacilityChange, ["name", "idk"], 0, "");
    onClickPtViews();
}
function onFacilityChange(){
	
}
function onError(err){
	
}
function onClickPrint(){
	angularPTUIgridWrapper.printDataGrid();
}
function onClickPtViews(){
	var startDT = $("#txtStartDate").data("kendoDatePicker");
	var endDT = $("#txtEndDate").data("kendoDatePicker");
	var txtFacility = $("#txtFacility").data("kendoComboBox");
	
	var sDate = new Date(startDT.value());
	sDate.setHours(0);
	sDate.setMinutes(0);
	sDate.setSeconds(0);
    sDate.setMilliseconds(0);
	
	var eDate = new Date(endDT.value())
	eDate.setHours(23);
	eDate.setMinutes(59);
	eDate.setSeconds(59);
    eDate.setMilliseconds(0);

	
	var ipUrl = "";
	ipUrl = ipAddress+"/reports/appointment-reports/?date-of-appointment=:bt:"+sDate.getTime()+","+eDate.getTime()+"&facility-id="+txtFacility.value()+"&is-active=1&is-deleted=0";
	// buildPatientRosterList([]);
	getAjaxObject(ipUrl,"GET",getReportDataList,onError);
}

var sortByAppointmentDateAndStaffName = function (a, b) {

    if (a.carer > b.carer) return 1;
    if (a.carer < b.carer) return -1;

    var date1 = a.appointmentDate;
    var date2 = b.appointmentDate;


    if (date1 > date2) return 1;
    if (date1 < date2) return -1;

}

var sortByAppointmentDateAndServiceUser = function (a, b) {

    if (a.ServiceUser > b.ServiceUser) return 1;
    if (a.ServiceUser < b.ServiceUser) return -1;

    var date1 = a.appointmentDate;
    var date2 = b.appointmentDate;


    if (date1 > date2) return 1;
    if (date1 < date2) return -1;

}
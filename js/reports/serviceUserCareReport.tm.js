var angularUIgridWrapper;
var parentRef = null;
var recordType = "1";
var dataArray = [];
var ADD = "add";
var UPDATE = "edit";
var VIEW = "view";
var DELETE = "delete";
var operation = "add";
var ds = "";
var IsFlag = 1;
var cntry = sessionStorage.countryName;
var selectPatientId,selProviderId;
var dtFMT = "dd/MM/yyyy";
var allDatesInWeek = [];
var daysOfWeek = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

$(document).ready(function () {
    
    $("#pnlPatient", parent.document).css("display", "none");

    sessionStorage.setItem("IsSearchPanel", "1");
    //themeAPIChange();
    parentRef = parent.frames['iframe'].window;
    // var dataOptions = {
    //     pagination: false,
    //     changeCallBack: onChange
    // }
    var cntry = sessionStorage.countryName;
    if((cntry.indexOf("India")>=0) || (cntry.indexOf("United Kingdom")>=0)){
        $("#lblFacility").text("Location");
    }else if(cntry.indexOf("United State")>=0) {
        $("#lblFacility").text("Facility");
    }
    // angularUIgridWrapper = new AngularUIGridWrapper("dgridStaffTimesheets", dataOptions);
    // angularUIgridWrapper.init();
    // buildDeviceListGrid([]);

});


$(window).load(function () {
    loading = false;
    //$(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded() {

    init();
    buttonEvents();
}

function init() {
    //$("#btnEdit").prop("disabled", true);
    //$("#btnDelete").prop("disabled", true);
    allowNumerics("txtAge");
    getAjaxObject(ipAddress + "/patient/list/?is-active=1&is-deleted=0", "GET", getPatientList, onError);
    getAjaxObject(ipAddress + "/master/Language/list?is-active=1&is-deleted=0", "GET", getLanguageValueList, onError);
    getAjaxObject(ipAddress + "/facility/list/?is-active=1&is-deleted=0", "GET", getFacilityList, onError);
    getAjaxObject(ipAddress + "/master/Gender/list/?is-active=1&is-deleted=0", "GET", getGenderValueList, onError);


    $("#txtDateOfWeek").kendoDatePicker({ format: dtFMT, value: new Date() });
    //$("#txtTDate").kendoDatePicker({ format: dtFMT, value: new Date() });
}



function onError(errorObj) {
    console.log(errorObj);
}

function buttonEvents() {

    $("#btnSubmit").off("click", onClickViewServiceUserCareReport);
    $("#btnSubmit").on("click", onClickViewServiceUserCareReport);

    // $("#btnEdit").off("click", onClickEdit);
    // $("#btnEdit").on("click", onClickEdit);

    $("#btnServiceUser").off("click");
    $("#btnServiceUser").on("click",onClickServiceUser);


    $("#btnStaffUser").off("click");
    $("#btnStaffUser").on("click", onClickStaff);

    $("#btnPrint").off("click");
    $("#btnPrint").on("click",onClickPrint);

    // $('input[type="radio"].StaffViewType').on('change', function () {
    //     if ($("#rbtnOld").is(":checked")) {
    //         $("#btnPrint").hide();
    //         selProviderId = null;
    //         $("#txtStaffUser").val("");
    //         $("#divOldTimeSheetReportTableBlock").show();
    //         $("#divStaffTimeSheetReportViewer").empty();
    //         $("#divStaffTimeSheetReport").hide();
    //         $("#divGrandTotalHours").hide();
    //         $("#tdGrandTotaScheduledHours").text("");
    //         $("#tdGrandTotaActualHours").text("");
    //     }
    //     else {
    //         $("#btnPrint").show();
    //         $("#divOldTimeSheetReportTableBlock").hide();
    //         $("#divStaffTimeSheetReport").show();
    //         buildDeviceListGrid([]);
    //     }
    // });

}

function onClickPrint(){
    if ($.trim($('#divSUCareReport').html()) !== "") {

        var facility = $("#cmbFacility option:selected").text();
        var facilityId = $("#cmbFacility").val();
        var communications;
        var communicationsDeatils1, communicationsDeatils2;
        var tempFacilityDataArry = _.where(facilityDataArry, {idk: Number(facilityId)});
        if(tempFacilityDataArry[0].communications && tempFacilityDataArry[0].communications != null) {
            communications = tempFacilityDataArry[0].communications;
            if (communications && communications != null) {
                if (communications[0].address1 != "") {
                    communicationsDeatils1 = communications[0].address1;
                }

                if (communications[0].address2 != "") {
                    communicationsDeatils1 = communicationsDeatils1 + ", " + communications[0].address2;
                }

                if (communications[0].city != "") {
                    communicationsDeatils2 = communications[0].city;
                }

                if (communications[0].state != "") {
                    communicationsDeatils2 = communicationsDeatils2 + ", " + communications[0].state;
                }

                if (communications[0].zip != "") {
                    communicationsDeatils2 = communicationsDeatils2 + ", " + communications[0].zip;
                }
            }
        }

        var contents = document.getElementById("divSUCareReport").outerHTML;// $("#divTaskReportsViewer").html();
        var frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({ "position": "absolute", "top": "-1000000px" });
        $("body").append(frame1);
        var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        var strDate;
        var DOB = new Date();
        strDate = kendo.toString(DOB, "dd/MM/yyyy hh:mm:ss tt");

        var age = $("#txtAge").val();
        var langVal = $("#cmbLang").val();
        var lang = "";
        if(langVal != 0){
            lang = $("#cmbLang option:selected").text();
        }

        var genderVal = $("#cmbGender").val();
        var gender = "";
        if(genderVal != 0){
            gender = $("#cmbGender option:selected").text();
        }

        //Create a new HTML document.
        frameDoc.document.write('<html><title>Service User Plan Care Report</title><head>');
        frameDoc.document.write('<link href="../../css/Theme01.css" rel="stylesheet" type="text/css" />');
        frameDoc.document.write('<link href="../../css/report/invoicestyle.css" rel="stylesheet" type="text/css" />');

        frameDoc.document.write('</head><body style="background-color: #fff;">');

        frameDoc.document.write('<table class="tblInvoiceHeader"><tr style="page-break-inside: avoid"><td><div class="reportName"><h2><span style="color:#D5AC57">Service User Plan Care Report</span> <br clear="all"><small style="color: #000000 ;">'+ strDate +'</small></h2></div></td>');
        frameDoc.document.write('<td><div class="reportLocations"><h2><span>'+facility+'<span></span></span></h2><address> '+ communicationsDeatils1 + '<br>'+communicationsDeatils2+'</address></div> </td></tr></table><br clear="all">');

        // frameDoc.document.write('<div class="reportName"><h2><span>Timesheet Report</span> <br clear="all"><small>'+ strDate +'</small></h2></div>');
        // frameDoc.document.write('<div class="reportLocations"><h2><span>'+facility+'<span></span></span></h2><address> '+ communicationsDeatils1 + '<br>'+communicationsDeatils2+'</address></div> <br clear="all">');

        frameDoc.document.write('<div class="searchedLabels">');
        frameDoc.document.write('<div class="divLabel"><span>Location :</span> <label>'+facility+'</label>' +" "+'<span>ServiceUser :</span> <label>'+printSUName.slice(0,-1)+'</label>' +" "+'<span>Gender :</span> <label>'+gender+'</label>' +" "+'<span>Age :</span> <label>'+age+'</label>' +" "+'<span>Language :</span> <label>'+lang+'</label></div>');
        frameDoc.document.write('</div>');

        frameDoc.document.write('<div class="timesheetinvoiceBlock">');
        
        //Append the DIV contents.
        frameDoc.document.write(contents);

        frameDoc.document.write('</div>');
       //frameDoc.document.write('</div> </div> </div>');
        frameDoc.document.write('</body></html>');


        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();

        }, 500);
    }
    else {
        customAlert.info("info","No data to print.");
    }
}


function printSUName(){
    if ($.trim($('#divSUCareReport').html()) !== "") {

        var facility = $("#cmbFacility option:selected").text();
        var facilityId = $("#cmbFacility").val();
        var communications;
        var communicationsDeatils1, communicationsDeatils2;
        var tempFacilityDataArry = _.where(facilityDataArry, {idk: Number(facilityId)});
        if(tempFacilityDataArry[0].communications && tempFacilityDataArry[0].communications != null) {
            communications = tempFacilityDataArry[0].communications;
            if (communications && communications != null) {
                if (communications[0].address1 != "") {
                    communicationsDeatils1 = communications[0].address1;
                }

                if (communications[0].address2 != "") {
                    communicationsDeatils1 = communicationsDeatils1 + ", " + communications[0].address2;
                }

                if (communications[0].city != "") {
                    communicationsDeatils2 = communications[0].city;
                }

                if (communications[0].state != "") {
                    communicationsDeatils2 = communicationsDeatils2 + ", " + communications[0].state;
                }

                if (communications[0].zip != "") {
                    communicationsDeatils2 = communicationsDeatils2 + ", " + communications[0].zip;
                }
            }
        }

        var contents = document.getElementById("divSUCareReport").outerHTML;// $("#divTaskReportsViewer").html();
        var frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({ "position": "absolute", "top": "-1000000px" });
        $("body").append(frame1);
        var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        var strDate;
        var DOB = new Date();
        strDate = kendo.toString(DOB, "dd/MM/yyyy hh:mm:ss tt");
        //Create a new HTML document.
        frameDoc.document.write('<html><title>Staff Weekly Planned Care Report</title><head>');
        frameDoc.document.write('<link href="../../css/Theme01.css" rel="stylesheet" type="text/css" />');
        frameDoc.document.write('<link href="../../css/invoicestyle.css" rel="stylesheet" type="text/css" />');
        frameDoc.document.write('</head><body style="background-color: #fff;">');

        frameDoc.document.write('<table class="tblInvoiceHeader"><tr style="page-break-inside: avoid"><td><div class="reportName"><h2><span style="color:#D5AC57">Staff Weekly Planned Care Report</span> <br clear="all"><small style="color:#000000">'+ strDate +'</small></h2></div></td>');
        frameDoc.document.write('<td><div class="reportLocations"><h2><span>'+facility+'<span></span></span></h2><address> '+ communicationsDeatils1 + '<br>'+communicationsDeatils2+'</address></div> </td></tr></table><br clear="all">');

        // frameDoc.document.write('<div class="reportName"><h2><span>Timesheet Report</span> <br clear="all"><small>'+ strDate +'</small></h2></div>');
        // frameDoc.document.write('<div class="reportLocations"><h2><span>'+facility+'<span></span></span></h2><address> '+ communicationsDeatils1 + '<br>'+communicationsDeatils2+'</address></div> <br clear="all">');

        frameDoc.document.write('<div class="searchedLabels">');
        frameDoc.document.write('<div class="divLabel"><span>Location :</span> <label>'+facility+'</label> <span>Staff :</span> <label>'+facilityId+'</label></div>');
        frameDoc.document.write('</div>');

        frameDoc.document.write('<div class="timesheetinvoiceBlock">');

        //Append the DIV contents.
        frameDoc.document.write(contents);

        frameDoc.document.write('</div>');
        //frameDoc.document.write('</div> </div> </div>');
        frameDoc.document.write('</body></html>');


        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();

        }, 500);
    }
    else {
        customAlert.info("info","No data to print.");
    }
}

function searchOnLoad(status) {
    buildDeviceListGrid([]);
    if (status == "active") {
        var urlExtn = ipAddress + "/homecare/vacations/?fields=*&parentTypeId=" + Number(parentRef.type) + "&parentId=" + Number(parentRef.patientId) + "&is-active=1&is-deleted=0";

    }
    else if (status == "inactive") {
        var urlExtn = ipAddress + "/homecare/vacations/?fields=*&parentTypeId=" + Number(parentRef.type) + "&parentId=" + Number(parentRef.patientId) + "&is-active=0&is-deleted=1"
    }
    getAjaxObject(urlExtn, "GET", handleGetVacationList, onError);
}


function onClickAdd() {
    $("#addPopup").show();
    $('#viewDivBlock').hide();
    $("#txtID").hide();
    $(".filter-heading").html("Add Staff Leave Request");
    parentRef.operation = "add";
    onClickReset();
}

function addReportMaster(opr) {
    var popW = 500;
    var popH = "43%";

    //$('.listDataWrapper').hide();
    //$('.addOrRemoveWrapper').show();
    $('.alert').remove();
    var profileLbl = "Add Task Type";
    parentRef.operation = opr;
    operation = opr;
    if (opr == "add") {
        $('#btnSave').html('<span><img src="../../img/medication/Save.png" class="providerLink-btn-img" />Save</span>');
        $('.tabContentTitle').text('Add Task Type');
        $('#btnReset').show();
        //var billActName = $("#txtBAN").val();
        $('#btnReset').trigger('click');
        //$("#txtBAN").val(billActName);
    } else {

        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if (selectedItems && selectedItems.length > 0) {
            var obj = selectedItems[0];
            parentRef.id = obj.idk;
            parentRef.displayOrder = obj.displayOrder;
            parentRef.type = obj.type;
            parentRef.DIO = obj.DIO;
            parentRef.activityGroup = obj.activityGroup;
            parentRef.isActive = obj.isActive;
            operation = UPDATE;
        }

        profileLbl = "Edit Task Type";
        $('.tabContentTitle').text('Edit Task Type');
        $('#btnReset').hide();
        $('#btnSave').html('<span><img src="../../img/medication/Save.png" class="providerLink-btn-img" />Update</span>');
        $('#btnReset').trigger('click');
    }
    var devModelWindowWrapper = new kendoWindowWrapper();
    devModelWindowWrapper.openPageWindow("../../html/masters/createactivityTypeList.html", profileLbl, popW, popH, true, closeaddReportMastertAction);
}
function closeaddReportMastertAction(evt, returnData) {
    if (returnData && returnData.status == "success") {
        var opr = returnData.operation;
        if (opr == "add") {
            customAlert.info("info", "Task Type created successfully");
        } else {
            customAlert.info("info", "Task Type updated successfully");
        }
        init();
    }
}
function onClickActive() {
    $(".btnInActive").removeClass("radioButton-active");
    $(".btnActive").addClass("radioButton-active");
}

function onClickInActive() {
    $(".btnActive").removeClass("radioButton-active");
    $(".btnInActive").addClass("radioButton-active");
}

function onClickDelete() {
    customAlert.confirm("Confirm", "Are you sure to delete?", function (response) {
        if (response.button == "Yes") {
            var dataObj = {};
            dataObj.createdBy = Number(sessionStorage.userId);
            dataObj.isDeleted = 1;
            dataObj.isActive = 0;
            var dataUrl = ipAddress + "/homecare/vacations/";
            var method = "DELETE";
            var selectedItems = angularUIgridWrapper.getSelectedRows();
            dataObj.id = selectedItems[0].idk;
            createAjaxObject(dataUrl, dataObj, method, onCreateDelete, onError);
        }
    });
}

function onCreateDelete(dataObj) {
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            var msg = "Vacation deleted successfully";
            customAlert.error("Info", msg);
            operation = ADD;
            init();
        }
    }
}


function onClickCancel() {
    $(".filter-heading").html("View Appointments");
    $("#viewDivBlock").show();
    $("#addPopup").hide();
}

function adjustHeight() {
    var defHeight = 280;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    angularUIgridWrapper.adjustGridHeight(cmpHeight);
}

function buildDeviceListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    otoptions.rowHeight = 100;
    gridColumns.push({
        "title": "Staff",
        "field": "Provider"
    });
    // gridColumns.push({
    //     "title": "Weekly Contract Hours",
    //     "field": "weeklyContractHours"
    // });

    gridColumns.push({
        "title": "Appointment Date Time",
        "field": "dateTimeOfAppointment"
    });
    /*gridColumns.push({
        "title": "DOW",
        "field": "dow"
    });*/
    gridColumns.push({
        "title": "Scheduled Hrs",
        "field": "durationTime"
    });

    gridColumns.push({
        "title": "Actual Hrs",
        "field": "actualDurationTime"
    });

    // if ($("#rbtnDetail").is(":checked")) {
    //     gridColumns.push({
    //         "title": "Comments",
    //         "field": "notes"
    //     });
    // }

    angularUIgridWrapper.creategrid(dataSource, gridColumns, otoptions);
    adjustHeight();
}

function onChange() {
    setTimeout(function () {
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        if (selectedItems && selectedItems.length > 0) {

            if ($("#rbtnDetail").is(":checked")) {
                $("#btnEdit").prop("disabled", false);
            }
            else {
                $("#btnEdit").prop("disabled", true);
            }

            //$("#btnEdit").prop("disabled", false);
        } else {
            $("#btnEdit").prop("disabled", true);
        }
    }, 100);
}

function onClickEdit() {

    var selectedItems = angularUIgridWrapper.getSelectedRows();

    
    parentRef.selectedItems = selectedItems;
    parentRef.facilityName = $("#cmbFacility option:selected").text();


    var popW = "50%";
    var popH = "60%";
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Edit Appointment";
    
    devModelWindowWrapper.openPageWindow("../../html/masters/editAppointment.html", profileLbl, popW, popH, true, onCloseEditAppointment);
    parent.setPopupWIndowHeaderColor("0bc56f", "FFF");

    //$("#viewDivBlock").hide();
    //$("#addPopup").show();
    //var selectedItems = angularUIgridWrapper.getSelectedRows();
    //if (selectedItems && selectedItems.length > 0) {
    //    var obj = selectedItems[0];
    //    if (obj.idk) {
    //        appointmentId = obj.idk;
    //    }
    //    else {
    //        appointmentId = obj.id;
    //    }

    //    if (obj.composition && obj.composition.appointmentReason.value) {
    //        $("#cmbReason").val(obj.composition.appointmentReason.value);
    //    } else {
    //        $("#cmbReason").val(obj.appointmentReason);
    //    }
    //    if (obj.composition && obj.composition.appointmentType) {
    //        $("#cmbStatus").val(obj.composition.appointmentType.value);
    //    }
    //    else {
    //        $("#cmbStatus").val(obj.appointmentType);
    //    }

    //    selectPatientId = obj.patientId;
    //    selectProviderId = obj.providerId;
    //    if (selectProviderId != 0) {
    //        if (obj.composition && obj.composition.provider) {
    //            $("#txtStaff").val(obj.composition.provider.lastName + " " + obj.composition.provider.firstName + " " + (obj.composition.provider.middleName != null ? obj.composition.provider.middleName : " "));
    //        } else {
    //            $("#txtStaff").val(obj.staffName);
    //        }

    //    }
    //    else {
    //        $("#txtStaff").val("");
    //    }
    //    $("#txtFacility").val(parentRef.facilityName);
    //    var txtStartDate = $("#txtStartDate").data("kendoDateTimePicker");
    //    if (txtStartDate) {
    //        txtStartDate.value(GetDateTimeEdit(obj.dateTimeOfAppointment));
    //    }
    //    var endappoinment = (obj.dateTimeOfAppointment + (Number(obj.duration) * 60000));
    //    var txtEndDate = $("#txtEndDate").data("kendoDateTimePicker");
    //    if (txtEndDate) {
    //        txtEndDate.value(GetDateTimeEdit(endappoinment));
    //    }
    //    $("#txtPayout").val(obj.payoutHourlyRate);
    //    $("#cmbBilling").val(obj.billToName);
    //}
}


var operation = "add";
var selFileResourceDataItem = null;

function onStatusChange() {
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if (cmbStatus && cmbStatus.selectedIndex < 0) {
        cmbStatus.select(0);
    }
}

function onClickReset() {
    $("#txtReason").val("");
    $("#txtFDate").val("");
    $("#txtTDate").val("");
    operation = ADD;
}

function themeAPIChange() {
    getAjaxObject(ipAddress + "/homecare/settings/?id=2", "GET", getThemeValue, onError);
}

function getThemeValue(dataObj) {
    console.log(dataObj);
    var tempCompType = [];
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.activityTypes) {
            if ($.isArray(dataObj.response.settings)) {
                tempCompType = dataObj.response.settings;
            } else {
                tempCompType.push(dataObj.response.settings);
            }
        }
        if (tempCompType.length > 0) {
            themesChange(tempCompType[0].value);
        }
    }
}

function themesChange(themeValue) {
    if (themeValue == 2) {
        loadAPi("../../Theme2/Theme02.css");
    }
    else if (themeValue == 3) {
        loadAPi("../../Theme3/Theme03.css");
    }
}

var facilityDataArry = [];

function getFacilityList(dataObj) {
    var dataArray = [];
    if (dataObj) {
        if ($.isArray(dataObj.response.facility)) {
            dataArray = dataObj.response.facility;
        } else {
            dataArray.push(dataObj.response.facility);
        }
    }
    facilityDataArry = dataArray;
    for (var i = 0; i < dataArray.length; i++) {
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if (dataArray[i].isActive == 1) {
            dataArray[i].Status = "Active";
        }
        $("#cmbFacility").append('<option value="' + dataArray[i].idk + '">' + dataArray[i].name + '</option>');
    }
}

function onClickServiceUser() {
    var popW = 800;
    var popH = 450;

    if (parentRef)
        parentRef.searchZip = true;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Service User";
    devModelWindowWrapper.openPageWindow("../../html/patients/searchPatient.html", profileLbl, popW, popH, true, onCloseServiceUserAction);
}
function onCloseServiceUserAction(evt, returnData) {
    if (returnData && returnData.status == "success") {
        selectPatientId = returnData.selItem.PID;
        $('#txtSU').val(returnData.selItem.LN + " " + returnData.selItem.FN + " " + returnData.selItem.MN);
    }
}

function onClickStaff(){
    var popW = 800;
    var popH = 450;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Staff";
    devModelWindowWrapper.openPageWindow("../../html/patients/staffList.html", profileLbl, popW, popH, true, onCloseStaffAction);
}

function onCloseStaffAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        selProviderId = returnData.selItem.PID;
        $('#txtStaffUser').val(returnData.selItem.LN+' '+returnData.selItem.FN);

    }
}

function onClickSubmit() {
    if ($("#txtSU").val() != "") {
        if ($("#txtFDate").val() !== "") {
            var txtAppPTDate = $("#txtFDate").data("kendoDatePicker");
            var selectedDate = txtAppPTDate.value();
            var selDate = new Date(selectedDate);


            // var selectedDate = GetDateTime("txtDate");


            for (let i = 1; i <= 7; i++) {
                var first = selectedDate.getDate() - (selectedDate.getDay() + 1) + i;
                var day = new Date(selectedDate.setDate(first));

                allDatesInWeek.push(day);
            }
            var day = selDate.getDate();
            var month = selDate.getMonth();
            month = month + 1;
            var year = selDate.getFullYear();

            var stDate = month + "/" + day + "/" + year;
            stDate = stDate + " 00:00:00";

            var startDate = new Date(stDate);
            var stDateTime = startDate.getTime();

            var etDate = month + "/" + day + "/" + year;
            etDate = etDate + " 23:59:59";

            var endtDate = new Date(etDate);
            var edDateTime = endtDate.getTime();

            var startDate = new Date(stDate);
            var endtDate = new Date(etDate);

            var day = startDate.getDate() - startDate.getDay();
            var pDate = new Date(startDate);
            pDate.setDate(day);

            endtDate.setDate(day + 6);
            var stDateTime = pDate.getTime();
            var edDateTime = endtDate.getTime();
            var txtAppPTFacility = $("#cmbFacility").val();
            parentRef.facilityId = txtAppPTFacility;
            parentRef.patientId = selectPatientId;
            parentRef.SUName = $("#txtSU").val();
            parentRef.facilityName = $("#cmbFacility option:selected").text();
            parentRef.selDate = pDate;
            parentRef.copyDate = txtAppPTDate.value();

            buildDeviceListGrid([]);

            var patientListURL = ipAddress + "/appointment/list/?facility-id=" + txtAppPTFacility + "&patient-id=" + selectPatientId + "&to-date=" + edDateTime + "&from-date=" + stDateTime + "&is-active=1";

            getAjaxObject(patientListURL, "GET", onPatientListData, onError);

        }
        else {
            customAlert.error("Error", "Please select date");
        }
    }
    else {
        customAlert.error("Error", "Please select service user");
    }
}

function onClickViewServiceUserCareReport() {

    $("#divSUCareReport").html("");

    var strMessage;
    var isValid = true;
    if ($("#cmbFacility").val() === 0) {
        strMessage = "Please select facility.";
        isValid = false;
    }
    var age = $("#txtAge").val();
    var lang = $("#cmbLang option:selected").text();
    var gender = $("#cmbGender option:selected").text();

    var langVal = $("#cmbLang").val();
    var genderVal = $("#cmbGender").val();

    var patientIds = 0;
    patientIds = $('#cmbServiceUser').multipleSelect('getSelects');


    if(patientIds == 0 && age == "" && langVal == "0" && genderVal == "0"){
        strMessage = "Please select any one of the above filter.";
        isValid = false;
    }

    if(isValid) {
        var facilityId = $("#cmbFacility").val();


        var apiURL = ipAddress + "/patient/roster/list/?facility-id="+facilityId+"&is-active=1&is-deleted=0";
        if (patientIds != 0) {
            apiURL = apiURL + "&patient-id="+patientIds.join(',');
        }

        if (age != 0){
            apiURL = apiURL + "&age="+age;
        }

        if (langVal != 0){
            apiURL = apiURL + "&language="+lang.toLowerCase();
        }
        if (genderVal != 0){
            apiURL = apiURL + "&gender="+gender.toLowerCase();
        }


        getAjaxObject(apiURL, "GET", onGetServiceUserCareReport1, onError);

    }
    else{
        customAlert.error("info",strMessage);
    }
}


function onGetServiceUserCareReport1(dataObj) {
    dtArray = [];
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.patientRoster) {
            if ($.isArray(dataObj.response.patientRoster)) {
                dtArray = dataObj.response.patientRoster;
            } else {
                dtArray.push(dataObj.response.patientRoster);
            }
        }
    }

    if (dtArray !== null && dtArray.length > 0) {

        var txtDateOfWeek = $("#txtDateOfWeek").data("kendoDatePicker");
        var selectedDate = txtDateOfWeek.value();

        var arrDatesInWeek = [];

        for (var i = 1; i <= 7; i++) {
            var first = selectedDate.getDate() - (selectedDate.getDay() + 1) + i;
            var day = new Date(selectedDate.setDate(first));

            arrDatesInWeek.push(day);
        }

        var allDaysInWeek = [];
        for (var counter4 = 0; counter4 < arrDatesInWeek.length; counter4++) {
            allDaysInWeek.push(daysOfWeek[arrDatesInWeek[counter4].getDay()] + " " + formatDate(arrDatesInWeek[counter4]));
        }

        var appointments = [];

        var lookup = {};
        var patientIds = [];

        for (var c = 0; c < dtArray.length; c++){
            var item;
            item = dtArray[c];
            if (item && item.patientId) {

                var patientId = item.patientId;

                if (!(patientId in lookup)) {
                    lookup[patientId] = 1;
                    patientIds.push(patientId);
                }
                var objSU = _.where(suArray, {id:  patientId});
                if(objSU && objSU.length > 0) {
                    dtArray[c].patientName = objSU[0].lastName+ " " + objSU[0].firstName;
                }
            }
        }


        var patientAppointments = [];

        if (patientIds !== null && patientIds.length > 0) {

            for (var j = 0; j < patientIds.length; j++) {

                var patientObjpush = [];

                var tmpSUAppts = $.grep(dtArray, function (e) {
                    return e.patientId === patientIds[j];
                });

                if (tmpSUAppts != null && tmpSUAppts.length > 0) {
                    // var suWeeklyAppts = [];
                    for (var w = 1; w < tmpSUAppts.length; w++) {
                        if (w <= 6) {
                            var tmpSUWeekIdAppts = $.grep(tmpSUAppts, function (e) {
                                return e.weekId == w;
                            });

                            if (tmpSUWeekIdAppts != null && tmpSUWeekIdAppts.length > 0) {
                                var objWeek = {};
                                if (tmpSUWeekIdAppts[0]) {
                                    var patientObj = {};

                                    patientObjpush.id = tmpSUWeekIdAppts[0].patientId;
                                    patientObjpush.patientName = tmpSUWeekIdAppts[0].patientName;
                                    patientObjpush.age = tmpSUWeekIdAppts[0].age;

                                    var strVal = $.trim(tmpSUWeekIdAppts[0].language.slice(0, -1));
                                    var lastChar = strVal.slice(-1);
                                    if(lastChar == ','){
                                        strVal = strVal.slice(0, -1);
                                    }
                                    strVal = $.trim(strVal);
                                    var lastChar = strVal.slice(-1);
                                    if(lastChar == ','){
                                        strVal = strVal.slice(0, -1);
                                    }

                                    patientObjpush.language = strVal;
                                    patientObjpush.gender = tmpSUWeekIdAppts[0].gender;
                                    patientObj.weekId = w;
                                }

                                var suWeeklyAppts = [];

                                for (var counter = 0; counter < arrDatesInWeek.length; counter++) {

                                    var obj = {};


                                    var tmpDate = arrDatesInWeek[counter];
                                    tmpDate.setHours(0, 0, 0, 0);


                                    var tmpAppointments = $.grep(tmpSUWeekIdAppts, function (e) {
                                        var apptDate = new Date();
                                        e.dateOfAppointment = apptDate.setHours(0, e.fromTime, 0, 0);
                                        return counter == e.weekDayStart;
                                    });

                                    obj.dateOfAppointment = allDaysInWeek[counter];
                                    obj.dayAppointments = [];


                                    if (tmpAppointments != null && tmpAppointments.length > 0) {
                                        obj.dayAppointments = tmpAppointments;
                                    }

                                    suWeeklyAppts.push(obj);
                                }
                                //objWeek.weeklyAppointments = suWeeklyAppts;
                                //patientObj.weeklyAppointments = suWeeklyAppts;
                                //patientObj.week = objWeek;
                                patientObj.weeklyAppointments = suWeeklyAppts
                                patientObjpush.push(patientObj);

                            }
                        }

                    }

                }
               // patientAppointments.push(patientObjpush);
                patientAppointments.push(patientObjpush);
            }
            bindStaffCareReport(patientAppointments);
        }


    }
    else {
        customAlert.info("Info", "No records found");
    }
}

var printSUName = '';
var providerTotalHrs, grandProviderTotalHrs;

function bindStaffCareReport(patientAppointments) {
    $("#tdGrandTotaPlannedHours").text();
    grandProviderTotalHrs = "";
    $("#divSUCareReport").html("");
    if (patientAppointments !== null && patientAppointments.length > 0) {
        var uniques = _.map(_.groupBy(patientAppointments,function(doc){
            return doc.id;
        }),function(grouped){
            return grouped[0];
        });
        if(uniques && uniques.length > 0) {
            uniques.sort(function (a, b) {
                var p1 = a.patientName;
                var p2 = b.patientName;

                if (p1 < p2) return -1;
                if (p1 > p2) return 1;
                return 0;
            });
        }

        for (var m = 0; m < uniques.length; m++) {
            providerTotalHrs = 0;
            var pObj = uniques[m];
            if (pObj && pObj != null) {

                var objSU = _.where(suArray, {id: Number(pObj.id)});
                var strHTML = '';
                if (objSU && objSU.length > 0) {
                    strHTML = strHTML + '<div class="table-block">';
                    strHTML = strHTML + '<div class="timeSheetReportdivHeading">';
                    printSUName = printSUName + objSU[0].lastName + " " + objSU[0].firstName + ",";
                    // strHTML = strHTML + "#" + pObj.id + " - " + objSU[0].lastName + " " + objSU[0].firstName;
                    strHTML = strHTML + "#" + pObj.id + " - " + pObj.patientName;

                    strHTML = strHTML + '<span class="headingSpn floatRight">';
                    var gender = (objSU[0].gender != null) ? objSU[0].gender.charAt(0) : "";
                    strHTML = strHTML + '(' + Math.round(pObj.age) + 'Y/' + gender + ') ' + pObj.language + '</span><div class="clearLine"></div>';
                    strHTML = strHTML + '<span class="headingSpn floatLeft" id="spWeekly' + pObj.id + '"></span>';
                    if (objSU && objSU.communications) {
                        strHTML = strHTML + '<span class="headingSpn floatRight">';
                        var Add;
                        if (objSU.communications.address1 && objSU.communications.address1 != '') {
                            Add = objSU.communications.address1 + ',';
                        }

                        if (objSU.communications.address2 && objSU.communications.address2 != '') {
                            Add = Add + objSU.communications.address2 + ',';
                        }

                        if (objSU.communications.city && objSU.communications.city != '') {
                            Add = Add + objSU.communications.city + ',';
                        }

                        if (objSU.communications.zip && objSU.communications.zip != '') {
                            Add = Add + objSU.communications.zip + ',';
                        }

                        if (objSU.communications.homePhone && objSU.communications.homePhone != '') {
                            Add = Add + objSU.communications.homePhone + ',';
                        }

                        if (objSU.communications.workPhone && objSU.communications.workPhone != '') {
                            Add = Add + objSU.communications.workPhone + ',';
                        }

                        if (objSU.communications.email && objSU.communications.email != '') {
                            Add = Add + objSU.communications.email + ',';
                        }
                        Add = Add.slice(0, -1);
                        strHTML = strHTML + Add;
                    }

                    strHTML = strHTML + '</div></div>';
                    strHTML = strHTML + '<div class="col-xs-12 taskReportTblBlock"><div class="table-responsive">';
                    // strHTML = strHTML + '<table class="tblTaskreport weeklyCareReportTbl"><thead><tr>';
                    var weeklyHTML = "";
                    var weeklyDuration = [];
                    for (var i = 0; i < pObj.length; i++) {
                        var arrAppointmentsCount = [];

                        weeklyHTML = weeklyHTML + '<table class="tblTaskreport weeklyCareReportTbl"><thead><tr>';
                        for (var j = 0; j < pObj[i].weeklyAppointments.length; j++) {

                            //strHTML = strHTML + '<th><span class="spnReportTitle">' + pObj.weeklyAppointments[j].dateOfAppointment.split(' ')[0] + '</th>';
                            weeklyHTML = weeklyHTML + '<th><span class="spnReportTitle">' + pObj[i].weeklyAppointments[j].dateOfAppointment.split(' ')[0] + '</th>';
                            arrAppointmentsCount.push(pObj[i].weeklyAppointments[j].dayAppointments.length);
                        }
                        weeklyHTML = weeklyHTML + '<span class="headingSubSpn">Week : ' + pObj[i].weekId + '</span><span class="headingSubSpn floatRight" id="spWeekId' +pObj.id+ pObj[i].weekId + '"></span></tr></thead>';
                        weeklyHTML = weeklyHTML + '<tbody>';


                        var columns = 7;
                        var rows = parseInt(Math.max(...arrAppointmentsCount));

                        var dayWiseDuration = {};

                        for (var rownumber = 0; rownumber < rows; rownumber++) {

                            weeklyHTML = weeklyHTML + '<tr>';

                            for (var columnnumber = 0; columnnumber < columns; columnnumber++) {

                                var tempAppointment = pObj[i].weeklyAppointments[columnnumber].dayAppointments[rownumber];

                                var currentDay = pObj[i].weeklyAppointments[columnnumber].dateOfAppointment.split(' ')[0];

                                if (!(currentDay in dayWiseDuration)) {
                                    dayWiseDuration[currentDay] = 0;
                                }

                                var apptTimeDuration = "";

                                if (tempAppointment !== null && typeof (tempAppointment) !== "undefined") {

                                    var appDate = new Date(tempAppointment.dateOfAppointment);

                                    var strH = kendo.toString(appDate, "hh");
                                    var strm = kendo.toString(appDate, "mm");
                                    var stT = kendo.toString(appDate, "tt");

                                    var time = strH;
                                    if (strm.length > 0) {
                                        time = time + ":" + strm;
                                    }
                                    time = time + stT;
                                    time = time.toLowerCase();

                                    // apptTimeDuration = time + '<br/>' + tempAppointment.duration + 'min';

                                    apptTimeDuration = time + '(' + tempAppointment.duration + ')' + '<br/>' + tempAppointment.provider;

                                    dayWiseDuration[currentDay] = dayWiseDuration[currentDay] + parseInt(tempAppointment.duration);

                                }
                                weeklyHTML = weeklyHTML + '<td>' + apptTimeDuration + '</td>';
                            }

                            weeklyHTML = weeklyHTML + '</tr>';
                        }


                        var durationInWeek = 0;

                        if (dayWiseDuration) {
                            weeklyHTML = weeklyHTML + '<tr class="TotalRow">';
                            for (key in dayWiseDuration) {
                                var totalHours = convertMinsToHrsMins(dayWiseDuration[key]);
                                totalHours = totalHours === '' ? '00:00' : totalHours;
                                providerTotalHrs = Number(providerTotalHrs) + Number(dayWiseDuration[key]);

                                durationInWeek = Number(durationInWeek) + Number(dayWiseDuration[key]);
                                weeklyHTML = weeklyHTML + '<td>' + totalHours + '</td>';
                            }
                            weeklyHTML = weeklyHTML + '</tr>';
                        }


                        weeklyDuration.push({
                            weekId : pObj[i].weekId,
                            duration : durationInWeek
                        })

                        weeklyHTML = weeklyHTML + '</tbody></table>';

                    }

                    strHTML = strHTML + weeklyHTML +'</div></div></div>';

                    $("#divSUCareReport").append(strHTML);
                    //$("#spWeekId" + pObj.id + i).html("Total Weeks Planned Hours: " + convertMinsToHrsMins(providerTotalHrs));

                    for(var w = 0 ; w < weeklyDuration.length ; w++){
                        grandProviderTotalHrs = Number(grandProviderTotalHrs)+weeklyDuration[w].duration;
                        $("#spWeekId"+pObj.id+ weeklyDuration[w].weekId).html("Weekly Planned Hours: " + convertMinsToHrsMins(weeklyDuration[w].duration));
                    }

                    $("#spWeekly" + pObj.id).html("Total Weeks Planned Hours: " + convertMinsToHrsMins(providerTotalHrs));
                    //$("#spWeekId" + i).html("Weekly Planned Hours: " + convertMinsToHrsMins(providerTotalHrs));

                }

            }
        }

        $("#divGrandTotalHours").show();
        $("#tdGrandTotaPlannedHours").text(convertMinsToHrsMins(grandProviderTotalHrs));
    }


}


function onGetServiceUserCareReport(dataObj) {
    
    dtArray = [];
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.appointment) {
            if ($.isArray(dataObj.response.appointment)) {
                dtArray = dataObj.response.appointment;
            } else {
                dtArray.push(dataObj.response.appointment);
            }
        }
    }

    if (dtArray !== null && dtArray.length > 0) {

        var txtDateOfWeek = $("#txtDateOfWeek").data("kendoDatePicker");
        var selectedDate = txtDateOfWeek.value();

        var arrDatesInWeek = [];

        for (var i = 1; i <= 7; i++) {
            var first = selectedDate.getDate() - (selectedDate.getDay() + 1) + i;
            var day = new Date(selectedDate.setDate(first));

            arrDatesInWeek.push(day);
        }

        var allDaysInWeek = [];
        for (var counter4 = 0; counter4 < arrDatesInWeek.length; counter4++) {
            allDaysInWeek.push(daysOfWeek[arrDatesInWeek[counter4].getDay()] + " " + formatDate(arrDatesInWeek[counter4]));
        }

        var appointments = [];

        var lookup = {};
        var providerIds = [];

        for (var item, c = 0; item = dtArray[c++];) {
            if (item.composition && item.composition.provider && item.composition.provider.id) {

                var providerId = item.composition.provider.id;

                if (!(providerId in lookup)) {
                    lookup[providerId] = 1;
                    providerIds.push(providerId);
                }
            }
        }


        var staffAppointments = [];

        if (providerIds !== null && providerIds.length > 0) {

            for (var j = 0; j < providerIds.length; j++) {

                var providerObj = {};


                var tmpSUAppts = $.grep(dtArray, function (e) {
                    return e.composition.provider.id === providerIds[j];
                });

                if (tmpSUAppts !== null && tmpSUAppts.length > 0) {
                    if (tmpSUAppts[0].composition && tmpSUAppts[0].composition.provider && tmpSUAppts[0].composition.provider.id) {
                        providerObj.id = tmpSUAppts[0].composition.provider.id;
                        providerObj.providerName = tmpSUAppts[0].composition.provider.lastName + " " + tmpSUAppts[0].composition.provider.firstName;
                    }

                    // if (tmpSUAppts[0].composition && tmpSUAppts[0].composition.provider && tmpSUAppts[0].composition.provider.dateOfBirth) {

                    //     var dateArr = tmpSUAppts[0].composition.provider.dateOfBirth.split("-");
                    //     var dob = parseInt(dateArr[1]) + "-" + parseInt(dateArr[2]) + "-" + parseInt(dateArr[0]);
                    //     var DOB = new Date(dob);
                    //     var today = new Date();
                    //     var age = today.getTime() - DOB.getTime();
                    //     age = Math.floor(age / (1000 * 60 * 60 * 24 * 365.25));

                    //     providerObj.age = age;

                    // }
                    // if (tmpSUAppts[0].composition && tmpSUAppts[0].composition.provider && tmpSUAppts[0].composition.provider.gender) {
                    //     providerObj.gender = tmpSUAppts[0].composition.provider.gender;
                    // }

                   // var suWeeklyAppts = [];


                    for (var counter = 0; counter < arrDatesInWeek.length; counter++) {

                        var obj = {};
                        var suWeeklyAppts = [];

                        var tmpDate = arrDatesInWeek[counter];
                        tmpDate.setHours(0, 0, 0, 0);


                        var tmpAppointments = $.grep(tmpSUAppts, function (e) {
                            var apptDate = new Date(e.dateOfAppointment);
                            apptDate.setHours(0, 0, 0, 0);
                            return apptDate.getTime();
                        });

                        obj.dateOfAppointment = allDaysInWeek[counter];
                        obj.dayAppointments = [];
                        

                        if (tmpAppointments != null && tmpAppointments.length > 0) {
                            obj.dayAppointments = tmpAppointments;

                        }

                        suWeeklyAppts.push(obj);

                    }
                    providerObj.weeklyAppointments = suWeeklyAppts;
                }

                staffAppointments.push(providerObj);
            }
            console.log(JSON.stringify(staffAppointments));
            bindServiceUserCareReport(staffAppointments);
        }

        
    }
    else {
        customAlert.info("Info", "No records found");
    }
}


function bindServiceUserCareReport(staffAppointments) {

    $("#divSUCareReport").html("");
    if (staffAppointments !== null && staffAppointments.length > 0) {

        for (var i = 0; i < staffAppointments.length; i++) {

            var pObj = staffAppointments[i];

            var strHTML = '';

            strHTML = strHTML + '<div class="table-block">';
            strHTML = strHTML + '<div class="timeSheetReportdivHeading">';
            strHTML = strHTML + pObj.providerName;
            //strHTML = strHTML + '<span class="headingSpn floatRight">';
            //strHTML = strHTML + '(' + pObj.age + '/' + pObj.gender.charAt(0) + ')</span>';
            strHTML = strHTML + '</div>';
            strHTML = strHTML + '<div class="col-xs-12 taskReportTblBlock"><div class="table-responsive">';
            strHTML = strHTML + '<table class="tblTaskreport weeklyCareReportTbl"><thead><tr>';

            var arrAppointmentsCount = [];

            for (var j = 0; j < pObj.weeklyAppointments.length; j++) {

                strHTML = strHTML + '<th><span class="spnReportTitle">' + pObj.weeklyAppointments[j].dateOfAppointment.split(' ')[0] + '</th>';
                arrAppointmentsCount.push(pObj.weeklyAppointments[j].dayAppointments.length);
            }
            strHTML = strHTML + '</tr></thead>';
            strHTML = strHTML + '<tbody>';

            var columns = 7;
            var rows = parseInt(Math.max(...arrAppointmentsCount));

            var dayWiseDuration = {};

            for (var rownumber = 0; rownumber < rows; rownumber++) {

                strHTML = strHTML + '<tr>';

                for (var columnnumber = 0; columnnumber < columns; columnnumber++) {

                    var tempAppointment = pObj.weeklyAppointments[columnnumber].dayAppointments[rownumber];

                    var currentDay = pObj.weeklyAppointments[columnnumber].dateOfAppointment.split(' ')[0];

                    if (!(currentDay in dayWiseDuration)){
                        dayWiseDuration[currentDay] = 0;
                    }
                   

                    var apptTimeDuration = "";

                    if (tempAppointment !== null && typeof (tempAppointment) !== "undefined") {

                        var appDate = new Date(tempAppointment.dateOfAppointment);

                        var strH = kendo.toString(appDate, "hh");
                        var strm = kendo.toString(appDate, "mm");
                        var stT = kendo.toString(appDate, "tt");

                        var time = strH;
                        if (strm.length > 0) {
                            time = time + ":" + strm;
                        }
                        time = time + stT;
                        time = time.toLowerCase();

                        apptTimeDuration = time + '(' + tempAppointment.duration + ')' + '<br/>' + tempAppointment.provider;

                        dayWiseDuration[currentDay] = dayWiseDuration[currentDay] + parseInt(tempAppointment.duration);

                    }


                    strHTML = strHTML + '<td>' + apptTimeDuration + '</td>';

                }

                strHTML = strHTML + '</tr>';
            }

            if(dayWiseDuration){
                strHTML = strHTML + '<tr class="TotalRow">';
                for(key in dayWiseDuration)   {
                    var totalHours = convertMinsToHrsMins(dayWiseDuration[key]);
                    totalHours = totalHours === '' ? '00:00' : totalHours;
                    strHTML = strHTML + '<td>'+totalHours+'Hrs</td>';
                }
                strHTML = strHTML + '</tr>';
            }


            strHTML = strHTML + '</tbody></table></div></div></div>';

            $("#divSUCareReport").append(strHTML);

        }

    }

}



function generateReportTables(reportViewerJson){
    if(reportViewerJson != null && reportViewerJson.length > 0){
        var insertAfterElement="";
        for(var counter = 0;counter < reportViewerJson.length;counter++){
            var reportDisplayName = reportViewerJson[counter].ProviderName;
            var reportDisplayNameId = "StaffTimeSheet" + (counter+1);
            var $report = $("<div class=\"col-xs-12 table-block\" id=\"div"+reportDisplayNameId+"\"> <div class=\"col-xs-12 timeSheetReportdivHeading\" style='page-break-before: always !important;'>"+reportDisplayName+" </div><div class=\"table-responsive\"> <table class=\"timesheetmembersTable\" id=\"tbl"+reportDisplayNameId+"\"> </table> </div> </div>");

            $("#divStaffTimeSheetReportViewer").append($report);
            bindTimeSheetReport(reportDisplayNameId,reportViewerJson[counter]);
        }
    }
}


function bindTimeSheetReport(reportDisplayNameId, reportData) {
    var dataObj = reportData.dataSource;

    var tableId = "#tbl"+reportDisplayNameId;


    var strHeading = "<thead>";
    var columns = [];
    if (dataObj != null && dataObj.length > 0) {
        var firstObj = dataObj[0];
        var c = 0;
        for (var key in firstObj) {
            if(key.toLowerCase() == "appt date time") {
                strHeading = strHeading + "<tr style=\"page-break-inside: avoid\"><th class=\"borderbuttomthin\" style='width:12%'>" + key + "</th>";
            }else{
                if (c === 0) {
                    strHeading = strHeading + "<th></th>";
                }
                else{
                    if(key.toLowerCase() == "serviceuser"){
                        strHeading = strHeading + "<th class='borderbuttomthin'>Service User</th>";
                    }
                    else if(key.toLowerCase() == "datetimeofappointment"){
                        strHeading = strHeading + "<th class='borderbuttomthin'>Appointment Date Time</th>";
                    }
                    else if(key.toLowerCase() == "scheduledhours"){
                        strHeading = strHeading + "<th class='borderbuttomthin'>Scheduled Hrs</th>";
                    }
                    else if(key.toLowerCase() == "actualhours"){
                        strHeading = strHeading + "<th class='borderbuttomthin'>Actual Hrs</th></tr>";
                    }
                }

            }
            columns.push(key);
            c = c + 1;
        }
    }
    strHeading = strHeading+"</thead>";

    $(tableId).append(strHeading);


    var strTableBody = "<tbody>";

    for(var counter = 0;counter<dataObj.length;counter++){
        var strTableRow = "<tr style=\"page-break-inside: avoid\">";

        for(var counter2= 0;counter2<columns.length;counter2++){
                if (counter2 > 0 && counter === dataObj.length-1) {
                    strTableRow = strTableRow + "<td class=\"borderbuttomthick\">"+ dataObj[counter][columns[counter2]]+"</td>";
                }
                else{
                    strTableRow = strTableRow + "<td>"+ dataObj[counter][columns[counter2]]+"</td>";
                }


        }
        strTableRow = strTableRow+"</tr>";

        strTableBody = strTableBody +strTableRow;
    }

    var totalRow = '<tr class="timesheetmembersTable-total" style="page-break-inside: avoid"><td></td><td align="left">Total Scheduled &amp; Actual Hours</td><td class=\"show' +
        '\">'+reportData.totalScheduledHours+'</td><td class=\"showgrandtotal\">'+reportData.totalActualHours+'</td></tr>';
    strTableBody = strTableBody+totalRow;
    strTableBody = strTableBody+"</tbody>";
    $(tableId).append(strTableBody);


}

function onPatientListData(dataObj) {
    dtArray = [];
    buildDeviceListGrid([]);
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.appointment) {
            if ($.isArray(dataObj.response.appointment)) {
                dtArray = dataObj.response.appointment;
            } else {
                dtArray.push(dataObj.response.appointment);
            }
        }
    }
    dtArray.sort(function (a, b) {
        var nameA = a.dateOfAppointment; // ignore upper and lowercase
        var nameB = b.dateOfAppointment; // ignore upper and lowercase
        if (nameA < nameB) {
            return -1;
        }
        if (nameA > nameB) {
            return 1;
        }
        // names must be equal
        return 0;
    });

    if (dtArray.length > 0) {
        for (var i = 0; i < dtArray.length; i++) {
            // $("#btnCopy").css("display", "");
            dtArray[i].idk = dtArray[i].id;
            dtArray[i].name = dtArray[i].composition.patient.lastName + " " + dtArray[i].composition.patient.firstName + " " + dtArray[i].composition.patient.middleName;


            var date = new Date(GetDateTimeEditDay(dtArray[i].dateOfAppointment));
            var day = date.getDay();
            var dow = getWeekDayName(day);

            var dtEnd = dtArray[i].dateOfAppointment + (Number(dtArray[i].duration) * 60000);

            dtArray[i].dateTimeOfAppointment = dow + " " + GetDateTimeEdit(dtArray[i].dateOfAppointment);
            dtArray[i].DW = dow;

            var dateED = new Date(GetDateTimeEditDay(dtEnd));
            var dayED = dateED.getDay();
            var dowED = getWeekDayName(dayED);

            dtArray[i].dateTimeOfAppointmentED = dowED + " " + GetDateTimeEdit(dtEnd);


            var dt = new Date(dtArray[i].dateOfAppointment);
            var strDT = "";
            if (dt) {
                strDT = kendo.toString(dt, "MM/dd/yyyy h:mm tt");
            }

            dtArray[i].dateTimeOfAppointment1 = strDT;


            dtArray[i].appointmentReasonDesc = dtArray[i].composition.appointmentReason.desc;
            dtArray[i].appointmentTypeDesc = dtArray[i].composition.appointmentType.desc;
            if (dtArray[i].providerId != 0) {
                dtArray[i].staffName = dtArray[i].composition.provider.lastName + " " + dtArray[i].composition.provider.firstName + " " + (dtArray[i].composition.provider.middleName != null ? dtArray[i].composition.provider.middleName : " ");
            } else {
                dtArray[i].staffName = "";
            }
        }


        buildDeviceListGrid(dtArray);
    }
    else {
        // $("#btnCopy").css("display", "none");
        customAlert.info("Info", "No appointments");
    }
}

function onClickCopy() {
    var selGridData = angularUIgridWrapper.getAllRows();
    var selList = [];
    for (var i = 0; i < selGridData.length; i++) {

        var dataRow = selGridData[i].entity;
        dataRow.appointmentType = "Con";
        dataRow.appointmentTypeDesc = "Confirmed";
        dataRow.composition.appointmentType.desc = "Confirmed";
        dataRow.composition.appointmentType.id = 7;
        dataRow.composition.appointmentType.value = "Con";
        selList.push(dataRow);
    }

    // if(selList.length>0){
    sessionStorage.setItem("appselList", JSON.stringify(selList));
    parentRef.appselList = selList;
    var popW = "60%";
    var popH = "80%";
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Copy To Appointments";
    parentRef.selValue = allDatesInWeek;
    devModelWindowWrapper.openPageWindow("../../html/masters/createCopyAppointment.html", profileLbl, popW, popH, true, copyAppointment);
    parent.setPopupWIndowHeaderColor("0bc56f", "FFF");
    // }
}
function copyAppointment(dataObj) {
    parentRef.selValue = null;
    parentRef.appselList = null;
    onClickSubmit();
}

function onCloseEditAppointment(dataObj) {
    parentRef.selectedItems = null;
    buildDeviceListGrid([]);
    onClickViewServiceUserCareReport();

}


function getWeekDayName(wk) {
    var wn = "";
    if (wk == 1) {
        return "Mon";
    } else if (wk == 2) {
        return "Tue";
    } else if (wk == 3) {
        return "Wed";
    } else if (wk == 4) {
        return "Thu";
    } else if (wk == 5) {
        return "Fri";
    } else if (wk == 6) {
        return "Sat";
    } else if (wk == 0) {
        return "Sun";
    }
    return wn;
}

function getFromDate(fromDate) {


    var day = fromDate.getDate();
    var month = fromDate.getMonth();
    month = month + 1;
    var year = fromDate.getFullYear();

    var strDate = month + "/" + day + "/" + year;
    strDate = strDate + " 00:00:00";

    var date = new Date(strDate);
    var strDateTime = date.getTime();

    return strDateTime;
}

function getToDate(toDate) {


    var day = toDate.getDate();
    var month = toDate.getMonth();
    month = month + 1;
    var year = toDate.getFullYear();

    var strDate = month + "/" + day + "/" + year;
    strDate = strDate + " 23:59:59";

    var date = new Date(strDate);
    var strDateTime = date.getTime();

    return strDateTime;
}

var sortByDateAsc = function (x, y) {
    // This is a comparison function that will result in dates being sorted in
    // ASCENDING order. As you can see, JavaScript's native comparison operators
    // can be used to compare dates. This was news to me.

    var date1 = new Date(x.dateOfAppointment);
    var date2 = new Date(y.dateOfAppointment);

    if (date1 > date2) return 1;
    if (date1 < date2) return -1;
    return 0;
};

function formatDate(date) {

    var dd = date.getDate();

    var mm = date.getMonth() + 1;
    var yyyy = date.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }

    if (mm < 10) {
        mm = '0' + mm;
    }

    return dd + '/' + mm + '/' + yyyy;
}

function getLanguageValueList(dataObj) {
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.codeTable) {
        if ($.isArray(dataObj.response.codeTable)) {
            dataArray = dataObj.response.codeTable;
        } else {
            dataArray.push(dataObj.response.codeTable);
        }
    }
    for (var i = 0; i < dataArray.length; i++) {
        dataArray[i].idk = dataArray[i].id;
        $("#cmbLang").append('<option value="' + dataArray[i].idk + '">' + dataArray[i].desc + '</option>');
    }
}

function getGenderValueList(dataObj) {
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.codeTable) {
        if ($.isArray(dataObj.response.codeTable)) {
            dataArray = dataObj.response.codeTable;
        } else {
            dataArray.push(dataObj.response.codeTable);
        }
    }
    for (var i = 0; i < dataArray.length; i++) {
        dataArray[i].idk = dataArray[i].id;
        $("#cmbGender").append('<option value="' + dataArray[i].idk + '">' + dataArray[i].desc + '</option>');
    }
}


function getPatientList(dataObj){
    var tempArray = [];
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.patient){
            if($.isArray(dataObj.response.patient)){
                tempArray = dataObj.response.patient;
            }else{
                tempArray.push(dataObj.response.patient);
            }
        }
    }else{
        var msg = dataObj.response.status.message;
        customAlert.error("Error",msg.replace("patient","service user"));
    }

    suArray = tempArray;
    tempArray.sort(function (a, b) {
        var p1 = a.lastName+ " " + a.firstName;
        var p2 = b.lastName+ " " + b.firstName;

        if (p1 < p2) return -1;
        if (p1 > p2) return 1;
        return 0;
    });

    for(var i=0;i<tempArray.length;i++){
        var obj = tempArray[i];
        if(obj){
            var dataItemObj = {};
            var suName = obj.lastName + " " + obj.firstName;
            if(suName != null && suName != " ") {
                $("#cmbServiceUser").append('<option value="' + obj.id + '">' + suName + '</option>');
            }

        }
    }

    $("#cmbServiceUser").multipleSelect({
        selectAll: true,
        width: 200,
        dropWidth: 200,
        multipleWidth: 200,
        placeholder: 'Select Service User',
        selectAllText: 'All'

    });

}

function onClickServiceUser(){
    var popW = 800;
    var popH = 450;

    if(parentRef)
        parentRef.searchZip = true;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Service User";
    devModelWindowWrapper.openPageWindow("../../html/patients/searchPatient.html", profileLbl, popW, popH, true, onCloseServiceUserAction);
}
var selPatientId = 0;
var suArray = [];
function onCloseServiceUserAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        selPatientId = returnData.selItem.PID;
        $('#txtServiceUser').val(returnData.selItem.LN + " "+ returnData.selItem.FN);
        getAjaxObject(ipAddress+"/patient/"+selPatientId,"GET",onGetPatientInfo,onError);

    }
}

function onGetPatientInfo(dataObj) {
    suArray = [];
    if (dataObj && dataObj.response && dataObj.response.patient) {
        suArray = dataObj.response;
    }
}
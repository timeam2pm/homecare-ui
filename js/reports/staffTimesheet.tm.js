﻿var angularUIgridWrapper;
var parentRef = null;
var recordType = "1";
var dataArray = [];
var ADD = "add";
var UPDATE = "edit";
var VIEW = "view";
var DELETE = "delete";
var operation = "add";
var ds = "";
var IsFlag = 1;
var cntry = sessionStorage.countryName;
var selectPatientId,selProviderId;
var dtFMT = "dd/MM/yyyy";
var allDatesInWeek = [];

$(document).ready(function () {
    $("#pnlPatient", parent.document).css("display", "none");
    sessionStorage.setItem("IsSearchPanel", "1");
    //themeAPIChange();
    parentRef = parent.frames['iframe'].window;
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    var cntry = sessionStorage.countryName;
    if((cntry.indexOf("India")>=0) || (cntry.indexOf("United Kingdom")>=0)){
        $("#lblFacility").text("Location :");
    }else if(cntry.indexOf("United State")>=0) {
        $("#lblFacility").text("Facility :");
    }
    angularUIgridWrapper = new AngularUIGridWrapper("dgridStaffTimesheets", dataOptions);
    angularUIgridWrapper.init();
    buildDeviceListGrid([]);

});


$(window).load(function () {
    loading = false;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded() {

    init();
    buttonEvents();
    adjustHeight();
}

function init() {
    //$("#btnEdit").prop("disabled", true);
    //$("#btnDelete").prop("disabled", true);

    getAjaxObject(ipAddress + "/facility/list/?is-active=1", "GET", getFacilityList, onError);

    //// if(cntry.indexOf("India")>=0){
    ////     $("#txtFDate").datepick({
    ////         changeMonth: true,
    ////         changeYear: true,
    ////         dateFormat: 'dd/mm/yyyy',
    ////         yearRange: "-200:+200"
    ////     });
    //// }else if(cntry.indexOf("United Kingdom")>=0){
    ////     $("#txtFDate").datepick({
    ////         changeMonth: true,
    ////         changeYear: true,
    ////         dateFormat: 'dd/mm/yyyy',
    ////         yearRange: "-200:+200"
    ////     });
    //// }else{
    ////     $("#txtFDate").datepick({
    ////         changeMonth: true,
    ////         changeYear: true,
    ////         dateFormat: 'mm/dd/yyyy',
    ////         yearRange: "-200:+200"
    ////     });
    //// }

    $("#txtFDate").kendoDatePicker({ format: dtFMT, value: new Date() });
    $("#txtTDate").kendoDatePicker({ format: dtFMT, value: new Date() });

}



function onError(errorObj) {
    console.log(errorObj);
}

function buttonEvents() {

    $("#btnSubmit").off("click", onClickViewStaffTimesheet);
    $("#btnSubmit").on("click", onClickViewStaffTimesheet);

    $("#btnEdit").off("click", onClickEdit);
    $("#btnEdit").on("click", onClickEdit);

    //$("#btnDelete").off("click", onClickDelete);
    //$("#btnDelete").on("click", onClickDelete);

    //$("#btnCopy").off("click");
    //$("#btnCopy").on("click", onClickCopy);

    //$("#btnCancel").off("click", onClickCancel);
    //$("#btnCancel").on("click", onClickCancel);

    //$("#btnReset").off("click", onClickReset);
    //$("#btnReset").on("click", onClickReset);

    //$(".popupClose").off("click", onClickCancel);
    //$(".popupClose").on("click", onClickCancel);

    $("#btnStaffUser").off("click");
    $("#btnStaffUser").on("click", onClickStaff);


    //$(".btnActive").on("click", function (e) {
    //    e.preventDefault();
    //    $('.alert').remove();
    //    searchOnLoad('active');
    //    onClickActive();
    //});
    //$(".btnInActive").on("click", function (e) {
    //    e.preventDefault();
    //    $('.alert').remove();
    //    searchOnLoad('inactive');
    //    onClickInActive();
    //});
    $("#btnPrint").off("click");
    $("#btnPrint").on("click",onClickPrint);

    $('input[type="radio"].StaffViewType').on('change', function () {
        if ($("#rbtnOld").is(":checked")) {
            $("#btnPrint").hide();
            selProviderId = null;
            $("#txtStaffUser").val("");
            $("#divOldTimeSheetReportTableBlock").show();
            $("#divStaffTimeSheetReportViewer").empty();
            $("#divStaffTimeSheetReport").hide();
            $("#divGrandTotalHours").hide();
            $("#tdGrandTotaScheduledHours").text("");
            $("#tdGrandTotaActualHours").text("");
        }
        else {
            $("#btnPrint").show();
            $("#divOldTimeSheetReportTableBlock").hide();
            $("#divStaffTimeSheetReport").show();
            buildDeviceListGrid([]);
        }
    });

}

function onClickPrint(){
    if ($.trim($('#divStaffTimeSheetReportViewer').html()) !== "") {

        var facilityId = $("#cmbFacility").val();
        var communications;
        var communicationsDeatils1, communicationsDeatils2;
        var tempFacilityDataArry = _.where(facilityDataArry, {idk: Number(facilityId)});
        if(tempFacilityDataArry[0].communications && tempFacilityDataArry[0].communications != null) {
            communications = tempFacilityDataArry[0].communications;
            if (communications && communications != null) {
                if (communications[0].address1 != "") {
                    communicationsDeatils1 = communications[0].address1;
                }

                if (communications[0].address2 != "") {
                    communicationsDeatils1 = communicationsDeatils1 + ", " + communications[0].address2;
                }

                if (communications[0].city != "") {
                    communicationsDeatils2 = communications[0].city;
                }

                if (communications[0].state != "") {
                    communicationsDeatils2 = communicationsDeatils2 + ", " + communications[0].state;
                }

                if (communications[0].zip != "") {
                    communicationsDeatils2 = communicationsDeatils2 + ", " + communications[0].zip;
                }
            }
        }

        var contents = document.getElementById("divPrint").outerHTML;// $("#divTaskReportsViewer").html();
        var frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({ "position": "absolute", "top": "-1000000px" });
        $("body").append(frame1);
        var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        //Create a new HTML document.
        frameDoc.document.write('<html><title>Timesheet Report</title><head>');
        frameDoc.document.write('<link href="../../css/Theme01.css" rel="stylesheet" type="text/css" />');
        frameDoc.document.write('<link href="../../css/report/invoicestyle.css" rel="stylesheet" type="text/css" />');
        // frameDoc.document.write('<link href="../../css/report/staffTime.css" rel="stylesheet" type="text/css" />');
        frameDoc.document.write('</head><body style="background-color: #fff;">');
        //frameDoc.document.write('<div class="col-xs-12 ibox"> <div class="col-xs-12 ibox-content"> <div class="col-xs-12 formArea">');
        var facility = $("#cmbFacility option:selected").text();
        var fromDate = $("#txtFDate").val();
        var toDate = $("#txtTDate").val();
        var staff = $("#txtStaffUser").val().length === 0 ? "ALL":$("#txtStaffUser").val();

        var strDate;
        var DOB = new Date();
        strDate = kendo.toString(DOB, "dd/MM/yyyy hh:mm:ss tt");

        frameDoc.document.write('<table class="tblInvoiceHeader"><tr style="page-break-inside: avoid"><td><div class="reportName"><h2><span style="color:#D5AC57">Timesheet Report</span> <br clear="all"><small style="color:#000000 ;">'+ strDate +'</small></h2></div></td>');
        frameDoc.document.write('<td><div class="reportLocations"><h2><span>'+facility+'<span></span></span></h2><address> '+ communicationsDeatils1 + '<br>'+communicationsDeatils2+'</address></div> </td></tr></table><br clear="all">');

        // frameDoc.document.write('<div class="reportName"><h2><span>Timesheet Report</span> <br clear="all"><small>'+ strDate +'</small></h2></div>');
        // frameDoc.document.write('<div class="reportLocations"><h2><span>'+facility+'<span></span></span></h2><address> '+ communicationsDeatils1 + '<br>'+communicationsDeatils2+'</address></div> <br clear="all">');

        frameDoc.document.write('<div class="searchedLabels">');
        frameDoc.document.write('<div class="divLabel"><span>Location :</span> <label>'+facility+'</label></div>');
        frameDoc.document.write('<div class="divLabel"><span>From Date :</span>  <label>'+fromDate+'</label></div>');
        frameDoc.document.write('<div class="divLabel"><span>To Date :</span> <label>'+toDate+'</label></div>');
        frameDoc.document.write('<div class="divLabel"><span>Staff :</span><span></span> <label>'+staff+'</label></div>');
        frameDoc.document.write('</div>');

        //Append the libs CSS file.
        //frameDoc.document.write('<link href="../../css/libs/css/bootstrap.min.css" rel="stylesheet" type="text/css" />');
        //frameDoc.document.write('<link href="../../css/libs/css/allcss.css" rel="stylesheet" type="text/css" />');



        frameDoc.document.write('<div class="col-xs-12">');
        //Append the DIV contents.
        frameDoc.document.write(contents);


        var grandTotalContents = document.getElementById("divGrandTotalHours").outerHTML;

        frameDoc.document.write('<div class="col-xs-12">');
        frameDoc.document.write(grandTotalContents);
        frameDoc.document.write('</div>');


        frameDoc.document.write('</div>');
       //frameDoc.document.write('</div> </div> </div>');
        frameDoc.document.write('</body></html>');


        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();

        }, 500);
    }
    else {
        customAlert.info("info","No data to print.");
    }
}

function searchOnLoad(status) {
    buildDeviceListGrid([]);
    if (status == "active") {
        var urlExtn = ipAddress + "/homecare/vacations/?fields=*&parentTypeId=" + Number(parentRef.type) + "&parentId=" + Number(parentRef.patientId) + "&is-active=1&is-deleted=0";

    }
    else if (status == "inactive") {
        var urlExtn = ipAddress + "/homecare/vacations/?fields=*&parentTypeId=" + Number(parentRef.type) + "&parentId=" + Number(parentRef.patientId) + "&is-active=0&is-deleted=1"
    }
    getAjaxObject(urlExtn, "GET", handleGetVacationList, onError);
}


function onClickAdd() {
    $("#addPopup").show();
    $('#viewDivBlock').hide();
    $("#txtID").hide();
    $(".filter-heading").html("Add Staff Leave Request");
    parentRef.operation = "add";
    onClickReset();
}

function addReportMaster(opr) {
    var popW = 500;
    var popH = "43%";

    //$('.listDataWrapper').hide();
    //$('.addOrRemoveWrapper').show();
    $('.alert').remove();
    var profileLbl = "Add Task Type";
    parentRef.operation = opr;
    operation = opr;
    if (opr == "add") {
        $('#btnSave').html('<span><img src="../../img/medication/Save.png" class="providerLink-btn-img" />Save</span>');
        $('.tabContentTitle').text('Add Task Type');
        $('#btnReset').show();
        //var billActName = $("#txtBAN").val();
        $('#btnReset').trigger('click');
        //$("#txtBAN").val(billActName);
    } else {

        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if (selectedItems && selectedItems.length > 0) {
            var obj = selectedItems[0];
            parentRef.id = obj.idk;
            parentRef.displayOrder = obj.displayOrder;
            parentRef.type = obj.type;
            parentRef.DIO = obj.DIO;
            parentRef.activityGroup = obj.activityGroup;
            parentRef.isActive = obj.isActive;
            operation = UPDATE;
        }

        profileLbl = "Edit Task Type";
        $('.tabContentTitle').text('Edit Task Type');
        $('#btnReset').hide();
        $('#btnSave').html('<span><img src="../../img/medication/Save.png" class="providerLink-btn-img" />Update</span>');
        $('#btnReset').trigger('click');
    }
    var devModelWindowWrapper = new kendoWindowWrapper();
    devModelWindowWrapper.openPageWindow("../../html/masters/createactivityTypeList.html", profileLbl, popW, popH, true, closeaddReportMastertAction);
}
function closeaddReportMastertAction(evt, returnData) {
    if (returnData && returnData.status == "success") {
        var opr = returnData.operation;
        if (opr == "add") {
            customAlert.info("info", "Task Type created successfully");
        } else {
            customAlert.info("info", "Task Type updated successfully");
        }
        init();
    }
}
function onClickActive() {
    $(".btnInActive").removeClass("radioButton-active");
    $(".btnActive").addClass("radioButton-active");
}

function onClickInActive() {
    $(".btnActive").removeClass("radioButton-active");
    $(".btnInActive").addClass("radioButton-active");
}

function onClickDelete() {
    customAlert.confirm("Confirm", "Are you sure to delete?", function (response) {
        if (response.button == "Yes") {
            var dataObj = {};
            dataObj.createdBy = Number(sessionStorage.userId);
            dataObj.isDeleted = 1;
            dataObj.isActive = 0;
            var dataUrl = ipAddress + "/homecare/vacations/";
            var method = "DELETE";
            var selectedItems = angularUIgridWrapper.getSelectedRows();
            dataObj.id = selectedItems[0].idk;
            createAjaxObject(dataUrl, dataObj, method, onCreateDelete, onError);
        }
    });
}

function onCreateDelete(dataObj) {
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            var msg = "Vacation deleted successfully";
            customAlert.error("Info", msg);
            operation = ADD;
            init();
        }
    }
}


function onClickCancel() {
    $(".filter-heading").html("View Appointments");
    $("#viewDivBlock").show();
    $("#addPopup").hide();
}

function adjustHeight() {
    var defHeight = 280;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    angularUIgridWrapper.adjustGridHeight(cmpHeight);
}

function buildDeviceListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    otoptions.rowHeight = 100;
    gridColumns.push({
        "title": "Staff",
        "field": "Provider"
    });
    // gridColumns.push({
    //     "title": "Weekly Contract Hours",
    //     "field": "weeklyContractHours"
    // });

    gridColumns.push({
        "title": "Appointment Date Time",
        "field": "dateTimeOfAppointment"
    });
    /*gridColumns.push({
        "title": "DOW",
        "field": "dow"
    });*/
    gridColumns.push({
        "title": "Scheduled Hrs",
        "field": "durationTime"
    });

    gridColumns.push({
        "title": "Actual Hrs",
        "field": "actualDurationTime"
    });

    // if ($("#rbtnDetail").is(":checked")) {
    //     gridColumns.push({
    //         "title": "Comments",
    //         "field": "notes"
    //     });
    // }

    angularUIgridWrapper.creategrid(dataSource, gridColumns, otoptions);
    adjustHeight();
}

function onChange() {
    setTimeout(function () {
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        if (selectedItems && selectedItems.length > 0) {

            if ($("#rbtnDetail").is(":checked")) {
                $("#btnEdit").prop("disabled", false);
            }
            else {
                $("#btnEdit").prop("disabled", true);
            }

            //$("#btnEdit").prop("disabled", false);
        } else {
            $("#btnEdit").prop("disabled", true);
        }
    }, 100);
}

function onClickEdit() {

    var selectedItems = angularUIgridWrapper.getSelectedRows();

    
    parentRef.selectedItems = selectedItems;
    parentRef.facilityName = $("#cmbFacility option:selected").text();


    var popW = "50%";
    var popH = "60%";
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Edit Appointment";
    
    devModelWindowWrapper.openPageWindow("../../html/masters/editAppointment.html", profileLbl, popW, popH, true, onCloseEditAppointment);
    parent.setPopupWIndowHeaderColor("0bc56f", "FFF");

    //$("#viewDivBlock").hide();
    //$("#addPopup").show();
    //var selectedItems = angularUIgridWrapper.getSelectedRows();
    //if (selectedItems && selectedItems.length > 0) {
    //    var obj = selectedItems[0];
    //    if (obj.idk) {
    //        appointmentId = obj.idk;
    //    }
    //    else {
    //        appointmentId = obj.id;
    //    }

    //    if (obj.composition && obj.composition.appointmentReason.value) {
    //        $("#cmbReason").val(obj.composition.appointmentReason.value);
    //    } else {
    //        $("#cmbReason").val(obj.appointmentReason);
    //    }
    //    if (obj.composition && obj.composition.appointmentType) {
    //        $("#cmbStatus").val(obj.composition.appointmentType.value);
    //    }
    //    else {
    //        $("#cmbStatus").val(obj.appointmentType);
    //    }

    //    selectPatientId = obj.patientId;
    //    selectProviderId = obj.providerId;
    //    if (selectProviderId != 0) {
    //        if (obj.composition && obj.composition.provider) {
    //            $("#txtStaff").val(obj.composition.provider.lastName + " " + obj.composition.provider.firstName + " " + (obj.composition.provider.middleName != null ? obj.composition.provider.middleName : " "));
    //        } else {
    //            $("#txtStaff").val(obj.staffName);
    //        }

    //    }
    //    else {
    //        $("#txtStaff").val("");
    //    }
    //    $("#txtFacility").val(parentRef.facilityName);
    //    var txtStartDate = $("#txtStartDate").data("kendoDateTimePicker");
    //    if (txtStartDate) {
    //        txtStartDate.value(GetDateTimeEdit(obj.dateTimeOfAppointment));
    //    }
    //    var endappoinment = (obj.dateTimeOfAppointment + (Number(obj.duration) * 60000));
    //    var txtEndDate = $("#txtEndDate").data("kendoDateTimePicker");
    //    if (txtEndDate) {
    //        txtEndDate.value(GetDateTimeEdit(endappoinment));
    //    }
    //    $("#txtPayout").val(obj.payoutHourlyRate);
    //    $("#cmbBilling").val(obj.billToName);
    //}
}


var operation = "add";
var selFileResourceDataItem = null;

function onStatusChange() {
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if (cmbStatus && cmbStatus.selectedIndex < 0) {
        cmbStatus.select(0);
    }
}

function onClickReset() {
    $("#txtReason").val("");
    $("#txtFDate").val("");
    $("#txtTDate").val("");
    operation = ADD;
}

function themeAPIChange() {
    getAjaxObject(ipAddress + "/homecare/settings/?id=2", "GET", getThemeValue, onError);
}

function getThemeValue(dataObj) {
    console.log(dataObj);
    var tempCompType = [];
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.activityTypes) {
            if ($.isArray(dataObj.response.settings)) {
                tempCompType = dataObj.response.settings;
            } else {
                tempCompType.push(dataObj.response.settings);
            }
        }
        if (tempCompType.length > 0) {
            themesChange(tempCompType[0].value);
        }
    }
}

function themesChange(themeValue) {
    if (themeValue == 2) {
        loadAPi("../../Theme2/Theme02.css");
    }
    else if (themeValue == 3) {
        loadAPi("../../Theme3/Theme03.css");
    }
}

var facilityDataArry = [];

function getFacilityList(dataObj) {
    var dataArray = [];
    if (dataObj) {
        if ($.isArray(dataObj.response.facility)) {
            dataArray = dataObj.response.facility;
        } else {
            dataArray.push(dataObj.response.facility);
        }
    }
    facilityDataArry = dataArray;
    for (var i = 0; i < dataArray.length; i++) {
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if (dataArray[i].isActive == 1) {
            dataArray[i].Status = "Active";
        }
        $("#cmbFacility").append('<option value="' + dataArray[i].idk + '">' + dataArray[i].name + '</option>');
    }
}

function onClickServiceUser() {
    var popW = 800;
    var popH = 450;

    if (parentRef)
        parentRef.searchZip = true;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Service User";
    devModelWindowWrapper.openPageWindow("../../html/patients/searchPatient.html", profileLbl, popW, popH, true, onCloseServiceUserAction);
}
function onCloseServiceUserAction(evt, returnData) {
    if (returnData && returnData.status == "success") {
        selectPatientId = returnData.selItem.PID;
        $('#txtSU').val(returnData.selItem.LN + " " + returnData.selItem.FN + " " + returnData.selItem.MN);
    }
}

function onClickStaff(){
    var popW = 800;
    var popH = 450;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Staff";
    devModelWindowWrapper.openPageWindow("../../html/patients/staffList.html", profileLbl, popW, popH, true, onCloseStaffAction);
}

function onCloseStaffAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        selProviderId = returnData.selItem.PID;
        $('#txtStaffUser').val(returnData.selItem.LN+' '+returnData.selItem.FN);

    }
}

function onClickSubmit() {
    if ($("#txtSU").val() != "") {
        if ($("#txtFDate").val() !== "") {
            var txtAppPTDate = $("#txtFDate").data("kendoDatePicker");
            var selectedDate = txtAppPTDate.value();
            var selDate = new Date(selectedDate);


            // var selectedDate = GetDateTime("txtDate");


            for (let i = 1; i <= 7; i++) {
                var first = selectedDate.getDate() - (selectedDate.getDay() + 1) + i;
                var day = new Date(selectedDate.setDate(first));

                allDatesInWeek.push(day);
            }
            var day = selDate.getDate();
            var month = selDate.getMonth();
            month = month + 1;
            var year = selDate.getFullYear();

            var stDate = month + "/" + day + "/" + year;
            stDate = stDate + " 00:00:00";

            var startDate = new Date(stDate);
            var stDateTime = startDate.getTime();

            var etDate = month + "/" + day + "/" + year;
            etDate = etDate + " 23:59:59";

            var endtDate = new Date(etDate);
            var edDateTime = endtDate.getTime();

            var startDate = new Date(stDate);
            var endtDate = new Date(etDate);

            var day = startDate.getDate() - startDate.getDay();
            var pDate = new Date(startDate);
            pDate.setDate(day);

            endtDate.setDate(day + 6);
            var stDateTime = pDate.getTime();
            var edDateTime = endtDate.getTime();
            var txtAppPTFacility = $("#cmbFacility").val();
            parentRef.facilityId = txtAppPTFacility;
            parentRef.patientId = selectPatientId;
            parentRef.SUName = $("#txtSU").val();
            parentRef.facilityName = $("#cmbFacility option:selected").text();
            parentRef.selDate = pDate;
            parentRef.copyDate = txtAppPTDate.value();

            buildDeviceListGrid([]);

            var patientListURL = ipAddress + "/appointment/list/?facility-id=" + txtAppPTFacility + "&patient-id=" + selectPatientId + "&to-date=" + edDateTime + "&from-date=" + stDateTime + "&is-active=1";

            getAjaxObject(patientListURL, "GET", onPatientListData, onError);

        }
        else {
            customAlert.error("Error", "Please select date");
        }
    }
    else {
        customAlert.error("Error", "Please select service user");
    }
}

function onClickViewStaffTimesheet() {

    $("#divGrandTotalHours").hide();
    $("#tdGrandTotaScheduledHours").text("");
    $("#tdGrandTotaActualHours").text("");
    $("#divStaffTimeSheetReportViewer").empty();

    var strMessage;
    var isValid = true;
    if ($("#txtStaffUser").val().length === 0 && $("#rbtnOld").is(":checked")) {
        strMessage = "Please select staff.";
        isValid = false;
    }

    if(isValid) {
        angularUIgridWrapper.refreshGrid();
        buildDeviceListGrid([]);

        var txtFDate = $("#txtFDate").data("kendoDatePicker");
        var txtTDate = $("#txtTDate").data("kendoDatePicker");

        var fDate = txtFDate.value();
        var tDate = txtTDate.value();

        var selFDate = new Date(fDate);
        var selTDate = new Date(tDate);

        var stDateTime = getFromDate(selFDate);
        var edDateTime = getToDate(selTDate);


        var facilityId = $("#cmbFacility").val();


        var apiUrl = "";
        if (selProviderId !== null && Number(selProviderId) > 0){
            apiUrl = ipAddress + "/homecare/reports/staff-timesheet/?facility-id=" + facilityId + "&provider-id=" + selProviderId + "&date-of-appointment=:bt:" + stDateTime + "," + edDateTime + "";
        }
        else{
            apiUrl = ipAddress + "/homecare/reports/staff-timesheet/?facility-id=" + facilityId + "&date-of-appointment=:bt:" + stDateTime + "," + edDateTime + "";
        }



        getAjaxObject(apiUrl, "GET", onStaffTimesheet, onError);
    }
    else{
        customAlert.error("info",strMessage);
    }
}

function onStaffTimesheet(dataObj) {
    dtArray = [];
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.staffTimesheets) {
            if ($.isArray(dataObj.response.staffTimesheets)) {
                dtArray = dataObj.response.staffTimesheets;
            } else {
                dtArray.push(dataObj.response.staffTimesheets);
            }
        }
    }
    if (dtArray && dtArray.length > 0) {
        // console.log(dtArray);
        if ($("#rbtnOld").is(":checked")) {
            var staffTimeSheetSummary = [];
            dtArray.sort(function (x, y) {
                var date1 = new Date(x);
                var date2 = new Date(y);

                if (date1 > date2) return 1;
                if (date1 < date2) return -1;
                return 0;
            });

            var totalScheduledHours = 0, totalActualHours = 0;

            for (var counter = 0; counter <= dtArray.length - 1; counter++) {
                if(counter == 0 && dtArray[counter].providerId == 0){
                    dtArray[counter].ProviderName = "Unallocated";
                }
                else{
                    if (counter == 0 && dtArray[counter].firstName !== null && dtArray[counter].firstName !== "" && dtArray[counter].lastName !== null && dtArray[counter].lastName !== "") {
                        dtArray[counter].ProviderName = dtArray[counter].lastName + " " + dtArray[counter].firstName;
                    }
                }


                if (dtArray[counter].weeklyContractHours === null) {
                    dtArray[counter].weeklyContractHours = 0;
                }


                if (dtArray[counter].hourlyRate === null) {
                    dtArray[counter].hourlyRate = 0;
                }

                if (dtArray[counter].dateOfAppointment) {
                    var date = new Date(GetDateTimeEditDay(dtArray[counter].dateOfAppointment));
                    var day = date.getDay();
                    var dow = getWeekDayName(day);

                    dtArray[counter].dateTimeOfAppointment = dow + " " + GetDateTimeEdit(dtArray[counter].dateOfAppointment);
                    var dtEnd = dtArray[counter].dateOfAppointment + (Number(dtArray[counter].duration) * 60000);
                    dtArray[counter].DW = dow;
                    var dateED = new Date(GetDateTimeEditDay(dtEnd));
                    var dayED = dateED.getDay();
                    var dowED = getWeekDayName(dayED);
                    dtArray[counter].dateTimeOfAppointmentED = GetDateTimeEdit(dtEnd);
                    dtArray[counter].dow = dowED;

                }

                if (dtArray[counter].duration) {
                    var totalMinutes = dtArray[counter].duration;

                    var hours = Math.floor(totalMinutes / 60);
                    var minutes = Math.round(totalMinutes % 60);
                    if (hours.toString().length == 1) {
                        hours = "0" + hours;
                    }
                    if (minutes.toString().length == 1) {
                        minutes = "0" + minutes;
                    }


                    dtArray[counter].durationTime = hours + ":" + minutes;
                    totalScheduledHours = totalScheduledHours + dtArray[counter].duration;
                }
                if (dtArray[counter].actualDuration) {
                    var totalMinutes = dtArray[counter].actualDuration;

                    var hours = Math.floor(totalMinutes / 60);
                    var minutes = Math.round(totalMinutes % 60);
                    if (hours.toString().length == 1) {
                        hours = "0" + hours;
                    }
                    if (minutes.toString().length == 1) {
                        minutes = "0" + minutes;
                    }


                    dtArray[counter].actualDurationTime = hours + ":" + minutes;
                    totalActualHours = totalActualHours + dtArray[counter].actualDuration;
                }
                if (counter == 0) {
                    dtArray[counter].Provider = dtArray[counter].ProviderName + '- ' + dtArray[counter].weeklyContractHours;
                }
            }



            var strTotalScheduledHours = "", strTotalActualHours = "";
            if (totalScheduledHours) {
                var hours = Math.floor(totalScheduledHours / 60);
                var minutes = Math.round(totalScheduledHours % 60);
                if (hours.toString().length == 1) {
                    hours = "0" + hours;
                }
                if (minutes.toString().length == 1) {
                    minutes = "0" + minutes;
                }

                strTotalScheduledHours = hours + ":" + minutes;
            }

            if (totalActualHours) {
                var hours = Math.floor(totalActualHours / 60);
                var minutes = Math.round(totalActualHours % 60);
                if (hours.toString().length == 1) {
                    hours = "0" + hours;
                }
                if (minutes.toString().length == 1) {
                    minutes = "0" + minutes;
                }

                strTotalActualHours = hours + ":" + minutes;
            }


            var objTotal = {};
            objTotal.ProviderName = "";
            objTotal.weeklyContractHours = "";
            objTotal.dateTimeOfAppointment = "";
            objTotal.dow = "Total";
            objTotal.durationTime = strTotalScheduledHours;
            objTotal.actualDurationTime = strTotalActualHours;

            dtArray.push(objTotal);
            buildDeviceListGrid(dtArray);
            $("#divOldTimeSheetReportTableBlock").show();
        }
        else{

            var reportViewerJson = [];
            var lookup = {};
            var providerIds = [];
            var grandTotalScheduledHours = 0, grandTotalActualHours = 0, grandContractHrs = 0, grandHourlyRate = 0;

            for (var item, c = 0; item = dtArray[c++];) {
                var providerId = item.providerId;

                if (!(providerId in lookup)) {
                    lookup[providerId] = 1;
                    providerIds.push(providerId);
                }
            }

            // debugger;

            if (providerIds !== null && providerIds.length > 0) {
                for (var i = 0; i < providerIds.length; i++) {
                    var report={};
                    // console.log(providerIds[i]);
                    var staffAppts = $.grep(dtArray, function (e, index) {

                        return Number(e.providerId) === Number(providerIds[i]);
                    });


                    if (staffAppts !== null && staffAppts.length > 0) {

                        if (staffAppts[0].providerId != 0 && staffAppts[0].providerFirstName !== null && staffAppts[0].providerFirstName !== "" && staffAppts[0].providerLastName !== null && staffAppts[0].providerLastName) {
                            report.ProviderName = staffAppts[0].providerLastName+ " " + staffAppts[0].providerFirstName;
                            report.providerId = staffAppts[0].providerId;

                            if (report.ProviderName.toLowerCase() === "u a") {
                                report.ProviderName = "Unallocated";
                            }
                        }
                        else{
                            report.ProviderName = "Unallocated";
                        }

                        if (staffAppts[0].weeklyContractHours == null) {
                            report.weeklyContractHours = 0;
                        }
                        else{
                            report.weeklyContractHours = staffAppts[0].weeklyContractHours;
                        }

                        if (staffAppts[0].hourlyRate == null) {
                            report.hourlyRate = 0;
                        }
                        else{
                            report.hourlyRate = staffAppts[0].hourlyRate;
                        }

                        report.dataSource = [];
                        var duration = 0, actualDuration = 0, weeklyContractHrs = 0, hourlyRate = 0;
                        for (var j = 0; j < staffAppts.length; j++) {
                            var tmpStaffAppt = {};

                            if (staffAppts[j].patientFirstName !== null && staffAppts[j].patientFirstName !== "" && staffAppts[j].patientLastName !== null && staffAppts[j].patientLastName) {
                                tmpStaffAppt.ServiceUser = staffAppts[j].patientFirstName + " " + staffAppts[j].patientLastName;
                            }

                            var dateTimeOfAppointment;
                            if (staffAppts[j].dateOfAppointment) {
                                var date = new Date(GetDateTimeEditDay(staffAppts[j].dateOfAppointment));
                                var day = date.getDay();
                                var dow = getWeekDayName(day);

                                dateTimeOfAppointment = dow + " " + GetDateTimeEdit(staffAppts[j].dateOfAppointment);

                            }
                            tmpStaffAppt.dateTimeOfAppointment = dateTimeOfAppointment;

                            if (staffAppts[j].appointmentReason && staffAppts[j].appointmentReason != null) {
                                tmpStaffAppt.appointmentReason = staffAppts[j].appointmentReason;
                            }
                            else{
                                tmpStaffAppt.appointmentReason = "";
                            }

                            if (staffAppts[j].duration !== null) {
                                duration = duration + staffAppts[j].duration;
                            }

                            if (staffAppts[j].actualDuration !== null) {
                                actualDuration = actualDuration + staffAppts[j].actualDuration;
                            }

                            if (staffAppts[j].duration) {

                                grandTotalScheduledHours = grandTotalScheduledHours + staffAppts[j].duration;

                                var totalScheduledMinutes = staffAppts[j].duration;

                                var hoursScheduled = Math.floor(totalScheduledMinutes / 60);
                                var minutesScheduled = Math.round(totalScheduledMinutes % 60);
                                if (hoursScheduled.toString().length == 1) {
                                    hoursScheduled = "0" + hoursScheduled;
                                }
                                if (minutesScheduled.toString().length == 1) {
                                    minutesScheduled = "0" + minutesScheduled;
                                }


                                tmpStaffAppt.ScheduledHours = hoursScheduled + ":" + minutesScheduled;
                            }
                            else{
                                tmpStaffAppt.ScheduledHours = "00:00";
                            }

                            if (staffAppts[j].actualDuration) {
                                grandTotalActualHours = grandTotalActualHours + staffAppts[j].actualDuration;

                                var totalActualMinutes = staffAppts[j].actualDuration;

                                var actualHours = Math.floor(totalActualMinutes / 60);
                                var actualMinutes = Math.round(totalActualMinutes % 60);
                                if (actualHours.toString().length == 1) {
                                    actualHours = "0" + actualHours;
                                }
                                if (actualMinutes.toString().length == 1) {
                                    actualMinutes = "0" + actualMinutes;
                                }


                                tmpStaffAppt.ActualHours = actualHours + ":" + actualMinutes;
                            }
                            else{
                                tmpStaffAppt.ActualHours = "00:00";
                            }

                            // if (staffAppts[j].weeklyContractHours == null) {
                            //     tmpStaffAppt.weeklyContractHours = 0;
                            // }
                            // else{
                            //     grandTotalActualHours = grandTotalActualHours + staffAppts[j].weeklyContractHours;
                            //     weeklyContractHrs = weeklyContractHrs + staffAppts[j].weeklyContractHours;
                            //     tmpStaffAppt.weeklyContractHours = staffAppts[j].weeklyContractHours;
                            // }
                            //
                            // if (staffAppts[j].hourlyRate == null) {
                            //     tmpStaffAppt.hourlyRate = 0;
                            // }
                            // else{
                            //     grandHourlyRate = grandHourlyRate + staffAppts[j].hourlyRate;
                            //     hourlyRate = hourlyRate + staffAppts[j].hourlyRate;
                            //     tmpStaffAppt.hourlyRate = staffAppts[j].hourlyRate;
                            // }


                            report.dataSource.push(tmpStaffAppt);
                        }

                        if (duration) {
                            var overallTotalScheduledMinutes = duration;

                            var overallHoursScheduled = Math.floor(overallTotalScheduledMinutes / 60);
                            var overallMinutesScheduled = Math.round(overallTotalScheduledMinutes % 60);
                            if (overallHoursScheduled.toString().length == 1) {
                                overallHoursScheduled = "0" + overallHoursScheduled;
                            }
                            if (overallMinutesScheduled.toString().length == 1) {
                                overallMinutesScheduled = "0" + overallMinutesScheduled;
                            }


                            report.totalScheduledHours = overallHoursScheduled + ":" + overallMinutesScheduled;
                        }
                        else{
                            report.totalScheduledHours = "00:00";
                        }

                        if (actualDuration) {
                            var overallTotalActualMinutes = actualDuration;

                            var overallActualHours = Math.floor(overallTotalActualMinutes / 60);
                            var overallActualMinutes = Math.round(overallTotalActualMinutes % 60);
                            if (overallActualHours.toString().length == 1) {
                                overallActualHours = "0" + overallActualHours;
                            }
                            if (overallActualMinutes.toString().length == 1) {
                                overallActualMinutes = "0" + overallActualMinutes;
                            }


                            report.totalActualHours = overallActualHours + ":" + overallActualMinutes;
                        }
                        else{
                            report.totalActualHours = "00:00";
                        }
                        report.weekyContractHours = weeklyContractHrs;
                        // report.hourlyRate = hourlyRate;


                    }
                    reportViewerJson.push(report);
                }
            }

            if (reportViewerJson !== null && reportViewerJson.length > 0) {

                generateReportTables(reportViewerJson);
                var tmpScheduledHours = "", tmpActualHours="";
                if (grandTotalScheduledHours > 0) {

                    var totalScheduledMinutes = grandTotalScheduledHours;

                    var hoursScheduled = Math.floor(totalScheduledMinutes / 60);
                    var minutesScheduled = Math.round(totalScheduledMinutes % 60);
                    if (hoursScheduled.toString().length == 1) {
                        hoursScheduled = "0" + hoursScheduled;
                    }
                    if (minutesScheduled.toString().length == 1) {
                        minutesScheduled = "0" + minutesScheduled;
                    }


                    tmpScheduledHours = hoursScheduled + ":" + minutesScheduled;
                }
                else{
                    tmpScheduledHours = "00:00";
                }



                if (grandTotalActualHours) {

                    var totalActualMinutes = grandTotalActualHours;

                    var actualHours = Math.floor(totalActualMinutes / 60);
                    var actualMinutes = Math.round(totalActualMinutes % 60);
                    if (actualHours.toString().length == 1) {
                        actualHours = "0" + actualHours;
                    }
                    if (actualMinutes.toString().length == 1) {
                        actualMinutes = "0" + actualMinutes;
                    }


                    tmpActualHours = actualHours + ":" + actualMinutes;
                }
                else{
                    tmpActualHours = "00:00";
                }

                $("#divGrandTotalHours").show();
                $("#tdGrandTotaScheduledHours").text(tmpScheduledHours);
                $("#tdGrandTotaActualHours").text(tmpActualHours);
                // $("#tdGrandTotalContractHrs").text(grandContractHrs);
                // $("#tdGrandTotalHourlyRate").text(grandHourlyRate);
            }
        }
    }
    else {
        customAlert.info("Info", "No timesheet details");
    }
}

function generateReportTables(reportViewerJson){
    if(reportViewerJson != null && reportViewerJson.length > 0){
        var insertAfterElement="";
        for(var counter = 0;counter < reportViewerJson.length;counter++){
            var reportDisplayName = reportViewerJson[counter].ProviderName;
            var providerId = "";
            var staffDetails = "";
            if(reportDisplayName.toLowerCase() != "unallocated"){
                providerId = "#"+reportViewerJson[counter].providerId+" - ";
                staffDetails = "Weekly Contract Hrs : "+reportViewerJson[counter].weeklyContractHours+" Hourly Rate : " +reportViewerJson[counter].hourlyRate;
            }
            var reportDisplayNameId = "StaffTimeSheet" + (counter+1);


            var $report = $("<div class=\"col-xs-12 table-block\" id=\"div"+reportDisplayNameId+"\"> <div class=\"col-xs-12 timeSheetReportdivHeading\" style='page-break-before: always !important;'>"+providerId + reportDisplayName+"<span class='headingSpn floatRight'>"+ staffDetails+"</span> </div><div class=\"table-responsive\"> <table class=\"timesheetmembersTable\" id=\"tbl"+reportDisplayNameId+"\"> </table> </div> </div>");

            $("#divStaffTimeSheetReportViewer").append($report);
            bindTimeSheetReport(reportDisplayNameId,reportViewerJson[counter]);
        }
    }
}


function bindTimeSheetReport(reportDisplayNameId, reportData) {
    var dataObj = reportData.dataSource;

    var tableId = "#tbl"+reportDisplayNameId;


    var strHeading = "<thead>";
    var columns = [];
    if (dataObj != null && dataObj.length > 0) {
        var firstObj = dataObj[0];
        var c = 0;
        for (var key in firstObj) {
            if(key.toLowerCase() == "appt date time") {
                strHeading = strHeading + "<tr style=\"page-break-inside: avoid\"><th class=\"borderbuttomthin\" style='width:12%'>" + key + "</th>";
            }else{
                if (c === 0) {
                    strHeading = strHeading + "<th></th>";
                }
                else{
                    if(key.toLowerCase() == "serviceuser"){
                        strHeading = strHeading + "<th class='borderbuttomthin'>Service User</th>";
                    }
                    else if(key.toLowerCase() == "datetimeofappointment"){
                        strHeading = strHeading + "<th class='borderbuttomthin'>Appointment Date Time</th>";
                    }
                    else if(key.toLowerCase() == "appointmentreason"){
                        strHeading = strHeading + "<th class='borderbuttomthin'>Appointment Reason</th>";
                    }
                    else if(key.toLowerCase() == "scheduledhours"){
                        strHeading = strHeading + "<th class='borderbuttomthin'>Scheduled Hrs</th>";
                    }
                    else if(key.toLowerCase() == "actualhours"){
                        strHeading = strHeading + "<th class='borderbuttomthin'>Actual Hrs</th>";
                    }
                    // else if(key.toLowerCase() == "hourlyrate"){
                    //     strHeading = strHeading + "<th class='borderbuttomthin'>Hourly Rate</th>";
                    // }
                    // else if(key.toLowerCase() == "weeklycontracthours"){
                    //     strHeading = strHeading + "<th class='borderbuttomthin'>Weekly Contract Hrs</th>";
                    // }
                }

            }
            columns.push(key);
            c = c + 1;
        }
    }
    strHeading = strHeading+"</thead>";

    $(tableId).append(strHeading);


    var strTableBody = "<tbody>";

    for(var counter = 0;counter<dataObj.length;counter++){
        var strTableRow = "<tr style=\"page-break-inside: avoid\">";

        for(var counter2= 0;counter2<columns.length;counter2++){
                if (counter2 > 0 && counter === dataObj.length-1) {
                    strTableRow = strTableRow + "<td class=\"borderbuttomthick\">"+ dataObj[counter][columns[counter2]]+"</td>";
                }
                else{
                    strTableRow = strTableRow + "<td>"+ dataObj[counter][columns[counter2]]+"</td>";
                }


        }
        strTableRow = strTableRow+"</tr>";

        strTableBody = strTableBody +strTableRow;
    }

    var totalRow = '<tr class="timesheetmembersTable-total" style="page-break-inside: avoid"><td></td><td align="left">Total Scheduled &amp; Actual Hours</td><td></td><td class=\"showgrandtotal\">'+reportData.totalScheduledHours+'</td><td class=\"showgrandtotal\">'+reportData.totalActualHours+'</td></tr>';
    strTableBody = strTableBody+totalRow;
    strTableBody = strTableBody+"</tbody>";
    $(tableId).append(strTableBody);


}

function onPatientListData(dataObj) {
    dtArray = [];
    buildDeviceListGrid([]);
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.appointment) {
            if ($.isArray(dataObj.response.appointment)) {
                dtArray = dataObj.response.appointment;
            } else {
                dtArray.push(dataObj.response.appointment);
            }
        }
    }
    dtArray.sort(function (a, b) {
        var nameA = a.dateOfAppointment; // ignore upper and lowercase
        var nameB = b.dateOfAppointment; // ignore upper and lowercase
        if (nameA < nameB) {
            return -1;
        }
        if (nameA > nameB) {
            return 1;
        }
        // names must be equal
        return 0;
    });

    if (dtArray.length > 0) {
        for (var i = 0; i < dtArray.length; i++) {
            // $("#btnCopy").css("display", "");
            dtArray[i].idk = dtArray[i].id;
            dtArray[i].name = dtArray[i].composition.patient.lastName + " " + dtArray[i].composition.patient.firstName + " " + dtArray[i].composition.patient.middleName;


            var date = new Date(GetDateTimeEditDay(dtArray[i].dateOfAppointment));
            var day = date.getDay();
            var dow = getWeekDayName(day);

            var dtEnd = dtArray[i].dateOfAppointment + (Number(dtArray[i].duration) * 60000);

            dtArray[i].dateTimeOfAppointment = dow + " " + GetDateTimeEdit(dtArray[i].dateOfAppointment);
            dtArray[i].DW = dow;

            var dateED = new Date(GetDateTimeEditDay(dtEnd));
            var dayED = dateED.getDay();
            var dowED = getWeekDayName(dayED);

            dtArray[i].dateTimeOfAppointmentED = dowED + " " + GetDateTimeEdit(dtEnd);


            var dt = new Date(dtArray[i].dateOfAppointment);
            var strDT = "";
            if (dt) {
                strDT = kendo.toString(dt, "MM/dd/yyyy h:mm tt");
            }

            dtArray[i].dateTimeOfAppointment1 = strDT;


            dtArray[i].appointmentReasonDesc = dtArray[i].composition.appointmentReason.desc;
            dtArray[i].appointmentTypeDesc = dtArray[i].composition.appointmentType.desc;
            if (dtArray[i].providerId != 0) {
                dtArray[i].staffName = dtArray[i].composition.provider.lastName + " " + dtArray[i].composition.provider.firstName + " " + (dtArray[i].composition.provider.middleName != null ? dtArray[i].composition.provider.middleName : " ");
            } else {
                dtArray[i].staffName = "";
            }
        }


        buildDeviceListGrid(dtArray);
    }
    else {
        // $("#btnCopy").css("display", "none");
        customAlert.info("Info", "No appointments");
    }
}

function onClickCopy() {
    var selGridData = angularUIgridWrapper.getAllRows();
    var selList = [];
    for (var i = 0; i < selGridData.length; i++) {

        var dataRow = selGridData[i].entity;
        dataRow.appointmentType = "Con";
        dataRow.appointmentTypeDesc = "Confirmed";
        dataRow.composition.appointmentType.desc = "Confirmed";
        dataRow.composition.appointmentType.id = 7;
        dataRow.composition.appointmentType.value = "Con";
        selList.push(dataRow);
    }

    // if(selList.length>0){
    sessionStorage.setItem("appselList", JSON.stringify(selList));
    parentRef.appselList = selList;
    var popW = "60%";
    var popH = "80%";
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Copy To Appointments";
    parentRef.selValue = allDatesInWeek;
    devModelWindowWrapper.openPageWindow("../../html/masters/createCopyAppointment.html", profileLbl, popW, popH, true, copyAppointment);
    parent.setPopupWIndowHeaderColor("0bc56f", "FFF");
    // }
}
function copyAppointment(dataObj) {
    parentRef.selValue = null;
    parentRef.appselList = null;
    onClickSubmit();
}

function onCloseEditAppointment(dataObj) {
    parentRef.selectedItems = null;
    buildDeviceListGrid([]);
    onClickViewStaffTimesheet();

}


function getWeekDayName(wk) {
    var wn = "";
    if (wk == 1) {
        return "Mon";
    } else if (wk == 2) {
        return "Tue";
    } else if (wk == 3) {
        return "Wed";
    } else if (wk == 4) {
        return "Thu";
    } else if (wk == 5) {
        return "Fri";
    } else if (wk == 6) {
        return "Sat";
    } else if (wk == 0) {
        return "Sun";
    }
    return wn;
}

function getFromDate(fromDate) {


    var day = fromDate.getDate();
    var month = fromDate.getMonth();
    month = month + 1;
    var year = fromDate.getFullYear();

    var strDate = month + "/" + day + "/" + year;
    strDate = strDate + " 00:00:00";

    var date = new Date(strDate);
    var strDateTime = date.getTime();

    return strDateTime;
}

function getToDate(toDate) {


    var day = toDate.getDate();
    var month = toDate.getMonth();
    month = month + 1;
    var year = toDate.getFullYear();

    var strDate = month + "/" + day + "/" + year;
    strDate = strDate + " 23:59:59";

    var date = new Date(strDate);
    var strDateTime = date.getTime();

    return strDateTime;
}

var sortByDateAsc = function (x, y) {
    // This is a comparison function that will result in dates being sorted in
    // ASCENDING order. As you can see, JavaScript's native comparison operators
    // can be used to compare dates. This was news to me.

    var date1 = new Date(x.dateOfAppointment);
    var date2 = new Date(y.dateOfAppointment);

    if (date1 > date2) return 1;
    if (date1 < date2) return -1;
    return 0;
};
var angularPTUIgridWrapper = null;
var dataArray = [];
var appReport = angular.module('appReport', []);
var selPatientId;
var selProviderId;
var appointmentIds = [];

appReport.controller('reportController', function($scope) {
    $scope.getData = function () {
        onClickPtViews();

    }
    $scope.printTable = function () {

        var divToPrint = document.getElementById("carertable");
        var htmlToPrint = '' +
            '<style type="text/css">' +
            'table th, table td, table {' +
            'border:solid 1px #f3f6f9' +
            ';' +
            'padding;0.5em;' +
            'border-collapse:collapse;'+
            '}' +
            '</style>';
        htmlToPrint += divToPrint.outerHTML;

        // var newWin = window.open("");
        // newWin.document.write(htmlToPrint);
        // newWin.document.close();
        // newWin.focus();
        // newWin.print();
        // newWin.close();

        // var newWin  = window.open("");
        // newWin.document.body.innerHTML = htmlToPrint;
        // newWin.focus();
        // newWin.print();
        // newWin.close();

        window.frames["print_frame"].document.body.innerHTML = htmlToPrint;
        window.frames["print_frame"].window.focus();
        window.frames["print_frame"].window.print();

    }
});


$(document).ready(function(){
    $("#divMain",parent.document).css("display","none");
    $("#divPatInfo",parent.document).css("display","");
    // $("#lblTitleName",parent.document).html("Carer In Out Report");
    var cntry = sessionStorage.countryName;
    if((cntry.indexOf("India")>=0) || (cntry.indexOf("United Kingdom")>=0)){
        $("#lblFacility").text("Location");
    }else if(cntry.indexOf("United State")>=0) {
        $("#lblFacility").text("Facility");
    }
});

$(window).load(function(){
    parentRef = parent.frames['iframe'].window;
    onMessagesLoaded();
});
function adjustHeight(){
    var defHeight = 160;
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 100;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    try{angularPTUIgridWrapper.adjustGridHeight(cmpHeight);}catch(e){};

}
function onMessagesLoaded() {
    buttonEvents();
    init();
}
var cntry = "";
var dtFMT = "dd/MM/yyyy";
var stDate = "";
var endDate = "";

function init(){
    //patientId = parentRef.patientId;
    //$("#dtFromDate").kendoDatePicker({value:new Date()});
    //$("#dtToDate").kendoDatePicker({value:new Date()});

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();


    cntry = sessionStorage.countryName;
    //getFacilityList();
    if(cntry.indexOf("India")>=0){
        dtFMT = INDIA_DATE_FMT;
    }else  if(cntry.indexOf("United Kingdom")>=0){
        dtFMT = ENG_DATE_FMT;
    }else  if(cntry.indexOf("United State")>=0){
        dtFMT = US_DATE_FMT;
    }
    $("#txtStartDate").kendoDatePicker({format:dtFMT});
    $("#txtEndDate").kendoDatePicker({format:dtFMT});

    startDT = $("#txtStartDate").kendoDatePicker({
        change: startChange,format:dtFMT,value:new Date()
    }).data("kendoDatePicker");

    endDT= $("#txtEndDate").kendoDatePicker({
        change: endChange,format:dtFMT,value:new Date()
    }).data("kendoDatePicker");

    endDT.min(new Date());

    // $("#txtStartDate").kendoDatePicker({value:new Date()});
    // $("#txtEndDate").kendoDatePicker({value:new Date()});
    // var dataOptionsPT = {
    //         pagination: false,
    //         paginationPageSize: 500,
    // 		changeCallBack: onPTChange
    //     }
    // angularPTUIgridWrapper = new AngularUIGridWrapper("careerGrid1", dataOptionsPT);
    //   angularPTUIgridWrapper.init();
    //  buildPatientRosterList([]);
    adjustHeight();
    //onClickPtViews();
}
//
// function getReportDataList(dataObj){
//     dataArray = [];
//     var tempdataArray = [];
//     if (dataObj && dataObj.response && dataObj.response.status) {
//         if (dataObj.response.status.code == "1") {
//             if (dataObj.response.appointment) {
//                 if ($.isArray(dataObj.response.appointment)) {
//                     dataArray = dataObj.response.appointment;
//                 } else {
//                     dataArray.push(dataObj.response.appointment);
//                 }
//             }
//         }
//     }
//
//
//     var sortOrderValue = $("#cmbSortOrder").val();
//     if(sortOrderValue == "1") {
//         dataArray.sort(function (a, b) {
//             var p1 = a.dateOfAppointment;
//             var p2 = b.dateOfAppointment;
//
//             if (p1 < p2) return -1;
//             if (p1 > p2) return 1;
//             return 0;
//         });
//     }
//     else if(sortOrderValue == "3") {
//         dataArray.sort(function (a, b) {
//             var p1 = a.dateOfAppointment;
//             var p2 = b.dateOfAppointment;
//             var o1, o2;
//             if (a.composition && a.composition.provider) {
//                 o1 = a.composition.provider.lastName.toLowerCase() + " " + a.composition.provider.firstName.toLowerCase();
//                 o2 = b.composition.provider.lastName.toLowerCase() + " " + b.composition.provider.firstName.toLowerCase();
//             }
//             if (p1 < p2) return -1;
//             if (p1 > p2) return 1;
//
//
//             if (o1 < o2) return -1;
//             if (o1 > o2) return 1;
//
//
//
//             // return 0;
//         });
//     }else if(sortOrderValue == "2") {
//         var sortByDateAsc = function (a, b) {
//             var p1 = a.dateOfAppointment;
//             var p2 = b.dateOfAppointment;
//             var o1, o2;
//             if (a.composition && a.composition.patient) {
//                 o1 = a.composition.patient.lastName.toLowerCase() + " " + a.composition.patient.firstName.toLowerCase();
//                 o2 = b.composition.patient.lastName.toLowerCase() + " " + b.composition.patient.firstName.toLowerCase();
//                 // o2 = b.composition.patient.lastName.toLowerCase();
//             }
//             // if (p1 > p2) return -1;
//             // if (p1 < p2) return 1;
//             if (p1 < p2) return 1;
//             if (p1 > p2) return -1;
//
//
//
//             if (o1 > o2) return -1;
//             if (o1 < o2) return 1;
//
//
//
//
//             // if (p1 === p2) {
//             //     if (a.composition && a.composition.patient) {
//             //         o1 = a.composition.patient.lastName.toLowerCase() + " " + a.composition.patient.firstName.toLowerCase();
//             //         o2 = b.composition.patient.lastName.toLowerCase() + " " + b.composition.patient.firstName.toLowerCase();
//             //         // o2 = b.composition.patient.lastName.toLowerCase();
//             //     }
//             //
//             //     //do the same as your already doing but using different data, from above
//             //     return (o1 < o2) ? -1 : (o1 > o2) ? 1 : 0;
//             // }
//             // else {
//             //     return (p1 < p2) ? -1 : (p1 > p2) ? 1 : 0;
//             // }
//
//
//             // if ((o1 < o2)&&(p1 < p2)) return -1;
//             // if ((o1 > o2)&&(p1 > p2) ) return 1;
//
//             // // if (p1 < p2) return -1;
//             // if (p1 > p2) return 1;
//
//
//             // return 0;
//         };
//
//         dataArray.sort(sortByDateAsc);
//         console.log(dataArray);
//
//         // const sortCols  = (a, b, attrs) => Object.keys(attrs)
//         //     .reduce((diff, k) =>  diff == 0 ? attrs[k](a[k], b[k]) : diff, 0);
//         //
//         // dataArray.sort(function(a, b) => sortCols(a, b, {
//         //     0: (a, b) => a.dateOfAppointment - b.dateOfAppointment,
//         //     1: (a, b) => a.composition.patient.lastName.toLowerCase() - b.composition.patient.lastName.toLowerCase()
//         // }))
//
//         // const criteria = ['dateOfAppointment', 'composition.patient.lastName']
//         // const data = [
//         //     { name: 'Mike', speciality: 'JS', age: 22 },
//         //     { name: 'Tom', speciality: 'Java', age: 30 },
//         //     { name: 'Mike', speciality: 'PHP', age: 40 },
//         //     { name: 'Abby', speciality: 'Design', age: 20 },
//         // ]
//
//         // dataArray = multisort(dataArray, criteria)
//
//         // dataArray.sort(function (a, b) {
//         //     var contentA =parseInt( $(a).attr('data-row'));
//         //     var contentB =parseInt( $(b).attr('data-row'));
//         //
//         //
//         //     //check if the rows are equal
//         //     if (contentA === contentB) {
//         //         var colA = parseInt( $(a).attr('data-col'));
//         //         var colB = parseInt( $(b).attr('data-col'));
//         //
//         //         //do the same as your already doing but using different data, from above
//         //         return (colA < colB) ? -1 : (colA > colB) ? 1 : 0;
//         //     }
//         //     else {
//         //         return (contentA < contentB) ? -1 : (contentA > contentB) ? 1 : 0;
//         //     }
//         // });
//     }
//
//     if (dataArray.length > 0) {
//         for (var d = 0; d < dataArray.length; d++) {
//             if (dataArray[d].dateOfAppointment) {
//                 dataArray[d].idk =dataArray[d].id;
//                 var date = new Date(GetDateTimeEditDay(dataArray[d].dateOfAppointment));
//                 var day = date.getDay();
//                 var dow = getWeekDayName(day);
//                 var dtEnd= dataArray[d].dateOfAppointment + (Number(dataArray[d].duration) * 60000);
//                 dataArray[d].dateTimeOfAppointment = dow + " " + GetDateTimeEdit(dataArray[d].dateOfAppointment);dataArray[d].DW = dow;
//                 var dateED = new Date(GetDateTimeEditDay(dtEnd));
//                 var dayED = dateED.getDay();
//                 var dowED = getWeekDayName(dayED);
//                 dataArray[d].dateTimeOfAppointmentED = dowED + " " + GetDateTimeEdit(dtEnd);
//
//                 dataArray[d].appDate = kendo.toString(new Date(dataArray[d].dateOfAppointment), "dd-MM-yyyy");
//                 dataArray[d].appTime = kendo.toString(new Date(dataArray[d].dateOfAppointment), "hh:mm tt");
//             }
//
//             if (dataArray[d].composition && dataArray[d].composition.provider) {
//                 dataArray[d].carer = dataArray[d].composition.provider.lastName + " " + dataArray[d].composition.provider.firstName;
//             }
//             if (dataArray[d].composition && dataArray[d].composition.patient) {
//                 dataArray[d].serviceUser = dataArray[d].composition.patient.lastName + " " + dataArray[d].composition.patient.firstName;
//             }
//             if (dataArray[d].composition && dataArray[d].composition.appointmentType) {
//                 dataArray[d].status = dataArray[d].composition.appointmentType.desc;
//             }
//             if (dataArray[d].composition && dataArray[d].composition.appointmentReason) {
//                 dataArray[d].reason = dataArray[d].composition.appointmentReason.desc;
//             }
//
//             if (dataArray[d].duration) {
//                 var totalMinutes = dataArray[d].duration;
//
//                 var hours = Math.floor(totalMinutes / 60);
//                 var minutes = Math.round(totalMinutes % 60);
//                 if (hours.toString().length == 1) {
//                     hours = "0"+hours;
//                 }
//                 if (minutes.toString().length == 1) {
//                     minutes = "0" + minutes;
//                 }
//
//
//                 dataArray[d].duration = hours + ":" + minutes;
//             }
//
//
//
//             tempdataArray.push(dataArray[d]);
//         }
//         var elem = angular.element(document.querySelector('[ng-app]'));
//         var injector = elem.injector();
//         var $rootScope = injector.get('$rootScope');
//         $rootScope.$apply(function(){
//             $rootScope.apidata = tempdataArray;
//         });
//         // buildPatientRosterList(tempdataArray);
//     }
//     else{
//         customAlert.error("info","No appointments");
//     }
//
//
// }

function getReportDataList(dataObj){
    dataArray = [];
    var tempdataArray = [];
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            if (dataObj.response.appointment) {
                if ($.isArray(dataObj.response.appointment)) {
                    dataArray = dataObj.response.appointment;
                } else {
                    dataArray.push(dataObj.response.appointment);
                }
            }
        }
    }

    console.log(dataArray);

    if (dataArray.length > 0) {
        for (var d = 0; d < dataArray.length; d++) {

            if (dataArray[d].syncTime) {

                if (dataArray[d].dateOfAppointment) {
                    dataArray[d].idk = dataArray[d].id;
                    var date = new Date(GetDateTimeEditDay(dataArray[d].dateOfAppointment));
                    var day = date.getDay();
                    var dow = getWeekDayName(day);
                    var dtEnd = dataArray[d].dateOfAppointment + (Number(dataArray[d].duration) * 60000);
                    dataArray[d].dateTimeOfAppointment = dow + " " + GetDateTimeEdit(dataArray[d].dateOfAppointment); dataArray[d].DW = dow;
                    var dateED = new Date(GetDateTimeEditDay(dtEnd));
                    var dayED = dateED.getDay();
                    var dowED = getWeekDayName(dayED);
                    dataArray[d].dateTimeOfAppointmentED = dowED + " " + GetDateTimeEdit(dtEnd);

                    dataArray[d].appDate = kendo.toString(new Date(dataArray[d].dateOfAppointment), "dd-MM-yyyy");
                    dataArray[d].appTime = kendo.toString(new Date(dataArray[d].dateOfAppointment), "hh:mm tt");

                    var apptDateTime = new Date(dataArray[d].dateOfAppointment);

                    var dateOfMonth = apptDateTime.getDate();
                    var apptmonth = apptDateTime.getMonth();
                    var apptyear = apptDateTime.getFullYear();
                    var appthours = apptDateTime.getHours();
                    var apptminutes = apptDateTime.getMinutes();
                    dataArray[d].appointmentDate = new Date(apptyear, apptmonth, dateOfMonth, appthours, apptminutes, 0, 0);

                }

                if (dataArray[d].composition && dataArray[d].composition.provider) {
                    dataArray[d].carer = dataArray[d].composition.provider.lastName + " " + dataArray[d].composition.provider.firstName;
                }
                if (dataArray[d].composition && dataArray[d].composition.patient) {
                    dataArray[d].serviceUser = dataArray[d].composition.patient.lastName + " " + dataArray[d].composition.patient.firstName;
                }
                if (dataArray[d].composition && dataArray[d].composition.appointmentType) {
                    dataArray[d].status = dataArray[d].composition.appointmentType.desc;
                }
                if (dataArray[d].composition && dataArray[d].composition.appointmentReason) {
                    dataArray[d].reason = dataArray[d].composition.appointmentReason.desc;
                }

                if (dataArray[d].duration) {
                    var totalMinutes = dataArray[d].duration;

                    var hours = Math.floor(totalMinutes / 60);
                    var minutes = Math.round(totalMinutes % 60);
                    if (hours.toString().length == 1) {
                        hours = "0" + hours;
                    }
                    if (minutes.toString().length == 1) {
                        minutes = "0" + minutes;
                    }


                    dataArray[d].duration = hours + ":" + minutes;
                }



                tempdataArray.push(dataArray[d]);

            }
        }

        if (tempdataArray != null && tempdataArray.length > 0) {
            var sortOrderValue = $("#cmbSortOrder option:selected").val();
            if (sortOrderValue == "1") {
                tempdataArray.sort(function (a, b) {
                    var p1 = a.dateOfAppointment;
                    var p2 = b.dateOfAppointment;

                    if (p1 < p2) return -1;
                    if (p1 > p2) return 1;
                    return 0;
                });
            }
            else if (sortOrderValue == "2") {
                tempdataArray.sort(sortByAppointmentDateAndServiceUser);
            }
            else if (sortOrderValue == "3") {
                tempdataArray.sort(sortByAppointmentDateAndStaffName);
            }
        }


        var elem = angular.element(document.querySelector('[ng-app]'));
        var injector = elem.injector();
        var $rootScope = injector.get('$rootScope');
        $rootScope.$apply(function(){
            $rootScope.apidata = tempdataArray;
        });
        // buildPatientRosterList(tempdataArray);
    }
    else{
        customAlert.error("info","No appointments");
    }


}

function buildAppointmentList(dataObj) {

    $("#divVisitSummaryReport").empty();

    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1" && dataObj.response.appointment){
        if($.isArray(dataObj.response.appointment)){
            dataArray = dataObj.response.appointment;
        }else{
            dataArray.push(dataObj.response.appointment);
        }
    }

    var dtArray = [];

    if(dataArray.length > 0){
        for(var c = 0 ; c < dataArray.length ; c++){
           if(dataArray[c].syncTime){
                dtArray.push(dataArray[c]);
            }
        }
    }


    if(dtArray.length > 0) {
        $("#divVisitSummaryReport").append('<table class="appointmentActivityReport"><tbody><tr><th style="text-align:center">Appointment Task Report</th></tr></tbody></table>');
        appointmentIds = [];
        for (var i = 0; i < dtArray.length; i++) {
            appointmentIds.push(dtArray[i].id);
            var strHTML = '<table class="appointmentActivityReport" id="tblPatient'+dtArray[i].id+'"> <tbody> <tr>';
            var patientObj = dtArray[i].composition.patient ? dtArray[i].composition.patient : '';
            if (patientObj != "") {
                var handOverNotes = dtArray[i].handoverNotes;
                var notes = dtArray[i].notes;
                if (handOverNotes == null) {
                    handOverNotes = "";
                }
                if (notes == null || notes == "null") {
                    notes = "";
                }
            }
            if (patientObj.dateOfBirth) {
                strHTML = strHTML + '<td width="33%" name="patient-appointment-name" style="text-align: left;font-size: 12px;padding: 0px 5px !important;"><b style="color: #0783b5;">Service User</b> </br>' + patientObj.firstName + ' ' + patientObj.middleName + ' ' + patientObj.lastName + '<br/>' + onGetDateTimeFormat(patientObj.dateOfBirth) + ' (' + calculateAge(new Date(patientObj.dateOfBirth)) + 'Y), ' + patientObj.gender + ')</td>';
            }
            else{
                strHTML = strHTML + '<td width="33%" name="patient-appointment-name" style="text-align: left;font-size: 12px;padding: 0px 5px !important;"><b style="color: #0783b5;">Service User</b> </br>' + patientObj.firstName + ' ' + patientObj.middleName + ' ' + patientObj.lastName + '<br/>' + patientObj.gender + '</td>';
            }

            strHTML = strHTML + '<td  width="34%" name="patient-appointment-caregiver" style="text-align: left;font-size: 12px;padding: 0px 5px !important;"><b style="color: #0783b5;">Staff</b> </br>' + dtArray[i].composition.provider.firstName + ' ' + dtArray[i].composition.provider.lastName + '</td>';
            strHTML = strHTML + '<td width="33%"> <table> <tr>';
            if (dtArray[i].dateOfAppointment) {
                var date = new Date(GetDateTimeEditDay(dtArray[i].dateOfAppointment));
                var day = date.getDay();
                var dow = getWeekDayName(day);
                var dateTimeOfAppointment = dow + " " + GetDateTimeEdit(dtArray[i].dateOfAppointment);
                strHTML = strHTML + '<td colspan="2" name="patient-appointment-date" style="text-align: left;font-size: 12px;border: 0px"><b style="color: #0783b5;">Appointment Date and Time</b> </br>' + dateTimeOfAppointment + '  (' + dtArray[i].duration + ' min)</td>';
            }
            strHTML = strHTML + '</tr><tr>';
            if (dtArray[i].inTime && dtArray[i].outTime) {
                strHTML = strHTML + '<td name="patient-appointment-intime" style="text-align: left;font-size: 12px;border: 0px"><b style="color: #0783b5;">In Time</b> </br>' + new Date(dtArray[i].inTime).toLocaleTimeString() + '</td> <td name="patient-appointment-outtime" style="text-align: left;font-size: 12px;border: 0px"><b style="color: #0783b5;">Out Time</b> </br>' + new Date(dtArray[i].outTime).toLocaleTimeString() + '</td></tr>';
            }
            strHTML = strHTML + '</tr> </table> </td> </tr> </tbody> </table>';


            strHTML = strHTML + '<table class="appointmentActivityReport" id="tblTasksPerformed'+dtArray[i].id+'"><tbody><tr><th>Tasks Performed</th></tr></tbody></table>';
            strHTML = strHTML + '<table class="appointmentActivityReport" id="tblActivitiesPerformed'+dtArray[i].id+'"><thead><tr><th>Image</th><th>Task Name</th><th>Outcome/Remarks</th><th>Time</th></tr></thead><tbody></tbody></table>';
            strHTML = strHTML + '<table class="appointmentActivityReport" id="tblNotes'+dtArray[i].id+'"><tbody><tr><th>Notes</th></tr></tbody></table>';
            strHTML = strHTML + '<table class="appointmentActivityReport" id="tblAppointmentNotes'+dtArray[i].id+'"><tbody><tr><td name="patient-appointment-notes" style="text-align: left !important;"/>'+notes+'</tr></tbody></table>';
            strHTML = strHTML + '<table class="appointmentActivityReport" id="tblHandOverNotes'+dtArray[i].id+'"><tbody><tr><th>HandOverNotes</th></tr></tbody></table>';
            strHTML = strHTML + '<table class="appointmentActivityReport" id="tblAppointmentHandOverNotes'+dtArray[i].id+'"><tbody><tr><td name="patient-appointment-handovernoes" style="text-align: left !important;"/>'+handOverNotes+'</tr></tbody></table>';

            $("#divVisitSummaryReport").append(strHTML);

        }
        getAjaxObjectAsync(ipAddress + "/patient/appointment/activity/list/?is-active=1&is-deleted=0&appointment-id=" + appointmentIds.join(','),"GET",getAppointmentActivities,onError);
    }
    $('.tableWrapper-appointment').removeClass('hideTable');
};

function getAppointmentActivities(dataObj){

    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1" && dataObj.response.patientAppointmentActivities){
        if($.isArray(dataObj.response.patientAppointmentActivities)){
            dataArray = dataObj.response.patientAppointmentActivities;
        } else{
            dataArray.push(dataObj.response.patientAppointmentActivities);
        }
    }

    if(dataArray !== null && dataArray.length > 0){
        if(appointmentIds !== null && appointmentIds.length > 0){
            for(var j = 0 ; j < appointmentIds.length ; j ++){
                var tmpApptActivities = dataArray.filter(function(e){
                    return parseInt(appointmentIds[j]) === e.appointmentId;
                });

                if(tmpApptActivities !== null && tmpApptActivities.length){
                    buildpatientAppointmentActivities(tmpApptActivities);
                }
            }
        }

        getAjaxObjectAsync(ipAddress + "/patient/appointment/medication-activity/list/?is-active=1&is-deleted=0&appointment-id=" + appointmentIds.join(','),"GET",getAppointmentMedicationActivity,onError);
    }

}

function getAppointmentMedicationActivity(dataObj){
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1" && dataObj.response.patientAppointmentActivities){
        if($.isArray(dataObj.response.patientAppointmentMedicationActivity)){
            dataArray = dataObj.response.patientAppointmentMedicationActivity;
        } else{
            dataArray.push(dataObj.response.patientAppointmentMedicationActivity);
        }
    }

    if(dataArray !== null && dataArray.length > 0){
        if(appointmentIds !== null && appointmentIds.length > 0){
            for(var j = 0 ; j < appointmentIds.length ; j ++){
                var tmpApptActivities = dataArray.filter(function(e){
                    return parseInt(appointmentIds[j]) === e.appointmentId;
                });

                if(tmpApptActivities !== null && tmpApptActivities.length){
                    patientAppointmentMedicationActivity(tmpApptActivities);
                }
            }
        }


    }
}

function calculateAge(birthday) {
    var ageDifMs = Date.now() - birthday.getTime();
    var ageDate = new Date(ageDifMs);
    return Math.abs(ageDate.getUTCFullYear() - 1970);
}

function buildpatientAppointmentActivities(dataArray) {

    if(dataArray.length > 0) {
        var Id = dataArray[0].appointmentId;
        IsVists = 0;
        var activityTypeArr = [];
        for (var i = 0; i < dataArray.length; i++) {
            activityTypeArr.push(dataArray[i].activityType);
        }
        var activityTypeArrFinal = activityTypeArr.reduce(function(p,c,i,activityTypeArr){
          if (p.indexOf(c) == -1) p.push(c);
          else p.push('')
          return p;
        }, [])
        for (var i = 0; i < dataArray.length; i++) {
            var notes = dataArray[i].notes;
            if(notes == "1" || notes == null || notes == "null"){
                notes = "";
            }
            var dot;
            if(dataArray[i].review == null || dataArray[i].review == 0) {
                dot = "";
            }
            else if(dataArray[i].review == 1) {
                dot = "greenDot";
            }
            else if(dataArray[i].review == 2){
                dot = "orangeDot";
            }
            else if(dataArray[i].review == 3){
                dot = "redDot";
            }
             var thumbnailURL;
             var thumbimg='';
             var fileURL;
            var fileimg = '';
            if(dataArray[i].activityThumbnailPresent==1){
                thumbnailURL = ipAddress+"/homecare/download/activities/thumbnail/?access_token="+sessionStorage.access_token+"&id="+dataArray[i].activityId+"&tenant="+sessionStorage.tenant;
                thumbimg ='<img  src="'+thumbnailURL +'" id="imgLogo1" style="padding:5px;padding-left:30px;width:80px;"></img>';
            }
            if(dataArray[i].filePresent==1){
                fileURL = ipAddress+"/homecare/download/patient-appointment-activities/?access_token="+sessionStorage.access_token+"&id="+dataArray[i].id+"&tenant="+sessionStorage.tenant;
                fileimg ='<img  src="'+fileURL +'" id="imgLogo1" style="padding:5px;padding-left:30px;width:100px;"></img>';
            }

            var remarks = "";
            if(dataArray[i].remarks != undefined && dataArray[i].remarks != null){
                remarks = dataArray[i].remarks;
            }
            var dateFD, dayFD, dowFD, dayWeekFD, dateED, dayED, dowED, dayWeekED, dateTime;
            if(dataArray[i].actionedDate != undefined) {
                dateFD = new Date(GetDateTimeEditDay(dataArray[i].actionedDate));
                dayFD = dateFD.getDay();
                dowFD = getWeekDayName(dayFD);

                dayWeekFD = " <br>"+dowFD + " " + GetDateTimeEdit(dataArray[i].actionedDate);
                dateTime = kendo.toString(dateFD,"hh:mm tt");
            }
            else{
                dayWeekFD = "";
                dateTime = "";
            }

            if(dataArray[i].actionedModifiedDate != undefined){
                 dateED = new Date(GetDateTimeEditDay(dataArray[i].actionedModifiedDate));
                 dayED = dateED.getDay();
                 dowED = getWeekDayName(dayED);

                 dayWeekED = " /<br>"+dowED + " " + GetDateTimeEdit(dataArray[i].actionedModifiedDate);
                  dateTime = kendo.toString(dateED,"hh:mm tt");
            }
            else{
                dayWeekED = "";
            }
            if(dataArray[i].required == null || dataArray[i].required == 0){
                if(activityTypeArrFinal[i] != "") {
                    $('#tblActivitiesPerformed'+Id+' tbody').append('<tr><td colspan="3" id="patient-appointment-activityName" name="patient-appointment-activityName">' + activityTypeArrFinal[i] + '<span style="float: left" class=' + dot + '></span></td></tr><tr><td width="10%" name="patient-appointment-image">'+thumbimg+'</td><td width="15%" name="patient-appointment-activity">' + dataArray[i].activity + '<span class="mandatory"></span><span style="float: left" class=' + dot + '></td><td width="45%" name="patient-appointment-activityNotes">' + fileimg + notes + '<br>' + remarks + '</td><td width="25%" style="text-align: left" name="patient-appointment-activityDate">' + dateTime+ '</td></tr>');
                }
                else{
                    $('#tblActivitiesPerformed'+Id+' tbody').append('<tr><td width="10%" name="patient-appointment-image">'+thumbimg+'</td><td width="15%" name="patient-appointment-activity">' + dataArray[i].activity + '<span style="float: left" class=' + dot + '></span></td><td width="60%" name="patient-appointment-activityNotes">' + fileimg +notes + '<br>' + remarks + '</td><td style="text-align: left"width="10%"  name="patient-appointment-activityDate">' +dateTime+ '</td></tr>');
                }
            }
            else{
                if(activityTypeArrFinal[i] != "") {
                    $('#tblActivitiesPerformed'+Id+' tbody').append('<tr><td colspan="3" id="patient-appointment-activityName" name="patient-appointment-activityName">' + activityTypeArrFinal[i] + '<span class="mandatory"></span> <span style="float: left" class=' + dot + '></span></td></tr><tr><td width="0%" name="patient-appointment-image">'+thumbimg+'</td><td width="15%" name="patient-appointment-activity">' + dataArray[i].activity + '<span class="mandatory"></span><span style="float: left" class=' + dot + '></span></td><td width="45%" name="patient-appointment-activityNotes">' +fileimg + notes + '<br>' +remarks + '</td><td width="25%" style="text-align: left" name="patient-appointment-activityDate">' + dateTime+ '</td></tr>');
                }
                else {
                    $('#tblActivitiesPerformed'+Id+' tbody').append('<tr><td width="10%" name="patient-appointment-image">'+thumbimg+'</td><td width="15%" name="patient-appointment-activity">' + dataArray[i].activity + '<span class="mandatory"></span> <span style="float: left" class=' + dot + '></span></td><td width="50%" name="patient-appointment-activityNotes">' + fileimg +notes + '<br>' + remarks + '</td><td width="10%" style="text-align: left" name="patient-appointment-activityDate">' + dateTime+ '</td></tr>');
                }
            }

        }
    }
    else{
        customAlert.info("Info","No visit(s) found");
        IsVists = 1;
    }
    $('.tableWrapper-appointment').removeClass('hideTable');
}
function patientAppointmentMedicationActivity(dataArray) {

    if(dataArray.length > 0) {
        var Id = dataArray[0].appointmentId;
        for (var i = 0; i < dataArray.length; i++) {
            // var dateString = new Date(dataArray[i].administrationTime).toLocaleDateString();
            // var dateString = GetDateTimeEdit(dataArray[i].administrationTime);
            var day;
            day = new Date(GetDateTimeEditDay(dataArray[i].administrationTime));
            day = day.getDay();
            day = getWeekDayName(day);

            var dateString = day + " " + GetDateTimeEdit(dataArray[i].administrationTime);
            // var timeString = new Date(dataArray[i].administrationTime).toLocaleTimeString();
            var notes = dataArray[i].notes;
            if(notes == null && notes == "null")
            {
                notes = "";
            }
            // $('#activitiesPeformed tbody').append('<tr><td name="patient-appointment-medicationTitle">Meds</td><td><p><span name="patient-appointment-medicationTradeName">'+dataArray[i].medicationTradeName+'</span><span name="patient-appointment-strength">'+dataArray[i].strength+'</span><span name="patient-appointment-medicationStrength">'+dataArray[i].medicationStrength+'</span></p><p><span name="patient-appointment-take">'+dataArray[i].take+'</span><span name="patient-appointment-medicationForm">'+dataArray[i].medicationForm+'</span><span name="patient-appointment-medicationFrequency">'+dataArray[i].medicationFrequency+'</span><span name="patient-appointment-medicationRoute">'+dataArray[i].medicationRoute+'</span></p><p><span name="patient-appointment-medicationInstructions">'+dataArray[i].medicationInstructions+'</span></p><p><span name="patient-appointment-instruction">'+dataArray[i].instruction+'</span></p></td><td name="patient-appointment-activityNotes"><p><span name="patient-appointment-administrationTime">' + dateString + ' '+ timeString +'</span></p><p><span name="patient-appointment-patientMedicationAction">'+dataArray[i].patientMedicationAction+'</span></p><p><span name="patient-appointment-notes">'+dataArray[i].notes+'</span></p></td></tr>');
            $('#tblActivitiesPerformed'+Id+' tbody').append('<tr><td name="patient-appointment-medicationTitle">Meds</td><td><p><span name="patient-appointment-medicationTradeName">'+dataArray[i].medicationTradeName+'</span><span name="patient-appointment-strength">'+dataArray[i].strength+'</span><span name="patient-appointment-medicationStrength">'+dataArray[i].medicationStrength+'</span></p><p><span name="patient-appointment-take">'+dataArray[i].take+'</span><span name="patient-appointment-medicationForm">'+dataArray[i].medicationForm+'</span><span name="patient-appointment-medicationFrequency">'+dataArray[i].medicationFrequency+'</span><span name="patient-appointment-medicationRoute">'+dataArray[i].medicationRoute+'</span></p><p><span name="patient-appointment-medicationInstructions">'+dataArray[i].medicationInstructions+'</span></p><p><span name="patient-appointment-instruction">'+dataArray[i].instruction+'</span></p></td><td name="patient-appointment-activityNotes"><p><span name="patient-appointment-administrationTime">' + dateString + '</span></p><p><span name="patient-appointment-patientMedicationAction">'+dataArray[i].patientMedicationAction+'</span></p><p><span name="patient-appointment-notes">'+notes+'</span></p></td><td></td></tr>');
        }
        $('.tableWrapper-appointment').removeClass('hideTable');
    }
}


var sortByAppointmentDateAndStaffName = function (a, b) {

    if (a.carer > b.carer) return 1;
    if (a.carer < b.carer) return -1;

    var date1 = a.appointmentDate;
    var date2 = b.appointmentDate;


    if (date1 > date2) return 1;
    if (date1 < date2) return -1;



}

var sortByAppointmentDateAndServiceUser = function (a, b) {

    if (a.serviceUser > b.serviceUser) return 1;
    if (a.serviceUser < b.serviceUser) return -1;

    var date1 = a.appointmentDate;
    var date2 = b.appointmentDate;


    if (date1 > date2) return 1;
    if (date1 < date2) return -1;



}

function buildPatientRosterList(dataSource){
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    otoptions.rowHeight = 100;
    gridColumns.push({
        "title": "Appt #",
        "field": "idk",
    });
    gridColumns.push({
        "title": "Appt Start Date",
        "field": "dateTimeOfAppointment",
    });
    gridColumns.push({
        "title": "Appt End Date",
        "field": "dateTimeOfAppointmentED",
    });
    gridColumns.push({
        "title": "SU Name",
        "field": "serviceUser",
        "width":"25%"
    });
    gridColumns.push({
        "title": "Staff Name",
        "field": "carer",
        "width":"15%"
    });

    gridColumns.push({
        "title": "Status",
        "field": "status",
    });
    gridColumns.push({
        "title": "Reason",
        "field": "reason",
    });

    angularPTUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
}
function secondsToHms(d) {
    d = Number(d);
    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    var hDisplay = h > 0 ? h + (h == 1 ? " hour, " : " hours, ") : "";
    var mDisplay = m > 0 ? m + (m == 1 ? " minute, " : " minutes, ") : "";
    var sDisplay = s > 0 ? s + (s == 1 ? " second" : " seconds") : "";
    return hDisplay + mDisplay + sDisplay;
}
function onPTChange(){

}
function startChange() {
    var startDate = startDT.value(),
        endDate = endDT.value();

    if (startDate) {
        startDate = new Date(startDate);
        startDate.setDate(startDate.getDate());
        endDT.min(startDate);
    } else if (endDate) {
        startDT.max(new Date(endDate));
    } else {
        endDate = new Date();
        startDT.max(endDate);
        endDT.min(endDate);
    }
}

function endChange() {
    var endDate = endDT.value(),
        startDate = startDT.value();

    if (endDate) {
        endDate = new Date(endDate);
        endDate.setDate(endDate.getDate());
        // startDT.max(endDate);
    } else if (startDate) {
        endDT.min(new Date(startDate));
    } else {
        endDate = new Date();
        // startDT.max(endDate);
        endDT.min(endDate);
    }
}
function buttonEvents(){
    $("#btnSearch1").off("click",onClickSearch);
    $("#btnSearch1").on("click",onClickSearch);

    $("#btnPtView").off("click",onClickPtViews);
    $("#btnPtView").on("click",onClickPtViews);

    $("#btnPrint").off("click",onClickPrint);
    $("#btnPrint").on("click",onClickPrint);

    $("#btnServiceUser").off("click");
    $("#btnServiceUser").on("click",onClickServiceUser);

    $("#btnStaff").off("click");
    $("#btnStaff").on("click",onClickStaff);

    $("#btnReset").off("click");
    $("#btnReset").on("click",onClickReset);

}
function onClickSearch(){
    var popW = "60%";
    var popH = 500;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search Patient";
    if(sessionStorage.clientTypeId == "2"){
        profileLbl = "Search Client";
    }
    devModelWindowWrapper.openPageWindow("../../html/patients/searchPatient.html", profileLbl, popW, popH, true, closePtAddAction);
}
function closePtAddAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        console.log(returnData);
        var pID = returnData.selItem.PID;
        viewPatientId = pID;
        var pName = returnData.selItem.FN+" "+returnData.selItem.MN+" "+returnData.selItem.LN;
        var viewpName = pName;
        if(pID != ""){
            //$("#lblName").text(pID+" - "+pName);
            $("#txtPatient").val(pName);
            /*var imageServletUrl = ipAddress + "/download/patient/photo/" + pID + "/?" + Math.round(Math.random() * 1000000);
            $("#imgPhoto").attr("src", imageServletUrl);

            getAjaxObject(ipAddress + "/patient/" + pID, "GET", onGetPatientInfo, onError);
        }*/
        }
    }
}
function getFacilityList(){
    getAjaxObject(ipAddress+"/facility/list/?is-active=1&is-deleted=0","GET",getFacilityDataList,onError);
}
function getFacilityDataList(dataObj){
    var dataArray = [];
    if(dataObj){
        if($.isArray(dataObj.response.facility)){
            dataArray = dataObj.response.facility;
        }else{
            dataArray.push(dataObj.response.facility);
        }
    }
    var tempDataArry = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
        }
        $("#cmbFacility").append('<option value="' + dataArray[i].idk + '">' + dataArray[i].name + '</option>');
    }
    onClickPtViews();
}
function onFacilityChange(){

}
function onError(err){

}
function onClickPrint(){
    //angularPTUIgridWrapper.printDataGrid();
    if ($.trim($('#divVisitSummaryReport').html()) !== "") {

        var contents = document.getElementById("divVisitSummaryReport").outerHTML;// $("#divTaskReportsViewer").html();
        var frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({ "position": "absolute", "top": "-1000000px" });
        $("body").append(frame1);
        var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();

        //Create a new HTML document.
        frameDoc.document.write('<html><title>Visit Summary Report</title><head>');

        frameDoc.document.write('<link href="../../css/external/css/bootstrap.min.css" rel="stylesheet" type="text/css"  />');
        frameDoc.document.write('<link href="../../css/Theme01.css" rel="stylesheet" rel="stylesheet" type="text/css"  />');
        frameDoc.document.write('<link href="../../css/report/careerInOutReport.css" rel="stylesheet" type="text/css" />');



        frameDoc.document.write('</head><body class="bodyClass" style="background-color: #fff;">');

        frameDoc.document.write('<div class="col-xs-12 prescription-contentDiv"> <div class="col-xs-12 ibox"> <div class="col-xs-12 ibox-content"> <div class="col-xs-12 ibox-content-inner"> <div class="col-xs-12 formArea">');


        //Append the DIV contents.
        frameDoc.document.write(contents);
        frameDoc.document.write('</div> </div> </div> </div> </div>');

        frameDoc.document.write('</body></html>');


        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();

        }, 500);
    }
    else {
        customAlert.info("info", "No data to print.");
    }
}
function onClickPtViews(){
    var ipUrl = ipAddress+"/appointment/list/?";
    var cmbFacility = $("#cmbFacility").val();


    if(selPatientId != null && selPatientId != 'undefined'){
        ipUrl = ipUrl+"patient-id="+selPatientId+"&";
    }
    var startDT = $("#txtStartDate").data("kendoDatePicker");
    var endDT = $("#txtEndDate").data("kendoDatePicker");


    var sDate = getFromDate(new Date(startDT.value()));
    ipUrl = ipUrl+"from-date="+sDate+"&";
    var eDate = getToDate(new Date(endDT.value()));
    ipUrl = ipUrl+"to-date="+eDate+"&is-active=1&is-deleted=0";
    getAjaxObject(ipUrl,"GET",buildAppointmentList,onError);
}



function onClickServiceUser(){
    var popW = 800;
    var popH = 450;

    if(parentRef)
        parentRef.searchZip = true;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Service User";
    devModelWindowWrapper.openPageWindow("../../html/patients/searchPatient.html", profileLbl, popW, popH, true, onCloseServiceUserAction);
}
function onCloseServiceUserAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        selPatientId = returnData.selItem.PID;
        $('#txtServiceUser').val(returnData.selItem.LN + " "+ returnData.selItem.FN);
    }
}

function onClickStaff(){
    var popW = 800;
    var popH = 450;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Staff";
    devModelWindowWrapper.openPageWindow("../../html/patients/staffList.html", profileLbl, popW, popH, true, onCloseStaffAction);
}

function onCloseStaffAction(evt,returnData){
    console.log(returnData);
    if(returnData && returnData.status == "success"){
        selProviderId = returnData.selItem.PID;
        $('#txtStaff').val(returnData.selItem.LN+' '+returnData.selItem.FN);

    }
}


function getFromDate(fromDate) {


    var day = fromDate.getDate();
    var month = fromDate.getMonth();
    month = month+1;
    var year = fromDate.getFullYear();

    var strDate = month+"/"+day+"/"+year;
    strDate = strDate+" 00:00:00";

    var date = new Date(strDate);
    date.setMilliseconds(0);
    var strDateTime = date.getTime();

    return strDateTime;
}

function getToDate(toDate) {
    var day = toDate.getDate();
    var month = toDate.getMonth();
    month = month+1;
    var year = toDate.getFullYear();

    var strDate = month+"/"+day+"/"+year;
    strDate = strDate+" 23:59:59";

    var date = new Date(strDate);
    date.setMilliseconds(0);
    var strDateTime = date.getTime();

    return strDateTime;
}

function onClickReset() {
    $("#txtServiceUser").val("");
    $("#txtStaff").val("");
    selProviderId = null;
    selPatientId = null;
    $("#txtStartDate").kendoDatePicker({format:dtFMT});
    $("#txtEndDate").kendoDatePicker({format:dtFMT});

    startDT = $("#txtStartDate").kendoDatePicker({
        change: startChange,format:dtFMT,value:new Date()
    }).data("kendoDatePicker");

    endDT= $("#txtEndDate").kendoDatePicker({
        change: endChange,format:dtFMT,value:new Date()
    }).data("kendoDatePicker");

    endDT.min(new Date());
    $("#cmbSortOrder").val(1);
}

function onGetDateTimeFormat(date) {
    var today = new Date(date);
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
     today = dd + '-' + mm + '-' + yyyy;
    return today;
}

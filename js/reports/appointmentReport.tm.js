var angularPTUIgridWrapper = null;
var dataArray = [];
var appReport = angular.module('appReport', []);
var selPatientId;
var selProviderId;

appReport.controller('reportController', function($scope) {
    $scope.getData = function () {
        onClickPtViews();

    }
    $scope.printTable = function () {

        var divToPrint = document.getElementById("carertable");
        var htmlToPrint = '' +
            '<style type="text/css">' +
            'table th, table td, table {' +
            'border:solid 1px #f3f6f9' +
            ';' +
            'padding;0.5em;' +
            'border-collapse:collapse;'+
            '}' +
            '</style>';
        htmlToPrint += divToPrint.outerHTML;

        // var newWin = window.open("");
        // newWin.document.write(htmlToPrint);
        // newWin.document.close();
        // newWin.focus();
        // newWin.print();
        // newWin.close();

        // var newWin  = window.open("");
        // newWin.document.body.innerHTML = htmlToPrint;
        // newWin.focus();
        // newWin.print();
        // newWin.close();

        window.frames["print_frame"].document.body.innerHTML = htmlToPrint;
        window.frames["print_frame"].window.focus();
        window.frames["print_frame"].window.print();

    }
});


$(document).ready(function(){
    $("#divMain",parent.document).css("display","none");
    $("#divPatInfo",parent.document).css("display","");
    // $("#lblTitleName",parent.document).html("Carer In Out Report");
    var cntry = sessionStorage.countryName;
    if((cntry.indexOf("India")>=0) || (cntry.indexOf("United Kingdom")>=0)){
        $("#lblFacility").text("Location");
    }else if(cntry.indexOf("United State")>=0) {
        $("#lblFacility").text("Facility");
    }
});

$(window).load(function(){
    parentRef = parent.frames['iframe'].window;
    onMessagesLoaded();
});
function adjustHeight(){
    var defHeight = 160;
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 100;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    try{angularPTUIgridWrapper.adjustGridHeight(cmpHeight);}catch(e){};

}
function onMessagesLoaded() {
    buttonEvents();
    init();
}
var cntry = "";
var dtFMT = "dd/MM/yyyy";
var stDate = "";
var endDate = "";

function init(){
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();


    cntry = sessionStorage.countryName;
    getFacilityList();
    if(cntry.indexOf("India")>=0){
        dtFMT = INDIA_DATE_FMT;
    }else  if(cntry.indexOf("United Kingdom")>=0){
        dtFMT = ENG_DATE_FMT;
    }else  if(cntry.indexOf("United State")>=0){
        dtFMT = US_DATE_FMT;
    }
    $("#txtStartDate").kendoDatePicker({format:dtFMT});
    $("#txtEndDate").kendoDatePicker({format:dtFMT});

    startDT = $("#txtStartDate").kendoDatePicker({
        change: startChange,format:dtFMT,value:new Date()
    }).data("kendoDatePicker");

    endDT= $("#txtEndDate").kendoDatePicker({
        change: endChange,format:dtFMT,value:new Date()
    }).data("kendoDatePicker");

    endDT.min(new Date());

    // $("#txtStartDate").kendoDatePicker({value:new Date()});
    // $("#txtEndDate").kendoDatePicker({value:new Date()});
    // var dataOptionsPT = {
    //         pagination: false,
    //         paginationPageSize: 500,
    // 		changeCallBack: onPTChange
    //     }
    // angularPTUIgridWrapper = new AngularUIGridWrapper("careerGrid1", dataOptionsPT);
    //   angularPTUIgridWrapper.init();
    //  buildPatientRosterList([]);
    adjustHeight();
    onClickPtViews();
}
//
// function getReportDataList(dataObj){
//     dataArray = [];
//     var tempdataArray = [];
//     if (dataObj && dataObj.response && dataObj.response.status) {
//         if (dataObj.response.status.code == "1") {
//             if (dataObj.response.appointment) {
//                 if ($.isArray(dataObj.response.appointment)) {
//                     dataArray = dataObj.response.appointment;
//                 } else {
//                     dataArray.push(dataObj.response.appointment);
//                 }
//             }
//         }
//     }
//
//
//     var sortOrderValue = $("#cmbSortOrder").val();
//     if(sortOrderValue == "1") {
//         dataArray.sort(function (a, b) {
//             var p1 = a.dateOfAppointment;
//             var p2 = b.dateOfAppointment;
//
//             if (p1 < p2) return -1;
//             if (p1 > p2) return 1;
//             return 0;
//         });
//     }
//     else if(sortOrderValue == "3") {
//         dataArray.sort(function (a, b) {
//             var p1 = a.dateOfAppointment;
//             var p2 = b.dateOfAppointment;
//             var o1, o2;
//             if (a.composition && a.composition.provider) {
//                 o1 = a.composition.provider.lastName.toLowerCase() + " " + a.composition.provider.firstName.toLowerCase();
//                 o2 = b.composition.provider.lastName.toLowerCase() + " " + b.composition.provider.firstName.toLowerCase();
//             }
//             if (p1 < p2) return -1;
//             if (p1 > p2) return 1;
//
//
//             if (o1 < o2) return -1;
//             if (o1 > o2) return 1;
//
//
//
//             // return 0;
//         });
//     }else if(sortOrderValue == "2") {
//         var sortByDateAsc = function (a, b) {
//             var p1 = a.dateOfAppointment;
//             var p2 = b.dateOfAppointment;
//             var o1, o2;
//             if (a.composition && a.composition.patient) {
//                 o1 = a.composition.patient.lastName.toLowerCase() + " " + a.composition.patient.firstName.toLowerCase();
//                 o2 = b.composition.patient.lastName.toLowerCase() + " " + b.composition.patient.firstName.toLowerCase();
//                 // o2 = b.composition.patient.lastName.toLowerCase();
//             }
//             // if (p1 > p2) return -1;
//             // if (p1 < p2) return 1;
//             if (p1 < p2) return 1;
//             if (p1 > p2) return -1;
//
//
//
//             if (o1 > o2) return -1;
//             if (o1 < o2) return 1;
//
//
//
//
//             // if (p1 === p2) {
//             //     if (a.composition && a.composition.patient) {
//             //         o1 = a.composition.patient.lastName.toLowerCase() + " " + a.composition.patient.firstName.toLowerCase();
//             //         o2 = b.composition.patient.lastName.toLowerCase() + " " + b.composition.patient.firstName.toLowerCase();
//             //         // o2 = b.composition.patient.lastName.toLowerCase();
//             //     }
//             //
//             //     //do the same as your already doing but using different data, from above
//             //     return (o1 < o2) ? -1 : (o1 > o2) ? 1 : 0;
//             // }
//             // else {
//             //     return (p1 < p2) ? -1 : (p1 > p2) ? 1 : 0;
//             // }
//
//
//             // if ((o1 < o2)&&(p1 < p2)) return -1;
//             // if ((o1 > o2)&&(p1 > p2) ) return 1;
//
//             // // if (p1 < p2) return -1;
//             // if (p1 > p2) return 1;
//
//
//             // return 0;
//         };
//
//         dataArray.sort(sortByDateAsc);
//         console.log(dataArray);
//
//         // const sortCols  = (a, b, attrs) => Object.keys(attrs)
//         //     .reduce((diff, k) =>  diff == 0 ? attrs[k](a[k], b[k]) : diff, 0);
//         //
//         // dataArray.sort(function(a, b) => sortCols(a, b, {
//         //     0: (a, b) => a.dateOfAppointment - b.dateOfAppointment,
//         //     1: (a, b) => a.composition.patient.lastName.toLowerCase() - b.composition.patient.lastName.toLowerCase()
//         // }))
//
//         // const criteria = ['dateOfAppointment', 'composition.patient.lastName']
//         // const data = [
//         //     { name: 'Mike', speciality: 'JS', age: 22 },
//         //     { name: 'Tom', speciality: 'Java', age: 30 },
//         //     { name: 'Mike', speciality: 'PHP', age: 40 },
//         //     { name: 'Abby', speciality: 'Design', age: 20 },
//         // ]
//
//         // dataArray = multisort(dataArray, criteria)
//
//         // dataArray.sort(function (a, b) {
//         //     var contentA =parseInt( $(a).attr('data-row'));
//         //     var contentB =parseInt( $(b).attr('data-row'));
//         //
//         //
//         //     //check if the rows are equal
//         //     if (contentA === contentB) {
//         //         var colA = parseInt( $(a).attr('data-col'));
//         //         var colB = parseInt( $(b).attr('data-col'));
//         //
//         //         //do the same as your already doing but using different data, from above
//         //         return (colA < colB) ? -1 : (colA > colB) ? 1 : 0;
//         //     }
//         //     else {
//         //         return (contentA < contentB) ? -1 : (contentA > contentB) ? 1 : 0;
//         //     }
//         // });
//     }
//
//     if (dataArray.length > 0) {
//         for (var d = 0; d < dataArray.length; d++) {
//             if (dataArray[d].dateOfAppointment) {
//                 dataArray[d].idk =dataArray[d].id;
//                 var date = new Date(GetDateTimeEditDay(dataArray[d].dateOfAppointment));
//                 var day = date.getDay();
//                 var dow = getWeekDayName(day);
//                 var dtEnd= dataArray[d].dateOfAppointment + (Number(dataArray[d].duration) * 60000);
//                 dataArray[d].dateTimeOfAppointment = dow + " " + GetDateTimeEdit(dataArray[d].dateOfAppointment);dataArray[d].DW = dow;
//                 var dateED = new Date(GetDateTimeEditDay(dtEnd));
//                 var dayED = dateED.getDay();
//                 var dowED = getWeekDayName(dayED);
//                 dataArray[d].dateTimeOfAppointmentED = dowED + " " + GetDateTimeEdit(dtEnd);
//
//                 dataArray[d].appDate = kendo.toString(new Date(dataArray[d].dateOfAppointment), "dd-MM-yyyy");
//                 dataArray[d].appTime = kendo.toString(new Date(dataArray[d].dateOfAppointment), "hh:mm tt");
//             }
//
//             if (dataArray[d].composition && dataArray[d].composition.provider) {
//                 dataArray[d].carer = dataArray[d].composition.provider.lastName + " " + dataArray[d].composition.provider.firstName;
//             }
//             if (dataArray[d].composition && dataArray[d].composition.patient) {
//                 dataArray[d].serviceUser = dataArray[d].composition.patient.lastName + " " + dataArray[d].composition.patient.firstName;
//             }
//             if (dataArray[d].composition && dataArray[d].composition.appointmentType) {
//                 dataArray[d].status = dataArray[d].composition.appointmentType.desc;
//             }
//             if (dataArray[d].composition && dataArray[d].composition.appointmentReason) {
//                 dataArray[d].reason = dataArray[d].composition.appointmentReason.desc;
//             }
//
//             if (dataArray[d].duration) {
//                 var totalMinutes = dataArray[d].duration;
//
//                 var hours = Math.floor(totalMinutes / 60);
//                 var minutes = Math.round(totalMinutes % 60);
//                 if (hours.toString().length == 1) {
//                     hours = "0"+hours;
//                 }
//                 if (minutes.toString().length == 1) {
//                     minutes = "0" + minutes;
//                 }
//
//
//                 dataArray[d].duration = hours + ":" + minutes;
//             }
//
//
//
//             tempdataArray.push(dataArray[d]);
//         }
//         var elem = angular.element(document.querySelector('[ng-app]'));
//         var injector = elem.injector();
//         var $rootScope = injector.get('$rootScope');
//         $rootScope.$apply(function(){
//             $rootScope.apidata = tempdataArray;
//         });
//         // buildPatientRosterList(tempdataArray);
//     }
//     else{
//         customAlert.error("info","No appointments");
//     }
//
//
// }

function getReportDataList(dataObj){
    dataArray = [];
    var tempdataArray = [];
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            if (dataObj.response.appointment) {
                if ($.isArray(dataObj.response.appointment)) {
                    dataArray = dataObj.response.appointment;
                } else {
                    dataArray.push(dataObj.response.appointment);
                }
            }
        }
    }



    if (dataArray.length > 0) {
        for (var d = 0; d < dataArray.length; d++) {
            if (dataArray[d].dateOfAppointment) {
                dataArray[d].idk =dataArray[d].id;
                var date = new Date(GetDateTimeEditDay(dataArray[d].dateOfAppointment));
                var day = date.getDay();
                var dow = getWeekDayName(day);
                var dtEnd= dataArray[d].dateOfAppointment + (Number(dataArray[d].duration) * 60000);
                dataArray[d].dateTimeOfAppointment = dow + " " + GetDateTimeEdit(dataArray[d].dateOfAppointment);dataArray[d].DW = dow;
                var dateED = new Date(GetDateTimeEditDay(dtEnd));
                var dayED = dateED.getDay();
                var dowED = getWeekDayName(dayED);
                dataArray[d].dateTimeOfAppointmentED = dowED + " " + GetDateTimeEdit(dtEnd);

                dataArray[d].appDate = kendo.toString(new Date(dataArray[d].dateOfAppointment), "dd-MM-yyyy");
                dataArray[d].appTime = kendo.toString(new Date(dataArray[d].dateOfAppointment), "hh:mm tt");

                var apptDateTime = new Date(dataArray[d].dateOfAppointment);

                var dateOfMonth = apptDateTime.getDate();
                var apptmonth = apptDateTime.getMonth();
                var apptyear = apptDateTime.getFullYear();
                var appthours = apptDateTime.getHours();
                var apptminutes = apptDateTime.getMinutes();
                dataArray[d].appointmentDate = new Date(apptyear, apptmonth, dateOfMonth, appthours, apptminutes, 0, 0);

            }

            if (dataArray[d].composition && dataArray[d].composition.provider) {
                dataArray[d].carer = dataArray[d].composition.provider.lastName + " " + dataArray[d].composition.provider.firstName;
            }
            if (dataArray[d].composition && dataArray[d].composition.patient) {
                dataArray[d].serviceUser = dataArray[d].composition.patient.lastName + " " + dataArray[d].composition.patient.firstName;
            }
            if (dataArray[d].composition && dataArray[d].composition.appointmentType) {
                dataArray[d].status = dataArray[d].composition.appointmentType.desc;
            }
            if (dataArray[d].composition && dataArray[d].composition.appointmentReason) {
                dataArray[d].reason = dataArray[d].composition.appointmentReason.desc;
            }

            if (dataArray[d].duration) {
                var totalMinutes = dataArray[d].duration;

                var hours = Math.floor(totalMinutes / 60);
                var minutes = Math.round(totalMinutes % 60);
                if (hours.toString().length == 1) {
                    hours = "0"+hours;
                }
                if (minutes.toString().length == 1) {
                    minutes = "0" + minutes;
                }


                dataArray[d].duration = hours + ":" + minutes;
            }



            tempdataArray.push(dataArray[d]);
        }

        if (tempdataArray != null && tempdataArray.length > 0) {
            var sortOrderValue = $("#cmbSortOrder option:selected").val();
            if (sortOrderValue == "1") {
                tempdataArray.sort(function (a, b) {
                    var p1 = a.dateOfAppointment;
                    var p2 = b.dateOfAppointment;

                    if (p1 < p2) return -1;
                    if (p1 > p2) return 1;
                    return 0;
                });
            }
            else if (sortOrderValue == "2") {
                tempdataArray.sort(sortByAppointmentDateAndServiceUser);
            }
            else if (sortOrderValue == "3") {
                tempdataArray.sort(sortByAppointmentDateAndStaffName);
            }
        }


        var elem = angular.element(document.querySelector('[ng-app]'));
        var injector = elem.injector();
        if(injector) {
            var $rootScope = injector.get('$rootScope');
            $rootScope.$apply(function () {
                $rootScope.apidata = tempdataArray;
            });
        }
        // buildPatientRosterList(tempdataArray);
    }
    else{
        customAlert.error("info","No appointments");
    }


}


var sortByAppointmentDateAndStaffName = function (a, b) {

    if (a.carer > b.carer) return 1;
    if (a.carer < b.carer) return -1;

    var date1 = a.appointmentDate;
    var date2 = b.appointmentDate;


    if (date1 > date2) return 1;
    if (date1 < date2) return -1;



}

var sortByAppointmentDateAndServiceUser = function (a, b) {

    if (a.serviceUser > b.serviceUser) return 1;
    if (a.serviceUser < b.serviceUser) return -1;

    var date1 = a.appointmentDate;
    var date2 = b.appointmentDate;


    if (date1 > date2) return 1;
    if (date1 < date2) return -1;



}

function buildPatientRosterList(dataSource){
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    otoptions.rowHeight = 100;
    gridColumns.push({
        "title": "Appt #",
        "field": "idk",
    });
    gridColumns.push({
        "title": "Appt Start Date",
        "field": "dateTimeOfAppointment",
    });
    gridColumns.push({
        "title": "Appt End Date",
        "field": "dateTimeOfAppointmentED",
    });
    gridColumns.push({
        "title": "SU Name",
        "field": "serviceUser",
        "width":"25%"
    });
    gridColumns.push({
        "title": "Staff Name",
        "field": "carer",
        "width":"15%"
    });

    gridColumns.push({
        "title": "Status",
        "field": "status",
    });
    gridColumns.push({
        "title": "Reason",
        "field": "reason",
    });

    angularPTUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
}
function secondsToHms(d) {
    d = Number(d);
    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    var hDisplay = h > 0 ? h + (h == 1 ? " hour, " : " hours, ") : "";
    var mDisplay = m > 0 ? m + (m == 1 ? " minute, " : " minutes, ") : "";
    var sDisplay = s > 0 ? s + (s == 1 ? " second" : " seconds") : "";
    return hDisplay + mDisplay + sDisplay;
}
function onPTChange(){

}
function startChange() {
    var startDate = startDT.value(),
        endDate = endDT.value();

    if (startDate) {
        startDate = new Date(startDate);
        startDate.setDate(startDate.getDate());
        endDT.min(startDate);
    } else if (endDate) {
        startDT.max(new Date(endDate));
    } else {
        endDate = new Date();
        startDT.max(endDate);
        endDT.min(endDate);
    }
}

function endChange() {
    var endDate = endDT.value(),
        startDate = startDT.value();

    if (endDate) {
        endDate = new Date(endDate);
        endDate.setDate(endDate.getDate());
        // startDT.max(endDate);
    } else if (startDate) {
        endDT.min(new Date(startDate));
    } else {
        endDate = new Date();
        // startDT.max(endDate);
        endDT.min(endDate);
    }
}
function buttonEvents(){
    $("#btnSearch1").off("click",onClickSearch);
    $("#btnSearch1").on("click",onClickSearch);

    $("#btnPtView").off("click",onClickPtViews);
    $("#btnPtView").on("click",onClickPtViews);

    $("#btnPrint").off("click",onClickPrint);
    $("#btnPrint").on("click",onClickPrint);

    $("#btnServiceUser").off("click");
    $("#btnServiceUser").on("click",onClickServiceUser);

    $("#btnStaff").off("click");
    $("#btnStaff").on("click",onClickStaff);

    $("#btnReset").off("click");
    $("#btnReset").on("click",onClickReset);
    
}
function onClickSearch(){
    var popW = "60%";
    var popH = 500;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search Patient";
    if(sessionStorage.clientTypeId == "2"){
        profileLbl = "Search Client";
    }
    devModelWindowWrapper.openPageWindow("../../html/patients/searchPatient.html", profileLbl, popW, popH, true, closePtAddAction);
}
function closePtAddAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        console.log(returnData);
        var pID = returnData.selItem.PID;
        viewPatientId = pID;
        var pName = returnData.selItem.FN+" "+returnData.selItem.MN+" "+returnData.selItem.LN;
        var viewpName = pName;
        if(pID != ""){
            //$("#lblName").text(pID+" - "+pName);
            $("#txtPatient").val(pName);
            /*var imageServletUrl = ipAddress + "/download/patient/photo/" + pID + "/?" + Math.round(Math.random() * 1000000);
            $("#imgPhoto").attr("src", imageServletUrl);

            getAjaxObject(ipAddress + "/patient/" + pID, "GET", onGetPatientInfo, onError);
        }*/
        }
    }
}
function getFacilityList(){
    getAjaxObject(ipAddress+"/facility/list/?is-active=1&is-deleted=0","GET",getFacilityDataList,onError);
}
function getFacilityDataList(dataObj){
    var dataArray = [];
    if(dataObj){
        if($.isArray(dataObj.response.facility)){
            dataArray = dataObj.response.facility;
        }else{
            dataArray.push(dataObj.response.facility);
        }
    }
    var tempDataArry = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
        }
        $("#cmbFacility").append('<option value="' + dataArray[i].idk + '">' + dataArray[i].name + '</option>');
    }
    onClickPtViews();
}
function onFacilityChange(){

}
function onError(err){

}
function onClickPrint(){
    angularPTUIgridWrapper.printDataGrid();
}
function onClickPtViews(){
    var ipUrl = ipAddress+"/appointment/list/?";
    var cmbFacility = $("#cmbFacility").val();
    if (cmbFacility != null && cmbFacility != undefined){
        ipUrl = ipUrl+"facility-id="+cmbFacility+"&";
    }
    if(selProviderId != null && selProviderId != 'undefined'){
        ipUrl = ipUrl+"provider-id="+selProviderId+"&";
    }
    if(selPatientId != null && selPatientId != 'undefined'){
        ipUrl = ipUrl+"patient-id="+selPatientId+"&";
    }
    var startDT = $("#txtStartDate").data("kendoDatePicker");
    var endDT = $("#txtEndDate").data("kendoDatePicker");


    var sDate = getFromDate(new Date(startDT.value()));
    ipUrl = ipUrl+"from-date="+sDate+"&";
    var eDate = getToDate(new Date(endDT.value()));
    ipUrl = ipUrl+"to-date="+eDate+"&is-active=1&is-deleted=0";
    getAjaxObject(ipUrl,"GET",getReportDataList,onError);
}



function onClickServiceUser(){
    var popW = 800;
    var popH = 450;

    if(parentRef)
        parentRef.searchZip = true;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Service User";
    parentRef.facilityId=$("cmbFacility").val();
    devModelWindowWrapper.openPageWindow("../../html/patients/searchPatient.html", profileLbl, popW, popH, true, onCloseServiceUserAction);
}
function onCloseServiceUserAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        selPatientId = returnData.selItem.PID;
        $('#txtServiceUser').val(returnData.selItem.LN + " "+ returnData.selItem.FN);
    }
}

function onClickStaff(){
    var popW = 800;
    var popH = 450;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Staff";
    devModelWindowWrapper.openPageWindow("../../html/patients/staffList.html", profileLbl, popW, popH, true, onCloseStaffAction);
}

function onCloseStaffAction(evt,returnData){
    console.log(returnData);
    if(returnData && returnData.status == "success"){
        selProviderId = returnData.selItem.PID;
        $('#txtStaff').val(returnData.selItem.LN+' '+returnData.selItem.FN);

    }
}


function getFromDate(fromDate) {


    var day = fromDate.getDate();
    var month = fromDate.getMonth();
    month = month+1;
    var year = fromDate.getFullYear();

    var strDate = month+"/"+day+"/"+year;
    strDate = strDate+" 00:00:00";

    var date = new Date(strDate);
    date.setMilliseconds(0);
    var strDateTime = date.getTime();

    return strDateTime;
}

function getToDate(toDate) {
    var day = toDate.getDate();
    var month = toDate.getMonth();
    month = month+1;
    var year = toDate.getFullYear();

    var strDate = month+"/"+day+"/"+year;
    strDate = strDate+" 23:59:59";

    var date = new Date(strDate);
    date.setMilliseconds(0);
    var strDateTime = date.getTime();

    return strDateTime;
}

function onClickReset() {
    $("#txtServiceUser").val("");
    $("#txtStaff").val("");
    selProviderId = null;
    selPatientId = null;
    $("#txtStartDate").kendoDatePicker({format:dtFMT});
    $("#txtEndDate").kendoDatePicker({format:dtFMT});

    startDT = $("#txtStartDate").kendoDatePicker({
        change: startChange,format:dtFMT,value:new Date()
    }).data("kendoDatePicker");

    endDT= $("#txtEndDate").kendoDatePicker({
        change: endChange,format:dtFMT,value:new Date()
    }).data("kendoDatePicker");

    endDT.min(new Date());
    $("#cmbSortOrder").val(1);
}
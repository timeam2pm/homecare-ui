var operation = "";
var selItem = null,setTaskGroupItem=null;
var ADD = "add";
var UPDATE = "update";
var VIEW = "view";
var DELETE = "delete";

var files = null;
var fileName = "";
var compressorSettings = {
    // toWidth: 30,
    // toHeight: 42,
    mimeType: 'image/png',
    mode: 'strict',
    quality: 0.2,
    grayScale: false,
    sepia: false,
    threshold: false,
    vReverse: false,
    hReverse: false,
    speed: 'low'
};
var isBrowseFlag = false;

var imagePhotoData = null;
var componentId;

var IsThumbnailPresent =0;
var statusArr = [{Key:'Active',Value:'Active'},{Key:'InActive',Value:'InActive'}];
var compType = [{Key:'Text',Value:'1'},{Key:'Text Area',Value:'2'},{Key:'Boolean',Value:'3'},{Key:'Checkbox',Value:'4'},{Key:'Radio Button',Value:'5'}];
var angularUIgridWrapper,angularUIgridWrapper2;
var parentRef = null;
var typeArr;
// = [{Key:'ADL',Value:'1'},{Key:'iADL',Value:'2'},{Key:'Care',Value:'3'},{Key:'Fluid',Value:'4'},{Key:'Food',Value:'5'}];
var masterTaskGroups,masterTaskReports,activityTypeId,tmpReportColumns;
var taskReportColumnLimit = 8;
var tmpSelectedComponents = [];
var reportDisplayName = "";
var deletedReportColumns = [];


$(document).ready(function(){
    sessionStorage.setItem("IsSearchPanel", "1");
    $("#pnlPatient",parent.document).css("display","none");
    //themeAPIChange();
    $("#divDuration").hide();
    $("#divCharge").hide();
    //bindTheGrid();
    bindTheTaskReportsGrid();

});


$(window).load(function(){
    loading = false;
    //$(window).resize(adjustHeight);
    //onLoaded();

    loadTaskReportsGrid();
    buttonEvents();

    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
    parentRef = parent.frames['iframe'].window;
    //getAjaxObject(ipAddress+"/master/type/list/?name=activity","GET",getActivityTypes1,onError);
    //init();
    //getAjaxObject(ipAddress+"/homecare/activity-types/?is-active=1&is-deleted=0&sort=type","GET",getTaskTypes,onError);
    // debugger;

    //adjustHeight();
    //$('.btnActive').trigger('click');
}

function init(){

    //$("#cmbType").kendoComboBox();
    //$("#txtID").kendoComboBox();


    getAjaxObject(ipAddress+"/master/type/list/?name=component","GET",getComponentTypes,onError);

}

function onTYpeChange(){
    var txtID = $("#txtID").data("kendoComboBox");
    if(txtID && txtID.selectedIndex<0){
        txtID.select(0);
    }
    if(txtID){
        var strVal = txtID.value();
        if(strVal  == '1' || strVal == '2' ){
            /*$("#divDuration").show();*/
            /*$("#divCharge").show();*/
            $("#txtDuration").attr("readonly",false);
            $("#txtCharge").attr("readonly",false);
            $("#spnDuration").hide();
            $("#spnCharge").hide();
        }else{
            $("#divDuration").hide();
            $("#divCharge").hide();
            //$("#txtDuration").attr("readonly",true);
            //$("#txtCharge").attr("readonly",true);
            $("#spnDuration").hide();
            $("#spnCharge").hide();
        }

    }
    if(operation == ADD) {
        var displayOrder = 0;
        displayOrder = _.where(actComponents, {activityTypeId: parseInt(strVal)}).length + 1;
        $("#txtDisplayOrder").val(displayOrder);
    }
}
function onCompTypeChange(){
    var cmbType = $("#cmbType").data("kendoComboBox");
    if(cmbType && cmbType.selectedIndex<0){
        cmbType.select(0);
    }
    $("#taDesc").val("");
    $("#taDesc").attr("readonly",false);
    if(cmbType.text().toLowerCase() == "check box" || cmbType.text().toLowerCase() == "diagram" || cmbType.text().toLowerCase() == "image"){
        $("#taDesc").attr("readonly",true);
    }

    if(cmbType.text().toLowerCase() == "diagram"){
        $("#divCamera").css("display","none");
    }
    else{
        $("#divCamera").css("display","");
    }

    if(operation == UPDATE && selItem && cmbType.text() != "check box"){
        $("#taDesc").val(selItem.description);
    }
}

function getComponentTypes(dataObj){
    var tempCompType = [];
    compType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.typeMaster){
            if($.isArray(dataObj.response.typeMaster)){
                tempCompType = dataObj.response.typeMaster;
            }else{
                tempCompType.push(dataObj.response.typeMaster);
            }
        }
    }
    for(var i=0;i<tempCompType.length;i++){
        var obj = {};
        obj.Key = tempCompType[i].type;
        obj.Value = tempCompType[i].id;
        compType.push(obj);
    }
    //setDataForSelection(compType, "cmbType", onCompTypeChange, ["Key", "Value"], 0, "");
     getAjaxObject(ipAddress+"/master/type/list/?name=activity","GET",getActivityTypes1,onError);

}

function getTaskReportsSCB(dataObj) {
    var tempTaskReports = [];
    compType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.taskReports){
            if($.isArray(dataObj.response.taskReports)){
                tempTaskReports = dataObj.response.taskReports;
            }else{
                tempTaskReports.push(dataObj.response.taskReports);
            }
        }
    }

    // masterTaskReports = tempTaskReports;
    var lookup = {};
    var items = tempTaskReports;
    var result = [];

    for (var item, i = 0; i<items.length;i++) {
        item = items[i];
        var name = item.name;

        if (!(name in lookup)) {
            lookup[name] = 1;
            result.push(name);
        }
    }

    masterTaskReports =items;

    var filterArray = [];

    if (result != null && result.length > 0){

        for(var counter=0;counter<result.length;counter++){
            var tempObj=_.where(masterTaskReports, {name: result[counter]});

            if (tempObj !== null && tempObj.length > 0 && tempObj[0].name && tempObj[0].name !== "") {
                filterArray.push(tempObj[0]);
            }

        }
    }

    if(filterArray !== null && filterArray.length > 0){
        filterArray.sort(sortByReportDisplayName);
        buildTaskReportsListGrid(filterArray);
    }


}

function getActivityTypes1(dataObj){
    // console.log('init');

    var tempCompType = [];
    typeArr = [];

    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.typeMaster) {
            if ($.isArray(dataObj.response.typeMaster)) {
                tempCompType = dataObj.response.typeMaster;
            } else {
                tempCompType.push(dataObj.response.typeMaster);
            }
        }
    }
    tempCompType.sort(function (a, b) {
        var nameA = a.type.toUpperCase(); // ignore upper and lowercase
        var nameB = b.type.toUpperCase(); // ignore upper and lowercase
        if (nameA < nameB) {
            return -1;
        }
        if (nameA > nameB) {
            return 1;
        }
        // names must be equal
        return 0;
    });

    for (var i = 0; i < tempCompType.length; i++) {
        var obj = {};
        obj.Key = tempCompType[i].type;
        obj.Value = tempCompType[i].id;
        typeArr.push(obj);

    }

    getAjaxObject(ipAddress+"/homecare/activity-types/?is-active=1&is-deleted=0&sort=type","GET",getTaskTypes,onError);

}

function getActivityTypes1Edit(dataObj){
    // console.log('init');

    var tempCompType = [];
    typeArr = [];

    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.typeMaster) {
            if ($.isArray(dataObj.response.typeMaster)) {
                tempCompType = dataObj.response.typeMaster;
            } else {
                tempCompType.push(dataObj.response.typeMaster);
            }
        }
    }
    tempCompType.sort(function (a, b) {
        var nameA = a.type.toUpperCase(); // ignore upper and lowercase
        var nameB = b.type.toUpperCase(); // ignore upper and lowercase
        if (nameA < nameB) {
            return -1;
        }
        if (nameA > nameB) {
            return 1;
        }
        // names must be equal
        return 0;
    });

    for (var i = 0; i < tempCompType.length; i++) {
        var obj = {};
        obj.Key = tempCompType[i].type;
        obj.Value = tempCompType[i].id;
        typeArr.push(obj);

    }

    getAjaxObject(ipAddress+"/homecare/activity-types/?is-active=1&is-deleted=0&sort=type","GET",getTaskTypesEdit,onError);

}

function getTaskTypes2(dataObj){

    var types = [];
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            if(dataObj.response.activityTypes){
                if($.isArray(dataObj.response.activityTypes)){
                    types = dataObj.response.activityTypes;
                }else{
                    types.push(dataObj.response.activityTypes);
                }
            }
            for(var i=0;i<types.length;i++){
                types[i].idk = types[i].id;
            }
        }
    }

    masterTaskGroups=types;

    getAjaxObject(ipAddress+"/homecare/task-reports/?is-active=1&is-deleted=0&access_token="+sessionStorage.access_token+"&fields=*,activity.*","GET",getTaskReportsSCB,onError);

}

function getTaskTypes(dataObj){

    var types = [];
    var taskGroups = [];
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            if(dataObj.response.activityTypes){
                if($.isArray(dataObj.response.activityTypes)){
                    types = dataObj.response.activityTypes;
                }else{
                    types.push(dataObj.response.activityTypes);
                }
            }

            for(var i=0;i<types.length;i++){

                if (types[i].reportDisplayName && types[i].reportDisplayName != "") {
                    types[i].idk = types[i].id;
                    taskGroups.push(types[i]);
                }


            }
        }
    }

    buildDeviceListGrid1(taskGroups);

    var urlExtn = '/activity/list?is-active=1&is-deleted=0&sort=activity';
    getAjaxObject(ipAddress+urlExtn,"GET",getInactiveDataList,onError);

}

function getTaskTypesEdit(dataObj){

    var types = [];
    var taskGroups = [];
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            if(dataObj.response.activityTypes){
                if($.isArray(dataObj.response.activityTypes)){
                    types = dataObj.response.activityTypes;
                }else{
                    types.push(dataObj.response.activityTypes);
                }
            }
            for(var i=0;i<types.length;i++){

                if (types[i].reportDisplayName && types[i].reportDisplayName != "") {
                    types[i].idk = types[i].id;
                    taskGroups.push(types[i]);
                }


            }
        }
    }

    buildDeviceListGrid1(taskGroups);

    var urlExtn = '/activity/list?is-active=1&is-deleted=0&sort=activity';
    getAjaxObject(ipAddress+urlExtn,"GET",getInactiveDataListEdit,onError);

}

function getActivityTypes(dataObj){
    var tempCompType = [];
    typeArr = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.typeMaster){
            if($.isArray(dataObj.response.typeMaster)){
                tempCompType = dataObj.response.typeMaster;
            }else{
                tempCompType.push(dataObj.response.typeMaster);
            }
        }
    }
    for(var i=0;i<tempCompType.length;i++){
        var obj = {};
        obj.Key = tempCompType[i].type;
        obj.Value = tempCompType[i].id;
        typeArr.push(obj);
    }

    // showGrid();
}
function showGrid(){
    // alert("5");
    // buildStateListGrid([]);
     getAjaxObject(ipAddress+"/activity/list?is-active=1&is-deleted=0&sort=activity","GET",getStateList,onError);

}
function onError(errorObj){
    console.log(errorObj);
}

function getActivityNameById(aId){
    for(var i=0;i<typeArr.length;i++){
        var item = typeArr[i];
        if(item && item.Value == aId){
            return item.Key;
        }
    }
    return "";
}
function getTypeNameById(aId){
    for(var i=0;i<compType.length;i++){
        var item = compType[i];
        if(item && item.Value == aId){
            return item.Key;
        }
    }
    return "";
}
var actComponents;
function getStateList(dataObj){
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.activity){
        if($.isArray(dataObj.response.activity)){
            dataArray = dataObj.response.activity;
        }else{
            dataArray.push(dataObj.response.activity);
        }

        var maxDisOrder = getMax(dataObj.response.activity, "displayOrder");
        var displayOrderMaxlength = maxDisOrder.displayOrder;

        sessionStorage.setItem("displayOrderMaxlength", (displayOrderMaxlength + 1));
    }
    var tempDataArry = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].displayOrder1 = dataArray[i].displayOrder;
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
        }

        dataArray[i].activityID = getActivityNameById(dataArray[i].activityTypeId);
        dataArray[i].Type = getTypeNameById(dataArray[i].componentId);

        if(dataArray[i].Type.toLowerCase() == "diagram") {
            dataArray[i].photo = ipAddress + "/homecare/download/activities/?" + Math.round(Math.random() * 1000000) +"&access_token=" + sessionStorage.access_token + "&id=" + dataArray[i].id + "&tenant=" + sessionStorage.tenant;
        }
        else{
            if (dataArray[i].thumbnailPresent == "1") {
                dataArray[i].photo = ipAddress + "/homecare/download/activities/thumbnail/?" + Math.round(Math.random() * 1000000) +"&access_token=" + sessionStorage.access_token + "&id=" + dataArray[i].id + "&tenant=" + sessionStorage.tenant;
            }
        }
    }
    actComponents = dataArray;

    getAjaxObject(ipAddress+"/homecare/activity-types/?is-active=1&is-deleted=0&sort=type","GET",getTaskTypes2,onError);
    //buildStateListGrid(dataArray);
}

function getInactiveDataList(dataObj){

    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.activity){
        if($.isArray(dataObj.response.activity)){
            dataArray = dataObj.response.activity;
        }else{
            dataArray.push(dataObj.response.activity);
        }
    }
    var tempDataArry = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].displayOrder1 = dataArray[i].displayOrder;
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
        }

        dataArray[i].activityID = getActivityNameById(dataArray[i].activityTypeId);
        dataArray[i].Type = getTypeNameById(dataArray[i].componentId);
        if(dataArray[i].Type.toLowerCase() == "diagram") {
            dataArray[i].photo = ipAddress + "/homecare/download/activities/?" + Math.round(Math.random() * 1000000) +"&access_token=" + sessionStorage.access_token + "&id=" + dataArray[i].id + "&tenant=" + sessionStorage.tenant;
        }
        else{
            if (dataArray[i].thumbnailPresent == "1") {
                dataArray[i].photo = ipAddress + "/homecare/download/activities/thumbnail/?" + Math.round(Math.random() * 1000000) +"&access_token=" + sessionStorage.access_token + "&id=" + dataArray[i].id + "&tenant=" + sessionStorage.tenant;
            }
        }
    }
    actComponents = dataArray;
    var selectedItems = angularUIgridWrapper1.getSelectedRows();
    if(selectedItems && selectedItems.length > 0) {
        var filterArray = [];
        filterArray = _.where(actComponents, {activityTypeId: selectedItems[0].idk});

        //buildStateListGrid(filterArray);
    }
}

function getInactiveDataListEdit(dataObj){
    debugger;
    console.log("hai");
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.activity){
        if($.isArray(dataObj.response.activity)){
            dataArray = dataObj.response.activity;
        }else{
            dataArray.push(dataObj.response.activity);
        }
    }
    var tempDataArry = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].displayOrder1 = dataArray[i].displayOrder;
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
        }

        dataArray[i].activityID = getActivityNameById(dataArray[i].activityTypeId);
        dataArray[i].Type = getTypeNameById(dataArray[i].componentId);
        if(dataArray[i].Type.toLowerCase() == "diagram") {
            dataArray[i].photo = ipAddress + "/homecare/download/activities/?" + Math.round(Math.random() * 1000000) +"&access_token=" + sessionStorage.access_token + "&id=" + dataArray[i].id + "&tenant=" + sessionStorage.tenant;
        }
        else{
            if (dataArray[i].thumbnailPresent == "1") {
                dataArray[i].photo = ipAddress + "/homecare/download/activities/thumbnail/?" + Math.round(Math.random() * 1000000) +"&access_token=" + sessionStorage.access_token + "&id=" + dataArray[i].id + "&tenant=" + sessionStorage.tenant;
            }
        }
    }
    actComponents = dataArray;
    var selectedItems = angularUIgridWrapper1.getSelectedRows();
    if(selectedItems && selectedItems.length > 0) {
        var filterArray = [];
        filterArray = _.where(actComponents, {activityTypeId: selectedItems[0].idk});
        //buildStateListGrid(filterArray);
    }

    //test(activityTypeId);
    //bindTaskGroupReportColumns(reportDisplayName);
}

function buttonEvents(){
    $("#btnEdit").off("click",onClickOK);
    $("#btnEdit").on("click",onClickOK);

    // $("#btnCancel").off("click",onClickCancel);
    // $("#btnCancel").on("click",onClickCancel);

    $("#btnDelete").off("click",onClickDelete);
    $("#btnDelete").on("click",onClickDelete);

    $("#btnAddTaskReport").off("click");
    $("#btnAddTaskReport").on("click",onClickAdd);


    $("#btnSaveDO").off("click",onClickSaveDO);
    $("#btnSaveDO").on("click",onClickSaveDO);

    $("#btnReset").off("click", onClickReset);
    $("#btnReset").on("click", onClickReset);

    $(".popupClose").off("click", onClickCancel);
    $(".popupClose").on("click", onClickCancel);

    $("#btnUpdate").off("click", onClickUpdate);
    $("#btnUpdate").on("click", onClickUpdate);


    // $(".popupClose").on("click", function(e) {
    //     e.preventDefault();
    //     buildStateListGrid([]);
    //     onClickCancel();
    //     // searchOnLoad('active');
    //     // onChange1();
    // });

    // $("#btnCancel").on("click", function() {
    //     // e.preventDefault();
    //     buildStateListGrid([]);
    //     onClickCancel();
    //     // $('.btnActive').trigger('click');
    //     // onChange1();
    // });

    $("#btnSave").off("click",onClickSave);
    $("#btnSave").on("click",onClickSave);

    // $("#btnSave").off("click",onClickSave);
    // $("#btnSave").on("click", function(e) {
    //     e.preventDefault();
    //     // buildStateListGrid([]);
    //     onClickSave();
    // });


    $("#btnBrowse").on("click", function(e) {
        e.preventDefault();
        if(IsRemove == 1){
            $("#imgPhoto").attr("src","");
        }
        onClickBrowse(e);
    });

    //
    // $("#btnBrowse").off("click", onClickBrowse);
    // $("#btnBrowse").on("click", onClickBrowse);

    $("#btnBrowse").off("mouseover", onClickBrowseOver);
    $("#btnBrowse").on("mouseover", onClickBrowseOver);

    $("#btnBrowse").off("mouseout", onClickBrowseOut);
    $("#btnBrowse").on("mouseout", onClickBrowseOut);

    $("#fileElem").off("change", onSelectionFiles);
    $("#fileElem").on("change", onSelectionFiles);

    $("#fileElem").off("click", onSelectionFiles);
    $("#fileElem").on("click", onSelectionFiles);


    $(".btnActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('active');
        onClickActive();
    });
    $(".btnInActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('inactive');
        onClickInActive();
    });

    // $("#appointment-task").off("click");
    // $("#appointment-task").on("click",onClickAddTasks);

    $("#btnSaveTaskReportColumns").off("click",onClickSaveReportColumns);
    $("#btnSaveTaskReportColumns").on("click",onClickSaveReportColumns);

    $("#btnEditTaskReport").off("click",onClickEditTaskReport);
    $("#btnEditTaskReport").on("click",onClickEditTaskReport);

    $("#btnCancelReport").off("click",onClickCancelReport);
    $("#btnCancelReport").on("click",onClickCancelReport);

    $("#btnPreview").off("click",onClickPreview);
    $("#btnPreview").on("click",onClickPreview);

    $("#btnUpdateTaskGroupName").off("click", onClickUpdateTaskGroupName);
    $("#btnUpdateTaskGroupName").on("click", onClickUpdateTaskGroupName);

    // $("#cmbTaskReportComponents").off("change", bindTaskReportColumns);
    // $("#cmbTaskReportComponents").on("change", bindTaskReportColumns);


    $("#cmbTaskReportComponents").on( "change", function() {
        // bindTaskReportColumns(activityTypeId);

        test(activityTypeId);
    });


}

function onClickUpdate() {
    $("#txtTaskGroupName").val($("#cmbTaskReportComponents option:selected").text());
    $("#myModalNorm").modal("show");
}

function onClickUpdateTaskGroupName() {
    // var $selectedColumns = $("#ulTaskReportColumns li");
    //
    // var dataObj = [];
    //
    // for (var counter = 0; counter < $selectedColumns.length; counter++) {
    //
    //     var objTaskColumn = {};
    //
    //     var attr = $($selectedColumns[counter]).attr('taskreportid');
    //     objTaskColumn.id = Number($($selectedColumns[counter]).attr("taskreportid"));
    //     objTaskColumn.activityID = Number($($selectedColumns[counter]).attr("componentid"));
    //     objTaskColumn.isActive = 1;
    //     objTaskColumn.activityTypeId = Number($($selectedColumns[counter]).attr("activitytypeid"));
    //
    //     dataObj.push(objTaskColumn);
    // }
    //
    // var tmpactivityID = Number($("#cmbTaskReportComponents").val());
    //
    //
    // if (dataObj != null && dataObj.length > 0) {
    //     var tmpDataObj = _.where(dataObj, { activityID: tmpactivityID });
    //
    //     if (tmpDataObj && tmpDataObj.length > 0) {
    //         // logic to update
    //     }
    // }
    var dataObj = [];
    var reportOldDisplayName = $("#cmbTaskReportComponents option:selected").text();
    var oldTmpDataObj = _.where(masterTaskReports, { name: reportOldDisplayName, activityTypeId : activityTypeId });
    var objTaskColumn = {};
    var reportDisplayName = $("#txtTaskGroupName").val();
    if(reportDisplayName != null && reportDisplayName != ""){
        var tmpDataObj = _.where(masterTaskReports, { name: reportDisplayName });
        if(tmpDataObj && tmpDataObj.length > 0){
            customAlert.error("Error","Same name already exist !");
        }
        else{

            objTaskColumn.id = oldTmpDataObj[0].id;
            objTaskColumn.modifiedBy = Number(sessionStorage.userId);
            objTaskColumn.createdBy=oldTmpDataObj[0].createdBy;
            objTaskColumn.activityID=oldTmpDataObj[0].activityId;
            objTaskColumn.isDeleted=0;
            objTaskColumn.displayOrder=oldTmpDataObj[0].displayOrder;
            objTaskColumn.name=reportDisplayName;
            objTaskColumn.isActive=1;
            objTaskColumn.activityTypeId=oldTmpDataObj[0].activityTypeId;

            dataObj.push(objTaskColumn);

            var dataUrl = ipAddress + "/homecare/task-reports/batch/";
            // console.log(JSON.stringify(dataObj));
            createAjaxObject(dataUrl, dataObj, "PUT", onReportNameUpdate, onReportColumnsSaveECB);
        }

    }
    else{
        customAlert.error("Error","Please enter report display name");
    }
}

function onReportNameUpdate(response) {
    // console.log("SUCCESS");
    // alert("TaskRreports created/updated/deleted successfully.");

    displaySessionErrorPopUp("Info", "Report name updated successfully", function(res) {
        //bindTheComboBox(activityTypeId);
    });

    $("#myModalNorm").modal("hide");

    // customAlert.info("info","Report name updated successfully");
    // console.log(JSON.stringify(response));

}

function onClickCancelReport() {
    setTaskGroupItem = null;
    activityTypeId = null;
    loadTaskReportsGrid();

}


function onClickEditTaskReport() {
    debugger;
    operation = UPDATE;

    if (setTaskGroupItem != null && setTaskGroupItem.name != null && setTaskGroupItem.name != "") {

        deletedReportColumns = [];

        $("#divTaskComponentsAdd").hide();
        $("#divTaskComponentsUpdate").show();
        $('#taskReportInner').addClass('taskReportInnerActive');
        $('#taskReportInnerBlock').addClass('taskReportBlockActive');

        //$("#divTaskReportColumnName").css('display', 'none');
        //$("#divTaskReportComponents").css('display', 'block');



        reportDisplayName = setTaskGroupItem.name;
        $("#txtTaskReportColumnName2").val(reportDisplayName);
        //activityTypeId = setTaskGroupItem.idk;

        $("#divTaskComponents").show();
        $("#divTaskReports").hide();


        bindTheGrid();

        getAjaxObject(ipAddress + "/master/type/list/?name=activity", "GET", getActivityTypes1Edit, onError);
        //init();
        //test(reportDisplayName);
        bindTaskGroupReportColumns();
        //bindTaskReportColumns2()

        //$('.btnActive').trigger('click');
        // searchOnLoad('active');


        //test(11);
        checkTaskReportColumnHeaders();
    }
}

function onClickActive() {
    $(".btnInActive").removeClass("radioButton-active");
    $(".btnActive").addClass("radioButton-active");
}
function onClickInActive() {
    $(".btnActive").removeClass("radioButton-active");
    $(".btnInActive").addClass("radioButton-active");
}

function searchOnLoad(status) {
    //buildStateListGrid([]);
    if(status == "active") {
        var urlExtn = '/activity/list?is-active=1&is-deleted=0&sort=activity';
    }
    else if(status == "inactive") {
        var urlExtn = '/activity/list?is-active=0&is-deleted=1&sort=activity';
    }

    getAjaxObject(ipAddress+urlExtn,"GET",getInactiveDataList,onError);
}

function adjustHeight(){
    var defHeight = 200;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
   angularUIgridWrapper2.adjustGridHeight(cmpHeight);
}

function adjustHeightGroup(){
    var defHeight = 154;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    angularUIgridWrapper1.adjustGridHeight(cmpHeight);
}


function showPtImage(){
    var node = '<div><img src="{{row.entity.photo}}" onerror="this.src=\'../../img/AppImg/HosImages/blank.png\'" style="width:50px"><spn>';
    node = node+"</div>";
    return node;
}

function showDefaultValue(){
    var node = '<div>';
    node += '<input  type="number" id="txtI2Val" ng-model="row.entity.displayOrder" value="{{row.entity.displayOrder}}" class="txtField" min="1"  max="1000" maxlength="5" onchange="maxLengthCheck(event)" style="height:25px;width:100px"></input>';
    node += '</div>';
    return node;
}

var selRow = null;
function maxLengthCheck(e){
    console.log(e);
    selRow = angular.element($(e.currentTarget).parent()).scope();
    setTimeout(function(){
        upDateDataSource();
    },100)
}

function upDateDataSource(){
    var selectedItems = angularUIgridWrapper.getSelectedRows();
    console.log(selectedItems);
    var selRowIndex = 0;
    if(selRow){
        var selItem = selRow.row.entity;//selectedItems[0];
        selRowIndex = selItem.idk;
        var idVal = selItem.displayOrder;
        idVal = Number(idVal);

        var begin = selItem.displayOrder;
        begin = Number(begin);

        var end = selItem.displayOrder1;
        end = Number(end);

        //selItem.DIO1 = end;
        //angularUIgridWrapper.refreshGrid();
        var rows = angularUIgridWrapper.getScope().gridApi.core.getVisibleRows();
        console.log(rows);

        for(var i=0;i<rows.length;i++){
            var rowItem = rows[i].entity;
            if(rowItem.id == selRow.row.entity.id){
                continue;
            }
            var rowValue = rowItem.displayOrder;
            if(end>begin){
                flag = true;
                if(rowValue>=begin && rowValue<end){
                    var rValue = (rowItem.displayOrder+1);
                    rowItem.displayOrder = rValue;
                    rowItem.displayOrder1 = rValue;
                }
            }else{
                flag = false;
                if(rowValue>end && rowValue < (begin+1)){
                    var rValue = (rowItem.displayOrder-1);
                    rowItem.displayOrder = rValue;
                    rowItem.displayOrder1 = rValue;
                }
            }
        }
        if(flag){
            selRow.row.entity.displayOrder1 = begin;
        }else{
            selRow.row.entity.displayOrder1 = begin;
        }
        angularUIgridWrapper.refreshGrid();
    }
}

function GetSortOrder(prop) {
    return function(a, b) {
        if (a[prop] > b[prop]) {
            return 1;
        } else if (a[prop] < b[prop]) {
            return -1;
        }
        return 0;
    }
}

var prevSelectedItem =[];
function onChange(){
    setTimeout(function(){
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if(selectedItems && selectedItems.length>0){
            selItem = selectedItems[0];
            $("#btnEdit").prop("disabled", false);
            $("#btnDelete").prop("disabled", false);
        }else{
            $("#btnEdit").prop("disabled", true);
            $("#btnDelete").prop("disabled", true);
        }
    },100)

}
function onChange1(){
    //buildStateListGrid([]);
    setTimeout(function(){
        var selectedItems = angularUIgridWrapper1.getSelectedRows();
        // console.log(selectedItems);
        // console.log(actComponents);
        // console.log(_.where(actComponents, {activityTypeId: selectedItems[0].idk}));
        if(selectedItems && selectedItems.length>0){
            // $("#btnEdit").prop("disabled", false);
            // $("#btnDelete").prop("disabled", false);
            bindTaskGroupComponents(selectedItems[0].idk);
            //bindTaskComponents(filterArray);
        }else{

            // $("#btnDelete").prop("disabled", true);
        }
    },100)

}

function onClickOK(){
    setTimeout(function(){
        $("#txtAddID").show();
        $(".filter-heading").html("Edit Task Components");
        // var tagFileId = ("fileElem");
        // var file = document.getElementById(tagFileId);
        // if(file && file.files[0] && file.files[0].name){
        //     fileName = null;
        //     file.files[0].name = null;
        //     file.files[0] = null;
        // }

        operation = "edit";
        parentRef.operation = "edit";
        isBrowseFlag = false;
        $("#viewDivBlock").hide();
        $(".taskComponentLeft").hide();
        $("#addTaskGroupPopup").show();
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if(selectedItems && selectedItems.length>0){
            parentRef.selitem = selectedItems[0];
            parentRef.operation = "update";
            selItem = selectedItems[0];
            // adlAdd("update");
            // getAjaxObject(ipAddress+"/master/type/list/?name=component","GET",getComponentTypes,onError);
            setDataForSelection(compType, "cmbType", onCompTypeChange, ["Key", "Value"], 0, "");
            setDataForSelection(typeArr, "txtID", onTYpeChange, ["Key", "Value"], 0, "");
            componentId = selItem.idk;
            $("#txtAddID").html("ID :"+componentId);
            $("#txtActivityName").val(selItem.activity);
            $("#txtDisplayName").val(selItem.reportDisplayName);
            $("#txtAbbrevation").val(selItem.abbr);
            $("#taDesc").val(selItem.description);
            $("#txtDuration").val(selItem.duration);
            $("#txtCharge").val(selItem.charge);
            $("#txtDisplayOrder").val(selItem.displayOrder);
            IsThumbnailPresent = selItem.thumbnailPresent;

            if(selItem.remarksRequired == 1){
                $("#chkRemarks").prop("checked",true);
            }
            else{
                $("#chkRemarks").prop("checked",false);
            }
            if(selItem.camera == 1){
                $("#chkCamera").prop("checked",true);
            }
            else{
                $("#chkCamera").prop("checked",false);
            }

            onShowImage();
            // if (componentId != "") {
            //     var imageServletUrl = ipAddress + "/homecare/download/components/thumbnail/?id=" + componentId + "&access_token=" + sessionStorage.access_token + "&tenant=" + sessionStorage.tenant;
            //     $("#imgPhoto").attr("src", imageServletUrl);
            // }


            $("#cmbStatus").val(selItem.isActive);
            var txtID = $("#txtID").data("kendoComboBox");
            if(txtID){

                var aid = getActivityNameById(selItem.activityTypeId);
                txtID.value(selItem.activityTypeId);
            }
            var cmbType = $("#cmbType").data("kendoComboBox");
            if(cmbType){
                var aid = getTypeNameById(selItem.componentId);
                cmbType.value(selItem.componentId);
            }

            onCompTypeChange();
            onTYpeChange();
        }
    debugger;
        $("#dgridActivTypeListgrid").css("height","260px");
        $("#dgridActivTypeListID").css("height","260px");
        $("#divTaskComponent").css("height","430px !important");

    })
}
function onClickDelete(){
    customAlert.confirm("Confirm", "Are you sure you want delete?",function(response){
        if(response.button == "Yes"){
            var selectedItems = angularUIgridWrapper.getSelectedRows();
            console.log(selectedItems);
            if(selectedItems && selectedItems.length>0){
                var selItem = selectedItems[0];
                if(selItem){
                    var dataUrl = ipAddress+"/activity/delete/";
                    var reqObj = {};
                    reqObj.id = selItem.idk;
                    reqObj.abbr = selItem.abbr;
                    reqObj.country = selItem.county;
                    reqObj.code = selItem.code;
                    reqObj.isDeleted = "1";
                    reqObj.isActive = "0";
                    reqObj.modifiedBy = sessionStorage.userId;//101
                    createAjaxObject(dataUrl,reqObj,"POST",onDeleteCountryt,onError);
                }
            }
        }
    });
}
function onDeleteCountryt(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            customAlert.info("info", "Task Component Deleted Successfully");
            showGrid();
        }else{
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}
function onClickAdd(){
    // isBrowseFlag = false;
    // $("#btnRemove").hide();
    // $("#addTaskGroupPopup").show();
    // $('#viewDivBlock').hide();
    // $('.taskComponentLeft').hide();
    // $("#txtAddID").hide();
    // $(".filter-heading").html("Add Task Component");
    // parentRef.selitem = null;
    deletedReportColumns = [];
    operation = ADD;
    $("#divTaskComponentsAdd").show();
    $("#divTaskComponentsUpdate").hide();
    $('#taskReportInner').removeClass('taskReportInnerActive');
    $('#taskReportInnerBlock').removeClass('taskReportBlockActive');

    $("#txtTaskReportColumnName").val("");
    $("#txtTaskReportColumnName2").val("");

    $("#divTaskComponents").show();
    $("#divTaskReports").hide();

    bindTheGrid();

    getAjaxObject(ipAddress+"/master/type/list/?name=activity","GET",getActivityTypes1,onError);
    //init();


    //$('.btnActive').trigger('click');
    //searchOnLoad('active');


}

function bindTaskReportColumns(activityTypeId){
    // $("#ulTaskReportColumns").empty();
    // $("#ulTaskComponents").empty();

    var filterArray = [];
    if (masterTaskReports != null && masterTaskReports.length > 0) {
        if (activityTypeId != null) {
            if (operation == UPDATE) {
                var reportName = reportDisplayName;
                filterArray = _.where(masterTaskReports, { activityTypeId: activityTypeId });
                filterArray = _.where(filterArray, { name: reportName });
            }

        }
    }

    if (filterArray !== null && filterArray.length > 0 && masterTaskGroups !== null && masterTaskGroups.length > 0) {
        for(var c = 0; c < filterArray.length ; c++){

            var tmpTaskGroup = _.where(masterTaskGroups, {idk: filterArray[c].activityTypeId});

            if (tmpTaskGroup !== null && tmpTaskGroup.length > 0) {
                filterArray[c].taskGroupName = tmpTaskGroup[0].reportDisplayName;
            }

        }
    }


    return filterArray;
}


function bindTaskReportColumns2(activityTypeId){
    // $("#ulTaskReportColumns").empty();
    // $("#ulTaskComponents").empty();

    var filterArray = [];
    if (masterTaskReports != null && masterTaskReports.length > 0) {
        //if (activityTypeId != null) {
            if (operation == UPDATE) {
                var reportName = reportDisplayName;
                //filterArray = _.where(masterTaskReports, { activityTypeId: activityTypeId });
                filterArray = _.where(masterTaskReports, { name: reportName });
            }

        //}
    }

    if (filterArray !== null && filterArray.length > 0 && masterTaskGroups !== null && masterTaskGroups.length > 0) {
        for(var c = 0; c < filterArray.length ; c++){

            var tmpTaskGroup = _.where(masterTaskGroups, {idk: filterArray[c].activityTypeId});

            if (tmpTaskGroup !== null && tmpTaskGroup.length > 0) {
                filterArray[c].taskGroupName = tmpTaskGroup[0].reportDisplayName;
            }

        }
    }


    return filterArray;
}

function adlAdd(operation){
    var popW = 800;
    var popH = 400;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    if(operation == "update"){
        profileLbl = "Edit Task Component";
    }
    else {
        profileLbl = "Add Task Component";
    }
    parentRef.operation = operation;//"add";
    devModelWindowWrapper.openPageWindow("../../html/masters/createADL.html", profileLbl, popW, popH, true, closeAddFacilitytAction);
}

function closeAddFacilitytAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        if(returnData.operation == "add"){
            customAlert.info("info", "Task Component Saved Successfully");
        }else{
            customAlert.info("info", "Task Component Updated Successfully");
        }
        // alert("a");
        onLoaded();
        // $(".btnActive").click();

    }
}

function onClickCancel(){
    $("#divTaskComponents").hide();
    $("#divTaskReports").show();
    deletedReportColumns = [];

}

var allRosterRowCount = 0;
var allRosterRowIndex = 0;
var allRosterRows = [];

function onClickSaveDO(){
    allRosterRowCount = 0;
    allRosterRowIndex = 0;
    allRosterRows = angularUIgridWrapper.getAllRows();
    allRosterRowCount = allRosterRows.length;
    if(allRosterRowCount>0){
        createDisplayOrder();
    }

}
function createDisplayOrder(){
    if(allRosterRowCount>allRosterRowIndex){
        var objRosterEntiry = allRosterRows[allRosterRowIndex];
        var rosterObj = objRosterEntiry.entity;
        var dataObj = {};
        dataObj.modifiedBy = Number(sessionStorage.userId);
        dataObj.isDeleted = 0;
        dataObj.isActive = 1;
        dataObj.activity = rosterObj.activity;
        dataObj.description = rosterObj.description;
        dataObj.duration = rosterObj.duration;
        dataObj.activityTypeId = rosterObj.activityTypeId;
        dataObj.componentId = rosterObj.componentId;
        dataObj.charge = rosterObj.charge;
        dataObj.displayOrder = rosterObj.displayOrder;
        dataObj.camera = rosterObj.camera;
        dataObj.thumbnailPresent = rosterObj.thumbnailPresent;
        dataObj.remarksRequired = rosterObj.remarksRequired;
        dataObj.id = rosterObj.idk;
        var dataUrl = ipAddress+"/activity/update";

        createAjaxObject(dataUrl,dataObj,"POST",onCreateDS,onError);
    }
}

function onCreateDS(dataObj){
    allRosterRowIndex = allRosterRowIndex+1;
    if(allRosterRowCount == allRosterRowIndex){
        customAlert.error("Info", "Display order updated successfully");
        // init();
        $(".btnActive").click();
    }else{
        createDisplayOrder();
    }
}

function getMax(arr, prop) {
    var max;
    var j=-1;
    for (var i=0 ; i<arr.length ; i++) {
        if (!max || parseInt(arr[i][prop]) > parseInt(max[prop]))
            max = arr[i];
    }
    j++;
    return max;
}
function buildDeviceListGrid1(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Task Groups",
        "field": "reportDisplayName",
        "width":"100%"
    });
    angularUIgridWrapper1.creategrid(dataSource, gridColumns,otoptions);
    adjustHeightGroup();
    //onIndividualRowClick();
}

function buildTaskReportsListGrid(dataSource) {

    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Task Report",
        "field": "name",
        "width":"100%"
    });
    angularUIgridWrapper2.creategrid(dataSource, gridColumns,otoptions);

    adjustHeight();

}
function bindTheGrid(){
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    // angularUIgridWrapper = new AngularUIGridWrapper("dgridADLList", dataOptions);
    // angularUIgridWrapper.init();

    var dataOptions1 = {
        pagination: false,
        changeCallBack: onChange1
    }
    angularUIgridWrapper1 = new AngularUIGridWrapper("dgridActivTypeList", dataOptions1);
    angularUIgridWrapper1.init();

    //buildStateListGrid([]);
    buildDeviceListGrid1([]);
}


function bindTheComboBox(activityTypeId) {
    getAjaxObject(ipAddress + "/homecare/task-reports/?is-active=1&is-deleted=0&activity-type-id=" + activityTypeId + "&fields=id,name", "GET", getTheComboBox, onError);
}

function getTheComboBox(dataObj) {
    $("#cmbTaskReportComponents").empty();
    var dataArray = [];
    if (dataObj) {
        if ($.isArray(dataObj.response.taskReports)) {
            dataArray = dataObj.response.taskReports;
        } else {
            dataArray.push(dataObj.response.taskReports);
        }
    }
    var tempDataArry = [];
    for (var i = 0; i < dataArray.length; i++) {
        dataArray[i].idk = dataArray[i].id;

        $("#cmbTaskReportComponents").append('<option value="' + dataArray[i].idk + '">' + dataArray[i].name + '</option>');
    }
};


function bindTheTaskReportsGrid(){
    var dataOptions = {
        pagination: false,
        changeCallBack: onTaskChange
    }

    angularUIgridWrapper2 = new AngularUIGridWrapper("dgridTaskReports", dataOptions);
    angularUIgridWrapper2.init();

    buildTaskReportsListGrid([]);
}

function onTaskChange(){
setTimeout(function(){
    var selectedItems = angularUIgridWrapper2.getSelectedRows();
    console.log(selectedItems);
    if(selectedItems && selectedItems.length>0){
        setTaskGroupItem = selectedItems[0];
        $("#btnEditTaskReport").prop("disabled", false);
        $("#btnDeleteTaskReport").prop("disabled", false);
    }else{
        $("#btnEditTaskReport").prop("disabled", true);
        $("#btnDeleteTaskReport").prop("disabled", true);
    }
},100)
    $("#btnEditTaskReport").prop("disabled", false);
    $("#btnDeleteTaskReport").prop("disabled", false);
}

function onClickAddTasks(){
    var popW = "60%"
    var popH = 520;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Appointment Reason Task List";
    devModelWindowWrapper.openPageWindow("../../html/masters/attachLinkTasks.html", profileLbl, popW, popH, true, closeTasksList);
}

function closeTasksList(evt,returnData){
    if(returnData && returnData.status == "success"){
        customAlert.info("info", "Appointment Reason Updated Successfully");
    }
}

function themesChange(themeValue){
    if(themeValue == 2){
        loadAPi("../../Theme2/Theme02.css");
    }
    else if(themeValue == 3){
        loadAPi("../../Theme3/Theme03.css");
    }
}

function themeAPIChange(){
    //getAjaxObject(ipAddress+"/homecare/settings/?id=2","GET",getThemeValue,onError);
}

function getThemeValue(dataObj){
    console.log(dataObj);
    var tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.activityTypes){
            if($.isArray(dataObj.response.settings)){
                tempCompType = dataObj.response.settings;
            }else{
                tempCompType.push(dataObj.response.settings);
            }
        }
        if(tempCompType.length > 0){
            themesChange(tempCompType[0].value);
        }
    }
}


function validation(){
    var flag = true;
    var strName = $("#txtActivityName").val();
    strName = $.trim(strName);
    if(strName == ""){
        customAlert.error("Error","Enter task name");
        flag = false;
        return false;
    }
    var txtID = $("#txtID").data("kendoComboBox");
    if(txtID){
        var strVal = txtID.value();
        if(strVal == '1' || strVal == '2' ){
            var txtDuration = $("#txtDuration").val();
            txtDuration = $.trim(txtDuration);
            if(txtDuration == ""){
                customAlert.error("Error","Enter duration");
                flag = false;
                return false;
            }
            var txtCharge = $("#txtCharge").val();
            txtCharge = $.trim(txtCharge);
            if(txtCharge == ""){
                customAlert.error("Error","Enter charge");
                flag = false;
                return false;
            }
        }
    }
    return flag;
}

function onClickReset() {
    if(operation == ADD) {
        operation = ADD;
    }
    else {
        operation = UPDATE;
    }
    $("#txtActivityName").val("");
    $("#txtDisplayName").val("");

    var txtID = $("#txtID").data("kendoComboBox");
    if(txtID){
        txtID.select(0);
    }

    $("#txtDuration").val("0");
    $("#txtCharge").val("0");
    $("#taDesc").val("");
    $("#cmbStatus").val(1);
    var cmbType = $("#cmbType").data("kendoComboBox");
    if(cmbType){
        cmbType.select(0);
    }

    $("#imgPhoto").attr("src", "");
}


var actComponents;
function getStateList1(dataObj){
    debugger;
    console.log(dataObj);
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.activity){
        if($.isArray(dataObj.response.activity)){
            dataArray = dataObj.response.activity;
        }else{
            dataArray.push(dataObj.response.activity);
        }

    }
    var tempDataArry = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].displayOrder1 = dataArray[i].displayOrder;
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
        }

        dataArray[i].activityID = getActivityNameById(dataArray[i].activityTypeId);
        dataArray[i].Type = getTypeNameById(dataArray[i].componentId);
    }
    actComponents = dataArray;
    //buildStateListGrid(dataArray);
}

function onSelectionFiles(event) {
    var imageCompressor = new ImageCompressor();
    console.log(event);
    files = event.target.files;
    fileName = "";
    //$('#txtFU').val("");
    if (files) {
        if (files.length > 0) {
            fileName = files[0].name;
            isBrowseFlag = true;
            // if (patientId != "") {
            var oFReader = new FileReader();
            oFReader.readAsDataURL(files[0]);
            oFReader.onload = function(oFREvent) {
                document.getElementById("imgPhoto").src = oFREvent.target.result;
                if (imageCompressor) {
                    imageCompressor.run(oFREvent.target.result, compressorSettings, function(small) {
                        document.getElementById("imgPhoto").src = small;
                        //isBrowseFlag = false;
                        imagePhotoData = small;
                        return small;
                    });
                }

            }
            // }
        }
    }
}

function onClickBrowse(e) {
    removeSelectedButtons();
    $("#btnBrowse").addClass("selClass");
    // $("#imgPhoto").attr("src", "");
    // isBrowseFlag = true;

    // if(IsRemove == 1){
    //     $("#imgPhoto").attr("src","");
    // }
    $("#imgPhoto").show();
    console.log(e);
    if (e.currentTarget && e.currentTarget.id) {
        var btnId = e.currentTarget.id;
        var fileTagId = ("fileElem" + btnId);
        var cmbType = $("#cmbType").data("kendoComboBox");
        if(cmbType.text().toLowerCase() == "diagram"){
            IsThumbnailPresent = 0;
        }else{
            IsThumbnailPresent = 1;
        }
        $("#fileElem").click();
    }
}

var IsRemove =0;
function removeSelectedButtons() {
    $("#btnBrowse").removeClass("selClass");
}

function onClickBrowseOver() {
    removeOverSelection();
    $("#btnBrowse").addClass("borderClass");
}

function onClickBrowseOut() {
    removeOverSelection();
}

function removeOverSelection() {
    $("#btnBrowse").removeClass("borderClass");
}

function onClickUploadPhoto() {
    if (componentId != "") {
        removeSelectedButtons();
        $("#btnUpload").addClass("selClass");
        if (isBrowseFlag) {
            //imagePhotoData
            var reqUrl;

            var cmbType = $("#cmbType").data("kendoComboBox");
            if(cmbType.text().toLowerCase() == "diagram") {
                reqUrl = ipAddress + "/homecare/upload/activities/";
            }
            else{
                reqUrl = ipAddress + "/homecare/upload/activities/thumbnail/";
            }

            reqUrl = reqUrl+"?access_token="+sessionStorage.access_token + "&id="+componentId;

            // var reqUrl = ipAddress + "/upload/patient/photo/stream/?patient-id=" + patientId + "&photo-ext=png&access_token=" + sessionStorage.access_token + "&tenant=" + sessionStorage.tenant
            // var xmlhttp = new XMLHttpRequest();
            // xmlhttp.open("POST", reqUrl, true);
            // //xmlhttp.open("POST", "/ROOT/upload/patient/photo/?patient-id=101&photo-ext=png", true);
            // xmlhttp.send(imagePhotoData);
            // xmlhttp.onreadystatechange = function() { //Call a function when the state changes.
            //     if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            //         customAlert.error("Info", "Image uploaded successfully");
            //     }
            // }


            var tagFileId = ("fileElem");
            var file = document.getElementById(tagFileId);

            if(file && file.files[0] && file.files[0].name){
                fileName = file.files[0].name;
            }

            var form = new FormData();
            if(operation != UPDATE){
                form.append("file",file.files[0]);
            }else{
                form.append("file",file.files[0]);
            }


            // var settings = {
            //     "async": true,
            //     "crossDomain": true,
            //     "url": reqUrl,
            //     "method": "POST",
            //     "processData": false,
            //     "contentType": false,
            //     "data": form,
            //     "headers": {
            //         "tenant": sessionStorage.tenant,
            //         "contentType": "multipart/form-data",
            //         "cache-control": "no-cache",
            //     },
            // }
            //
            // $.ajax(settings).done(function (response) {
            //     console.log(response);
            // });

            Loader.showLoader();
            $.ajax({
                url: reqUrl,
                type: 'POST',
                data: form,
                processData: false,
                contentType:false,// "multipart/form-data",
                // contentType: "application/json",
                headers: {
                    tenant: sessionStorage.tenant
                },
                success: function(data, textStatus, jqXHR){
                    if(typeof data.error === 'undefined'){
                        Loader.hideLoader();
                        onSuccess(data);
                    }else{
                        Loader.hideLoader();
                    }
                },
                error: function(jqXHR, textStatus, errorThrown){
                    Loader.hideLoader();
                }
            });

        }
    }
}


function onSuccess(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg;
            if(operation == ADD){
                msg = "Task Component Saved Successfully";
            }else{
                msg = "Task Component Updated Successfully";
            }
            var obj = {};
            obj.status = "success";
            obj.operation = operation;
            // displaySessionErrorPopUp("Info", msg, function(res) {

            // onLoaded();
            // onChange1();
            // buildStateListGrid([]);
            // buildDeviceListGrid1([]);
            //getAjaxObject(ipAddress + "/activity/list?is-active=1&is-deleted=0&sort=activity", "GET", function(dataObj) {

            //     if (dataObj && dataObj.response && dataObj.response.activity) {
            //         if ($.isArray(dataObj.response.activity)) {
            //             dataArray = dataObj.response.activity;
            //         } else {
            //             dataArray.push(dataObj.response.activity);
            //         }
            //
            //         var maxDisOrder = getMax(dataObj.response.activity, "displayOrder");
            //         var displayOrderMaxlength = maxDisOrder.displayOrder;
            //
            //         sessionStorage.setItem("displayOrderMaxlength", (displayOrderMaxlength + 1));
            //     }
            //     var tempDataArry = [];
            //     for (var i = 0; i < dataArray.length; i++) {
            //         dataArray[i].idk = dataArray[i].id;
            //         dataArray[i].displayOrder1 = dataArray[i].displayOrder;
            //         dataArray[i].Status = "InActive";
            //         if (dataArray[i].isActive == 1) {
            //             dataArray[i].Status = "Active";
            //         }
            //         dataArray[i].activityID = getActivityNameById(dataArray[i].activityTypeId);
            //         dataArray[i].Type = getTypeNameById(dataArray[i].componentId);
            //
            //         if (dataArray[i].Type.toLowerCase() == "diagram") {
            //             dataArray[i].photo = ipAddress + "/homecare/download/activities/?" + Math.round(Math.random() * 1000000) +"&access_token=" + sessionStorage.access_token + "&id=" + dataArray[i].id + "&tenant=" + sessionStorage.tenant;
            //         }
            //         else {
            //             if (dataArray[i].thumbnailPresent == "1") {
            //                 dataArray[i].photo = ipAddress + "/homecare/download/activities/thumbnail/?" + Math.round(Math.random() * 1000000) +"&access_token=" + sessionStorage.access_token + "&id=" + dataArray[i].id + "&tenant=" + sessionStorage.tenant;
            //             }
            //         }
            //     }
            //     actComponents = dataArray;
            // }, function() {})
            //
            //
            //     // bindTheGrid();
            //
            //
            //
            //     // $(".btnActive").click();
            //     // onClickActive();
            //
            //
            // }, function() {});

            // buildStateListGrid([]);
            // buildDeviceListGrid1([]);

            // onLoaded();
            // var isRun = 0;
            // showGrid();
            // angularUIgridWrapper1.refreshGrid();
            // angularUIgridWrapper.refreshGrid();


            // setInterval(function(){
            //     if(isRun==0) {
            //         isRun=1;
            displaySessionErrorPopUp("Info", msg, function(res) {
                // operation = ADD;
                // onClickCancel();
                // onLoaded();
                // onChange1();

                // onClickReset();
                // buildStateListGrid([]);
                showGrid();
                onChange1();
                angularUIgridWrapper1.refreshGrid();
                angularUIgridWrapper.refreshGrid();
            });

            // }
            // },1000);
            // onClickCancel();

            // })
        }else{
            customAlert.error("Error", dataObj.response.status.message);
        }
    }else{
        console.log("hh");l
    }
    // onClickReset();
}

function onShowImage() {
    $("#btnRemove").hide();
    var imageServletUrl;
    if (selItem) {
        var aid = getTypeNameById(selItem.componentId);
        if(aid.toLowerCase() == "diagram" ){
            imageServletUrl = ipAddress+"/homecare/download/activities/?"+ Math.round(Math.random() * 1000000) +"&access_token="+sessionStorage.access_token+"&id="+selItem.idk+"&tenant="+sessionStorage.tenant;
        }
        else{
            if(selItem.thumbnailPresent == 1){
                imageServletUrl = ipAddress+"/homecare/download/activities/thumbnail/?"+ Math.round(Math.random() * 1000000) +"&access_token="+sessionStorage.access_token+"&id="+selItem.idk+"&tenant="+sessionStorage.tenant;
                $("#btnRemove").show();
            }
        }

        // $("#imgPhoto").attr("src", imageServletUrl);
        document.getElementById("imgPhoto").src =imageServletUrl;
    } else {
        $("#imgPhoto").attr("src", "../../img/AppImg/HosImages/blank.png");
    }
}

function onClickSave(){
    if(validation()) {
        // buildStateListGrid([]);
        var strName = $("#txtActivityName").val();
        strName = $.trim(strName);
        var txtID = $("#txtID").data("kendoComboBox");
        var cmbType = $("#cmbType").data("kendoComboBox");
        var taDesc = $("#taDesc").val();
        taDesc = $.trim(taDesc);
        var txtDuration = $("#txtDuration").val();
        txtDuration = $.trim(txtDuration);

        var txtCharge = $("#txtCharge").val();

        var isActive =$("#cmbStatus").val();

        var IsRemarksChecked = 0;
        var IsCameraChecked = 0;

        if ($("#chkRemarks").is(':checked')) {
            IsRemarksChecked = 1;
        }

        if ($("#chkCamera").is(':checked')) {
            IsCameraChecked = 1;
        }


        var dataObj = {};
        dataObj.activity = strName;
        dataObj.description = taDesc;
        dataObj.duration = Number(txtDuration);
        dataObj.activityTypeId = Number(txtID.value());
        dataObj.componentId = Number(cmbType.value());
        dataObj.charge = Number(txtCharge);
        dataObj.displayOrder = $("#txtDisplayOrder").val();
        dataObj.remarksRequired = IsRemarksChecked;
        dataObj.camera = IsCameraChecked;
        dataObj.reportDisplayName = $("#txtDisplayName").val();

        dataObj.isActive = isActive;
        dataObj.createdBy = Number(sessionStorage.userId);//"101";//sessionStorage.uName;
        if(isActive == 1){
            dataObj.isDeleted = 0;
        }else{
            dataObj.isDeleted = 1;
        }

        dataObj.thumbnailPresent = IsThumbnailPresent;

        if (operation == ADD) {
            var dataUrl = ipAddress + "/activity/create";
            createAjaxObject(dataUrl, dataObj, "POST", onCreate, onError);
        } else {
            dataObj.id = selItem.idk;
            var dataUrl = ipAddress + "/activity/update";
            createAjaxObject(dataUrl, dataObj, "POST", onCreate, onError);
        }
    }
}

function onCreate(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            var cmbType = $("#cmbType").data("kendoComboBox");
            if(operation == ADD) {
                if(dataObj.response.activity){
                    componentId = dataObj.response.activity.id;
                }
                if (cmbType.text().toLowerCase() == "diagram") {
                    if (isBrowseFlag) {
                        var tagFileId = ("fileElem");
                        var file = document.getElementById(tagFileId);

                        if (file && file.files[0] && file.files[0].name) {
                            fileName = file.files[0].name;
                        }

                        onClickUploadPhoto();
                    }
                    else {
                        customAlert.error("error", "Upload Image");
                    }
                }
                else{
                    if (isBrowseFlag) {
                        onClickUploadPhoto();
                    }
                    else {
                        onSuccess(dataObj);
                    }
                }
            }
            else{
                if (isBrowseFlag) {
                    onClickUploadPhoto();
                }
                else {
                    onSuccess(dataObj);
                }
            }
        }else{
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}



function onRemove(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg;
            msg = "Image Removed Successfully";
            var obj = {};
            obj.status = "success";
            obj.operation = operation;
            displaySessionErrorPopUp("Info", msg, function(res) {
                $("#imgPhoto").attr("src", "");
                $("#fileElem").val("");
                var tagFileId = ("fileElem");
                var file = document.getElementById(tagFileId);
                if(file && file.files[0] && file.files[0].name){
                    fileName = null;
                    file.files[0].name = null;
                    file.files[0] = null;
                }

                $("#btnRemove").hide();
                imagePhotoData = null;
                files = null;
                fileName = "";
                IsRemove = 1;
                // location.reload();
                $("#imgFieldSet").trigger("update");
                buildStateListGrid([]);
                showGrid();
                onChange1();
                angularUIgridWrapper1.refreshGrid();
                angularUIgridWrapper.refreshGrid();
                // operation = ADD;
                // onLoaded();
                // onChange1();
                // showGrid();
                // onClickReset();
                // onClickCancel();
                // $(".btnActive").click();
                // onChange1();
            })
        }else{
            customAlert.error("Error", dataObj.response.status.message);
        }
    }else{

    }
    // onClickReset();
}


function bindTaskComponents(dataSource){
    $("#ulTaskComponents").empty();
    for (var counter = 0; counter < dataSource.length; counter++) {
        var id = "taskComponent-" + counter;
        var $item = $('<li id="' + id + '" class="list-group-item" componentid="'+dataSource[counter].componentId+'" activitytypeid="'+dataSource[counter].activityTypeId+'" reportDisplayName="'+dataSource[counter].reportDisplayName+'" displayorder="0">'+dataSource[counter].activity+'</li>');
        $item.droppable({
            accept: '#divTaskReportColumnsDroppable .ui-draggable',
            drop: function (e, ui) {
                $(ui.draggable).appendTo("#ulTaskComponents");
                resetDisplayOrder();
            }
        });
        $item.draggable({
            helper: "clone"
        });
        $item.appendTo('#ulTaskComponents');


    }

    var $item = $('#divTaskReportColumnsDroppable');
    $item.droppable({
        accept: '#divTaskComponentsDraggable .ui-draggable',
        drop: function (e, ui) {
            $(ui.draggable).appendTo("#ulTaskReportColumns");
            resetDisplayOrder();
        }
    });


};

function bindTaskComponentsAndColumns(unselectedComponents, taskReportColumns){
    $("#ulTaskComponents").empty();

    // if(unselectedComponents != null && unselectedComponents.length > 0){
    //     for (var counter = 0; counter < unselectedComponents.length; counter++) {
    //         var id = "taskComponent-" + counter;
    //         var $item = $('<li id="' + id + '" class="list-group-item draggable-item" componentid="'+unselectedComponents[counter].idk+'" activitytypeid="'+unselectedComponents[counter].activityTypeId+'" reportdisplayname="'+unselectedComponents[counter].reportDisplayName+'" taskgroupname="'+unselectedComponents[counter].taskGroupName+'" componentname="'+unselectedComponents[counter].activity+'" displayorder="0">'+unselectedComponents[counter].reportDisplayName+'</li>');
    //         $item.appendTo('#ulTaskComponents');
    //     }
    // }

    //$("#ulTaskReportColumns").empty();

    console.log("taskReportColumns");
    console.log(taskReportColumns);

    if(taskReportColumns != null && taskReportColumns.length > 0){

        for (var counter = 0; counter < taskReportColumns.length; counter++) {
            var id = "taskReport-" + counter;
            var $item = $('<li id="' + id + '" class="list-group-item" taskreportid="'+taskReportColumns[counter].id+'" componentid="'+taskReportColumns[counter].activityId+'" activitytypeid="'+taskReportColumns[counter].activityTypeId+'" taskgroupname="'+taskReportColumns[counter].taskGroupName+'" componentname="'+taskReportColumns[counter].activityName+'" reportdisplayname="'+taskReportColumns[counter].reportDisplayName+'" displayorder="'+taskReportColumns[0].displayOrder+'" createdby="'+taskReportColumns[0].createdBy+'"><span style="float: left;">'+taskReportColumns[counter].taskGroupName+'</span> - <span style="float: right;">'+taskReportColumns[counter].activityName+'<img src="../../img/remove.png" class="img-close" title="Remove"/></span></li>');
            $item.appendTo('#ulTaskReportColumns');
        }
    }

    initDragAndDrop();
}



function onClickSaveReportColumns() {

    var $selectedColumns = $("#ulTaskReportColumns li");

    var isValid = true;
    var errorMessage = "";

    if ($selectedColumns.length == 0) {
        isValid = false;
        errorMessage = "Please add atleast one task report column.";
    }

    if (isValid) {
        if ($selectedColumns != null && $selectedColumns.length > taskReportColumnLimit) {
            isValid = false;
            errorMessage = "Please add only " + taskReportColumnLimit + " task report columns.";
        }
    }

    if (isValid) {
        if (operation === ADD) {
            if ($("#txtTaskReportColumnName").val().length === 0) {
                isValid = false;
                errorMessage = "Please enter report display name.";
            }
        }
    }

    if (isValid) {
        var dataObj = [], dataObj2 = [];

        for (var counter = 0; counter < $selectedColumns.length; counter++) {

            var objTaskColumn={};

          var attr = $($selectedColumns[counter]).attr('taskreportid');

          if (typeof attr !== typeof undefined && attr !== false) {
              objTaskColumn.id = Number($($selectedColumns[counter]).attr("taskreportid"));
              objTaskColumn.modifiedBy = Number(sessionStorage.userId);
              //objTaskColumn.createdBy=Number($($selectedColumns[counter]).attr("createdby"))
          }
          else{
              objTaskColumn.createdBy=Number(sessionStorage.userId);
          }

          objTaskColumn.activityID=Number($($selectedColumns[counter]).attr("componentid"));
          objTaskColumn.isDeleted=0;

          objTaskColumn.displayOrder=counter+1;
          //objTaskColumn.name = $($selectedColumns[counter]).attr("reportdisplayname");
          if(operation == UPDATE) {
              objTaskColumn.name = $("#txtTaskReportColumnName2").val();
          }
          else{
              objTaskColumn.name = $("#txtTaskReportColumnName").val();
          }
          objTaskColumn.isActive=1;
          objTaskColumn.activityTypeId=Number($($selectedColumns[counter]).attr("activitytypeid"));

          dataObj.push(objTaskColumn);
      }


      var  deletedTaskReportColumns=[], componentIds=[];


      if (deletedReportColumns !== null && deletedReportColumns.length > 0) {
        for(var counter2=0;counter2 < deletedReportColumns.length;counter2++){
            dataObj.push(deletedReportColumns[counter2]);
        }
      }

    //   if(tmpReportColumns != null && tmpReportColumns.length > 0 && dataObj != null && dataObj.length > 0){

    //     $.each(dataObj,function(index,item){
    //         componentIds.push(item.activityID);
    //     })

    //     deletedTaskReportColumns = tmpReportColumns.filter(function(item){
    //         return componentIds.indexOf(item.activityId) === -1;
    //     });

    //     if(deletedTaskReportColumns != null && deletedTaskReportColumns.length > 0){
    //         for(var counter2=0;counter2 < deletedTaskReportColumns.length;counter2++){


    //             var objTaskColumn={};

    //             objTaskColumn.id = deletedTaskReportColumns[counter2].id;
    //             objTaskColumn.modifiedBy = Number(sessionStorage.userId);
    //             objTaskColumn.createdBy=deletedTaskReportColumns[counter2].createdBy;
    //             objTaskColumn.activityID=deletedTaskReportColumns[counter2].activityId;
    //             objTaskColumn.isDeleted=1;
    //             objTaskColumn.displayOrder=deletedTaskReportColumns[counter2].displayOrder;
    //             objTaskColumn.name=deletedTaskReportColumns[counter2].name;
    //             objTaskColumn.isActive=0;
    //             objTaskColumn.activityTypeId=deletedTaskReportColumns[counter2].activityTypeId;

    //             dataObj.push(objTaskColumn);
    //         }
    //     }
    // }



    var dataUrl = ipAddress + "/homecare/task-reports/batch/";
    console.log("reports data");
    console.log(JSON.stringify(dataObj));
    createAjaxObject(dataUrl, dataObj, "PUT", onReportColumnsSaveSCB, onReportColumnsSaveECB);

  }
  else{
      // alert(errorMessage);
      customAlert.error("error",errorMessage);
  }

};

function onReportColumnsSaveSCB(response) {
    // console.log("SUCCESS");
    // alert("TaskRreports created/updated/deleted successfully.");
    customAlert.info("info","TaskReports created/updated successfully");
    // console.log(JSON.stringify(response));
    loadTaskReportsGrid();
}

function onReportColumnsSaveECB(response) {
    // console.log("ERROR");
    // console.log(JSON.stringify(response));
}

function resetDisplayOrder(){
    var $reportColumns=$("#ulTaskReportColumns li");

    if ($reportColumns != null && $reportColumns.length > 0){
        for (var counter=0;counter < $reportColumns.length;counter++){
            $($reportColumns[counter]).prop("displayorder",(counter+1)+"");
        }
    }
}


function test(activityTypeId){

    var filterArray = [];
    filterArray = _.where(actComponents, {activityTypeId: activityTypeId});


    if (filterArray !== null && filterArray.length > 0 && masterTaskGroups !== null && masterTaskGroups.length > 0) {
        for(var c = 0; c < filterArray.length ; c++){

            var tmpTaskGroup = _.where(masterTaskGroups, {idk: filterArray[c].activityTypeId});

            if (tmpTaskGroup !== null && tmpTaskGroup.length > 0) {
                filterArray[c].taskGroupName = tmpTaskGroup[0].reportDisplayName;
            }

        }
    }

    var tmpTaskReportColumns = bindTaskReportColumns(reportDisplayName);
    tmpReportColumns = tmpTaskReportColumns;

    var activityIds = [];
    if(operation == UPDATE) {
        //bindTheComboBox(activityTypeId);
    }

    if (tmpTaskReportColumns != null && tmpTaskReportColumns.length > 0){

        tmpTaskReportColumns.sort(sortByDisplayOrder);

        if (tmpSelectedComponents === null || (tmpSelectedComponents !== null  && tmpSelectedComponents.length === 0)) {
            tmpSelectedComponents = tmpTaskReportColumns;

            for(var c = 0; c < tmpSelectedComponents.length ; c++){
                tmpSelectedComponents[c].taskReportColumnId = tmpSelectedComponents[c].id;
            }

        }

        // $.each(tmpTaskReportColumns, function (index, item) {
        //     activityIds.push(item.activityId);
        // });

        $.each(tmpSelectedComponents, function (index, item) {
            activityIds.push(item.activityId);
        });

        var unselectedTaskComponents = filterArray.filter(function(item){
            return activityIds.indexOf(item.idk) === -1;
        });

        bindTaskComponentsAndColumns(unselectedTaskComponents,tmpTaskReportColumns);
    }
    else{
        bindTaskComponentsAndColumns(filterArray,[]);
    }
}


function bindTaskGroupReportColumns(){

    var tmpTaskReportColumns = bindTaskReportColumns2(reportDisplayName);
    tmpReportColumns = tmpTaskReportColumns;

    var activityIds = [];


    if (tmpTaskReportColumns != null && tmpTaskReportColumns.length > 0){

        tmpTaskReportColumns.sort(sortByDisplayOrder);

        // if (tmpSelectedComponents === null || (tmpSelectedComponents !== null  && tmpSelectedComponents.length === 0)) {
        //     tmpSelectedComponents = tmpTaskReportColumns;

        //     for(var c = 0; c < tmpSelectedComponents.length ; c++){
        //         tmpSelectedComponents[c].taskReportColumnId = tmpSelectedComponents[c].id;
        //     }

        // }

        // $.each(tmpSelectedComponents, function (index, item) {
        //     activityIds.push(item.activityId);
        // });

        // var unselectedTaskComponents = filterArray.filter(function(item){
        //     return activityIds.indexOf(item.idk) === -1;
        // });

        if(tmpTaskReportColumns != null && tmpTaskReportColumns.length > 0){

            for (var counter = 0; counter < tmpTaskReportColumns.length; counter++) {
                var id = "taskReport-" + counter;
                var $item = $('<li id="' + id + '" class="list-group-item" onclick="RemoveTaskGroupReportColumn(this)" taskreportid="'+tmpTaskReportColumns[counter].id+'" componentid="'+tmpTaskReportColumns[counter].activityId+'" activitytypeid="'+tmpTaskReportColumns[counter].activityTypeId+'" taskgroupname="'+tmpTaskReportColumns[counter].taskGroupName+'" componentname="'+tmpTaskReportColumns[counter].activityName+'" reportdisplayname="'+tmpTaskReportColumns[counter].name+'" displayorder="'+tmpTaskReportColumns[0].displayOrder+'" createdby="'+tmpTaskReportColumns[0].createdBy+'"><span style="float: left;">'+tmpTaskReportColumns[counter].taskGroupName+'</span> - <span style="float: right;">'+tmpTaskReportColumns[counter].activityName+'<img src="../../img/remove.png" class="img-close" title="Remove" /></span></li>');
                $item.appendTo('#ulTaskReportColumns');
            }
        }

        initDragAndDrop();


}
}



function bindTaskGroupComponents(activityTypeId) {

    var filterArray = [];
    filterArray = _.where(actComponents, { activityTypeId: activityTypeId });


    if (filterArray !== null && filterArray.length > 0 && masterTaskGroups !== null && masterTaskGroups.length > 0) {
        for (var c = 0; c < filterArray.length; c++) {

            var tmpTaskGroup = _.where(masterTaskGroups, { idk: filterArray[c].activityTypeId });

            if (tmpTaskGroup !== null && tmpTaskGroup.length > 0) {
                filterArray[c].taskGroupName = tmpTaskGroup[0].reportDisplayName;
            }

        }
    }

    var tmpTaskReportColumns = bindTaskReportColumns2(reportDisplayName);
    tmpReportColumns = tmpTaskReportColumns;

    var activityIds = [];


    if (tmpTaskReportColumns != null && tmpTaskReportColumns.length > 0) {

        tmpTaskReportColumns.sort(sortByDisplayOrder);

        if (tmpSelectedComponents === null || (tmpSelectedComponents !== null && tmpSelectedComponents.length === 0)) {
            tmpSelectedComponents = tmpTaskReportColumns;

            for (var c = 0; c < tmpSelectedComponents.length; c++) {
                tmpSelectedComponents[c].taskReportColumnId = tmpSelectedComponents[c].id;
            }

        }


        $.each(tmpSelectedComponents, function (index, item) {
            activityIds.push(item.activityId);
        });

        var unselectedTaskComponents = filterArray.filter(function (item) {
            return activityIds.indexOf(item.idk) === -1;
        });


        $("#ulTaskComponents").empty();

        if (unselectedTaskComponents != null && unselectedTaskComponents.length > 0) {
            for (var counter = 0; counter < unselectedTaskComponents.length; counter++) {
                var id = "taskComponent-" + counter;
                var $item = $('<li id="' + id + '" class="list-group-item draggable-item" componentid="' + unselectedTaskComponents[counter].idk + '" activitytypeid="' + unselectedTaskComponents[counter].activityTypeId + '" reportdisplayname="' + unselectedTaskComponents[counter].name + '" taskgroupname="' + unselectedTaskComponents[counter].taskGroupName + '" componentname="' + unselectedTaskComponents[counter].activity + '" displayorder="0">' + unselectedTaskComponents[counter].activity + '</li>');
                $item.appendTo('#ulTaskComponents');
            }

            initDragAndDrop();
        }

    }
    else{
        $("#ulTaskComponents").empty();

        if (filterArray != null && filterArray.length > 0) {
            for (var counter = 0; counter < filterArray.length; counter++) {
                var id = "taskComponent-" + counter;
                var $item = $('<li id="' + id + '" class="list-group-item draggable-item" componentid="' + filterArray[counter].idk + '" activitytypeid="' + filterArray[counter].activityTypeId + '" reportdisplayname="' + filterArray[counter].name + '" taskgroupname="' + filterArray[counter].taskGroupName + '" componentname="' + filterArray[counter].activity + '" displayorder="0">' + filterArray[counter].activity + '</li>');
                $item.appendTo('#ulTaskComponents');
            }

            initDragAndDrop();
        }
    }

}





function getNameByComponentId(componentId) {

    for(var i=0;i<actComponents.length;i++){
        var item = actComponents[i];
        if(item && item.idk == componentId){
            return item.activity;
        }
    }
    return "";
}

function initDragAndDrop() {
    // $( ".droppable-area1, .droppable-area2" ).sortable({
    //     connectWith: ".connected-sortable",
    //     stack: '.connected-sortable ul'
    // }).disableSelection();

    // var id = "taskReport-" + counter;
    // var $item = $('<li id="' + id + '" class="list-group-item" taskreportid="'+taskReportColumns[counter].id+'" componentid="'+taskReportColumns[counter].activityId+'" activitytypeid="'+taskReportColumns[counter].activityTypeId+'" taskgroupname="'+taskReportColumns[counter].taskGroupName+'" componentname="'+taskReportColumns[counter].componentname+'" reportdisplayname="'+taskReportColumns[counter].reportDisplayName+'" displayorder="'+taskReportColumns[0].displayOrder+'" createdby="'+taskReportColumns[0].createdBy+'">'+taskReportColumns[counter].taskGroupName+' - '+taskReportColumns[counter].reportDisplayName+'</li>');
    // $item.appendTo('#ulTaskReportColumns');

    // var id = "taskComponent-" + counter;
    // var $item = $('<li id="' + id + '" class="list-group-item draggable-item" componentid="'+unselectedComponents[counter].idk+'" activitytypeid="'+unselectedComponents[counter].activityTypeId+'" reportdisplayname="'+unselectedComponents[counter].reportDisplayName+'" taskgroupname="'+unselectedComponents[counter].taskGroupName+'" componentname="'+unselectedComponents[counter].activity+'" displayorder="0">'+unselectedComponents[counter].reportDisplayName+'</li>');
    // $item.appendTo('#ulTaskComponents');




    $("#divTaskComponentsDraggable .draggable-item").draggable({
		helper: 'clone'
	});
	$("#ulTaskReportColumns").droppable({
		accept: '.draggable-item',
        drop: function (e, ui) {

            // customAlert.confirm("Confirm", "Are you sure want to update appointment?", function (response) {
            // 	if (response.button == "Yes") {

            //alert("sdskhdk");

            // debugger;
            var activityId = parseInt($(ui.draggable).attr("componentid"));
            var activityTypeId = parseInt($(ui.draggable).attr("activitytypeid"));
            var taskGroupName = $(ui.draggable).attr("taskgroupname");
            var componentname = $(ui.draggable).attr("componentname");
            var reportdisplayname = $(ui.draggable).attr("reportdisplayname");

            //if (tmpSelectedComponents === null || (tmpSelectedComponents !== null && tmpSelectedComponents.length > 0)) {

                var dataObj = {};
                dataObj.activityId = activityId;
                dataObj.activityTypeId = activityTypeId;
                dataObj.taskGroupName = taskGroupName;
                dataObj.activityName = componentname;
                dataObj.reportDisplayName = reportdisplayname;

                tmpSelectedComponents.push(dataObj);


                var $item = $('<li class="list-group-item" componentid="' + activityId + '" activitytypeid="' + activityTypeId + '" taskgroupname="' + taskGroupName + '" componentname="' + componentname + '" reportdisplayname="' + reportdisplayname + '"><span style="float: left;">' + taskGroupName + '</span> - <span style="float: right;">' + componentname + '<img src="../../img/remove.png" class="img-close" title="Remove" onclick="RemoveTaskGroupReportColumn(this)"/></span></li>');
                $item.appendTo('#ulTaskReportColumns');




                $(ui.draggable).remove();

                checkTaskReportColumnHeaders();

            //}


        }
    });

}

function loadTaskReportsGrid() {
    $("#divTaskComponents").hide();
    $("#divTaskReports").show();
    $("#ulTaskComponents").empty();
    $("#ulTaskReportColumns").empty();

    getAjaxObject(ipAddress+"/master/type/list/?name=activity","GET",function (dataObj){
        var tempCompType = [];
        typeArr = [];

        if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
            if (dataObj.response.typeMaster) {
                if ($.isArray(dataObj.response.typeMaster)) {
                    tempCompType = dataObj.response.typeMaster;
                } else {
                    tempCompType.push(dataObj.response.typeMaster);
                }
            }
        }
        tempCompType.sort(function (a, b) {
            var nameA = a.type.toUpperCase(); // ignore upper and lowercase
            var nameB = b.type.toUpperCase(); // ignore upper and lowercase
            if (nameA < nameB) {
                return -1;
            }
            if (nameA > nameB) {
                return 1;
            }
            // names must be equal
            return 0;
        });

        for (var i = 0; i < tempCompType.length; i++) {
            var obj = {};
            obj.Key = tempCompType[i].type;
            obj.Value = tempCompType[i].id;
            typeArr.push(obj);

        }
        showGrid();
    },onError);



    // getAjaxObject(ipAddress+"/homecare/activity-types/?is-active=1&is-deleted=0&sort=type","GET",getTaskTypes2,onError);




}   // ascending order
function sortByDisplayOrder(x,y) {
    return x.displayOrder - y.displayOrder;
}


function onClickPreview(){
    var popW = 1000;
    var popH = 400;

    var tmpArray = masterTaskReports;

    for(var counter=0;counter < tmpArray.length ;counter++){

        var reportDisplayName = _.where(masterTaskGroups, {idk: tmpArray[counter].activityTypeId})[0].reportDisplayName;

        tmpArray[counter].reportDisplayName = reportDisplayName;

        sessionStorage.taskReportColumns = JSON.stringify(tmpArray);
    }



    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Task Report Preview";
    devModelWindowWrapper.openPageWindow("../../html/reports/taskReportsPreview.html", profileLbl, popW, popH, true, closeAddFacilitytAction);
}

function closeAddFacilitytAction(evt,returnData){
    // if(returnData && returnData.status == "success"){
    //     if(returnData.operation == "add"){
    //         customAlert.info("info", "Task Component Saved Successfully");
    //     }else{
    //         customAlert.info("info", "Task Component Updated Successfully");
    //     }
    //     onLoaded();
    //     // $(".btnActive").click();
    //
    // }
}


function getReportDisplayNameById(aId){
    for(var i=0;i<actComponents.length;i++){
        var item = actComponents[i];
        if(item && item.idk == aId){
            return item.reportDisplayName;
        }
    }
    return "";
}

function RemoveTaskGroupReportColumn(e){
    var $c = $(e)

    var attr = $($c).attr('taskreportid');

    if (typeof attr !== typeof undefined && attr !== false) {
        var taskreportid = parseInt($($c).attr("taskreportid"))
        var activityId = parseInt($($c).attr("componentid"));
        var activityTypeId = parseInt($($c).attr("activitytypeid"));
        var taskGroupName = $($c).attr("taskgroupname");
        var componentname = $($c).attr("componentname");
        var reportdisplayname = $($c).attr("reportdisplayname");

        //if (tmpSelectedComponents === null || (tmpSelectedComponents !== null && tmpSelectedComponents.length > 0)) {

        var dataObj = {};
        dataObj.id = taskreportid;
        dataObj.modifiedBy = Number(sessionStorage.userId);
        dataObj.activityID = activityId;
        dataObj.isDeleted = 1;
        dataObj.isActive = 0;
        dataObj.activityTypeId = activityTypeId;
        if (operation === ADD) {
            dataObj.name = $("#txtTaskReportColumnName").val();
        }
        else{
            dataObj.name = $("#txtTaskReportColumnName2").val();
        }


        deletedReportColumns.push(dataObj);
    }
    else{
        $("#ulTaskComponents").empty();
    }
    $(e).remove();

    checkTaskReportColumnHeaders();
}



function sortByReportDisplayName (a, b) {

    var reportDisplayNameA = a.name.toUpperCase();
    var reportDisplayNameB = b.name.toUpperCase();

    var comparison = 0;
    if (reportDisplayNameA > reportDisplayNameB) {
      comparison = 1;
    } else if (reportDisplayNameA < reportDisplayNameB) {
      comparison = -1;
    }
    return comparison;

}



function checkTaskReportColumnHeaders() {
    debugger;
    var $li = $("#ulTaskReportColumns li");

    if ($li !== null && $li.length > 0) {
        $("#divTaskReportColumnHeaders").css("visibility", "visible");
    }
    else {
        $("#divTaskReportColumnHeaders").css("visibility", "hidden");
    }
}
var parentRef = null;
var appReport = angular.module('appReport', []);
var IsVists = 0;

appReport.controller('reportController', function($scope) {
    // $scope.getData = function () {
    //     onClickPtViews();
    // }
    $scope.printTable = function () {
        onClickPrint();

        // var divToPrint = document.getElementById("tableAppointmentContent");
        //
        // var htmlToPrint = '' +
        //     '<style type="text/css">' +
        //     'table th, table td, table {' +
        //     'border:solid 1px #f3f6f9' +
        //     ';' +
        //     'padding;0.5em;' +
        //     'border-collapse:collapse;'+
        //     '}' +
        //     '</style>';
        // htmlToPrint += divToPrint.outerHTML;
        //
        //
        //
        // window.frames["print_frame"].document.body.innerHTML = htmlToPrint;
        // window.frames["print_frame"].window.focus();
        // window.frames["print_frame"].window.print();

    }
});


$(document).ready(function() {
    parentRef = parent.frames['iframe'].window;
	var patientActivity = {
		init: function() {
			allowNumerics('patientAppointmentId-input');
			patientActivity.bindEvents();
            $('#patientAppointmentId-link').click();
			var doc = new jsPDF();
            var specialElementHandlers = {
                '#editor': function (element, renderer) {
                    return true;
                }
            };

		},
		bindEvents: function() {
			$('#patientAppointmentId-link').on('click', function() {
				var searchAppVal = parentRef.idk;
				if(searchAppVal != "") {
					$('#activitiesPeformed tbody').empty();
					getAjaxObjectAsync(ipAddress + "/appointment/list/?is-active=1&is-deleted=0&id=" + searchAppVal,"GET",buildAppointmentList,onError);
					getAjaxObjectAsync(ipAddress + "/patient/appointment/activity/list/?is-active=1&is-deleted=0&appointment-id=" + searchAppVal,"GET",buildpatientAppointmentActivities,onError);
					getAjaxObjectAsync(ipAddress + "/patient/appointment/medication-activity/list/?is-active=1&is-deleted=0&appointment-id=" + searchAppVal,"GET",patientAppointmentMedicationActivity,onError);
				}
			});
			function buildAppointmentList(dataObj) {
				console.log(dataObj);
				var dataArray = [];
			    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1" && dataObj.response.appointment){
			        if($.isArray(dataObj.response.appointment)){
			            dataArray = dataObj.response.appointment;
			        }else{
			            dataArray.push(dataObj.response.appointment);
			        }
			    }
			    if(dataArray.length > 0) {
			    	var patientObj = dataArray[0].composition.patient ? dataArray[0].composition.patient : '';
			    	if(patientObj != "") {
			    		var handOverNotes = dataArray[0].handoverNotes;
                        var notes = dataArray[0].notes;
                        if(handOverNotes == null){
                            handOverNotes = "";
						}
                        if(notes == null || notes == "null"){
                            notes = "";
                        }
                        $('[name="patient-appointment-notes"]').text(notes);
                        $('[name="patient-appointment-handovernoes"]').text(handOverNotes );


			    		if(patientObj.dateOfBirth) {
			    			$('[name="patient-appointment-dob"]').text(patientObj.dateOfBirth);
			    			$('[name="patient-appointment-age"]').text(patientActivity._calculateAge(new Date(patientObj.dateOfBirth)) + "Y, "+patientObj.gender);
                            $('[name="patient-appointment-name"]').html("<b style='color: #0783b5;'>Service User</b> </br>" +patientObj.firstName + " "+ patientObj.middleName+ " "+ patientObj.lastName +
								"<br/>" + onGetDateTimeFormat(patientObj.dateOfBirth) + " (" +patientActivity._calculateAge(new Date(patientObj.dateOfBirth)) + "Y), "+patientObj.gender);
			    		}
			    		else{
                            $('[name="patient-appointment-name"]').html("<b style='color: #0783b5;'>Service User</b> </br>" +patientObj.firstName + " "+ patientObj.middleName+ " "+ patientObj.lastName +
                                "<br/>" +patientObj.gender);
						}
			    		// $('[name="patient-appointment-gender"]').text(patientObj.gender);
			    		if(dataArray[0].dateOfAppointment) {
                            var date = new Date(GetDateTimeEditDay(dataArray[0].dateOfAppointment));
                            var day = date.getDay();
                            var dow = getWeekDayName(day);
                           var dateTimeOfAppointment = dow + " " + GetDateTimeEdit(dataArray[0].dateOfAppointment);
			    			$('[name="patient-appointment-date"]').html("<b style='color: #0783b5;'>Appointment Date and Time</b> </br>" + dateTimeOfAppointment + "  ("+dataArray[0].duration+" min)");
			    			$('[name="patient-appointment-time"]').text(new Date(dataArray[0].dateOfAppointment).toLocaleTimeString());
			    		}
			    		if(dataArray[0].providerId != 0) {
                            if (dataArray[0].composition.provider) {
                                $('[name="patient-appointment-caregiver"]').html("<b style='color: #0783b5;'>Staff</b> </br>" + dataArray[0].composition.provider.firstName + " " + dataArray[0].composition.provider.lastName);
                            }
                        }
			    		else{
                            $('[name="patient-appointment-caregiver"]').html("<b style='color: #0783b5;'>Staff</b> </br>" + "Un Allocated");
                        }
			    		if(dataArray[0].inTime && dataArray[0].outTime) {
			    			$('[name="patient-appointment-intime"]').html("<b style='color: #0783b5;'>In Time</b> </br>" +new Date(dataArray[0].inTime).toLocaleTimeString());
			    			$('[name="patient-appointment-outtime"]').html("<b style='color: #0783b5;'>Out Time</b> </br>" +new Date(dataArray[0].outTime).toLocaleTimeString());
			    		}
			    	}
			    }
                $('.tableWrapper-appointment').removeClass('hideTable');
			};
			function buildpatientAppointmentActivities(dataObj) {
				console.log(dataObj);
				var dataArray = [];
			    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1" && dataObj.response.patientAppointmentActivities){
			        if($.isArray(dataObj.response.patientAppointmentActivities)){
			            dataArray = dataObj.response.patientAppointmentActivities;
			        } else{
			            dataArray.push(dataObj.response.patientAppointmentActivities);
			        }
			    }
                dataArray.sort(function(a, b) {
                    var nameA = a.activityTypeDisplayOrder; // ignore upper and lowercase
                    var nameB = b.activityTypeDisplayOrder; // ignore upper and lowercase
                    if (nameA < nameB) {
                        return -1;
                    }
                    if (nameA > nameB) {
                        return 1;
                    }
                    // names must be equal
                    return 0;
                });
			    if(dataArray.length > 0) {
                    IsVists = 0;
			    	var activityTypeArr = [];
			    	for (var i = 0; i < dataArray.length; i++) {
			    		activityTypeArr.push(dataArray[i].activityType);
			    	}
					var activityTypeArrFinal = activityTypeArr.reduce(function(p,c,i,activityTypeArr){
					  if (p.indexOf(c) == -1) p.push(c);
					  else p.push('')
					  return p;
					}, [])
			    	for (var i = 0; i < dataArray.length; i++) {
			    		var notes = dataArray[i].notes.replace('\n','; ');
			    		if(notes == "1" || notes == null || notes == "null"){
                            notes = "";
						}
						var dot;
                        if(dataArray[i].review == null || dataArray[i].review == 0) {
                            dot = "";
                        }
                        else if(dataArray[i].review == 1) {
                            dot = "greenDot";
                        }
                        else if(dataArray[i].review == 2){
                            dot = "orangeDot";
						}
                        else if(dataArray[i].review == 3){
                            dot = "redDot";
                        }
						 var thumbnailURL;
                         var thumbimg='';
                         var fileURL;
                        var fileimg = '';
						if(dataArray[i].activityThumbnailPresent==1){
							thumbnailURL = ipAddress+"/homecare/download/activities/thumbnail/?access_token="+sessionStorage.access_token+"&id="+dataArray[i].activityId+"&tenant="+sessionStorage.tenant;
                            thumbimg ='<img  src="'+thumbnailURL +'" id="imgLogo1" style="padding:5px;padding-left:30px;width:80px;"></img>';
						}
                        if(dataArray[i].filePresent==1){
                            fileURL = ipAddress+"/homecare/download/patient-appointment-activities/?access_token="+sessionStorage.access_token+"&id="+dataArray[i].id+"&tenant="+sessionStorage.tenant;
                            fileimg ='<img  src="'+fileURL +'" id="imgLogo1" style="padding:5px;padding-left:30px;width:100px;"></img>';
                        }

                        var remarks = "";
                        if(dataArray[i].remarks != undefined && dataArray[i].remarks != null){
                            remarks = dataArray[i].remarks;
                        }
                        var dateFD, dayFD, dowFD, dayWeekFD, dateED, dayED, dowED, dayWeekED, dateTime;
                        if(dataArray[i].actionedDate != undefined) {
                            dateFD = new Date(GetDateTimeEditDay(dataArray[i].actionedDate));
                            dayFD = dateFD.getDay();
                            dowFD = getWeekDayName(dayFD);

                            dayWeekFD = " <br>"+dowFD + " " + GetDateTimeEdit(dataArray[i].actionedDate);
                            dateTime = kendo.toString(dateFD,"hh:mm tt");
                        }
                        else{
                            dayWeekFD = "";
                            dateTime = "";
						}

                        if(dataArray[i].actionedModifiedDate != undefined){
                             dateED = new Date(GetDateTimeEditDay(dataArray[i].actionedModifiedDate));
                             dayED = dateED.getDay();
                             dowED = getWeekDayName(dayED);

                             dayWeekED = " /<br>"+dowED + " " + GetDateTimeEdit(dataArray[i].actionedModifiedDate);
                              dateTime = kendo.toString(dateED,"hh:mm tt");
                        }
                        else{
                            dayWeekED = "";
						}
                        if(dataArray[i].required == null || dataArray[i].required == 0){
                            if(activityTypeArrFinal[i] != "") {
                                $('#activitiesPeformed tbody').append('<tr><td colspan="3" id="patient-appointment-activityName" name="patient-appointment-activityName">' + activityTypeArrFinal[i] + '<span style="float: left" class=' + dot + '></span></td></tr><tr><td width="10%" name="patient-appointment-image">'+thumbimg+'</td><td width="15%" name="patient-appointment-activity">' + dataArray[i].activity + '<span class="mandatory"></span><span style="float: left" class=' + dot + '></td><td width="45%" name="patient-appointment-activityNotes">' + fileimg + notes + '<br>' + remarks + '</td><td width="25%" style="text-align: left" name="patient-appointment-activityDate">' + dateTime+ '</td></tr>');
                            }
                            else{
                                $('#activitiesPeformed tbody').append('<tr><td width="10%" name="patient-appointment-image">'+thumbimg+'</td><td width="15%" name="patient-appointment-activity">' + dataArray[i].activity + '<span style="float: left" class=' + dot + '></span></td><td width="60%" name="patient-appointment-activityNotes">' + fileimg +notes + '<br>' + remarks + '</td><td style="text-align: left"width="10%"  name="patient-appointment-activityDate">' +dateTime+ '</td></tr>');
                            }
                        }
                        else{
                            if(activityTypeArrFinal[i] != "") {
                                $('#activitiesPeformed tbody').append('<tr><td colspan="3" id="patient-appointment-activityName" name="patient-appointment-activityName">' + activityTypeArrFinal[i] + '<span class="mandatory"></span> <span style="float: left" class=' + dot + '></span></td></tr><tr><td width="0%" name="patient-appointment-image">'+thumbimg+'</td><td width="15%" name="patient-appointment-activity">' + dataArray[i].activity + '<span class="mandatory"></span><span style="float: left" class=' + dot + '></span></td><td width="45%" name="patient-appointment-activityNotes">' +fileimg + notes + '<br>' +remarks + '</td><td width="25%" style="text-align: left" name="patient-appointment-activityDate">' + dateTime+ '</td></tr>');
                            }
                            else {
                                $('#activitiesPeformed tbody').append('<tr><td width="10%" name="patient-appointment-image">'+thumbimg+'</td><td width="15%" name="patient-appointment-activity">' + dataArray[i].activity + '<span class="mandatory"></span> <span style="float: left" class=' + dot + '></span></td><td width="50%" name="patient-appointment-activityNotes">' + fileimg +notes + '<br>' + remarks + '</td><td width="10%" style="text-align: left" name="patient-appointment-activityDate">' + dateTime+ '</td></tr>');
                            }
                        }

			    	}
				}
			    else{
			        customAlert.info("Info","No visit(s) found");
                    IsVists = 1;
                }
                $('.tableWrapper-appointment').removeClass('hideTable');
			}
			function patientAppointmentMedicationActivity(dataObj) {
				console.log(dataObj);
				var dataArray = [];
			    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1" && dataObj.response.patientAppointmentMedicationActivity) {
			        if($.isArray(dataObj.response.patientAppointmentMedicationActivity)){
			            dataArray = dataObj.response.patientAppointmentMedicationActivity;
			        } else{
			            dataArray.push(dataObj.response.patientAppointmentMedicationActivity);
			        }
			    }
			    if(dataArray.length > 0) {
			    	for (var i = 0; i < dataArray.length; i++) {
			    		// var dateString = new Date(dataArray[i].administrationTime).toLocaleDateString();
						// var dateString = GetDateTimeEdit(dataArray[i].administrationTime);
						var day;
                        day = new Date(GetDateTimeEditDay(dataArray[i].administrationTime));
                        day = day.getDay();
                        day = getWeekDayName(day);

                        var dateString = day + " " + GetDateTimeEdit(dataArray[i].administrationTime);
			    		// var timeString = new Date(dataArray[i].administrationTime).toLocaleTimeString();
                        var notes = dataArray[i].notes;
                        if(notes == null && notes == "null")
                        {
                            notes = "";
                        }
			    		// $('#activitiesPeformed tbody').append('<tr><td name="patient-appointment-medicationTitle">Meds</td><td><p><span name="patient-appointment-medicationTradeName">'+dataArray[i].medicationTradeName+'</span><span name="patient-appointment-strength">'+dataArray[i].strength+'</span><span name="patient-appointment-medicationStrength">'+dataArray[i].medicationStrength+'</span></p><p><span name="patient-appointment-take">'+dataArray[i].take+'</span><span name="patient-appointment-medicationForm">'+dataArray[i].medicationForm+'</span><span name="patient-appointment-medicationFrequency">'+dataArray[i].medicationFrequency+'</span><span name="patient-appointment-medicationRoute">'+dataArray[i].medicationRoute+'</span></p><p><span name="patient-appointment-medicationInstructions">'+dataArray[i].medicationInstructions+'</span></p><p><span name="patient-appointment-instruction">'+dataArray[i].instruction+'</span></p></td><td name="patient-appointment-activityNotes"><p><span name="patient-appointment-administrationTime">' + dateString + ' '+ timeString +'</span></p><p><span name="patient-appointment-patientMedicationAction">'+dataArray[i].patientMedicationAction+'</span></p><p><span name="patient-appointment-notes">'+dataArray[i].notes+'</span></p></td></tr>');
                        $('#activitiesPeformed tbody').append('<tr><td name="patient-appointment-medicationTitle">Meds</td><td><p><span name="patient-appointment-medicationTradeName">'+dataArray[i].medicationTradeName+'</span><span name="patient-appointment-strength">'+dataArray[i].strength+'</span><span name="patient-appointment-medicationStrength">'+dataArray[i].medicationStrength+'</span></p><p><span name="patient-appointment-take">'+dataArray[i].take+'</span><span name="patient-appointment-medicationForm">'+dataArray[i].medicationForm+'</span><span name="patient-appointment-medicationFrequency">'+dataArray[i].medicationFrequency+'</span><span name="patient-appointment-medicationRoute">'+dataArray[i].medicationRoute+'</span></p><p><span name="patient-appointment-medicationInstructions">'+dataArray[i].medicationInstructions+'</span></p><p><span name="patient-appointment-instruction">'+dataArray[i].instruction+'</span></p></td><td name="patient-appointment-activityNotes"><p><span name="patient-appointment-administrationTime">' + dateString + '</span></p><p><span name="patient-appointment-patientMedicationAction">'+dataArray[i].patientMedicationAction+'</span></p><p><span name="patient-appointment-notes">'+notes+'</span></p></td><td></td></tr>');
			    	}
			    	$('.tableWrapper-appointment').removeClass('hideTable');
				}
			}
			function onError(errObj) {
				console.log(errObj);
    			customAlert.error("Error", "Error");
			}
		},
		_calculateAge: function(birthday) {
		    var ageDifMs = Date.now() - birthday.getTime();
		    var ageDate = new Date(ageDifMs);
		    return Math.abs(ageDate.getUTCFullYear() - 1970);
		}
	};
	patientActivity.init();

    // $("#saveAsPdf").off("click");
    // $("#saveAsPdf").on("click",onClickPrint);
});

function onGetDateTimeFormat(date) {
    var today = new Date(date);
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
     today = dd + '-' + mm + '-' + yyyy;
    return today;
}


function onClickPrint() {

    // if ($.trim($('#divTaskReportsViewer').html()) !== "") {
    //$("#tableAppointmentContent").empty();
    window.frames["print_frame"].document.head.innerHTML = "";
    window.frames["print_frame"].document.body.innerHTML = "";
    if(IsVists == 0) {

         var contents = document.getElementById("tableAppointmentContent").outerHTML;// $("#divTaskReportsViewer").html();
        // var frame1 = $('<iframe />');
        // frame1[0].name = "frame1";
        // frame1.css({"position": "absolute", "top": "-1000000px"});
        // $("body").append(frame1);
        // var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        // frameDoc.document.open();
        // //Create a new HTML document.
        // frameDoc.document.write('<html><head><title>DIV Contents</title>');
        // frameDoc.document.write('</head><body class="bodyClass">');
        // frameDoc.document.write('<div class="col-xs-12 ibox"> <div class="col-xs-12 ibox-content"> <div class="col-xs-12 formArea">');
        // //Append the libs CSS file.
        // //frameDoc.document.write('<link href="../../css/libs/css/bootstrap.min.css" rel="stylesheet" type="text/css" />');
        // //frameDoc.document.write('<link href="../../css/libs/css/allcss.css" rel="stylesheet" type="text/css" />');
        // frameDoc.document.write('<link href="../../css/Theme01.css" rel="stylesheet" type="text/css" />');
        //
        // //Append the DIV contents.
        // frameDoc.document.innerHTML = contents;
        // //frameDoc.document.write(contents);
        // frameDoc.document.write(contents);
        // frameDoc.document.write('</div> </div> </div>');
        // frameDoc.document.write('</body></html>');


        //frameDoc.document.close();

        //window.frames["frame1"].focus();
        //window.frames["frame1"].print();
        // var html='<html><head><title>DIV Contents</title>';
        // html += '</head>';
        //

        var htmlToPrint = '' +
            '<style type="text/css">' +
            'table th, table td, table {' +
            'border:solid 1px #000000' +
            ';' +
            'padding;0.5em;' +
            'border-collapse:collapse;'+
            '}' +
            '</style><style href="../../css/Theme01.css" rel="stylesheet" type="text/css" />';
        var hhtml = '';
        hhtml = ''+ '<head><title>Report</title><link href="../../css/Theme01.css" rel="stylesheet" type="text/css" /></head>';

        var bhtml = '';
        bhtml += '<body class="bodyClass"><div class="col-xs-12 ibox"> <div class="col-xs-12 ibox-content"> <div class="col-xs-12 formArea">';
        //bhtml +='<link href="../../css/Theme01.css" rel="stylesheet" type="text/css" />';
       bhtml += contents;
        bhtml += '</div> </div> </div>';
        bhtml += '</body>';


        // var htmlToPrint = '' +
        //     '<style type="text/css">' +
        //     'table th, table td, table {' +
        //     'border:solid 1px #f3f6f9' +
        //     ';' +
        //     'padding;0.5em;' +
        //     'border-collapse:collapse;'+
        //     '}' +
        //     '</style>';

        // frameDoc.document.open();
        // //Create a new HTML document.
        //window.frames["print_frame"].document.open();
        //window.frames["print_frame"].document.write('<html><head>');
        //window.frames["print_frame"].document.write('</head><body class="bodyClass">');
        //window.frames["print_frame"].document.write('<div class="col-xs-12 ibox"> <div class="col-xs-12 ibox-content"> <div class="col-xs-12 formArea">');
        //Append the libs CSS file.
        //frameDoc.document.write('<link href="../../css/libs/css/bootstrap.min.css" rel="stylesheet" type="text/css" />');
        //frameDoc.document.write('<link href="../../css/libs/css/allcss.css" rel="stylesheet" type="text/css" />');
        //window.frames["print_frame"].document.write('<link href="../../css/Theme01.css" rel="stylesheet" type="text/css" />');

        //Append the DIV contents.
            htmlToPrint += contents;
        //frameDoc.document.write(contents);
        window.frames["print_frame"].document.head.innerHTML = hhtml;
        window.frames["print_frame"].document.body.innerHTML = bhtml;
        // window.frames["print_frame"].document.write(contents);
        // window.frames["print_frame"].document.write('</div> </div> </div>');
        // window.frames["print_frame"].document.write('</body></html>');
        // // window.frames["print_frame"].document.head.innerHTML= html;
        // window.frames["print_frame"].document.body.innerHTML= bhtml;
        //window.frames["print_frame"].document.body.innerHTML = htmlToPrint;
        // setTimeout(function() {
            window.frames["print_frame"].window.focus();
            window.frames["print_frame"].window.print();
        // },0);
        //window.frames["print_frame"].window.location.reload();
        //window.frames["print_frame"].document.close();
        //frame1.remove();
    }
    else{
        customAlert.info("Info","No visit(s) found");
    }
    // }
    // else {
    //     customAlert.info("info","No data to print.");
    // }
}
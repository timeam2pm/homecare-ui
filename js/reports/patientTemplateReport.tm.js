var parentRef = null;
var recordType = "1";
var dataArray = [];
var selItem = null;
var resultDataObj;
$(document).ready(function(){
	parentRef = parent.frames['iframe'].window;
});

$(window).load(function(){
	loading = false;
	//$(window).resize(adjustHeight);
	onLoaded();
	if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
	$("#btnPrint").off("click");
    $("#btnPrint").on("click",onClickPrint);
	init()
}
function init(){
	console.log(parentRef);
	//resultDataObj = parentRef;
	var patientId = sessionStorage.patientId;
	getAjaxObject(ipAddress + "/patient/"+patientId+"/", "GET", onGetPatientInfo, onError);
}

function onError(errorObj){
	console.log(errorObj);
}

function buttonEvents(){
	$("#btnCancelDet").off("click",onClickCancel);
	$("#btnCancelDet").on("click",onClickCancel);
}

function onClickCancel(){
	var obj = {};
	var windowWrapper = new kendoWindowWrapper();
	windowWrapper.closePageWindow(obj);
}
function onGetPatientInfo(dataObj) {
    patientInfoObject = dataObj.response;
    
    var dob = dformat(new Date(patientInfoObject.patient.dateOfBirth).getTime(), 'dd-MM-yyyy');
    console.log(patientInfoObject);
    var dt = new Date(patientInfoObject.patient.modifiedDate);
	dt = kendo.toString(dt, "dd-MM-yyyy h:mm:tt");
    var modifiedDate = dt;
    $('#first').text(patientInfoObject.patient.firstName);
    $('#middle').text(patientInfoObject.patient.middleName);
    $('#last').text(patientInfoObject.patient.lastName);
    $('#dateOfBirth').text(dob);
    $('#sugender').text(patientInfoObject.patient.gender);
    $('#add1').text(patientInfoObject.communication.address1);
    $('#add2').text(patientInfoObject.communication.address2);
    $('#city').text(patientInfoObject.communication.city);
    $('#state').text(patientInfoObject.communication.state);
    $('#country').text(patientInfoObject.communication.country);
    $('#zip').text(patientInfoObject.communication.zip);
    $('#hPhone').text(patientInfoObject.communication.homePhone);
    $('#cPhone').text(patientInfoObject.communication.cellPhone);
    $('#rDateTime').text(modifiedDate);
    setFormData(parentRef.dataObj);
    
}
function setFormData(resultDataObj){
	$("#divMinitemplates").empty();
	var strHtml = "";
	selItem = parentRef.selItem;
	$('#templateName').text(selItem.name);
	$("#divPanel").html("");
	miniTemplates = resultDataObj.response.templateValues[0].componentValues;
	$("#txtNotes").text(resultDataObj.response.templateValues[0].notes);
	// console.log(miniTemplates);
	var eles = '';
	var miniTemplatesArray = selItem.miniTemplates;
	
	for(var l=0;l<miniTemplatesArray.length;l++){
	var miniTemplateById = _.where(miniTemplates,{miniTemplateId:parseInt(miniTemplatesArray[l].id)});
	strHtml = strHtml+'<div class="table-block">';
	if(miniTemplateById.length)
	{
		miniTemplateById = _.sortBy(miniTemplateById, 'miniComponentDisplayOrder');
		//strHtml = strHtml+'<div class="panel-heading" style="background-color: #F4F4F4 !important;">'+miniTemplatesArray[l].name+'</div>';
		strHtml = strHtml+'<div class="timeSheetReportdivHeading">'+miniTemplatesArray[l].name+'</div>';
	}
	strHtml = strHtml + '<div class="col-xs-12 taskReportTblBlock"> <div class="table-responsive"> <table class="tblTaskreport weeklyCareReportTbl"><tbody>';
	for(var m=0;m<miniTemplateById.length;m++){
		strHtml = strHtml +'<tr>'
		var mItem = miniTemplateById[m];
		if(mItem){
			var mItemHref = mItem.miniComponentName;
			var mValue = mItem.value || '';
			var mItemId = "m"+mItem.id;
			var pBodyItem = "p"+mItem.id;
			var strhref = "#mini"+mItem.id;
			
			if(mItem.miniComponentType && mItem.miniComponentType.toLowerCase().indexOf('date')>-1&&mValue.length>0)
				{
					var dob = dformat(new Date(parseInt(mValue)).getTime(), 'dd-MM-yyyy');
					mValue = dob;
				}




			if(mValue.indexOf('~')>-1)
				mValue = mValue.split('~');
			if(mItem.miniComponentName && mItem.miniComponentName.toLowerCase() != "hline" && mItem.miniComponentName.toLowerCase() != "serviceuser" && mItem.miniComponentName.toLowerCase() != "staff"){
                if(mItem.miniComponentName.length >= 120) {
					//strHtml += '<div class="row"><div class="col-xs-6 leftDiv leftDivReport-height"><label>' + mItem.miniComponentName + '</label></div><div class="col-xs-6 rightDiv leftDivReport-height"><label>' + mValue + '</label></div></div>';
					strHtml += '<td class="borderbuttomthick" style="width: 80px;">' + mItem.miniComponentName + '</td><td class="borderbuttomthick">' + mValue + '</td>';
                }
                else{
					//strHtml += '<div class="row"><div class="col-xs-6 leftDiv"><label>' + mItem.miniComponentName + '</label></div><div class="col-xs-6 rightDiv"><label>' + mValue + '</label></div></div>';
					strHtml += '<td class="borderbuttomthick">' + mItem.miniComponentName + '</td><td class="borderbuttomthick">' + mValue + '</td>';
				}
			}

			//}
			//strHtml = strHtml+eles;
		}
		strHtml = strHtml +'</tr>'
	}
	strHtml = strHtml + '</tbody></table></div></div></div>';
	}
	//$("#divPanel").html(strHtml);
	$("#divMinitemplates").html(strHtml);

	if(!resultDataObj.response.templateValues[0].notes.length)
		$('.notes').css('display','none');
	console.log(miniTemplatesArray);
	/*for(var n=0;n<miniTemplatesArray.length;n++){
		componenets = miniTemplatesArray[m];
		console.log(miniTemplatesArray);
		var eles = '<div class="col-xs-6"><label>'+componenets[n].label+'</label></div><div class="col-xs-6"></div>'
	}*/
	
}

function dformat(time, format) {
            var t = new Date(time);
            var tf = function (i) { return (i < 10 ? '0' : '') + i };
            return format.replace(/yyyy|MM|dd|HH|mm|ss/g, function (a) {
                switch (a) {
                    case 'yyyy':
                        return tf(t.getFullYear());
                        break;
                    case 'MM':
                        return tf(t.getMonth() + 1);
                        break;
                    case 'mm':
                        return tf(t.getMinutes());
                        break;
                    case 'dd':
                        return tf(t.getDate());
                        break;
                    case 'HH':
                        return tf(t.getHours());
                        break;
                    case 'ss':
                        return tf(t.getSeconds());
                        break;
                }
            })
        }
function printReport(){
	$('#report-print').html($('.panel').html());
	$('#report-print').printMe({ "path": ["../../css/libs/css/bootstrap.min.css","../../js/libs/angular/ui-grid-angular.css","../../css/libs/css/timeam.css","../../css/libs/css/customStyles.css","../../css/patient/patient-report.css"]});
}

function onClickPrint(){
	if ($.trim($('#divMinitemplates').html()) !== "") {

		var contents = document.getElementById("divPrint").outerHTML;// $("#divTaskReportsViewer").html();
		var frame1 = $('<iframe />');
		frame1[0].name = "frame1";
		frame1.css({ "position": "absolute", "top": "-1000000px" });
		$("body").append(frame1);
		var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
		frameDoc.document.open();
		//Create a new HTML document.
		frameDoc.document.write('<html><title>Form Data Report</title><head>');

		frameDoc.document.write('<link href="../../css/report/careerInOutReport.css" rel="stylesheet" type="text/css" />');
		frameDoc.document.write('<link href="../../css/report/invoicestyle.css" rel="stylesheet" type="text/css" />');
		frameDoc.document.write('<style>.borderrightthin { padding-left: 18px !important;text-align: center;}su{color:black;}</style>');
		frameDoc.document.write('</head><body style="background-color: #fff;">');

		//Append the DIV contents.
		frameDoc.document.write(contents);

		//frameDoc.document.write('</div> </div> </div>');
		frameDoc.document.write('</body></html>');
		frameDoc.document.close();

		var iFrameDOM = $("iframe[name=frame1").contents();
		iFrameDOM.find("#btnPrint").css("display", "none");
		setTimeout(function () {
			window.frames["frame1"].focus();
			window.frames["frame1"].print();
			frame1.remove();

		}, 500);
	}
	else {
		customAlert.info("info","No data to print.");
	}
}
$(document).ready(function(){
});

$(window).load(function(){
	if(sessionStorage.clientTypeId == "2"){
		$("#lblAppByPatient").text("Appointment By Client");
		$("#lblPTT").text("Client :");
		$("#lblPTR").text("Client :");
	}
	parentRef = parent.frames['iframe'].window;
	onMessagesLoaded();
});
function adjustHeight(){
	var defHeight = 260;
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 100;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    
   // $("#ptscheduler1").css({height: (cmpHeight+90) + 'px' });
    $("#ptscheduler").css({height: (cmpHeight) + 'px' });
}
function onMessagesLoaded() {
	buttonEvents();
	//init();
}
var cntry = "";
var dtFMT = "dd/MM/yyyy";
var stDate = "";
var endDate = "";

function init(){
	//patientId = parentRef.patientId;
	//$("#dtFromDate").kendoDatePicker({value:new Date()});
	//$("#dtToDate").kendoDatePicker({value:new Date()});
	cntry = sessionStorage.countryName;
	 if(cntry.indexOf("India")>=0){
		 dtFMT = INDIA_DATE_FMT;
	 }else  if(cntry.indexOf("United Kingdom")>=0){
		 dtFMT = ENG_DATE_FMT;
	 }else  if(cntry.indexOf("United State")>=0){
		 dtFMT = US_DATE_FMT;
	 }
	$("#txtStartDate").kendoDatePicker({format:dtFMT});
	$("#txtEndDate").kendoDatePicker({format:dtFMT});
	
	startDT = $("#txtStartDate").kendoDatePicker({
         change: startChange,format:dtFMT
     }).data("kendoDatePicker");

     endDT= $("#txtEndDate").kendoDatePicker({
         change: endChange,format:dtFMT
     }).data("kendoDatePicker");

     endDT.min(new Date());
     
     adjustHeight();
}
function startChange() {
    var startDate = startDT.value(),
    endDate = endDT.value();

    if (startDate) {
        startDate = new Date(startDate);
        startDate.setDate(startDate.getDate());
        endDT.min(startDate);
    } else if (endDate) {
        startDT.max(new Date(endDate));
    } else {
        endDate = new Date();
        startDT.max(endDate);
        endDT.min(endDate);
    }
}

function endChange() {
    var endDate = endDT.value(),
    startDate = startDT.value();

    if (endDate) {
        endDate = new Date(endDate);
        endDate.setDate(endDate.getDate());
        startDT.max(endDate);
    } else if (startDate) {
        endDT.min(new Date(startDate));
    } else {
        endDate = new Date();
        startDT.max(endDate);
        endDT.min(endDate);
    }
}
function buttonEvents(){
	$("#btnSearch1").off("click",onClickSearch);
	$("#btnSearch1").on("click",onClickSearch);
	
	$("#btnPtView").off("click",onClickPtViews);
	$("#btnPtView").on("click",onClickPtViews);
}
function onClickSearch(){
	 var popW = "60%";
	    var popH = 500;

	    var profileLbl;
	    var devModelWindowWrapper = new kendoWindowWrapper();
	    profileLbl = "Search Patient";
		if(sessionStorage.clientTypeId == "2"){
			 profileLbl = "Search Client";
		}
	    devModelWindowWrapper.openPageWindow("../../html/patients/searchPatient.html", profileLbl, popW, popH, true, closePtAddAction);
}
function closePtAddAction(evt,returnData){
	if(returnData && returnData.status == "success"){
		console.log(returnData);
		var pID = returnData.selItem.PID;
		viewPatientId = pID;
		var pName = returnData.selItem.FN+" "+returnData.selItem.MN+" "+returnData.selItem.LN;
		var viewpName = pName;
		if(pID != ""){
			//$("#lblName").text(pID+" - "+pName);
			$("#txtPatient").val(pName);
			/*var imageServletUrl = ipAddress + "/download/patient/photo/" + pID + "/?" + Math.round(Math.random() * 1000000);
	        $("#imgPhoto").attr("src", imageServletUrl);
	        
	        getAjaxObject(ipAddress + "/patient/" + pID, "GET", onGetPatientInfo, onError);
		}*/
	}
}
}
function onClickPtViews(){
	
}
var angularPTUIgridWrapper = null;
var dataArray = [];
var appReport = angular.module('appReport', []);
var selPatientId;
var selProviderId;
var appointmentDelayValue;

appReport.controller('reportController', function($scope) {
    $scope.getData = function () {
        onClickPtViews();
    }
    $scope.printTable = function () {

        var divToPrint = document.getElementById("carertable");

        var htmlToPrint = '' +
            '<style type="text/css">' +
            'table th, table td, table {' +
            'border:solid 1px #f3f6f9' +
            ';' +
            'padding;0.5em;' +
            'border-collapse:collapse;'+
            '}' +
            '</style>';
        htmlToPrint += divToPrint.outerHTML;

        // var newWin = window.open("");
        // newWin.document.write(htmlToPrint);
        // newWin.document.close();
        // newWin.focus();
        // newWin.print();
        // newWin.close();

        // var newWin  = window.open("");
        // newWin.document.body.innerHTML = htmlToPrint;
        // newWin.focus();
        // newWin.print();
        // newWin.close();

        window.frames["print_frame"].document.body.innerHTML = htmlToPrint;
        window.frames["print_frame"].window.focus();
        window.frames["print_frame"].window.print();

    }
});


$(document).ready(function(){
    $("#divMain",parent.document).css("display","none");
    $("#divPatInfo",parent.document).css("display","");
    // $("#lblTitleName",parent.document).html("Carer In Out Report");
    var cntry = sessionStorage.countryName;
    if((cntry.indexOf("India")>=0) || (cntry.indexOf("United Kingdom")>=0)){
        $("#lblFacility").text("Location");
    }else if(cntry.indexOf("United State")>=0) {
        $("#lblFacility").text("Facility");
    }
});

$(window).load(function(){
    parentRef = parent.frames['iframe'].window;
    settingValue();
    onMessagesLoaded();
});
function adjustHeight(){
    var defHeight = 160;
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 100;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    try{angularPTUIgridWrapper.adjustGridHeight(cmpHeight);}catch(e){};

}
function onMessagesLoaded() {
    buttonEvents();
    init();
}
var cntry = "";
var dtFMT = "dd/MM/yyyy";
var stDate = "";
var endDate = "";

function init(){
    //patientId = parentRef.patientId;
    //$("#dtFromDate").kendoDatePicker({value:new Date()});
    //$("#dtToDate").kendoDatePicker({value:new Date()});

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();


    cntry = sessionStorage.countryName;
    getFacilityList();
    if(cntry.indexOf("India")>=0){
        dtFMT = INDIA_DATE_FMT;
    }else  if(cntry.indexOf("United Kingdom")>=0){
        dtFMT = ENG_DATE_FMT;
    }else  if(cntry.indexOf("United State")>=0){
        dtFMT = US_DATE_FMT;
    }
    $("#txtStartDate").kendoDatePicker({format:dtFMT});
    $("#txtEndDate").kendoDatePicker({format:dtFMT});

    startDT = $("#txtStartDate").kendoDatePicker({
        change: startChange,format:dtFMT,value:new Date()
    }).data("kendoDatePicker");

    endDT= $("#txtEndDate").kendoDatePicker({
        change: endChange,format:dtFMT,value:new Date()
    }).data("kendoDatePicker");

    endDT.min(new Date());
    // onClickPtViews();
}

function getReportDataList(dataObj){
    dataArray = [];
    var tempdataArray = [];
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            if (dataObj.response.appointmentReports) {
                if ($.isArray(dataObj.response.appointmentReports)) {
                    dataArray = dataObj.response.appointmentReports;
                } else {
                    dataArray.push(dataObj.response.appointmentReports);
                }
            }
        }
    }

    if (dataArray.length > 0) {
        for (var d = 0; d < dataArray.length; d++) {
            if (dataArray[d].providerId > 0) {
                if (dataArray[d].dateOfAppointment) {
                    var date = new Date(GetDateTimeEditDay(dataArray[d].dateOfAppointment));
                    var day = date.getDay();
                    var dow = getWeekDayName(day);
                    var dtEnd = dataArray[d].dateOfAppointment + (Number(dataArray[d].duration) * 60000);
                    dataArray[d].dateTimeOfAppointment = dow + " " + GetDateTimeEdit(dataArray[d].dateOfAppointment);
                    dataArray[d].DW = dow;
                    var dateED = new Date(GetDateTimeEditDay(dtEnd));
                    var dayED = dateED.getDay();
                    var dowED = getWeekDayName(dayED);
                    // dataArray[d].dateTimeOfAppointmentED = dowED + " " + GetDateTimeEdit(dtEnd);

                    dataArray[d].appDate = kendo.toString(new Date(dataArray[d].dateOfAppointment), "dd-MM-yyyy");
                    dataArray[d].appTime = kendo.toString(new Date(dataArray[d].dateOfAppointment), "hh:mm tt");

                    var apptDateTime = new Date(dataArray[d].dateOfAppointment);

                    var dateOfMonth = apptDateTime.getDate();
                    var apptmonth = apptDateTime.getMonth();
                    var apptyear = apptDateTime.getFullYear();
                    var appthours = apptDateTime.getHours();
                    var apptminutes = apptDateTime.getMinutes();
                    dataArray[d].appointmentDate = new Date(apptyear, apptmonth, dateOfMonth, appthours, apptminutes, 0, 0);

                }
                dataArray[d].serviceUser = dataArray[d].serviceUserLastName + " " + dataArray[d].serviceUserFirstName;
                dataArray[d].carer = dataArray[d].providerLastName + " " + dataArray[d].providerFirstName;

                if (dataArray[d].inTime) {
                    var inTime = kendo.toString(new Date(dataArray[d].inTime), "hh:mm tt");
                    if (inTime != "0") {
                        dataArray[d].inTime = inTime;
                    }
                }
                var totalMinutes = dataArray[d].delay;

                var hours = Math.floor(totalMinutes / 60);
                var minutes = Math.round(totalMinutes % 60);
                if (minutes.toString().length == 1) {
                    minutes = "0" + minutes;
                }


                dataArray[d].delayTime = hours + ":" + minutes;

                tempdataArray.push(dataArray[d]);
            }
        }

        if (tempdataArray != null && tempdataArray.length > 0) {
            var sortOrderValue = $("#cmbSortOrder option:selected").val();
            if (sortOrderValue == "1") {
                tempdataArray.sort(function (a, b) {
                    var p1 = a.dateOfAppointment;
                    var p2 = b.dateOfAppointment;

                    if (p1 < p2) return -1;
                    if (p1 > p2) return 1;
                    return 0;
                });
            }
            else if (sortOrderValue == "2") {
                tempdataArray.sort(sortByAppointmentDateAndServiceUser);
            }
            else if (sortOrderValue == "3") {
                tempdataArray.sort(sortByAppointmentDateAndStaffName);
            }
        }

        var elem = angular.element(document.querySelector('[ng-app]'));
        var injector = elem.injector();
        var $rootScope = injector.get('$rootScope');
        $rootScope.$apply(function(){
            $rootScope.apidata = tempdataArray;
        });
    }
    else{
        customAlert.error("info","No appointments");
    }

    // buildPatientRosterList(dataArray);
}
function secondsToHms(d) {
    d = Number(d);
    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    var hDisplay = h > 0 ? h + (h == 1 ? " hour, " : " hours, ") : "";
    var mDisplay = m > 0 ? m + (m == 1 ? " minute, " : " minutes, ") : "";
    var sDisplay = s > 0 ? s + (s == 1 ? " second" : " seconds") : "";
    return hDisplay + mDisplay + sDisplay;
}

function startChange() {
    var startDate = startDT.value(),
        endDate = endDT.value();

    if (startDate) {
        startDate = new Date(startDate);
        startDate.setDate(startDate.getDate());
        endDT.min(startDate);
    } else if (endDate) {
        startDT.max(new Date(endDate));
    } else {
        endDate = new Date();
        startDT.max(endDate);
        endDT.min(endDate);
    }
}

function endChange() {
    var endDate = endDT.value(),
        startDate = startDT.value();

    if (endDate) {
        endDate = new Date(endDate);
        endDate.setDate(endDate.getDate());
        startDT.max(endDate);
    } else if (startDate) {
        endDT.min(new Date(startDate));
    } else {
        endDate = new Date();
        startDT.max(endDate);
        endDT.min(endDate);
    }
}
function buttonEvents(){
    $("#btnPtView").off("click",onClickPtViews);
    $("#btnPtView").on("click",onClickPtViews);

    $("#btnPrint").off("click",onClickPrint);
    $("#btnPrint").on("click",onClickPrint);

}

function getFacilityList(){
    getAjaxObject(ipAddress+"/facility/list/?is-active=1","GET",getFacilityDataList,onError);
}
function getFacilityDataList(dataObj){
    var dataArray = [];
    if(dataObj){
        if($.isArray(dataObj.response.facility)){
            dataArray = dataObj.response.facility;
        }else{
            dataArray.push(dataObj.response.facility);
        }
    }
    var tempDataArry = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
        }
        $("#cmbFacility").append('<option value="' + dataArray[i].idk + '">' + dataArray[i].name + '</option>');
    }
    onClickPtViews();
}

function onError(err){

}
function onClickPrint(){
    angularPTUIgridWrapper.printDataGrid();
}
function onClickPtViews(){
    var ipUrl = ipAddress+"/reports/appointment-reports/?";
    var cmbFacility = $("#cmbFacility").val();
    if (cmbFacility != null && cmbFacility != undefined){
        ipUrl = ipUrl+"facility-id="+cmbFacility+"&";
    }

    var startDT = $("#txtStartDate").data("kendoDatePicker");
    var endDT = $("#txtEndDate").data("kendoDatePicker");


    var sDate = getFromDate(new Date(startDT.value()));
    var eDate = getToDate(new Date(endDT.value()));
    ipUrl = ipUrl+"date-of-appointment=:bt:"+sDate+","+eDate+"&delay=:gt:"+appointmentDelayValue+"&is-active=1&is-deleted=0";
    getAjaxObject(ipUrl,"GET",getReportDataList,onError);
}





function getFromDate(fromDate) {
    var day = fromDate.getDate();
    var month = fromDate.getMonth();
    month = month+1;
    var year = fromDate.getFullYear();

    var strDate = month+"/"+day+"/"+year;
    strDate = strDate+" 00:00:00";

    var date = new Date(strDate);
    date.setMilliseconds(0);
    var strDateTime = date.getTime();

    return strDateTime;
}

function getToDate(toDate) {
    var day = toDate.getDate();
    var month = toDate.getMonth();
    month = month+1;
    var year = toDate.getFullYear();

    var strDate = month+"/"+day+"/"+year;
    strDate = strDate+" 23:59:59";

    var date = new Date(strDate);
    date.setMilliseconds(0);
    var strDateTime = date.getTime();

    return strDateTime;
}

function settingValue(){
    getAjaxObject(ipAddress+"/homecare/settings/?id=4","GET",getSettingValue,onError);
}


function getSettingValue(dataObj){
    var tempCompType;
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.settings){
            if($.isArray(dataObj.response.settings)){
                tempCompType = dataObj.response.settings;
            }else{
                tempCompType.push(dataObj.response.settings);
            }
        }
        if(tempCompType.length > 0){
            appointmentDelayValue = tempCompType[0].value;
        }
    }
}


var sortByAppointmentDateAndStaffName = function (a, b) {

    if (a.carer > b.carer) return 1;
    if (a.carer < b.carer) return -1;

    var date1 = a.appointmentDate;
    var date2 = b.appointmentDate;


    if (date1 > date2) return 1;
    if (date1 < date2) return -1;



}

var sortByAppointmentDateAndServiceUser = function (a, b) {

    if (a.serviceUser > b.serviceUser) return 1;
    if (a.serviceUser < b.serviceUser) return -1;

    var date1 = a.appointmentDate;
    var date2 = b.appointmentDate;


    if (date1 > date2) return 1;
    if (date1 < date2) return -1;

}
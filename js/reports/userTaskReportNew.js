var masterTaskGroups,masterTaskReports,filteredTaskGroups,selectPatient;
var parentRef = null;
var dtFMT = "dd/MM/yyyy";
var masterReportDisplayNames = [];

$(document).ready(function () {
    //bindTaskReports();
});

$(window).load(function(){
    loading = false;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
    fnctaskReports();
    init();
    buttonEvents();
    adjustHeight();
}

function buttonEvents(){

    $("#btnView").off("click",onClickView);
    $("#btnView").on("click",onClickView);


    $("#btnServiceUser").off("click");
    $("#btnServiceUser").on("click",onClickServiceUser);

    $("#btnPrint").off("click");
    $("#btnPrint").on("click",onClickPrint);

}

function adjustHeight(){
    var defHeight = 280;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    //angularUIgridWrapper.adjustGridHeight(cmpHeight);
}

function init(){
    $("#txtFromDate").kendoDatePicker({format:dtFMT,value:new Date()});
    $("#txtToDate").kendoDatePicker({format:dtFMT,value:new Date()});

    //$("#cmbTaskReports").igCombo();


    bindTaskReports();
}


function getTaskReportValueList(dataObj) {
    var cmbId = "cmbTaskReports";
    if (dataObj && dataObj.length > 0) {

        for (var i = 0; i < dataObj.length; i++) {
            if (cmbId != '') {
                $("#" + cmbId).append('<option value="' + dataObj[i].idk + '">' + dataObj[i].name + '</option>');
            }
        }
        //$('#cmbTaskReports').multiselect({
        //    includeSelectAllOption: true
        //});
        $("#cmbTaskReports").multipleSelect({
            selectAll: true,
            width: 200,
            dropWidth: 200,
            multipleWidth: 200,
            placeholder: 'Select Task Reports',
            selectAllText: 'All'

        });

        // setMultipleDataForSelection(dataObj, "cmbTaskReports", onTaskReportChange, ["reportDisplayName", "idk"], 0, "");
    }
}

function getTableFirstListArray(dataObj,cmbId) {
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.codeTable) {
        if ($.isArray(dataObj.response.codeTable)) {
            dataArray = dataObj.response.codeTable;
        } else {
            dataArray.push(dataObj.response.codeTable);
        }
    }
    var tempDataArry = [];
    for (var i = 0; i < dataArray.length; i++) {
        if (dataArray[i].isActive == 1) {
            var obj = dataArray[i];
            obj.idk = dataArray[i].id;
            obj.status = dataArray[i].Status;
            tempDataArry.push(obj);
            if(cmbId != '') {
                $("#" + cmbId).append('<option value="' + dataArray[i].idk + '">' + dataArray[i].desc + '</option>');
            }

        }
    }

    return tempDataArry;

}

function onTaskReportChange() {
    onComboChange("cmbTaskReports");
}

function onComboChange(cmbId) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb && cmb.selectedIndex < 0) {
        cmb.select(0);
    }
}

function onClickView() {
    $(".userTask-table-block").remove();
    $("#divTaskReportsViewer").empty();
    clearPatientAndFilterDetails();

    var isValid = true;
    var strMessage = "";


    var selected = $('#cmbTaskReports').multipleSelect('getSelects');
    //var selected = $("#cmbTaskReports option:selected");
    //var message = "";
    //selected.each(function () {
    //    message += $(this).text() + " " + $(this).val() + "\n";
    //});

    if (selected == null || selected.length == 0) {
        strMessage = "Please select atleast one task report.";
        isValid = false;
    }

    if (isValid) {
        if ($("#txtSU").val().length === 0) {
            strMessage = "Please select service user.";
            isValid = false;
        }
    }

    if (isValid) {
        if ($("#txtFromDate").val().length === 0) {
            strMessage = "Please select from date.";
            isValid = false;
        }
    }

    if (isValid) {
        if ($("#txtToDate").val().length === 0) {
            strMessage = "Please select to date.";
            isValid = false;
        }
    }
    if (isValid) {
        $("#table-block").show();
        getPatientAppointmentActivities();
    }
    else{
        // strMessage = strMessage.replace("patient","service user");
        customAlert.error("info",strMessage);
    }
}

function bindTaskReports() {
    getAjaxObject(ipAddress + "/homecare/activity-types/?is-active=1&is-deleted=0&sort=type", "GET", getTaskTypes, onError);
}


function getTaskTypes(dataObj) {

    var types = [];
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            if (dataObj.response.activityTypes) {
                if ($.isArray(dataObj.response.activityTypes)) {
                    types = dataObj.response.activityTypes;
                } else {
                    types.push(dataObj.response.activityTypes);
                }
            }
            for (var i = 0; i < types.length; i++) {
                types[i].idk = types[i].id;
            }
        }
    }

    masterTaskGroups = types;

    getAjaxObject(ipAddress + "/homecare/task-reports/?is-active=1&is-deleted=0&access_token=" + sessionStorage.access_token + "&fields=*,activity.*", "GET", getTaskReportsSCB, onError);

}

function getTaskReportsSCB(dataObj) {

    var tempTaskReports = [];
    compType = [];
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.taskReports) {
            if ($.isArray(dataObj.response.taskReports)) {
                tempTaskReports = dataObj.response.taskReports;
            } else {
                tempTaskReports.push(dataObj.response.taskReports);
            }
        }
    }

    masterTaskReports = tempTaskReports;

    var lookup = {};
    var items = tempTaskReports;
    var reportNames = [];

    // for (var item, i = 0; item = tempTaskReports[i++];) {
    //     var activityTypeId = item.activityTypeId;

    //     if (!(activityTypeId in lookup)) {
    //         lookup[activityTypeId] = 1;
    //         activityTypeIds.push(activityTypeId);
    //     }
    // }

    // var filterArray = [];

    // if (activityTypeIds != null && activityTypeIds.length > 0) {

    //     filterArray = masterTaskGroups.filter(function (item, index) {
    //             return activityTypeIds.indexOf(item.idk) >= 0;
    //     });
    // }


    // if (activityTypeIds != null && activityTypeIds.length > 0) {

    //     filterArray = masterTaskGroups.filter(function (item, index) {
    //             return reportNames.indexOf(item.name) >= 0;
    //     });
    // }
    var c = 0;
    for (var item, i = 0; item = tempTaskReports[i++];) {
        var reportName = item.name;

        if (!(reportName in lookup) && reportName !== "") {
            c = c + 1;
            lookup[reportName] = 1;
            masterReportDisplayNames.push({
                idk: c,
                name: reportName
            })
        }
    }

    // var filterArray = [];


    // if (reportNames != null && reportNames.length > 0) {

    //     filterArray = masterTaskGroups.filter(function (item, index) {
    //             return reportNames.indexOf(item.name) >= 0;
    //     });
    // }

    masterReportDisplayNames.sort(sortByReportDisplayName);
    //filteredTaskGroups = filterArray;
    getTaskReportValueList(masterReportDisplayNames);
}

function onError(errorObj) {
    console.log(errorObj);
}

var activityTypeIds = [];
function getPatientAppointmentActivities() {
    var activityTypes;

    var reportDisplayNames = [];

    var reportDisplayNameIds = $('#cmbTaskReports').multipleSelect('getSelects');

    if (reportDisplayNameIds !== null && reportDisplayNameIds.length > 0) {

        for (var i = 0; i < reportDisplayNameIds.length; i++) {
            var tmpName = $.grep(masterReportDisplayNames, function (item) {
                return parseInt(item.idk) === parseInt(reportDisplayNameIds[i]);
            });

            if (tmpName !== null && tmpName.length > 0) {
                reportDisplayNames.push(tmpName[0].name);
            }

        }


        if (reportDisplayNames !== null && reportDisplayNames.length > 0) {

            var tempActivityTypeIds = [];
            for (var counter = 0; counter < reportDisplayNames.length; counter++) {

                var arrActivityTypeIds = $.grep(masterTaskReports, function (e) {
                    return e.name.toLowerCase() === reportDisplayNames[counter].toLowerCase();
                });

                if (arrActivityTypeIds !== null && arrActivityTypeIds.length > 0) {
                    for (var counter2 = 0; counter2 < arrActivityTypeIds.length; counter2++) {
                        tempActivityTypeIds.push(arrActivityTypeIds[counter2].activityTypeId);
                    }
                }
            }

            var lookup = {};
            var items = tempActivityTypeIds;
            var reportNames = [];

            for (var item, i = 0; item = tempActivityTypeIds[i++];) {
                var activityTypeId = item;

                if (!(activityTypeId in lookup)) {
                    lookup[activityTypeId] = 1;
                    activityTypeIds.push(activityTypeId);
                }
            }

        }

    }

    var patientId = selectPatient.PID;
    var txtFromDate = $("#txtFromDate").data("kendoDatePicker");
    var txtToDate = $("#txtToDate").data("kendoDatePicker");

    var selectedFromDate = txtFromDate.value();
    var selectedToDate = txtToDate.value();
    var selFromDate = new Date(selectedFromDate);
    var selToDate = new Date(selectedToDate);

    var fromDate = getFromDate(selFromDate);
    var toDate = getToDate(selToDate);


    //activityTypeIds = [1];
    //fromDate = "1523932854000";
    //toDate = "1526524854000";
    //patientId = 142;

    var taskReportNames = [];
    var lookup = {};
    for (var counter = 0; counter < activityTypeIds.length; counter++) {
        var tmpReports = _.where(masterTaskReports, { activityTypeId: parseInt(activityTypeIds[counter]) });
        if (tmpReports != null && tmpReports.length > 0) {
            for (var item, i = 0; item = tmpReports[i++];) {
                var reportName = item.name;

                if (!(reportName in lookup)) {
                    lookup[reportName] = 1;
                    taskReportNames.push(reportName);
                }
            }
        }
    }


    filteredTaskGroups = masterTaskGroups.filter(function (item, index) {
        return activityTypeIds.indexOf(item.idk) >= 0;
    });


    //var apiURL = ipAddress + "/patient/appointment/activity/list?from-date=" + fromDate + "&to-date=" + toDate + "&patient-id=" + patientId + "&activity-type-id=" + activityTypeIds.join(",");
    var apiURL = ipAddress + "/homecare/reports/task-reports-view/?patient-id=" + patientId + "&date-of-appointment=:bt:" + fromDate + "," + toDate + "&task-report-name=" + taskReportNames.join(",");
    getAjaxObject(apiURL, "GET", getPatientAppointmentActivitiesSCB, onError);
}

function getFromDate(fromDate) {


    var day = fromDate.getDate();
    var month = fromDate.getMonth();
    month = month + 1;
    var year = fromDate.getFullYear();

    var strDate = month + "/" + day + "/" + year;
    strDate = strDate + " 00:00:00";

    var date = new Date(strDate);
    var strDateTime = date.getTime();

    return strDateTime;
}

function getToDate(toDate) {


    var day = toDate.getDate();
    var month = toDate.getMonth();
    month = month + 1;
    var year = toDate.getFullYear();

    var strDate = month + "/" + day + "/" + year;
    strDate = strDate + " 23:59:59";

    var date = new Date(strDate);
    var strDateTime = date.getTime();

    return strDateTime;
}

function getPatientAppointmentActivitiesSCB(dataObj) {
    var tmpPatientAppointmentActivities = [];

    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.taskReport) {
            if ($.isArray(dataObj.response.taskReport)) {
                tmpPatientAppointmentActivities = dataObj.response.taskReport;
            } else {
                tmpPatientAppointmentActivities.push(dataObj.response.taskReport);
            }
        }
    }
    tmpPatientAppointmentActivities.sort(function (a, b) {
        var p1 = a.dateOfAppointment;
        var p2 = b.dateOfAppointment;

        if (p1 < p2) return -1;
        if (p1 > p2) return 1;
        return 0;
    });
    if (tmpPatientAppointmentActivities != null && tmpPatientAppointmentActivities.length > 0) {
        $(".userTaskDetails").css("display", "");
        buildReportViewerJson(filteredTaskGroups, masterTaskReports, tmpPatientAppointmentActivities);
    }
    else {
        $(".userTaskDetails").css("display", "none");
        customAlert.info("info", "No service user appointment activities.");
        // clearFilterSelection();
    }
}


function buildReportViewerJson(taskReports, taskReportColumns, patientAppointmentActivities) {
    var uniqueAppointmentDetails = [];
    var tabledata;
    if (patientAppointmentActivities != null && patientAppointmentActivities.length > 0) {
        var lookup = {};
        var items = patientAppointmentActivities;

        for (var item, i = 0; item = patientAppointmentActivities[i++];) {
            var appointmentId = item.appointmentId;

            if (!(appointmentId in lookup)) {
                lookup[appointmentId] = 1;
                var objData = {};
                objData.appointmentId = item.appointmentId;
                objData.dateOfAppointment = new Date(parseInt(item.dateOfAppointment));
                objData.actionedDate = item.actionedDate && item.actionedDate != "null" ? new Date(parseInt(item.actionedDate)) : "";
                objData.actionedModifiedDate = item.actionedModifiedDate && item.actionedModifiedDate != "null" ? new Date(parseInt(item.actionedModifiedDate)) : "";
                uniqueAppointmentDetails.push(objData);
            }
        }

        if (uniqueAppointmentDetails != null && uniqueAppointmentDetails.length > 0) {
            uniqueAppointmentDetails.sort(sortByDateAsc);

            var reportViewerJson = [];
            var taskReportsView = [];
            // for (var activeCounter = 0; activeCounter < activityTypeIds.length; activeCounter++) {

            // }

            for (var activeCounter = 0; activeCounter < activityTypeIds.length; activeCounter++) {
                taskReportsView = _.where(taskReports, { idk: Number(activityTypeIds[activeCounter]) });
                for (var counter = 0; counter < taskReportsView.length; counter++) {
                    // var activeIds = activityTypeIds.split(',');
                    // for (var activeCounter = 0; activeCounter < activityTypeIds.length; activeCounter++) {
                    //     if(activityTypeIds[activeCounter] == taskReports[counter].idk) {
                    var objTaskReport = {};

                    var tmpReportDisplayName = taskReportsView[counter].reportDisplayName;
                    var tmpActivityTypeId = taskReportsView[counter].idk;
                    objTaskReport.reportDisplayName = tmpReportDisplayName;
                    objTaskReport.dataSource = [];

                    for (var counter2 = 0; counter2 < uniqueAppointmentDetails.length; counter2++) {
                        objTaskReport.dataSource[counter2] = {};
                        objTaskReport.dataSource[counter2]["Appt Date Time"] = convertDateToString(uniqueAppointmentDetails[counter2].dateOfAppointment);
                        // objTaskReport.dataSource[counter2]["Recorded Date Time"] = convertDateToString(uniqueAppointmentDetails[counter2].actionedDate);
                        // objTaskReport.dataSource[counter2]["Rec Modified Date Time"] = convertDateToString(uniqueAppointmentDetails[counter2].actionedModifiedDate);

                        var tmpAppointmentId = uniqueAppointmentDetails[counter2].appointmentId;

                        var tmpTaskReportColumns = _.where(masterTaskReports, { activityTypeId: tmpActivityTypeId });

                        if (tmpTaskReportColumns != null && tmpTaskReportColumns.length > 0) {
                            for (var counter3 = 0; counter3 < tmpTaskReportColumns.length; counter3++) {
                                var tmpActivityId = tmpTaskReportColumns[counter3].activityId;
                                //objTaskReport.dataSource[counter2][tmpTaskReportColumns[counter3].name] = getDataForCell(tmpAppointmentId, tmpActivityTypeId, tmpActivityId, patientAppointmentActivities);
                                var data;
                                data = getDataForCellNew(tmpAppointmentId, tmpActivityTypeId, tmpActivityId, patientAppointmentActivities);
                            }
                        }

                        //     }
                        // }

                    }
                    tabledata = tabledata + data;
                    reportViewerJson.push(objTaskReport);

                }
            }

            if (reportViewerJson != null && reportViewerJson.length > 0) {
                bindPatientAndFilterDetails();
                generateReportTables(reportViewerJson, tabledata);
                // clearFilterSelection();
            }
        }
    }

}



function generateReportTables(reportViewerJson, data){
    if(reportViewerJson != null && reportViewerJson.length > 0){
        var insertAfterElement="";
    }
    for(var counter = 0;counter < reportViewerJson.length;counter++){
        var reportDisplayName = reportViewerJson[counter].reportDisplayName;


        var reportDisplayNameId = (reportDisplayName.replace(/ +/g, "")) + counter;
        // var $report = $("<div class=\"col-xs-12 userTask-table-block\" id=\"div"+reportDisplayNameId+"\"> <h5>"+reportDisplayName+"</h5> <div class=\"table-responsive\"> <div class=\"prescription-table-header\"> <table class=\"table contact-tbl\" id=\"tbl"+reportDisplayNameId+"\"> </table> </div> </div> </div>");

        var $report = "<thead><tr><th colspan='3'><span class='spnReportTitle'>"+reportDisplayName+"</span><span class='spnReportTitle2'>"+ reportViewerJson[counter].dataSource[0]["Appt Date Time"] +"</span></th></tr>";

        // if(insertAfterElement.length === 0)
        //     $report.insertAfter(".userTaskDetails");
        // else
        //     $report.insertAfter(insertAfterElement);
        //
        // insertAfterElement = "#div"+reportDisplayNameId;

        //$("#divTaskReportsViewer").append($report);
        //bindTaskReport(reportDisplayNameId,reportViewerJson[counter].dataSource);

        $("#tblTaskreport").append($report + data);
    }
}

function bindPatientAndFilterDetails() {
    if (selectPatient !== null && selectPatient && typeof selectPatient !== 'undefined') {
        $("#lblName").text(selectPatient.lastName + " " + selectPatient.firstName);
        $("#divServiceUser").text(selectPatient.PID +   " - " +selectPatient.lastName + " " + selectPatient.firstName);
        $("#lblAddress1").text(selectPatient.address1);
        $("#lblAddress2").text(selectPatient.address2);
        $("#lblLandLine").text(selectPatient.homePhone);
        $("#lblMobile").text(selectPatient.cellPhone);
    }

    var txtFromDate = $("#txtFromDate").data("kendoDatePicker");
    var txtToDate = $("#txtToDate").data("kendoDatePicker");

    var dateFD = new Date(formatDate(txtFromDate.value()));
    dateFD = dateFD.getDay();
    dateFD = getWeekDayName(dateFD);
    dateFD = dateFD + " "+formatDate(txtFromDate.value());
    $("#lblFromDate").text("From Date : " + dateFD);

    var dateED = new Date(formatDate(txtToDate.value()));
    dateED = dateED.getDay();
    dateED = getWeekDayName(dateED);
    dateED = dateED + " "+formatDate(txtToDate.value());

    $("#lblToDate").text("To Date : " + dateED);
}


function bindTaskReport(reportDisplayNameId, dataObj) {
    var tableId = "#tbl"+reportDisplayNameId;

    if (tmpArray[0].actionedDate && tmpArray[0].actionedDate != "null") {
        var dateED = new Date(GetDateTimeEditDay(tmpArray[0].actionedDate));
        var dayED = dateED.getDay();
        var dowED = getWeekDayName(dayED);

        var dayWeek = dowED + " " + GetDateTimeEdit(tmpArray[0].actionedDate);

        var firstObj = dataObj[0];

        for (var key in firstObj) {
            if(key.toLowerCase() == "appt date time") {
                strHeading = strHeading + "<th style='width:12%'>" + key + "</th>";
            }else{
                strHeading = strHeading + "<th>" + key + "</th>";
            }
            columns.push(key);
        }
    }
    strHeading = strHeading+"</thead>";

    $(tableId).append(strHeading);


    var strTableBody = "<tbody>";

    for(var counter = 0;counter<dataObj.length;counter++){
        var strTableRow = "<tr>";

        for(var counter2= 0;counter2<columns.length;counter2++){
            strTableRow = strTableRow + "<td>"+ dataObj[counter][columns[counter2]]+"</td>";
        }
        strTableRow = strTableRow+"</tr>";

        strTableBody = strTableBody +strTableRow;
    }
    strTableBody = strTableBody+"</tbody>";
    $(tableId).append(strTableBody);


}

function getDataForCell(appointmentId, activityTypeId,activityId,patientAppointments) {
    var strReturnData = "";
    var tmpArray = patientAppointments.filter(function (index, item) {
        // return item.appointmentId == 1548 && item.activityTypeId == 5 && item.activityId == 11;
        if(index.appointmentId == appointmentId && index.activityTypeId == activityTypeId && index.activityId == activityId){
            return index;
        }
    });
    // var tmpArray =patientAppointments;
    var fileURL,fileimg;
    if (tmpArray && tmpArray.length > 0) {

        if(tmpArray[0].review == null || tmpArray[0].review == 0) {
            // dot = "greenDot";
            dot ='<img  src="../../img/report/icon-Green-circle.png" id="imgApptAct" style="padding-left:30px;width:50px;height:18px;float:right">s</img>';
        }
        else if(tmpArray[0].review == 1) {
            // dot = "orangeDot";
            dot ='<img  src="../../img/report/icon-orange-circle.png" id="imgApptAct" style="padding-left:30px;width:50px;height:18px;float:right"></img>';
        }
        else if(tmpArray[0].review == 2){
            // dot = "redDot";
            dot ='<img  src="../../img/report/icon-red-circle.png" id="imgApptAct" style="padding-left:30px;width:50px;height:18px;float:right"></img>';

        }
        if(tmpArray[0].filePresent){
            fileURL = ipAddress+"/homecare/download/patient-appointment-activities/?access_token="+sessionStorage.access_token+"&id="+tmpArray[0].id+"&tenant="+sessionStorage.tenant;
            fileimg ='<img  src="'+fileURL +'" id="imgApptAct" style="padding:5px;padding-left:30px;width:100px;height:64px;float:right"></img>';
            strReturnData = fileimg;

        }

        // strReturnData = strReturnData+"<br>" + "<b>Review : </b>";
        // if (tmpArray[0].review && tmpArray[0].review != "null") {

        // }
        if(strReturnData != ""){
            strReturnData = strReturnData+"<br>" + "<span class='usertaskstdHeading'>Notes :</span>";
        }else{
            strReturnData = "<span class='usertaskstdHeading'>Notes :</span>";
        }

        if (tmpArray[0].notes && tmpArray[0].notes.length > 0 && tmpArray[0].notes != "null") {
            strReturnData = strReturnData + " " + tmpArray[0].notes;
        }

        strReturnData = strReturnData+"<br>" +"<span class='usertaskstdHeading'>Remarks : </span> ";
        if (tmpArray[0].remarks && tmpArray[0].remarks.length > 0 && tmpArray[0].remarks != "null") {
            strReturnData = strReturnData + " " + tmpArray[0].remarks ;
        }
        strReturnData = strReturnData+"<br>"  + "<span class='usertaskstdHeading'>Actioned Date :</span>";
        if (tmpArray[0].actionedDate && tmpArray[0].actionedDate != "null") {
            var dateED = new Date(GetDateTimeEditDay(tmpArray[0].actionedDate));
            var dayED = dateED.getDay();
            var dowED = getWeekDayName(dayED);

            var dayWeek = dowED + " " + GetDateTimeEdit(tmpArray[0].actionedDate);
            strReturnData = strReturnData + " " +dayWeek ;
        }
        strReturnData = strReturnData +"<br>" + "<span class='usertaskstdHeading'>Modified Date :</span>";
        if (tmpArray[0].actionedModifiedDate && tmpArray[0].actionedModifiedDate != "null") {
            var dateED = new Date(GetDateTimeEditDay(tmpArray[0].actionedModifiedDate));
            var dayED = dateED.getDay();
            var dowED = getWeekDayName(dayED);

            var dayWeek = dowED + " " + GetDateTimeEdit(tmpArray[0].actionedModifiedDate);
            strReturnData = strReturnData + "  " + dayWeek;
        }
        strReturnData = strReturnData + dot;
    }
    return strReturnData;
}


    function getDataForCellNew(appointmentId, activityTypeId,activityId,patientAppointments) {
        var strReturnData = "";
        var tmpArray = patientAppointments.filter(function (index, item) {
            // return item.appointmentId == 1548 && item.activityTypeId == 5 && item.activityId == 11;
            if (index.appointmentId == appointmentId && index.activityTypeId == activityTypeId && index.activityId == activityId) {
                return index;
            }
        });
        // var tmpArray =patientAppointments;
        var fileURL, fileimg;
        if (tmpArray && tmpArray.length > 0) {

            // if (tmpArray[0].review == null || tmpArray[0].review == 0) {
            //     // dot = "greenDot";
            //     dot = '<img  src="../../img/report/icon-Green-circle.png" id="imgApptAct" style="padding-left:30px;width:50px;height:18px;float:right">s</img>';
            // } else if (tmpArray[0].review == 1) {
            //     // dot = "orangeDot";
            //     dot = '<img  src="../../img/report/icon-orange-circle.png" id="imgApptAct" style="padding-left:30px;width:50px;height:18px;float:right"></img>';
            // } else if (tmpArray[0].review == 2) {
            //     // dot = "redDot";
            //     dot = '<img  src="../../img/report/icon-red-circle.png" id="imgApptAct" style="padding-left:30px;width:50px;height:18px;float:right"></img>';
            //
            // }
            // if (tmpArray[0].filePresent) {
            //     fileURL = ipAddress + "/homecare/download/patient-appointment-activities/?access_token=" + sessionStorage.access_token + "&id=" + tmpArray[0].id + "&tenant=" + sessionStorage.tenant;
            //     fileimg = '<img  src="' + fileURL + '" id="imgApptAct" style="padding:5px;padding-left:30px;width:100px;height:64px;float:right"></img>';
            //     strReturnData = fileimg;
            //
            // }

            // strReturnData = strReturnData+"<br>" + "<b>Review : </b>";
            // if (tmpArray[0].review && tmpArray[0].review != "null") {

            // }

            //
            // if(strReturnData != ""){
            //     strReturnData = strReturnData+"<br>" + "<span class='usertaskstdHeading'>Notes :</span>";
            // }else{
            //     strReturnData = "<span class='usertaskstdHeading'>Notes :</span>";
            // }
            //
            // if (tmpArray[0].notes && tmpArray[0].notes.length > 0 && tmpArray[0].notes != "null") {
            //     strReturnData = strReturnData + " " + tmpArray[0].notes;
            // }
            //
            // strReturnData = strReturnData+"<br>" +"<span class='usertaskstdHeading'>Remarks : </span> ";
            // if (tmpArray[0].remarks && tmpArray[0].remarks.length > 0 && tmpArray[0].remarks != "null") {
            //     strReturnData = strReturnData + " " + tmpArray[0].remarks ;
            // }
            //
            var dayWeek;
            if (tmpArray[0].actionedDate && tmpArray[0].actionedDate != "null") {
                var dateED = new Date(GetDateTimeEditDay(tmpArray[0].actionedDate));
                var dayED = dateED.getDay();
                var dowED = getWeekDayName(dayED);

                dayWeek= dowED + " " + GetDateTimeEdit(tmpArray[0].actionedDate);
            }

            var tableData = "<tr><td>" + tmpArray[0].activity + "<span class='astrick'>*</span></td><td>" + dayWeek + "</td><td>" + tmpArray[0].notes + "</td></tr>";
            //strReturnData = strReturnData + dot;
        }
        return tableData;
    }


function onClickServiceUser(){
    var popW = 800;
    var popH = 450;

    if(parentRef)
        parentRef.searchZip = true;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Service User";
    devModelWindowWrapper.openPageWindow("../../html/reports/patientList.html", profileLbl, popW, popH, true, onCloseServiceUserAction);
}
function onCloseServiceUserAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        selectPatient = returnData.selItem;
        $('#txtSU').val(returnData.selItem.lastName + " "+ returnData.selItem.firstName+" "+returnData.selItem.middleName);
        getAjaxObject(ipAddress + "/patient/" + selectPatient.PID+"?access_token="+sessionStorage.access_token, "GET", onGetPatientInfo, onError);
    }
}

function onGetPatientInfo(dataObj){

    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            if (dataObj.response.communication) {
                if (dataObj.response.communication.address1) {
                    selectPatient.address1 =dataObj.response.communication.address1;
                }
                if (dataObj.response.communication.address2) {
                    selectPatient.address2 =dataObj.response.communication.address2;
                }
                if (dataObj.response.communication.homePhone) {
                    selectPatient.homePhone =dataObj.response.communication.homePhone;
                }
                if (dataObj.response.communication.cellPhone) {
                    selectPatient.cellPhone =dataObj.response.communication.cellPhone;
                }
            }
        }
    }

}


var sortByDateAsc = function (date1, date2) {
    // This is a comparison function that will result in dates being sorted in
    // ASCENDING order. As you can see, JavaScript's native comparison operators
    // can be used to compare dates. This was news to me.

    date1 = new Date(date1);
    date2 = new Date(date2);

    if (date1 > date2) return 1;
    if (date1 < date2) return -1;
    return 0;
};

var sortByDateDec = function (date1, date2) {
    // This is a comparison function that will result in dates being sorted in
    // DESCENDING order.
    date1 = new Date(date1);
    date2 = new Date(date2);
    if (date1 > date2) return -1;
    if (date1 < date2) return 1;
    return 0;
};

var daysOfWeek = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

function formatDate(date) {

    var dd = date.getDate();

    var mm = date.getMonth() + 1;
    var yyyy = date.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }

    if (mm < 10) {
        mm = '0' + mm;
    }

    return dd+ '/' + mm + '/' + yyyy;
}

function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}

function convertDateToString(date) {
    if (Object.prototype.toString.call(date) === "[object Date]") {


        var strDate = formatDate(date);
        var strTime = formatAMPM(date);

        return strDate + " " + strTime;
    }
    else{
        return "";
    }
}


function clearPatientAndFilterDetails() {
    $("#lblName").text("");
    $("#lblAddress1").text("");
    $("#lblAddress2").text("");
    $("#lblLandLine").text("");
    $("#lblMobile").text("");
    $("#lblFromDate").text("");
    $("#lblToDate").text("");
}

function clearFilterSelection() {
    selectPatient = null;
    $("#txtSU").val("");
    $('#cmbTaskReports').multipleSelect('uncheckAll');

    //$('#cmbTaskReports').closest('.ui-igcombo').find('.ui-igcombo-clearicon').trigger('click');
    $("#txtFromDate").kendoDatePicker({ format: dtFMT, value: new Date() });
    $("#txtToDate").kendoDatePicker({ format: dtFMT, value: new Date() });

}

function onClickPrint() {

    if ($.trim($('#divTaskReportsViewer').html()) !== "") {

        var contents = document.getElementById("divPrint").outerHTML;// $("#divTaskReportsViewer").html();
        var frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({ "position": "absolute", "top": "-1000000px" });
        $("body").append(frame1);
        var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        //Create a new HTML document.
        frameDoc.document.write('<html><head><title>DIV Contents</title>');
        frameDoc.document.write('</head><body class="bodyClass">');
        frameDoc.document.write('<div class="col-xs-12 ibox"> <div class="col-xs-12 ibox-content"> <div class="col-xs-12 formArea">');
        //Append the external CSS file.
        //frameDoc.document.write('<link href="../../css/external/css/bootstrap.min.css" rel="stylesheet" type="text/css" />');
        //frameDoc.document.write('<link href="../../css/external/css/allcss.css" rel="stylesheet" type="text/css" />');
        frameDoc.document.write('<link href="../../css/report/taskReport.css" rel="stylesheet" type="text/css" />');



        //Append the DIV contents.
        frameDoc.document.write(contents);
        frameDoc.document.write('</div> </div> </div>');
        frameDoc.document.write('</body></html>');




        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();
        }, 500);
    }
    else {
        customAlert.info("info","No data to print.");
    }
}




function sortByReportDisplayName(a, b) {

    var reportDisplayNameA = a.name.toUpperCase();
    var reportDisplayNameB = b.name.toUpperCase();

    var comparison = 0;
    if (reportDisplayNameA > reportDisplayNameB) {
        comparison = 1;
    } else if (reportDisplayNameA < reportDisplayNameB) {
        comparison = -1;
    }
    return comparison;

}

function fnctaskReports(){
    getAjaxObject(ipAddress+"/homecare/task-reports/?is-active=1&is-deleted=0&access_token="+sessionStorage.access_token+"&fields=*,activity.*","GET",getTaskReportsSCB,onError);
}


function getTaskReportsSCB(dataObj) {
    var tempTaskReports = [];
    compType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.taskReports){
            if($.isArray(dataObj.response.taskReports)){
                tempTaskReports = dataObj.response.taskReports;
            }else{
                tempTaskReports.push(dataObj.response.taskReports);
            }
        }
    }

    // masterTaskReports = tempTaskReports;
    var lookup = {};
    var items = tempTaskReports;
    var result = [];

    for (var item, i = 0; i<items.length;i++) {
        item = items[i];
        var name = item.name;

        if (!(name in lookup)) {
            lookup[name] = 1;
            result.push(name);
        }
    }

    masterTaskReports =items;

    var filterArray = [];

    if (result != null && result.length > 0){

        for(var counter=0;counter<result.length;counter++){
            var tempObj=_.where(masterTaskReports, {name: result[counter]});

            if (tempObj !== null && tempObj.length > 0 && tempObj[0].name && tempObj[0].name !== "") {
                filterArray.push(tempObj[0]);
            }

        }
    }
}

function bindTaskReportColumns2(activityTypeId){
    // $("#ulTaskReportColumns").empty();
    // $("#ulTaskComponents").empty();

    var filterArray = [];
    if (masterTaskReports != null && masterTaskReports.length > 0) {
        //if (activityTypeId != null) {
        if (operation == UPDATE) {
            var reportName = reportDisplayName;
            //filterArray = _.where(masterTaskReports, { activityTypeId: activityTypeId });
            filterArray = _.where(masterTaskReports, { name: reportName });
        }

        //}
    }

    if (filterArray !== null && filterArray.length > 0 && masterTaskGroups !== null && masterTaskGroups.length > 0) {
        for(var c = 0; c < filterArray.length ; c++){

            var tmpTaskGroup = _.where(masterTaskGroups, {idk: filterArray[c].activityTypeId});

            if (tmpTaskGroup !== null && tmpTaskGroup.length > 0) {
                filterArray[c].taskGroupName = tmpTaskGroup[0].reportDisplayName;
            }

        }
    }


    return filterArray;
}
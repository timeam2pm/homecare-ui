var angularPTUIgridWrapper = null;
var dataArray = [];
var appReport = angular.module('appReport', []);

appReport.controller('reportController', function($scope) {
	$scope.getData = function () {
        onClickPtViews();
	}
    $scope.printTable = function () {

        var divToPrint = document.getElementById("carertable");

        var htmlToPrint = '' +
            '<style type="text/css">' +
            'table , tr, td, th{'+
    			'border:1px solid #000'+
    			'font-weight:bold'+
    			'font-size:13px'+
    		'}'+
            'table{' +
            'font-family: arial, sans-serif'+
            'border-collapse: collapse'+
            'width: 100%'+
			';' +
            'padding;0.5em;' +
            'border-collapse:collapse;'+
            '}' +
            '</style>';
        htmlToPrint += divToPrint.outerHTML;

        // var newWin = window.open("");
        // newWin.document.write(htmlToPrint);
        // newWin.document.close();
        // newWin.focus();
        // newWin.print();
        // newWin.close();

        // var newWin  = window.open("");
        // newWin.document.body.innerHTML = htmlToPrint;
        // newWin.focus();
        // newWin.print();
        // newWin.close();


        window.frames["print_frame"].document.body.innerHTML = htmlToPrint;
        window.frames["print_frame"].window.focus();
        window.frames["print_frame"].window.print();

    }
});


$(document).ready(function(){
    $("#divMain",parent.document).css("display","none");
    $("#divPatInfo",parent.document).css("display","");
    $("#lblTitleName",parent.document).html("Invoice Report");
});

$(window).load(function(){
	parentRef = parent.frames['iframe'].window;
	onMessagesLoaded();
});
function adjustHeight(){
	var defHeight = 160;
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 100;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    try{angularPTUIgridWrapper.adjustGridHeight(cmpHeight);}catch(e){};
    
}
function onMessagesLoaded() {
	buttonEvents();
	init();
}
var cntry = "";
var dtFMT = "dd/MM/yyyy";
var stDate = "";
var endDate = "";

function init(){
	//patientId = parentRef.patientId;
	//$("#dtFromDate").kendoDatePicker({value:new Date()});
	//$("#dtToDate").kendoDatePicker({value:new Date()});

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();


	$("#txtFacility").kendoComboBox();
	cntry = sessionStorage.countryName;
	//getFacilityList();
	 if(cntry.indexOf("India")>=0){
		 dtFMT = INDIA_DATE_FMT;
	 }else  if(cntry.indexOf("United Kingdom")>=0){
		 dtFMT = ENG_DATE_FMT;
	 }else  if(cntry.indexOf("United State")>=0){
		 dtFMT = US_DATE_FMT;
	 }
	 
	 var currDate = kendo.toString(new Date(), dtFMT);
	 $("#txtRunOn").val(currDate);
	$("#txtStartDate").kendoDatePicker({format:dtFMT});
	$("#txtEndDate").kendoDatePicker({format:dtFMT});
	
	startDT = $("#txtStartDate").kendoDatePicker({
         change: startChange,format:dtFMT,value:new Date()
     }).data("kendoDatePicker");

     endDT= $("#txtEndDate").kendoDatePicker({
         change: endChange,format:dtFMT,value:new Date(),max:new Date()
     }).data("kendoDatePicker");

     endDT.min(new Date());

    // $("#txtStartDate").kendoDatePicker({value:new Date()});
    // $("#txtEndDate").kendoDatePicker({value:new Date()});
     // var dataOptionsPT = {
		//         pagination: false,
		//         paginationPageSize: 500,
		// 		changeCallBack: onPTChange
		//     }
     // angularPTUIgridWrapper = new AngularUIGridWrapper("careerGrid1", dataOptionsPT);
	  //   angularPTUIgridWrapper.init();
		//  buildPatientRosterList([]);
		 
     // adjustHeight();
     
     getBillPlans();
	 
    
}
function getBillPlans(){
	var ipUrl = "";
	ipUrl = ipAddress+"/carehome/bill-to/?is-active=1&is-deleted=0&fields=*,whomToBill.value";
	getAjaxObject(ipUrl,"GET",getBillDataList,onError);
}
function getBillDataList(dataObj){
	console.log(dataObj);
	var dataArray = [];
	if(dataObj){
		if($.isArray(dataObj.response.billTo)){
			dataArray = dataObj.response.billTo;
		}else{
			dataArray.push(dataObj.response.billTo);
		}
	}
	if(dataArray.length>0){
		dataArray.unshift({id:"",name:""});
	}
	setDataForSelection(dataArray, "txtBillName", onFacilityChange, ["name", "id"], 0, "");
	onClickPtViews();
	
}
function buildPatientRosterList(dataSource){
	var gridColumns = [];
	var otoptions = {};
	otoptions.noUnselect = false;
	otoptions.rowHeight = 100; 
	gridColumns.push({
        "title": "Carer",
        "field": "carer",
    });
	  gridColumns.push({
	        "title": "Service User",
	        "field": "ServiceUser",
	    });
	  gridColumns.push({
	        "title": "Service User Location",
	        "field": "address",
	        "width":"25%"
	    });
	  gridColumns.push({
	        "title": "Appointment Time",
	        "field": "appTime",
	        "width":"15%"
	    });
	  gridColumns.push({
	        "title": "Time In",
	        "field": "inTime",
	    });
	  gridColumns.push({
	        "title": "In GPS Location",
	        "field": "inLocation",
	    });
	  gridColumns.push({
	        "title": "Time Out",
	        "field": "outTime",
	    });
	  gridColumns.push({
	        "title": "Out GPS Location",
	        "field": "outLocation",
	    });
	   gridColumns.push({
	        "title": "Actual Visit Time",
	        "field": "visitTime",
	    });
 
    angularPTUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
}
function getReportDataList(dataObj){

        console.log(dataObj);
    dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            if (dataObj.response.invoiceRecords) {
                if ($.isArray(dataObj.response.invoiceRecords)) {
                    dataArray = dataObj.response.invoiceRecords;
                } else {
                    dataArray.push(dataObj.response.invoiceRecords);
                }
            }
        }
    }
    $("#carertable").html("");
    if (dataArray.length > 0) {
    	  var strTable = '<table class="context-menu-one" style="border:1px solid #000">';
        for (var d = 0; d < dataArray.length; d++) {
        	var dItem = dataArray[d];
        	if(dItem){
        		var dArr = [];
        		if(dItem.appointments){
        			if($.isArray(dItem.appointments)){
        				dArr = dItem.appointments;
        			}else{
        				dArr.push(dItem.appointments);
        			}
        		}
        		strTable = strTable+'<tr style="border:1px solid #000"><td style="border:1px solid #000" colspan="3">';
        		if(dItem.patient){
        			var patient = dItem.patient;
        			var strPatient = "<b>Service User Name</b><br><br>";
        			strPatient = strPatient+patient.fristName+" "+patient.middleName+" "+patient.lastName+"<br>";
        			strPatient = strPatient+patient.address1+"  "+patient.address2+"<br>";
        			strPatient = strPatient+patient.city+","+patient.state+","+patient.country+"<br>";
        			strTable = strTable+strPatient;
        		}
        		strTable = strTable+'</td>';
        		strTable = strTable+'<td style="border:1px solid #000" colspan="3">';
        		if(dItem.billTo){
        			var billTo = dItem.billTo;
        			var strBill = "<b>Bill to Name</b><br><br>";
        			strBill = strBill+billTo.name+"<br>";
        			strBill = strBill+patient.address1+"  "+patient.address2+"<br>";
        			strBill = strBill+patient.city+","+patient.state+","+patient.country+"<br>";
        			strTable = strTable+strBill;
        		}
        		strTable = strTable+'</td>';
        		strTable = strTable+'</tr>';
        		//strTable = strTable+'<thead>';
          	  strTable = strTable+'<tr>';
          	  strTable = strTable+'<th style="text-align: center;border:1px solid #000" class="trBgColor">Appointment Date & Time</th>';
          	  strTable = strTable+'<th style="text-align: center;border:1px solid #000" class="trBgColor">Contract Category</th>';
          	  strTable = strTable+'<th style="text-align: center;border:1px solid #000" class="trBgColor">Contract Rate Type</th>';
          	  strTable = strTable+'<th style="text-align: center;border:1px solid #000" class="trBgColor">Duration (mins) / Session</th>';
          	  strTable = strTable+'<th style="text-align: center;border:1px solid #000" class="trBgColor">Rate</th>';
          	  strTable = strTable+'<th style="text-align: center;border:1px solid #000" class="trBgColor">Amount</th>';
          	  strTable = strTable+'</tr>';
          	  //strTable = strTable+'</thead>';
          	  var amt = 0;
        		for(var j=0;j<dArr.length;j++){
        			var jItem = dArr[j];
        			if(jItem){
        				strTable = strTable+'<tr style="border:1px solid #000">';
        				if(jItem.appointmentDate){
        					strTable = strTable+'<td style="border:1px solid #000">'+jItem.appointmentDate+'</td>';
        				}else{
        					strTable = strTable+'<td style="border:1px solid #000"></td>';
        				}
        				if(jItem.shiftValue){
        					strTable = strTable+'<td style="border:1px solid #000">'+jItem.shiftValue+'</td>';
        				}else{
        					strTable = strTable+'<td style="border:1px solid #000"></td>';
        				}
        				if(jItem.billingRateType){
        					strTable = strTable+'<td style="border:1px solid #000">'+jItem.billingRateType+'</td>';
        				}else{
        					strTable = strTable+'<td style="border:1px solid #000"></td>';
        				}
        				if(jItem.duration){
        					strTable = strTable+'<td style="border:1px solid #000">'+jItem.duration+'</td>';
        				}else{
        					strTable = strTable+'<td style="border:1px solid #000"></td>';
        				}
        				if(jItem.patientBillingRate){
        					strTable = strTable+'<td style="border:1px solid #000;text-align:right">'+jItem.patientBillingRate.toFixed(2)+'</td>';
        				}else{
        					strTable = strTable+'<td style="border:1px solid #000"></td>';
        				}
        				if(jItem.rate){
        					strTable = strTable+'<td style="border:1px solid #000;text-align:right">'+jItem.rate.toFixed(2)+'</td>';
        					amt = amt+Number(jItem.rate);
        				}else{
        					strTable = strTable+'<td style="border:1px solid #000"></td>';
        				}
        				
        				strTable = strTable+'</tr>';
        			}
        		}
        		amt = amt.toFixed(2);
        		strTable = strTable+'<tr style="border:1px solid #000"><td></td><td></td><td></td><td></td><td colspan="2" style="text-align:right">Total :'+amt+'</td></tr>';
        	}
        }
        strTable = strTable+'</tbody></table>';
        $("#carertable").html(strTable);
    }


   /* var elem = angular.element(document.querySelector('[ng-app]'));
    var injector = elem.injector();
    var $rootScope = injector.get('$rootScope');
    $rootScope.$apply(function(){
        $rootScope.apidata = dataArray;
    });*/




	// buildPatientRosterList(dataArray);
}
function secondsToHms(d) {
    d = Number(d);
    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    var hDisplay = h > 0 ? h + (h == 1 ? " hour, " : " hours, ") : "";
    var mDisplay = m > 0 ? m + (m == 1 ? " minute, " : " minutes, ") : "";
    var sDisplay = s > 0 ? s + (s == 1 ? " second" : " seconds") : "";
    return hDisplay + mDisplay + sDisplay; 
}
function onPTChange(){
	
}
function startChange() {
    var startDate = startDT.value(),
    endDate = endDT.value();

    if (startDate) {
        startDate = new Date(startDate);
        startDate.setDate(startDate.getDate());
        endDT.min(startDate);
    } else if (endDate) {
        startDT.max(new Date(endDate));
    } else {
        endDate = new Date();
        startDT.max(endDate);
        endDT.min(endDate);
    }
}

function endChange() {
    var endDate = endDT.value(),
    startDate = startDT.value();

    if (endDate) {
        endDate = new Date(endDate);
        endDate.setDate(endDate.getDate());
        startDT.max(endDate);
    } else if (startDate) {
        endDT.min(new Date(startDate));
    } else {
        endDate = new Date();
        startDT.max(endDate);
        endDT.min(endDate);
    }
}
function buttonEvents(){
	$("#btnSearch1").off("click",onClickSearch);
	$("#btnSearch1").on("click",onClickSearch);
	
	$("#btnPtView").off("click",onClickPtViews);
	$("#btnPtView").on("click",onClickPtViews);
	
	$("#btnPrint").off("click",onClickPrint);
	$("#btnPrint").on("click",onClickPrint);
}
function onClickSearch(){
	 var popW = "60%";
	    var popH = 500;

	    var profileLbl;
	    var devModelWindowWrapper = new kendoWindowWrapper();
	    profileLbl = "Search Patient";
		if(sessionStorage.clientTypeId == "2"){
			 profileLbl = "Search Client";
		}
	    devModelWindowWrapper.openPageWindow("../../html/patients/searchPatient.html", profileLbl, popW, popH, true, closePtAddAction);
}
function closePtAddAction(evt,returnData){
	if(returnData && returnData.status == "success"){
		console.log(returnData);
		var pID = returnData.selItem.PID;
		viewPatientId = pID;
		var pName = returnData.selItem.FN+" "+returnData.selItem.MN+" "+returnData.selItem.LN;
		var viewpName = pName;
		if(pID != ""){
			//$("#lblName").text(pID+" - "+pName);
			$("#txtPatient").val(pName);
			/*var imageServletUrl = ipAddress + "/download/patient/photo/" + pID + "/?" + Math.round(Math.random() * 1000000);
	        $("#imgPhoto").attr("src", imageServletUrl);
	        
	        getAjaxObject(ipAddress + "/patient/" + pID, "GET", onGetPatientInfo, onError);
		}*/
	}
}
}
function getFacilityList(){
	getAjaxObject(ipAddress+"/facility/list/?is-active=1","GET",getFacilityDataList,onError);
}
function getFacilityDataList(dataObj){
	var dataArray = [];
	if(dataObj){
		if($.isArray(dataObj.response.facility)){
			dataArray = dataObj.response.facility;
		}else{
			dataArray.push(dataObj.response.facility);
		}
	}
	var tempDataArry = [];
	for(var i=0;i<dataArray.length;i++){
		dataArray[i].idk = dataArray[i].id; 
		dataArray[i].Status = "InActive";
		if(dataArray[i].isActive == 1){
			dataArray[i].Status = "Active";
		}
	}
	setDataForSelection(dataArray, "txtFacility", onFacilityChange, ["name", "idk"], 0, "");
}
function onFacilityChange(){
	
}
function onError(err){
	
}
function onClickPrint(){
	angularPTUIgridWrapper.printDataGrid();
}
function onClickPtViews(){
	var startDT = $("#txtStartDate").data("kendoDatePicker");
	var endDT = $("#txtEndDate").data("kendoDatePicker");
	var txtBillName = $("#txtBillName").data("kendoComboBox");
	
	var sDate = new Date(startDT.value());
	sDate.setHours(0);
	sDate.setMinutes(0);
	sDate.setSeconds(0);
	
	var eDate = new Date(endDT.value())
	eDate.setHours(23);
	eDate.setMinutes(59);
	eDate.setSeconds(59);
	
	var ipUrl = "";
	if(txtBillName.text() != ""){
		ipUrl = ipAddress+"/homecare/reports/invoice/?date-of-appointment=:bt:"+sDate.getTime()+","+eDate.getTime()+"&bill-to-name="+txtBillName.text();
	}else{
		ipUrl = ipAddress+"/homecare/reports/invoice/?date-of-appointment=:bt:"+sDate.getTime()+","+eDate.getTime();
	}
	
	// buildPatientRosterList([]);
	getAjaxObject(ipUrl,"GET",getReportDataList,onError);
}

$(document).ready(function() {
	var topicalApplication = {
		init: function() {
			allowNumerics('patientAppointmentId-input');
			$(".datepicker").kendoDatePicker();
			$('.datepicker').on('keyup keypress', function(e) {
				e.preventDefault();
			});
			topicalApplication.bindEvents();
			var doc = new jsPDF();
            var specialElementHandlers = {
                '#editor': function (element, renderer) {
                    return true;
                }
            };
            $('#saveAsPdf').click(function () {   
                doc.fromHTML($('#tableAppointmentContent').html(), 15, 15, {
                    'width': 170,
                        'elementHandlers': specialElementHandlers
                });
                doc.save('patient-activity.pdf');
            });
		},
		bindEvents: function() {
			$('#patientAppointmentId-link').on('click', function() {
				var searchAppVal = 1524;//$('#patientAppointmentId-input').val();
				if(searchAppVal != "") {
					$('#activitiesPeformed tbody').empty();
					getAjaxObjectAsync(ipAddress + "/appointment/list/?is-active=1&is-deleted=0&id=" + searchAppVal,"GET",buildAppointmentList,onError);
					getAjaxObjectAsync(ipAddress + "/patient/appointment/activity/list/?is-active=1&is-deleted=0&appointment-id=" + searchAppVal,"GET",buildpatientAppointmentActivities,onError);
					getAjaxObjectAsync(ipAddress + "/patient/appointment/medication-activity/list/?is-active=1&is-deleted=0&appointment-id=" + searchAppVal,"GET",patientAppointmentMedicationActivity,onError);
				}
			});
			var dataMedicateList = ['/provider','/facility'];
			$.each(dataMedicateList,function(i,value) {
				var urlExtn = value + '/list/';
				retrieveSelectData(value,urlExtn);
			});
			function retrieveSelectData(formName,url) {
				$('.medicationPrescription-select').each(function() {
					var selfVal = $(this).attr('medicate-pres-select');
					var $selectVal = $(this).attr('medicate-pres-select');
					if($selectVal != "Action" && $selectVal != "Status" && $selectVal != "Transmission" && $selectVal != "Facility Name" && $selectVal != "Prescribed By") {
						$(this).html('<option value="" disabled selected>'+selfVal+'</option>');
					} else {
						$(this).html('');
					}
				});
				getAjaxObject(ipAddress+url+'?is-active=1',"GET", onRetreiveSelectDataSuccess, onError);
				function onRetreiveSelectDataSuccess(resp) {
					var medicationPresData = [], dataArr = [];
					if(formName == '/provider' && resp && resp.response && resp.response.provider) {
						var providersData = resp.response.provider;
						for (var i = 0; i < providersData.length; i++) {
							if(providersData[i].type == 100) {
								var provideObj = {
									"id": providersData[i].id,
									"abbreviation": providersData[i].abbreviation,
									"firstName": providersData[i].firstName,
									"lastName": providersData[i].lastName
								}
								$('#medication-prescribedBy-list').append('<option data-prescription-val="'+provideObj.id+'">'+provideObj.abbreviation+' '+provideObj.firstName+','+provideObj.lastName+'</option>');
							}
						}
					}
					else if(formName == '/facility' && resp && resp.response && resp.response.facility) {
						var facilityData = resp.response.facility;
						globalFacilityData = resp.response.facility;
						for (var i = 0; i < facilityData.length; i++) {
							var facilityObj = {
								"id": facilityData[i].id,
								"displayName": facilityData[i].displayName
							}
							$('#medication-facilityName-list').append('<option data-prescription-val="'+facilityObj.id+'">'+facilityObj.displayName+'</option>');
						}
					}
				}
			}
			function onError(errObj){
				console.log(errObj);
				customAlert.error("Error","Error");
			}


			function buildAppointmentList(dataObj) {
				console.log(dataObj);
				var dataArray = [];
			    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1" && dataObj.response.appointment){
			        if($.isArray(dataObj.response.appointment)){
			            dataArray = dataObj.response.appointment;
			        }else{
			            dataArray.push(dataObj.response.appointment);
			        }
			    }
			    if(dataArray.length > 0) {
			    	var patientObj = dataArray[0].composition.patient ? dataArray[0].composition.patient : '';
			    	if(patientObj != "") {
			    		$('[name="patient-appointment-name"]').text(patientObj.firstName + patientObj.middleName + patientObj.lastName);
			    		if(patientObj.dateOfBirth) {
			    			$('[name="patient-appointment-dob"]').text(patientObj.dateOfBirth);
			    			$('[name="patient-appointment-age"]').text(topicalApplication._calculateAge(new Date(patientObj.dateOfBirth)));
			    		}
			    		$('[name="patient-appointment-gender"]').text(patientObj.gender);
			    		if(dataArray[0].dateOfAppointment) {
			    			$('[name="patient-appointment-date"]').text(new Date(dataArray[0].dateOfAppointment).toLocaleDateString());
			    			$('[name="patient-appointment-time"]').text(new Date(dataArray[0].dateOfAppointment).toLocaleTimeString());
			    		}
			    		if(dataArray[0].composition.provider) {
			    			$('[name="patient-appointment-caregiver"]').text(dataArray[0].composition.provider.firstName + ", " + dataArray[0].composition.provider.lastName);
			    		}
			    		if(dataArray[0].inTime && dataArray[0].outTime) {
			    			$('[name="patient-appointment-intime"]').text(new Date(dataArray[0].inTime).toLocaleTimeString());
			    			$('[name="patient-appointment-outtime"]').text(new Date(dataArray[0].outTime).toLocaleTimeString());
			    		}
			    	}
			    }
			};
			function buildpatientAppointmentActivities(dataObj) {
				console.log(dataObj);
				var dataArray = [];
			    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1" && dataObj.response.patientAppointmentActivities){
			        if($.isArray(dataObj.response.patientAppointmentActivities)){
			            dataArray = dataObj.response.patientAppointmentActivities;
			        } else{
			            dataArray.push(dataObj.response.patientAppointmentActivities);
			        }
			    }
			    if(dataArray.length > 0) {
			    	var activityTypeArr = [];
			    	for (var i = 0; i < dataArray.length; i++) {
			    		activityTypeArr.push(dataArray[i].activityType);
			    	}
					var activityTypeArrFinal = activityTypeArr.reduce(function(p,c,i,activityTypeArr){
					  if (p.indexOf(c) == -1) p.push(c);
					  else p.push('')
					  return p;
					}, [])
			    	for (var i = 0; i < dataArray.length; i++) {
			    		$('#activitiesPeformed tbody').append('<tr><td name="patient-appointment-activityName">'+activityTypeArrFinal[i]+'</td><td name="patient-appointment-activity">'+dataArray[i].activity+'</td><td name="patient-appointment-activityNotes">'+dataArray[i].notes+'</td></tr>');
			    	}
				}
			}
			function patientAppointmentMedicationActivity(dataObj) {
				console.log(dataObj);
				var dataArray = [];
			    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1" && dataObj.response.patientAppointmentMedicationActivity) {
			        if($.isArray(dataObj.response.patientAppointmentMedicationActivity)){
			            dataArray = dataObj.response.patientAppointmentMedicationActivity;
			        } else{
			            dataArray.push(dataObj.response.patientAppointmentMedicationActivity);
			        }
			    }
			    if(dataArray.length > 0) {
			    	for (var i = 0; i < dataArray.length; i++) {
			    		var dateString = new Date(dataArray[i].administrationTime).toLocaleDateString();
			    		var timeString = new Date(dataArray[i].administrationTime).toLocaleTimeString();
			    		$('#activitiesPeformed tbody').append('<tr><td name="patient-appointment-medicationTitle">Meds</td><td><p><span name="patient-appointment-medicationTradeName">'+dataArray[i].medicationTradeName+'</span><span name="patient-appointment-strength">'+dataArray[i].strength+'</span><span name="patient-appointment-medicationStrength">'+dataArray[i].medicationStrength+'</span></p><p><span name="patient-appointment-take">'+dataArray[i].take+'</span><span name="patient-appointment-medicationForm">'+dataArray[i].medicationForm+'</span><span name="patient-appointment-medicationFrequency">'+dataArray[i].medicationFrequency+'</span><span name="patient-appointment-medicationRoute">'+dataArray[i].medicationRoute+'</span></p><p><span name="patient-appointment-medicationInstructions">'+dataArray[i].medicationInstructions+'</span></p><p><span name="patient-appointment-instruction">'+dataArray[i].instruction+'</span></p></td><td name="patient-appointment-activityNotes"><p><span name="patient-appointment-administrationTime">' + dateString + ' '+ timeString +'</span></p><p><span name="patient-appointment-patientMedicationAction">'+dataArray[i].patientMedicationAction+'</span></p><p><span name="patient-appointment-notes">'+dataArray[i].notes+'</span></p></td></tr>');
			    	}
			    	$('.tableWrapper-appointment').removeClass('hideTable');
				}
			}
			function onError(errObj) {
				console.log(errObj);
    			customAlert.error("Error", "Error");
			}
		}
	};
	topicalApplication.init();
});
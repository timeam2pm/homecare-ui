var angularUIgridWrapper = AngularUIGridWrapper();
//var ipAddress = "http://stage.timeam.com";
var  stArray = [{key:'ACTIVE',text:'ACTIVE'},{key:'INACTIVE',text:'INACTIVE'},{key:'BOTH',text:'BOTH'}];

var searchData = [{key:'4',text:'Last Name'},{key:'1',text:'All Service Users'},{key:'2',text:'Service User Id'},{key:'3',text:'First Name'},{key:'5',text:'Last Name, First Name'}];
$(document).ready(function(){
});

$(window).load(function(){
	$("#cmbStatus").kendoComboBox();
	$("#txtPatientId").kendoComboBox();
	
	$("#cmbStatus").data("kendoComboBox").input.attr("placeholder", "Status");
	$("#txtPatientId").data("kendoComboBox").input.attr("placeholder", "Search Parameters");
	
	setDataForSelection(stArray, "cmbStatus", onStatusChange, ["text", "key"], 0, "");
	setDataForSelection(searchData, "txtPatientId", onPTChange, ["text", "key"], 0, "");
	
	onMessagesLoaded();
});

function onStatusChange(){
	var cmbZip = $("#cmbZip").data("kendoComboBox");
	if(cmbZip && cmbZip.selectedIndex<0){
		cmbZip.select(0);
	}
}
function onPTChange(){
	var txtPatientId = $("#txtPatientId").data("kendoComboBox");
	if(txtPatientId && txtPatientId.selectedIndex<0){
		txtPatientId.select(0);
	}
}
	function onMessagesLoaded() {
		var dataOptions = {
	        pagination: false,
	        paginationPageSize: 500,
			changeCallBack: onChange
	    }

		angularUIgridWrapper = new AngularUIGridWrapper("dgridZipList", dataOptions);
	    angularUIgridWrapper.init();
	    var dataArray = [];
	    var dataObj = {};
	    buildDeviceTypeGrid(dataArray);
		
		setTimeout(function(){
			$(".searchPanel").hide();
		},100);

	buttonEvents();
	getPatientList();
}
	function getPatientList(){
		var patientListURL = ipAddress+"/homecare/tenant-users/?is-active=1&is-deleted=0";
		 getAjaxObject(patientListURL,"GET",onPatientListData,onErrorMedication);
	}
	function allowNumerics(ctrlId){
		$("#"+ctrlId).keypress(function (e) {
			var txtPatientId = $("#txtPatientId").data("kendoComboBox");
			if(txtPatientId && txtPatientId.value() == 2){
				 if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
						return false;
					}
			}
			
	   });
	}
	function onClickSearch(){
		buildDeviceTypeGrid([]);
		var strSearch = $("#txtSearch").val();
		strSearch = $.trim(strSearch);
		if(strSearch != ""){
			var txtPatientId = $("#txtPatientId").data("kendoComboBox");
			if(txtPatientId){
				var idx = txtPatientId.value();
				if(idx == 1){
					getPatientList();
				}else if(idx == 2){
					patientListURL = ipAddress+"/patient/list/?id="+strSearch;
					 getAjaxObject(patientListURL,"GET",onPatientListData,onErrorMedication);
				}else if(idx == 3){
					patientListURL = ipAddress+"/patient/list/?first-name="+strSearch;
					 getAjaxObject(patientListURL,"GET",onPatientListData,onErrorMedication);
				}else if(idx == 4){
					patientListURL = ipAddress+"/patient/list/?last-name="+strSearch;
					 getAjaxObject(patientListURL,"GET",onPatientListData,onErrorMedication);
				}else if(idx == 5){
					var pArray = strSearch.split(",");
					if(pArray){
						if(pArray.length == 2){
							var lName = pArray[0];
							var fName = pArray[1];
							if(fName && lName){
								patientListURL = ipAddress+"/patient/list/?first-name="+fName+"&last-name="+lName;
								 getAjaxObject(patientListURL,"GET",onPatientListData,onErrorMedication);
							}else if(fName){
								patientListURL = ipAddress+"/patient/list/?first-name="+fName;
								 getAjaxObject(patientListURL,"GET",onPatientListData,onErrorMedication);
							}else if(lName){
								patientListURL = ipAddress+"/patient/list/?last-name="+lName;
								 getAjaxObject(patientListURL,"GET",onPatientListData,onErrorMedication);
							}
						}else if(pArray.length == 1){
							patientListURL = ipAddress+"/patient/list/?first-name="+strSearch;
							 getAjaxObject(patientListURL,"GET",onPatientListData,onErrorMedication);
						}
					}
				}
			}
		}else{
			getPatientList();
		}
	}
	function onPatientListData(dataObj){
		console.log(dataObj);
		var tempArray = [];
		var dataArray = [];
		if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
			if(dataObj.response.tenantUsers){
				if($.isArray(dataObj.response.tenantUsers)){
					tempArray = dataObj.response.tenantUsers;
				}else{
					tempArray.push(dataObj.response.tenantUsers);
				}	
			}
		}else{
			customAlert.error("Error",dataObj.response.status.message.replace('patient','service user'));
		}
		
		for(var i=0;i<tempArray.length;i++){
			var obj = tempArray[i];
			if(obj){
				var dataItemObj = {};
				dataItemObj.PID = obj.id;
				dataItemObj.FN = obj.firstName;
				dataItemObj.LN = obj.lastName;
				dataItemObj.WD = obj.weight;
				dataItemObj.HD = obj.height;
				  if(obj.middleName){
					  dataItemObj.MN = obj.middleName;
				  }else{
					  dataItemObj.MN =  "";
				  }
				  dataItemObj.GR = obj.gender;
				  /*if(obj.gender == "1"){
					  dataItemObj.GR = "Male";
				  }else{
					  dataItemObj.GR = "Female";
				  }*/
				  if(obj.status){
					  dataItemObj.ST = obj.status;
				  }else{
					  dataItemObj.ST = "ACTIVE";
				  }
				  dataItemObj.DOB = kendo.toString(new Date(obj.dateOfBirth),"MM/dd/yyyy");
				  
				  dataArray.push(dataItemObj);
			}
		}
		buildDeviceTypeGrid(dataArray);
	}
	function onErrorMedication(errobj){
		console.log(errobj);
	}
function buttonEvents(){
	$("#btnSave").off("click");
	$("#btnSave").on("click",onClickOK);
	//$("#dgridPatient").on("click",ondblclick);
	
	$("#btnDelete").off("click");
	$("#btnDelete").on("click",onClickDelete);
	
	$("#btnCancelDet").off("click");
	$("#btnCancelDet").on("click",onClickCancel);
	
	$("#btnZipSearch").off("click");
	$("#btnZipSearch").on("click",onClickSearch);
	
	allowNumerics("txtSearch");
}
$(window).resize(adjustHeight);
function adjustHeight(){
	var defHeight = 120;
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 120;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    try{angularUIgridWrapper.adjustGridHeight(cmpHeight);}catch(e){};
    
    var cmpHeight = window.innerHeight - 180;
    $("#divPtInfo").height(cmpHeight);

}
function buildDeviceTypeGrid(dataSource) {
    var gridColumns = [];
	var otoptions = {};
	otoptions.noUnselect = false;
	otoptions.rowHeight = 100; 
   gridColumns.push({
        "title": "ID",
        "field": "PID",
        "enableColumnMenu": false,
    });
    gridColumns.push({
        "title": "Last Name",
        "field": "LN",
        "enableColumnMenu": false,
        "width": "15%",
    });
    gridColumns.push({
        "title": "First Name",
        "field": "FN",
        "enableColumnMenu": false,
    });

	gridColumns.push({
        "title": "Middle Name",
        "field": "MN",
        "enableColumnMenu": false,
        "width": "15%",
    });
	/*gridColumns.push({
        "title": "Date of Birth",
        "field": "DOB",
        "enableColumnMenu": false,
        "width": "17%",
    });*/
	gridColumns.push({
        "title": "Gender",
        "field": "GR",
        "enableColumnMenu": false,
        "width": "10%",
    });
	gridColumns.push({
        "title": "Status",
        "field": "ST",
        "enableColumnMenu": false,
        "width": "15%",
    });
	angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
	adjustHeight();
}
function onChange(){
	setTimeout(function(){
		 var selectedItems = angularUIgridWrapper.getSelectedRows();
		 console.log(selectedItems);
		 if(selectedItems && selectedItems.length>0){
			 $("#btnSave").prop("disabled", false);
			    $("#btnDelete").prop("disabled", false);
		 }else{
			 $("#btnSave").prop("disabled", true);
			    $("#btnDelete").prop("disabled", true);
		 }
		 //var strPatientId = selectedItems[0].PID; 
		// getAjaxObject(ipAddress+"/patient/"+strPatientId,"GET",onGetPatientInfo,onErrorMedication);
	});
}
function onErrorMedication(errobj){
	console.log(errobj);
}
function onGetPatientInfo(dataObj){
	if(dataObj && dataObj.response && dataObj.response.patient){
		//$("#ptDOB",parent.document).html(dataObj.response.patient.dateOfBirth);
		$("#txtID").val(dataObj.response.patient.id);
		$("#txtExtID1").val(dataObj.response.patient.externalId1);
		$("#txtExtID2").val(dataObj.response.patient.externalId2);
		ptExtId2 = dataObj.response.patient.externalId2;
		$("#txtPrefix").val(dataObj.response.patient.prefix);
		$("#txtStatus").val(dataObj.response.patient.status);
		$("#txtNN").val(dataObj.response.patient.nickname);
		$("#txtFN").val(dataObj.response.patient.firstName);
		$("#txtMN").val(dataObj.response.patient.middleName);
		$("#txtLN").val(dataObj.response.patient.lastName);
		var dt = new Date(dataObj.response.patient.dateOfBirth);
		if(dt){
			var strDT = kendo.toString(dt,"MM-dd-yyyy");
			$("#txtDOB").val(strDT);
			
			/*var currDate = new Date();
			console.log(currDate.getFullYear()-dt.getFullYear());
			var strAge = currDate.getFullYear()-dt.getFullYear();*/
			strAge = getAge(dt);
			$("#txtAge").val(strAge);
		}
		var ssn = showSSNNumver(dataObj.response.patient.ssn);
		$("#txtSSN").val(ssn);
		$("#txtWeight").val(dataObj.response.patient.weight);
		$("#txtHeight").val(dataObj.response.patient.height);
		$("#txtGender").val(dataObj.response.patient.gender);
		$("#txtEthnicity").val(dataObj.response.patient.ethnicity);
		$("#txtRace").val(dataObj.response.patient.race);
		$("#txtLan").val(dataObj.response.patient.language);
	}
	if(dataObj && dataObj.response && dataObj.response.communication){
		var commArray = [];
		if($.isArray(dataObj.response.communication)){
			commArray = dataObj.response.communication;
		}else{
			commArray.push(dataObj.response.communication);
		}
		var comObj = commArray[0];
		$("#txtAddr1").val(comObj.address1);
		$("#txtAddr2").val(comObj.address2);
		$("#txtSMS").val(comObj.sms);
		$("#txtCell").val(comObj.cellPhone);
		$("#txtExtension").val(comObj.workPhoneExt);
		$("#txtWORK").val(comObj.workPhone);
		$("#txtHExt").val(comObj.homePhoneExt);
		$("#txtHM").val(comObj.homePhone);
		
		$("#txtCity").val(comObj.city);
		$("#txtState").val(comObj.state);
		$("#txtZip").val(comObj.zip);
		$("#txtZip4").val(comObj.zipFour);
		$("#txtCity").val(comObj.city);
	}
}
function showPtDetails(){
	var node = '<div><spn class="pt">Id: {{row.entity.PID}}</spn><br><spn class="pt">Name: {{row.entity.FN}} &nbsp;&nbsp;{{row.entity.MN}} &nbsp;&nbsp;{{row.entity.LN}}</spn><br>';
	node = node+'<spn class="pt">Gender: {{row.entity.GR}}</spn><br><spn class="pt">Date Of Birth: {{row.entity.DOB}}</spn></div>';
	return node;
}

function onClickOK(){
	setTimeout(function(){
		 var selectedItems = angularUIgridWrapper.getSelectedRows();
		 console.log(selectedItems);
		 if(selectedItems && selectedItems.length>0){
			 var obj = {};
			 obj.selItem = selectedItems[0];
			// var onCloseData = new Object();
			 obj.status = "success";
				var windowWrapper = new kendoWindowWrapper();
				windowWrapper.closePageWindow(obj);
		 }
	})
}
function ondblclick(){
	var selectedItems = angularUIgridWrapper.getSelectedRows();
	console.log(selectedItems);
	if(selectedItems && selectedItems.length>0){
			 var obj = {};
			 obj.selItem = selectedItems[0];
			 obj.status = "success";
				var windowWrapper = new kendoWindowWrapper();
				windowWrapper.closePageWindow(obj);
		 }
}
function onClickDelete(){
	customAlert.confirm("Confirm", "Are you sure you want delete?",function(response){
		if(response.button == "Yes"){
			var selectedItems = angularUIgridWrapper.getSelectedRows();
			 console.log(selectedItems);
			 if(selectedItems && selectedItems.length>0){
				 var selItem = selectedItems[0];
				 if(selItem){
					 var dataUrl = ipAddress+"/patient/delete/";
					 var reqObj = {};
					 reqObj.id = selItem.PID;
					 reqObj.isDeleted = "1";
					 reqObj.modifiedBy = sessionStorage.userId;
					 createAjaxObject(dataUrl,reqObj,"POST",onDeletePatient,onErrorMedication);
				 }
			 }
		}
	});
}
function onDeletePatient(dataObj){
	if(dataObj && dataObj.response && dataObj.response.status){
		if(dataObj.response.status.code == "1"){
			customAlert.info("info", "Patient Deleted Successfully");
			getPatientList();
		}else{
			customAlert.error("error", dataObj.message);
		}
	}
}
/*function getAjaxObject(dataUrl,method,successFunction,errorFunction){
	Loader.showLoader();
	$.ajax({
		  type: method,
		  url: dataUrl,
		  data: null,
		  context: this,
			cache: false,
		  success: function( data, statusCode, jqXHR ){
			  Loader.hideLoader();
			  successFunction(data);
		  },
		  error: function( jqXHR, textStatus, errorThrown ){
			  Loader.hideLoader();
			  errorFunction(jqXHR);
		  },
		  contentType: "application/json",
		});

}*/
function onClickCancel(){
	popupClose(false);
}
function popupClose(st){
	var onCloseData = new Object();
	onCloseData.status = st;
	var windowWrapper = new kendoWindowWrapper();
	windowWrapper.closePageWindow(onCloseData);
}


var angularPTUIgridWrapper = null;
var dataArray = [];
var appReport = angular.module('appReport', []);
var selPatientId;
var selProviderId;
var appointmentIds = [];

appReport.controller('reportController', function($scope) {
    $scope.getData = function () {
        onClickPtViews();

    }
    $scope.printTable = function () {

        // var divToPrint = document.getElementById("carertable");
        // var htmlToPrint = '' +
        //     '<style type="text/css">' +
        //     'table th, table td, table {' +
        //     'border:solid 1px #f3f6f9' +
        //     ';' +
        //     'padding;0.5em;' +
        //     'border-collapse:collapse;'+
        //     '}' +
        //     '</style>';
        // htmlToPrint += divToPrint.outerHTML;
        //
        // // var newWin = window.open("");
        // // newWin.document.write(htmlToPrint);
        // // newWin.document.close();
        // // newWin.focus();
        // // newWin.print();
        // // newWin.close();
        //
        // // var newWin  = window.open("");
        // // newWin.document.body.innerHTML = htmlToPrint;
        // // newWin.focus();
        // // newWin.print();
        // // newWin.close();
        //
        // window.frames["print_frame"].document.body.innerHTML = htmlToPrint;
        // window.frames["print_frame"].window.focus();
        // window.frames["print_frame"].window.print();

    }
});


$(document).ready(function(){
    $("#divMain",parent.document).css("display","none");
    $("#divPatInfo",parent.document).css("display","");
    // $("#lblTitleName",parent.document).html("Carer In Out Report");
    var cntry = sessionStorage.countryName;
    if((cntry.indexOf("India")>=0) || (cntry.indexOf("United Kingdom")>=0)){
        $("#lblFacility").text("Location");
    }else if(cntry.indexOf("United State")>=0) {
        $("#lblFacility").text("Facility");
    }
});

$(window).load(function(){
    parentRef = parent.frames['iframe'].window;
    onMessagesLoaded();
});
function adjustHeight(){
    var defHeight = 160;
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 100;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    try{angularPTUIgridWrapper.adjustGridHeight(cmpHeight);}catch(e){};

}
function onMessagesLoaded() {
    buttonEvents();
    init();
}
var cntry = "";
var dtFMT = "dd/MM/yyyy";
var stDate = "";
var endDate = "";

function init(){
    //patientId = parentRef.patientId;
    //$("#dtFromDate").kendoDatePicker({value:new Date()});
    //$("#dtToDate").kendoDatePicker({value:new Date()});

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();


    cntry = sessionStorage.countryName;
    //getFacilityList();
    if(cntry.indexOf("India")>=0){
        dtFMT = INDIA_DATE_FMT;
    }else  if(cntry.indexOf("United Kingdom")>=0){
        dtFMT = ENG_DATE_FMT;
    }else  if(cntry.indexOf("United State")>=0){
        dtFMT = US_DATE_FMT;
    }
    $("#txtStartDate").kendoDatePicker({format:dtFMT});
    $("#txtEndDate").kendoDatePicker({format:dtFMT});

    startDT = $("#txtStartDate").kendoDatePicker({
        change: startChange,format:dtFMT,value:new Date()
    }).data("kendoDatePicker");

    endDT= $("#txtEndDate").kendoDatePicker({
        change: endChange,format:dtFMT,value:new Date()
    }).data("kendoDatePicker");

    endDT.min(new Date());

    // $("#txtStartDate").kendoDatePicker({value:new Date()});
    // $("#txtEndDate").kendoDatePicker({value:new Date()});
    // var dataOptionsPT = {
    //         pagination: false,
    //         paginationPageSize: 500,
    // 		changeCallBack: onPTChange
    //     }
    // angularPTUIgridWrapper = new AngularUIGridWrapper("careerGrid1", dataOptionsPT);
    //   angularPTUIgridWrapper.init();
    //  buildPatientRosterList([]);
    adjustHeight();
    //onClickPtViews();
    getBillPlans();
}
function getBillPlans() {
	var ipUrl = "";
	ipUrl = ipAddress + "/carehome/bill-to/?is-active=1&is-deleted=0&fields=*,whomToBill.value";
	getAjaxObject(ipUrl, "GET", getBillDataList, onError);
}

var billToNameArr = [];
function getBillDataList(dataObj) {
	var dataArray = [];
	if (dataObj) {
		if ($.isArray(dataObj.response.billTo)) {
			dataArray = dataObj.response.billTo;
		} else {
			dataArray.push(dataObj.response.billTo);
		}
	}
	if (dataArray.length > 0) {
		dataArray.unshift({ id: "", name: "" });
	}

	dataArray.sort(sortByBillToName);


    billToNameArr = dataArray;
	for (var i = 0; i < dataArray.length; i++) {
		if (dataArray[i].id && dataArray[i].id > 0 && dataArray[i].name && dataArray[i].name !== "") {
			$("#cmbBillName").append('<option value="' + dataArray[i].id + '">' + dataArray[i].name + '</option>');
		}
	}
	//onClickPtViews();
}

function getReportDataList(dataObj){
    dataArray = [];
    var tempdataArray = [];
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            if (dataObj.response.appointment) {
                if ($.isArray(dataObj.response.appointment)) {
                    dataArray = dataObj.response.appointment;
                } else {
                    dataArray.push(dataObj.response.appointment);
                }
            }
        }
    }

    if (dataArray.length > 0) {
        for (var d = 0; d < dataArray.length; d++) {

            if (dataArray[d].syncTime) {

                if (dataArray[d].dateOfAppointment) {
                    dataArray[d].idk = dataArray[d].id;
                    var date = new Date(GetDateTimeEditDay(dataArray[d].dateOfAppointment));
                    var day = date.getDay();
                    var dow = getWeekDayName(day);
                    var dtEnd = dataArray[d].dateOfAppointment + (Number(dataArray[d].duration) * 60000);
                    dataArray[d].dateTimeOfAppointment = dow + " " + GetDateTimeEdit(dataArray[d].dateOfAppointment); dataArray[d].DW = dow;
                    var dateED = new Date(GetDateTimeEditDay(dtEnd));
                    var dayED = dateED.getDay();
                    var dowED = getWeekDayName(dayED);
                    dataArray[d].dateTimeOfAppointmentED = dowED + " " + GetDateTimeEdit(dtEnd);

                    dataArray[d].appDate = kendo.toString(new Date(dataArray[d].dateOfAppointment), "dd-MM-yyyy");
                    dataArray[d].appTime = kendo.toString(new Date(dataArray[d].dateOfAppointment), "hh:mm tt");

                    var apptDateTime = new Date(dataArray[d].dateOfAppointment);

                    var dateOfMonth = apptDateTime.getDate();
                    var apptmonth = apptDateTime.getMonth();
                    var apptyear = apptDateTime.getFullYear();
                    var appthours = apptDateTime.getHours();
                    var apptminutes = apptDateTime.getMinutes();
                    dataArray[d].appointmentDate = new Date(apptyear, apptmonth, dateOfMonth, appthours, apptminutes, 0, 0);

                }

                if (dataArray[d].composition && dataArray[d].composition.provider) {
                    dataArray[d].carer = dataArray[d].composition.provider.lastName + " " + dataArray[d].composition.provider.firstName;
                }
                if (dataArray[d].composition && dataArray[d].composition.patient) {
                    dataArray[d].serviceUser = dataArray[d].composition.patient.lastName + " " + dataArray[d].composition.patient.firstName;
                }
                if (dataArray[d].composition && dataArray[d].composition.appointmentType) {
                    dataArray[d].status = dataArray[d].composition.appointmentType.desc;
                }
                if (dataArray[d].composition && dataArray[d].composition.appointmentReason) {
                    dataArray[d].reason = dataArray[d].composition.appointmentReason.desc;
                }

                if (dataArray[d].duration) {
                    var totalMinutes = dataArray[d].duration;

                    var hours = Math.floor(totalMinutes / 60);
                    var minutes = Math.round(totalMinutes % 60);
                    if (hours.toString().length == 1) {
                        hours = "0" + hours;
                    }
                    if (minutes.toString().length == 1) {
                        minutes = "0" + minutes;
                    }


                    dataArray[d].duration = hours + ":" + minutes;
                }



                tempdataArray.push(dataArray[d]);

            }
        }

        if (tempdataArray != null && tempdataArray.length > 0) {
            var sortOrderValue = $("#cmbSortOrder option:selected").val();
            if (sortOrderValue == "1") {
                tempdataArray.sort(function (a, b) {
                    var p1 = a.dateOfAppointment;
                    var p2 = b.dateOfAppointment;

                    if (p1 < p2) return -1;
                    if (p1 > p2) return 1;
                    return 0;
                });
            }
            else if (sortOrderValue == "2") {
                tempdataArray.sort(sortByAppointmentDateAndServiceUser);
            }
            else if (sortOrderValue == "3") {
                tempdataArray.sort(sortByAppointmentDateAndStaffName);
            }
        }


        var elem = angular.element(document.querySelector('[ng-app]'));
        var injector = elem.injector();
        var $rootScope = injector.get('$rootScope');
        $rootScope.$apply(function(){
            $rootScope.apidata = tempdataArray;
        });
        // buildPatientRosterList(tempdataArray);
    }
    else{
        customAlert.error("info","No appointments");
    }


}

function buildAppointmentList(dataObj) {
    $("#tableDailyDetailReport").hide();
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1" && dataObj.response.appointment){
        if($.isArray(dataObj.response.appointment)){
            dataArray = dataObj.response.appointment;
        }else{
            dataArray.push(dataObj.response.appointment);
        }
    }

    if (dataArray !== null && dataArray.length > 0) {

        var billTo = $("#cmbBillName option:selected").text();
        var billToId = parseInt($("#cmbBillName option:selected").val());

        dataArray = dataArray.filter(function (e) {
            return e.billToName.toLowerCase() === billTo.toLowerCase();
        });
    }


    if (dataArray !== null && dataArray.length > 0) {


        dataArray.sort(sortByDateAsc);


        if (dataArray[0] && dataArray[0].composition && dataArray[0].composition.patient && dataArray[0].composition.patient.communications) {

            getAjaxObject(ipAddress + "/facility/list?id="+dataArray[0].facilityId, "GET", onGetFacility, onError);

            var patientObj = dataArray[0].composition.patient;
            $("#tdPatientName").text(patientObj.lastName + " " + patientObj.firstName);


            var DOB = new Date(patientObj.dateOfBirth);
            var today = new Date();
            var age = today.getTime() - DOB.getTime();
            age = Math.floor(age / (1000 * 60 * 60 * 24 * 365.25));
            var strDOB = kendo.toString(DOB, "dd/MM/yyyy");
            $("#tdPatientDOB").text(strDOB + " (" + age + "Y), " + patientObj.gender);

            $("#tdPatientAddress1").text(patientObj.communications[0].houseNumber+", "+patientObj.communications[0].address1);
            $("#tdPatientAddress2").text(patientObj.communications[0].state + ", " + patientObj.communications[0].city + ", " + patientObj.communications[0].zip);



            var startDT = $("#txtStartDate").data("kendoDatePicker");
            var endDT = $("#txtEndDate").data("kendoDatePicker");
        
        
            var sDate = new Date(startDT.value());
            var eDate = new Date(endDT.value());

            $("#tdFromDate").text(dformat(sDate,'dd/MM/yyyy'));
            $("#tdToDate").text(dformat(eDate,'dd/MM/yyyy'));


            var tmpDept = billToNameArr.filter(function(b){
                return b.id === billToId;
            });

            if(tmpDept !== null && tmpDept.length > 0){
                $("#tdDepartment").text(tmpDept[0].department);
                var terms;
                $("#ulReportTerms").html();
                if(tmpDept[0].terms != null) {
                    terms = tmpDept[0].terms.split('\n');
                    var termsHTML="";

                    for(var d=0;d<terms.length;d++) {
                        termsHTML += "<li type='none'>"+terms[d]+"</li>";
                    }
                    $("#ulReportTerms").html(termsHTML);
                }

            }

            $("#tdBillTo").text(billTo);


            
            $("#tdInvoiceDate").text(kendo.toString(today, "dd/MM/yyyy"));


            var lookup = {};
            var doas = [];

            for (var item, c = 0; item = dataArray[c++];) {
                if (item && item.dateOfAppointment) {

                    var apptDate = new Date(item.dateOfAppointment);
                    apptDate.setHours(0,0,0,0);
                    var apptTime = apptDate.getTime();

                    if (!(apptTime in lookup)) {
                        lookup[apptTime] = 1;
                        doas.push(apptTime);
                    }
                }
            }

            var dtArray = [];
            for(var j = 0; j < doas.length ; j++){
                var tmpAppts = dataArray.filter(function(e){
                    var tmpApptDate = new Date(e.dateOfAppointment);
                    tmpApptDate.setHours(0,0,0,0);

                    if(tmpApptDate.getTime() === doas[j])
                        return e;
                    else
                        return null;

                });

                if(tmpAppts !== null && tmpAppts.length > 0){
                    var totalAmount = 0;
                    var obj = {};
                    for(var k = 0 ; k < tmpAppts.length ; k++){
                        
                        var day = new Date(GetDateTimeEditDay(tmpAppts[k].dateOfAppointment)).getDay();
                        var dow = getWeekDayName(day);
        
                        var doa = dow + " " + GetDateEdit(tmpAppts[k].dateOfAppointment);

                        obj.doa = doa;

                        var appDate = new Date(tmpAppts[k].dateOfAppointment);

                        var strH = kendo.toString(appDate, "hh");
                        var strm = kendo.toString(appDate, "mm");
                        var stT = kendo.toString(appDate, "tt");

                        var time = strH;
                        if (strm.length > 0) {
                            time = time + ":" + strm;
                        }
                        time = time +" "+ stT;
                        if(obj.time && obj.time !== ""){
                            obj.time = obj.time+", "+time;    
                        }
                        else{
                            obj.time = time;
                        }

                        if(obj.duration && obj.duration !== null){
                            obj.duration = obj.duration +","+tmpAppts[k].duration;
                        }
                        else{
                            obj.duration = tmpAppts[k].duration;
                        }

                        if(obj.payoutHourlyRate && obj.payoutHourlyRate !== null){
                            if(tmpAppts[k].payoutHourlyRate && tmpAppts[k].payoutHourlyRate != null) {
                                obj.payoutHourlyRate = obj.payoutHourlyRate +","+tmpAppts[k].payoutHourlyRate;
                            }
                            else{
                                obj.payoutHourlyRate = obj.payoutHourlyRate +","+0;
                            }
                        }
                        else{
                            if(tmpAppts[k].payoutHourlyRate && tmpAppts[k].payoutHourlyRate != null) {
                                obj.payoutHourlyRate = tmpAppts[k].payoutHourlyRate;
                            }
                            else{
                                obj.payoutHourlyRate = 0;
                            }
                        }
                    
                        if(obj.amount && obj.amount !== null){
                            obj.amount = obj.amount +","+(tmpAppts[k].duration * tmpAppts[k].payoutHourlyRate);
                        }
                        else{
                            obj.amount = (tmpAppts[k].duration * tmpAppts[k].payoutHourlyRate);
                        }


                        totalAmount = totalAmount + (tmpAppts[k].duration * tmpAppts[k].payoutHourlyRate);
                        
                        obj.total = totalAmount;
                    }
                    dtArray.push(obj);
                }
            }


            var strHTML = '';
            var grandTotal = 0;
            for (var i = 0; i < dtArray.length; i++) {
                var obj = dtArray[i];
                strHTML = strHTML + '<tr class="blueTextTr">';
                strHTML = strHTML + '<td class="text-center td-black">' + (i + 1) + '</td>';

                
                strHTML = strHTML + '<td class="text-left td-black">' + obj.doa + '</td>';

               
                strHTML = strHTML + '<td class="text-left td-black" style="word-break: break-all;">' + obj.time + '</td>';
                strHTML = strHTML + '<td class="text-left td-black" style="word-break: break-all;">' + obj.duration + '</td>';
                strHTML = strHTML + '<td class="text-right td-black" style="word-break: break-all;">' + obj.payoutHourlyRate + '</td>';
                strHTML = strHTML + '<td class="text-right td-black" style="word-break: break-all;">' + obj.amount+ '</td>';
                if(obj.total > 0){
                    strHTML = strHTML+'<td class="text-right" style="width: 14%">'+obj.total+'</td>';
                    grandTotal =grandTotal+obj.total;
                }
                else{
                    strHTML = strHTML+'<td class="text-right"></td>';
                }
                
                strHTML = strHTML+'</tr>';
            }
            
            var strTotalHTML='';
            strHTML = strHTML + '<tr class="totalTr"><td class="text-center">In Words : </td><td class="text-center" colspan="4"><span class="spnBlueText">' + convertNumberToWords(grandTotal) + ' Only</span></td><td class="text-right" style="font-weight: 700">Grand Total</td><td class="text-right" style="font-weight: 700">' + grandTotal + '</td></tr>';
            $(strHTML).insertAfter("#trAppointments");
            $("#tableDailyDetailReport").show();
        }

        var elem = angular.element(document.querySelector('[ng-app]'));
        var injector = elem.injector();
        var $rootScope = injector.get('$rootScope');
        $rootScope.$apply(function(){
            $rootScope.apidata = dataArray;
        });
    }
    else{
        
        customAlert.info("Info","No appointment(s) found");
    }
    
};

function onGetFacility(dataObj){
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1" && dataObj.response.facility){
        if($.isArray(dataObj.response.facility)){
            dataArray = dataObj.response.facility;
        } else{
            dataArray.push(dataObj.response.facility);
        }
    }

    if(dataArray !== null && dataArray.length > 0){

        var billingAccountId = dataArray[0].billingAccountId;

        

        getAjaxObject(ipAddress + "/billing-account/list?id="+billingAccountId, "GET", onGetBillingAccount, onError);

        // $("#tdGroupAccountName").text(dataArray[0].name);
        // $("#tdGroupAccountDisplayName").text(dataArray[0].displayName);

        // if(dataArray[0].communications){
        //     var comm = dataArray[0].communications[0];
        //     $("#tdGroupAccountAddress1").text(comm.houseNumber+", "+comm.address1);
        //     $("#tdGroupAccountAddress2").text(comm.state + ", " + comm.city + ", " + comm.zip);

        //     $("#tdGroupAccountMobile").text("Phone : " + comm.cellPhone + ", " + comm.email);

        //     $("#tdAddress1").text(comm.houseNumber+", "+comm.address1);
        //     $("#tdAddress2").text(comm.state + ", " + comm.city + ", " + comm.zip);

        //     $("#tdPhone").text(comm.cellPhone);
        //     $("#tdEmail").text(comm.email);
        // }
    }
}

function onGetBillingAccount(dataObj){
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1" && dataObj.response.billingAccount){
        if($.isArray(dataObj.response.billingAccount)){
            dataArray = dataObj.response.billingAccount;
        } else{
            dataArray.push(dataObj.response.billingAccount);
        }
    }

    if(dataArray !== null && dataArray.length > 0){
        $("#tdGroupAccountName").text(dataArray[0].name);
        $("#tdGroupAccountDisplayName").text(dataArray[0].displayName);

        $("#thGroupAccountName").text(dataArray[0].name);
        $("#tdForGroup").text("For "+dataArray[0].name);

        $("#tdBankSortCode").text(dataArray[0].bankSortCode);
        $("#tdAccountNumber").text(dataArray[0].bankAccountNumber);
        $("#tdRemittanceEmail").text(dataArray[0].remittanceEmail);

        $("#tdInvoiceNumber").html("");

        if(dataArray[0].communications){
            var comm = dataArray[0].communications[0];
            $("#tdGroupAccountAddress1").text(comm.address1+", "+comm.address2);
            $("#tdGroupAccountAddress2").text(comm.city + " - " + comm.houseNumber);

            $("#tdGroupAccountMobile").text("Phone : " + comm.cellPhone + ", " + comm.email);

            $("#tdAddress1").text(comm.address1+", "+comm.address2);
            $("#tdAddress2").text(comm.city + " - " + comm.houseNumber);

            $("#tdPhone").text(comm.cellPhone);
            $("#tdEmail").text(comm.email);
        }
    }
}

function getAppointmentActivities(dataObj){
   
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1" && dataObj.response.patientAppointmentActivities){
        if($.isArray(dataObj.response.patientAppointmentActivities)){
            dataArray = dataObj.response.patientAppointmentActivities;
        } else{
            dataArray.push(dataObj.response.patientAppointmentActivities);
        }
    }

    if(dataArray !== null && dataArray.length > 0){
        if(appointmentIds !== null && appointmentIds.length > 0){
            for(var j = 0 ; j < appointmentIds.length ; j ++){
                var tmpApptActivities = dataArray.filter(function(e){
                    return parseInt(appointmentIds[j]) === e.appointmentId;
                });

                if(tmpApptActivities !== null && tmpApptActivities.length){
                    buildpatientAppointmentActivities(tmpApptActivities);
                }
            }
        }

        getAjaxObjectAsync(ipAddress + "/patient/appointment/medication-activity/list/?is-active=1&is-deleted=0&appointment-id=" + appointmentIds.join(','),"GET",getAppointmentMedicationActivity,onError);
    }

}

function getAppointmentMedicationActivity(dataObj){
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1" && dataObj.response.patientAppointmentActivities){
        if($.isArray(dataObj.response.patientAppointmentMedicationActivity)){
            dataArray = dataObj.response.patientAppointmentMedicationActivity;
        } else{
            dataArray.push(dataObj.response.patientAppointmentMedicationActivity);
        }
    }

    if(dataArray !== null && dataArray.length > 0){
        if(appointmentIds !== null && appointmentIds.length > 0){
            for(var j = 0 ; j < appointmentIds.length ; j ++){
                var tmpApptActivities = dataArray.filter(function(e){
                    return parseInt(appointmentIds[j]) === e.appointmentId;
                });

                if(tmpApptActivities !== null && tmpApptActivities.length){
                    patientAppointmentMedicationActivity(tmpApptActivities);
                }
            }
        }

        
    }
}

function calculateAge(birthday) {
    var ageDifMs = Date.now() - birthday.getTime();
    var ageDate = new Date(ageDifMs);
    return Math.abs(ageDate.getUTCFullYear() - 1970);
}

function buildpatientAppointmentActivities(dataArray) {
    
    if(dataArray.length > 0) {
        var Id = dataArray[0].appointmentId;
        IsVists = 0;
        var activityTypeArr = [];
        for (var i = 0; i < dataArray.length; i++) {
            activityTypeArr.push(dataArray[i].activityType);
        }
        var activityTypeArrFinal = activityTypeArr.reduce(function(p,c,i,activityTypeArr){
          if (p.indexOf(c) == -1) p.push(c);
          else p.push('')
          return p;
        }, [])
        for (var i = 0; i < dataArray.length; i++) {
            var notes = dataArray[i].notes;
            if(notes == "1" || notes == null || notes == "null"){
                notes = "";
            }
            var dot;
            if(dataArray[i].review == null || dataArray[i].review == 0) {
                dot = "";
            }
            else if(dataArray[i].review == 1) {
                dot = "greenDot";
            }
            else if(dataArray[i].review == 2){
                dot = "orangeDot";
            }
            else if(dataArray[i].review == 3){
                dot = "redDot";
            }
             var thumbnailURL;
             var thumbimg='';
             var fileURL;
            var fileimg = '';
            if(dataArray[i].activityThumbnailPresent==1){
                thumbnailURL = ipAddress+"/homecare/download/activities/thumbnail/?access_token="+sessionStorage.access_token+"&id="+dataArray[i].activityId+"&tenant="+sessionStorage.tenant;
                thumbimg ='<img  src="'+thumbnailURL +'" id="imgLogo1" style="padding:5px;padding-left:30px;width:80px;"></img>';
            }
            if(dataArray[i].filePresent==1){
                fileURL = ipAddress+"/homecare/download/patient-appointment-activities/?access_token="+sessionStorage.access_token+"&id="+dataArray[i].id+"&tenant="+sessionStorage.tenant;
                fileimg ='<img  src="'+fileURL +'" id="imgLogo1" style="padding:5px;padding-left:30px;width:100px;"></img>';
            }

            var remarks = "";
            if(dataArray[i].remarks != undefined && dataArray[i].remarks != null){
                remarks = dataArray[i].remarks;
            }
            var dateFD, dayFD, dowFD, dayWeekFD, dateED, dayED, dowED, dayWeekED, dateTime;
            if(dataArray[i].actionedDate != undefined) {
                dateFD = new Date(GetDateTimeEditDay(dataArray[i].actionedDate));
                dayFD = dateFD.getDay();
                dowFD = getWeekDayName(dayFD);

                dayWeekFD = " <br>"+dowFD + " " + GetDateTimeEdit(dataArray[i].actionedDate);
                dateTime = kendo.toString(dateFD,"hh:mm tt");
            }
            else{
                dayWeekFD = "";
                dateTime = "";
            }

            if(dataArray[i].actionedModifiedDate != undefined){
                 dateED = new Date(GetDateTimeEditDay(dataArray[i].actionedModifiedDate));
                 dayED = dateED.getDay();
                 dowED = getWeekDayName(dayED);

                 dayWeekED = " /<br>"+dowED + " " + GetDateTimeEdit(dataArray[i].actionedModifiedDate);
                  dateTime = kendo.toString(dateED,"hh:mm tt");
            }
            else{
                dayWeekED = "";
            }
            if(dataArray[i].required == null || dataArray[i].required == 0){
                if(activityTypeArrFinal[i] != "") {
                    $('#tblActivitiesPerformed'+Id+' tbody').append('<tr><td colspan="3" id="patient-appointment-activityName" name="patient-appointment-activityName">' + activityTypeArrFinal[i] + '<span style="float: left" class=' + dot + '></span></td></tr><tr><td width="10%" name="patient-appointment-image">'+thumbimg+'</td><td width="15%" name="patient-appointment-activity">' + dataArray[i].activity + '<span class="mandatory"></span><span style="float: left" class=' + dot + '></td><td width="45%" name="patient-appointment-activityNotes">' + fileimg + notes + '<br>' + remarks + '</td><td width="25%" style="text-align: left" name="patient-appointment-activityDate">' + dateTime+ '</td></tr>');
                }
                else{
                    $('#tblActivitiesPerformed'+Id+' tbody').append('<tr><td width="10%" name="patient-appointment-image">'+thumbimg+'</td><td width="15%" name="patient-appointment-activity">' + dataArray[i].activity + '<span style="float: left" class=' + dot + '></span></td><td width="60%" name="patient-appointment-activityNotes">' + fileimg +notes + '<br>' + remarks + '</td><td style="text-align: left"width="10%"  name="patient-appointment-activityDate">' +dateTime+ '</td></tr>');
                }
            }
            else{
                if(activityTypeArrFinal[i] != "") {
                    $('#tblActivitiesPerformed'+Id+' tbody').append('<tr><td colspan="3" id="patient-appointment-activityName" name="patient-appointment-activityName">' + activityTypeArrFinal[i] + '<span class="mandatory"></span> <span style="float: left" class=' + dot + '></span></td></tr><tr><td width="0%" name="patient-appointment-image">'+thumbimg+'</td><td width="15%" name="patient-appointment-activity">' + dataArray[i].activity + '<span class="mandatory"></span><span style="float: left" class=' + dot + '></span></td><td width="45%" name="patient-appointment-activityNotes">' +fileimg + notes + '<br>' +remarks + '</td><td width="25%" style="text-align: left" name="patient-appointment-activityDate">' + dateTime+ '</td></tr>');
                }
                else {
                    $('#tblActivitiesPerformed'+Id+' tbody').append('<tr><td width="10%" name="patient-appointment-image">'+thumbimg+'</td><td width="15%" name="patient-appointment-activity">' + dataArray[i].activity + '<span class="mandatory"></span> <span style="float: left" class=' + dot + '></span></td><td width="50%" name="patient-appointment-activityNotes">' + fileimg +notes + '<br>' + remarks + '</td><td width="10%" style="text-align: left" name="patient-appointment-activityDate">' + dateTime+ '</td></tr>');
                }
            }

        }
    }
    else{
        customAlert.info("Info","No visit(s) found");
        IsVists = 1;
    }
    $('.tableWrapper-appointment').removeClass('hideTable');
}
function patientAppointmentMedicationActivity(dataArray) {
    
    if(dataArray.length > 0) {
        var Id = dataArray[0].appointmentId;
        for (var i = 0; i < dataArray.length; i++) {
            // var dateString = new Date(dataArray[i].administrationTime).toLocaleDateString();
            // var dateString = GetDateTimeEdit(dataArray[i].administrationTime);
            var day;
            day = new Date(GetDateTimeEditDay(dataArray[i].administrationTime));
            day = day.getDay();
            day = getWeekDayName(day);

            var dateString = day + " " + GetDateTimeEdit(dataArray[i].administrationTime);
            // var timeString = new Date(dataArray[i].administrationTime).toLocaleTimeString();
            var notes = dataArray[i].notes;
            if(notes == null && notes == "null")
            {
                notes = "";
            }
            // $('#activitiesPeformed tbody').append('<tr><td name="patient-appointment-medicationTitle">Meds</td><td><p><span name="patient-appointment-medicationTradeName">'+dataArray[i].medicationTradeName+'</span><span name="patient-appointment-strength">'+dataArray[i].strength+'</span><span name="patient-appointment-medicationStrength">'+dataArray[i].medicationStrength+'</span></p><p><span name="patient-appointment-take">'+dataArray[i].take+'</span><span name="patient-appointment-medicationForm">'+dataArray[i].medicationForm+'</span><span name="patient-appointment-medicationFrequency">'+dataArray[i].medicationFrequency+'</span><span name="patient-appointment-medicationRoute">'+dataArray[i].medicationRoute+'</span></p><p><span name="patient-appointment-medicationInstructions">'+dataArray[i].medicationInstructions+'</span></p><p><span name="patient-appointment-instruction">'+dataArray[i].instruction+'</span></p></td><td name="patient-appointment-activityNotes"><p><span name="patient-appointment-administrationTime">' + dateString + ' '+ timeString +'</span></p><p><span name="patient-appointment-patientMedicationAction">'+dataArray[i].patientMedicationAction+'</span></p><p><span name="patient-appointment-notes">'+dataArray[i].notes+'</span></p></td></tr>');
            $('#tblActivitiesPerformed'+Id+' tbody').append('<tr><td name="patient-appointment-medicationTitle">Meds</td><td><p><span name="patient-appointment-medicationTradeName">'+dataArray[i].medicationTradeName+'</span><span name="patient-appointment-strength">'+dataArray[i].strength+'</span><span name="patient-appointment-medicationStrength">'+dataArray[i].medicationStrength+'</span></p><p><span name="patient-appointment-take">'+dataArray[i].take+'</span><span name="patient-appointment-medicationForm">'+dataArray[i].medicationForm+'</span><span name="patient-appointment-medicationFrequency">'+dataArray[i].medicationFrequency+'</span><span name="patient-appointment-medicationRoute">'+dataArray[i].medicationRoute+'</span></p><p><span name="patient-appointment-medicationInstructions">'+dataArray[i].medicationInstructions+'</span></p><p><span name="patient-appointment-instruction">'+dataArray[i].instruction+'</span></p></td><td name="patient-appointment-activityNotes"><p><span name="patient-appointment-administrationTime">' + dateString + '</span></p><p><span name="patient-appointment-patientMedicationAction">'+dataArray[i].patientMedicationAction+'</span></p><p><span name="patient-appointment-notes">'+notes+'</span></p></td><td></td></tr>');
        }
        $('.tableWrapper-appointment').removeClass('hideTable');
    }
}


var sortByAppointmentDateAndStaffName = function (a, b) {

    if (a.carer > b.carer) return 1;
    if (a.carer < b.carer) return -1;

    var date1 = a.appointmentDate;
    var date2 = b.appointmentDate;


    if (date1 > date2) return 1;
    if (date1 < date2) return -1;



}

var sortByAppointmentDateAndServiceUser = function (a, b) {

    if (a.serviceUser > b.serviceUser) return 1;
    if (a.serviceUser < b.serviceUser) return -1;

    var date1 = a.appointmentDate;
    var date2 = b.appointmentDate;


    if (date1 > date2) return 1;
    if (date1 < date2) return -1;



}

function buildPatientRosterList(dataSource){
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    otoptions.rowHeight = 100;
    gridColumns.push({
        "title": "Appt #",
        "field": "idk",
    });
    gridColumns.push({
        "title": "Appt Start Date",
        "field": "dateTimeOfAppointment",
    });
    gridColumns.push({
        "title": "Appt End Date",
        "field": "dateTimeOfAppointmentED",
    });
    gridColumns.push({
        "title": "SU Name",
        "field": "serviceUser",
        "width":"25%"
    });
    gridColumns.push({
        "title": "Staff Name",
        "field": "carer",
        "width":"15%"
    });

    gridColumns.push({
        "title": "Status",
        "field": "status",
    });
    gridColumns.push({
        "title": "Reason",
        "field": "reason",
    });

    angularPTUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
}
function secondsToHms(d) {
    d = Number(d);
    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    var hDisplay = h > 0 ? h + (h == 1 ? " hour, " : " hours, ") : "";
    var mDisplay = m > 0 ? m + (m == 1 ? " minute, " : " minutes, ") : "";
    var sDisplay = s > 0 ? s + (s == 1 ? " second" : " seconds") : "";
    return hDisplay + mDisplay + sDisplay;
}
function onPTChange(){

}
function startChange() {
    var startDate = startDT.value(),
        endDate = endDT.value();

    if (startDate) {
        startDate = new Date(startDate);
        startDate.setDate(startDate.getDate());
        endDT.min(startDate);
    } else if (endDate) {
        startDT.max(new Date(endDate));
    } else {
        endDate = new Date();
        startDT.max(endDate);
        endDT.min(endDate);
    }
}

function endChange() {
    var endDate = endDT.value(),
        startDate = startDT.value();

    if (endDate) {
        endDate = new Date(endDate);
        endDate.setDate(endDate.getDate());
        // startDT.max(endDate);
    } else if (startDate) {
        endDT.min(new Date(startDate));
    } else {
        endDate = new Date();
        // startDT.max(endDate);
        endDT.min(endDate);
    }
}
function buttonEvents(){
    $("#btnSearch1").off("click",onClickSearch);
    $("#btnSearch1").on("click",onClickSearch);

    $("#btnPtView").off("click",onClickPtViews);
    $("#btnPtView").on("click",onClickPtViews);

    $("#btnPrint").off("click",onClickPrint);
    $("#btnPrint").on("click",onClickPrint);

    $("#btnServiceUser").off("click");
    $("#btnServiceUser").on("click",onClickServiceUser);

    $("#btnStaff").off("click");
    $("#btnStaff").on("click",onClickStaff);

    $("#btnReset").off("click");
    $("#btnReset").on("click",onClickReset);
    
}
function onClickSearch(){
    var popW = "60%";
    var popH = 500;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search Patient";
    if(sessionStorage.clientTypeId == "2"){
        profileLbl = "Search Client";
    }
    devModelWindowWrapper.openPageWindow("../../html/patients/searchPatient.html", profileLbl, popW, popH, true, closePtAddAction);
}
function closePtAddAction(evt,returnData){
    if(returnData && returnData.status == "success"){

        var pID = returnData.selItem.PID;
        viewPatientId = pID;
        var pName = returnData.selItem.FN+" "+returnData.selItem.MN+" "+returnData.selItem.LN;
        var viewpName = pName;
        if(pID != ""){
            //$("#lblName").text(pID+" - "+pName);
            $("#txtPatient").val(pName);
            /*var imageServletUrl = ipAddress + "/download/patient/photo/" + pID + "/?" + Math.round(Math.random() * 1000000);
            $("#imgPhoto").attr("src", imageServletUrl);

            getAjaxObject(ipAddress + "/patient/" + pID, "GET", onGetPatientInfo, onError);
        }*/
        }
    }
}
function getFacilityList(){
    getAjaxObject(ipAddress+"/facility/list/?is-active=1&is-deleted=0","GET",getFacilityDataList,onError);
}
function getFacilityDataList(dataObj){
    var dataArray = [];
    if(dataObj){
        if($.isArray(dataObj.response.facility)){
            dataArray = dataObj.response.facility;
        }else{
            dataArray.push(dataObj.response.facility);
        }
    }
    var tempDataArry = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
        }
        $("#cmbFacility").append('<option value="' + dataArray[i].idk + '">' + dataArray[i].name + '</option>');
    }
    onClickPtViews();
}
function onFacilityChange(){

}
function onError(err){

}
function onClickPrint(){
    //angularPTUIgridWrapper.printDataGrid();
    customAlert.confirm("Confirm", "Are you sure want to generate invoice number?", function (response) {
        if (response.button == "Yes") {


            var startDT = $("#txtStartDate").data("kendoDatePicker");
            var endDT = $("#txtEndDate").data("kendoDatePicker");

            var sDate = getFromDate(new Date(startDT.value()));
            var eDate = getToDate(new Date(endDT.value()));

            var billToId = parseInt($("#cmbBillName option:selected").val());

            var dt = new Date();
            var obj = {
                "generatedDate":dt.getTime(),
                "createdBy":Number(sessionStorage.userId),
                "isActive":1,
                "isDeleted":0,
                "billingPeriodFrom":sDate,
                "billingPeriodTo":eDate,
                "billToId":billToId,
                "patientId":selPatientId
            };
            createAjaxObject(ipAddress+"homecare/invoice-numbers/",obj,"POST",onCreate,onError);

        }
        else{
            if ($.trim($('#tableDailyDetailReport').html()) !== "") {



                var contents = document.getElementById("tableDailyDetailReport").outerHTML;// $("#divTaskReportsViewer").html();
                var frame1 = $('<iframe />');
                frame1[0].name = "frame1";
                frame1.css({ "position": "absolute", "top": "-1000000px" });
                $("body").append(frame1);
                var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
                frameDoc.document.open();

                //Create a new HTML document.
                frameDoc.document.write('<html><title><h3>Day Wise Detail Invoice Report</h3></title><head>');

                frameDoc.document.write('<link href="../../css/external/css/bootstrap.min.css" rel="stylesheet"></link>');
                frameDoc.document.write('<link href="../../css/Theme01.css" rel="stylesheet" type="text/css" />');


                frameDoc.document.write('</head><body style="background-color: #fff;">');


                //Append the DIV contents.
                frameDoc.document.write(contents);


                frameDoc.document.write('</body></html>');


                frameDoc.document.close();
                setTimeout(function () {
                    window.frames["frame1"].focus();
                    window.frames["frame1"].print();
                    frame1.remove();

                }, 500);
            }
            else {
                customAlert.info("info","No data to print.");
            }
        }
    });
}
function onClickPtViews(){
    var startDT = $("#txtStartDate").data("kendoDatePicker");
    var endDT = $("#txtEndDate").data("kendoDatePicker");

    var sDate = getFromDate(new Date(startDT.value()));
    var eDate = getToDate(new Date(endDT.value()));

    var billToId = parseInt($("#cmbBillName option:selected").val());

    // var dt = new Date();
    // var obj = {
    //     "generatedDate":dt.getTime(),
    //     "createdBy":Number(sessionStorage.userId),
    //     "isActive":1,
    //     "isDelted":0,
    //     "billingPeriodFrom":sDate,
    //     "billingPeriodTo":eDate,
    //     "billToId":billToId,
    //     "patientId":selPatientId
    // };
    // createAjaxObject(ipAddress+"/homecare/invoice-numbers/",obj,"POST",onCreate,onError);



    var ipUrl = ipAddress+"/appointment/list/?";
    
    if(selPatientId != null && selPatientId != 'undefined'){
        ipUrl = ipUrl+"patient-id="+selPatientId+"&";
    }
   



    ipUrl = ipUrl+"from-date="+sDate+"&";

    ipUrl = ipUrl+"to-date="+eDate+"&is-active=1&is-deleted=0";
    getAjaxObject(ipUrl,"GET",buildAppointmentList,onError);
}

function onCreate(dataObj){

    var dataArray = [];
    if(dataObj){
        if($.isArray(dataObj.response["invoice-numbers"])){
            dataArray = dataObj.response["invoice-numbers"];
        }else{
            dataArray.push(dataObj.response["invoice-numbers"]);
        }
    }
    if(dataArray && dataArray.length > 0){
        $("#tdInvoiceNumber").text(dataArray[0].id);
    }

    if ($.trim($('#tableDailyDetailReport').html()) !== "") {



        var contents = document.getElementById("tableDailyDetailReport").outerHTML;// $("#divTaskReportsViewer").html();
        var frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({ "position": "absolute", "top": "-1000000px" });
        $("body").append(frame1);
        var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();

        //Create a new HTML document.
        frameDoc.document.write('<html><title>Daily Detail Report</title><head>');

        frameDoc.document.write('<link href="../../css/external/css/bootstrap.min.css" rel="stylesheet"></link>');
        frameDoc.document.write('<link href="../../css/Theme01.css" rel="stylesheet" type="text/css" />');


        frameDoc.document.write('</head><body style="background-color: #fff;">');


        //Append the DIV contents.
        frameDoc.document.write(contents);


        frameDoc.document.write('</body></html>');


        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();

        }, 500);
    }
    else {
        customAlert.info("info","No data to print.");
    }
}




function onClickServiceUser(){
    var popW = 800;
    var popH = 450;

    if(parentRef)
        parentRef.searchZip = true;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Service User";
    devModelWindowWrapper.openPageWindow("../../html/patients/searchPatient.html", profileLbl, popW, popH, true, onCloseServiceUserAction);
}
function onCloseServiceUserAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        selPatientId = returnData.selItem.PID;
        $('#txtServiceUser').val(returnData.selItem.LN + " "+ returnData.selItem.FN);
    }
}

function onClickStaff(){
    var popW = 800;
    var popH = 450;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Staff";
    devModelWindowWrapper.openPageWindow("../../html/patients/staffList.html", profileLbl, popW, popH, true, onCloseStaffAction);
}

function onCloseStaffAction(evt,returnData){

    if(returnData && returnData.status == "success"){
        selProviderId = returnData.selItem.PID;
        $('#txtStaff').val(returnData.selItem.LN+' '+returnData.selItem.FN);

    }
}


function getFromDate(fromDate) {


    var day = fromDate.getDate();
    var month = fromDate.getMonth();
    month = month+1;
    var year = fromDate.getFullYear();

    var strDate = month+"/"+day+"/"+year;
    strDate = strDate+" 00:00:00";

    var date = new Date(strDate);
    date.setMilliseconds(0);
    var strDateTime = date.getTime();

    return strDateTime;
}

function getToDate(toDate) {
    var day = toDate.getDate();
    var month = toDate.getMonth();
    month = month+1;
    var year = toDate.getFullYear();

    var strDate = month+"/"+day+"/"+year;
    strDate = strDate+" 23:59:59";

    var date = new Date(strDate);
    date.setMilliseconds(0);
    var strDateTime = date.getTime();

    return strDateTime;
}

function onClickReset() {
    $("#cmbBillName").val("1");
    selPatientId = null;
    $("#txtServiceUser").val("");
    $("#tableDailyDetailReport").hide();
}

function onGetDateTimeFormat(date) {
    var today = new Date(date);
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
     today = dd + '-' + mm + '-' + yyyy;
    return today;
}



function sortByBillToName (a, b) {

    var nameA = a.name.toUpperCase();
    var nameB = b.name.toUpperCase();

    var comparison = 0;
    if (nameA > nameB) {
      comparison = 1;
    } else if (nameA < nameB) {
      comparison = -1;
    }
    return comparison;

}


var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
function formatDate(apptdate) {
	
	var formatted_date = apptdate.getDate() + "-" + months[apptdate.getMonth()] + "-" + apptdate.getFullYear();
	return formatted_date;
}


function getWeekDayName(wk){
	var wn = "";
	if(wk == 1){
		return "Mon";
	}else if(wk == 2){
		return "Tue";
	}else if(wk == 3){
		return "Wed";
	}else if(wk == 4){
		return "Thu";
	}else if(wk == 5){
		return "Fri";
	}else if(wk == 6){
		return "Sat";
	}else if(wk == 0){
		return "Sun";
	}
	return wn;
}

var sortByDateAsc = function (x, y) {
    // This is a comparison function that will result in dates being sorted in
    // ASCENDING order. As you can see, JavaScript's native comparison operators
    // can be used to compare dates. This was news to me.

    date1 = new Date(x.dateOfAppointment);
    date2 = new Date(y.dateOfAppointment);

    if (date1 > date2) return 1;
    if (date1 < date2) return -1;
    return 0;
};


function convertNumberToWords(amount) {
    var words = new Array();
    words[0] = '';
    words[1] = 'One';
    words[2] = 'Two';
    words[3] = 'Three';
    words[4] = 'Four';
    words[5] = 'Five';
    words[6] = 'Six';
    words[7] = 'Seven';
    words[8] = 'Eight';
    words[9] = 'Nine';
    words[10] = 'Ten';
    words[11] = 'Eleven';
    words[12] = 'Twelve';
    words[13] = 'Thirteen';
    words[14] = 'Fourteen';
    words[15] = 'Fifteen';
    words[16] = 'Sixteen';
    words[17] = 'Seventeen';
    words[18] = 'Eighteen';
    words[19] = 'Nineteen';
    words[20] = 'Twenty';
    words[30] = 'Thirty';
    words[40] = 'Forty';
    words[50] = 'Fifty';
    words[60] = 'Sixty';
    words[70] = 'Seventy';
    words[80] = 'Eighty';
    words[90] = 'Ninety';
    amount = amount.toString();
    var atemp = amount.split(".");
    var number = atemp[0].split(",").join("");
    var n_length = number.length;
    var words_string = "";
    if (n_length <= 9) {
        var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
        var received_n_array = new Array();
        for (var i = 0; i < n_length; i++) {
            received_n_array[i] = number.substr(i, 1);
        }
        for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
            n_array[i] = received_n_array[j];
        }
        for (var i = 0, j = 1; i < 9; i++, j++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                if (n_array[i] == 1) {
                    n_array[j] = 10 + parseInt(n_array[j]);
                    n_array[i] = 0;
                }
            }
        }
        value = "";
        for (var i = 0; i < 9; i++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                value = n_array[i] * 10;
            } else {
                value = n_array[i];
            }
            if (value != 0) {
                words_string += words[value] + " ";
            }
            if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Crores ";
            }
            if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Lakhs ";
            }
            if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Thousand ";
            }
            if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
                words_string += "Hundred and ";
            } else if (i == 6 && value != 0) {
                words_string += "Hundred ";
            }
        }
        words_string = words_string.split("  ").join(" ");
    }
    return words_string;
}
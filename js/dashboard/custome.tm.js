﻿var SUmaleCount = localStorage.getItem("SUmaleCount");
var SUfemaleCount = localStorage.getItem("SUfemaleCount");


// Service User
var randomScalingFactor = function () {
    return Math.round(SUmaleCount);
};

var randomScalingFactor1 = function () {
    return Math.round(SUfemaleCount);
};

var config = {
    type: 'doughnut',
    data: {
        datasets: [{
            data: [
                randomScalingFactor(),
                randomScalingFactor1(),
            ],
            backgroundColor: [
				'#846bb9', '#ff1e6d'
            ],
            label: 'Dataset 1'
        }],
        labels: [
			'Male',
			'Female'
        ]
    },
    options: {
        responsive: true,
        legend: false,
        animation: {
            animateScale: true,
            animateRotate: true
        }
    }
};

window.onload = function () {
    var ctx = document.getElementById('chart-area').getContext('2d');
    window.myDoughnut = new Chart(ctx, config);
};
// ==============================================================
// Support Card Chart
// ==============================================================
var opts = {
    angle: 0, // The span of the gauge arc
    lineWidth: 0.32, // The line thickness
    radiusScale: 1, // Relative radius
    pointer: {
        length: 0.44, // // Relative to gauge radius
        strokeWidth: 0.04, // The thickness
        color: '#f3f3f3' // Fill color
    },
    limitMax: false, // If false, the max value of the gauge will be updated if value surpass max
    limitMin: false, // If true, the min value of the gauge will be fixed unless you set it manually
    colorStart: '#fece6a', // Colors
    colorStop: '#fece6a', // just experiment with them
    strokeColor: '#eafaf1', // to see which ones work best for you
    generateGradient: true,

    highDpiSupport: true // High resolution support
};
var target = document.getElementById('support'); // your canvas element
var gauge = new Gauge(target).setOptions(opts); // create sexy gauge!
gauge.maxValue = 3000; // set max gauge value
gauge.setMinValue(0); // Prefer setter over gauge.minValue = 0
gauge.animationSpeed = 45; // set animation speed (32 is default value)
gauge.set(1850); // set actual value 


// ==============================================================
// Sales
// ==============================================================

var sparklineLogin = function () {
    $('#salesmonth').sparkline([7, 10, 9, 11, 9, 10, 12, 8], {
        type: 'bar',
        height: '80',
        barWidth: '8',
        width: '100%',
        resize: true,
        barSpacing: '8',
        barColor: '#3f50f6'
    });
};

var sparkResize;

sparklineLogin();

// ==============================================================
// Campaign
// ==============================================================

var chart1 = c3.generate({
    bindto: '#campaign-v2',
    data: {
        columns: [
            ['Direct Sales', 25],
            ['Referral Sales', 15],
            ['Afilliate Sales', 10],
            ['Indirect Sales', 15],
            ['Inside Sales', 10]
        ],

        type: 'donut',
        tooltip: {
            show: true
        }
    },
    donut: {
        label: {
            show: false
        },
        title: '130k',
        width: 44
    },

    legend: {
        hide: true
    },
    color: {
        pattern: [
            '#7ababc',
            '#f8e367',
            '#93ccce',
            '#8abe6e',
            '#e18197',
        ]
    }
});

d3.select('#campaign-v2 .c3-chart-arcs-title');

// ==============================================================
// Horiantal
// ==============================================================

var bar6 = new Chartist.Bar('#chartBar6', {
    labels: ['Q1', 'Q2', 'Q3', 'Q4'],
    series: [
      [800000, 1200000, 1400000, 1300000],
      [200000, 400000, 500000, 300000],
      [100000, 200000, 400000, 600000]
    ]
}, {
    stackBars: true,
    horizontalBars: true,
    axisX: {
        labelInterpolationFnc: function (value) {
            return (value / 1000) + 'k';
        }
    },
    chartPadding: {
        bottom: 0,
        left: 0,
        right: 40
    }
}).on('draw', function (data) {
    if (data.type === 'bar') {
        data.element.attr({
            style: 'stroke-width: 30px'
        });
    }
});


// Simple pie chart

var data = {
    series: [5, 3, 4]
};

var sum = function (a, b) { return a + b };

new Chartist.Pie('#profitLoss', data, {
    labelInterpolationFnc: function (value) {
        return Math.round(value / data.series.reduce(sum) * 100) + '%';
    }
});





// Stocked bar chart

new Chartist.Bar('#expensesChart', {
    labels: ['Q1', 'Q2', 'Q3', 'Q4'],
    series: [
        [800000, 1200000, 1400000, 1300000],
        [200000, 400000, 500000, 300000],
        [100000, 200000, 400000, 600000]
    ]
}, {
    stackBars: true,
    axisY: {
        labelInterpolationFnc: function (value) {
            return (value / 1000) + 'k';
        }
    }
}).on('draw', function (data) {
    if (data.type === 'bar') {
        data.element.attr({
            style: 'stroke-width: 30px'
        });
    }
});




// Overal
c3.generate({
    bindto: '#Overalchart',
    data: {
        columns: [
            ['data', 50.4]
        ],

        type: 'gauge'
    },
    color: {
        pattern: ['#fece6a', '#fece6a']

    }
});


// Service User
var randomScalingFactor = function() {
	return Math.round(0 * 100);
};

var config = {
	type: 'doughnut',
	data: {
		datasets: [{
			data: [
                randomScalingFactor(),
                randomScalingFactor(),
			],
			backgroundColor: [
				'#846bb9', '#ff1e6d'
			],
			label: 'Dataset 1'
		}],
		labels: [
			'Male',
			'Female'
		]
	},
	options: {
		responsive: true,
		legend: false,
		animation: {
			animateScale: true,
			animateRotate: true
		}
	}
};

window.onload = function() {
	var ctx = document.getElementById('chart-area').getContext('2d');
	window.myDoughnut = new Chart(ctx, config);
};




		
// Expenses

new Chartist.Bar('#expensesChart', {
    labels: ['', '', '', ''],
    series: [
        [800000, 1200000, 1400000, 1300000],
        [200000, 400000, 500000, 300000],
        [100000, 200000, 400000, 600000]
    ]
}, {
    stackBars: true,
    axisY: {
        labelInterpolationFnc: function (value) {
            return (value / 1000) + '';
        }
    }
}).on('draw', function (data) {
    if (data.type === 'bar') {
        data.element.attr({
            style: 'stroke-width: 15px'
        });
    }
});


// Balance Sheet

new Chartist.Bar('#balanceSheet', {
    labels: ['', '', '', '', '', '', ''],
    series: [
        [5, 4, 3, 7, 5, 10, 3]
    ]
}, {
    seriesBarDistance: 10,
    reverseData: true,
    horizontalBars: true,
    axisY: {
        offset: 0
    }
});


//Appointments


//Detail Total

new Chartist.Pie('#detailTotal', {
    series: [20, 13, 20, 47],
    
}, {
    donut: true,
	colors: ['#000', '#f8e367', '#93ccce', '#f8e367'],
    donutWidth: 40,
    donutSolid: true,
    startAngle: 270,
    showLabel: true
});

//Profit & Losses

new Chartist.Pie('#profitLoss', {
    series: [20, 13, 20, 47],
    
}, {
    donut: true,
	colors: ['#000', '#f8e367', '#93ccce', '#f8e367'],
    donutWidth: 40,
    donutSolid: true,
    startAngle: 270,
    showLabel: true
});


var angularPgridWrapper = null;
var angularAgridWrapper = null;

var carePArray = [];
var snoArr = ['1','2','3','4'];
var cnArr = ['Joseph Anthony','Michal Adam','Nurse Nurse','Provider1 Provider1'];
var anArr = ['Thomos Edition'];
var pnArr = ['Patient1','Patient2','Patient3','Patient4','Patient5','Patient6','Patient7','Patient8','Patient9','Patient10'];
var stArr = ['9:30 AM','10:00 AM','10:30 AM','11:00 AM','11:30 AM','12:00 AM','12:30 AM','2:00 AM','4:00 AM','4:30 AM'];
var etArr = ['10:30 AM','11:00 AM','11:30 AM','12:00 AM','12:30 AM','2:00 AM','2:30 AM','3:00 AM','5:00 AM','6:30 AM'];
var duArr = ['8:32','10:00','5:00','3:00'];

$(document).ready(function() {
	init();
	buttonEvents();
});

function init(){
	  var dataOptionsPT = {pagination: false,paginationPageSize: 500,changeCallBack: onPTChange};
	  angularPgridWrapper = new AngularUIGridWrapper("grdCareGiver", dataOptionsPT);
	  angularPgridWrapper.init();
	  
	  var dataOptions = {pagination: false,paginationPageSize: 500,changeCallBack: onChange};
	  angularAgridWrapper = new AngularUIGridWrapper("grdACareGiver", dataOptionsPT);
	  angularAgridWrapper.init();
	  
	  for(var i=0;i<snoArr.length;i++){
		  var sItem = {};
		  sItem.Sno = snoArr[i];
		  sItem.PN = pnArr[i];
		  sItem.CN = cnArr[i];
		  sItem.ST = stArr[i];
		  sItem.ET = etArr[i];
		  sItem.DUR = duArr[i];
		  carePArray.push(sItem);
	  }
	  
	  var apcarePArray = [];
	  for(var i=0;i<anArr.length;i++){
		  var sItem = {};
		  sItem.Sno = snoArr[i];
		  sItem.PN = pnArr[i];
		  sItem.CN = anArr[i];
		  sItem.ST = stArr[i];
		  sItem.ET = etArr[i];
		  //sItem.DUR = duArr[i];
		  apcarePArray.push(sItem);
	  }
	  
	  
	  buildPGrid(carePArray);
	  buildAGrid(apcarePArray);
	  
	  createPresentChart();
	 // createAbsentChart();
}
function buttonEvents(){
	$("#btnList").off("click");
	$("#btnList").on("click",onClickList);
	
	$("#btnChart").off("click");
	$("#btnChart").on("click",onClickChart);
	
	$("#btnCancelDet").off("click");
	$("#btnCancelDet").on("click",onClickCancel);
}

function adjustPanels(){
	var defHeight = 400;
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 100;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    try{
    		angularPgridWrapper.adjustGridHeight(cmpHeight);
    		angularAgridWrapper.adjustGridHeight(cmpHeight);
    		
    		$("#chartCareGiver").height(cmpHeight);
    		$("#chartACareGiver").height(cmpHeight);
    		
    }catch(e){};
}

function buildPGrid(dataSource){
	var gridColumns = [];
	var otoptions = {};
	otoptions.noUnselect = false;
	otoptions.rowHeight = 100; 
	gridColumns.push({
        "title": "SNo",
        "field": "Sno",
        "width":"30%"
    });
	  gridColumns.push({
	        "title": "Provider",
	        "field": "CN",
	        "width":"30%"
	    });
	  gridColumns.push({
	        "title": "Hours Worked",
	        "field": "DUR",
	        "width":"39%"
	    });
    angularPgridWrapper.creategrid(dataSource, gridColumns,otoptions);
  adjustPanels();
}
function onPTChange(){
	
}
function buildAGrid(dataSource){
	var gridColumns = [];
	var otoptions = {};
	otoptions.noUnselect = false;
	otoptions.rowHeight = 100; 
	gridColumns.push({
        "title": "SNo",
        "field": "Sno",
        "width":"30%"
    });
	  gridColumns.push({
	        "title": "Provider",
	        "field": "CN",
	    });
	 
    angularAgridWrapper.creategrid(dataSource, gridColumns,otoptions);
  adjustPanels();
}
function onChange(){
	
}
function onClickList(){
	removeLinks();
	$("#divList").show();
	$("#btnList").removeClass("btn-primary");
	$("#btnList").addClass("btn-default");
}
function onClickChart(){
	removeLinks();
	$("#divChart").show();
	$("#btnChart").removeClass("btn-primary");
	$("#btnChart").addClass("btn-default");
}
function removeLinks(){
	$("#divList").hide();
	$("#divChart").hide();
	
	$("#btnList").removeClass("btn-default");
	$("#btnChart").removeClass("btn-default");
	
	$("#btnList").addClass("btn-primary");
	$("#btnChart").addClass("btn-primary");
}
function createPresentChart() {
    $("#chartCareGiver").kendoChart({
        legend: {
            visible: true
        },
        seriesDefaults: {
            type: "bar"
        },
        series: [{
            name: "Caretaker Visit",
            data: [5,10,14,20,15]
        }],
        valueAxis: {
            max: 25,
            line: {
                visible: true
            },
            minorGridLines: {
                visible: true
            },
        },
        categoryAxis: {
            categories: ["Care Taker1", "Care Taker2", "Care Taker3", "Care Taker4", "Care Taker5"],
            majorGridLines: {
                visible: true
            }
        },
        tooltip: {
            visible: true,
            template: "#= series.name #: #= value #"
        }
    });
}

function createAbsentChart() {
    $("#chartACareGiver").kendoChart({
        legend: {
            visible: true
        },
        seriesDefaults: {
            type: "bar"
        },
        series: [{
            name: "Caretaker Visit",
            data: [5,10,14,20,15]
        }],
        valueAxis: {
            max: 25,
            line: {
                visible: true
            },
            minorGridLines: {
                visible: true
            },
        },
        categoryAxis: {
            categories: ["Care Taker6", "Care Taker7", "Care Taker8", "Care Taker9", "Care Taker10"],
            majorGridLines: {
                visible: true
            }
        },
        tooltip: {
            visible: true,
            template: "#= series.name #: #= value #"
        }
    });
}

function onClickCancel(){
	popupClose(false);
}

function popupClose(obj){
	var windowWrapper = new kendoWindowWrapper();
	windowWrapper.closePageWindow(obj);
}

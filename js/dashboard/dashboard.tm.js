$(document).ready(function() {
	init();
	buttonEvents();
});

function init(){
	$("#txtPRFacility").kendoComboBox();
	$("#txtPRDate").kendoDatePicker();
	adjustPanels();
}
function buttonEvents(){
	$("#lblCaregivers").off("click");
	$("#lblCaregivers").on("click",onClickCareGivers);
	
	$("#lblPAppointment").off("click");
	$("#lblPAppointment").on("click",onClickPAppointments);
	
	$("#lblPCSeen").off("click");
	$("#lblPCSeen").on("click",onClickPAVisited);
	
	$("#lblPCY").off("click");
	$("#lblPCY").on("click",onClickPAYetVisit);
	
	$("#lblCH").off("click");
	$("#lblCH").on("click",onClickCaregiverHours);
	
	$("#lblSchBA").off("click");
	$("#lblSchBA").on("click",onClickSchAppoints);
	
	$("#lblCP").off("click");
	$("#lblCP").on("click",onClickCaregiverPay);
	
	$("#lblKM").off("click");
	$("#lblKM").on("click",onClickKM);
	
}
function onClickCareGivers(){
	var popW = "80%";
    var popH = "80%";
    var profileLbl = "Total Caregivers";
    var devModelWindowWrapper = new kendoWindowWrapper();
    devModelWindowWrapper.openPageWindow("../../html/dashboard/caregiverDetails.html", profileLbl, popW, popH, true, closeCaregiver);
}
function closeCaregiver(evt,returnData){
	
}
function onClickPAppointments(){
	var popW = "60%";
    var popH = "60%";
    var profileLbl = "Total Patients";
    var devModelWindowWrapper = new kendoWindowWrapper();
    devModelWindowWrapper.openPageWindow("../../html/dashboard/patientAppDetails.html", profileLbl, popW, popH, true, closeCaregiver);
}
function onClickSchAppoints(){
	var popW = "60%";
    var popH = "60%";
    var profileLbl = "Total Amount Charged for the Appointments";
    var devModelWindowWrapper = new kendoWindowWrapper();
    devModelWindowWrapper.openPageWindow("../../html/dashboard/caregiverAmount.html", profileLbl, popW, popH, true, closeCaregiver);
}
function onClickPAVisited(){
	var popW = "60%";
    var popH = "60%";
    var profileLbl = "Total Patients Visited";
    var devModelWindowWrapper = new kendoWindowWrapper();
    devModelWindowWrapper.openPageWindow("../../html/dashboard/patientAppDetails.html", profileLbl, popW, popH, true, closeCaregiver);
}
function onClickPAYetVisit(){
	var popW = "60%";
    var popH = "60%";
    var profileLbl = "Total  Patients to be visited";
    var devModelWindowWrapper = new kendoWindowWrapper();
    devModelWindowWrapper.openPageWindow("../../html/dashboard/patientAppDetails.html", profileLbl, popW, popH, true, closeCaregiver);
}
function onClickCaregiverHours(){
	var popW = "80%";
    var popH = "80%";
    var profileLbl = "Total scheduled caregiver hours";
    var devModelWindowWrapper = new kendoWindowWrapper();
    devModelWindowWrapper.openPageWindow("../../html/dashboard/caregiverHours.html", profileLbl, popW, popH, true, closeCaregiver);
}
function onClickCaregiverPay(){
	var popW = "80%";
    var popH = "80%";
    var profileLbl = "Total Payout Amount to Caregivers";
    var devModelWindowWrapper = new kendoWindowWrapper();
    devModelWindowWrapper.openPageWindow("../../html/dashboard/caregiverPayment.html", profileLbl, popW, popH, true, closeCaregiver);
}
function onClickKM(){
	var popW = "80%";
    var popH = "80%";
    var profileLbl = "Total Caregiver Miles Driven";
    var devModelWindowWrapper = new kendoWindowWrapper();
    devModelWindowWrapper.openPageWindow("../../html/dashboard/caregiverKM.html", profileLbl, popW, popH, true, closeCaregiver);
}
function adjustPanels(){
		pnlHeight = window.innerHeight;
		pnlHeight = pnlHeight-100;
		var divHeight = pnlHeight/3;
		$("#divMain").height(pnlHeight);
		
		$("#divFirst").height(divHeight);
		$("#divSec").height(divHeight);
		$("#divThird").height(divHeight);
		
		$(".db").height(divHeight);
		
		var pnlBodyHeight = divHeight;//-35;
		$(".panel-body").height(pnlBodyHeight);
		
}


var MessageService = "MessageService";
var previousGroups = {};
var win_messages = self.top;

/*
*	Function to load desired group messages in local storage by calling corresponding backend service and parsing the returned json data.
*/
function loadGroupMessages(groups,resultHandler){
	var params = {};
	params['groupNames'] = groups;
	var toSend = $.param(params,true);//$(gropus).serializeArray();//$(dataString).serializeArray()
	var flag = false;
	if(win_messages.messagesByGroup == undefined){
		win_messages.messagesByGroup = {};
		sendRequestForMessages(toSend, resultHandler);
		return;
	}
	var myGroupArr = [];
	for(var k=0; k<groups.length; k++)
	{
		if(win_messages.messagesByGroup[groups[k]] == undefined){
			myGroupArr.push(groups[k]);
			flag = true;
		}else{
			previousGroups[groups[k]] = win_messages.messagesByGroup[groups[k]];
		}
	}
	win_messages.messagesByGroup = previousGroups;	
	
	if(flag == true){
		params = {};
		params['groupNames'] = myGroupArr;
		toSend = $.param(params,true);
		sendRequestForMessages(toSend, resultHandler);
	}
	else
	resultHandler();
}

/*
 * Function to send an ajax call based on groupNames
 * 
 */
function sendRequestForMessages(toSend, resultHandler){
	clientAPIManager.sendRequest(MessageService, "getMessagesByGroups", true, "POST", toSend, function(dataXML){createMessagesAsObject(dataXML);
	resultHandler(dataXML)}, function(data){resultHandler(data)}, true);
}

/*
*	Function to take XML obect returned by ajax call and store messages in Global Object
*/
function createMessagesAsObject(data){
	if(data){
		var msgGrpArray = $(data).find("MessageGroup");
		for(var i=0;i<msgGrpArray.length;i++){
			var msgGrp = msgGrpArray[i];
			var group = $(msgGrp).attr("groupName");
			win_messages.messagesByGroup[group] = {};
			var msgArray = $(msgGrp).find("M");
			for(var j=0;j<msgArray.length;j++){
				var message = msgArray[j];
				win_messages.messagesByGroup[group][$(message).children("C").text()] = $(message).children("T").text();
			}
		}
	}
	
}

/*
*	Function to get localized text with given message group name, message code.
*	If no localized text is found in local storage, it would return the default value passed.
*/
function getLocaleStringWithDefault(groupName, code, defaultValue){
	if(win_messages.messagesByGroup != undefined)
		if(win_messages.messagesByGroup[groupName] != null && win_messages.messagesByGroup[groupName] != undefined)
			if(win_messages.messagesByGroup[groupName][code] != undefined)
				return win_messages.messagesByGroup[groupName][code];
	return defaultValue;
}

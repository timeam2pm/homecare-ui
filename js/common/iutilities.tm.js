$(window).load(function() {

    // Panel events

    jQuery(function($) {
        $('.panel-heading span.panel-clickable').on("click", function(e) {
            var panelbody = $($(this).parent().parent().children()[1]);
            if(panelbody != undefined && panelbody != null)
            if (panelbody.hasClass('panel-collapsed')) {
                // expand the panel
                panelbody.slideDown("fast",function(){try{expandPanel();}catch(e){};});
                panelbody.removeClass('panel-collapsed');
                panelbody.parent().find('.panel-heading i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');  
                
            } else {
                // collapse the panel
                panelbody.slideUp("fast", function(){try{minmizePanel();}catch(e){};});
                panelbody.addClass('panel-collapsed');
                panelbody.parent().find('.panel-heading i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');             
            }
        });
    });

    var filename = [["../../../../app/ui/styles/"+sessionStorage.UICustomizationKey+"/css/newTheme.css", "css"]];
   // window.top.DCSSloadChildResources(filename, document);

    

});
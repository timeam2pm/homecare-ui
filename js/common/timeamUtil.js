var statusArray = [{Key:"ACTIVE",Value:"ACTIVE"},{Key:"INACTIVE",Value:"INACTIVE"}];

var ACTIVE = "ACTIVE";
var INACTIVE = "INACTIVE";

function setDataForSelection(data, container, changeMethod, namesArr, selIndex, optionLabelValue) {
    var selList = [];
    if ($.isArray(data) == false) {
        var obj = {};
        obj[namesArr[0]] = data[namesArr[0]];
        obj[namesArr[1]] = data[namesArr[1]];
        selList.push(obj);

    } else {
        selList = data;
    }

    $("#" + container).kendoComboBox({
        dataTextField: namesArr[0],
        dataValueField: namesArr[1],
        dataSource: selList,
        optionLabel: " ",
        change: function(e) {
            $("#" + container).parent().find("span.k-dropdown-wrap").attr("title", $("#" + container).data("kendoComboBox").text());
            try {
                changeMethod(e);
            } catch (e1) {

            }
        },
        dataBound: function(e) {
            setTimeout(function() {
                var dropDownList = $("#" + container).data("kendoComboBox");
                /*if (dropDownList.list.width() >= $("#" + container).width()) {
                    dropDownList.list.width(dropDownList.list.width() + 20);
                }*/
                if(dropDownList && dropDownList.list){
                	dropDownList.list.css("min-width", $("#" + container).width());
                }
            }, 200);
        },
        template: function(e) {
            var tip = e[namesArr[0]];
            return '<div class="overFlow" title="' + tip + '">' + tip + '</div>';
        }
    });
    var containerTypeDropdownlist = $("#" + container).data("kendoComboBox");
    containerTypeDropdownlist.select(selIndex);
    $("#" + container).parent().find("span.k-dropdown-wrap").attr("title", $("#" + container).data("kendoComboBox").text());
    //$("#" + container).data('kendoComboBox').trigger("change");
}
function getComboBoxDataItem(cmbId){
	var cmb = $("#"+cmbId).data("kendoComboBox");
	if(cmb){
		return cmb.dataItem();
	}
	return null;
}
function getComboBoxReference(cmbId){
	var cmb = $("#"+cmbId).data("kendoComboBox");
	if(cmb){
		return cmb;
	}
	return null;
}
function getTreeReference(treeId){
	var treeView = $('#'+treeId).data('kendoTreeView');
	if(treeView){
		return treeView;
	}
	return null;
}
function getSelectTreeNode(treeId){
	var tv = $('#'+treeId).data('kendoTreeView');
	if(tv){
		var selTreeItem = tv.dataItem(tv.select());
		if(selTreeItem){
			return selTreeItem;
		}
	}
	return null;
}
function removeNullPayLoads(item){
	for (var i in item){
		//console.log($.isEmptyObject(item[i]));
		if($.isEmptyObject(item[i])){
			item[i] = "";
		}
	}
	return item;
}
function removeNullObject(itemObj){
	if($.isEmptyObject(itemObj)){
			return "";
	}
		return itemObj;
}

function allowNumerics(ctrlId){
	$("#"+ctrlId).keypress(function (e) {
		 if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
			return false;
		}
   });
}
function allowDecimals(ctrlId){
	$("#"+ctrlId).keypress(function (e) {
		 if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 95 && e.which != 46) {
			return false;
		}
   });
}
function allowSessionTime(ctrlId){
	$("#"+ctrlId).keypress(function (e) {
		//console.log(e.which);
		 if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which !=45) {
			return false;
		}
   });
}
function allowSplAlphaNumericWithoutSpace(ctrlId){
	$("#"+ctrlId).keypress(function (e) {
		//console.log(e.which);
		 if ((e.which >= 64 && e.which < 91) || (e.which > 96 && e.which < 123) || (e.which >=47 && e.which <=57) || (e.which > 34 && e.which < 39)){
              return true;
			}else{
              return false;
		}
   });
}
function allowNegativeValue(ctrlId){
	$("#"+ctrlId).keypress(function (e) {
		//console.log(e.which);
		 if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which !=45) {
			return false;
		}
   });
}
function allowNegativeDecimalValue(ctrlId){
	$("#"+ctrlId).keypress(function (e) {
		//console.log(e.which);
		 if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which !=45 && e.which !=46) {
			return false;
		}
   });
}
function allowAlphabets(ctrlId){
	$("#"+ctrlId).keypress(function (e) {
		//console.log(e.which);
		 if ((e.which > 64 && e.which < 91) || (e.which > 96 && e.which < 123)){
              return true;
			}else{
              return false;
		}
   });
}

function allowAlphabetsSapaceLN(ctrlId){
	$("#"+ctrlId).keypress(function (e) {
		//console.log(e.which);
		if ((e.which > 64 && e.which < 91) || (e.which > 96 && e.which < 123) || (e.which == 45 ) || (e.which == 32) ){

			return true;
		}else{

			return false;
		}
	});
}

function Validate(event) {
    var regex = new RegExp("^[0-9+?]");
    var key = String.fromCharCode(event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
} 

function Validate(event) {
    var regex = new RegExp("^[a-zA-Z0-9-_.? ]");
    var key = String.fromCharCode(event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
}  
function Validate1(event) {
    var regex = new RegExp("^[a-zA-Z0-9.//:?]");
    var key = String.fromCharCode(event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
}   
function allowAlphaNumeric(ctrlId){
	$("#"+ctrlId).keypress(function (e) {
		console.log(e.which);
		 if ((e.which > 64 && e.which < 91) || (e.which > 96 && e.which < 123) || (e.which >=47 && e.which <=57)){
              return true;
			}else{
              return false;
		}
   });
}
function allowSplAlphaNumeric(ctrlId){
	$("#"+ctrlId).keypress(function (e) {
		//console.log(e.which);
		 if ((e.which > 64 && e.which < 91) || (e.which > 96 && e.which < 123) || (e.which >=47 && e.which <=57) || (e.which == 32) || (e.which > 34 && e.which < 39)){
              return true;
			}else{
              return false;
		}
   });
}
function allowSpaceAlphaNumeric(ctrlId){
	$("#"+ctrlId).keypress(function (e) {
		//console.log(e.which);
		 if ((e.which > 64 && e.which < 91) || (e.which > 96 && e.which < 123) || (e.which >=47 && e.which <=57) || (e.which == 32)){
              return true;
			}else{
              return false;
		}
   });
}

function restrictLessAndGreathan(ctrlId){
	$("#"+ctrlId).keypress(function (e) {
		console.log(e.which);
		 if (e.which == 60 || e.which == 62) {
			return false;
		}
   });
}
function allowPhoneNumber(ctrlId){
	$("#"+ctrlId).keypress(function (e) {
			//console.log(e.which);
		 if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 40 && e.which != 41 && e.which != 45) {
			return false;
		}
   });
}
function validateEmail(field) {
	var field = field.trim();
	var flag = true;
	var regex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	if (regex.test(field)) {
		flag = true;
	} else {
		flag = false;
	}
	return flag;
}
function getIndexByText(cmbId,attr,searchStr){
	var cmb = $("#"+cmbId).data("kendoComboBox");
	if(cmb){
		var ds = cmb.dataSource;
		if(ds && ds.total()>0){
			var recLength = ds.total();
			for(var i=0;i<recLength;i++){
				var item = ds.at(i);
				if(item && item[attr] == searchStr){
					return i;
				}
			}
		}
	}
	return -1;
}

function allowAlphaNumericwithSapce(ctrlId){
    $("#"+ctrlId).keypress(function (e) {
        //console.log(e.which);
        if ((e.which > 64 && e.which < 91) || (e.which > 96 && e.which < 123) || (e.which >=48 && e.which <=57) || (e.which == 32))
        {
        	return true;
        }else{
            return false;
        }
    });
}
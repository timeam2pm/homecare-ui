var tempThis;
var tempUrl;
var tempTextContent;
var levelNum;
var menuCloseInterval;
var loginFirst = true;
var triggerAfterLoad = [false,false];
var notificationTxtMsgSelcted = false;
var allowCelltipforAllGrids = false;
var inThirdLevel = "";

function MenuSuccessHandler(data) {
    try {
        // IE 11 blink issue
        var detectIEregexp = /Trident.*rv[ :]*(\d+\.\d+)/;
        if (navigator.userAgent.indexOf('MSIE') != -1 || detectIEregexp.test(navigator.userAgent)) {
            $("body").css("overflow", "hidden");
            $("#iframe").css("position", "absolute");
        }
    } catch (e) {}
	var xmlResponse = $(data).find('ApplicationMenu');
MenuSuccess( xmlResponse );
}

function checkIfUserAlreadyAgreedAggrement(){
	var getUserName = sessionStorage.getItem("userName");
	var getUserOrg = sessionStorage.getItem("userOrganization");
	var queryParams = "username="+getUserName+"&organization="+getUserOrg;
	clientAPIManager.sendRequest( "CustomerRegistrationService", "isAggredUser", true, "POST", queryParams, isAggredUserSuccess, isAggredUserFail, true );
}

function isAggredUserSuccess(response) {
	var aggredData = $.trim( $(response).find('Aggred').text() );
	if(aggredData == "N"){
		var userAggreementWindowWrapper = new kendoWindowWrapper();
		userAggreementWindowWrapper.openPageWindow("../../../login/html/useraggrement.html","Terms of Service", "60%", "60%", true, closeAction);	
	}
}

function closeAction() {
	
}

function isAggredUserFail(failResp) {
	var aggredFail = $.trim( $(failResp).find('Description').text() );
	alert(aggredFail);
}

function MenuSuccess(xmlResponse) {
    //var xmlResponse = $(data).find('ApplicationMenu');
	var getUserAggrementRequiredVal = sessionStorage.getItem("showUserAggrement");
	if(getUserAggrementRequiredVal == "yes") {
		checkIfUserAlreadyAgreedAggrement();
	}
	var firstLevelMenuArr = xmlResponse.children('MenuItem');
	var firstLevelMenuLen = firstLevelMenuArr.length;	
	for( i=0; i< firstLevelMenuLen; i++){
        var hasChildArr = firstLevelMenuArr.eq(i).children('MenuItem');
		var leve2Test = "";
		var level2 = "";
		var level3Exists = "";
		if(hasChildArr.length > 0){
            for (j = 0; j < hasChildArr.length; j++) {
			var level3Test = '';
			var level3 = '';
				
                var innerChildArr = hasChildArr.eq(j).children('MenuItem');
                if (innerChildArr != undefined && innerChildArr.length > 0) {
                    var level3ChildArr = hasChildArr.eq(j).children('MenuItem');
					for( k=0; k<level3ChildArr.length; k++ ){
						console.log( level3ChildArr.eq(k).attr('label') );
                        var level3URL = SecurityObjectAsQS(level3ChildArr.eq(k));
						var level3ImgSRC =level3ChildArr.eq(k).attr('icon');
						var level3Title = level3ChildArr.eq(k).attr('label');
                        if (level3URL != '' && level3ChildArr.eq(k).children('MenuItem').length <= 0)
							level3Data = level3URL;
						else
							level3Data = "";						
						level3Test += '<li class="level-3-event"><a rel="'+level3Data+'" href="javascript:void(0);"><img class="menu-small-icon" src="'+level3ImgSRC+'" alt="" />'+level3Title+'</a></li>';						
					}
					level3 = '<li class="level-3"><ul class="level-3-reference">'+level3Test+'</ul></li>';
				}else{
					level3 = '';
				}
					var level2URL = SecurityObjectAsQS(hasChildArr.eq(j));
					var level2ImgSRC = hasChildArr.eq(j).attr('icon');
					var level2Title = hasChildArr.eq(j).attr('label');		
					if (level2URL != '' && hasChildArr.eq(j).children('MenuItem').length <= 0)
						level2Data = level2URL;
					else
						level2Data = "";
					
					if(level3 != "")
						level3Exists = "level3Exists";

					leve2Test += '<li class="second-level-list "><ul class="level-2-reference"><li class="level-2-event"><a rel="'+level2Data+'" href="javascript:void(0)"><img src="'+level2ImgSRC+'" alt="" class="menu-small-icon" />'+level2Title+'</a></li>' + level3 + '</ul></li>';
			}
			
			level2 = '<li class="level-2 '+level3Exists+'" style="display: none;"><ul>'+leve2Test+'</ul></li>';
	}else{
		level2 = "";
	}	
        var level1URL = SecurityObjectAsQS(firstLevelMenuArr.eq(i));
        if (level1URL != '' && firstLevelMenuArr.eq(i).children('MenuItem').length <= 0)
		level1Data = level1URL;
	else
		level1Data = "";	
		
	var level1ImgSRC =firstLevelMenuArr.eq(i).attr('icon');
	var level1Title = firstLevelMenuArr.eq(i).attr('label');
	var menuArrow = "";
	if(level3Exists != "")
		menuArrow = '<span class="menu-arrow"></span>';
	var level1Event = $('<li class="first-level-list"><ul class="level-1-reference"><li class="level-1-event"><a rel="'+level1Data+'" href="javascript:void(0);"><img src="'+level1ImgSRC+'" alt="" class="menu-small-icon"><span class="menu-text">'+level1Title+'</span>'+menuArrow+'</a></li>'+level2+'</ul></li>')
	$(level1Event).appendTo("#first-level");
	}
	
	$('#item-logout').click(goToLoginScreen);
	$('#item-changepwd').click(doChangePwd);
	$('#item-forgotpwd').click(doForgotPwd);
	$('#notificationTxtMsg').click(onNotificationTxtMsgClicked);
	
	/*Single level menu */
	if ($('.level-2').length == 0)
	{
		var target = $("#first-level");  		  
		$("#submenu-container").html(target.html());
		$(".iapp-header").html('');
		$("#menu-icon").on("click", function(e){
			var container = $("#submenu-container");
			if(container.is(":visible")){
				e.preventDefault(); container.slideUp();
			}else{
				container.slideDown();
			}				
		});		
	}
	
	$('.iapp-logo').click(goToLoginScreen);
	$('#menu-info-icon').show();
	$('#menu-info-icon').css('display','inline');
	triggerAfterLoad[1] = true;
	loadFirstMenuItem();
	//checkForNewMessages();

	/*$(".menu-text, .menu-arrow ").bind("mouseover",function(){
		// $(this).rotate({animateTo:45});
		$(this).parent().find(".menu-arrow").addClass("menu-arrow-over");
	});
	$(".menu-text, .menu-arrow ").bind("mouseout",function(){
		// $(this).rotate({animateTo:0});
		$(this).parent().find(".menu-arrow").removeClass("menu-arrow-over");
	});*/
    $('.iapp-logo').attr('title', 'Logout');
    $('#notificationTxtMsg').attr('title', 'Text messages');
	
}

function onNotificationTxtMsgClicked(){
	notificationTxtMsgSelcted = true;
	if(triggerAfterLoad[0] == true && triggerAfterLoad[1] == true){
		$(".first-level-list").first().removeClass("firstLeveFocus");
		$(".first-level-list").first().find(".level-1-event").trigger('click');
	}
}

function loadFirstMenuItem(){
	if(triggerAfterLoad[0] == true && triggerAfterLoad[1] == true){
		if(loginFirst)
		$(".first-level-list").first().find(".level-1-event").trigger('click');
	}
}

function SecurityObjectAsQS(obj) {
    var soList = obj.children('securityobject');
    var urlString = "securityobjects=";

    if (soList == undefined || soList.length == 0) {
        return obj.attr('data');
    } else {

        for (var j = 0; j < soList.length; j++) {
            if (j == soList.length - 1)
                urlString += soList.eq(j).attr("Name");
            else
                urlString += soList.eq(j).attr("Name") + ",";
        }
        if (obj.attr('data').indexOf("?") == -1)
            return obj.attr('data') + "?" + urlString;
        else
            return obj.attr('data') + "&" + urlString;

    }

}

function MenuFail(data) {
    if (data != null) {
        var errorDesc = $.trim($(data).find('Description').text());
        alert(" Error :" + errorDesc);
    } else {
        alert(" Unknown Error ");
    }
    goToLoginScreen();
}

function goToLoginScreen(){
	//clientAPIManager.logOutSession();
	clientAPIManager.cleanLogOutSession();
}

function doChangePwd(){
	alert('Change password functionality goes here.');	
}

function doForgotPwd(){
	alert('Forgot password functionality goes here.');
}

function level1MenuSel(obj) {
	$('#ms1').hide();
	$("#s1").show();
	var curText = obj.textContent;
	$('#level1').text("");
	$('#level2').text(curText);
}

function level2MenuSel(obj) {
	$("#s1").show();
	var level1Text = $('.menu-text', $(obj).closest(".first-level-list")).text();
	var curText = obj.textContent;
	$('#level1').text(level1Text);
	$('#level2').text(curText);
	$('#ms1').show();
}

$(function(){
	$('.iapp-logo').click(goToLoginScreen);
	if(sessionStorage.uName!=null && sessionStorage.uName!="") {
		$('#username').show();
		$('#username').html("Welcomec"+ sessionStorage.uName);
		$('#user-msg').html('Welcome '+sessionStorage.uName+ '<div class="iapp-logo"></div>');
	}

	var windowHeight =  $(window).height();	
	var menuData = clientAPIManager.getMenuXML();
	
	var menuData = '<ApplicationMenu><MenuItem label="Patients" data="ui/ts/ui/html/patients/patientList.html" icon="../../styles/default/images/patient.png"></MenuItem><MenuItem label="Appointments" data="app/ts/ui/html/report/report.html" icon="../../styles/default/images/Appointment.png"></MenuItem><MenuItem label="Health Records" data="app/ts/ui/html/health/healthMenu.html" icon="../../styles/default/images/Diet.png"></MenuItem><MenuItem label="Accounts" data="app/ts/ui/html/report/report.html" icon="../../styles/default/images/accounts.png"></MenuItem><MenuItem label="Masters" data="app/ts/ui/html/masters/masterList.html"></MenuItem><MenuItem label="Reports"><MenuItem label="Patients" data="app/ts/ui/html/report/report.html" icon=""></MenuItem><MenuItem label="Doctors" data="app/ts/ui/html/report/report.html" icon=""></MenuItem><MenuItem label="Appointments" data="app/ts/ui/html/report/report.html" icon=""></MenuItem></MenuItem><MenuItem label="Settings"><MenuItem label="Country" data="app/ts/ui/html/country/countryList.html" icon=""></MenuItem><MenuItem label="County" data="app/ts/ui/html/report/report.html" icon=""></MenuItem><MenuItem label="Zip" data="app/ts/ui/html/zip/zipList.html" icon=""></MenuItem></MenuItem></ApplicationMenu>';
	
	menuData = $(menuData);
	if(menuData == null || $.trim(menuData)=='' || $.trim(menuData)=='undefined') {
		var serverCatalogName = sessionStorage.serverMenu;
        if (sessionStorage.userOrganization != null && $.trim(sessionStorage.userOrganization) != "") {
            serverCatalogName = sessionStorage.userOrganization + '_' + serverCatalogName;
        }
        var menuName = {
            'menuName': serverCatalogName
        };
        fetchMenu(menuName, MenuSuccessHandler, MenuFail);
    } else {
		MenuSuccess(menuData);//$(''+clientAPIManager.getMenuXML()+''));
	}	
	
	$("#menu-info-icon").on("click", function(e){
		var container = $("#menu-info-box");
		if(container.is(":visible")){
			e.preventDefault();
			$(".pull-menu-list").slideToggle("slow");
		}else{
			$(".pull-menu-list").slideToggle("slow");
		}				
	});
	
	$("#menu-icon").on("click mouseenter", function(e){
		
		var container = $("#side-menu");
		if(container.is(":visible")){
			e.preventDefault();
		}else{
			$("#side-menu").animate({
			}, function(){
				$("#side-menu .level-2").css('left', $('.first-level-list').position().left+"px");
			});

			$("#menu-icon").removeClass("active");
		}				
	});
	$(document).on("mouseenter", "#side-menu", function(){
		if( !$(".menu-text").is(":visible")  ){
			$(".menu-text").show();
			$("#menu-icon").addClass("active");
		}	
		$("#side-menu .level-2").css('left', $('.first-level-list').position().left+"px");		
	});
	
	$(document).on("click",".level-1-reference li.level-1-event", function(e){
		 validNavigation = true;

		var self = $(this);			
		if($(self).closest('.first-level-list').hasClass("firstLeveFocus") != true) {
			if ($('.level-2').length != 0)
				$('#iframe').addClass('greyOut');
			var url = $(self).find('a').attr('rel');
			if(url != ""){
				sessionStorage.curPage = this.textContent;
				clientAPIManager.sendIframeRequest(url);
				level1MenuSel(this);
				loadMenu();
			}
			  
			$(".second-level-list").removeClass("secondLevelFocus");
			$(".second-level-list").removeClass("bottomNotch");
			$(self).closest(".first-level-list").toggleClass("firstLeveFocus").siblings().removeClass("firstLeveFocus").find(".level-2").fadeOut();
			
			$(self).closest(".submenu-list .first-level-list").toggleClass("bottomNotch").siblings().removeClass("bottomNotch").find(".level-2").fadeOut();
			var target = $(self).siblings(".level-2");  		  			
			if(target.length>0) {
				$(".dropdownMenuLevel2").html("");
				$(".dropdownMenuLevel2").css({left:($(self).offset().left + 10)})
				$("#submenu-container").html(target.html());
				if(target.hasClass("level3Exists")){					
					$("#submenu-container").html("");
					$(".dropdownMenuLevel2").hide();
					convertinitDropdownList($(".dropdownMenuLevel2").html(target.html()));
					$(".dropdownMenuLevel2").slideDown(200, function() {
						inThirdLevel = "3menu";
						setTimeout(function(){
							preload3levelto2level();
						},200);
						
					});
					$(".menu-text").each(function (i,val){
						$(val).parent().find(".menu-arrow").removeClass("menu-arrow-over");
					});
					$(self).find(".menu-arrow").addClass("menu-arrow-over");
				}
				else{
					onDropdownListClear();
					var mitem = $("#submenu-container").find(".level-2-reference li.level-2-event")[0];
					// if($("#submenu-container").find(".level-2-reference li.level-2-event").length == 1 || loginFirst){
						loginFirst = false;
						$(mitem).trigger("click");				
					// }
					inThirdLevel = "";
				}
				/*if(notificationTxtMsgSelcted){
					var mitem = $("#submenu-container").find(".level-2-reference li.level-2-event");
					for (var kk = 0; kk < mitem.length; kk++) {
						if($(mitem[kk]).find("a").text()=="Text Messages"){
							notificationTxtMsgSelcted = false;
							$(mitem[kk]).trigger("click");
						}
					};
				}*/
			}
		}
		else{
			/*if($(".dropdownMenuLevel2").css('display') != "none"){
				$('#iframe').removeClass('greyOut');
			}*/
			$(".dropdownMenuLevel2").slideToggle(200);
			$(self).find(".menu-arrow").toggleClass("menu-arrow-over");
		}
	});

	$(document).on("click", ".level-2-reference li.level-2-event", function(){
	 	validNavigation = true;
		if ($('.level-2').length != 0)
			$('#iframe').removeClass('greyOut');
		var self = $(this);
		if(inThirdLevel != "3menu"){
			$(".dropdownMenuLevel2").slideUp(200);
			$(".menu-text").each(function (i,val){
				$(val).parent().find(".menu-arrow").removeClass("menu-arrow-over");
			});
		}
		
		if($(self).closest('.second-level-list').hasClass("secondLevelFocus") != true) { 
			var url = $(self).find('a').attr('rel');
			if(url != ""){
				sessionStorage.curPage = this.textContent;
				clientAPIManager.sendIframeRequest(url);
				level2MenuSel(this);
				loadMenu();
			}
			var target = $(self).siblings(".level-3");
			$(self).closest(".second-level-list").toggleClass("bottomNotch").toggleClass("secondLevelFocus").siblings().removeClass("secondLevelFocus").removeClass("bottomNotch").find(".level-3").slideUp("slow");	
			
			if( target.is(":visible") )
				target.slideUp("slow", function(){
					$(self).closest(".level-2").animate({
						top: (target.closest(".level-2").height()+61)+"px"
					})
				});
			else{
				target.slideDown("slow", function(){
					var bodyHeight = target.offset().top + target.closest(".level-2").height();
					var winHeight = $(window).height();
					var level3Scroll = $(window).height()-150
					var level3Ref = $(self).siblings(".level-3").find(".level-3-reference");
					if(level3Ref.height() > level3Scroll){
						$(level3Ref).css({
							'max-height': level3Scroll+"px",
							'overflow-y':'scroll'
						});
					}	
					
					if( bodyHeight > winHeight){
						var top = 0;
						if( target.closest(".level-2").height() > level3Scroll ){
							top = (target.closest(".level-2").outerHeight()-level3Scroll)-25;
						}else{
							top = target.offset().top - target.closest(".level-2").height();
						}
						$(self).closest(".level-2").animate({
							top: top,
						})
					}
				});
			}		
		}
	});
	
	$(document).on("mouseup", function (e){
		var container = $("#side-menu");
		var targetId = $.trim( $(e.target).attr('id') );
		
		if(targetId == "menu-icon" && !$(e.target).hasClass("active")){
			$("#side-menu").trigger("mouseenter");
			$(e.target).addClass("active");
		}
		else if (targetId!="menu-info-icon"){
			$("#menu-info-box").hide();
		}
	});

});

function convertinitDropdownList(ele){
	ele.find("li").each(function(i,val){
		try{
			if($(val).attr("class").indexOf("level-3") == -1){
				if($(val).attr("class").indexOf("second-level-list") != -1){
					$(val).attr("class","second-level-dropdown-list");
				}else
					$(val).attr("class","");
			}
		}
		catch(e){}
	});
	ele.find("ul").each(function(i,val){
		try{
			if($(val).attr("class").indexOf("level-2-reference") != -1)
			$(val).attr("class","level-2-dropdown-reference");
		}
		catch(e){}
	});
	$(".second-level-dropdown-list").off("click",onDropdownListClick);
	$(".second-level-dropdown-list").on("click",onDropdownListClick);
}

function preload3levelto2level(){
	$($($(".second-level-dropdown-list")[0]).find("a")[0]).addClass('current');
	$("#submenu-container").html(convertlevel3tolevel2($($(".second-level-dropdown-list")[0]).find(".level-3")));
	setTimeout(function(){
		var mitem = $("#submenu-container").find(".level-2-reference li.level-2-event")[0];
		$(mitem).trigger("click");			
	},200);
}

function onDropdownListClick(e){
	inThirdLevel = "3menuClicked";
	onDropdownListClear();
	$(e.target).addClass('current');
	$("#submenu-container").html(convertlevel3tolevel2($(e.currentTarget).find(".level-3")));
	setTimeout(function(){
		var mitem = $("#submenu-container").find(".level-2-reference li.level-2-event")[0];
		$(mitem).trigger("click");			
	},200);
}

function onDropdownListClear(){
	$(".menu-text").each(function (i,val){
		$(val).parent().find(".menu-arrow").removeClass("menu-arrow-over");
	});
	$(".dropdownMenuLevel2 a").each(function (i,val){
		if($(val).hasClass("current"))
		{
			$(val).removeClass("current");
		}
	});
}

function convertlevel3tolevel2(ele){
	if(inThirdLevel != "3menu"){
		$(".dropdownMenuLevel2").slideUp(200);
		$(".menu-text").each(function (i,val){
			$(val).parent().find(".menu-arrow").removeClass("menu-arrow-over");
		});
	}
	var str = "<ul>";
	ele.find("li").each(function(i,val){
		try{
			if($(val).attr("class").indexOf("level-3-event") != -1){
				// $(val).attr("class","level-2-event");
				str += '<li class="second-level-list">';
				str += '			<ul class="level-2-reference">';
				str += '				<li class="level-2-event">';
				str += $(val).html();
				str += '				</li>';
				str += '			</ul>';
				str += '</li>';
			}
		}
		catch(e){}
	});
	str += "</ul>"
	/*ele.find("ul").each(function(i,val){
		try{
			if($(val).attr("class").indexOf("level-3-reference") != -1){
				$(val).attr("class","level-2-reference");
			}
		}
		catch(e){}
	});*/
	return str;
}

function loadMenu(){
	$("#menu-info-box").hide("slow");
	var container = $("#side-menu");
	if( container.is(":visible") ){
		container.animate({
		}, function(){
			$("#side-menu .level-2").css('left', $('.first-level-list').position().left+"px");
			$(".level-2, .level-3").hide();
		});
	}else{
	}
	setTimeout(function(){validNavigation = false;}, 100);
}
function checkReference() {
    var isUnloadFlagSet = false;
    if ($("#iframe")[0].contentWindow.unloadFlagOnMenuChange != null) {
        isUnloadFlagSet = $("#iframe")[0].contentWindow.unloadFlagOnMenuChange;
    }
    return isUnloadFlagSet;

}

function continueMenuEvent() {
    sessionStorage.curPage = tempTextContent;
    clientAPIManager.sendIframeRequest(tempUrl);
    if (levelNum == 1) {
        level1MenuSel(tempThis);
    } else if (levelNum == 2) {
        level2MenuSel(tempThis);
    } else if (levelNum == 3) {
        level3MenuSel(tempThis);
    }
    loadMenu();
}
var validNavigation = false;

function endSession() {
    // Browser or broswer tab is closed
    // Do sth here ...
    //clientAPIManager.logOutSession();         
    clientAPIManager.cleanLogOutSession();
    validNavigation = true;
}

function wireUpEvents() {

    window.onbeforeunload = function() {
        if (!validNavigation) {
            endSession();
        }
    }

    // Attach the event keypress to exclude the F5 refresh
    $(document).bind('keypress', function(e) {
        if (e.keyCode == 116) {
            validNavigation = true;
        }
    });

    // Attach the event click for all links in the page
    $("a").bind("click", function() {
        validNavigation = true;
    });

    // Attach the event submit for all forms in the page
    $("form").bind("submit", function() {
        validNavigation = true;
    });

    // Attach the event click for all inputs in the page
    $("input[type=submit]").bind("click", function() {
        validNavigation = true;
    });

}


$(document).ready(function() {
    $("#lastLgnTime").html("Last Login Time : " + sessionStorage.lastLoginTime);
    var leftVal = ((window.innerWidth - ($("#poweredby").width() + $("#copyright").width())) / 2 - ($("#lastLgnTime").width() / 2));
    $("#lastLgnTime").attr("style", "padding-left:" + leftVal + "px !important;");
    if (sessionStorage.showCopyRight != undefined && sessionStorage.showCopyRight != "yes")
        $("#copyright").hide();

    if (sessionStorage.showLastLoginTime != undefined && sessionStorage.showLastLoginTime != "yes")
        $("#lastLgnTime").hide();
    wireUpEvents();
});

$(window).resize(function() {
    if (sessionStorage.showLastLoginTime != undefined && sessionStorage.showLastLoginTime == "yes") {
        var leftVal = ((window.innerWidth - ($("#poweredby").width() + $("#copyright").width())) / 2 - ($("#lastLgnTime").width() / 2));
        $("#lastLgnTime").attr("style", "padding-left:" + leftVal + "px !important;");
    }
})
var pWid = 300;
var pHei = 150;

function getDimesionsforAlerts(alertmessage) {
	if(alertmessage){
		if (alertmessage.length < 90) {
	        pWid = 300;
	        pHei = 120;
	    }else if (alertmessage.length < 100) {
	        pWid = 300;
	        pHei = 150;
	    } else if (alertmessage.length < 150) {
	        pWid = 300;
	        pHei = 150;
	    } else if (alertmessage.length >= 150 && alertmessage.length <= 200) {
	        pWid = 350;
	        pHei = 180;
	    } else if (alertmessage.length > 200 && alertmessage.length < 250) {
	        pWid = 400;
	        pHei = 200;
	    } else if (alertmessage.length > 250) {
	        pWid = 500;
	        pHei = 250;
	    }
	}
}

function displayInfoPopUp(alerttitle, alertmessage) {
    console.log(alertmessage + "  \n\n " + alerttitle);
    getDimesionsforAlerts(alertmessage);
    $.when(kendo.ui.ExtAlertDialog.show({
        title: alerttitle,
        message: alertmessage,
        icon: "k-ext-information",
        width: pWid,
        height: pHei
    }))
    $(".k-ext-dialog-buttons button").removeClass('k-button');
    $(".k-ext-dialog-buttons button").addClass('btn-save');
    try{
    	$(".k-ext-dialog-buttons button").text(getLocaleStringWithDefault("VF_UI_COMMON", "OK", "Ok"));
    }
    catch(e){}
}

function displayErrorPopUp(alerttitle, alertmessage) {
    console.log(alertmessage + "  \n\n " + alerttitle);
    getDimesionsforAlerts(alertmessage);
    $.when(kendo.ui.ExtAlertDialog.show({
        title: alerttitle,
        message: alertmessage,
        icon: "k-ext-error",
        width: pWid,
        height: pHei
    }))

    $(".k-ext-dialog-buttons button").removeClass('k-button');
    $(".k-ext-dialog-buttons button").addClass('btn-save');
    try{
    	$(".k-ext-dialog-buttons button").text(getLocaleStringWithDefault("VF_UI_COMMON", "OK", "Ok"));
    }
    catch(e){}
}

function confirmDialog(titletxt, msg, yescallback,nocallback) {
    getDimesionsforAlerts(msg);
    $.when(kendo.ui.ExtYesNoDialog.show({
        title: titletxt,
        message: msg,
        icon: "k-ext-question",
        width: pWid,
        height: pHei
    })).done(function(response) {
        if (response.button == "Yes") {
        	if(yescallback)
            $('#resp').html(kendo.format("{0} button clicked", yescallback(response)));
        } else {
        	if(nocallback)
            $('#resp').html(kendo.format("{0} button clicked"),nocallback(response));
        }
    });
    $(".k-ext-dialog-buttons button").removeClass('k-button');
    $(".k-ext-dialog-buttons button").each(function(i,val){
    	if($(val).text().toLowerCase() == "yes"){
	    	$(val).addClass('btn-save');
	    	try{
		    	$(val).text(getLocaleStringWithDefault("VF_UI_COMMON", "YES", "Yes"));
		    }
		    catch(e){}
    	}
	    if($(val).text().toLowerCase() == "no"){
	    	$(val).addClass('btn-cancel');
	    	try{
		    	$(val).text(getLocaleStringWithDefault("VF_UI_COMMON", "NO", "No"));
		    }
		    catch(e){}
	    }
    });
   
}

function getAvailPageHeight(){
	var hHeight = $("header").height();
	var fHeight = $("footer").height();

	return hHeight+fHeight;
}

function showContentFrame(argument) {
	if(argument){
		$("#iframe").show();
	}else{
		$("#iframe").hide();
	}
}

$(window).load(function() {
	
	var dt = new Date();
	$("#tmZone").text(dt); 
	
	setInterval(
			function(){ 
					var dt = new Date();
					//$("#tmZone").text(dt); 
				}, 1000);

    var filename = [["../../styles/"+sessionStorage.userOrgKey+"/css/serverMenu.css", "css", "true"]];
    filename.push(["../../styles/"+sessionStorage.userOrgKey+"/css/newTheme.css", "css"]);
    DCSSloadChildResources(filename, document);    
    triggerAfterLoad[0] = true;
	loadFirstMenuItem();
});

var filesLoaded = 0;
var docObj = document;
var filesList = [];

function DCSSloadChildResources(files, rootObj) {
    // body...
    filesList = files;
    filesLoaded = 0;
    docObj = rootObj;
    DCSSloadFiles();

}

function DCSSloadFiles() {
    if(filesLoaded >= filesList.length){
    	if(filesList[0][2] != "true")
        showContentFrame(true);
    	$("body #page").css({"display":"block"});
        return;
    }

    DCSSloadResource(filesList[filesLoaded], DCSSloadFiles)
    // body...
}

function DCSSloadResource(url, callback){

    if (url[1] == "js") {
        var file = docObj.createElement('script')
        file.setAttribute("type", "text/javascript")
        file.setAttribute("src", url[0])
    } else if (url[1] == "css") {
        var file = docObj.createElement("link")
        file.setAttribute("rel", "stylesheet")
        file.setAttribute("type", "text/css")
        file.setAttribute("href", url[0])
        file.setAttribute("onError", errorCall)
    }
    if (file.readyState){  //IE
        file.onreadystatechange = function(){
            if (file.readyState == "loaded" || file.readyState == "complete"){
                file.onreadystatechange = null;
                filesLoaded++;
                callback();
            }
        };
    } else {  //Others
        file.onload = function(){
            filesLoaded++;
            callback();
        };    
    }

    if (typeof file != "undefined")
        docObj.getElementsByTagName("head")[0].appendChild(file)

    $.ajax({
	  url: url[0],
	  context: document
	}).done(function(data) {
	}).error(function(){
		filesLoaded++;
        callback();
	})

}

function errorCall(argument) {
	alert("error");
}
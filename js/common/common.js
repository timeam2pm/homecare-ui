var ipAddress, wsURL = "wss://27.7.61.73/comm-handshake?userId=",
    callIdleTime = 6e4,
    videoCallVer = "2.0",
    sessionExpTime = 10,
    videoPatientId = "",
    url = window.location.origin;
sessionStorage.setItem("IsSearchPanel", "0"); // for Grid Search Here "0" hide and "1" show
sessionStorage.setItem("IsPostalCodeManual", "1"); // for Grid Search Here "0" hide and "1" show
sessionStorage.setItem("CareVersion", "2"); // for Grid Search Here "0" hide and "1" show


function validateEmail(e) {
    console.log(e);
    return !!/^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/.test(e)
}

function loadAPi(file) {
    var head = document.getElementsByTagName('body');
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = file;
    head.appendChild(script);
}

function getComboListIndex(e, a, s) {
    var n = $("#" + e).data("kendoComboBox");
    if (n)
        for (var t = n.dataSource, o = t.total(), i = 0; i < o; i++) {
            var r = t.at(i);
            if (r && r[a] == s) return i
        }
    return -1
}

function getAge(e) {
    var a = new Date,
        s = new Date(e),
        n = a.getFullYear() - s.getFullYear(),
        t = a.getMonth() - s.getMonth();
    return (t < 0 || 0 === t && a.getDate() < s.getDate()) && n--, n
}

function setComboBoxIndex(e, a) {
    if (a >= 0) {
        var s = $("#" + e).data("kendoComboBox");
        s && s.select(a)
    }
}

 ipAddress = "http://27.7.61.73:8080";

//url.toLowerCase().includes("localhost:") ? ipAddress = "https://apps.timeam.com/" : url.toLowerCase().includes("apps.") ? ipAddress = url : url.toLowerCase().includes("app.") && (ipAddress = url);

//url.toLowerCase().includes("localhost:") ? ipAddress = "http://27.7.61.73/" : url.toLowerCase().includes("27.") ? ipAddress = url : url.toLowerCase().includes("app.") && (ipAddress = url);
var 
    US_DATE_FMT = "MM/dd/yyyy",
    INDIA_DATE_FMT = "dd/MM/yyyy",
    ENG_DATE_FMT = "dd/MM/yyyy",
    US_DATE_FMT1 = "MM/dd/yyyy h:mm:ss tt",
    INDIA_DATE_FMT1 = "dd/MM/yyyy h:mm:ss tt",
    ENG_DATE_FMT1 = "dd/MM/yyyy h:mm:ss tt";

function getCountryDateFormat() {
    var e = "dd/MM/yyyy",
        a = sessionStorage.countryName;
    return a.indexOf("India") >= 0 ? e = INDIA_DATE_FMT : a.indexOf("United Kingdom") >= 0 ? e = ENG_DATE_FMT : a.indexOf("United State") >= 0 && (e = US_DATE_FMT), e
}

function getCountryDateTimeFormat() {
    var e = "dd/MM/yyyy h:mm:ss tt",
        a = sessionStorage.countryName;
    return a.indexOf("India") >= 0 ? e = INDIA_DATE_FMT1 : a.indexOf("United Kingdom") >= 0 ? e = ENG_DATE_FMT1 : a.indexOf("United State") >= 0 && (e = US_DATE_FMT1), e
}

function getCountryName() {
    var e = [],
        a = {
            C: "Baker Island,Howland Island",
            T: "720"
        };
    e.push(a), (a = {}).C = "American Samoa,Jarvis Island,Kingman Reef,Midway Islands,Niue,Palmyra Atoll", a.T = "660", e.push(a), (a = {}).C = "Cook Islands,French Polynesia (Pape'ete),Johnston Atoll,United States (Honolulu, Adak)", a.T = "600", e.push(a), (a = {}).C = "French Polynesia (Marquesas Islands)", a.T = "570", e.push(a), (a = {}).C = "French Polynesia (Mangareva),United States (Anchorage)", a.T = "540", e.push(a), (a = {}).C = "Canada (Vancouver),Mexico (Tijuana),Pitcairn Islands,United States (Los Angeles)", a.T = "480", e.push(a), (a = {}).C = "Canada (Edmonton),Mexico (La Paz),United States (Denver, Phoenix)", a.T = "420", e.push(a), (a = {}).C = "Belize,Canada (Winnipeg, Regina),Chile (Easter Island),Costa Rica,Galapagos Islands,El Salvador,Guatemala,Honduras,Mexico (Mexico City),Nicaragua,United States (Chicago)", a.T = "360", e.push(a), (a = {}).C = "Bahamas,Brazil (Rio Branco),Canada (Ottawa),Cayman Islands,Colombia,Cuba,Ecuador,Haiti,Jamaica,Navassa Island,Panama,Peru,United States (Washington, DC)", a.T = "300", e.push(a), (a = {}).C = "Anguilla,Antigua and Barbuda,Aruba,Chile (Santiago),Barbados,Bermuda,Bolivia,Brazil (Manaus),Canada (Halifax),Cura�ao,Dominica,Dominican Republic,Greenland (Thule Air Base),Grenada,Guadeloupe,Guyana,Martinique,Montserrat,Paraguay,Puerto Rico,Saint Kitts and Nevis,Saint Lucia,Saint Vincent and the Grenadines,Sint Maarten,Turks and Caicos Islands, Trinidad and Tobago,US Virgin Islands,British Virgin Islands,Venezuela,United States", a.T = "240", e.push(a), (a = {}).C = "Canada (Newfoundland)", a.T = "210", e.push(a), (a = {}).C = "Argentina,Brazil (Brasilia),Falkland Islands,French Guiana,Greenland (Nuuk),Saint Pierre and Miquelon,Suriname,Uruguay", a.T = "180", e.push(a), (a = {}).C = "Brazil (Fernando de Noronha),South Georgia and the South Sandwich Islands", a.T = "120", e.push(a), (a = {}).C = "Cape Verde,Greenland (Ittoqqortoormiit),Portugal (Azores)", a.T = "60", e.push(a), (a = {}).C = "Burkina Faso,Faroe Islands,The Gambia,Ghana,Guinea,Guinea-Bissau,Iceland,Ireland,Liberia,Mali,Mauritania,Morocco,Portugal (Lisbon),Saint Helena,Senegal,Sierra Leone, Spain (Canary Islands),Togo,United Kingdom,Western Sahara", a.T = "0", e.push(a), (a = {}).C = "Albania,Andorra,Algeria,Angola,Austria,Belgium,Benin,Bosnia and Herzegovina,Cameroon,Central African Republic,Chad,Republic of the Congo,Congo, Democratic Republic of the (Kinshasa),Croatia,Czech Republic,Denmark,France,Gabon,Germany,Gibraltar,Hungary,Italy,Libya,Liechtenstein,Luxembourg,Republic of Macedonia,Malta,Monaco,Netherlands,Niger, Nigeria,Norway,Poland,San Marino,Serbia and Montenegro,Slovakia,Spain (Madrid),Sweden,Switzerland,Tunisia,Vatican City,Burkina Faso,Faroe Islands,The Gambia,Ghana,Guinea,Guinea-Bissau,Iceland,Ireland,Liberia,Mali,Mauritania,Morocco,Portugal (Lisbon),Saint Helena,Senegal,Sierra Leone, Spain (Canary Islands),Togo,United Kingdom,Western Sahara", a.T = "-60", e.push(a), (a = {}).C = "Botswana,Bulgaria,Burundi,Democratic Republic of the Congo (Lubumbashi),Cyprus,Egypt,Estonia,Finland,Greece,Israel,Jordan,Latvia,Lebanon,Lesotho,Lithuania,Malawi, Moldova,Mozambique,Namibia,Palestine,Romania,Russia (Kaliningrad),Rwanda,South Africa,Sudan,Swaziland,Syria,Ukraine,Zambia,Zimbabwe", a.T = "-120", e.push(a), (a = {}).C = "Yemen,Uganda,Tanzania,South Sudan,Somalia,Saudi Arabia,Bahrain,Belarus,Comoros,Djibouti,Eritrea,Ethiopia,Iraq,Kenya,Kuwait,Madagascar,Qatar,Russia (Moscow)", a.T = "-180", e.push(a), (a = {}).C = "Iran", a.T = "-210", e.push(a), (a = {}).C = "Reunion,Russia (Samara),Seychelles,United Arab Emirates,Oman,Mauritius,Armenia,Azerbaijan,Georgia", a.T = "-240", e.push(a), (a = {}).C = "Afghanistan", a.T = "-270", e.push(a), (a = {}).C = "Kazakhstan (Aktobe),Maldives,Pakistan,Russia (Yekaterinburg),Tajikistan,Turkmenistan,Uzbekistan", a.T = "-300", e.push(a), (a = {}).C = "India,Sri Lanka", a.T = "-330", e.push(a), (a = {}).C = "Nepal", a.T = "-345", e.push(a), (a = {}).C = "Bangladesh,Bhutan,British Indian Ocean Territory,Kazakhstan (Astana),Kyrgyzstan,Russia (Omsk)", a.T = "-360", e.push(a), (a = {}).C = "Cocos (Keeling) Islands,Myanmar", a.T = "-390", e.push(a), (a = {}).C = "Cambodia,Christmas Island,Indonesia (Jakarta),Laos,Russia (Novosibirsk),Thailand,Vietnam", a.T = "-420", e.push(a), (a = {}).C = "Australia (Perth),Brunei,China,Hong Kong,Indonesia (Bali),Macau,Malaysia,Mongolia (Ulaanbaatar),Philippines,Russia (Irkutsk),Singapore,Taiwan", a.T = "-480", e.push(a), (a = {}).C = "North Korea", a.T = "-510", e.push(a), (a = {}).C = "Australia (Eucla)", a.T = "-525", e.push(a), (a = {}).C = "East Timor,Indonesia (Jayapura),Japan,South Korea,Palau,Russia (Yakutsk)", a.T = "-540", e.push(a), (a = {}).C = "Australia (Adelaide)", a.T = "-570", e.push(a), (a = {}).C = "Australia (Canberra),Guam,Federated States of Micronesia (Yap), Northern Mariana Islands,Papua New Guinea (Port Moresby),Russia (Vladivostok)", a.T = "-600", e.push(a), (a = {}).C = "Australia (Lord Howe Island)", a.T = "-630", e.push(a), (a = {}).C = "Federated States of Micronesia (Palikir),New Caledonia,Norfolk Island,Russia (Magadan, Sakhalin),Solomon Islands,Vanuatu", a.T = "-660", e.push(a), (a = {}).C = "Fiji,Kiribati (Gilbert Islands),Marshall Islands,Nauru,New Zealand (Wellington),Russia (Kamchatka Krai),Tuvalu,United States (Wake Island),France (Wallis and Futana)", a.T = "-720", e.push(a), (a = {}).C = "New Zealand (Chatham Islands)", a.T = "-765", e.push(a), (a = {}).C = "Kiribati (Phoenix Islands),Tonga,Samoa,Tokelau", a.T = "-780", e.push(a), (a = {}).C = "Kiribati (Line Islands)", a.T = "-840", e.push(a);
    for (var s = new Date, n = 0; n < e.length; n++) {
        var t = e[n];
        if (Number(t.T) == s.getTimezoneOffset()) return t.C
    }
    return ""
}


function getCountyNameByTZ(tz){
    var tzArray = [];
    tzArray.push({Country:"American Samoa,Jarvis Island,Kingman Reef,Midway Islands,Niue,Palmyra Atoll",Key:"(GMT-11:00) Midway Island, Samoa",Value:'Pacific/Midway',Offset:660});
    tzArray.push({Country:"Cook Islands,French Polynesia (Pape'ete),Johnston Atoll,United States (Honolulu, Adak)",Key:"(GMT-10:00) Hawaii",Value:'Pacific/Honolulu',Offset:600});
    tzArray.push({Country:"French Polynesia (Mangareva),United States (Anchorage)",Key:"(GMT-09:00) Alaska",Value:'US/Alaska',Offset:540});
    tzArray.push({Country:"Canada (Vancouver),Mexico (Tijuana),Pitcairn Islands,United States (Los Angeles)",Key:"(GMT-08:00) Pacific Time (US & Canada)",Value:'America/Los_Angeles',Offset:480});
    tzArray.push({Country:"Canada (Vancouver),Mexico (Tijuana),Pitcairn Islands,United States (Los Angeles)",Key:"(GMT-08:00) Tijuana, Baja California",Value:'America/Tijuana',Offset:480});
    tzArray.push({Country:"Canada (Edmonton),Mexico (La Paz),United States (Denver, Phoenix)",Key:"(GMT-07:00) Arizona",Value:'US/Arizona',Offset:420});
    tzArray.push({Country:"Canada (Edmonton),Mexico (La Paz),United States (Denver, Phoenix)",Key:"(GMT-07:00) Chihuahua, La Paz, Mazatlan",Value:'America/Chihuahua',Offset:420});
    tzArray.push({Country:"Canada (Edmonton),Mexico (La Paz),United States (Denver, Phoenix)",Key:"(GMT-07:00) Mountain Time (US & Canada)",Value:'US/Mountain',Offset:420});
    tzArray.push({Country:"Belize,Canada (Winnipeg, Regina),Chile (Easter Island),Costa Rica,Galapagos Islands,El Salvador,Guatemala,Honduras,Mexico (Mexico City),Nicaragua,United States (Chicago)",Key:"(GMT-06:00) Central America",Value:'America/Managua',Offset:360});
    tzArray.push({Country:"Belize,Canada (Winnipeg, Regina),Chile (Easter Island),Costa Rica,Galapagos Islands,El Salvador,Guatemala,Honduras,Mexico (Mexico City),Nicaragua,United States (Chicago)",Key:"(GMT-06:00) Central Time (US & Canada)",Value:'US/Central',Offset:360});
    tzArray.push({Country:"Belize,Canada (Winnipeg, Regina),Chile (Easter Island),Costa Rica,Galapagos Islands,El Salvador,Guatemala,Honduras,Mexico (Mexico City),Nicaragua,United States (Chicago)",Key:"(GMT-06:00) Guadalajara, Mexico City, Monterrey",Value:'America/Mexico_City',Offset:360});
    tzArray.push({Country:"Belize,Canada (Winnipeg, Regina),Chile (Easter Island),Costa Rica,Galapagos Islands,El Salvador,Guatemala,Honduras,Mexico (Mexico City),Nicaragua,United States (Chicago)",Key:"(GMT-06:00) Saskatchewan",Value:'Canada/Saskatchewan',Offset:360});
    tzArray.push({Country:"Bahamas,Brazil (Rio Branco),Canada (Ottawa),Cayman Islands,Colombia,Cuba,Ecuador,Haiti,Jamaica,Navassa Island,Panama,Peru,United States (Washington, DC)",Key:"(GMT-05:00) Bogota, Lima, Quito, Rio Branco",Value:'America/Bogota',Offset:300});
    tzArray.push({Country:"Bahamas,Brazil (Rio Branco),Canada (Ottawa),Cayman Islands,Colombia,Cuba,Ecuador,Haiti,Jamaica,Navassa Island,Panama,Peru,United States (Washington, DC)",Key:"(GMT-05:00) Eastern Time (US & Canada)",Value:'US/Eastern',Offset:300});
    tzArray.push({Country:"Bahamas,Brazil (Rio Branco),Canada (Ottawa),Cayman Islands,Colombia,Cuba,Ecuador,Haiti,Jamaica,Navassa Island,Panama,Peru,United States (Washington, DC)",Key:"(GMT-05:00) Indiana (East)",Value:'US/East-Indiana',Offset:300});
    tzArray.push({Country:"Anguilla,Antigua and Barbuda,Aruba,Chile (Santiago),Barbados,Bermuda,Bolivia,Brazil (Manaus),Canada (Halifax),Cura�ao,Dominica,Dominican Republic,Greenland (Thule Air Base),Grenada,Guadeloupe,Guyana,Martinique,Montserrat,Paraguay,Puerto Rico,Saint Kitts and Nevis,Saint Lucia,Saint Vincent and the Grenadines,Sint Maarten,Turks and Caicos Islands, Trinidad and Tobago,US Virgin Islands,British Virgin Islands,Venezuela,United States",Key:"(GMT-04:00) Atlantic Time (Canada)",Value:'Canada/Atlantic',Offset:240});
    tzArray.push({Country:"Anguilla,Antigua and Barbuda,Aruba,Chile (Santiago),Barbados,Bermuda,Bolivia,Brazil (Manaus),Canada (Halifax),Cura�ao,Dominica,Dominican Republic,Greenland (Thule Air Base),Grenada,Guadeloupe,Guyana,Martinique,Montserrat,Paraguay,Puerto Rico,Saint Kitts and Nevis,Saint Lucia,Saint Vincent and the Grenadines,Sint Maarten,Turks and Caicos Islands, Trinidad and Tobago,US Virgin Islands,British Virgin Islands,Venezuela,United States",Key:"(GMT-04:00) Caracas, La Paz",Value:'America/Caracas',Offset:240});
    tzArray.push({Country:"Anguilla,Antigua and Barbuda,Aruba,Chile (Santiago),Barbados,Bermuda,Bolivia,Brazil (Manaus),Canada (Halifax),Cura�ao,Dominica,Dominican Republic,Greenland (Thule Air Base),Grenada,Guadeloupe,Guyana,Martinique,Montserrat,Paraguay,Puerto Rico,Saint Kitts and Nevis,Saint Lucia,Saint Vincent and the Grenadines,Sint Maarten,Turks and Caicos Islands, Trinidad and Tobago,US Virgin Islands,British Virgin Islands,Venezuela,United States",Key:"(GMT-04:00) Manaus",Value:'America/Manaus',Offset:240});
    tzArray.push({Country:"Anguilla,Antigua and Barbuda,Aruba,Chile (Santiago),Barbados,Bermuda,Bolivia,Brazil (Manaus),Canada (Halifax),Cura�ao,Dominica,Dominican Republic,Greenland (Thule Air Base),Grenada,Guadeloupe,Guyana,Martinique,Montserrat,Paraguay,Puerto Rico,Saint Kitts and Nevis,Saint Lucia,Saint Vincent and the Grenadines,Sint Maarten,Turks and Caicos Islands, Trinidad and Tobago,US Virgin Islands,British Virgin Islands,Venezuela,United States",Key:"(GMT-04:00) Santiago",Value:'America/Santiago',Offset:240});
    tzArray.push({Country:"Argentina,Brazil (Brasilia),Falkland Islands,French Guiana,Greenland (Nuuk),Saint Pierre and Miquelon,Suriname,Uruguay",Key:"(GMT-03:30) Newfoundland",Value:'Canada/Newfoundland',Offset:180});
    tzArray.push({Country:"Argentina,Brazil (Brasilia),Falkland Islands,French Guiana,Greenland (Nuuk),Saint Pierre and Miquelon,Suriname,Uruguay",Key:"(GMT-03:00) Brasilia",Value:'America/Sao_Paulo',Offset:180});
    tzArray.push({Country:"Argentina,Brazil (Brasilia),Falkland Islands,French Guiana,Greenland (Nuuk),Saint Pierre and Miquelon,Suriname,Uruguay",Key:"(GMT-03:00) Buenos Aires, Georgetown",Value:'America/Argentina/Buenos_Aires',Offset:180});
    tzArray.push({Country:"Argentina,Brazil (Brasilia),Falkland Islands,French Guiana,Greenland (Nuuk),Saint Pierre and Miquelon,Suriname,Uruguay",Key:"(GMT-03:00) Greenland",Value:'America/Godthab',Offset:180});
    tzArray.push({Country:"Argentina,Brazil (Brasilia),Falkland Islands,French Guiana,Greenland (Nuuk),Saint Pierre and Miquelon,Suriname,Uruguay",Key:"(GMT-03:00) Montevideo",Value:'America/Montevideo',Offset:180});
    tzArray.push({Country:"Brazil (Fernando de Noronha),South Georgia and the South Sandwich Islands",Key:"(GMT-02:00) Mid-Atlantic",Value:'America/Noronha',Offset:120});
    tzArray.push({Country:"Cape Verde,Greenland (Ittoqqortoormiit),Portugal (Azores)",Key:"(GMT-01:00) Cape Verde Is",Value:'Atlantic/Cape_Verde',Offset:60});
    tzArray.push({Country:"Cape Verde,Greenland (Ittoqqortoormiit),Portugal (Azores)",Key:"(GMT-01:00) Azores",Value:'Atlantic/Azores',Offset:60});
    tzArray.push({Country:"Burkina Faso,Faroe Islands,The Gambia,Ghana,Guinea,Guinea-Bissau,Iceland,Ireland,Liberia,Mali,Mauritania,Morocco,Portugal (Lisbon),Saint Helena,Senegal,Sierra Leone, Spain (Canary Islands),Togo,United Kingdom,Western Sahara",Key:"(GMT+00:00) Casablanca, Monrovia, Reykjavik",Value:'Africa/Casablanca',Offset:0});
    tzArray.push({Country:"Burkina Faso,Faroe Islands,The Gambia,Ghana,Guinea,Guinea-Bissau,Iceland,Ireland,Liberia,Mali,Mauritania,Morocco,Portugal (Lisbon),Saint Helena,Senegal,Sierra Leone, Spain (Canary Islands),Togo,United Kingdom,Western Sahara",Key:"(GMT+00:00) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London",Value:'Etc/Greenwich',Offset:0});
    tzArray.push({Country:"Albania,Andorra,Algeria,Angola,Austria,Belgium,Benin,Bosnia and Herzegovina,Cameroon,Central African Republic,Chad,Republic of the Congo,Congo, Democratic Republic of the (Kinshasa),Croatia,Czech Republic,Denmark,France,Gabon,Germany,Gibraltar,Hungary,Italy,Libya,Liechtenstein,Luxembourg,Republic of Macedonia,Malta,Monaco,Netherlands,Niger, Nigeria,Norway,Poland,San Marino,Serbia and Montenegro,Slovakia,Spain (Madrid),Sweden,Switzerland,Tunisia,Vatican City,Burkina Faso,Faroe Islands,The Gambia,Ghana,Guinea,Guinea-Bissau,Iceland,Ireland,Liberia,Mali,Mauritania,Morocco,Portugal (Lisbon),Saint Helena,Senegal,Sierra Leone, Spain (Canary Islands),Togo,United Kingdom,Western Sahara",Key:"(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna",Value:'Europe/Amsterdam',Offset:-60});
    tzArray.push({Country:"Albania,Andorra,Algeria,Angola,Austria,Belgium,Benin,Bosnia and Herzegovina,Cameroon,Central African Republic,Chad,Republic of the Congo,Congo, Democratic Republic of the (Kinshasa),Croatia,Czech Republic,Denmark,France,Gabon,Germany,Gibraltar,Hungary,Italy,Libya,Liechtenstein,Luxembourg,Republic of Macedonia,Malta,Monaco,Netherlands,Niger, Nigeria,Norway,Poland,San Marino,Serbia and Montenegro,Slovakia,Spain (Madrid),Sweden,Switzerland,Tunisia,Vatican City,Burkina Faso,Faroe Islands,The Gambia,Ghana,Guinea,Guinea-Bissau,Iceland,Ireland,Liberia,Mali,Mauritania,Morocco,Portugal (Lisbon),Saint Helena,Senegal,Sierra Leone, Spain (Canary Islands),Togo,United Kingdom,Western Sahara",Key:"(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague",Value:'Europe/Belgrade',Offset:-60});
    tzArray.push({Country:"Albania,Andorra,Algeria,Angola,Austria,Belgium,Benin,Bosnia and Herzegovina,Cameroon,Central African Republic,Chad,Republic of the Congo,Congo, Democratic Republic of the (Kinshasa),Croatia,Czech Republic,Denmark,France,Gabon,Germany,Gibraltar,Hungary,Italy,Libya,Liechtenstein,Luxembourg,Republic of Macedonia,Malta,Monaco,Netherlands,Niger, Nigeria,Norway,Poland,San Marino,Serbia and Montenegro,Slovakia,Spain (Madrid),Sweden,Switzerland,Tunisia,Vatican City,Burkina Faso,Faroe Islands,The Gambia,Ghana,Guinea,Guinea-Bissau,Iceland,Ireland,Liberia,Mali,Mauritania,Morocco,Portugal (Lisbon),Saint Helena,Senegal,Sierra Leone, Spain (Canary Islands),Togo,United Kingdom,Western Sahara",Key:"(GMT+01:00) Brussels, Copenhagen, Madrid, Paris",Value:'Europe/Brussels',Offset:-60});
    tzArray.push({Country:"Albania,Andorra,Algeria,Angola,Austria,Belgium,Benin,Bosnia and Herzegovina,Cameroon,Central African Republic,Chad,Republic of the Congo,Congo, Democratic Republic of the (Kinshasa),Croatia,Czech Republic,Denmark,France,Gabon,Germany,Gibraltar,Hungary,Italy,Libya,Liechtenstein,Luxembourg,Republic of Macedonia,Malta,Monaco,Netherlands,Niger, Nigeria,Norway,Poland,San Marino,Serbia and Montenegro,Slovakia,Spain (Madrid),Sweden,Switzerland,Tunisia,Vatican City,Burkina Faso,Faroe Islands,The Gambia,Ghana,Guinea,Guinea-Bissau,Iceland,Ireland,Liberia,Mali,Mauritania,Morocco,Portugal (Lisbon),Saint Helena,Senegal,Sierra Leone, Spain (Canary Islands),Togo,United Kingdom,Western Sahara",Key:"(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb",Value:'Europe/Sarajevo',Offset:-60});
    tzArray.push({Country:"Botswana,Bulgaria,Burundi,Democratic Republic of the Congo (Lubumbashi),Cyprus,Egypt,Estonia,Finland,Greece,Israel,Jordan,Latvia,Lebanon,Lesotho,Lithuania,Malawi, Moldova,Mozambique,Namibia,Palestine,Romania,Russia (Kaliningrad),Rwanda,South Africa,Sudan,Swaziland,Syria,Ukraine,Zambia,Zimbabwe",Key:"(GMT+01:00) West Central Africa",Value:'Africa/Lagos',Offset:-120});
    tzArray.push({Country:"Botswana,Bulgaria,Burundi,Democratic Republic of the Congo (Lubumbashi),Cyprus,Egypt,Estonia,Finland,Greece,Israel,Jordan,Latvia,Lebanon,Lesotho,Lithuania,Malawi, Moldova,Mozambique,Namibia,Palestine,Romania,Russia (Kaliningrad),Rwanda,South Africa,Sudan,Swaziland,Syria,Ukraine,Zambia,Zimbabwe",Key:"(GMT+02:00) Amman",Value:'Asia/Amman',Offset:-120});
    tzArray.push({Country:"Botswana,Bulgaria,Burundi,Democratic Republic of the Congo (Lubumbashi),Cyprus,Egypt,Estonia,Finland,Greece,Israel,Jordan,Latvia,Lebanon,Lesotho,Lithuania,Malawi, Moldova,Mozambique,Namibia,Palestine,Romania,Russia (Kaliningrad),Rwanda,South Africa,Sudan,Swaziland,Syria,Ukraine,Zambia,Zimbabwe",Key:"(GMT+02:00) Athens, Bucharest, Istanbul",Value:'Europe/Athens',Offset:-120});
    tzArray.push({Country:"Botswana,Bulgaria,Burundi,Democratic Republic of the Congo (Lubumbashi),Cyprus,Egypt,Estonia,Finland,Greece,Israel,Jordan,Latvia,Lebanon,Lesotho,Lithuania,Malawi, Moldova,Mozambique,Namibia,Palestine,Romania,Russia (Kaliningrad),Rwanda,South Africa,Sudan,Swaziland,Syria,Ukraine,Zambia,Zimbabwe",Key:"(GMT+02:00) Beirut",Value:'Asia/Beirut',Offset:-120});
    tzArray.push({Country:"Botswana,Bulgaria,Burundi,Democratic Republic of the Congo (Lubumbashi),Cyprus,Egypt,Estonia,Finland,Greece,Israel,Jordan,Latvia,Lebanon,Lesotho,Lithuania,Malawi, Moldova,Mozambique,Namibia,Palestine,Romania,Russia (Kaliningrad),Rwanda,South Africa,Sudan,Swaziland,Syria,Ukraine,Zambia,Zimbabwe",Key:"(GMT+02:00) Cairo",Value:'Africa/Cairo',Offset:-120});
    tzArray.push({Country:"Botswana,Bulgaria,Burundi,Democratic Republic of the Congo (Lubumbashi),Cyprus,Egypt,Estonia,Finland,Greece,Israel,Jordan,Latvia,Lebanon,Lesotho,Lithuania,Malawi, Moldova,Mozambique,Namibia,Palestine,Romania,Russia (Kaliningrad),Rwanda,South Africa,Sudan,Swaziland,Syria,Ukraine,Zambia,Zimbabwe",Key:"(GMT+02:00) Harare, Pretoria",Value:'Africa/Harare',Offset:-120});
    tzArray.push({Country:"Botswana,Bulgaria,Burundi,Democratic Republic of the Congo (Lubumbashi),Cyprus,Egypt,Estonia,Finland,Greece,Israel,Jordan,Latvia,Lebanon,Lesotho,Lithuania,Malawi, Moldova,Mozambique,Namibia,Palestine,Romania,Russia (Kaliningrad),Rwanda,South Africa,Sudan,Swaziland,Syria,Ukraine,Zambia,Zimbabwe",Key:"(GMT+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius",Value:'Europe/Helsinki',Offset:-120});
    tzArray.push({Country:"Botswana,Bulgaria,Burundi,Democratic Republic of the Congo (Lubumbashi),Cyprus,Egypt,Estonia,Finland,Greece,Israel,Jordan,Latvia,Lebanon,Lesotho,Lithuania,Malawi, Moldova,Mozambique,Namibia,Palestine,Romania,Russia (Kaliningrad),Rwanda,South Africa,Sudan,Swaziland,Syria,Ukraine,Zambia,Zimbabwe",Key:"(GMT+02:00) Jerusalem",Value:'Asia/Jerusalem',Offset:-120});
    tzArray.push({Country:"Botswana,Bulgaria,Burundi,Democratic Republic of the Congo (Lubumbashi),Cyprus,Egypt,Estonia,Finland,Greece,Israel,Jordan,Latvia,Lebanon,Lesotho,Lithuania,Malawi, Moldova,Mozambique,Namibia,Palestine,Romania,Russia (Kaliningrad),Rwanda,South Africa,Sudan,Swaziland,Syria,Ukraine,Zambia,Zimbabwe",Key:"(GMT+02:00) Minsk",Value:'Europe/Minsk',Offset:-120});
    tzArray.push({Country:"Botswana,Bulgaria,Burundi,Democratic Republic of the Congo (Lubumbashi),Cyprus,Egypt,Estonia,Finland,Greece,Israel,Jordan,Latvia,Lebanon,Lesotho,Lithuania,Malawi, Moldova,Mozambique,Namibia,Palestine,Romania,Russia (Kaliningrad),Rwanda,South Africa,Sudan,Swaziland,Syria,Ukraine,Zambia,Zimbabwe",Key:"(GMT+02:00) Windhoek",Value:'Africa/Windhoek',Offset:-120});
    tzArray.push({Country:"Yemen,Uganda,Tanzania,South Sudan,Somalia,Saudi Arabia,Bahrain,Belarus,Comoros,Djibouti,Eritrea,Ethiopia,Iraq,Kenya,Kuwait,Madagascar,Qatar,Russia (Moscow)",Key:"(GMT+03:00) Kuwait, Riyadh, Baghdad",Value:'Asia/Kuwait',Offset:-180});
    tzArray.push({Country:"Yemen,Uganda,Tanzania,South Sudan,Somalia,Saudi Arabia,Bahrain,Belarus,Comoros,Djibouti,Eritrea,Ethiopia,Iraq,Kenya,Kuwait,Madagascar,Qatar,Russia (Moscow)",Key:"(GMT+03:00) Moscow, St. Petersburg, Volgograd",Value:'Europe/Moscow',Offset:-180});
    tzArray.push({Country:"Yemen,Uganda,Tanzania,South Sudan,Somalia,Saudi Arabia,Bahrain,Belarus,Comoros,Djibouti,Eritrea,Ethiopia,Iraq,Kenya,Kuwait,Madagascar,Qatar,Russia (Moscow)",Key:"(GMT+03:00) Nairobi",Value:'Africa/Nairobi',Offset:-180});
    tzArray.push({Country:"Yemen,Uganda,Tanzania,South Sudan,Somalia,Saudi Arabia,Bahrain,Belarus,Comoros,Djibouti,Eritrea,Ethiopia,Iraq,Kenya,Kuwait,Madagascar,Qatar,Russia (Moscow)",Key:"(GMT+03:00) Tbilisi",Value:'Asia/Tbilisi',Offset:-180});
    tzArray.push({Country:"Iran",Key:"(GMT+03:30) Tehran",Value:'Asia/Tehran',Offset:-210});
    tzArray.push({Country:"Reunion,Russia (Samara),Seychelles,United Arab Emirates,Oman,Mauritius,Armenia,Azerbaijan,Georgia",Key:"(GMT+04:00) Abu Dhabi, Muscat",Value:'Asia/Muscat',Offset:-240});
    tzArray.push({Country:"Reunion,Russia (Samara),Seychelles,United Arab Emirates,Oman,Mauritius,Armenia,Azerbaijan,Georgia",Key:"(GMT+04:00) Baku",Value:'Asia/Baku',Offset:-240});
    tzArray.push({Country:"Reunion,Russia (Samara),Seychelles,United Arab Emirates,Oman,Mauritius,Armenia,Azerbaijan,Georgia",Key:"(GMT+04:00) Yerevan",Value:'Asia/Yerevan',Offset:-240});
    tzArray.push({Country:"Afghanistan",Key:"(GMT+04:30) Kabul",Value:'Asia/Kabul',Offset:-270});
    tzArray.push({Country:"Kazakhstan (Aktobe),Maldives,Pakistan,Russia (Yekaterinburg),Tajikistan,Turkmenistan,Uzbekistan",Key:"(GMT+05:00) Yekaterinburg",Value:'Asia/Yekaterinburg',Offset:-300});
    tzArray.push({Country:"Kazakhstan (Aktobe),Maldives,Pakistan,Russia (Yekaterinburg),Tajikistan,Turkmenistan,Uzbekistan",Key:"(GMT+05:00) Islamabad, Karachi, Tashkent",Value:'Asia/Karachi',Offset:-300});
    tzArray.push({Country:"India,Sri Lanka",Key:"(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi",Value:'Asia/Calcutta',Offset:-330});
    tzArray.push({Country:"India,Sri Lanka",Key:"(GMT+05:30) Sri Jayawardenapura",Value:'Asia/Calcutta',Offset:-330});
    tzArray.push({Country:"Nepal",Key:"(GMT+05:45) Kathmandu",Value:'Asia/Katmandu',Offset:-345});

    if(tz){
        for(var i=0;i<tzArray.length;i++){
            var tItem = tzArray[i];
            if(tItem && tItem.Value == tz){
                return tItem.Country;
            }
        }
        return "";
    }else{
        return getCountryName();
    }
}

function displaySessionErrorPopUp(e, a, s) {
    $.when(kendo.ui.ExtAlertDialog.show({
        title: e,
        message: a,
        icon: "k-ext-error"
    })).done(function(e) {
        s(e)
    })
}

function displayConfirmSessionErrorPopUp(e, a, s) {
    $.when(kendo.ui.ExtOkCancelDialog.show({
        title: e,
        message: a,
        icon: "k-ext-error"
    })).done(function(e) {
        if (e.button == "OK") {
            s(e)
        }
    })
}

var thisDataUrl = "",
    thisMethod = "",
    thisSuccessFunction = null,
    thisErrorFunction = null;

function getAjaxObject(e, a, s, n) {
    var t = Number(sessionStorage.expires_in);
    t -= 5e3, timer || (timer = setInterval(function() {
        var e = ipAddress + "/login",
            a = {
                grant_type: "refresh_token"
            };
        a.refresh_token = sessionStorage.refresh_token, getAccessCode(e, a, "POST")
    }, t));
    var o = new Date,
        i = sessionStorage.pt;
    if (c = (c = (o.getTime() - (i = Number(i))) / 6e4).toFixed(0), sessionExpTime > c) {
        if (sessionStorage.access_token) {
            thisDataUrl = e, thisMethod = a, thisSuccessFunction = s, thisErrorFunction = n;
            var r = (o = new Date).getTime(),
                d = sessionStorage.tm,
                c = (t = sessionStorage.expires_in, r - d);
            if (sessionStorage.pt = o.getTime(), t >= c) {
                e = e.indexOf("?") >= 0 ? e + "&access_token=" + sessionStorage.access_token : e + "?access_token=" + sessionStorage.access_token;
                sessionStorage.tenant, Loader.showLoader(), $.ajax({
                    type: a,
                    url: e,
                    data: null,
                    context: this,
                    cache: !1,
                    success: function(e, a, n) {
                        Loader.hideLoader(), s(e)
                    },
                    error: function(e, a, s) {
                        Loader.hideLoader(), n(e)
                    },
                    headers: {
                        tenant: sessionStorage.tenant
                    },
                    contentType: "application/json"
                })
            } else {
                sessionStorage.access_token = "";
                var u = ipAddress + "/login",
                    l = {
                        grant_type: "refresh_token"
                    };
                l.refresh_token = sessionStorage.refresh_token, getAccessCode(u, l, "POST"), console.log("diff")
            }
        }
    } else displaySessionErrorPopUp("Session Expire", "Session Expired", function(e) {
        sessionStorage.uName = "", top.location.href = "../../html/login/login.html"
    })
}

function getAjaxObjectAsync(e, a, s, n) {
    var t = Number(sessionStorage.expires_in);
    t -= 5e3, console.log("Expire:" + t), timer || (timer = setInterval(function() {
        var e = ipAddress + "/login",
            a = {
                grant_type: "refresh_token"
            };
        a.refresh_token = sessionStorage.refresh_token, getAccessCode(e, a, "POST")
    }, t));
    var o = new Date,
        i = sessionStorage.pt;
    if (c = (c = (o.getTime() - (i = Number(i))) / 6e4).toFixed(0), console.log("diff" + c), sessionExpTime > c) {
        if (sessionStorage.access_token) {
            thisDataUrl = e, thisMethod = a, thisSuccessFunction = s, thisErrorFunction = n;
            var r = (o = new Date).getTime(),
                d = sessionStorage.tm,
                c = (t = sessionStorage.expires_in, r - d);
            if (sessionStorage.pt = o.getTime(), t >= c) e = e.indexOf("?") >= 0 ? e + "&access_token=" + sessionStorage.access_token : e + "?access_token=" + sessionStorage.access_token, Loader.showLoader(), $.ajax({
                type: a,
                url: e,
                data: null,
                context: this,
                cache: !1,
                success: function(e, a, n) {
                    Loader.hideLoader(), s(e)
                },
                error: function(e, a, s) {
                    Loader.hideLoader(), n(e)
                },
                headers: {
                    tenant: sessionStorage.tenant
                },
                contentType: "application/json",
                async: !1
            });
            else {
                sessionStorage.access_token = "";
                var u = ipAddress + "/login",
                    l = {
                        grant_type: "refresh_token"
                    };
                l.refresh_token = sessionStorage.refresh_token, getAccessCode(u, l, "POST"), console.log("diff")
            }
        }
    } else displaySessionErrorPopUp("Session Expire", "Session Expired", function(e) {
        sessionStorage.uName = "", top.location.href = "../../html/login/login.html"
    })
}

function getAccessCode(e, a, s) {
    $.ajax({
        type: s,
        url: e,
        data: JSON.stringify(a),
        context: this,
        cache: !1,
        success: function(e, a, s) {
            if (Loader.hideLoader(), e.response.token.access_token && (sessionStorage.access_token = e.response.token.access_token), e.response.token.expires_in) {
                var n = Number(e.response.token.expires_in);
                n -= 20, n *= 1e3, sessionStorage.expires_in = n
            }
            e.response.token.refresh_token && (sessionStorage.refresh_token = e.response.token.refresh_token);
            var t = new Date;
            sessionStorage.tm = t.getTime()
        },
        error: function(e, a, s) {
            Loader.hideLoader()
        },
        contentType: "application/json"
    })
}
var thisDataObj = null,
    timer = null;

function createAjaxObject(e, a, s, n, t) {
    if (sessionStorage.expires_in) {
        var o = Number(sessionStorage.expires_in);
        o -= 5e3, timer && clearInterval(timer), timer = setInterval(function() {
            var e = {
                grant_type: "refresh_token"
            };
            e.refresh_token = sessionStorage.refresh_token
        }, o)
    }
    if (sessionStorage.access_token) {
        thisDataUrl = e, thisMethod = s, thisDataObj = a, thisSuccessFunction = n, thisErrorFunction = t;
        var i = (new Date).getTime(),
            r = sessionStorage.tm;
        if ((o = sessionStorage.expires_in) >= i - r) e = e.indexOf("?") >= 0 ? e + "&access_token=" + sessionStorage.access_token : e + "?access_token=" + sessionStorage.access_token, Loader.showLoader(), $.ajax({
            type: s,
            url: e,
            data: JSON.stringify(a),
            context: this,
            cache: !1,
            success: function(e, a, s) {
                Loader.hideLoader(), n(e)
            },
            error: function(e, a, s) {
                Loader.hideLoader(), t(e)
            },
            headers: {
                tenant: sessionStorage.tenant
            },
            contentType: "application/json"
        });
        else {
            console.log("diff"), sessionStorage.access_token = "";
            var d = ipAddress + "/login",
                c = {
                    grant_type: "refresh_token"
                };
            c.refresh_token = sessionStorage.refresh_token, getPostAccessCode(d, c, "POST")
        }
    }
}

function createAjaxObjectAsync(e, a, s, n, t) {
    if (sessionStorage.expires_in) {
        var o = Number(sessionStorage.expires_in);
        o -= 5e3, timer && clearInterval(timer), timer = setInterval(function() {
            var e = {
                grant_type: "refresh_token"
            };
            e.refresh_token = sessionStorage.refresh_token
        }, o)
    }
    if (sessionStorage.access_token) {
        thisDataUrl = e, thisMethod = s, thisDataObj = a, thisSuccessFunction = n, thisErrorFunction = t;
        var i = (new Date).getTime(),
            r = sessionStorage.tm;
        if ((o = sessionStorage.expires_in) >= i - r) e = e.indexOf("?") >= 0 ? e + "&access_token=" + sessionStorage.access_token : e + "?access_token=" + sessionStorage.access_token, Loader.showLoader(), $.ajax({
            type: s,
            url: e,
            data: JSON.stringify(a),
            context: this,
            cache: !1,
            success: function(e, a, s) {
                Loader.hideLoader(), n(e)
            },
            error: function(e, a, s) {
                Loader.hideLoader(), t(e)
            },
            headers: {
                tenant: sessionStorage.tenant
            },
            contentType: "application/json",
            async: !1
        });
        else {
            console.log("diff"), sessionStorage.access_token = "";
            var d = ipAddress + "/login",
                c = {
                    grant_type: "refresh_token"
                };
            c.refresh_token = sessionStorage.refresh_token, getPostAccessCode(d, c, "POST")
        }
    }
}

function getPostAccessCode(e, a, s) {
    $.ajax({
        type: s,
        url: e,
        data: JSON.stringify(a),
        context: this,
        cache: !1,
        success: function(e, a, s) {
            Loader.hideLoader(), sessionStorage.access_token = e.response.token.access_token;
            var n = Number(e.response.token.expires_in);
            n -= 10, n *= 1e3, sessionStorage.expires_in = n, sessionStorage.refresh_token = e.response.token.refresh_token;
            var t = new Date;
            sessionStorage.tm = t.getTime()
        },
        error: function(e, a, s) {
            Loader.hideLoader()
        },
        headers: {
            tenant: sessionStorage.tenant
        },
        contentType: "application/json"
    })
}

function loadAPi(file) {
    var fileref=document.createElement("link")
    fileref.setAttribute("rel", "stylesheet")
    // fileref.setAttribute("type", "text/css")
    fileref.setAttribute("href", file)
    document.getElementsByTagName("head")[0].appendChild(fileref);
}

function allowNumericsWithDash(ctrlId){
    $("#"+ctrlId).keypress(function (e) {
        var txtPatientId = $("#txtPatientId").data("kendoComboBox");
        if(txtPatientId && txtPatientId.value() == 2){
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57 || e.which == 189)) {
                return false;
            }
        }

    });
}


function onGetDate(DateOfBirth) {
    var strDT = "";
    if (DateOfBirth != "") {
        if (DateOfBirth != "LongTerm") {

            // var dd = DateOfBirth.getDate();
            // var mm = DateOfBirth.getMonth() + 1; //January is 0!
            //
            // var yyyy = DateOfBirth.getFullYear();
            // if (dd < 10) {
            //     dd = '0' + dd;
            // }
            // if (mm < 10) {
            //     mm = '0' + mm;
            // }
            // var today = mm + '/' + dd + '/' + yyyy;

            var dt = new Date(DateOfBirth);
            // var strDT =  kendo.toString(new Date(obj.dateOfBirth),"dd/MM/yyyy");
            if (dt) {
                var cntry = sessionStorage.countryName;
                if (cntry.indexOf("India") >= 0) {
                    strDT = kendo.toString(dt, "dd/MM/yyyy");
                } else if (cntry.indexOf("United Kingdom") >= 0) {
                    strDT = kendo.toString(dt, "dd/MM/yyyy");
                } else {
                    strDT = kendo.toString(dt, "MM/dd/yyyy");
                }
            }
            return strDT;
        }
        else {
            return "LongTerm";
        }
    }
    else {
        return strDT;
    }
}


function dformat(time, format) {
    var t = new Date(time);
    var tf = function (i) { return (i < 10 ? '0' : '') + i };
    return format.replace(/yyyy|MM|dd|HH|mm|ss/g, function (a) {
        switch (a) {
            case 'yyyy':
                return tf(t.getFullYear());
                break;
            case 'MM':
                return tf(t.getMonth() + 1);
                break;
            case 'mm':
                return tf(t.getMinutes());
                break;
            case 'dd':
                return tf(t.getDate());
                break;
            case 'HH':
                return tf(t.getHours());
                break;
            case 'ss':
                return tf(t.getSeconds());
                break;
        }
    })
}


function themeAPIChange(){
    getAjaxObject(ipAddress+"/homecare/settings/?id=1","GET",getThemeValue,onError);
}


function getThemeValue(dataObj){
    var tempCompType;
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.settings){
            if($.isArray(dataObj.response.settings)){
                tempCompType = dataObj.response.settings;
            }else{
                tempCompType.push(dataObj.response.settings);
            }
        }
        if(tempCompType.length > 0){
            sessionStorage.setItem("IsActivity", tempCompType[0].value); // for "1- Patient Activity, 2-Appointment Reason Activity"
            if(sessionStorage.IsActivity == 1){
                $("#addAppointmentTasks").css("display","none");
            }else if(tempCompType[0].value == 2){
                $("#addAppointmentTasks").css("display","");
            }
        }
    }
}

function onError(errorObj){
    console.log(errorObj);
}

function GetDateEdit(objDate){
    var dt = new Date(objDate);
    var strDT = "";
    if (dt) {
        var cntry = sessionStorage.countryName;
        if (cntry.indexOf("India") >= 0) {
            strDT = kendo.toString(dt, "dd/MM/yyyy");
        } else if (cntry.indexOf("United Kingdom") >= 0) {
            strDT = kendo.toString(dt, "dd/MM/yyyy");
        } else {
            strDT = kendo.toString(dt, "MM/dd/yyyy");
        }
    }
    return strDT;
}

function GetDateTimeEdit(objDate){
    var dt = new Date(objDate);
    var strDT = "";
    if (dt) {
        var cntry = sessionStorage.countryName;
        if (cntry.indexOf("India") >= 0) {
            strDT = kendo.toString(dt, "dd/MM/yyyy h:mm tt");
        } else if (cntry.indexOf("United Kingdom") >= 0) {
            strDT = kendo.toString(dt, "dd/MM/yyyy h:mm tt");
        } else {
            strDT = kendo.toString(dt, "MM/dd/yyyy h:mm tt");
        }
    }
    return strDT;
}

function GetDateTimeEditDate(objDate){
    var dt = new Date(objDate);
    var strDT = "";
    if (dt) {
        var cntry = sessionStorage.countryName;
        strDT = kendo.toString(dt, "MM/dd/yyyy");
    }
    var DT = new Date(strDT);
    var returnValue = new Date(DT).getTime();

    return returnValue;
}


function GetDateTimeEditTime(objDate){
    var dt = new Date(objDate);
    var strDT = "";
    if (dt) {
        strDT = kendo.toString(dt, "MM/dd/yyyy h:mm tt");
    }
    // var returnValue = new Date(strDT).getTime();

    var DT = new Date(strDT);
    var returnValue = new Date(DT, "h:mm tt").getTime();

    return returnValue;
}


function GetDateTimeEditLeave(objDate){
    var dt = new Date(objDate);
    var strDT = "";
    var stT = "";
    var strH = "";
    var strm = "";
    if (dt) {
        var cntry = sessionStorage.countryName
        if (cntry.indexOf("India") >= 0) {
            // strDT = kendo.toString(dt, "dd/MM/yyyy hh:mm")
            strDT = kendo.toString(dt, "yyyy-MM-dd")
            strT = kendo.toString(dt, "hh:mm tt")
            strH = kendo.toString(dt, "hh")
            strm = kendo.toString(dt, "mm")
            stT = kendo.toString(dt, "tt")
        }
        else if (cntry.indexOf("United Kingdom") >= 0){
            strDT = kendo.toString(dt, "yyyy-MM-dd")
            strT = kendo.toString(dt, "hh:mm tt")
            strH = kendo.toString(dt, "hh")
            strm = kendo.toString(dt, "mm")
            stT = kendo.toString(dt, "tt")
        }
        else {
            strDT = kendo.toString(dt, "yyyy-MM-dd");
            // strDT = kendo.toString(dt, "yyyy-MM-dd")
            strT = kendo.toString(dt, "hh:mm tt")
            strH = kendo.toString(dt, "hh")
            strm = kendo.toString(dt, "mm")
            stT = kendo.toString(dt, "tt")
        }
        if(strH == "12"){
            strH = "00";
        }
        if(stT == "PM"){
            strH = Number(strH)+12;
        }
        strDT = strDT+"T"+strH+":"+strm;

    }    return strDT

}

function GetDateTime(Id){
    var strDate = "";
    var dt = document.getElementById(''+ Id +'').value;
    var dob = null;
    var mm = 0;
    var dd = 0;
    var yy = 0;
    var cntry = sessionStorage.countryName;
    if (cntry.indexOf("India") >= 0) {
        var dtArray = dt.split("/");
        dob = (dtArray[1] + "/" + dtArray[0] + "/" + dtArray[2]);
    } else if (cntry.indexOf("United Kingdom") >= 0) {
        var dtArray = dt.split("/");
        dob = (dtArray[1] + "/" + dtArray[0] + "/" + dtArray[2]);
    } else {
        var dtArray = dt.split("/");
        dob = (dtArray[0] + "/" + dtArray[1] + "/" + dtArray[2]);
    }
    if (dob) {
        var DOB = new Date(dob);
        strDate = kendo.toString(DOB, "dd/mm/yyyy");
    }
    var returnValue = new Date(dob).getTime();

    return returnValue;
}

function GetDateTimeLeave(Id){
    var strDate = "";
    var dt = document.getElementById(''+ Id +'').value;
    var dob = null;
    var mm = 0;
    var dd = 0;
    var yy = 0;
    dt = dt.replace("T"," ");
    var cntry = sessionStorage.countryName;
    if (cntry.indexOf("India") >= 0) {
        var dtArray = dt.split("-");
        dob = (dtArray[0] + "/" + dtArray[1] + "/" + dtArray[2]);
    } else if (cntry.indexOf("United Kingdom") >= 0) {
        var dtArray = dt.split("-");
        dob = (dtArray[0] + "/" + dtArray[1] + "/" + dtArray[2]);
    } else {
        var dtArray = dt.split("-");
        dob = (dtArray[1] + "/" + dtArray[0] + "/" + dtArray[2]);
    }
    // if (dob) {
    //     var DOB = new Date(dob);
    //     strDate = kendo.toString(DOB, "dd/mm/yyyy");
    // }
    var returnValue = new Date(dob).getTime();

    return returnValue;
}

function getWeekDayName(wk){
    var wn = "";
    if(wk == 1){
        return "Mon";
    }else if(wk == 2){
        return "Tue";
    }else if(wk == 3){
        return "Wed";
    }else if(wk == 4){
        return "Thu";
    }else if(wk == 5){
        return "Fri";
    }else if(wk == 6){
        return "Sat";
    }else if(wk == 0){
        return "Sun";
    }
    return wn;
}

function getCounMonthName(wk){
    var wn = "";
    if(wk == 0){
        return "Jan";
    }else if(wk == 1){
        return "Feb";
    }else if(wk == 3){
        return "Wed";
    }else if(wk == 4){
        return "Thu";
    }else if(wk == 5){
        return "Fri";
    }else if(wk == 6){
        return "Sat";
    }
    return wn;
}

function GetDateTimeEditDay(objDate){
    var dt = new Date(objDate);
    var strDT = "";
    if (dt) {
        var cntry = sessionStorage.countryName;
        // if (cntry.indexOf("India") >= 0) {
        //     strDT = kendo.toString(dt, "dd/MM/yyyy h:mm tt");
        // } else if (cntry.indexOf("United Kingdom") >= 0) {
        //     strDT = kendo.toString(dt, "dd/MM/yyyy h:mm tt");
        // } else {
        strDT = kendo.toString(dt, "MM/dd/yyyy h:mm tt");

    }
    return strDT;
}


function getFromDate(fromDate) {
    var day = fromDate.getDate();
    var month = fromDate.getMonth();
    month = month + 1;
    var year = fromDate.getFullYear();

    var strDate = month + "/" + day + "/" + year;
    strDate = strDate + " 00:00:00";

    var date = new Date(strDate);
    var strDateTime = date.getTime();

    return strDateTime;
}

function getToDate(toDate) {
    var day = toDate.getDate();
    var month = toDate.getMonth();
    month = month + 1;
    var year = toDate.getFullYear();

    var strDate = month + "/" + day + "/" + year;
    strDate = strDate + " 23:59:59";

    var date = new Date(strDate);
    var strDateTime = date.getTime();

    return strDateTime;
}

function convertMinsToHrsMins(minutes) {
    if(minutes != 0) {
        var h = Math.floor(minutes / 60);
        var m = minutes % 60;
        h = h < 10 ? '0' + h : h;
        m = m < 10 ? '0' + m : m;
        return h + ':' + m;
    }
    else{
        return '';
    }
}

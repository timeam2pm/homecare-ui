// update lists for dropdown fields
function setDataForComboBox(data, container, changeMethod, namesArr, selIndex, listSize) {
	var selList = [];
    if ($.isArray(data) == false) {
        var obj = {};
        obj[namesArr[0]] = data[namesArr[0]];
        obj[namesArr[1]] = data[namesArr[1]];
        selList.push(obj);

    } else {
        selList = data;
    }

    $(container).kendoComboBox({
        dataTextField: namesArr[0],
        dataValueField: namesArr[1],
        filter: "contains",
        suggest: true,
        dataSource: selList,
        index: selIndex,
        change: changeMethod,
        open: function(e) {
            if (listSize) {
                setTimeout(function() {
                    var dropdownlist = $(container).data("kendoComboBox");
                    dropdownlist.list.height(listSize * 27 + 3);
                }, 10);
            }
        },
        dataBound: function(e) {
            setTimeout(function() {
                var comboBox = $(container).data("kendoComboBox");
                if (comboBox.list.width() >= $(container).width())
                    //comboBox.list.width(comboBox.list.width());
                //comboBox.list.css("min-width", $(container).width());

                $(container).parent().find("span.k-dropdown-wrap").attr("title", $(container).data("kendoComboBox").text());
                if (listSize) {
                    comboBox.list.height(listSize * 27 + 3);
                }

            }, 20);
        },
        select: function(e) {
            setTimeout(function() {
                $(container).parent().find("span.k-dropdown-wrap").attr("title", $(container).data("kendoComboBox").text());
            }, 200);
        }

    });
}

// get selectedIndex of dropdown field by its value
function getSelectedIndexByText(field, value, sel, type) {
    var selIndex = 0;
    var dataObject = $(field).data("kendoComboBox").dataSource;
    if (dataObject.options.data != null && dataObject.options.data.length > 0) {
        for (var j = 0; j < dataObject.options.data.length; j++) {
            if (value == "") {
                if (dataObject.options.data[j] == sel) {
                    selIndex = j;
                    break;
                }
            } else {
                if (dataObject.options.data[j][value] == sel) {
                    selIndex = j;
                    break;
                }
            }
        }
    }
    return selIndex;
}

// get selectedIndex of dropdown field by its value
function getSelectedIndexByKey(field, value, sel) {
    var selIndex = 0;
    var dataObject = $(field).data("kendoComboBox").dataSource
    if (dataObject.options.data != null && dataObject.options.data.length > 0) {
        for (var j = 0; j < dataObject.options.data.length; j++) {
            if (dataObject.options.data[j][value] == sel) {
                selIndex = j;
                break;
            }
        }
    }
    return selIndex;
}

function displayInfoPopUp(alerttitle, alertmessage) {
    $.when(kendo.ui.ExtAlertDialog.show({
        title: alerttitle,
        message: alertmessage,
        icon: "k-ext-information"
    }))
}

function displayErrorPopUp(alerttitle, alertmessage) {
    $.when(kendo.ui.ExtAlertDialog.show({
        title: alerttitle,
        message: alertmessage,
        icon: "k-ext-error"
    }))
}

function setDataForSelection(data, container, changeMethod, namesArr, selIndex, optionLabelValue) {
    var selList = [];
    if ($.isArray(data) == false) {
        var obj = {};
        obj[namesArr[0]] = data[namesArr[0]];
        obj[namesArr[1]] = data[namesArr[1]];
        selList.push(obj);

    } else {
        selList = data;
    }

    $("#" + container).kendoComboBox({
        dataTextField: namesArr[0],
        dataValueField: namesArr[1],
        dataSource: selList,
        optionLabel: " ",
        change: function(e) {
            $("#" + container).parent().find("span.k-dropdown-wrap").attr("title", $("#" + container).data("kendoComboBox").text());
            try {
                changeMethod(e);
            } catch (e1) {

            }
        },
        dataBound: function(e) {
            setTimeout(function() {
                var dropDownList = $("#" + container).data("kendoComboBox");
                if(dropDownList){
                	 if (dropDownList.list.width() >= $("#" + container).width()) {
                         //dropDownList.list.width(dropDownList.list.width()-100);
                     }
                	 //dropDownList.list.width(dropDownList.list.width()-100);
                     //dropDownList.list.css("min-width", $("#" + container).width()-100);
                }
               
            }, 200);
        },
        template: function(e) {
            var tip = e[namesArr[0]];
            return '<div class="overFlow" title="' + tip + '">' + tip + '</div>';
        }
    });
    var containerTypeDropdownlist = $("#" + container).data("kendoComboBox");
    if(containerTypeDropdownlist){
    	  containerTypeDropdownlist.select(selIndex);
    	  $("#" + container).parent().find("span.k-dropdown-wrap").attr("title", $("#" + container).data("kendoComboBox").text());
    }
  
   
}
function convertToDateMMDDYYYY(date) {
    if (date == "") return "";
    var d = new Date(date);
    var str = "" + d.getFullYear() + "";
    str += "-";
    str += "" + (d.getMonth() + 1) > 9 ? (d.getMonth() + 1) : "0" + (d.getMonth() + 1);
    str += "-";
    str += "" + d.getDate() > 9 ? d.getDate() : "0" + d.getDate();

    return str;

}
var kendoWindowContainerId = "kendoWindowContainer";
var kendoWindowWrapper = function () {

};

kendoWindowWrapper.prototype = {
    windowContainer: null,
    windowWidth: 0,
    windowHeight: 0,
    windowTitle: '',
    windowIsModal: true,
    content: '',
    windowCloseCalled:false,
    hasDestroy:true
};

function checkQuestionMark(cUrl){
	if(cUrl.indexOf("?") != -1) {
		return true;
	}
	else {
		return false;
	}
}

//Kendo Window: Use openPageWindow() to open a window and display a new page. This uses IFrame.
kendoWindowWrapper.prototype.openPageWindow = function (url, title, width, height, isModalWindow, callBackFunction) {

	var clientHeight = window.top.innerHeight || document.body.clientHeight;
	var currSessionId = clientAPIManager.getSessionId();
	var isQuestionMarkAvail = checkQuestionMark(url);
	if(isQuestionMarkAvail == true){
		url = url + "&sessionId="+currSessionId;	
	}else{
		url = url + "?sessionId="+currSessionId;	
	}
    this.content = url;
    this.windowTitle = title;
    this.windowWidth = width;
    this.windowHeight = height;
    this.windowIsModal = isModalWindow;
    //var win = this.windowContainer.data("kendoWindow");
    //if (!win) {
    if (parent.$("#kendoWindowContainer").length === 0 ) {
        var rootEle = document.documentElement;
        var container = $("<div id='" + kendoWindowContainerId + "'></div>");
        container.appendTo(parent.$('body'));
        this.openWindow(container.attr("id"), title, width, height, isModalWindow, callBackFunction);
    }
    else if (parent.$("#kendoWindowContainer1").length === 0 ) {
	
        var container1 = $("<div id='" + kendoWindowContainerId + "1'></div>");
        container1.appendTo(parent.$('body'));
        this.openWindow(container1.attr("id"), title, width, height, isModalWindow, callBackFunction);
    }else if (parent.$("#kendoWindowContainer2").length === 0){
	
        var container2 = $("<div id='" + kendoWindowContainerId + "2'></div>");
        container2.appendTo(parent.$('body'));
        this.openWindow(container2.attr("id"), title, width, height, isModalWindow, callBackFunction);
    }else if (parent.$("#kendoWindowContainer3").length === 0){
	
        var container3 = $("<div id='" + kendoWindowContainerId + "3'></div>");
        container3.appendTo(parent.$('body'));
        this.openWindow(container3.attr("id"), title, width, height, isModalWindow, callBackFunction);
    }else if (parent.$("#kendoWindowContainer4").length === 0){
	
        var container4 = $("<div id='" + kendoWindowContainerId + "4'></div>");
        container4.appendTo(parent.$('body'));
        this.openWindow(container4.attr("id"), title, width, height, isModalWindow, callBackFunction);
    }else{
		var container5 = $("<div id='" + kendoWindowContainerId + "5'></div>");
        container5.appendTo(parent.$('body'));
        this.openWindow(container5.attr("id"), title, width, height, isModalWindow, callBackFunction);
	}
	
	
};

kendoWindowWrapper.prototype.getWindowContainer = function(){
    var container = null;
	if (parent.$('#'+kendoWindowContainerId + "2").length > 0) {
	container = parent.$('#'+kendoWindowContainerId+"2");
	} else if (parent.$('#'+kendoWindowContainerId + "1").length > 0) {
	container = parent.$('#'+kendoWindowContainerId+"1");
	} else {
	container = parent.$('#'+kendoWindowContainerId);
	}
	return container;
}

//Kendo window: Use closePageWindow() to close a page opened with openPageWindow()
kendoWindowWrapper.prototype.closePageWindow = function (returnData) {
    var container;
    if (parent.$('#'+kendoWindowContainerId + "5").length > 0) {
        container = parent.$('#'+kendoWindowContainerId+"5");
    }else if (parent.$('#'+kendoWindowContainerId + "4").length > 0) {
        container = parent.$('#'+kendoWindowContainerId+"4");
    }else if (parent.$('#'+kendoWindowContainerId + "3").length > 0) {
        container = parent.$('#'+kendoWindowContainerId+"3");
    }else if (parent.$('#'+kendoWindowContainerId + "2").length > 0) {
        container = parent.$('#'+kendoWindowContainerId+"2");
    }else if (parent.$('#'+kendoWindowContainerId + "1").length > 0) {
        container = parent.$('#'+kendoWindowContainerId+"1");
    } else {
        container = parent.$('#'+kendoWindowContainerId);
    }
    if (container === null) {
        return;
    }
    var win = container.data("kendoWindow");
    if (win !== undefined && win !== null) {
        container.trigger("onClosingWindow.kendoWrapper", returnData);
        //container.off("onClosingWindow.kendoWrapper");
		
		//For handling browser back, close, redirect, etc cleaning up session
		sessionStorage.validNavigation = true;
		
        win.close();
    }
};

var getEffects = function () {
    return ((true ? "zoom " : "") +
            (true ? "fadeIn" : "")) || false;
};

kendoWindowWrapper.prototype.openWindow = function (containerId, title, width, height, isModalWindow, callBackFunction, hasToDestroy) {
    this.windowTitle = title;
    this.windowWidth = width;
    this.windowHeight = height;
    this.windowIsModal = isModalWindow;
    this.hasDestroy = (hasToDestroy == false) ? false : true;
    var container;
    if (parent.$("#kendoWindowContainer").length === 0) {
        container  = $('#'+containerId);
    }
    else {
        container = parent.$('#'+containerId);
    }
	var animationEffect = false;
	var toppad="none";
	
	var win = container.data("kendoWindow");
	var ref = this;
	
    if (!win) {
        win = container.kendoWindow({
            actions: ["close"],
            title: this.windowTitle,
            content: this.content,
            height: this.windowHeight,
            width: this.windowWidth,
            visible: false,
			resizable: false,
            modal: this.windowIsModal,
            iframe: (this.content !== null) ? true : false,
			animation : { open: { effects: getEffects() }, close: { effects: getEffects(), reverse: true} },
			open: function (e) {
            this.wrapper.css({ top: toppad })},

            deactivate: function (e) {
            	if(ref.hasDestroy == undefined || ref.hasDestroy == true)
                this.destroy();
            },
            close: function (event) {
                container.trigger("onClosingWindow.kendoWrapper", { cancelled: true });
            }
        }).data("kendoWindow");
    }
    if (win === null || win === undefined) {
        alert("Error when opening window. Check the arguments.");
        return;
    }

    container.one("onClosingWindow.kendoWrapper", callBackFunction);
    win.center();
    win.open();
};

//Kendo window: Use closeWindow() to close the window opened with openWindow()
//containerId: The id of the div passed as containerId in openWindow()
kendoWindowWrapper.prototype.closeWindow = function (containerId, returnData) {
	var container;
    if (parent.$("#kendoWindowContainer1").length === 0) {
        container = $(containerId);
    }
    else {
        container = parent.$(containerId);
    }
    var win = container.data("kendoWindow");
    if (win !== undefined && win !== null) {
        win.close(returnData);
    }
};


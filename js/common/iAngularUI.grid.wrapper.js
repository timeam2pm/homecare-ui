var imagePath = "../../../../app/ui/styles/default/images/Default/";


var createRow, updateRow, deleteRow, detailsRow, multiSortWindow, searchWindow, exportWindow, formatWindow, columnDetailsWindow, columnsOrderWindow;
var dataSource, searchCounter;
var AngularUIGridWrapper = function(divId, options) {
    var appName, appController, appId, dataOptions;

    this.containerId = divId;
    dataOptions = {
        downloadItem: false,
        addItem: false,
        clearUserPreferences: true,
        orderColumn: true,
        exportCsv: true,
        print: true,
        multiSort: true,
        multiSelect: false,
        countRows: true,
        rendererSpace: "5px",
        extreBtn: false,
        extreBtnTitle: "Exter btn",
        extreBtnClass: "",
        toolBar: true,
        noUnselect:true,
        editBeforeCallBack: function() {},
        editAfterCallBack: function() {},
        changeCallBack: function() {},
        addCallBack: function() {},
        paginationChangeCallBack: function() {},
        sortCallBack: function() {},
        extraBtnCallBack: function() {}
    }
    dataOptions = $.extend({}, dataOptions, options);
    var appName = this.containerId + 'app';
    var appController = this.containerId + 'Ctrl';
    var str = "";
    $("#" + this.containerId).html("");
    str += ('<div  id="' + this.containerId + 'ID" ng-app="' + appName + '" ng-controller="' + appController + '">');
    /* str += ('<button id="refreshButton" type="button" class="btn btn-success" ng-click="refreshData()" style="display:none;">Refresh Data</button>  <strong>Calls Pending:</strong> <span ng-bind="callsPending" style="display:none;"></span>');
    str += ('<br>');
    str += ('<br>');
    str += ('<strong>{{ myData.length }} rows</strong> <strong>selelcted AssetID {{selectedAssetID }}</strong>');
    str += ('<br>');*/
    //str += ('<input ng-model="searchText" ng-change="refreshFilterData()" placeholder="Search...">');
    str += ('<div style="border-bottom:solid 1px #ccc;border-right:none;border-left:none;border-top:none;" ng-show="otherOptions.toolBar" class="k-toolbar k-grid-toolbar">');
    //str += ('<span ng-show="otherOptions.addItem" class="wrapperButton createNewRecord" id="addNewRecord_' + this.containerId + '"><img src="../../../../app/ui/styles/default/images/Default/add-icon.png" class="imgStylesClass" title="Adding new record" ng-click="openAddNew()"></span>');
    //str += ('<span ng-show="otherOptions.clearUserPreferences" class="wrapperButton clearUserPreferences" id="clearGridData_' + this.containerId + '"><img src="../../../../app/ui/styles/default/images/Default/grid-delete.png" class="imgStylesClass" title="Clear user preferences" ng-click="openClear()"></span>');
   // str += ('<span ng-show="otherOptions.orderColumn" class="wrapperButton orderedColumns" id="columnOrder_' + this.containerId + '"><img src="../../../../app/ui/styles/default/images/Default/order-column.png" class="imgStylesClass" title="Show/hide and order columns" ng-click="openShowHideColumns()"></span>');
    //str += ('<span ng-show="otherOptions.exportCsv" class="wrapperButton exportFile" id="exportFile_' + this.containerId + '"><img src="../../../../app/ui/styles/default/images/Default/csv6.png" class="imgStylesClass" title="Export to CSV" ng-click="openExportCSV()"></span>');
    //str += ('<span ng-show="otherOptions.downloadItem" class="wrapperButton createNewRecord" id="downloadFile_' + this.containerId + '"><img src="' + imagePath + 'downloadArrow.png" class="imgStylesClass" title="Download File"/></span>');
    //str += ('<span ng-show="otherOptions.print" class="wrapperButton printGrid" id="printGrid_' + this.containerId + '"><img src="../../../../app/ui/styles/default/images/Default/printer-icon.png" class="imgStylesClass" title="Print grid"  ng-click="openPrint()"></span>');
  //  str += ('<span ng-show="otherOptions.multiSort" class="wrapperButton multiSort" id="multiSort_' + this.containerId + '"><img src="../../../../app/ui/styles/default/images/Default/up-down.png" class="imgStylesClass" title="Multi-column sorting" ng-click="openMultiSort()"></span>');
    str += ('<span ng-show="otherOptions.countRows" class="countRows" id="countRows_' + this.containerId + '" title="No of rows">| {{nofrows() }} Rows |</span>');
    str += ('<span ng-show="otherOptions.extreBtn" class="wrapperButton createNewRecord" id="extreBtn_' + this.containerId + '" ng-click="extraBtn()"><span class="{{otherOptions.extraBtnClass}}" title="{{otherOptions.extreBtnTitle}}">{{otherOptions.extreBtnTitle}}</span></span>');
    var IsSearchPanel = sessionStorage.IsSearchPanel;
    if(IsSearchPanel == "1") {
        str += ('<span class="searchPanel" style="float:left;position:relative;top:0px;max-width:150px !important;">');

        str += ('<span class="k-textbox k-space-left"><input type="text" class="searchBoxClass default" id="filterDataSource_' + this.containerId + '" ng-model="searchText" ng-change="refreshFilterData()" placeholder=" Search here..">');

        str += ('<a id="columnSpecificFilter_' + this.containerId + '" class="k-icon k-i-search" title="Click on icon to choose column " ng-click="globalAndcolumnSearch()">&nbsp;</a></span>');
    }
        str += ('</span><div class="titleToolbar"></div></div>');

    //actual grid div
    if (dataOptions.pagination == true)
        str += ('<div id="' + this.containerId + 'grid" ui-grid="gridOptions" ui-grid-edit ui-grid-save-state ui-grid-selection ui-grid-pagination ui-grid-resize-columns ui-grid-move-columns ui-grid-exporter ui-grid-auto-resize class="grid" style="height: {{gridHeight}}px" right-click ng-dblclick ="doubleClick($event)"></div>');
    else
        str += ('<div id="' + this.containerId + 'grid" ui-grid="gridOptions" ui-grid-edit ui-grid-save-state ui-grid-selection ui-grid-resize-columns ui-grid-move-columns ui-grid-exporter ui-grid-auto-resize class="grid" style="height: {{gridHeight}}px" right-click ng-dblclick ="doubleClick($event)"></div>');
    //actual grid div

    //str += ('<div id="columnRowContextMenu' + this.containerId + '" style="display:none;" class="clsColDetailsContextMenu"><ul><li //class="Details">Details</li></ul></div>');

    str += ('<div id="popups' + this.containerId + '"></div>');
    str += ('</div>');

    $("#" + this.containerId).append(str);

    // app module declaration
    var app = angular.module(appName, ['ngTouch', 'ui.grid', 'ui.grid.edit', 'ui.grid.resizeColumns', 'ui.grid.saveState', 'ui.grid.selection', 'ui.grid.exporter', 'ui.grid.pagination', 'ui.grid.autoResize']);
    // app controller declaration
    app.controller(appController, ['$scope', '$filter', '$http', '$timeout', '$interval', 'uiGridConstants',
        function($scope, $filter, $http, $timeout, $interval, uiGridConstants) {
            //$scope.gridHeight = 300;
            $scope.detailsSearchList = [];
            $scope.selectedSearchColumns = [];
            $scope.orginalcolumnsdata = [];
            $scope.fieldHeaders = [];
            $scope.filterOptions = {
                filterText: ''
            };

            $scope.nofrows = function() {
                var num = "";
                if (dataOptions.pagination == true) {
                    if ($scope.gridApi.grid.options.totalItems != $scope.totalItemsLength)
                        num = $scope.gridApi.grid.options.totalItems + " of " + $scope.totalItemsLength;
                    else
                        num = $scope.gridApi.grid.options.totalItems;
                } else {
                    num = $scope.gridApi.grid.getVisibleRowCount();
                }

                return num;
            }

            $scope.gridOptions = {
                enableRowSelection: true,
                enableRowHeaderSelection: false,
                exporterCsvColumnSeparator: "\t",
                exporterCsvFilename: "myFile.csv",
                exporterOlderExcelCompatibility: true,
                enableColumnHeavyVirt: true,
                enableCellEdit: false,
                editableCellTemplate: false
            };

            $scope.otherOptions = dataOptions;

            if (dataOptions.paginationPageSize != undefined && dataOptions.paginationPageSize > 0) {
                $scope.gridOptions.paginationPageSize = dataOptions.paginationPageSize;
            }

            if (dataOptions.pagination == true) {
                $scope.gridOptions.paginationPageSizes = [500, 1000, 2000, 5000];
            }

            $scope.gridOptions.data = 'myData';
            $scope.idCounter = 0;

            $scope.gridOptions.onRegisterApi = function(gridApi) {
                $scope.gridApi = gridApi;
                gridApi.selection.on.rowSelectionChanged($scope, function() {
                    //console.log("row selelcted " + JSON.stringify($scope.gridApi.selection.getSelectedRows()));
                    if (dataOptions.changeCallBack != null || dataOptions.changeCallBack != undefined)
                    	dataOptions.changeCallBack($scope.containerId);
                    // $scope.selectedAssetID = $scope.gridApi.selection.getSelectedRows()[0].ASSETID;
                });
                gridApi.core.on.sortChanged($scope, function(grid, sortColumns) {
                    // console.log("sorting change");
                    $scope.getGridData();
                    setDatatoLocalStorage($scope.containerId, $scope.state);
                    dataOptions.sortCallBack();
                });
                gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
                    dataOptions.editAfterCallBack(rowEntity, colDef, newValue, oldValue);
                });
                gridApi.edit.on.beginCellEdit($scope, function(rowEntity) {
                    dataOptions.editBeforeCallBack(rowEntity);
                });
                try {
                    gridApi.pagination.on.paginationChanged($scope, function() {
                        dataOptions.paginationChangeCallBack();
                    });
                } catch (e) {}
            };

            $scope.gridOptions.rowIdentity = function(row) {
                return row.id;
            };
            $scope.gridOptions.getRowIdentity = function(row) {
                return row.id;
            };

            var myDummyData;

            $scope.refreshData = function() {
                $scope.gridApi.core.refresh();
            };

            $scope.reloadGrid = function(data, coldata, options) {
            	$('#filterDataSource_' + $scope.containerId + '').val('');
                $scope.myData = [];
                $scope.otherOptions = $.extend({}, $scope.otherOptions, options);

                //var containerId = $scope.gridApi.grid.element.attr("id").split("grid")[0];

                var i = 0;
                if (!$.isArray(data)) data = [];
                data.forEach(function(row) {
                    // row.displayName = row.title;
                    row.id = i;
                    i++;
                    $scope.myData.push(row);
                    $scope.idCounter = row.id;
                });
                //$scope.myData = data;
                myDummyData = angular.copy($scope.myData);
                $scope.totalItemsLength = $scope.myData.length-1;
                // $scope.filterOptions = $scope.filterOptions;

                $scope.gridOptions.enableColumnResizing = true;
                $scope.gridOptions.enableFiltering = false;
                $scope.gridOptions.enableGridMenu = false;
                $scope.gridOptions.showGridFooter = false;
                $scope.gridOptions.showColumnFooter = false;
                $scope.gridOptions.fastWatch = true;
               // $scope.gridOptions.enableSorting = true;
                //$scope.gridOptions.enableColumnMenus = true;
                $scope.gridOptions.flatEntityAccess = true;
                $scope.gridOptions.exporterPdfDefaultStyle = {
                    fontSize: 9
                };
                $scope.gridOptions.exporterPdfTableStyle = {
                    margin: [30, 30, 30, 30]
                };
                $scope.gridOptions.exporterPdfTableHeaderStyle = {
                    fontSize: 10,
                    bold: true,
                    italics: true,
                    color: 'red'
                };
                $scope.gridOptions.exporterPdfOrientation = 'portrait';
                $scope.gridOptions.exporterPdfPageSize = 'LETTER';
                $scope.gridOptions.exporterPdfMaxGridWidth = 500;
                $scope.gridOptions.multiSelect = $scope.otherOptions.multiSelect;
                $scope.gridOptions.modifierKeysToMultiSelect = false;
                $scope.gridOptions.noUnselect = !$scope.otherOptions.multiSelect;
                if(!$scope.otherOptions.noUnselect)
                    $scope.gridOptions.noUnselect = $scope.otherOptions.noUnselect

                //checking data from local storage and applying sort fields
                var localdata = getDatafromLocalStorage($scope.containerId);
                $scope.selectedSearchColumns = [];
                //-------------checking new add or modied fields-------------
                try {
                    if (coldata.length == localdata.columns.length) {
                        for (var j = 0; j < coldata.length; j++) {
                            var exists = false;
                            for (var i = 0; i < localdata.columns.length; i++) {
                                if (localdata.columns[i].name == coldata[j].field) {
                                    exists = true;
                                    break;
                                }
                            };
                            if (!exists) {
                                localdata = "";
                                break;
                            }
                        }
                    } else {
                    	if(coldata && coldata.length > 0)
                        localdata = "";
                    }
                } catch (e) {
//                    console.log("error in data checking localstorage");
                    localdata = "";
                }

                //-------------checking new add or modied fields-------------
                var newcolumndata = [];
                if (localdata != "") {
                    if (localdata.columns) {
                        for (var idx = 0; idx < localdata.columns.length; idx++) {
                            for (var j = 0; j < coldata.length; j++) {
                                if (coldata[j].field == localdata.columns[idx].name && !$.isEmptyObject(localdata.columns[idx].sort)) {
                                    coldata[j].sort = localdata.columns[idx].sort;
                                    break;
                                }

                            }
                            for (var j = 0; j < coldata.length; j++) {
                                if (coldata[j].field == localdata.columns[idx].name) {
                                    coldata[j].visible = false;
                                    if (localdata.columns[idx].visible != false && localdata.columns[idx].visible != "false")
                                        coldata[j].visible = true;
                                    newcolumndata.push(coldata[j]);
                                    break;
                                }

                            }
                        }
                    }
                    if (newcolumndata && newcolumndata.length > 0)
                        coldata = newcolumndata;
                    if (localdata.selectedSearchColumns) {
                        $scope.selectedSearchColumns = localdata.selectedSearchColumns;
                    }

                }

                for (var i = 0; i < coldata.length; i++) {
                    coldata[i].disableColumnMenu = true;
                    if (coldata[i].displayName == undefined)
                        coldata[i].displayName = coldata[i].title;
                    if (coldata[i].minWidth == undefined)
                        coldata[i].minWidth = 50;
                    if (coldata[i].cellTooltip == undefined)
                        coldata[i].cellTooltip = true;
                    if (coldata[i].headerTooltip == undefined)
                        coldata[i].headerTooltip = true;
                    coldata[i].menuItems = [{
                        title: 'Filter',
                        action: function(context, colfilter) {
                            /* alert('context = '+context+' event = '+event+' title = '+title+' Grid ID: ' + this.grid.id);*/
                            // alert('select = ' + colfilter.select1 + ' input = ' + colfilter.input1 + ' select = ' + colfilter.select2 + ' select = ' + colfilter.select3 + ' select = ' + colfilter.input2);
                            var filter = {};
                            if (colfilter) {
                                filter = {
                                    field: context.context.col.field,
                                    displayName: context.context.col.displayName,
                                    condition: {
                                        condition1: colfilter.select1 ? colfilter.select1 : "",
                                        condition2: colfilter.select2 ? colfilter.select2 : "",
                                        condition3: colfilter.select3 ? colfilter.select3 : ""
                                    },
                                    value: {
                                        value1: colfilter.input1 ? colfilter.input1 : "",
                                        value2: colfilter.input2 ? colfilter.input2 : ""
                                    }
                                }
                            }
                            updateFiltertoLocalStorage($scope.containerId, filter);
                            $scope.searchfilterfunction($scope.containerId);
                        }
                    }];
                };

                $scope.gridOptions.columnDefs = angular.copy(coldata);
                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.ALL);

                //setTimeout(function() {

                    var allow = false;
                    if (!($scope.selectedSearchColumns && $scope.selectedSearchColumns.length > 0))
                        allow = true;
                    for (var idx = 0; idx < $scope.gridOptions.columnDefs.length; idx++) {
                        $scope.fieldHeaders[$scope.gridOptions.columnDefs[idx].field] = {
                            "displayName": $scope.gridOptions.columnDefs[idx].displayName,
                            "dataType": angular.isDefined($scope.gridOptions.columnDefs[idx].dataType) ? $scope.gridOptions.columnDefs[idx].dataType : "string",
                            "index": idx
                        }
                        if (allow)
                            $scope.selectedSearchColumns.push($scope.gridOptions.columnDefs[idx].field);
                    }
                    $scope.getGridData();

                    setDatatoLocalStorage($scope.containerId, $scope.state);

                    $scope.gridApi.core.refresh();
                // $(window).resize();
                //}, 200);

            }

            $scope.selectRowByIndex = function(idx) {
                $scope.gridApi.selection.clearSelectedRows();
                var visibleRows = $scope.gridApi.core.getVisibleRows();
                if (visibleRows.length == 0) return;
                $scope.gridApi.selection.toggleRowSelection(visibleRows[idx].entity, false);               
                $scope.gridApi.core.refresh();
            }

            $scope.scrollToIndex = function(idx){
                var visibleRows = $scope.gridApi.core.getVisibleRows();
                if(idx >=  visibleRows.length)
                    idx = visibleRows.length-1;
                if(idx < 0)
                    idx = 0;
                $scope.gridApi.grid.scrollTo(visibleRows[idx].entity);
            }

            $scope.refreshFilterData = function() {
                // $scope.myData = $filter('filter')(myDummyData, $scope.searchText, undefined);
                if ($scope.searchCounter != null) {
                    clearTimeout($scope.searchCounter);
                }
                $scope.searchCounter = setTimeout(function() {
                    $scope.searchfilterfunction($scope.containerId);
                    $scope.gridApi.core.refresh();
                }, 200);
            };

            $scope.searchfilterfunction = function(containerId) {
                var localdata = getDatafromLocalStorage(containerId);
                var fieldHeaders = $scope.fieldHeaders;
                var columnsFilterdata = [];
                if (localdata != "") {
                    columnsFilterdata = localdata.filters
                }
                $scope.myData = myDummyData.filter(function(item) {
                    var searchrow = false;
                    if ($scope.searchText && $scope.searchText.length > 0 && $scope.selectedSearchColumns.length > 0) {
                        // angular.forEach(item, function(value, key) 
                        for (var key in item) {
                            if ($scope.selectedSearchColumns.indexOf(key) > -1) {
                                try {
                                    var value = (item[key]).toLowerCase();
                                    if (fieldHeaders[key].dataType == "date") {
                                        var datekey = key.split('MS')[0];
                                        value = (item[datekey]).toLowerCase()
                                    }

                                    var searchtxt = ($scope.searchText).toLowerCase();
                                    if (value.indexOf(searchtxt) > -1 && !searchrow) {
                                        searchrow = true;
                                    }
                                } catch (e) {}
                            }

                        }
                    } else {
                        searchrow = true;
                    }

                    if (searchrow == true) {
                        //Check for individual column filters.
                        return $scope.checkIndividualColumnFilter(columnsFilterdata, item);
                    }
                });
            }

            $scope.checkIndividualColumnFilter = function(columnsFilterdata, item) {
                var searchfilter = false;
                // var columnsdata = $scope.gridOptions.columnDefs
                if (!columnsFilterdata)
                    return true;
                var checkAllFilters = 0;
                for (var i = 0; i < columnsFilterdata.length; i++) {

                    var filterObj = columnsFilterdata[i];
                    if (!$.isEmptyObject(filterObj)) {
                        searchfilter = true;

                        var reg, condition1, condition2;
                        switch (Number(filterObj.condition.condition1)) {
                            case uiGridConstants.filter.STARTS_WITH:
                                {
                                    try {
                                        reg = new RegExp('^' + filterObj.value.value1, 'i');
                                        condition1 = reg.test(item[$scope.findDataType(filterObj.field)]);
                                    } catch (e) {
                                        condition1 = item[$scope.findDataType(filterObj.field)].indexOf(filterObj.value.value1) == 0;
                                    }
                                }
                                break;
                            case uiGridConstants.filter.ENDS_WITH:
                                {
                                    try {
                                        reg = new RegExp(filterObj.value.value1 + '$', 'i');
                                        condition1 = reg.test(item[$scope.findDataType(filterObj.field)]);
                                    } catch (e) {
                                        condition1 = item[$scope.findDataType(filterObj.field)].indexOf(filterObj.value.value1) == (item[$scope.findDataType(filterObj.field)].length - 1);
                                    }
                                }
                                break;
                            case uiGridConstants.filter.EXACT:
                                {
                                    try {
                                        reg = new RegExp('^' + filterObj.value.value1 + '$', 'i');
                                        condition1 = reg.test(item[$scope.findDataType(filterObj.field)]);
                                    } catch (e) {
                                        condition1 = item[$scope.findDataType(filterObj.field)] == filterObj.value.value1;
                                    }
                                }
                                break;
                            case uiGridConstants.filter.CONTAINS:
                                {
                                    try {
                                        reg = new RegExp(filterObj.value.value1, 'i');
                                        condition1 = reg.test(item[$scope.findDataType(filterObj.field)]);
                                    } catch (e) {
                                        condition1 = item[$scope.findDataType(filterObj.field)].indexOf(filterObj.value.value1) != -1;
                                    }
                                }
                                break;
                            case uiGridConstants.filter.GREATER_THAN:
                                {
                                    condition1 = (item[$scope.findDataType(filterObj.field)] > filterObj.value.value1);
                                }
                                break;
                            case uiGridConstants.filter.GREATER_THAN_OR_EQUAL:
                                {
                                    condition1 = (item[$scope.findDataType(filterObj.field)] >= filterObj.value.value1);
                                }
                                break;
                            case uiGridConstants.filter.LESS_THAN:
                                {
                                    condition1 = (item[$scope.findDataType(filterObj.field)] < filterObj.value.value1);
                                }
                                break;
                            case uiGridConstants.filter.LESS_THAN_OR_EQUAL:
                                {
                                    condition1 = (item[$scope.findDataType(filterObj.field)] <= filterObj.value.value1);
                                }
                                break;
                            case uiGridConstants.filter.NOT_EQUAL:
                                {
                                    condition1 = (item[$scope.findDataType(filterObj.field)] != filterObj.value.value1);
                                }
                                break;
                        }
                        switch (Number(filterObj.condition.condition3)) {
                            case uiGridConstants.filter.STARTS_WITH:
                                {
                                    try {
                                        reg = new RegExp('^' + filterObj.value.value2, 'i');
                                        condition2 = reg.test(item[$scope.findDataType(filterObj.field)]);
                                    } catch (e) {
                                        condition2 = item[$scope.findDataType(filterObj.field)].indexOf(filterObj.value.value2) == 0;
                                    }
                                }
                                break;
                            case uiGridConstants.filter.ENDS_WITH:
                                {
                                    try {
                                        reg = new RegExp(filterObj.value.value2 + '$', 'i');
                                        condition2 = reg.test(item[$scope.findDataType(filterObj.field)]);
                                    } catch (e) {
                                        condition2 = item[$scope.findDataType(filterObj.field)].indexOf(filterObj.value.value2) == (item[$scope.findDataType(filterObj.field)].length - 1);
                                    }
                                }
                                break;
                            case uiGridConstants.filter.EXACT:
                                {
                                    try {
                                        reg = new RegExp('^' + filterObj.value.value2 + '$', 'i');
                                        condition2 = reg.test(item[$scope.findDataType(filterObj.field)]);
                                    } catch (e) {
                                        condition2 = item[$scope.findDataType(filterObj.field)] == filterObj.value.value2;
                                    }
                                }
                                break;
                            case uiGridConstants.filter.CONTAINS:
                                {
                                    try {
                                        reg = new RegExp(filterObj.value.value1, 'i');
                                        condition1 = reg.test(item[$scope.findDataType(filterObj.field)]);
                                    } catch (e) {
                                        condition1 = item[$scope.findDataType(filterObj.field)].indexOf(filterObj.value.value1) != -1;
                                    }
                                }
                                break;
                            case uiGridConstants.filter.GREATER_THAN:
                                {
                                    condition2 = (item[$scope.findDataType(filterObj.field)] > filterObj.value.value2);
                                }
                                break;
                            case uiGridConstants.filter.GREATER_THAN_OR_EQUAL:
                                {
                                    condition2 = (item[$scope.findDataType(filterObj.field)] >= filterObj.value.value2);
                                }
                                break;
                            case uiGridConstants.filter.LESS_THAN:
                                {
                                    condition2 = (item[$scope.findDataType(filterObj.field)] < filterObj.value.value2);
                                }
                                break;
                            case uiGridConstants.filter.LESS_THAN_OR_EQUAL:
                                {
                                    condition2 = (item[$scope.findDataType(filterObj.field)] <= filterObj.value.value2);
                                }
                                break;
                            case uiGridConstants.filter.NOT_EQUAL:
                                {
                                    condition2 = (item[$scope.findDataType(filterObj.field)] != filterObj.value.value2);
                                }
                                break;
                        }
                        if (filterObj.condition.condition2 == "And") {
                            if ($.trim(filterObj.value.value1) != "" && $.trim(filterObj.value.value2) != "") {
                                if (condition1 && condition2)
                                    checkAllFilters++;
                                else
                                    return false;
                            } else {
                                if ($.trim(filterObj.value.value1) != "") {
                                    if (condition1)
                                        checkAllFilters++;
                                    else
                                        return false;
                                } else {
                                    if (condition2)
                                        checkAllFilters++;
                                    else
                                        return false;
                                }
                            }
                        } else {
                            if ($.trim(filterObj.value.value1) != "" && $.trim(filterObj.value.value2) != "") {
                                if (condition1 || condition2)
                                    checkAllFilters++;
                                else
                                    return false;
                            } else {
                                if ($.trim(filterObj.value.value2) != "") {
                                    if (condition2)
                                        checkAllFilters++;
                                    else
                                        return false;
                                } else {
                                    if (condition1)
                                        checkAllFilters++;
                                    else
                                        return false;
                                }
                            }
                        }

                    }

                }
                if (checkAllFilters < columnsFilterdata.length)
                    return false;
                else
                    return true;
            }

            $scope.findDataType = function(key) {
                var datekey;
                if ($scope.fieldHeaders[key].dataType == "date") {
                    datekey = key.split("MS")[0];
                    return datekey;
                }
                return key
            }

            $scope.setGridSelection = function(entity) {
                $scope.gridApi.selection.clearSelectedRows();
                // var visibleRows= $scope.gridApi.core.getVisibleRows();
                $scope.gridApi.selection.toggleRowSelection(entity, false);
                $scope.gridApi.core.refresh();
            }

            $scope.updatechanges = function() {
                $scope.reloadGrid($scope.myData, $scope.orginalcolumnsdata, "");
            }

            $scope.openMultiSort = function() {
                openMultiSortWindowAngular(this.containerId);
            }
            $scope.openPrint = function() {
                printGrid(this.containerId, $scope.myData);
            }
            $scope.openExportCSV = function() {
                openExportWindowAngular(this.containerId);
            }
            $scope.openShowHideColumns = function() {
                openShowHideWindowAngular(this.containerId);
            }
            $scope.openClear = function() {
                localStorage.removeItem("AngularUIgrid" + $scope.containerId);
                $scope.searchText = "";
                $scope.searchfilterfunction($scope.containerId);
                $scope.updatechanges();
            }
            $scope.openAddNew = function() {
                dataOptions.addCallBack();
            }

            $scope.extraBtn = function() {
                dataOptions.extraBtnCallBack();
            }

            $scope.globalAndcolumnSearch = function() {
                //columnSpecificSearchAngular(this.containerId);
            }

            $scope.setGridData = function() {
                var localdata = getDatafromLocalStorage($scope.containerId);
                if (localdata != "")
                    $scope.state = localdata
                $scope.gridApi.saveState.restore($scope, $scope.state);
            }
            $scope.getGridData = function() {
                $scope.state = $scope.gridApi.saveState.save();
            }

            $scope.adjustGridHeight = function(hei) {
                $scope.gridHeight = hei;
            }

            $scope.applyGlobalSearch = function() {
                searchWindow.close();
            }

            $scope.doubleClick = function(e) {
                if ($($(e.target).closest('div')).hasClass('ng-binding') && $($(e.target).closest('div')).hasClass('ui-grid-cell-contents')) {
                    if (dataOptions.dbClickCallBack)
                        dataOptions.dbClickCallBack();
                }
            }

            $scope.searchindetails = function(arr, str) {
                $scope.detailsSearchList = arr;
                var myDummyData = angular.copy($scope.detailsSearchList);
                if (str != "" && str != undefined)
                    $scope.detailsSearchList = $filter('filter')(myDummyData, str, undefined);
                return $scope.detailsSearchList;
            }

            $scope.insertAt = function(data, num) {
                var idvalue = 0;
                if (num) {
                    try {
                        for (var i = $scope.myData.length - 1; i >= num; i--) {
                            var obj = $scope.myData[i];
                            obj.id = obj.id + 1;
                            $scope.myData[(i + 1)] = obj;
                        };
                        data.id = $scope.myData[num].id - 1;
                        $scope.myData[num] = data;
                    } catch (e) {}
                } else {
                    if ($scope.myData && $scope.myData.length > 0)
                        /*idvalue = ($scope.myData[$scope.myData.length - 1].id) + 1;
                    data.id = idvalue;*/
                    $scope.idCounter ++;
                    data.id = $scope.idCounter;
                   // $scope.myData.push(data);
                   $scope.myData.unshift(data);
                    // $scope.myData.splice(0,0,data);
                }
                $scope.refreshData();
            }

            $scope.update = function(data) {
                for (var i = 0; i < $scope.myData.length; i++) {
                    if ($scope.myData[i].id == data.id) {
                        $scope.myData[i] = data;
                        break;
                    }
                };
                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.ALL);
                setTimeout(function() {
                    $scope.gridApi.core.redrawCanvas();
                }, 100);
            }

            $scope.remove = function(data) {
                var pos = -1;
                for (var i = 0; i < $scope.myData.length; i++) {
                    if ($scope.myData[i].id == data.id) {
                        pos = i;
                        break;
                    }
                };
                if (pos > -1) {
                    $scope.myData.splice(pos, 1);
                }
                $scope.refreshData();
            }
            
            $scope.setRowColor = function(key, value, color, condition) {
                setTimeout(function(){
                    var visibleRows = $scope.gridApi.core.getVisibleRows();
                    for (var i = 0; i < visibleRows.length; i++) {
//                        $($("#"+$scope.containerId+"ID .ui-grid-row")[i]).find(".ui-grid-cell-contents.ng-binding").css({"background-color":""});
                        if (visibleRows[i].entity[key] == value) {
                            $($("#"+$scope.containerId+"ID .ui-grid-row")[i]).find(".ui-grid-cell-contents.ng-binding").addClass(color);
                        }
                    };
                },500);
            }

        }
    ]);
    app.directive('rightClick', function() {
        document.oncontextmenu = function(e) {
            if (e.target.hasAttribute('right-click')) {
                return false;
            }
        };
        return function(scope, el, attrs) {
            el.bind('contextmenu', function(e) {
                e.preventDefault();
                try {
                    if ($($(e.target).closest('div')).hasClass('ng-binding') && $($(e.target).closest('div')).hasClass('ui-grid-cell-contents')) {
                        var entity = angular.element($(e.target).closest('div').parent().parent().parent()).scope().row.entity;
                        // var index = $(e.target).closest('div').parent().parent().parent().index();
                        scope.setGridSelection(entity);
                        var localCords = $("#" + scope.containerId).offset();
                        var localPos = $(this).position();
                        if (Math.round(localPos.top) > Math.round(localCords.top))
                            localCords.top = 30;
                        if (e.pageX && e.pageY) {
                            $("#columnRowContextMenu" + scope.containerId).css({
                                display: "block",
                                top: (e.pageY - localCords.top) + "px",
                                left: (e.pageX - localCords.left - 10) + "px"
                            });
                        } else {
                            if (e.clientX && e.clientY) {
                                $("#columnRowContextMenu" + scope.containerId).css({
                                    display: "block",
                                    top: (e.clientY - localCords.top - 10) + "px",
                                    left: (e.clientX - localCords.left - 10) + "px"
                                });
                            }
                        }
                        $(document).off('mousedown');
                        $(document).on('mousedown', function(event) {
                            try {
                                if (event.target.textContent == "Details") {
                                    //showRowDetailsWindowAngular(scope.containerId, scope.gridApi.selection.getSelectedRows());
                                }
                            } catch (e) {
                                console.log("error " + e)
                            }
                            $("#columnRowContextMenu" + scope.containerId).css({
                                display: "none",
                                top: -100 + "px",
                                left: -100 + "px"
                            });
                        });
                    }
                } catch (e) {}
            });
        }
    });
}


AngularUIGridWrapper.prototype.init = function() {
    angular.bootstrap($("#" + this.containerId + "ID"), [this.containerId + 'app']);
    angular.element(document.getElementById(this.containerId + 'ID')).scope().containerId = this.containerId;
}

AngularUIGridWrapper.prototype.getSelectedRows = function() {
    return angular.element(document.getElementById(this.containerId + 'ID')).scope().gridApi.selection.getSelectedRows();
}

AngularUIGridWrapper.prototype.getSelectedRowIndex = function() {
    var arr = this.getVisibleRows();
    var selectedArr = this.getSelectedRows();
    var returnArr = [];
    for (var i = 0; i < arr.length; i++) {
        for (var j = 0; j < selectedArr.length; j++) {
            if(arr[i].entity.id == selectedArr[j].id)
                returnArr.push(i);
        };
        
    };
    return returnArr;
}

AngularUIGridWrapper.prototype.getVisibleRows = function() {
    return angular.element(document.getElementById(this.containerId + 'ID')).scope().gridApi.core.getVisibleRows();
}

AngularUIGridWrapper.prototype.creategrid = function(gridDataSource, columndata, gridOptions) {
    angular.element(document.getElementById(this.containerId + 'ID')).scope().orginalcolumnsdata = angular.copy(columndata);
    angular.element(document.getElementById(this.containerId + 'ID')).scope().reloadGrid(gridDataSource, columndata, gridOptions);
}

AngularUIGridWrapper.prototype.adjustGridHeight = function(height) {
    angular.element(document.getElementById(this.containerId + 'ID')).scope().adjustGridHeight((height));
    var grid = $('#' + this.containerId + 'grid');
    grid.height(height);
}
AngularUIGridWrapper.prototype.selectRowByIndex = function(index) {
    angular.element(document.getElementById(this.containerId + 'ID')).scope().selectRowByIndex(index);
}

AngularUIGridWrapper.prototype.scrollToIndex = function(index) {
    angular.element(document.getElementById(this.containerId + 'ID')).scope().scrollToIndex(index);
}

AngularUIGridWrapper.prototype.getAllRows = function() {
    return angular.element(document.getElementById(this.containerId + 'ID')).scope().gridApi.grid.rows;
}

AngularUIGridWrapper.prototype.getScope = function() {
    return angular.element(document.getElementById(this.containerId + 'ID')).scope();
}

AngularUIGridWrapper.prototype.dataArray = function() {
    return angular.element(document.getElementById(this.containerId + 'ID')).scope().myData;
}

AngularUIGridWrapper.prototype.dataOptions = function() {
    return angular.element(document.getElementById(this.containerId + 'ID')).scope().otherOptions;
}

AngularUIGridWrapper.prototype.insert = function(obj, idx) {
    angular.element(document.getElementById(this.containerId + 'ID')).scope().insertAt(obj, idx);
}

AngularUIGridWrapper.prototype.update = function(obj) {
    angular.element(document.getElementById(this.containerId + 'ID')).scope().update(obj);
}
AngularUIGridWrapper.prototype.deleteItem = function(obj) {
    angular.element(document.getElementById(this.containerId + 'ID')).scope().remove(obj);
}

AngularUIGridWrapper.prototype.refreshGrid = function() {
    return angular.element(document.getElementById(this.containerId + 'ID')).scope().refreshData();
}

AngularUIGridWrapper.prototype.selectAll = function() {
    $scope.gridApi.selection.selectAllRows();
};

AngularUIGridWrapper.prototype.clearAll = function() {
    var scope = angular.element(document.getElementById(this.containerId + 'ID')).scope();
    scope.gridApi.selection.clearSelectedRows();
    scope.refreshData();
};

AngularUIGridWrapper.prototype.rowColor = function(key,value,color,condition) {
    angular.element(document.getElementById(this.containerId + 'ID')).scope().setRowColor(key,value,color,condition);
}

//setting grid data to localstorage
function setDatatoLocalStorage(containerId, data) {
    if (!data.selectedSearchColumns) {
        var selectedSearchColumns = angular.element(document.getElementById(containerId + 'ID')).scope().selectedSearchColumns;
        data.selectedSearchColumns = selectedSearchColumns;
    }
    localStorage.setItem("AngularUIgrid" + containerId, JSON.stringify(data));
}

function updateFiltertoLocalStorage(containerId, filterData) {
    var localdata = getDatafromLocalStorage(containerId);
    if (localdata != "") {
        if (!localdata.filters)
            localdata.filters = [];
        var exists = -1;
        for (var i = 0; i < localdata.filters.length; i++) {
            if (localdata.filters[i].field == filterData.field) {
                exists = i;
                localdata.filters[i] = filterData;
            }
        };
        if ($.trim(filterData.value.value1) == "" && $.trim(filterData.value.value2) == "") {
            if (exists > -1) {
                localdata.filters.splice(exists, 1);
            }

        } else {
            if (exists == -1)
                localdata.filters.push(filterData);
        }

        localStorage.setItem("AngularUIgrid" + containerId, JSON.stringify(localdata));
    }
}

//getting grid data to localstorage
function getDatafromLocalStorage(containerId) {
    var data = localStorage.getItem("AngularUIgrid" + containerId);
    if (data != null && data != "undefined" && data != "") {
        data = JSON.parse(data);
    } else
        data = "";

    return data;
}

function saveAngularData() {

}

function openMultiSortWindowAngular(containerId) {
    //multicolumn sort popup window
    // if (otherOptions_local.toolbarSort != false) {
    $('#popups' + containerId + '').html('');
    multiSortWindow = $("#popups" + containerId + '').kendoWindow({
        title: "Select sort columns",
        modal: true,
        visible: false,
        resizable: false,
        width: 450,
        height: 300
    }).data("kendoWindow");
    // }

    $("#popups" + containerId + '').parent().css({
        "width": "450px",
        "height": "300px"
    });
    var gridId = containerId;
    $('<div style="margin-left: 10px"><div><span id="firstRowMultiSort" style="line-height: 30px">Select column :&nbsp;&nbsp;&nbsp;<select id="sortComboBox' + gridId + '"></select><br /></span><span style="line-height: 40px" id="addSortOptions" ><input type="radio" checked="true" value="asc" name="sorting" class="sorting" id="sortAsc" /> Ascending &nbsp;&nbsp;&nbsp;<input type="radio"  name="sorting" value="desc" class="sorting" id="sortDsc"/> Descending &nbsp;&nbsp;&nbsp;<input class="wrapperButton themeButton" type="button" id="btnAddSortColumn' + gridId + '" value="Add" /></span></div><div><span style="font-weight: bold; padding-left:34px;  bottom: 2px;">Sort on:</span></div><div><span ><table style="margin-top: 5px;"><tr><td style="padding-left:34px;"><div id="appendSortColumn" style="min-width:360px; height:170px; border:2px solid lightGray; border-radius: 5px;  vertical-align: top; overflow:auto"><ul id="columnAppendUL' + gridId + '" class="tabSelect"></ul> </div></td><td ><div style="height: 30px; width: 30px"><img src="' + imagePath + 'up-arrow.png" class="sortWindowIcon" title="Move up" id="btnMoveUp' + gridId + '" /></div> <div style="height: 30px; width: 30px"><img src="' + imagePath + 'down-arrow.png" class="sortWindowIcon" title="Move down" id="btnMoveDown' + gridId + '" /></div><div style="height: 30px; width: 30px"><img src="' + imagePath + 'delete-icon.png" class="sortWindowDeleteIcon" title="Cancel" id="iconSortlistRemove' + gridId + '" /></div></td></tr></table></span></div> <div style="margin-top: 5px"><div style="padding-left:50px;"><span><input class="wrapperButton themeButton" type="button" id="btnSortReset' + gridId + '" value="Reset" />&nbsp;&nbsp;<input class="wrapperButton themeButton" type="button" id="btnSortClear' + gridId + '" value="Clear" />&nbsp;&nbsp;<input class="wrapperButton themeButton" type="button" id="btnSortOk' + gridId + '" value="Ok" />&nbsp;&nbsp;<input class="wrapperButton themeButton" type="button" id="btnSortClose' + gridId + '" value="Cancel" /></div></span></div></div>').appendTo('#popups' + containerId + '');

    $("#sortComboBox" + gridId + "").html("");
    $('#columnAppendUL' + gridId + '').html("");
    var localdata = getDatafromLocalStorage(containerId);
    var fieldHeaders = angular.element(document.getElementById(containerId + 'ID')).scope().fieldHeaders;
    var sortOptionApplied = "";
    var sortOptionNotApplied = "";
    if (localdata.columns) {
        for (var i = 0; i < localdata.columns.length; i++) {
            if (localdata.columns[i].visible != false) {
                if (!$.isEmptyObject(localdata.columns[i].sort) && localdata.columns[i].visible != false) {
                    sortOptionApplied += "<li rel=" + localdata.columns[i].sort.direction + " field=" + localdata.columns[i].name + ">" + fieldHeaders[localdata.columns[i].name].displayName + "(" + localdata.columns[i].sort.direction + ")</li>";
                } else {
                    if(fieldHeaders[localdata.columns[i].name].dataType && fieldHeaders[localdata.columns[i].name].dataType.toLowerCase() != "checkbox")
                        sortOptionNotApplied += "<option value='" + localdata.columns[i].name + "'>" + fieldHeaders[localdata.columns[i].name].displayName + "</option>";
                }
            }
        }
    }
    $("#sortComboBox" + gridId + "").html(sortOptionNotApplied);
    $('#columnAppendUL' + gridId + '').html(sortOptionApplied);
    multiSortWindow.center().open();
    if ($('#sortComboBox' + gridId + ' option').length == 0) {
        $("#firstRowMultiSort").hide();
    } else {
        $("#firstRowMultiSort").show();
    }

    // add column to list box for multi-column sorting
    $("#btnAddSortColumn" + gridId + "").click(function(e) {
        /*if ($('#sortComboBox' + gridId + ' option').length == 1) {
            $("#firstRowMultiSort").hide();
        }*/
        //removing added item from drop down list
        var selectedItem = $('#sortComboBox' + gridId + '').val();
        var selectedText = $('#sortComboBox' + gridId + '' + ' option:selected').text();
        if (selectedItem != null) {
            $('#sortComboBox' + gridId + '' + ' option:selected').remove()
            var selectedOrder = $('.sorting:checked').val();
            $("#columnAppendUL" + gridId + "").append("<li rel=" + selectedOrder + " field=" + selectedItem + ">" + selectedText + "(" + selectedOrder + ")" + "</li>");
        }
        if ($('#sortComboBox' + gridId + ' option').length == 0) {
            $("#firstRowMultiSort").hide();
        } else {
            $("#firstRowMultiSort").show();
        }
        e.preventDefault();
    });

    //highlight the selected column for multi search
    $(document).on('click', "#columnAppendUL" + gridId + " li", function() {
        $("#columnAppendUL" + gridId + " li").removeClass('selected');
        $(this).addClass('selected');
    });


    //selected item moving next position
    $("#btnMoveUp" + gridId + "").click(function(e) {
        var current = $('.selected');
        if ($('.selected').length == 0) {
            alert('Select item from list box.')
            return;
        }
        e.preventDefault();
        current.prev().before(current);
    });

    //selected column moving previous position
    $("#btnMoveDown" + gridId + "").click(function(e) {
        var current = $('.selected');
        if ($('.selected').length == 0) {
            alert('Select item from list box.')
            return;
        }
        e.preventDefault();
        current.next().after(current);
    });
    //remove the slected item form list box
    $("#iconSortlistRemove" + gridId + "").click(function(e) {
        var selectedText = $('.selected').text();
        var selTextArray = selectedText.split("(");
        if (selTextArray && selTextArray.length > 0) {
            selectedText = selTextArray[0];
        }

        //client-side validation for slected item from list box
        if ($('.selected').length == 0) {
            alert('Select item from list box.')
            return;
        } else {
            //appenidng removed column to drop down list
            $('#sortComboBox' + gridId + '').append("<option value='" + $('.selected').attr('field') + "'>" + selectedText + "</option>");
        }
        $('.selected').remove();
        if ($('#sortComboBox' + gridId + ' option').length == 0) {
            $("#firstRowMultiSort").hide();
        } else {
            $("#firstRowMultiSort").show();
        }
        e.preventDefault();

    });

    $("#btnSortReset" + gridId + "").click(function() {
        $("#sortComboBox" + gridId + "").html("");
        $('#columnAppendUL' + gridId + '').html("");
        var localdata = getDatafromLocalStorage(containerId);
        var fieldHeaders = angular.element(document.getElementById(containerId + 'ID')).scope().fieldHeaders;
        var sortOptionApplied = "";
        var sortOptionNotApplied = "";
        if (localdata.columns) {
            for (var i = 0; i < localdata.columns.length; i++) {
                if (localdata.columns[i].visible != false) {
                    if (!$.isEmptyObject(localdata.columns[i].sort) && localdata.columns[i].visible != false) {
                        sortOptionApplied += "<li rel=" + localdata.columns[i].sort.direction + " field=" + localdata.columns[i].name + ">" + fieldHeaders[localdata.columns[i].name].displayName + "(" + localdata.columns[i].sort.direction + ")</li>";
                    } else {
                    	if(fieldHeaders[localdata.columns[i].name].dataType && fieldHeaders[localdata.columns[i].name].dataType.toLowerCase() != "checkbox")
                            sortOptionNotApplied += "<option value='" + localdata.columns[i].name + "'>" + fieldHeaders[localdata.columns[i].name].displayName + "</option>";
                    }
                }
            }
        }
        $("#sortComboBox" + gridId + "").html(sortOptionNotApplied);
        $('#columnAppendUL' + gridId + '').html(sortOptionApplied);

        if ($('#sortComboBox' + gridId + ' option').length == 0) {
            $("#firstRowMultiSort").hide();
        } else {
            $("#firstRowMultiSort").show();
        }

    });

    // clearing all columns from list box and appenidng to drop down list
    $("#btnSortClear" + gridId + "").click(function() {
        if (($("#columnAppendUL" + gridId + " li").text() == "")) {
            alert('There are no columns in listbox to clear.')
            return;
        }
        var listlen = $("#columnAppendUL" + gridId + " li").length;
        for (i = 0; i < listlen; i++) {
            var field = $("#columnAppendUL" + gridId + " li").eq(i).attr("field");
            var fieldVal = $("#columnAppendUL" + gridId + " li").eq(i).text().split("(")[0];
            $('#sortComboBox' + gridId + '').append("<option value='" + field + "'>" + fieldVal + "</option>");
        }
        $("#columnAppendUL" + gridId).empty();

        if ($('#sortComboBox' + gridId + ' option').length == 0) {
            $("#firstRowMultiSort").hide();
        } else {
            $("#firstRowMultiSort").show();
        }
    });
    //sorting with selected columns 
    $("#btnSortOk" + gridId + "").click(function() {
        var listlen = $("#columnAppendUL" + gridId + " li").length;
        var dsSort = [];
        var listValue;
        var localdata = getDatafromLocalStorage(containerId);
        var fieldHeaders = angular.element(document.getElementById(containerId + 'ID')).scope().fieldHeaders;
        //sorting columns
        if (localdata.columns) {
            for (var i = 0; i < localdata.columns.length; i++) {
                localdata.columns[i].sort = {};
            }
        }
        for (i = 0; i < listlen; i++) {
            //sorting
            listValue = $("#columnAppendUL" + gridId + " li").eq(i).text();
            var listValArray = listValue.split("(");
            if (listValArray && listValArray.length > 0) {
                listValue = listValArray[0];
            }
            var field = $("#columnAppendUL" + gridId + " li").eq(i).attr('field');
            var dir = $("#columnAppendUL" + gridId + " li").eq(i).attr('rel');
            var idx = fieldHeaders[field].index;
            for (var k = 0; k < localdata.columns.length; k++) {
                if (localdata.columns[k].name == field) {
                    localdata.columns[k].sort = {
                        direction: dir,
                        priority: i
                    }
                    break;
                }
            }

        }

        setDatatoLocalStorage(gridId, localdata);
        angular.element(document.getElementById(containerId + 'ID')).scope().setGridData();
        // angular.element(document.getElementById(containerId + 'ID')).scope().updatechanges();
        //closing sort window
        multiSortWindow.close();
    });
    //closing sorting window
    $("#btnSortClose" + gridId + "").click(function(e) {
        multiSortWindow.close();
        e.preventDefault();
    });
}

function openShowHideWindowAngular(containerId) {
    $('#popups' + containerId + '').html('');

    //if(columnsOrderWindow == undefined)
    columnsOrderWindow = $("#popups" + containerId + '').kendoWindow({
        title: "Select columns",
        modal: true,
        visible: false,
        resizable: false,
        width: 475,
        height: 260
    }).data("kendoWindow");
    // }
    $("#popups" + containerId + '').parent().css({
        "width": "475px",
        "height": "260px"
    });
    var gridId = containerId;
    $("<div style='padding-left:25px'><table><tr><td><b>Deselected columns</b></td><td></td><td><b>Selected ordered columns</b></td><td></td></tr><tr><td id='appendHideColumn' style='min-width:200px; height:170px; border:2px solid lightGray; border-radius: 5px; vertical-align: top;'><select multiple id='hideColumnAppend" + gridId + "' class='tabSelect ui-grid-multiselection-for-showhideColumns'></select></td><td><div style='height: 30px; width: 30px'><img src='" + imagePath + "left-arrow.png' class='columnOrderWindowIcon' title='Move left' id='btnColumnMoveLeft" + gridId + "' /></div> <div style='height: 30px; width: 30px'><img src='" + imagePath + "right-arrow.png' class='columnOrderWindowIcon' title='Move right' id='btnColumnMoveRight" + gridId + "' /></div></td><td id='appendOrderColumn' style='min-width:180px; height:170px; border:2px solid lightGray; border-radius: 5px; vertical-align: top;'><select multiple id='orderColumnAppend" + gridId + "' class='tabSelect ui-grid-multiselection-for-showhideColumns'></select></td><td><div style='height: 30px; width: 30px'><img src='" + imagePath + "up-arrow.png' class='columnOrderWindowIcon' title='Move up' id='btnColumnMoveUp" + gridId + "' /></div> <div style='height: 30px; width: 30px'><img src='" + imagePath + "down-arrow.png' class='columnOrderWindowIcon' title='Move down' id='btnColumnMoveDown" + gridId + "' /></div></td></tr></table><div style='margin-top: 5px'><center><input class='wrapperButton themeButton' type='button' value='Ok' id='btnOkColumnVisibility" + gridId + "' />&nbsp;&nbsp;<input class='wrapperButton themeButton' type='button' value='Cancel' id='btnCancelColumnVisibility" + gridId + "' /></center></div></div>").appendTo("#popups" + containerId + '');

    var localdata = getDatafromLocalStorage(containerId);
    var fieldHeaders = angular.element(document.getElementById(containerId + 'ID')).scope().fieldHeaders;
    var otherItems = [];
    if (localdata.columns) {
        for (var i = 0; i < localdata.columns.length; i++) {
            var columnTitle = localdata.columns[i].name;
            if(fieldHeaders && fieldHeaders[columnTitle]){
                var idx = fieldHeaders[columnTitle].index;
                localdata.columns[idx].displayName = fieldHeaders[columnTitle].displayName;
                if(fieldHeaders[columnTitle].dataType && (fieldHeaders[columnTitle].dataType).toLowerCase() == "checkbox"){
                    otherItems.push(localdata.columns[idx]);
                }else{
                    if (localdata.columns[i].visible && (localdata.columns[i].visible == true)){
                        $("#orderColumnAppend" + gridId + "").append("<option rel=" + i + " field='" + localdata.columns[i].name + "'>" + localdata.columns[idx].displayName + "</option>");
                    }else{
                        $("#hideColumnAppend" + gridId + "").append("<option rel=" + i + " field='" + localdata.columns[i].name + "'>" + localdata.columns[idx].displayName + "</option>");
                    }                
                }                
            }
        }
    }

    //selected column moving next position
    $("#btnColumnMoveUp" + gridId + "").click(function(e) {
        var $option = $("#orderColumnAppend" + gridId + " option:selected" );
        if($option.length > 0){           
            $option.first().prev().before($option);
        }

    });

    //selected column moving previous position
    $("#btnColumnMoveDown" + gridId + "").click(function(e) {
        var $option = $("#orderColumnAppend" + gridId + " option:selected" );
        if($option.length > 0){           
            $option.last().next().after($option);
        }
    });
    //selected column moving left to hide
    $("#btnColumnMoveLeft" + gridId + "").click(function(e) {
        var $option = $("#orderColumnAppend" + gridId + " option:selected" );
        if ($("#orderColumnAppend" + gridId + " option").length == 0) {
            alert('There are no columns to move left.')
            return;
        } else if ($option.length == 0) {
            alert('Select item from list box.')
            return;
        }       
         $("#orderColumnAppend" + gridId).find(':selected').appendTo("#hideColumnAppend" + gridId);
    });

    //selected column moving right to show
    $("#btnColumnMoveRight" + gridId + "").click(function(e) {
        var $option = $("#hideColumnAppend" + gridId + " option:selected" );
        if ($("#hideColumnAppend" + gridId + " option").length == 0) {
            alert('There are no columns to move right.')
            return;
        } else if ($option.length == 0) {
            alert('Select item from list box.')
            return;
        }
        $("#hideColumnAppend" + gridId).find(':selected').appendTo("#orderColumnAppend" + gridId);
    });
     //sorting with selected columns 
    $("#btnOkColumnVisibility" + gridId + "").click(function() {
        var showColumnsLength = $("#orderColumnAppend" + gridId + " option").length;
        var showColumnIndex;
        var hideColumnsLength = $("#hideColumnAppend" + gridId + " option").length;
        var localdata = getDatafromLocalStorage(containerId);
        var fieldHeaders = angular.element(document.getElementById(containerId + 'ID')).scope().fieldHeaders;
        var tempOrderedColumns = otherItems;

        for (var index = 0; index < showColumnsLength; index++) {
            showColumnIndex = $("#orderColumnAppend" + gridId + " option").eq(index).attr('rel');
            localdata.columns[showColumnIndex].visible = true;
            tempOrderedColumns.push(localdata.columns[showColumnIndex]);
        }
        for (var index = 0; index < hideColumnsLength; index++) {
            var hideColumnIndex = $("#hideColumnAppend" + gridId + " option").eq(index).attr('rel');
            localdata.columns[hideColumnIndex].visible = false;
            tempOrderedColumns.push(localdata.columns[hideColumnIndex]);
        }
        localdata.columns = tempOrderedColumns;
        setDatatoLocalStorage(gridId, localdata);
        angular.element(document.getElementById(containerId + 'ID')).scope().setGridData();
        // angular.element(document.getElementById(containerId + 'ID')).scope().updatechanges();
        //closing sort window
        columnsOrderWindow.close();
    });
     // close column ordered window
    $("#btnCancelColumnVisibility" + gridId + "").click(function() {
        columnsOrderWindow.close();
    });
      //opening show/hide columns window
    columnsOrderWindow.center().open();
}

function openExportWindowAngular(containerId) {
    $('#popups' + containerId + '').html('');
    //$("#popups").parent().removeAttr("style");

    exportWindow = $("#popups" + containerId + '').kendoWindow({
        title: "Select contents to file",
        modal: true,
        visible: false,
        resizable: false,
        width: 335,
        height: 120
    }).data("kendoWindow");

    $("#popups" + containerId + '').parent().css({
        "width": "335px",
        "height": "120px"
    });
    var gridId = containerId;

    $('<div id="exportDiv' + gridId + '" style="margin-left: 13px"><p>Copy headers :&nbsp;&nbsp; <input type="checkbox" checked="true" id="' + gridId + 'idCopyHeaders" /></p><p style="margin-left:28px"><span>Delimiter </span>: &nbsp;&nbsp;<select id="delimiterId' + gridId + '" style="width: 157px"><option selected="true" value="0" >Comma</option><option value="1">Tab</option><option value="2">Semicolon</option><option value="3" id="idOthers">Others</option></select>&nbsp;&nbsp;<input type="text" id="othersTextBox' + gridId + '" class="othersTextBox" maxlength="1" style="width:30px;padding:3px 5px !important; display:none"/></p><p style="margin-left: 30px"><input class="wrapperButton themeButton" type="button" id="btnAllRows' + gridId + '" value="All rows" />&nbsp;&nbsp;<input class="wrapperButton themeButton" type="button" id="btnSelectedRows' + gridId + '" value="Selected row" />&nbsp;&nbsp;<input type="button" class="wrapperButton themeButton"  id="btnCancel' + gridId + '" value="Cancel" /></p></div>').appendTo('#popups' + containerId + '');

    //opening export window
    exportWindow.center().open();

    // show others text box
    var delimiter = ",";
    //delimiter drop down box on change function
    $('#delimiterId' + gridId + '').change(function() {
        var value = $(this).val();
        if (value == 0) {
            delimiter = ",";
            $("#othersTextBox" + gridId + '').hide();
        }
        if (value == 1) {
            delimiter = "\t";
            $("#othersTextBox"+ gridId + '').hide();
        }
        if (value == 2) {
            delimiter = ";";
            $("#othersTextBox"+ gridId + '').hide();
        }

        // showing other textbox to be given other delimiter by user
        if (value == 3) {
            $("#othersTextBox"+ gridId + '').show();
            $("#othersTextBox"+ gridId + '').focus();
        }
    });

    // save all rows
    $("#btnAllRows"+ gridId + ", #btnSelectedRows"+ gridId + "").click(function(e) {

        //  client-side validation for other delimiter
        if ($('#delimiterId' + gridId + '').val() == 3) {
            delimiter = document.getElementById('othersTextBox').value;
            if (delimiter == "") {
                alert("Enter other delimiter ");
                return;
            }
        }
        var scope = angular.element(document.getElementById(containerId + 'ID')).scope()

        if ($('#' + gridId + 'idCopyHeaders').is(':checked')) {
            scope.gridOptions.headersRequired = true;
        } else {
            scope.gridOptions.headersRequired = false;
        }
        scope.gridOptions.exporterCsvColumnSeparator = delimiter;
        if ($(e.target).attr("id") == "btnAllRows"+ gridId + "")
            scope.gridApi.exporter.csvExport("all");
        else {
            if (scope.gridApi.selection.getSelectedRows().length == 0) {
                alert("Select the row");
                return;
            }
            scope.gridApi.exporter.csvExport("selected");
        }

        //closing export window
        exportWindow.close();
        e.preventDefault();
    }); //closing exporting all rows function


    //closing the export window
    $("#btnCancel"+ gridId + "").click(function(e) {
        exportWindow.close();
        e.preventDefault();
    });
}

//column specific search filtering function
function columnSpecificSearchAngular(containerId) {
    //column specified search
    // if (otherOptions_local.toolbarSearch != false) {
    searchWindow = $("#popups" + containerId + '').kendoWindow({
        title: "Select columns",
        modal: true,
        visible: false,
        resizable: false,
        width: 350,
        height: 320
    }).data("kendoWindow");
    // }
    $('#popups' + containerId + '').html('');
    $("#popups" + containerId + '').parent().css({
        "width": "350px",
        "height": "320px"
    });
    var selectedSearchColumns = angular.element(document.getElementById(containerId + 'ID')).scope().selectedSearchColumns;

    $('<span id="columnSearchButtons" /><div style="margin:0 15px;"><div style="border:solid 1px #ccc; margin-bottom:7px;  background-color: #f5f5f5;border: 1px solid #ccc; border-radius: 4px; height:230px; overflow:auto;"><span id="columnsearch" style="font-size: 12px; font-family:Arial;"/></pre></div><span id="columnSearchOkCancelButtons" style="font-size: 12px; font-family:Arial"/>').appendTo('#popups' + containerId + '');

    //open column search window
    searchWindow.center().open();
    $('#columnSearchButtons').html('');
    $('#columnsearch').html('');
    $('#columnSearchButtons').append('<div style="margin-left:15px; line-height:45px;"><span>Select &nbsp;</span><input class="wrapperButton themeButton" type="button" value="All" id="btnAddAllColumns" />&nbsp;&nbsp;<input class="wrapperButton themeButton" type="button" value="None" id="btnRemoveAllColumns" /></span></div>');
    var k = 1;
    var columnsdata = angular.element(document.getElementById(containerId + 'ID')).scope().gridOptions.columnDefs
    for (var i = 0; i < columnsdata.length; i++) {
        if (columnsdata[i].displayName != "" && columnsdata[i].displayName != undefined) {
            if (selectedSearchColumns.indexOf(columnsdata[i].field) != -1) {
                $('#columnsearch').append("<div style='margin-left:10px;'><span><input type='checkbox' id='columnsearchCB" + k + "" + containerId + "' data-item='" + columnsdata[i].displayName + "' class='columnsearchCB" + containerId + "' checked>&nbsp;&nbsp;" + columnsdata[i].displayName + "<br /></input></span></div>");
            } else {
                $('#columnsearch').append("<div style='margin-left:10px;'><span><input type='checkbox' id='columnsearchCB" + k + "" + containerId + "' data-item='" + columnsdata[i].displayName + "' class='columnsearchCB" + containerId + "' >&nbsp;&nbsp;" + columnsdata[i].displayName + "<br /></input></span></div>");
            }

            k++;
        }
    };
    $('#columnSearchOkCancelButtons').html('');
    $('#columnSearchOkCancelButtons').append('<div><center><input class="wrapperButton themeButton" type="button" value="Ok" id="btnOkColumnSearch' + containerId + '" />&nbsp;&nbsp;<input class="wrapperButton themeButton" type="button" value="Cancel" id="btnCancelColumnSearch' + containerId + '" /></center></div>');
    // select all columns to search
    $(document).on('click', "#btnAddAllColumns", function() {
        for (var i = 1; i <= columnsdata.length; i++) {
            $("#columnsearchCB" + i + "" + containerId + "").each(function() {
                // checked all boxes true
                $(this)[0].checked = true;
                selectedSearchColumns.push(columnsdata[(i - 1)].field)
            });
        }
    });
    // reset selected search columns
    $(document).on('click', "#btnRemoveAllColumns", function() {
        selectedSearchColumns = [];
        for (var i = 1; i <= columnsdata.length; i++) {
            $("#columnsearchCB" + i + "" + containerId + "").each(function() {
                //unchecked all check boxes
                $(this)[0].checked = false;
            });
        }
    });

    //saving selected columns to search
    $(document).on('click', '#btnOkColumnSearch' + containerId + '', function() {
        selectedSearchColumns = [];
        for (var i = 1; i <= columnsdata.length; i++) {
            $("#columnsearchCB" + i + "" + containerId + "").each(function() {
                if ($(this)[0].checked == true) {
                    if (columnsdata[(i - 1)] && columnsdata[(i - 1)].field)
                        selectedSearchColumns.push(columnsdata[(i - 1)].field);
                }

            });
        }
        var localdata = getDatafromLocalStorage(containerId);
        localdata.selectedSearchColumns = selectedSearchColumns;
        angular.element(document.getElementById(containerId + 'ID')).scope().selectedSearchColumns = selectedSearchColumns;
        setDatatoLocalStorage(containerId, localdata);
        angular.element(document.getElementById(containerId + 'ID')).scope().applyGlobalSearch();
    });
    //closing sorting window
    $("#btnCancelColumnSearch" + containerId + "").click(function(e) {
        searchWindow.close();
        e.preventDefault();
    });
}

function printGrid(gridID, data) {
    var gridElement = $("#" + gridID);
    var win = window.open('', '', 'width=1000, height=500, scrollbars=yes, resizable=yes');
    var doc = win.document.open();
    var htmlStart =
        '<!DOCTYPE html>' +
        '<html>' +
        '<head>' +
        '<meta charset="utf-8" />' +
        '<title>Print grid data</title>' +
        '<link href="../../../../iapp/ui/styles/default/css/kendo.common.min.css" rel="stylesheet" /> ' +
        '<link href="../../../../iapp/ui/styles/default/css/bootstrap.min.css" rel="stylesheet" /> ' +
        '<style>' +
        'html { font: 11pt sans-serif; }' +
        '.k-grid, .k-grid-content { height: auto !important; }' +
        '.k-grid, .k-grid-content td {border-bottom: solid 1px !important }' +
        '.k-grid-toolbar, .k-grid-pager > .k-link { display: none; }' +
        '.k-link { text-decoration: none !important; }' +
        'div.k-grid-header .k-header, div.k-grid-footer { padding-right: 0px !important; background-color: #F5F5F5; font-weight:bold !important;}' +
        '.k-alt { background-color: #F5F5F5 !important; }' +
        '.k-print-header { background-color: #D7D7D7 !important; }' +
        'td { border:1px solid #000 }' +
        'tr[role="row"] { height:50px; }' +
        '<style>' +
        'html { font: 11pt sans-serif; }' +
        '</style>' +
        '</head>' +
        '<body style="overflow-y:scroll !important;overflow-x:scroll !important;">',
        htmlEnd =
        '</body>' +
        '</html>';
    //writing the records data on document
    // doc.write(htmlStart + gridElement.clone()[0].outerHTML + htmlEnd);
    var localdata = getDatafromLocalStorage(gridID);
    var fieldHeaders = angular.element(document.getElementById(gridID + 'ID')).scope().fieldHeaders;
     
    var printOptions = angular.element(document.getElementById(gridID + 'ID')).scope().otherOptions;
    var searchString = "";

    if(printOptions.printTitle){
        searchString += '<ol class="breadcrumb" style="margin-bottom:0px !important; font-weight:bold"><li style="text-decoration: underline;font-size: 18px;">'+printOptions.printTitle+'</li></ol>';
    }

    if(printOptions.printMetaData){
        printOptions = printOptions.printMetaData;
        if($.isArray(printOptions)){
            searchString += '<div style="padding:20px 10px;">';
            var index = 0;
            var fieldValue = "";
            for(var k=0; k<printOptions.length; k++){
                fieldValue = (printOptions[k].value == undefined) ? "" : printOptions[k].value;
                if(printOptions[k].name != undefined){
                    index ++;
                    searchString += '<label style="width:150px; text-align:right; margin-right:2px;">'+printOptions[k].name+' : </label><input style="margin-right: 25px;width:150px;" placeholder="'+fieldValue+'" disabled>';
                    if(index == 3){
                        searchString += '<br/><div style="width:100%;height:4px;"></div>';
                        index = 0;
                    }
                }
            }
            searchString += '</div>';
        }
    }
     
    var bodystr = searchString;
    
    bodystr += '<table role="grid" class="k-grid" style="height: auto; width: 100%;" cellspacing="0" cellpadding="0">';
    bodystr += "<tr>";
    var contentWidth = 0;
    if (localdata.columns) {
        for (var i = 0; i < localdata.columns.length; i++) {
            var columnTitle = localdata.columns[i].name;
            var idx = fieldHeaders[columnTitle].index;
            localdata.columns[idx].displayName = fieldHeaders[columnTitle].displayName;
            var wid = isNaN(localdata.columns[i].width) ? localdata.columns[i].width : localdata.columns[i].width + "px";
            if (localdata.columns[i].visible != false) {
                if (fieldHeaders[columnTitle].dataType == undefined || fieldHeaders[columnTitle].dataType != "checkbox") {
                    bodystr += "<td  data-role='sortable' style='text-align: center;width:" + wid + ";' class='k-print-header'><div>" + fieldHeaders[columnTitle].displayName + "</div></td>";
                    contentWidth += localdata.columns[i].width + 2;
                }
            }
        }
    }

    bodystr += "</tr>";
    for (var k = 0; k < data.length; k++) {
        var row = data[k];
        bodystr += "<tr role='row'>";
        if (localdata.columns) {
            for (var i = 0; i < localdata.columns.length; i++) {
                var value = row[localdata.columns[i].name] == undefined ? " " : row[localdata.columns[i].name];
                if (fieldHeaders[localdata.columns[i].name].dataType == "date") {
                    var datekey = localdata.columns[i].name.substr(0, localdata.columns[i].name.length - 2);
                    value = row[datekey] == undefined ? " " : row[datekey];
                }
                if(fieldHeaders[localdata.columns[i].name].dataType == "checkbox"){
                    continue;
                }
                var wid = isNaN(localdata.columns[i].width) ? localdata.columns[i].width : localdata.columns[i].width + "px";
                if (localdata.columns[i].visible != false) {
                    if (k % 2 == 0)
                        bodystr += "<td role='gridcell' style='width:" + wid + ";word-wrap: break-word;'>" + value + "</td>";
                    else
                        bodystr += "<td class='k-alt' role='gridcell' style='width:" + wid + ";word-wrap: break-word;'>" + value + "</td>";
                }
            }
        }
        bodystr += "</tr>";
    }
    bodystr += "</table>";
    bodystr += "</div></div>";
    var bodystrpre = '<div id="firstdiv" role="grid" class="k-grid k-widget k-secondary" style="width:' + (contentWidth + 20) + 'px;">'
    bodystrpre += '<div class="k-grid-content" data-role="virtualscrollable" style="width: ' + (contentWidth + 20) + 'px; overflow: hidden; padding-right: 18px; height: 204px;"><div class="k-virtual-scrollable-wrap">';
    bodystr = bodystrpre + bodystr

    doc.write(htmlStart + bodystr + htmlEnd);
    //closing the document
    doc.close();

    //opening the printer
    setTimeout(function() {
        win.print();
    }, 1000);
}

// open row details popup window function
function showRowDetailsWindowAngular(containerId, rowData) {
    // grid column details window
    columnDetailsWindow = $("#popups" + containerId + '').kendoWindow({
        title: "Details",
        modal: true,
        visible: false,
        resizable: false,
    }).data("kendoWindow");


  //columnDetails div contents clearing
    var localdata = getDatafromLocalStorage(containerId);
    var popupHeight = 100;
    popupHeight = popupHeight + localdata.columns.length*28;
    $('#popups' + containerId + '').html('');
    var winWid = "820px";
    try{
        if(window.innerWidth <= 820){
            winWid = "90%";
        }
    }catch(e){}

    if(popupHeight > 310){
        $("#popups" + containerId + '').parent().css({
            "width": winWid,
            "height": "340px"
        });
    }else if(popupHeight < 200){
        $("#popups" + containerId + '').parent().css({
            "width": winWid,
            "height": "220px"
        });
    }else{
        $("#popups" + containerId + '').parent().css({
            "width": winWid,
            "height": popupHeight+"px"
        });
    }
    $('<div class="clsColDetailsGrid"><div id="colDetailsGrid"><div style="background-color:#fff;"><input placeholder="Search..."  id="detailsSearch' + containerId + '" type="text" maxlength="64" class="form-control input-sm " style="width:200px;"/></div><div style="height:35px;padding-left:0px;padding-right:0px;" class="details-gridHeader col-sm-12"><div style="text-align:center;" class="col-sm-6">Name</div><div style="float:left;text-align:center;" class="col-sm-6">Value</div></div><div id="detailsGridCols"><div id="detailsTable' + containerId + '"class="table table-bordered table-striped detailsContainer"></div></div></div>').appendTo('#popups' + containerId + '');

    var fieldHeaders = angular.element(document.getElementById(containerId + 'ID')).scope().fieldHeaders;
    var list = [];
    if (localdata.columns) {
        for (var i = 0; i < localdata.columns.length; i++) {
            var key = localdata.columns[i].name;
            var value = rowData[0][key] == undefined ? "" : rowData[0][key];
            if (fieldHeaders[key] && fieldHeaders[key].dataType == "date") {
                var datekey = key.substr(0, key.length - 2)
                value = rowData[0][datekey] == undefined ? "" : rowData[0][datekey];
            }
            if (fieldHeaders[key] && fieldHeaders[key].dataType && (fieldHeaders[key].dataType).toLowerCase()  == "checkbox") {
                continue;
            }
            list.push({
                "title": fieldHeaders[key].displayName,
                "value": value
            })
            var str = "";
            str += "<div class='details-Cols col-xs-12' >";
            str += "<div class='col-xs-6'>" + fieldHeaders[key].displayName + "</div>";
            str += "<div class='col-xs-6 displayvalue"+ containerId+"'>" + "</div>";
            str += "</div>";
            $("#detailsTable" + containerId).append(str);
        };
        $(".displayvalue"+ containerId).each(function (index){
            try{
                this.textContent = list[index].value;
            }
            catch(e){
                this.textContent = "";
            }
        })
    }
    columnDetailsWindow.center().open();

    $("#detailsSearch" + containerId).off("keyup");
    $("#detailsSearch" + containerId).on("keyup", function(event) {
        var newArr = (angular.element(document.getElementById(containerId + 'ID')).scope().searchindetails(list, $("#detailsSearch" + containerId).val()));
        $("#detailsTable" + containerId).html("");
        for (var i = 0; i < newArr.length; i++) {
            var str = "";
            str += "<div class='details-Cols col-xs-12' >";
            str += "<div class='col-xs-6'>" + newArr[i].title + "</div>";
            str += "<div class='col-xs-6 displayvalue"+ containerId+"'>" + newArr[i].value + "</div>";
            str += "</div>";
            $("#detailsTable" + containerId).append(str);
        };
        $(".displayvalue"+ containerId).each(function (index){
            this.textContent = newArr[index].value;
        })
    });
    //columnDetailsWindow.open();
    try {
        setTimeout(function() {
            columnDetailsWindow.center();
        }, 100);
    } catch (e) {}

}
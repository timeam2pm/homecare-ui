var customAlert = new function(){
	return{
	info:function(alerttitle,alertmessage){
			$.when(kendo.ui.ExtAlertDialog.show({
				title: alerttitle, 
				message: alertmessage,
				icon: "k-ext-information" })
				)
		},
		error:function(alerttitle,alertmessage){
			$.when(kendo.ui.ExtAlertDialog.show({
				title: alerttitle, 
				 message: alertmessage,
				icon: "k-ext-error" })
			)
		},
		warning:function(alerttitle,alertmessage){
			$.when(kendo.ui.ExtAlertDialog.show({
				title: alerttitle, 
				 message: alertmessage,
				icon: "k-ext-warning" })
			)
		},
		confirm:function(alerttitle,alertmessage,confirmFunction){
			$.when(kendo.ui.ExtYesNoDialog.show({ 
				title: alerttitle, 
				message: alertmessage,
				icon: "k-ext-question" })
				).done(function (response) {
				      confirmFunction(response)
				});
     }
  };
};
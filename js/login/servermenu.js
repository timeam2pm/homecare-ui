var url = window.location.href;
var path = window.location.pathname;
callAPI();

$(document).ready(function(){
    // callBodyMinifyJs();
    buutonEvents();
});

function callAPI() {
    if(url.toLowerCase().includes("localhost:")) {
        document.addEventListener('contextmenu', event => event.preventDefault());

        loadAPi('../../js/common/common.js');
        loadAPi('../../js/login/login.js');
        //callBodyAPI();
        callBodyMinifyJs();
    }
    else if(url.toLowerCase().includes("stage.") || url.toLowerCase().includes("app.")){
        document.addEventListener('contextmenu', event => event.preventDefault());
        loadAPi('../../js/common/common.js');
        loadAPi('../../js/login/login.js');
        callBodyAPI();
    }
}
function callBodyAPI() {

    if (path.toLowerCase().includes("formdataedit")) {
        loadAPi('../../js/forms/formDataEdit.js');
    } else if (path.toLowerCase().includes("formdataentry")) {
        loadAPi('../../js/forms/formDataEntry.js');
    } else if (path.toLowerCase().includes("formdatalist")) {
        loadAPi('../../js/forms/formDataList.js');
    } else if (path.toLowerCase().includes("formdesign")) {
        loadAPi('../../js/forms/formdesigner.js');
        loadAPi('../../js/forms/formDesignerList.js');
    } else if (path.toLowerCase().includes("formdesignerdatalist")) {
        loadAPi('../../js/forms/formDesignerDataList.js');
    } else if (path.toLowerCase().includes("formdesignlist")) {
        loadAPi('../../js/forms/formDesignList.js');
    } else if (path.toLowerCase().includes("minitemplatedesignlist")) {
        loadAPi('../../js/forms/miniTemplateDesignList.js');
    } else if (path.toLowerCase().includes("templatedesignlist")) {
        loadAPi('../../js/forms/templateDesignList.js');
    } else if (path.toLowerCase().includes("careerinoutreport")) {
        loadAPi('../../js/reports/CareerInOutReport.tm.js');
    } else if (path.toLowerCase().includes("reportdesigner")) {
        loadAPi('../../js/reports/reportDesignerList.js');
    } else if (path.toLowerCase().includes("reportmaster")) {
        loadAPi('../../js/reports/reportList.tm.js');
    } else if (path.toLowerCase().includes("reportsearchserviceuser")) {
        loadAPi('../../js/reports/reportSearchPatient.tm.js');
    } else if (path.toLowerCase().includes("topicalapplication")) {
        loadAPi('../../js/reports/topicalApplication.tm.js');
    } else if (path.toLowerCase().includes("createpatient")) {
        loadAPi('../../js/patients/createPatient.tm.js');
    } else if (path.toLowerCase().includes("doctorlist")) {
        loadAPi('../../js/masters/doctorList.js');
    } else if (path.toLowerCase().includes("createdoctor")) {
        loadAPi('../../js/masters/createDoctor.js');
    } else if (path.toLowerCase().includes("adllist")) {
        loadAPi('../../js/masters/adlList.js');
    } else if (path.toLowerCase().includes("patienttemplatereport")) {
        loadAPi('../../js/reports/patientTemplateReport.tm.js');
    } else if (path.toLowerCase().includes("createappointment")) {
        loadAPi('../../js/patients/createAppointment.tm.js');
    } else if (path.toLowerCase().includes("createcopyappointment")) {
        loadAPi('../../js/masters/createCopyAppointment.js');
    } else if (path.toLowerCase().includes("copyappointment")) {
        loadAPi('../../js/masters/copyAppointment.js');
    } else if (path.toLowerCase().includes("taskreport")) {
        loadAPi('../../js/reports/taskReport.tm.js');
    } else if (path.toLowerCase().includes("providerappointmentlist")) {
        loadAPi('../../js/patients/providerAppointmentList.tm.js');
    } else if (path.toLowerCase().includes("appointmentreport")) {
        loadAPi('../../js/reports/appointmentReport.tm.js');
    } else if (path.toLowerCase().includes("viewappointment")) {
        loadAPi('../../js/masters/viewAppointment.js');
    } else if (path.toLowerCase().includes("letterlist")) {
        loadAPi('../../js/forms/letterList.js');
    } else if (path.toLowerCase().includes("letteredit")) {
        loadAPi('../../js/forms/letterEdit.js');
    } else if (path.toLowerCase().includes("letterentry")) {
        loadAPi('../../js/forms/letterEntry.js');
    } else if (path.toLowerCase().includes("securitypermissions")) {
        loadAPi('../../js/masters/securityPermissions.js');
    } else if (path.toLowerCase().includes("patientbilling")) {
        loadAPi('../../js/patients/patientBilling.tm.js');
    } else if (path.toLowerCase().includes("stafftimesheet")) {
        loadAPi('../../js/reports/staffTimesheet.tm.js');
    } else if (path.toLowerCase().includes("dashboard")) {
        loadAPi('../../js/masters/dashboard.js');
    } else if (path.toLowerCase().includes("serviceusercarereport")) {
        loadAPi('../../js/reports/serviceUserCareReport.tm.js');
    } else if (path.toLowerCase().includes("serviceuserreport")) {
        loadAPi('../../js/reports/serviceUserReport.tm.js');
    } else if (path.toLowerCase().includes("staffcarereport")) {
        loadAPi('../../js/reports/staffCareReport.tm.js');
    } else if (path.toLowerCase().includes("patientactivity")) {
        loadAPi('../../js/reports/patientActivity.tm.js');
    } else if (path.toLowerCase().includes("taskreportnew")) {
        loadAPi('../../js/reports/taskReportNew.tm.js');
    } else if (path.toLowerCase().includes("deleteappointmentlist")) {
        loadAPi('../../js/masters/deleteAppointmentList.js');
    } else if (path.toLowerCase().includes("billingratetype")) {
        loadAPi('../../js/billing/billingRateType.js');
    }
    else if (path.toLowerCase().includes("setuplist")) {
        loadAPi('../../js/setup/setupList.tm.js');
    }
}

function callBodyMinifyJs(){
    if(path.toLowerCase().includes("formdataedit")) {
        loadAPi('../../js/forms/formDataEdit.tm.js');
    }
    else if(path.toLowerCase().includes("formdataentry")) {
        loadAPi('../../js/forms/formDataEntry.tm.js');
    }
    else if(path.toLowerCase().includes("formdatalist")) {
        loadAPi('../../js/forms/formDataList.tm.js');
    }
    else if(path.toLowerCase().includes("formdesign")) {
        loadAPi('../../js/forms/formdesigner.tm.js');
        loadAPi('../../js/forms/formDesignerList.tm.js');
    }
    else if(path.toLowerCase().includes("formdesignerdatalist")) {
        loadAPi('../../js/forms/formDesignerDataList.tm.js');
    }
    else if(path.toLowerCase().includes("formdesignlist")) {
        loadAPi('../../js/forms/formDesignList.tm.js');
    }
    else if(path.toLowerCase().includes("minitemplatedesignlist")) {
        loadAPi('../../js/forms/miniTemplateDesignList.tm.js');
    }
    else if(path.toLowerCase().includes("templatedesignlist")) {
        loadAPi('../../js/forms/templateDesignList.tm.js');
    }
    else if(path.toLowerCase().includes("careerinoutreport")) {
        loadAPi('../../js/reports/CareerInOutReport..js');
    }
    else if(path.toLowerCase().includes("reportdesigner")) {
        loadAPi('../../js/reports/reportDesignerList.min.js');
    }
    else if(path.toLowerCase().includes("reportmaster")) {
        loadAPi('../../js/reports/reportList.js');
    }
    else if(path.toLowerCase().includes("reportsearchserviceuser")) {
        loadAPi('../../js/reports/reportSearchPatient.js');
    }
    else if(path.toLowerCase().includes("topicalapplication")) {
        loadAPi('../../js/reports/topicalApplication.js');
    }
    else if(path.toLowerCase().includes("createpatient")) {
        loadAPi('../../js/patients/createPatient.js');
    }
    else if(path.toLowerCase().includes("doctorlist")) {
        loadAPi('../../js/masters/doctorList.tm.js');
    }
    else if(path.toLowerCase().includes("createdoctor")) {
        loadAPi('../../js/masters/createDoctor.tm.js');
    }
    else if(path.toLowerCase().includes("adllist")) {
        loadAPi('../../js/masters/adlList.tm.js');
    }
    else if(path.toLowerCase().includes("patienttemplatereport")) {
        loadAPi('../../js/reports/patientTemplateReport.js');
    }
    else if(path.toLowerCase().includes("createappointment")) {
        loadAPi('../../js/patients/createAppointment.tm.js');
    }
    else if(path.toLowerCase().includes("createcopyappointment")) {
        loadAPi('../../js/masters/createCopyAppointment.tm.js');
    }
    else if(path.toLowerCase().includes("copyappointment")) {
        loadAPi('../../js/masters/copyAppointment.tm.js');
    }
    else if(path.toLowerCase().includes("taskreport")) {
        loadAPi('../../js/reports/taskReport.js');
    }
    else if(path.toLowerCase().includes("providerappointmentlist")) {
        loadAPi('../../js/patients/providerAppointmentList.js');
    }
    else if(path.toLowerCase().includes("appointmentreport")) {
        loadAPi('../../js/reports/appointmentReport.js');
    }
    else if(path.toLowerCase().includes("viewappointment")) {
        loadAPi('../../js/masters/viewAppointment.tm.js');
    }
    else if(path.toLowerCase().includes("letterlist")) {
        loadAPi('../../js/forms/letterList.tm.js');
    }
    else if(path.toLowerCase().includes("letteredit")) {
        loadAPi('../../js/forms/letterEdit.tm.js');
    }
    else if(path.toLowerCase().includes("letterentry")) {
        loadAPi('../../js/forms/letterEntry.tm.js');
    }
    else if(path.toLowerCase().includes("securitypermissions")) {
        loadAPi('../../js/masters/securityPermissions.tm.js');
    }
    else if(path.toLowerCase().includes("patientbilling")) {
        loadAPi('../../js/patients/patientBilling.js');
    }
    else if(path.toLowerCase().includes("stafftimesheet")) {
        loadAPi('../../js/reports/staffTimesheet.js');
    }
    else if(path.toLowerCase().includes("dashboard")) {
        loadAPi('../../js/masters/dashboard.tm.js');
    }
    else if(path.toLowerCase().includes("serviceusercarereport")) {
        loadAPi('../../js/reports/serviceUserCareReport.js');
    }
    else if(path.toLowerCase().includes("serviceuserreport")) {
        loadAPi('../../js/reports/serviceUserReport.js');
    }
    else if(path.toLowerCase().includes("staffcarereport")) {
        loadAPi('../../js/reports/staffCareReport.js');
    }
    else if(path.toLowerCase().includes("patientactivity")) {
        loadAPi('../../js/reports/patientActivity.js');
    }
    else if(path.toLowerCase().includes("taskreportnew")) {
        loadAPi('../../js/reports/taskReportNew.js');
    }
    else if(path.toLowerCase().includes("deleteappointmentlist")) {
        loadAPi('../../js/masters/deleteAppointmentList.tm.js');
    }
    else if(path.toLowerCase().includes("billingratetype")) {
        loadAPi('../../js/billing/billingRateType.tm.js');
    }
    else if (path.toLowerCase().includes("setuplist")) {
        loadAPi('../../js/setup/setupList.js');
    }
}

function loadAPi(file) {
    var head = document.getElementsByTagName('head')[0];
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = file;
    head.appendChild(script);
}



$(window).load(function(){
    var dt = new Date();
    //$("#tmZone").text(dt);
    console.log(sessionStorage.uName);
    // themeAPIChange();
    $("#txtWelcome").text("Welcome "+sessionStorage.uName);
    setInterval(
        function(){
            var dt = new Date();
            // callAPI();
            //$("#tmZone").text(dt);
        }, 1000);
});



function themeAPIChange(){
    getAjaxObject(ipAddress+"/homecare/settings/?id=1","GET",getThemeValue,onError);
}


function getThemeValue(dataObj){
    var tempCompType;
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.settings){
            if($.isArray(dataObj.response.settings)){
                tempCompType = dataObj.response.settings;
            }else{
                tempCompType.push(dataObj.response.settings);
            }
        }
        if(tempCompType.length > 0){
            sessionStorage.setItem("IsActivity", tempCompType[0].value); // for "1- Patient Activity, 2-Appointment Reason Activity"
            if(sessionStorage.IsActivity == 1){
                $("#addAppointmentTasks").css("display","none");
            }else if(tempCompType[0].value == 2){
                $("#addAppointmentTasks").css("display","");
            }
        }
    }
}

function onError(errorObj){
    console.log(errorObj);
}

function buutonEvents(){
    $("#aReset").off("click", onClickReset);
    $("#aReset").on("click", onClickReset);

    $("#aAbout").off("click", onClickAbout);
    $("#aAbout").on("click", onClickAbout);

    $("#aUpdateEmail").off("click", onClickReset);
    $("#aUpdateEmail").on("click", onClickReset);


    $("#btnUpdate").off("click");
    $("#btnUpdate").on("click",validatePassword);

    $('#txtPassword + .glyphicon').on('click', function() {
        $(this).toggleClass('glyphicon-eye-close').toggleClass('glyphicon-eye-open'); // toggle our classes for the eye icon
        var passwordInput = document.getElementById('txtPassword');
        if (passwordInput.type == 'password')
        {
            passwordInput.type='text';
            /*passStatus.className='fa fa-eye-slash';*/
        }
        else
        {
            passwordInput.type='password';
            /*passStatus.className='fa fa-eye';*/
        }
    });
}
function onClickReset(){
    $("#resetPassword").show();
    createUserObj();
    var name = sessionStorage.fullName;
    $("#lblUserName").html("User Name : " + name);
}

function onClickAbout(){
    $("#divAbout").show();
    var e = ipAddress + "/homecare/download/img/logowithtext/?access_token=" + sessionStorage.access_token + "&tenant=" + sessionStorage.tenant + "&" + Math.round(1e6 * Math.random());
    $("#imgLogo1").attr("src", e)
}


function onClickUpdate(){
    $("#updateEmail").show();
    /*createUserObj();*/
    var name = sessionStorage.fullName;
    $("#lblUserName").html("User Name : " + name);
}


function validatePassword() {
    var pwd = $("#txtPassword").val();
    var cpwd = $("#txtConfirmPassword").val();

    if(pwd == "" || cpwd== ""){
        customAlert.error("error","Please enter mandatory fields");
    }
    else if(pwd != cpwd){
        customAlert.error("error","Enter confirm password same as password");
    }else{
        onClickUpdate();
    }
}

function onClickUpdate(){
    var dataUrl = ipAddress+"/user/password/reset/";
    var reqObj = {};
    var idk = sessionStorage.universal_user_id;
    var pwd = $("#txtPassword").val();
    var oldpwd = $("#txtOldPassword").val();
    reqObj.universalUserId = Number(idk);
    reqObj.oldPassword = oldpwd;
    reqObj.newPassword = pwd;
    createAjaxObject(dataUrl,reqObj,"POST",onUpdateSuccess,onError);
}



(function($) {
    $.fn.extend({
        passwordValidation: function(_options, _callback, _confirmcallback) {
            //var _unicodeSpecialSet = "^\\x00-\\x1F\\x7F\\x80-\\x9F0-9A-Za-z"; //All chars other than above (and C0/C1)
            var CHARSETS = {
                upperCaseSet: "A-Z", 	//All UpperCase (Acii/Unicode)
                lowerCaseSet: "a-z", 	//All LowerCase (Acii/Unicode)
                digitSet: "0-9", 		//All digits (Acii/Unicode)
                specialSet: "\\x20-\\x2F\\x3A-\\x40\\x5B-\\x60\\x7B-\\x7E\\x80-\\xFF", //All Other printable Ascii
            }
            var _defaults = {
                minLength: 12,		  //Minimum Length of password
                minUpperCase: 2,	  //Minimum number of Upper Case Letters characters in password
                minLowerCase: 2,	  //Minimum number of Lower Case Letters characters in password
                minDigits: 2,		  //Minimum number of digits characters in password
                minSpecial: 2,		  //Minimum number of special characters in password
                maxRepeats: 5,		  //Maximum number of repeated alphanumeric characters in password dhgurAAAfjewd <- 3 A's
                maxConsecutive: 3,	  //Maximum number of alphanumeric characters from one set back to back
                noUpper: false,		  //Disallow Upper Case Lettera
                noLower: false,		  //Disallow Lower Case Letters
                noDigit: false,		  //Disallow Digits
                noSpecial: false,	  //Disallow Special Characters
                //NOT IMPLEMENTED YET allowUnicode: false,  //Switches Ascii Special Set out for Unicode Special Set
                failRepeats: true,    //Disallow user to have x number of repeated alphanumeric characters ex.. ..A..a..A.. <- fails if maxRepeats <= 3 CASE INSENSITIVE
                failConsecutive: true,//Disallow user to have x number of consecutive alphanumeric characters from any set ex.. abc <- fails if maxConsecutive <= 3
                confirmField: undefined
            };

            //Ensure parameters are correctly defined
            if($.isFunction(_options)) {
                if($.isFunction(_callback)) {
                    if($.isFunction(_confirmcallback)) {
                        console.log("Warning in passValidate: 3 or more callbacks were defined... First two will be used.");
                    }
                    _confirmcallback = _callback;
                }
                _callback = _options;
                _options = {};
            }

            //concatenate user options with _defaults
            _options = $.extend(_defaults, _options);
            if(_options.maxRepeats < 2) _options.maxRepeats = 2;

            function charsetToString() {
                return CHARSETS.upperCaseSet + CHARSETS.lowerCaseSet + CHARSETS.digitSet + CHARSETS.specialSet;
            }

            //GENERATE ALL REGEXs FOR EVERY CASE
            function buildPasswordRegex() {
                var cases = [];

                //if(_options.allowUnicode) CHARSETS.specialSet = _unicodeSpecialSet;
                if(_options.noUpper) 	cases.push({"regex": "(?=" + CHARSETS.upperCaseSet + ")",  																				"message": "Password can't contain an Upper Case Letter"});
                else 					cases.push({"regex": "(?=" + ("[" + CHARSETS.upperCaseSet + "][^" + CHARSETS.upperCaseSet + "]*").repeat(_options.minUpperCase) + ")", 	"message": "Password must contain at least " + _options.minUpperCase + " Upper Case Letters."});
                if(_options.noLower) 	cases.push({"regex": "(?=" + CHARSETS.lowerCaseSet + ")",  																				"message": "Password can't contain a Lower Case Letter"});
                else 					cases.push({"regex": "(?=" + ("[" + CHARSETS.lowerCaseSet + "][^" + CHARSETS.lowerCaseSet + "]*").repeat(_options.minLowerCase) + ")", 	"message": "Password must contain at least " + _options.minLowerCase + " Lower Case Letters."});
                if(_options.noDigit) 	cases.push({"regex": "(?=" + CHARSETS.digitSet + ")", 																					"message": "Password can't contain a Number"});
                else 					cases.push({"regex": "(?=" + ("[" + CHARSETS.digitSet + "][^" + CHARSETS.digitSet + "]*").repeat(_options.minDigits) + ")", 			"message": "Password must contain at least " + _options.minDigits + " Digits."});
                if(_options.noSpecial) 	cases.push({"regex": "(?=" + CHARSETS.specialSet + ")", 																				"message": "Password can't contain a Special Character"});
                else 					cases.push({"regex": "(?=" + ("[" + CHARSETS.specialSet + "][^" + CHARSETS.specialSet + "]*").repeat(_options.minSpecial) + ")", 		"message": "Password must contain at least " + _options.minSpecial + " Special Characters."});

                cases.push({"regex":"[" + charsetToString() + "]{" + _options.minLength + ",}", "message":"Password must contain at least " + _options.minLength + " characters."});

                return cases;
            }
            var _cases = buildPasswordRegex();

            var _element = this;
            var $confirmField = (_options.confirmField != undefined)? $(_options.confirmField): undefined;

            //Field validation on every captured event
            function validateField() {
                var failedCases = [];

                //Evaluate all verbose cases
                $.each(_cases, function(i, _case) {
                    if($(_element).val().search(new RegExp(_case.regex, "g")) == -1) {
                        failedCases.push(_case.message);
                    }
                });
                if(_options.failRepeats && $(_element).val().search(new RegExp("(.)" + (".*\\1").repeat(_options.maxRepeats - 1), "gi")) != -1) {
                    failedCases.push("Password can not contain " + _options.maxRepeats + " of the same character case insensitive.");
                }
                if(_options.failConsecutive && $(_element).val().search(new RegExp("(?=(.)" + ("\\1").repeat(_options.maxConsecutive) + ")", "g")) != -1) {
                    failedCases.push("Password can't contain the same character more than " + _options.maxConsecutive + " times in a row.");
                }

                //Determine if valid
                var validPassword = (failedCases.length == 0) && ($(_element).val().length >= _options.minLength);
                var fieldsMatch = true;
                if($confirmField != undefined) {
                    fieldsMatch = ($confirmField.val() == $(_element).val());
                }

                _callback(_element, validPassword, validPassword && fieldsMatch, failedCases);
            }

            //Add custom classes to fields
            this.each(function() {
                //Validate field if it is already filled
                if($(this).val()) {
                    validateField().apply(this);
                }

                $(this).toggleClass("jqPassField", true);
                if($confirmField != undefined) {
                    $confirmField.toggleClass("jqPassConfirmField", true);
                }
            });

            //Add event bindings to the password fields
            return this.each(function() {
                $(this).bind('keyup focus input proprtychange mouseup', validateField);
                if($confirmField != undefined) {
                    $confirmField.bind('keyup focus input proprtychange mouseup', validateField);
                }
            });
        }
    });
})(jQuery);

function createUserObj() {
    var self = this,
        $pwdElem = $("#txtPassword"),
        uId = '',
        tzArray = [],
        operation = "",
        ADD = "add",
        UPDATE = "update",
        finalCheck = false;
    this.init = function() {
        this.bindEvents();
        allowSplAlphaNumericWithoutSpace('txtPassword');
        allowSplAlphaNumericWithoutSpace('txtConfirmPassword');

    };

    this.bindEvents = function() {

        // $('.cmp-acc-text-input-container').find('input.cmp-acc-text-input[required="required"], input.required-input').on('focusout blur keyup', function() {
        // 	var $this = $(this);
        // 	$this.closest('.cmp-acc-text-input-container').find('.cmp-acc-info').removeClass('cmp-acc-banner cmp-acc-defaultposition').addClass('cmp-acc-hide');
        // 	if($this.attr('id') !== 'cmp-acc-password') {
        // 		errortypeobj = {
        // 			'background': "url(../../img/create-account/warning@44.png) no-repeat"
        // 		}
        // 		successtypeobj = {
        // 			'background': 'url(../../img/create-account/check@44.png) no-repeat'
        // 		}
        // 		if($this.val().length == 0) {
        // 			$this.closest('.cmp-acc-text-input-container').find('.cmp-acc-status').css(errortypeobj);
        // 			$this.addClass('cmp-acc-input-error');
        // 		} else if($this.attr('type') !== 'password') {
        // 			$this.closest('.cmp-acc-text-input-container').find('.cmp-acc-status').css(successtypeobj);
        // 			$this.removeClass('cmp-acc-input-error');
        // 		}
        // 	} else if ($this.attr('id') === 'cmp-acc-password') {
        // 		errortypeobj = {
        // 			'background': "url(../../img/create-account/lock-left-grey@3x.png) no-repeat"
        // 		}
        // 		successtypeobj = {
        // 			'background': 'url(../../img/create-account/lock-right-grey@3x.png) no-repeat'
        // 		}
        // 		if($this.val().length == 0) {
        // 			$this.closest('.cmp-acc-text-input-container').find('.cmp-acc-status').css(errortypeobj);
        // 			$this.addClass('cmp-acc-input-error');
        // 		} else {
        // 			$this.closest('.cmp-acc-text-input-container').find('.cmp-acc-status').css(successtypeobj);
        // 			$this.removeClass('cmp-acc-input-error');
        // 		}
        // 	}
        // }).on('focus', function() {
        // 	var $this = $(this);
        // 	$this.closest('.cmp-acc-text-input-container').find('.cmp-acc-info').removeClass('cmp-acc-hide').addClass('cmp-acc-banner cmp-acc-defaultposition');
        // });
        /*$('.cmp-acc-select-security').on('change', function() {
            $('.cmp-acc-select-security').find('option').show();
            $('.cmp-acc-select-security').each(function() {
                var $this = $(this);
                var selfValue = $this.val();
                if(selfValue) {
                    // $this.find('option').show();
                    $('.cmp-acc-select-security').not(this).find('option[value="'+selfValue+'"]').hide();
                    //$this.find('option[value="'+selfValue+'"]').show();
                }
            });
        });*/
        var pwrValidObj = {
            confirmField: "#txtConfirmPassword",
            minLength: 8,
            minUpperCase: 1,
            minLowerCase: 1,
            minDigits: 1,
            minSpecial: 1
        };
        var numberRegex = /\d/;
        var splRegex = /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;
        $pwdElem.passwordValidation(pwrValidObj, function(element, valid, match, failedCases) {
            $("#errors").html("<pre>" + failedCases.join("\n") + "</pre>");
            if(valid) {
                $('#cmp-acc-password-validator-message-success').removeClass('cmp-acc-hide');
                $('#cmp-acc-password-validator-message-fail').addClass('cmp-acc-hide');
                $pwdElem.removeClass('cmp-acc-input-error');
                $pwdElem.closest('.cmp-acc-text-input-container').find('.cmp-acc-status').css({'background': 'url(../../img/create-account/lock-success@3x.png) no-repeat'});
            }
            if(!valid) {
                $('#cmp-acc-password-validator-message-success').addClass('cmp-acc-hide');
                $('#cmp-acc-password-validator-message-fail').removeClass('cmp-acc-hide');
                $pwdElem.addClass('cmp-acc-input-error');
                $pwdElem.closest('.cmp-acc-text-input-container').find('.cmp-acc-status').css({'background': 'url(../../img/create-account/lock-right-grey@3x.png) no-repeat'});
            }
            if(valid && match) {
                $('#cmp-acc-error-confirm-password').addClass('cmp-acc-hide');
                $('#cmp-acc-confirm-password').removeClass('cmp-acc-input-error');
                $('#cmp-acc-confirm-password').closest('.cmp-acc-text-input-container').find('.cmp-acc-status').css({'background': 'url(../../img/create-account/check@44.png) no-repeat'});
            }
            if(!valid || !match) {
                $('#cmp-acc-error-confirm-password').removeClass('cmp-acc-hide');
                $('#cmp-acc-confirm-password').addClass('cmp-acc-input-error');
                $('#cmp-acc-confirm-password').closest('.cmp-acc-text-input-container').find('.cmp-acc-status').css({'background': 'url(../../img/create-account/warning@44.png) no-repeat'});
            }
            if($pwdElem.val().length >= 8) {
                $('#cmp-acc-password-min-num-of-chars').addClass('message-success');
                $pwdElem.removeClass('cmp-acc-input-error');
            } else {
                $('#cmp-acc-password-min-num-of-chars').removeClass('message-success');
                $pwdElem.addClass('cmp-acc-input-error');
            }
            if($pwdElem.val().match(/[a-z]/) && $pwdElem.val().match(/[A-Z]/)) {
                $('#cmp-acc-password-lower-upper-chars').addClass('message-success');
                $pwdElem.removeClass('cmp-acc-input-error');
            } else {
                $('#cmp-acc-password-lower-upper-chars').removeClass('message-success');
                $pwdElem.addClass('cmp-acc-input-error');
            }
            if(numberRegex.test($pwdElem.val())) {
                $('#cmp-acc-password-use-number').addClass('message-success');
                $pwdElem.removeClass('cmp-acc-input-error');
            } else {
                $('#cmp-acc-password-use-number').removeClass('message-success');
                $pwdElem.addClass('cmp-acc-input-error');
            }
            if(splRegex.test($pwdElem.val())) {
                $('#cmp-acc-password-use-symbol').addClass('message-success');
                $pwdElem.removeClass('cmp-acc-input-error');
            } else {
                $('#cmp-acc-password-use-symbol').removeClass('message-success');
                $pwdElem.addClass('cmp-acc-input-error');
            }
        });
        /*$('.cmp-acc-user-available').on('click', function(e) {
            e.preventDefault();
            var strUN = $("#cmp-acc-email").val();
            $('.user-valid-alert').remove();
            if (strUN != "" && validateEmail(strUN)) {
                var dataUrl = ipAddress + "/user/is-available/" + strUN + "/";
                getAjaxObject(dataUrl, "GET", getUserAvailable, onError);
            } else if(strUN != "" && !validateEmail(strUN)) {

            }
        });
        var checkFlag = true;
        $('#cmp-acc-sign-up-submit-btn').on('click', function(e) {
            e.preventDefault();
            checkFlag = true;
            $('.cmp-acc-text-input-container:visible').find('input[required="required"], input.required-input, select[required="required"]').each(function() {
                if($(this).val() === '' || $(this).hasClass('cmp-acc-input-error')) {
                    checkFlag = false;
                }
            });
            if (checkFlag) {
                hideShowContent();
            }
        });
        $('.cmp-acc-text-input-container').find('input[required="required"], input.required-input, select[required="required"]').on('change', function() {
            if($(this).val() === '' || $(this).hasClass('cmp-acc-input-error')) {
                checkFlag = false;
            }
        });
        $('#nextBtn, #nextBtnAfterDevices,  #nextBtnAfterVerifyCode').on('click', function(e) {
            e.preventDefault();
            var strUN = $("#cmp-acc-email").val();
            checkFlag = true;
            $('.cmp-acc-text-input-container:visible').find('input[required="required"], input.required-input, select[required="required"]').each(function() {
                if($(this).val() === '' || $(this).hasClass('cmp-acc-input-error')) {
                    checkFlag = false;
                }
            });
            if (checkFlag) {
                if($(this).attr('id') == 'nextBtnAfterVerifyCode') {
                    finalCheck = true;
                    var dataUrl = ipAddress + "/user/is-available/" + strUN + "/";
                    getAjaxObject(dataUrl, "GET", getUserCreate, onError);
                } else {
                    hideShowContent();
                }
            }
        });*/

    };
    this.init();
};





function onUpdateSuccess(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            customAlert.info("info", "User password updated successfully");
            $("#resetPassword").hide();
        }else{
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}

function closePopup(id) {
    $('#' + id).hide();
}


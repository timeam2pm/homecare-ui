var mySessionsWindow, sessionsOrg, saasOrgKey;

//var ipAddress = "http://192.168.0.106:8080";
//var ipAddress = "http://stage.timeam.com";

$(document).ready(function(){
	//$('#username').data('holder',$('#username').attr('placeholder'));

$('#username').focusin(function(){
   // $(this).attr('placeholder','');
});
$('#username').focusout(function(){
   // $(this).attr('placeholder',$(this).data('holder'));
});

//$('#password').data('holder',$('#password').attr('placeholder'));

$('#password').focusin(function(){
    //$(this).attr('placeholder','');
});
$('#password').focusout(function(){
    //$(this).attr('placeholder',$(this).data('holder'));
});
/*var hasTouch = window.navigator.msMaxTouchPoints || Modernizr.touch;
	if( !hasTouch){
		$('body').addClass('clsbody');
		$('.form-row input').addClass('clsinput');
		$('.form-row select').addClass('clsselect');
		$('#loginbtn').removeClass('form-row');
		$('#loginbtn').addClass('clsform-row');
		$('#reset-btn').removeClass('form-row');
		$('#reset-btn').addClass('clsform-row');
	}*/

});

$(window).resize(function(){
	adjustHeight();
});
var devModelWindowWrapper  = null;
function window_maximize(e){
	console.log("maximize");
}
function window_minimize(e){
	console.log("minimize");
}
$(window).load(function(){
	setTimeout(function(){
		//$("#username").val("");
		//$("#password").val("");
		adjustHeight();
		$("#username").addClass("userNameStyle");
		$(parent).off("loadImapImageEvent");	
		$(parent).on("loadImapImageEvent",function(e,obj){
			console.log(e);
		});	
		if(sessionStorage.uName){
			adjustFrameHeight();
			setTimeout(function(){
				if(sessionStorage.countryName == "India" || sessionStorage.countryName == "UK"){
					$("#addZip").text("Zip");
					$("addState").text("State");
				}else{
					$("#addZip").text("Postal Code");
					$("#addState").text("County");
				}
			},2000);
			$("#txtWelcome").text("Hello     "+"  "+sessionStorage.fullName);
			$("#videoFrame").attr('src', "../../html/patients/videoCallModule.html") ;
			adjustFrameHeight();
			minimizeVideFrame();
			//$("#divVideoPanelFrame").show();
			/*var chooseEntityWindow = $("#divVideoPanelFrame").kendoWindow({
				   title: "Video Call",content:"",modal: false,width: "80%",height:"80%",  actions: ["Minimize"],
				   position: {left: "10%",top: "10%"}
				}).data("kendoWindow");
			if(chooseEntityWindow){
				chooseEntityWindow.open();	
				chooseEntityWindow.minimize();
				
				var dialog = $("#divVideoPanelFrame").data("kendoWindow");
				dialog.bind("maximize", window_maximize);
				dialog.bind("minimize", window_minimize);
			}*/
			//chooseEntityWindow.setOptions({ width: "20%"});
			/*if(chooseEntityWindow){
				chooseEntityWindow.open();	
				chooseEntityWindow.minimize();
			}*/
			/* var myWindow = $("#divVideoWindow");
			 myWindow.kendoWindow({
                 width: "50%",height:"50%",
                 title: "Video Call",
                 content: "../../html/patients/videoCallModule.html",
                 actions: ["Minimize"],
                 close: onVideoWindowClose
             }).data("kendoWindow").center().open();*/
			//$("#videoFrame").attr('src', "../../html/patients/videoCallModule.html") ;
			//$("#videoFrame").attr('src', "../../img/login/ipad.jpg") ;
			
			/*var popW = "80%";
		    var popH = "80%";
		    var profileLbl;
		    devModelWindowWrapper = new kendoWindowWrapper();
		    profileLbl = "Video Call";
		    devModelWindowWrapper.openPageWindow("../../html/patients/videoCallModule.html", profileLbl, popW, popH, false, onVideoWindowClose);*/
			$("#lblPatient").trigger("click");//,onClickPatient);
			
			setTimeout(function(){
				//console.log(devModelWindowWrapper.getWindowContainer());
				//var dialog = $("#kendoWindowContainerWindw").data("kendoWindow");
				//dialog.bind("maximize", window_maximize);
				//dialog.bind("minimize", window_minimize);
				//
			})
		}
		 
		/*var dataObj = {};
		dataObj.username = 'user1';
		dataObj.password = 'pass';
		
		$.ajax({
			  type: "POST",
			  url: "http://123.201.150.184//login",
			  data: dataObj,
			  context: this,
				cache: false,
			  success: function( data, statusCode, jqXHR ){
				 console.log(data); 
			  },
			  error: function( jqXHR, textStatus, errorThrown ){
				  console.log(jqXHR);
			  },
			  contentType: "application/json",
			});*/
		
		//$("#username").css("background-color", "yellow");
		//$("#username").focus();
	},1000);
	
	$("#logout").off("click");
	$("#logout").on("click",onClickLogout);
	
	$("#txtDOB").kendoDatePicker();
	$("#gender").kendoComboBox();
	$("#seqQuetion1").kendoComboBox();
	$("#seqQuetion2").kendoComboBox();
	$("#seqQuetion3").kendoComboBox();
	$("#seqQuetion4").kendoComboBox();
	$("#seqQuetion5").kendoComboBox();
	
	//$("#btnUserVailable").off("click",onClickAvailable);
	//$("#btnUserVailable").on("click",onClickAvailable);
	
	//$("#btnCreate").off("click",onClickCreate);
	//$("#btnCreate").on("click",onClickCreate);
	
	//$("#btnCancel").off("click",onClickCancel);
	//$("#btnCancel").on("click",onClickCancel);
	
	$("#loginbtn").off("click",onClickLogin);
	$("#loginbtn").on("click",onClickLogin);
	
	$("#username").keyup(function(event){
	    if(event.keyCode == 13){
	        $("#loginbtn").trigger("click");
	    }
	});
	$("#password").keyup(function(event){
	    if(event.keyCode == 13){
	        $("#loginbtn").trigger("click");
	    }
	});
	
	//$("#spSignUp").off("click");
	//$("#spSignUp").on("click",onClickSignUp);
	
	$("#imgPatient").off("click");
	$("#imgPatient").on("click",onClickPatient);
	
	$("#imgMaster").off("click");
	$("#imgMaster").on("click",onClickMasters);
	
	$("#imgSchedule").off("click");
	$("#imgSchedule").on("click",onClickSchedule);
	
	$("#imgSetting").off("click");
	$("#imgSetting").on("click",onClickSetup);
	
	$("#imgDB").off("click");
	$("#imgDB").on("click",onClickDB);
	
	$("#lblReports").off("click");
	$("#lblReports").on("click",onClickReports);
	
	$("#imgVideoCall").off("click");
	$("#imgVideoCall").on("click",onClickVideoCall);
	
	/*$("#lblSettings").off("click");
	$("#lblSettings").on("click",onClickSettings);*/
	
	//$("#lblMasters").off("click");
	//$("#lblMasters").on("click",onClickMasters);
	
	/*$("#lblSetUp").off("click");
	$("#lblSetUp").on("click",onClickSetup);*/
	
	//$("#loginbtn").off("mouseover",onClickMouseOver);
	//$("#loginbtn").on("mouseover",onClickMouseOver);
	
	$("#loginbtn").off("mouseout");
	$("#loginbtn").on("mouseout",function(){
		$("#loginbtn").removeClass("loginHoverStyle");
	});
	
	
	$("#addADL").off("click");
	$("#addADL").on("click",onClickAddADL);
	
	$("#addAccount").off("click");
	$("#addAccount").on("click",onClickAddPatient);
	
	$("#addBillAccount").off("click");
	$("#addBillAccount").on("click",onClickAddBillAccount);
	
	$("#addFacility").off("click");
	$("#addFacility").on("click",onClickAddFacility);
	
	$("#addDoctor").off("click",onClickAddDoctor);
	$("#addDoctor").on("click",onClickAddDoctor);
	
	$("#addEmployer").off("click",onClickAddEmployer);
    $("#addEmployer").on("click",onClickAddEmployer);
	
    $("#createTable").off("click",onClickCreateTable);
    $("#createTable").on("click",onClickCreateTable);
    
    $("#addValues").off("click",onClickAddValues);
    $("#addValues").on("click",onClickAddValues);
	
	 $("#addContract").off("click",onClickAddContract);
    $("#addContract").on("click",onClickAddContract);
    
    $("#addCountry").off("click",onClickAddCountry);
    $("#addCountry").on("click",onClickAddCountry);
    
    $("#addState").off("click",onClickAddState);
    $("#addState").on("click",onClickAddState);
    
    $("#addZip").off("click",onClickAddZip);
    $("#addZip").on("click",onClickAddZip);
    
    $("#addMedication").off("click",onClickAddMedication);
    $("#addMedication").on("click",onClickAddMedication);
    
    $("#addCommunication").off("click",onClickAddCommunication);
    $("#addCommunication").on("click",onClickAddCommunication);
    
    $("#addPharmacy").off("click",onClickAddPharmacy);
    $("#addPharmacy").on("click",onClickAddPharmacy);
    
    $("#appointmentTypes").off("click",onClickAppTypes);
    $("#appointmentTypes").on("click",onClickAppTypes);
    
    $("#addResource").off("click");
    $("#addResource").on("click",onClickFileResource);
    
	
	var dt = new Date();
	$("#tmZone").text(dt); 
	
	setInterval(
			function(){ 
					var dt = new Date();
					$("#tmZone").text(dt); 
				}, 1000);
});
function onVideoWindowClose(){
	
}
function onClickAddPatient(){
	var myIframe = parent.frames["ptframe"];
	if(myIframe && myIframe.contentWindow){
		myIframe.contentWindow.onClickAddPatient();
	}
}
function onClickCreateTable(){
	collapseMasterMenu();
	$("#ptframe").attr('src', "../../html/masters/codeTableList.html") ;
}
function onClickAddValues(){
	collapseMasterMenu();
	$("#ptframe").attr('src', "../../html/masters/tableValueList.html") ;
}
function onClickAddContract(){
	collapseMasterMenu();
	$("#ptframe").attr('src', "../../html/masters/contract.html") ;
}
function onClickAddCountry(){
	collapseMasterMenu();
	$("#ptframe").attr('src', "../../html/masters/countryList.html") ;
}
function onClickAddState(){
	collapseMasterMenu();
	$("#ptframe").attr('src', "../../html/masters/stateList.html") ;
}
function onClickAddZip(){
	collapseMasterMenu();
	$("#ptframe").attr('src', "../../html/masters/zipList.html") ;
}
function onClickAddMedication(){
	collapseMasterMenu();
	//$("#ptframe").attr('src', "../../html/masters/createForm.html") ;
	$("#ptframe").attr('src', "../../html/masters/medication.html") ;
}
function onClickAddPharmacy(){
	collapseMasterMenu();
	//$("#ptframe").attr('src', "../../html/masters/createForm.html") ;
	$("#ptframe").attr('src', "../../html/masters/pharmacy.html") ;
}

function onClickAddCommunication(){
	collapseMasterMenu();
	//$("#ptframe").attr('src', "../../html/masters/createForm.html") ;
	$("#ptframe").attr('src', "../../html/masters/communication.html") ;
}
function onClickAppTypes(){
	collapseMasterMenu();
	//$("#ptframe").attr('src', "../../html/masters/createForm.html") ;
	$("#ptframe").attr('src', "../../html/masters/appointment.html") ;
}
function onClickFileResource(){
	collapseMasterMenu();
	$("#ptframe").attr('src', "../../html/masters/dietList.html") ;
}
function onClickLogout(){
	var myIframe = parent.frames["videoFrame"];
	if(myIframe && myIframe.contentWindow){
		//myIframe.contentWindow.onLogoutDisconnect();
	}
	
	sessionStorage.uName = "";
	top.location.href = "../../html/login/login.html"; 
}
window.onbeforeunload = function () {
	//onClickLogout();
}
function onClickAvailable(){
	//clientAPIManager.sendRequest( "AdministrationService", "login", false, "POST", $("#login-form").serialize(), loginSuccess, loginFail, true );
}
function showPatientPanel(){
	alert("Show Patient");
}
function onClickSignUp(e){
	e.stopPropagation();
	$("body").removeClass("bodyClass");
	$("body").addClass("signUpClass");
	
	$("#divLogin").hide();
	$("#signInBox").show();
}
function onClickCreate(){

	$("#signupContinueBox").show();
	$("#signInBox").hide();
}
function onClickCancel(e){
	e.stopPropagation();
	
	$("#divLogin").show();
	$("#signInBox").hide();
}
function onClickMouseOver(){
	var flag = loginValidation();
	if(flag){
		$("#loginbtn").addClass("loginHoverStyle");
	}else{
		$("#loginbtn").removeClass("loginHoverStyle");
	}
}
function loginValidation(){
	var flag = true;
		var uName = $("#username").val();
		var pass = $("#password").val();
		if(uName == ""){
			//customAlert.error("Error","Enter User name");
			flag = false;
			return false;
		}if(pass == ""){
			//customAlert.error("Error","Enter User password");
			flag = false;
			return false;
		}
	return flag;
}
function adjustHeight(){
	var defHeight = 520;
	 var cmpHeight = window.innerHeight - defHeight;
	//$("#divLoginBox").css({height:(cmpHeight)+'px'});
	
	//$(".loginbgClass").css({height:(cmpHeight+260)+'px'});
	//$(".loginSectionClass").css({height:(cmpHeight+260)+'px'});
	
	$("#l1").show();
	$("#l2").show();
	$("#l3").show();
	$("#l4").show();
}
function adjustFrameHeight(){
	var defHeight = 100;
	 var cmpHeight = window.innerHeight - defHeight;
	$("#ptframe").css({height:(cmpHeight)+'px'});
	$("#videoFrame").css({height:(cmpHeight)+'px'});
	
	setTimeout(function(){
		$("#divVideoPanelFrame").draggable();
	},2000)
	
}
function onClickLogin(e){
	e.stopPropagation();
	//submitForm(" ../../../app/login/html/servermenu.html");
	var flag = loginValidation();
	if(flag){
		var dataObj = {};
		dataObj.username = $("#username").val();
		dataObj.password = $("#password").val();
		var loginUrl = ipAddress+"/login";
		
		$.ajax({
			  type: "POST",
			  url: loginUrl,
			  data: JSON.stringify(dataObj),
			  context: this,
				cache: false,
			  success: function( data, statusCode, jqXHR ){
				  console.log(data);
				  if(data && data.response && data.response.status && data.response.status.code == 1){
					  var fullName = data.response.user.firstName+','+data.response.user.lastName;
					  sessionStorage.fullName = fullName;//$("#username").val();
					  sessionStorage.uName = $("#username").val();
					  sessionStorage.userId = data.response.user.id;
					  sessionStorage.userTypeId = data.response.user.userTypeId;
					  sessionStorage.countryName =  getCountryName();
					  
					  submitForm(" ../../html/login/servermenu.html");
								adjustFrameHeight();
								  setTimeout(function(){
									  $("#videoFrame").attr('src', "../../html/patients/videoCallModule.html") ;
									  $("#lblPatient").trigger("click");
								  },1000);
								  
					  $.getJSON('//freegeoip.net/json/?callback=?', function(dataObj) {
						  
						  var dataUrl = ipAddress+"/user/location";
						  
						  sessionStorage.country = dataObj.country_code;
						  dataObj.createdBy = sessionStorage.userId;
						  dataObj.countryCode = dataObj.country_code;
						  dataObj.countryName = dataObj.country_name;
						  dataObj.regionCode = dataObj.region_code;
						  dataObj.regionName = dataObj.region_name;
						  dataObj.timeZone = dataObj.time_zone;
						  dataObj.metroCode = dataObj.metro_code;
						  dataObj.zipCode = dataObj.zip_code;
						  sessionStorage.countryName = dataObj.country_name;
						  sessionStorage.countryName =  getCountryName();
						  console.log(dataObj);
							createAjaxObject(dataUrl,dataObj,"POST",function(dObj){
								submitForm(" ../../html/login/servermenu.html");
								adjustFrameHeight();
								  setTimeout(function(){
									  $("#videoFrame").attr('src', "../../html/patients/videoCallModule.html") ;
									  $("#lblPatient").trigger("click");
								  },1000);
							},function(){});
						 // console.log(data);
						  //console.log(JSON.stringify(data, null, 2));
						});
					  /*submitForm(" ../../html/login/servermenu.html");
					  setTimeout(function(){
						  $("#videoFrame").attr('src', "../../html/patients/videoCallModule.html") ;
						  $("#lblPatient").trigger("click");
					  },1000);*/
					  //onClickPatient()
					 
					 // submitForm(sessionStorage.landingPage);
				  }else{
					  customAlert.error("Error","Invalid user name/password");
				  }
				 
			  },
			  error: function( jqXHR, textStatus, errorThrown ){
				  console.log(jqXHR);
				  customAlert.error("Error","Invalid user name/password");
			  },
			  contentType: "application/json",
			});
	}
}
function loginSuccess(resObj){
	console.log(resObj);
}
function loginFail(resObj){
	console.log(resObj);
}
function xmlToString(xmlData) { 

    var xmlString;
    //IE
    if (window.ActiveXObject){
        xmlString = xmlData.xml;
    }
    // code for Mozilla, Firefox, Opera, etc.
    else{
        xmlString = (new XMLSerializer()).serializeToString(xmlData);
    }
    return xmlString;
} 
function removeLinkClasses(){
	$("#btnPatient").hide();
	$("#btnMaster").hide();
	$("#btnSetup").hide();
	$("#imgPatient").removeClass("dbSelection");
	$("#imgDB").removeClass("dbSelection");
	$("#imgMaster").removeClass("dbSelection");
	$("#imgVideoCall").removeClass("dbSelection");
	$("#lblReports").removeClass("dbSelection");
	$("#lblSettings").removeClass("dbSelection");
	$("#lblMasters").removeClass("dbSelection");
	$("#lblSetUp").removeClass("dbSelection");
}
function onClickPatient(){
	//$("#spnPtInfo").hide();
	//$("#divPatInfo").hide();
	removeLinkClasses();
	$("#btnPatient").show();
	$("#btnPatient").addClass("buttonSelection");
	$("#imgPatient").addClass("dbSelection");
	$("#ptframe").attr('src', "../../html/patients/patientList.html") ;
}
function onClickMasters(){
	removeLinkClasses();
	$("#btnMaster").show();
	$("#btnMaster").addClass("buttonSelection");
	$("#imgMaster").addClass("dbSelection");
}

function maximizeVideFrame(){
	try{
		var myIframe = parent.frames["videoFrame"];
		if(myIframe && myIframe.contentWindow){
			myIframe.contentWindow.showMinimizeButton();
			myIframe.contentWindow.hideMaximizeButton();
		}
	}catch(err){}
	
	$('#divVideoPanelFrame').css("right","0px");
}
function minimizeVideFrame(){
	try{
		var myIframe = parent.frames["videoFrame"];
		if(myIframe && myIframe.contentWindow){
			myIframe.contentWindow.hideMinimizeButton();
			myIframe.contentWindow.showMaximizeButton();
		}
	}catch(err){}
	
	var wdth = $('#divVideoPanelFrame').width();
	wdth = wdth-45;
	wdth = "-"+wdth+"px";
	$('#divVideoPanelFrame').css("right",wdth);
}
function executeVideoFunction(){
	console.log("execute video");
//	$("#divPatientFrame").removeClass("col-sm-12 col-md-12 col-lg-12 col-xs-12 noPadding");
	//$("#divPatientFrame").addClass("col-sm-10 col-md-10 col-lg-10 col-xs-10 noPadding");
	//$("#divVideoPanelFrame").show();
}
function executePatientFunction(){
	console.log("execute patient");
	//$("#divPatientFrame").addClass("col-sm-12 col-md-12 col-lg-12 col-xs-12 noPadding");
	//$("#divPatientFrame").removeClass("col-sm-10 col-md-10 col-lg-10 col-xs-10 noPadding");
	//$("#divVideoPanelFrame").hide();
}
function onClickVideoCall(){
	removeLinkClasses();
	$("#imgVideoCall").addClass("dbSelection");
	
	var myIframe = parent.frames["videoFrame"];
	if(myIframe && myIframe.contentWindow){
		myIframe.contentWindow.onClickCall();
	}
}
function onClickSchedule(){
	removeLinkClasses();
	$("#imgSchedule").addClass("buttonSelection");
	$("#imgSchedule").addClass("dbSelection");
	
	//$("#ptframe").attr('src', "../../html/patients/providerAppointmentList.html") ;
	$("#ptframe").attr('src', "../../html/patients/createAppointment.html") ;
}

function collapseMasterMenu(){
	$("#addMasterMenu").removeClass("in");
	$("#addMasterMenu").addClass("out");
}
function onClickAddADL(){
	//alert("ADL");
	collapseMasterMenu();
	$("#ptframe").attr('src', "../../html/masters/adlList.html") ;
}
function onClickAddBillAccount(){
	collapseMasterMenu();
	$("#ptframe").attr('src', "../../html/masters/accountList.html") ;
}
function onClickAddFacility(){
	collapseMasterMenu();
	$("#ptframe").attr('src', "../../html/masters/facilityList.html") ;
}
function onClickAddDoctor(){
	collapseMasterMenu();
	$("#ptframe").attr('src', "../../html/masters/doctorList.html") ;
}
function onClickAddEmployer(){
	collapseMasterMenu();
	$("#ptframe").attr('src', "../../html/masters/employerList.html") ;
}
function onClickSetup(){
	removeLinkClasses();
	$("#lblSetUp").addClass("activeStyle");
	$("#ptframe").attr('src', "../../html/setup/setupList.html") ;
}
function onClickDB(){
	removeLinkClasses();
	$("#imgDB").addClass("activeStyle");
	$("#ptframe").attr('src', "../../html/dashboard/dashboard.html") ;
}

function onClickReports(){
	removeLinkClasses();
	$("#lblReports").addClass("activeStyle");
	$("#ptframe").attr('src', "../../html/report/report.html") ;
}

function onClickSettings(){
	removeLinkClasses();
	$("#lblSettings").addClass("activeStyle");
	$("#ptframe").attr('src', "../../html/report/report.html") ;
}

/*function onClickMasters(){
	removeLinkClasses();
	$("#lblMasters").addClass("activeStyle");
	$("#ptframe").attr('src', "../../html/masters/masterList.html") ;
}*/
function submitFormData() {
	var fElement=document.createElement('FORM');
	fElement.name='myForm';
	fElement.method='POST';
	fElement.action=postURL;
		
	var inputElement=document.createElement('INPUT');
	inputElement.type='HIDDEN';
	inputElement.name='sessionId';
	inputElement.value=clientAPIManager.getSessionId();
		
	fElement.appendChild(inputElement);
	document.body.appendChild(fElement);
	fElement.submit();
	
	
}

var postURL = "";
function submitForm(posturl){
	postURL = posturl;
	submitFormData();
}
/*function  loginSuccess(data){
	var xmlResponse = $(data).find('Response');
	var status = $.trim( $(xmlResponse).find('Status').text() );
	var sessionID = $.trim( $(xmlResponse).find('SessionID').text() );
	var pwdResetReminder = $.trim( $(xmlResponse).find('PwdExpDescription').text() );
	
	sessionsOrg = $.trim( $(xmlResponse).find('Organization').text() );
	saasOrgKey = $.trim( $(xmlResponse).find('SaasOrgKey').text() );
	sessionStorage.userOrgKey = saasOrgKey;
	sessionStorage.userOrganization = sessionsOrg;
	sessionStorage.sessionID = sessionID;
	sessionStorage.lastLoginTime = $.trim( $(xmlResponse).find('LastLoginTime').text() );
	
	submitForm(sessionStorage.landingPage);
}
*/
function setOrgFocus() {
	$('#tiOrganization').focus();
}

/*function loginFail(data){
	var xmlResponse = $(data).find('Response');
	var reason = $.trim( $(xmlResponse).find('Reason').text() );
	var desc = $.trim( $(xmlResponse).find('Description').text() );
	$( "#organization" ).remove();
		$("#login-box p.success-msg")
			.text( $.trim( $(xmlResponse)
			.find('Description').text() ) )
			.show("slow")
			.css('color', 'red');
}*/

$(function() {

	sessionStorage.curPage="";
//	document.getElementById("username").focus();
	getRootPath();	

//	$.ajax({
//		url: "../conf/properties.xml",
//		beforeSend: function(){},
//		success: function( data, statusCode, jqXHR ){
//			var xmlResponse = $(data).find('Properties');
//			sessionStorage.frontController = $.trim( $(xmlResponse).find('ServerUrl').text() );
//			sessionStorage.landingPage = $.trim( $(xmlResponse).find('LandingPage').text() );
//			sessionStorage.organizationDisplayType = $.trim( $(xmlResponse).find('orgDispType').text() );
//			sessionStorage.showCopyRight = $.trim( $(xmlResponse).find('copyRights').text() );
//			sessionStorage.showLastLoginTime = $.trim( $(xmlResponse).find('lastLoginTime').text() );
//			sessionStorage.showUserAggrement = $.trim( $(xmlResponse).find('userAgreementRequired').text() );
//		},
//		error: function(){}
//	});
	
	
	$("#login-form").on("keyup", function(e){
		if (e.keyCode == 13) {
			//$("#loginbtn").trigger("click");
		}
	});
	
	$("#reset-btn").on("click", function(){
		$('#login-form')[0].reset();
		$("#login-box p.success-msg").hide();
	});
	
	

	
	
	$("#btnForgetPass").on("click",function(e){
		e.stopPropagation();
		$("#divLogin").hide();
		$("#forgotPassBox").show();
	});
	
	$("#btnForgotId").on("click",function(e){
		e.stopPropagation();
		$("#forgotPassBox").hide();
		$("#forgotIDBox").show();
	});
	
	$("#btnContinue").on("click",function(e){
		e.stopPropagation();
		$("#forgotPassBox").hide();
		$("#unlockBox").show();
	});
	
	$("#btnforgetIdContinue").on("click",function(e){
		e.stopPropagation();
		$("#forgotIDBox").hide();
		$("#login-box-outer").show();
		$("#divLogin").show();
		//$("#unlockBox").show();
	});
	$("#btnUnlockCancel").on("click",function(e){
		$("#unlockBox").hide();
		$("#login-box-outer").show();
		$("#divLogin").show();
	})
	$("#btnAcctCancel").on("click",function(e){
		$("#unlockAccountBox").hide();
		$("#login-box-outer").show();
		$("#divLogin").show();
	})
	$("#btnAcctCreate").on("click",function(e){
		$("#unlockAccountBox").hide();
		$("#login-box-outer").show();
		$("#divLogin").show();
	})
	$("#btnUnlockCreate").on("click",function(e){
		e.stopPropagation();
		$("#unlockBox").hide();
		$("#unlockAccountBox").show();
	})
	
	$("#btnSignupCompContinue").on("click",function(e){
		$("#signupContinueBox").hide();
		$("#login-box-outer").show();
		$("#divLogin").show();
	})
	
	/*$("#login-box-outer").on("click", "#loginbtn", function(e){
		e.stopPropagation();
		
		//$("#login-form").hide();
		//alert("error");
		//sessionStorage.userName = document.getElementById("username").value;
		submitForm(sessionStorage.landingPage);
		//clientAPIManager.sendRequest( "AdministrationService", "login", false, "POST", $("#login-form").serialize(), loginSuccess, loginFail, true );
	});*/

	/*$('#slides').superslides({
		hashchange: true,
	 	play: 2000,  
	});*/

	/*$('#slides').on('mouseenter', function() {
		$(this).superslides('stop');
	});
	$('#slides').on('mouseleave', function() {
		$(this).superslides('start');
	});*/

//	sliderContentChange();
	$(".slides-pagination a, .slides-arrow").on("click", function(){
			sliderContentChange()
	});

	$("#mobile-navigation").on("click", function(){
		var nav = $("#mobile-nav");
		if(nav.is(":visible"))
			nav.slideUp("slow", function(){
				$("#login-box-outer").fadeIn("slow")
			});
		else
			$("#login-box-outer").fadeOut("slow", function(){
				nav.slideDown("slow");
			});
	});
	/*$("#slides").swipe({
	
		swipeLeft:function(event, direction, distance, duration, fingerCount) {
			//This only fires when the user swipes left
			$("#next-arrow").trigger("click");
			
		},
		swipeRight:function(event, direction, distance, duration, fingerCount) {
			//This only fires when the user swipes right
			$("#prev-arrow").trigger("click");
		},
	});*/
	
	// setting redircting page url for logout where they login
	var tempURL = window.location.pathname.split("/").pop();
	if(tempURL == undefined)tempURL = "login.html";
	sessionStorage.redirectPageOnLogout	= "login.html";	
	
});


function getRootPath(){
	var loc = location.href;
	var endindex = GetSubstringIndex(loc,"/",4);
	endindex = endindex+1;
	var tempurl = loc.substr(0,endindex);

	sessionStorage.rootURL = tempurl;
}


function GetSubstringIndex(str, substring, n) {
    var times = 0, index = null;

    while (times < n && index !== -1) {
        index = str.indexOf(substring, index+1);
        times++;
    }

    return index;
}


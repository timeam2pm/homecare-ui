var mySessionsWindow, sessionsOrg, saasOrgKey, myLoginObj;



function setWIndowHeaderColor(e, o) {
    $("#kendoWindowContainer").parent().find(".k-header,.k-window-titlebar").css("cssText", "background-color:#" + e + " !important;margin-top:-30px"), $("#kendoWindowContainer").parent().find(".k-window-title").css("cssText", "color:#" + o + " !important;")
}

function setPopupWIndowHeaderColor(e, o) {
    $("#kendoWindowContainer1").parent().find(".k-header,.k-window-titlebar").css("cssText", "background-color:#" + e + " !important;margin-top:-30px"), $("#kendoWindowContainer1").parent().find(".k-window-title").css("cssText", "color:#" + o + " !important;")
}

function onClickAddPatient() {
    var e = parent.frames.ptframe;
    e && e.contentWindow && e.contentWindow.onClickAddPatient()
}

function onClickCreateTable() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/codeTableList.html")
}

function onClickAddValues() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/tableValueList.html")
}

function onClickLeaveApprove() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/leaveApprove.html")
}

function onClickCreateManager() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/createManagerGroups.html")
}

function onClickAddDisplay() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/displayLabelList.html")
}

function onClickAddUserMapping() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/userMapping.html")
}

function onClickAddStaffUserMapping() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/staffUserMapping.html")
}

function onClickSURegistration() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/timeamERP/html/master/suRegistration.html")
}

function onClickwecareBatch() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/timeamERP/html/master/batchNumber.html")
}

function onClickwecareCustomer() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../..//html/timeamERP/html/master/customer.html")
}

function onClickwecareEmp() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/timeamERP/html/master/employee.html")
}

function onClickwecareGST() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/timeamERP/html/gst/gst.html")
}

function onClickwecareItem() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/timeamERP/html/master/item.html")
}

function onClickwecareJournal() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/timeamERP/html/entry/journal.html")
}

function onClickwecareSetup() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/timeamERP/html/setup/setup.html")
}

function onClickwecareTransporter() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/timeamERP/html/master/transporter.html")
}


function onClickAddStaffAppointment() {
    collapseMasterMenu(),$("#ptframe").attr("src", "../../html/patients/deleteAppointmentLogin.html")
    // var profileLbl;
    // var devModelWindowWrapper = new kendoWindowWrapper();
    // profileLbl = "Appointment Login";
    // var popW = "50%";
    // var popH = "50%";
    // devModelWindowWrapper.openPageWindow("../../html/patients/deleteAppointmentLogin.html", profileLbl, popW, popH, false, onError);
    // parent.setPopupWIndowHeaderColor("0bc56f","FFF");
}

function onClickShowMediReport() {
    collapseReportMenu(), $("#ptframe").attr("src", "../../html/patients/medicationReport.html")
}

function onClickGeoReport() {
    collapseReportMenu(), $("#ptframe").attr("src", "../../html/patients/geoReport.html")
}

function onClickMentalReport() {
    collapseReportMenu(), $("#ptframe").attr("src", "../../html/patients/mentalAssessmentReport.html")
}

function onClickFoodReport() {
    collapseReportMenu(), $("#ptframe").attr("src", "../../html/patients/foodReport.html")
}

function onClickIronLogReport() {
    collapseReportMenu(), $("#ptframe").attr("src", "../../html/patients/ironLogReport.html")
}

function onClickCarerInOutReport() {
    collapseReportMenu(), $("#ptframe").attr("src", "../../html/reports/CareerInOutReport.html")
}

function onClickAppointmentReport() {
    collapseReportMenu(), $("#ptframe").attr("src", "../../html/reports/appointmentReport.html")

}

function onClickStaffOverlapReport() {
    collapseReportMenu(), $("#ptframe").attr("src", "../../html/reports/staffOverlapReport.html")
}

function onClickStaffTimesheet() {
    collapseReportMenu(), $("#ptframe").attr("src", "../../html/reports/staffTimesheet.html")
}

function onClickAppointmentDelayReport() {
    collapseReportMenu(), $("#ptframe").attr("src", "../../html/reports/appointmentDelayReport.html")
}

function onClickPatientInvoice2(){
    collapseReportMenu(), $("#ptframe").attr("src", "../../html/reports/invoicereport.html")
}

function onClickReportInvoices(){
    collapseReportMenu(), $("#ptframe").attr("src", "../../html/reports/reportInvoices.html")
}

function onClickDayDetailReport(){
    collapseReportMenu(), $("#ptframe").attr("src", "../../html/reports/dayDetailReport.html")
}

function onClickDayInvoiceReport(){
    collapseReportMenu(), $("#ptframe").attr("src", "../../html/reports/dayWiseReport.html")
}

function onClickVisitSummaryReport(){
    collapseReportMenu(), $("#ptframe").attr("src", "../../html/reports/visitSummaryReport.html")
}

function onClickCreateTemplate() {
    collapseReportMenu(), $("#ptframe").attr("src", "../../html/forms/templateDesignList.html")
}

function onClickCreateMiniTemplate() {
    collapseReportMenu(), $("#ptframe").attr("src", "../../html/forms/miniTemplateDesignList.html")
}

function onClickCreateFormData() {
    collapseReportMenu(), $("#ptframe").attr("src", "../../html/forms/formDataList.html")
}

function onClickLetter() {
    collapseReportMenu(), $("#ptframe").attr("src", "../../html/forms/letterDesignList.html")
}

function onClickLetterData() {
    collapseReportMenu(), $("#ptframe").attr("src", "../../html/forms/letterList.html")
}

function onClickManagers() {
    collapseManagerMenu(), $("#ptframe").attr("src", "../../html/masters/carerApproveList.html")
}

function onClickPatientActivity() {
    collapseReportMenu(), $("#imgPatient").addClass("activeStyle"), $("#ptframe").attr("src", "../../html/reports/patientActivity.html")
}

function onClickReportDesigner() {
    collapseReportMenu(), $("#ptframe").attr("src", "../../html/reports/reportDesigner.html"), sessionStorage.setItem("reportPageName", "designer")
}

function onClickReportViewer() {
    collapseReportMenu(), $("#ptframe").attr("src", "../../html/reports/reportDesigner.html"), sessionStorage.setItem("reportPageName", "viewer")
}

function onClickReportMaster() {
    collapseReportMenu(), $("#ptframe").attr("src", "../../html/reports/reportMaster.html")
}

function onClickFormDesign() {
    collapseReportMenu(), $("#ptframe").attr("src", "../../html/forms/formDesign.html")
}

function onClickDailyFluidReport() {
    collapseReportMenu(), $("#ptframe").attr("src", "../../html/patients/dailyFluidReport.html")
}

function onClickTaskReport() {
    collapseReportMenu(), $("#ptframe").attr("src", "../../html/reports/taskReport.html")
}

function onClickTaskViewer() {
    collapseReportMenu(), $("#ptframe").attr("src", "../../html/reports/taskReportNew.html")
}

function onClickStaffCareReprot() {
    collapseReportMenu(), $("#ptframe").attr("src", "../../html/reports/staffCareReport.html")
}

function onClickSUCareReport() {
    collapseReportMenu(), $("#ptframe").attr("src", "../../html/reports/serviceUserCareReport.html")
}

function onClickSUReport() {
    collapseReportMenu(), $("#ptframe").attr("src", "../../html/reports/serviceUserReport.html")
}




function onClickStaffTimeSheet() {
    collapseReportMenu(), $("#ptframe").attr("src", "../../html/reports/staffTimeSheet.html")
}


function onClickTopicalApplication() {
    collapseReportMenu(), $("#ptframe").attr("src", "../../html/reports/topicalApplication.html")
}

function collapsePatientActivityMenu() {
    $("#btnReport").show(), $("#addReportMenu").removeClass("in")
}

function onClickRepositionChart() {
    collapseReportMenu(), $("#ptframe").attr("src", "../../html/patients/repositionReport.html")
}

function onClickActType() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/activityTypeList.html")
}

function onClickBodyPart() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/bodyPartsList.html")
}

function onClickCommunication() {
    sessionStorage.setItem("sessionMasterCommValue", "country");
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/addCountry.html")
    
}

function onClickCommCounty() {
    sessionStorage.setItem("sessionMasterCommValue", "state");
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/addCountry.html")
    
}

function onClickCommCity() {
    sessionStorage.setItem("sessionMasterCommValue", "city");
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/addCountry.html")
    
}

function onClickAddCountry() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/countryList.html")
}

function onClickAddState() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/stateList.html")
}

function onClickAddZip() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/zipList.html")
}

function onClickFileResource() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/userEnableList.html")
}

function onClickViewAppointments() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/viewAppointment.html")
}

function onClickDB() {
    hideTopBar(), removeLinkClasses(), $("#imgDB").addClass("dbSelection"), $("#ptframe").attr("src", "../../html/masters/dashboard.html")
}

function onClickLogout() {
    var e = sessionStorage.loginHisID;
    if (e) {
        var o = ipAddress + "/homecare/user-login-history/",
            t = JSON.parse(sessionStorage.loginHisObj);
        t.token = sessionStorage.access_token;
        t.refresh_token = sessionStorage.refresh_token;
        t.id = e, t.logoutTime = (new Date).getTime(), t.modifiedBy = sessionStorage.userId, createAjaxObject(o, t, "PUT", function(e) {
            console.log(e), sessionStorage.uName = "", top.location.href = "../../html/login/login.html"
        })
    } else sessionStorage.uName = "", top.location.href = "../../html/login/login.html"
}

function onClickAvailable() {}

function showPatientPanel() {
    alert("Show Patient")
}

function onClickSignUp(e) {
    e.stopPropagation(), $("body").removeClass("bodyClass"), $("body").addClass("signUpClass"), $("#divLogin").hide(), $("#signInBox").show()
}

function onClickCreate() {
    $("#signupContinueBox").show(), $("#signInBox").hide()
}

function onClickCancel(e) {
    e.stopPropagation(), $("#divLogin").show(), $("#signInBox").hide()
}

function onClickMouseOver() {
    loginValidation() ? $("#loginbtn").addClass("loginHoverStyle") : $("#loginbtn").removeClass("loginHoverStyle")
}

function loginValidation() {
    var e = !0,
        o = $("#username").val(),
        t = $("#password").val();
    return "" == o ? (e = !1, !1) : "" == t ? (e = !1, !1) : e
}

function adjustHeight() {
    window.innerHeight
}

function displayHeaderMenu() {
    $(window).width() > 767 ? $("#divSignIn ul").show() : $("#divSignIn ul").hide()
}

function maximizeVideFrame() {
    try {
        var e = parent.frames.videoFrame;
        e && e.contentWindow && (e.contentWindow.showMinimizeButton(), e.contentWindow.hideMaximizeButton())
    } catch (e) {}
    $("#divVideoPanelFrame").css("right", "0px")
}

function minimizeVideFrame() {
    try {
        var e = parent.frames.videoFrame;
        e && e.contentWindow && (e.contentWindow.hideMinimizeButton(), e.contentWindow.showMaximizeButton())
    } catch (e) {}
    var o = $("#divVideoPanelFrame").width();
    o = "-" + (o -= 45) + "px", $("#divVideoPanelFrame").css("right", o)
}

function adjustFrameHeight() {
    var e = window.innerHeight - 100;
    $("#ptframe").css({
        height: e + "px"
    }), $("#videoFrame").css({
        height: e + "px"
    })
}

function onClickLogin(e) {

    if (e.stopPropagation(), loginValidation()) {

        var o = {},
            t = $("#username").val(),
            n = $("#password").val();
        o.username = t, o.password = n, o.grant_type = "password",o.accessType = 1;
        var i = ipAddress + "/login";
        $.ajax({
            type: "POST",
            url: i,
            data: JSON.stringify(o),
            context: this,
            cache: !1,
            success: function(e, o, n) {
                if (console.log(e), myLoginObj = e, e && e.response && e.response.status && 1 == e.response.status.code) {
                    localStorage.setItem("userName", t), sessionStorage.access_token = e.response.token.access_token;
                    sessionStorage.password_expired = e.response.token.password_expired;


                    var i = Number(e.response.token.expires_in);
                    i -= 20, i *= 1e3, sessionStorage.expires_in = i, sessionStorage.refresh_token = e.response.token.refresh_token, sessionStorage.universal_user_id = e.response.token.universal_user_id, sessionStorage.tenant = e.response.token.tenant, sessionStorage.uName = $("#username").val(), sessionStorage.countryName = getCountryName();
                    var l = new Date;
                    sessionStorage.tm = l.getTime(), sessionStorage.pt = l.getTime(), sessionStorage.clientTypeId = "2", "2" == sessionStorage.clientTypeId && $("#lblPtLbl").text("Service User :"), $.getJSON("https://api.ipify.org/?format=json", function(e) {
                        ipAddress;
                        var o = e.ip;
                        // $.getJSON("https://ipinfo.io/", function(e) {
                        // $.getJSON("http://ip-api.com/json/24.48.0.1", function(e) {
                        console.log(e);

                        var n = {};
                        // n.uid = sessionStorage.universal_user_id, n.ip = o, n.city = "Hyderabad", n.deviceOs = "IOS", n.regionName = "Telangana", n.isp = "act", n.latitude = "1.222", n.type = "ipv4", n.deviceName = "del", n.clientTime = "12.22.222", n.hostname = "apt.xy.com", n.regionCode = "TS", n.loginTime = (new Date).getTime(), n.timeZoneCode = "IST", n.countryCode = "IN", n.gmtOffset = "30", n.browserVersion = "1.2.2", n.localIp = "2.2.2.2", n.browserName = "chrome", n.threatLevel = "low", n.continentCode = "AS", n.longitude = "2.222", n.zip = "500055", n.ip = "1.1.1.1", n.timeZone = "Aisa/", n.logoutTime = null, n.deviceCategory = "web", n.daylightSaving = 0, n.deviceModel = "123", n.countryName = "India", n.continentName = "Asia", n.asn = "asn", n.createdBy = sessionStorage.userId, n.isActive = 1, n.isDeleted = 0;
                        n.token = sessionStorage.access_token;
                        n.refresh_token = sessionStorage.refresh_token;
                        n.uid = sessionStorage.universal_user_id, n.ip = o, n.city = e.city, n.deviceOs = "IOS", n.regionName = "Telangana", n.isp = "act", n.latitude = "1.222", n.type = "ipv4", n.deviceName = "del", n.clientTime = "12.22.222", n.hostname = "apt.xy.com", n.regionCode = "TS", n.loginTime = (new Date).getTime(), n.timeZoneCode = "IST", n.countryCode = "IN", n.gmtOffset = "30", n.browserVersion = "1.2.2", n.localIp = "2.2.2.2", n.browserName = "chrome", n.threatLevel = "low", n.continentCode = "AS", n.longitude = "2.222", n.zip = "500055", n.ip = "1.1.1.1", n.timeZone = "Aisa/", n.logoutTime = null, n.deviceCategory = "web", n.daylightSaving = 0, n.deviceModel = "123", n.countryName = "India", n.continentName = "Asia", n.asn = "asn", n.createdBy = sessionStorage.userId, n.isActive = 1, n.isDeleted = 0;
                        var i = ipAddress + "/homecare/user-login-history/";  sessionStorage.loginHisObj = JSON.stringify(n), createAjaxObject(i, n, "POST", function(e) {
                            console.log(e);
                            try {
                                e.response["user-login-history"].id && (sessionStorage.loginHisID = e.response["user-login-history"].id)
                            } catch (e) {}
                            getAjaxObject(ipAddress + "/homecare/tenant-users/?is-active=1&is-deleted=0&universal-user-id=" + sessionStorage.universal_user_id, "GET", function(e) {
                                if (console.log(e), e && e.response && e.response.status && e.response.tenantUsers && "1" == e.response.status.code) {
                                    var o = e.response.tenantUsers[0],
                                        n = o.firstName + "," + o.lastName;
                                    // sessionStorage.countryName = getCountyNameByTZ(o.timeZone);

                                    // if(o.userTypeId != 700) {
                                    //     $("#addLeaveApprove",parent.document).css("display","none");
                                    //     $("#addLeaveApprove").css("display","none");
                                    // }
                                    sessionStorage.userId = o.id, sessionStorage.userTypeId = o.userTypeId, sessionStorage.fullName = n;

                                    if(o.userTypeId != 701) {
                                        if(sessionStorage.password_expired == 'true'){
                                            $("#resetPassword").show();
                                            createUserObj();
                                            var name = sessionStorage.fullName;
                                            $("#lblUserName").html("User Name : " + name);
                                            $("#username").val("");
                                            $("#password").val("");
                                        }
                                        else{
                                            800 == sessionStorage.userTypeId ? submitForm(" ../../html/login/councilHomePage.html") : submitForm("invoice" == t ? " ../../html/login/invoiceHomePage.html" : " ../../html/login/servermenu.html"), adjustFrameHeight()

                                        }
                                        localStorage.setItem("pageName", "");

                                    }
                                    else{
                                        submitForm(" ../../html/forms/formDataList.html"), adjustFrameHeight()
                                        localStorage.setItem("pageName", "form");
                                    }



                                }
                            }, function() {})
                        }, function() {})
                        // })
                    }), setTimeout(function() {}, 1e3)
                } else customAlert.error("Error", "Invalid user name/password")
            },
            error: function(e, o, t) {
                console.log(e), customAlert.error("Error", "Invalid user name/password")
            },
            contentType: "application/json"
        })
    }
}

function loginSuccess(e) {
    console.log(e)
}

function loginFail(e) {
    console.log(e)
}

function xmlToString(e) {
    return window.ActiveXObject ? e.xml : (new XMLSerializer).serializeToString(e)
}

function removeLinkClasses() {
    $("#btnPatient").hide(), $("#btnMaster").hide(), $("#btnBilling").hide(), $("#btnReports").hide(), $("#btnManagers").hide(), $("#imgDB").removeClass("dbSelection"), $("#imgPatient").removeClass("dbSelection"), $("#imgMaster").removeClass("dbSelection"), $("#lblReports").removeClass("dbSelection"), $("#lblSettings").removeClass("dbSelection"), $("#lblMasters").removeClass("dbSelection"), $("#lblSetUp").removeClass("dbSelection"), $("#imgProvider").removeClass("dbSelection"), $("#imgSchedule").removeClass("dbSelection"), $("#imgReport").removeClass("dbSelection"), $("#imgSetting").removeClass("dbSelection"), $("#imgVideoCall").removeClass("dbSelection"), $("#imgManager").removeClass("dbSelection")
}

function onClickImgCareHome() {
    removeLinkClasses(), $("#imgCareHome").addClass("dbSelection"), $("#ptframe").attr("src", "../../html/carehome/carehome.html")
}

function hideTopBar() {
    $("#divIdMain").hide(), $("#userImg").hide(), $("#divPatInfo").hide(), $("#divMain").show(), $("#lblTitleName", parent.document).html(""), $("#divTitleName").show()
}

function onClickPatient() {
    $("#ptframe",parent.document).css("padding","100px 0 50px 0");
    hideTopBar(), removeLinkClasses(), /*$("#btnPatient").show(),*/ $("#imgPatient").addClass("dbSelection"), $("#ptframe").attr("src", "../../html/patients/patientList.html"),$("#pnlPatient",parent.document).css("display","block");
}

function onClickProvider() {
    hideTopBar(), removeLinkClasses(), $("#btnPatient").hide(), $("#imgProvider").addClass("dbSelection"), $("#ptframe").attr("src", "../../html/masters/doctorList.html")
}

function onClickManagerProvider() {
    hideTopBar(), removeLinkClasses(), $("#imgManager").addClass("dbSelection"), $("#btnManagers").show()
}

function onClickMasters() {
    $("#ptframe",parent.document).css("padding","50px 0 50px 0");
    if(Number(sessionStorage.userTypeId) != 700){
        $("#addLeaveApprove").hide();
    }
    collapseReportMenu(),$("#btnMaster").show(), $("#imgMaster").addClass("dbSelection"),  $("#addMasterMenu").removeClass("in"), $("#addMasterMenu").toggleClass("out collapse in")
}

function onClickReports() {
    $("#ptframe",parent.document).css("padding","50px 0 50px 0");
    collapseMasterMenu(),removeLinkClasses(), $("#btnReports").hide(), $("#imgReport").addClass("dbSelection"),$("#addReporttMenu").removeClass("in"),$("#addReporttMenu").toggleClass("out collapse in")
}

function onClickSchedule() {
    hideTopBar(), removeLinkClasses(), $("#imgSchedule").addClass("dbSelection"), $("#ptframe").attr("src", "../../html/patients/createAppointment.html")
}

function collapseMasterMenu() {
    $("#addMasterMenu").removeClass("in"), $("#addMasterMenu").addClass("out"), $("#addBillingMenu").addClass("out")
}

function collapseBillingMenu() {
    $("#addBillingMenu").removeClass("in"), $("#addBillingMenu").addClass("out"), $("#addMasterMenu").addClass("out")
}

function collapseReportMenu() {
    $("#addReporttMenu").removeClass("in"), $("#addReporttMenu").addClass("out"), $("#sub_categoria_4").removeClass("in"), $("#sub_categoria_4").addClass("out")
}

function collapseManagerMenu() {
    $("#addManagerMenu").removeClass("in"), $("#addReporttMenu").addClass("out")
}

function onClickAddADL() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/adlList.html")
}

function onClickAddDocument() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/documentType.html")
}

function onClickAddUnAvailable() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/unallocatedappointments.html")
}

function onClickAddCopyAppointment() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/copyAppointment.html")
}

function onClickAddEmployment() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/employmentType.html")
}
function onClickAddRateType() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/billing/billingRateType.html")
}

function onClickAddEntity() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/entityRelation.html")
}
function onClickAddNoteType() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/noteType.html")
}
function onClickAddNoteStatus() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/noteStatus.html")
}
function onClickAddAlertType() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/alertType.html")
}
function onClickAddAlertStatus() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/alertStatus.html")
}
function onClickAddAlertGeneratedBy() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/alertGeneratedBy.html")
}

function onClickPharmacy() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/pharmacy.html")
}

function onClickAddBillAccount() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/accountList.html")
}

function onClickAddFacility() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/facilityList.html")
}

function onClickAddDoctor() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/doctorList.html")
}


function onClickAddVitals() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/vitalList.html")
}

function onClickMedication() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/medication.html")
    sessionStorage.setItem("sessionMedicationValue", "form");
}

function onClickStrengthMedication() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/medication.html")
    sessionStorage.setItem("sessionMedicationValue", "strength");
}

function onClickRouteMedication() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/medication.html")
    sessionStorage.setItem("sessionMedicationValue", "route");
}

function onClickFrequencyMedication() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/medication.html")
    sessionStorage.setItem("sessionMedicationValue", "frequency");
}

function onClickInstrucionMedication() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/medication.html")
    sessionStorage.setItem("sessionMedicationValue", "instruction");
}

function onClickActionMedication() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/medication.html")
    sessionStorage.setItem("sessionMedicationValue", "action");
}

function onClickStatusMedication() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/medication.html")
    sessionStorage.setItem("sessionMedicationValue", "status");
}

function onClickTransmissionMedication() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/medication.html")
    sessionStorage.setItem("sessionMedicationValue", "transmission");
}

function onClickRxFormMedication() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/medication.html")
    sessionStorage.setItem("sessionMedicationValue", "rxform");
}

function onClickPrescription() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/prescription.html")
}

function onClickMasterPatient() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/patient.html")
}

function onClickMedicalConditionDiet() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/medicalCondition.html")
    sessionStorage.setItem("sessionMedicalConditionValue", "diet");
}

function onClickMedicalConditionIllness() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/medicalCondition.html")
    sessionStorage.setItem("sessionMedicalConditionValue", "illness");
}

function onClickAppointmentReasonTasks() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/attachLinkTasks.html")
}

function onClickAddSecurityPermissions() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/securityPermissions.html")
}

function onClickMedicalConditionAllergy() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/medicalCondition.html")
    sessionStorage.setItem("sessionMedicalConditionValue", "allergy");
}

function onClickMedicalConditionExercise() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/medicalCondition.html")
    sessionStorage.setItem("sessionMedicalConditionValue", "exercise");
}

function onClickAppointment() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/appointment.html");
    sessionStorage.setItem("sessionAppointmentValue", "status");
}

function onClickAppointmentReason() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/appointment.html");
    sessionStorage.setItem("sessionAppointmentValue", "reason");
}

function onClickAddEmployer() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/masters/employerList.html")
}

function onClickSetup() {
    hideTopBar(), "2" == sessionStorage.clientTypeId && $("#addUserPatientMap").text("Service User User Mapping"), removeLinkClasses(), $("#imgSetting").addClass("dbSelection"), $("#ptframe").attr("src", "../../html/setup/setupList.html")
}

function onClickVideoCall() {
    removeLinkClasses(), $("#imgVideoCall").addClass("dbSelection");
    $("#divVideoPanelFrame",parent.document).css("display","block !important");
    var e = parent.frames.videoFrame;
    e && e.contentWindow && e.contentWindow.onClickCall()
}

function onClickSettings() {
    removeLinkClasses(), $("#lblSettings").addClass("activeStyle"), $("#ptframe").attr("src", "../../html/report/report.html")
}

function submitFormData() {
    var e = document.createElement("FORM");
    e.name = "myForm", e.method = "POST", e.action = postURL;
    var o = document.createElement("INPUT");
    o.type = "HIDDEN", o.name = "sessionId", o.value = clientAPIManager.getSessionId(), e.appendChild(o), document.body.appendChild(e), e.submit()
}
$(document).ready(function() {
    // themeAPIChange();

    if(sessionStorage.userId == 298 || sessionStorage.userId == 229){
        $("#addSecurityPermissions",parent.document).css("display","block");
        $("#wecaremenu",parent.document).css("display","block");
    }
    else{
        $("#addSecurityPermissions",parent.document).css("display","none");
        $("#wecaremenu",parent.document).css("display","none");
    }

    var e = window.location.href;
    e.toLowerCase().includes("localhost:") ? (document.getElementById("bgimg") && (document.getElementById("bgimg").src = "../../img/login/caregiver.jpg"), document.getElementById("imgLogo") && (document.getElementById("imgLogo").src = "../../img/login/loginbackground.png"), document.getElementById("logoImg") && (document.getElementById("logoImg").src = "../../img/login/homecare.png")) : e.toLowerCase().includes("stage.") ? (document.getElementById("bgimg") && (document.getElementById("bgimg").src = "../../img/login/caregiver.jpg"), document.getElementById("imgLogo") && (document.getElementById("imgLogo").src = "../../img/login/loginbackground.png"), document.getElementById("logoImg") && (document.getElementById("logoImg").src = "../../img/login/homecare.png")) : e.toLowerCase().includes("app.") && (document.getElementById("bgimg") && (document.getElementById("bgimg").src = "../../img/login/appcare.jpg"), document.getElementById("imgLogo") && (document.getElementById("imgLogo").src = "../../img/login/loginbackground_app.png"), document.getElementById("logoImg") && (document.getElementById("logoImg").src = "../../img/login/homecare_app.png")), $("#username").focusin(function() {}), $("#username").focusout(function() {}), $("#password").focusin(function() {}), $("#password").focusout(function() {})
}), $(window).resize(function() {
    adjustHeight(), displayHeaderMenu()
}), $(window).load(function() {
    setTimeout(function() {
        var e = localStorage.getItem("userName");

        (e || $("#username").val(e), adjustHeight(), $("#username").addClass("userNameStyle"), $(parent).off("loadImapImageEvent"), $(parent).on("loadImapImageEvent", function(e, o) {
            console.log(e)
        }), sessionStorage.uName) || location.href.indexOf("servermenu.html") >= 0 && onClickLogout();
        sessionStorage.uName && (setTimeout(function() {
            var e = ipAddress + "/homecare/download/img/logowithtext/?access_token=" + sessionStorage.access_token + "&tenant=" + sessionStorage.tenant + "&" + Math.round(1e6 * Math.random());
            $("#imgLogo1").attr("src", e)
        }, 1e3), "2" == sessionStorage.clientTypeId && ($("#lblPtLbl").text("Service User Id :"), $("#spnPatient").text("Service User"), $("#addAccount").text("Create Service User"), $("#divPT").attr("title", "Service User"), $("#addUserPatientMap").text("Service User User Mapping")), "1000" == sessionStorage.userTypeId && $("#divUsers").show(), "700" == sessionStorage.userTypeId && $("#divMN").show(), $("#imgPatient").show(), sessionStorage.uName, adjustFrameHeight(), $("#txtWelcome").text(sessionStorage.fullName), $("#videoFrame").attr("src", "../../html/patients/videoCallModule.html"), adjustFrameHeight(), minimizeVideFrame(), $("#lblPatient").trigger("click"))
    }, 1e3), $("#logout").off("click"), $("#logout").on("click", onClickLogout), $("#txtDOB").kendoDatePicker(), $("#gender").kendoComboBox(), $("#seqQuetion1").kendoComboBox(), $("#seqQuetion2").kendoComboBox(), $("#seqQuetion3").kendoComboBox(), $("#seqQuetion4").kendoComboBox(), $("#seqQuetion5").kendoComboBox(), $("#loginbtn").off("click", onClickLogin), $("#loginbtn").on("click", onClickLogin), $("#username").keyup(function(e) {
        13 == e.keyCode && $("#loginbtn").trigger("click")
    }), $("#password").keyup(function(e) {
        13 == e.keyCode && $("#loginbtn").trigger("click")
    }), $("#imgCareHome").off("click"), $("#imgCareHome").on("click", onClickImgCareHome), $("#imgPatient").off("click"), $("#imgPatient").on("click", onClickPatient), $("#imgProvider").off("click"), $("#imgProvider").on("click", onClickProvider), $("#imgManager").off("click"), $("#imgManager").on("click", onClickManagerProvider), $("#imgMaster").off("click"), $("#imgMaster").on("click", onClickMasters), $("#imgReport").off("click"), $("#imgReport").on("click", onClickReports), $("#imgSchedule").off("click"), $("#imgSchedule").on("click", onClickSchedule), $("#lblDB").off("click"), $("#lblDB").on("click", onClickDB), $("#lblSettings").off("click"), $("#lblSettings").on("click", onClickSettings), $("#imgSetting").off("click"), $("#imgSetting").on("click", onClickSetup), $("#loginbtn").off("mouseout"), $("#loginbtn").on("mouseout", function() {
        $("#loginbtn").removeClass("loginHoverStyle")
    }), $("#suRegistration").off("click", onClickSURegistration), $("#suRegistration").on("click", onClickSURegistration),
        $("#wecareBatch").off("click", onClickwecareBatch), $("#wecareBatch").on("click", onClickwecareBatch),
        $("#wecareCust").off("click", onClickwecareCustomer), $("#wecareCust").on("click", onClickwecareCustomer),
        $("#wecareEmp").off("click", onClickwecareEmp), $("#wecareEmp").on("click", onClickwecareEmp),
        $("#wecareGST").off("click", onClickwecareGST), $("#wecareGST").on("click", onClickwecareGST),
        $("#wecareItem").off("click", onClickwecareItem), $("#wecareItem").on("click", onClickwecareItem),
        $("#wecareJournal").off("click", onClickwecareJournal), $("#wecareJournal").on("click", onClickwecareJournal),
        $("#wecareSetup").off("click", onClickwecareSetup), $("#wecareSetup").on("click", onClickwecareSetup),
        $("#wecareTrans").off("click", onClickwecareTransporter), $("#wecareTrans").on("click", onClickwecareTransporter),
        $("#reportInvoices").off("click"), $("#reportInvoices").on("click", onClickReportInvoices),
        $("#liDayInvoiceReport").off("click"), $("#liDayInvoiceReport").on("click", onClickDayInvoiceReport),
        $("#liDayDetailReport").off("click"), $("#liDayDetailReport").on("click", onClickDayDetailReport),
        $("#addVisitSummaryReport").off("click"), $("#addVisitSummaryReport").on("click", onClickVisitSummaryReport),

        $("#addADL").off("click"), $("#addADL").on("click", onClickAddADL), $("#addUnAvailable").off("click"), $("#addUnAvailable").on("click", onClickAddUnAvailable), $("#addDocumentype").off("click"), $("#addDocumentype").on("click", onClickAddDocument),$("#addEmploymentType").off("click"), $("#addEmploymentType").on("click", onClickAddEmployment),$("#addRateType").off("click"), $("#addRateType").on("click", onClickAddRateType),$("#addEntityRelation").off("click"), $("#addEntityRelation").on("click", onClickAddEntity),$("#addNoteType").off("click"), $("#addNoteType").on("click", onClickAddNoteType),$("#addNoteStatus").off("click"), $("#addNoteStatus").on("click", onClickAddNoteStatus),$("#addAlertType").off("click"), $("#addAlertType").on("click", onClickAddAlertType),$("#addAlertStatus").off("click"), $("#addAlertStatus").on("click", onClickAddAlertStatus),$("#addAlertGeneratedBy").off("click"), $("#addAlertGeneratedBy").on("click", onClickAddAlertGeneratedBy),$("#addAccount").off("click"), $("#addAccount").on("click", onClickAddPatient), $("#addPharmacy").off("click"), $("#addPharmacy").on("click", onClickPharmacy), $("#addBillAccount").off("click"), $("#addBillAccount").on("click", onClickAddBillAccount), $("#addFacility").off("click"), $("#addFacility").on("click", onClickAddFacility), $("#addDoctor").off("click", onClickAddDoctor), $("#addDoctor").on("click", onClickAddDoctor), $("#addVitals").off("click", onClickAddVitals), $("#addVitals").on("click", onClickAddVitals), $("#addForm").off("click", onClickMedication), $("#addForm").on("click", onClickMedication),$("#addRoute").on("click", onClickRouteMedication),$("#addRxForm").on("click", onClickRxFormMedication),$("#addTransmission").on("click", onClickTransmissionMedication),$("#addStatus").on("click", onClickStatusMedication),$("#addAction").on("click", onClickActionMedication), $("#addInstructions").on("click", onClickInstrucionMedication), $("#addFrequency").on("click", onClickFrequencyMedication), $("#addStrength").on("click", onClickStrengthMedication), $("#addPrescription").off("click", onClickPrescription), $("#addPrescription").on("click", onClickPrescription), $("#addPatient").off("click", onClickMasterPatient), $("#addPatient").on("click", onClickMasterPatient),$("#addAppointmentTasks").on("click", onClickAppointmentReasonTasks), $("#addIllness").off("click", onClickMedicalConditionIllness), $("#addIllness").on("click", onClickMedicalConditionIllness), $("#addDiet").off("click", onClickMedicalConditionDiet), $("#addDiet").on("click", onClickMedicalConditionDiet),$("#addExercise").on("click", onClickMedicalConditionExercise),$("#addAllergy").on("click", onClickMedicalConditionAllergy), $("#appointmentStatus").off("click", onClickAppointment), $("#appointmentReason").on("click", onClickAppointmentReason), $("#appointmentStatus").on("click", onClickAppointment), $("#addPatientActivity").off("click"), $("#addPatientActivity").on("click", onClickPatientActivity), $("#addReportDesigner").off("click"), $("#addReportDesigner").on("click", onClickReportDesigner), $("#addReportViewer").off("click"), $("#addReportViewer").on("click", onClickReportViewer), $("#addReportMaster").off("click"), $("#addReportMaster").on("click", onClickReportMaster), $("#addFormDesign").off("click"), $("#addFormDesign").on("click", onClickFormDesign), $("#addCopyAppointment").off("click"), $("#addCopyAppointment").on("click", onClickAddCopyAppointment),$("#addTaskReport").off("click"), $("#addTaskReport").on("click", onClickTaskReport),$("#addTaskViewer").off("click"), $("#addTaskViewer").on("click", onClickTaskViewer),$("#addStaffTimeSheet").off("click"), $("#addStaffTimeSheet").on("click", onClickStaffTimeSheet),$("#addSUReport").off("click"), $("#addSUReport").on("click", onClickSUReport),$("#addStaffCareReprot").off("click"), $("#addStaffCareReprot").on("click", onClickStaffCareReprot),$("#addSUCareReport").off("click"), $("#addSUCareReport").on("click", onClickSUCareReport),$("#addDailyFluidReport").off("click"), $("#addDailyFluidReport").on("click", onClickDailyFluidReport), $("#addTopicalApplication").off("click"), $("#addTopicalApplication").on("click", onClickTopicalApplication), $("#addRepositionChart").off("click"), $("#addRepositionChart").on("click", onClickRepositionChart), $("#addEmployer").off("click", onClickAddEmployer), $("#addEmployer").on("click", onClickAddEmployer), $("#createTable").off("click", onClickCreateTable), $("#createTable").on("click", onClickCreateTable), $("#addValues").off("click", onClickAddValues), $("#addValues").on("click", onClickAddValues),$("#addLeaveApprove").off("click", onClickLeaveApprove), $("#addLeaveApprove").on("click", onClickLeaveApprove),$("#addCreaeManager").off("click", onClickCreateManager), $("#addCreaeManager").on("click", onClickCreateManager),$("#addDisplay").off("click", onClickAddDisplay), $("#addDisplay").on("click", onClickAddDisplay),$("#addUserMapping").off("click", onClickAddUserMapping), $("#addUserMapping").on("click", onClickAddUserMapping),$("#addStaffUserMapping").off("click", onClickAddStaffUserMapping), $("#addStaffUserMapping").on("click", onClickAddStaffUserMapping),$("#addStaffAppointment").off("click", onClickAddStaffAppointment), $("#addStaffAppointment").on("click", onClickAddStaffAppointment), $("#addMediReport").off("click", onClickShowMediReport), $("#addMediReport").on("click", onClickShowMediReport), $("#addGeoRep").off("click", onClickGeoReport), $("#addGeoRep").on("click", onClickGeoReport), $("#addMentalAssessmentRep").off("click", onClickMentalReport), $("#addMentalAssessmentRep").on("click", onClickMentalReport), $("#foodReport").off("click", onClickFoodReport), $("#foodReport").on("click", onClickFoodReport), $("#ironReport").off("click", onClickIronLogReport), $("#ironReport").on("click", onClickIronLogReport), $("#appointmentReport").off("click"), $("#appointmentReport").on("click", onClickAppointmentReport),$("#appointmentDelayReport").off("click"), $("#appointmentDelayReport").on("click", onClickAppointmentDelayReport),$("#staffOverlapReport").off("click"), $("#staffOverlapReport").on("click", onClickStaffOverlapReport),$("#carerInOutReport").off("click"), $("#carerInOutReport").on("click", onClickCarerInOutReport),$("#patientInvoice2").off("click"), $("#patientInvoice2").on("click", onClickPatientInvoice2), $("#addTemplate").off("click", onClickCreateTemplate), $("#addTemplate").on("click", onClickCreateTemplate), $("#addMiniTemplate").off("click", onClickCreateMiniTemplate), $("#addMiniTemplate").on("click", onClickCreateMiniTemplate), $("#addFormData").off("click", onClickCreateFormData), $("#addFormData").on("click", onClickCreateFormData),$("#addLetter").off("click", onClickLetter), $("#addLetter").on("click", onClickLetter), $("#addLetterData").off("click", onClickLetterData), $("#addLetterData").on("click", onClickLetterData),  $("#addManagers").off("click", onClickManagers), $("#addManagers").on("click", onClickManagers), $("#addActType").off("click", onClickActType), $("#addActType").on("click", onClickActType), $("#addBodyPart").off("click", onClickBodyPart), $("#addBodyPart").on("click", onClickBodyPart), $("#addCommCountry").off("click", onClickCommunication), $("#addCommCountry").on("click", onClickCommunication),$("#addCounty").off("click", onClickCommCounty), $("#addCounty").on("click", onClickCommCounty),$("#addCity").off("click", onClickCommCity), $("#addCity").on("click", onClickCommCity), $("#addCountry").off("click", onClickAddCountry), $("#addCountry").on("click", onClickAddCountry), $("#addState").off("click", onClickAddState), $("#addState").on("click", onClickAddState), $("#addZip").off("click", onClickAddZip), $("#addZip").on("click", onClickAddZip), $("#addResource").off("click"), $("#addResource").on("click", onClickFileResource),$("#addViewAppointments").off("click"), $("#addViewAppointments").on("click", onClickViewAppointments), $("#imgDB").off("click"), $("#imgDB").on("click", onClickDB), $("#imgVideoCall").off("click"), $("#imgVideoCall").on("click", onClickVideoCall), $("#addType").off("click"), $("#addType").on("click", onClickBillingType), $("#addPeriod").off("click"), $("#addPeriod").on("click", onClickPeriod), $("#addCategory").off("click"), $("#addCategory").on("click", onClickBillCategory), $("#addModeofTravel").off("click"), $("#addModeofTravel").on("click", onClickModeofTravel), $("#addBillTo").off("click"), $("#addBillTo").on("click", onClickBillTo), $("#addBillToWhom").off("click"), $("#addBillToWhom").on("click", onClickBillToWhom),$("#addSecurityPermissions").off("click", onClickAddSecurityPermissions), $("#addSecurityPermissions").on("click", onClickAddSecurityPermissions), $("#btnMaster").on("click", function(e) {
        e.preventDefault(), collapseBillingMenu()
    }), $("#btnBilling").on("click", function(e) {
        e.preventDefault(), collapseMasterMenu()
    });

    var e = new Date;
    $("#tmZone").text(e), setInterval(function() {
        var e = new Date;
        $("#tmZone").text(e)
    }, 1e3)

}), window.onbeforeunload = function() {};
var postURL = "";

function submitForm(e) {
    postURL = e, submitFormData()
}

function setOrgFocus() {
    $("#tiOrganization").focus()
}

function getRootPath() {
    var e = location.href,
        o = GetSubstringIndex(e, "/", 4);
    o += 1;
    var t = e.substr(0, o);
    sessionStorage.rootURL = t
}

function GetSubstringIndex(e, o, t) {
    for (var n = 0, i = null; n < t && -1 !== i;) i = e.indexOf(o, i + 1), n++;
    return i
}

function onClickBillingType() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/billing/billingType.html")
}

function onClickBillTo() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/billing/billTo.html")
}

function onClickBillToWhom() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/billing/whomToBill.html")
}

function onClickBillCategory() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/billing/category.html")
}

function onClickModeofTravel() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/billing/modeofTravel.html")
}

function onClickPeriod() {
    collapseMasterMenu(), $("#ptframe").attr("src", "../../html/billing/period.html")
}
$(function() {
    sessionStorage.curPage = "", getRootPath(), $("#login-form").on("keyup", function(e) {
        e.keyCode
    }), $("#reset-btn").on("click", function() {
        $("#login-form")[0].reset(), $("#login-box p.success-msg").hide()
    }), $("#btnForgetPass").on("click", function(e) {
        e.stopPropagation(), $("#divLogin").hide(), $("#forgotPassBox").show()
    }), $("#btnForgotId").on("click", function(e) {
        e.stopPropagation(), $("#forgotPassBox").hide(), $("#forgotIDBox").show()
    }), $("#btnContinue").on("click", function(e) {
        e.stopPropagation(), $("#forgotPassBox").hide(), $("#unlockBox").show()
    }), $("#btnforgetIdContinue").on("click", function(e) {
        e.stopPropagation(), $("#forgotIDBox").hide(), $("#login-box-outer").show(), $("#divLogin").show()
    }), $("#btnUnlockCancel").on("click", function(e) {
        $("#unlockBox").hide(), $("#login-box-outer").show(), $("#divLogin").show()
    }), $("#btnAcctCancel").on("click", function(e) {
        $("#unlockAccountBox").hide(), $("#login-box-outer").show(), $("#divLogin").show()
    }), $("#btnAcctCreate").on("click", function(e) {
        $("#unlockAccountBox").hide(), $("#login-box-outer").show(), $("#divLogin").show()
    }), $("#btnUnlockCreate").on("click", function(e) {
        e.stopPropagation(), $("#unlockBox").hide(), $("#unlockAccountBox").show()
    }), $("#btnSignupCompContinue").on("click", function(e) {
        $("#signupContinueBox").hide(), $("#login-box-outer").show(), $("#divLogin").show()
    }), $(".slides-pagination a, .slides-arrow").on("click", function() {
        sliderContentChange()
    }), $("#mobile-navigation").on("click", function() {
        var e = $("#mobile-nav");
        e.is(":visible") ? e.slideUp("slow", function() {
            $("#login-box-outer").fadeIn("slow")
        }) : $("#login-box-outer").fadeOut("slow", function() {
            e.slideDown("slow")
        })
    });
    var e = window.location.pathname.split("/").pop();
    void 0 == e && (e = "login.html"), sessionStorage.redirectPageOnLogout = "login.html", $(".headerMenu a").on("click", function(e) {
        e.preventDefault(), $("#divSignIn").find("ul").is(":visible") ? $("#divSignIn").find("ul").slideUp(300) : $("#divSignIn").find("ul").slideDown(300), $(this).closest(".headerMenu").toggleClass("removeMenu")
    }), $("#councilMainPage") && $("#councilMainPage").length > 0 && onClickImgCareHome(), $("#invoiceMainPage") && $("#invoiceMainPage").length > 0 && $("#ptframe").attr("src", "../../html/invoice/invoice.html")
});



(function($) {
    $.fn.extend({
        passwordValidation: function(_options, _callback, _confirmcallback) {
            //var _unicodeSpecialSet = "^\\x00-\\x1F\\x7F\\x80-\\x9F0-9A-Za-z"; //All chars other than above (and C0/C1)
            var CHARSETS = {
                upperCaseSet: "A-Z", 	//All UpperCase (Acii/Unicode)
                lowerCaseSet: "a-z", 	//All LowerCase (Acii/Unicode)
                digitSet: "0-9", 		//All digits (Acii/Unicode)
                specialSet: "\\x20-\\x2F\\x3A-\\x40\\x5B-\\x60\\x7B-\\x7E\\x80-\\xFF", //All Other printable Ascii
            }
            var _defaults = {
                minLength: 12,		  //Minimum Length of password
                minUpperCase: 2,	  //Minimum number of Upper Case Letters characters in password
                minLowerCase: 2,	  //Minimum number of Lower Case Letters characters in password
                minDigits: 2,		  //Minimum number of digits characters in password
                minSpecial: 2,		  //Minimum number of special characters in password
                maxRepeats: 5,		  //Maximum number of repeated alphanumeric characters in password dhgurAAAfjewd <- 3 A's
                maxConsecutive: 3,	  //Maximum number of alphanumeric characters from one set back to back
                noUpper: false,		  //Disallow Upper Case Lettera
                noLower: false,		  //Disallow Lower Case Letters
                noDigit: false,		  //Disallow Digits
                noSpecial: false,	  //Disallow Special Characters
                //NOT IMPLEMENTED YET allowUnicode: false,  //Switches Ascii Special Set out for Unicode Special Set
                failRepeats: true,    //Disallow user to have x number of repeated alphanumeric characters ex.. ..A..a..A.. <- fails if maxRepeats <= 3 CASE INSENSITIVE
                failConsecutive: true,//Disallow user to have x number of consecutive alphanumeric characters from any set ex.. abc <- fails if maxConsecutive <= 3
                confirmField: undefined
            };

            //Ensure parameters are correctly defined
            if($.isFunction(_options)) {
                if($.isFunction(_callback)) {
                    if($.isFunction(_confirmcallback)) {
                        console.log("Warning in passValidate: 3 or more callbacks were defined... First two will be used.");
                    }
                    _confirmcallback = _callback;
                }
                _callback = _options;
                _options = {};
            }

            //concatenate user options with _defaults
            _options = $.extend(_defaults, _options);
            if(_options.maxRepeats < 2) _options.maxRepeats = 2;

            function charsetToString() {
                return CHARSETS.upperCaseSet + CHARSETS.lowerCaseSet + CHARSETS.digitSet + CHARSETS.specialSet;
            }

            //GENERATE ALL REGEXs FOR EVERY CASE
            function buildPasswordRegex() {
                var cases = [];

                //if(_options.allowUnicode) CHARSETS.specialSet = _unicodeSpecialSet;
                if(_options.noUpper) 	cases.push({"regex": "(?=" + CHARSETS.upperCaseSet + ")",  																				"message": "Password can't contain an Upper Case Letter"});
                else 					cases.push({"regex": "(?=" + ("[" + CHARSETS.upperCaseSet + "][^" + CHARSETS.upperCaseSet + "]*").repeat(_options.minUpperCase) + ")", 	"message": "Password must contain at least " + _options.minUpperCase + " Upper Case Letters."});
                if(_options.noLower) 	cases.push({"regex": "(?=" + CHARSETS.lowerCaseSet + ")",  																				"message": "Password can't contain a Lower Case Letter"});
                else 					cases.push({"regex": "(?=" + ("[" + CHARSETS.lowerCaseSet + "][^" + CHARSETS.lowerCaseSet + "]*").repeat(_options.minLowerCase) + ")", 	"message": "Password must contain at least " + _options.minLowerCase + " Lower Case Letters."});
                if(_options.noDigit) 	cases.push({"regex": "(?=" + CHARSETS.digitSet + ")", 																					"message": "Password can't contain a Number"});
                else 					cases.push({"regex": "(?=" + ("[" + CHARSETS.digitSet + "][^" + CHARSETS.digitSet + "]*").repeat(_options.minDigits) + ")", 			"message": "Password must contain at least " + _options.minDigits + " Digits."});
                if(_options.noSpecial) 	cases.push({"regex": "(?=" + CHARSETS.specialSet + ")", 																				"message": "Password can't contain a Special Character"});
                else 					cases.push({"regex": "(?=" + ("[" + CHARSETS.specialSet + "][^" + CHARSETS.specialSet + "]*").repeat(_options.minSpecial) + ")", 		"message": "Password must contain at least " + _options.minSpecial + " Special Characters."});

                cases.push({"regex":"[" + charsetToString() + "]{" + _options.minLength + ",}", "message":"Password must contain at least " + _options.minLength + " characters."});

                return cases;
            }
            var _cases = buildPasswordRegex();

            var _element = this;
            var $confirmField = (_options.confirmField != undefined)? $(_options.confirmField): undefined;

            //Field validation on every captured event
            function validateField() {
                var failedCases = [];

                //Evaluate all verbose cases
                $.each(_cases, function(i, _case) {
                    if($(_element).val().search(new RegExp(_case.regex, "g")) == -1) {
                        failedCases.push(_case.message);
                    }
                });
                if(_options.failRepeats && $(_element).val().search(new RegExp("(.)" + (".*\\1").repeat(_options.maxRepeats - 1), "gi")) != -1) {
                    failedCases.push("Password can not contain " + _options.maxRepeats + " of the same character case insensitive.");
                }
                if(_options.failConsecutive && $(_element).val().search(new RegExp("(?=(.)" + ("\\1").repeat(_options.maxConsecutive) + ")", "g")) != -1) {
                    failedCases.push("Password can't contain the same character more than " + _options.maxConsecutive + " times in a row.");
                }

                //Determine if valid
                var validPassword = (failedCases.length == 0) && ($(_element).val().length >= _options.minLength);
                var fieldsMatch = true;
                if($confirmField != undefined) {
                    fieldsMatch = ($confirmField.val() == $(_element).val());
                }

                _callback(_element, validPassword, validPassword && fieldsMatch, failedCases);
            }

            //Add custom classes to fields
            this.each(function() {
                //Validate field if it is already filled
                if($(this).val()) {
                    validateField().apply(this);
                }

                $(this).toggleClass("jqPassField", true);
                if($confirmField != undefined) {
                    $confirmField.toggleClass("jqPassConfirmField", true);
                }
            });

            //Add event bindings to the password fields
            return this.each(function() {
                $(this).bind('keyup focus input proprtychange mouseup', validateField);
                if($confirmField != undefined) {
                    $confirmField.bind('keyup focus input proprtychange mouseup', validateField);
                }
            });
        }
    });
})(jQuery);

function createUserObj() {
    var self = this,
        $pwdElem = $("#txtPassword"),
        uId = '',
        tzArray = [],
        operation = "",
        ADD = "add",
        UPDATE = "update",
        finalCheck = false;
    this.init = function() {
        this.bindEvents();
        allowSplAlphaNumericWithoutSpace('txtPassword');
        allowSplAlphaNumericWithoutSpace('txtConfirmPassword');

    };

    this.bindEvents = function() {

        // $('.cmp-acc-text-input-container').find('input.cmp-acc-text-input[required="required"], input.required-input').on('focusout blur keyup', function() {
        // 	var $this = $(this);
        // 	$this.closest('.cmp-acc-text-input-container').find('.cmp-acc-info').removeClass('cmp-acc-banner cmp-acc-defaultposition').addClass('cmp-acc-hide');
        // 	if($this.attr('id') !== 'cmp-acc-password') {
        // 		errortypeobj = {
        // 			'background': "url(../../img/create-account/warning@44.png) no-repeat"
        // 		}
        // 		successtypeobj = {
        // 			'background': 'url(../../img/create-account/check@44.png) no-repeat'
        // 		}
        // 		if($this.val().length == 0) {
        // 			$this.closest('.cmp-acc-text-input-container').find('.cmp-acc-status').css(errortypeobj);
        // 			$this.addClass('cmp-acc-input-error');
        // 		} else if($this.attr('type') !== 'password') {
        // 			$this.closest('.cmp-acc-text-input-container').find('.cmp-acc-status').css(successtypeobj);
        // 			$this.removeClass('cmp-acc-input-error');
        // 		}
        // 	} else if ($this.attr('id') === 'cmp-acc-password') {
        // 		errortypeobj = {
        // 			'background': "url(../../img/create-account/lock-left-grey@3x.png) no-repeat"
        // 		}
        // 		successtypeobj = {
        // 			'background': 'url(../../img/create-account/lock-right-grey@3x.png) no-repeat'
        // 		}
        // 		if($this.val().length == 0) {
        // 			$this.closest('.cmp-acc-text-input-container').find('.cmp-acc-status').css(errortypeobj);
        // 			$this.addClass('cmp-acc-input-error');
        // 		} else {
        // 			$this.closest('.cmp-acc-text-input-container').find('.cmp-acc-status').css(successtypeobj);
        // 			$this.removeClass('cmp-acc-input-error');
        // 		}
        // 	}
        // }).on('focus', function() {
        // 	var $this = $(this);
        // 	$this.closest('.cmp-acc-text-input-container').find('.cmp-acc-info').removeClass('cmp-acc-hide').addClass('cmp-acc-banner cmp-acc-defaultposition');
        // });
        /*$('.cmp-acc-select-security').on('change', function() {
            $('.cmp-acc-select-security').find('option').show();
            $('.cmp-acc-select-security').each(function() {
                var $this = $(this);
                var selfValue = $this.val();
                if(selfValue) {
                    // $this.find('option').show();
                    $('.cmp-acc-select-security').not(this).find('option[value="'+selfValue+'"]').hide();
                    //$this.find('option[value="'+selfValue+'"]').show();
                }
            });
        });*/
        var pwrValidObj = {
            confirmField: "#txtConfirmPassword",
            minLength: 8,
            minUpperCase: 1,
            minLowerCase: 1,
            minDigits: 1,
            minSpecial: 1
        };
        var numberRegex = /\d/;
        var splRegex = /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;
        $pwdElem.passwordValidation(pwrValidObj, function(element, valid, match, failedCases) {
            $("#errors").html("<pre>" + failedCases.join("\n") + "</pre>");
            if(valid) {
                $('#cmp-acc-password-validator-message-success').removeClass('cmp-acc-hide');
                $('#cmp-acc-password-validator-message-fail').addClass('cmp-acc-hide');
                $pwdElem.removeClass('cmp-acc-input-error');
                $pwdElem.closest('.cmp-acc-text-input-container').find('.cmp-acc-status').css({'background': 'url(../../img/create-account/lock-success@3x.png) no-repeat'});
            }
            if(!valid) {
                $('#cmp-acc-password-validator-message-success').addClass('cmp-acc-hide');
                $('#cmp-acc-password-validator-message-fail').removeClass('cmp-acc-hide');
                $pwdElem.addClass('cmp-acc-input-error');
                $pwdElem.closest('.cmp-acc-text-input-container').find('.cmp-acc-status').css({'background': 'url(../../img/create-account/lock-right-grey@3x.png) no-repeat'});
            }
            if(valid && match) {
                $('#cmp-acc-error-confirm-password').addClass('cmp-acc-hide');
                $('#cmp-acc-confirm-password').removeClass('cmp-acc-input-error');
                $('#cmp-acc-confirm-password').closest('.cmp-acc-text-input-container').find('.cmp-acc-status').css({'background': 'url(../../img/create-account/check@44.png) no-repeat'});
            }
            if(!valid || !match) {
                $('#cmp-acc-error-confirm-password').removeClass('cmp-acc-hide');
                $('#cmp-acc-confirm-password').addClass('cmp-acc-input-error');
                $('#cmp-acc-confirm-password').closest('.cmp-acc-text-input-container').find('.cmp-acc-status').css({'background': 'url(../../img/create-account/warning@44.png) no-repeat'});
            }
            if($pwdElem.val().length >= 8) {
                $('#cmp-acc-password-min-num-of-chars').addClass('message-success');
                $pwdElem.removeClass('cmp-acc-input-error');
            } else {
                $('#cmp-acc-password-min-num-of-chars').removeClass('message-success');
                $pwdElem.addClass('cmp-acc-input-error');
            }
            if($pwdElem.val().match(/[a-z]/) && $pwdElem.val().match(/[A-Z]/)) {
                $('#cmp-acc-password-lower-upper-chars').addClass('message-success');
                $pwdElem.removeClass('cmp-acc-input-error');
            } else {
                $('#cmp-acc-password-lower-upper-chars').removeClass('message-success');
                $pwdElem.addClass('cmp-acc-input-error');
            }
            if(numberRegex.test($pwdElem.val())) {
                $('#cmp-acc-password-use-number').addClass('message-success');
                $pwdElem.removeClass('cmp-acc-input-error');
            } else {
                $('#cmp-acc-password-use-number').removeClass('message-success');
                $pwdElem.addClass('cmp-acc-input-error');
            }
            if(splRegex.test($pwdElem.val())) {
                $('#cmp-acc-password-use-symbol').addClass('message-success');
                $pwdElem.removeClass('cmp-acc-input-error');
            } else {
                $('#cmp-acc-password-use-symbol').removeClass('message-success');
                $pwdElem.addClass('cmp-acc-input-error');
            }
        });
        /*$('.cmp-acc-user-available').on('click', function(e) {
            e.preventDefault();
            var strUN = $("#cmp-acc-email").val();
            $('.user-valid-alert').remove();
            if (strUN != "" && validateEmail(strUN)) {
                var dataUrl = ipAddress + "/user/is-available/" + strUN + "/";
                getAjaxObject(dataUrl, "GET", getUserAvailable, onError);
            } else if(strUN != "" && !validateEmail(strUN)) {

            }
        });
        var checkFlag = true;
        $('#cmp-acc-sign-up-submit-btn').on('click', function(e) {
            e.preventDefault();
            checkFlag = true;
            $('.cmp-acc-text-input-container:visible').find('input[required="required"], input.required-input, select[required="required"]').each(function() {
                if($(this).val() === '' || $(this).hasClass('cmp-acc-input-error')) {
                    checkFlag = false;
                }
            });
            if (checkFlag) {
                hideShowContent();
            }
        });
        $('.cmp-acc-text-input-container').find('input[required="required"], input.required-input, select[required="required"]').on('change', function() {
            if($(this).val() === '' || $(this).hasClass('cmp-acc-input-error')) {
                checkFlag = false;
            }
        });
        $('#nextBtn, #nextBtnAfterDevices,  #nextBtnAfterVerifyCode').on('click', function(e) {
            e.preventDefault();
            var strUN = $("#cmp-acc-email").val();
            checkFlag = true;
            $('.cmp-acc-text-input-container:visible').find('input[required="required"], input.required-input, select[required="required"]').each(function() {
                if($(this).val() === '' || $(this).hasClass('cmp-acc-input-error')) {
                    checkFlag = false;
                }
            });
            if (checkFlag) {
                if($(this).attr('id') == 'nextBtnAfterVerifyCode') {
                    finalCheck = true;
                    var dataUrl = ipAddress + "/user/is-available/" + strUN + "/";
                    getAjaxObject(dataUrl, "GET", getUserCreate, onError);
                } else {
                    hideShowContent();
                }
            }
        });*/

    };
    this.init();
};





function onUpdateSuccess(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            customAlert.info("info", "User password updated successfully");
            $("#resetPassword").hide();
        }else{
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}

function closePopup(id) {
    $('#' + id).hide();
}
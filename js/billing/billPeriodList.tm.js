var angularUIgridWrapper;
var parentRef = null;
var recordType = "1";
var dataArray = [];
var ADD = "add";
var UPDATE = "edit";
var VIEW = "view";
var DELETE = "delete";
var operation = "add";
var atID = "";
var ds  = "";

var IsFlag = 1;

var statusArr = [{Key:'Active',Value:'Active'},{Key:'InActive',Value:'InActive'}];

$(document).ready(function(){
    $("#pnlPatient",parent.document).css("display","none");
    sessionStorage.setItem("IsSearchPanel", "1");
    themeAPIChange();
    parentRef = parent.frames['iframe'].window;
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }

    angularUIgridWrapper = new AngularUIGridWrapper("dgridBillTypeList", dataOptions);
    angularUIgridWrapper.init();
    buildDeviceListGrid([]);

});


$(window).load(function(){
    loading = false;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
    init();
    buttonEvents();
    adjustHeight();
}

function init(){
    $("#btnEdit").prop("disabled", true);
    $("#btnDelete").prop("disabled", true);

    buildDeviceListGrid([]);
    getAjaxObject(ipAddress+"/homecare/billing-periods/?is-active=1&is-deleted=0","GET",getActivityTypes,onError);
}

function getActivityTypes(dataObj){
    console.log(dataObj);
    var tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.roles){
            if($.isArray(dataObj.response.roles)){
                tempCompType = dataObj.response.roles;
            }else{
                tempCompType.push(dataObj.response.roles);
            }
        }
    }

    for(var i=0;i<tempCompType.length;i++){
        tempCompType[i].idk = tempCompType[i].id;
    }

    buildDeviceListGrid(tempCompType);
}
function onError(errorObj){
    console.log(errorObj);
}

function buttonEvents(){
    $("#btnSave").off("click",onClickSave);
    $("#btnSave").on("click",onClickSave);

    $("#btnEdit").off("click",onClickEdit);
    $("#btnEdit").on("click",onClickEdit);

    $("#btnDelete").off("click",onClickDelete);
    $("#btnDelete").on("click",onClickDelete);

    $("#btnAdd").off("click");
    $("#btnAdd").on("click",onClickAdd);

    $("#btnCancel").off("click");
    $("#btnCancel").on("click",onClickCancel);

    $("#btnReset").off("click",onClickReset);
    $("#btnReset").on("click",onClickReset);

    $(".popupClose").off("click", onClickCancel);
    $(".popupClose").on("click", onClickCancel);


    $(".btnActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('active');
        onClickActive();
    });
    $(".btnInActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('inactive');
        onClickInActive();
    });

    allowAlphabets("txtAbbreviation");
    allowAlphaNumericwithSapce("txtDescription");
}

function searchOnLoad(status) {
    buildDeviceListGrid([]);
    if(status == "active") {
        var urlExtn = ipAddress + "/homecare/billing-periods/?is-active=1&is-deleted=0&sort=code";
    }
    else if(status == "inactive") {
        var urlExtn =  ipAddress + "/homecare/billing-periods/?is-active=0&is-deleted=1&sort=code";
    }
    getAjaxObject(urlExtn,"GET",getActivityTypes,onError);
}


function onClickAdd(){
    $("#addPopup").show();
    $('#viewDivBlock').hide();
    $("#txtID").hide();
    $(".filter-heading").html(" Add Billing period");
    parentRef.operation = "add";
    onClickReset();
}


function addReportMaster(opr){
    var popW = 500;
    var popH = "43%";
    $('.alert').remove();
    var profileLbl = "Add Task Type";
    parentRef.operation = opr;
    operation = opr;
    if(opr == "add"){
        $('#btnSave').html('<span><img src="../../img/medication/Save.png" class="providerLink-btn-img" />Save</span>');
        $('.tabContentTitle').text('Add Task Type');
        $('#btnReset').show();
        //var billActName = $("#txtBAN").val();
        $('#btnReset').trigger('click');
        //$("#txtBAN").val(billActName);
    }else{

        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if (selectedItems && selectedItems.length > 0) {
            var obj = selectedItems[0];
            parentRef.id = obj.idk;
            parentRef.displayOrder  = obj.displayOrder;
            parentRef.type = obj.type;
            parentRef.DIO = obj.DIO;
            parentRef.activityGroup = obj.activityGroup;
            parentRef.isActive = obj.isActive;
            operation = UPDATE;
        }

        profileLbl = "Edit Task Type";
        $('.tabContentTitle').text('Edit Task Type');
        $('#btnReset').hide();
        $('#btnSave').html('<span><img src="../../img/medication/Save.png" class="providerLink-btn-img" />Update</span>');
        $('#btnReset').trigger('click');
    }
    var devModelWindowWrapper = new kendoWindowWrapper();
    devModelWindowWrapper.openPageWindow("../../html/masters/createactivityTypeList.html", profileLbl, popW, popH, true, closeaddReportMastertAction);
}
function closeaddReportMastertAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        var opr = returnData.operation;
        if(opr == "add"){
            customAlert.info("info", "Task Type created successfully");
        }else{
            customAlert.info("info", "Task Type updated successfully");
        }
        init();
    }
}
function onClickActive() {
    $(".btnInActive").removeClass("radioButton-active");
    $(".btnActive").addClass("radioButton-active");
}

function onClickInActive() {
    $(".btnActive").removeClass("radioButton-active");
    $(".btnInActive").addClass("radioButton-active");
}

function onClickDelete(){
    customAlert.confirm("Confirm", "Are you sure to delete?",function(response){
        if(response.button == "Yes"){
            var dataObj = {};
            dataObj.createdBy = Number(sessionStorage.userId);
            dataObj.isDeleted = 1;
            dataObj.isActive = 0;
            dataObj.id = atID;
            var dataUrl = ipAddress +"/homecare/billing-periods/?id="+ atID ;
            var method = "DELETE";
            createAjaxObject(dataUrl, dataObj, method, onCreateDelete, onError);
        }
    });
}

function onCreateDelete(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "Billing Period deleted successfully";
            customAlert.error("Info", msg);
            $("#txtAT").val("");
            operation = ADD;
            init();
        }
    }
}
function onClickSave(){
    var strAbb = $("#txtAbbreviation").val();
    strAbb = $.trim(strAbb);

    var strDescription = $("#txtDescription").val();
    strDescription = $.trim(strDescription);

    var dataObj = {};
    dataObj.createdBy = Number(sessionStorage.userId);
    if(parseInt($("#cmbStatus").val()) == 1){
        dataObj.isDeleted = 0;
    }
    else{
        dataObj.isDeleted = 1;
    }

    dataObj.isActive = parseInt($("#cmbStatus").val());
    dataObj.code = strAbb;
    dataObj.value = strDescription;
    var dataUrl = ipAddress +"/homecare/billing-periods/";
    var method = "POST";
    if(operation == UPDATE){
        method = "PUT";
        dataObj.id = atID;
    }
    createAjaxObject(dataUrl, dataObj, method, onCreate, onError);
}

function onCreate(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "Billing Period created successfully";
            if(operation == UPDATE){
                msg = "Billing Period updated successfully"
            }else{

            }
            displaySessionErrorPopUp("Info", msg, function(res) {
                // $("#txtAT").val("");
                operation = ADD;
                onClickCancel();
                init();
                onClickActive();
            })
        }else{
            customAlert.error("Error", dataObj.response.status.message);
        }
    }else{

    }
    onClickReset();
}function onClickCancel(){
    $(".filter-heading").html("View Billing Period ");
    $("#viewDivBlock").show();
    $("#addTaskGroupPopup").hide();


}

function adjustHeight(){
    var defHeight = 230;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    angularUIgridWrapper.adjustGridHeight(cmpHeight);
}

function buildDeviceListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Abbreviation",
        "field": "code"
    });
    gridColumns.push({
        "title": "Data Values",
        "field": "value"
    });

    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}

function onChange(){
    setTimeout(function() {
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if (selectedItems && selectedItems.length > 0) {
            var obj = selectedItems[0];
            atID = obj.idk;
            $("#btnEdit").prop("disabled", false);
            $("#btnDelete").prop("disabled", false);
        } else {
            $("#btnEdit").prop("disabled", true);
            $("#btnDelete").prop("disabled", true);
        }
    });
}
function onClickEdit() {
    $("#txtID").show();
    $(".filter-heading").html("Edit Billing Period");
    parentRef.operation = "edit";
    $("#viewDivBlock").hide();
    $("#addPopup").show();
    var selectedItems = angularUIgridWrapper.getSelectedRows();
    if (selectedItems && selectedItems.length > 0) {
        var obj = selectedItems[0];
        atID = obj.idk;
        $("#txtAbbreviation").val(obj.code);
        $("#txtDescription").val(obj.value);
        $("#txtID").html("ID : " + obj.idk);

        $("#cmbStatus").val(obj.isActive);
    }
}


var operation = "add";
function onStatusChange(){
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if(cmbStatus && cmbStatus.selectedIndex<0){
        cmbStatus.select(0);
    }
}

function onClickReset() {
    if(operation == ADD) {
        operation = ADD;
    }
    else {
        operation = UPDATE;
    }
    $("#txtAbbreviation").val("");
    $("#txtDescription").val("");
    $("#cmbStatus").val(1);


}
function themeAPIChange(){
    getAjaxObject(ipAddress+"/homecare/settings/?id=2","GET",getThemeValue,onError);
}

function getThemeValue(dataObj){
    console.log(dataObj);
    var tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.activityTypes){
            if($.isArray(dataObj.response.settings)){
                tempCompType = dataObj.response.settings;
            }else{
                tempCompType.push(dataObj.response.settings);
            }
        }
        if(tempCompType.length > 0){
            themesChange(tempCompType[0].value);
        }
    }
}

function themesChange(themeValue){
    if(themeValue == 2){
        loadAPi("../../Theme2/Theme02.css");
    }
    else if(themeValue == 3){
        loadAPi("../../Theme3/Theme03.css");
    }
}

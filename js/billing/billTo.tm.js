var angularUIgridWrapper;
var parentRef = null;
var recordType = "1";
var dataArray = [];
var ADD = "add";
var UPDATE = "edit";
var VIEW = "view";
var DELETE = "delete";
var operation = "add";
var atID = "";
var ds  = "";

var IsFlag = 0;
var IsPostalCodeManual = sessionStorage.IsPostalCodeManual;
var IsPostalFlag = "0";
var cntry = sessionStorage.countryName;

var billToId, cityId;

$(document).ready(function(){
    $("#pnlPatient",parent.document).css("display","none");
    sessionStorage.setItem("IsSearchPanel", "1");
    // themeAPIChange();
    getCountryIsPostalCode();
    document.addEventListener('contextmenu', event => event.preventDefault());

    parentRef = parent.frames['iframe'].window;
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("dgridBillTypeList", dataOptions);
    angularUIgridWrapper.init();
    buildDeviceListGrid([]);

});


$(window).load(function(){
    loading = false;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
    init();
    buttonEvents();
    adjustHeight();
}

function init(){
    allowNumericsWithDash("txtMobilePhone");
    allowNumericsWithDash("txtHomePhone");
    allowNumericsWithDash("txtFax");
    onGetBillType();
    $("#billTitle").html("View Bill To");
    $("#btnEdit").prop("disabled", true);
    $("#btnDelete").prop("disabled", true);
    $("#divTypeDetails").css("display","none");
    $(".topContainerPopup").css("display","");
    buildDeviceListGrid([]);
    getAjaxObject(ipAddress+"/carehome/bill-to/?is-active=1&is-deleted=0&fields=*,whomToBill.value","GET",getActivityTypes,onError);
}
var tempCompType = [];
function getActivityTypes(dataObj){
    // console.log(dataObj);
    tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.billTo){
            if($.isArray(dataObj.response.billTo)){
                tempCompType = dataObj.response.billTo;
            }else{
                tempCompType.push(dataObj.response.billTo);
            }
        }
    }

    for(var i=0;i<tempCompType.length;i++){
        tempCompType[i].idk = tempCompType[i].id;
    }

    buildDeviceListGrid(tempCompType);
}
function onError(errorObj){
    console.log(errorObj);
}

function buttonEvents(){
    $("#btnSave").off("click",onClickSave);
    $("#btnSave").on("click",onClickSave);

    $("#btnEdit").off("click",onClickEdit);
    $("#btnEdit").on("click",onClickEdit);

    $("#btnDelete").off("click",onClickDelete);
    $("#btnDelete").on("click",onClickDelete);

    $("#btnAdd").off("click");
    $("#btnAdd").on("click",onClickAdd);

    $("#btnCancel").off("click");
    $("#btnCancel").on("click",onClickCancel);

    $("#btnReset").off("click",onClickReset);
    $("#btnReset").on("click",onClickReset);

    $(".popupClose").off("click", onClickCancel);
    $(".popupClose").on("click", onClickCancel);

    $("#btnBillToZipSearch").off("click");
    $("#btnBillToZipSearch").on("click", onClickZipSearch);

    $("#btnCityToZipSearch").off("click");
    $("#btnCityToZipSearch").on("click", onClickZipSearch);

    $("#cmbBillToWhom").change(function() {
        var selectText = $("#cmbBillToWhom option:selected").text();
        if(selectText.toLowerCase() != "person" && selectText.toLowerCase() != "individual"){
            $("#divPerson").css("display","");
        }else{
            $("#divPerson").css("display","none");
        }
    });

    $(".btnActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('active');
        onClickActive();
    });
    $(".btnInActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('inactive');
        onClickInActive();
    });
    // allowAlphabets("txtAbbreviation");
    allowAlphaNumericwithSapce("txtName");
    // allowPhoneNumber("txtHomePhone");
    // allowPhoneNumber("txtWorkPhone");
    // allowPhoneNumber("txtMobilePhone");
}
var allRosterRowCount = 0;
var allRosterRowIndex = 0;
var allRosterRows = [];

function searchOnLoad(status) {
    buildDeviceListGrid([]);
    if(status == "active") {
        var urlExtn = ipAddress + "/carehome/bill-to/?is-active=1&is-deleted=0&fields=*,whomToBill.value";
    }
    else if(status == "inactive") {
        var urlExtn =  ipAddress + "/carehome/bill-to/?is-active=0&is-deleted=1&fields=*,whomToBill.value";
    }
    getAjaxObject(urlExtn,"GET",getActivityTypes,onError);
}


function onClickAdd(){
    $("#addPopup").show();
    $('#viewDivBlock').hide();
    $("#txtID").hide();
    $(".filter-heading").html("Add Bill To");
    parentRef.operation = "add";
    IsFlag = 0;
    onClickReset();
}
function onClickActive() {
    $(".btnInActive").removeClass("radioButton-active");
    $(".btnActive").addClass("radioButton-active");
}

function onClickInActive() {
    $(".btnActive").removeClass("radioButton-active");
    $(".btnInActive").addClass("radioButton-active");
}

function onClickDelete(){
    customAlert.confirm("Confirm", "Are you sure to delete?",function(response){
        if(response.button == "Yes"){
            var dataObj = {};
            dataObj.createdBy = Number(sessionStorage.userId);
            dataObj.isDeleted = 1;
            dataObj.isActive = 0;
            dataObj.id = atID;
            var dataUrl = ipAddress +"/carehome/bill-to/?id="+ atID ;
            var method = "DELETE";
            createAjaxObject(dataUrl, dataObj, method, onCreateDelete, onError);
        }
    });
}

function onCreateDelete(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "Whom to bill deleted successfully";
            customAlert.error("Info", msg);
            $("#txtAT").val("");
            operation = ADD;
            init();
        }
    }
}
function onClickSave(){
    var sEmail = $('#txtEmail').val();
    if ((sEmail != "" && validateEmail(sEmail) || sEmail == "")) {
        var strPersonName = $("#txtContactName").val();
        strPersonName = $.trim(strPersonName);

        var strName = $("#txtName").val();
        strName = $.trim(strName);

        var dataObj = {};
        dataObj.createdBy = Number(sessionStorage.userId);
        if(parseInt($("#cmbStatus").val()) == 1){
            dataObj.isDeleted = 0;
        }
        else{
            dataObj.isDeleted = 1;
        }
        var terms = $("#txtTerms").val();
        if(terms != "") {
            dataObj.terms = terms;
        }
        else{
            dataObj.terms = null;
        }

        dataObj.isActive = parseInt($("#cmbStatus").val());
        dataObj.contactPersonName = strPersonName;
        dataObj.name = strName;
        dataObj.whomToBillId = $("#cmbBillToWhom").val();
        dataObj.department = ($("#txtDepartment").val() != "") ? $("#txtDepartment").val() : null ;

        var dataUrl = ipAddress +"/carehome/bill-to/";
        var method = "POST";
        if(operation == UPDATE){
            method = "PUT";
            dataObj.id = atID;
            billToId = atID;
        }
        if(strName != "") {
            if(checkBillToName()){
                createAjaxObject(dataUrl, dataObj, method, onCreate, onError);
            }else{
                customAlert.error("Error","Bill To Name is existed. Please enter another Name");
            }
        }
        else{
            customAlert.error("Error","Please Fill All required fields");
        }
    }
    else{
        customAlert.error("Error","your EmailId is invalid,Please enter the valid EmailId");
    }


}

function onCreate(dataObj) {
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            // var msg = "Whom to bill created successfully";
            if (operation == UPDATE) {

            } else {
                billToId = dataObj.response['bill-to'].id;

            }
            // displaySessionErrorPopUp("Info", msg, function (res) {
            //     // $("#txtAT").val("");
            //     operation = ADD;
            //     init();
            // })
            IsFlag = 0;
            saveBillToCommunication(dataObj);
        } else {
            customAlert.error("Error", dataObj.response.status.message);
        }
    }
}

function onClickCancel(){
    $(".filter-heading").html("View Bill To");
    $("#viewDivBlock").show();
    $("#addPopup").hide();
}

function adjustHeight(){
    var defHeight = 230;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    angularUIgridWrapper.adjustGridHeight(cmpHeight);
}

function buildDeviceListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Name",
        "field": "name"
    });
    gridColumns.push({
        "title": "Bill To Entity",
        "field": "whomToBillValue"
    });

    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
}

function onChange(){
    setTimeout(function() {
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if (selectedItems && selectedItems.length > 0) {
            var obj = selectedItems[0];
            atID = obj.idk;
            $("#btnEdit").prop("disabled", false);
            $("#btnDelete").prop("disabled", false);
        } else {
            $("#btnEdit").prop("disabled", true);
            $("#btnDelete").prop("disabled", true);
        }
    });
}

function onClickEdit(){
    onClickReset();
    $("#txtID").show();
    $(".filter-heading").html("Edit Bill To");
    parentRef.operation = "edit";
    $("#viewDivBlock").hide();
    $("#addPopup").show();
    var selectedItems = angularUIgridWrapper.getSelectedRows();
    if (selectedItems && selectedItems.length > 0) {
        var obj = selectedItems[0];
        atID = obj.idk;
        billToId = obj.idk;
        IsFlag = 1;

        $("#txtID").html("ID :" +obj.idk);
        $("#cmbBillToWhom").val(obj.whomToBillId);
        $("#cmbStatus").val(obj.isActive);
        $("#txtContactName").val(obj.contactPersonName);
        $("#txtName").val(obj.name);
        if(obj.terms != null)
            $("#txtTerms").val(obj.terms);
        if(obj.department != null){
            $("#txtDepartment").val(obj.department);
        }
        else{
            $("#txtDepartment").val("");
        }
        getAjaxObject(ipAddress + "/homecare/communications/?parentId="+ billToId +"&parentTypeId=901&fields=*,address.*", "GET", saveBillToCommunication, onError);
    }
}

var operation = "add";

function onStatusChange(){
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if(cmbStatus && cmbStatus.selectedIndex<0){
        cmbStatus.select(0);
    }
}

function onClickReset() {
    if(operation == ADD) {
        operation = ADD;
    }
    else {
        operation = UPDATE;
    }

    $("#txtContactName").val("");
    $("#txtName").val("");
    $("#txtHomePhone").val("");
    $("#txtWorkPhone").val("");
    $("#txtMobilePhone").val("");
    $("#txtFax").val("");
    $("#txtCountry").val("");
    $("#txtPostalCode").val("");
    $("#txtEmail").val("");
    $("#cmbStatus").val("");
    $("#txtAddress1").val("");
    $("#txtAddress2").val("");
    $("#txtCity").val("");
    $("#txtCounty").val("");
    $("#txtDepartment").val("");
    $("#txtTerms").val("");
}

function validations(){
    allowAlphabets("txtContactName");
    allowAlphabets("txtName");
    allowPhoneNumber("txtHomePhone");
    allowPhoneNumber("txtWorkPhone");
    allowPhoneNumber("txtMobilePhone");
    validateEmail("txtEmail");
}

var $selfZipRow = "";
function onClickZipSearch() {
    var popW = 600;
    var popH = 500;

    parentRef.searchZip = true;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search Zip";
    var cntry = sessionStorage.countryName;
    if (cntry.indexOf("India") >= 0) {
        profileLbl = "Search Postal Code";
    } else if (cntry.indexOf("United Kingdom") >= 0) {
        if(IsPostalCodeManual == "1"){
            profileLbl = "Search City";
        }
        else{
            profileLbl = "Search Postal Code";
        }
    } else if (cntry.indexOf("United State") >= 0) {
        profileLbl = "Search Zip";
    } else {
        profileLbl = "Search Zip";
    }
    $selfZipRow = $(this);
    devModelWindowWrapper.openPageWindow("../../html/masters/zipList.html", profileLbl, popW, popH, true, onCloseSearchZipAction);

    if (!$(this).closest('tr').attr('data-item-edited') == true && !$(this).closest('tr').attr('data-item-new') == true) {
        $(this).closest('tr').attr('data-item-edited', true);
    }
}

var zipSelItem = null;

function onCloseSearchZipAction(evt, returnData) {
    console.log(returnData);
    if (returnData && returnData.status == "success") {
        var selItem = returnData.selItem;
        if (selItem) {

            $('#txtCountry').val('');
            $('#txtPostalCode').val('');
            $('#txtCounty').val('');
            $('#txtCity').val('');

            zipSelItem = selItem;
            cityId = zipSelItem.idk;
            $(this).find('input').attr('data-attr-city-id', cityId);
            if(IsPostalFlag != "1") {
                if (zipSelItem.zip) {
                    $('#txtPostalCode').val(zipSelItem.zip);
                }
            }

            if (zipSelItem.country) {
                $('#txtCountry').val(zipSelItem.country);
            }
            if (zipSelItem.state) {
                $('#txtCounty').val(zipSelItem.state);
            }

            if (zipSelItem.city) {
                $('#txtCity').val(zipSelItem.city);
            }
        }

    }
}

function onGetBillType(){
    getAjaxObject(ipAddress+"/homecare/whom-to-bill/?is-active=1&is-deleted=0","GET",getWhomtoBillValues,onError);
}

function getWhomtoBillValues(dataObj) {
    var dataArray
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            dataArray = dataObj.response.whomtobill || [];
        }
    }
    for(var i=0;i<dataArray.length;i++){
        $("#cmbBillToWhom").append('<option value="'+dataArray[i].id+'">'+dataArray[i].code+'</option>');
    }
}
var communicationId = null;
function saveBillToCommunication(obj) {
    if (obj && obj.response && obj.response.status && obj.response.status.code == "1") {
        if (IsFlag == 1){
            if(obj.response.communications){
                $("#txtAddress1").val(obj.response.communications[0].address1);
                $("#txtAddress2").val(obj.response.communications[0].address2);
                $("#txtCountry").val(obj.response.communications[0].addressCountry);
                $("#txtEmail").val(obj.response.communications[0].email);
                $("#txtFax").val(obj.response.communications[0].fax);
                if(IsPostalFlag == "1") {
                    $("#txtPostalCode").val(obj.response.communications[0].houseNumber);
                }
                else{
                    $("#txtPostalCode").val(obj.response.communications[0].addressZip);
                }
                $("#txtHomePhone").val(obj.response.communications[0].homePhone);
                $("#txtMobilePhone").val(obj.response.communications[0].cellPhone);
                $("#txtCounty").val(obj.response.communications[0].addressState);
                $("#txtCity").val(obj.response.communications[0].addressCity);
                cityId = obj.response.communications[0].cityId;
                communicationId= obj.response.communications[0].id;
            }
        }
        else{
            var reqObj = {};
            var updateArr = [];
            reqObj.address1 = $("#txtAddress1").val();
            reqObj.address2 = $("#txtAddress2").val();
            reqObj.cellPhone = $("#txtMobilePhone").val();
            reqObj.homePhone = $("#txtHomePhone").val();

            if(zipSelItem){
                reqObj.cityId = zipSelItem.idk;
            }
            else{
                reqObj.cityId = cityId;
            }
            if(IsPostalFlag == "1") {
                reqObj.houseNumber = $("#txtPostalCode").val();
            }

            reqObj.createdBy= parseInt(sessionStorage.userId);
            reqObj.defaultCommunication= 1
            reqObj.email= $("#txtEmail").val();
            reqObj.fax= $("#txtFax").val();
            reqObj.isActive = 1;
            reqObj.isDeleted= 0;
            reqObj.parentTypeId = 901;
            reqObj.sms = "yes";
            reqObj.parentId = billToId;
            if(communicationId){
                if(communicationId != null){
                    reqObj.id = communicationId;
                    updateArr.push(reqObj);
                    createAjaxObjectAsync(ipAddress + '/homecare/communications/batch/', updateArr, "PUT", getBillToCommunictiondetails, onError);
                }
            }
            else
            {
                updateArr.push(reqObj);
                createAjaxObjectAsync(ipAddress + '/homecare/communications/batch/', updateArr, "POST", getBillToCommunictiondetails, onError);
            }
        }
    }
}

function getBillToCommunictiondetails(obj) {
    if (obj && obj.response && obj.response.status && obj.response.status.code == "1") {
        var msg = "Bill To created successfully";
        if (operation == UPDATE) {
            msg = "Bill To updated successfully"
        }
        displaySessionErrorPopUp("Info", msg, function (res) {
            operation = ADD;
            init();
            onClickCancel();
            onClickActive();
        })
    }
}

function getCountryIsPostalCode() {
    if (cntry.indexOf("United Kingdom") >= 0) {
        if (IsPostalCodeManual == "1") {
            IsPostalFlag = "1";
            $("#btnCityToZipSearch").css("display","");
            $("#lblCity").html("City");
            $("#txtPostalCode").attr("readonly", false);
            $("#btnBillToZipSearch").css("display","none");
        }

    }
}

function checkBillToName(){
    var strName = $("#txtName").val();
    strName = $.trim(strName);
    var IsAdd = 0;
    var IsEdit = 0;

    if(operation == ADD) {
        for (var i = 0; i < tempCompType.length; i++) {
            if(tempCompType[i].name.toLowerCase() == strName.toLowerCase()){
                IsAdd = IsAdd + 1;
            }
        }
    }
    else {
        for (var i = 0; i < tempCompType.length; i++) {
            if (tempCompType[i].name.toLowerCase() == strName.toLowerCase()) {
                IsEdit = IsEdit + 1;
            }
        }
    }

    if((IsAdd != 0) || (IsEdit >1)){
        return false;
    }
    else {
        return true;
    }
}


function themeAPIChange(){
    getAjaxObject(ipAddress+"/homecare/settings/?id=2","GET",getThemeValue,onError);
}

function getThemeValue(dataObj){
    console.log(dataObj);
    var tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.activityTypes){
            if($.isArray(dataObj.response.settings)){
                tempCompType = dataObj.response.settings;
            }else{
                tempCompType.push(dataObj.response.settings);
            }
        }
        if(tempCompType.length > 0){
            themesChange(tempCompType[0].value);
        }
    }
}

function themesChange(themeValue){
    if(themeValue == 2){
        loadAPi("../../Theme2/Theme02.css");
    }
    else if(themeValue == 3){
        loadAPi("../../Theme3/Theme03.css");
    }
}

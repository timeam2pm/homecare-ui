var angularUIgridWrapper;
var angularUISelgridWrapper;

var parentRef = null;
var patientId = "";

var activityid = "1";

var ADL = "1";
var IADL = "2";
var CARE = "3";
var FLUID = "4";
var FOOD = "5";
var menuLoaded = false;

var typeArr = [{Key:'ADL',Value:'1'},{Key:'iADL',Value:'2'},{Key:'Care',Value:'3'},{Key:'Fluid',Value:'4'},{Key:'Food',Value:'5'}];
var compType = [{Key:'Text',Value:'1'},{Key:'Text Area',Value:'2'},{Key:'Boolean',Value:'3'},{Key:'Checkbox',Value:'4'},{Key:'Radio Button',Value:'5'}];

var selDate = null;
var facility = null;
var provider = null;
var provider1 = null;
var facilityId = null;
var providerId = null;
var provider1Id = null;
$(document).ready(function(){
	parentRef = parent.frames['iframe'].window;
	patientId = parentRef.patientId;
	selDate = parentRef.selDate;
	facility = parentRef.facility;
	provider = parentRef.provider;
	provider1 = parentRef.provider1;
	
	facilityId = parentRef.facilityId;
	providerId = parentRef.providerId;
	provider1Id = parentRef.provider1Id;
	
	var strLbl1 = provider;
	$("#lbl1").text(strLbl1);
	
	var strLbl2 = provider1;//+" Appointments"
	$("#lbl2").text(strLbl2);
	
	var dataOptions = {
        pagination: false,
        changeCallBack: onChange
	}
	angularUIgridWrapper = new AngularUIGridWrapper("dgAvlProviderActivityList", dataOptions);
	angularUIgridWrapper.init();
	buildFileResourceListGrid([]);
	
	var dataOptions1 = {
	        pagination: false,
	        changeCallBack: onChange1
		}
		angularUISelgridWrapper = new AngularUIGridWrapper("dgSelProviderActivityList", dataOptions1);
		angularUISelgridWrapper.init();
		buildFileResourceSelListGrid([]);
		
		getProviderAppList();
});


$(window).load(function(){
	$(window).resize(adjustHeight);
	//loadMenus();
	onLoaded();
	if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function getProviderAppList(){
	var dt = selDate;
	var day = dt.getDate();
	var month = dt.getMonth();
	month = month+1;
	var year = dt.getFullYear();
	
	var stDate = month+"/"+day+"/"+year;
	stDate = stDate+" 00:00:00";
	
	var startDate = new Date(stDate);
	var stDateTime = startDate.getTime();
	
	var etDate = month+"/"+day+"/"+year;
	etDate = etDate+" 23:59:59";
	
	var endtDate = new Date(etDate);
	var edDateTime = endtDate.getTime();
	var patientListURL = ipAddress+"/appointment/list/?facility-id="+facilityId+"&provider-id="+providerId+"&to-date="+edDateTime+"&from-date="+stDateTime+"&is-active=1";//+"&is-active=1&is-deleted=0"
	getAjaxObject(patientListURL,"GET",onStaffPatientList2DData,onError);
}
var pt2dAppTimeArray = [];
function onStaffPatientList2DData(dataObj){
	console.log(dataObj);
	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		if($.isArray(dataObj.response.appointment)){
			pt2dAppTimeArray = dataObj.response.appointment;
		}else{
			pt2dAppTimeArray.push(dataObj.response.appointment);
		}
	}
	var items = [];
	var selAMCount = 0;
	var selPMCount = 0;
	for(var i=0;i<pt2dAppTimeArray.length;i++){
		var item = pt2dAppTimeArray[i];
		if(item){
			item.idk = item.id;
			item.pName = item.composition.patient.firstName;
			var dt = new Date(item.dateOfAppointment);
			//console.log(dt.getHours());
			if(dt.getHours()>=12){
				selPMCount = selPMCount+1;
			}else{
				selAMCount = selAMCount+1;
			}
			item.tm = kendo.toString(dt,"h:mm:ss tt");
			items.push(item);
		}
	}
	$("#spnSelAM").text(selAMCount);
	$("#spnSelPM").text(selPMCount);
	var total = Number(selAMCount)+Number(selPMCount);
	$("#spnSelAMTotal").text(total);
	buildFileResourceSelListGrid(items);
	getProvider1List();
}

function getProvider1List(){
	var dt = selDate;
	var day = dt.getDate();
	var month = dt.getMonth();
	month = month+1;
	var year = dt.getFullYear();
	
	var stDate = month+"/"+day+"/"+year;
	stDate = stDate+" 00:00:00";
	
	var startDate = new Date(stDate);
	var stDateTime = startDate.getTime();
	
	var etDate = month+"/"+day+"/"+year;
	etDate = etDate+" 23:59:59";
	
	var endtDate = new Date(etDate);
	var edDateTime = endtDate.getTime();
	var patientListURL = ipAddress+"/appointment/list/?facility-id="+facilityId+"&provider-id="+provider1Id+"&to-date="+edDateTime+"&from-date="+stDateTime+"&is-active=1";//+"&is-active=1&is-deleted=0"
	getAjaxObject(patientListURL,"GET",onStaffPatientList2DData1,onError);
}
var pt2dAppTimeArray1 = [];
function onStaffPatientList2DData1(dataObj){
	console.log(dataObj);
	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		if($.isArray(dataObj.response.appointment)){
			pt2dAppTimeArray1 = dataObj.response.appointment;
		}else{
			pt2dAppTimeArray1.push(dataObj.response.appointment);
		}
	}
	var items = [];
	var AvlAMCount = 0;
	var AvlPMCount = 0;
	for(var i=0;i<pt2dAppTimeArray1.length;i++){
		var item = pt2dAppTimeArray1[i];
		if(item){
			item.idk = item.id;
			item.pName = item.composition.patient.firstName;
			var dt = new Date(item.dateOfAppointment);
			//console.log(dt.getHours());
			if(dt.getHours()>=12){
				AvlPMCount = AvlPMCount+1;
			}else{
				AvlAMCount = AvlAMCount+1;
			}
			item.tm = kendo.toString(dt,"h:mm:ss tt");
			items.push(item);
		}
	}
	$("#spnAvlAM").text(AvlAMCount);
	$("#spnAvlPM").text(AvlPMCount);
	var tot = Number(AvlAMCount)+Number(AvlPMCount);
	$("#spnAvlTotal").text(AvlPMCount);
	buildFileResourceListGrid(items);
}
function loadMenus(){
	var strMenu = "";
	$("#setupPlanMenu").html("");
	for(var i=0;i<typeArr.length;i++){
		var item = typeArr[i];
		var strItem = item.Key;
		var strVal = item.Value;
		if(i == 0){
			strMenu = strMenu+'<a href="#" class="list-group-item sub-item paddingLeftStyle subMenuStyle selectedMenu" id='+strVal+' name='+strVal+'>'+strItem+'</a>';
		}else{
			strMenu = strMenu+'<a href="#" class="list-group-item sub-item paddingLeftStyle subMenuStyle" id='+strVal+' name='+strVal+'>'+strItem+'</a>';
		}
		
	}
	console.log(strItem);
	$("#setupPlanMenu").append(strMenu);
	for(var j=0;j<typeArr.length;j++){
		var item = typeArr[j];
		var strItem = item.Key;
		var strVal = item.Value;
		$("#"+strVal).off("click");
		$("#"+strVal).on("click",onClickItem);
	}
}
function onClickItem(evt){
	console.log(evt);
	removeSelections();
	var strId = evt.currentTarget.id;
	$("#"+strId).addClass("selectedMenu");
	var strId = evt.currentTarget.name;
	var strActivity = getActivityNameById(strId);
	$("#lblActivity").text(strActivity);
	activityid = strId;
	showGrid();
	//init();
}
function removeSelections(){
	for(var i=0;i<typeArr.length;i++){
		var item = typeArr[i];
		var strItem = item.Key;
		var strVal = item.Value;
		$("#"+strVal).removeClass("selectedMenu");
	}
}
function onLoaded(){
	init();
    buttonEvents();
	adjustHeight();
}

var recordType = "";
function init(){
	//getAjaxObject(ipAddress+"/master/type/list/?name=component","GET",getComponentTypes,onError);
}
function getComponentTypes(dataObj){
	var tempCompType = [];
	compType = [];
	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		if(dataObj.response.typeMaster){
			if($.isArray(dataObj.response.typeMaster)){
				tempCompType = dataObj.response.typeMaster;
			}else{
				tempCompType.push(dataObj.response.typeMaster);
			}
		}
	}
	for(var i=0;i<tempCompType.length;i++){
		var obj = {};
		obj.Key = tempCompType[i].type;
		obj.Value = tempCompType[i].id;
		compType.push(obj);
	}
	getAjaxObject(ipAddress+"/master/type/list/?name=activity","GET",getActivityTypes,onError);
}

function getActivityTypes(dataObj){
	var tempCompType = [];
	typeArr = [];
	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		if(dataObj.response.typeMaster){
			if($.isArray(dataObj.response.typeMaster)){
				tempCompType = dataObj.response.typeMaster;
			}else{
				tempCompType.push(dataObj.response.typeMaster);
			}
		}
	}
	for(var i=0;i<tempCompType.length;i++){
		var obj = {};
		obj.Key = tempCompType[i].type;
		obj.Value = tempCompType[i].id;
		typeArr.push(obj);
	}
	if(!menuLoaded){
		//loadMenus();
	}
	showGrid();
}
function showGrid(){
	buildFileResourceListGrid([]);
	buildFileResourceSelListGrid([]);
	
	getAjaxObject(ipAddress+"/patient/activity/list/"+patientId,"GET",getPatientDietList,onError);
}
function onError(errorObj){
	console.log(errorObj);
}
var interestArr = [];
var dataArray = [];

function getPatientDietList(dataObj){
	console.log(dataObj);
	interestArr = [];
	 if(dataObj && dataObj.response && dataObj.response.patientActivity){
			if($.isArray(dataObj.response.patientActivity)){
				interestArr = dataObj.response.patientActivity;
			}else{
				interestArr.push(dataObj.response.patientActivity);
			}
		}
	 var actArr = [];
	 for(var i=0;i<interestArr.length;i++){
		 if(interestArr[i].activity.activityTypeId == activityid){
			 interestArr[i].idk = interestArr[i].id;
			 interestArr[i].activityID = getActivityNameById(interestArr[i].activity.activityTypeId);
			 interestArr[i].charge =  interestArr[i].activity.charge;
			 interestArr[i].duration =  interestArr[i].activity.duration;
			 interestArr[i].description =  interestArr[i].activity.description;
			 interestArr[i].activity = interestArr[i].activity.activity;
			 
			 actArr.push(interestArr[i]);
		 }
	 }
	buildFileResourceListGrid(actArr);
	getAjaxObject(ipAddress+"/activity/list/","GET",getActivityList,onError);
}
function getActivityList(dataObj){
	console.log(dataObj);
	dataArray = [];
	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		if($.isArray(dataObj.response.activity)){
			dataArray = dataObj.response.activity;
		}else{
			dataArray.push(dataObj.response.activity);
		}
	}
	var tempDataArray = [];
	for(var i=0;i<dataArray.length;i++){
		if(dataArray[i].id && dataArray[i].activityTypeId == activityid){
			dataArray[i].idk = dataArray[i].id; 
			dataArray[i].Status = "InActive";
			if(dataArray[i].isActive == 1){
				dataArray[i].Status = "Active";
			}
			dataArray[i].activityID = getActivityNameById(dataArray[i].activityTypeId);
			dataArray[i].Type = getTypeNameById(dataArray[i].componentId);
			if(!getFileExist(dataArray[i].id)){
				tempDataArray.push(dataArray[i]);
			}
		}
	}
	buildFileResourceSelListGrid(tempDataArray);
}
function getActivityNameById(aId){
	for(var i=0;i<typeArr.length;i++){
		var item = typeArr[i];
		if(item && item.Value == aId){
			return item.Key;
		}
	}
	return "";
}
function getTypeNameById(aId){
	for(var i=0;i<compType.length;i++){
		var item = compType[i];
		if(item && item.Value == aId){
			return item.Key;
		}
	}
	return "";
}
function getFileExist(id1){
	var flag = false;
	for(var i=0;i<interestArr.length;i++){
		var dataObj = interestArr[i];
		if(dataObj && dataObj.activityId == id1){
			flag = true;
			break;
		}
	}
	return flag;
}
function buttonEvents(){
	$("#btnCancelDet").off("click",onClickCancel);
	$("#btnCancelDet").on("click",onClickCancel);
	
	$("#btnAdd").off("click");
	$("#btnAdd").on("click",onClickAdd);
	
	$("#btnSubRight").off("click");
	$("#btnSubRight").on("click",onClickSubRight);
	
	$("#btnSubLeft").off("click");
	$("#btnSubLeft").on("click",onClickSubLeft);
	
	$("#chkSelAM").off("click");
	$("#chkSelAM").on("click",onClickSelAM);
	
	$("#chkSelPM").off("click");
	$("#chkSelPM").on("click",onClickSelPM);
	
	$("#chkAvlAM").off("click");
	$("#chkAvlAM").on("click",onClickAvlAM);
	
	$("#chkAvlPM").off("click");
	$("#chkAvlPM").on("click",onClickAvlPM);
}

function onClickAddADL(){
	$("#btnADL").addClass("selectButtonBarClass");
	$("#btniADL").removeClass("selectButtonBarClass");
	$("#btnCare").removeClass("selectButtonBarClass");
	$("#btnFluid").removeClass("selectButtonBarClass");
	$("#btnFood").removeClass("selectButtonBarClass");
	activityid = ADL;
	init();
}
function onClickAddiADL(){
	$("#btnADL").removeClass("selectButtonBarClass");
	$("#btniADL").addClass("selectButtonBarClass");
	$("#btnCare").removeClass("selectButtonBarClass");
	$("#btnFluid").removeClass("selectButtonBarClass");
	$("#btnFood").removeClass("selectButtonBarClass");
	activityid = IADL;
	init();
}
function onClickAddCare(){
	$("#btnADL").removeClass("selectButtonBarClass");
	$("#btniADL").removeClass("selectButtonBarClass");
	$("#btnCare").addClass("selectButtonBarClass");
	$("#btnFluid").removeClass("selectButtonBarClass");
	$("#btnFood").removeClass("selectButtonBarClass");
	activityid = CARE;
	init();
}
function onClickAddFluid(){
	$("#btnADL").removeClass("selectButtonBarClass");
	$("#btniADL").removeClass("selectButtonBarClass");
	$("#btnCare").removeClass("selectButtonBarClass");
	$("#btnFluid").addClass("selectButtonBarClass");
	$("#btnFood").removeClass("selectButtonBarClass");
	activityid = FLUID;
	init();
}
function onClickAddFood(){
	$("#btnADL").removeClass("selectButtonBarClass");
	$("#btniADL").removeClass("selectButtonBarClass");
	$("#btnCare").removeClass("selectButtonBarClass");
	$("#btnFluid").removeClass("selectButtonBarClass");
	$("#btnFood").addClass("selectButtonBarClass");
	activityid = FOOD;
	init();
}
function adjustHeight(){
	var defHeight = 170;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    if(angularUIgridWrapper){
    	angularUIgridWrapper.adjustGridHeight(cmpHeight);
    }
	if(angularUISelgridWrapper){
		angularUISelgridWrapper.adjustGridHeight(cmpHeight);
	}
	
	 $("#divButttons").height(cmpHeight+50);
}
function toggleSelectAll(e) {
    var checked = e.currentTarget.checked;
    var rows = angularUIgridWrapper.getScope().gridApi.core.getVisibleRows(); 
    for (var i = 0; i < rows.length; i++) {
        rows[i].entity["SEL"] = checked;
    }
}
function toggleSelectAll1(e) {
    var checked = e.currentTarget.checked;
    var rows = angularUISelgridWrapper.getScope().gridApi.core.getVisibleRows(); 
    for (var i = 0; i < rows.length; i++) {
        rows[i].entity["SEL"] = checked;
    }
}

function onClickSelAM(){
	var chkSelAM = $("#chkSelAM").is(':checked');
	
	var selRows = angularUISelgridWrapper.getAllRows();
	var dArray = [];
	for(var i=0;i<selRows.length;i++){
		var rowObj = selRows[i].entity;
		if(rowObj){
			var dt = new Date(rowObj.dateOfAppointment);
			if(dt.getHours()<12){
				rowObj.SEL = chkSelAM;
			}
		}
	}
	angularUISelgridWrapper.refreshGrid();
}
function onClickAvlAM(){
	var chkAvlAM = $("#chkAvlAM").is(':checked');
	
	var selRows = angularUIgridWrapper.getAllRows();
	var dArray = [];
	for(var i=0;i<selRows.length;i++){
		var rowObj = selRows[i].entity;
		if(rowObj){
			var dt = new Date(rowObj.dateOfAppointment);
			if(dt.getHours()<12){
				rowObj.SEL = chkAvlAM;
			}
		}
	}
	angularUIgridWrapper.refreshGrid();
}
function onClickAvlPM(){
	var chkAvlAM = $("#chkAvlPM").is(':checked');
	
	var selRows = angularUIgridWrapper.getAllRows();
	var dArray = [];
	for(var i=0;i<selRows.length;i++){
		var rowObj = selRows[i].entity;
		if(rowObj){
			var dt = new Date(rowObj.dateOfAppointment);
			if(dt.getHours()>=12){
				rowObj.SEL = chkAvlAM;
			}
		}
	}
	angularUIgridWrapper.refreshGrid();
}

function onClickSelPM(){
var chkSelAM = $("#chkSelPM").is(':checked');
	
	var selRows = angularUISelgridWrapper.getAllRows();
	var dArray = [];
	for(var i=0;i<selRows.length;i++){
		var rowObj = selRows[i].entity;
		if(rowObj){
			var dt = new Date(rowObj.dateOfAppointment);
			if(dt.getHours()>=12){
				rowObj.SEL = chkSelAM;
			}
		}
	}
	angularUISelgridWrapper.refreshGrid();
}
function buildFileResourceListGrid(dataSource) {
	var gridColumns = [];
	var otoptions = {};
	otoptions.noUnselect = false;
	 gridColumns.push({
	        "title": "Select",
	        "field": "SEL",
	        "cellTemplate": showCheckBoxTemplate(),
	        "headerCellTemplate": "<input type='checkbox' class='check1' ng-model='TCSELECT' onclick='toggleSelectAll(event);' style='width:20px;height:20px;'></input>",
	        "width":"15%"
		});
	 gridColumns.push({
	        "title": "Service User",
	        "field": "pName",
		});
    gridColumns.push({
        "title": "Time",
        "field": "tm",
	});
    gridColumns.push({
        "title": "Duration",
        "field": "duration",
	});
   angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions); 
	adjustHeight();
	//onIndividualRowClick();
}
var prevSelectedItem =[];
function onChange(){
	
}
function buildFileResourceSelListGrid(dataSource) {
	var gridColumns = [];
	var otoptions = {};
	otoptions.noUnselect = false;
	 gridColumns.push({
	        "title": "Select",
	        "field": "SEL",
	        "cellTemplate": showCheckBoxTemplate(),
	        "headerCellTemplate": "<input type='checkbox' class='check1' ng-model='TCSELECT' onclick='toggleSelectAll1(event);' style='width:20px;height:20px;'></input>",
	        "width":"15%"
		});
	 gridColumns.push({
	        "title": "Service User",
	        "field": "pName",
		});
	    gridColumns.push({
	        "title": "Time",
	        "field": "tm",
		});
	    gridColumns.push({
	        "title": "Duration",
	        "field": "duration",
		});
   angularUISelgridWrapper.creategrid(dataSource, gridColumns,otoptions); 
	adjustHeight();
	//onIndividualRowClick();
}
var prevSelectedItem =[];
function onChange1(){
	
}

function showCheckBoxTemplate(){
	var node = '<div style="text-align:center;padding-top: 6px;"><input type="checkbox" class="check1" id="chkbox" ng-model="row.entity.SEL" style="width:20px;height:20px;margin:0px;"   onclick="onSelect(event);"></input></div>';
	return node;
}
function onSelect(evt){
	console.log(evt);
}

function onClickSubRight(){
//	alert("right");
	 var selGridData = angularUISelgridWrapper.getAllRows();
	 var selList = [];
	    for (var i = 0; i < selGridData.length; i++) {
	        var dataRow = selGridData[i].entity;
	        if(dataRow.SEL){
	        	angularUISelgridWrapper.deleteItem(dataRow);
	        	//dataRow.SEL = false;
	        	angularUIgridWrapper.insert(dataRow);
	        }
	    }  
}
function onClickSubLeft(){
	//alert("click");
	 var selGridData = angularUIgridWrapper.getAllRows();
	 var selList = [];
	    for (var i = 0; i < selGridData.length; i++) {
	        var dataRow = selGridData[i].entity;
	        if(dataRow.SEL){
	        	angularUIgridWrapper.deleteItem(dataRow);
	        	angularUISelgridWrapper.insert(dataRow);
	        }
	    }  
}
function onClickAdd(){
	var rows = angularUIgridWrapper.getAllRows();
	var selRows = angularUISelgridWrapper.getAllRows();
	var dArray = [];
	for(var i=0;i<rows.length;i++){
		var rowObj = rows[i].entity;
		if(rowObj){
			rowObj.providerId1 = provider1Id;
			dArray.push(rowObj);
		}
	}
	
	for(var j=0;j<selRows.length;j++){
		var rowObj = selRows[j].entity;
		if(rowObj){
			rowObj.providerId1 = providerId;
			//dArray.push(rowObj);
		}
	}
	
	var appArray = [];
	for(var d=0;d<dArray.length;d++){
		var row = dArray[d];
		if(row){
			var obj = {};
			obj.createdBy = 101;
			obj.isActive = 1;
			obj.isDeleted = 0;
			
			obj.dateOfAppointment = Number(row.dateOfAppointment);
			obj.duration = Number(row.duration);
			obj.providerId = Number(row.providerId1);
			obj.facilityId = Number(row.facilityId);
			obj.patientId =  Number(row.patientId);
			obj.appointmentType = row.appointmentType;
			obj.notes = row.notes;
			obj.appointmentReason = row.appointmentReason;
			obj.id = row.idk;
			appArray.push(obj);
		}
	}
	console.log(appArray);
	
	var dataUrl = ipAddress+"/appointment/update";
	createAjaxObject(dataUrl,appArray,"POST",onCreate,onError);

	
}
function onCreate(dataObj){
	if(dataObj && dataObj.response && dataObj.response.status){
		if(dataObj.response.status.code == "1"){
			customAlert.error("Info", "Appointment updated successfully");
		}
	}
}
function popupClose(st){
	var onCloseData = new Object();
	onCloseData = st;
	var windowWrapper = new kendoWindowWrapper();
	windowWrapper.closePageWindow(onCloseData);
}
function onClickCancel(){
	var obj = {};
	var windowWrapper = new kendoWindowWrapper();
	windowWrapper.closePageWindow(obj);
}

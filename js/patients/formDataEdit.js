var parentRef = null;
var recordType = "1";
var dataArray = [];
var dataTypeArray = [{Key:'Text',Value:'1'},{Key:'Number',Value:'2'},{Key:'Boolean',Value:'3'}];
var unitArray =  [{Key:'Kg',Value:'1'},{Key:'Lt',Value:'2'}];
var statusArray =  [{Key:'ACTIVE',Value:'1'},{Key:'INACTIVE',Value:'2'}];
var selItem = null;
var selItem1 = null;
var resultDataObj = null;
$(document).ready(function(){
	parentRef = parent.frames['iframe'].window;
	//recordType = parentRef.dietType;
});

$(window).load(function(){
	loading = false;
	$(window).resize(adjustHeight);
onLoaded();
	if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
	selItem = parentRef.selItem;
	selItem1 = parentRef.selItem1;
	resultDataObj = parentRef.dataObj;
	if(selItem){
		 $("#lblFrmData").text(selItem.notes);
		buttonEvents();
		adjustHeight();
		setFormData();
	}
}
function init(){
	 $("#btnEdit").prop("disabled", true);
	 $("#btnDelete").prop("disabled", true);
	buildDeviceListGrid([]);
	//buildDeviceListGrid1([]);
	  var selectedItems = angularUIgridWrapper1.getSelectedRows();
      console.log(selectedItems);
      $("#lblFrmData").text("");
      if(selectedItems && selectedItems.length>0){
    	  $("#lblFrmData").text(selectedItems[0].notes);
    	  var idk = selectedItems[0].idk;
    	  	var ipaddress = ipAddress+"/homecare/template-values/?formTemplateId="+idk+"&patientId=101&fields=notes,patientId,formTemplateId,CreatedDate,providerId";
    		getAjaxObject(ipaddress,"GET",handleGetVacationList,onError);
      }
	
	//getAjaxObject(ipAddress+"/homecare/activity-types/?is-active=1&is-deleted=0&sort=display-order","GET",getActivityTypes,onError);
}
var frmDesignArray = [];
function handleGetVacationList(dataObj){
	console.log(dataObj);
	frmDesignArray = [];
	if(dataObj && dataObj.response && dataObj.response.status ){
		if(dataObj.response.status.code == "1"){
			if(dataObj.response.formTemplateValues){
				if($.isArray(dataObj.response.formTemplateValues)){
					frmDesignArray = dataObj.response.formTemplateValues;
				}else{
					frmDesignArray.push(dataObj.response.formTemplateValues);
				}
			}
		}
	}
	for(var i=0;i<frmDesignArray.length;i++){
		var dt = new Date(frmDesignArray[i].createdDate);
		dt = kendo.toString(dt, "MM-dd-yyyy h:mm:tt");
		frmDesignArray[i].crDate = dt;
	}
	buildDeviceListGrid(frmDesignArray);
	/*var strHtml = "";
	$("#divFormData").html("");
	for(var j=0;j<frmDesignArray.length;j++){
		var frmObj = frmDesignArray[j];
		frmObj.idk = frmObj.id;
		frmObj.length1 = frmObj.length;
		var frmId = "f"+frmObj.idk;
		var unit  ="";
		if(frmObj.unitValue){
			unit = frmObj.unitValue;
		}else{
		
		}
		strHtml = strHtml+'<div class="col-sm-6 col-md-6 col-lg-6 col-xs-6 form-group">';
		strHtml = strHtml+'<label class="col-xs-12 col-sm-12 col-md-12 col-lg-12 control-label input-sm noPadding">'+frmObj.name+' :</label>';
		strHtml = strHtml+'<div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 noPadding">';
		strHtml = strHtml+'<div class="col-sm-9 col-md-9 col-lg-9 col-xs-8 noPadding">';
		strHtml = strHtml+'	<input id="'+frmId+'" type="text" class="form-control input-sm"></input>';
		strHtml = strHtml+'</div>';
		strHtml = strHtml+'<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label input-sm noPadding">&nbsp;&nbsp;'+unit+'</label>';
		strHtml = strHtml+'</div></div>';
	}
	$("#divFormData").html(strHtml);*/
	//getFormData();
	//buildDeviceListGrid(vacation);
}
var fromArray = [];
var miniTemplates = [];
var COMP_TEXT = "text box";
var COMP_TEXT_AREA = "text area"
var COMP_BOOLEAN = "boolean";
var COMP_CHECK_BOX = "check box"
var COMP_RADIO_BUTTON = "radio button"
var COMP_GROUP_CHECK_BOX = "group check box"
var COMP_IMAGE = "image"
var COMP_DATE_PICKER = "date picker";

var DT_INT = "INT";
var DT_VARCHAR = "VARCHAR";
var DT_DATETIME = "DATETIME";
var DT_DECIMAL = "DECIMAL";
var DT_DATE = "DATE";
function setFormData(){
	var strHtml = "";
	$("#divPanel").html("");
	miniTemplates = resultDataObj.response.templateValues[0].componentValues;
	$("#txtNotes").val(resultDataObj.response.templateValues[0].notes);
	for(var m=0;m<miniTemplates.length;m++){
		var mItem = miniTemplates[m];
		if(mItem){
			$("#lblFrmData").text(mItem.miniTemplateName);
			mItem.idk = mItem.id;
			var frmId = "f"+mItem.id;
			var mValue = mItem.value;
			var unit  ="";
			mItem.frmId = frmId;
			if(mItem.miniUnits){
				unit = mItem.miniUnits;
			}else{
			
			}
			strHtml = strHtml+'<div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 form-group">';
			strHtml = strHtml+'<label class="col-xs-12 col-sm-12 col-md-12 col-lg-12 control-label input-sm noPadding">'+mItem.miniComponentName+' :</label>';
			strHtml = strHtml+'<div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 noPadding">';
			strHtml = strHtml+'<div class="col-sm-11 col-md-11 col-lg-11 col-xs-11 noPadding">';
			if(mItem.miniComponentType == COMP_TEXT){
				strHtml = strHtml+'	<input id="'+frmId+'" type="text" class="form-control input-sm" value="'+mValue+'"></input>';
			}else if(mItem.miniComponentType == COMP_TEXT_AREA){
				strHtml = strHtml+'	<textarea id="'+frmId+'"  class="form-control input-sm" value="'+mValue+'"></textarea>';
			}else if(mItem.miniComponentType == COMP_DATE_PICKER){
				strHtml = strHtml+'	<input id="'+frmId+'" type="text" class="form-control input-sm noBorder"></input>';
			}else if(mItem.miniComponentType == COMP_RADIO_BUTTON || mItem.miniComponentType == COMP_CHECK_BOX){
				var notes = mItem.miniComponentNotes;
				console.log(notes);
				if(notes){
					var notesArray = notes.split("\n");
					strHtml = strHtml+'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group" >';
					for(var n=0;n<notesArray.length;n++){
						var mItem1 = notesArray[n];
						if(mItem1){
							if(mItem.miniComponentType == COMP_RADIO_BUTTON){
								if(mValue == mItem1){
									strHtml = strHtml+'<input id="rd" type="radio" name="'+frmId+'"  value="'+mItem1+'" checked  />&nbsp;';
								}else{
									strHtml = strHtml+'<input id="rd" type="radio" name="'+frmId+'"  value="'+mItem1+'"  />&nbsp;';
								}
								
							}else if(mItem.miniComponentType == COMP_CHECK_BOX){
								if(mValue == mItem1){
									strHtml = strHtml+'<input id="chk" type="checkbox" name="'+frmId+'"  value="'+mItem1+'" checked  />&nbsp;';
								}else{
									strHtml = strHtml+'<input id="chk" type="checkbox" name="'+frmId+'"  value="'+mItem1+'"  />&nbsp;';
								}
								
							}
							strHtml = strHtml+'<span id="lbl">'+mItem1+'</span>&nbsp;&nbsp;&nbsp;&nbsp;';
						}
					}
					strHtml = strHtml+'</div>';
				}
			}
			
			strHtml = strHtml+'</div>';
			strHtml = strHtml+'<label class="col-xs-1 col-sm-1 col-md-1 col-lg-1 control-label input-sm noPadding">&nbsp;&nbsp;'+unit+'</label>';
			strHtml = strHtml+'</div></div>';
			fromArray.push(mItem);
		}
	}
	strHtml = strHtml+'<div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 form-group">';
	strHtml = strHtml+'<div class="navBar" style="padding-bottom:2px">';
	strHtml = strHtml+'<button id="btn-save" class="btn-save">Save</button></div>';
	$("#divPanel").html(strHtml);
	
	$("#btn-save").off("click");
	$("#btn-save").on("click",onClickSave);
	
	setTimeout(function(){
		for(var k=0;k<miniTemplates.length;k++){
			var mItem = miniTemplates[k];
			if(mItem){
				mItem.idk = mItem.id;
				var frmId = "f"+mItem.id;
				var mValue = mItem.value;
				var unit  ="";
				mItem.frmId = frmId;
				 if(mItem.miniComponentType == COMP_DATE_PICKER){
					 if(mItem.miniDataTypeValue == DT_DATE){
						 $("#"+frmId).kendoDatePicker({value:new Date(mValue)});
							var cDate = $("#"+frmId).data("kendoDatePicker");
							if(cDate){
								cDate.value(new Date(mValue));
							}
						}else if(mItem.miniDataTypeValue == DT_DATETIME){
							 $("#"+frmId).kendoDateTimePicker({value:new Date(mValue)});
							var cDate = $("#"+frmId).data("kendoDateTimePicker");
							if(cDate){
								cDate.value(new Date(mValue));
							}
						}
				 }
			}
		}
	},100);
}
var currId = "";
function onClickItem(evt){
	console.log(evt);
	currId = $(evt.currentTarget).attr("idk");
	 var idk = currId;
	 if($("#p"+currId).children().length == 0){
		 var ipaddress = ipAddress+"/homecare/components/?is-active=1&is-deleted=0&fields=*,component.*,dataType.*,unit.*&miniTemplateId="+idk;
			getAjaxObject(ipaddress,"GET",handleGetVacationList,onError);
	 }
}
var frmDesignArray = [];
var tempFormDesignArray = [];
function handleGetVacationList(dataObj){
	console.log(dataObj);
	frmDesignArray = [];
	if(dataObj && dataObj.response && dataObj.response.status ){
		if(dataObj.response.status.code == "1"){
			if(dataObj.response.miniComponents){
				if($.isArray(dataObj.response.miniComponents)){
					frmDesignArray = dataObj.response.miniComponents;
				}else{
					frmDesignArray.push(dataObj.response.miniComponents);
				}
			}
		}
	}
	var obj = {};
	obj.idk = currId;
	obj.values = frmDesignArray;
	obj.operation = "add";
	tempFormDesignArray.push(obj);
	var strHtml = "";
	$("#p"+currId).html("");
	for(var j=0;j<frmDesignArray.length;j++){
		var frmObj = frmDesignArray[j];
		frmObj.idk = frmObj.id;
		frmObj.length1 = frmObj.length;
		var frmId = "f"+frmObj.idk;
		var unit  ="";
		if(frmObj.unitValue){
			unit = frmObj.unitValue;
		}else{
		
		}
		strHtml = strHtml+'<div class="col-sm-6 col-md-6 col-lg-6 col-xs-6 form-group">';
		strHtml = strHtml+'<label class="col-xs-12 col-sm-12 col-md-12 col-lg-12 control-label input-sm noPadding">'+frmObj.name+' :</label>';
		strHtml = strHtml+'<div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 noPadding">';
		strHtml = strHtml+'<div class="col-sm-11 col-md-11 col-lg-11 col-xs-11 noPadding">';
		strHtml = strHtml+'	<input id="'+frmId+'" type="text" class="form-control input-sm"></input>';
		strHtml = strHtml+'</div>';
		strHtml = strHtml+'<label class="col-xs-1 col-sm-1 col-md-1 col-lg-1 control-label input-sm noPadding">&nbsp;&nbsp;'+unit+'</label>';
		strHtml = strHtml+'</div></div>';
	}
	var btnId = "b"+currId;
	strHtml = strHtml+'<div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 form-group">';
	strHtml = strHtml+'<div class="navBar" style="padding-bottom:2px">';
	strHtml = strHtml+'<button id="'+btnId+'" class="btn-save" btnId="'+currId+'">Save</button></div>';
	
	$("#p"+currId).html(strHtml);
	$("#"+btnId).off("click");
	$("#"+btnId).on("click",onClickSave);
}
function onClickSave(evt){
	//var btnId = evt.currentTarget;
	//btnId = $(btnId).attr("btnId");
	//var fromArray = getFormDesignValues(btnId);
	//console.log(fromArray);
	 var miniCompArray = [];
		for(var f=0;f<fromArray.length;f++){
			var fId = fromArray[f].idk;
			fId = "f"+fId;
			var fData = "";//$("#"+fId).val();
			var frmObj = fromArray[f];
			var fId = fromArray[f].idk;
			fId = "f"+fId;
			var fData = "";
			var cmpId = fromArray[f].miniComponentId;
			
			if(frmObj.miniComponentType == COMP_DATE_PICKER){
				if(frmObj.miniDataTypeValue == DT_DATE){
					var cmbDate = $("#"+fId).data("kendoDatePicker");
					fData = cmbDate.value();
				}else if(frmObj.miniDataTypeValue == DT_DATETIME){
					var cmbDate = $("#"+fId).data("kendoDateTimePicker");
					fData = cmbDate.value();
				}
			}else if(frmObj.miniComponentType == COMP_TEXT || frmObj.miniComponentType == COMP_TEXT_AREA){
				 fData = $("#"+fId).val();
			}else if(frmObj.miniComponentType == COMP_CHECK_BOX || frmObj.miniComponentType == COMP_RADIO_BUTTON){
				if(frmObj.miniComponentType == COMP_CHECK_BOX){
					fData = $('input[name='+fId+']').val();
				}else if(frmObj.miniComponentType == COMP_RADIO_BUTTON){
					fData = $("input[type='radio'][name="+fId+"]:checked").val();
				}
			}
			
			var obj = {};
			obj.miniComponentId = cmpId;
			obj.id = fromArray[f].idk;
			obj.miniTemplateId = fromArray[f].miniTemplateId;
			obj.miniUnits = fromArray[f].miniUnits;
			obj.miniComponentName = fromArray[f].miniComponentName;
			obj.value = fData;
			obj.isDeleted = 0;
			obj.modifiedBy = 101;
			obj.isActive = 1;
			miniCompArray.push(obj);
		}
		var reqObj = {};
		reqObj.notes = $("#txtNotes").val();
		reqObj.patientId = 101;
		reqObj.isActive = 1;
		reqObj.isDeleted = 0;
		reqObj.reportedOn = new Date().getTime();
		reqObj.modifiedBy = 101;
		reqObj.providerId = 101;
		reqObj.templateId = Number(selItem.idk);
		reqObj.componentValues = miniCompArray;
		reqObj.id = resultDataObj.response.templateValues[0].id;
		
		dataUrl = ipAddress +"/homecare/template-values/";
		var method  = "PUT";
		createAjaxObject(dataUrl, reqObj, method, onCreate, onError);
}
function onCreate(dataObj){
	console.log(dataObj);
	if(dataObj && dataObj.response && dataObj.response.status ){
		if(dataObj.response.status.code == "1"){
			var msg = "";
			msg = "Form Values are   updated  Successfully";
			displaySessionErrorPopUp("Info", msg, function(res) {
	        })
		}else{
			customAlert.error("Error", dataObj.response.status.message);
		}
	}else{
		
	}
}
function getFormDesignValues(btnId){
	for(var a=0;a<tempFormDesignArray.length;a++){
		var aItem = tempFormDesignArray[a];
		if(aItem && aItem.idk == btnId){
			return aItem.values;
		}
	}
	return null;
}
function getFormData(){
	 var selectedItems = angularUIgridWrapper1.getSelectedRows();
     console.log(selectedItems);
	var ipaddress = ipAddress+"/homecare/component-values/?is-active=1&is-deleted=0&miniTemplateId="+selectedItems[0].idk;
	getAjaxObject(ipaddress,"GET",handleGetFormDataValues,onError);
}
function handleGetFormDataValues(dataObj){
	console.log(dataObj);
}
function onError(errorObj){
	console.log(errorObj);
}

function buttonEvents(){
	/*$("#btnSave").off("click",onClickSave);
	$("#btnSave").on("click",onClickSave);
	
	$("#btnView").off("click",onClickView);
	$("#btnView").on("click",onClickView);
	
	$("#btnReport").off("click",onClickReport);
	$("#btnReport").on("click",onClickReport);
	
	
	$("#btnCancelDet").off("click",onClickCancel);
	$("#btnCancelDet").on("click",onClickCancel);
	
	
	$("#btnEdit").off("click",onClickEdit);
	$("#btnEdit").on("click",onClickEdit);
	
	$("#btnDelete").off("click",onClickDelete);
	$("#btnDelete").on("click",onClickDelete);*/
}

function resetData(){
	for(var f=0;f<frmDesignArray.length;f++){
		var fId = frmDesignArray[f].idk;
		fId = "f"+fId;
		 $("#"+fId).val("");
	}
}
function adjustHeight(){
	var defHeight = 150;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    var frm = (cmpHeight)+"px";
    $("#divFormData").css({height:frm});
}


function getComboListIndex(cmbId, attr, attrVal) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb) {
        var ds = cmb.dataSource;
        var totalRec = ds.total();
        for (var i = 0; i < totalRec; i++) {
            var dtItem = ds.at(i);
            if (dtItem && dtItem[attr] == attrVal) {
                cmb.select(i);
                return i;
            }
        }
    }
    return -1;
}
function closeVideoScreen(evt,returnData){
	
}
var operation = "add";
var selFileResourceDataItem = null;
function onClickCancel(){
	var obj = {};
	var windowWrapper = new kendoWindowWrapper();
	windowWrapper.closePageWindow(obj);
}

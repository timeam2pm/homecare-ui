var parentRef = null;

var patientId = "";

$(document).ready(function(){
});


$(window).load(function(){
	loading = false;
	$(window).resize(adjustHeight);
onLoaded();
	if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
	parentRef = parent.frames['iframe'].window;
	init();
    buttonEvents();
	adjustHeight();
}

function init(){
	patientId = parentRef.patientId;
	getAjaxObject(ipAddress+"/patient/medications/"+patientId,"GET",onGetVitalsSuccess,onError);
	//getAjaxObject(ipAddress+"/patient/reports/holter/"+patientId,"GET",onGetHolterReportsData,onError);
}
function onGetHBTPatientData(dataObj){
	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		getVitals();
	}
}
function getVitals(){
	
}
function getVitalInfo(vName,dt){
	for(var i=0;i<vitalDataArray.length;i++){
		var item = vitalDataArray[i];
		if(item.vital == vName && item.date == dt){
			return item.value;
		}
	}
	return "";
}

function getRowColors(index) {
    var classes = ['active', 'success', 'info', 'warning', 'danger'];
   // console.log(index);
    if (index % 2 === 0 && index / 2 < classes.length) {
    	//console.log(classes[index / 2]);
    	return classes[index / 2];
    	//console.log(index+','+classes[index / 2]);
      /*  return {
            classes: classes[index / 2]
        };*/
    }
    return "";
}

function onGetVitalsSuccess(dataObj){
	$("#divTable").text("");
	var medicArray = [];
	if(dataObj){
		if($.isArray(dataObj)){
			medicArray = dataObj;
		}else{
			medicArray.push(dataObj);
		}
	}
	/*var strTable = '<table data-toggle="table" data-classes="table table-hover table-condensed"  data-row-style="rowColors"  data-striped="true"  data-sort-name="Quality"  data-sort-order="desc"  data-pagination="true"	 data-click-to-select="true">';
	strTable = strTable+'<thead>';
	strTable = strTable+'<tr>';
	strTable = strTable+'<th class="col-xs-1" data-field="Date" data-sortable="true">Date</th>';
	strTable = strTable+'<th class="col-xs-1" data-field="Name" data-sortable="true">Name</th>';
	strTable = strTable+'<th class="col-xs-1" data-field="Strength" data-sortable="true">Strength</th>';
	strTable = strTable+'<th class="col-xs-1" data-field="Form" data-sortable="true">Form</th>';
	strTable = strTable+'<th class="col-xs-1" data-field="Schedule" data-sortable="true">Schedule</th>';
	strTable = strTable+'<th class="col-xs-1" data-field="Instructions" data-sortable="true">Instructions</th>';
	strTable = strTable+'</tr></thead>*/
	var strTable = '<table class="table">';
	strTable = strTable+'<thead class="fillsHeader">';
	strTable = strTable+'<tr>';
	//strTable = strTable+'<th class="textAlign whiteColor">Date</th>';
	strTable = strTable+'<th class="textAlign whiteColor" style="width:210px">Name</th>';
	strTable = strTable+'<th class="textAlign whiteColor">Morning</th>';
	strTable = strTable+'<th class="textAlign whiteColor">Lunch</th>';
	strTable = strTable+'<th class="textAlign whiteColor">Evening</th>';
	strTable = strTable+'<th class="textAlign whiteColor">Night</th>';
	strTable = strTable+'<th class="textAlign whiteColor">Strength</th>';
	strTable = strTable+'<th class="textAlign whiteColor">Form</th>';
	strTable = strTable+'<th class="textAlign whiteColor">Schedule</th>';
	strTable = strTable+'<th class="textAlign whiteColor">Instructions</th>';
	//strTable = strTable+'<th class="textAlign whiteColor">Timing</th>';
	strTable = strTable+'</tr>';
	strTable = strTable+'</thead>';
	strTable = strTable+'<tbody>';

	for(var i=0;i<medicArray.length;i++){
		var dataItem = medicArray[i];
		if(dataItem){
			//console.log(dataItem);
			var className = "success";
			if(i%2 == 0){
				className = "danger";
			}
			if(i%3 == 0){
				className = "info";
			}
			if(i%3 == 0){
				className = "active";
			}
			var strMorning = "";
			var strLunch = "";
			var strEvening = "";
			var strNight = "";
			var timing = dataItem.timing;
			var timiningArray = timing.split(" ");
			//console.log(timiningArray);
			className = getRowColors(i);
			strTable = strTable+'<tr class="'+className+'">';
			//strTable = strTable+'<td>'+kendo.toString(new Date(dataItem.date), "MM/dd/yyyy")+'</td>';
			//strTable = strTable+'<td>'+dataItem.name+'</td>';
			if(dataItem.fileResourceId && dataItem.fileResourceId !="null"){
				strTable = strTable+'<td class="textAlign"><a id="'+dataItem.fileResourceId+'" onClick="onClickMyAudio(event)" style="cursor:pointer" class="hyperLinkStyle">'+dataItem.name+'</a></td>';	
			}else{
				strTable = strTable+'<td class="textAlign">'+dataItem.name+'</td>';
			}
			
			//		
			//var strAction = "";
			for(var j=0;j<timiningArray.length;j++){
				var item = timiningArray[j];
				if(item == "1"){
					strMorning  = '<img  src="../../img/AppImg/HosImages/blue_1.png"  class="cusrsorStyle medicationIcon" title="Morning"></img>&nbsp;&nbsp;&nbsp;';
				}else if(item == "2"){
					strLunch = '<img  src="../../img/AppImg/HosImages/green_1.png"  class="cusrsorStyle medicationIcon" title="Lunch"></img>&nbsp;&nbsp;&nbsp;';
				}else if(item == "3"){
					strEvening = '<img  src="../../img/AppImg/HosImages/orange_1.png"  class="cusrsorStyle medicationIcon" title="Evening"></img>&nbsp;&nbsp;&nbsp;';
				}else if(item == "4"){
					strNight = '<img  src="../../img/AppImg/HosImages/red_1.png"  class="cusrsorStyle medicationIcon" title="Night"></img>';
				}
			}
			strTable = strTable+'<td class="textAlign">'+strMorning+'</td>';
			strTable = strTable+'<td class="textAlign">'+strLunch+'</td>';
			strTable = strTable+'<td class="textAlign">'+strEvening+'</td>';
			strTable = strTable+'<td class="textAlign">'+strNight+'</td>';
			
			strTable = strTable+'<td class="textAlign">'+dataItem.strength+'</td>';
			strTable = strTable+'<td class="textAlign">'+dataItem.form+'</td>';
			strTable = strTable+'<td class="textAlign">'+dataItem.schedule+'</td>';
			strTable = strTable+'<td class="textAlign">'+dataItem.instructions+'</td>';
			//strTable = strTable+'<td>'+strAction+'</td>';
			strTable = strTable+'</tr>';
		}
	}
	strTable = strTable+'</tbody>';
	strTable = strTable+'</table>';
	$("#divTable").append(strTable);
}
var imgImage = null;
function onClickMyAudio(event){
	var audioId = "";
	if(imgImage){
		$(imgImage).removeClass("textBorder");
	}
	
	if(event.currentTarget){
		audioId = event.currentTarget.id;
		imgImage = event.currentTarget;
		$(imgImage).addClass("textBorder");
	}
	console.log(audioId);
	if(audioId && audioId != "null"){
		playAudioFile(audioId);
	}
}
function playAudioFile(aid){
	parentRef.audioId = aid;
	var popW = 600;
    var popH = 200;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Audio";
    devModelWindowWrapper.openPageWindow("../../html/patients/showAudio.html", profileLbl, popW, popH, true, closeVideoScreen);
}
function closeVideoScreen(evt,returnData){
	//$(imgImage).removeClass("imgBorder");
	//$(imgImage).removeClass("textBorder");
}
function onError(errorObj){
	console.log(errorObj);
}
var holterReportDataArray = [];
function onGetHolterReportsData(dataObj){
		$("#divTable").text("");
		var dataArray = [];
		if(dataObj){
			if($.isArray(dataObj)){
				dataArray = dataObj;
			}else{
				dataArray.push(dataObj);
			}
		}
		holterReportDataArray = dataArray;
		var strTable = '<table class="table">';
		strTable = strTable+'<thead class="fillsHeader whiteColor">';
		strTable = strTable+'<tr>';
		//strTable = strTable+'<th>Patient ID</th>';
		strTable = strTable+'<th class="textAlign whiteColor" style="width:220px">Date Generated</th>';
		strTable = strTable+'<th class="textAlign whiteColor">File Name</th>';
		strTable = strTable+'<th class="textAlign whiteColor">File Type</th>';
		strTable = strTable+'</tr>';
		strTable = strTable+'</thead>';
		strTable = strTable+'<tbody>';
		for(var i=0;i<holterReportDataArray.length;i++){
			var dataItem = holterReportDataArray[i];
			if(dataItem){
				//console.log(dataItem);
				var className = getRowColors(i);
				strTable = strTable+'<tr class="'+className+'">';
				//strTable = strTable+'<td>'+dataItem.patientId+'</td>';
				strTable = strTable+'<td class="textAlign">'+kendo.toString(new Date(dataItem.createdDate),"MM-dd-yyyy h:mm:ss")+'</td>';
				strTable = strTable+'<td class="textAlign">'+dataItem.fileName+'</td>';
				var dietId = dataItem.id;
				if(dataItem.fileType == "mp4"){
					strTable = strTable+'<td style="width:200px;padding:0px" class="textAlign"><img id="'+dietId+'" src="../../img/AppImg/HosImages/video.png"  class="cusrsorStyle videoIcon" onClick="onClickGenerate(event)"></td>';
				}else if(dataItem.fileType == "pdf"){
					strTable = strTable+'<td style="width:200px;padding:0px" class="textAlign"><img  id="'+dietId+'" src="../../img/AppImg/HosImages/pdf.png"    class="cusrsorStyle pdfIcon" onClick="onClickHolterReport(event)"></td>';
				}
				strTable = strTable+'</tr>';
			}
		}
		strTable = strTable+'</tbody>';
		strTable = strTable+'</table>';
		$("#divHolterReports").append(strTable);
	}
function buttonEvents(){
	$("#btnCancelDet").off("click",onClickCancel);
	$("#btnCancelDet").on("click",onClickCancel);
	
	$("#btnAdd").off("click",onClickAdd);
	$("#btnAdd").on("click",onClickAdd);
	
}

function adjustHeight(){
	var defHeight =100;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    $("#divTable").height(cmpHeight);
	//angularUIgridWrapper.adjustGridHeight(cmpHeight);
}


function onClickPatientCall(obj){
	var popW = "60%";
    var popH = 400;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Patient Call Chart";
    parentRef.selObj = obj;
    devModelWindowWrapper.openPageWindow("../../html/patients/callPatient.html", profileLbl, popW, popH, false, closeCallPatient);
}
function closeCallPatient(evt,returnData){
	if(returnData && returnData.status == "success"){
		customAlert.info("Save","Patient Call Chart Created Successfully");
		init();
	}
}
function onClickAdd(){
	var popW = "85%";
    var popH = "85%";
    parentRef.patientId = patientId;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Patient Medication";
    devModelWindowWrapper.openPageWindow("../../html/masters/prescription.html", profileLbl, popW, popH, false, closeCallPatient);
}
function onClickCancel(){
	var obj = {};
	var windowWrapper = new kendoWindowWrapper();
	windowWrapper.closePageWindow(obj);
}

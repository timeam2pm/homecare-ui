var parentRef = null;
var operation = "";
var selItem = null;
var ADD = "add";
var UPDATE = "edit";
var VIEW = "view";
var DELETE = "delete";
var patientId = "";
var rs = "";
var selRSItem = null;

var towArr = [{Key:'0',Value:'Sunday'},{Key:'1',Value:'Monday'},{Key:'2',Value:'Tuesday'},{Key:'3',Value:'Wednesday'},{Key:'4',Value:'Thursday'},{Key:'5',Value:'Friday'},{Key:'6',Value:'Saturday'}];

var photoExt = "";
var pid = "";
var pname = "";

var fid = "";
var fname = "";
var selList = [];
var prsViewArray = [];

$(document).ready(function() {
    parentRef = parent.frames['iframe'].window;
    var pnlHeight = window.innerHeight;
});

$(window).load(function(){
    parentRef = parent.frames['iframe'].window;
    selList = parentRef.selList;
    init();
    buttonEvents();
});

function buttonEvents(){
    //$("#chkOverride").off("click");
    //$("#chkOverride").on("click",onChangeOverride);

    $("#btnSave").off("click");
    $("#btnSave").on("click",onClickOK);

    $("#btnCancelDet").off("click");
    $("#btnCancelDet").on("click",onClickCancel);
}

function init(){
    //if(selList[0].dowK == selList[0].doeK){
    var selDays = [];
    for(var s=0;s<towArr.length;s++){
        var sItem = towArr[s];
        if(sItem && sItem.Key != selList[0].dowK){
            selDays.push(sItem);
        }
    }
    var checkInputs = function(elements) {
        elements.each(function() {
            var element = $(this);
            var input = element.children("input");

            input.prop("checked", element.hasClass("k-state-selected"));
        });
    };
    var required = $("#txtRDow").kendoMultiSelect({
        itemTemplate: "<input type='checkbox'/> #:data.Value#",
        autoClose: false,
        dataSource: selDays,
        dataTextField: "Value",
        dataValueField: "Key",
        dataBound: function() {
            var items = this.ul.find("li");
            setTimeout(function() {
                checkInputs(items);
            });
        },
        change: function() {
            var items = this.ul.find("li");
            checkInputs(items);
        }
    }).data("kendoMultiSelect");

}

function onClickOK(){
    var selArray = [];
    var required = $("#txtRDow").data("kendoMultiSelect");
    if(required){
        var arr = required._dataItems;
        if(arr && arr.length>0){
            for(var j=0;j<arr.length;j++){
                selArray.push(arr[j].Key);
            }
            parentRef.selArray = selArray;
            var obj = {};
            obj.status = "true";
            popupClose(obj);
        }
    }
    if(selArray.length == 0){
        customAlert.error("Error", "Please select start days");
    }
}



function onClickCancel() {
    var obj = {};
    obj.status = "false";
    popupClose(obj);
}


function popupClose(obj) {
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}
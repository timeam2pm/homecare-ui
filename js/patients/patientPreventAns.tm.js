var angularUIgridWrapper = AngularUIGridWrapper();

var rangePicker = null;
$(document).ready(function(){
});

$(window).load(function(){
	init();
});

	function init() {
		var dataOptions = {
	        pagination: false,
	        paginationPageSize: 500,
			changeCallBack: onChange
	    }

		angularUIgridWrapper = new AngularUIGridWrapper("dgridPtPrevent", dataOptions);
	    angularUIgridWrapper.init();
	    
	    var dataArray = [];
	    
	    buildCallChartGrid(dataArray);
	    
	    getAjaxObject("http://stage.timeam.com/patient/preventative-services/101","GET",onGetInterestsSuccess,onErrorMedication);
	    //buildCallChartGrid([]);

	buttonEvents();
}
	function onGetInterestsSuccess(dataObj){
		console.log(dataObj);
		var interestArr = [];
		if(dataObj){
			if($.isArray(dataObj)){
				interestArr = dataObj;
			}else{
				interestArr.push(dataObj);
			}
		}
		console.log(interestArr);
		 buildCallChartGrid(interestArr);
	}
	function onErrorMedication(errobj){
		console.log(errobj);
	}
	function getAjaxObject(dataUrl,method,successFunction,errorFunction){
		Loader.showLoader();
		$.ajax({
			  type: method,
			  url: dataUrl,
			  data: null,
			  context: this,
				cache: false,
			  success: function( data, statusCode, jqXHR ){
				  Loader.hideLoader();
				  successFunction(data);
			  },
			  error: function( jqXHR, textStatus, errorThrown ){
				  Loader.hideLoader();
				  errorFunction(jqXHR);
			  },
			  contentType: "application/json",
			});

	}
function buttonEvents(){
	$("#btnSave").off("click");
	$("#btnSave").on("click",onClickOK);
	
	$("#btnCancelDet").off("click");
	$("#btnCancelDet").on("click",onClickCancel);
}
$(window).resize(adjustHeight);
function adjustHeight(){
	var defHeight = 160;
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    try{angularUIgridWrapper.adjustGridHeight(cmpHeight);}catch(e){};

}
function buildCallChartGrid(dataSource) {
    var gridColumns = [];
	var otoptions = {};
	otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Question",
        "field": "question",
        "width":"80%",
        "enableColumnMenu": false,
    });
	gridColumns.push({
        "title": "Answer",
        "field": "answer",
        "enableColumnMenu": false,
        "cellTemplate": showDefaultValue()
    });
		angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
	adjustHeight();
}
function onChange(){
}
function showDefaultValue(){
	var node = '<label style="margin-top:-10px;width:100%;text-align:center"  ng-hide="((row.entity.controlType && row.entity.controlType!=\'0\'))">';
	node = node+'<input  type="radio"   ng-value="1"    ng-model="row.entity.answer" style="width:15px" />';
	node = node+'<span>Yes</span>&nbsp;&nbsp;&nbsp;&nbsp;';
	node = node+'<input  type="radio"   ng-value="0"  ng-model="row.entity.answer" style="width:15px" />';
	node = node+'<span>No</span>&nbsp;&nbsp;&nbsp;&nbsp;';
	node = node+'<label>';
	return node;
	/*var node = '<div>';
	node += '<input  type="text" id="txtUnsignDefVal"    ng-model="row.entity.answer" ng-hide="((row.entity.answer && row.entity.answer!=\'\') || row.entity.FT == \'\'  ||  row.entity.FT != \'UnsignedIntProperty\' )" value={{row.entity.PV}} onkeypress="onUnSignedKeyPress(event)" style="color:#000;margin: 0;padding: 0;">';
	node += '<input  type="text" id="txtSignDefVal"    ng-disabled="row.entity.PTYPE == \'VIEW\'?\'all\':\'\' || row.entity.PTYPE == \'CUSTOM\'?\'all\':\'\' " ng-model="row.entity.PV" ng-hide="((row.entity.BC && row.entity.BC!=\'\') || row.entity.FT == \'\'   ||  row.entity.FT != \'SignedIntProperty\' )" value={{row.entity.PV}}  onkeypress="onSignedKeyPress(event)" style="color:#000;margin: 0;padding: 0;">';
	node += '<div style="position: absolute;width:100px">'
	node += '<input type="text" ng-disabled="row.entity.PTYPE == \'VIEW\'?\'all\':\'\'  || row.entity.PTYPE == \'CUSTOM\'?\'all\':\'\' " ng-hide="((row.entity.BC && row.entity.BC!=\'\') || row.entity.FT == \'\'   || row.entity.FT == \'UnsignedIntProperty\'  ||  row.entity.FT == \'SignedIntProperty\')"  ng-model="row.entity.PV" onchange="textFocusChange(event)" style="position: absolute;left: 0;top: 0;padding: 0;z-index: 78;margin: 0;" value="{{row.entity.PV}}">'
	node += '<select id="rowDropdown" onchange="onChangeGroup()" ng-disabled="row.entity.PTYPE == \'VIEW\'?\'all\':\'\'  || row.entity.PTYPE == \'CUSTOM\'?\'all\':\'\' " ng-hide="( (row.entity.BC && row.entity.BC!=\'\') || row.entity.FT == \'\'   || row.entity.FT == \'UnsignedIntProperty\'  ||  row.entity.FT == \'SignedIntProperty\')"  ng-model="row.entity.PV" style="position: relative;width: 130px;left: 0;top: 0;margin: 0;padding: 0;">';
	node += '<option ng-repeat="item in row.entity.ITEMS" value="{{item}}">{{item}}</option>'
	node += '</select>';
	node += '</div>';
	 node += '<input   type="checkbox" id="chkBmp"  ng-disabled="row.entity.PTYPE == \'VIEW\'?\'all\':\'\'  || row.entity.PTYPE == \'CUSTOM\'?\'all\':\'\' " ng-hide="((row.entity.BC && row.entity.BC!=\'\') || row.entity.FT == \'EnumProperty\'  || row.entity.FT == \'DataProperty\'  || row.entity.FT == \'UnsignedIntProperty\'  ||  row.entity.FT == \'SignedIntProperty\')"  ng-model="row.entity.PV" style="margin:5px;width:20px;height:15px" onchange="isBitMapChange(event);"">';
	    node += '</input>';
	return node;*/
}
function onClickOK(){
	//popupClose(true);
	setTimeout(function(){
		 var selectedItems = angularUIgridWrapper.getSelectedRows();
		 console.log(selectedItems);
		 if(selectedItems && selectedItems.length>0){
			 var obj = {};
			 obj.selItem = selectedItems[0];
			 obj.status = "success";
				var windowWrapper = new kendoWindowWrapper();
				windowWrapper.closePageWindow(obj);
		 }
	})
}
function onClickCancel(){
	popupClose(false);
}
function popupClose(st){
	var onCloseData = new Object();
	onCloseData.status = st;
	var windowWrapper = new kendoWindowWrapper();
	windowWrapper.closePageWindow(onCloseData);
}

function handleGetError(e) {
	var errLbl = getLocaleStringWithDefault(DC_UI_COMM, 'ERROR_MESSAGE', "Error");
	if(e && e.RestData && e.RestData.Description){
		window.top.displayErrorPopUp(errLbl,e.RestData.Description);
	}
}
function handleGetHttpError(e) {
   var errLbl = getLocaleStringWithDefault(DC_UI_COMM, 'ERROR_MESSAGE', "Error");
   var errDesc = getLocaleStringWithDefault(DC_UI_COMM, 'HTTP_ERROR', 'Http Error');
	// window.top.displayErrorPopUp(errLbl,errDesc);
}
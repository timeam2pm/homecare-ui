var parentRef = null;
var selObj = null;
var conversation = null;
var conversationsClient = null;
var activeConversation = null;
var participant = null;
var previewMedia = null;
//var ipAddress = "http://192.168.0.106:8080";
//var ipAddress = "http://stage.timeam.com";
var screenType = "";
//var accessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImN0eSI6InR3aWxpby1mcGE7dj0xIn0.eyJqdGkiOiJTSzdhZDQwMzUzMTBhYmI3ZGQ3MTU4YTA1ZTI4NDgxYzQ4LTE0NzU5MjM0MjYiLCJpc3MiOiJTSzdhZDQwMzUzMTBhYmI3ZGQ3MTU4YTA1ZTI4NDgxYzQ4Iiwic3ViIjoiQUMxMTFmOThlNDQ4MmE3NWRhMjU0MDVhYjg2YzgxMmE5NiIsImV4cCI6MTQ3NTkyNzAyNiwiZ3JhbnRzIjp7ImlkZW50aXR5IjoiMTIzIiwicnRjIjp7ImNvbmZpZ3VyYXRpb25fcHJvZmlsZV9zaWQiOiJWUzBkOTdiODgxMGViMGFhMTA2MTgxZjMwNDgwNTBkMzUzIn19fQ.UjycWnbwH06XVw4jSdPyWxJj1hQMlK8p0-o4gq-w31U";

var accessToken = "";
var callDate = "";
var beginTime = "";
var endTime = "";
var callDuration = "";
var beginMs = "";
var endMs = "";
var remoteUsers = [];

$(document).ready(function(){
    previewMedia = null;
    participant = null;
    activeConversation = null;
    conversationsClient = null;
    parentRef = parent.frames['iframe'].window;
    var pnlHeight = window.innerHeight;
    var imgHeight = pnlHeight-90;
    $("#divTop").height(imgHeight);
});

$(window).load(function(){
    selObj = parentRef.selObj;
    console.log(selObj);
    init();
    buttonEvents();
});
function init(){
    allowAlphabets("txtPatientName");
    allowNumerics("txtPatientID");
    allowNumerics("txtPatientDOB");
    allowAlphabets("txtPatientGender");
    allowAlphabets("taReason");
    allowAlphabets("taOther");
    allowAlphabets("taRemarks");


    $("#txtUser").val(sessionStorage.uName);
    console.log(selObj);

    if(parentRef.patientInfo){
        var dt = new Date(parentRef.patientInfo.dateOfBirth);
        var strDT = "";
        if(dt){
            strDT = kendo.toString(dt,"MM-dd-yyyy");
        }
        $("#txtPatientName").val(parentRef.patientInfo.firstName);
        $("#txtPatientID").val(parentRef.patientInfo.id);
        $("#txtPatientDOB").val(strDT);
        $("#txtPatientGender").val(parentRef.patientInfo.gender);
    }
    if(selObj){
        $("#btnSave").hide();
        $("#divVideoCall").hide();
        //$("#txtPatientID").val(selObj.PID);
        //$("#txtPatientName").val(selObj.PN);
        $("#lblCallDate").text("Call Date : "+selObj.dateOfCall);
        $("#lblBT").text("Begin Time :"+selObj.beginTime);
        $("#lblET").text("End Time :"+selObj.endTime);
        $("#lblCDT").text("Call Duration : "+selObj.callDuration);
        if(selObj.patientCondition == "Fine"){
            $("#rdFine").attr("checked",true);
        }else if(selObj.patientCondition == "Not able to talk"){
            $("#rdTalk").attr("checked",true);
        }else if(selObj.patientCondition == "Sleepy"){
            $("#rdSleepy").attr("checked",true);
        }else if(selObj.patientCondition == "In Pain"){
            $("#rdPain").attr("checked",true);
        }
        if(selObj.takenMedication == "Yes"){
            $("#rdYes").attr("checked",true);
        }else{
            $("#rdNo").attr("checked",true);
        }
        if(selObj.needAnyAssistance == "Yes"){
            $("#rdAYes").attr("checked",true);
        }else{
            $("#rdANo").attr("checked",true);
        }

        $("#taReason").val(selObj.reasonForCall);
        $("#taOther").val(selObj.anyOther);
        $("#taRemarks").val(selObj.notes);

    }else{
        initVideoCall();
    }
}
function setPopupWindowHeight(pos){
    var windowWrapper = new kendoWindowWrapper();
    var container = windowWrapper.getWindowContainer();
    if(container){
        var dialog = container.data("kendoWindow");
        //	dialog.setOptions({height: heightPer,width:widthPer});
        //dialog.setOptions({left: 500});
        dialog.setOptions({position:{left:500,top:500}});
        dialog.maximize();
        //dialog.left();
    }
}
function onGetAccessCode(dataObj){
    console.log(dataObj);
    accessToken = dataObj.token;
    startConversion();
}
function initVideoCall(){
    //$("#divVideoCall").show();
    $("#lblCallDate").text("Call Date : "+getCallDate());
    $("#lblBT").text("Begin Time :"+getBeginTime());
    getEndTime();
    //showLocalPreview();
    //startConversion();
}
function getCallDate(){
    var dt = new Date();
    beginMs = dt.getTime();
    return kendo.toString(dt,"yyyy-MM-dd");
}
function getBeginTime(){
    var dt = new Date();
    beginTime = kendo.toString(dt,"hh:mm:ss tt");
    return beginTime;
}
function getEndTime(){
    setInterval(function(){
        var dt = new Date();
        endMs = dt.getTime();
        endTime = kendo.toString(dt,"hh:mm:ss tt");
        $("#lblET").text("End Time :"+endTime);
        var diffMin = (endMs-beginMs)/(1000*60);
        diffMin = diffMin.toFixed(2);
        callDuration = diffMin;
        $("#lblCDT").text("Call Duration : "+diffMin +"mins");
    },1000)
}
function getCallDuration(){
    setInterval(function(){
        var dt = new Date();
        $("#lblET").text("End Time :"+kendo.toString(dt,"hh:mm:ss tt"));
    },1000)
}
function showLocalPreview(){
    if (!previewMedia) {
        previewMedia = new Twilio.Conversations.LocalMedia();
        Twilio.Conversations.getUserMedia().then(
            function (mediaStream) {
                previewMedia.addStream(mediaStream);
                previewMedia.attach('#local-media');
            },
            function (error) {
                console.error('Unable to access local media', error);
                console.log('Unable to access Camera and Microphone');
            }
        );
    };
}
function startConversion(){
    var accessManager = new Twilio.AccessManager(accessToken);
    conversationsClient = new Twilio.Conversations.Client(accessManager);
    conversationsClient.listen().then(
        clientConnected,
        function (error) {
            console.log('Could not connect to Twilio: ' + error.message);
        }
    );
}
function onClickCall(from){
    var inviteTo = from;
    if (activeConversation) {
        // add a participant
        activeConversation.invite(inviteTo);
    } else {
        // create a conversation
        var options = {};
        if (previewMedia) {
            options.localMedia = previewMedia;
        }
        conversationsClient.inviteToConversation(inviteTo, options).then(
            conversationStarted,
            function (error) {
                console.log('Unable to create conversation');
                console.error('Unable to create conversation', error);
            }
        );
    }
}
function clientConnected() {
    Loader.hideLoader();
    console.log("Connected to Twilio. Listening for incoming Invites as '" + conversationsClient.identity + "'");
    conversationsClient.on('invite', function (invite) {
        console.log('Incoming invite from: ' + invite.from);
        // invite.accept().then(conversationStarted);
        if(sessionStorage.uName == "nurse"){
            // setPopupWindowHeight(675,"70%");
            setPopupWindowHeight("100px");
        }
        customAlert.confirm("Info","Call from "+invite.from+"",function(evt){
            console.log(evt);
            if(evt.button == "Yes"){
                if(sessionStorage.uName == "nurse"){
                    // setPopupWindowHeight(675,"70%");
                }
                invite.accept().then(conversationStarted);
            }else{
                setPopupWindowHeight("10000px");
            }
        })
    });
    setTimeout(function(){
        /*  var from = parentRef.from;
          if(from && from != undefined){
             onClickCall(from);
          }  */
        var options = {};
        if (previewMedia) {
            options.localMedia = previewMedia;
        }
        if(parentRef.from){
            var inviteTo = parentRef.from;
            console.log("invitation from :"+inviteTo)
            conversationsClient.inviteToConversation(inviteTo, options).then(
                conversationStarted,
                function (error) {
                    console.log('Unable to create conversation');
                    console.log('Unable to create conversation', error);
                }
            );
        }
    },1000)
};
function isParticipation(user){
    var flag = false;
    for(var i=0;i<remoteUsers.length;i++){
        if(remoteUsers[i] == user){
            flag = true;
            return true;
        }
    }
    return flag;
}
function conversationStarted(conversation) {
    console.log('In an active Conversation');
    parentRef.from = null;
    activeConversation = conversation;
    if (activeConversation) {
        //onClickInvite();
    }
    // draw local video, if not already previewing
    // if (!previewMedia) {


    // }
    // when a participant joins, draw their video on screen
    conversation.on('participantConnected', function (participant) {
        //  participant.media.attach('#local-media');
        console.log(remoteUsers);
        console.log(sessionStorage.uName+"Participant '" + participant.identity + "' connected");
        // if(sessionStorage.uName != participant.identity){
        //if(!isParticipation(participant.identity)){
        remoteUsers.push(participant.identity);
        participant.media.attach('#remote-media');
        //}
        // 	}

    });
    // when a participant disconnects, note in log
    conversation.on('participantDisconnected', function (participant) {
        console.log("Participant '" + participant.identity + "' disconnected");
    });
    // when the conversation ends, stop capturing local video
    conversation.on('ended', function (conversation) {
        console.log("Connected to Twilio. Listening for incoming Invites as '" + conversationsClient.identity + "'");
        conversation.localMedia.stop();
        conversation.disconnect();
        activeConversation = null;
    });
};
function buttonEvents(){
    $("#btnSave").off("click");
    $("#btnSave").on("click",onClickOK);

    $("#btnCancelDet").off("click");
    $("#btnCancelDet").on("click",onClickCancel);

    $("#btnPrint").off("click");
    $("#btnPrint").on("click",onClickPrint);

    $("#btnCall").off("click");
    $("#btnCall").on("click",onClickInvite);

}
function onClickInvite(){
    parentRef.invFlag = false;
    var strPatient = $("#txtInviteName").val();
    strPatient = $.trim(strPatient);
    if(strPatient != ""){
        onClickCall(strPatient);
    }
}
function getAjaxObject(dataUrl,method,dataObj,successFunction,errorFunction){
    Loader.showLoader();
    var dObj = null;
    if(dataObj){
        dObj = JSON.stringify(dataObj);
    }
    $.ajax({
        type: method,
        url: dataUrl,
        data: dObj,
        context: this,
        cache: false,
        success: function( data, statusCode, jqXHR ){
            Loader.hideLoader();
            successFunction(data);
        },
        error: function( jqXHR, textStatus, errorThrown ){
            Loader.hideLoader();
            errorFunction(jqXHR);
        },
        contentType: "application/json",
    });

}//
function onClickOK(){
    var reqObj = {};
    reqObj.dateOfCall = getCallDate();
    reqObj.beginTime = beginTime;
    reqObj.endTime = endTime;
    reqObj.callDuration = callDuration*60;
    reqObj.patientCondition = $('input:radio[name="Condition"]:checked').val();
    reqObj.takenMedication = $('input:radio[name="Medication"]:checked').val();
    reqObj.needAnyAssistance = $('input:radio[name="Assistance"]:checked').val();
    reqObj.reasonForCall = $('#taReason').val();
    reqObj.notes = $('#taRemarks').val();
    reqObj.anyOther = $('#taOther').val();
    reqObj.userId = sessionStorage.userId;//101;
    reqObj.patientId = parentRef.patientInfo.id;
    reqObj.deleted = 0;
    //popupClose(false);
    console.log(reqObj);
    var dataUrl = ipAddress+"/patient/calldata/create";
    getAjaxObject(dataUrl,"POST",reqObj,onSavePatieentCallChat,onErrorMedication);
}
function onSavePatieentCallChat(dataobj){
    console.log(dataobj);
    popupClose("success");
    customAlert.info("Save","Patient Call Chart Created Successfully");
}
function onClickCancel(){
    popupClose(false);
}
function onClickPrint(){
    var $this = $(this);
    var originalContent = $('#topContainerPopup').html();
    // var printArea = $this.parents('.printableArea').html();

    $('body').html(originalContent);
    window.print();
}
function popupClose(st){
    var onCloseData = new Object();
    onCloseData.status = st;
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(onCloseData);
}

function onErrorMedication(errobj){
    console.log(errobj);
}
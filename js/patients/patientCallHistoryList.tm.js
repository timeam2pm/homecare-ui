var angularUIgridWrapper;
var parentRef = null;

var patientId = "";

$(document).ready(function(){
	var dataOptions = {
        pagination: false,
        changeCallBack: onChange
	}
	angularUIgridWrapper = new AngularUIGridWrapper("dgridPatientCallHistoryLList", dataOptions);
	angularUIgridWrapper.init();
	buildCallChartGrid([]);
});


$(window).load(function(){
	loading = false;
	$(window).resize(adjustHeight);
onLoaded();
	if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
	parentRef = parent.frames['iframe'].window;
	init();
    buttonEvents();
	adjustHeight();
}

function init(){
	patientId = parentRef.patientId;
	buildCallChartGrid([]);
	getAjaxObject(ipAddress+"/patient/calldata/"+patientId,"GET",getStateList,onError);
}
function onError(errorObj){
	console.log(errorObj);
}
function getStateList(dataObj){
	var dataArray = [];
	if(dataObj){
		if($.isArray(dataObj)){
			dataArray = dataObj;
		}else{
			dataArray.push(dataObj);
		}
	}
	var tempArray = [];
	for(var i=0;i<dataArray.length;i++){
		var dataItem = dataArray[i];
		if(dataItem){
			dataItem.dateOfCall = kendo.toString(new Date(dataItem.dateOfCall),"MM-dd-yyyy");
			tempArray.push(dataItem);
		}
	}
	 buildCallChartGrid(dataArray);
}
function buttonEvents(){
	$("#btnView").off("click",onClickOK);
	$("#btnView").on("click",onClickOK);
	
	$("#btnCancelDet").off("click",onClickCancel);
	$("#btnCancelDet").on("click",onClickCancel);
	
}

function adjustHeight(){
	var defHeight = 120;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
	angularUIgridWrapper.adjustGridHeight(cmpHeight);
}

function buildCallChartGrid(dataSource) {
    var gridColumns = [];
	var otoptions = {};
	otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Call Date",
        "field": "dateOfCall",
        "enableColumnMenu": false,
    });
	gridColumns.push({
        "title": "Begin Time",
        "field": "beginTime",
        "enableColumnMenu": false,
    });
	gridColumns.push({
        "title": "End Time",
        "field": "endTime",
        "enableColumnMenu": false,
    });
	gridColumns.push({
        "title": "Call Duration",
        "field": "callDuration",
        "enableColumnMenu": false,
    });
	angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
	adjustHeight();
}
var prevSelectedItem =[];
function onChange(){
	setTimeout(function(){
		var selectedItems = angularUIgridWrapper.getSelectedRows();
		 console.log(selectedItems);
		 if(selectedItems && selectedItems.length>0){
			 $("#btnView").prop("disabled", false);
		 }else{
			 $("#btnView").prop("disabled", true);
		 }
	},100)
	 
}

function onClickOK(){
	setTimeout(function(){
		 var selectedItems = angularUIgridWrapper.getSelectedRows();
		 console.log(selectedItems);
		 if(selectedItems && selectedItems.length>0){
			 var obj = {};
			 obj = selectedItems[0];
			 onClickPatientCall(obj);
		 }
	})
}
function onClickPatientCall(obj){
	var popW = "60%";
    var popH = 400;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Patient Call Chart";
    parentRef.selObj = obj;
    devModelWindowWrapper.openPageWindow("../../html/patients/callPatient.html", profileLbl, popW, popH, false, closeCallPatient);
}
function closeCallPatient(evt,returnData){
	if(returnData && returnData.status == "success"){
		customAlert.info("Save","Patient Call Chart Created Successfully");
		init();
	}
}
function onClickCancel(){
	var obj = {};
	var windowWrapper = new kendoWindowWrapper();
	windowWrapper.closePageWindow(obj);
}

var parentRef = null;

var patientId = "";

$(document).ready(function(){
});


$(window).load(function(){
	loading = false;
	$(window).resize(adjustHeight);
onLoaded();
	if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
	parentRef = parent.frames['iframe'].window;
	init();
    buttonEvents();
	adjustHeight();
}

function init(){
	patientId = parentRef.patientId;
	getAjaxObject(ipAddress+"/patient/preventative-services/"+patientId,"GET",onGetVitalsSuccess,onError);
	//getAjaxObject(ipAddress+"/patient/reports/holter/"+patientId,"GET",onGetHolterReportsData,onError);
}
function onGetHBTPatientData(dataObj){
	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		getVitals();
	}
}
function getVitals(){
	
}
function getVitalInfo(vName,dt){
	for(var i=0;i<vitalDataArray.length;i++){
		var item = vitalDataArray[i];
		if(item.vital == vName && item.date == dt){
			return item.value;
		}
	}
	return "";
}

function getRowColors(index) {
    var classes = ['active', 'success', 'info', 'warning', 'danger'];
   // console.log(index);
    if (index % 2 === 0 && index / 2 < classes.length) {
    	//console.log(classes[index / 2]);
    	return classes[index / 2];
    	//console.log(index+','+classes[index / 2]);
      /*  return {
            classes: classes[index / 2]
        };*/
    }
    return "";
}
var interestArr = [];
function onGetVitalsSuccess(dataObj){
	$("#divInterests").text("");
	interestArr = [];
	if(dataObj){
		if($.isArray(dataObj)){
			interestArr = dataObj;
		}else{
			interestArr.push(dataObj);
		}
	}
	var strTable = '<table class="table">';
	strTable = strTable+'<thead class="fillsHeader">';
	strTable = strTable+'<tr>';
	strTable = strTable+'<th class="textAlign whiteColor">Question</th>';
	strTable = strTable+'<th class="textAlign whiteColor">Answer</th>';
	strTable = strTable+'<th class="textAlign whiteColor">Action</th>';
	strTable = strTable+'</tr>';
	strTable = strTable+'</thead>';
	strTable = strTable+'<tbody>';
	for(var i=0;i<interestArr.length;i++){
		var dataItem = interestArr[i];
		if(dataItem){
			//console.log(dataItem);
			var className = getRowColors(i);
			strTable = strTable+'<tr  class="'+className+'" style="cursor:pointer">';
			//strTable = strTable+'<td>'+dataItem.patientId+'</td>';
			strTable = strTable+'<td>'+dataItem.question+'</td>';
			//strTable = strTable+'<td>'+dataItem.link+'</td>';
			strTable = strTable+'<td>'+dataItem.answer+'</td>';
			strTable = strTable+'<td style="cursor:pointer"><span id="'+dataItem.id+'" onClick="myRowFunction(event)">Edit</span></td>';
			strTable = strTable+'</tr>';
		}
	}
	strTable = strTable+'</tbody>';
	strTable = strTable+'</table>';
	$("#divInterests").append(strTable);
}
function myRowFunction(event){
	console.log(event);
	var evtId = "";
	if(event.currentTarget){
		evtId = event.currentTarget.id;
	}
	evtId = Number(evtId)-1;
	var dataObj = interestArr[evtId];
	parentRef.screenType = "Edit";
	if(dataObj){
		parentRef.dataObj = dataObj;
		onClickCreateInterest();
	}
}
function onClickCreateInterest(){
	var popW = "67%";
    var popH = 450;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Create Preventive";
    devModelWindowWrapper.openPageWindow("../../html/patients/createInterest.html", profileLbl, popW, popH, true, closeInterestScreen);
}
function closeInterestScreen(evt,returnData){
	if(returnData && returnData.status == "success"){
		init();
	}
}
var imgImage = null;
function onClickGenerate(event){
	console.log(event);
	var dietVideoId = "";
	if(imgImage){
		$(imgImage).removeClass("imgBorder");
	}
	
	if(event.currentTarget){
		dietVideoId = event.currentTarget.id;
		imgImage = event.currentTarget;
		$(imgImage).addClass("imgBorder");
	}
	var fileType = findFileType(dietDataArray,dietVideoId);
	showPdfVideo(dietVideoId,fileType,"diet");
}
function showPdfVideo(fileId,fileType,sType){
	parentRef.dietId = fileId;
	parentRef.sType = sType;
	var urlPath = "";
	if(sType == "diet"){
		urlPath = ipAddress+"/patient/diet/download/"
	}else{
		urlPath = ipAddress+"/patient/exercise/download/"
	}
	var popW =900;
    var popH = 580;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
	if(fileType && fileType.toLowerCase() == "pdf"){
		 profileLbl = "Document";
		 popW = 1100;
		 devModelWindowWrapper.openPageWindow("../../html/patients/showPdf.html", profileLbl, popW, popH, true, closeVideoScreen);
			//var reqUrl = urlPath+fileId;
			//window.open(reqUrl, "popupWindow", "width=1000,height=600,scrollbars=yes");
			//$(imgImage).removeClass("imgBorder");
			//$(imgImage).removeClass("textBorder");
	}else{
	    profileLbl = "Video";
	    devModelWindowWrapper.openPageWindow("../../html/patients/showVideo.html", profileLbl, popW, popH, true, closeVideoScreen);
	}
}
function onClickDietYoutube(event){
	var dietVideoId = "";
	var urlPath = "";
	if(imgImage){
		$(imgImage).removeClass("imgBorder");
	}
	
	if(event.currentTarget){
		dietVideoId = event.currentTarget.id;
		imgImage = event.currentTarget;
		$(imgImage).addClass("imgBorder");
	}
	for(var i=0;i<dietDataArray.length;i++){
		var item = dietDataArray[i];
		if(item && item.id == event.currentTarget.id){
			urlPath = item.youtubeLink;
			iItem = item;
			break;
		}
	}
	if(urlPath && urlPath.indexOf("www.youtube.com")>=0){
		playYouTubeFile(urlPath);
	}
}
function playYouTubeFile(urlPath){
	var popW =900;
    var popH = 580;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Video";
    parentRef.sType = "illness";
    parentRef.illUrlPath = urlPath;
    devModelWindowWrapper.openPageWindow("../../html/patients/showVideo.html", profileLbl, popW, popH, true, closeVideoScreen);
}
var imgImage = null;
function onClickMyAudio(event){
	var audioId = "";
	if(imgImage){
		$(imgImage).removeClass("textBorder");
	}
	
	if(event.currentTarget){
		audioId = event.currentTarget.id;
		imgImage = event.currentTarget;
		$(imgImage).addClass("textBorder");
	}
	console.log(audioId);
	if(audioId && audioId != "null"){
		playAudioFile(audioId);
	}
}
function playAudioFile(aid){
	parentRef.audioId = aid;
	var popW = 600;
    var popH = 200;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Audio";
    devModelWindowWrapper.openPageWindow("../../html/patients/showAudio.html", profileLbl, popW, popH, true, closeVideoScreen);
}
function closeVideoScreen(evt,returnData){
	//$(imgImage).removeClass("imgBorder");
	//$(imgImage).removeClass("textBorder");
}
function onError(errorObj){
	console.log(errorObj);
}
var holterReportDataArray = [];
function onGetHolterReportsData(dataObj){
		$("#divTable").text("");
		var dataArray = [];
		if(dataObj){
			if($.isArray(dataObj)){
				dataArray = dataObj;
			}else{
				dataArray.push(dataObj);
			}
		}
		holterReportDataArray = dataArray;
		var strTable = '<table class="table">';
		strTable = strTable+'<thead class="fillsHeader whiteColor">';
		strTable = strTable+'<tr>';
		//strTable = strTable+'<th>Patient ID</th>';
		strTable = strTable+'<th class="textAlign whiteColor" style="width:220px">Date Generated</th>';
		strTable = strTable+'<th class="textAlign whiteColor">File Name</th>';
		strTable = strTable+'<th class="textAlign whiteColor">File Type</th>';
		strTable = strTable+'</tr>';
		strTable = strTable+'</thead>';
		strTable = strTable+'<tbody>';
		for(var i=0;i<holterReportDataArray.length;i++){
			var dataItem = holterReportDataArray[i];
			if(dataItem){
				//console.log(dataItem);
				var className = getRowColors(i);
				strTable = strTable+'<tr class="'+className+'">';
				//strTable = strTable+'<td>'+dataItem.patientId+'</td>';
				strTable = strTable+'<td class="textAlign">'+kendo.toString(new Date(dataItem.createdDate),"MM-dd-yyyy h:mm:ss")+'</td>';
				strTable = strTable+'<td class="textAlign">'+dataItem.fileName+'</td>';
				var dietId = dataItem.id;
				if(dataItem.fileType == "mp4"){
					strTable = strTable+'<td style="width:200px;padding:0px" class="textAlign"><img id="'+dietId+'" src="../../img/AppImg/HosImages/video.png"  class="cusrsorStyle videoIcon" onClick="onClickGenerate(event)"></td>';
				}else if(dataItem.fileType == "pdf"){
					strTable = strTable+'<td style="width:200px;padding:0px" class="textAlign"><img  id="'+dietId+'" src="../../img/AppImg/HosImages/pdf.png"    class="cusrsorStyle pdfIcon" onClick="onClickHolterReport(event)"></td>';
				}
				strTable = strTable+'</tr>';
			}
		}
		strTable = strTable+'</tbody>';
		strTable = strTable+'</table>';
		$("#divHolterReports").append(strTable);
	}
function buttonEvents(){
	$("#btnCancelDet").off("click",onClickCancel);
	$("#btnCancelDet").on("click",onClickCancel);
	
	$("#btnCreate").off("click",onClickCreate);
	$("#btnCreate").on("click",onClickCreate);
	
}

function adjustHeight(){
	var defHeight = 60;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    $("#divPtHolter").height(cmpHeight);
	//angularUIgridWrapper.adjustGridHeight(cmpHeight);
}


function onClickPatientCall(obj){
	var popW = "60%";
    var popH = 400;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Patient Call Chart";
    parentRef.selObj = obj;
    devModelWindowWrapper.openPageWindow("../../html/patients/callPatient.html", profileLbl, popW, popH, false, closeCallPatient);
}
function closeCallPatient(evt,returnData){
	if(returnData && returnData.status == "success"){
		customAlert.info("Save","Patient Call Chart Created Successfully");
		init();
	}
}
function onClickCreate(){
	parentRef.screenType = "Create";
    // parentRef.screenType = "Create";
		var popW = "67%";
	    var popH = 450;
	    var profileLbl;
	    var devModelWindowWrapper = new kendoWindowWrapper();
	    profileLbl = "Create Preventive";
	    devModelWindowWrapper.openPageWindow("../../html/patients/createInterest.html", profileLbl, popW, popH, true, closeInterestScreen);
}
function closeInterestScreen(evt,returnData){
	if(returnData && returnData.status == "success"){
		//customAlert.error("Save","Created");
		init();
	}
}
function onClickCancel(){
	var obj = {};
	var windowWrapper = new kendoWindowWrapper();
	windowWrapper.closePageWindow(obj);
}

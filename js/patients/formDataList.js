var angularUIgridWrapper;
var angularUIgridWrapper1;
var parentRef = null;
var recordType = "1";
var dataArray = [];
var dataTypeArray = [{Key:'Text',Value:'1'},{Key:'Number',Value:'2'},{Key:'Boolean',Value:'3'}];
var unitArray =  [{Key:'Kg',Value:'1'},{Key:'Lt',Value:'2'}];
var statusArray =  [{Key:'ACTIVE',Value:'1'},{Key:'INACTIVE',Value:'2'}];

$(document).ready(function(){
	parentRef = parent.frames['iframe'].window;
	//recordType = parentRef.dietType;
	var dataOptions = {
        pagination: false,
        changeCallBack: onChange
	}
	angularUIgridWrapper = new AngularUIGridWrapper("dgridDataDesignTemplateList", dataOptions);
	angularUIgridWrapper.init();
	
	var dataOptions1 = {
	        pagination: false,
	        changeCallBack: onChange1
		}
		angularUIgridWrapper1 = new AngularUIGridWrapper("dgridDataTemplateList", dataOptions1);
		angularUIgridWrapper1.init();
		
	buildDeviceListGrid([]);
	buildDeviceListGrid1([]);
});

$(window).load(function(){
	loading = false;
	$(window).resize(adjustHeight);
onLoaded();
	if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
	 $("#btnEdit").prop("disabled", true);
	 $("#btnDelete").prop("disabled", true);
	//$("#cmbTemplates").kendoComboBox();
	//$("#txtCompType").kendoComboBox();
	//$("#txtDT").kendoComboBox();
	//$("#cmbUnits").kendoComboBox();
	$("#cmbProvider").kendoComboBox();
	$("#cmbPatients").kendoComboBox();
	
	//setDataForSelection(dataTypeArray, "txtDT", function(){}, ["Key", "Value"], 0, "");
	//setDataForSelection(unitArray, "cmbUnits", function(){}, ["Key", "Value"], 0, "");
	//setDataForSelection(statusArray, "cmbStatus", function(){}, ["Key", "Value"], 0, "");
	
	//$("#txtChars").kendoNumericTextBox({step:1,min:1,max:255,decimals: 0,value:10,format: "0",change:onChangeMETime});
	 //getTemplates();
	 var ipaddress = ipAddress+"/homecare/templates/?is-active=1&is-deleted=0";
		getAjaxObject(ipaddress,"GET",handleGetTemplateList,onError);
		
		var patientListURL = ipAddress+"/patient/list/"+sessionStorage.userId;
		if(sessionStorage.userTypeId == "200" || sessionStorage.userTypeId == "100"){
			patientListURL = ipAddress+"/patient/list?is-active=1";
		}else{
			
		}
		 getAjaxObject(patientListURL,"GET",onPatientListData,onError);
		 
		//getComponents();
		//getDataTypes();
		//getUnits();
	//init();
    buttonEvents();
	adjustHeight();
}
function onPatientListData(dataObj){
	var tempArray = [];
	var dataArray = [];
	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		if(dataObj.response.patient){
			if($.isArray(dataObj.response.patient)){
				tempArray = dataObj.response.patient;
			}else{
				tempArray.push(dataObj.response.patient);
			}	
		}
	}else{
		customAlert.error("Error",dataObj.response.status.message);
	}
	
	for(var i=0;i<tempArray.length;i++){
		var obj = tempArray[i];
		if(obj){
			var dataItemObj = {};
			dataItemObj.PID = obj.id;
			dataItemObj.FN = obj.firstName;
			dataItemObj.LN = obj.lastName;
			dataItemObj.WD = obj.weight;
			dataItemObj.HD = obj.height;
			  if(obj.middleName){
				  dataItemObj.MN = obj.middleName;
			  }else{
				  dataItemObj.MN =  "";
			  }
			  dataItemObj.GR = obj.gender;
			  if(obj.status){
				  dataItemObj.ST = obj.status;
			  }else{
				  dataItemObj.ST = "ACTIVE";
			  }
			  dataArray.push(dataItemObj);
		}
	}
	setDataForSelection(dataArray, "cmbPatients", function(){}, ["FN", "PID"], 0, "");
}
function getComponents(){
	getAjaxObject(ipAddress+"/master/type/list/?name=component","GET",getComponentTypes,onError);
}
function getDataTypes(){
	getAjaxObject(ipAddress+"/homecare/data-types/","GET",onGetDataTypes,onError);
}
function onGetDataTypes(dataObj){
	console.log(dataObj);
	var tempDataType = [];
	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		if(dataObj.response.dataTypes){
			if($.isArray(dataObj.response.dataTypes)){
				tempDataType = dataObj.response.dataTypes;
			}else{
				tempDataType.push(dataObj.response.dataTypes);
			}
		}
	}
	setDataForSelection(tempDataType, "txtDT", function(){}, ["value", "id"], 0, "");
}
function getUnits(){
	getAjaxObject(ipAddress+"/homecare/units/","GET",onGetUnits,onError);
}
function onGetUnits(dataObj){
	console.log(dataObj);
	var tempUnits = [];
	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		if(dataObj.response.units){
			if($.isArray(dataObj.response.units)){
				tempUnits = dataObj.response.units;
			}else{
				tempUnits.push(dataObj.response.units);
			}
		}
	}
	setDataForSelection(tempUnits, "cmbUnits", function(){}, ["value", "id"], 0, "");
}
function getComponentTypes(dataObj){
	var tempCompType = [];
	compType = [];
	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		if(dataObj.response.typeMaster){
			if($.isArray(dataObj.response.typeMaster)){
				tempCompType = dataObj.response.typeMaster;
			}else{
				tempCompType.push(dataObj.response.typeMaster);
			}
		}
	}
	for(var i=0;i<tempCompType.length;i++){
		var obj = {};
		obj.Key = tempCompType[i].type;
		obj.Value = tempCompType[i].id;
		compType.push(obj);
	}
	setDataForSelection(compType, "txtCompType", function(){}, ["Key", "Value"], 0, "");
}
function onChangeMETime(event){
	onChangeNumericBox(event);
}
function onChangeNumericBox(event){
	if(event && event.sender && event.sender.element){
			var boxId = $(event.sender.element).attr("id");
			if(boxId){
				var txtSpBandBegin1 = $("#"+boxId).data("kendoNumericTextBox");
				 if(txtSpBandBegin1){
					 if(txtSpBandBegin1.value() == ""){
						 return;
					 }
					 if(txtSpBandBegin1 && txtSpBandBegin1.value() !=0  && !txtSpBandBegin1.value()){
						 var maxVal = txtSpBandBegin1.min();
						 //txtSpBandBegin1.value(maxVal);
					 }
				 }
			}
	}
}
function handleGetTemplateList(dataObj){
	console.log(dataObj);
	var vacation = [];
	if(dataObj && dataObj.response && dataObj.response.status ){
		if(dataObj.response.status.code == "1"){
			if(dataObj.response.templates){
				if($.isArray(dataObj.response.templates)){
					vacation = dataObj.response.templates;
				}else{
					vacation.push(dataObj.response.templates);
				}
			}
		}
	}
	for(var j=0;j<vacation.length;j++){
		vacation[j].idk = vacation[j].id; 
	}
	//setDataForSelection(vacation, "cmbTemplates", onTemplateChange, ["name", "idk"], 0, "");
	//onTemplateChange();
	buildDeviceListGrid1(vacation);
}
function onTemplateChange(){
	$("#divFormData").html("");
	buildDeviceListGrid1([])
	var cmbTemplates = $("#cmbTemplates").data("kendoComboBox");
     buildDeviceListGrid1([]);
     var selectedItems = cmbTemplates.dataItem();
     	var miniTemplates = selectedItems.miniTemplates;
     	var arr = [];
     	if($.isArray(miniTemplates)){
     		arr = miniTemplates;
     	}else{
     		arr.push(miniTemplates);
     	}
     	var miniArray = [];
     	for(var x1=0;x1<miniTemplates.length;x1++){
     		miniTemplates[x1].idk = miniTemplates[x1].id;
     		var obj = {};
     		obj.idk = miniTemplates[x1].id;
     		obj.name = miniTemplates[x1].name;
     		obj.notes = miniTemplates[x1].notes;
     		miniArray.push(obj);
     	}
     	buildDeviceListGrid1(miniArray);
     	onChange1();
}
/*function getTemplates(){
	var ipaddress = ipAddress+"/homecare/form-templates/";
	getAjaxObject(ipaddress,"GET",handleGetTemplateList,onError);
}*/
/*function handleGetTemplateList(dataObj){
	var vacation = [];
	if(dataObj && dataObj.response && dataObj.response.status ){
		if(dataObj.response.status.code == "1"){
			if(dataObj.response.formTemplates){
				if($.isArray(dataObj.response.formTemplates)){
					vacation = dataObj.response.formTemplates;
				}else{
					vacation.push(dataObj.response.formTemplates);
				}
			}
		}
	}
	for(var j=0;j<vacation.length;j++){
		vacation[j].idk = vacation[j].id; 
	}
	setDataForSelection(vacation, "cmbTemplates", function(){}, ["name", "idk"], 0, "");
}*/
function init(){
	 $("#btnEdit").prop("disabled", true);
	 $("#btnDelete").prop("disabled", true);
	buildDeviceListGrid([]);
	//buildDeviceListGrid1([]);
	  var selectedItems = angularUIgridWrapper1.getSelectedRows();
      console.log(selectedItems);
      $("#lblFrmData").text("");
      if(selectedItems && selectedItems.length>0){
    	  $("#lblFrmData").text(selectedItems[0].notes);
    	  var idk = selectedItems[0].idk;
    	  	var ipaddress = ipAddress+"/homecare/template-values/?templateId="+idk+"&patientId=101&fields=notes,patientId,templateId,CreatedDate,providerId";
    		getAjaxObject(ipaddress,"GET",handleGetVacationList,onError);
      }
	
	//getAjaxObject(ipAddress+"/homecare/activity-types/?is-active=1&is-deleted=0&sort=display-order","GET",getActivityTypes,onError);
}
var frmDesignArray = [];
function handleGetVacationList(dataObj){
	console.log(dataObj);
	frmDesignArray = [];
	if(dataObj && dataObj.response && dataObj.response.status ){
		if(dataObj.response.status.code == "1"){
			if(dataObj.response.templateValues){
				if($.isArray(dataObj.response.templateValues)){
					frmDesignArray = dataObj.response.templateValues;
				}else{
					frmDesignArray.push(dataObj.response.templateValues);
				}
			}
		}
	}
	for(var i=0;i<frmDesignArray.length;i++){
		var dt = new Date(frmDesignArray[i].createdDate);
		dt = kendo.toString(dt, "MM-dd-yyyy h:mm:tt");
		frmDesignArray[i].crDate = dt;
	}
	buildDeviceListGrid(frmDesignArray);
	/*var strHtml = "";
	$("#divFormData").html("");
	for(var j=0;j<frmDesignArray.length;j++){
		var frmObj = frmDesignArray[j];
		frmObj.idk = frmObj.id;
		frmObj.length1 = frmObj.length;
		var frmId = "f"+frmObj.idk;
		var unit  ="";
		if(frmObj.unitValue){
			unit = frmObj.unitValue;
		}else{
		
		}
		strHtml = strHtml+'<div class="col-sm-6 col-md-6 col-lg-6 col-xs-6 form-group">';
		strHtml = strHtml+'<label class="col-xs-12 col-sm-12 col-md-12 col-lg-12 control-label input-sm noPadding">'+frmObj.name+' :</label>';
		strHtml = strHtml+'<div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 noPadding">';
		strHtml = strHtml+'<div class="col-sm-9 col-md-9 col-lg-9 col-xs-8 noPadding">';
		strHtml = strHtml+'	<input id="'+frmId+'" type="text" class="form-control input-sm"></input>';
		strHtml = strHtml+'</div>';
		strHtml = strHtml+'<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label input-sm noPadding">&nbsp;&nbsp;'+unit+'</label>';
		strHtml = strHtml+'</div></div>';
	}
	$("#divFormData").html(strHtml);*/
	//getFormData();
	//buildDeviceListGrid(vacation);
}
function getFormData(){
	 var selectedItems = angularUIgridWrapper1.getSelectedRows();
     console.log(selectedItems);
	var ipaddress = ipAddress+"/homecare/component-values/?is-active=1&is-deleted=0&miniTemplateId="+selectedItems[0].idk;
	getAjaxObject(ipaddress,"GET",handleGetFormDataValues,onError);
}
function handleGetFormDataValues(dataObj){
	console.log(dataObj);
}
function onError(errorObj){
	console.log(errorObj);
}

function buttonEvents(){
	$("#btnSave").off("click",onClickSave);
	$("#btnSave").on("click",onClickSave);
	
	$("#btnView").off("click",onClickView);
	$("#btnView").on("click",onClickView);
	
	$("#btnReport").off("click",onClickReport);
	$("#btnReport").on("click",onClickReport);
	
	
	$("#btnCancelDet").off("click",onClickCancel);
	$("#btnCancelDet").on("click",onClickCancel);
	
	
	$("#btnEdit").off("click",onClickEdit);
	$("#btnEdit").on("click",onClickEdit);
	
	$("#btnDelete").off("click",onClickDelete);
	$("#btnDelete").on("click",onClickDelete);
}
function onClickReport(){
	var popW = "70%";
    var popH = "70%";

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Template Patient Report";
    
    devModelWindowWrapper.openPageWindow("../../html/masters/patientTemplateReport.html", profileLbl, popW, popH, true, closeAddPatientAction);
}
function closeAddPatientAction(evt,returnData){
	
}
function onClickDelete(){
	customAlert.confirm("Confirm", "Are you sure to delete?",function(response){
		if(response.button == "Yes"){
			var dataObj = {};
		    dataObj.createdBy = Number(sessionStorage.userId);; 
		    dataObj.isDeleted = 1; 
		    dataObj.isActive = 0; 
		    var dataUrl = ipAddress +"/homecare/components/";
		    var method = "DELETE";
		    var selectedItems = angularUIgridWrapper.getSelectedRows();
		    dataObj.id = selectedItems[0].idk;
		    createAjaxObject(dataUrl, dataObj, method, onCreateDelete, onError);
		}
	});
	
}
function onCreateDelete(dataObj){
	if(dataObj && dataObj.response && dataObj.response.status ){
		if(dataObj.response.status.code == "1"){
			var msg = "Template deleted successfully";
			customAlert.error("Info", msg);
			operation = ADD;
			init();
		}
	}
}
function onClickView(){
	 var selGridData = angularUIgridWrapper1.getAllRows();
	 var selList = [];
	 var strIds  = "";
	    for (var i = 0; i < selGridData.length; i++) {
	        var dataRow = selGridData[i].entity;
	        if(dataRow.SEL){
	        	strIds = strIds+dataRow.idx+",";
	        	selList.push(dataRow);
	        }
	    }  
	    console.log(selList);
	    if(selList.length>0){
	    	strIds = strIds.substring(0,strIds.length-1);
	    	 var ipaddress = ipAddress+"/homecare/components/?is-active=1&is-deleted=0&fields=*,component.*,dataType.*,unit.*&miniTemplateId=:in:"+strIds;
	 		getAjaxObject(ipaddress,"GET",handleGetMiniComponentsValues,onError);
	    }
}
function handleGetMiniComponentsValues(dataObj){
	console.log(dataObj);
}
function onCloseVacation(evt,returnData){
	init();
}
function onClickSave(){
	 var selectedItems = angularUIgridWrapper1.getSelectedRows();
     console.log(selectedItems);
     if(selectedItems && selectedItems.length>0){
    	 var popW = "40%";
    	    var popH = "75%";

    	    var profileLbl;
    	    var devModelWindowWrapper = new kendoWindowWrapper();
    	    profileLbl = "From Data Entry";
    	    parentRef.selItem = selectedItems[0];
    	    devModelWindowWrapper.openPageWindow("../../html/masters/formDataEntry.html", profileLbl, popW, popH, true, onCloseVacation);
     }
	
	/*try{
		  var selectedItems = angularUIgridWrapper1.getSelectedRows();
	      console.log(selectedItems);
	      var miniCompArray = [];
		for(var f=0;f<frmDesignArray.length;f++){
			var fId = frmDesignArray[f].idk;
			fId = "f"+fId;
			var fData = $("#"+fId).val();
			var cmpId = frmDesignArray[f].idk;
			
			var obj = {};
			obj.miniComponentId = cmpId;
			obj.miniTemplateId = selectedItems[0].idk;
			obj.value = fData;
			obj.isDeleted = 0;
			obj.createdBy = 101;
			obj.isActive = 1;
			miniCompArray.push(obj);
		}
		var cmbTemplates = $("#cmbTemplates").data("kendoComboBox");
		var reqObj = {};
		reqObj.notes = "";
		reqObj.patientId = 101;
		reqObj.isActive = 1;
		reqObj.isDeleted = 0;
		reqObj.reportedOn = new Date().getTime();
		reqObj.createdBy = 101;
		reqObj.providerId = 101;
		reqObj.formTemplateId = Number(cmbTemplates.value());
		reqObj.miniComponentValues = miniCompArray;
		
			console.log(		reqObj);
			var method = "POST";
			if(operation == UPDATE){
				var selectedItems1 = angularUIgridWrapper.getSelectedRows();
				reqObj.id = selectedItems1[0].idk;
				method = "PUT";
			}
			dataUrl = ipAddress +"/homecare/form-template-values/";
			createAjaxObject(dataUrl, reqObj, method, onCreate, onError);
	}catch(ex){
		console.log(ex);
	}*/
}
function onCreate(dataObj){
	console.log(dataObj);
	if(dataObj && dataObj.response && dataObj.response.status ){
		if(dataObj.response.status.code == "1"){
			var msg = "";
			msg = "Form  Created  Successfully";
			if(operation == UPDATE){
				msg = "Form  Updated  Successfully";
			}
			displaySessionErrorPopUp("Info", msg, function(res) {
				resetData();
				operation = ADD;
				//onChange1();
				//init();
	        })
		}else{
			customAlert.error("Error", dataObj.response.status.message);
		}
	}else{
		
	}
}

function resetData(){
	for(var f=0;f<frmDesignArray.length;f++){
		var fId = frmDesignArray[f].idk;
		fId = "f"+fId;
		 $("#"+fId).val("");
	}
}
function adjustHeight(){
	var defHeight = 200;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    var frm = (cmpHeight-250)+"px";
    //$("#divFormData").css({height:frm});
    if(angularUIgridWrapper){
    	angularUIgridWrapper.adjustGridHeight((cmpHeight));
    }
    if(angularUIgridWrapper1){
    	angularUIgridWrapper1.adjustGridHeight((cmpHeight+50));
    }
}

function buildDeviceListGrid(dataSource) {
	var gridColumns = [];
	var otoptions = {};
	otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Date",
        "field": "crDate",
	});
    gridColumns.push({
        "title": "Notes",
        "field": "notes",
	});
   angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions); 
	adjustHeight();
	//onIndividualRowClick();
}
function buildDeviceListGrid1(dataSource) {
	var gridColumns = [];
	var otoptions = {};
	otoptions.noUnselect = false;
	 /*gridColumns.push({
	        "title": "Select",
	        "field": "SEL",
	        "cellTemplate": showCheckBoxTemplate(),
	        "width":"15%"
		});*/
    gridColumns.push({
        "title": "Template",
        "field": "name",
	});
   angularUIgridWrapper1.creategrid(dataSource, gridColumns,otoptions); 
	adjustHeight();
	//onIndividualRowClick();
}
function showCheckBoxTemplate(){
	var node = '<div style="text-align:center;padding-top: 6px;"><input type="checkbox" class="check1" id="chkbox" ng-model="row.entity.SEL" style="width:20px;height:20px;margin:0px;"   onclick="onSelect(event);"></input></div>';
	return node;
}
function onSelect(evt){
	console.log(evt);
}
var selRow = null;
function onClickAction(){
	
}

var prevSelectedItem =[];
function onChange(){
	setTimeout(function() {
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if (selectedItems && selectedItems.length > 0) {
        	var obj = selectedItems[0];
        	atID = obj.idk;
        	$("#btnEdit").prop("disabled", false);
            $("#btnDelete").prop("disabled", false);
        	//onClickEdit();
        } else {
            $("#btnEdit").prop("disabled", true);
            $("#btnDelete").prop("disabled", true);
        	//resetData();
        }
    });
}
function onChange1(){
	setTimeout(function() {
        var selectedItems = angularUIgridWrapper1.getSelectedRows();
        console.log(selectedItems);
        //buildDeviceListGrid([]);
        if(selectedItems && selectedItems.length>0){
        	$("#divEvents").removeClass("addEvents");
        	/*var miniTemplates = selectedItems[0].miniTemplates;
        	var arr = [];
        	if($.isArray(miniTemplates)){
        		arr = miniTemplates;
        	}else{
        		arr.push(miniTemplates);
        	}
        	for(var x1=0;x1<miniTemplates.length;x1++){
        		miniTemplates[x1].idk = miniTemplates[x1].id;
        	}
        	buildDeviceListGrid(miniTemplates);*/
        	init();
        }else{
        	$("#divEvents").addClass("addEvents");
        }
	});
}
var operation = "ADD";
var ADD = "ADD";
var UPDATE = "UPDATE";
var atID = "";
var ds  = "";
function onClickEdit(){
	var selectedItems = angularUIgridWrapper.getSelectedRows();
    console.log(selectedItems);
    if (selectedItems && selectedItems.length > 0) {
    	var obj = selectedItems[0];
    	
    	 var ipaddress = ipAddress+"/homecare/template-values/?templateId="+obj.templateId+"&patientId="+obj.patientId+"&createdDate="+obj.createdDate;//+"&fields=*,dataType.*,unit.*,component.*";
			getAjaxObject(ipaddress,"GET",handleGetFormValues,onError);
    	/*operation = UPDATE;
    	$("#txtCompName").val(obj.name);
    	$("#txtNotes").val(obj.notes);
    	getComboListIndex("txtCompType", "Value", obj.componentTypeId);
    	getComboListIndex("txtDT", "Value", obj.dataTypeId);
    	getComboListIndex("cmbUnits", "Value", obj.unitId);*/
    }
}

function handleGetFormValues(dataObj){
	console.log(dataObj);
	if(dataObj && dataObj.response && dataObj.response.status ){
		if(dataObj.response.status.code == "1"){
			 var selectedItems1 = angularUIgridWrapper.getSelectedRows();
		     console.log(selectedItems1);
		     var selectedItems = angularUIgridWrapper1.getSelectedRows();
		     console.log(selectedItems);
		     if(selectedItems && selectedItems.length>0){
		    	 var popW = "40%";
		    	    var popH = "60%";

		    	    var profileLbl;
		    	    var devModelWindowWrapper = new kendoWindowWrapper();
		    	    profileLbl = "From Data Edit";
		    	    parentRef.selItem1 = selectedItems1[0];
		    	    parentRef.selItem = selectedItems[0];
		    	    parentRef.dataObj = dataObj;
		    	    devModelWindowWrapper.openPageWindow("../../html/masters/formDataEdit.html", profileLbl, popW, popH, true, onCloseVacation);
		     }
		}
	}
}
function getComboListIndex(cmbId, attr, attrVal) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb) {
        var ds = cmb.dataSource;
        var totalRec = ds.total();
        for (var i = 0; i < totalRec; i++) {
            var dtItem = ds.at(i);
            if (dtItem && dtItem[attr] == attrVal) {
                cmb.select(i);
                return i;
            }
        }
    }
    return -1;
}
function closeVideoScreen(evt,returnData){
	
}
var operation = "add";
var selFileResourceDataItem = null;
function onClickCancel(){
	var obj = {};
	var windowWrapper = new kendoWindowWrapper();
	windowWrapper.closePageWindow(obj);
}

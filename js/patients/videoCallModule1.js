var previewMedia = null;
var ws = null;
var userCalling = false;
var activeRoom = null;
var participantArray = [];
var userList = [];

var delayTime = 60000;

var twilioAccessToken = "";
$(document).ready(function(){
	parentRef = parent.frames['iframe'].window;
});
$(window).load(function(){
	triggerEvent();
	showPanelHeight();
	adjustVideoHeight();
	buttonEvents();
	init();
});
$(window).resize(function(){
	showPanelHeight();
	//adjustHeight();
	adjustVideoHeight();
});
var pnlHeight = 0;
function showPanelHeight(){
	pnlHeight = window.innerHeight;
	pnlHeight = pnlHeight-30;
	$("#leftPanel").height(pnlHeight);
	$("#centerPanel").height(pnlHeight);
	$("#rightPanel").height(pnlHeight);
	$("#leftButtonBar").height(pnlHeight);
	$("#divPanel").height(pnlHeight);
	$("#divVideoCall").height((pnlHeight-20));
}
var imgHeight = 0;
function adjustVideoHeight(){
	var pnlHeight = window.innerHeight;
	imgHeight = pnlHeight-60;
	var ptHeight = imgHeight/2-20;
	
	var nurHeight = (imgHeight-ptHeight);
	/*if(pnlHeight<=550){
		imgHeight = pnlHeight-250;
	}*/
	console.log(pnlHeight);
	$("#divPtVideo").height((ptHeight));
	//$("#patient-media").height((ptHeight));
	
	$("#divNurVideo").height((nurHeight/2));
	//$("#remote-media").height((nurHeight/2));
	
	$("#divLocVideo").height((nurHeight/2));
	//$("#local-media video").height((nurHeight/2));
}
function buttonEvents(){
	 $("#btnCall").off("click");
	    $("#btnCall").on("click",onClickCall);
	    
	    $("#btnAccept").off("click");
	    $("#btnAccept").on("click",onClickAccept);
	    
	    $("#btnDecline").off("click");
	    $("#btnDecline").on("click",onClickDecline);
	    
	    $("#btnDisconnect").off("click");
	    $("#btnDisconnect").on("click",onClickDisconnect);
	    
}
function init(){
	if(sessionStorage.uName == "nurse"){
		//$("#txtInviteName").val("doctor");
	}
	 if((sessionStorage.userTypeId == "500" || sessionStorage.userTypeId == "600") || sessionStorage.userId != 101 || sessionStorage.userId != 104 || sessionStorage.userId != 100){
		 getAjaxObject(ipAddress+"/user/list", "GET", onGetUserList, onErrorMedication);
	 }
	
}
function onGetUserList(dataObj){
	console.log(dataObj);
	userList = [];
	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		if($.isArray(dataObj.response.user)){
			userList = dataObj.response.user;
		}else{
			userList.push(dataObj.response.user);
		}
	}
	//showLocalPreview();
	webSocketConnect();
}
function initTwilio(){
	if(videoCallVer == "1.0"){
		var accessManager = new Twilio.AccessManager(twilioAccessToken);
		// create a Conversations Client and connect to Twilio
		conversationsClient = new Twilio.Conversations.Client(accessManager);
		conversationsClient.listen().then(
		  clientConnected,
		  function (error) {
		   console.log('Could not connect to Twilio: ' + error.message);
		  }
		);
	}else{
		//if(sessionStorage.uName == "nurse1"){
			inviteTo =   sessionStorage.userId;//"room1";
			videoClient = new Twilio.Video.Client(twilioAccessToken);
			 videoClient.connect({ to: inviteTo}).then(roomJoined,
				        function(error) {
				 				log('Could not connect to Twilio: ' + error.message);
				        });
		//}
	}
}

function showLocalPreview(){
	if (!previewMedia) {
		if(videoCallVer == "1.0"){
		    previewMedia = new Twilio.Conversations.LocalMedia();
		    Twilio.Conversations.getUserMedia().then(
		      function (mediaStream) {
		        previewMedia.addStream(mediaStream);
		       // $("#local-media video").height(300);
		        previewMedia.attach('#local-media');
		      },
		      function (error) {
		        console.error('Unable to access local media', error);
		        console.log('Unable to access Camera and Microphone');
		      }
		    );
		  }else{
			//  $("#local-media video").height(300);
			  setInterval(function(){setAudioType()});
			  previewMedia = new Twilio.Video.LocalMedia();
			    Twilio.Video.getUserMedia().then(
			    function (mediaStream) {
			    	previewMedia.addStream(mediaStream);
			    	previewMedia.attach('#local-media');
			      setTimeout(function(){
			    	  $("#local-media").find("video").height($("#divLocVideo").height());//css('cssText', "130px !important");
			    	  if(callType == AURIO_TYPE){
			    		  previewMedia.removeCamera();
			    		  previewMedia.stop();
			    		  $("#local-media").find("video").hide();
			    		  $("#local-media").find("video").remove();
			    	  }else{
			    		  $("#local-media").find("video").show();
			    	  }
			    	 
			      },100);
			     
			     // previewMedia.attach('#patient-media');
			     // previewMedia.attach('#remote-media');
			      $("#lblLogin").text(sessionStorage.uName);
			    },
			    function (error) {
			      console.error('Unable to access local media', error);
			      log('Unable to access Camera and Microphone');
			    });
			    
			    $("#btnLocMute").off("click");
			    $("#btnLocMute").on("click",onClickLocalMute);
		  }
	}
}
function onClickLocalMute(){
	console.log(previewMedia);
	/*var audioExist = $("#local-media").find("audio")[0];
	if(audioExist){
		$("#local-media").find("audio")[0].remove();
	}else{
		$("#local-media").append("<audio></audio>");
	}*/
	if(previewMedia){
		if(previewMedia.isMuted){
			previewMedia.unmute();
			previewMedia.mute(false);
			//previewMedia.unpause();
		}else{
			previewMedia.mute(true);
			//previewMedia.pause()
		}
	}
}
function webSocketConnect(){
	wsURL = wsURL+sessionStorage.userId;
	ws = new WebSocket(wsURL);
	ws.onmessage = function(data) {
		getUserData(data.data);
	}
}
var receiverName = "";
var receiverId = "";
var senderId = "";
var senderName = "";
var roomName = "";
var patientId = "";
var timer = null;
var autoCallId = "300";
var autoPatientId = "";

//var receiveUType = "";
//var receiveCallType = "";

function getUserData(data){
	console.log(data);
	
	var obj = JSON.parse(data);
	if(obj){
		receiverName = obj.receiverName;
		receiverId = obj.receiverId;
		roomName = obj.room;
		senderId = obj.senderId;
		senderName = obj.senderName;
		callType = obj.callType;
		userType = obj.userType;
		patientId = "";
		if(obj.patientId){
			patientId = obj.patientId;
			autoPatientId = patientId;
		}
		 
		if(obj.status == "Call" || obj.status == "Answered"){
			if(obj.status == "Call"){
				if((patientId == autoCallId || senderId == "110") || (senderId == "115")){
					setTimeout(function(){
						onClickAccept();
					},1000)
					
				}else{
					document.getElementById('audioCall').play();
					  setButtonBlink();
					  $("#divInCall").show();
					  $("#lblName").text( senderName);
					  
					  if(timer){
						  clearTimeout(timer);
					  }
					  timer = setTimeout(onCallDecline,callIdleTime);
				}
			}else{
				if(participantArray.indexOf(receiverName) == -1){
					getAjaxObject(ipAddress+"/video/token/"+receiverName, "GET", onGetSenderAccessCode, onErrorMedication);
				}else{
					if(activeRoom && activeRoom.participants ){
							if(activeRoom.participants.size>0){
								//getAjaxObject(ipAddress+"/video/token/"+receiverName, "GET", onGetSenderAccessCode, onErrorMedication);
							}else if(activeRoom.participants.size == 0){
								activeRoom.disconnect();
								activeRoom = null;
								participantArray = [];
								//removeParticipant(sessionStorage.uName);
								 getAjaxObject(ipAddress+"/video/token/"+receiverName, "GET", onGetReceiverAccessCode, onErrorMedication);
							}
							//activeRoom.disconnect();
							//activeRoom = null;
							 //removeParticipant(sessionStorage.uName);
						 // getAjaxObject(ipAddress+"/video/token/"+receiverName, "GET", onGetReceiverAccessCode, onErrorMedication);
					  }
				}
			}
		}else if(obj.status == "Declined"){
			Loader.hideLoader();
			var strName = "Call got rejected by "+senderName;//+" Rejected";
			customAlert.error("Information !",strName);
		}else if(obj.status == "Unanswered"){
			Loader.hideLoader();
			var strName = "Un answered "+senderName;//+" Rejected";
			customAlert.error("Information !",strName);
		}else if(obj.status == "disconnect"){
			if(activeRoom && activeRoom.participants.size == 1){
				activeRoom.disconnect();
				activeRoom = null;
				participantArray = [];
			}else{
				if(activeRoom){
					activeRoom.participants.forEach(function(participant) {
						if(participant.identity == senderName){
							console.log(participant);
							participant.emit("participantDisconnected");
							participant._signaling.disconnect();
							 participant.media.detach();
							 removeParticipant(senderName);
						}
					});
				}
				
			}
				/**/
			
		}else if(obj.status == "offline"){
			console.log("fail");
			Loader.hideLoader();
			var strName = "Unavailable";//+" Rejected";
			customAlert.error("Information !",strName);
		}
			/*activeRoom.participants.forEach(function(participant) {
				if(participant.identity == strLabel){
					console.log(participant);
					participant.emit("participantDisconnected");
					participant._signaling.disconnect();
					 participant.media.detach();
					 removeParticipant(participant.identity);*/
		}
	}
var clientInvite = null;
function onClickAccept(){
	 if(timer){
		  clearTimeout(timer);
	  }
	 timer = null;
	if(videoCallVer == "1.0"){
		if(clientInvite){
			 $("#navBar").hide();
			 $("#divInCall").hide();
			 $("#navDisconnect").show();
			 document.getElementById('audioCall').pause();
			 if(timer){
				 clearInterval(timer);
			 }
			 timer = null
			clientInvite.accept().then(conversationStarted);
		}
	}else{
		showLocalPreview();
		 $("#navBar").hide();
		 $("#divInCall").hide();
		 document.getElementById('audioCall').pause();
		// room.participants
		 if(participantArray.indexOf(receiverName) == -1){
			 getAjaxObject(ipAddress+"/video/token/"+receiverName, "GET", onGetReceiverAccessCode, onErrorMedication);
		 }
		 var dataObj = {};
		  dataObj.senderId = receiverId;
		  dataObj.senderName = receiverName;
		  dataObj.receiverId = senderId;//getUserIdByName();//"104";
		  dataObj.receiverName = senderName;
		  if(activeRoom && activeRoom.participants &&  activeRoom.participants.size>0){
			  dataObj.room =  activeRoom.name;
		  }else{
			  dataObj.room =  roomName;
		  }
		  dataObj.status =  "Answered";//,data "status": "calling"
		  dataObj.userType = userType;
		  dataObj.callType = callType;
		  console.log(dataObj);
		  sendData(JSON.stringify(dataObj));
		  
		  showPatientInfo();
		  
		  /*function closeUserCallList(evt,returnData){
				console.log(returnData);
				if(returnData && returnData.status == "success"){
					var user = returnData.user;
					$("#txtInviteName").val(user.userName);*/
		  setTimeout(function(){
				  if(autoPatientId == autoCallId){
					  var dataObj = {};
					  dataObj.status = "success";
					  dataObj.user = {};
					  dataObj.user.userName = "doctor2";
					  dataObj.user.callType = callType;
					  closeUserCallList({}, dataObj);
			  }
		  },20000);
		 
					
		  if(participantArray.indexOf(receiverName)>=0 && activeRoom && activeRoom.participants && activeRoom.participants.size == 0){
				activeRoom.disconnect();
				activeRoom = null;
				removeParticipant(sessionStorage.uName);
				 getAjaxObject(ipAddress+"/video/token/"+receiverName, "GET", onGetReceiverAccessCode, onErrorMedication);
			}
		  		  
	}
}
function onCallDecline(){
	 if(timer){
		  clearTimeout(timer);
	  }
	 timer = null;
	$("#navBar").hide();
	 $("#divInCall").hide();
	 $("#navDisconnect").hide();
	 var dataObj = {};
	  dataObj.senderId = receiverId;
	  dataObj.senderName = receiverName;
	  dataObj.receiverId = senderId;//getUserIdByName();//"104";
	  dataObj.receiverName = senderName;
	  dataObj.room = roomName;
	  dataObj.userType = userType;
	  dataObj.callType = callType;
	  dataObj.status = "Unanswered";
	  console.log(dataObj);
	  sendData(JSON.stringify(dataObj));
}
function onClickDecline(){
	 if(timer){
		  clearTimeout(timer);
	  }
	 timer = null;
	 $("#navBar").hide();
	 $("#divInCall").hide();
	 $("#navDisconnect").hide();
	 if(clientInvite){
		 clientInvite.reject();
	 }
	//  document.getElementById('audioCall').stop();
	 document.getElementById('audioCall').pause();
	 if(timer){
		 clearInterval(timer);
	 }
	 timer = null;
	 if(videoCallVer == "2.0"){
		 var dataObj = {};
		  dataObj.senderId = receiverId;
		  dataObj.senderName = receiverName;
		  dataObj.receiverId = senderId;//getUserIdByName();//"104";
		  dataObj.receiverName = senderName;
		  dataObj.room = roomName;
		  dataObj.status = "Declined";
		  console.log(dataObj);
		  sendData(JSON.stringify(dataObj));
	 }
}

function onGetReceiverAccessCode(dataObj){
	Loader.showLoader();
	var token = dataObj.token;
	identity = dataObj.identity;
	videoClient = new Twilio.Video.Client(token);
	 videoClient.connect({ to: roomName,video:false,audio:false}).then(roomJoined,
		        function(error) {
		 				log('Could not connect to Twilio: ' + error.message);
		        });
}
function onGetSenderAccessCode(dataObj){
	Loader.showLoader();
	setTimeout(removeLoader,5000);
	var token = dataObj.token;
	identity = dataObj.identity;
	var client1 = new Twilio.Video.Client(token);
	client1.connect({ to: roomName}).then(roomJoined,
		        function(error) {
		 				log('Could not connect to Twilio: ' + error.message);
		        });
}
function sendData(data) {
	if(ws){
		ws.send(data);
	}
}
var timer = null; 
var count = 1;
function setButtonBlink(){
	timer = setInterval(function(){
		if(count%2 == 0){
			$("#navBar").addClass("buttonBorder");
			$("#btnInviteImage").show();
		}else{
			$("#navBar").removeClass("buttonBorder");
			$("#btnInviteImage").show();
		}
		count++;
	}, 1000);
}
function clientConnected() {
	Loader.hideLoader();
	//$("#btnButton").show();
	//setButtonBlink();
	
	if(parentRef.invFlag){
		console.log("Connected to Twilio. Listening for incoming Invites as '" + conversationsClient.identity + "'");
		  conversationsClient.on('invite', function (invite) {
			  $("#navBar").show();
			  document.getElementById('audioCall').play();
			  setButtonBlink();
			  $("#divInCall").show();
			  $("#lblName").text( invite.from);
			 // $("#audioCall").play();
			  clientInvite = invite;
			  if(parentRef.invFlag){
				  parentRef.from =  invite.from;//sessionStorage.uName;
				  console.log('Incoming invite from: ' + invite.from);
				  //onClickPatientCall(null);
				  /*customAlert.confirm("Confirm", invite.from+" calling. Do you want accept?",function(response){
					  console.log(response);
					  var inviteFrom = invite.from;
					  if(response.button == "Yes"){
						  invite.accept().then(conversationStarted);
					  }
				  });*/
			  }
		  });
	}
	
	  /*conversation.on('participantConnected', function (participant) {
		  console.log("Participant '" + participant.identity + "' connected");
	  });*/
}
function onclikPatientCallToNurse(){
	
}
var aConversation = null;
function conversationStarted(conversation) {
	 console.log('In an active Conversation');
	 activeConversation = conversation;
	 //aConversation = conversation;
	 // activeConversation = conversation;
	//  parentRef.activeConversion = conversation;
	  //parentRef.conversationsClient = conversationsClient;
	  //parentRef.screenType = "Patient";
	  //onClickPatientCall(null);
	  conversation.on('participantConnected', function (participant) {
		  console.log("Participant '" + participant.identity + "' connected");
		  //parentRef.participant = participant;
		  //onClickPatientCall(null);
		  $("#navDisconnect").show();
		  participant.media.attach('#remote-media');
	  });
	  // when a participant disconnects, note in log
	  conversation.on('participantFailed', function (participant) {
		  console.log("Participant failed to connect: '" + participant.identity );
	  });
	  conversation.on('participantDisconnected', function (participant) {
		 // $("#navDisconnect").hide();
		  console.log("Participant '" + participant.identity + "' disconnected");
		  console.log(conversationsClient);
		 // if(sessionStorage.uName != "text3"){
			// activeConversation = null;  
		  //}
	  });
	  conversation.on('ended', function (conversation) {
		  $("#navDisconnect").hide();
		  console.log("Connected to Twilio. Listening for incoming Invites as '" + conversationsClient.identity + "'");
	    conversation.localMedia.stop();
	    conversation.disconnect();
	    activeConversation = null;
	  });
	  conversation.on('disconnected', function(conversation) {
		  /* Release the local camera and microphone if we're no longer using this 
		  LocalMedia instance in another Conversation */
		  //conversation.localMedia.stop();
		});
	//  onClickPatientCall(null);
	  // draw local video, if not already previewing
	 /* if (!previewMedia) {
	    conversation.localMedia.attach('#local-media');
	  }
	  // when a participant joins, draw their video on screen
	  conversation.on('participantConnected', function (participant) {
		  console.log("Participant '" + participant.identity + "' connected");
	    participant.media.attach('#remote-media');
	  });
	  // when a participant disconnects, note in log
	  conversation.on('participantDisconnected', function (participant) {
		  console.log("Participant '" + participant.identity + "' disconnected");
	  });
	  // when the conversation ends, stop capturing local video
	  conversation.on('ended', function (conversation) {
		  console.log("Connected to Twilio. Listening for incoming Invites as '" + conversationsClient.identity + "'");
	    conversation.localMedia.stop();
	    conversation.disconnect();
	    activeConversation = null;
	  });*/
	}
function onLogoutDisconnect(){
	if (activeRoom) {
		removeParticipant(sessionStorage.uName);
		activeRoom.disconnect();
		activeRoom = null;
  }
}
	function onClickDisconnect(){
		$("#btnDisconnect").addClass("btnSelection");
		//$("#btnDisconnect").removeClass("btnSelection");
		setTimeout(function(){
			$("#btnDisconnect").removeClass("btnSelection");
		},1000)
		if(videoCallVer == "1.0"){
			if(activeConversation){
				//activeConversation.localMedia.stop();
				activeConversation.disconnect();
				$("#navDisconnect").hide();
				activeConversation = null;
				//aConversation = null;
				setTimeout(function(){
					//initTwilio();
				},1000)
			}
		}else{
			if (activeRoom) {
				userCalling = false;
				removeParticipant(sessionStorage.uName);
				activeRoom.disconnect();
				activeRoom = null;
				
				if(previewMedia){
					 previewMedia.removeCamera();
					 previewMedia.stop();
					 previewMedia.detach('#local-media');
					 $("#lblLogin").text("");
					 previewMedia = null;
				}
				
				
				//showLocalPreview();
		  }
		}
	}
	
	var buttonClickDelayTime = 10000;
	function triggerEvent(){
		var evt = new MouseEvent("click", {view: window,bubbles: true,cancelable: true,clientX: 20});
	        var ele = document.getElementById("btn");
		setInterval(function(){ 
			ele.dispatchEvent(evt);
			$("#btn").trigger('click');
		},buttonClickDelayTime);
	}
	
	function onClickCall(){
		$("#btnCall").addClass("btnSelection");
		//$("#btnCall").removeClass("btnSelection");
		setTimeout(function(){
			$("#btnCall").removeClass("btnSelection");
		},1000)
		var popW = "60%";
	    var popH = 500;

	    var profileLbl;
	    var devModelWindowWrapper = new kendoWindowWrapper();
	    profileLbl = "User List";
	    devModelWindowWrapper.openPageWindow("../../html/patients/userCallList.html", profileLbl, popW, popH, false, closeUserCallList);
	    
		/*var obj = new Object();
		$(parent).trigger("loadImapImageEvent",obj);	*/
		//videoPatientId = "101";
	// myIframe.contentWindow.postMessage('hello', '*');
		//frame.PtNo = "101";
	   // frame.yourMethod('hello');
		
		/**/
		
	}
	
	var AURIO_TYPE = "Audio";
	var VIDEO_TYPE = "Video";
	
	var callType = "";
	var userType = "";
	
	function getUserTypeById(){
		if(sessionStorage.userTypeId == "200"){
			return "Nurse";
		}else if(sessionStorage.userTypeId == "100"){
			return "Doctor";
		}else if(sessionStorage.userTypeId == "500"){
			return "Patient";
		}else if(sessionStorage.userTypeId == "600"){
			return "Relative";
		}
	}
	
	function closeUserCallList(evt,returnData){
		console.log(returnData);
		if(returnData && returnData.status == "success"){
			var user = returnData.user;
			callType = returnData.callType;
			userType = getUserTypeById();
			$("#txtInviteName").val(user.userName);
		if($.trim($("#txtInviteName").val()) != "" && sessionStorage.uName != $.trim($("#txtInviteName").val())){
			setTimeout(removeLoader,delayTime);
			showLocalPreview();
			Loader.showLoader();
			var inviteTo = $("#txtInviteName").val();
			inviteTo = $.trim(inviteTo);
			var userId = getUserIdByName(inviteTo);
			if(userId == ""){
				Loader.hideLoader();
				customAlert.error("Information !","Unavailable");
				return;
			}
			console.log(activeRoom);
			if(!activeRoom){
				if(!userCalling){
					userCalling = true;
					//initTwilio();
					  var dataObj = {};
					  dataObj.senderId = sessionStorage.userId;
					  dataObj.senderName = sessionStorage.uName;
					  dataObj.receiverId = userId;//getUserIdByName();//"104";
					  dataObj.receiverName = inviteTo;
					  dataObj.room =  sessionStorage.userId;//userId;//"room1";
					  dataObj.status =  "Call";//,data "status": "calling"
					  dataObj.callType = callType;
					  dataObj.userType = userType;
					  console.log(dataObj);
					  sendData(JSON.stringify(dataObj));
				}else{
					//initTwilio();
					var inviteTo = $("#txtInviteName").val();
				//	var userId = getUserIdByName(inviteTo);
					  var dataObj = {};
					  dataObj.senderId = sessionStorage.userId;
					  dataObj.senderName = sessionStorage.uName;
					  dataObj.receiverId = userId;//getUserIdByName();//"104";
					  dataObj.receiverName = inviteTo;
					  dataObj.room =  sessionStorage.userId;//userId;//"room1";
					  dataObj.status =  "Call";//,data "status": "calling"
					  dataObj.callType = callType;
					  dataObj.userType = userType;
					  console.log(dataObj);
					  sendData(JSON.stringify(dataObj));
				}
			}else{
				//var inviteTo = $("#txtInviteName").val();
				var flag = true;
				if(activeRoom){
					activeRoom.participants.forEach(function(participant) {
						if(participant.identity == inviteTo){
							Loader.hideLoader();
							var strName = (inviteTo+" Already in the room");//+" Rejected";
							customAlert.error("Information !",strName);
							flag = false;
							//return;
						}
					});
				}
				if(flag){
					//var userId = getUserIdByName(inviteTo);
					  var dataObj = {};
					  dataObj.senderId = sessionStorage.userId;
					  dataObj.senderName = sessionStorage.uName;
					  dataObj.receiverId = userId;//getUserIdByName();//"104";
					  dataObj.receiverName = inviteTo;
					  if(activeRoom.name){
						  dataObj.room =  activeRoom.name;//userId;//"room1";
					  }else{
						  dataObj.room =  sessionStorage.userId;
					  }
					  //userId;//"room1";
					  dataObj.status =  "Call";//,data "status": "calling"
					  dataObj.callType = callType;
					  dataObj.userType = userType;
					  console.log(dataObj);
					  sendData(JSON.stringify(dataObj));
				}
			}
		}
		}
	}
	function getUserIdByName(inviteTo){
		for(var i=0;i<userList.length;i++){
			var userObj = userList[i];
			if(userObj && userObj.username == inviteTo){
				return userObj.id;
			}
		}
		return "";
	}
	function removeParticipant(nm){
		for(var i = participantArray.length - 1; i >= 0; i--) {
		    if(participantArray[i] === nm) {
		    	participantArray.splice(i, 1);
		    }
		}
	}
	function isDuplicateParticipant(nm){
		for(var i = participantArray.length - 1; i >= 0; i--) {
		    if(participantArray[i] === nm) {
		    	return true;
		    }
		}
		return false;
	}
	
	function onClickPtDisconnect(evt){
		participantDisconnected(evt);
	}
	
	function participantDisconnected(evt){
		console.log(activeRoom);
		if(activeRoom){
			var strLabel = $(evt.currentTarget.parentElement.parentElement.parentElement).find("label").text();
			activeRoom.participants.forEach(function(participant) {
				if(participant.identity == strLabel){
					console.log(participant);
					
					 
					// activeRoom && activeRoom.participants && activeRoom.participants.size == 0
					 if(activeRoom  && activeRoom.participants.size == 1){
						 activeRoom.disconnect();
						 activeRoom = null;
						 participantArray = [];
					 }else{
						 participant.emit("disconnected");
							participant._signaling.disconnect();
							 participant.media.detach();
							 removeParticipant(participant.identity);
							 
							 var dataObj = {};
							  dataObj.senderId = sessionStorage.userId;
							  dataObj.senderName = sessionStorage.uName;
							  var recId = getUserIdByName(strLabel);
							  dataObj.receiverId = recId;
							  dataObj.receiverName = strLabel;
							  if(activeRoom.name){
								  dataObj.room =  activeRoom.name;
							  }else{
								  dataObj.room =  sessionStorage.userId;
							  }
							  //userId;//"room1";
							  dataObj.status =  "disconnect";//,data "status": "calling"
							  dataObj.userType = userType;
							  dataObj.callType = callType;
							  console.log(dataObj);
							  sendData(JSON.stringify(dataObj));
					 }
					 
				}
			 });
		}
	}
	function onClickNurDisconnect(evt){
		participantDisconnected(evt);
	}
	
	function onClickPtMute(evt){
		participantMuted(evt);
	}
	function onClickNurMute(evt){
		participantMuted(evt);
	}
	function participantMuted(evt){
		if(activeRoom){
			var strLabel = $(evt.currentTarget.parentElement.parentElement.parentElement).find("label").text();
			activeRoom.participants.forEach(function(participant) {
				if(participant.identity == strLabel){
					//console.log(participant);
					/*var divAudio = $(evt.currentTarget.parentElement.parentElement.parentElement).find("audio")[0];
					if(divAudio){
						$(evt.currentTarget.parentElement.parentElement.parentElement).find("audio").remove();
					}else{
						$(evt.currentTarget.parentElement.parentElement.parentElement).append("<audio></audio>");
					}*/
					
					participant.media.audioTracks.forEach(function(track) {
						console.log(track);
						/*if(track._signaling._state == "enabled"){
							track._signaling.disable();
						}else{
							track._signaling.enable();
						}*/
						console.log(track._getAllAttachedElements()[0].src);
						//var detachedAudioEl = track.detach();//'div#audio-track');
					});
					/*var divAudio = $(evt.currentTarget.parentElement.parentElement.parentElement).find("audio");
					if(divAudio){
						divAudio = divAudio[0];
						if(divAudio){
							if($(evt.currentTarget.parentElement.parentElement.parentElement).find("audio")[0].paused){
								$(evt.currentTarget.parentElement.parentElement.parentElement).find("audio")[0].play();
							}else{
								$(evt.currentTarget.parentElement.parentElement.parentElement).find("audio")[0].pause();
							}
						}
					}*/
					/*if(participant.media.isMuted){
						participant.media.isMuted = false;
					}else{
						participant.media.isMuted = true;
					}*/
				}
			 });
		}
	}
	function removeLoader(){
		Loader.hideLoader();
	}
	function hideNavigationBar(){
		$("#btnCall").hide();
		$("#btnDisconnect").hide();
		$("#txtInviteName").hide();
		$("#rightPanel").hide();
	}
	function showPatientInfo(){
		var myIframe = parent.frames["ptframe"];
		if(myIframe && myIframe.contentWindow && patientId != ""){
			myIframe.contentWindow.targetFunction(patientId);
		}
	}
	
	function setAudioType(){
		if(callType == AURIO_TYPE){
			$("#local-media").find("video").hide();
			$("#patient-media").find("video").hide();
			$("#remote-media").find("video").hide();
			//$("#local-media").find("video").remove();
			//$("#remote-media").find("video").remove();
			//$("#local-media").find("video").remove();
		}else{
			$("#local-media").find("video").show();
			$("#patient-media").find("video").show();
			$("#remote-media").find("video").show();
		}
	}
	function roomJoined(room) {
		setInterval(function(){setAudioType()});
		divFilled = false;
		  activeRoom = room;
		  console.log(room);
		  if(!isDuplicateParticipant(identity)){
			  participantArray.push(identity);
		  }
		
		  log("Joined as '" + identity + "'");
//		  document.getElementById('button-join').style.display = 'inline';
		//  document.getElementById('button-leave').style.display = 'inline';

		  // Draw local video, if not already previewing
		  if (!previewMedia) {
			  	room.localParticipant.media.attach('#local-media');
		  }
		  
		  setTimeout(removeLoader,5000);
		  room.participants.forEach(function(participant) {
		    log("Already in Room: '" + participant.identity + "'");
		    var ptArr = $('#patient-media').find("video");
		  //  if (ptArr.length == 0 && divFilled == false){
		    	Loader.hideLoader();
		    	
		    	divFilled = true;
		    	
		    	if(participant.identity == "john" || participant.identity == "test1" || participant.identity == "mary"){
		    		$("#lblPt").text(participant.identity);
		    		$("#divPt").show();
			    	 $("#btnPtDisconnect").off("click");
			 	    $("#btnPtDisconnect").on("click",onClickPtDisconnect);
			 	    
			 	    $("#btnPtMute").off("click");
			 	    $("#btnPtMute").on("click",onClickPtMute);
		    		participant.media.attach('#patient-media');
			    		setTimeout(function(){
			    			var ptHeight = 'height:'+(imgHeight+30)+'px !important';//
			    			console.log($("#divPtVideo").height());
			    			$("#patient-media").find("video").height($("#divPtVideo").height());
			    			if(callType == AURIO_TYPE){
			    				$("#patient-media").find("video").hide();
			    			}else{
			    				$("#patient-media").find("video").show();
			    			}
			    		},2000);
			    	}else{
			    		Loader.hideLoader();
			    		var ptArr = $('#remote-media').find("video");
			    		if(ptArr.length == 0){
			    			$("#lblNur").text(participant.identity);
					    	participant.media.attach('#remote-media');
					    	setTimeout(function(){
		    		    		$("#remote-media").find("video").height($("#divNurVideo").height());
		    		    		if(callType == AURIO_TYPE){
		    		    			$("#remote-media").find("video").hide();
		    		    		}else{
		    		    			$("#remote-media").find("video").show();
		    		    		}
		    		    	},2000);
					    	$("#divNur").show();
					    	$("#btnNurDisconnect").off("click");
						 	$("#btnNurDisconnect").on("click",onClickNurDisconnect);
						 	
						 	$("#btnNurMute").off("click");
					 	    $("#btnNurMute").on("click",onClickNurMute);
			    		}
			    	}
		  });

		 
		  room.on('participantConnected', function (participant) {
		    log("Joining: '" + participant.identity + "'");
		    			Loader.hideLoader();
				    	 if(participant.identity != identity){
				    		 if(!isDuplicateParticipant(identity)){
				    			 participantArray.push(identity); 
				    		 }
				    		 	
				    		 	var ptArr = $('#patient-media').find("video");
				    		 //   if (ptArr.length == 0){
				    		    	Loader.hideLoader();
				    		    	
				    		    	if(participant.identity == "john" || participant.identity == "test1" || participant.identity == "mary"){
				    		    		$("#lblPt").text(participant.identity);
					    		    	participant.media.attach('#patient-media');
				    		    		setTimeout(function(){
				    		    			var ptHeight = 'height:'+(imgHeight+30)+'px !important';//
				    		    			console.log($("#divPtVideo").height());
				    		    			$("#patient-media").find("video").height($("#divPtVideo").height());
				    		    			if(callType == AURIO_TYPE){
				    		    				$("#patient-media").find("video").hide();
				    		    			}else{
				    		    				$("#patient-media").find("video").show();
				    		    			}
				    		    			
				    		    			//$("#patient-media video").css('cssText', ptHeight);
				    		    			//$("#patient-media video").css('cssText', 'height:200px !important;');
				    		    		},2000)
				    		    		$("#divPt").show();
					    		    	 $("#btnPtDisconnect").off("click");
					    			 	    $("#btnPtDisconnect").on("click",onClickPtDisconnect);
					    			 	    
					    			 	   $("#btnPtMute").off("click");
					    			 	    $("#btnPtMute").on("click",onClickPtMute);
				    		    		//$("#patient-media video").addClass("remoteVideoStyle");
				    		    	}else{
				    		    		Loader.hideLoader();
				    		    		var ptArr = $('#remote-media').find("video");
				    		    	if(ptArr.length == 0){
				    		    			$("#lblNur").text(participant.identity);
						    		    	participant.media.attach('#remote-media');
						    		    	
						    		    	setTimeout(function(){
						    		    		$("#remote-media").find("video").height($("#divNurVideo").height());
						    		    		if(callType == AURIO_TYPE){
						    		    			$("#remote-media").find("video").hide();
						    		    		}else{
						    		    			$("#remote-media").find("video").show();
						    		    		}
						    		    	},2000);
						    		    	
						    		    	$("#divNur").show();
						    		    	$("#btnNurDisconnect").off("click");
									 	    $("#btnNurDisconnect").on("click",onClickNurDisconnect);
									 	    
									 	   $("#btnNurMute").off("click");
									 	    $("#btnNurMute").on("click",onClickNurMute);
				    		    		}
				    		    	}
				    		    	
				    		    //}else{
				    		    
				    		   // }
				  	  	    } else{
				  	  	    	//participant.media.detach();
				  	  	    }
				  //  }
		  
		    });

		  // When a participant disconnects, note in log
		  room.on('participantDisconnected', function (participant) {
		    log("Participant '" + participant.identity + "' left the room");
		    participant.media.detach();
		    removeParticipant(participant.identity);
		    var str1 = $("#lblPt").text();
			    if(str1 == participant.identity){
			    	$("#lblPt").text("");
			    	$("#divPt").hide();
			    	// $("#btnPtDisconnect").off("click");
			    	// $("#btnPtMute").off("click");
			    }else{
			    	$("#lblNur").text("");
			    	$("#divNur").hide();
			    	//$("#btnNurDisconnect").off("click");
			    	//$("#btnNurMute").off("click");
			    }
			   
			    activeRoom.participants.forEach(function(participant) {
			    	//activeRoom.localParticipant.media.detach();
			    	console.log(participant.identity);
			    });
			    if(activeRoom){
			    	 if(activeRoom.name != sessionStorage.userId){
			    		 	if(activeRoom.participants.size == 0){
			    		 		  if(activeRoom){
			    				    	activeRoom.disconnect();
			    				    }
			    		 		  participantArray = [];
			    		 		 activeRoom = null;
			    		 		previewMedia.removeCamera();
			    		 		previewMedia.stop();
			    		 		 previewMedia.detach('#local-media');
			    				 $("#lblLogin").text("");
			    				 previewMedia = null;
			    		 	}
					    }
			    }
			   if(participant.identity == "mary"){
			    	 if(activeRoom){
					    	activeRoom.disconnect();
					    	participantArray = [];
					    }
			    	 if(previewMedia){
			    		 previewMedia.removeCamera();
		    		 		previewMedia.stop();
		    		 		 previewMedia.detach('#local-media');
		    				 $("#lblLogin").text("");
		    				 previewMedia = null;
			    	 }
			    	
			    }
			   
		  });

		  // When we are disconnected, stop capturing local video
		  // Also remove media for all remote participants
		  room.on('disconnected', function () {
		    log('Left');
		    room.localParticipant.media.detach();
		    room.participants.forEach(function(participant) {
		    	 log("Participant list '" + participant.identity + "' left the room");
		      participant.media.detach();
		      var str1 = $("#lblPt").text();
			    if(str1 == participant.identity){
			    	$("#lblPt").text("");
			    	$("#divPt").hide();
			    	// $("#btnPtDisconnect").off("click");
			    }else{
			    	$("#lblNur").text("");
			    	$("#divNur").hide();
			    	//$("#btnNurDisconnect").off("click");
			    }
			    removeParticipant(participant.identity);
			    	//var lblStr =  $("#remote-media").find("label").text();
			    	//console.log(lblStr);
			   // }catch(ex){}
		    //  $("#remote-media").text("");
		    });
		   /* if(previewMedia){
	    		 previewMedia.removeCamera();
   		 		previewMedia.stop();
   		 		 previewMedia.detach('#local-media');
   				 $("#lblLogin").text("");
   				 previewMedia = null;
	    	 }*/
		    if(activeRoom){
		    	activeRoom.disconnect();
		    	participantArray = [];
		    	
		    }
		   activeRoom = null;
		   // removeParticipant(sessionStorage.uName);
		  //  document.getElementById('button-join').style.display = 'inline';
		    //document.getElementById('button-leave').style.display = 'inline';
		  });
		}
	function log(msg){
		console.log(msg);
	}
	function onErrorMedication(errobj){
		console.log(errobj);
	}
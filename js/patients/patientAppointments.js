var angularUIgridWrapper = AngularUIGridWrapper();
//var ipAddress = "http://stage.timeam.com";
var  stArray = [{key:'AM',text:'AM'},{key:'PM',text:'PM'}];

var searchData = [{key:'1',text:'Patient Id'},{key:'2',text:'Date of Birth'},{key:'3',text:'First Name'},{key:'4',text:'Last Name'},{key:'5',text:'SSN'},{key:'6',text:'First Name, Last Name'}];
$(document).ready(function(){
});
var parentRef = null;
$(window).load(function(){
	parentRef = parent.frames['iframe'].window;
	//$("#cmbStatus").kendoComboBox();
	//$("#txtPatientId").kendoComboBox();
	
	//$("#cmbStatus").data("kendoComboBox").input.attr("placeholder", "Status");
	//$("#txtPatientId").data("kendoComboBox").input.attr("placeholder", "Search Parameters");
	
	var hours = [];
	for(var i=1;i<=12;i++){
		var obj = {};
		obj.text = i;
		obj.key = i;
		hours.push(obj);
	}
	
	var minutes = [];
	for(var i=0;i<=59;i++){
		var obj = {};
		obj.text = i;
		obj.key = i;
		minutes.push(obj);
	}
	 $("#dtDate").kendoDatePicker();
	    $("#cmbHour").kendoComboBox();
	    $("#cmbMin").kendoComboBox();
	    $("#cmbAM").kendoComboBox();
	    
	setDataForSelection(hours, "cmbHour", function(){}, ["text", "key"], 0, "");
	setDataForSelection(minutes, "cmbMin", function(){}, ["text", "key"], 0, "");
	setDataForSelection(stArray, "cmbAM", function(){}, ["text", "key"], 0, "");
	
	onMessagesLoaded();
});

function onStatusChange(){
	var cmbZip = $("#cmbZip").data("kendoComboBox");
	if(cmbZip && cmbZip.selectedIndex<0){
		cmbZip.select(0);
	}
}
function onPTChange(){
	var txtPatientId = $("#txtPatientId").data("kendoComboBox");
	if(txtPatientId && txtPatientId.selectedIndex<0){
		txtPatientId.select(0);
	}
}
	function onMessagesLoaded() {
		var dataOptions = {
	        pagination: false,
	        paginationPageSize: 500,
			changeCallBack: onChange
	    }

		angularUIgridWrapper = new AngularUIGridWrapper("dgridPatientAppointment", dataOptions);
	    angularUIgridWrapper.init();
	    var dataArray = [];
	    var dataObj = {};
	    buildDeviceTypeGrid(dataArray);
	   
	buttonEvents();
	getPatientList();
}
	var patientId = ""
	function getPatientList(){
		buildDeviceTypeGrid([]);
		patientId = parentRef.patientId;
		var patientListURL = ipAddress+"/patient/appointments/"+patientId;
	/*	if(sessionStorage.userTypeId == "200" || sessionStorage.userTypeId == "100"){
			patientListURL = ipAddress+"/patient/list";
		}else{
			
		}*/
	//	var patientListURL = ipAddress+"/patient/list/"+sessionStorage.userId;
		 getAjaxObject(patientListURL,"GET",onPatientListData,onErrorMedication);
	}
	function onClickSearch(){
		var strSearch = $("#txtSearch").val();
		strSearch = $.trim(strSearch);
		if(strSearch != ""){
			var txtPatientId = $("#txtPatientId").data("kendoComboBox");
			if(txtPatientId){
				var idx = txtPatientId.selectedIndex;
				if(idx == 0){
				}
			}
		}else{
			getPatientList();
		}
	}
	function onPatientListData(dataObj){
		console.log(dataObj);
		var tempArray = [];
		var dataArray = [];
		if(dataObj){
			if($.isArray(dataObj)){
				tempArray = dataObj;
			}else{
				tempArray.push(dataObj);
			}
		}
		
		for(var i=0;i<tempArray.length;i++){
			var dataItem = tempArray[i];
			if(dataItem){
				var dataItemObj = {};
				dataItem.DT = kendo.toString(new Date(dataItem.dateOfAppointment), "MM/dd/yyyy");
				dataItem.TM = dataItem.timeOfAppointment;
				dataItem.DOC = dataItem.doctor;
				dataItem.REA = dataItem.reason;
				dataItem.FEA = dataItem.facility;
				dataItem.ST = dataItem.status;
				dataItem.IDK = dataItem.id;
				dataItem.PID = dataItem.patientId;
				dataArray.push(dataItem);
			}
		}
		buildDeviceTypeGrid(dataArray);
		 $("#btnSave").prop("disabled", true);
		 $("#btnSend").prop("disabled", true);
	}
	function onErrorMedication(errobj){
		console.log(errobj);
	}
function buttonEvents(){
	$("#btnSave").off("click");
	$("#btnSave").on("click",onClickOK);
	
	$("#btnSend").off("click");
	$("#btnSend").on("click",onClickSend);
	
	$("#btnCancelDet").off("click");
	$("#btnCancelDet").on("click",onClickCancel);
	
	$("#search_btn").off("click");
	$("#search_btn").on("click",onClickSearch)
}
$(window).resize(adjustHeight);
function adjustHeight(){
	var defHeight = 200;
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 120;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    try{angularUIgridWrapper.adjustGridHeight(cmpHeight);}catch(e){};

}
function buildDeviceTypeGrid(dataSource) {
    var gridColumns = [];
	var otoptions = {};
	otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Date",
        "field": "DT",
        "enableColumnMenu": false,
        "width":150
    });
    gridColumns.push({
        "title": "Time",
        "field": "TM",
        "enableColumnMenu": false,
        "width":100
    });
	gridColumns.push({
        "title": "Doctor",
        "field": "DOC",
        "enableColumnMenu": false,
    });
	gridColumns.push({
        "title": "Reason",
        "field": "REA",
        "enableColumnMenu": false,
    });
	gridColumns.push({
        "title": "Facility",
        "field": "FEA",
        "enableColumnMenu": false,
    });
	gridColumns.push({
        "title": "Status",
        "field": "ST",
        "enableColumnMenu": false,
        "width":150
    });
	angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
	adjustHeight();
}
var selPtId = "";
function onChange(){
	setTimeout(function(){
		 var selectedItems = angularUIgridWrapper.getSelectedRows();
		 console.log(selectedItems);
		 if(selectedItems && selectedItems.length>0){
			 selPtId = selectedItems[0].PID;
			 $("#btnSave").prop("disabled", false);
			 $("#btnSend").prop("disabled", false);
			 
			 var dt = new Date(selectedItems[0].DT);
			 console.log(dt);
			 if(dt){
				 var strDT = kendo.toString(dt,"MM/dd/yyyy");
				 var dtDOB = $("#dtDate").data("kendoDatePicker");
					if(dtDOB){
						dtDOB.value(strDT);
					}
			 }
			 var tm = selectedItems[0].TM;
			 if(tm){
				 var tmArray = tm.split(" ");
				 var tm2 = tmArray[1];
				 var cmbAM = $("#cmbAM").data("kendoComboBox")
				 if(tm2 == "AM"){
					cmbAM.select(0);
				 }else{
					 cmbAM.select(1);
				 }
				 var tm1 = tmArray[0];
				 var tm1Array = tm1.split(":");
				 getComboListIndex("cmbHour", "text", Number(tm1Array[0]));
				 getComboListIndex("cmbMin", "text", Number(tm1Array[1]));
				 
				 $("#txtStatus").val(selectedItems[0].ST);
				 $("#txtFacility").val(selectedItems[0].FEA);
				 $("#txtDoctor").val(selectedItems[0].DOC);
				 $("#txtReason").val(selectedItems[0].REA);
			 }
		 }else{
			 $("#btnSend").prop("disabled", true);
			 $("#btnSave").prop("disabled", true);
		 }
	});
}
function getComboListIndex(cmbId,attr,attrVal){
	var cmb = $("#"+cmbId).data("kendoComboBox");
	if(cmb){
		var ds = cmb.dataSource;
		var totalRec = ds.total();
		for(var i=0;i<totalRec;i++){
			var dtItem = ds.at(i);
			if(dtItem && dtItem[attr] == attrVal){
				cmb.select(i);
				return i;
			}
		}
	}
	return -1;
}
var strLbl = "";
function onClickSend(){
	strLbl = "Alert";
	updateItem();
}

function sendAlert(){
	var selectedItems = angularUIgridWrapper.getSelectedRows();
	 console.log(selectedItems);
	 if(selPtId !=""){
		 var obj = {};
		 //obj = selectedItems[0];
		 
		 var patientListURL = ipAddress+"/patient/appointment/alert/"+selPtId;
		 getAjaxObject(patientListURL,"GET",onPatientSendAlert,onErrorMedication);
	 }
}
function onPatientSendAlert(dataObj){
	if(dataObj && dataObj.response && dataObj.response.status){
		if(dataObj.response.status.code == "1"){
			customAlert.error("Success",dataObj.response.status.message);
		}else{
			customAlert.error("Error",dataObj.response.status.message);
		}
	}
}
function onClickOK(){
	strLbl = "Update";
	updateItem();
}

function updateItem(){
	setTimeout(function(){
		 var selectedItems = angularUIgridWrapper.getSelectedRows();
		 console.log(selectedItems);
		 if(selectedItems && selectedItems.length>0){
			 var obj = {};
			 obj = selectedItems[0];
			 
			 var dtDOB = $("#dtDate").data("kendoDatePicker");
				if(dtDOB){
					var dt = new Date(dtDOB.value());
					obj.dateOfAppointment = dt.getTime();
					obj.id = obj.IDK;
					
					var tm = "";
					var cmbHour = $("#cmbHour").data("kendoComboBox");
					var cmbMin = $("#cmbMin").data("kendoComboBox");
					var cmbAM = $("#cmbAM").data("kendoComboBox");
					
					tm = cmbHour.value()+":"+cmbMin.value()+" "+cmbAM.value();
					obj.timeOfAppointment = tm;
					
					var dataUrl = ipAddress+"/patient/appointment/update";
					
					console.log(obj);
					createAjaxObject(dataUrl,obj,"POST",onCreate,onErrorMedication);
				}
		 }
	})
}
function onCreate(dataObj){
	if(dataObj.code == "1"){
		getPatientList();
		if(strLbl == "Alert"){
			setTimeout(function(){
				sendAlert();	
			})
		}else{
			customAlert.info("info", dataObj.message);
		}
		
	}
}
function onClickDelete(){
	customAlert.confirm("Confirm", "Are you sure you want delete?",function(response){
		if(response.button == "Yes"){
			var selectedItems = angularUIgridWrapper.getSelectedRows();
			 console.log(selectedItems);
			 if(selectedItems && selectedItems.length>0){
				 var selItem = selectedItems[0];
				 if(selItem){
					 var dataUrl = ipAddress+"/patient/delete/";
					 var reqObj = {};
					 reqObj.id = selItem.PID;
					 reqObj.isDeleted = "1";
					 reqObj.modifiedBy = "101";
					 createAjaxObject(dataUrl,reqObj,"POST",onDeletePatient,onErrorMedication);
				 }
			 }
		}
	});
}
function onDeletePatient(dataObj){
	if(dataObj && dataObj.response && dataObj.response.status){
		if(dataObj.response.status.code == "1"){
			customAlert.info("info", "Patient Deleted Successfully");
			getPatientList();
		}else{
			customAlert.error("error", dataObj.message);
		}
	}
}
function getAjaxObject(dataUrl,method,successFunction,errorFunction){
	Loader.showLoader();
	$.ajax({
		  type: method,
		  url: dataUrl,
		  data: null,
		  context: this,
			cache: false,
		  success: function( data, statusCode, jqXHR ){
			  Loader.hideLoader();
			  successFunction(data);
		  },
		  error: function( jqXHR, textStatus, errorThrown ){
			  Loader.hideLoader();
			  errorFunction(jqXHR);
		  },
		  contentType: "application/json",
		});

}
function onClickCancel(){
	popupClose(false);
}
function popupClose(st){
	var onCloseData = new Object();
	onCloseData.status = st;
	var windowWrapper = new kendoWindowWrapper();
	windowWrapper.closePageWindow(onCloseData);
}


var angularUIgridWrapper = null; //AngularUIGridWrapper();
//var ipAddress = "http://stage.timeam.com";
var parentRef = null;
$(document).ready(function() {});

$(window).load(function() {
    parentRef = parent.frames['iframe'].window;
    onMessagesLoaded();
});

function onMessagesLoaded() {
    var dataArray = [];
    var dataObj = {};

    buttonEvents();
    init();
}
var startDT = null;
var endDT = null;

function init() {
    $("#cmbUsers").kendoComboBox();
    $("#cmbProviders").kendoComboBox();

    var fmt = getCountryDateFormat();
    startDT = $("#txtStartDate").kendoDatePicker({
        change: startChange,
        format: fmt
    }).data("kendoDatePicker");

    endDT = $("#txtEndDate").kendoDatePicker({
        change: endChange,
        format: fmt
    }).data("kendoDatePicker");

    var dt = new Date();
    var stDate = new Date();
    stDate.setDate(stDate.getDate());
    startDT.value(stDate);

    var endDate = new Date();
    endDate.setDate(endDate.getDate());
    endDT.value(endDate);
}
function printData() {
	var prtContent = document.getElementById("divTableGrid");
	var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
	WinPrint.document.write(prtContent.innerHTML);
	WinPrint.document.close();
	WinPrint.focus();
	WinPrint.print();
	WinPrint.close();
}
function startChange() {
    var startDate = startDT.value(),
        endDate = endDT.value();

    if (startDate) {
        startDate = new Date(startDate);
        startDate.setDate(startDate.getDate());
        endDT.min(startDate);
    } else if (endDate) {
        startDT.max(new Date(endDate));
    } else {
        endDate = new Date();
        startDT.max(endDate);
        endDT.min(endDate);
    }
}

function endChange() {
    var endDate = endDT.value(),
        startDate = startDT.value();

    if (endDate) {
        endDate = new Date(endDate);
        endDate.setDate(endDate.getDate());
        startDT.max(endDate);
    } else if (startDate) {
        endDT.min(new Date(startDate));
    } else {
        endDate = new Date();
        startDT.max(endDate);
        endDT.min(endDate);
    }
}

function getPatientList() {
    var txtStartDate = $("#txtStartDate").data("kendoDatePicker");
    var txtEndDate = $("#txtEndDate").data("kendoDatePicker");

    if (txtStartDate.value() && txtEndDate.value()) {
        var endDate = new Date(txtEndDate.value());
        var stDate = new Date(txtStartDate.value());
        stDate.setHours(0, 0, 0);
        endDate.setHours(23, 59, 59);
        var edTime = endDate.getTime();

        if (parentRef.screenType == "providers") {
            var patientListURL = ipAddress + "/homecare/gps/?user-id=" + parentRef.providerid + "&time-of-record=:bt:" + stDate.getTime() + "," + edTime;
        } else if (parentRef.screenType == "patients") {
            var patientListURL = ipAddress + "/homecare/gps/?user-id=" + sessionStorage.userId + "&time-of-record=:bt:" + stDate.getTime() + "," + edTime;
        } else if (sessionStorage.uName == "admin") {
            var cmbProviders = $("#cmbProviders").data("kendoComboBox");
            var cmbUsers = $("#cmbUsers").data("kendoComboBox");
            //console.log($("input:radio[name='ignitionOn']:checked").val());
            var uType = $("input:radio[name='Users']:checked").val();
            if (uType && cmbUsers.text() && cmbProviders.text()) {
                if (uType == "providers") {
                    var uIdItem = cmbProviders.dataItem();
                    var patientListURL = ipAddress + "/homecare/gps/?user-id=" + uIdItem.userId + "&time-of-record=:bt:" + stDate.getTime() + "," + edTime;
                } else {
                    var patientListURL = ipAddress + "/homecare/gps/?user-id=" + cmbUsers.value() + "&time-of-record=:bt:" + stDate.getTime() + "," + edTime;
                }
            }
        }

        getAjaxObject(patientListURL, "GET", onPatientListData, onErrorMedication);
    }

}
function onPatientListData(dataObj) {
    console.log(dataObj);
    var tempArray = [];
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.gps) {
            if ($.isArray(dataObj.response.gps)) {
                tempArray = dataObj.response.gps;
            } else {
                tempArray.push(dataObj.response.gps);
            }
        }
    } else {
        customAlert.error("Error", dataObj.response.status.message);
    }

    for (var i = 0; i < tempArray.length; i++) {
        var obj = tempArray[i];
        if (obj) {
            var dataItemObj = {};
            dataItemObj.DTR = kendo.toString(new Date(obj.timeOfRecord), "MM/dd/yyyy hh:mm:ss");;
            dataItemObj.LOC = obj.location;
            if (obj.logType == "1") {
                dataItemObj.IN = "In Time";
            } else {
                dataItemObj.IN = "Out Time";
            }

            dataItemObj.AD = "";
            dataItemObj.PN = "";
            dataItemObj.LAT = obj.latitude;
            dataItemObj.LNG = obj.longitude;
            dataArray.push(dataItemObj);
        }
    }
    console.log(dataArray);
    $('.btn-print').show();
    for (var i = 0; i < dataArray.length; i++) {
    	$('#tracking-content').append('<tr><td>'+dataArray[i].DTR+'</td><td>'+dataArray[i].LOC+'</td><td>'+dataArray[i].IN+'</td><td><label>'+dataArray[i].AD+'</label><label>'+dataArray[i].PN+'</label></td></tr>');
    }
}

function onErrorMedication(errobj) {
    console.log(errobj);
}

function buttonEvents() {
    $("#btnSearch").off("click");
    $("#btnSearch").on("click", getPatientList);

    $('#btnPrint').off('click', printData);
	$('#btnPrint').on('click', printData);
    
    allowNumerics("txtSearch");
}
var angularUIgridWrapper;
var angularUISelgridWrapper;

var parentRef = null;
var patientId = "";

$(document).ready(function(){
	parentRef = parent.frames['iframe'].window;
	patientId = parentRef.patientId;
	var dataOptions = {
        pagination: false,
        changeCallBack: onChange
	}
	angularUIgridWrapper = new AngularUIGridWrapper("dgAvlPatientDietList", dataOptions);
	angularUIgridWrapper.init();
	buildFileResourceListGrid([]);
	
	var dataOptions1 = {
	        pagination: false,
	        changeCallBack: onChange1
		}
		angularUISelgridWrapper = new AngularUIGridWrapper("dgSelPatientDietList", dataOptions1);
		angularUISelgridWrapper.init();
		buildFileResourceSelListGrid([]);
});


$(window).load(function(){
	$(window).resize(adjustHeight);
	onLoaded();
	if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
	init();
    buttonEvents();
	adjustHeight();
}

var recordType = "";
function init(){
	var str1 = "";
	var str2 = "";
	recordType = parentRef.recordType;
	if(recordType == "1"){
		str1 = "Diet Master Files :";
		str2 = "Diet Files Attached :";
		getAjaxObject(ipAddress+"/patient/diet/"+patientId,"GET",getPatientDietList,onError);
	}else if(recordType == "5"){
		str1 = "illness Master Files:";
		str2 = "illness Files Attached :";
		getAjaxObject(ipAddress+"/patient/illness/"+patientId,"GET",getPatientDietList,onError);
	}else if(recordType == "2"){
		str1 = "Excercise Master Files :";
		str2 = "Excercise Files Attached :";
		getAjaxObject(ipAddress+"/patient/exercise/"+patientId,"GET",getPatientDietList,onError);
	}
	$("#lbl1").text(str1);
	$("#lbl2").text(str2);
}
function onError(errorObj){
	console.log(errorObj);
}
var dietArray = [];
var dataArray = [];

function getPatientDietList(dataObj){
	console.log(dataObj);
	 dietArray = [];
	if(dataObj){
		if($.isArray(dataObj)){
			dietArray = dataObj;
		}else{
			dietArray.push(dataObj);
		}
	}
	buildFileResourceListGrid(dietArray);
	getAjaxObject(ipAddress+"/file-resource/list/"+recordType,"GET",getCountryList,onError);
}
function getCountryList(dataObj){
	console.log(dataObj);
	dataArray = [];
	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		if($.isArray(dataObj.response.fileResource)){
			dataArray = dataObj.response.fileResource;
		}else{
			dataArray.push(dataObj.response.fileResource);
		}
	}
	var tempDataArray = [];
	for(var i=0;i<dataArray.length;i++){
		if(dataArray[i].fileName){
			dataArray[i].idk = dataArray[i].id; 
			dataArray[i].Status = "InActive";
			if(dataArray[i].isActive == 1){
				dataArray[i].Status = "Active";
			}
			if(!getFileExist(dataArray[i].fileName)){
				tempDataArray.push(dataArray[i]);
			}
		}
	}
	buildFileResourceSelListGrid(tempDataArray);
}
function getFileExist(id1){
	var flag = false;
	for(var i=0;i<dietArray.length;i++){
		var dataObj = dietArray[i];
		if(dataObj && dataObj.fileName == id1){
			flag = true;
			break;
		}
	}
	return flag;
}
function buttonEvents(){
	$("#btnCancelDet").off("click",onClickCancel);
	$("#btnCancelDet").on("click",onClickCancel);
	
	$("#btnAdd").off("click");
	$("#btnAdd").on("click",onClickAdd);
	
	$("#btnSubRight").off("click");
	$("#btnSubRight").on("click",onClickSubRight);
	
	$("#btnSubLeft").off("click");
	$("#btnSubLeft").on("click",onClickSubLeft);
	
}
function adjustHeight(){
	var defHeight = 140;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    if(angularUIgridWrapper){
    	angularUIgridWrapper.adjustGridHeight(cmpHeight);
    }
	if(angularUISelgridWrapper){
		angularUISelgridWrapper.adjustGridHeight(cmpHeight);
	}
}

function buildFileResourceListGrid(dataSource) {
	var gridColumns = [];
	var otoptions = {};
	otoptions.noUnselect = false;
	 gridColumns.push({
	        "title": "Select",
	        "field": "SEL",
	        "cellTemplate": showCheckBoxTemplate(),
	        "width":"15%"
		});
    gridColumns.push({
        "title": "File Name",
        "field": "fileName",
         "width":"67%"
	});
    gridColumns.push({
        "title": "File Type",
        "field": "fileType",
	});
   angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions); 
	adjustHeight();
	//onIndividualRowClick();
}
var prevSelectedItem =[];
function onChange(){
	
}
function buildFileResourceSelListGrid(dataSource) {
	var gridColumns = [];
	var otoptions = {};
	otoptions.noUnselect = false;
	 gridColumns.push({
	        "title": "Select",
	        "field": "SEL",
	        "cellTemplate": showCheckBoxTemplate(),
	        "width":"15%"
		});
    gridColumns.push({
        "title": "File Name",
        "field": "fileName",
        "width":"67%"
	});
    gridColumns.push({
        "title": "File Type",
        "field": "fileType",
	});
   angularUISelgridWrapper.creategrid(dataSource, gridColumns,otoptions); 
	adjustHeight();
	//onIndividualRowClick();
}
var prevSelectedItem =[];
function onChange1(){
	
}

function showCheckBoxTemplate(){
	var node = '<div style="text-align:center;padding-top: 6px;"><input type="checkbox" class="check1" id="chkbox" ng-model="row.entity.SEL" style="width:20px;height:20px;margin:0px;"   onclick="onSelect(event);"></input></div>';
	return node;
}
function onSelect(evt){
	console.log(evt);
}

function onClickSubRight(){
//	alert("right");
	 var selGridData = angularUISelgridWrapper.getAllRows();
	 var selList = [];
	    for (var i = 0; i < selGridData.length; i++) {
	        var dataRow = selGridData[i].entity;
	        if(dataRow.SEL){
	        	angularUISelgridWrapper.deleteItem(dataRow);
	        	//dataRow.SEL = false;
	        	angularUIgridWrapper.insert(dataRow);
	        }
	    }  
}
function onClickSubLeft(){
	//alert("click");
	 var selGridData = angularUIgridWrapper.getAllRows();
	 var selList = [];
	    for (var i = 0; i < selGridData.length; i++) {
	        var dataRow = selGridData[i].entity;
	        if(dataRow.SEL){
	        	angularUIgridWrapper.deleteItem(dataRow);
	        	angularUISelgridWrapper.insert(dataRow);
	        }
	    }  
}
function onClickAdd(){
	var rows = angularUIgridWrapper.getAllRows();
	var selRows = angularUISelgridWrapper.getAllRows();
	//console.log(rows);
	var dArray = [];
	for(var i=0;i<rows.length;i++){
		var rowObj = rows[i].entity;
		if(rowObj){
			if(!rowObj.patientFileResourceId){
				var obj = {};
				//obj.id = "";
				obj.createdBy = sessionStorage.userId;
				obj.isActive = "1";
				obj.isDeleted = "0";
				obj.fileResourceId = rowObj.idk;
				obj.patientId = patientId;
				obj.isAccessed = "0";
				dArray.push(obj);
		}
	 }
	}
	
	for(var j=0;j<selRows.length;j++){
		var rowObj = selRows[j].entity;
		if(rowObj){
			if(rowObj.patientFileResourceId){
				var obj = {};
				obj.id = rowObj.patientFileResourceId;
				obj.createdBy = sessionStorage.userId;
				obj.isActive = "1";
				obj.isDeleted = "1";
				obj.fileResourceId = rowObj.patientFileResourceId;
				obj.patientId = patientId;
				obj.isAccessed = "0";
				dArray.push(obj);
			}
		}
	}
	var dataObj = {};
	dataObj = dArray;
	var dataUrl = ipAddress+"/patient/file-resource/add-or-remove";
	createAjaxObject(dataUrl,dataObj,"POST",onCreate,onError);
}
function onCreate(dataObj){
	console.log(dataObj);
	var status = "fail";
	var flag = true;
	if(dataObj){
		if(dataObj && dataObj.response){
			if(dataObj.response.status){
				if(dataObj.response.status.code == "1"){
					status = "success";
				}else{
					flag = false;
					customAlert.info("error", dataObj.response.status.message);
				}
			}
		}
	}
	if(flag){
		var obj = {};
		obj.status = status;
		popupClose(obj);
	}
}
function popupClose(st){
	var onCloseData = new Object();
	onCloseData = st;
	var windowWrapper = new kendoWindowWrapper();
	windowWrapper.closePageWindow(onCloseData);
}
function onClickCancel(){
	var obj = {};
	var windowWrapper = new kendoWindowWrapper();
	windowWrapper.closePageWindow(obj);
}

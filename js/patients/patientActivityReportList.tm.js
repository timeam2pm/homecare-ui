var angularUIgridWrapper;
var angularUISelgridWrapper;
var angularUIgridWrapper1;

var angularUIgridWrapperA;
var angularUISelgridWrapperA;
var angularUIgridWrapper1A;

var parentRef = null;
var patientId = "";

var activityid = "1";

var ADL = "1";
var IADL = "2";
var CARE = "3";
var FLUID = "4";
var FOOD = "5";
var menuLoaded = false;

var typeArr = [{Key:'ADL',Value:'1'},{Key:'iADL',Value:'2'},{Key:'Care',Value:'3'},{Key:'Fluid',Value:'4'},{Key:'Food',Value:'5'}];
var compType = [{Key:'Text',Value:'1'},{Key:'Text Area',Value:'2'},{Key:'Boolean',Value:'3'},{Key:'Checkbox',Value:'4'},{Key:'Radio Button',Value:'5'}];


$(document).ready(function(){
    // parentRef = parent.frames['iframe'].window;
    $("#pnlPatient",parent.document).css("display","none");
    sessionStorage.setItem("IsSearchPanel", "1");
    $("#liReason").addClass("activeList");
    $("#liReason",parent.document).addClass("activeList");
    parentRef = parent.frames['iframe'].window;
    patientId = parentRef.patientId;
    // getAjaxObject(ipAddress+"/homecare/appointment-reason-activities/?fields=*,activity.*","GET",getAppointmentReasonList,onError);

    var dataOptionsTaskGroup = {
        pagination: false,
        changeCallBack: onChangeAppointmentReason
    }
    angularUIgridWrapper1 = new AngularUIGridWrapper("dgridActivTypeList", dataOptionsTaskGroup);
    angularUIgridWrapper1.init();
    buildStateListGrid([]);

    var dataOptions1 = {
        pagination: false,
        changeCallBack: onChange1
    }
    angularUISelgridWrapper = new AngularUIGridWrapper("dgSelPatientActivityList", dataOptions1);
    angularUISelgridWrapper.init();
    buildFileResourceSelListGrid([]);

    // Popup grids

    var dataOptionsTaskGroup1 = {
        pagination: false,
        changeCallBack: onChangeAppointmentReason1
    }
    angularUIgridWrapper1A = new AngularUIGridWrapper("dgridActivTypeList1", dataOptionsTaskGroup1);
    angularUIgridWrapper1A.init();
    buildStateListGrid1([]);

    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapperA = new AngularUIGridWrapper("dgAvlPatientActivityList", dataOptions);
    angularUIgridWrapperA.init();
    buildFileResourceListGrid1([]);

    var dataOptions2 = {
        pagination: false,
        changeCallBack: onChange2
    }
    angularUISelgridWrapperA = new AngularUIGridWrapper("dgSelPatientActivityList1", dataOptions2);
    angularUISelgridWrapperA.init();
    buildFileResourceSelListGrid1([]);

});

function onChange2(){

}

$(window).load(function(){
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onClickItem(evt){
    removeSelections();
    var strId = evt.currentTarget.id;
    $("#"+strId).addClass("selectedMenu");
    var strId = evt.currentTarget.name;
    var strActivity = getActivityNameById(strId);
    $("#lblActivity").text(strActivity);
    activityid = strId;
    showGrid();
    //init();
}
function removeSelections(){
    for(var i=0;i<typeArr.length;i++){
        var item = typeArr[i];
        var strItem = item.Key;
        var strVal = item.Value;
        $("#"+strVal).removeClass("selectedMenu");
    }
}
function onLoaded(){
    // $("#cmbAppointmentReason").kendoComboBox();
    // setDataForSelection(vacation, "cmbAppointmentReason", function() {
    //     onTemplateChange();
    // }, ["desc", "idk"], 0, "");
    getAjaxObject(ipAddress+"/homecare/appointment-reason-activities/?fields=*,activity.*","GET",getAppointmentReasonList,onError);
    // getAjaxObject(ipAddress+"/homecare/appointment-reason-activities/?patient-id="+patientId,"GET",getPatientAppointmentReasonList,onError);
    $("#cmbAppointmentReason1").kendoComboBox();
    init();
    // getAjaxObject(ipAddress+"/homecare/activity-types/?is-active=1&is-deleted=0&sort=type","GET",getTaskTypes,onError);


    var url = ipAddress+"/master/appointment_reason/list/?is-active=1&is-deleted=0";
    getAjaxObject(url,"GET",handleGetTemplateList1,onError);
    buttonEvents();
    adjustHeight();
}

var recordType = "";
function init(){
    $("#cmbAppointmentReason").kendoComboBox({change:onTemplateChange});
    setDataForSelection(vacation, "cmbAppointmentReason1", function() {
        onTemplateChange1();
    }, ["desc", "idk"], 0, "");
    getAjaxObject(ipAddress+"/master/type/list/?name=component","GET",getComponentTypes,onError);
}

function getComponentTypes(dataObj){
    var tempCompType = [];
    compType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.typeMaster){
            if($.isArray(dataObj.response.typeMaster)){
                tempCompType = dataObj.response.typeMaster;
            }else{
                tempCompType.push(dataObj.response.typeMaster);
            }
        }
    }
    for(var i=0;i<tempCompType.length;i++){
        var obj = {};
        obj.Key = tempCompType[i].type;
        obj.Value = tempCompType[i].id;
        compType.push(obj);
    }
    getAjaxObject(ipAddress+"/master/type/list/?name=activity","GET",getActivityTypes,onError);
}

function getActivityTypes(dataObj){
    var tempCompType = [];
    typeArr = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.typeMaster){
            if($.isArray(dataObj.response.typeMaster)){
                tempCompType = dataObj.response.typeMaster;
            }else{
                tempCompType.push(dataObj.response.typeMaster);
            }
        }
    }
    tempCompType.sort(function(a, b) {
        var nameA = a.type.toUpperCase(); // ignore upper and lowercase
        var nameB = b.type.toUpperCase(); // ignore upper and lowercase
        if (nameA < nameB) {
            return -1;
        }
        if (nameA > nameB) {
            return 1;
        }
        // names must be equal
        return 0;
    });

    for(var i=0;i<tempCompType.length;i++){
        var obj = {};
        obj.Key = tempCompType[i].type;
        obj.Value = tempCompType[i].id;
        typeArr.push(obj);
    }
    // showGrid();
}
function showGrid(){
    buildFileResourceSelListGrid([]);
    buildFileResourceListGrid1([]);
    buildFileResourceSelListGrid1([]);
    getAjaxObject(ipAddress+"/activity/list?is-active=1&is-deleted=0&activity-type-id="+ activityid +"&sort=activity","GET",getStateList,onError);
}

function onError(errorObj){
    //console.log(errorObj);
}
var interestArr = [];
var dataArray = [];


function getActivityList(dataObj){
    dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if($.isArray(dataObj.response.activity)){
            dataArray = dataObj.response.activity;
        }else{
            dataArray.push(dataObj.response.activity);
        }
    }
    var tempDataArray = [];
    var reasonDataArray = [];
    var cmbAppointmentReason = $("#cmbAppointmentReason").data("kendoComboBox");

    for(var i=0;i<dataArray.length;i++){
        if(dataArray[i].id && dataArray[i].activityTypeId == activityid && dataArray[i].isDeleted == 0){
            dataArray[i].idk = dataArray[i].id;
            dataArray[i].Status = "InActive";
            if(dataArray[i].isActive == 1){
                dataArray[i].Status = "Active";
            }
            dataArray[i].activityID = getActivityNameById(dataArray[i].activityTypeId);
            dataArray[i].Type = getTypeNameById(dataArray[i].componentId);
            if(!getFileExist(dataArray[i].id)){
                if(dataArray[i].appointmentReasonId == Number(cmbAppointmentReason.value())){
                    reasonDataArray.push(dataArray[i]);
                }else{
                    tempDataArray.push(dataArray[i]);
                }

            }
        }
    }
    buildFileResourceSelListGrid(reasonDataArray);
}

function getActivityNameById(aId){
    for(var i=0;i<typeArr.length;i++){
        var item = typeArr[i];
        if(item && item.Value == aId){
            return item.Key;
        }
    }
    return "";
}

function getComponentNameById(aId){
    for(var i=0;i<actComponents.length;i++){
        var item = actComponents[i];
        if(item && item.componentId == aId){
            return item.activity;
        }
    }
    return "";
}


function getTypeNameById(aId){
    for(var i=0;i<compType.length;i++){
        var item = compType[i];
        if(item && item.Value == aId){
            return item.Key;
        }
    }
    return "";
}
function getFileExist(id1){
    var flag = false;
    for(var i=0;i<interestArr.length;i++){
        var dataObj = interestArr[i];
        if(dataObj && dataObj.activityId == id1){
            flag = true;
            break;
        }
    }
    return flag;
}
function buttonEvents(){
    $("#btnCancel").off("click",onClickCancel);
    $("#btnCancel").on("click",onClickCancel);

    $("#btnSave").off("click",onClickSave);
    $("#btnSave").on("click",onClickSave);

    $("#btnAdd").off("click");
    $("#btnAdd").on("click",onClickAddTasks);

    $("#btnSubRight").off("click");
    $("#btnSubRight").on("click",onClickSubRight);

    $("#btnSubLeft").off("click");
    $("#btnSubLeft").on("click",onClickSubLeft);

    $("#btnADL").off("click",onClickAddADL);
    $("#btnADL").on("click",onClickAddADL);

    $("#btniADL").off("click",onClickAddiADL);
    $("#btniADL").on("click",onClickAddiADL);

    $("#btnCare").off("click",onClickAddCare);
    $("#btnCare").on("click",onClickAddCare);

    $("#btnFluid").off("click",onClickAddFluid);
    $("#btnFluid").on("click",onClickAddFluid);

    $("#btnFood").off("click",onClickAddFood);
    $("#btnFood").on("click",onClickAddFood);

}

function onClickAddADL(){
    $("#btnADL").addClass("selectButtonBarClass");
    $("#btniADL").removeClass("selectButtonBarClass");
    $("#btnCare").removeClass("selectButtonBarClass");
    $("#btnFluid").removeClass("selectButtonBarClass");
    $("#btnFood").removeClass("selectButtonBarClass");
    activityid = ADL;
    init();
}
function onClickAddiADL(){
    $("#btnADL").removeClass("selectButtonBarClass");
    $("#btniADL").addClass("selectButtonBarClass");
    $("#btnCare").removeClass("selectButtonBarClass");
    $("#btnFluid").removeClass("selectButtonBarClass");
    $("#btnFood").removeClass("selectButtonBarClass");
    activityid = IADL;
    init();
}
function onClickAddCare(){
    $("#btnADL").removeClass("selectButtonBarClass");
    $("#btniADL").removeClass("selectButtonBarClass");
    $("#btnCare").addClass("selectButtonBarClass");
    $("#btnFluid").removeClass("selectButtonBarClass");
    $("#btnFood").removeClass("selectButtonBarClass");
    activityid = CARE;
    init();
}
function onClickAddFluid(){
    $("#btnADL").removeClass("selectButtonBarClass");
    $("#btniADL").removeClass("selectButtonBarClass");
    $("#btnCare").removeClass("selectButtonBarClass");
    $("#btnFluid").addClass("selectButtonBarClass");
    $("#btnFood").removeClass("selectButtonBarClass");
    activityid = FLUID;
    init();
}
function onClickAddFood(){
    $("#btnADL").removeClass("selectButtonBarClass");
    $("#btniADL").removeClass("selectButtonBarClass");
    $("#btnCare").removeClass("selectButtonBarClass");
    $("#btnFluid").removeClass("selectButtonBarClass");
    $("#btnFood").addClass("selectButtonBarClass");
    activityid = FOOD;
    init();
}
function adjustHeight(){
    var defHeight = 200;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;

    if(angularUISelgridWrapper){
        angularUISelgridWrapper.adjustGridHeight(cmpHeight);
    }

    $("#divButttons").height(cmpHeight+50);
}

function adjustHeight1(){
    var defHeight = 300;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;

    if(angularUISelgridWrapperA){
        angularUISelgridWrapperA.adjustGridHeight(cmpHeight);
    }

}

function toggleSelectAll(e) {
    var checked = e.currentTarget.checked;
    var rows = angularUIgridWrapperA.getScope().gridApi.core.getVisibleRows();
    for (var i = 0; i < rows.length; i++) {
        rows[i].entity["SEL"] = checked;
    }
}

function toggleSelectAll1(e) {
    var checked = e.currentTarget.checked;
    var rows = angularUISelgridWrapperA.getScope().gridApi.core.getVisibleRows();
    for (var i = 0; i < rows.length; i++) {
        rows[i].entity["SEL"] = checked;
    }
}

function toggleReqAll(event){
    var checked = event.currentTarget.checked;
    var rows = angularUIgridWrapperA.getScope().gridApi.core.getVisibleRows();
    for (var i = 0; i < rows.length; i++) {
        rows[i].entity["REQ"] = checked;
    }
}
function buildFileResourceListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Select",
        "field": "SEL",
        "cellTemplate": showCheckBoxTemplate(),
        "headerCellTemplate": "<input type='checkbox' class='check1' ng-model='TCSELECT' onclick='toggleSelectAll(event);' style='width:20px;height:20px;'></input>",
        "width":"15%"
    });
    gridColumns.push({
        "title": "Component",
        "field": "activity",
    });
    /*gridColumns.push({
     "title": "Charge",
     "field": "charge",
     });
     gridColumns.push({
     "title": "Duration",
     "field": "duration",
     });*/
    gridColumns.push({
        "title": "Required",
        "field": "REQ",
        "headerCellTemplate": "<div><input type='checkbox' class='check1' ng-model='TCSELEREQ' onclick='toggleReqAll(event);' style='width:20px;height:20px;'></input> <span>Required</span></div>",
        "cellTemplate": showRequireCheckBoxTemplate(),
    });
    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}
function showReqHeaderCheckBoxTemplate(){
    var node = '<div style="text-align:center;padding-top: 6px;"><input type="checkbox" class="check1" id="chkbox1" ng-model="row.entity.REQ" style="width:20px;height:20px;margin:0px;"   onclick="onReqSelect(event);"></input></div>';
    return node;
}

function showRequireCheckBoxTemplate(){
    var node = '<div style="text-align:center;padding-top: 6px;"><input type="checkbox" class="check1" id="chkbox" ng-model="row.entity.REQ" style="width:20px;height:20px;margin:0px;"   onclick="onSelect(event);"></input></div>';
    return node;
}
var prevSelectedItem =[];
function onChange(){

}
function buildFileResourceSelListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    // gridColumns.push({
    //     "title": "Select",
    //     "field": "SEL",
    //     "cellTemplate": showCheckBoxTemplate(),
    //     "headerCellTemplate": "<input type='checkbox' class='check1' ng-model='TCSELECT' onclick='toggleSelectAll1(event);' style='width:20px;height:20px;'></input>",
    //     "width":"15%"
    // });
    gridColumns.push({
        "title": "Component",
        "field": "activity"
    });

   gridColumns.push({
     "title": "Required",
     "field": "REQ",
   });
   gridColumns.push({
    "title": "Display Order",
    "field": "displayOrder"
    });


    angularUISelgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();

}
var prevSelectedItem =[];
function onChange1(){

}

function showCheckBoxTemplate(){
    var node = '<div style="text-align:center;padding-top: 6px;"><input type="checkbox" class="check1" id="chkbox" ng-model="row.entity.SEL" style="width:20px;height:20px;margin:0px;"   onclick="onSelect(event);"></input></div>';
    return node;
}
function onSelect(evt){
    //console.log(evt);
}

function onClickSubRight(){
//	alert("right");
    var selGridData = angularUISelgridWrapperA.getAllRows();
    var selList = [];
    var cmbAppointmentReason = $("#cmbAppointmentReason1").data("kendoComboBox");
    for (var i = 0; i < selGridData.length; i++) {
        selGridData[i].entity.appointmentReasonId = Number(cmbAppointmentReason.value());
        var dataRow = selGridData[i].entity;
        if(dataRow.SEL){
            angularUISelgridWrapperA.deleteItem(dataRow);
            //dataRow.SEL = false;
            angularUIgridWrapperA.insert(dataRow);
        }
    }
}
function onClickSubLeft(){
    //alert("click");
    var selGridData = angularUIgridWrapperA.getAllRows();
    var selList = [];
    for (var i = 0; i < selGridData.length; i++) {
        var dataRow = selGridData[i].entity;
        if(dataRow.SEL){
            angularUIgridWrapperA.deleteItem(dataRow);
            angularUISelgridWrapperA.insert(dataRow);
        }
    }
}


function onCreate(dataObj){
    var status = "fail";
    var flag = true;
    if(dataObj){
        if(dataObj && dataObj.response){
            if(dataObj.response.status){
                if(dataObj.response.status.code == "1"){
                    status = "Saved successfully";
                    displaySessionErrorPopUp("Info", status, function(res) {
                        onLoaded();
                        onClickCancel();
                    })
                }else{
                    flag = false;
                    customAlert.info("error", dataObj.response.status.message);
                }
            }
        }
    }
    if(flag){
        var obj = {};
        obj.status = status;
        popupClose(obj);
    }
}
function popupClose(st){
    var onCloseData = new Object();
    onCloseData = st;
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(onCloseData);
}

function onClickCancel(){
    $("#divGrid").css("display","block");
    $("#addPopup").css("display","none");
    buildFileResourceSelListGrid([]);
    buildFileResourceListGrid1([]);
    buildFileResourceSelListGrid1([]);
    buildStateListGrid1([]);
    onLoaded();
}

function onChangeAppointmentReason(){
    buildFileResourceSelListGrid([]);
    setTimeout(function(){
        var selectedItems = angularUIgridWrapper1.getSelectedRows();
        if(selectedItems && selectedItems.length>0){
            activityid = selectedItems[0].activityActivityTypeId;
            buildFileResourceSelListGrid([]);
            buildFileResourceListGrid1([]);
            buildFileResourceSelListGrid1([]);

            getAjaxObject(ipAddress+"/activity/list?is-active=1&is-deleted=0&activity-type-id="+ activityid +"&sort=activity","GET",getStateList1,onError);
        }else{
            $("#btnSave").prop("disabled", true);
            $("#btnDelete").prop("disabled", true);
        }
    },100)

}

function buildStateListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Task Groups",
        "field": "activityType",
    });
    gridColumns.push({
        "title": "Display Order",
        "field": "displayOrder"
    });
    angularUIgridWrapper1.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
}

function getTaskTypes(dataObj){
    var types = [];
    var suTypes = [];
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            if(dataObj.response.activityTypes){
                if($.isArray(dataObj.response.activityTypes)){
                    types = dataObj.response.activityTypes;
                }else{
                    types.push(dataObj.response.activityTypes);
                }
            }


            for(var i=0;i<types.length;i++){
                types[i].idk = types[i].id;
                var obj = _.where(patientAppointmentReasonDataArray, {activityActivityTypeId:  types[i].idk })
                if(obj && obj.length > 0){
                    obj[0].displayOrder = types[i].displayOrder;
                    suTypes.push(obj[0]);
                }
            }
        }
    }

     var uniquespatientAppointmentReasonDataArray = _.map(_.groupBy(suTypes,function(doc){
                return doc.appointmentReasonId;
            }),function(grouped){
                return grouped[0];
            });

    if (uniquespatientAppointmentReasonDataArray !== null && uniquespatientAppointmentReasonDataArray.length > 0) {
        uniquespatientAppointmentReasonDataArray.sort(sortByDisplayOrderAsc);
    }

    var cmbAppointmentReason = $("#cmbAppointmentReason").data("kendoComboBox");
    var appointmentReasonId = Number(cmbAppointmentReason.value());

    var FilappointmentReasonDataArray = _.where(uniquespatientAppointmentReasonDataArray, {appointmentReasonId: appointmentReasonId})
    buildStateListGrid(FilappointmentReasonDataArray);

    buildStateListGrid1(types);

}


function getTaskTypes1(dataObj){
    buildStateListGrid1([]);
    var types = [];
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            if(dataObj.response.activityTypes){
                if($.isArray(dataObj.response.activityTypes)){
                    types = dataObj.response.activityTypes;
                }else{
                    types.push(dataObj.response.activityTypes);
                }
            }
            for(var i=0;i<types.length;i++){
                types[i].idk = types[i].id;
            }
        }
    }
    buildStateListGrid1(types);
}

var actComponents;
function getStateList(dataObj){
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.activity){
        if($.isArray(dataObj.response.activity)){
            dataArray = dataObj.response.activity;
        }else{
            dataArray.push(dataObj.response.activity);
        }
    }
    var cmbAppointmentReason = $("#cmbAppointmentReason").data("kendoComboBox");
    var appointmentReasonId = Number(cmbAppointmentReason.value());
    var tempDataArray  = [];
    var reasonDataArray = [];
    var tempDataArray1  = [];
    var reasonDataArray1 = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        // if(dataArray[i].appointmentReasonId == Number(cmbAppointmentReason.value())){
        //     reasonDataArray.push(dataArray[i]);
        // }else{
        //     tempDataArray.push(dataArray[i]);
        // }
        tempDataArray.push(dataArray[i]);

    }
    actComponents = dataArray;
    var listArray = dataArray;

    var cmbAppointmentReason1 = $("#cmbAppointmentReason1").data("kendoComboBox");
    var appointmentReasonId1 = Number(cmbAppointmentReason1.value());

    var values;
    for(var j=0;j<=(tempDataArray.length-1);j++) {
        var item = tempDataArray[j];
        if (appointmentReasonDataArray) {
            var FilappointmentReasonDataArray = _.where(appointmentReasonDataArray, {appointmentReasonId: appointmentReasonId})
            var FilappointmentReasonDataArray1 = _.where(appointmentReasonDataArray, {appointmentReasonId: appointmentReasonId1})
            for (var i = 0; i <=(FilappointmentReasonDataArray.length-1); i++) {
                if (FilappointmentReasonDataArray[i].activityId == item.idk && FilappointmentReasonDataArray[i].isActive == 1 && FilappointmentReasonDataArray1[i].patientId == patientId) {
                    tempDataArray[j].appidk = FilappointmentReasonDataArray[i].idk;
                    if(FilappointmentReasonDataArray[i].required){
                        tempDataArray[j].REQ = true;
                    }
                    else{
                        tempDataArray[j].REQ = false;
                    }
                    reasonDataArray.push(tempDataArray[j]);

                }
            }
            for (var i = 0; i <=(FilappointmentReasonDataArray1.length-1); i++) {
                if (FilappointmentReasonDataArray1[i].activityId == item.idk && FilappointmentReasonDataArray1[i].isActive == 1 && FilappointmentReasonDataArray1[i].patientId == patientId){
                    tempDataArray[j].appidk = FilappointmentReasonDataArray1[i].idk;
                    if(FilappointmentReasonDataArray1[i].required){
                        tempDataArray[j].REQ = true;
                    }
                    else{
                        tempDataArray[j].REQ = false;
                    }
                    reasonDataArray1.push(tempDataArray[j]);
                }
            }
        }
    }
    var val1 = _.difference(listArray, reasonDataArray1);

    buildFileResourceSelListGrid(reasonDataArray);
    buildFileResourceListGrid1(reasonDataArray1);
    buildFileResourceSelListGrid1(val1);

}

function getStateList1(dataObj){
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.activity){
        if($.isArray(dataObj.response.activity)){
            dataArray = dataObj.response.activity;
        }else{
            dataArray.push(dataObj.response.activity);
        }
    }
    var cmbAppointmentReason = $("#cmbAppointmentReason").data("kendoComboBox");
    var appointmentReasonId = Number(cmbAppointmentReason.value());
    var tempDataArray  = [];
    var reasonDataArray = [];
    var tempDataArray1  = [];
    var reasonDataArray1 = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        // if(dataArray[i].appointmentReasonId == Number(cmbAppointmentReason.value())){
        //     reasonDataArray.push(dataArray[i]);
        // }else{
        //     tempDataArray.push(dataArray[i]);
        // }
        tempDataArray.push(dataArray[i]);

    }
    actComponents = dataArray;
    var listArray = dataArray;

    var cmbAppointmentReason1 = $("#cmbAppointmentReason1").data("kendoComboBox");
    var appointmentReasonId1 = Number(cmbAppointmentReason1.value());

    var values;
    for(var j=0;j<=(tempDataArray.length-1);j++) {
        var item = tempDataArray[j];
        if (appointmentReasonDataArray) {
            var FilappointmentReasonDataArray = _.where(appointmentReasonDataArray, {appointmentReasonId: appointmentReasonId})
            var FilappointmentReasonDataArray1 = _.where(appointmentReasonDataArray, {appointmentReasonId: appointmentReasonId1})
            for (var i = 0; i <=(FilappointmentReasonDataArray.length-1); i++) {
                if (FilappointmentReasonDataArray[i].activityId == item.idk && FilappointmentReasonDataArray[i].isActive == 1 && FilappointmentReasonDataArray[i].patientId == patientId) {
                    tempDataArray[j].appidk = FilappointmentReasonDataArray[i].idk;
                    if(FilappointmentReasonDataArray[i].required){
                        tempDataArray[j].REQ = true;
                    }
                    else{
                        tempDataArray[j].REQ = false;
                    }
                    reasonDataArray.push(tempDataArray[j]);

                }
            }
            for (var i = 0; i <=(FilappointmentReasonDataArray1.length-1); i++) {
                if (FilappointmentReasonDataArray1[i].activityId == item.idk && FilappointmentReasonDataArray1[i].isActive == 1) {
                    tempDataArray[j].appidk = FilappointmentReasonDataArray1[i].idk;
                    if(FilappointmentReasonDataArray1[i].required){
                        tempDataArray[j].REQ = true;
                    }
                    else{
                        tempDataArray[j].REQ = false;
                    }
                    reasonDataArray1.push(tempDataArray[j]);

                }
            }
        }
    }
    var val1 = _.difference(listArray, reasonDataArray1);

    buildFileResourceSelListGrid(reasonDataArray);
    // buildFileResourceListGrid1(reasonDataArray1);
    // buildFileResourceSelListGrid1(val1);
}



var appointmentReasonDataArray = [];
var patientAppointmentReasonDataArray = [];
function getAppointmentReasonList(dataObj){
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.appointmentReasonActivities){
        if($.isArray(dataObj.response.appointmentReasonActivities)){
            dataArray = dataObj.response.appointmentReasonActivities;
        }else{
            dataArray.push(dataObj.response.appointmentReasonActivities);
        }
    }

    var tempDataArray  = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        tempDataArray.push(dataArray[i]);
    }
    appointmentReasonDataArray = tempDataArray;
    patientAppointmentReasonDataArray = _.where(appointmentReasonDataArray, {patientId: patientId});

    var url = ipAddress+"/master/appointment_reason/list/?is-active=1&is-deleted=0";
    getAjaxObject(url,"GET",handleGetTemplateList,onError);

}

var vacation = [];
var vacationReason = [];
function handleGetTemplateList(dataObj){
    if(dataObj && dataObj.response && dataObj.response.codeTable ){
        if(dataObj.response.status.code == "1"){
            if(dataObj.response.codeTable){
                if($.isArray(dataObj.response.codeTable)){
                    vacation = dataObj.response.codeTable;
                }else{
                    vacation.push(dataObj.response.codeTable);
                }
            }
        }
    }


    var uniques = _.map(_.groupBy(appointmentReasonDataArray,function(doc){
        return doc.appointmentReasonId;
    }),function(grouped){
        return grouped[0];
    });
    var linkupReasons =[];
    for(var j=0;j<vacation.length;j++){
        vacation[j].idk = vacation[j].id;
        for(var i=0;i<uniques.length;i++) {
            if(vacation[j].idk == uniques[i].appointmentReasonId){
                linkupReasons.push(vacation[j]);
            }


        }

    }
    vacationReason = vacation;
    vacation = linkupReasons;

    if(vacation.length ==0) {
        var msg = "Appoinment Reason Task Components does not exist. Do you want add?";
        displaySessionErrorPopUp("Info", msg, function (res) {
            $("#btnAdd").click();
        });
    }

    setDataForSelection(linkupReasons, "cmbAppointmentReason1", onTemplateChange1, ["desc", "idk"], 0, "");

    var arrayjson = [];

    var uniquespatientAppointmentReasonDataArray = _.map(_.groupBy(patientAppointmentReasonDataArray,function(doc){
        return doc.appointmentReasonId;
    }),function(grouped){
        return grouped[0];
    });

    for(var j=0;j<uniquespatientAppointmentReasonDataArray.length;j++) {
        var array = _.where(vacationReason, {idk : uniquespatientAppointmentReasonDataArray[j].appointmentReasonId});
        if(array && array != null){
            arrayjson.push(array[0]);
        }
    }
    setDataForSelection(arrayjson, "cmbAppointmentReason", onTemplateChange, ["desc", "idk"], 0, "");

    // onTemplateChange();

    // setTimeout(function(){
    //
    //     //buildDeviceListGrid1(miniArray);
    //     //onChange1();
    //     init();
    //     adjustHeight();
    // },100);

    // var cmbAppointmentReason = $("#cmbAppointmentReason").data("kendoComboBox");
    // var appointmentReasonId = Number(cmbAppointmentReason.value());
    getComboListIndex("cmbAppointmentReason", "idk", firstappointmentReasonId);
    onTemplateChange();
    // getAjaxObject(ipAddress+"/homecare/appointment-reason-activities/","GET",getAppointmentReasonList,onError);
}

function onTemplateChange() {
    // onComboChange("cmbAppointmentReason");
    buildStateListGrid([]);
    buildFileResourceSelListGrid([]);
    var cmbAppointmentReason = $("#cmbAppointmentReason").data("kendoComboBox");
    var appointmentReasonId = Number(cmbAppointmentReason.value());
    getAjaxObject(ipAddress+"/homecare/activity-types/?is-active=1&is-deleted=0&sort=type&appointment-reason-id="+appointmentReasonId,"GET",getTaskTypes,onError);
    // getAjaxObject(ipAddress+"/homecare/appointment-reason-activities/?fields=*,activity.*&appointment-reason-id="+appointmentReasonId,"GET",getAppointmentReasonList,onError);
}

function removeByIndex(arr, index) {
    arr.splice(index, 1);
}

var firstappointmentReasonId;

function onClickAddTasks() {
    $("#divGrid").css("display", "none");
    $("#addPopup").css("display", "block");
    var cmbAppointmentReason = $("#cmbAppointmentReason").data("kendoComboBox");
    var appointmentReasonId = Number(cmbAppointmentReason.value());

    firstappointmentReasonId = appointmentReasonId;
    getComboListIndex("cmbAppointmentReason1", "idk", appointmentReasonId);
    onTemplateChange1();
    // getAjaxObject(ipAddress+"/homecare/activity-types/?is-active=1&is-deleted=0&sort=type","GET",getTaskTypes1,onError);

}

function getComboListIndex(cmbId, attr, attrVal) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb) {
        var ds = cmb.dataSource;
        var totalRec = ds.total();
        for (var i = 0; i < totalRec; i++) {
            var dtItem = ds.at(i);
            if (dtItem && dtItem[attr] == attrVal) {
                cmb.select(i);
                return i;
            }
        }
    }
    return -1;
}

function onChangeAppointmentReason1(){
    buildFileResourceListGrid1([]);
    buildFileResourceSelListGrid1([]);
    setTimeout(function(){
        var selectedItems = angularUIgridWrapper1A.getSelectedRows();
        if(selectedItems && selectedItems.length>0){
            activityid = selectedItems[0].idk;
            showGrid();
        }else{
            $("#btnSave").prop("disabled", true);
            $("#btnDelete").prop("disabled", true);
        }
    },100)

}


function buildStateListGrid1(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Task Groups",
        "field": "type",
    });
    angularUIgridWrapper1A.creategrid(dataSource, gridColumns,otoptions);
    // adjustHeight();
}


function buildFileResourceSelListGrid1(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Select",
        "field": "SEL",
        "cellTemplate": showCheckBoxTemplate(),
        "headerCellTemplate": "<input type='checkbox' class='check1' ng-model='TCSELECT' onclick='toggleSelectAll1(event);' style='width:20px;height:20px;'></input>",
        "width":"15%"
    });
    gridColumns.push({
        "title": "Component",
        "field": "activity",
    });
    /*gridColumns.push({
     "title": "Charge",
     "field": "charge",
     });
     gridColumns.push({
     "title": "Duration",
     "field": "duration",
     });*/
    angularUISelgridWrapperA.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();

    var selGridData = angularUIgridWrapperA.getAllRows();
    angularUISelgridWrapperA.deleteItem(selGridData);

}


function handleGetTemplateList1(dataObj){
    var vacation = [];
    if(dataObj && dataObj.response && dataObj.response.codeTable ){
        if(dataObj.response.status.code == "1"){
            if(dataObj.response.codeTable){
                if($.isArray(dataObj.response.codeTable)){
                    vacation = dataObj.response.codeTable;
                }else{
                    vacation.push(dataObj.response.codeTable);
                }
            }
        }
    }
    for(var j=0;j<vacation.length;j++){
        vacation[j].idk = vacation[j].id;
    }
    setDataForSelection(vacation, "cmbAppointmentReason1", onTemplateChange1, ["desc", "idk"], 0, "");
    var cmbAppointmentReason = $("#cmbAppointmentReason1").data("kendoComboBox");
    var appointmentReasonId = Number(cmbAppointmentReason.value());
    getAjaxObject(ipAddress+"/homecare/appointment-reason-activities/?is-active=1&is-deleted=0&fields=*,activity.*","GET",getAppointmentReasonList1,onError);
    // /homecare/appointment-reason-activities/?access_token=37ff49f0-b4b2-4c9c-b272-d13408e71fbf&fields=*,activity.*&appointment-reason-id=9
}

function getAppointmentReasonList1(dataObj){
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.appointmentReasonActivities){
        if($.isArray(dataObj.response.appointmentReasonActivities)){
            dataArray = dataObj.response.appointmentReasonActivities;
        }else{
            dataArray.push(dataObj.response.appointmentReasonActivities);
        }
    }

    var tempDataArray  = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        tempDataArray.push(dataArray[i]);
    }
    appointmentReasonDataArray = tempDataArray;

    // buildFileResourceListGrid(tempDataArray);
}

function onTemplateChange1() {
    buildStateListGrid1([]);
    buildFileResourceListGrid1([]);
    buildFileResourceListGrid1([]);
    var cmbAppointmentReason = $("#cmbAppointmentReason1").data("kendoComboBox");
    var appointmentReasonId = Number(cmbAppointmentReason.value());
    getAjaxObject(ipAddress+"/homecare/activity-types/?is-active=1&is-deleted=0&sort=type&appointment-reason-id="+appointmentReasonId,"GET",getTaskTypes1,onError);
}

function removeByIndex(arr, index) {
    arr.splice(index, 1);
}

function onClickSave(){
    var rows = angularUIgridWrapperA.getAllRows();
    var selRows = angularUISelgridWrapperA.getAllRows();
    var dArray = [];


    var cmbAppointmentReason = $("#cmbAppointmentReason1").data("kendoComboBox");
    var appointmentReasonId = Number(cmbAppointmentReason.value());

    var method ="POST";
    for(var j=0;j<selRows.length;j++){
        method = "PUT";
        var rowObj = selRows[j].entity;
        if(rowObj.appidk) {
            var obj = {};
            obj.id = rowObj.appidk;
            obj.createdBy = Number(rowObj.createdBy);
            obj.modifiedBy = Number(sessionStorage.userId);
            // obj.modifiedDate = new Date().getTime();
            obj.patientId= patientId;

            obj.isDeleted = 1;
            obj.activityId = rowObj.idk;
            obj.appointmentReasonId = appointmentReasonId;
            obj.isActive = 0;
            if(rowObj.REQ){
                obj.required = 1;
            }else{
                obj.required = 0;
            }
            dArray.push(obj);
        }
    }



    for(var j=0;j<rows.length;j++){
        var rowObj = rows[j].entity;
        if(!rowObj.appidk) {
            var obj = {};
            // obj.id = rowObj.appidk;
            obj.createdBy = Number(sessionStorage.userId);
            // obj.modifiedBy = Number(sessionStorage.userId);
            // obj.createdDate = new Date().getTime();
            obj.patientId= patientId;
            obj.isDeleted = 0;
            obj.activityId = rowObj.idk;
            obj.appointmentReasonId = rowObj.appointmentReasonId;
            obj.isActive = 1;
            if(rowObj.REQ){
                obj.required = 1;
            }else{
                obj.required = 0;
            }
            dArray.push(obj);
        }
        else{
            var obj = {};
            method = "PUT";
            obj.id = rowObj.appidk;
            obj.createdBy = Number(rowObj.createdBy);
            obj.modifiedBy = Number(sessionStorage.userId);
            obj.patientId= patientId;
            // obj.modifiedDate = new Date().getTime();
            obj.isDeleted = 0;
            obj.activityId = rowObj.idk;
            obj.appointmentReasonId = rowObj.appointmentReasonId;
            obj.isActive = 1;
            if(rowObj.REQ){
                obj.required = 1;
            }else{
                obj.required = 0;
            }
            dArray.push(obj);
        }
    }
    var dataObj = {};

    dataObj = dArray;
    var dataUrl = ipAddress+"/homecare/appointment-reason-activities/batch/";
    createAjaxObject(dataUrl,dataObj,method,onCreate,onError);
}
function onCreate(dataObj){
    var status = "fail";
    var flag = true;
    if(dataObj){
        if(dataObj && dataObj.response){
            if(dataObj.response.status){
                if(dataObj.response.status.code == "1"){
                    status = "Saved successfully";
                    // customAlert.info("info", status);
                    displaySessionErrorPopUp("Info", status, function(res) {
                        // init();
                        onLoaded();
                        //getAjaxObject(ipAddress+"/homecare/appointment-reason-activities/?is-active=1&is-deleted=0&fields=*,activity.*","GET",getAppointmentReasonList1,onError);
                        // onClickCancel();
                        $("#divGrid").css("display","block");
                        $("#addPopup").css("display","none");

                    });
                }else{
                    flag = false;
                    customAlert.info("error", dataObj.response.status.message);
                }

            }
        }
    }
}


function buildFileResourceListGrid1(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Select",
        "field": "SEL",
        "cellTemplate": showCheckBoxTemplate(),
        "headerCellTemplate": "<input type='checkbox' class='check1' ng-model='TCSELECT' onclick='toggleSelectAll(event);' style='width:20px;height:20px;'></input>",
        "width":"15%"
    });
    gridColumns.push({
        "title": "Component",
        "field": "activity",
    });
    /*gridColumns.push({
     "title": "Charge",
     "field": "charge",
     });
     gridColumns.push({
     "title": "Duration",
     "field": "duration",
     });*/
    gridColumns.push({
        "title": "Required",
        "field": "REQ",
        "headerCellTemplate": "<div><input type='checkbox' class='check1' ng-model='TCSELEREQ' onclick='toggleReqAll(event);' style='width:20px;height:20px;'></input><span class='spnrequired'>Required</span></div>",
        "cellTemplate": showRequireCheckBoxTemplate(),
    });
    angularUIgridWrapperA.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}
function showReqHeaderCheckBoxTemplate(){
    var node = '<div style="text-align:center;padding-top: 6px;"><input type="checkbox" class="check1" id="chkbox1" ng-model="row.entity.REQ" style="width:20px;height:20px;margin:0px;"   onclick="onReqSelect(event);"></input></div>';
    return node;
}
// function toggleReqAll(event){
//     var checked = event.currentTarget.checked;
//     var rows = angularUIgridWrapper.getScope().gridApi.core.getVisibleRows();
//     for (var i = 0; i < rows.length; i++) {
//         rows[i].entity["REQ"] = checked;
//     }
// }

function showRequireCheckBoxTemplate(){
    var node = '<div style="text-align:center;padding-top: 6px;"><input type="checkbox" class="check1" id="chkbox" ng-model="row.entity.REQ" style="width:20px;height:20px;margin:0px;"   onclick="onSelect(event);"></input></div>';
    return node;
}

var sortByDisplayOrderAsc = function (x, y) {
    // This is a comparison function that will result in dates being sorted in
    // ASCENDING order. As you can see, JavaScript's native comparison operators
    // can be used to compare dates. This was news to me.


    return x.displayOrder - y.displayOrder;
};
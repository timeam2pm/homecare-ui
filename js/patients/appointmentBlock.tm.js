var parentRef = null;
var angularUIgridWrapper = null;
var angularUIgridWrapperL = null;

var startDT  = null;
var endDT = null;

var startDTL  = null;
var endDTL = null;

var appTypeArr = [{Key:'Notes1',Value:'Notes1'},{Key:'Notes2',Value:'Notes2'},{Key:'Notes3',Value:'Notes3'}];

var cntry = "";
var dtFMT = "dd/MM/yyyy";

$(document).ready(function(){
    parentRef = parent.frames['iframe'].window;
});

$(window).load(function(){
    init();
    buttonEvents();
});
function onFacilityChange(){

}
function getProviderList(dataObj){
    console.log(dataObj);
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if($.isArray(dataObj.response.provider)){
            dataArray = dataObj.response.provider;
        }else{
            dataArray.push(dataObj.response.provider);
        }
    }else{
        //customAlert.error("Error",dataObj.response.status.message);
    }
    var tempDataArry = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
        }
    }
    //providerArray = dataArray;
    setDataForSelection(dataArray, "cmbDoctor", onFacilityChange, ["firstName", "idk"], 0, "");
}
function init(){

    cntry = sessionStorage.countryName;
    if(cntry.indexOf("India")>=0){
        dtFMT = INDIA_DATE_FMT;
    }else  if(cntry.indexOf("United Kingdom")>=0){
        dtFMT = ENG_DATE_FMT;
    }else  if(cntry.indexOf("United State")>=0){
        dtFMT = US_DATE_FMT;
    }

    $("#cmbNotes").kendoComboBox();
    $("#cmbDoctor").kendoComboBox();

    setDataForSelection(appTypeArr, "cmbNotes", onNotesChange, ["Value", "Key"], 0, "");
    getAjaxObject(ipAddress+"/provider/list","GET",getProviderList,onError);
    $("#txtStartDate").kendoDatePicker({ min: new Date()});

    //$("#txtStartDate1").kendoDatePicker();
    //$("#txtEndDate1").kendoDatePicker();
    $("#txtStartTime").kendoTimePicker({interval: 15,format:dtFMT});
    $("#txtEndTime").kendoTimePicker({interval: 15,format:dtFMT});


    $("#txtStartDateL").kendoDatePicker({format:dtFMT});
    $("#txtEndDateL").kendoDatePicker({format:dtFMT});

    startDT = $("#txtStartDate1").kendoDatePicker({
        change: startChange,format:dtFMT
    }).data("kendoDatePicker");

    endDT = $("#txtEndDate1").kendoDatePicker({
        change: endChange,format:dtFMT
    }).data("kendoDatePicker");

    startDTL = $("#txtStartDateL").kendoDatePicker({
        change: startChangeL,format:dtFMT
    }).data("kendoDatePicker");

    endDTL = $("#txtEndDateL").kendoDatePicker({
        change: endChangeL,format:dtFMT
    }).data("kendoDatePicker");

    var currDT = new Date();
    var cMS = currDT.getTime();
    cMS = cMS+(24*60*60*1000);
    startDT.min(new Date());
    endDT.min(new Date());

    startDTL.min(new Date());
    endDTL.min(new Date());


    var dataOptions = {pagination: false,paginationPageSize: 500,changeCallBack: onChange};
    angularUIgridWrapper = new AngularUIGridWrapper("appBlockDays", dataOptions);
    angularUIgridWrapper.init();

    var dataOptions = {pagination: false,paginationPageSize: 500,changeCallBack: onChange1};
    angularUIgridWrapperL = new AngularUIGridWrapper("appBlockDaysList", dataOptions);
    angularUIgridWrapperL.init();

    adjustHeight();
    buildItemGrid([]);
    buildItemGridL([]);
}
function onNotesChange(){
    var cmbNotes = $("#cmbNotes").data("kendoComboBox");
    if(cmbNotes){
        if(cmbNotes.selectedIndex<0){
            cmbNotes.select(0);
        }
    }
}
function startChange() {
    var startDate = startDT.value(),
        endDate = endDT.value();

    if (startDate) {
        startDate = new Date(startDate);
        startDate.setDate(startDate.getDate());
        endDT.min(startDate);
    } else if (endDate) {
        startDT.max(new Date(endDate));
    } else {
        endDate = new Date();
        startDT.max(endDate);
        endDT.min(endDate);
    }
}

function endChange() {
    var endDate = endDT.value(),
        startDate = startDT.value();

    if (endDate) {
        endDate = new Date(endDate);
        endDate.setDate(endDate.getDate());
        startDT.max(endDate);
    } else if (startDate) {
        endDT.min(new Date(startDate));
    } else {
        endDate = new Date();
        startDT.max(endDate);
        endDT.min(endDate);
    }
}
function startChangeL() {
    var startDate = startDTL.value();
    var endDate = endDTL.value();

    if (startDate) {
        startDate = new Date(startDate);
        startDate.setDate(startDate.getDate());
        endDTL.min(startDate);
    } else if (endDate) {
        startDTL.max(new Date(endDate));
    } else {
        endDate = new Date();
        startDTL.max(endDate);
        endDTL.min(endDate);
    }
}

function endChangeL() {
    var endDate = endDTL.value();
    var startDate = startDTL.value();

    if (endDate) {
        endDate = new Date(endDate);
        endDate.setDate(endDate.getDate());
        startDTL.max(endDate);
    } else if (startDate) {
        endDTL.min(new Date(startDate));
    } else {
        endDate = new Date();
        startDTL.max(endDate);
        endDTL.min(endDate);
    }
}

function getGMTDateFromLocaleDate(sTime){
    var now = new Date(sTime);
    var utc = new Date(now.getTime() + now.getTimezoneOffset() * 60000);
    return utc.getTime();
}
function getLocalTimeFromGMT(sTime){
    var dte = new Date(sTime);
    var dt = new Date(dte.getTime() - dte.getTimezoneOffset()*60*1000);
    return dt.getTime();
}
function onClickAdd(){
    var dtObj  ={};
    //dtObj.SD  = $("#txtStartDate").val();
    //dtObj.ST = $("#txtStartTime").val();
    //dtObj.ET = $("#txtEndTime").val();
    var note = "";
    var sd = $("#txtStartDate").val();
    var st = $("#txtStartTime").data("kendoTimePicker");
    var et = $("#txtEndTime").data("kendoTimePicker");
    var cmbNotes = $("#cmbNotes").data("kendoComboBox");
    if(cmbNotes){
        note = cmbNotes.text();
    }
    if(st.value() && et.value()){
        var stValue = new Date(st.value());
        var etValue = new Date(et.value());
        var stv = stValue.getTime();
        var etv = etValue.getTime();
        if(etv>stv){
            //dtObj.SD  = $("#txtStartDate").val();
            dtObj.ST =  $("#txtStartTime").val();
            dtObj.ET = $("#txtEndTime").val();
            dtObj.NOTE = note;
            angularUIgridWrapper.insert(dtObj, 0);
            st.value("");
            et.value("");
        }else{
            customAlert.error("Error", "End Time should be greater than  Start Time");
        }
    }else{
        customAlert.error("Error", "Enter Proper Time");
    }

}
function buildItemGrid(dataSource){
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Select",
        "field": "SEL",
        "cellTemplate": showCheckBoxTemplate(),
        "headerCellTemplate": "<input type='checkbox' class='check1' ng-model='TCSELECT' onclick='toggleSelectAll(event);' style='width:20px;height:20px;'></input>",
        "width":"15%"
    });
    gridColumns.push({
        "title": "Start Time",
        "field": "ST",
    });
    gridColumns.push({
        "title": "End Time",
        "field": "ET",
    });
    gridColumns.push({
        "title": "Notes",
        "field": "NOTE",
    });
    /*gridColumns.push({
        "title": "Remove",
        "field": "DEL",
        "cellTemplate":showDeleteTemplate()
    });*/
    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
}
function buildItemGridL(dataSource){
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Select",
        "field": "SEL",
        "cellTemplate": showCheckBoxTemplate1(),
        "headerCellTemplate": "<input type='checkbox' class='check1' ng-model='TCSELECT' onclick='toggleSelectAll1(event);' style='width:20px;height:20px;'></input>",
        "width":"15%"
    });
    gridColumns.push({
        "title": "Start Date",
        "field": "SD",
    });
    gridColumns.push({
        "title": "Start Time",
        "field": "ST",
    });
    gridColumns.push({
        "title": "End Time",
        "field": "ET",
    });
    angularUIgridWrapperL.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
}
function showCheckBoxTemplate(){
    var node = '<div style="text-align:center;padding-top: 6px;"><input type="checkbox" class="check1" id="chkbox" ng-model="row.entity.SEL" style="width:20px;height:20px;margin:0px;"   onclick="onSelect(event);"></input></div>';
    return node;
}
function showCheckBoxTemplate1(){
    var node = '<div style="text-align:center;padding-top: 6px;"><input type="checkbox" class="check1" id="chkbox" ng-model="row.entity.SEL" style="width:20px;height:20px;margin:0px;"   onclick="onSelect1(event);"></input></div>';
    return node;
}
function toggleSelectAll(evt){
    console.log(evt);
    var checked = evt.currentTarget.checked;
    var rows = angularUIgridWrapper.getScope().gridApi.core.getVisibleRows();
    for (var i = 0; i < rows.length; i++) {
        rows[i].entity["SEL"] = checked;
    }
}
function toggleSelectAll1(evt){
    var checked = evt.currentTarget.checked;
    var rows = angularUIgridWrapperL.getScope().gridApi.core.getVisibleRows();
    for (var i = 0; i < rows.length; i++) {
        rows[i].entity["SEL"] = checked;
    }
}
function onSelect(){

}
function onSelect1(){

}
function onChange(){

}
function onChange1(){

}
function showDeleteTemplate(){
    var node = '<div style="text-align:center;padding-top:5px"><span onclick="onClickDel()" >Remove</span></div>';
    return node;
}
function onClickDel(){
    // window.top.confirmDialog(confirmTitleMessage,getLocaleStringWithDefault(LMSS_WIS_OUT, 'DO_YOU_WANT_DEL', "Are you sure you want to delete?"),onDelConfirm);
    setTimeout(function(){
        var arrSelRows = angularUIgridWrapper.getSelectedRows();
        console.log(arrSelRows);
        if(arrSelRows){
            var item = arrSelRows[0];
            if(item){
                angularUIgridWrapper.deleteItem(item);
                //window.top.displayErrorPopUp(infoTitleMessage,getLocaleStringWithDefault(LMSS_WIS_OUT, "DEL_SUCCESS", "Item deleted successfully"));
            }
        }
    })
}
function adjustHeight() {
    var defHeight = 400;
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    try{
        angularUIgridWrapper.adjustGridHeight(cmpHeight);
        angularUIgridWrapperL.adjustGridHeight(cmpHeight);
    }catch(e){};
}
function buttonEvents(){
    $("#btnSave").off("click");
    $("#btnSave").on("click",onClickSave);

    $("#btnAdd").off("click");
    $("#btnAdd").on("click",onClickAdd);

    $("#btnApplyDays").off("click");
    $("#btnApplyDays").on("click",onClickApplyDays);

    $("#btnAdd1").off("click");
    $("#btnAdd1").on("click",onClickAddDays);

    $("#btnBack").off("click");
    $("#btnBack").on("click",onClickBack);

    $("#btnDelete").off("click");
    $("#btnDelete").on("click",onClickDeleteBlockDays);

    $("#btnEdit").off("click");
    $("#btnEdit").on("click",onClickEditBlockDays);


    $("#hAdd").off("click");
    $("#hAdd").on("click",onClickhAdd);

    $("#hList").off("click");
    $("#hList").on("click",onClickhList);

    $("#btnView").off("click");
    $("#btnView").on("click",onClickViewList);

    $("#btnCancelDet").off("click");
    $("#btnCancelDet").on("click",onClickCancel);

}

function onClickhAdd(){
    console.log("hAdd");
    $("#divAdd").addClass("select");
    $("#divList").removeClass("select");

    $("#divSecond").show();
    $("#aST").show();
    $("#aET").show();
    $("#divNotes").show();
    $("#lSD").hide();
    $("#lED").hide();
    $("#btnAdd").show();
    $("#btnView").hide();
    $("#aBD").show();
    $("#lDB").hide();
    $("#btnSave").show();
    $("#btnEdit").hide();
    $("#btnDelete").hide();
    $("#btnApplyDays").show();
    $("#btnSave").show();
}
function onClickhList(){
    console.log("hList");
    $("#divAdd").removeClass("select");
    $("#divList").addClass("select");

    $("#divSecond").hide();
    $("#aST").hide();
    $("#aET").hide();
    $("#divNotes").hide();
    $("#lSD").show();
    $("#lED").show();
    $("#btnAdd").hide();
    $("#btnView").show();
    $("#aBD").hide();
    $("#lDB").show();
    $("#btnSave").hide();
    $("#btnEdit").show();
    $("#btnDelete").show();
    $("#btnApplyDays").hide();
    $("#btnSave").hide();
}
function onClickApplyDays(){
    var count = getSelectedRows();
    if(count>0){
        $("#divSecond").show();
    }
}

function onClickViewList(){
    if($("#txtStartDateL").val() && $("#txtEndDateL").val()){
        buildItemGridL([]);
        var sDT =  $("#txtStartDateL").val();
        sDT = new Date(sDT);
        var day = sDT.getDate();
        var month = sDT.getMonth();
        month = month+1;
        var year = sDT.getFullYear();

        var stDate = month+"/"+day+"/"+year;
        stDate = stDate+" 00:00:00";

        var startDate = new Date(stDate);
        var stDateTime = startDate.getTime();

        var eDT = $("#txtEndDateL").val();
        eDT = new Date(eDT);

        var day = eDT.getDate();
        var month = eDT.getMonth();
        month = month+1;
        var year = eDT.getFullYear();
        var etDate = month+"/"+day+"/"+year;
        etDate = etDate+" 23:59:59";

        var endtDate = new Date(etDate);
        var edDateTime = endtDate.getTime();
        //edDateTime = getGMTDateFromLocaleDate(edDateTime);//parentRef.providerId
        var cmbDoctor = $("#cmbDoctor").data("kendoComboBox");
        var patientListURL = ipAddress+"/appointment-block/list/?provider-id="+cmbDoctor.value()+"&from-date="+stDateTime+"&to-date="+edDateTime;
        getAjaxObject(patientListURL,"GET",onPatientBlockDays,onError);
    }else{
        customAlert.error("Error", "select dates");
    }
}
function onPatientBlockDays(dataObj){
    console.log(dataObj);
    var blArray = [];
    if(dataObj.response.appointmentBlock){
        if($.isArray(dataObj.response.appointmentBlock)){
            blArray = dataObj.response.appointmentBlock;
        }else{
            blArray.push(dataObj.response.appointmentBlock);
        }
    }
    var arr = [];
    for(var i=0;i<blArray.length;i++){
        var bItem = blArray[i];
        if(bItem){
            var obj = {};
            obj.SD = kendo.toString(new Date(bItem.startDateTime),"MM/dd/yyyy");
            obj.ST = formatAMPM(new Date(bItem.startDateTime));
            obj.ET = formatAMPM(new Date(bItem.endDateTime));
            obj.idk = bItem.id;
            arr.push(obj);
        }
    }
    buildItemGridL(arr);
}
function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}

function onClickEditBlockDays(){
    var selectItem = null;
    var selArray = [];
    var selGridData = angularUIgridWrapperL.getAllRows();
    for(var i=0;i<selGridData.length;i++){
        var selItem = selGridData[i];
        if(selItem){
            var item = selItem.entity;
            if(item && item.SEL){
                //selectItem = item;
                //break;
                selArray.push(item);
            }
        }
    }
    if(selArray && selArray.length>0){
        parentRef.selArray = selArray;
        var popW = "50%";
        var popH = "40%";
        var profileLbl;
        var devModelWindowWrapper = new kendoWindowWrapper();
        profileLbl = "Update Appointment Block Timings";
        devModelWindowWrapper.openPageWindow("../../html/patients/appointmentBlockUpdate.html", profileLbl, popW, popH, true, closeBlockAddAction);
    }else{
        customAlert.error("Error", "Select row for edit");
    }

}
function closeBlockAddAction(evt,returnData){
    if(returnData.status == "success"){
        customAlert.error("Info", "Appointment block timings are updated");
        onClickViewList();
    }
}
function onClickDeleteBlockDays(){
    var selGridData = angularUIgridWrapperL.getAllRows();
    var selArray = [];
    for(var i=0;i<selGridData.length;i++){
        var selItem = selGridData[i];
        if(selItem){
            var item = selItem.entity;
            if(item && item.SEL){
                item.id = item.idk;
                item.modifiedBy = 101;
                item.isActive = 0;
                item.isDeleted = 1;
                selArray.push(item);
            }
        }
    }
    if(selArray && selArray.length>0){
        customAlert.confirm("Confirm", "Are you sure you want delete?",function(response){
            if(response.button == "Yes"){
                dataUrl = ipAddress+"/appointment-block/delete";
                createAjaxObject(dataUrl,selArray,"POST",onDelete,onError);
                //console.log(selGridData);
                /*for(var i=0;i<selGridData.length;i++){
                    var selItem = selGridData[i];
                    if(selItem){
                        var item = selItem.entity;
                        if(item && item.SEL){
                            var reqObj = {};
                            reqObj.id = Number(item.idk);
                            reqObj.modifiedBy = 101;
                            reqObj.isActive = 0;
                            reqObj.isDeleted = 1;
                            dataUrl = ipAddress+"/appointment-block/delete";
                            createAjaxObject(dataUrl,reqObj,"POST",onDelete,onError);
                        }
                    }
                }*/
            }
        });
    }
}
function onDelete(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == 1){
        customAlert.error("Info", "Appointment block day deleted successfully");
        onClickViewList();
    }else{
        customAlert.error("error", "error");
    }
}
function onClickAddDays(){

}
function onClickBack(){
    $("#divSecond").hide();
    $("#divStart").show();
    $("#btnSave").show();
    $("#btnApplyDays").show();
    $("#btnBack").hide();
}

function getSelectedRows(){
    var rowCount = 0;
    var selGridData = angularUIgridWrapper.getAllRows();
    for (var j = 0; j < selGridData.length; j++) {
        var dataRow = selGridData[j].entity;
        if(dataRow.SEL){
            rowCount = rowCount+1;
        }
    }
    return rowCount;
}
function getSelectedRows1(){
    var rowCount = 0;
    var selGridData = angularUIgridWrapperL.getAllRows();
    for (var j = 0; j < selGridData.length; j++) {
        var dataRow = selGridData[j].entity;
        if(dataRow.SEL){
            rowCount = rowCount+1;
        }
    }
    return rowCount;
}
function onClickSave(){

    var txtStartDate1 = $("#txtStartDate1").val();
    var txtEndDate1 = $("#txtEndDate1").val();

    if(txtStartDate1 && txtEndDate1){
        var selGridData = angularUIgridWrapper.getAllRows();
        console.log(selGridData);
        var len = selGridData.length;
        var stDate = new Date(txtStartDate1);
        var etDate = new Date(txtEndDate1);

        var ist = stDate.getDate();
        var iet = etDate.getDate();
        var diff = iet-ist;
        var selarr = [];
        for(var i=0;i<=diff;i++){
            var dt = new Date(txtStartDate1);
            dt = dt.getTime()+(86400000*i);
            var sdt = new Date(st);
            sdt = kendo.toString(new Date(dt),"MM/dd/yyyy");
            for (var j = 0; j < selGridData.length; j++) {
                var dataRow = selGridData[j].entity;
                if(dataRow.SEL){
                    //console.log(dataRow);
                    var sd = sdt;
                    var st = dataRow.ST;
                    var et = dataRow.ET;
                    var obj = {};
                    obj.SD = sd;
                    obj.ST = st;
                    obj.ET = et;
                    selarr.push(obj);
                    //	angularUIgridWrapper.insert(obj);
                }
            }
        }

        var arr = [];
        //onClickAddDays();
        var selGridData = angularUIgridWrapper.getAllRows();
        for (var j = 0; j < selGridData.length; j++) {
            var dataRow = selGridData[j].entity;
            var obj = {};
            obj.createdBy = 101;
            obj.isActive = 1;
            obj.isDeleted = 0;
            var sd = dataRow.SD;
            var st = dataRow.ST;
            var et = dataRow.ET;

            var sdt = sd+" "+st;
            var edt = sd+" "+et;

            var sDate = new Date(sdt);
            var eDate = new Date(edt);
            var cmbDoctor = $("#cmbDoctor").data("kendoComboBox");
            obj.startDateTime = sDate.getTime();
            obj.endDateTime = eDate.getTime();
            obj.providerId = cmbDoctor.value();//parentRef.providerId;
            obj.facilityId = parentRef.facilityId;

            //arr.push(obj);
        }
        for (var k = 0; k < selarr.length; k++) {
            var dataRow = selarr[k];
            var obj = {};
            obj.createdBy = 101;
            obj.isActive = 1;
            obj.isDeleted = 0;
            var sd = dataRow.SD;
            var st = dataRow.ST;
            var et = dataRow.ET;

            var sdt = sd+" "+st;
            var edt = sd+" "+et;

            var sDate = new Date(sdt);
            var eDate = new Date(edt);

            console.log(sDate+","+eDate);
            var satFlag = $("#chkSat").is(":checked");
            var sunFlag = $("#chkSun").is(":checked");

            if(!satFlag){
                var sDay = sDate.getDay();
                if(sDay == 6){
                    continue;
                }
            }
            if(!sunFlag){
                var sDay = sDate.getDay();
                if(sDay == 0){
                    continue;
                }
            }
            var cmbDoctor = $("#cmbDoctor").data("kendoComboBox");
            obj.startDateTime = sDate.getTime();
            obj.endDateTime = eDate.getTime();
            obj.providerId = cmbDoctor.value();//parentRef.providerId;
            obj.facilityId = parentRef.facilityId;

            arr.push(obj);
        }
        console.log(arr);
        var dataUrl = "";
        dataUrl = ipAddress+"/appointment-block/create";
        createAjaxObject(dataUrl,arr,"POST",onCreate,onError);
    }else{
        customAlert.error("Error", "Select Proper Dates");
    }

}
function onCreate(dataObj){
    console.log(dataObj);
    if(dataObj.response.status.code == "1"){
        customAlert.error("Info", "Appointment blocked timings saved successfully");
        setTimeout(function(){
            popupClose("success");
        },2000);
    }else{
        customAlert.error("Error", "Appointment blocked created failure");
    }
}
function onClickCancel(){
    popupClose("false");
}
function popupClose(st){
    var onCloseData = new Object();
    onCloseData.status = st;
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(onCloseData);
}

function onError(errorObj){
    console.log(errorObj);
}
var angularUIgridWrapper;
var parentRef = null;
var operation = "";
var selItem = null;
var ADD = "add";
var UPDATE = "update";
var VIEW = "view";
var DELETE = "delete";
var facilityId = "";
var patientInfoObject = null;
var commId = "";
var statusArr = [{Key:'Active',Value:'Active'},{Key:'InActive',Value:'InActive'}];
var filterArr = [{ Key: '', Value: 'All' }, { Key: 'id', Value: 'Facility ID' }, { Key: 'abbr', Value: 'Abbreviation' }, { Key: 'name', Value: 'Name' }, { Key: 'display-name', Value: 'Display Name' }, { Key: 'billing-account-id', Value: 'Billing Account Id' }];
var IsPostalCodeManual = sessionStorage.IsPostalCodeManual;
var cntry = sessionStorage.countryName;

$(document).ready(function(){
    // $("#pnlPatient",parent.document).css("display","none");
    // themeAPIChange();
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("dgridWeekNumberList", dataOptions);
    angularUIgridWrapper.init();
    buildWeekNumberListGrid([]);
    // getCountryZoneName();
});

var apptArr = [];
$(window).load(function(){

    parentRef = parent.frames['iframe'].window;
    loading = false;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }


    if(parentRef.pid){
        var patientRosterURL = ipAddress+"/patient/roster/list/?patient-id="+parentRef.pid+"&is-active=1";
    }

    getAjaxObject(patientRosterURL,"GET",onGetRosterList,onErrorMedication);


    //apptArr = getDateArray(parentRef.sttime,parentRef.edtime);


    //buildWeekNumberListGrid(apptArr);



});


function onGetRosterList(dataObj){
    // console.log(dataObj);
    buildWeekNumberListGrid([]);
    var rosterArray = [];
    if(dataObj && dataObj.response &&  dataObj.response.status && dataObj.response.status.code == 1){
        if($.isArray(dataObj.response.patientRoster)){
            rosterArray = dataObj.response.patientRoster;
        }else{
            rosterArray.push(dataObj.response.patientRoster);
        }
    }



    var arr = [], dt = new Date(parentRef.sttime);



    var enddate = new Date(parentRef.edtime)

    while (dt <= enddate) {

        var obj = {};
        obj.apptDate = formatDate(dt); //kendo.toString(new Date(dt), "dd-MM-yyyy");

        dt.setHours(0, 0, 0, 0);

        obj.dateOfAppointment = dt.getTime();
        obj.weekNumber = dt.getWeekNo();
        obj.dayOfWeek = dt.getDay();

        arr.push(obj);
        dt.setDate(dt.getDate() + 1);
    }




    if(rosterArray !== null && rosterArray.length > 0 && arr !== null && arr.length > 0){


        for(var c = 0 ; c < arr.length ; c++){

            if(rosterArray !== null && rosterArray.length > 0){

                var tmpArr = rosterArray.filter(function(item){
                    return item.weekId === arr[c].weekNumber && item.weekDayStart === arr[c].dayOfWeek;
                });

                if(tmpArr !== null && tmpArr.length > 0){
                    apptArr.push(arr[c]);

                    for(var i = 0; i < tmpArr.length ; i++){
                        var index = rosterArray.findIndex(x => x.id === tmpArr[i].id);
                        if(index > -1){
                            rosterArray.splice(index, 1);
                        }
                    }
                }
            }
            else{
                break;
            }

        }



        // for(var i=0;i<rosterArray.length;i++){

        // 	var obj = {};

        // 	rosterArray[i].idk = rosterArray[i].id;

        // 	var dt = new Date(rosterArray[i].dateOfAppointment);

        // 	obj.idk = rosterArray[i].id;
        // 	obj.apptDateOriginal = rosterArray[i].dateOfAppointment;
        // 	obj.apptDate = formatDate(dt);
        // 	obj.weekNumber = dt.getWeekNo();
        // 	obj.dayOfWeek = dt.getDayOfWeek();
        // 	apptArr.push(obj);


        // }

        if(apptArr !== null && apptArr.length > 0){
            buildWeekNumberListGrid(apptArr);
        }
    }

}

function onErrorMedication(errobj){
    console.log(errobj);
}

var getDateArray = function(start, end) {

    var arr = [],
        dt = new Date(start);


    var enddate = new Date(end)

    while (dt <= enddate) {

        var obj = {};
        obj.apptDate = formatDate(dt); //kendo.toString(new Date(dt), "dd-MM-yyyy");
        obj.weekNumber = dt.getWeekNo();

        arr.push(obj);
        dt.setDate(dt.getDate() + 1);
    }

    return arr;

}


function onLoaded(){
    $("#btnEdit").prop("disabled", true);
    $("#btnDelete").prop("disabled", true);
    init();
    buttonEvents();
    adjustHeight();
    $('.btnActive').trigger('click');
}

function init(){

    buildWeekNumberListGrid([]);

}
function onError(errorObj){
    console.log(errorObj);
}
function getFacilityList(dataObj){
    console.log(dataObj);
    var dataArray = [];
    if(dataObj){
        if($.isArray(dataObj.response.facility)){
            dataArray = dataObj.response.facility;
        }else{
            dataArray.push(dataObj.response.facility);
        }
    }
    var tempDataArry = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
            var createdDate = new Date(dataArray[i].createdDate);
            var createdDateString = createdDate.toLocaleDateString();
            var modifiedDate = new Date(dataArray[i].modifiedDate);
            var modifiedDateString = modifiedDate.toLocaleDateString();
            dataArray[i].createdDateString = createdDateString;
            dataArray[i].modifiedDateString = modifiedDateString;
        }
    }
    if(dataArray.length == 0 && dataObj.response.status.code == "0") {
        // $('.customAlertFacility').append('<div class="alert alert-danger">'+dataObj.response.status.message+'</div>');
        customAlert.error("Error",dataObj.response.status.message);
    }
    buildWeekNumberListGrid(dataArray);
}
function buttonEvents(){
    $("#btnEdit").off("click",onClickOK);
    $("#btnEdit").on("click",onClickOK);

    $("#btnUpdateWeekNo").off("click",onClickUpdateWeekNo);
    $("#btnUpdateWeekNo").on("click",onClickUpdateWeekNo);


    $("#btnGO").off("click",onClickGO);
    $("#btnGO").on("click",onClickGO);


    $("#btnDelete").off("click",onClickDelete);
    $("#btnDelete").on("click",onClickDelete);

    $("#btnAdd").off("click");
    $("#btnAdd").on("click",onClickChangeWeekNumber);

    $("#btnCancel").off("click",onClickCancel);
    $("#btnCancel").on("click",onClickCancel);

    $("#btnSave").off("click",onClickSave);
    $("#btnSave").on("click",onClickSave);

    $("#btnSearch").off("click",onClickSearch);
    $("#btnSearch").on("click",onClickSearch);

    $("#btnCitySearch").off("click");
    $("#btnCitySearch").on("click", onClickZipSearch);

    $("#btnReset").off("click",onClickReset);
    $("#btnReset").on("click",onClickReset);

    $("#btnZipSearch").off("click");
    $("#btnZipSearch").on("click",onClickZipSearch);

    $("#AddbtnZipSearch").off("click");
    $("#AddbtnZipSearch").on("click", onClickAdditionalZipSearch);

    $("#AddbtnCitySearch").off("click");
    $("#AddbtnCitySearch").on("click", onClickAdditionalZipSearch);

    $("#facilityFilter-btn").off("click");
    $("#facilityFilter-btn").on("click", onClickFilterBtn);

    $(".popupClose").off("click", onClickCancel);
    $(".popupClose").on("click", onClickCancel);

    $('#tabsUL .boxtablink').on('click', function() {
        $(this).closest('li').addClass('active').siblings('li').removeClass('active');
        // $('.tab-content').hide();
        showTab = $(this).attr('data-toggle');
        // $('#' + showTab).show();
        $('.alert').remove();
        if (showTab == "Detailstab") {
            $("#tab1").addClass("tabContent-active");
            $("#tab2").removeClass("tabContent-active");
        }
        else {
            $("#tab2").addClass("tabContent-active");
            $("#tab1").removeClass("tabContent-active");
        }
    });

    $(".btnActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('active');
        onClickActive();
    });
    $(".btnInActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('inactive');
        onClickInActive();
    });

    $("#txtWPhone").on('keyup', function(e) {
        if($(this).val().length > 0) {
            $("#txtWExtension").removeAttr("disabled");
        }
        else {
            $("#txtWExtension").attr("disabled","disabled");
        }
    });
    $("#txtHPhone").on('keyup', function(e) {
        if($(this).val().length > 0) {
            $("#txtExtension").removeAttr("disabled");
        }
        else {
            $("#txtExtension").attr("disabled","disabled");
        }
    });
    $("#txtHPhone1").on('keyup', function(e) {
        if($(this).val().length > 0) {
            $("#txtExtension1").removeAttr("disabled");
        }
        else {
            $("#txtExtension1").attr("disabled","disabled");
        }
    });
    $("#cmbFilterByValue").on('keyup', function(e) {
        if($(this).val().length > 0) {
            $("#facilityFilter-btn").removeAttr("disabled");
        }
        else {
            $("#facilityFilter-btn").attr("disabled","disabled");
        }
    });
    $("#txtCell").on('keyup', function(e) {
        if($(this).val().length > 0) {
            $("#cmbSMS").closest('.k-dropdown-wrap').removeClass('k-state-disabled').find('.k-input').removeAttr("disabled");
        }
        else {
            $("#cmbSMS").closest('.k-dropdown-wrap').addClass('k-state-disabled').find('.k-input').attr("disabled","disabled");
        }
    });
    $("#txtCell1").on('keyup', function(e) {
        if($(this).val().length > 0) {
            $("#AddcmbSMS").closest('.k-dropdown-wrap').removeClass('k-state-disabled').find('.k-input').removeAttr("disabled");
        }
        else {
            $("#AddcmbSMS").closest('.k-dropdown-wrap').addClass('k-state-disabled').find('.k-input').attr("disabled","disabled");
        }
    });
}
function searchOnLoad(status) {
    buildWeekNumberListGrid([]);
    if(status == "active") {
        var urlExtn = '/facility/list?is-active=1&is-deleted=0';
    }
    else if(status == "inactive") {
        var urlExtn = '/facility/list?is-active=0&is-deleted=1';
    }
    getAjaxObject(ipAddress+urlExtn,"GET",getFacilityList,onError);
}

function adjustHeight(){
    var defHeight = 240;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    angularUIgridWrapper.adjustGridHeight(cmpHeight);
}
function onClickActive() {
    $(".btnInActive").removeClass("radioButton-active");
    $(".btnActive").addClass("radioButton-active");
}
function onClickInActive() {
    $(".btnActive").removeClass("radioButton-active");
    $(".btnInActive").addClass("radioButton-active");
}
function onClickFilterBtn(e) {
    e.preventDefault();
    $('.alert').remove();
    var filterName = $("#cmbFilterByName").val();
    var filterValue = $("#cmbFilterByValue").val();
    buildWeekNumberListGrid([]);
    /*if($(".btnActive").hasClass("selectButtonBarClass")) {
        var urlExtn = '/billing-account/list?is-active=1&is-deleted=0';
    }
    else {
        var urlExtn = '/billing-account/list?is-active=0&is-deleted=1';
    }*/
    getAjaxObject(ipAddress+"/facility/list?is-active=1&is-deleted=0&"+filterName+"="+filterValue,"GET",getFacilityList,onError);
}

function buildWeekNumberListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;

    gridColumns.push({
        "title": "Date",
        "field": "apptDate",

    });
    gridColumns.push({
        "title": "Week Number",
        "field": "weekNumber",

    });

    gridColumns.push({
        "title": "Day Of Week",
        "field": "dayOfWeek",

    });






    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();

}
var prevSelectedItem =[];

function getCountryZoneName() {
    var countryName = sessionStorage.countryName;
    if (countryName.indexOf("India") >= 0) {
        $('.postalCode').html('Postal Code :<span class="mandatoryField">*</span>');
        $('.postalCodeAdditional').html('Postal Code');
        $('.stateLabel').text("State");
        $('.zipFourWrapper').addClass('hideZipFourWrapper');
    } else if (countryName.indexOf("United Kingdom")) {
        if (IsPostalCodeManual == "1") {
            $("#btnCitySearch").css("display", "");
            $("#AddbtnCitySearch").css("display", "");
            $("#lblCity").html("City" + "<span class='mandatoryClass'>*</span>");
            $("#lblAdditionalCity").html("City");
            $("#cmbZip").attr("readonly", false);
            $("#AddcmbZip").attr("readonly", false);
            $("#btnZipSearch").css("display", "none");
            $("#AddbtnZipSearch").css("display", "none");
            $(".postalCode").html("Postal Code : ");
            $(".postalCodeAdditional").html("Postal Code : ");
            $('.stateLabel').text("County");
            $('.zipFourWrapper').addClass('hideZipFourWrapper');

        }else{
            $('.postalCode').html('Postal Code :<span class="mandatoryField">*</span>');
            $('.postalCodeAdditional').html('Postal Code: <span class="mandatoryField">*</span>');
            $('.stateLabel').text("County");
            $('.zipFourWrapper').addClass('hideZipFourWrapper');
        }
    } else {
        $('.postalCode').html('Zip :<span class="mandatoryField">*</span>');
        $('.postalCodeAdditional').html('Zip');
        $('.stateLabel').text("State");
        $('.zipFourWrapper').removeClass('hideZipFourWrapper');
    }

    /*$.getJSON('//freegeoip.net/json/?callback=', function(dataObj) {
        var dataUrl = ipAddress + "/user/location";
        console.log(dataObj);
        sessionStorage.country = dataObj.country_code;
        dataObj.createdBy = sessionStorage.userId;
        dataObj.countryCode = dataObj.country_code;
        dataObj.countryName = dataObj.country_name;
        dataObj.regionCode = dataObj.region_code;
        dataObj.regionName = dataObj.region_name;
        dataObj.timeZone = dataObj.time_zone;
        dataObj.metroCode = dataObj.metro_code;
        dataObj.zipCode = dataObj.zip_code;
        sessionStorage.countryName = dataObj.country_name;
        var countryName = sessionStorage.countryName;
        if (countryName.indexOf("India") >= 0) {
            $('.postalCode').html('Postal Code :<span class="mandatoryField">*</span>');
            $('.postalCodeAdditional').html('Postal Code');
            $('.stateLabel').text("State");
            $('.zipFourWrapper').addClass('hideZipFourWrapper');
        } else if (countryName.indexOf("United Kingdom")) {
            $('.postalCode').html('Postal Code :<span class="mandatoryField">*</span>');
            $('.postalCodeAdditional').html('Postal Code');
            $('.stateLabel').text("County");
            $('.zipFourWrapper').addClass('hideZipFourWrapper');
        } else {
        	$('.postalCode').html('Zip :<span class="mandatoryField">*</span>');
        	$('.postalCodeAdditional').html('Zip');
            $('.stateLabel').text("State");
            $('.zipFourWrapper').removeClass('hideZipFourWrapper');
        }
    });*/
}
function onChange(){
    setTimeout(function(){
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        if(selectedItems && selectedItems.length>0){
            selItem = selectedItems[0];
            //  $("#btnEdit").prop("disabled", false);
            //  $("#btnDelete").prop("disabled", false);

            $("#btnAdd").prop("disabled", false);
        }else{
            //  $("#btnEdit").prop("disabled", true);
            //  $("#btnDelete").prop("disabled", true);
            $("#btnAdd").prop("disabled", true);
        }
    },100)
}

function onClickGO(){



    var popW = "90%";
    var popH = "80%";

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();

    parentRef.apptArr = apptArr;

    profileLbl = "Appointments";
    devModelWindowWrapper.openPageWindow("../../html/patients/createRosterAppCompare.html", profileLbl, popW, popH, true, closePtRAddAction);
    //parent.setPopupWIndowHeaderColor1("0bc56f","FFF");

}

function closePtRAddAction(evt,returnData){

}

function onClickUpdateWeekNo() {
    var weekno = parseInt($("#cmbWeekNumber option:selected").val());

    var dow = "";
    var selDate;
    if (selItem !== null && selItem.dayOfWeek) {
        dow = selItem.dayOfWeek;

        var dateArr = selItem.split(' ');

        var y = dateArr[0];
        var m = months.findIndex(x => x === dateArr[1]);
        var d = dateArr[2];

        selDate = new Date(y,m,d);

    }

    if (apptArr !== null && apptArr.length > 0) {

        var tmpappt = apptArr.filter(function (item) {
            return item.weekNumber === weekno;// && dow === item.dayOfWeek;
        })

        if (tmpappt !== null && tmpappt.length > 0) {
            customAlert.info("info", "Already Roster is created.");
        }
        else{
            customAlert.confirm("Confirm", "Are you sure want to update?",function(response){
                if(response.button == "Yes"){
                    for (var c = 0; c < tmpappt.length; c++) {
                        const index = apptArr.findIndex((e) => e.id === tmpappt[c].id);

                        if (index > 0) {

                            var reviseddate = getNewDate(selDate,weekno,tmpappt[c].dayOfWeek);

                            apptArr[index].apptDate = formatDate(reviseddate);
                            apptArr[index].dateOfAppointment = reviseddate.getTime();
                            apptArr[index].weekNumber = reviseddate.getWeekNo();
                            apptArr[index].dayOfWeek = reviseddate.getDay();

                        }
                    }
                }
            });

        }
    }

    buildWeekNumberListGrid(apptArr);

    $("#viewDivBlock").show();
    $("#changeWeekBlock").hide();

    $("#cmbWeekNumber").val("1");
}




Date.prototype.addDays = function(days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
}

function getNewDate(seldate,weekno,dow){


    var firstDayOfWeek = new Date(seldate.getFullYear(), seldate.getMonth(), 1);

    var diffDays = firstDayOfWeek.getDate() - firstDayOfWeek.getDay();

    var firstDayInWeek = new Date(firstDayOfWeek.setDate(diffDays));

    if(weekno === 1)
        weekno = 0;

    var daysToAdd = (weekno * 7) + dow;

    var finalDate = firstDayInWeek.addDays(daysToAdd);

    return finalDate;

}


function onClickOK(){
    setTimeout(function(){
        $("#txtID").show();
        parentRef.operation = "edit";
        $("#viewDivBlock").hide();
        $("#addTaskGroupAccountPopup").show();
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if(selectedItems && selectedItems.length>0){
            var obj = {};
            obj.selItem = selectedItems[0];
            selAccountItem = selectedItems[0];
            addFacility("update");
            // var onCloseData = new Object();
            /*obj.status = "success";
            obj.operation = "ok";
               var windowWrapper = new kendoWindowWrapper();
               windowWrapper.closePageWindow(obj);*/
        }
    })
}
function onClickDelete(){
    customAlert.confirm("Confirm", "Are you sure you want delete?",function(response){
        if(response.button == "Yes"){
            var selectedItems = angularUIgridWrapper.getSelectedRows();
            console.log(selectedItems);
            if(selectedItems && selectedItems.length>0){
                var selItem = selectedItems[0];
                if(selItem){
                    var dataUrl = ipAddress+"/facility/delete/";
                    var reqObj = {};
                    reqObj.id = selItem.idk;
                    reqObj.abbr = selItem.abbr;
                    reqObj.isDeleted = "1";
                    reqObj.isActive = "0";
                    reqObj.modifiedBy = sessionStorage.userId;
                    createAjaxObject(dataUrl,reqObj,"POST",onDeleteCountryt,onError);
                }
            }
        }
    });
}
function onDeleteCountryt(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            customAlert.info("info", "Facility Deleted Successfully");
            /*buildWeekNumberListGrid([]);
            getAjaxObject(ipAddress + "/facility/list/", "GET", getFacilityList, onError);*/
            init();
        }else{
            customAlert.error("error", dataObj.message);
        }
    }
}
function onClickChangeWeekNumber(){

    $("#changeWeekBlock").show();
    $('#viewDivBlock').hide();


}
var selAccountItem = null;
function addFacility(opr){
    var popW = 850;
    var popH = 550;

    /*var profileLbl = "Add Facility";
    var devModelWindowWrapper = new kendoWindowWrapper();*/
    $('.listDataWrapper').hide();
    $('.addOrRemoveWrapper').show();
    $('.alert').remove();

    parentRef.operation = opr;
    parentRef.selItem = selAccountItem;
    //facilityId = selAccountItem.idk;
    if (opr == "add") {
        $('#btnSave').html('Save');
        $('.tabContentTitle').text('Add Facility');
        $('#btnReset').show();
        $('#btnReset').trigger('click');
        /*profileLbl = "Add Billing Account";
        parentRef.selItem = null;*/
    } else {
        /*profileLbl = "Edit Billing Account";
        parentRef.selItem = selAccountItem;*/
        $('#btnReset').trigger('click');
        $('.tabContentTitle').text('Edit Facility');
        $('#btnReset').hide();
        $('#btnSave').html('Update');
        console.log(selAccountItem);
        var communicationLen = selAccountItem.communications != null ? selAccountItem.communications.length : 0;
        var cityIndexVal = "";
        var dupCityIndexVal = "";
        for(var i=0; i<communicationLen; i++) {
            if(selAccountItem.communications[i].parentTypeId == 400) {
                cityIndexVal = i;
            }
            if(selAccountItem.communications[i].parentTypeId == 401) {
                dupCityIndexVal = i;
            }
        }
        if(cityIndexVal != "" || (cityIndexVal == 0 && selAccountItem.communications != null)) {
            $("#cityIdHiddenValue").val(selAccountItem.communications[cityIndexVal].cityId);
            $("#txtCountry").val(selAccountItem.communications[cityIndexVal].country);
            if (cntry.indexOf("United Kingdom") >= 0) {
                if(IsPostalCodeManual == "1"){
                    $("#cmbZip").val(selAccountItem.communications[cityIndexVal].houseNumber);
                }
                else{
                    $("#cmbZip").val(selAccountItem.communications[cityIndexVal].zip);
                }
            }
            else{
                $("#cmbZip").val(selAccountItem.communications[cityIndexVal].zip);
            }

            $("#txtZip4").val(selAccountItem.communications[cityIndexVal].zipFour);
            $("#txtState").val(selAccountItem.communications[cityIndexVal].state);
            $("#txtCity").val(selAccountItem.communications[cityIndexVal].city);
            $("#txtHPhone1").val(selAccountItem.communications[cityIndexVal].homePhone);
            $("#txtAdd1").val(selAccountItem.communications[cityIndexVal].address1);
            $("#txtAdd2").val(selAccountItem.communications[cityIndexVal].address2);
            $("#txtWPhone").val(selAccountItem.communications[cityIndexVal].workPhone);
            $("#txtWExtension").val(selAccountItem.communications[cityIndexVal].workPhoneExt);
            $("#txtFax").val(selAccountItem.communications[cityIndexVal].fax);
            $("#txtCell").val(selAccountItem.communications[cityIndexVal].cellPhone);
            getComboListIndex("cmbSMS", "desc", selAccountItem.communications[cityIndexVal].sms);
            $("#txtEmail").val(selAccountItem.communications[cityIndexVal].email);
            getComboListIndex("cmbZip", "idk", selAccountItem.communications[cityIndexVal].cityId);
            zipSelItem = selAccountItem.communications[cityIndexVal];
            zipSelItem.idk = selAccountItem.communications[cityIndexVal].id;
            //zipSelItem.cityId = selAccountItem.communications[cityIndexVal].cityId;
            //getAjaxObject(ipAddress+"/city/list/?id="+selAccountItem.communications[cityIndexVal].cityId,"GET",onCityList,onError);
        }
        if(dupCityIndexVal != "") {
            $("#AddcityIdHiddenValue").val(selAccountItem.communications[dupCityIndexVal].cityId);
            $("#AddtxtCountry").val(selAccountItem.communications[dupCityIndexVal].country);
            $("#txtCountry").val(selAccountItem.communications[cityIndexVal].country);
            if (cntry.indexOf("United Kingdom") >= 0) {
                if(IsPostalCodeManual == "1"){
                    $("#AddcmbZip").val(selAccountItem.communications[dupCityIndexVal].houseNumber);
                }
                else{
                    $("#AddcmbZip").val(selAccountItem.communications[dupCityIndexVal].zip);
                }
            }
            else{
                $("#AddcmbZip").val(selAccountItem.communications[dupCityIndexVal].zip);
            }

            $("#AddtxtZip4").val(selAccountItem.communications[dupCityIndexVal].zipFour);
            $("#AddtxtState").val(selAccountItem.communications[dupCityIndexVal].state);
            $("#AddtxtCity").val(selAccountItem.communications[dupCityIndexVal].city);
            AddzipSelItem = selAccountItem.communications[dupCityIndexVal];
            AddzipSelItem.idk = selAccountItem.communications[dupCityIndexVal].id;
            //AddzipSelItem.cityId = selAccountItem.communications[dupCityIndexVal].cityId;
            if(selAccountItem.communications[dupCityIndexVal].homePhoneExt) {
                $("#txtExtension1").val(selAccountItem.communications[dupCityIndexVal].homePhoneExt);
            }
            if(selAccountItem.communications[dupCityIndexVal].homePhone) {
                $("#txtHPhone1").val(selAccountItem.communications[dupCityIndexVal].homePhone);
            }
            if(selAccountItem.communications[dupCityIndexVal].address1) {
                $("#AddtxtAdd1").val(selAccountItem.communications[dupCityIndexVal].address1);
            }
            if(selAccountItem.communications[dupCityIndexVal].address2) {
                $("#AddtxtAdd2").val(selAccountItem.communications[dupCityIndexVal].address2);
            }
            if(selAccountItem.communications[dupCityIndexVal].cellPhone) {
                $("#txtCell1").val(selAccountItem.communications[dupCityIndexVal].cellPhone);
            }
            if(selAccountItem.communications[dupCityIndexVal].sms) {
                //$("#AddcmbSMS").val(selAccountItem.communications[dupCityIndexVal].sms);
                getComboListIndex("cmbSMS", "desc", selAccountItem.communications[dupCityIndexVal].sms);
            }
            if(selAccountItem.communications[dupCityIndexVal].email) {
                $("#txtEmail1").val(selAccountItem.communications[dupCityIndexVal].email);
            }
            //getAjaxObject(ipAddress+"/city/list/?id="+selAccountItem.communications[dupCityIndexVal].cityId,"GET",onAdditionalCityList,onError);
        }
        $("#txtID").html("ID :"+selAccountItem.idk);
        $("#txtExtID1").val(selAccountItem.externalId1);
        $("#txtExtID2").val(selAccountItem.externalId2);
        getComboListIndex("cmbPrefix", "value", selAccountItem.contactPrefix);
        getComboListIndex("cmbSuffix", "value", selAccountItem.contactSuffix);
        $("#txtFN").val(selAccountItem.contactFirstName);
        $("#txtLN").val(selAccountItem.contactLastName);
        $("#txtMN").val(selAccountItem.contactMiddleName);
        //$("#txtWeight").val(dataObj.response.patient.weight);
        //$("#txtHeight").val(dataObj.response.patient.height);
        $("#txtNN").val(selAccountItem.contactNickName);
        $("#txtAbbreviation").val(selAccountItem.abbreviation);
        $("#txtN").val(selAccountItem.name);
        $("#txtDN").val(selAccountItem.displayName);
        $("#txtFTI").val(selAccountItem.federalTaxId);
        $("#txtFTEIN").val(selAccountItem.federalTaxEin);
        $("#txtSTI").val(selAccountItem.stateTaxId);
        $("#txtNOTT").val(selAccountItem.taxName);
        $("#txtNPI").val(selAccountItem.npi);
        $("#txtTI").val(selAccountItem.taxonomyId);

        getComboListIndex("cmbFTIT", "desc", selAccountItem.federalTaxIdType);
        getComboListIndex("cmbStatus", "desc", selAccountItem.Status);
        /*getComboListIndex("cmbPrefix", "desc", selAccountItem.contactPrefix);*/

        //	$("#txtSMS").val(comObj.sms);
        getComboListIndex("cmbBilling", "idk", selAccountItem.billingAccountId);
        /*$("#txtCountry").val(selAccountItem.country);
        $("#cmbZip").val(selAccountItem.zip);
        $("#txtZip4").val(selAccountItem.zipFour);
        $("#txtState").val(selAccountItem.state);
        $("#txtCity").val(selAccountItem.city);*/
    }

    //devModelWindowWrapper.openPageWindow("../../html/masters/createFacility.html", profileLbl, popW, popH, true, closeAddFacilitytAction);
}
function closeAddFacilitytAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        var opr = returnData.operation;
        if(opr == "add"){
            customAlert.info("info", "Facility Created Successfully");
        }else{
            customAlert.info("info", "Facility Updated Successfully");
        }
    }
    // init();
}

function onClickCancel(){
    $("#viewDivBlock").show();
    $("#changeWeekBlock").hide();
}
function onClickClose() {
    $('.listDataWrapper').show();
    $('.addOrRemoveWrapper').hide();
    $("#btnEdit").prop("disabled", true);
    $("#btnDelete").prop("disabled", true);
    /*buildWeekNumberListGrid([]);
    getAjaxObject(ipAddress+"/facility/list?is-active=1&is-deleted=0","GET",getFacilityList,onError);*/
    $('.selectButtonBarClass').trigger('click');
}


function onClickZipSearch(){
    var popW = 600;
    var popH = 500;

    parentRef.searchZip = true;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search Zip";
    var cntry = sessionStorage.countryName;
    if(cntry.indexOf("India")>=0){
        profileLbl = "Search Postal Code";
    }else if(cntry.indexOf("United Kingdom")>=0){
        if(IsPostalCodeManual == "1"){
            profileLbl = "Search City";
        }
        else{
            profileLbl = "Search Postal Code";
        }
    }else if(cntry.indexOf("United State")>=0){
        profileLbl = "Search Zip";
    }else{
        profileLbl = "Search Zip";
    }
    devModelWindowWrapper.openPageWindow("../../html/masters/zipList.html", profileLbl, popW, popH, true, onCloseSearchZipAction);
}
function onClickAdditionalZipSearch() {
    var popW = 600;
    var popH = 500;

    parentRef.searchZip = true;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();

    if(IsPostalCodeManual == "1"){
        profileLbl = "Search City";
    }
    else{
        profileLbl = "Search Zip";
    }
    devModelWindowWrapper.openPageWindow("../../html/masters/zipList.html", profileLbl, popW, popH, true, onCloseSearchAdditionalZipAction);
}
var zipSelItem = null;
var AddzipSelItem = null;

function onCloseSearchZipAction(evt,returnData){
    console.log(returnData);
    if(returnData && returnData.status == "success"){
        //console.log();
        $("#cmbZip").val("");
        $("#txtZip4").val("");
        $("#txtState").val("");
        $("#txtCountry").val("");
        $("#txtCity").val("");
        var selItem = returnData.selItem;
        if(selItem){
            zipSelItem = selItem;
            if(zipSelItem){
                cityId = zipSelItem.idk;
                if (cntry.indexOf("United Kingdom") >= 0) {
                    if(IsPostalCodeManual != "1") {
                        if (zipSelItem.zip) {
                            $("#cmbZip").val(zipSelItem.zip);
                        }
                    }
                }
                else {
                    if (zipSelItem.zip) {
                        $("#cmbZip").val(zipSelItem.zip);
                    }
                }

                if(zipSelItem.zipFour){
                    $("#txtZip4").val(zipSelItem.zipFour);
                }
                if(zipSelItem.state){
                    $("#txtState").val(zipSelItem.state);
                }
                if(zipSelItem.country){
                    $("#txtCountry").val(zipSelItem.country);
                }
                if(zipSelItem.city){
                    $("#txtCity").val(zipSelItem.city);
                }
            }
        }
    }
}
function onCloseSearchAdditionalZipAction(evt, returnData) {
    console.log(returnData);
    if (returnData && returnData.status == "success") {
        //console.log();
        $("#AddcmbZip").val("");
        $("#AddtxtZip4").val("");
        $("#AddtxtState").val("");
        $("#AddtxtCountry").val("");
        $("#AddtxtCity").val("");
        var selItem = returnData.selItem;
        if (selItem) {
            AddzipSelItem = selItem;
            if (AddzipSelItem) {
                cityId = AddzipSelItem.idk;
                //AddzipSelItem.cityId = cityId;
                if (cntry.indexOf("United Kingdom") >= 0) {
                    if(IsPostalCodeManual != "1") {
                        if (AddzipSelItem.zip) {
                            $("#AddcmbZip").val(AddzipSelItem.zip);
                        }
                    }
                }
                else {
                    if (AddzipSelItem.zip) {
                        $("#AddcmbZip").val(AddzipSelItem.zip);
                    }
                }
                if (AddzipSelItem.zipFour) {
                    $("#AddtxtZip4").val(AddzipSelItem.zipFour);
                }
                if (AddzipSelItem.state) {
                    $("#AddtxtState").val(AddzipSelItem.state);
                }
                if (AddzipSelItem.country) {
                    $("#AddtxtCountry").val(AddzipSelItem.country);
                }
                if (AddzipSelItem.city) {
                    $("#AddtxtCity").val(AddzipSelItem.city);
                }
            }
        }
    }
}
function getZip(){
    getAjaxObject(ipAddress+"/city/list?is-active=1","GET",getZipList,onError);
}
function getZipList(dataObj){
    //var dArray = getTableListArray(dataObj);
    var dArray = [];
    if(dataObj && dataObj.response && dataObj.response.cityExt){
        if($.isArray(dataObj.response.cityext)){
            dArray = dataObj.response.cityext;
        }else{
            dArray.push(dataObj.response.cityext);
        }
    }
    if(dArray && dArray.length>0){
        //setDataForSelection(dArray, "cmbZip", onZipChange, ["zip", "id"], 0, "");
        //onZipChange();
    }
    getPrefix();

}

function getPrefix(){
    getAjaxObject(ipAddress+"/master/Prefix/list?is-active=1","GET",getCodeTableValueList,onError);
}
function onComboChange(cmbId){
    var cmb = $("#"+cmbId).data("kendoComboBox");
    if(cmb && cmb.selectedIndex<0){
        cmb.select(0);
    }
}
function onPrefixChange(){
    onComboChange("cmbPrefix");
}
function onSuffixChange() {
    onComboChange("cmbSuffix");
}
function onPtChange(){
    onComboChange("cmbStatus");
}
function onGenderChange(){
    onComboChange("cmbGender");
}
function onSMSChange(){
    onComboChange("cmbSMS");
}
function onAddSMSChange() {
    onComboChange("AddcmbSMS");
}
function onLanChange(){
    onComboChange("cmbLan");
}
function onRaceChange(){
    onComboChange("cmbRace");
}
function onEthnicityChange(){
    onComboChange("cmbEthicity");
}
function onZipChange(){
    onComboChange("cmbZip");
    var cmbZip = $("#cmbZip").data("kendoComboBox");
    if(cmbZip){
        var dataItem = cmbZip.dataItem();
        if(dataItem){
            $("#txtZip4").val(dataItem.zipFour);
            $("#txtCountry").val(dataItem.country);
            $("#txtState").val(dataItem.state);
            $("#txtCity").val(dataItem.city);
        }
    }
}
function getCodeTableValueList(dataObj){
    var dArray = getTableListArray(dataObj);
    if(dArray && dArray.length>0){
        setDataForSelection(dArray, "cmbPrefix", onPrefixChange, ["value", "idk"], 0, "");
    }
    getAjaxObject(ipAddress+"/master/Suffix/list?is-active=1","GET",getCodeTableSuffixValueList,onError);
}
function getCodeTableSuffixValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbSuffix", onSuffixChange, ["value", "idk"], 0, "");
    }
    getAjaxObject(ipAddress+"/master/Status/list?is-active=1","GET",getPatientStatusValueList,onError);
}

function getPatientStatusValueList(dataObj){
    var dArray = getTableFirstListArray(dataObj);
    if(dArray && dArray.length>0){
        setDataForSelection(dArray, "cmbStatus", onPtChange, ["desc", "idk"], 0, "");
    }
    //getAjaxObject(ipAddress+"/master/Gender/list/","GET",getGenderValueList,onError);
    getAjaxObject(ipAddress+"/billing-account/list?is-active=1","GET",getAccountList,onError);
}
/*function getGenderValueList(dataObj){
	var dArray = getTableListArray(dataObj);
	if(dArray && dArray.length>0){
		setDataForSelection(dArray, "cmbGender", onGenderChange, ["desc", "idk"], 0, "");
	}
	getAjaxObject(ipAddress+"/master/SMS/list/","GET",getSMSValueList,onError);
}*/
function getSMSValueList(dataObj){
    var dArray = getTableListArray(dataObj);
    if(dArray && dArray.length>0){
        setDataForSelection(dArray, "cmbSMS", onSMSChange, ["desc", "idk"], 0, "");
        setDataForSelection(dArray, "AddcmbSMS", onAddSMSChange, ["desc", "idk"], 0, "");
    }
    //getAjaxObject(ipAddress+"/master/Language/list/","GET",getLanguageValueList,onError);
}
/*function getLanguageValueList(dataObj){
	var dArray = getTableListArray(dataObj);
	if(dArray && dArray.length>0){
		setDataForSelection(dArray, "cmbLan", onLanChange, ["desc", "idk"], 0, "");
	}
	getAjaxObject(ipAddress+"/master/Race/list/","GET",getRaceValueList,onError);
}
function getRaceValueList(dataObj){
	var dArray = getTableListArray(dataObj);
	if(dArray && dArray.length>0){
		setDataForSelection(dArray, "cmbRace", onRaceChange, ["desc", "idk"], 0, "");
	}
	getAjaxObject(ipAddress+"/master/Ethnicity/list/","GET",getEthnicityValueList,onError);
}
function getEthnicityValueList(dataObj){
	var dArray = getTableListArray(dataObj);
	if(dArray && dArray.length>0){
		setDataForSelection(dArray, "cmbEthicity", onEthnicityChange, ["desc", "idk"], 0, "");
	}
	getAjaxObject(ipAddress+"/billing-account/list/","GET",getAccountList,onError);
}*/
function getAccountList(dataObj){
    var dataArray = [];
    if(dataObj && dataObj.response.status && dataObj.response.status.code == "1"){
        if($.isArray(dataObj.response.billingAccount)){
            dataArray = dataObj.response.billingAccount;
        }else{
            dataArray.push(dataObj.response.billingAccount);
        }
    }
    var tempDataArry = [];
    var obj = {};
    obj.name = "";
    obj.idk = "";
    tempDataArry.push(obj);
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
            var obj = dataArray[i];
            obj.idk = dataArray[i].id;
            obj.status = dataArray[i].Status;
            tempDataArry.push(obj);
        }
    }
    dataArray = tempDataArry;
    setDataForSelection(dataArray, "cmbBilling", onBillAccountChange, ["name", "idk"], 0, "");

    if(operation == UPDATE && facilityId != ""){
        getFacilityInfo();
    }
    getAjaxObject(ipAddress+"/master/SMS/list/","GET",getSMSValueList,onError);
}
function onBillAccountChange(){
    var cmbBilling = $("#cmbBilling").data("kendoComboBox");
    if(cmbBilling && cmbBilling.selectedIndex<0){
        cmbBilling.select(0);
    }
}
function getFacilityInfo(){
    getAjaxObject(ipAddress+"/facility/"+facilityId,"GET",onGetFacilityInfo,onError);
}
function getComboListIndex(cmbId,attr,attrVal){
    var cmb = $("#"+cmbId).data("kendoComboBox");
    if(cmb){
        var ds = cmb.dataSource;
        var totalRec = ds.total();
        for(var i=0;i<totalRec;i++){
            var dtItem = ds.at(i);
            if(dtItem && dtItem[attr] == attrVal){
                cmb.select(i);
                return i;
            }
        }
    }
    return -1;
}
function onGetFacilityInfo(dataObj){
    patientInfoObject = dataObj;
    if(dataObj && dataObj.response && dataObj.response.facility){
        $("#txtID").val(dataObj.response.facility.id);
        $("#txtExtID1").val(dataObj.response.facility.externalId1);
        $("#txtExtID2").val(dataObj.response.facility.externalId2);
        $("#txtAbbreviation").val(dataObj.response.facility.abbreviation);
        $("#txtN").val(dataObj.response.facility.name);
        $("#txtDN").val(dataObj.response.facility.displayName);
        $("#txtFTI").val(dataObj.response.facility.federalTaxId);
        $("#txtFTEIN").val(dataObj.response.facility.federalTaxEin);
        $("#txtSTI").val(dataObj.response.facility.stateTaxId);
        $("#txtNOTT").val(dataObj.response.facility.taxName);
        $("#txtNPI").val(dataObj.response.facility.npi);
        $("#txtTI").val(dataObj.response.facility.taxonomyId);

        getComboListIndex("cmbPrefix","value",dataObj.response.facility.contactPrefix);
        $("#txtFN").val(dataObj.response.facility.firstName);
        $("#txtLN").val(dataObj.response.facility.lastName);
        $("#txtMN").val(dataObj.response.facility.middleName);
        $("#txtWeight").val(dataObj.response.facility.weight);
        $("#txtHeight").val(dataObj.response.facility.height);
        $("#txtNN").val(dataObj.response.facility.nickname);
        /*var dt = new Date(dataObj.response.facility.dateOfBirth);
        if(dt){
            var strDT = kendo.toString(dt,"MM/dd/yyyy");
            var dtDOB = $("#dtDOB").data("kendoDatePicker");
            if(dtDOB){
                dtDOB.value(strDT);
            }
        }
        $("#txtSSN").val(dataObj.response.facility.ssn);*/

        getComboListIndex("cmbStatus","idk",dataObj.response.facility.isActive);
        getComboListIndex("cmbBilling","idk",dataObj.response.facility.billingAccountId);

        //getComboListIndex("cmbGender","desc",dataObj.response.facility.gender);
        //getComboListIndex("cmbEthicity","desc",dataObj.response.facility.ethnicity);
        //getComboListIndex("cmbRace","desc",dataObj.response.facility.race);
        //getComboListIndex("cmbLan","desc",dataObj.response.facility.language);

        if(dataObj && dataObj.response && dataObj.response.communication){

            var commArray = [];
            if($.isArray(dataObj.response.communication)){
                commArray = dataObj.response.communication;
            }else{
                commArray.push(dataObj.response.communication);
            }
            var comObj = commArray[0];
            commId = comObj.id;
            cityId = comObj.cityId;
            $("#txtCity").val(comObj.city);
            $("#txtState").val(comObj.state);
            $("#txtCountry").val(comObj.country);
            $("#cmbZip").val(comObj.zip);
            $("#txtZip4").val(comObj.zipFour);
            $("#txtAdd1").val(comObj.address1);
            $("#txtAdd2").val(comObj.address2);
            //	$("#txtSMS").val(comObj.sms);
            getComboListIndex("cmbSMS","desc",comObj.sms);
            getComboListIndex("cmbZip","idk",comObj.cityId);
            onZipChange();
            $("#txtCell").val(comObj.cellPhone);
            $("#txtWPExt").val(comObj.workPhoneExt);
            $("#txtWPhone").val(comObj.workPhone);
            $("#txtExtension").val(comObj.homePhoneExt);
            $("#txtHPhone").val(comObj.homePhone);
            $("#txtEmail").val(comObj.email);

            if(comObj.defaultCommunication == "1"){
                $("#rdHome").prop("checked",true);
            }else if(comObj.defaultCommunication == "2"){
                $("#rdWork").prop("checked",true);
            }else if(comObj.defaultCommunication == "3"){
                $("#rdCell").prop("checked",true);
            }else if(comObj.defaultCommunication == "4"){
                $("#rdEmail").prop("checked",true);
            }
        }

    }
}
function getTableListArray(dataObj){
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.codeTable){
        if($.isArray(dataObj.response.codeTable)){
            dataArray = dataObj.response.codeTable;
        }else{
            dataArray.push(dataObj.response.codeTable);
        }
    }
    var tempDataArry = [];
    var obj = {};
    obj.desc = "";
    obj.zip = "";
    obj.value = "";
    obj.idk = "";
    tempDataArry.push(obj);
    for(var i=0;i<dataArray.length;i++){
        if(dataArray[i].isActive == 1){
            var obj = dataArray[i];
            obj.idk = dataArray[i].id;
            obj.status = dataArray[i].Status;
            tempDataArry.push(obj);
        }
    }
    return tempDataArry;
}
function getTableFirstListArray(dataObj) {
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.codeTable) {
        if ($.isArray(dataObj.response.codeTable)) {
            dataArray = dataObj.response.codeTable;
        } else {
            dataArray.push(dataObj.response.codeTable);
        }
    }
    var tempDataArry = [];
    for (var i = 0; i < dataArray.length; i++) {
        if (dataArray[i].isActive == 1) {
            var obj = dataArray[i];
            obj.idk = dataArray[i].id;
            obj.status = dataArray[i].Status;
            tempDataArry.push(obj);
        }
    }
    return tempDataArry;
}

function onClickReset(){
    if(operation == ADD) {
        operation = ADD;
    }
    else {
        operation = UPDATE;
    }

    facilityId = "";
    $("#txtID").val("");
    $("#txtExtID1").val("");
    $("#txtExtID2").val("");
    setComboReset("cmbPrefix");
    setComboReset("cmbSuffix");
    $("#txtFN").val("");
    $("#txtLN").val("");
    $("#txtMN").val("");
    $("#txtN").val("");
    $("#txtNUCC").val("");
    $("#txtFTIT").val("");
    $("#txtFTI").val("");
    $("#txtFTEIN").val("");
    $("#txtSTI").val("");
    $("#txtNOTT").val("");
    $("#txtNPI").val("");
    $("#txtTI").val("");
    $("#txtDN").val("");
    $("#txtCity").val("");
    $("#txtState").val("");
    $("#txtCountry").val("");
    $("#txtZip4").val("");
    $("#txtNN").val("");
    $("#cmbZip").val("");
    /*var dtDOB = $("#txtNOTT").data("kendoDatePicker");
    if(dtDOB){
        dtDOB.value("");
    }*/
    //$("#txtSSN").val("");
    setComboReset("cmbStatus");
    setComboReset("cmbBilling");
    setComboReset("cmbType");
    setComboReset("cmbZip");
    setComboReset("cmbLan");
    $("#txtAdd1").val("");
    $("#txtAdd2").val("");
    setComboReset("cmbSMS");
    setComboReset("AddcmbSMS");
    $("#txtCell").val("");
    $("#txtType").val("");
    $("#txtAbbreviation").val("");

    $("#txtWExtension").val("");
    $("#txtWPhone").val("");
    $("#txtExtension").val("");
    $("#txtHPhone").val("");
    $("#txtEmail").val("");
    $("#txtFax").val("");
    $("#txtHPhone1").val("");
    $("#txtExtension1").val("");
    $("#txtCell1").val("");
    $("#txtEmail1").val("");
    $("#AddtxtAdd1").val("");
    $("#AddtxtAdd2").val("");
    $("#AddcmbZip").val("");
    $("#AddtxtCity").val("");
    $("#AddtxtState").val("");
    $("#AddtxtCountry").val("");
    $("#AddtxtZip4").val("");
    //$("#txtCell1").val("");

    $("#rdHome").prop("checked",true);

}
function setComboReset(cmbId){
    /*var strAbbr = $("#txtAbbrevation").val();
        strAbbr = $.trim(strAbbr);
        if(strAbbr == ""){
            customAlert.error("Error","Enter Abbrevation");
            flag = false;
            return false;
        }
        var strCode = $("#txtCode").val();
        strCode = $.trim(strCode);
        if(strCode == ""){
            customAlert.error("Error","Enter Code");
            flag = false;
            return false;
        }
        var strName = $("#txtName").val();
        strName = $.trim(strName);
        if(strName == ""){
            customAlert.error("Error","Enter Country");
            flag = false;
            return false;
        }*/
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb) {
        cmb.select(0);
    }
}

function getComboDataItem(cmbId){
    var dItem = null;
    var cmb = $("#"+cmbId).data("kendoComboBox");
    if(cmb && cmb.dataItem()){
        dItem =  cmb.dataItem();
        return cmb.text();
    }
    return "";
}

function getComboDataItemValue(cmbId){
    var dItem = null;
    var cmb = $("#"+cmbId).data("kendoComboBox");
    if(cmb && cmb.dataItem()){
        dItem =  cmb.dataItem();
        return cmb.value();
    }
    return "";
}
var cityId = "";
function onClickSave(){
    var IsPCFlag = "0";
    var sEmail = $('#txtEmail').val();

    if (sEmail != "" && !validateEmail(sEmail)){
        // $('.customAlert').append('<div class="alert alert-danger">your EmailId is invalid,Please enter the valid EmailId</div>');
        customAlert.error("Error","Your EmailId is invalid,Please enter the valid EmailId");
        return false;
    }
    /*if(validation()){*/
    $('.alert').remove();
    var strId = $("#txtID").val();
    var strExtId1 = $("#txtExtID1").val();
    var strExtId2 = $("#txtExtID2").val();
    var strAbbreviation = $("#txtAbbreviation").val();//getComboDataItem("txtAbbreviation");
    var strPrefix = getComboDataItem("cmbPrefix");
    var strSuffix = getComboDataItem("cmbSuffix");
    var strNickName = $("#txtN").val();
    var strStatus = getComboDataItem("cmbStatus");
    var strFN = $("#txtFN").val();
    var strMN = $("#txtMN").val();
    var strLN = $("#txtLN").val();
    var strName = $("#txtN").val();
    var strDisplayName = $("#txtDN").val();

    var strNUCC = $("#txtNUCC").val();
    var strType = getComboDataItem("cmbType");
    var strAdd1 = $("#txtAdd1").val();
    var strAdd2 = $("#txtAdd2").val();
    var strCity = $("#txtCity").val();
    var strState = $("#txtState").val();
    var strCountry = $("#txtCountry").val();
    var strFederalTaxIDType = $("#txtFTIT").val();
    var strFederalTaxID = $("#txtFTI").val();
    var strFederalTaxEIN = $("#txtFTEIN").val();
    var strStateTaxID = $("#txtSTI").val();
    var strNameOfTheTax = $("#txtNOTT").val();
    var strNPI = $("#txtNPI").val();
    var strToxonomyID = $("#txtTI").val();
    var strFax = $("#txtFax").val();
    var strHomephone = $("#txtHPhone1").val();
    var strHExtension1 = $("#txtExtension1").val();
    var strCell1 = $("#txtCell1").val();
    /*var strEmail1 = $("#txtEm1").val();*/

    var strZip = getComboDataItemValue("cmbZip");
    var strZip4 = $("#txtZip4").val();
    var strHPhone = $("#txtHPhone").val();
    var strExt = $("#txtExtension").val();
    var strWp = $("#txtWPhone").val();
    var strWpExt = $("#txtWPExt").val();
    var strCell = $("#txtCell").val();

    //var strSMS = getComboDataItem("cmbSMS");

    var strEmail = $("#txtEmail").val();
    var strLan = getComboDataItem("cmbLan");
    var strRace = getComboDataItem("cmbRace");
    var strEthinicity = getComboDataItem("cmbEthicity");

    var dataObj = {};
    dataObj.createdBy = sessionStorage.userId;
    /*var statusId = $("#cmbStatus").data("kendoComboBox");*/
    if(strStatus == "InActive") {
        dataObj.isActive = 0;
    }
    else if(strStatus == "Active") {
        dataObj.isActive = 1;
    }
    dataObj.isDeleted =  0;
    dataObj.externalId1 = strExtId1;
    dataObj.externalId2 = strExtId2;
    dataObj.abbreviation = strAbbreviation;
    dataObj.name = strNickName;//"name8";
    dataObj.displayName = $("#txtDN").val();
    dataObj.federalTaxIdType = 1;
    dataObj.federalTaxId = $("#txtFTI").val();//"10101";
    dataObj.federalTaxEin = $("#txtFTEIN").val();//"federalTaxEin";
    dataObj.stateTaxId = $("#txtSTI").val();//"1012";
    dataObj.taxName = $("#txtNOTT").val();//"tax name";
    dataObj.npi= $("#txtNPI").val();//"npi";
    dataObj.taxonomyId= $("#txtTI").val();//"npi";
    dataObj.contactPrefix = strPrefix;//"1";
    dataObj.contactSuffix = strSuffix;
    dataObj.contactFirstName = strFN;
    dataObj.contactMiddleName = strMN;
    dataObj.contactLastName = strLN;
    dataObj.contactNickName = strNickName;
    //dataObj.sms = getComboDataItem("cmbSMS");

    var cmbBilling = $("#cmbBilling").data("kendoComboBox");

    var billIdk = cmbBilling.value();
    billIdk = Number(billIdk);
    dataObj.billingAccountId = billIdk;

    var comm = [];
    var comObj = {};
    var strhouseNumber = "";
    var strAdditionalhouseNumber = "";
    var comObj1 = {};
    comObj.defaultCommunication = 1;
    /*comObj.country = $("#txtCountry").val();//"101";
    comObj.city = $("#txtCity").val();*/
    if (cntry.indexOf("United Kingdom") >= 0) {
        if(IsPostalCodeManual == "1"){
            if($("#txtCity").val() != ""){
                comObj.cityId = zipSelItem.cityId || zipSelItem.idk;
            }

            if($("#AddtxtCity").val() != "") {
                comObj1.cityId = AddzipSelItem.cityId || AddzipSelItem.idk;
            }
            strhouseNumber = $("#cmbZip").val();
            strAdditionalhouseNumber = $("#AddcmbZip").val();
            IsPCFlag = "1";
        }
        else{
            var cmbZip = $("#cmbZip").val();
            if (cmbZip) {
                comObj.cityId = zipSelItem.cityId || zipSelItem.idk;
            }
            if($("#AddcmbZip").val() != "") {
                $("#AddcmbZip").data("kendoComboBox");
                comObj1.cityId = AddzipSelItem.cityId || AddzipSelItem.idk;
            }
            IsPCFlag = "0";
        }
    }
    else{
        var cmbZip = $("#cmbZip").val();
        if (cmbZip) {
            comObj.cityId = zipSelItem.cityId || zipSelItem.idk;
        }
        if($("#AddcmbZip").val() != "") {
            $("#AddcmbZip").data("kendoComboBox");
            comObj1.cityId = AddzipSelItem.cityId || AddzipSelItem.idk;
        }
        IsPCFlag = "0";
    }

    if(strStatus == "InActive") {
        comObj.isActive = 0;
    }
    else if(strStatus == "Active") {
        comObj.isActive = 1;
    }
    /*comObj.countryId = cityId;*/
    comObj.isDeleted = 0;
    /*comObj.zipFour = $("#txtZip4").val();//"2323";*/
    comObj.sms = getComboDataItem("cmbSMS");
    /*comObj.state = $("#txtState").val();*/
    comObj.workPhoneExt = $("#txtWExtension").val();
    comObj.email = $("#txtEmail").val();
    /*comObj.zip = strZip;*/
    comObj.parentTypeId = 400;
    comObj.address2 = $("#txtAdd2").val();
    comObj.address1 = $("#txtAdd1").val();
    //comObj.homePhone = $("#txtHPhone").val();
    /*comObj.stateId = 1;
    comObj.areaCode = "232";*/
    comObj.fax = $("#txtFax").val();
    comObj.homePhoneExt = $("#txtExtension").val();
    comObj.workPhone = $("#txtWPhone").val();
    comObj.cellPhone = $("#txtCell").val();
    comObj.houseNumber = strhouseNumber;
    /*comObj.sms = getComboDataItem("AddcmbSMS");*/


    comObj1.defaultCommunication = 1;
    /*comObj1.country = $("#txtCountry").val();//"101";
    comObj1.city = $("#txtCity").val();*/

    if(strStatus == "InActive") {
        comObj1.isActive = 0;
    }
    else if(strStatus == "Active") {
        comObj1.isActive = 1;
    }
    /*comObj1.countryId = cityId;*/
    comObj1.isDeleted = 0;
    /*comObj1.zipFour = $("#txtZip4").val();//"2323";*/
    /*comObj1.state = $("#txtState").val();*/
    /*comObj1.workPhoneExt = $("#txtWExtension").val();*/
    comObj1.email = $("#txtEmail1").val();
    /*comObj1.zip = strZip;*/
    comObj1.parentTypeId = 401;
    comObj1.address2 = $("#AddtxtAdd2").val();
    comObj1.address1 = $("#AddtxtAdd1").val();
    comObj1.homePhone = $("#txtHPhone1").val();
    /*comObj1.stateId = 1;
    comObj1.areaCode = "232";*/
    comObj1.homePhoneExt = $("#txtExtension1").val();
    /*comObj1.workPhone = $("#txtWPhone").val();*/
    comObj1.cellPhone = $("#txtCell1").val();
    comObj1.houseNumber = strAdditionalhouseNumber;

    comObj1.sms = getComboDataItem("AddcmbSMS");

    /*if (operation == UPDATE) {
        comObj1.modifiedBy = sessionStorage.userId;
        comObj1.isDeleted = "0";
        comObj1.parentId = selAccountItem.idk;
        //comObj.cityId = $("#cityIdHiddenValue").val();
        //comObj1.modifiedDate = new Date().getTime();
    } else {
        comObj1.createdBy = sessionStorage.userId;
        //comObj1.createdDate = new Date().getTime();
    }*/
    if (operation == UPDATE) {
        var communicationLen = selAccountItem.communications != null ? selAccountItem.communications.length : 0;
        var cityIndexVal = "";
        var dupCityIndexVal = "";
        for(var i=0; i<communicationLen; i++) {
            if(selAccountItem.communications[i].parentTypeId == 400) {
                cityIndexVal = i;
            }
            if(selAccountItem.communications[i].parentTypeId == 401) {
                dupCityIndexVal = i;
            }
        }
        if(cityIndexVal != "" || (cityIndexVal == 0 && selAccountItem.communications != null)) {
            comObj.id = selAccountItem.communications[cityIndexVal].id;
            comObj.modifiedBy = sessionStorage.userId;
            comObj.parentId = selAccountItem.idk;
        }
        if(dupCityIndexVal != "") {
            comObj1.id = selAccountItem.communications[dupCityIndexVal].id;
            comObj1.modifiedBy = sessionStorage.userId;
            comObj1.parentId = selAccountItem.idk;
        }
        //comm1.modifiedDate = new Date().getTime();
    }
    else {
        comObj.createdBy = Number(sessionStorage.userId);
    }

    comm.push(comObj);

    if($("#txtCell1").val() != "" || $("#txtHPhone1").val() != "") {
        // if($("#AddcmbZip").val() != "" && $("#AddtxtAdd1").val() != "") {
        if($("#txtEmail1").val() != "" && !validateEmail($("#txtEmail1").val())) {
            $("body").animate({
                scrollTop: 0
            }, 500);
            // $('.customAlert').append('<div class="alert alert-danger">Please enter valid email address of Contact Person</div>');
            customAlert.error("Error","Please enter valid email address of Contact Person");
            return false;
        }
        else {
            comObj1.createdBy = Number(sessionStorage.userId);
            comm.push(comObj1);
        }
        // }
        // else {
        // 	$("body").animate({
        // 	    scrollTop: 0
        // 	}, 500);
        // 	if(sessionStorage.countryName.indexOf("India") >= 0 || sessionStorage.countryName.indexOf("United Kingdom") >= 0) {
        //     	$('.customAlert').append('<div class="alert alert-danger">Please fill Address 1 and Postal Code of Contact Person</div>');
        //     }
        //     else {
        //     	$('.customAlert').append('<div class="alert alert-danger">Please fill Address 1 and Zip of Contact Person</div>');
        //     }
        //     return false;
        // }
    }
    // else if($("#AddcmbZip").val() != "" || $("#AddtxtAdd1").val() != "") {
    // 	// if($(("#txtCell1").val() == "" || $("#txtHPhone1").val() == "")) {
    // 	// 	$("body").animate({
    // 	//     scrollTop: 0
    // 	// }, 500);
    // 	// $('.customAlert').append('<div class="alert alert-danger">Please enter Email or Homephone or Mobile Phone of Contact Person</div>');
    //    // 	return false;
    // 	// }
    // 	if($("#AddcmbZip").val() != "") {
    // 		$("body").animate({
    // 	    scrollTop: 0
    // 	}, 500);
    // 	$('.customAlert').append('<div class="alert alert-danger">Please enter Email or Homephone or Mobile Phone of Contact Person</div>');
    //    	return false;
    // 	}
    // 	if($("#AddtxtAdd1").val() != "") {
    // 		$("body").animate({
    // 	    scrollTop: 0
    // 	}, 500);
    // 	$('.customAlert').append('<div class="alert alert-danger">Please enter Email or Homephone or Mobile Phone of Contact Person</div>');
    //    	return false;
    // 	}
    // }
    dataObj.communications = comm;
    console.log(dataObj);

    if(strStatus != "" && strAbbreviation != "" && strName != "" && ((IsPCFlag == "0" && $("#cmbZip").val() != "") || (IsPCFlag == "1" && $("#txtCity").val() != "" )) && strAdd1 != "" && cmbBilling.value() != "") {
        // if(!validateEmail(strEmail)) {
        // 	$("body").animate({
        //     scrollTop: 0
        // }, 500);
        // $('.customAlert').append('<div class="alert alert-danger">Please enter valid Email Address</div>');
        // }
        // else {


        if(operation == ADD){
            var dataUrl = ipAddress+"/facility/create";
            createAjaxObject(dataUrl,dataObj,"POST",onCreate,onError);
        }else if(operation == UPDATE){
            dataObj.modifiedBy = sessionStorage.userId;
            dataObj.id = selAccountItem.idk;
            var dataUrl = ipAddress+"/facility/update";
            createAjaxObject(dataUrl,dataObj,"POST",onUpdate,onError);
        }
        // }
    }
    else{
        // $("body,html").animate({
        //     scrollTop: 0
        // }, 500);
        // $('.customAlert').append('<div class="alert alert-danger">Please fill all the Required Fields</div>');
        customAlert.info("Error", "Please fill all the Required Fields");
    }
    /*}*/
}
function onFilterChange() {
    var cmbFilterByName = $("#cmbFilterByName").data("kendoComboBox");
    if (cmbFilterByName && cmbFilterByName.selectedIndex < 0) {
        cmbFilterByName.select(0);
    }
    if($("#cmbFilterByName").val() == "id" || $("#cmbFilterByName").val() == "billing-account-id") {
        $("#cmbFilterByValue").removeAttr("disabled");
        $("#cmbFilterByValue").attr("type","number");
    }
    else if($("#cmbFilterByName").val() == "All") {
        $("#cmbFilterByValue").attr("disabled","disabled");
        $("#accountFilter-btn").attr("disabled","disabled");
        $("#cmbFilterByValue").val('');
        buildWeekNumberListGrid([]);
        getAjaxObject(ipAddress+"/facility/list?is-active=1&is-deleted=0","GET",getFacilityList,onError);
    }
    else {
        $("#cmbFilterByValue").removeAttr("disabled");
        $("#cmbFilterByValue").attr("type","text");
    }
}

function onCreate(dataObj){
    console.log(dataObj);
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {

            var msg = dataObj.response.status.message;
            displaySessionErrorPopUp("Info", msg, function(res) {
                operation = ADD;
                init();
                onClickCancel();
            })

        } else {
            // $('.customAlert').append('<div class="alert alert-danger">'+dataObj.response.status.message+'</div>');
            // /*customAlert.error("error", dataObj.response.status.message);*/
            customAlert.info("Error", dataObj.response.status.message);
        }
    }
}
function onUpdate(dataObj){
    console.log(dataObj);
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            // /*var obj = {};
            // obj.status = "success";
            // obj.operation = operation;
            // popupClose(obj);*/
            // // $('.customAlert').append('<div class="alert alert-success">'+dataObj.response.status.message+'</div>');
            // customAlert.info("info", dataObj.response.status.message);
            // $('.tabContentTitle').text('Update Facility');
            // var id = $('#txtID').val();
            // $('#btnCancel').trigger('click');
            // // getAjaxObject(ipAddress+"/facility/list?id="+id,"GET",updateList,onError);


            var msg = dataObj.response.status.message;
            displaySessionErrorPopUp("Info", msg, function(res) {
                operation = ADD;
                init();
                onClickCancel();
            })

        } else {
            // $('.customAlert').append('<div class="alert alert-danger">'+dataObj.response.status.message+'</div>');
            customAlert.error("Error", dataObj.response.status.message);
            /*customAlert.error("error", dataObj.response.status.message);*/
        }
    }
    return false;
}
function updateList(dataObj) {
    console.log(dataObj);
    selAccountItem = dataObj.response.facility[0];
    selAccountItem.idk = selAccountItem.id;
    addFacility("update");
}

function onError(errObj){
    console.log(errObj);
    customAlert.error("Error","Error");
}

function onClickSearch(){
    /*var obj = {};
    obj.status = "search";
    popupClose(obj);*/
}
/*function popupClose(obj){
	var windowWrapper = new kendoWindowWrapper();
	windowWrapper.closePageWindow(obj);
}*/
function validateEmail(sEmail) {
    console.log(sEmail);
    var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    if (filter.test(sEmail)) {
        return true;
    }
    else {
        return false;
    }
}


function themeAPIChange(){
    getAjaxObject(ipAddress+"/homecare/settings/?id=2","GET",getThemeValue,onError);
}


function getThemeValue(dataObj){
    console.log(dataObj);
    var tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.settings){
            if($.isArray(dataObj.response.settings)){
                tempCompType = dataObj.response.settings;
            }else{
                tempCompType.push(dataObj.response.settings);
            }
        }
        if(tempCompType.length > 0){
            themesChange(tempCompType[0].value);
        }
    }
}

function themesChange(themeValue){
    if(themeValue == 2){
        loadAPi("../../Theme2/Theme02.css");
    }
    else if(themeValue == 3){
        loadAPi("../../Theme3/Theme03.css");
    }
}




var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
function formatDate(apptdate) {

    var formatted_date = apptdate.getDate() + " " + months[apptdate.getMonth()] + " " + apptdate.getFullYear();
    return formatted_date;
}

Date.prototype.getWeekNo = function() {
    // var onejan = new Date(this.getFullYear(),0,1);
    // var today = new Date(this.getFullYear(),this.getMonth(),this.getDate());
    // var dayOfYear = ((today - onejan +1)/86400000);
    // return Math.ceil(dayOfYear/7)
    var firstDay = new Date(this.getFullYear(), this.getMonth(), 1).getDay();
    return Math.ceil((this.getDate() + firstDay)/7);
};


Date.prototype.getDayOfWeek = function() {
    var days = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];

    var day = days[this.getDay()];
    return day;
};



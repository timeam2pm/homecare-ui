var parentRef = null;

var patientId = "";
var angularUIgridWrapper = null;
var angularUIgridWrapperH = null;
var towArr = [{Key:'0',Value:'Sun'},{Key:'1',Value:'Mon'},{Key:'2',Value:'Tue'},{Key:'3',Value:'Wed'},{Key:'4',Value:'Thu'},{Key:'5',Value:'Fri'},{Key:'6',Value:'Sat'}];
var cntry = sessionStorage.countryName;

$(document).ready(function(){
	if((cntry.indexOf("India")>=0) || (cntry.indexOf("United Kingdom")>=0)){
		$("#lblFacility").text("Location");
	}else if(cntry.indexOf("United State")>=0){
		$("#lblFacility").text("Facility");
	}
});


$(window).load(function(){
	loading = false;
	$(window).resize(adjustHeight);
	onLoaded();
	if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
    onGetPatientInfo();
});

function onLoaded(){
	parentRef = parent.frames['iframe'].window;
	init();
    buttonEvents();
	adjustHeight();
}
var startDT = null;
var endDT = null;
function init(){
	var fmt = getCountryDateFormat();
	/*$("#cmbFecility").kendoComboBox();*/
	$("#cmbProvider").kendoComboBox();
	 startDT = $("#txtStartDate").kendoDatePicker({
         change: startChange,format:fmt
     }).data("kendoDatePicker");

     endDT = $("#txtEndDate").kendoDatePicker({
         change: endChange,format:fmt
     }).data("kendoDatePicker");
	patientId = parentRef.patientId;	
	
	getAjaxObject(ipAddress+"/facility/list/?is-active=1","GET",getFacilityList,onError);
	
	var dataOptions = {pagination: false, paginationPageSize: 500,changeCallBack: onChange};
		angularUIgridWrapper = new AngularUIGridWrapper("dgridProviderAppList", dataOptions);
	    angularUIgridWrapper.init();
	    buildProviderAppList([]);
	    
	    var dataOptions1 = {pagination: false, paginationPageSize: 500,changeCallBack: onChange1};
		angularUIgridWrapperH = new AngularUIGridWrapper("dgridProviderAppListH", dataOptions1);
	    angularUIgridWrapperH.init();
	    buildProviderAppListH([]);

	    //&from-date=05-NOV-2017&days=1
	    var dt = new Date();
	    var stDate = new Date();
	    stDate.setDate(stDate.getDate()-7);
	    startDT.value(stDate);
	    
	    var endDate = new Date();
	    endDate.setDate(endDate.getDate());
	    endDT.value(endDate);
	    
	    //dt = kendo.toString(new Date(dt), "dd-MMM-yyyy");
	    var patientListURL = ipAddress+"/appointment/list/?patient-id="+patientId+"&to-date="+endDate.getTime()+"&from-date="+stDate.getTime()+"&is-active=1";
		 //getAjaxObject(patientListURL,"GET",onPatientListData1,onError);
	    
}
function getFacilityList(dataObj){
	var dataArray = [];
	if(dataObj){
		if($.isArray(dataObj.response.facility)){
			dataArray = dataObj.response.facility;
		}else{
			dataArray.push(dataObj.response.facility);
		}
	}
	var tempDataArry = [];
	for(var i=0;i<dataArray.length;i++){
		dataArray[i].idk = dataArray[i].id; 
		dataArray[i].Status = "InActive";
		if(dataArray[i].isActive == 1){
			dataArray[i].Status = "Active";
		}
		if(parentRef.facilityId == dataArray[i].idk){
			$("#txtRLocation").val(dataArray[i].name);
		}
	}
	var obj  ={};
	obj.name = "";
	obj.idk = "";
	//dataArray.unshift(obj);
	//setDataForSelection(dataArray, "cmbFecility", onFacilityChange, ["name", "idk"], 0, "");
	getAjaxObject(ipAddress+"/provider/list/?is-active=1","GET",onGetProviderList,onError);
}
function onFacilityChange(){
	// var cmbFecility = $("#cmbFecility").data("kendoComboBox");
	// if(cmbFecility){
	// 	if(cmbFecility.selectedIndex<0){
	// 		cmbFecility.select(0);
	// 	}
	// }
	if(parentRef.facilityId>0){
		getProviderList();
	}else{
		setDataForSelection([], "cmbProvider", onProviderChange, ["", ""], 0, "");
		var cmbProvider = $("#cmbProvider").data("kendoComboBox");
		if(cmbProvider){
			cmbProvider.text("");
			cmbProvider.value("");
		}
		getAjaxObject(ipAddress+"/provider/list/?is-active=1","GET",onGetProviderList,onError);
	}
}
function getProviderList(){
	// var cmbFecility = $("#cmbFecility").data("kendoComboBox");
	var facilityId = parentRef.facilityId;
	var patientListURL = ipAddress+"/provider/list/?facility-id="+facilityId+"&is-active=1";
	getAjaxObject(patientListURL,"GET",onGetProviderList,onError);
}
function onGetProviderList(dataObj){
	console.log(dataObj);
	var dataArray = [];
	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		if($.isArray(dataObj.response.provider)){
			dataArray = dataObj.response.provider;
		}else{
			dataArray.push(dataObj.response.provider);
		}
	}

    // dataArray.sort(function (a, b) {
    //     var nameA = a.lastName.toUpperCase(); // ignore upper and lowercase
    //     var nameB = b.lastName.toUpperCase(); // ignore upper and lowercase
    //     if (nameA < nameB) {
    //         return -1;
    //     }
    //     if (nameA > nameB) {
    //         return 1;
    //     }
    //     // names must be equal
    //     return 0;
    // });

    dataArray.sort(function (a, b) {
        var aSize = a.lastName.toUpperCase();
        var bSize = b.lastName.toUpperCase();
        var aLow = a.firstName.toUpperCase();
        var bLow = b.firstName.toUpperCase();
        // console.log(aLow + " | " + bLow);

        if(aSize == bSize)
        {
            return (aLow < bLow) ? -1 : (aLow > bLow) ? 1 : 0;
        }
        else
        {
            return (aSize < bSize) ? -1 : 1;
        }
    });

	var tempDataArry = [];
	for(var i=0;i<dataArray.length;i++){
		dataArray[i].idk = dataArray[i].id; 
		dataArray[i].Status = "InActive";
		if(dataArray[i].isActive == 1){
			dataArray[i].Status = "Active";
		}
        dataArray[i].name = dataArray[i].lastName+ " "+dataArray[i].firstName;
	}
	var obj  ={};
	obj.name = "";
	obj.idk = "";
	dataArray.unshift(obj);
	setDataForSelection(dataArray, "cmbProvider", onProviderChange, ["name", "idk"], 0, "");
	
	onClickSearch();
}
function onProviderChange(){
	
}
function startChange() {
    var startDate = startDT.value(),
    endDate = endDT.value();

    if (startDate) {
        startDate = new Date(startDate);
        startDate.setDate(startDate.getDate());
        endDT.min(startDate);
    } else if (endDate) {
        startDT.max(new Date(endDate));
    } else {
        endDate = new Date();
        startDT.max(endDate);
        endDT.min(endDate);
    }
}

function endChange() {
    var endDate = endDT.value(),
    startDate = startDT.value();

    if (endDate) {
        endDate = new Date(endDate);
        endDate.setDate(endDate.getDate());
        startDT.max(endDate);
    } else if (startDate) {
        endDT.min(new Date(startDate));
    } else {
        endDate = new Date();
        startDT.max(endDate);
        endDT.min(endDate);
    }
}
function onPatientListData1(dataObj){
	console.log(dataObj);
	var dtArray = [];
	if(dataObj && dataObj.response && dataObj.response.appointment){
		if($.isArray(dataObj.response.appointment)){
			dtArray = dataObj.response.appointment;
		}else{
			dtArray.push(dataObj.response.appointment);
		}
	}
    // dtArray.sort(function (a, b) {
    //     var nameA = a.dateOfAppointment; // ignore upper and lowercase
    //     var nameB = b.dateOfAppointment; // ignore upper and lowercase
    //     if (nameA < nameB) {
    //         return -1;
    //     }
    //     if (nameA > nameB) {
    //         return 1;
    //     }
    //     // names must be equal
    //     return 0;
    // });

    // dtArray.slice(0).sort((a,b) => b.dateOfAppointment - a.dateOfAppointment);
	//
    // dtArray.sort(function (a, b) {
    //     var nameA = a.dateOfAppointment; // ignore upper and lowercase
    //     var nameB = b.dateOfAppointment; // ignore upper and lowercase
    //     if (nameA < nameB) {
    //         return 1;
    //     }
    //     if (nameA > nameB) {
    //         return -1;
    //     }
    //     // names must be equal
    //     return 0;
    // });
	//
    // dtArray.slice(0).sort((a,b) => b.dateOfAppointment - a.dateOfAppointment);

    dtArray.sort(function (a, b) {
        var p1 = GetDateTimeEditDate(a.dateOfAppointment);
        var p2 = GetDateTimeEditDate(b.dateOfAppointment);
        var o1, o2;
        o1 = (a.dateOfAppointment-p1);
        o2 = (b.dateOfAppointment-p2);
        if (p1 < p2) return 1;
        if (p1 > p2) return -1;
        // if(p1 = p2){
            if (o1 < o2) return -1;
            if (o1 > o2) return 1;
		// }
        return 0;
    });

	var dArray = [];
	for(var j=0;j<dtArray.length;j++){
		var item = dtArray[j];
		var fDateMS = item.dateOfAppointment;


        var date = new Date(GetDateTimeEditDay(item.dateOfAppointment));
        var day = date.getDay();
        var dow = getWeekDayName(day);

        item.dateTimeOfAppointment = dow + " " + GetDateTimeEdit(item.dateOfAppointment);


		var fDate = new Date(fDateMS);
		var tmFmt = getCountryDateFormat();//getCountryDateTimeFormat();
		fDate = kendo.toString(new Date(fDateMS), tmFmt);
		var fTM = kendo.toString(new Date(fDateMS), "hh:mm tt");
		item.fromDate = fDate;
		item.TM = fTM;
		var day = new Date(fDateMS).getDay();
		var dow = getWeekDayName(day);
		item.DOW = dow;
		if(item.createdDate){
			var fmt = getCountryDateFormat();
			item.CD = kendo.toString(new Date(item.createdDate), fmt);
		}
		item.CB = item.createdBy;
		if(item.composition && item.composition.appointmentType && item.composition.appointmentType.desc){
			item.AppType = item.composition.appointmentType.desc;
		}else{
			item.AppType = "";
		}
		if(item.composition && item.composition.appointmentReason && item.composition.appointmentReason.desc){
			item.Reason = item.composition.appointmentReason.desc;
		}else{
			item.Reason = "";
		}
		
		if(item.providerId != 0 && item.composition && item.composition.provider){
			item.Provider = item.composition.provider.lastName +" " +item.composition.provider.firstName;
		}
		else if(item.providerId == 0){
			item.Provider = "Un Allocated";
		}else{
			item.Provider = "";
		}
		if(item.composition && item.composition.facility && item.composition.facility.displayName){
			item.Fecility = item.composition.facility.displayName;
		}else{
			item.Fecility = "";
		}
		// if(item.outTime && item.inTime){
        if(item.syncTime){
			item.RP = "View";
		}else{
			item.RP = "";
		}
		item.idk = item.id;
		dArray.push(item);
	}

	buildProviderAppList(dArray);
}
function getWeekDayName(wk){
	var wn = "";
	if(wk == 1){
		return "Mon";
	}else if(wk == 2){
		return "Tue";
	}else if(wk == 3){
		return "Wed";
	}else if(wk == 4){
		return "Thu";
	}else if(wk == 5){
		return "Fri";
	}else if(wk == 6){
		return "Sat";
	}else if(wk == 0){
		return "Sun";
	}
	return wn;
}
function buildProviderAppList(dataSource){
	var gridColumns = [];
	var otoptions = {};
	otoptions.noUnselect = false;
	gridColumns.push({
        "title": "Date",
        "field": "dateTimeOfAppointment",
        "enableColumnMenu": false,
        "cellTemplate": "<div  style='text-decoration:underline;cursor:pointer' class='ui-grid-cell-contents' onclick='onClickLink()'><a style='font-size: 13px'>{{row.entity.dateTimeOfAppointment}}</a></div>",
        "width":"23%"
    });
	// gridColumns.push({
    //     "title": "Time",
    //     "field": "TM",
    //     "enableColumnMenu": false,
    // });
	// gridColumns.push({
    //     "title": "DOW",
    //     "field": "DOW",
    //     "enableColumnMenu": false,
    // });
	gridColumns.push({
        "title": "Duration(min)",
        "field": "duration",
        "enableColumnMenu": false,
        "width":"10%"
    });
	 gridColumns.push({
	        "title": "Status",
	        "field": "AppType",
	        "enableColumnMenu": false,
	    });
	 gridColumns.push({
	        "title": "Reason",
	        "field": "Reason",
	        "enableColumnMenu": false,
	    });
	if((cntry.indexOf("India")>=0) || (cntry.indexOf("United Kingdom")>=0)){
		gridColumns.push({
			"title": "Location",
			"field": "Fecility",
			"enableColumnMenu": false,
		});
	}else if(cntry.indexOf("United State")>=0){
		gridColumns.push({
			"title": "Facility",
			"field": "Fecility",
			"enableColumnMenu": false,
		});
	}
	gridColumns.push({
        "title": "Staff",
        "field": "Provider",
        "enableColumnMenu": false,
    });
	gridColumns.push({
        "title": "Report",
        "field": "RP",
        "enableColumnMenu": false,
        "cellTemplate": "<div  style='text-decoration:underline;cursor:pointer' class='ui-grid-cell-contents' onclick='onClickRPLink()'><a style='font-size: 13px'>{{row.entity.RP}}</a></div>",
        "width":"8%"
    });
	angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
	adjustHeight();
}
function onClickLink(){
	setTimeout(function(){
		console.log("click");
		var rows = angularUIgridWrapper.getSelectedRows();
		//console.log(rows);
		if(rows && rows.length>0){
			var row = rows[0];
			if(row){
				console.log(row.idk);
				var dataUrl = ipAddress+"/appointment/history/list/?id="+row.idk;
				getAjaxObject(dataUrl,"GET",getAppHistoryList,onError);
				
			}
		}
	},100)
}
function onClickRPLink(){
	setTimeout(function(){
	var rows = angularUIgridWrapper.getSelectedRows();
	//console.log(rows);
	if(rows && rows.length>0){
		var row = rows[0];
		if(row){
			// console.log(row.idk);
			 var popW = "80%";
			    var popH = "70%";
			    var profileLbl;
			    var devModelWindowWrapper = new kendoWindowWrapper();
			    profileLbl = "Visit Report";
			    parentRef.idk = row.idk;
			    devModelWindowWrapper.openPageWindow("../../html/reports/patientActivity.html", profileLbl, popW, popH, true, onCloseSearchZipAction);
		}
	}
	},100);
}
function onCloseSearchZipAction(evt,returnData){
	
}
function getAppHistoryList(dataObj){
	// console.log(dataObj);
	var dtArray = [];
	if(dataObj && dataObj.response && dataObj.response.appointmentHistory){
		if($.isArray(dataObj.response.appointmentHistory)){
			dtArray = dataObj.response.appointmentHistory;
		}else{
			dtArray.push(dataObj.response.appointmentHistory);
		}
	}
	var dArray = [];
	for(var j=0;j<dtArray.length;j++){
		var item = dtArray[j];
		var fDate = item.dateOfAppointment;
		fDate = new Date(fDate);
		fDate = kendo.toString(new Date(fDate), "dd-MMM-yyyy h:mm:ss tt");
		
		item.fromDate = fDate;
		if(item.modifiedDate){
			item.CD = kendo.toString(new Date(item.modifiedDate), "dd-MMM-yyyy");
		}else if(item.createdDate){
			item.CD = kendo.toString(new Date(item.createdDate), "dd-MMM-yyyy");
		}
		if(item.modifiedBy){
			item.CB = item.modifiedBy;
		}else{
			item.CB = item.createdBy;
		}
		
		item.AppType = item.appointmentTypeDesc;
		item.Reason = item.appointmentReasonDesc;
		item.Provider = item.providerFirstName;
		item.Fecility = item.facilityName;
		item.idk = item.id;
		
		dArray.push(item);
	}
	buildProviderAppListH(dArray);
}
function onChange(){
	
}
function onChange1(){
	
}
function buildProviderAppListH(dataSource){
	var gridColumns = [];
	var otoptions = {};
	otoptions.noUnselect = false;
	gridColumns.push({
        "title": "Appointment Date",
        "field": "fromDate",
        "enableColumnMenu": false,
        "width":"25%"
    });
	gridColumns.push({
        "title": "Duration(min)",
        "field": "duration",
        "enableColumnMenu": false,
    });
	 gridColumns.push({
	        "title": "Appointment Type",
	        "field": "AppType",
	        "enableColumnMenu": false,
	    });
	 gridColumns.push({
	        "title": "Reason",
	        "field": "Reason",
	        "enableColumnMenu": false,
	    });

	if((cntry.indexOf("India")>=0) || (cntry.indexOf("United Kingdom")>=0)){
		gridColumns.push({
			"title": "Location",
			"field": "Fecility",
			"enableColumnMenu": false,
		});
	}else if(cntry.indexOf("United State")>=0){
		gridColumns.push({
			"title": "Facility",
			"field": "Fecility",
			"enableColumnMenu": false,
		});
	}
	gridColumns.push({
        "title": "Staff",
        "field": "Provider",
        "enableColumnMenu": false,
    });
	
	angularUIgridWrapperH.creategrid(dataSource, gridColumns,otoptions);
	adjustHeight();
}
function adjustHeight(){
	var defHeight = 350;
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 300;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    try{
    	angularUIgridWrapper.adjustGridHeight(cmpHeight);
    	angularUIgridWrapperH.adjustGridHeight(cmpHeight);
    }catch(e){};
}
function onError(errorObj){
	console.log(errorObj);
}
function buttonEvents(){
	$("#btnClose").off("click",onClickCancel);
	$("#btnClose").on("click",onClickCancel);
	
	$("#btnSearch").off("click",onClickSearch);
	$("#btnSearch").on("click",onClickSearch);
	
	$("#btnAdd").off("click",onClickAdd);
	$("#btnAdd").on("click",onClickAdd);

    $("#btnSubmit").off("click", onClickView);
    $("#btnSubmit").on("click", onClickView);
}

var providerId = "";
var fromDate = "";
var toDate = "";
function onClickSearch(){
	buildProviderAppList([]);
	//var cmbFecility = $("#cmbFecility").data("kendoComboBox");
	var cmbProvider = $("#cmbProvider").data("kendoComboBox");
	var txtStartDate = $("#txtStartDate").data("kendoDatePicker");
	var txtEndDate = $("#txtEndDate").data("kendoDatePicker");
	
	patientId = parentRef.patientId;	
	
	var patientListURL = "";//
	if(txtStartDate.value() && txtEndDate.value()){
		var endDate = new Date(txtEndDate.value());
		var stDate = new Date(txtStartDate.value());
		stDate.setHours(0, 0, 0, 0);
		endDate.setHours(23, 59, 59);
		var edTime = endDate.getTime();
		// edTime = edTime+86400000;
        edTime = edTime;
		
		patientListURL = ipAddress+"/appointment/list/?patient-id="+patientId+"&to-date="+edTime+"&from-date="+stDate.getTime()+"&is-active=1";
		if(parentRef.facilityId != 0){
			patientListURL = patientListURL+"&facility-id="+parentRef.facilityId;
		}
		if(cmbProvider && cmbProvider.value()){
			patientListURL = patientListURL+"&provider-id="+cmbProvider.value();
		}
		 getAjaxObject(patientListURL,"GET",onPatientListData1,onError);
	}else{
		customAlert.error("Error", "Select Dates");
	}
}
function onClickAdd(){
}

function addAppointment(){
}
function closeAppointment(evt,returnData){
}

function onClickCancel(){
	var obj = {};
	var windowWrapper = new kendoWindowWrapper();
	windowWrapper.closePageWindow(obj);
}

function onGetPatientInfo() {
	$("#divIdMain").show();
    $("#lblptID").html(parentRef.patientId);
    $("#ptName").html(parentRef.patientInfo.lastName+" "+parentRef.patientInfo.firstName+" "+parentRef.patientInfo.middleName);
    $("#lblGender").html(parentRef.patientInfo.gender);
    var dt = new Date(parentRef.patientInfo.dateOfBirth);
    if(dt){
        var strDT = onGetDate(parentRef.patientInfo.dateOfBirth);
        var currDate = new Date();
        var strAge = getAge(dt);// currDate.getFullYear()-dt.getFullYear();
        strDT = strDT+"  ("+strAge+" Y)"
        $("#ptDOB").html(strDT);
        var str1 = strDT+' , '+parentRef.patientInfo.gender;//+" - "+dataObj.response.patient.firstName+" "+dataObj.response.patient.middleName+" "+dataObj.response.patient.lastName;
        $("#pt2").text(str1);
    }
    $("#lblStatus").html(parentRef.patientInfo.status);
    var imageServletUrl = "";
    if(parentRef.patientInfo.photoExt){
        imageServletUrl = ipAddress+"/download/patient/photo/"+parentRef.patientId+"/?"+Math.round(Math.random()*1000000)+"&access_token="+sessionStorage.access_token+"&tenant="+sessionStorage.tenant;;
    }else if(parentRef.patientInfo.gender == "Male"){
        imageServletUrl = "../../img/AppImg/HosImages/male_profile.png";
    }else if(parentRef.patientInfo.gender == "Female"){
        imageServletUrl = "../../img/AppImg/HosImages/profile_female.png";
    }
    $("#userImg").show();
    $("#userImg").attr("src",imageServletUrl);

    if(parentRef.patientInfo.dnr == 1){
        $("#userImg").css("border","3px solid red");
    }
    else{
        $("#userImg").css("border","3px solid white");
    }
}


function tabLink(id, current){
    var dtFMT = null;
    cntry = sessionStorage.countryName;
    if(cntry.indexOf("India")>=0){
        dtFMT = INDIA_DATE_FMT;
    }else  if(cntry.indexOf("United Kingdom")>=0){
        dtFMT = ENG_DATE_FMT;
    }else  if(cntry.indexOf("United State")>=0){
        dtFMT = US_DATE_FMT;
    }

    $('.boxtablink').removeClass('boxtablink-active');
    $('.tabContent').removeClass('tabContent-active');
    $('#' + id).addClass('tabContent-active');
    $(current).addClass('boxtablink-active');
    if(id == "tab2"){
        $("#txtDateOfWeek").kendoDatePicker({ format: dtFMT, value: new Date() });
	}
}


function onClickView() {
    onClickRotaSearch();

    $(".rosterappointmentTableBlock").css("display", "block");
    $(".rosterappointmentReportTableBlock").css("display", "block");
}

var allDatesInWeek = [];

function onClickRotaSearch() {
    if (patientId != "") {
        if ($("#txtDateOfWeek").val() !== "") {
            var txtDateOfWeek = $("#txtDateOfWeek").data("kendoDatePicker");
            var selDate = txtDateOfWeek.value();
            var selectedDate = new Date(selDate);
            allDatesInWeek = [];

            for (let i = 1; i <= 7; i++) {
                var first = selectedDate.getDate() - (selectedDate.getDay() + 1) + i;
                var day = new Date(selectedDate.setDate(first));

                allDatesInWeek.push(day);
            }
            var firstDayOfWeek = allDatesInWeek[0];
            var lastDayOfWeek = allDatesInWeek[allDatesInWeek.length - 1];

            var fromDate = getFromDate(firstDayOfWeek);
            var toDate = getToDate(lastDayOfWeek);

            var patientRosterURL = ipAddress + "/appointment/list/?patient-id=" + patientId + "&is-active=1&from-date=" + fromDate + "&to-date=" + toDate;
            // buildPatientRosterList([]);
            getAjaxObject(patientRosterURL, "GET", onGetAppointmentRosterList, onError);
        }
    }
}


function onGetAppointmentRosterList(dataObj) {
    var rosterArray = [];
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == 1) {
        if ($.isArray(dataObj.response.appointment)) {
            rosterArray = dataObj.response.appointment;
        } else {
            rosterArray.push(dataObj.response.appointment);
        }
    }
    var tempRosterArray = [];
    for (var counter = 0; counter < rosterArray.length; counter++) {
        if (rosterArray[counter] && rosterArray[counter].dateOfAppointment !== 0 && rosterArray[counter].duration !== 0) {
            var dateOfAppointment = new Date(parseInt(rosterArray[counter].dateOfAppointment));
            var hours = dateOfAppointment.getHours();
            var minutes = dateOfAppointment.getMinutes();
            var tempTime = (Number(hours) * 60) + Number(minutes);
            rosterArray[counter].fromTime = tempTime;
            rosterArray[counter].toTime = tempTime + Number(rosterArray[counter].duration);
            rosterArray[counter].weekDayEnd = dateOfAppointment.getDay();
            rosterArray[counter].weekDayStart = dateOfAppointment.getDay();

            if (rosterArray[counter].composition && rosterArray[counter].composition.provider) {
                var tempPatient = rosterArray[counter].composition.provider;
                var staffName = tempPatient.lastName + " " + tempPatient.firstName + " " + (tempPatient.middleName != null ? tempPatient.middleName : "");
                rosterArray[counter].provider = staffName.trim();
            }

            tempRosterArray.push(rosterArray[counter]);
        }
    }
    if(tempRosterArray.length > 0) {
        rosterArray = tempRosterArray;
        providerRosterArray = rosterArray;
        $("#divTable").html("");
        $("#divCareerTable").html("");
        getServiceUsersByProviders();
        var strTable = "<table class='fixed-first-column'>";
        strTable = strTable + "<tr>";
        strTable = strTable + "<th class='column-head'>Day</th>";
        for (var i = 0; i < prsViewArray.length; i++) {
            if (i % 4 == 0) {
                strTable = strTable + "<th colspan='4'>" + prsViewArray[i].tm + "</th>";
            }
        }
        strTable = strTable + "</tr>";
        for (var k = 0; k <= 6; k++) {
            var date = onGetHeadDate(k);
            strTable = strTable + "<tr>";
            strTable = strTable + "<td class='column-head' style=''>" +date +" ("+ getWeekDayName(k) + ")</td>";
            for (var j = 0; j < prsViewArray.length; j++) {
                var itemData = getItemData(prsViewArray[j], k);
                if (itemData) {
                    if (itemData.color == "green") {
                        sdt = "";
                        var tle = itemData.provider;
                        tle = tle.substring(0, tle.length - 1);
                        tle = tle + "\n";
                        //tle = tle+st+"-"+et;
                        var countArr = tle.split(",");
                        var count = countArr.length;
                        // if(count == 1){
                        //     count = "";
                        // }
                        if (count > 1) {
                            strTable = strTable + "<td style='background-color: #44b1f7; font-weight: bold; color:#fff;' title='" + tle + "'>" + count + "</td>";
                        } else {
                            strTable = strTable + "<td style='background-color: #63c67c; font-weight: bold; color:#fff;' title='" + tle + "'>" + count + "</td>";
                        }

                    } else {
                        var tItem = prsViewArray[j];
                        var hours = Number(tItem.h);
                        hours = hours * 3600;

                        var minutes = Number(tItem.m);
                        minutes = minutes * 60;

                        var currMS = (hours + minutes);

                        if (sdt == "") {
                            sdt = currMS;
                            currClass = Math.floor(Math.random() * 10000000);
                            currClass = "C" + currClass;
                            if (posClass == "") {
                                posClass = currClass;
                            }
                        }
                        classArray[currClass] = currMS;
                        //edt = getProviderEndDateTime(tItem.tm,currMS,prId,txtId,et);
                        strTable = strTable + "<td st=" + sdt + " et=" + et + " class='" + currClass + "'></td>";
                    }
                } else {
                    sdt = "";
                    strTable = strTable + "<td></td>";
                }
            }
            strTable = strTable + "</tr>";
        }
        strTable = strTable + "</table>";
        $("#divTable").html(strTable);
        $(".tableHints").css("display", "block");
        $(".spanReportDuration").css("display", "block");
    }
    else{
    	customAlert.info("Info","No appointments");
	}
}



var prsViewArray = [];
var prsViewArray = [{idk:'1',h:'0',m:'0',tm:'12:00 AM'},{idk:'2',h:'0',m:'15',tm:''},{idk:'3',h:'0',m:'30',tm:''},{idk:'4',h:'0',m:'45',tm:''},{idk:'5',h:'1',m:'0',tm:'01:00 AM'}];
prsViewArray.push({idk:'6',h:'1',m:'15',tm:''});
prsViewArray.push({idk:'7',h:'1',m:'30',tm:''});
prsViewArray.push({idk:'8',h:'1',m:'45',tm:''});
prsViewArray.push({idk:'9',h:'2',m:'0',tm:'02:00 AM'});
prsViewArray.push({idk:'10',h:'2',m:'15',tm:''});
prsViewArray.push({idk:'11',h:'2',m:'30',tm:''});
prsViewArray.push({idk:'11',h:'2',m:'45',tm:''});
prsViewArray.push({idk:'12',h:'3',m:'0',tm:'03:00 AM'})
prsViewArray.push({idk:'13',h:'3',m:'15',tm:''});
prsViewArray.push({idk:'14',h:'3',m:'30',tm:''});
prsViewArray.push({idk:'15',h:'3',m:'45',tm:''})
prsViewArray.push({idk:'16',h:'4',m:'0',tm:'04:00 AM'});
prsViewArray.push({idk:'17',h:'4',m:'15',tm:''});
prsViewArray.push({idk:'18',h:'4',m:'30',tm:''});
prsViewArray.push({idk:'19',h:'4',m:'45',tm:''});
prsViewArray.push({idk:'20',h:'5',m:'0',tm:'05:00 AM'});
prsViewArray.push({idk:'21',h:'5',m:'15',tm:''});
prsViewArray.push({idk:'22',h:'5',m:'30',tm:''});
prsViewArray.push({idk:'23',h:'5',m:'45',tm:''});
prsViewArray.push({idk:'24',h:'6',m:'0',tm:'06:00 AM'});
prsViewArray.push({idk:'25',h:'6',m:'15',tm:''});
prsViewArray.push({idk:'26',h:'6',m:'30',tm:''});
prsViewArray.push({idk:'27',h:'6',m:'45',tm:''});
prsViewArray.push({idk:'28',h:'7',m:'0',tm:'07:00 AM'});
prsViewArray.push({idk:'29',h:'7',m:'15',tm:''});
prsViewArray.push({idk:'30',h:'7',m:'30',tm:''});
prsViewArray.push({idk:'31',h:'7',m:'45',tm:''});
prsViewArray.push({idk:'32',h:'8',m:'0',tm:'08:00 AM'})
prsViewArray.push({idk:'33',h:'8',m:'15',tm:''});
prsViewArray.push({idk:'34',h:'8',m:'30',tm:''});
prsViewArray.push({idk:'35',h:'8',m:'45',tm:''});
prsViewArray.push({idk:'36',h:'9',m:'0',tm:'09:00 AM'});
prsViewArray.push({idk:'37',h:'9',m:'15',tm:''});
prsViewArray.push({idk:'38',h:'9',m:'30',tm:''});
prsViewArray.push({idk:'39',h:'9',m:'45',tm:''});
prsViewArray.push({idk:'40',h:'10',m:'0',tm:'10:00 AM'});
prsViewArray.push({idk:'41',h:'10',m:'15',tm:''});
prsViewArray.push({idk:'42',h:'10',m:'30',tm:''});
prsViewArray.push({idk:'43',h:'10',m:'45',tm:''});
prsViewArray.push({idk:'44',h:'11',m:'0',tm:'11:00 AM'});
prsViewArray.push({idk:'45',h:'11',m:'15',tm:''});
prsViewArray.push({idk:'46',h:'11',m:'30',tm:''});
prsViewArray.push({idk:'47',h:'11',m:'45',tm:''});
prsViewArray.push({idk:'48',h:'12',m:'0',tm:'12:00 PM'});
prsViewArray.push({idk:'49',h:'12',m:'15',tm:''})
prsViewArray.push({idk:'50',h:'12',m:'30',tm:''});
prsViewArray.push({idk:'51',h:'12',m:'45',tm:''});
prsViewArray.push({idk:'52',h:'13',m:'0',tm:'01:00 PM'});
prsViewArray.push({idk:'53',h:'13',m:'15',tm:''});
prsViewArray.push({idk:'54',h:'13',m:'30',tm:''});
prsViewArray.push({idk:'55',h:'13',m:'45',tm:''});
prsViewArray.push({idk:'56',h:'14',m:'0',tm:'02:00 PM'});
prsViewArray.push({idk:'57',h:'14',m:'15',tm:''});
prsViewArray.push({idk:'58',h:'14',m:'30',tm:''});
prsViewArray.push({idk:'59',h:'14',m:'45',tm:''});
prsViewArray.push({idk:'60',h:'15',m:'0',tm:'03:00 PM'});
prsViewArray.push({idk:'61',h:'15',m:'15',tm:''});
prsViewArray.push({idk:'62',h:'15',m:'30',tm:''});
prsViewArray.push({idk:'63',h:'15',m:'45',tm:''});
prsViewArray.push({idk:'64',h:'16',m:'0',tm:'04:00 PM'});
prsViewArray.push({idk:'65',h:'16',m:'15',tm:''});
prsViewArray.push({idk:'66',h:'16',m:'30',tm:''});
prsViewArray.push({idk:'67',h:'16',m:'45',tm:''});
prsViewArray.push({idk:'68',h:'17',m:'0',tm:'05:00 PM'});
prsViewArray.push({idk:'69',h:'17',m:'15',tm:''});
prsViewArray.push({idk:'70',h:'17',m:'30',tm:''});
prsViewArray.push({idk:'71',h:'17',m:'45',tm:''});
prsViewArray.push({idk:'72',h:'18',m:'0',tm:'06:00 PM'});
prsViewArray.push({idk:'73',h:'18',m:'15',tm:''});
prsViewArray.push({idk:'74',h:'18',m:'30',tm:''});
prsViewArray.push({idk:'75',h:'18',m:'45',tm:''});
prsViewArray.push({idk:'76',h:'19',m:'0',tm:'07:00 PM'});
prsViewArray.push({idk:'77',h:'19',m:'15',tm:''});
prsViewArray.push({idk:'78',h:'19',m:'30',tm:''});
prsViewArray.push({idk:'79',h:'19',m:'45',tm:''});
prsViewArray.push({idk:'80',h:'20',m:'0',tm:'08:00 PM'});
prsViewArray.push({idk:'81',h:'20',m:'15',tm:''});
prsViewArray.push({idk:'82',h:'20',m:'30',tm:''});
prsViewArray.push({idk:'83',h:'20',m:'45',tm:''});
prsViewArray.push({idk:'84',h:'21',m:'0',tm:'09:00 PM'});
prsViewArray.push({idk:'85',h:'21',m:'15',tm:''});
prsViewArray.push({idk:'86',h:'21',m:'30',tm:''});
prsViewArray.push({idk:'87',h:'21',m:'45',tm:''});
prsViewArray.push({idk:'88',h:'22',m:'0',tm:'10:00 PM'});
prsViewArray.push({idk:'89',h:'22',m:'15',tm:''});
prsViewArray.push({idk:'90',h:'22',m:'30',tm:''});
prsViewArray.push({idk:'91',h:'22',m:'45',tm:''});
prsViewArray.push({idk:'92',h:'23',m:'0',tm:'11:00 PM'});
prsViewArray.push({idk:'93',h:'23',m:'15',tm:''});
prsViewArray.push({idk:'94',h:'23',m:'30',tm:''});
prsViewArray.push({idk:'95',h:'23',m:'45',tm:''});

function getItemData(tItem,idVal){
    var prItemData = getProviderItemData(tItem,idVal);
    if(prItemData){
        //console.log(prItemData);
        prItemData.color = "green";
        return prItemData;
    }
    return null;
}
function getProviderItemData(tItem,idVal){
    var data = "";
    var ds = providerRosterArray;
    for(var p=0;p<ds.length;p++){
        var pItem = ds[p];
        if(pItem  &&  (pItem.weekDayStart == idVal || pItem.weekDayEnd == idVal)){
            var hours = Number(tItem.h);
            hours = hours*3600;

            var minutes = Number(tItem.m);
            minutes = minutes*60;

            var currMS = (hours+minutes);
            var stTime = Number(pItem.fromTime)*60;
            stTime = Number(stTime);

            var etTime = Number(pItem.toTime)*60;
            etTime = Number(etTime);

            if(pItem.weekDayEnd!= idVal){
                etTime = (23*3600)+(45*60);//-stTime;//(2*3600)+30*60);
            }

            if(pItem.weekDayStart != idVal){
                stTime = 0;//stTime-2000;//(2*3600)+30*60);
            }

            etTime = etTime-(15*60);

            if(currMS>=stTime && currMS<=etTime){ //&& tItem.h<=et.getHours()
                var sDate = new Date();//item.startDateTime);
                var eDate = new Date();
                var strData = {};
                sDate.setHours(0);
                sDate.setMinutes(0);
                sDate.setSeconds(0);
                sDate.setSeconds(pItem.fromTime*60);

                eDate.setHours(0,0,0,0);
                eDate.setSeconds(pItem.toTime*60);
                var st = new Date(sDate);
                var et = new Date(eDate);
                strData.st = kendo.toString(st,"hh:mm tt");//pItem.fromTimeK;
                strData.et = kendo.toString(et,"hh:mm tt");;//pItem.toTimeK;
                var provider = getProviderDetails(tItem,idVal);
                strData.provider = provider;
                return strData;
            }
        }
    }
    return null;
}


function getProviderDetails(tItem,idVal){
    var data = "";
    var ds = providerRosterArray;
    var strData = "";
    for(var p=0;p<ds.length;p++){
        var pItem = ds[p];
        if(pItem  && (pItem.weekDayStart == idVal || pItem.weekDayEnd == idVal)){
            var hours = Number(tItem.h);
            hours = hours*3600;

            var minutes = Number(tItem.m);
            minutes = minutes*60;

            var currMS = (hours+minutes);
            var stTime = Number(pItem.fromTime)*60;
            stTime = Number(stTime);

            var etTime = Number(pItem.toTime)*60;
            etTime = Number(etTime);

            if(currMS>=stTime && currMS<=etTime){ //&& tItem.h<=et.getHours()
                strData = strData+pItem.provider+",";
            }
        }
    }
    return strData;
}

function getServiceUsersByProviders(){
    var suArray = getAllServiceUsers();
    var strTable = "<table style='width: 100%;'>";
    strTable = strTable+"<tr>";
    strTable = strTable+"<th>Staff</th>";
    for(var i=0;i<towArr.length;i++){
        strTable = strTable+"<th>"+towArr[i].Value+"</thcla>";
    }
    strTable = strTable+"<th>Total</th>";
    strTable = strTable+"</tr>";
    var providerList = suArray;
    careerTotal = 0;
    if(providerList.length>0){
        for(var j=0;j<providerList.length;j++){
            strTable = strTable+"<tr>";
            strTable = strTable+"<td>"+providerList[j]+"</td>";
            var total = 0;
            for(var k=0;k<towArr.length;k++){
                var hours = getProviderHours(towArr[k].Key,providerList[j]);
                total = total+Number(hours);
                strTable = strTable+"<td >"+convertMinsToHrsMins(hours)+"</td>";
            }
            careerTotal =  careerTotal + total;
            strTable = strTable+"<td>"+convertMinsToHrsMins(total)+"</td>";
            strTable = strTable+"</tr>";
        }
    }
    $("#sncareerTotal").html("Week Totals : " + convertMinsToHrsMins(careerTotal));


    strTable = strTable+"</table>";
    $("#divCareerTable").html(strTable);
}


function getAllServiceUsers(){
    var arr = [];
    for(var p=0;p<providerRosterArray.length;p++){
        var pItem = providerRosterArray[p];
        if (pItem) {
            var SUName;
            if (pItem && pItem.provider) {
                SUName = pItem.provider;
            } else if (pItem && pItem.composition && pItem.composition.provider) {
                var tempPatient = pItem.composition.provider;
                SUName = tempPatient.lastName + " " + tempPatient.firstName + " " + (tempPatient.middleName != null ? tempPatient.middleName : "");
                SUName = SUName.trim();
            }

            if (arr.length == 0) {
                arr.push(SUName);
            }
            if (arr.indexOf(SUName) == -1) {
                arr.push(SUName);
            }
        }
    }
    return arr;
}

function getProviderHours(day,pr){
    var ds = providerRosterArray
    var strData = "";
    var arrProvider = [];
    var total = 0;
    for(var p=0;p<ds.length;p++){
        var pItem = ds[p];
        if(pItem  && pItem.provider == pr && pItem.weekDayStart == day){
            var ft = pItem.fromTime;
            var tt = pItem.toTime;
            var diff = tt-ft;
            total = total+diff;
        }
    }
    return total;
}


function onGetHeadDate(k){
    var day = allDatesInWeek[k].getDate();
    var month = allDatesInWeek[k].getMonth();
    month = month+1;
    var date;
    if (cntry.indexOf("India") >= 0) {
        date = day+"/"+month;
    } else if (cntry.indexOf("United Kingdom") >= 0) {
        date = day+"/"+month;
    } else {
        date = month+"/"+day;
    }
    return date;
}
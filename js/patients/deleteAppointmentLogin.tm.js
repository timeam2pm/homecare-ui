var parentRef = null;
var operation = "";
var selItem = null;
var ADD = "add";
var UPDATE = "edit";
var VIEW = "view";
var DELETE = "delete";
var patientId = "";
var rs = "";
var selRSItem = null;

var towArr = [{Key:'0',Value:'Sunday'},{Key:'1',Value:'Monday'},{Key:'2',Value:'Tuesday'},{Key:'3',Value:'Wednesday'},{Key:'4',Value:'Thursday'},{Key:'5',Value:'Friday'},{Key:'6',Value:'Saturday'}];

var photoExt = "";
var pid = "";
var pname = "";

var fid = "";
var fname = "";
var selList = [];
var prsViewArray = [];
var loginPassword;

$(document).ready(function() {
    $("#kendoWindowContainerWindw_wnd_title",parent.document).css("top","0px");
    parentRef = parent.frames['iframe'].window;
    var pnlHeight = window.innerHeight;
    appointmentLogin();
});

$(window).load(function(){
    parentRef = parent.frames['iframe'].window;
    buttonEvents();
});

function buttonEvents(){

    $("#btnSave").off("click");
    $("#btnSave").on("click",onClickOK);

    $("#btnCancelDet").off("click");
    $("#btnCancelDet").on("click",onClickCancel);
}


function onClickOK(){
    var password = $("#txtPassword").val();
    if(password != null && password != ""){
        if(password==loginPassword){
            var popW = "85%";
            var popH = "80%";
            // var profileLbl;
            // var devModelWindowWrapper = new kendoWindowWrapper();
            // profileLbl = "Delete Appointments";

            var profileLbl;
            var devModelWindowWrapper = new kendoWindowWrapper();
            profileLbl = "Staff Appointments";
            // parentRef.staffFacilityId = staffFacilityId;
            // parentRef.staffId = staffId;
            // parentRef.leaveFromDate = leaveFromDate;
            // parentRef.leaveToDate = leaveToDate;
            devModelWindowWrapper.openPageWindow("../../html/masters/deleteAppointmentList.html", profileLbl, popW, popH, true, onDeleteAppointmentClose);
            // devModelWindowWrapper.openPageWindow("../../html/masters/staffAppointment.html", profileLbl, popW, popH, true, onDeleteAppointmentClose);


        }
        else{
            customAlert.error("Error", "Password is incorrect");
        }
    }
    else{
        customAlert.error("Error", "Please enter password");
    }
}

function onDeleteAppointmentClose(){
    $("#txtPassword").val("");
}


function onClickCancel() {
    var obj = {};
    obj.status = "true";
    popupClose(obj);
}


function popupClose(obj) {
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}


function appointmentLogin(){
    getAjaxObject(ipAddress+"/homecare/settings/?id=3","GET",getValue,onError);
}

function getValue(dataObj){
    var tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.settings){
            if($.isArray(dataObj.response.settings)){
                tempCompType = dataObj.response.settings;
            }else{
                tempCompType.push(dataObj.response.settings);
            }
        }
        if(tempCompType.length > 0){
            loginPassword=tempCompType[0].value;
        }
    }
}
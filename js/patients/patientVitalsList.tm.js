var parentRef = null;

var patientId = "";
var ptAge  = "";
var ptGender = "";

$(document).ready(function(){
});


$(window).load(function(){
	loading = false;
	$(window).resize(adjustHeight);
onLoaded();
	if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
	parentRef = parent.frames['iframe'].window;
	init();
    buttonEvents();
	adjustHeight();
}

function init(){
	patientId = parentRef.patientId;
	ptAge = parentRef.ptAge;
	ptGender = parentRef.ptGender;
	getVitals();
	//getAjaxObject(ipAddress+"/hbt/getPatientData/"+patientId,"GET",onGetHBTPatientData,onError);
	//getAjaxObject(ipAddress+"/patient/reports/holter/"+patientId,"GET",onGetHolterReportsData,onError);
}
function onGetHBTPatientData(dataObj){
	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		
	}
}
function getVitals(){
	getAjaxObject(ipAddress+"/homecare/patient-vitals/?patient-id="+patientId+"&is-active=1&is-deleted=0&fields=*,vital.*","GET",onGetVitalsSuccess,onError);
}
function getVitalInfo(vName,dt){
	for(var i=0;i<vitalDataArray.length;i++){
		var item = vitalDataArray[i];
		if(item.vital == vName && item.date == dt){
			return item.value;
		}
	}
	return "";
}
function mycomparator(a,b) {
	  return Number(a.readingDate) - Number(b.readingDate);
	}
function formatAMPM(date) {
	  var hours = date.getHours();
	  var minutes = date.getMinutes();
	  var ampm = hours >= 12 ? 'PM' : 'AM';
	  hours = hours % 12;
	  hours = hours ? hours : 12; // the hour '0' should be '12'
	  minutes = minutes < 10 ? '0'+minutes : minutes;
	  var strTime = hours + ':' + minutes + ' ' + ampm;
	  return strTime;
	}
var vitalDataArray = [];
function onGetVitalsSuccess(dataObj){
	var vitalsArray = [];
	if(dataObj && dataObj.response && dataObj.response.patientVitals){
		if($.isArray(dataObj.response.patientVitals)){
			vitalsArray = dataObj.response.patientVitals;
		}else{
			vitalsArray.push(dataObj.response.patientVitals);
		}
	}
	vitalDataArray = vitalsArray;
	//var headers = getVitalHeaders();
	//console.log(headers);
	$("#divVitals").html("");
	var strTable = '<table class="callPatientTbl patientActivityTbl">';
	strTable = strTable+'<thead>';
	strTable = strTable+'<tr>';
	strTable = strTable+'<th>Vital ID</th>';
	strTable = strTable+'<th>Abbreviation</th>';
	strTable = strTable+'<th>Description</th>';
	strTable = strTable+'<th>Range From</th>';
	strTable = strTable+'<th>Range To</th>';
	strTable = strTable+'</tr>';
	strTable = strTable+'</thead>';
	strTable = strTable+'<tbody>';
	
	for(var i=0;i<vitalDataArray.length;i++){
		var dataItem = vitalDataArray[i];
		if(dataItem){
			//console.log(dataItem);
			var className = "success";
			if(i%2 == 0){
				className = "danger";
			}
			if(i%3 == 0){
				className = "info";
			}
			if(i%3 == 0){
				className = "active";
			}
			//console.log(timiningArray);
			className = getRowColors(i);
			strTable = strTable+'<tr>';
			ptAge = Number(ptAge);
			
			var rFrom = "";
			var rTo = "";
			if(ptAge<=10){
				rFrom = dataItem.vitalPediatricRangeFrom;
				rTo = dataItem.vitalPediatricRangeTo;
			}else if(ptGender == "Male"){
				rFrom = dataItem.vitalMaleRangeFrom;
				rTo = dataItem.vitalMaleRangeTo;
			}else{
				rFrom = dataItem.vitalFemaleRangeFrom;
				rTo = dataItem.vitalFemaleRangeTo;
			}
			
			strTable = strTable+'<td>'+dataItem.id+'</td>';
			strTable = strTable+'<td>'+dataItem.vitalAbbreviation+'</td>';
			strTable = strTable+'<td>'+dataItem.vitalDescription+'</td>';
			strTable = strTable+'<td>'+rFrom+'</td>';
			strTable = strTable+'<td>'+rTo+'</td>';
			/*strTable = strTable+'<td class="textAlign">'+dataItem.vitalMaleRangeFrom+'</td>';
			strTable = strTable+'<td class="textAlign">'+dataItem.vitalMaleRangeTo+'</td>';
			strTable = strTable+'<td class="textAlign">'+dataItem.vitalFemaleRangeFrom+'</td>';
			strTable = strTable+'<td class="textAlign">'+dataItem.vitalFemaleRangeTo+'</td>';
			strTable = strTable+'<td class="textAlign">'+dataItem.vitalPediatricRangeFrom+'</td>';
			strTable = strTable+'<td class="textAlign">'+dataItem.vitalPediatricRangeTo+'</td>';*/
			
			//strTable = strTable+'<td>'+strAction+'</td>';
			strTable = strTable+'</tr>';
		}
	}
	strTable = strTable+'</tbody>';
	strTable = strTable+'</table>';
	$("#divVitals").append(strTable);
}
function getRowColors(index) {
    var classes = ['active', 'success', 'info', 'warning', 'danger'];
   // console.log(index);
    if (index % 2 === 0 && index / 2 < classes.length) {
    	//console.log(classes[index / 2]);
    	return classes[index / 2];
    	//console.log(index+','+classes[index / 2]);
      /*  return {
            classes: classes[index / 2]
        };*/
    }
    return "";
}
function onError(errorObj){
	console.log(errorObj);
}
function getVitalHeaders(){
	vitalDataArray.sort(mycomparator);
	var vitalArr = [];
	for(var i=0;i<vitalDataArray.length;i++){
		var item = vitalDataArray[i];
		var rd = Number(item.readingDate);
		var dtOffsetDate = new Date();
		var offset = dtOffsetDate.getTimezoneOffset();
		offset = offset*60*1000;
		rd = rd+Math.abs(offset);
		var dt = new Date(rd);
		var strDT = kendo.toString(dt,"MM-dd-yyyy");
		strDT = strDT+" "+formatAMPM(dt);
		item.date = strDT;
		if(vitalArr.indexOf(strDT)<0){
			vitalArr.push(strDT);
		}
	}
	if(vitalArr.length>0){
		vitalArr = vitalArr.reverse();
	}
	return vitalArr;
}
var holterReportDataArray = [];
function onGetHolterReportsData(dataObj){
		$("#divVitals").text("");
		var dataArray = [];
		if(dataObj){
			if($.isArray(dataObj)){
				dataArray = dataObj;
			}else{
				dataArray.push(dataObj);
			}
		}
		holterReportDataArray = dataArray;
		var strTable = '<table class="callPatientTbl patientActivityTbl">';
		strTable = strTable+'<thead>';
		strTable = strTable+'<tr>';
		//strTable = strTable+'<th>Patient ID</th>';
		strTable = strTable+'<th>Date Generated</th>';
		strTable = strTable+'<th >File Name</th>';
		strTable = strTable+'<th >File Type</th>';
		strTable = strTable+'</tr>';
		strTable = strTable+'</thead>';
		strTable = strTable+'<tbody>';
		for(var i=0;i<holterReportDataArray.length;i++){
			var dataItem = holterReportDataArray[i];
			if(dataItem){
				//console.log(dataItem);
				var className = getRowColors(i);
				strTable = strTable+'<tr class="'+className+'">';
				//strTable = strTable+'<td>'+dataItem.patientId+'</td>';
				strTable = strTable+'<td>'+kendo.toString(new Date(dataItem.createdDate),"MM-dd-yyyy h:mm:ss")+'</td>';
				strTable = strTable+'<td>'+dataItem.fileName+'</td>';
				var dietId = dataItem.id;
				if(dataItem.fileType == "mp4"){
					strTable = strTable+'<td><img id="'+dietId+'" src="../../img/AppImg/HosImages/video.png"  class="cusrsorStyle videoIcon" onClick="onClickGenerate(event)"></td>';
				}else if(dataItem.fileType == "pdf"){
					strTable = strTable+'<td><img  id="'+dietId+'" src="../../img/AppImg/HosImages/pdf.png"    class="cusrsorStyle pdfIcon" onClick="onClickHolterReport(event)"></td>';
				}
				strTable = strTable+'</tr>';
			}
		}
		strTable = strTable+'</tbody>';
		strTable = strTable+'</table>';
		$("#divVitals").append(strTable);
	}
function buttonEvents(){
	$("#btnCancelDet").off("click",onClickCancel);
	$("#btnCancelDet").on("click",onClickCancel);
	
	$("#btnAdd").off("click");
	$("#btnAdd").on("click",onClickAddDiet);
	
	$("#btnEntry").off("click");
	$("#btnEntry").on("click",onClickDataEntry);
	
	$("#btnReport").off("click");
	$("#btnReport").on("click",onClickReport);
	
}

function onClickAddDiet(){
	var popW = "74%";
    var popH = "70%";

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    parentRef.patientId = patientId;
    devModelWindowWrapper.openPageWindow("../../html/patients/patientVitalAddList.html", "Vitals Attach", popW, popH, true, closeDietExcersizeList);
}
function closeDietExcersizeList(evt,returnData){
	if(returnData && returnData.status == "success"){
		customAlert.info("info", "Vital(s) attached/removed successfully.");
		init();
	}
}

function onClickDataEntry(){
	var popW = "75%";
    var popH = "60%";

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    parentRef.patientId = patientId;
    devModelWindowWrapper.openPageWindow("../../html/patients/patientVitalEntryList.html", "Vitals Entry", popW, popH, true, closeDietExcersizeList1);
}
function closeDietExcersizeList1(evt,res){
	
}
function onClickReport(){
	var popW = "65%";
    var popH = "87%";

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    parentRef.patientId = patientId;
    devModelWindowWrapper.openPageWindow("../../html/patients/patientVitalReportList.html", "Vitals Report", popW, popH, true, closeDietExcersizeList1);
}
function closeDietExcersizeList1(evt,res){
	
}
function adjustHeight(){
	var defHeight = 60;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    //$("#divPtHolter").height(cmpHeight);
	//angularUIgridWrapper.adjustGridHeight(cmpHeight);
}


function onClickPatientCall(obj){
	var popW = "60%";
    var popH = 400;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Patient Call Chart";
    parentRef.selObj = obj;
    devModelWindowWrapper.openPageWindow("../../html/patients/callPatient.html", profileLbl, popW, popH, false, closeCallPatient);
}
function closeCallPatient(evt,returnData){
	if(returnData && returnData.status == "success"){
		customAlert.info("Save","Patient Call Chart Created Successfully");
		init();
	}
}
function onClickCancel(){
	var obj = {};
	var windowWrapper = new kendoWindowWrapper();
	windowWrapper.closePageWindow(obj);
}

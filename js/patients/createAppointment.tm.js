var angularUIgridWrapper;
var angularUIgridUnavWrapper;
var angularUISelgridWrapper;

var angularPTUIgridWrapper = null;
var facilityArr = [{Key:'Facility1',Value:'Facility1'},{Key:'Facility2',Value:'Facility2'}];

var doctorArr = [{Key:'Doctor1',Value:'Doctor1'},{Key:'Doctor2',Value:'Doctor2'}];

var appTypeArr = [{Key:'Type1',Value:'Type1'},{Key:'Type2',Value:'Type2'}];

var towArr = [{Key:'1',Value:'1'},{Key:'2',Value:'2'},{Key:'3',Value:'3'},{Key:'4',Value:'4'},{Key:'5',Value:'5'},{Key:'6',Value:'6'},{Key:'7',Value:'7'}];
var colors = ['red', 'blue', 'green', 'teal', 'rosybrown', 'tan', 'plum', 'saddlebrown'];
var daysOfWeek = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

var DAY_VIEW = "DayView";
var appTypeArray = [];
var reasonArray = [];

var dArray = [];
var parentRef = null;

var startDT  = null;
var endDT = null;

var flatAppointments = [];
var allDatesInWeekUnAvailable = [];
var APIResponse = [];
// var appointid ;
var staffId;
var IsAlertPopup = 0;
var radioValue = "1";
var staffStDateTime;
var staffEdDateTime;
var staffAppointments = [], appointmentsArr = [];
var max;
var providersArray = [];
var appointmentdtFMT = "dd/MM/yyyy hh:mm tt";
var copiedAppointment = {};
var weeklyCopiedAppointment = {};
var weeklyAppointments = [];

$(document).ready(function(){

	var cntry = sessionStorage.countryName;
	if((cntry.indexOf("India")>=0) || (cntry.indexOf("United Kingdom")>=0)){
		$("#lblFacility").text("Location");
		$("#lblEditFacility").text("Location :");
		$("#lblPRFacility2").text("Location :");
		$("#lblStFacility").text("Location :");
		$("#lblPRSFacility").text("Location :");
		$("#lbl2dFacility").text("Location :");
		$("#lblPTFacility").text("Location :");
		$("#lblAppPTFacility").text("Location :");
		$("#lblStaffWeeklyViewFacility").text("Location :");
		$("#lblStaffApptReportFacility").text("Location :");
		$("#lblPopupFacility").text("Location :");


	}else if(cntry.indexOf("United State")>=0){
		$("#lblFacility").text("Facility");
		$("#lblEditFacility").text("Facility :");
		$("#lblPRFacility2").text("Facility :");
		$("#lblStFacility").text("Facility :");
		$("#lblPRSFacility").text("Facility :");
		$("#lbl2dFacility").text("Facility :");
		$("#lblPTFacility").text("Facility :");
		$("#lblAppPTFacility").text("Facility :");
		$("#lblStaffWeeklyViewFacility").text("Facility :");
		$("#lblStaffApptReportFacility").text("Facility :");
		$("#lblPopupFacility").text("Facility :");
	}
});

var viewModel = null;
$(window).load(function(){

    if (prViewDateArray != null && prViewDateArray.length > 0) {
    for (var i = 0; i < prViewDateArray.length; i++) {
        prViewDateArray[i].idk = (i + 1);
    }
}

	viewModel = kendo.observable({
        selectedProduct: null,
        isPrimitive: false,
        isVisible: true,
        isEnabled: true,
        products: new kendo.data.DataSource({
        	data: [
        	       { value: "Jane Doe" ,text:"wwwww"},
        	       { value: "John Doe",text:"ggggggggg" }
        	     ]
        })
    });
	if(sessionStorage.clientTypeId == "2"){
		$("#lblAppByPatient").text("Appointment By Service User");
		$("#lblPTT").text("Service User :");
		$("#lblPTR").text("Service User :");
	}
	parentRef = parent.frames['iframe'].window;
	onMessagesLoaded();
});

function onFacilityChange(e){

}
function combobox_change(){
	var cmbFacility = $("#cmbFacility option:selected").val();
	var facilityId = Number(cmbFacility);
	if (facilityId > 0) {
		getAjaxObject(ipAddress+"/provider/list/?is-active=1&facility-id="+facilityId,"GET",getProviderListToCreateAppt,onError);
	}
}

function getProviderListToCreateAppt(dataObj){
	var dataArray = [];
	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		if($.isArray(dataObj.response.provider)){
			dataArray = dataObj.response.provider;
		}else{
			dataArray.push(dataObj.response.provider);
		}
	}else{
		//customAlert.error("Error",dataObj.response.status.message);
	}
	var tempDataArry = [];
	for(var i=0;i<dataArray.length;i++){
		dataArray[i].idk = dataArray[i].id;
		dataArray[i].psName = dataArray[i].lastName+" "+dataArray[i].firstName;
		dataArray[i].Status = "InActive";
		if(dataArray[i].isActive == 1){
			dataArray[i].Status = "Active";
		}
	}

	dataArray.sort(function (a, b) {
		var p1 = a.lastName.toLowerCase();
		var p2 = b.lastName.toLowerCase();
		var o1, o2;
		o1 = a.firstName.toLowerCase();
		o2 = b.firstName.toLowerCase();

		if (p1 < p2) return -1;
		if (p1 > p2) return 1;

		if (o1 < o2) return -1;
		if (o1 > o2) return 1;
		return 0;
	});
	twoDProviderAttay  = JSON.parse(JSON.stringify(dataArray));
	providerArray = dataArray;
	// setDataForSelection(dataArray, "cmbDoctor", onFacilityChange, ["psName", "idk"], 0, "");
	// setDataForSelection(dataArray, "cmbPtDoctor", onFacilityChange, ["psName", "idk"], 0, "");
	// setDataForSelection(dataArray, "txtRProvider", onFacilityChange, ["psName", "idk"], 0, "");

	// setDataForSelection(dataArray, "txt2dProvider", on2DProviderChange, ["psName", "idk"], 0, "");
	// on2DProviderChange();


	$("#cmbProvider").empty();

	var cmbId = "cmbProvider";
	for (var i = 0; i < dataArray.length; i++) {
		if (cmbId != '') {
			var staffName = dataArray[i].lastName+ " " + dataArray[i].firstName;
			$("#" + cmbId).append('<option value="' + dataArray[i].id + '">' + staffName + '</option>');
		}
	}

	$("#cmbProvider").multipleSelect({
		selectAll: false,
		width: 200,
		dropWidth: 200,
		multipleWidth: 200,
		placeholder: 'Select Staff'
		// selectAllText: 'All'

	});


	//<input type='checkbox'/>
	// var required = $("#required").kendoMultiSelect({
	//     itemTemplate: "<div style='width:100px;float:left'>#:data.lastName#  #:data.firstName# </div>",
	//     autoClose: false,
	//     dataSource: dataArray,
	//     dataTextField: "psName",
	//     dataValueField: "idk",
	//     dataBound: function() {
	//       var items = this.ul.find("li");
	//       setTimeout(function() {
	//         //checkInputs(items);
	//       });
	//     },
	//     change: function() {
	//      /* var items = this.ul.find("li");
	//       checkInputs(items);*/
	//     	var ms1 = $("#required").getKendoMultiSelect();
	//         var values = this.value();//distinctValues(this.value());
	//        // console.log(values);
	//     }
	//   }).data("kendoMultiSelect");

}

function getColor(data) {
    if (data.title === "Mark A Henry") {
      return "Pink";
    } else {
      return "orange";
    }
  }
function adjustHeight(){
	var defHeight = 260;
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 100;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    try{angularPTUIgridWrapper.adjustGridHeight(cmpHeight);}catch(e){};

    if(angularUIgridWrapper){
    	angularUIgridWrapper.adjustGridHeight((cmpHeight-20));
    }
	if(angularUISelgridWrapper){
		angularUISelgridWrapper.adjustGridHeight((cmpHeight-20));
	}

    $("#prscheduler1").css({ height: (cmpHeight + 90) + 'px' });
    $("#prscheduler3").css({ height: (cmpHeight + 90) + 'px' });
    $("#prscheduler").css({ height: (cmpHeight + 80) + 'px' });
    //$("#prscheduler4").css({ height: (cmpHeight + 80) + 'px' });
    $("#prscheduler2").css({ height: (cmpHeight + 90) + 'px' });
    $("#prsscheduler").css({ height: (cmpHeight + 80) + 'px' });
    $("#prschedulerH").css({ height: (cmpHeight + 80) + 'px' });
    $("#prschedulerH2").css({ height: (cmpHeight + 80) + 'px' });
    $("#pr2dView").css({ height: (cmpHeight + 40) + 'px' });
    $("#pr2dView1").css({ height: (cmpHeight + 40) + 'px' });
    $("#SUscheduler").css({ height: (cmpHeight + 40) + 'px' });
    $("#SUscheduler1").css({ height: (cmpHeight + 40) + 'px' });
    $("#SUschedulerApp").css({ height: (cmpHeight + 40) + 'px' });
}
function onMessagesLoaded() {

	init();
	buttonEvents();
}

var stDate = "";
var endDate = "";
var strDoc = "";

function scrollToHour(hour) {
    var time = new Date();
    time.setHours(hour);
    time.setMinutes(0);
    time.setSeconds(0);
    time.setMilliseconds(0);

    var scheduler = $("#scheduler").data("kendoScheduler");
    var contentDiv = scheduler.element.find("div.k-scheduler-content");
    var rows = contentDiv.find("tr");

    for (var i = 0; i < rows.length; i++) {
      var slot = scheduler.slotByElement(rows[i]);
	if(slot.startDate) {
		var slotTime = kendo.toString(slot.startDate, "HH:mm");
		var targetTime = kendo.toString(time, "HH:mm");

		if (targetTime === slotTime) {
			scheduler.view()._scrollTo($(rows[i]).find("td:first")[0], contentDiv[0]);
		}
	}
    };
  }

function getColorBasedOnHour(date) {
    /*var difference = date.getTime() - kendo.date.getDate(date);
    var hours = difference / kendo.date.MS_PER_HOUR;

    if (hours >= 0 && hours <3) {
      return "#e8e0e0";
    } else if (hours >= 13 && hours < 17) {
    } */
	return "#FFF";
  }
var cntry = "";
var dtFMT = "dd/MM/yyyy";


function getPatientBillData(e) {
	if(patientId != ""){
		getPatientData(e, 0);
	}
  }
var patientBillArray = [];
function getPatientData(e) {
	patientBillArray = [];
	getAjaxObject(ipAddress+"/carehome/patient-billing/?is-active=1&patientId="+patientId+"&fields=*,shift.*,travelMode.*,billingType.*,billingPeriod.*,billTo.name,billingRateType.code","GET",function(dataObj){
		var tempCompType = [];
	    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
	        if(dataObj.response.patientBilling){
	            if($.isArray(dataObj.response.patientBilling)){
	                tempCompType = dataObj.response.patientBilling;
	            }else{
	                tempCompType.push(dataObj.response.patientBilling);
	            }
	        }
	    }
	    for(var q=0;q<tempCompType.length;q++){
	    	var qItem = tempCompType[q];
	    	if(qItem){
	    		qItem.value = qItem.billToname;
	    		qItem.text = qItem.billToname;
	    		patientBillArray.push(qItem);
	    	}
	    }
			e.success(patientBillArray);
	},onError);
 }

function getAssigneeResourceData(e) {
    getData(e, 0);
  }


function getReasonData(e) {
	getAjaxObject(ipAddress+"/master/appointment_reason/list/?is-active=1","GET",function(dataObj){
		 var dtArray = getTableListArray(dataObj);
			//console.log(dtArray);
			var dArray = [];
			for(var d=0;d<dtArray.length;d++){
				var dItem = dtArray[d];
				if(dItem){
					var obj = {};
					obj.text = dItem.desc;
					obj.value = dItem.value;
					dArray.push(obj);
				}
			}
			e.success(dArray);
	},onError);
 }
  function getData(e, index) {
  	// var scheduler = $("#scheduler").data('kendoScheduler');
     //var resourcesData = scheduler.resources[index].dataSource.view();
	  getAjaxObject(ipAddress+"/master/appointment_type/list/?is-active=1","GET",function(dataObj){
		  var dtArray = getTableListArray(dataObj);
			//console.log(dtArray);
			var dArray = [];
			for(var d=0;d<dtArray.length;d++){
				var dItem = dtArray[d];
				if(dItem){
					var obj = {};
					obj.text = dItem.desc;
					obj.value = dItem.value;
					dArray.push(obj);
				}
			}
			 /*dArray.push({ text: "Confirmed", value: "Con"});
			 dArray.push({ text: "Finished", value: "Fin"});
			 dArray.push({ text: "We Cancelled", value: "WeCan"});
			 dArray.push({ text: "Patient Cancelled", value: "PatCan"});*/
		     e.success(dArray);
	  },onError);
		//getAjaxObject(ipAddress+"/master/appointment_reason/list/?is-active=1","GET",getReasonList,onError);
	 //resourcesData.toJSON());
  }
	function init(){
		//patientId = parentRef.patientId;
		//$("#dtFromDate").kendoDatePicker({value:new Date()});
		//$("#dtToDate").kendoDatePicker({value:new Date()});
		//console.log($("#ownerId").val())
		$("#txt2dProvider1").kendoComboBox();
		$("#txt2dProvider").kendoComboBox();
		cntry = sessionStorage.countryName;
		 if(cntry.indexOf("India")>=0){
			 dtFMT = INDIA_DATE_FMT;
		 }else  if(cntry.indexOf("United Kingdom")>=0){
			 dtFMT = ENG_DATE_FMT;
		 }else  if(cntry.indexOf("United State")>=0){
			 dtFMT = US_DATE_FMT;
		 }
		 if(cntry.indexOf("India")>=0){

		 }else{
			 $("#liAppByPatient").hide();
			 $("#liRoster").hide();
		 }
		$("#txtStartDate").kendoDatePicker({format:dtFMT,value:new Date()});
		$("#txtEndDate").kendoDatePicker({format:dtFMT});
        $("#txtPRDate").kendoDatePicker({ format: dtFMT, value: new Date() });
        $("#txtPRDate2").kendoDatePicker({ format: dtFMT, value: new Date(),change:onChangePRDate2 });
        $("#txtSTPRDate").kendoDatePicker({format:dtFMT,value:new Date(),change:onChangeSTPRDate });
		$("#txtPRSDate").kendoDatePicker({format:dtFMT,value:new Date(),change:onChangePRSDate});

		$("#txt2dDate").kendoDatePicker({format:dtFMT,value:new Date()});

		$("#txtPTDate").kendoDatePicker({format:dtFMT,value:new Date(),change:onChangePTDate});
        $("#txtStaffWeeklyViewDate").kendoDatePicker({ format: dtFMT, value: new Date() });
        $("#txtStaffApptFromDate").kendoDatePicker({ format: dtFMT, value: new Date() });
        $("#txtStaffApptToDate").kendoDatePicker({ format: dtFMT, value: new Date() });
		$("#txtAppPTDate").kendoDatePicker({format:dtFMT,value:new Date(),change:onChangeAppPTDate});
		//$("#txt2dDate1").kendoDatePicker({format:dtFMT,value:new Date()});

		startDT = $("#txtStartDate").kendoDatePicker({
	         change: startChange,format:dtFMT
	     }).data("kendoDatePicker");

	     endDT= $("#txtEndDate").kendoDatePicker({
	         change: endChange,format:dtFMT
	     }).data("kendoDatePicker");


	   //  startDT.min(new Date());
	     endDT.min(new Date());

		$('label[for="recurrenceRule"]').parent().hide();
		//$("#cmbFacility").kendoComboBox();
		//$("#txtPTFacility").kendoComboBox();
        //$("#txtAppPTFacility").kendoComboBox();
        //$("#txtStaffWeeklyViewFacility").kendoComboBox();
        //$("#txtStaffApptReportFacility").kendoComboBox();
		//$("#cmbPtFacility").kendoComboBox();
		$("#cmbDoctor").kendoComboBox();
		$("#cmbPtDoctor").kendoComboBox();
		$("#cmbPatient").kendoComboBox();
		$("#cmbAppTypes").kendoComboBox();
		$("#cmbReason").kendoComboBox();
		//$("#txtRFacility").kendoComboBox();
		//$("#txtRPatient").kendoComboBox();
		$("#txtRFecility").kendoComboBox();
		$("#txtRProvider").kendoComboBox();
		$("#txtRDOW").kendoComboBox();
		//$("#txtDuration").kendoComboBox();
		$("#txtRAppType").kendoComboBox();
		$("#txtRReason").kendoComboBox();
		$("#txtStartTime").kendoTimePicker();
		//$("#txtPTDate").kendoTimePicker();
		allowNumerics("txtDuration");
		//$("#cmbAppType").kendoComboBox();

		//setDataForSelection(facilityArr, "cmbFacility", onFacilityChange, ["Value", "Key"], 0, "");
		//setDataForSelection(doctorArr, "cmbDoctor", onFacilityChange, ["Value", "Key"], 0, "");
		setDataForSelection(towArr, "txtRDOW", onFacilityChange, ["Value", "Key"], 0, "");

		getAjaxObject(ipAddress+"/master/appointment_type/list/?is-active=1","GET",getAppointmentList,onError);
		getAjaxObject(ipAddress+"/master/appointment_reason/list/?is-active=1","GET",getReasonList,onError);
        getAjaxObject(ipAddress + "/facility/list/?is-active=1", "GET", getFacilityList, onError);
        getAjaxObject(ipAddress + "/provider/list/?is-active=1&is-deleted=0", "GET", getProvidersList, onError);

		getPatientList();


		var defHeight = 200;
	    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
	        defHeight = 100;
	    }
	    var cmpHeight = window.innerHeight - defHeight;
	    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;

	    var dataOptionsPT = {
		        pagination: false,
		        paginationPageSize: 500,
				changeCallBack: onPTChange
		    }

	    angularPTUIgridWrapper = new AngularUIGridWrapper("rGrid", dataOptionsPT);
	    angularPTUIgridWrapper.init();
		 buildPatientRosterList([]);

	    if(angularPTUIgridWrapper){
	    	angularPTUIgridWrapper.adjustGridHeight(cmpHeight);
	    }
	    // var dataOptions = {
	    //         pagination: false,
	    //         changeCallBack: onAppPatientChange
	    // 	}
	    // 	angularUIgridWrapper = new AngularUIGridWrapper("dgAppPatientList", dataOptions);
	    // 	angularUIgridWrapper.init();
	    // 	buildFileResourceListGrid([]);

	    	// var dataOptions1 = {
	    	//         pagination: false,
	    	//         changeCallBack: onAppProviderChange
	    	// 	}
	    	// 	angularUISelgridWrapper = new AngularUIGridWrapper("dgAppProviderList", dataOptions1);
	    	// 	angularUISelgridWrapper.init();
	    	// 	buildFileResourceSelListGrid([]);





	    adjustHeight();
		$("#scheduler").kendoScheduler({
			 height: cmpHeight,
			 slotTemplate: "<div style='background:#=getColorBasedOnHour(date)#; height: 100%;width: 100%;opacity:0.3'></div>",
			 footer: false,
			  date: new Date(),
			  allDaySlot:false,
			  currentTimeMarker: false,
			  minorTickCount: 1, // display one time slot per major tick
			   minorTick: 15,
			   majorTick:15,
			  messages: {
				    deleteWindowTitle: "Delete Appointment",
				    editable: {
				        confirmation: "Are you sure you want to delete this Appointment?"
				      },
				      editor: {
				          editorTitle: "Create Appointment"
				      }
				  },
			  views: [
			            { type: "day", selected: true },
			            { type: "week" },
			            { type: "month" },
			        ],
			        dataBound: function(e) {
			        	//console.log(e.sender._selectedView.options.name);
			        	DAY_VIEW = e.sender._selectedView.options.name;
			        	if(DAY_VIEW == "DayView"){
			        		$($(".k-scheduler-times")[0]).parent().parent().hide();
			        	}else{
			        		$($(".k-scheduler-times")[0]).parent().parent().show();
			        	}

			        },
			 editable: {
			            template: $("#customEditorTemplate").html(),
			          },
			          resources: [
			                      {
			                          field: "ownerId",
			                          dataSource: [
			                              { text: "Meeting Room 101", value: 1, color: "#6eb3fa" },
			                              { text: "Meeting Room 201", value: 2, color: "#f58a8a" }
			                          ],
			                          title: "Room"
			                      },
			                      {
			                          field: "attendees",
			                          dataSource: [
			                              { text: "Alex", value: 1, color: "#f8a398" },
			                              { text: "Bob", value: 2, color: "#51a0ed" },
			                              { text: "Charlie", value: 3, color: "#56ca85" }
			                          ],
			                          multiple: true,
			                          title: "Attendees"
			                      }
			                  ],
			          schema: {
			              model: {
			                id: "taskId",
			                fields: {
			                  taskId: { from: "TaskID", type: "number" },
			                  title: { from: "Title", defaultValue: "No title", validation: { required: true } },
			                  start: { type: "date", from: "Start" },
			                  end: { type: "date", from: "End" },
			                  startTimezone: { from: "StartTimezone" },
			                  doctor: { type:'string',from: "Doctor" },
			                  dob: { type:'string',from: "dob" },
			                  endTimezone: { from: "EndTimezone" },
			                  description1: { from: "Description1" },
			                  recurrenceId: { from: "RecurrenceID" },
			                  recurrenceRule: { from: "RecurrenceRule" },
			                  recurrenceException: { from: "RecurrenceException" },
			                  ownerId: { from: "OwnerID", defaultValue: 'Confirmed' },
			                  image: { from: "image", defaultValue: "../../img/AppImg/HosImages/patient1.png" },
			                  patientID: { from: "patientID", defaultValue: 1 },
			                  isAllDay: { type: "boolean", from: "IsAllDay" }
			                }
			              }
			            },
			            remove: scheduler_remove,
			            navigate:scheduler_navigate,
			            save: scheduler_save,
			            //cancel: scheduler_cancel,
		            	eventTemplate: $("#event-template").html(),
		            	resizeStart: function(e){
						  //console.log(e.event);
			        	    if(e.event.room){
			        	    	// e.preventDefault();
			        	    }
		            	},
		            	resizeEnd: function(e){
		            		//console.log(e);
		            	},
		            	resize:function(e){
		            		// console.log("resized");
		            	},
			 edit: function(e) {// "week","month",
        	    //console.log(e.event);
        	    if(e.event.room){
        	    	e.preventDefault();
        	    }
			   //patientId = e.event.patientID;

				// var required = $("#required").data("kendoMultiSelect");

				// if(required){
				// 	dArray = [];
				// 	var arr = required.listView._dataItems;
				// 	for(var i=0;i<arr.length;i++){
				// 		var item = arr[i];
				// 		if(item){
				// 			var fName = item.firstName+" "+item.middleName+" "+item.lastName;
				// 			dArray.push(fName);
				// 		}
				// 	}
				// }

				var pdArray = [];
				var values = $('#cmbProvider').multipleSelect('getSelects');

				if (values !== null && values.length > 0) {

					values = $.map(values,function(item) {
						return parseInt(item);
					});

					pdArray = providerArray.filter(function (item, index) {
						return values.indexOf(item.id) >= 0;
					});

					if(pdArray !== null && pdArray.length > 0){

						dArray = [];
						for(var c = 0 ; c < pdArray.length ; c++){
							dArray.push(pdArray[c].firstName+" "+pdArray[c].middleName+" "+pdArray[c].lastName);
						}
					}
				}
				if(dArray.length == 0){
					customAlert.error("Error", "Please select staff");
					e.preventDefault();
				}

				var dt = new Date(e.event.start);
				var cnt = getAppointmentStartCount(dt.getTime());
				//e.sender.select().events[0].id
				 if(DAY_VIEW == "DayView"){
			             if(e.sender && e.sender.select() && e.sender.select().events ){
			        	    	//e.preventDefault();
			        	    	var sEvents = e.sender.select().events;
			        	    	stDate = e.sender.select().start;
			        	    	endDate = e.sender.select().end;
			        	    	if((sEvents.length>0  && sEvents[0].id != "") || patientId != ""){
			        	    		patientId = "";
			        	    		//addAppointment();
			        	    		//return false;
			        	    	}else if(sEvents && (sEvents.length == 0 || sEvents[0].id == "") && patientId == "" && dArray.length>0){
			        	    		e.preventDefault();
			        	    		//patientId = "101";
			                 	   var popW = "60%";
			                 	    var popH = 500;

			                 	    var profileLbl;
			                 	    var devModelWindowWrapper = new kendoWindowWrapper();
			                 	    profileLbl = "Search Service User";
									if(sessionStorage.clientTypeId == "2"){
										profileLbl = "Search Service User";
									}
									parentRef.facilityId = Number($("#cmbFacility option:selected").val());
									if (dArray !== null && dArray.length === 1) {
										devModelWindowWrapper.openPageWindow("../../html/patients/searchPatient.html", profileLbl, popW, popH, true, closeAddAction);
									}
			        	    	}
			        	    }
	        	    }else{
	        	    	 e.preventDefault();
	        	    }
			  },
			  selectable: true,
			});
		    	  var scheduler = $("#scheduler").data("kendoScheduler");
		  		if(scheduler){
		  			scheduler.setOptions({
		  		          minorTick: 15,
		  		          majorTick:15,
		  		          minorTickCount:1
		  		      });
		  		}
		  		setTimeout(function(){
		  			 scheduler.resize(true);
		  			scrollToHour(9);
		    	// scheduler.options.selectable = false;
		    	  //scheduler.options.editable = false;
				},2000);

		     // var arr = {id: 1, start: new Date("11/5/2017 08:00 AM"), end: new Date("11/5/2017 10:00 AM"), title: "Interview",ownerId:"strDoc",facility:"strFacility",doctor:"strDoc"};
		     // scheduler.dataSource.add(arr);

		      var operation = parentRef.operation;
				if(operation == "view"){
					//$("#btnSave").hide();
					/*var providerId = parentRef.providerId;
					var fromDate = parentRef.fromDate;
					provider = parentRef.provider;
					facility = parentRef.facility;
					var strDT = kendo.toString(new Date(fromDate), "dd-MMM-yyyy");

					var patientListURL = ipAddress+"/appointment/by-provider/"+providerId+"/"+strDT+"/1";*/
					 //getAjaxObject(patientListURL,"GET",onPatientListData,onErrorMedication);

					//https://stage.timeam.com/appointment/by-provider/9/04-nov-2017/5
				}else{

				}
		    $(".k-scheduler-times-all-day").each(function () {
		      //  $(this).hide();
		       // $(this).parent().closest('tr').next('tr').hide();
		        //$(this).closest('th').prev('th').attr("rowspan", "1")
		    });

		   // initPatientSchedule();
	}
	var scheduleFlag = false;
	function onClickTabs(e){
		// console.log(e);
        $("#dgridUnAllocatedAppointmentsList").css("display", "none");
        $('link[href="../../css/billing/billingType.css"]').remove();
		var tab8 = $(e.currentTarget).attr("href");
		if(tab8 == "#tab8"){
            IsAlertPopup = 1;
            loadAPi("../../css/billing/billingType.css");
			$("#divPatients").hide();
			$("#SUschedulerApp").show();
			$("#divTab8").show();

			$("#SUschedulerApp").html("");
		}
		setTimeout(function(){
			if(!scheduleFlag){
				initPatientSchedule();
				scheduleFlag = true;
			}
		})
}

function onChangeAppPTDate(){
	debugger;
	onClickUnallocatedAppPTSView();
}

function onChangePTDate(){
	onClickPTSView();
}

function onChangePRSDate(){
	onClickViewStaffApp();
}

function onChangePRDate2(){
    $("#liStaffViewAppt").hide();
    $("#divStaffViewAppt").hide();
	onClickViewProviderApp();
}

function onChangeSTPRDate(){
    $("#divReStaffViewAppt").hide();
	onClickSTViewProviderApp();
}


function getProvidersList(dataObj) {
    var dtArray = [];

    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.provider) {
            if ($.isArray(dataObj.response.provider)) {
                dtArray = dataObj.response.provider;
            } else {
                dtArray.push(dataObj.response.provider);
            }
        }
    }

    var cmbId = "cmbStaff";
    if (dtArray && dtArray.length > 0) {

		dtArray.sort(function (a, b) {
			var p1 = a.lastName+ " " + a.firstName;;
			var p2 = b.lastName+ " " + b.firstName;;;

			if (p1 < p2) return -1;
			if (p1 > p2) return 1;
			return 0;
		});
		$("#cmbWeeklyStaff").append('<option value="0">Un Allocated</option>');
        for (var i = 0; i < dtArray.length; i++) {
            if (cmbId != '') {

                var staffName = dtArray[i].lastName+ " " + dtArray[i].firstName;

                $("#" + cmbId).append('<option value="' + dtArray[i].id + '">' + staffName + '</option>');
				$("#cmbWeeklyStaff").append('<option value="' + dtArray[i].id + '">' + staffName + '</option>');
            }
        }
        //$('#cmbTaskReports').multiselect({
        //    includeSelectAllOption: true
        //});


        $("#cmbStaff").multipleSelect({
            selectAll: true,
            width: 200,
            dropWidth: 200,
            multipleWidth: 200,
            placeholder: 'Select Staff',
            selectAllText: 'All'

        });

		$("#cmbWeeklyStaff").multipleSelect({
			selectAll: true,
			width: 200,
			dropWidth: 200,
			multipleWidth: 200,
			placeholder: 'Select Staff',
			selectAllText: 'All'

		});

        // setMultipleDataForSelection(dtArray, "cmbTaskReports", onTaskReportChange, ["reportDisplayName", "idk"], 0, "");
    }
}

function startChange() {
    var txtStartTime = $("#txtStartTimePopUp").data("kendoDateTimePicker");
    var txtEndTime = $("#txtEndTimePopUp").data("kendoDateTimePicker");
    var startTime = txtStartTime.value();



    if (startTime) {
        var sTime = new Date(startTime);
        var tTime = sTime.getTime();
        tTime = tTime + (15 * 60 * 1000);
        var strTime = new Date(tTime);
        txtEndTime.min(strTime);
        txtEndTime.value(strTime);
        // txtEndTime.max(today1);
        //end.value(startTime);
    }

}
	function endChange(){

	}
	function getPatientList(){
		var patientListURL = ipAddress+"/patient/list/"+sessionStorage.userId;
		if(sessionStorage.userTypeId == "200" || sessionStorage.userTypeId == "100"){
			patientListURL = ipAddress+"/patient/list/?is-active=1&";
		}else{

		}
	getAjaxObject(patientListURL,"GET",onPTPatientListData,onErrorMedication);
	}
	function onPTPatientListData(dataObj){
		var tempArray = [];
		var dataArray = [];
		if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
			if(dataObj.response.patient){
				if($.isArray(dataObj.response.patient)){
					tempArray = dataObj.response.patient;
				}else{
					tempArray.push(dataObj.response.patient);
				}
			}
		}else{
			var msg = dataObj.response.status.message;
			customAlert.error("Error",msg.replace("patient","service user"));
		}

		tempArray.sort(function (a, b) {
			var p1 = a.lastName+ " " + a.firstName;
			var p2 = b.lastName+ " " + b.firstName;

			if (p1 < p2) return -1;
			if (p1 > p2) return 1;
			return 0;
		});

		for(var i=0;i<tempArray.length;i++){
			var obj = tempArray[i];
			if(obj){
				var dataItemObj = {};
				dataItemObj.PID = obj.id;
				dataItemObj.FN = obj.firstName;
				dataItemObj.LN = obj.lastName;
				dataItemObj.WD = obj.weight;
				dataItemObj.HD = obj.height;

				  if(obj.middleName){
					  dataItemObj.MN = obj.middleName;
				  }else{
					  dataItemObj.MN =  "";
				  }
				  dataItemObj.firstName = dataItemObj.FN+" "+ dataItemObj.MN+" "+dataItemObj.LN;
				  dataItemObj.GR = obj.gender;
				  if(obj.status){
					  dataItemObj.ST = obj.status;
				  }else{
					  dataItemObj.ST = "ACTIVE";
				  }
				  dataItemObj.DOB = kendo.toString(new Date(obj.dateOfBirth),"MM/dd/yyyy");

				  var suName =  dataItemObj.LN+" "+dataItemObj.FN+" "+ dataItemObj.MN;

				/*$("#cmbWeeklyServiceUser").append('<option value="' + obj.id + '">' + suName + '</option>');*/




				  dataArray.push(dataItemObj);
			}
		}

		/*$("#cmbWeeklyServiceUser").multipleSelect({
			/!*selectAll: true,*!/
			width: 200,
			dropWidth: 200,
			multipleWidth: 200,
			placeholder: 'Select Service Users'
			/!*selectAllText: 'All'*!/

		});*/
		var obj = {};
		obj.firstName = "";
		obj.PID = "";
		//dataArray.unshift(obj);
		//setDataForSelection(dataArray, "txtRPatient", onFacilityChange, ["firstName", "PID"], 0, "");
		//getPatientRosterList();
		//buildDeviceTypeGrid(dataArray);
	}
	function onAppPatientChange(){

	}
function onAppProviderChange(){

	}
	var appPatientArray = [];
	function onClickPtViews(){
		var txtStartDate = $("#txtStartDate").data("kendoDatePicker");
		if(txtStartDate != ""){
			/*var scheduler = $("#ptscheduler").data("kendoScheduler");
			scheduler.dataSource.data([]);
			var dt = new Date(txtStartDate.value())
			var currentWeekDay = dt.getDay();
			var lessDays = currentWeekDay == 0 ? 6 : currentWeekDay-1
			var wkStart = new Date(new Date(dt).setDate(dt.getDate()- lessDays));
			var wkEnd = new Date(new Date(wkStart).setDate(wkStart.getDate()+6));

			var startDateView = kendo.toString(new Date(wkStart),"dd-MMM-yyyy").toUpperCase();

			var patientListURL = ipAddress+"/appointment/by-patient/"+viewPatientId+"/"+startDateView+"/7"
			getAjaxObject(patientListURL,"GET",function(dataObj){
				var ptArray = [];
				if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
					if(dataObj.response.appointment){
						if($.isArray(dataObj.response.appointment)){
							ptArray = dataObj.response.appointment;
						}else{
							ptArray.push(dataObj.response.appointment);
						}
					}
					var pm = 0;
					var am = 0;
					for(var i=0;i<ptArray.length;i++){
						var item = ptArray[i];
						var ms = item.dateOfAppointment;
						var d1 = new Date(ms);
						var est = item.dateOfAppointment;
						var st = kendo.toString(new Date(est), "g");
						var eet = est+(item.duration*60*1000);
						var et = kendo.toString(new Date(eet), "g");
						if(d1.getHours()>12){
							pm++;
						}else{
							am++;
						}
						var appIdk = item.id;
						var est = item.dateOfAppointment;
						var st = kendo.toString(new Date(est), "g");
						var eet = est+(item.duration*60*1000);
						var et = kendo.toString(new Date(eet), "g");


						var strPatient =  viewPatientId+", "+viewpName;

						var pDOB = viewpDT;//item.composition.patient.dateOfBirth;

						var appTYpe = item.appointmentType;
						var appTYpeV = getAppTypeValue(appTYpe, appTypeArray);
						var note = item.notes;
						var desc = item.appointmentReason;
						var descV = getAppTypeValue(desc, reasonArray);
						var dt = pDOB;//new Date(pDOB);
						var strAge = viewPTAge;//getAge(dt);
						var provider = getProviderName(item.providerId);
						 var arr = {description1V:""+desc+"",ownerIdV:""+appTYpe+"",taskId: item.id,notes:note,dob1:pDOB,age:strAge,image:'../../img/AppImg/HosImages/patient1.png',patientID:viewPatientId,id: appIdk, description:appIdk,start: new Date(st), end: new Date(et), title: ""+strPatient+"",ownerId:""+appTYpe+"",facility:""+facility+"",doctor:""+provider+"",description1:""+desc+""};
					      scheduler.dataSource.add(arr);

					}
				}
			},onErrorMedication);*/

			var dt = new Date(txtStartDate.value())  //current date of week
			var currentWeekDay = dt.getDay();
			var lessDays = currentWeekDay == 0 ? 6 : currentWeekDay-1
			var wkStart = new Date(new Date(dt).setDate(dt.getDate()- lessDays));
			var wkEnd = new Date(new Date(wkStart).setDate(wkStart.getDate()+6));

			var startDateView = kendo.toString(new Date(wkStart),"dd-MMM-yyyy").toUpperCase();
			//https://stage.timeam.com/appointment/by-patient/101/22-JAN-2018/7
				var patientListURL = ipAddress+"/appointment/by-patient/"+viewPatientId+"/"+startDateView+"/7"
				getAjaxObject(patientListURL,"GET",function(dataObj){
				var sdt = wkStart;
				var first = kendo.toString(new Date(sdt),"MM/dd/yyyy");
				var fDay = wkStart.getDay();
				$("#ptscheduler").text("");
				var strTable = '<table class="table">';
				strTable = strTable+'<thead class="fillsHeader">';
				strTable = strTable+'<tr>';
				strTable = strTable+'<th class="textAlign whiteColor">'+first +" (Mon) "+'</th>';
				sdt = new Date(new Date(wkStart).setDate(wkStart.getDate()+1));
				var sDay = sdt.getDay();
				var second = kendo.toString(new Date(sdt),"MM/dd/yyyy");
				strTable = strTable+'<th class="textAlign whiteColor">'+second+" (Tue) "+'</th>';
				sdt = new Date(new Date(wkStart).setDate(wkStart.getDate()+2));
				var tDay = sdt.getDay();
				var third = kendo.toString(new Date(sdt),"MM/dd/yyyy");
				strTable = strTable+'<th class="textAlign whiteColor">'+third +" (Wed) "+'</th>';
				sdt = new Date(new Date(wkStart).setDate(wkStart.getDate()+3));
				var foDay = sdt.getDay();
				var four = kendo.toString(new Date(sdt),"MM/dd/yyyy");
				strTable = strTable+'<th class="textAlign whiteColor">'+four +" (Thu) "+'</th>';
				sdt = new Date(new Date(wkStart).setDate(wkStart.getDate()+4));
				var fiDay = sdt.getDay();
				var five = kendo.toString(new Date(sdt),"MM/dd/yyyy");
				strTable = strTable+'<th class="textAlign whiteColor">'+five +" (Fri) "+'</th>';
				sdt = new Date(new Date(wkStart).setDate(wkStart.getDate()+5));
				var sixDay = sdt.getDay();
				var six = kendo.toString(new Date(sdt),"MM/dd/yyyy");
				strTable = strTable+'<th class="textAlign whiteColor">'+six +" (Sat) "+'</th>';
				sdt = new Date(new Date(wkStart).setDate(wkStart.getDate()+6));
				var sevenDay = sdt.getDay();
				var seven = kendo.toString(new Date(sdt),"MM/dd/yyyy");
				strTable = strTable+'<th class="textAlign whiteColor">'+seven +" (Sun) "+'</th>';
				strTable = strTable+'</tr>';
				strTable = strTable+'</thead>';
				strTable = strTable+'<tbody>';
				//console.log(dataObj);
				var ptArray = [];
				if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
					if(dataObj.response.appointment){
						if($.isArray(dataObj.response.appointment)){
							ptArray = dataObj.response.appointment;
						}else{
							ptArray.push(dataObj.response.appointment);
						}
					}
				}
				appPatientArray = ptArray;
				for(var p=0;p<ptArray.length;p++){
					var item = ptArray[p];
					item.ST = "0";
					item.PR = "0";
				}
				var fDayArr = [];
				var sDayArr = [];
				var tDayArr = [];
				var foDayArr = [];
				var fiDayArr = [];
				var sixDayArr = [];
				var seventhDayArr = [];
				var col = columnSequenceSort(ptArray);
				var rowArray = [];
				for(var p=0;p<ptArray.length;p++){
					//console.log(ptArray[p]);
					var item = ptArray[p];
					if(item){
						var ms = item.dateOfAppointment;
						if(rowArray.toString().indexOf(ms) == -1){
							rowArray.push(ms);
						}
						/*var dt = new Date(item.dateOfAppointment);
						if(dt.getDay() == fDay){
							fDayArr.push(item);
						}else if(dt.getDay() == sDay){
							sDayArr.push(item);
						}else if(dt.getDay() == tDay){
							tDayArr.push(item);
						}else if(dt.getDay() == foDay){
							foDayArr.push(item);
						}else if(dt.getDay() == fiDay){
							fiDayArr.push(item);
						}else if(dt.getDay() == sixDay){
							sixDayArr.push(item);
						}else if(dt.getDay() == sevenDay){
							seventhDayArr.push(item);
						}*/
					}
				}
				//console.log(rowArray.length);
				var rows = 0;
				/*if(fDayArr.length>rows){
					rows = fDayArr.length;
				}
				if(sDayArr.length>rows){
					rows = sDayArr.length;
				}
				if(tDayArr.length>rows){
					rows = tDayArr.length;
				}
				if(foDayArr.length>rows){
					rows = foDayArr.length;
				}
				if(fiDayArr.length>rows){
					rows = fiDayArr.length;
				}
				if(sixDayArr.length>rows){
					rows = sixDayArr.length;
				}
				if(seventhDayArr.length>rows){
					rows = seventhDayArr.length;
				}*/
				//console.log(rows);
				rows = 0;
				for(var r=0;r<rowArray.length;r++){
					strTable = strTable+'<tr>';
					strTable = strTable+'<td style="text-align:center">'+getPatientDayInfo(rowArray[r],fDay,ptArray)+'</td>';
					strTable = strTable+'<td style="text-align:center">'+getPatientDayInfo(rowArray[r],sDay,ptArray)+'</td>';
					strTable = strTable+'<td style="text-align:center">'+getPatientDayInfo(rowArray[r],tDay,ptArray)+'</td>';
					strTable = strTable+'<td style="text-align:center">'+getPatientDayInfo(rowArray[r],foDay,ptArray)+'</td>';
					strTable = strTable+'<td style="text-align:center">'+getPatientDayInfo(rowArray[r],fiDay,ptArray)+'</td>';
					strTable = strTable+'<td style="text-align:center">'+getPatientDayInfo(rowArray[r],sixDay,ptArray)+'</td>';
					strTable = strTable+'<td style="text-align:center">'+getPatientDayInfo(rowArray[r],sevenDay,ptArray)+'</td>';
					strTable = strTable+'</tr>';
				}

				strTable = strTable+'</tbody>';
				strTable = strTable+'</table>';
				$("#ptscheduler").append(strTable);
			},onErrorMedication);

		}
	}
	function allowNumerics(ctrlId){
		$("#"+ctrlId).keypress(function (e) {
				 if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
						return false;
					}
	   });
	}
	function getPatientDayInfo(ms,pDay,ptArray){
		var strData = "";
		for(var p=0;p<ptArray.length;p++){
			//console.log(ptArray[p]);
			var item = ptArray[p];
			if(item && item.ST == "0"){
				var dt = new Date(item.dateOfAppointment);
				if(dt.getDay() == pDay && ms == item.dateOfAppointment){
					item.ST = "1";
					//strData = "1";
					var st1 = "";
					var ms = item.dateOfAppointment;
					var d1 = new Date(ms);
					var est = item.dateOfAppointment;
					var st = kendo.toString(new Date(est), "g");
					var eet = est+(item.duration*60*1000);
					var et = kendo.toString(new Date(eet), "g");
					/*if(d1.getHours()>12){
						//pm++;
						st1 = "Evening Visit";
					}else{
						//am++;
						st1 = "Moring Visit";
					}*/
					var appIdk = item.id;
					var est = item.dateOfAppointment;
					var st = kendo.toString(new Date(est), "t");
					var eet = est+(item.duration*60*1000);
					var et = kendo.toString(new Date(eet), "t");

					var appTYpe = item.appointmentType;
					var appTYpeV = getAppTypeValue(appTYpe, appTypeArray);
					var note = item.notes;
					var desc = item.appointmentReason;
					var descV = getAppTypeValue(desc, reasonArray);
					//var dt = pDOB;//new Date(pDOB);
					//var strAge = viewPTAge;//getAge(dt);
					var provider = getProviderNames(ms,pDay,ptArray,item.providerId);//getProviderName(item.providerId);
					strData = provider+"<br>";
					strData = strData+st+" - "+et+" - "+item.duration+"<br>";
					//strData = strData+""+item.duration+"<br>";
					strData = strData+""+appTYpeV+" - "+descV+"<br>";
					//strData = strData+""+descV+"<br>";
					//strData = strData+""+st1+"<br>";
					strData = strData+"<a href='#' id="+appIdk+" onclick=onPAppEdit(event) style=font-weight:bold>View</a>&nbsp;&nbsp;&nbsp;";

					return strData;
				}
			}
		}
		return "";
	}
	//appPatientArray
	var appPatientItem =  null;
	function onPAppEdit(event){
		//console.log(event);
		var prArray = [];
		var appIDK = event.currentTarget.id;
		for(var a=0;a<appPatientArray.length;a++){
			var item = appPatientArray[a];
			if(item && item.id == appIDK){
				appPatientItem = item;
				prArray.push(item.providerId);
				break;
			}
		}
		//console.log(appPatientItem);
		var ms = appPatientItem.dateOfAppointment;
		var dt = new Date(ms);
		if(dt){
			$("#liAppByProvider").addClass("active");
			$("#tab1").addClass("active");

			$("#liAppByPatient").removeClass("active");
			$("#tab2").removeClass("active");

			  var scheduler = $("#scheduler").data("kendoScheduler");
			  if(scheduler){
				  	//scheduler.setOptions({ date: dt });
					//scheduler.view(scheduler.view().name);
				  scheduler.date(dt);
					scrollToHour(dt.getHours());
			  }
		}

		//var required = $("#required").data("kendoMultiSelect");
		// if(required){
		// 	required.value(prArray);
		// }
		var required = $('#cmbProvider').multipleSelect('getSelects');
		if(required !== null && required.length > 0){
			$('#cmbProvider').multipleSelect('setSelects',required);
		}

		onClickViews();
	}
	function getProviderNames(ms,pDay,ptArray,prID){
		var pNames = "";
		for(var p=0;p<ptArray.length;p++){
			var item = ptArray[p];
			if(item && item.PR == "0"){
				var ms = item.dateOfAppointment;
				var d1 = new Date(ms);
				if(ms == ms && d1.getDay() == pDay ){//&& prID == item.providerId
					var provider = getProviderName( item.providerId);
					pNames = provider;
					item.PR = "1";
					return pNames;
				}
			}
		}
		if(pNames.length>0){
			pNames = pNames.substring(0, pNames.length-1);
		}
		return pNames;
	}
	function initPatientSchedule(){
		var defHeight = 200;
	    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
	        defHeight = 100;
	    }
	    var cmpHeight = window.innerHeight - defHeight;
	    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;



	    $("#divPT").css({height: cmpHeight + 'px' });
	    $("#ptscheduler1").css({height: cmpHeight + 'px' });
	    $("#ptscheduler").css({height: cmpHeight + 'px' });
	    /*$("#ptscheduler").kendoScheduler({
			 height: cmpHeight,
			 footer: false,
			  date: new Date(),
			  allDaySlot:false,
			  currentTimeMarker: false,
			  minorTickCount: 1, // display one time slot per major tick
			   minorTick: 60,
			   majorTick:60,
			   messages: {
				    deleteWindowTitle: "Delete Appointment",
				    editable: {
				        confirmation: "Are you sure you want to delete this Appointment?"
				      },
				      editor: {
				          editorTitle: "Create Appointment"
				      }
				  },
			  views: [
			            { type: "week" , selected: true},
			        ],
			        dataBound: function(e) {
			        	//console.log(e.sender._selectedView.options.name);
			        	//DAY_VIEW = e.sender._selectedView.options.name;
			        	if(DAY_VIEW == "DayView"){
			        		//$($(".k-scheduler-times")[0]).parent().parent().hide();
			        	}else{
			        		//$($(".k-scheduler-times")[0]).parent().parent().show();
			        	}

			        },
			        editable: {
			            template: $("#customEditorTemplate").html(),
			          },
			          schema: {
			              model: {
			                id: "taskId",
			                fields: {
			                  taskId: { from: "TaskID", type: "number" },
			                  title: { from: "Title", defaultValue: "No title", validation: { required: true } },
			                  start: { type: "date", from: "Start" },
			                  end: { type: "date", from: "End" },
			                  startTimezone: { from: "StartTimezone" },
			                  doctor: { type:'string',from: "Doctor" },
			                  dob: { type:'string',from: "dob" },
			                  endTimezone: { from: "EndTimezone" },
			                  description1: { from: "Description1" },
			                  recurrenceId: { from: "RecurrenceID" },
			                  recurrenceRule: { from: "RecurrenceRule" },
			                  recurrenceException: { from: "RecurrenceException" },
			                  ownerId: { from: "OwnerID", defaultValue: 1 },
			                  image: { from: "image", defaultValue: "../../img/AppImg/HosImages/patient1.png" },
			                  patientID: { from: "patientID", defaultValue: 1 },
			                  isAllDay: { type: "boolean", from: "IsAllDay" }
			                }
			              }
			            },
			            remove: scheduler_remove1,
			            navigate:scheduler_navigate1,
			            save: scheduler_save1,
			            cancel: scheduler_cancel1,
		            	eventTemplate: $("#event-template").html(),
		            	resizeStart: function(e){
						  console.log(e.event);
						  e.preventDefault();
		            	},
		            	resizeEnd: function(e){
		            		console.log(e);
		            	},
			 edit: function(e) {// "week","month",
        	    console.log(e.event);
        	    if(e.event.room){
        	    	e.preventDefault();
        	    }
			 }

	    });*/
	}
	function scheduler_remove1(e){

	}
	function scheduler_save1(e){

	}
	function scheduler_navigate1(e){

	}
	function scheduler_cancel1(e){

	}
	function getAppointmentList(dataObj){
		var dArray = getTableListArray(dataObj);
		if(dArray && dArray.length>0){
			appTypeArray = dArray;
			setDataForSelection(dArray, "cmbAppTypes", onAppTypeChange, ["desc", "value"], 0, "");
            setDataForSelection(dArray, "txtRAppType", onAppTypeChange, ["desc", "value"], 0, "");
            setDataForSelection(dArray, "cmbStatusPopUp", onAppTypeChange, ["desc", "value"], 1, "");
			var cmbAppTypes = $("#cmbAppTypes").data("kendoComboBox");
			if(cmbAppTypes){
				cmbAppTypes.select(1);
			}
		}
	}
	function getReasonList(dataObj){
		var dArray = getTableListArray(dataObj);
		if(dArray && dArray.length>0){
			reasonArray = dArray;
            setDataForSelection(dArray, "cmbReason", onReasonChange, ["desc", "value"], 0, "");
            setDataForSelection(dArray, "cmbReasonPopUp", onReasonChange, ["desc", "value"], 0, "");
            setDataForSelection(dArray, "txtRReason", onReasonChange, ["desc", "value"], 0, "");



		}
	}
	function onAppTypeChange(){
		var cmbAppTypes = $("#cmbAppTypes").data("kendoComboBox");
		if(cmbAppTypes && cmbAppTypes.selectedIndex<0){
			cmbAppTypes.select(0)
		}
	}
	function onReasonChange(){
		var cmbReason = $("#cmbReason").data("kendoComboBox");
		if(cmbReason && cmbReason.selectedIndex<0){
			cmbReason.select(0)
		}
}

function onPatientBillingTypeChange() {
    var cmbBillTypePopUp = $("#cmbBillTypePopUp").data("kendoComboBox");
    if (cmbBillTypePopUp && cmbBillTypePopUp.selectedIndex < 0) {
        cmbBillTypePopUp.select(0)
    }
}

	function getTableListArray(dataObj){
		var dataArray = [];
		if(dataObj && dataObj.response && dataObj.response.codeTable){
			if($.isArray(dataObj.response.codeTable)){
				dataArray = dataObj.response.codeTable;
			}else{
				dataArray.push(dataObj.response.codeTable);
			}
		}
		var tempDataArry = [];
		var obj = {};
		obj.desc = "";
		obj.value = "";
		//tempDataArry.push(obj);
		for(var i=0;i<dataArray.length;i++){
			if(dataArray[i].isActive == 1){
				var obj = dataArray[i];
				//obj.idk = dataArray[i].id;
				//obj.status = dataArray[i].Status;
				tempDataArry.push(obj);
			}
		}
		return tempDataArry;
	}
	var pAVlTimings = [];
	function onClickViews(){
		pAVlTimings = [];
		//var ms1 = $("#required").getKendoMultiSelect();
	    var values = $('#cmbProvider').multipleSelect('getSelects');

	    if(values.length == 0){
			customAlert.error("Error", "Please select staff");
			return;
		}else if(values.length > 3){
			customAlert.error("Error", "Please select maximum of 3 staffs");
			return;
		}
		else{
			var API_URL = ipAddress + '/homecare/availabilities/';
			getAjaxObjectAsync(API_URL + '?parent-id=:in:'+values.join()+'&parent-type-id=201&is-active=1&is-deleted=0',"GET", getAvailabilityList,onErrorMedication);
		}
	}
	function getAvailabilityList(dataObj){
		//console.log(dataObj);
		if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1" && dataObj.response.availabilities){
	        if($.isArray(dataObj.response.availabilities)){
	        	pAVlTimings = dataObj.response.availabilities;
	        }else{
	        	pAVlTimings.push(dataObj.response.availabilities);
	        }
	    }
		onGetBlockDays();
	}
function onClickBlockDays() {
	var popW = "65%";
	var popH = "70%";

	var cmbDoctor = $("#cmbDoctor").data("kendoComboBox");
	//var cmbFacility = $("#cmbFacility").data("kendoComboBox");

	parentRef.providerId = cmbDoctor.value();
	parentRef.facilityId = Number($("#cmbFacility option:selected").val());
	var profileLbl;
	var devModelWindowWrapper = new kendoWindowWrapper();
	profileLbl = "Appointment Block Timings";
	var scheduler = $("#scheduler").data("kendoScheduler");
	devModelWindowWrapper.openPageWindow("../../html/patients/appointmentBlock.html", profileLbl, popW, popH, true, closeBlockAddAction);
	//var required = $("#required").data("kendoMultiSelect");
	//console.log(required);
}
	var stTime = "";
	var endTime = "";
	var stDate = "";
	var endDate = "";
	function closeBlockAddAction(evt,re){
		//console.log(re);
		if(re.status == "success"){
			/*stTime = re.ST;
			endTime = re.ET;
			var scheduler = $("#scheduler").data("kendoScheduler");
			stDate = scheduler.date();
			navigateDateTime(stDate,stTime,endTime);*/
		}
		onGetBlockDays();
	}
	function isDateAreEqual(edt){
		//var scheduler = $("#scheduler").data("kendoScheduler");
		var dt1 = new Date(stDate);
		var dt2 = new Date(edt);
		if(dt1.getTime() == dt2.getTime()){
			return true;
		}
		return false;
	}
	function navigateDateTime(eDate,stTm,etTm){
		var scheduler = $("#scheduler").data("kendoScheduler");
		//console.log(scheduler.date());
		var dt = eDate;//scheduler.date();
		dt = kendo.toString(dt,"yyyy/MM/dd");
		if(isDateAreEqual(eDate)){
			var st = dt+" "+stTime;
			var et = dt+" "+endTime;
		}else{
			var st = dt+" "+stTm;
			var et = dt+" "+etTm;
		}

		//console.log(new Date(st)+","+new Date(et));
		if(scheduler){
			scheduler.setOptions({
		          minorTick: 15,
		          majorTick:15,
		          minorTickCount:1,
		          startTime: new Date(st),
		          endTime: new Date(et)
		      });
		      scheduler.view(scheduler.view().name);
		}
	}
	function scheduler_add(e){
		try{
				//console.log(scheduler);
				//var scheduler = $("#scheduler").data("kendoScheduler");
				//scheduler.addEvent({dob:''});
		}catch(ex){}

	}
	var provider = "";
	var facility = "";
	function scheduler_navigate(e){
		//console.log(e);
		var cmbDoctor = $("#cmbDoctor").data("kendoComboBox");
		var cmbFacility = $("#cmbFacility").data("kendoComboBox");
		var scheduler = $("#scheduler").data("kendoScheduler");
		var dtFromDate = e.date;//new Date();//scheduler.date();//new Date();//$("#dtFromDate").data("kendoDatePicker");
		//var dtFromDate = $("#dtFromDate").data("kendoDatePicker");

		var providerId = cmbDoctor.value();
		var fromDate = dtFromDate;//dtFromDate.value();
		//fromDate = fromDate.getTime();
		//fromDate = getGMTDateFromLocaleDate(fromDate);


		var strDT = kendo.toString(new Date(fromDate), "dd-MMM-yyyy");

		var scheduler = $("#scheduler").data("kendoScheduler");
		scheduler.dataSource.data([]);
		//onGetBlockDays();
		//navigateDateTime(e.date,"12:00 AM", "11:45 PM");
		DAY_VIEW = e.view;
		onGetBlockDays();
		//getBlockDaysByDate(fromDate);
		//onClickAppSearch(fromDate);
		/*if(DAY_VIEW == "month" || DAY_VIEW == "week"){
			$('.k-event').css('cssText', 'width:20% !important;');
			var strDT = "01-DEC-2017";//kendo.toString(new Date(""), "dd-MMM-yyyy");
			var patientListURL = ipAddress+"/appointment/by-provider/"+providerId+"/"+strDT+"/30";
			 getAjaxObject(patientListURL,"GET",onPatientListData,onErrorMedication);
		}else{
			$('.k-event').css('cssText', 'width:99% !important;');
			var patientListURL = ipAddress+"/appointment/by-provider/"+providerId+"/"+strDT+"/1";
			 getAjaxObject(patientListURL,"GET",onPatientListData,onErrorMedication);
		}*/

	}

	function getGMTDateFromLocaleDate(sTime){
		   var now = new Date(sTime);
		   var utc = new Date(now.getTime() + now.getTimezoneOffset() * 60000);
		   return utc.getTime();
		}
	function getLocalTimeFromGMT(sTime){
		  var dte = new Date(sTime);
		  var dt = new Date(dte.getTime() - dte.getTimezoneOffset()*60*1000);
		  return dt.getTime();
	}
	function getAppTypeValue(strV,arr){
		for(var i=0;i<arr.length;i++){
			var item = arr[i];
			if(item && item["value"] == strV){
				return item["desc"];
			}
		}
		return "";
	}
	function isObjectExist(am,flag){
		for(var c=0;c<appViewDays.length;c++){
			var item = appViewDays[c];
			if(item){
				if(flag){
					if(item.AM == am){
						var num = Number(item.AMT);
						num = num+1;
						item.AMT = num;
					}
				}else{
					if(item.PM == am){
						var num = Number(item.PMT);
						num = num+1;
						item.PMT = num;
					}
				}
			}
		}
	}
	function getCountAppViews(am,flag){
		for(var c=0;c<appViewDays.length;c++){
			var item = appViewDays[c];
			if(flag){
				if(item.AM == am){
					return item.AMT;
				}
			}else{
				if(item.PM == am){
					return item.PMT;
				}
			}
		}
		return "";
	}
	function columnSequenceSort(arrToSort){
		 arrToSort.sort(function (item1,item2) {
				//console.log(a,b);
				var seq1 = Number(item1.dateOfAppointment);
				var seq2 = Number(item2.dateOfAppointment);
				return (seq1-seq2);
	            //return a[strObjParamToSortBy] > b[strObjParamToSortBy];
	        });
	}
	function isAppointmentExist(pdId,appTime, appId){
		for(var c=0;c<dtArray.length;c++){
			var item = dtArray[c];
			if(item.dateOfAppointment == appTime && pdId == item.providerId){
				return true;
			}
		}
		return false;
	}
	function getAppointmentIDK(pdId,appTime,idk){
		for(var c=0;c<dtArray.length;c++){
			var item = dtArray[c];
			if(item.dateOfAppointment == appTime && pdId == item.providerId && idk == item.id){
				return item;
			}
		}
		return "";
	}

	function getAppointmentStartCount(ms){
		var count = 0;
		for(var c=0;c<dtArray.length;c++){
			var item = dtArray[c];
			if(item.dateOfAppointment == ms){
				count = count+1;
			}
		}
		return count;
	}
	function columnAvlSort(arrToSort){
		 arrToSort.sort(function (item1,item2) {
				//console.log(a,b);
				var seq1 = Number(item1.stm);
				var seq2 = Number(item2.stm);
				return (seq1-seq2);
	            //return a[strObjParamToSortBy] > b[strObjParamToSortBy];
	        });
	}
	var dtArray = [];
    var dtAppArry = [];
	function onPatientListData(dataObj){
		//console.log(dataObj);
		//Loader.showLoader();
		dtArray = [];
		if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
			if(dataObj.response.appointment){
				if($.isArray(dataObj.response.appointment)){
					dtArray = dataObj.response.appointment;
				}else{
					dtArray.push(dataObj.response.appointment);
				}
			}
		}
		//appViewDays
		if(dtArray.length > 0) {
			for (var a = 0; a < appViewDays.length; a++) {
				var item = appViewDays[a];
				if (item) {
					item.AMT = 0;
					item.PMT = 0;
				}
			}
			var scheduler = $("#scheduler").data("kendoScheduler");
			scheduler.dataSource.data([]);
			var cmbFacility = $("#cmbFacility").data("kendoComboBox");
			//var cmbDoctor = $("#cmbDoctor").data("kendoComboBox");
			//console.log(pAVlTimings);
			var ptAvlArray = [];
			for (var p = 0; p < pAVlTimings.length; p++) {
				var sdt = pAVlTimings[p].startTime;
				var edt = pAVlTimings[p].endTime;

				var sDate = scheduler.date();
				sDate.setHours(0);
				sDate.setMinutes(0);
				sDate.setSeconds(0);
				sDate.setSeconds(Number(sdt));

				var eDate = scheduler.date();
				eDate.setHours(0);
				eDate.setMinutes(0);
				eDate.setSeconds(0);
				eDate.setSeconds(Number(edt));

				var st = kendo.toString(new Date(sDate), "g");
				var et = kendo.toString(new Date(eDate), "g");
				//console.log(st,et);
				var arr = {
					title: "available times",
					id: "",
					room: '',
					start: new Date(st),
					end: new Date(et),
					description: 'Avl'
				};//room:1
				console.log(scheduler.date().getDay(), pAVlTimings[p].dayOfWeek);
				if ((scheduler.date().getDay()) == (pAVlTimings[p].dayOfWeek)) {
					pAVlTimings[p].stm = new Date(st).getTime();
					pAVlTimings[p].etm = new Date(et).getTime();
					ptAvlArray.push(pAVlTimings[p]);
				}
				// scheduler.dataSource.add(arr);
			}

			//console.log(blArray);

			for (var i = 0; i < blArray.length; i++) {
				//	var st = kendo.toString(new Date(blArray[i].startDateTime), "g");
				//var et = kendo.toString(new Date(blArray[i].endDateTime), "g");
				var sdt = blArray[i].startDateTime;
				//sdt = getLocalTimeFromGMT(sdt);

				var edt = blArray[i].endDateTime;
				//edt = getLocalTimeFromGMT(edt);

				var st = kendo.toString(new Date(sdt), "g");
				var et = kendo.toString(new Date(edt), "g");
				//description1:""+desc+"
				var arr = {
					title: "block times",
					id: blArray[i].id,
					room: 1,
					start: new Date(st),
					end: new Date(et),
					description: 'Block'
				};
				scheduler.dataSource.add(arr);
			}
			columnAvlSort(ptAvlArray);
			// console.log(ptAvlArray);
			try {
				if (ptAvlArray.length > 0) {
					var stTime = 0;
					var etTime = 0;
					var currDate = scheduler.date();
					currDate.setHours(0);
					currDate.setMinutes(0);
					currDate.setSeconds(0);
					currDate.setMilliseconds(0);
					stTime = currDate.getTime();
					etTime = ptAvlArray[0].stm;
					var arr = {
						title: "block times",
						id: "2",
						room: 1,
						start: new Date(stTime),
						end: new Date(etTime),
						description: 'Block'
					};
					scheduler.dataSource.add(arr);
					for (var pt = 0; pt < ptAvlArray.length; pt++) {

						//stTime = 0;
						//etTime = 0;

						if (pt == 0) {
							stTime = ptAvlArray[pt].etm;
							etTime = ptAvlArray[pt + 1].stm;//1544257800000;//(currDate.getTime()+(23*60*60*1000)+(59*60*1000));
						} else if (pt == ptAvlArray.length - 1) {

						} else {
							stTime = ptAvlArray[pt].etm;
							etTime = ptAvlArray[pt + 1].stm;
						}
						// console.log(new Date(stTime), new Date(etTime));
						var arr = {
							title: "block times",
							id: "2",
							room: 1,
							start: new Date(stTime),
							end: new Date(etTime),
							description: 'Block'
						};
						// stTime = etTime;
						scheduler.dataSource.add(arr);
					}
					stTime = ptAvlArray[ptAvlArray.length - 1].etm;
					etTime = currDate.getTime() + (23 * 60 * 60 * 1000) + (59 * 60 * 1000);
					// console.log(new Date(stTime), new Date(etTime));
					var arr = {
						title: "block times",
						id: "2",
						room: 1,
						start: new Date(stTime),
						end: new Date(etTime),
						description: 'Block'
					};
					scheduler.dataSource.add(arr);
				}
			} catch (ex) {
			}

			var facility = $("#cmbFacility option:selected").text(); //cmbFacility.text();
			//var provider  = cmbDoctor.text();
			var am = 0;
			var pm = 0;

			/*var ms1 = $("#required").getKendoMultiSelect();
            var values = ms1.value();*/
			var col = columnSequenceSort(dtArray);
			//console.log(dtArray);
			dtAppArry = [];
			var msArray = [];
			for (var d = 0; d < dtArray.length; d++) {
				var item = dtArray[d];
				var ms = item.dateOfAppointment;
				if (dtAppArry.length == 0) {
					dtAppArry.push(item);
					msArray.push(ms);
				} else if (dtAppArry.length > 0) {
					var strArray = msArray.toString();
					//if(strArray.indexOf(ms) == -1){
					msArray.push(ms);
					dtAppArry.push(item);
					//}
				}
			}


			//  var required = $("#required").data("kendoMultiSelect");
			// 	if(required){
			// 		//var pdArray = [];
			// 		var arr = required.listView._dataItems;
			// 		for(var i=0;i<arr.length;i++){
			// 			var item = arr[i];
			// 			if(item){
			// 				pdArray.push(item.idk);
			// 			}
			// 		}
			// 	}

			var pdArray = [];
			var values = $('#cmbProvider').multipleSelect('getSelects');

			if (values !== null && values.length > 0) {


				pdArray = $.map(values, function (item) {
					return Number(item);
				});

				// pdArray = providerArray.filter(function (item, index) {
				// 	return values.indexOf(item.providerId) >= 0;
				// });

			}
			//console.log(pdArray);
			//console.log(dtAppArry);
			for (var j = 0; j < dtAppArry.length; j++) {
				for (var i = 0; i < pdArray.length; i++) {
					if (dtAppArry[j] && dtAppArry[j] != null) {
						var item = dtAppArry[j];
						var ms = item.dateOfAppointment;//getLocalTimeFromGMT(dtArray[i].dateOfAppointment);
						var d1 = new Date(ms);
						var pdId = pdArray[i];
						var appId = dtAppArry[j].id;
						if(dtAppArry[j].providerId == pdArray[i]) {
                            pdId = pdArray[i];

                            var est = item.dateOfAppointment;
                            var st = kendo.toString(new Date(est), "g");//new Date(dtArray[i].dateOfAppointment);
                            var eet = est + (item.duration * 60 * 1000);
                            var et = kendo.toString(new Date(eet), "g");//ms+(dtArray[i].duration*60*1000)

                            if (isAppointmentExist(pdId, ms, appId)) {
                                var provider = getProviderName(pdId);
                                //console.log(provider);
                                if (d1.getHours() >= 12) {
                                    pm++;
                                    var spnPM = "spnPM" + pdId;
                                    isObjectExist(spnPM, false);
                                    var cnt = getCountAppViews(spnPM, false);
                                    $("#divProviders").find("#" + spnPM).text(cnt);
                                } else {
                                    am++;
                                    var spnAM = "spnAM" + pdId;//item.providerId;
                                    isObjectExist(spnAM, true);
                                    var cnt = getCountAppViews(spnAM, true);
                                    $("#divProviders").find("#" + spnAM).text(cnt);
                                }
                                var item = getAppointmentIDK(pdId, ms, item.id);//dtAppArry[j];
                                var appIdk = item.id;//getAppointmentIDK(pdId,ms);
                                //var ampm = (hours >= 12) ? "PM" : "AM";
                                var est = item.dateOfAppointment;
                                //console.log(dtArray[i].id+" "+"Local SEC: "+est+" Local Date:"+new Date(est));
                                //est = getLocalTimeFromGMT(est);
                                //console.log("UTC SEC: "+est+" UTC:"+new Date(est));
                                var st = kendo.toString(new Date(est), "g");//new Date(dtArray[i].dateOfAppointment);

                                var eet = est + (item.duration * 60 * 1000);
                                //eet = getLocalTimeFromGMT(eet);

                                var et = kendo.toString(new Date(eet), "g");//ms+(dtArray[i].duration*60*1000)
                                var strPatient;
                                var pDOB;
                                if (item != undefined && item != "") {
                                    strPatient = item.composition.patient.id + ", " + item.composition.patient.firstName + " " + item.composition.patient.middleName + " " + item.composition.patient.lastName;
                                    pDOB = item.composition.patient.dateOfBirth;
                                }


                                var appTYpe = item.appointmentType;
                                var appTYpeV = getAppTypeValue(appTYpe, appTypeArray);
                                var note = item.notes;
                                var desc = item.appointmentReason;
                                var descV = getAppTypeValue(desc, reasonArray);
                                var dt = new Date(pDOB);
                                var strAge = getAge(dt);
                                var provider = getProviderName(pdId);//item.providerId);
                                var arr;
                                if (item != undefined && item != "") {
                                    arr = {
                                        bill: "" + item.billToName + "",
                                        swiftBillingId: "" + item.swiftBillingId + "",
                                        shiftValue: "" + item.shiftValue + "",
                                        pRate: "" + item.payoutHourlyRate + "",
                                        patientBillingRate: "" + item.patientBillingRate + "",
                                        billingRateType: "" + item.billingRateType + "",
                                        description1V: "" + desc + "",
                                        ownerIdV: "" + appTYpe + "",
                                        taskId: item.id,
                                        notes: note,
                                        dob1: pDOB,
                                        age: strAge,
                                        image: '../../img/AppImg/HosImages/patient1.png',
                                        patientID: item.composition.patient.id,
                                        id: appIdk,
                                        description: appIdk,
                                        start: new Date(st),
                                        end: new Date(et),
                                        title: "" + strPatient + "",
                                        ownerId: "" + appTYpe + "",
                                        facility: "" + facility + "",
                                        doctor: "" + provider + "",
                                        description1: "" + desc + "",
                                        facilityValue: 'Location'
                                    };
                                }

                                //var arr = {id: 1, start: new Date("11/5/2017 08:00 AM"), end: new Date("11/5/2017 11:00 AM"), title: "Interview",ownerId:"strDoc",facility:"strFacility",doctor:"strDoc"};
                                scheduler.dataSource.add(arr);
                                delete dtAppArry[j];
                                // dtArray = $.grep(dtArray, function (e) {
                                //     return e.id != appId;
                                // });
                                //delete dtArray[j];
                            } else {
                                //var arr = {start: new Date(st), end: new Date(et)};
                                var arr = {
                                    title: "Not created",
                                    id: "",
                                    room: "",
                                    start: new Date(st),
                                    end: new Date(et),
                                    description: "A1"
                                };
                                scheduler.dataSource.add(arr);
                            }
                        }
                        // else {
                        //     //var arr = {start: new Date(st), end: new Date(et)};
                        //     var arr = {
                        //         title: "Not created",
                        //         id: "",
                        //         room: "",
                        //         start: new Date(st),
                        //         end: new Date(et),
                        //         description: "A1"
                        //     };
                        //     scheduler.dataSource.add(arr);
                        // }

					}
				}
			}
			//	console.log(appViewDays);
			$("#spnAM").text(am);
			$("#spnPM").text(pm);

			try {
				setTimeout(function () {
					var events = $(".k-event");
					var cmbDoctor = $("#cmbDoctor").data("kendoComboBox");
					/* setTimeout(function(){
                          //  scheduler.resize(true);
                            if(DAY_VIEW == "DayView"){
                                  scheduler.options.selectable = true;
                                  scheduler.options.editable = true;
                          }else{
                              scheduler.options.selectable = false;
                              scheduler.options.editable = false;
                          }
                          },2000);*/
					if (DAY_VIEW == "DayView") {
						//$('.k-event').css('width', '25%');
					} else {
						$('.k-event').css('width', '140px');
					}

					var values = $('#cmbProvider').multipleSelect('getSelects');

					if (values !== null && values.length > 0) {

						values = $.map(values,function(item) {
							return parseInt(item);
						});
					}

					var provider = cmbDoctor.text();
					for (var e = 0; e < events.length; e++) {
						var pName = $(events[e]).find("#desc1").text();
						pName = $.trim(pName);
						if (pName != "" && pName != "Avl" && pName != "Block") {
							var pItem = getPatientAppDetails(pName, dtArray);
							if (pItem) {
								if (DAY_VIEW == "DayView") {
									//$('.k-event').css('cssText', 'width:99% !important;');
									//$(events[e]).find("#imgId").show();
									//$('.k-event').css('width', '25%');
									$(events[e]).css('width', '22%');
									$(events[e]).find("#eventId").show();
									var imageServletUrl = ipAddress + "/download/patient/photo/" + pItem.patientId + "/?" + Math.round(Math.random() * 1000000) + "&access_token=" + sessionStorage.access_token + "&tenant=" + sessionStorage.tenant;
									$(events[e]).find("#imgId").attr("src", imageServletUrl);
									$(events[e]).css("background-color", getRandomColor());
									var pDOB = pItem.composition.patient.dateOfBirth;
									var dt = new Date(pDOB);
									var sDT = kendo.toString(dt, "MM/dd/yyyy");
									var strAge = getAge(dt);
									strAge = " (" + strAge + ")";
									var appTYpe = pItem.appointmentType;
									appTYpe = getAppTypeValue(appTYpe, appTypeArray);
									var gender = pItem.composition.patient.gender;
									var note = pItem.notes;
									$(events[e]).find("#pID").text("");
									pName = pItem.patientId + " - " + pItem.composition.patient.firstName + " " + pItem.composition.patient.middleName + " " + pItem.composition.patient.lastName;
									var strId = pName;//+" -  "+sDT+" "+strAge+" , "+gender;
									$(events[e]).find("#pName").text(strId);
									if (pItem && pItem.providerId) {
										provider = getProviderName(pItem.providerId);
									}
									$(events[e]).find("#provider").text(provider);
									if (appTYpe) {
										$(events[e]).find("#appId").text(appTYpe);
									}
									if (note) {
										$(events[e]).find("#notes").text(note);
									}
									//var tip = "Id: "+pItem.patientId+"\n";
									var tip = "Name: " + pItem.composition.patient.firstName + " " + pItem.composition.patient.middleName + " " + pItem.composition.patient.lastName + "\n";
									tip = tip + "DOB: " + sDT + strAge + "\n"
									tip = tip + "Staff : " + pItem.composition.provider.lastName + " " + pItem.composition.provider.firstName + "\n";
									tip = tip + "Duration : " + pItem.duration + "\n";
									tip = tip + "Bill To : " + pItem.billToName + "\n";
									tip = tip + "Bill Rate : " + pItem.patientBillingRate + "\n";
									tip = tip + "Payout Rate : " + pItem.payoutHourlyRate + "\n";
									tip = tip + "Status : " + appTYpe + "\n";
									tip = tip + "Reason : " + pItem.composition.appointmentReason.desc + "\n";
									tip = tip + "Notes : " + note + "\n";

									var pindex = values.indexOf(pItem.providerId);
									if (pindex === 0) {
										$(events[e]).css('left','2px');
									}
									else if (pindex === 1) {
										$(events[e]).css('left','466px');
									}
									else if (pindex === 2) {
										$(events[e]).css('left','850px');
									}

									//$('.k-event').attr('title', tip);
									$(events[e]).attr("title", tip);
									$("#scheduler").find(" .k-event-delete").show();
									//$("#scheduler").off("dblclick");
								} else {
									$('.k-event').css('width', '140px');
									buttonEvents();
									$("#scheduler").find(" .k-event-delete").hide();
									$(events[e]).find("#eventId").sh();
									//$(events[e]).find("#pID").text("");
									//$(events[e]).find("#imgId").hide();
									$(events[e]).css("background-color", "green");
								}
							} else {
								//$("#scheduler").find(" .k-event-delete").hide();
								$(events[e]).find(" .k-event-delete").hide();
								$(events[e]).css("background-color", "white");
							}
						} else {
							var bText = $(events[e]).find("h3").text();
							//console.log(pName);
							//$(events[e]).find(" .k-event-delete").hide();
							if (pName == "Block") {
								$(events[e]).css("background-color", "orange");
								$(events[e]).find("#eventBId").show();
							} else {
								$(events[e]).css("background-color", "white");
								$(events[e]).find("#eventAId").show();
							}


						}
					}
					//Loader.hideLoader();
				}, 2000);
			} catch (ex) {
			}
			//$('.k-event').css('width', '99%');

			//$(".k-event").css("background-color","red")
		}
		else{
			customAlert.error("Info", "No appointments found.");
		}
	}
	function getPatientAppDetails(pName,pArray){
		var pNameArray = pName.split(" ");
		var fn = "";
		var mn = "";
		var ln = "";
		if(pNameArray[0]){
			fn = pNameArray[0];
		}
		if(pNameArray[1]){
			mn = pNameArray[1];
		}
		if(pNameArray[2]){
			ln = pNameArray[2];
		}
		for(var i=0;i<pArray.length;i++){
			var pItem = pArray[i];
			var fName = pItem.composition.patient.firstName;
			var mName = pItem.composition.patient.middleName;
			var lName = pItem.composition.patient.lastName;
			if(pItem.id == pName){ // == fn && mName == mn && lName == ln){
				return pItem;
			}
			//var strPatient = dtArray[i].composition.patient.firstName+" "+dtArray[i].composition.patient.middleName+" "+dtArray[i].composition.patient.lastName;
		}
		return null;
	}
	function convertHex(hex,opacity){
	    hex = hex.replace('#','');
	    r = parseInt(hex.substring(0,2), 16);
	    g = parseInt(hex.substring(2,4), 16);
	    b = parseInt(hex.substring(4,6), 16);

		// Add Opacity to RGB to obtain RGBA
	    result = 'rgba('+r+','+g+','+b+','+opacity/100+')';
	    return result;
	}
	function getRandomColor() {
		  var letters = '0123456789ABCDEF';
		  var color = '#';
		  for (var i = 0; i < 6; i++) {
		    color += letters[Math.floor(Math.random() * 16)];
		  }
		  return convertHex(color,30);
		}
	function getBGRandomColor() {
		  var color = '#999999';
		  return convertHex(color,30);
	}
	function onErrorMedication(errobj){
		//console.log(errobj);
	}
	function onError(errorObj){
		//console.log(errorObj);
	}
	function scheduler_change(e){
		//onClickSearch();
		getBlockDaysByDate(e.start);
		//onClickAppSearch(e.start);
	}
	var isBlockDay = false;
	function scheduler_remove(e){
		//console.log(e);
		var reqObj = {};
		reqObj.id = Number(e.event.id);
        appointmentSaveItem = e.event;
        deleteAppointmentSave();
		// reqObj.modifiedBy = Number(sessionStorage.userId);
		// reqObj.isActive = 0;
		// reqObj.isDeleted = 1;
		//
		// var dataUrl = ipAddress+"/appointment/delete";
		// isBlockDay = false;
		// if(e.event.room){
		// 	isBlockDay = true;
		// 	dataUrl = ipAddress+"/appointment-block/delete";
		// }
		// var arr = [];
		// arr.push(reqObj);
		// createAjaxObject(dataUrl,reqObj,"POST",onDelete,onError);
	}
	function onDelete(dataObj){
		//console.log(dataObj);
		if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == 1){
			if(!isBlockDay){
				customAlert.error("Info", "Appointment is deleted successfully");
				var scheduler = $("#scheduler").data("kendoScheduler");
				onClickAppSearch(scheduler.date());
			}else{
				customAlert.error("Info", "Appointment block day deleted successfully");
				onGetBlockDays();
			}
		}else{
			customAlert.error("Error", "error");
		}
	}
	function scheduler_cancel(e){
		//console.log(e);
		var dt = e.event.start;
		//getBlockDaysByDate(dt);
		onClickAppSearch(dt);
	}
	var saveDate = "";
	var updateFlag = false;
	var appointmentSaveItem = null;
	var appProviderArr = [];
	var appCount = 0;
	var appProviderLength = 0;
	function scheduler_save(e){
		//console.log(e);
		appCount = 0;
		var cmbDoctor = $("#cmbDoctor").data("kendoComboBox");
		appointmentSaveItem = e.event;
		var doc = appointmentSaveItem.doctor;
		if(doc){
			if(doc.indexOf(",")>=0){
				appProviderArr = doc.split(",");
				appProviderLength = appProviderArr.length;
			}else{
				appProviderArr[0] = doc;
				appProviderLength = 1;
			}
			appointmentSave();
		}
	}

	function getProviderId(pName){
		var idx = "";
		for(var p=0;p<providerArray.length;p++){
			var item = providerArray[p];
			 var fName = item.firstName+" "+item.middleName+" "+item.lastName;
			if(item && fName == pName){
				return item.idk;
			}
		}
		return "";
	}
	function getProviderName(pk){
		var idx = "";
		for(var p=0;p<providerArray.length;p++){
			var item = providerArray[p];
			if(item && item.idk == pk){
				return item.firstName+" "+item.middleName+" "+item.lastName;
			}
		}
		return "";
	}

	function getPatientBillingDetails(pBid){
		for(var p=0;p<patientBillArray.length;p++){
			var pItem = patientBillArray[p];
			if(pItem && pItem.billToname == pBid){
				return pItem;
			}
		}
		return null;
	}
	function appointmentSave(){
		//var cmbFacility = $("#cmbFacility").data("kendoComboBox");
		var obj = {};
		obj.createdBy = Number(sessionStorage.userId);
		obj.isActive = 1;
		obj.isDeleted = 0;
		var dsItem = appointmentSaveItem;//e.event;
		saveDate = dsItem.start;
		var sDate = new Date(dsItem.start);
		var eDate = new Date(dsItem.end);
		var diff = eDate.getTime() - sDate.getTime();
		diff = diff/1000;
		diff = diff/60;
		var nSec = sDate.getTime();
		var uSec = getGMTDateFromLocaleDate(nSec);

		//console.log("Local SEC:"+nSec+"Local Date:"+new Date(nSec));
		//console.log("UTC SEC:"+uSec+"UTC:"+new Date(uSec));

		obj.dateOfAppointment = nSec;//uSec;//getGMTDateFromLocaleDate(sDate.getTime());//sDate.getTime();//
		//obj.dateOfAppointment1 = sDate.getTime();//
		obj.duration = diff;
		var providerId = getProviderId(appProviderArr[appCount]);
		obj.providerId = Number(providerId);
		obj.facilityId = Number($("#cmbFacility option:selected").val());
		obj.patientId = dsItem.patientID;//parentRef.patientId;
		obj.appointmentType = dsItem.ownerId;
		obj.notes = dsItem.notes;//$("#taArea").val();
		if(dsItem.id == 0){
			obj.shiftValue = appointmentSaveItem.bill.shiftValue;
			obj.billingRateType = appointmentSaveItem.bill.billingRateTypeCode;
			obj.patientBillingRate = appointmentSaveItem.bill.rate;
			obj.billingRateTypeId = appointmentSaveItem.bill.billingRateTypeId;
			obj.swiftBillingId = appointmentSaveItem.bill.swiftBillingId;
			obj.payoutHourlyRate = appointmentSaveItem.pRate;
			obj.billToName = appointmentSaveItem.bill.billToname;
			obj.billToId = appointmentSaveItem.bill.billToId;
		}else{
			var pBillItem = getPatientBillingDetails(appointmentSaveItem.bill);
			obj.shiftValue = pBillItem.shiftValue;
			obj.billingRateType = pBillItem.billingRateTypeCode;
			obj.patientBillingRate = pBillItem.rate;
			obj.billingRateTypeId = pBillItem.billingRateTypeId;
			obj.swiftBillingId = pBillItem.swiftBillingId;
			obj.payoutHourlyRate = appointmentSaveItem.pRate;
			obj.billToName = pBillItem.billToname;
			obj.billToId = pBillItem.billToId;
		}

		if( dsItem.description1 &&  dsItem.description1.text){
			//$("#taArea").val();;
		}else{
			//obj.appointmentReason = "";
		}
		obj.appointmentReason = dsItem.description1;

		//var flag = eventDatesStartValid( sDate.getTime(),eDate.getTime());
		//console.log(flag);
		var dataUrl = "";
		if(dsItem.id == 0){
			updateFlag = false;
			dataUrl = ipAddress+"/appointment/create";
		}else{
			updateFlag = true;
			obj.id = dsItem.id;
			/*obj.shiftValue = dsItem.shiftValue;
			obj.billingRateType = dsItem.billingRateType;
			obj.patientBillingRate = dsItem.patientBillingRate;
			obj.billingRateTypeId = dsItem.billingRateTypeId;
			obj.swiftBillingId = dsItem.swiftBillingId;*/
			dataUrl = ipAddress+"/appointment/update";
		}
		var arr = [];
		arr.push(obj);
		createAjaxObject(dataUrl,arr,"POST",onCreate,onError);
	}
	function eventDatesStartValid(stDate,etDate){
		for(var i=0;i<blArray.length;i++){
			var est = blArray[i].startDateTime;//kendo.toString(new Date(blArray[i].startDateTime), "g");
			var eet = blArray[i].endDateTime;//kendo.toString(new Date(blArray[i].endDateTime), "g");
			var flag = false;
			//console.log("st:"+new Date(stDate));
			//console.log("et:"+new Date(etDate));

			//console.log(new Date(est));
			//console.log(new Date(eet));

			if(stDate>=est){
				if(stDate>=eet){

				}else{
					return false;
				}
			}else if(est<=stDate){
				if(eet<=etDate){

				}else{
					return false;
				}
			}

		}
		return true;
	}
	var facilityArr2 = [];
	function getFacilityList(dataObj){
		var dataArray = [];
		if(dataObj){
			if($.isArray(dataObj.response.facility)){
				dataArray = dataObj.response.facility;
			}else{
				dataArray.push(dataObj.response.facility);
			}
		}
		var tempDataArry = [];
		for(var i=0;i<dataArray.length;i++){
			dataArray[i].idk = dataArray[i].id;
			dataArray[i].Status = "InActive";
			if(dataArray[i].isActive == 1){
				dataArray[i].Status = "Active";
			}
		}
		facilityArr2 = dataArray;
		bindFacilityDropDown("cmbFacility",dataArray);
		$("#cmbFacility").on("change",combobox_change);
		// setDataForSelection(dataArray, "cmbFacility", onFacilityChange, ["name", "idk"], 0, "");
		// var combobox = $("#cmbFacility").data("kendoComboBox");
		// combobox.bind("change", combobox_change);
		combobox_change();



		bindFacilityDropDown("cmbPtFacility",dataArray);
		//setDataForSelection(dataArray, "cmbPtFacility", onFacilityChange, ["name", "idk"], 0, "");
		bindFacilityDropDown("txtRFacility",dataArray);
		//setDataForSelection(dataArray, "txtRFacility", onFacilityChange, ["name", "idk"], 0, "");

		// setDataForSelection(dataArray, "txtPRFacility", onFacilityChange, ["name", "idk"], 0, "");

		bindFacilityDropDown("txtPRFacility2",dataArray);
		//setDataForSelection(dataArray, "txtPRFacility2", onFacilityChange, ["name", "idk"], 0, "");

		bindFacilityDropDown("txtStFacility",dataArray);
		//setDataForSelection(dataArray, "txtStFacility", onFacilityChange, ["name", "idk"], 0, "");

		bindFacilityDropDown("txtPRSFacility",dataArray);
		//setDataForSelection(dataArray, "txtPRSFacility", onFacilityChange, ["name", "idk"], 0, "");

		bindFacilityDropDown("txt2dFacility",dataArray);
		//setDataForSelection(dataArray, "txt2dFacility", onFacilityChange, ["name", "idk"], 0, "");

		bindFacilityDropDown("txtStaffWeeklyViewFacility",dataArray);
		//setDataForSelection(dataArray, "txtStaffWeeklyViewFacility", onFacilityChange, ["name", "idk"], 0, "");

		bindFacilityDropDown("txtStaffApptReportFacility",dataArray);
		//setDataForSelection(dataArray, "txtStaffApptReportFacility", onFacilityChange, ["name", "idk"], 0, "");

		bindFacilityDropDown("txtPTFacility",dataArray);
		//setDataForSelection(dataArray, "txtPTFacility", onFacilityChange, ["name", "idk"], 0, "");

		bindFacilityDropDown("txtAppPTFacility",dataArray);
		//setDataForSelection(dataArray, "txtAppPTFacility", onFacilityChange, ["name", "idk"], 0, "");

		//setDataForSelection(dataArray, "txt2dFacility1", onFacilityChange, ["name", "idk"], 0, "");

		onClickViewProviderApp();
        onClickSTViewProviderApp();
        // onGetUnAllocated();
		onClickViewStaffApp();
		onClick2dView();
		//onClick2dView1();
		getAjaxObject(ipAddress+"/provider/list/?is-active=1","GET",getProviderList,onError);
	}

	function bindFacilityDropDown(cmbid, dataArray) {
		if (dataArray !== null && dataArray.length > 0) {
			for (var i = 0; i < dataArray.length; i++) {
				$("#" + cmbid).append('<option value="' + dataArray[i].idk + '">' + dataArray[i].name + '</option>');
			}
		}

	}


	var checkInputs = function(elements) {
        elements.each(function() {
              var element = $(this);
          var input = element.children("input");
          input.prop("checked", element.hasClass("k-state-selected"));
        });
      };
      var providerArray = [];
      var twoDProviderAttay = [];

      // function on2DProviderChange(){
      //     // on2DProvider1Change();
      //     // var txt2dProvider1 = $("#txt2dProvider").data("kendoComboBox");
      //     // var career1 = txt2dProvider1.value();
	  //
    	//   var txt2dProvider = $("#txt2dProvider1").data("kendoComboBox");
    	//   var career = txt2dProvider.value();
    	//   var prArray1 = [];
    	//   for(var i=0;i<twoDProviderAttay.length;i++){
      //         twoDProviderAttay[i].name = twoDProviderAttay[i].lastName +' '+ twoDProviderAttay[i].firstName;
    	// 	  var item = twoDProviderAttay[i];
    	// 	  if(item && item.idk != career) {
      //             if (item) {
      //                 prArray1.push(item);
      //             }
      //         }
    	//   }
    	//   setDataForSelection(prArray1, "txt2dProvider", on2DProvider1Change, ["name", "idk"], 0, "");
      //     on2DProvider1Change();
      // }
      // function on2DProvider1Change(){
    	//   var txt2dProvider = $("#txt2dProvider").data("kendoComboBox");
    	//   var career = txt2dProvider.value();
    	//   var prArray1 = [];
    	//   for(var i=0;i<twoDProviderAttay.length;i++){
      //         twoDProviderAttay[i].name = twoDProviderAttay[i].lastName +' '+ twoDProviderAttay[i].firstName;
    	// 	  var item = twoDProviderAttay[i];
    	// 	  if(item && item.idk != career) {
      //             if (item) {
      //                 prArray1.push(item);
      //             }
      //         }
    	//   }
    	//   setDataForSelection(prArray1, "txt2dProvider1", "", ["name", "idk"], 0, "");
      //     on2DProviderChange();
      // }

function on2DProviderChange(){
    var txt2dProvider = $("#txt2dProvider").data("kendoComboBox");
    var career = txt2dProvider.value();
    var prArray1 = [];
    for(var i=0;i<twoDProviderAttay.length;i++){
        twoDProviderAttay[i].name = twoDProviderAttay[i].lastName +' '+ twoDProviderAttay[i].firstName;
        var item = twoDProviderAttay[i];
        if(item && item.idk != career){
            prArray1.push(item);
        }
    }
    setDataForSelection(prArray1, "txt2dProvider1", on2DProvider1Change, ["name", "idk"], 0, "");
    // on2DProvider1Change();

}
function on2DProvider1Change(){

    var txt2dProvider1 = $("#txt2dProvider").data("kendoComboBox");
    var career1 = txt2dProvider.value();

    var txt2dProvider = $("#txt2dProvider1").data("kendoComboBox");
    var career = txt2dProvider.value();
    var prArray1 = [];
    for(var i=0;i<twoDProviderAttay.length;i++){
        twoDProviderAttay[i].name = twoDProviderAttay[i].lastName +' '+ twoDProviderAttay[i].firstName;
        var item = twoDProviderAttay[i];
        if(item && item.idk != career){
            prArray1.push(item);
        }
    }
    setDataForSelection(prArray1, "txt2dProvider", on2DProviderChange, ["name", "idk"], 0, "");
    getComboListIndex("txt2dProvider", "idk", career1);
}
    function on2DProviderChange2(){
        var txt2dProvider = $("#txt2dProvider1").data("kendoComboBox");
        var career = txt2dProvider.value();
        var prArray1 = [];
        for(var i=0;i<twoDProviderAttay.length;i++){
            twoDProviderAttay[i].name = twoDProviderAttay[i].lastName +' '+ twoDProviderAttay[i].firstName;
            var item = twoDProviderAttay[i];
            if(item && item.idk != career) {
                if (item) {
                    prArray1.push(item);
                }
            }
        }
        setDataForSelection(prArray1, "txt2dProvider", on2DProvider1Change, ["name", "idk"], 0, "");
        // txt2dProvider1.value(career1);
    }

	function getProviderList(dataObj){
		//console.log(dataObj);
		var dataArray = [];
		if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
			if($.isArray(dataObj.response.provider)){
				dataArray = dataObj.response.provider;
			}else{
				dataArray.push(dataObj.response.provider);
			}
		}else{
			//customAlert.error("Error",dataObj.response.status.message);
		}
		var tempDataArry = [];
		for(var i=0;i<dataArray.length;i++){
			dataArray[i].idk = dataArray[i].id;
			dataArray[i].psName = dataArray[i].lastName+" "+dataArray[i].firstName;
			dataArray[i].Status = "InActive";
			if(dataArray[i].isActive == 1){
				dataArray[i].Status = "Active";
			}
		}

        dataArray.sort(function (a, b) {
            var p1 = a.lastName.toLowerCase();
            var p2 = b.lastName.toLowerCase();
            var o1, o2;
			o1 = a.firstName.toLowerCase();
			o2 = b.firstName.toLowerCase();

            if (p1 < p2) return -1;
            if (p1 > p2) return 1;

            if (o1 < o2) return -1;
            if (o1 > o2) return 1;
            return 0;
        });
		twoDProviderAttay  = JSON.parse(JSON.stringify(dataArray));
		providerArray = dataArray;
		setDataForSelection(dataArray, "cmbDoctor", onFacilityChange, ["psName", "idk"], 0, "");
		setDataForSelection(dataArray, "cmbPtDoctor", onFacilityChange, ["psName", "idk"], 0, "");
		setDataForSelection(dataArray, "txtRProvider", onFacilityChange, ["psName", "idk"], 0, "");

		setDataForSelection(dataArray, "txt2dProvider", on2DProviderChange, ["psName", "idk"], 0, "");
		on2DProviderChange();

		//<input type='checkbox'/>
		// var required = $("#required").kendoMultiSelect({
        //     itemTemplate: "<div style='width:100px;float:left'>#:data.lastName#  #:data.firstName# </div>",
        //     autoClose: false,
        //     dataSource: dataArray,
        //     dataTextField: "psName",
        //     dataValueField: "idk",
        //     dataBound: function() {
        //       var items = this.ul.find("li");
        //       setTimeout(function() {
        //         //checkInputs(items);
        //       });
        //     },
        //     change: function() {
        //      /* var items = this.ul.find("li");
        //       checkInputs(items);*/
        //     	var ms1 = $("#required").getKendoMultiSelect();
        //         var values = this.value();//distinctValues(this.value());
        //        // console.log(values);
        //     }
        //   }).data("kendoMultiSelect");

		// setTimeout(function(){
		// 	var required = $("#required").data("kendoMultiSelect");
		// 	if(required){
		// 		//required.values(['Thomas']);
		// 	}
		// })

		//onGetBlockDays();
		//onClickSearch();
	}
	var appViewDays = [];
	function onGetBlockDays(){
		//  var required = $("#required").data("kendoMultiSelect");
		// 	if(required){
		// 		var dArr = [];
		// 		var arr = required.listView._dataItems;
		// 		for(var i=0;i<arr.length;i++){
		// 			var item = arr[i];
		// 			if(item){
		// 				dArr.push(item);
		// 			}
		// 		}
		// 	}
			var dArr = [];
			var values = $('#cmbProvider').multipleSelect('getSelects');

			if (values !== null && values.length > 0) {

				values = $.map(values,function(item) {
					return parseInt(item);
				});

				dArr = providerArray.filter(function (item, index) {
					return values.indexOf(item.id) >= 0;
				});
			}


			appViewDays = [];
			var strProviders = ""
			for(var d=0;d<dArr.length;d++){
				var spnAM = "spnAM"+dArr[d].idk;
				var spnPM = "spnPM"+dArr[d].idk;
				var strDiv = '<div class="col-sm-4 col-md-4 col-lg-4 col-xs-4" style="padding-left:0px;border:1px solid #999;text-align:center">';
				strDiv = strDiv+'<div class="col-sm-4 col-md-4 col-lg-4 col-xs-4" style="padding-left:5px">';
				strDiv = strDiv+'<label class="col-xs-12 col-sm-12 col-md-12 col-lg-12 control-label input-sm noPadding lblFontBold colorStyle">'+dArr[d].firstName+'</label>';
				strDiv = strDiv+"</div>";
				strDiv = strDiv+'<div class="col-sm-3 col-md-3 col-lg-3 col-xs-3" style="padding-left:5px">';
				strDiv = strDiv+'<a href="#" class="colorStyle">AM <span class="badge" id="'+spnAM+'">0</span></a>';
				strDiv = strDiv+"</div>";
				strDiv = strDiv+'<div class="col-sm-3 col-md-3 col-lg-3 col-xs-3" style="padding-left:5px">';
				strDiv = strDiv+'<a href="#" class="colorStyle">PM <span class="badge" id="'+spnPM+'">0</span></a>';
				strDiv = strDiv+"</div>";
				strDiv = strDiv+"</div>";
				strProviders = strProviders+strDiv;
				var obj = {};
				obj.AM = spnAM;
				obj.AMT = 0;
				appViewDays.push(obj);
				var obj = {};
				obj.PM = spnPM;
				obj.PMT = 0;
				appViewDays.push(obj);
			}
			//console.log(appViewDays);
			//$("#divProviders").remove();
			$("#divProviders").html("");
			$("#divProviders").html(strProviders);
		var scheduler = $("#scheduler").data("kendoScheduler");
        if(scheduler) {
            var dt = new Date(scheduler.date());
            getBlockDaysByDate(dt);
        }
	}
	function getBlockDaysByDate(dt){
		//var cmbFacility = $("#cmbFacility").data("kendoComboBox");
		var cmbDoctor = $("#cmbDoctor").data("kendoComboBox");
		var providerId = cmbDoctor.value();
		var facilityId = Number($("#cmbFacility option:selected").val());

		var day = dt.getDate();
		var month = dt.getMonth();
		month = month+1;
		var year = dt.getFullYear();

		var stDate = month+"/"+day+"/"+year;
		stDate = stDate+" 00:00:00";

		var startDate = new Date(stDate);
		var stDateTime = startDate.getTime();

		//stDateTime = getGMTDateFromLocaleDate(stDateTime);

		var etDate = month+"/"+day+"/"+year;
		etDate = etDate+" 23:59:59";

		var endtDate = new Date(etDate);
		var edDateTime = endtDate.getTime();
		//edDateTime = getGMTDateFromLocaleDate(edDateTime);
		var patientListURL = "";//
		if(DAY_VIEW == "DayView"){
			patientListURL = ipAddress+"/appointment-block/list/?provider-id="+providerId+"&from-date="+stDateTime+"&to-date="+edDateTime+"&is-active=1";
		}else if(DAY_VIEW == "WeekView"){
			var startDate = new Date(stDate);
			var endtDate = new Date(etDate);

			var day = startDate.getDate()-startDate.getDay();
			startDate.setDate(day);

			endtDate.setDate(day+startDate.getDay());
			var stDateTime = startDate.getTime();
			var edDateTime = endtDate.getTime();
			//var sDate = new Date();
			//sDate.setFullYear(year, month, date)
			patientListURL = ipAddress+"/appointment-block/list/?provider-id="+providerId+"&from-date="+stDateTime+"&to-date="+edDateTime+"&is-active=1";;
		}else{
			var startDate = new Date(stDate);
			var endtDate = new Date(etDate);
			startDate.setDate(1);
			endtDate.setDate(31);
			var stDateTime = startDate.getTime();
			var edDateTime = endtDate.getTime();
			patientListURL = ipAddress+"/appointment-block/list/?provider-id="+providerId+"&from-date="+stDateTime+"&to-date="+edDateTime+"&is-active=1";;
		}
		 getAjaxObject(patientListURL,"GET",onPatientBlockDays,onErrorMedication);

	}
	var blArray = [];
	function onPatientBlockDays(dataObj){
		//console.log(dataObj);
		blArray = [];
		if(dataObj.response.appointmentBlock){
			if($.isArray(dataObj.response.appointmentBlock)){
				blArray = dataObj.response.appointmentBlock;
			}else{
				blArray.push(dataObj.response.appointmentBlock);
			}
		}

		var scheduler = $("#scheduler").data("kendoScheduler");
		for(var i=0;i<blArray.length;i++){
			var sdt = blArray[i].startDateTime;
			//sdt = getLocalTimeFromGMT(sdt);

			var edt = blArray[i].endDateTime;
			//edt = getLocalTimeFromGMT(edt);

			var st = kendo.toString(new Date(sdt), "g");
			var et = kendo.toString(new Date(edt), "g");
			 var arr = {title:"block times",id:blArray[i].id,room:1,start: new Date(st), end: new Date(et)};
			   scheduler.dataSource.add(arr);
		}
		setTimeout(function(){
			var events = $(".k-event");
			for(var e=0;e<events.length;e++){
				//console.log(events[e]);
				$(events[e]).css("background","#787b7a");
				$(events[e]).css("color","#FFF");
			}
			onClickAppSearch(scheduler.date());
		},2000);

	}
	var patientId = "";
	function addAppointment(){
		var scheduler = $("#scheduler").data("kendoScheduler");
		//var cmbFacility = $("#cmbFacility").data("kendoComboBox");
		var cmbDoctor = $("#cmbDoctor").data("kendoComboBox");

		var strFacility = "";
		var strDoc = "";
		if($("#cmbFacility")){
			strFacility = $("#cmbFacility option:selected").text();
		}
		if(cmbDoctor){
			strDoc = cmbDoctor.text();
		}
    	//scheduler.addEvent({ownerId:""+strDoc+"",facility:""+strFacility+"",doctor:""+strDoc+"", title: ""+patientId+"",start:stDate,end:endDate});
	}
	var patientReturnItem = null;
	function closeAddAction(evt,returnData){

		if(returnData && returnData.status == "success"){
			//console.log(returnData);
			patientId = returnData.selItem.PID;
			//parentRef.patientId = patientId;
			patientReturnItem = returnData;
		//	parentRef.selItem = selItem;
			  var popW = 800;
			    var popH = 450;

			    parentRef.searchZip = true;
			    var profileLbl;
			    var devModelWindowWrapper = new kendoWindowWrapper();
			    profileLbl = "Search Service User Billing";
			    parentRef.patientId = patientId;
			    var rDataObj = {};
			    rDataObj.status = "success";
			    onCloseBillingAction({},rDataObj);
			   // devModelWindowWrapper.openPageWindow("../../html/patients/serviceUserBillingSearch.html", profileLbl, popW, popH, true, onCloseBillingAction);

		 }
	}
	var ptBillId = "";
	var shiftValue = "";
	var billingRateType = "";
	var patientBillingRate = "";
	var billingRateTypeId = "";
	var swiftBillingId = "";

	function onCloseBillingAction(evt,rData){
	    if(rData && rData.status == "success"){
	    	//ptBillId = rData.selItem.idk;
	    	//shiftValue = rData.selItem.shiftValue;
	    	//billingRateType = rData.selItem.billingRateTypeCode;
	    	//patientBillingRate = rData.selItem.patientBillingRate;
	    //	billingRateTypeId = rData.selItem.billingRateTypeId;
	    	//swiftBillingId = rData.selItem.swiftBillingId;

	    	var pID = patientReturnItem.selItem.PID;
			var pName = pID+","+patientReturnItem.selItem.FN+" "+patientReturnItem.selItem.MN+" "+patientReturnItem.selItem.LN;
			if(pID != ""){
				var scheduler = $("#scheduler").data("kendoScheduler");
				var cmbFacility = $("#cmbFacility").data("kendoComboBox");
				var cmbDoctor = $("#cmbDoctor").data("kendoComboBox");

				var strFacility = "";
				var strDoc = "";
				var strAppType = "";
				var strAppTypeValue = "";

				var strR = "";
				var strRV = "";

				debugger;
				// if(cmbFacility){
				// 	strFacility = cmbFacility.text();
				// }

				if (patientReturnItem.selItem.facilityId && patientReturnItem.selItem.facilityId !== null && patientReturnItem.selItem.facilityId > 0) {
					if (facilityArr2 !== null && facilityArr2.length > 0) {
						var tmp = [];
						tmp.push(patientReturnItem.selItem.facilityId);

						var tmpArr = facilityArr2.filter(function(item,index){
							return tmp.indexOf(item.idk) >= 0;
						})

						if (tmpArr !== null && tmpArr.length > 0) {
							strFacility = tmpArr[0].name;
						}

					}
				}



				if(cmbDoctor){
					strDoc = cmbDoctor.text();
				}
				strDoc = "";
				if(dArray){
					for(var j=0;j<dArray.length;j++){
						strDoc = strDoc+dArray[j]+",";
					}
					if(strDoc.length>0){
						strDoc = strDoc.substring(0,strDoc.length-1);
					}
				}
				var cmbAppTypes = $("#cmbAppTypes").data("kendoComboBox");
				if(cmbAppTypes){
					strAppType = cmbAppTypes.text();
					strAppTypeValue = cmbAppTypes.value();
				}
				var cmbReason = $("#cmbReason").data("kendoComboBox");
				if(cmbReason){
					strR = cmbReason.text();
					strRV = cmbReason.value();
				}

				if (dArray !== null && dArray.length === 1) {
					//console.log(stDate+','+endDate);
					scheduler.addEvent({ description1V: "" + strRV + "", description1: "" + strRV + "", ownerIdV: "" + strAppTypeValue + "", patientID: "" + pID + "", ownerId: "" + strAppTypeValue + "", facility: "" + strFacility + "", doctor: "" + strDoc + "", title: "" + pName + "", start: stDate, end: endDate });
				}



			}

	     //   $('#txtRContact').val(returnData.selItem.billToname);
	    }
	}
	function onErrorMedication(errobj){
		//console.log(errobj);
	}
	function onshowImage(){
		  $("#imgPhoto").attr( "src", "../../img/AppImg/HosImages/male_profile.png" );
	}
function buttonEvents(){
	  $("#imgPhoto").error(function() {
		   onshowImage();
	   });

	$("#btnSave").off("click");
	$("#btnSave").on("click",onClickSave);

	$("#btnCancel").off("click");
	$("#btnCancel").on("click",onClickCancel);

	$("#btnSearch1").off("click",onClickSearch);
	$("#btnSearch1").on("click",onClickSearch);

	$("#btnBlock").off("click",onClickBlockDays);
	$("#btnBlock").on("click",onClickBlockDays);

	$("#btnView").off("click",onClickViews);
	$("#btnView").on("click",onClickViews);

	$("#btnPtView").off("click",onClickPtViews);
	$("#btnPtView").on("click",onClickPtViews);


	$("#btnRAdd").off("click",onClickAddRoster);
	$("#btnRAdd").on("click",onClickAddRoster);

	$("#btnREdit").off("click",onClickEditRoster);
	$("#btnREdit").on("click",onClickEditRoster);

	$("#btnRDel").off("click",onClickDeleteRoster);
	$("#btnRDel").on("click",onClickDeleteRoster);

	$("#btnRRoster").off("click",onClickCreateRoster);
	$("#btnRRoster").on("click",onClickCreateRoster);

	$("#btnRApp").off("click",onClickCreateRosterAppointment);
	$("#btnRApp").on("click",onClickCreateRosterAppointment);

	$("#btnRSearch1").off("click",onClickRSearch);
	$("#btnRSearch1").on("click",onClickRSearch);

	$("#btnRSearch").off("click",onClickRPSearch);
	$("#btnRSearch").on("click",onClickRPSearch);

	$("#btnServiceUser").off("click");
	$("#btnServiceUser").on("click",onClickServiceUser);

	 $('#prscheduler').scroll(function(e) {
		 //console.log(e);
	 });
	$("#btnCarePlan").off("click",onClickCarePlan);
	$("#btnCarePlan").on("click",onClickCarePlan);

	$("#btnPRView").off("click",onClickViewProviderApp);
    $("#btnPRView").on("click", onClickViewProviderApp);

    $("#btnPRView2").off("click", onClickViewProviderApp);
    $("#btnPRView2").on("click", onClickViewProviderApp);

    $("#btnSTPRView").off("click",onClickSTViewProviderApp);
    $("#btnSTPRView").on("click",onClickSTViewProviderApp);

	$("#btnPRSView").off("click",onClickViewStaffApp);
	$("#btnPRSView").on("click",onClickViewStaffApp);

	$("#btn2dView").off("click",onClick2dView);
	$("#btn2dView").on("click",onClick2dView);

	$("#btn2dView1").off("click",onClick2dView1);
	$("#btn2dView1").on("click",onClick2dView1);

	$("#btn2dViewMove").off("click",onClick2dViewMove);
	$("#btn2dViewMove").on("click",onClick2dViewMove);

	$("#btnPTSView").off("click",onClickPTSView);
	$("#btnPTSView").on("click",onClickPTSView);


    $("#btnBack").off("click",onClickBackToList);
    $("#btnBack").on("click",onClickBackToList);

    $("#btnStaffWeeklyView").off("click", onClickStaffWeeklyView);
    $("#btnStaffWeeklyView").on("click", onClickStaffWeeklyView);

	// $("#btnAppPTSView").off("click",onClickAppPTSView);
    // 	// $("#btnAppPTSView").on("click",onClickAppPTSView);

    $("input[name=Comm]").on( "change", function() {
        radioValue = $(this).val();
        if(radioValue == "1"){
            buildProvidersListGrid([]);
            var url = ipAddress+"/homecare/providers/filter-by/appointments/?start-time="+staffStDateTime+"&end-time="+staffEdDateTime+"&type=201";
            getAjaxObject(url, "GET", onStaffListData, onErrorMedication);
        }else{
            buildProvidersListGrid([]);
            var urlExtn = '/provider/list?is-active=1&is-deleted=0';
            getAjaxObject(ipAddress+urlExtn,"GET",onAllStaffListData,onErrorMedication);
        }
    } )

	$("#cmbSelectField").on( "change", function() {
		onGetPreviousHistory();
		});

    $('#btnAppPTSView').on('click', function(e) {
        e.preventDefault();

		onClickAppPTSView();
    });


	 $("#divProfileNavs li a[data-toggle='tab'").off("click");
	 $("#divProfileNavs li a[data-toggle='tab'").on("click",onClickTabs);

	 $("#btnAppAppointment").off("click",onClickAvlPatient);
    $("#btnAppAppointment").on("click", onClickAvlPatient);

    $("#btnCreateApptPopUp").off("click", onClickSaveAppointmentPopUp);
    $("#btnCreateApptPopUp").on("click", onClickSaveAppointmentPopUp);

    $("#btnCancelSaveAppt").off("click", onClickCancelSaveAppt);
    $("#btnCancelSaveAppt").on("click", onClickCancelSaveAppt);

    $("#btnStaffApptReportView").off("click", onClickStaffApptReportView);
    $("#btnStaffApptReportView").on("click", onClickStaffApptReportView);


    $("#btnSendEmail").off("click", onClickSendMail);
    $("#btnSendEmail").on("click", onClickSendMail);


	 $("#scheduler").off("click");
		$("#scheduler").on("click", '.k-event', function (e) {
			 var curr = e.currentTarget;
			    var pName = $(curr).find("#desc1").text();
				var pItem = getPatientAppDetails(pName,dtArray);
				if(pItem){
					patientId = pItem.patientId;
				}
		});

	if(DAY_VIEW != "DayView"){
		$("#scheduler").off("dblclick");
		$("#scheduler").on("dblclick", '.k-event', function (e) {
			if(DAY_VIEW != "DayView"){
				 //console.log("sss");
				    var curr = e.currentTarget;
				    var pName = $(curr).find("#desc1").text();
					var pItem = getPatientAppDetails(pName,dtArray);
					if(pItem){
						//console.log(pItem);

						var scheduler = $("#scheduler").data("kendoScheduler");
						 scheduler.view("day");
						 var dt = new Date(pItem.dateOfAppointment);
						 onClickAppSearch(dt);
					}
			}

		});
	}

	$("#liPreviousWeek").off("click",onClickPreviousWeek);
	$("#liPreviousWeek").on("click",onClickPreviousWeek);

	$("#liNextWeek").off("click",onClickNextWeek);
	$("#liNextWeek").on("click",onClickNextWeek);

	$("#liPreviousDay").off("click",onClickPreviousDay);
	$("#liPreviousDay").on("click",onClickPreviousDay);

	$("#liNextDay").off("click",onClickNextDay);
	$("#liNextDay").on("click",onClickNextDay);

	$("#liPreviousMonth").off("click",onClickPreviousMonth);
	$("#liPreviousMonth").on("click",onClickPreviousMonth);

	$("#liNextMonth").off("click",onClickNextMonth);
	$("#liNextMonth").on("click",onClickNextMonth);

	$("#liRePreviousWeek").off("click",onClickRePreviousWeek);
	$("#liRePreviousWeek").on("click",onClickRePreviousWeek);

	$("#liReNextWeek").off("click",onClickReNextWeek);
	$("#liReNextWeek").on("click",onClickReNextWeek);

	$("#liRePreviousDay").off("click",onClickRePreviousDay);
	$("#liRePreviousDay").on("click",onClickRePreviousDay);

	$("#liReNextDay").off("click",onClickReNextDay);
	$("#liReNextDay").on("click",onClickReNextDay);

	$("#liRePreviousMonth").off("click",onClickRePreviousMonth);
	$("#liRePreviousMonth").on("click",onClickRePreviousMonth);

	$("#liReNextMonth").off("click",onClickReNextMonth);
	$("#liReNextMonth").on("click",onClickReNextMonth);

	$("#txtPRFacility2").off("change",onChangePRFacility2);
	$("#txtPRFacility2").on("change",onChangePRFacility2);

	$("#btnTodayPRDate2").off("click",onClickTodayPRDate2);
	$("#btnTodayPRDate2").on("click",onClickTodayPRDate2);

	$("#txtStFacility").off("change",onChangeStFacility);
	$("#txtStFacility").on("change",onChangeStFacility);

	$("#btnTodaySTPRDate").off("click",onClickTodaySTPRDate);
	$("#btnTodaySTPRDate").on("click",onClickTodaySTPRDate);

	$("#txtPRSFacility").off("change",onChangePRSFacility);
	$("#txtPRSFacility").on("change",onChangePRSFacility);

	$("#btnTodayPRSView").off("click",onClickTodayPRSView);
	$("#btnTodayPRSView").on("click",onClickTodayPRSView);

	$("#txtPTFacility").off("change",onChangePTFacility);
	$("#txtPTFacility").on("change",onChangePTFacility);

	$("#txtAppPTFacility").off("change",onChangeAppPTFacility);
	$("#txtAppPTFacility").on("change",onChangeAppPTFacility);

}

function onClickUnallocatedAppPTSView(){
	debugger;
	var dataOptionsUnavailable = {};

	angularUIgridUnavWrapper = new AngularUIGridWrapper("dgridUnAllocatedAppointmentsList", dataOptionsUnavailable);
	angularUIgridUnavWrapper.init();
	buildAppointmentsListGrid([]);
	adjustHeightUnAvailable();

	$('#dgridUnAllocatedAppointmentsList').on('click', 'div.ui-grid-cell', function () {
		var index = $(this).index();

		if (index > 0) {
			var selectedTime = $(this).parent().find('div:eq(0)').text();
			var selectedDate = $('#dgridUnAllocatedAppointmentsList div.ui-grid-header-cell-row .ui-grid-header-cell:eq(' + index + ') span').text();

			showPatients(selectedTime, selectedDate);
		}

	})
	onClickSubmit();
}

function onChangeAppPTFacility(){
	debugger;
	onClickUnallocatedAppPTSView();
}

function onChangeStFacility(){
	onClickSTViewProviderApp();
}

function onChangePTFacility(){
	onClickPTSView();
}

function onChangePRSFacility(){
	onClickViewStaffApp();
}

function onClickTodayPRSView(){
	var  txtPRSDate = $("#txtPRSDate").data("kendoDatePicker");

	if(txtPRSDate && txtPRSDate.value()){
		var selDate = new Date();

		txtPRSDate.value(GetDateTimeEditSD(selDate));
		$("#btnPRSView").trigger("click");

	}
}

function onClickTodaySTPRDate(){
	var  txtSTPRDate = $("#txtSTPRDate").data("kendoDatePicker");

	if(txtSTPRDate && txtSTPRDate.value()){
		var selDate = new Date();

		txtSTPRDate.value(GetDateTimeEditSD(selDate));
		$("#btnSTPRView").trigger("click");

	}
}

function onClickTodayPRDate2(){
	var  txtPRDate = $("#txtPRDate2").data("kendoDatePicker");

	if(txtPRDate && txtPRDate.value()){
		var selDate = new Date();

		// $("#txtPRDate2").val(GetDateTimeEditSD(weekSD));
		txtPRDate.value(GetDateTimeEditSD(selDate));
		$("#btnPRView2").trigger("click");

	}
}

function onChangePRFacility2(e){
	onClickViewProviderApp();
}

var resizeFlag = null;
$(window).resize(function(){
	// console.log("resize");
	// if(resizeFlag){
	// 	clearTimeout(resizeFlag);
	// }
	// resizeFlag = setTimeout(function(){
	// 	if($("#liAppByProvider").hasClass('active')) {
    //         onClickViews();
    //     }
	// },400);

});

var viewPatientId = "";
var viewpName = "";
var viewpDT = "";
var viewPTAge = "";
function closePtAddAction(evt,returnData){


	if(returnData && returnData.status == "success"){
		//console.log(returnData);
		var pID = returnData.selItem.PID;
		viewPatientId = pID;
		var pName = returnData.selItem.FN+" "+returnData.selItem.MN+" "+returnData.selItem.LN;
		viewpName = pName;
		if(pID != ""){
			$("#lblName").text(pID+" - "+pName);
			$("#txtPatient").val(pName);
			var imageServletUrl = ipAddress + "/download/patient/photo/" + pID + "/?" + Math.round(Math.random() * 1000000)+"&access_token="+sessionStorage.access_token+"&tenant="+sessionStorage.tenant;
	        $("#imgPhoto").attr("src", imageServletUrl);

	        getAjaxObject(ipAddress + "/patient/" + pID, "GET", onGetPatientInfo, onError);
		}
	}
}
function onGetPatientInfo(dataObj){
	if (dataObj.response.patient.dateOfBirth) {
        var dt = new Date(dataObj.response.patient.dateOfBirth);
        if (dt) {
        	var strDT = "";
        	var cntry = sessionStorage.countryName;
        	if(cntry.indexOf("India")>=0){
        		strDT = kendo.toString(dt, "dd/MM/yyyy");
        	}else if(cntry.indexOf("United Kingdom")>=0){
        		strDT = kendo.toString(dt, "dd/MM/yyyy");
        	}else{
        		strDT = kendo.toString(dt, "MM/dd/yyyy");
        	}
        }
        viewpDT = strDT;
        var strAge = getAge(dt);
        viewPTAge = strAge;
        var dob = strDT+"( "+strAge+" )"+","+dataObj.response.patient.gender;
        $("#lblDOB").text(dob);
        if (dataObj && dataObj.response && dataObj.response.communication) {
        	 var commArray = [];
             if ($.isArray(dataObj.response.communication)) {
                 commArray = dataObj.response.communication;
             } else {
                 commArray.push(dataObj.response.communication);
             }
             var comObj = commArray[0];
             commId = comObj.id;
             var addr = comObj.address1+","+comObj.address2;//+','+comObj.zip+"\n"+comObj.homePhone+","+comObj.cellPhone;
             $("#lblAddr").text(addr);
             var strZip = comObj.city+","+comObj.state+","+comObj.zip;
             $("#lblZip").text(strZip);
             var strPhone = comObj.homePhone+"&nbsp;&nbsp;"+comObj.cellPhone;
             $("#lblhp").html(strPhone);
             //$("#lblcell").text(comObj.cellPhone);
        }
	}
       // $("#txtAge").val(strAge);
}


function onClickPreviousWeek(){
	var  txtPRDate = $("#txtPRDate2").data("kendoDatePicker");

	if(txtPRDate && txtPRDate.value()){
		var selDate = txtPRDate.value();

		var previousWeek = getLastWeek(selDate);
		var weekSD = getWeekStartDate(previousWeek);

		// $("#txtPRDate2").val(GetDateTimeEditSD(weekSD));
		txtPRDate.value(GetDateTimeEditSD(previousWeek));
		$("#btnPRView2").trigger("click");

	}
}

function onClickPreviousDay(){
	var  txtPRDate = $("#txtPRDate2").data("kendoDatePicker");

	if(txtPRDate && txtPRDate.value()){
		var selDate = txtPRDate.value();

		var previousWeek = getLastDay(selDate);
		var weekSD = getWeekStartDate(previousWeek);

		// $("#txtPRDate2").val(GetDateTimeEditSD(weekSD));
		txtPRDate.value(GetDateTimeEditSD(previousWeek));
		$("#btnPRView2").trigger("click");

	}
}

function onClickPreviousMonth(){
	var  txtPRDate = $("#txtPRDate2").data("kendoDatePicker");

	if(txtPRDate && txtPRDate.value()){
		var selDate = txtPRDate.value();

		var previousWeek = getLastMonth(selDate);
		var weekSD = getWeekStartDate(previousWeek);

		// $("#txtPRDate2").val(GetDateTimeEditSD(weekSD));
		txtPRDate.value(GetDateTimeEditSD(previousWeek));
		$("#btnPRView2").trigger("click");

	}
}

function onClickNextWeek(){
	var  txtPRDate = $("#txtPRDate2").data("kendoDatePicker");

	if(txtPRDate && txtPRDate.value()){
		var selDate = txtPRDate.value();

		var previousWeek = getNextWeek(selDate);
		var weekSD = getWeekStartDate(previousWeek);

		// $("#txtPRDate2").val(GetDateTimeEditSD(weekSD));
		txtPRDate.value(GetDateTimeEditSD(previousWeek));
		$("#btnPRView2").trigger("click");

	}
}


function onClickNextDay(){
	var  txtPRDate = $("#txtPRDate2").data("kendoDatePicker");

	if(txtPRDate && txtPRDate.value()){
		var selDate = txtPRDate.value();

		var previousWeek = getNextDay(selDate);
		var weekSD = getWeekStartDate(previousWeek);

		// $("#txtPRDate2").val(GetDateTimeEditSD(weekSD));
		txtPRDate.value(GetDateTimeEditSD(previousWeek));
		$("#btnPRView2").trigger("click");

	}
}

function onClickNextMonth(){
	var  txtPRDate = $("#txtPRDate2").data("kendoDatePicker");

	if(txtPRDate && txtPRDate.value()){
		var selDate = txtPRDate.value();

		var previousWeek = getNextMonth(selDate);
		var weekSD = getWeekStartDate(previousWeek);

		// $("#txtPRDate2").val(GetDateTimeEditSD(weekSD));
		txtPRDate.value(GetDateTimeEditSD(previousWeek));
		$("#btnPRView2").trigger("click");

	}
}




function onClickRePreviousWeek(){
	var  txtPRDate = $("#txtSTPRDate").data("kendoDatePicker");

	if(txtPRDate && txtPRDate.value()){
		var selDate = txtPRDate.value();

		var previousWeek = getLastWeek(selDate);
		var weekSD = getWeekStartDate(previousWeek);

		// $("#txtPRDate2").val(GetDateTimeEditSD(weekSD));
		txtPRDate.value(GetDateTimeEditSD(previousWeek));
		$("#btnSTPRView").trigger("click");

	}
}

function onClickRePreviousDay(){
	var  txtPRDate = $("#txtSTPRDate").data("kendoDatePicker");

	if(txtPRDate && txtPRDate.value()){
		var selDate = txtPRDate.value();

		var previousWeek = getLastDay(selDate);
		var weekSD = getWeekStartDate(previousWeek);

		// $("#txtPRDate2").val(GetDateTimeEditSD(weekSD));
		txtPRDate.value(GetDateTimeEditSD(previousWeek));
		$("#btnSTPRView").trigger("click");

	}
}

function onClickRePreviousMonth(){
	var  txtPRDate = $("#txtSTPRDate").data("kendoDatePicker");

	if(txtPRDate && txtPRDate.value()){
		var selDate = txtPRDate.value();

		var previousWeek = getLastMonth(selDate);
		var weekSD = getWeekStartDate(previousWeek);

		// $("#txtPRDate2").val(GetDateTimeEditSD(weekSD));
		txtPRDate.value(GetDateTimeEditSD(previousWeek));
		$("#btnSTPRView").trigger("click");

	}
}

function onClickReNextWeek(){
	var  txtPRDate = $("#txtSTPRDate").data("kendoDatePicker");

	if(txtPRDate && txtPRDate.value()){
		var selDate = txtPRDate.value();

		var previousWeek = getNextWeek(selDate);
		var weekSD = getWeekStartDate(previousWeek);

		// $("#txtPRDate2").val(GetDateTimeEditSD(weekSD));
		txtPRDate.value(GetDateTimeEditSD(previousWeek));
		$("#btnSTPRView").trigger("click");

	}
}


function onClickReNextDay(){
	var  txtPRDate = $("#txtSTPRDate").data("kendoDatePicker");

	if(txtPRDate && txtPRDate.value()){
		var selDate = txtPRDate.value();

		var previousWeek = getNextDay(selDate);
		var weekSD = getWeekStartDate(previousWeek);

		// $("#txtPRDate2").val(GetDateTimeEditSD(weekSD));
		txtPRDate.value(GetDateTimeEditSD(previousWeek));
		$("#btnSTPRView").trigger("click");

	}
}

function onClickReNextMonth(){
	var  txtPRDate = $("#txtSTPRDate").data("kendoDatePicker");

	if(txtPRDate && txtPRDate.value()){
		var selDate = txtPRDate.value();

		var previousWeek = getNextMonth(selDate);
		var weekSD = getWeekStartDate(previousWeek);

		// $("#txtPRDate2").val(GetDateTimeEditSD(weekSD));
		txtPRDate.value(GetDateTimeEditSD(previousWeek));
		$("#btnSTPRView").trigger("click");

	}
}



function getLastWeek(dt) {
	var lastWeek = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate() - 7);
	return lastWeek;
  }

function getLastDay(dt) {
	var lastDay = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate() - 1);
	return lastDay;
}

function getLastMonth(dt) {
	var lastMonth = new Date(dt.getFullYear(), dt.getMonth()-1, dt.getDate());
	return lastMonth;
}

  function getNextWeek(dt) {
	var nextWeek = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate() + 7);
	return nextWeek;
  }

function getNextDay(dt) {
	var nextDay = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate() + 1);
	return nextDay;
}

function getNextMonth(dt) {
	var nextMonth = new Date(dt.getFullYear(), dt.getMonth()+ 1, dt.getDate());
	return nextMonth;
}

function getWeekStartDate(d) {
	d = new Date(d);
	var day = d.getDay(),
		diff = d.getDate() - day + (day == 0 ? -6:0); // adjust when day is sunday
	return new Date(d.setDate(diff));
  }




function GetDateTimeEditSD(dt){

    var strDT = "";
    if (dt) {
        var cntry = sessionStorage.countryName;
        if (cntry.indexOf("India") >= 0) {
            strDT = kendo.toString(dt, "dd/MM/yyyy");
        } else if (cntry.indexOf("United Kingdom") >= 0) {
            strDT = kendo.toString(dt, "dd/MM/yyyy");
        } else {
            strDT = kendo.toString(dt, "MM/dd/yyyy");
        }
    }
    return strDT;
}


function onClickRSearch(){
	 var popW = "60%";
	    var popH = 500;

	    var profileLbl;
	    var devModelWindowWrapper = new kendoWindowWrapper();
	    profileLbl = "Search Patient";
		if(sessionStorage.clientTypeId == "2"){
			profileLbl = "Search Service User";
		}
	    devModelWindowWrapper.openPageWindow("../../html/patients/searchPatient.html", profileLbl, popW, popH, true, closePtRAddAction);
}
var roseterPID = "";
var rosterPName = "";
function closePtRAddAction(evt,returnData){
	if(returnData && returnData.selItem && returnData.selItem.PID){
		var pID = returnData.selItem.PID;
		roseterPID = pID;
		var pName = returnData.selItem.FN+" "+returnData.selItem.MN+" "+returnData.selItem.LN;
		rosterPName = pName;
		if(pID != ""){
			$("#txtRPatient").val(pName);
		}
	}
}
function onClickSearch(){
	 var popW = "60%";
	    var popH = 500;

	    var profileLbl;
	    var devModelWindowWrapper = new kendoWindowWrapper();
	    profileLbl = "Search Service User";
		// if(sessionStorage.clientTypeId == "2"){
		// 	 profileLbl = "Search Service User";
		// }
	    devModelWindowWrapper.openPageWindow("../../html/patients/searchPatient.html", profileLbl, popW, popH, true, closePtAddAction);
	/*var scheduler = $("#scheduler").data("kendoScheduler");
	if(scheduler){
		var dt = scheduler.options.date;
		onClickAppSearch(dt);
	}*/

	/*var cmbDoctor = $("#cmbDoctor").data("kendoComboBox");
	var cmbFacility = $("#cmbFacility").data("kendoComboBox");
	var scheduler = $("#scheduler").data("kendoScheduler");
	var dtFromDate = new Date();//scheduler.date();//new Date();//$("#dtFromDate").data("kendoDatePicker");
	//var dtFromDate = $("#dtFromDate").data("kendoDatePicker");

	var providerId = cmbDoctor.value();
	var fromDate = dtFromDate;//dtFromDate.value();

	var strDT = kendo.toString(new Date(fromDate), "dd-MMM-yyyy");

	var scheduler = $("#scheduler").data("kendoScheduler");
	scheduler.dataSource.data([]);
	var patientListURL = ipAddress+"/appointment/by-provider/"+providerId+"/"+strDT+"/1";
	 getAjaxObject(patientListURL,"GET",onPatientListData,onErrorMedication);*/
}
function onClickAppSearch(dt){
	//var cmbDoctor = $("#cmbDoctor").data("kendoComboBox");
	//var ms1 = $("#required").getKendoMultiSelect();
    //var values = ms1.value();
	var cmbFacility = $("#cmbFacility").data("kendoComboBox");
	var scheduler = $("#scheduler").data("kendoScheduler");
	var dtFromDate = dt;//new Date();//scheduler.date();//new Date();//$("#dtFromDate").data("kendoDatePicker");
	//var dtFromDate = $("#dtFromDate").data("kendoDatePicker");



	//var ms1 = $("#required").getKendoMultiSelect();
	//var values = ms1.value();

	var values = $('#cmbProvider').multipleSelect('getSelects');

    if(values.length == 0){
		customAlert.error("Error", "Please select staff");
		return;
	}
	var providerId = values;//cmbDoctor.value();
	var fromDate = dtFromDate;//dtFromDate.value();

	var day = dt.getDate();
	var month = dt.getMonth();
	month = month+1;
	var year = dt.getFullYear();

	var stDate = month+"/"+day+"/"+year;
	stDate = stDate+" 00:00:00";

	var startDate = new Date(stDate);
	var stDateTime = startDate.getTime();

	var etDate = month+"/"+day+"/"+year;
	etDate = etDate+" 23:59:59";

	var endtDate = new Date(etDate);
	var edDateTime = endtDate.getTime();
	var patientListURL = "";
	scheduler.dataSource.data([]);

	var intfacilityid = Number($("#cmbFacility option:selected").val());

	if(DAY_VIEW == "DayView"){
		//edDateTime = getGMTDateFromLocaleDate(edDateTime);
		//stDateTime = getGMTDateFromLocaleDate(stDateTime);
		patientListURL = ipAddress+"/appointment/list/?facility-id="+intfacilityid+"&provider-id="+providerId.join()+"&to-date="+edDateTime+"&from-date="+stDateTime+"&is-active=1";//+"&is-active=1&is-deleted=0"
		//console.log(patientListURL);
		//console.log(new Date(stDateTime)+","+new Date(edDateTime))
	}else if(DAY_VIEW == "WeekView"){
			var startDate = new Date(stDate);
			var endtDate = new Date(etDate);

			var day = startDate.getDate()-startDate.getDay();
			startDate.setDate(day);

			endtDate.setDate(day+startDate.getDay());
			var stDateTime = startDate.getTime();
			var edDateTime = endtDate.getTime();

			//edDateTime = getGMTDateFromLocaleDate(edDateTime);
			//stDateTime = getGMTDateFromLocaleDate(stDateTime);

			//console.log(new Date(stDateTime)+","+new Date(edDateTime));
			patientListURL = ipAddress+"/appointment/list/?facility-id="+intfacilityid+"&provider-id="+providerId.join()+"&to-date="+edDateTime+"&from-date="+stDateTime+"&is-active=1";;
		}else{
			var startDate = new Date(stDate);
			var endtDate = new Date(etDate);
			startDate.setDate(1);
			endtDate.setDate(31);
			var stDateTime = startDate.getTime();
			var edDateTime = endtDate.getTime();

			//edDateTime = getGMTDateFromLocaleDate(edDateTime);
			//stDateTime = getGMTDateFromLocaleDate(stDateTime);

			patientListURL = ipAddress+"/appointment/list/?facility-id="+intfacilityid+"&provider-id="+providerId.join()+"&to-date="+edDateTime+"&from-date="+stDateTime+"&is-active=1";;
		}
	 getAjaxObject(patientListURL,"GET",onPatientListData,onErrorMedication);
}
function onClickSave(){
	/*var scheduler = $("#scheduler").data("kendoScheduler");
	if(scheduler){
		scheduler.setOptions({
	          minorTick: 15,
	          majorTick:15,
	          minorTickCount:1,
	          startTime: new Date("2017/12/11 12:00 PM"),
	          endTime: new Date("2017/12/11 2:00 PM")
	      });
	      scheduler.view(scheduler.view().name);

	      setTimeout(function(){
	    	  scheduler.resize(true);
			},2000);
	}*/
}


function onClickStaffApptReportView() {

    var txtStaffApptFromDate = $("#txtStaffApptFromDate").data("kendoDatePicker");
    var txtStaffApptToDate = $("#txtStaffApptToDate").data("kendoDatePicker");

    var isValid = true;
    var strMessage = "";

    var selected = $('#cmbStaff').multipleSelect('getSelects');

    if (selected == null || selected.length == 0) {
        strMessage = "Please select atleast one staff.";
        isValid = false;
    }

    if (txtStaffApptFromDate && txtStaffApptToDate && isValid) {

        var fDate = new Date(txtStaffApptFromDate.value());
        var tDate = new Date(txtStaffApptToDate.value());
        if (fDate.getTime() > tDate.getTime()) {
            isValid = false;
            strMessage = "From Date should be less than To Date.";
        }
    }



    if (isValid) {
        $("#divSendEmail").css("display", "none");
        $("#prsStaffApptReport3").html("");

        onGetStaffApptReportView();
    } else {
        customAlert.error("Error", strMessage);
    }
}

function onClickSendMail() {
    var txtStaffApptFromDate = $("#txtStaffApptFromDate").data("kendoDatePicker");
    var txtStaffApptToDate = $("#txtStaffApptToDate").data("kendoDatePicker");

    var fDate = txtStaffApptFromDate.value();
    var tDate = txtStaffApptToDate.value();


    var selFDate = new Date(fDate);
    var selTDate = new Date(tDate);
    var stDateTime = getFromDate(selFDate);
    var edDateTime = getToDate(selTDate);

    var txtStaffApptReportFacility = $("#txtStaffApptReportFacility").data("kendoComboBox");

    var providerIds = [];

    providerIds = $('#cmbStaff').multipleSelect('getSelects');

	var timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
    var sendEmailURL = ipAddress + "/homecare/email/provider/appointments/?is-active=1&is-deleted=0&provider-id=" + mailProviderIds + "&date-of-appointment=:bt:" + stDateTime + "," + edDateTime + "&time-zone=" + timeZone;

    getAjaxObject(sendEmailURL, "GET", onSendEmail, onErrorMedication);


}

function onSendEmail(dataObj) {
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            var msg = "Mail sent successfully.";
            displaySessionErrorPopUp("Info", msg, function (res) {
                $("#divSendEmail").css("display", "none");
                $("#prsStaffApptReport3").html("");

                $('#cmbStaff').multipleSelect('uncheckAll');

                $("#txtStaffApptFromDate").kendoDatePicker({ format: dtFMT, value: new Date() });
                $("#txtStaffApptToDate").kendoDatePicker({ format: dtFMT, value: new Date() });

                // var txtStaffApptReportFacility = $("#txtStaffApptReportFacility").data("kendoComboBox");
                // if (txtStaffApptReportFacility && txtStaffApptReportFacility.selectedIndex < 0) {
                //     txtStaffApptReportFacility.select(0)
				// }

            })
        } else {
            customAlert.error("Error", dataObj.response.status.message);
        }
    } else {
        customAlert.error("Error", dataObj.response.status.message);
    }
}

function onCreate(dataObj){
	//console.log(dataObj);
	if(dataObj && dataObj.response && dataObj.response.status){
		if(dataObj.response.status.code == "1"){
			appCount = appCount+1;
			if(appProviderArr.length == appCount){
				if(!updateFlag){
					customAlert.error("Info", "Appointment created successfully");
				}else{
					customAlert.error("Info", "Appointment updated successfully");
				}
				var dt =saveDate;
				onClickAppSearch(dt);
			}else{
				appointmentSave();
			}
			/**/

			/*var obj = {};
			obj.status = "success";
			obj.operation = "add";
			popupClose(obj);*/


		}else{
			customAlert.error("Error", dataObj.response.status.message);
		}
	}
}

function onClickDeleteRoster(){
	 var selectedItems = angularPTUIgridWrapper.getSelectedRows();
	 if(selectedItems && selectedItems.length>0){
		 customAlert.confirm("Confirm", "Are you sure you want delete?",function(response){
			 if(response.button == "Yes"){
				 var obj = selectedItems[0];
				 if(obj.idk){
					 var rosterObj = {};
					 rosterObj.id = obj.idk;
					 rosterObj.modifiedBy = Number(sessionStorage.userId);;
					 rosterObj.isActive = 0;
					 rosterObj.isDeleted = 1;
					 var dataUrl = ipAddress+"/patient/roster/delete";
						createAjaxObject(dataUrl,rosterObj,"POST",onPatientRosterDelete,onError);
				 }else{
					 angularPTUIgridWrapper.deleteItem(selectedItems[0]);
				 }
			 }
		 });
	 }
}
function onPatientRosterDelete(dataObj){
	onClickRPSearch();
}


var allRosterRows = [];
var allRosterRowCount = 0;
var allRosterRowIndex = 0;
function onClickCreateRoster(){
	allRosterRowCount = 0;
	allRosterRowIndex = 0;
	allRosterRows = angularPTUIgridWrapper.getAllRows();
	allRosterRowCount = allRosterRows.length;
	if(allRosterRowCount>0){
		saveRoster();
	}
}
function saveRoster(){
	if(allRosterRowCount>allRosterRowIndex){
		var objRosterEntiry = allRosterRows[allRosterRowIndex];
		var rosterObj = objRosterEntiry.entity;
		rosterObj.createdBy = Number(sessionStorage.userId);
		rosterObj.isActive = 1;
		rosterObj.isDeleted = 0;

		rosterObj.patientId = rosterObj.patientK;
		rosterObj.facilityId = rosterObj.facilityK;
		rosterObj.contractId = rosterObj.contractK;
		rosterObj.providerId = rosterObj.providerK;
		rosterObj.weekDay = rosterObj.dowK;
		rosterObj.fromTime = rosterObj.fromTimeK;
		rosterObj.toTime = rosterObj.toTimeK;
		rosterObj.careTypeId = 2;
		rosterObj.duration = rosterObj.duration;
		rosterObj.reason = rosterObj.reason;
		rosterObj.notes = rosterObj.notes;
		var dataUrl = "";
		if(rosterObj.idk){
			var rObj = {};
			rObj.id = rosterObj.idk;
			rObj.modifiedBy = Number(sessionStorage.userId);
			rObj.isActive = 1;
			rObj.isDeleted = 0;
			rObj.patientId = rosterObj.patientK;
			rObj.contractId = rosterObj.contractK;
			rObj.providerId = rosterObj.providerK;
			rObj.weekDay = rosterObj.dowK;
			rObj.fromTime = rosterObj.fromTimeK;
			rObj.toTime = rosterObj.toTimeK;
			rObj.careTypeId = rosterObj.careTypeId;
			rObj.duration = rosterObj.duration;
			rObj.reason = rosterObj.reason;
			rObj.notes = rosterObj.notes;
			rObj.facilityId = rosterObj.facilityK;

			dataUrl = ipAddress+"/patient/roster/update";
			createAjaxObject(dataUrl,rObj,"POST",onPatientRosterCreate,onError);
		}else{
			dataUrl = ipAddress+"/patient/roster/create";
			createAjaxObject(dataUrl,rosterObj,"POST",onPatientRosterCreate,onError);
		}
	}
}
function onPatientRosterCreate(dataObj){
	allRosterRowIndex = allRosterRowIndex+1;
	if(allRosterRowCount == allRosterRowIndex){
		customAlert.error("Info", "Roster created successfully");
		buildPatientRosterList([]);
		onClickRPSearch();
		//getPatientRosterList();
	}else{
		saveRoster();
	}
}

function onClickCarePlan(){
	if(roseterPID && rosterPName){
		var popW = "90%";
	    var popH = "80%";
	    var profileLbl;
	    var devModelWindowWrapper = new kendoWindowWrapper();
	    profileLbl = "Care Plan";
	   showPatientRoster();
	    devModelWindowWrapper.openPageWindow("../../html/patients/createPlanRoster.html", profileLbl, popW, popH, false, closeRosterPlan);
	}
}
function closeRosterPlan(evt,returnData){

}
function onClickCreateRosterAppointment(){
	var txtRPatient = $("#txtRPatient").data("kendoComboBox");
	if(txtRPatient.value()){
		var popW = "400px";
	    var popH = "500px";
	    var profileLbl;
	    var devModelWindowWrapper = new kendoWindowWrapper();
	    profileLbl = "Create Roster Appointment";
	    parentRef.pid = txtRPatient.value();
	    devModelWindowWrapper.openPageWindow("../../html/patients/createRosterAppointment.html", profileLbl, popW, popH, false, closeRosterAppointment);
	}

}
function closeRosterAppointment(evt,returnData){

}
function onClickEditRoster(){
	 var selectedItems = angularPTUIgridWrapper.getSelectedRows();
	 if(selectedItems && selectedItems.length>0){
		 var popW = "60%";
		    var popH = "60%";
		    var profileLbl;
		    var devModelWindowWrapper = new kendoWindowWrapper();
		    profileLbl = "Edit Roster";
		    parentRef.RS = "edit";
		    parentRef.selRSItem = selectedItems[0];
		    showPatientRoster()
		    devModelWindowWrapper.openPageWindow("../../html/patients/createRoster.html", profileLbl, popW, popH, false, closePatientRoster);
	 }
}

function showPatientRoster(){
	//var txtRFacility = $("#txtRFacility").data("kendoComboBox");
	var txtRPatient = $("#txtRPatient").data("kendoComboBox");

	parentRef.pid = roseterPID;
	parentRef.pname = rosterPName;



	parentRef.fid = Number($("#txtRFacility option:selected").val());
	parentRef.fname = $("#txtRFacility option:selected").text();
}
function onClickAddRoster(){
	//angularPTUIgridWrapper.insert({},0);
	var popW = "60%";
    var popH = "60%";
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Create Roster";
    parentRef.RS = "create";
    parentRef.selRSItem = null;
    showPatientRoster();
    devModelWindowWrapper.openPageWindow("../../html/patients/createRoster.html", profileLbl, popW, popH, false, closePatientRoster);
	/*var txtDuration = $("#txtDuration").val();
	txtDuration = $.trim(txtDuration);
	if(txtDuration != ""){
		var txtRPatient = $("#txtRPatient").data("kendoComboBox");
		var txtRFecility = $("#txtRFecility").data("kendoComboBox");
		var txtRProvider = $("#txtRProvider").data("kendoComboBox");
		var txtRDOW = $("#txtRDOW").data("kendoComboBox");
		var txtStartTime = $("#txtStartTime").data("kendoTimePicker");
		var txtRReason = $("#txtRReason").data("kendoComboBox");

		var obj = {};
		obj.createdBy = Number(sessionStorage.userId);
		obj.isActive = 1;
		obj.isDeleted = 0;
		obj.patinetId = Number(txtRPatient.value());
		obj.contractId = Number(txtRFecility.value());
		obj.providerId = Number(txtRProvider.value());
		obj.weekDay = Number(txtRDOW.value());
		var dt = new Date(txtStartTime.value());
		obj.fromTime = dt.getTime();
		obj.toTime = dt.getTime();
		obj.careTypeId = 2;
		obj.duration = Number(txtDuration);
		obj.reason = txtRReason.text();
		obj.notes = $("#taRNotes").val();

		console.log(obj);

		var dataUrl = "";
		dataUrl = ipAddress+"/patient/roster/create";
		createAjaxObject(dataUrl,obj,"POST",onRosterCreate,onError);


	}else{
		customAlert.error("error", "Enter Duration");
	}*/
}
function closePatientRoster(evt,returnData){
	//console.log(returnData);
	if(returnData && returnData.status == "success"){
		var obj = returnData;
		var st = returnData.rs;
		if(st == "create"){
			angularPTUIgridWrapper.insert(obj,0);
			/*var dow = obj.dow;
			var dowk = obj.dowK;

			var strDowArray = dow.split(",");
			var strDowKArray = dowk.split(",");*/

			/*for(var s=0;s<strDowArray.length;s++){
				var sObj = obj;
				sObj.dow = strDowArray[s];
				sObj.dowK = strDowKArray[s];
				angularPTUIgridWrapper.insert(sObj,0);
				angularPTUIgridWrapper.refreshGrid();
			}*/
		}else{
			 var selectedItems = angularPTUIgridWrapper.getSelectedRows();
			 var dgItem = selectedItems[0];
			 dgItem.contract = obj.contract;
			 dgItem.contractK = obj.contractK;

			 dgItem.facility = obj.facility;
			 dgItem.facilityK = obj.facilityK;

			 dgItem.provider = obj.provider;
			 dgItem.providerK = obj.providerK;

			 dgItem.fromTime = obj.fromTime;
			 dgItem.fromTimeK = obj.fromTimeK;

			 dgItem.toTime = obj.toTime;
			 dgItem.toTimeK = obj.toTimeK;

			 dgItem.duration = obj.duration;

			 dgItem.AppType = obj.AppType;
			 dgItem.AppTypeK = obj.AppTypeK;

			 dgItem.reason = obj.reason;
			 dgItem.reasonK = obj.reasonK;

			 dgItem.notes = obj.notes;

			 dgItem.patient = obj.patient;
			 dgItem.patientK = obj.patientK;

			 dgItem.dow = obj.dow;
			 dgItem.dowK = obj.dowK;
		}

		angularPTUIgridWrapper.refreshGrid();
	}
}
function onRosterCreate(dataObj){
	//console.log(dataObj);
}
function onClickRPSearch(){//patient-id
	var txtRPatient = $("#txtRPatient").data("kendoComboBox");
	if(txtRPatient){
		roseterPID = txtRPatient.value();
	}
	if(roseterPID != ""){
		var patientRosterURL = ipAddress+"/patient/roster/list/?patient-id="+roseterPID;
	}else{
		var patientRosterURL = ipAddress+"/patient/roster/list/?is-active=1";
	}
	buildPatientRosterList([]);
	getAjaxObject(patientRosterURL,"GET",onGetRosterList,onErrorMedication);
}
function getPatientRosterList(){
	var patientRosterURL = ipAddress+"/patient/roster/list/?is-active=1";
	getAjaxObject(patientRosterURL,"GET",onGetRosterList,onErrorMedication);
}
function getWeekDayName(wk){
	var wn = "";
	if(wk == 1){
		return "Monday";
	}else if(wk == 2){
		return "Tuesday";
	}else if(wk == 2){
		return "Tuesday";
	}else if(wk == 3){
		return "Wednesday";
	}else if(wk == 4){
		return "Thursday";
	}else if(wk == 5){
		return "Friday";
	}else if(wk == 6){
		return "Saturday";
	}else if(wk == 0){
		return "Sunday";
	}
	return wn;
}
function onGetRosterList(dataObj){
	//console.log(dataObj);
	var rosterArray = [];
	if(dataObj && dataObj.response &&  dataObj.response.status && dataObj.response.status.code == 1){
		if($.isArray(dataObj.response.patientRoster)){
			rosterArray = dataObj.response.patientRoster;
		}else{
			rosterArray.push(dataObj.response.patientRoster);
		}
	}
	for(var i=0;i<rosterArray.length;i++){
		rosterArray[i].idk = rosterArray[i].id;
		rosterArray[i].contractK = rosterArray[i].contractId;
		rosterArray[i].contract = rosterArray[i].contract;

		rosterArray[i].facility = rosterArray[i].facility;
		rosterArray[i].facilityK = rosterArray[i].facilityId;

		rosterArray[i].provider = rosterArray[i].provider;
		rosterArray[i].providerK = rosterArray[i].providerId;

		rosterArray[i].dow = getWeekDayName(rosterArray[i].weekDay);
		rosterArray[i].dowK = rosterArray[i].weekDay;

		var sdt = new Date();
		sdt.setHours(0, 0, 0, 0);
		sdt.setMinutes(Number(rosterArray[i].fromTime));
		//var sdms = rosterArray[i].fromTime;
		rosterArray[i].fromTimeK = rosterArray[i].fromTime;
		rosterArray[i].fromTime = kendo.toString(sdt,"t");


		var edt = new Date();
		edt.setHours(0, 0, 0, 0);
		edt.setMinutes(Number(rosterArray[i].toTime));

		rosterArray[i].toTimeK = rosterArray[i].toTime;
		rosterArray[i].toTime = kendo.toString(edt,"t");

		rosterArray[i].duration = rosterArray[i].duration;

		rosterArray[i].AppType = rosterArray[i].AppType;
		rosterArray[i].AppTypeK = rosterArray[i].AppTypeK;

		rosterArray[i].reason = rosterArray[i].reason;
		rosterArray[i].reasonK = rosterArray[i].reasonK;

		rosterArray[i].dow = rosterArray[i].dow;
		rosterArray[i].dowK = rosterArray[i].dowK;

		rosterArray[i].patient = rosterArray[i].patientId;
		rosterArray[i].patientK = rosterArray[i].patientId;
	}
	buildPatientRosterList(rosterArray);
}
function buildPatientRosterList(dataSource){
	var gridColumns = [];
	var otoptions = {};
	otoptions.noUnselect = false;
	otoptions.rowHeight = 100;
	gridColumns.push({
        "title": "Contact",
        "field": "contract",
        "width": "15%",
    });
	  gridColumns.push({
	        "title": "DOW",
	        "field": "dow",
	        "width": "10%",
	    });
	  gridColumns.push({
	        "title": "Start Time",
	        "field": "fromTime",
	        "width": "10%",
	    });
	  gridColumns.push({
	        "title": "End Time",
	        "field": "toTime",
	        "width": "10%",
	    });
	  gridColumns.push({
	        "title": "Duration",
	        "field": "duration",
	        "width": "5%",
	    });
	  gridColumns.push({
	        "title": "Visit/Appointment Type",
	        "field": "AppType",
	        "width": "20%",
	    });
	  gridColumns.push({
	        "title": "Reason",
	        "field": "reason",
	        "width": "10%",
	    });
	  gridColumns.push({
	        "title": "Provider",
	        "field": "provider",
	        "width": "15%",
	    });
	   gridColumns.push({
	        "title": "Facility",
	        "field": "facility",
	        "width": "15%",
	    });
    gridColumns.push({
        "title": "Notes",
        "field": "notes",
    });

    angularPTUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
}
function onPTChange(){
	setTimeout(function(){
		var selectedItems = angularPTUIgridWrapper.getSelectedRows();
		 //console.log(selectedItems);
		 if(selectedItems && selectedItems.length>0){
			 $("#btnREdit").prop("disabled", false);
			 $("#btnRDel").prop("disabled", false);
		 }else{
			 $("#btnREdit").prop("disabled", true);
			 $("#btnRDel").prop("disabled", true);
		 }
	},100)
}
function showOperations(){
	var node = '<div style="text-align:center"><span style="cursor:pointer;font-weight:bold;font-size:15px">Edit</span>&nbsp;&nbsp;<span style="cursor:pointer;font-weight:bold;font-size:15px">Delete</span>';
	node = node+"</div>";
	return node;
}
function onClick2dView1(){
	var txt2dDate = $("#txt2dDate").data("kendoDatePicker");
	var txt2dProvider1 = $("#txt2dProvider1").data("kendoComboBox");
	if(txt2dProvider1 && txt2dDate){
		var patientListURL = ipAddress+"/homecare/availabilities/?parent-id="+txt2dProvider1.value()+"&parent-type-id=201&is-active=1&is-deleted=0";
		getAjaxObject(patientListURL,"GET",onPatientAvlTime1,onErrorMedication);
	}
}
var pt2dAvlTimeArray1 = [];
function onPatientAvlTime1(dataObj){
	//console.log(dataObj);
	pt2dAvlTimeArray1= [];
	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		if($.isArray(dataObj.response.availabilities)){
			pt2dAvlTimeArray1 = dataObj.response.availabilities;
		}else{
			pt2dAvlTimeArray1.push(dataObj.response.availabilities);
		}
	}
	//var txt2dFacility = $("#txt2dFacility").data("kendoComboBox");
	var intfacilityid = Number($("#txt2dFacility option:selected").val());
	var txtPRDate = $("#txt2dDate").data("kendoDatePicker");
	var txt2dProvider1 = $("#txt2dProvider1").data("kendoComboBox");
	var dt = txtPRDate.value();

	var day = dt.getDate();
	var month = dt.getMonth();
	month = month+1;
	var year = dt.getFullYear();

	var stDate = month+"/"+day+"/"+year;
	stDate = stDate+" 00:00:00";

	var startDate = new Date(stDate);
	var stDateTime = startDate.getTime();

	var etDate = month+"/"+day+"/"+year;
	etDate = etDate+" 23:59:59";

	var endtDate = new Date(etDate);
	var edDateTime = endtDate.getTime();



	var patientListURL = ipAddress+"/appointment/list/?facility-id="+intfacilityid+"&provider-id="+txt2dProvider1.value()+"&to-date="+edDateTime+"&from-date="+stDateTime+"&is-active=1";//+"&is-active=1&is-deleted=0"
	getAjaxObject(patientListURL,"GET",onStaffPatientList2DData1,onErrorMedication);
}
var  pt2dAppTimeArray1 = [];
var pt2StaffArray1 = [];
function onStaffPatientList2DData1(dataObj){
	//console.log(dataObj);
	pt2dAppTimeArray1 = [];
	pt2StaffArray1 = [];
	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		if($.isArray(dataObj.response.appointment)){
			pt2dAppTimeArray1 = dataObj.response.appointment;
		}else{
			pt2dAppTimeArray1.push(dataObj.response.appointment);
		}
	}

	columnSequenceSort(pt2dAppTimeArray1);
	for(var s=0;s<pt2dAppTimeArray1.length;s++){
		var item = pt2dAppTimeArray1[s];
		if(item){
			if(staffSArray.length == 0){
				pt2StaffArray1.push(item);
			}else{
				if(!isPt2StaffSExist1(item.dateOfAppointment)){
					pt2StaffArray1.push(item);
				}
			}
		}
	}
	var prDTView = [];
	for(var x=0;x<psrViewDateArray2DView.length;x++){
		var xItem = psrViewDateArray2DView[x];
		if(xItem){
				prDTView.push(xItem);
		}
	}
	var txt2dProvider1 = $("#txt2dProvider1").data("kendoComboBox");
	var provider2dArray1 = [];
	var obj = {};
	obj.name  = txt2dProvider1.text();
	obj.id  = txt2dProvider1.value();
	provider2dArray1.push(obj);

	$("#pr2dView1").text("");
	var strTable = '<table class="table" id="tdh">';
	strTable = strTable+'<thead class="fillsHeader">';
	var pid1Array = [];
	var pids1Array = [];
	for(var i=0;i<prDTView.length;i++){
		var item = prDTView[i];
		strTable = strTable+'<tr style="height:20px">';
		strTable = strTable+'<th class="textAlign whiteColor fntStyle" style="width:80px;height:20px;border:1px solid #000;">'+item.tm +'</th>';
		var strObj = getProviderStaff2dAppDays(item,provider2dArray1[0],pt2dAppTimeArray1);
		var pid = "";
		if(strObj.id){
			pid = strObj.id;
			if(pid1Array.length == 0){
				pid1Array.push(pid);
			}else{
				if(pid1Array.indexOf(pid) == -1){
					pid1Array.push(pid);
					//pid = "";
				}else{
					pid = "";
				}
			}
		}
		if(pid){
			strTable = strTable+'<th class="textAlign whiteColor fntStyle" style="width:50px;height:20px;border:1px solid #000;"><div style="width:20px;height:10px;margin-left:10px;background:green"></div></th>';
		}else{
			strTable = strTable+'<th class="textAlign whiteColor fntStyle" style="width:50px;height:20px;border:1px solid #000;"></th>';
		}
		for(var y=0;y<provider2dArray1.length;y++){
			var pItem = provider2dArray1[y];
				var strObj = getProviderStaff2dAppDays(item,provider2dArray1[y],pt2dAppTimeArray1);
				var strBData = getStaff2AppDays(item,provider2dArray1[y]);
				if(strObj){
					var pid = "";
					if(strObj.id){
						pid = strObj.id;
						var name = strObj.name;
						var st = strObj.st;
						var et = strObj.et;
						var reason = strObj.reason;
						var desc = strObj.desc;
						var strName = name+"<br />"+desc+","+reason+"<br />"+st+"-"+et+"<br />";
						if(pids1Array.length == 0){
							pids1Array.push(pid);
						}else{
							if(pids1Array.indexOf(pid) == -1){
								pids1Array.push(pid);
								//pid = "";
							}else{
								strName = "";
							}
						}
					}
					strTable = strTable+'<th title="'+strObj.strData+'" style="background:green;border:1px solid green;height:20px;color:#FFF">'+strName+'</th>';
				}else if(strBData){
					strTable = strTable+'<th title="'+strBData+'" style="background:white;border:1px solid white;height:20px;"></th>';
				}else{
					strTable = strTable+'<th style="background:orange;border:1px solid white;height:20px;"></th>';
				}
		}
		strTable = strTable+'</tr>';

	}
	strTable = strTable+'</thead>';
	strTable = strTable+'</table>';
	$("#pr2dView1").append(strTable);

	/*$("#pr2dView1").text("");
	var strTable = '<table class="table" id="tdh">';
	strTable = strTable+'<thead class="fillsHeader">';
	strTable = strTable+'<tr style="height:20px">';
	//strTable = strTable+'<th class="textAlign whiteColor headcol fntStyle">Staff Name</th>';
	for(var i=0;i<prDTView.length;i++){
		var item = prDTView[i];
		//var strDT = kendo.toString(new Date(item.dateOfAppointment),"t");
		strTable = strTable+'<th class="textAlign whiteColor fntStyle">'+item.tm +'</th>';
	}
	strTable = strTable+'</tr>';
	strTable = strTable+'</thead>';
	strTable = strTable+'<tbody>';
	var txt2dProvider = $("#txt2dProvider1").data("kendoComboBox");
	var providerArray = [];
	var obj = {};
	obj.name  = txt2dProvider.text();
	obj.id  = txt2dProvider.value();
	providerArray.push(obj);
	for(var y=0;y<providerArray.length;y++){
		var pItem = providerArray[y];
		var obj = getProviderStatusCount(pItem.id,pt2dAppTimeArray);
		var strTitle = pItem.lastName+","+pItem.firstName+" <br/> "+obj.am+"+"+obj.pm+"="+obj.tot;
		strTable = strTable+'<tr style="height:20px">';
		//strTable = strTable+'<td class="headcol fntStyle">'+strTitle+'</td>';
		for(var z=0;z<prDTView.length;z++){
			var item = prDTView[z];
			var strData = getProviderStaff2dAppDays1(item,providerArray[y],pt2dAppTimeArray1);
			var strBData = getStaff2AppDays1(item,providerArray[y]);

			//console.log(strData);
			if(strData){
				strTable = strTable+'<td title="'+strData+'" style="background:green;border:1px solid green;height:15px;"></td>';
			}else if(strBData){
				strTable = strTable+'<td title="'+strBData+'" style="background:white;border:1px solid white;height:15px;"></td>';
			}else{
				strTable = strTable+'<td style="background:orange;border:1px solid white;height:15px;"></td>';
			}
		}
		strTable = strTable+'</tr>';
	}
	strTable = strTable+'</tbody>';
	strTable = strTable+'</table>';
	$("#pr2dView1").append(strTable);*/
}
function onClickPTSView(){
	//var txt2dFacility = $("#txtPTFacility").data("kendoComboBox");
	var intfacilityid = Number($("#txtPTFacility option:selected").val());
	var txtPRDate = $("#txtPTDate").data("kendoDatePicker");
	var dt = txtPRDate.value();

	var day = dt.getDate();
	var month = dt.getMonth();
	month = month+1;
	var year = dt.getFullYear();

	var stDate = month+"/"+day+"/"+year;
	stDate = stDate+" 00:00:00";

	var startDate = new Date(stDate);
	var stDateTime = startDate.getTime();

	var etDate = month+"/"+day+"/"+year;
	etDate = etDate+" 23:59:59";

	var endtDate = new Date(etDate);
	var edDateTime = endtDate.getTime();
	//var patientListURL = ipAddress+"/appointment/list/?facility-id="+txt2dFacility.value()+"&provider-id="+txt2dProvider.value()+"&to-date="+edDateTime+"&from-date="+stDateTime+"&is-active=1";//+"&is-active=1&is-deleted=0"
	var patientListURL = ipAddress+"/patient/list/?facility-id="+intfacilityid+"&is-active=1";
	getAjaxObject(patientListURL,"GET",onGetFacilityByPatient,onErrorMedication);
}
var strSUArray = [];
function onGetFacilityByPatient(dataObj){
	//console.log(dataObj);
	strSUArray = [];
	$("#SUscheduler").text("");
	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		if($.isArray(dataObj.response.patient)){
			strSUArray = dataObj.response.patient;
		}else{
			strSUArray.push(dataObj.response.patient);
		}
	}

	if(strSUArray.length > 0) {

		var txtPRDate = $("#txtPTDate").data("kendoDatePicker");
		var dt = txtPRDate.value();

		var day = dt.getDate();
		var month = dt.getMonth();
		month = month + 1;
		var year = dt.getFullYear();

		var stDate = month + "/" + day + "/" + year;
		stDate = stDate + " 00:00:00";

		var startDate = new Date(stDate);
		var stDateTime = startDate.getTime();

		var etDate = month + "/" + day + "/" + year;
		etDate = etDate + " 23:59:59";

		var endtDate = new Date(etDate);
		var edDateTime = endtDate.getTime();
		var pdArray = [];
		if (strSUArray.length > 0) {
			for (var p = 0; p < strSUArray.length; p++) {
				pdArray.push(strSUArray[p].id);
			}
		}
		//var txtPTFacility = $("#txtPTFacility").data("kendoComboBox");
		var intfacilityid = Number($("#txtPTFacility option:selected").val());
		var patientListURL = ipAddress + "/appointment/list/?facility-id=" + intfacilityid + "&to-date=" + edDateTime + "&from-date=" + stDateTime + "&is-active=1";//+"&is-active=1&is-deleted=0"

		getAjaxObject(patientListURL, "GET", onPatientAppList, onErrorMedication);
	}
	else{
		customAlert.info("Info","No appointment details.");
	}

}
var suAppArray = [];
function onPatientAppList(dataObj){

	//console.log(dataObj);
	suAppArray = [];
	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		if($.isArray(dataObj.response.appointment)){
			suAppArray = dataObj.response.appointment;
		}else{
			suAppArray.push(dataObj.response.appointment);
		}
	}

	$("#SUscheduler").text("");
	$("#SUscheduler").append("");
	$("#SUscheduler1").append("");
	if(suAppArray && suAppArray[0] != undefined && suAppArray.length>0){
		var strTable = '<table class="table" id="tdh">';
		strTable = strTable+'<thead class="fillsHeader">';
		strTable = strTable+'<tr style="background:#999">';
		strTable = strTable+'<th style="width:150px;border-right:1px solid #FFF">Service User</th>';
        strTable = strTable+'<th style="width:100px;border-right:1px solid #FFF">Reason</th>';
		strTable = strTable+'<th style="width:50px;border-right:1px solid #FFF">AM</th>';
		strTable = strTable+'<th style="width:50px;border-right:1px solid #FFF">PM</th>';
		strTable = strTable+'<th style="width:50px;border-right:1px solid #FFF">Total</th>';
		strTable = strTable+'<th style="width:100px;border-right:1px solid #FFF">View</th>';
		strTable = strTable+'</tr>';
		strTable = strTable+'</thead>';
		strTable = strTable+'<tbody>';
        strSUArray.sort(function (a, b) {
            var p1 = a.lastName.toLowerCase();
            var p2 = b.lastName.toLowerCase();
            var o1, o2;

			o1 = a.firstName.toLowerCase();
			o2 = b.firstName.toLowerCase();

            if (p1 < p2) return -1;
            if (p1 > p2) return 1;

            if (o1 < o2) return -1;
            if (o1 > o2) return 1;
            return 0;
        });
		for(var i=0;i<strSUArray.length;i++){
			var item = strSUArray[i];
			var pName = item.lastName +" "+item.firstName;
			var pTotalItem = amPMPatientInfo1(item.id);
			var am = "";
			var pm = "";
			var tot = "";
			if(pTotalItem){
				am = pTotalItem.AM;
				pm = pTotalItem.PM;
				tot = pTotalItem.TOT;
			}
			var class1  = "active";
			if(i == 0){

			}else{
				if(i%4 == 0){
					class1  = "info";
				}if(i%4 == 1){
					class1  = "success";
				}if(i%4 == 2){
					class1  = "warning";
				}if(i%4 == 3){
					class1  = "danger";
				}
			}
			if(am != 0) {
				var strTempTable = "";
				strTempTable = strTempTable + '<tr class="' + class1 + '">';
				strTempTable = strTempTable + '<td class="textAlign whiteColor" style="border:1px solid #000;font-size:13px;height:10px">' + pName + '</td>';
				strTempTable = strTempTable + '<td class="textAlign whiteColor" style="border:1px solid #000;font-size:13px;height:10px">' + pTotalItem.reason + '</td>';
				strTempTable = strTempTable + '<td class="textAlign whiteColor " style="border:1px solid #000;font-size:15px;height:10px">' + am + '</td>';
				strTempTable = strTempTable + '<td class="textAlign whiteColor " style="border:1px solid #000;font-size:15px;height:10px">' + pm + '</td>';
				strTempTable = strTempTable + '<td class="textAlign whiteColor " style="border:1px solid #000;font-size:15px;height:10px">' + tot + '</td>';
				strTempTable = strTempTable + '<td class="textAlign whiteColor " style="border:1px solid #000;font-size:15px;height:10px"><button type="button" class="btn btn-primary"  pid="' + item.id + '" style="width:100px;height:30px !important"  onclick="onPTClick(event)">View</button></td>';
				strTempTable = strTempTable + '</tr>';
				strTempTable = strTempTable+'</tbody>';
				strTempTable = strTempTable+'</table>';



				$("#SUscheduler").append(strTable + strTempTable);
				$("#SUscheduler1").append("");
			}
			// else{
			// 	customAlert.info("Info","No appointments details.");
			// }
		}


	}
	else{
		customAlert.info("Info","No appointments details.");
	}
}
function onPTClick(e){
	//console.log(e);
	var pid = $(e.target).attr("pid");
	pid = Number(pid);
	$("#SUscheduler1").text("");
	if(pid){
		if(suAppArray.length>0){
			var strTable = '<table class="table" id="tdh">';
			strTable = strTable+'<thead class="fillsHeader">';
			strTable = strTable+'<tr style="background:#999">';
			strTable = strTable+'<th style="width:150px;border-right:1px solid #FFF">Service User</th>';
			strTable = strTable+'<th style="width:50px;border-right:1px solid #FFF">Carer</th>';
			strTable = strTable+'<th style="width:50px;border-right:1px solid #FFF">Start Time</th>';
			strTable = strTable+'<th style="width:50px;border-right:1px solid #FFF">Duration</th>';
            strTable = strTable+'<th style="width:100px;border-right:1px solid #FFF">Reason</th>';
			strTable = strTable+'</tr>';
			strTable = strTable+'</thead>';
			strTable = strTable+'<tbody>';
				var pArray = amPMPatientInfo(pid);
				for(var i=0;i<pArray.length;i++){
					var pTotalItem = pArray[i];
					if(pTotalItem){
						var class1  = "active";
						if(i == 0){

						}else{
							if(i%4 == 0){
								class1  = "info";
							}if(i%4 == 1){
								class1  = "success";
							}if(i%4 == 2){
								class1  = "warning";
							}if(i%4 == 3){
								class1  = "danger";
							}
						}
						strTable = strTable+'<tr class="'+class1+'">';
						strTable = strTable+'<td class="textAlign whiteColor" style="border:1px solid #000;font-size:13px;height:10px">'+pTotalItem.pName+'</td>';
						strTable = strTable+'<td class="textAlign whiteColor " style="border:1px solid #000;font-size:15px;height:10px">'+pTotalItem.prName+'</td>';
						strTable = strTable+'<td class="textAlign whiteColor " style="border:1px solid #000;font-size:15px;height:10px">'+pTotalItem.tm+'</td>';
						strTable = strTable+'<td class="textAlign whiteColor " style="border:1px solid #000;font-size:15px;height:10px">'+pTotalItem.duration+'</td>';
                        strTable = strTable+'<td class="textAlign whiteColor" style="border:1px solid #000;font-size:13px;height:10px">'+pTotalItem.reason+'</td>';
						strTable = strTable+'</tr>';
					}
				}

			strTable = strTable+'</tbody>';
			strTable = strTable+'</table>';

			$("#SUscheduler1").append(strTable);
		}
	}
}
function amPMPatientInfo(pid){
	var selPMCount = 0;
	var selAMCount = 0;
	var total = 0;
	var arr = [];
    var reason = "";
    suAppArray.sort(function (a, b) {
        var p1 = a.dateOfAppointment;
        var p2 = b.dateOfAppointment;

        if (p1 < p2) return -1;
        if (p1 > p2) return 1;
        return 0;
    });
	for(var i=0;i<suAppArray.length;i++){
		var item = suAppArray[i];
		if(item.patientId == pid){
			var obj = {};
			var dt = new Date(item.dateOfAppointment);
			obj.tm = kendo.toString(dt,"h:mm tt");
			obj.duration = item.duration;
			obj.pName = item.composition.patient.firstName+" "+item.composition.patient.lastName;
			obj.prName = item.composition.provider.firstName+" "+item.composition.provider.lastName;
            obj.reason = item.appointmentReason;
			arr.push(obj);
		}
	}
	return arr;
}
function amPMPatientInfo1(pid){
	var selPMCount = 0;
	var selAMCount = 0;
	var reason = "";
	var total = 0;
	for(var i=0;i<suAppArray.length;i++){
		var item = suAppArray[i];
		if(item && item.patientId == pid){
			var dt = new Date(item.dateOfAppointment);
			if(dt.getHours()>=12){
				selPMCount = selPMCount+1;
			}else{
				selAMCount = selAMCount+1;
			}
            reason = item.appointmentReason;
		}

	}
	var obj = {};
	obj.AM = selAMCount;
	obj.PM = selPMCount;
	obj.TOT = (selAMCount+selPMCount);
	obj.reason = reason;
	return obj;
}
function onClick2dViewMove(){
	 var popW = "60%";
	    var popH = "60%";
	    var profileLbl;
	    var devModelWindowWrapper = new kendoWindowWrapper();
	    profileLbl = "Move Appointments";
	    var cmbDate = $("#txt2dDate").data("kendoDatePicker");
	    //var txt2dFacility = $("#txt2dFacility").data("kendoComboBox");
	    var txt2dProvider = $("#txt2dProvider").data("kendoComboBox");
	    var txt2dProvider1 = $("#txt2dProvider1").data("kendoComboBox");

	    parentRef.selDate = cmbDate.value();
	    parentRef.facility = $("#txt2dFacility option:selected").text();//txt2dFacility.text();
	    parentRef.provider = txt2dProvider.text();
	    parentRef.provider1 = txt2dProvider1.text();

	    parentRef.facilityId = Number($("#txt2dFacility option:selected").val());//txt2dFacility.value();
	    parentRef.providerId = txt2dProvider.value();
	    parentRef.provider1Id = txt2dProvider1.value();

	    profileLbl = profileLbl+" -  "+kendo.toString(new Date(cmbDate.value()),"dd/MM/yyyy"); ;
	    devModelWindowWrapper.openPageWindow("../../html/patients/providerAppMoveList.html", profileLbl, popW, popH, false, closeProviderMoveList);
}
function closeProviderMoveList(){
	onClick2dView();
}
function onClick2dView(){
	var txt2dDate = $("#txt2dDate").data("kendoDatePicker");
	var txt2dProvider = $("#txt2dProvider").data("kendoComboBox");
	if(txt2dProvider && txt2dDate){
		var patientListURL = ipAddress+"/homecare/availabilities/?parent-id="+txt2dProvider.value()+"&parent-type-id=201&is-active=1&is-deleted=0";
		getAjaxObject(patientListURL,"GET",onPatientAvlTime,onErrorMedication);
	}
}

var pt2dAvlTimeArray = [];
function onPatientAvlTime(dataObj){
	//console.log(dataObj);
	pt2dAvlTimeArray = [];
	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		if($.isArray(dataObj.response.availabilities)){
			pt2dAvlTimeArray = dataObj.response.availabilities;
		}else{
			pt2dAvlTimeArray.push(dataObj.response.availabilities);
		}
	}
	//var txt2dFacility = $("#txt2dFacility").data("kendoComboBox");
	var intfacilityid = Number($("#txt2dFacility option:selected").val());
	var txtPRDate = $("#txt2dDate").data("kendoDatePicker");
	var txt2dProvider = $("#txt2dProvider").data("kendoComboBox");
	var dt = txtPRDate.value();

	var day = dt.getDate();
	var month = dt.getMonth();
	month = month+1;
	var year = dt.getFullYear();

	var stDate = month+"/"+day+"/"+year;
	stDate = stDate+" 00:00:00";

	var startDate = new Date(stDate);
	var stDateTime = startDate.getTime();

	var etDate = month+"/"+day+"/"+year;
	etDate = etDate+" 23:59:59";

	var endtDate = new Date(etDate);
	var edDateTime = endtDate.getTime();
	var patientListURL = ipAddress+"/appointment/list/?facility-id="+intfacilityid+"&provider-id="+txt2dProvider.value()+"&to-date="+edDateTime+"&from-date="+stDateTime+"&is-active=1";//+"&is-active=1&is-deleted=0"
	getAjaxObject(patientListURL,"GET",onStaffPatientList2DData,onErrorMedication);
}
var pt2dAppTimeArray = [];
var pt2StaffArray = [];
function onStaffPatientList2DData(dataObj){
	//console.log(dataObj);
	pt2dAppTimeArray = [];
	pt2StaffArray = [];
	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		if($.isArray(dataObj.response.appointment)){
			pt2dAppTimeArray = dataObj.response.appointment;
		}else{
			pt2dAppTimeArray.push(dataObj.response.appointment);
		}
	}

	columnSequenceSort(pt2dAppTimeArray);
	for(var s=0;s<pt2dAppTimeArray.length;s++){
		var item = pt2dAppTimeArray[s];
		if(item){
			if(staffSArray.length == 0){
				pt2StaffArray.push(item);
			}else{
				if(!isPt2StaffSExist(item.dateOfAppointment)){
					pt2StaffArray.push(item);
				}
			}
		}
	}
	var prDTView = [];
	for(var x=0;x<psrViewDateArray2DView.length;x++){
		var xItem = psrViewDateArray2DView[x];
		if(xItem){
				prDTView.push(xItem);
		}
	}
	var txt2dProvider = $("#txt2dProvider").data("kendoComboBox");
	var provider2dArray = [];
	var obj = {};
	obj.name  = txt2dProvider.text();
	obj.id  = txt2dProvider.value();
	provider2dArray.push(obj);
	$("#pr2dView").text("");
	var strTable = '<table class="table" id="tdh">';
	strTable = strTable+'<thead class="fillsHeader">';
	var pidArray = [];
	var pidsArray = [];
	for(var i=0;i<prDTView.length;i++){
		var item = prDTView[i];
		strTable = strTable+'<tr style="height:20px">';
		strTable = strTable+'<th class="textAlign whiteColor fntStyle" style="width:80px;height:20px;border:1px solid #000;">'+item.tm +'</th>';
		//var strData = getProviderStaff2dAppDaysDot(item,provider2dArray[0],pt2dAppTimeArray);
		var strObj = getProviderStaff2dAppDays(item,provider2dArray[0],pt2dAppTimeArray);
		var pid = "";
		if(strObj.id){
			pid = strObj.id;
			if(pidArray.length == 0){
				pidArray.push(pid);
			}else{
				if(pidArray.indexOf(pid) == -1){
					pidArray.push(pid);
					//pid = "";
				}else{
					pid = "";
				}
			}
		}
		if(pid){
			strTable = strTable+'<th class="textAlign whiteColor fntStyle" style="width:50px;height:20px;border:1px solid #000;"><div style="width:20px;height:10px;margin-left:10px;background:green"></div></th>';
		}else{
			strTable = strTable+'<th class="textAlign whiteColor fntStyle" style="width:50px;height:20px;border:1px solid #000;"></th>';
		}
		for(var y=0;y<provider2dArray.length;y++){
			var pItem = provider2dArray[y];
			var strObj = getProviderStaff2dAppDays(item,provider2dArray[y],pt2dAppTimeArray);
				var strBData = getStaff2AppDays(item,provider2dArray[y]);
				if(strObj){
					var pid = "";
					if(strObj.id){
						pid = strObj.id;
						var name = strObj.name;
						var st = strObj.st;
						var et = strObj.et;
						var reason = strObj.reason;
						var desc = strObj.desc;
						var strName = name+"<br />"+desc+","+reason+"<br />"+st+"-"+et+"<br />";
						if(pidsArray.length == 0){
							pidsArray.push(pid);
						}else{
							if(pidsArray.indexOf(pid) == -1){
								pidsArray.push(pid);
								//pid = "";
							}else{
								strName = "";
							}
						}
					}

					strTable = strTable+'<th title="'+strObj.strData+'" style="background:green;height:20px;border:1px solid green;color:#FFF">'+strName+'</th>';
				}else if(strBData){
					strTable = strTable+'<th title="'+strBData+'" style="background:white;height:20px;border:1px solid white;"></th>';
				}else{
					strTable = strTable+'<th style="background:orange;border:1px solid white;height:20px;"></th>';
				}
		}
		strTable = strTable+'</tr>';

	}
	strTable = strTable+'</thead>';
	strTable = strTable+'</table>';
	$("#pr2dView").append(strTable);

	onClick2dView1();

}
function onClickViewStaffApp(){
	var txtPRSDate = $("#txtPRSDate").data("kendoDatePicker");
	//var txtPRSFacility = $("#txtPRSFacility").data("kendoComboBox");
	if(txtPRSDate && txtPRSDate.value()){
		$("#prsscheduler5").text("");
		//https://stage.timeam.com/provider/list/?facility-id=4&is-active=1
		var facilityId = Number($("#txtPRSFacility option:selected").val());//txtPRSFacility.value();
		var patientListURL = ipAddress+"/provider/list/?facility-id="+facilityId+"&is-active=1&sort=lastName,firstName";
		getAjaxObject(patientListURL,"GET",onGetPRSSFacilityProviderList,onErrorMedication);
	}else{
		customAlert.error("Error", "Please select date");
	}
}
var prsViewArray = [];
var psrViewDateArray = [{idk:'1',h:'5',m:'0',tm:'5:00 AM'},{idk:'2',h:'5',m:'15',tm:''},{idk:'3',h:'5',m:'30',tm:''},{idk:'4',h:'5',m:'45',tm:''},{idk:'5',h:'6',m:'0',tm:'6:00 AM'}];
psrViewDateArray.push({idk:'6',h:'6',m:'15',tm:''});
psrViewDateArray.push({idk:'7',h:'6',m:'30',tm:''});
psrViewDateArray.push({idk:'8',h:'6',m:'45',tm:''});
psrViewDateArray.push({idk:'9',h:'7',m:'0',tm:'7:00 AM'});
psrViewDateArray.push({idk:'10',h:'7',m:'15',tm:''});
psrViewDateArray.push({idk:'11',h:'7',m:'30',tm:''});
psrViewDateArray.push({idk:'11',h:'7',m:'45',tm:''});
psrViewDateArray.push({idk:'12',h:'8',m:'0',tm:'8:00 AM'})
psrViewDateArray.push({idk:'13',h:'8',m:'15',tm:''});
psrViewDateArray.push({idk:'14',h:'8',m:'30',tm:''});
psrViewDateArray.push({idk:'15',h:'8',m:'45',tm:''})
psrViewDateArray.push({idk:'16',h:'9',m:'0',tm:'9:00 AM'});
psrViewDateArray.push({idk:'17',h:'9',m:'15',tm:''});
psrViewDateArray.push({idk:'18',h:'9',m:'30',tm:''});
psrViewDateArray.push({idk:'19',h:'9',m:'45',tm:''});
psrViewDateArray.push({idk:'20',h:'10',m:'0',tm:'10:00 AM'});
psrViewDateArray.push({idk:'21',h:'10',m:'15',tm:''});
psrViewDateArray.push({idk:'22',h:'10',m:'30',tm:''});
psrViewDateArray.push({idk:'23',h:'10',m:'45',tm:''});
psrViewDateArray.push({idk:'24',h:'11',m:'0',tm:'11:00 AM'});
psrViewDateArray.push({idk:'25',h:'11',m:'15',tm:''});
psrViewDateArray.push({idk:'26',h:'11',m:'30',tm:''});
psrViewDateArray.push({idk:'27',h:'11',m:'45',tm:''});
psrViewDateArray.push({idk:'28',h:'12',m:'0',tm:'12:00 PM'});
psrViewDateArray.push({idk:'29',h:'12',m:'15',tm:''});
psrViewDateArray.push({idk:'30',h:'12',m:'30',tm:''});
psrViewDateArray.push({idk:'31',h:'12',m:'45',tm:''});
psrViewDateArray.push({idk:'32',h:'13',m:'0',tm:'01:00 PM'})
psrViewDateArray.push({idk:'33',h:'13',m:'15',tm:''});
psrViewDateArray.push({idk:'34',h:'13',m:'30',tm:''});
psrViewDateArray.push({idk:'35',h:'13',m:'45',tm:''});
psrViewDateArray.push({idk:'36',h:'14',m:'0',tm:'02:00 PM'});
psrViewDateArray.push({idk:'37',h:'14',m:'15',tm:''});
psrViewDateArray.push({idk:'38',h:'14',m:'30',tm:''});
psrViewDateArray.push({idk:'39',h:'14',m:'45',tm:''});
psrViewDateArray.push({idk:'40',h:'15',m:'0',tm:'03:00 PM'});
psrViewDateArray.push({idk:'41',h:'15',m:'15',tm:''});
psrViewDateArray.push({idk:'42',h:'15',m:'30',tm:''});
psrViewDateArray.push({idk:'43',h:'15',m:'45',tm:''});
psrViewDateArray.push({idk:'44',h:'16',m:'0',tm:'04:00 PM'});
psrViewDateArray.push({idk:'45',h:'16',m:'15',tm:''});
psrViewDateArray.push({idk:'46',h:'16',m:'30',tm:''});
psrViewDateArray.push({idk:'47',h:'16',m:'45',tm:''});
psrViewDateArray.push({idk:'48',h:'17',m:'0',tm:'05:00 PM'});
psrViewDateArray.push({idk:'49',h:'17',m:'15',tm:''})
psrViewDateArray.push({idk:'50',h:'17',m:'30',tm:''});
psrViewDateArray.push({idk:'51',h:'17',m:'45',tm:''});
psrViewDateArray.push({idk:'52',h:'18',m:'0',tm:'06:00 PM'});
psrViewDateArray.push({idk:'53',h:'18',m:'15',tm:''});
psrViewDateArray.push({idk:'54',h:'18',m:'30',tm:''});
psrViewDateArray.push({idk:'55',h:'18',m:'45',tm:''});
psrViewDateArray.push({idk:'56',h:'19',m:'0',tm:'07:00 PM'});
psrViewDateArray.push({idk:'57',h:'19',m:'15',tm:''});
psrViewDateArray.push({idk:'58',h:'19',m:'30',tm:''});
psrViewDateArray.push({idk:'59',h:'19',m:'45',tm:''});
psrViewDateArray.push({idk:'60',h:'20',m:'0',tm:'08:00 PM'});
psrViewDateArray.push({idk:'61',h:'20',m:'15',tm:''});
psrViewDateArray.push({idk:'62',h:'20',m:'30',tm:''});
psrViewDateArray.push({idk:'63',h:'20',m:'45',tm:''});
psrViewDateArray.push({idk:'64',h:'21',m:'0',tm:'09:00 PM'});
psrViewDateArray.push({idk:'65',h:'21',m:'15',tm:''});
psrViewDateArray.push({idk:'66',h:'21',m:'30',tm:''});
psrViewDateArray.push({idk:'67',h:'21',m:'45',tm:''});
psrViewDateArray.push({idk:'68',h:'22',m:'0',tm:'10:00 PM'});
psrViewDateArray.push({idk:'69',h:'22',m:'15',tm:''});
psrViewDateArray.push({idk:'70',h:'23',m:'30',tm:''});
psrViewDateArray.push({idk:'71',h:'23',m:'45',tm:''});
psrViewDateArray.push({idk:'72',h:'24',m:'0',tm:'11:00 PM'});
psrViewDateArray.push({idk:'73',h:'24',m:'15',tm:''});
psrViewDateArray.push({idk:'74',h:'24',m:'30',tm:''});
psrViewDateArray.push({idk:'75',h:'24',m:'45',tm:''});
psrViewDateArray.push({idk:'76',h:'0',m:'0',tm:'12:00 AM'});
psrViewDateArray.push({idk:'77',h:'0',m:'15',tm:''});
psrViewDateArray.push({idk:'78',h:'0',m:'30',tm:''});
psrViewDateArray.push({idk:'79',h:'0',m:'45',tm:''});
psrViewDateArray.push({idk:'80',h:'1',m:'0',tm:'01:00 AM'});
psrViewDateArray.push({idk:'81',h:'1',m:'15',tm:''});
psrViewDateArray.push({idk:'82',h:'1',m:'30',tm:''});
psrViewDateArray.push({idk:'83',h:'1',m:'45',tm:''});
psrViewDateArray.push({idk:'84',h:'2',m:'0',tm:'02:00 AM'});
psrViewDateArray.push({idk:'85',h:'2',m:'15',tm:''});
psrViewDateArray.push({idk:'86',h:'2',m:'30',tm:''});
psrViewDateArray.push({idk:'87',h:'2',m:'45',tm:''});
psrViewDateArray.push({idk:'88',h:'3',m:'0',tm:'03:00 AM'});
psrViewDateArray.push({idk:'89',h:'3',m:'15',tm:''});
psrViewDateArray.push({idk:'90',h:'3',m:'30',tm:''});
psrViewDateArray.push({idk:'91',h:'3',m:'45',tm:''});
psrViewDateArray.push({idk:'92',h:'4',m:'0',tm:'04:00 AM'});
psrViewDateArray.push({idk:'93',h:'4',m:'15',tm:''});
psrViewDateArray.push({idk:'94',h:'4',m:'30',tm:''});
psrViewDateArray.push({idk:'95',h:'4',m:'45',tm:''});


var psrViewDateArray2DView = [{idk:'1',h:'5',m:'0',tm:'05:00 AM'},{idk:'2',h:'5',m:'15',tm:'05:15 AM'},{idk:'3',h:'5',m:'30',tm:'05:30 AM'},{idk:'4',h:'5',m:'45',tm:'05:45 AM'},{idk:'5',h:'6',m:'0',tm:'06:00 AM'}];
psrViewDateArray2DView.push({idk:'6',h:'6',m:'15',tm:'06:15 AM'});
psrViewDateArray2DView.push({idk:'7',h:'6',m:'30',tm:'06:30 AM'});
psrViewDateArray2DView.push({idk:'8',h:'6',m:'45',tm:'06:45 AM'});
psrViewDateArray2DView.push({idk:'9',h:'7',m:'0',tm:'07:00 AM'});
psrViewDateArray2DView.push({idk:'10',h:'7',m:'15',tm:'07:15 AM'});
psrViewDateArray2DView.push({idk:'11',h:'7',m:'30',tm:'07:30 AM'});
psrViewDateArray2DView.push({idk:'11',h:'7',m:'45',tm:'07:45 AM'});
psrViewDateArray2DView.push({idk:'12',h:'8',m:'0',tm:'08:00 AM'})
psrViewDateArray2DView.push({idk:'13',h:'8',m:'15',tm:'08:15 AM'});
psrViewDateArray2DView.push({idk:'14',h:'8',m:'30',tm:'08:30 AM'});
psrViewDateArray2DView.push({idk:'15',h:'8',m:'45',tm:'08:45 AM'})
psrViewDateArray2DView.push({idk:'16',h:'9',m:'0',tm:'09:00 AM'});
psrViewDateArray2DView.push({idk:'17',h:'9',m:'15',tm:'09:15 AM'});
psrViewDateArray2DView.push({idk:'18',h:'9',m:'30',tm:'09:30 AM'});
psrViewDateArray2DView.push({idk:'19',h:'9',m:'45',tm:'09:45 AM'});
psrViewDateArray2DView.push({idk:'20',h:'10',m:'0',tm:'10:00 AM'});
psrViewDateArray2DView.push({idk:'21',h:'10',m:'15',tm:'10:15 AM'});
psrViewDateArray2DView.push({idk:'22',h:'10',m:'30',tm:'10:30 AM'});
psrViewDateArray2DView.push({idk:'23',h:'10',m:'45',tm:'10:45 AM'});
psrViewDateArray2DView.push({idk:'24',h:'11',m:'0',tm:'11:00 AM'});
psrViewDateArray2DView.push({idk:'25',h:'11',m:'15',tm:'11:15 AM'});
psrViewDateArray2DView.push({idk:'26',h:'11',m:'30',tm:'11:30 AM'});
psrViewDateArray2DView.push({idk:'27',h:'11',m:'45',tm:'11:45 AM'});
psrViewDateArray2DView.push({idk:'28',h:'12',m:'0',tm:'12:00 PM'});
psrViewDateArray2DView.push({idk:'29',h:'12',m:'15',tm:'12:15 PM'});
psrViewDateArray2DView.push({idk:'30',h:'12',m:'30',tm:'12:30 PM'});
psrViewDateArray2DView.push({idk:'31',h:'12',m:'45',tm:'12:45 PM'});
psrViewDateArray2DView.push({idk:'32',h:'13',m:'0',tm:'01:00 PM'})
psrViewDateArray2DView.push({idk:'33',h:'13',m:'15',tm:'01:15 PM'});
psrViewDateArray2DView.push({idk:'34',h:'13',m:'30',tm:'01:30 PM'});
psrViewDateArray2DView.push({idk:'35',h:'13',m:'45',tm:'01:45 PM'});
psrViewDateArray2DView.push({idk:'36',h:'14',m:'0',tm:'02:00 PM'});
psrViewDateArray2DView.push({idk:'37',h:'14',m:'15',tm:'02:15 PM'});
psrViewDateArray2DView.push({idk:'38',h:'14',m:'30',tm:'02:30 PM'});
psrViewDateArray2DView.push({idk:'39',h:'14',m:'45',tm:'02:45 PM'});
psrViewDateArray2DView.push({idk:'40',h:'15',m:'0',tm:'03:00 PM'});
psrViewDateArray2DView.push({idk:'41',h:'15',m:'15',tm:'03:15 PM'});
psrViewDateArray2DView.push({idk:'42',h:'15',m:'30',tm:'03:30 PM'});
psrViewDateArray2DView.push({idk:'43',h:'15',m:'45',tm:'03:45 PM'});
psrViewDateArray2DView.push({idk:'44',h:'16',m:'0',tm:'04:00 PM'});
psrViewDateArray2DView.push({idk:'45',h:'16',m:'15',tm:'04:15 PM'});
psrViewDateArray2DView.push({idk:'46',h:'16',m:'30',tm:'04:30 PM'});
psrViewDateArray2DView.push({idk:'47',h:'16',m:'45',tm:'04:45 PM'});
psrViewDateArray2DView.push({idk:'48',h:'17',m:'0',tm:'05:00 PM'});
psrViewDateArray2DView.push({idk:'49',h:'17',m:'15',tm:'05:15 PM'})
psrViewDateArray2DView.push({idk:'50',h:'17',m:'30',tm:'05:30 PM'});
psrViewDateArray2DView.push({idk:'51',h:'17',m:'45',tm:'05:45 PM'});
psrViewDateArray2DView.push({idk:'52',h:'18',m:'0',tm:'06:00 PM'});
psrViewDateArray2DView.push({idk:'53',h:'18',m:'15',tm:'06:15 PM'});
psrViewDateArray2DView.push({idk:'54',h:'18',m:'30',tm:'06:30 PM'});
psrViewDateArray2DView.push({idk:'55',h:'18',m:'45',tm:'06:45 PM'});
psrViewDateArray2DView.push({idk:'56',h:'19',m:'0',tm:'07:00 PM'});
psrViewDateArray2DView.push({idk:'57',h:'19',m:'15',tm:'07:15 PM'});
psrViewDateArray2DView.push({idk:'58',h:'19',m:'30',tm:'07:30 PM'});
psrViewDateArray2DView.push({idk:'59',h:'19',m:'45',tm:'07:45 PM'});
psrViewDateArray2DView.push({idk:'60',h:'20',m:'0',tm:'08:00 PM'});
psrViewDateArray2DView.push({idk:'61',h:'20',m:'15',tm:'08:15 PM'});
psrViewDateArray2DView.push({idk:'62',h:'20',m:'30',tm:'08:30 PM'});
psrViewDateArray2DView.push({idk:'63',h:'20',m:'45',tm:'08:45 PM'});
psrViewDateArray2DView.push({idk:'64',h:'21',m:'0',tm:'09:00 PM'});
psrViewDateArray2DView.push({idk:'65',h:'21',m:'15',tm:'09:15 PM'});
psrViewDateArray2DView.push({idk:'66',h:'21',m:'30',tm:'09:30 PM'});
psrViewDateArray2DView.push({idk:'67',h:'21',m:'45',tm:'09:45 PM'});
psrViewDateArray2DView.push({idk:'68',h:'22',m:'0',tm:'10:00 PM'});
psrViewDateArray2DView.push({idk:'69',h:'22',m:'15',tm:'10:15 PM'});
psrViewDateArray2DView.push({idk:'70',h:'23',m:'30',tm:'10:30 PM'});
psrViewDateArray2DView.push({idk:'71',h:'23',m:'45',tm:'10:45 PM'});
psrViewDateArray2DView.push({idk:'72',h:'24',m:'0',tm:'11:00 PM'});
psrViewDateArray2DView.push({idk:'73',h:'24',m:'15',tm:'11:15 PM'});
psrViewDateArray2DView.push({idk:'74',h:'24',m:'30',tm:'11:30 PM'});
psrViewDateArray2DView.push({idk:'75',h:'24',m:'45',tm:'11:45 PM'});
psrViewDateArray2DView.push({idk:'76',h:'0',m:'0',tm:'12:00 AM'});
psrViewDateArray2DView.push({idk:'77',h:'0',m:'15',tm:'12:15 AM'});
psrViewDateArray2DView.push({idk:'78',h:'0',m:'30',tm:'12:30 AM'});
psrViewDateArray2DView.push({idk:'79',h:'0',m:'45',tm:'12:45 AM'});
psrViewDateArray2DView.push({idk:'80',h:'1',m:'0',tm:'01:00'});
psrViewDateArray2DView.push({idk:'81',h:'1',m:'15',tm:'01:15 AM'});
psrViewDateArray2DView.push({idk:'82',h:'1',m:'30',tm:'01:30 AM'});
psrViewDateArray2DView.push({idk:'83',h:'1',m:'45',tm:'01:145 AM'});
psrViewDateArray2DView.push({idk:'84',h:'2',m:'0',tm:'02:00 AM'});
psrViewDateArray2DView.push({idk:'85',h:'2',m:'15',tm:'02:15 AM'});
psrViewDateArray2DView.push({idk:'86',h:'2',m:'30',tm:'02:30 AM'});
psrViewDateArray2DView.push({idk:'87',h:'2',m:'45',tm:'02:45 AM'});
psrViewDateArray2DView.push({idk:'88',h:'3',m:'0',tm:'03:00 AM'});
psrViewDateArray2DView.push({idk:'89',h:'3',m:'15',tm:'03:15 AM'});
psrViewDateArray2DView.push({idk:'90',h:'3',m:'30',tm:'03:30 AM'});
psrViewDateArray2DView.push({idk:'91',h:'3',m:'45',tm:'3:45 AM'});
psrViewDateArray2DView.push({idk:'92',h:'4',m:'0',tm:'04:00 AM'});
psrViewDateArray2DView.push({idk:'93',h:'4',m:'15',tm:'04:15 AM'});
psrViewDateArray2DView.push({idk:'94',h:'4',m:'30',tm:'04:30 AM'});
psrViewDateArray2DView.push({idk:'95',h:'4',m:'45',tm:'04:45 AM'});

var prsAppViewArray = [{idk:'1',h:'5',m:'0',tm:'5:00 AM'},{idk:'2',h:'6',m:'0',tm:'6:00 AM'}];
prsAppViewArray.push({idk:'3',h:'7',m:'0',tm:'7:00 AM'});
prsAppViewArray.push({idk:'4',h:'8',m:'0',tm:'8:00 AM'})
prsAppViewArray.push({idk:'5',h:'9',m:'0',tm:'9:00 AM'});
prsAppViewArray.push({idk:'6',h:'10',m:'0',tm:'10:00 AM'});
prsAppViewArray.push({idk:'7',h:'11',m:'0',tm:'11:00 AM'});
prsAppViewArray.push({idk:'8',h:'12',m:'0',tm:'12:00 PM'});
prsAppViewArray.push({idk:'9',h:'13',m:'0',tm:'01:00 PM'})
prsAppViewArray.push({idk:'10',h:'14',m:'0',tm:'02:00 PM'});
prsAppViewArray.push({idk:'11',h:'15',m:'0',tm:'03:00 PM'});
prsAppViewArray.push({idk:'12',h:'16',m:'0',tm:'04:00 PM'});
prsAppViewArray.push({idk:'13',h:'17',m:'0',tm:'05:00 PM'});
prsAppViewArray.push({idk:'14',h:'18',m:'0',tm:'06:00 PM'});
prsAppViewArray.push({idk:'15',h:'19',m:'0',tm:'07:00 PM'});
prsAppViewArray.push({idk:'16',h:'20',m:'0',tm:'08:00 PM'});
prsAppViewArray.push({idk:'17',h:'21',m:'0',tm:'09:00 PM'});
prsAppViewArray.push({idk:'18',h:'22',m:'0',tm:'10:00 PM'});
prsAppViewArray.push({idk:'19',h:'23',m:'0',tm:'11:00 PM'});
prsAppViewArray.push({idk:'20',h:'0',m:'0',tm:'12:00 AM'});
prsAppViewArray.push({idk:'21',h:'1',m:'0',tm:'01:00 AM'});
prsAppViewArray.push({idk:'22',h:'2',m:'0',tm:'02:00 AM'});
prsAppViewArray.push({idk:'23',h:'3',m:'0',tm:'03:00 AM'});
prsAppViewArray.push({idk:'24',h:'4',m:'0',tm:'04:00 AM'});

var appDTArray = [];
function getAppDTItem(hur,dt){
	var count = 0;
	var countObj = {};
	var appId = "";
	for(var d=0;d<appDTArray.length;d++){
		var dItem = appDTArray[d];
		if(dItem && dItem.dateOfAppointment){
			var date1 = new Date(dt);
			//console.log(date1);
			var totalSec = dItem.dateOfAppointment;
			var date2 = new Date(totalSec);
			//console.log(date2);
			var sttotal = dt+(hur*60*60*1000);
			var endtotal = sttotal+(1*60*60*1000);
			if(totalSec>=sttotal && totalSec<=endtotal){
				appId = dItem.id;
				count = count+1;
			}
		}
	}
	countObj.id = appId;
	countObj.count = count;
	return countObj;
}
function onPatientAppListData(dataObj){
	// console.log(dataObj);
	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		if(dataObj.response.appointment){
			if($.isArray(dataObj.response.appointment)){
				appDTArray = dataObj.response.appointment;
			}else{
				appDTArray.push(dataObj.response.appointment);
			}
		}
		var txtAppPTDate = $("#txtAppPTDate").data("kendoDatePicker");
		// console.log(txtAppPTDate.value());
		var selectedDate = txtAppPTDate.value();
		var allDatesInWeek = [];
		for (var i = 1; i <= 7; i++) {
	        var first = selectedDate.getDate() - (selectedDate.getDay() + 1) + i;
	        var day = new Date(selectedDate.setDate(first));
	        day.setHours(0);
	        day.setMinutes(0);
	        day.setSeconds(0);
	        day.setMilliseconds(0);
	       var  dt = kendo.toString(day,"ddd dd/MM/yyyy");
	       var obj = {};
	       obj.day = day.getTime();
	       obj.dt = dt;
	        allDatesInWeek.push(obj);
	    }

		//console.log(allDatesInWeek);

		var strTable = '<table class="table" id="tdh">';
		strTable = strTable+'<thead class="fillsHeader">';
		strTable = strTable+'<tr style="background:#999">';
		strTable = strTable+'<th style="width:250px;border-right:1px solid #FFF">Time</th>';
		for(var j=0;j<allDatesInWeek.length;j++){
			strTable = strTable+'<th style="width:250px;border-right:1px solid #FFF">'+allDatesInWeek[j].dt+'</th>';
		}
		strTable = strTable+'</tr>';
		strTable = strTable+'</thead>';
		strTable = strTable+'<tbody>';
		for(var k=0;k<prsAppViewArray.length;k++){
			var item = prsAppViewArray[k];
			strTable = strTable+'<tr>';
			strTable = strTable+'<td>'+item.tm+'</td>';
			for(var l=0;l<allDatesInWeek.length;l++){
				var aItem = allDatesInWeek[l].day;
				var  appObj = getAppDTItem(item.h,aItem);
				if(appObj.count == 0){
					strTable = strTable+'<td>'+appObj.count+'</td>';
				}else{
					var appId = appObj.id;
					strTable = strTable+'<td style="cursor:pointer;font-weight:bold" appId='+appId+' onClick=appEventClick(event)>'+appObj.count+'</td>';
				}

			}
			strTable = strTable+'</tr>';
		}

		strTable = strTable+'</tbody>';
		strTable = strTable+'</table>';
		$("#SUschedulerApp").append(strTable);
	}else{
		customAlert.error("Error", "Error");
	}
}
function getAppointmentByZeroProvider(appId){
	for(var a=0;a<appDTArray.length;a++){
		var aItem = appDTArray[a];
		if(aItem && aItem.id == appId){
			return aItem;
		}
	}
	return null;
}
function appEventClick(evt){
	// console.log(evt);
	$("#divTab8").hide();
	$("#SUschedulerApp").hide();
	$("#divPatients").show();
	var appid = $(evt.currentTarget).attr("appid");
	var appPatientArray = [];
	if(appid != ""){
		var appItem = getAppointmentByZeroProvider(appid);
		if(appItem){
			var obj = {};
			obj.idk = appItem.id;
			obj.appointmentReason = appItem.appointmentReason;
			obj.createdBy = Number(sessionStorage.userId);
			obj.isActive = 1;
			obj.isDeleted = 0;
			obj.facilityId = appItem.facilityId;
			obj.patientId = appItem.patientId;
			obj.appointmentType = appItem.appointmentType;
			obj.notes = appItem.notes;
			obj.shiftValue = appItem.shiftValue;
			obj.billingRateType = appItem.billingRateType;
			obj.patientBillingRate = appItem.patientBillingRate;
			obj.swiftBillingId = appItem.swiftBillingId;
			obj.payoutHourlyRate = appItem.payoutHourlyRate;
			obj.billToName = appItem.billToName;
			//obj.billToId = appItem.patientBillingRate;
			var fName = appItem.composition.patient.firstName+" "+appItem.composition.patient.middleName+" "+appItem.composition.patient.lastName;
			obj.patient = fName;
			appPatientArray.push(obj);
			//obj.billingRateTypeId = appItem.id;
		}
	}
	buildFileResourceListGrid(appPatientArray);

	getAjaxObject(ipAddress+"/provider/list/?is-active=1","GET",getAppProviderList,onError);
}
function getAppProviderList(dataObj){
	var dataArray = [];
	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		if($.isArray(dataObj.response.provider)){
			dataArray = dataObj.response.provider;
		}else{
			dataArray.push(dataObj.response.provider);
		}
	}else{
		customAlert.error("Error",dataObj.response.status.message);
	}
	var tempDataArry = [];
	for(var i=0;i<dataArray.length;i++){
		dataArray[i].idk = dataArray[i].id;
		dataArray[i].staff = dataArray[i].lastName+" "+dataArray[i].firstName;
		dataArray[i].Status = "InActive";
		if(dataArray[i].isActive == 1){
			dataArray[i].Status = "Active";
		}
	}
	// buildFileResourceSelListGrid(dataArray);
}
function buildFileResourceListGrid(dataSource) {
	var gridColumns = [];
	var otoptions = {};
	otoptions.noUnselect = false;
	 gridColumns.push({
	        "title": "Patient",
	        "field": "patient",
		});

   angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
	adjustHeight();
}
function buildFileResourceSelListGrid(dataSource) {
	var gridColumns = [];
	var otoptions = {};
	otoptions.noUnselect = false;
	 gridColumns.push({
	        "title": "Staff",
	        "field": "staff",
		});

   angularUISelgridWrapper.creategrid(dataSource, gridColumns,otoptions);
	adjustHeight();
	//onIndividualRowClick();
}
function onClickAvlPatient(){
	// console.log(angularUIgridWrapper);
	var selPatients = angularUIgridWrapper.getSelectedRows();
	var selProviders = angularUISelgridWrapper.getSelectedRows();
	if(selPatients.length == 0){
		customAlert.error("Error","Select service user");
	}else if(selProviders.length == 0){
		customAlert.error("Error","Select staff");
	}else{
		var selPtientItem = selPatients[0];
		var selProviderItem = selProviders[0];

		var appObj = {};
		appObj = selPtientItem;
		appObj.id = selPtientItem.idk;
		appObj.providerId = selProviderItem.idk;

		var dataUrl = ipAddress+"/appointment/update";
	var arr = [];
	arr.push(appObj);
	createAjaxObject(dataUrl,arr,"POST",onAvlAppCreate,onError);
	}
}
function onAvlAppCreate(dataObj){
	if(dataObj && dataObj.response && dataObj.response.status){
		if(dataObj.response.status.code == "1"){
			customAlert.error("Info","Appointment Updated Successfully");
		}else{
			customAlert.error("Error",dataObj.response.status.message);
		}
	}else{
		customAlert.error("Error",dataObj.response.status.message);
	}
}
function onClickAppPTSView(){
	var txtAppPTDate = $("#txtAppPTDate").data("kendoDatePicker");
	// console.log(txtAppPTDate.value());
	var selectedDate = txtAppPTDate.value();
	var selDate = new Date(selectedDate);

	var day = selDate.getDate();
	var month = selDate.getMonth();
	month = month+1;
	var year = selDate.getFullYear();

	var stDate = month+"/"+day+"/"+year;
	stDate = stDate+" 00:00:00";

	var startDate = new Date(stDate);
	var stDateTime = startDate.getTime();

	var etDate = month+"/"+day+"/"+year;
	etDate = etDate+" 23:59:59";

	var endtDate = new Date(etDate);
	var edDateTime = endtDate.getTime();

	var startDate = new Date(stDate);
			var endtDate = new Date(etDate);

			var day = startDate.getDate()-startDate.getDay();
			var pDate = new Date(startDate);
			pDate.setDate(day);

			endtDate.setDate(day+6);
			var stDateTime = pDate.getTime();
			var edDateTime = endtDate.getTime();
//var txtAppPTFacility = $("#txtAppPTFacility").data("kendoComboBox");
var intfacilityid = Number($("#txtAppPTFacility option:selected").val());
var providerId = "0";
var patientListURL = ipAddress+"/appointment/list/?facility-id="+intfacilityid+"&provider-id="+providerId+"&to-date="+edDateTime+"&from-date="+stDateTime+"&is-active=1";;

getAjaxObject(patientListURL,"GET",onPatientAppListData,onErrorMedication);

	$("#SUschedulerApp").text("");

}

function onGetPRSSFacilityProviderList(dataObj){
	//console.log(dataObj);
	prsViewArray = [];

	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		if($.isArray(dataObj.response.provider)){
			prsViewArray = dataObj.response.provider;
		}else{
			prsViewArray.push(dataObj.response.provider);
		}
	}
	if(prsViewArray.length>0){
		var pdArray = [];
		for(var p=0;p<prsViewArray.length;p++){
			pdArray.push(prsViewArray[p].id);
		}
		var txtPRDate = $("#txtPRSDate").data("kendoDatePicker");
		var dt = txtPRDate.value();

		var day = dt.getDate();
		var month = dt.getMonth();
		month = month+1;
		var year = dt.getFullYear();

		var stDate = month+"/"+day+"/"+year;
		stDate = stDate+" 00:00:00";

		var startDate = new Date(stDate);
		var stDateTime = startDate.getTime();

		var etDate = month+"/"+day+"/"+year;
		etDate = etDate+" 23:59:59";

		var endtDate = new Date(etDate);
		var edDateTime = endtDate.getTime();

		//var txtPRFacility = $("#txtPRSFacility").data("kendoComboBox");
		//var patientListURL = ipAddress+"/appointment-block/list/?facility-id="+txtPRFacility.value()+"&to-date="+edDateTime+"&from-date="+stDateTime+"&is-active=1";//+"&is-active=1&is-deleted=0"

		var intfacilityid = Number($("#txtPRSFacility option:selected").val());
		var patientListURL = ipAddress+"/homecare/availabilities/?facility-id="+intfacilityid+"&parent-type-id=201&is-active=1&is-deleted=0";
		getAjaxObject(patientListURL,"GET",onStaffPatientListBlockData,onErrorMedication);
}
}

var staffBlockData = [];
function onStaffPatientListBlockData(dataObj){
	//console.log(dataObj);
	staffBlockData = [];
	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		if(dataObj.response.availabilities){
			if($.isArray(dataObj.response.availabilities)){
				staffBlockData = dataObj.response.availabilities;
			}else{
				staffBlockData.push(dataObj.response.availabilities);
			}
		}
	}
	var txtPRDate = $("#txtPRSDate").data("kendoDatePicker");
	var dt = txtPRDate.value();

	var day = dt.getDate();
	var month = dt.getMonth();
	month = month+1;
	var year = dt.getFullYear();

	var stDate = month+"/"+day+"/"+year;
	stDate = stDate+" 00:00:00";

	var startDate = new Date(stDate);
	var stDateTime = startDate.getTime();

	var etDate = month+"/"+day+"/"+year;
	etDate = etDate+" 23:59:59";

	var endtDate = new Date(etDate);
	var edDateTime = endtDate.getTime();
	var pdArray = [];
	if(prsViewArray.length>0){
		for(var p=0;p<prsViewArray.length;p++){
			pdArray.push(prsViewArray[p].id);
		}
	}
	//var txtPRSFacility = $("#txtPRSFacility").data("kendoComboBox");
	var intfacilityid = Number($("#txtPRSFacility option:selected").val());
	var patientListURL = ipAddress+"/appointment/list/?facility-id="+intfacilityid+"&provider-id="+pdArray.toString()+"&to-date="+edDateTime+"&from-date="+stDateTime+"&is-active=1";//+"&is-active=1&is-deleted=0"

	getAjaxObject(patientListURL,"GET",onStaffPatientListData,onErrorMedication);
}

function isStaffSExist(ms){
	for(var r=0;r<staffSArray.length;r++){
		var rItem = staffSArray[r];
		if(rItem.dateOfAppointment == ms){
			return true;
		}
	}
	return false;
}
function isPt2StaffSExist(ms){
	for(var r=0;r<pt2StaffArray.length;r++){
		var rItem = pt2StaffArray[r];
		if(rItem.dateOfAppointment == ms){
			return true;
		}
	}
	return false;
}
function isPt2StaffSExist1(ms){
	for(var r=0;r<pt2StaffArray1.length;r++){
		var rItem = pt2StaffArray1[r];
		if(rItem.dateOfAppointment == ms){
			return true;
		}
	}
	return false;
}
var staffSArray = [];
	function onStaffPatientListData(dataObj){
		staffSArray = [];
		var pdtArray = [];
		if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
			if(dataObj.response.appointment){
				if($.isArray(dataObj.response.appointment)){
					pdtArray = dataObj.response.appointment;
				}else{
					pdtArray.push(dataObj.response.appointment);
				}
			}
		}
		columnSequenceSort(pdtArray);
		for(var s=0;s<pdtArray.length;s++){
			var item = pdtArray[s];
			if(item){
				if(staffSArray.length == 0){
					staffSArray.push(item);
				}else{
					if(!isStaffSExist(item.dateOfAppointment)){
						staffSArray.push(item);
					}
				}
			}
		}
		//if(staffSArray.length>0){
			//var stDT = new Date(staffSArray[0].dateOfAppointment);
			//var etDT = new Date(staffSArray[staffSArray.length-1].dateOfAppointment);

			//console.log(stDT+","+etDT);

		//	var sth = stDT.getHours();
			//var eth = etDT.getHours();

			var prDTView = [];
			for(var x=0;x<psrViewDateArray.length;x++){
				var xItem = psrViewDateArray[x];
				if(xItem){
					//if(xItem.h>=sth && xItem.h<=eth){
						prDTView.push(xItem);
					//}
				}
			}
			//console.log(prDTView);
			//prViewDateArray
			$("#prsscheduler5").text("");
			var strTable = '<table class="table">';
			strTable = strTable+'<thead class="fillsHeader">';
			strTable = strTable+'<tr>';
			strTable = strTable+'<th class="textAlign whiteColor" style="width:200px;font-size:10px">Staff Name</th>';
			strTable = strTable+'</tr>';
			strTable = strTable+'</thead>';
			strTable = strTable+'<tbody>';
			/*for(var y1=0;y1<prsViewArray.length;y1++){
				var pItem = prsViewArray[y1];
				var obj = getProviderStatusCount(pItem.id,pdtArray);
				var strTitle = pItem.firstName+","+pItem.lastName+"<br>"+obj.am+"+"+obj.pm+"="+obj.tot;
				strTable = strTable+'<tr>';
				strTable = strTable+'<td>'+strTitle+'</td>';
				strTable = strTable+'</tr>';
			}

			strTable = strTable+'</tbody>';
			strTable = strTable+'</table>';*/
			//$("#prschedulerH").append(strTable);

			var strTable = '<table class="table" id="tdh">';
			strTable = strTable+'<thead class="fillsHeader">';
			strTable = strTable+'<tr style="height:20px">';
			strTable = strTable+'<th class="textAlign whiteColor headcol fntStyle" styte="1px solid #000">Staff Name</th>';
			for(var i=0;i<prDTView.length;i++){
				var item = prDTView[i];
				//var strDT = kendo.toString(new Date(item.dateOfAppointment),"t");
				strTable = strTable+'<th class="textAlign whiteColor fntStyle" styte="1px solid #000">'+item.tm +'</th>';
			}
			strTable = strTable+'</tr>';
			strTable = strTable+'</thead>';
			strTable = strTable+'<tbody>';
			for(var y=0;y<prsViewArray.length;y++){
				var pItem = prsViewArray[y];
				var obj = getProviderStatusCount(pItem.id,pdtArray);
				var strTitle = pItem.lastName+","+pItem.firstName+" <br/> "+obj.am+"+"+obj.pm+"="+obj.tot;
				strTable = strTable+'<tr style="height:20px">';
				strTable = strTable+'<td class="headcol fntStyle">'+strTitle+'</td>';
				for(var z=0;z<prDTView.length;z++){
					var item = prDTView[z];
					var strData = getProviderStaffAppDays(item,prsViewArray[y],pdtArray);
					var strBData = getStaffAppDays(item,prsViewArray[y]);

					//console.log(strData);
					if(strData){
						strTable = strTable+'<td title="'+strData+'" style="background:green;border:1px solid green;height:15px;"></td>';
					}else if(strBData){
						strTable = strTable+'<td title="'+strBData+'" style="background:white;border:1px solid white;height:15px;"></td>';
					}else{
						strTable = strTable+'<td style="background:orange;border:1px solid white;height:15px;"></td>';
					}
				}
				strTable = strTable+'</tr>';
			}
			strTable = strTable+'</tbody>';
			strTable = strTable+'</table>';
        $("#prsscheduler5").append(strTable);

		//}


	}

var IsPreviousDay = 0;
function onClickViewProviderApp(){
	IsPreviousDay = 0;
	// var txtPRDate = $("#txtPRDate").data("kendoDatePicker");
    // var txtPRFacility = $("#txtPRFacility").data("kendoComboBox");
    // if ($("#tab10").css("display").toLowerCase() == "block") {
	//var txtPRFacility = $("#txtPRFacility2").data("kendoComboBox");
	var intfacilityid = Number($("#txtPRFacility2 option:selected").val());
    // }
    // if ($("#tab10").css("display").toLowerCase() == "block") {
	var  txtPRDate = $("#txtPRDate2").data("kendoDatePicker");
    // }
	if(txtPRDate && txtPRDate.value()){
		var selDate = txtPRDate.value();
		var date = new Date();
		var previousDay = getLastDay(date);
		var previousDayTime = previousDay.getTime();
		var selDateTime = selDate.getTime();
		if(selDateTime < previousDayTime){
			IsPreviousDay = 1;
		}

        $("#prscheduler").text("");
        $("#prscheduler4").text("");
		//https://stage.timeam.com/provider/list/?facility-id=4&is-active=1
		var facilityId = intfacilityid;//txtPRFacility.value();
		var patientListURL = ipAddress+"/provider/list/?facility-id="+facilityId+"&is-active=1&sort=lastName,firstName";
		getAjaxObject(patientListURL,"GET",onGetFacilityProviderList,onErrorMedication);
	}else{
		customAlert.error("Error", "Please select date");
	}
}

function onClickStaffWeeklyView() {

    // var txtStaffWeeklyViewFacility = $("#txtStaffWeeklyViewFacility").data("kendoComboBox");

    var txtStaffWeeklyViewDate = $("#txtStaffWeeklyViewDate").data("kendoDatePicker");

    if (txtStaffWeeklyViewDate && txtStaffWeeklyViewDate.value()) {
        $("#prsStaffWeeklyView3").html("");
        // var facilityId = txtStaffWeeklyViewFacility.value();
        // var patientListURL = ipAddress + "/provider/list/?facility-id=" + facilityId + "&is-active=1&sort=lastName,firstName";
        // getAjaxObject(patientListURL, "GET", onGetFacilityProviderForWeeklyView, onErrorMedication);
			var IsValid = "1";
		var staffselected = $('#cmbWeeklyStaff').multipleSelect('getSelects');

		if (staffselected == null || staffselected.length == 0) {
			IsValid = "0";
			customAlert.error("error","Please select at least staff");
		}
		if(IsValid == "1" && (selPatientId == null || selPatientId == "")){
			IsValid = "0";
			customAlert.error("error","Please select at least service user");
		}

		if(IsValid == "1"){
			onGetFacilityProviderForWeeklyView();
		}

    } else {
        customAlert.error("Error", "Please select date");
    }
}


function onClickSTViewProviderApp(){
	IsPreviousDay = 0;
    var txtPRDate = $("#txtSTPRDate").data("kendoDatePicker");
	onGetUnAllocated();
    if(txtPRDate && txtPRDate.value()){
		var selDate = txtPRDate.value();
		var date = new Date();
		var previousDay = getLastDay(date);
		var previousDayTime = previousDay.getTime();
		var selDateTime = selDate.getTime();
		if(selDateTime < previousDayTime){
			IsPreviousDay = 1;
		}

        $("#prschedulerST").text("");
        var facilityId = Number($("#txtStFacility option:selected").val());//txtPRFacility.value();
        var patientListURL = ipAddress+"/provider/list/?facility-id="+facilityId+"&is-active=1&sort=lastName,firstName";
        getAjaxObject(patientListURL,"GET",onGetSTFacilityProviderList,onErrorMedication);
    }else{
        customAlert.error("Error", "Please select date");
    }
}

var prViewArray = [];


// var prViewDateArray = [{ "idk": 1, "h": "0", "m": "0", "tm": "12am 12.15am" }, { "idk": 2, "h": "0", "m": "15", "tm": "12:15 12:30 AM" }, { "idk": 3, "h": "0", "m": "30", "tm": "12:30 12:45 AM" }, { "idk": 4, "h": "0", "m": "45", "tm": "12:45 1:00 AM" }, { "idk": 5, "h": "1", "m": "0", "tm": "1:00 1:15 AM" }, { "idk": 6, "h": "1", "m": "15", "tm": "1:15 1:30 AM" }, { "idk": 7, "h": "1", "m": "30", "tm": "1:30 1:45 AM" }, { "idk": 8, "h": "1", "m": "45", "tm": "1:45 2:00 AM" }, { "idk": 9, "h": "2", "m": "0", "tm": "2:00 2:15 AM" }, { "idk": 10, "h": "2", "m": "15", "tm": "2:15 2:30 AM" }, { "idk": 11, "h": "2", "m": "30", "tm": "2:30 2:45 AM" }, { "idk": 12, "h": "2", "m": "45", "tm": "2:45 3:00 AM" }, { "idk": 13, "h": "3", "m": "0", "tm": "3:00 3:15 AM" }, { "idk": 14, "h": "3", "m": "15", "tm": "3:15 3:30 AM" }, { "idk": 15, "h": "3", "m": "30", "tm": "3:30 3:45 AM" }, { "idk": 16, "h": "3", "m": "45", "tm": "3:45 4:00 AM" }, { "idk": 17, "h": "4", "m": "0", "tm": "4:00 4:15 AM" }, { "idk": 18, "h": "4", "m": "15", "tm": "4:15 4:30 AM" }, { "idk": 19, "h": "4", "m": "30", "tm": "4:30 4:45 AM" }, { "idk": 20, "h": "4", "m": "45", "tm": "4:45 5:00 AM" }, { "idk": 21, "h": "5", "m": "0", "tm": "5:00 5:15 AM" }, { "idk": 22, "h": "5", "m": "15", "tm": "5:15 5:30 AM" }, { "idk": 23, "h": "5", "m": "30", "tm": "5:30 5:45 AM" }, { "idk": 24, "h": "5", "m": "45", "tm": "5:45 6:00 AM" }, { "idk": 25, "h": "6", "m": "0", "tm": "6:00 6:15 AM" }, { "idk": 26, "h": "6", "m": "15", "tm": "6:15 6:30 AM" }, { "idk": 27, "h": "6", "m": "30", "tm": "6:30 6:45 AM" }, { "idk": 28, "h": "6", "m": "45", "tm": "6:45 7:00 AM" }, { "idk": 29, "h": "7", "m": "0", "tm": "7:00 7:15 AM" }, { "idk": 30, "h": "7", "m": "15", "tm": "7:15 7:30 AM " }, { "idk": 31, "h": "7", "m": "30", "tm": "7:30 7:45 AM" }, { "idk": 32, "h": "7", "m": "45", "tm": "7:45 8:00 AM" }, { "idk": 33, "h": "8", "m": "0", "tm": "8:00 8:15 AM" }, { "idk": 34, "h": "8", "m": "15", "tm": "8:15 8:30 AM" }, { "idk": 35, "h": "8", "m": "30", "tm": "8:30 8:45 AM" }, { "idk": 36, "h": "8", "m": "45", "tm": "8:45 9:00 AM" }, { "idk": 37, "h": "9", "m": "0", "tm": "9:00 9:15 AM" }, { "idk": 38, "h": "9", "m": "15", "tm": "9:15 9:30 AM" }, { "idk": 39, "h": "9", "m": "30", "tm": "9:30 9:45 AM" }, { "idk": 40, "h": "9", "m": "45", "tm": "9:45 10:00 AM" }, { "idk": 41, "h": "10", "m": "0", "tm": "10:00 10:15 AM" }, { "idk": 42, "h": "10", "m": "15", "tm": "10:15 10:30 AM" }, { "idk": 43, "h": "10", "m": "30", "tm": "10:30 10:45 AM" }, { "idk": 44, "h": "10", "m": "45", "tm": "10:45 11:00 AM" }, { "idk": 45, "h": "11", "m": "0", "tm": "11:00 11:15 AM" }, { "idk": 46, "h": "11", "m": "15", "tm": "11:15 11:30 AM" }, { "idk": 47, "h": "11", "m": "30", "tm": "11:30 11:45 AM" }, { "idk": 48, "h": "11", "m": "45", "tm": "11:45 12:00 AM" }, { "idk": 49, "h": "12", "m": "0", "tm": "12:00 12:15 PM" }, { "idk": 50, "h": "12", "m": "15", "tm": "12:15 12:30 PM" }, { "idk": 51, "h": "12", "m": "30", "tm": "12:30 12:45 PM" }, { "idk": 52, "h": "12", "m": "45", "tm": "12:45 1:00 PM" }, { "idk": 53, "h": "13", "m": "0", "tm": "1:00 1:15 PM" }, { "idk": 54, "h": "13", "m": "15", "tm": "1:15 1:30 PM" }, { "idk": 55, "h": "13", "m": "30", "tm": "1:30 1:45 PM" }, { "idk": 56, "h": "13", "m": "45", "tm": "1:45 2:00 PM" }, { "idk": 57, "h": "14", "m": "0", "tm": "2:00 2:15 PM" }, { "idk": 58, "h": "14", "m": "15", "tm": "2:15 2:30 PM" }, { "idk": 59, "h": "14", "m": "30", "tm": "2:30 2:45 PM" }, { "idk": 60, "h": "14", "m": "45", "tm": "2:45 3:00 PM" }, { "idk": 61, "h": "15", "m": "0", "tm": "3:00 3:15 PM" }, { "idk": 62, "h": "15", "m": "15", "tm": "3:15 3:30 PM" }, { "idk": 63, "h": "15", "m": "30", "tm": "3:30 3:45 PM" }, { "idk": 64, "h": "15", "m": "45", "tm": "3:45 4:00 PM" }, { "idk": 65, "h": "16", "m": "0", "tm": "4:00 4:15 PM" }, { "idk": 66, "h": "16", "m": "15", "tm": "4:15 4:30 PM" }, { "idk": 67, "h": "16", "m": "30", "tm": "4:30 4:45 PM" }, { "idk": 68, "h": "16", "m": "45", "tm": "4:45 5:00 PM" }, { "idk": 69, "h": "17", "m": "0", "tm": "5:00 5:15 PM" }, { "idk": 70, "h": "17", "m": "15", "tm": "5:15 5:30 PM" }, { "idk": 71, "h": "17", "m": "30", "tm": "5:30 5:45 PM" }, { "idk": 72, "h": "17", "m": "45", "tm": "5:45 6:00 PM" }, { "idk": 73, "h": "18", "m": "0", "tm": "6:00 6:15 PM" }, { "idk": 74, "h": "18", "m": "15", "tm": "6:15 6:30 PM" }, { "idk": 75, "h": "18", "m": "30", "tm": "6:30 6:45 PM" }, { "idk": 76, "h": "18", "m": "45", "tm": "6:45 7:00 PM" }, { "idk": 77, "h": "19", "m": "0", "tm": "7:00 7:15 PM" }, { "idk": 78, "h": "19", "m": "15", "tm": "7:15 7:30 PM" }, { "idk": 79, "h": "19", "m": "30", "tm": "7:30 7:45 PM" }, { "idk": 80, "h": "19", "m": "45", "tm": "7:45 8:00 PM" }, { "idk": 81, "h": "20", "m": "0", "tm": "8:00 8:15 PM" }, { "idk": 82, "h": "20", "m": "15", "tm": "8:15 8:30 PM" }, { "idk": 83, "h": "20", "m": "30", "tm": "8:30 8:45 PM" }, { "idk": 84, "h": "20", "m": "45", "tm": "8:45 9:00 PM" }, { "idk": 85, "h": "21", "m": "0", "tm": "9:00 9:15 PM" }, { "idk": 86, "h": "21", "m": "15", "tm": "9:15 9:30 PM" }, { "idk": 87, "h": "21", "m": "30", "tm": "9:30 9:45 PM" }, { "idk": 88, "h": "21", "m": "45", "tm": "9:45 10:00 PM" }, { "idk": 89, "h": "22", "m": "0", "tm": "10:00 10:15 PM" }, { "idk": 90, "h": "22", "m": "15", "tm": "10:15 10:30 PM" }, { "idk": 91, "h": "22", "m": "30", "tm": "10:30 10:45 PM" }, { "idk": 92, "h": "22", "m": "45", "tm": "10:45 11:00 PM" }, { "idk": 93, "h": "23", "m": "0", "tm": "11:00 11:15 PM" }, { "idk": 94, "h": "23", "m": "15", "tm": "11:15 11:30 PM" }, { "idk": 95, "h": "23", "m": "30", "tm": "11:30 11:45 PM" }, { "idk": 96, "h": "23", "m": "45", "tm": "11:45 12:00 PM" }];
// var prViewDateArray = [{ "idk": 1, "h": "0", "m": "0", "tm": "12 AM" }, { "idk": 2, "h": "0", "m": "15", "tm": "12.15" }, { "idk": 3, "h": "0", "m": "30", "tm": "12.30" }, { "idk": 4, "h": "0", "m": "45", "tm": "12.45" }, { "idk": 5, "h": "1", "m": "0", "tm": "1 AM" }, { "idk": 6, "h": "1", "m": "15", "tm": "1.15" }, { "idk": 7, "h": "1", "m": "30", "tm": "1:30" }, { "idk": 8, "h": "1", "m": "45", "tm": "1:45" }, { "idk": 9, "h": "2", "m": "0", "tm": "2 AM" }, { "idk": 10, "h": "2", "m": "15", "tm": "2:15" }, { "idk": 11, "h": "2", "m": "30", "tm": "2:30" }, { "idk": 12, "h": "2", "m": "45", "tm": "2:45" }, { "idk": 13, "h": "3", "m": "0", "tm": "3 AM" }, { "idk": 14, "h": "3", "m": "15", "tm": "3:15" }, { "idk": 15, "h": "3", "m": "30", "tm": "3:30" }, { "idk": 16, "h": "3", "m": "45", "tm": "3:45" }, { "idk": 17, "h": "4", "m": "0", "tm": "4 AM" }, { "idk": 18, "h": "4", "m": "15", "tm": "4:15" }, { "idk": 19, "h": "4", "m": "30", "tm": "4:30" }, { "idk": 20, "h": "4", "m": "45", "tm": "4:45" }, { "idk": 21, "h": "5", "m": "0", "tm": "5 AM" }, { "idk": 22, "h": "5", "m": "15", "tm": "5:15" }, { "idk": 23, "h": "5", "m": "30", "tm": "5:30" }, { "idk": 24, "h": "5", "m": "45", "tm": "5:45" }, { "idk": 25, "h": "6", "m": "0", "tm": "6 AM" }, { "idk": 26, "h": "6", "m": "15", "tm": "6:15" }, { "idk": 27, "h": "6", "m": "30", "tm": "6:30" }, { "idk": 28, "h": "6", "m": "45", "tm": "6:45" }, { "idk": 29, "h": "7", "m": "0", "tm": "7 AM" }, { "idk": 30, "h": "7", "m": "15", "tm": "7:15" }, { "idk": 31, "h": "7", "m": "30", "tm": "7:30" }, { "idk": 32, "h": "7", "m": "45", "tm": "7:45" }, { "idk": 33, "h": "8", "m": "0", "tm": "8 AM" }, { "idk": 34, "h": "8", "m": "15", "tm": "8:15" }, { "idk": 35, "h": "8", "m": "30", "tm": "8:30" }, { "idk": 36, "h": "8", "m": "45", "tm": "8:45" }, { "idk": 37, "h": "9", "m": "0", "tm": "9 AM" }, { "idk": 38, "h": "9", "m": "15", "tm": "9:15" }, { "idk": 39, "h": "9", "m": "30", "tm": "9:30" }, { "idk": 40, "h": "9", "m": "45", "tm": "9:45" }, { "idk": 41, "h": "10", "m": "0", "tm": "10 AM" }, { "idk": 42, "h": "10", "m": "15", "tm": "10:15" }, { "idk": 43, "h": "10", "m": "30", "tm": "10:30" }, { "idk": 44, "h": "10", "m": "45", "tm": "10:45" }, { "idk": 45, "h": "11", "m": "0", "tm": "11 AM" }, { "idk": 46, "h": "11", "m": "15", "tm": "11:15" }, { "idk": 47, "h": "11", "m": "30", "tm": "11:30" }, { "idk": 48, "h": "11", "m": "45", "tm": "11:45" }, { "idk": 49, "h": "12", "m": "0", "tm": "12 PM" }, { "idk": 50, "h": "12", "m": "15", "tm": "12:15" }, { "idk": 51, "h": "12", "m": "30", "tm": "12:30" }, { "idk": 52, "h": "12", "m": "45", "tm": "12:45" }, { "idk": 53, "h": "13", "m": "0", "tm": "1 PM" }, { "idk": 54, "h": "13", "m": "15", "tm": "1:15" }, { "idk": 55, "h": "13", "m": "30", "tm": "1:30" }, { "idk": 56, "h": "13", "m": "45", "tm": "1:45" }, { "idk": 57, "h": "14", "m": "0", "tm": "2 PM" }, { "idk": 58, "h": "14", "m": "15", "tm": "2:15" }, { "idk": 59, "h": "14", "m": "30", "tm": "2:30" }, { "idk": 60, "h": "14", "m": "45", "tm": "2:45" }, { "idk": 61, "h": "15", "m": "0", "tm": "3 PM" }, { "idk": 62, "h": "15", "m": "15", "tm": "3:15" }, { "idk": 63, "h": "15", "m": "30", "tm": "3:30" }, { "idk": 64, "h": "15", "m": "45", "tm": "3:45" }, { "idk": 65, "h": "16", "m": "0", "tm": "4 PM" }, { "idk": 66, "h": "16", "m": "15", "tm": "4:15" }, { "idk": 67, "h": "16", "m": "30", "tm": "4:30" }, { "idk": 68, "h": "16", "m": "45", "tm": "4:45" }, { "idk": 69, "h": "17", "m": "0", "tm": "5 PM" }, { "idk": 70, "h": "17", "m": "15", "tm": "5:15" }, { "idk": 71, "h": "17", "m": "30", "tm": "5:30" }, { "idk": 72, "h": "17", "m": "45", "tm": "5:45" }, { "idk": 73, "h": "18", "m": "0", "tm": "6 PM" }, { "idk": 74, "h": "18", "m": "15", "tm": "6:15" }, { "idk": 75, "h": "18", "m": "30", "tm": "6:30" }, { "idk": 76, "h": "18", "m": "45", "tm": "6:45" }, { "idk": 77, "h": "19", "m": "0", "tm": "7 PM" }, { "idk": 78, "h": "19", "m": "15", "tm": "7:15" }, { "idk": 79, "h": "19", "m": "30", "tm": "7:30" }, { "idk": 80, "h": "19", "m": "45", "tm": "7:45" }, { "idk": 81, "h": "20", "m": "0", "tm": "8 PM" }, { "idk": 82, "h": "20", "m": "15", "tm": "8:15" }, { "idk": 83, "h": "20", "m": "30", "tm": "8:30" }, { "idk": 84, "h": "20", "m": "45", "tm": "8:45" }, { "idk": 85, "h": "21", "m": "0", "tm": "9 PM" }, { "idk": 86, "h": "21", "m": "15", "tm": "9:15" }, { "idk": 87, "h": "21", "m": "30", "tm": "9:30" }, { "idk": 88, "h": "21", "m": "45", "tm": "9:45" }, { "idk": 89, "h": "22", "m": "0", "tm": "10 PM" }, { "idk": 90, "h": "22", "m": "15", "tm": "10:15" }, { "idk": 91, "h": "22", "m": "30", "tm": "10:30" }, { "idk": 92, "h": "22", "m": "45", "tm": "10:45" }, { "idk": 93, "h": "23", "m": "0", "tm": "11 PM" }, { "idk": 94, "h": "23", "m": "15", "tm": "11:15" }, { "idk": 95, "h": "23", "m": "30", "tm": "11:30" }, { "idk": 96, "h": "23", "m": "45", "tm": "11:45" }];
var prViewDateArray = [{ "idk": 1, "h": "0", "m": "0", "tm": "12 AM" }, { "idk": 2, "h": "0", "m": "15", "tm": "" }, { "idk": 3, "h": "0", "m": "30", "tm": "" }, { "idk": 4, "h": "0", "m": "45", "tm": "" }, { "idk": 5, "h": "1", "m": "0", "tm": "1 AM" }, { "idk": 6, "h": "1", "m": "15", "tm": "" }, { "idk": 7, "h": "1", "m": "30", "tm": "" }, { "idk": 8, "h": "1", "m": "45", "tm": "" }, { "idk": 9, "h": "2", "m": "0", "tm": "2 AM" }, { "idk": 10, "h": "2", "m": "15", "tm": "" }, { "idk": 11, "h": "2", "m": "30", "tm": "" }, { "idk": 12, "h": "2", "m": "45", "tm": "" }, { "idk": 13, "h": "3", "m": "0", "tm": "3 AM" }, { "idk": 14, "h": "3", "m": "15", "tm": "" }, { "idk": 15, "h": "3", "m": "30", "tm": "" }, { "idk": 16, "h": "3", "m": "45", "tm": "" }, { "idk": 17, "h": "4", "m": "0", "tm": "4 AM" }, { "idk": 18, "h": "4", "m": "15", "tm": "" }, { "idk": 19, "h": "4", "m": "30", "tm": "" }, { "idk": 20, "h": "4", "m": "45", "tm": "" }, { "idk": 21, "h": "5", "m": "0", "tm": "5 AM" }, { "idk": 22, "h": "5", "m": "15", "tm": "" }, { "idk": 23, "h": "5", "m": "30", "tm": "" }, { "idk": 24, "h": "5", "m": "45", "tm": "5:45" }, { "idk": 25, "h": "6", "m": "0", "tm": "6 AM" }, { "idk": 26, "h": "6", "m": "15", "tm": "" }, { "idk": 27, "h": "6", "m": "30", "tm": "" }, { "idk": 28, "h": "6", "m": "45", "tm": "" }, { "idk": 29, "h": "7", "m": "0", "tm": "7 AM" }, { "idk": 30, "h": "7", "m": "15", "tm": "" }, { "idk": 31, "h": "7", "m": "30", "tm": "" }, { "idk": 32, "h": "7", "m": "45", "tm": "" }, { "idk": 33, "h": "8", "m": "0", "tm": "8 AM" }, { "idk": 34, "h": "8", "m": "15", "tm": "" }, { "idk": 35, "h": "8", "m": "30", "tm": "" }, { "idk": 36, "h": "8", "m": "45", "tm": "" }, { "idk": 37, "h": "9", "m": "0", "tm": "9 AM" }, { "idk": 38, "h": "9", "m": "15", "tm": "" }, { "idk": 39, "h": "9", "m": "30", "tm": "" }, { "idk": 40, "h": "9", "m": "45", "tm": "" }, { "idk": 41, "h": "10", "m": "0", "tm": "10 AM" }, { "idk": 42, "h": "10", "m": "15", "tm": "" }, { "idk": 43, "h": "10", "m": "30", "tm": "" }, { "idk": 44, "h": "10", "m": "45", "tm": "" }, { "idk": 45, "h": "11", "m": "0", "tm": "11 AM" }, { "idk": 46, "h": "11", "m": "15", "tm": "" }, { "idk": 47, "h": "11", "m": "30", "tm": "" }, { "idk": 48, "h": "11", "m": "45", "tm": "" }, { "idk": 49, "h": "12", "m": "0", "tm": "12 PM" }, { "idk": 50, "h": "12", "m": "15", "tm": "" }, { "idk": 51, "h": "12", "m": "30", "tm": "" }, { "idk": 52, "h": "12", "m": "45", "tm": "" }, { "idk": 53, "h": "13", "m": "0", "tm": "1 PM" }, { "idk": 54, "h": "13", "m": "15", "tm": "" }, { "idk": 55, "h": "13", "m": "30", "tm": "" }, { "idk": 56, "h": "13", "m": "45", "tm": "" }, { "idk": 57, "h": "14", "m": "0", "tm": "2 PM" }, { "idk": 58, "h": "14", "m": "15", "tm": "" }, { "idk": 59, "h": "14", "m": "30", "tm": "" }, { "idk": 60, "h": "14", "m": "45", "tm": "" }, { "idk": 61, "h": "15", "m": "0", "tm": "3 PM" }, { "idk": 62, "h": "15", "m": "15", "tm": "" }, { "idk": 63, "h": "15", "m": "30", "tm": "" }, { "idk": 64, "h": "15", "m": "45", "tm": "" }, { "idk": 65, "h": "16", "m": "0", "tm": "4 PM" }, { "idk": 66, "h": "16", "m": "15", "tm": "" }, { "idk": 67, "h": "16", "m": "30", "tm": "" }, { "idk": 68, "h": "16", "m": "45", "tm": "" }, { "idk": 69, "h": "17", "m": "0", "tm": "5 PM" }, { "idk": 70, "h": "17", "m": "15", "tm": "" }, { "idk": 71, "h": "17", "m": "30", "tm": "" }, { "idk": 72, "h": "17", "m": "45", "tm": "" }, { "idk": 73, "h": "18", "m": "0", "tm": "6 PM" }, { "idk": 74, "h": "18", "m": "15", "tm": "" }, { "idk": 75, "h": "18", "m": "30", "tm": "" }, { "idk": 76, "h": "18", "m": "45", "tm": "" }, { "idk": 77, "h": "19", "m": "0", "tm": "7 PM" }, { "idk": 78, "h": "19", "m": "15", "tm": "" }, { "idk": 79, "h": "19", "m": "30", "tm": "7:30" }, { "idk": 80, "h": "19", "m": "45", "tm": "" }, { "idk": 81, "h": "20", "m": "0", "tm": "8 PM" }, { "idk": 82, "h": "20", "m": "15", "tm": "" }, { "idk": 83, "h": "20", "m": "30", "tm": "" }, { "idk": 84, "h": "20", "m": "45", "tm": "" }, { "idk": 85, "h": "21", "m": "0", "tm": "9 PM" }, { "idk": 86, "h": "21", "m": "15", "tm": "" }, { "idk": 87, "h": "21", "m": "30", "tm": "" }, { "idk": 88, "h": "21", "m": "45", "tm": "" }, { "idk": 89, "h": "22", "m": "0", "tm": "10 PM" }, { "idk": 90, "h": "22", "m": "15", "tm": "" }, { "idk": 91, "h": "22", "m": "30", "tm": "" }, { "idk": 92, "h": "22", "m": "45", "tm": "" }, { "idk": 93, "h": "23", "m": "0", "tm": "11 PM" }, { "idk": 94, "h": "23", "m": "15", "tm": "" }, { "idk": 95, "h": "23", "m": "30", "tm": "" }, { "idk": 96, "h": "23", "m": "45", "tm": "" }];





var dts = ['10:15 10:30 AM','10:30 10:45 AM','10:45 11:00 AM','11:00 11:15 AM','11:15 11:30 AM','11:30 11:45 AM','11:45 12:00 AM','12:00 12:15 PM','12:15 12:30 PM','12:30 12:45 PM','12:45 1:00 PM','1:00 1:15 PM','1:15 1:30 PM','1:30 1:45 PM','1:45 2:00 PM','2:00 2:15 PM','2:15 2:30 PM','2:30 2:45 PM','2:45 3:00 PM','3:00 3:15 PM','3:15 3:30 PM','3:30 3:45 PM','3:45 4:00 PM','4:00 4:15 PM','4:15 4:30 PM','4:30 4:45 PM','4:45 5:00 PM','5:00 5:15 PM','5:15 5:30 PM','5:30 5:45 PM','5:45 6:00 PM','6:00 6:15 PM','6:15 6:30 PM','6:30 6:45 PM','6:45 7:00 PM','7:00 7:15 PM','7:15 7:30 PM','7:30 7:45 PM','7:45 8:00 PM','8:00 8:15 PM','8:15 8:30 PM','8:30 8:45 PM','8:45 9:00 PM','9:00 9:15 PM','9:15 9:30 PM','9:30 9:45 PM','9:45 10:00 PM','10:00 10:15 PM','10:15 10:30 PM','10:30 10:45 PM','10:45 11:00 PM','11:00 11:15 PM','11:15 11:30 PM','11:30 11:45 PM','11:45 12:00 PM']
var hds = ['10','10','10','11','11','11','11','12','12','12','12','13','13','13','13','14','14','14','14','15','15','15','15','16','16','16','16','17','17','17','17','18','18','18','18','19','19','19','19','20','20','20','20','21','21','21','21','22','22','22','22','23','23','23','23'];
var mds = ['15','30','45','0','15','30','45','0','15','30','45','0','15','30','45','0','15','30','45','0','15','30','45','0','15','30','45','0','15','30','45','0','15','30','45','0','15','30','45','0','15','30','45','0','15','30','45','0','15','30','45','0','15','30','45'];

//for(var k=0;k<dts.length;k++){
//    var obj = {};
//    obj.idk = parseInt((parseInt(k) + 30));
//	obj.h = hds[k];
//	obj.m = mds[k];
//	obj.tm = dts[k];
//	prViewDateArray.push(obj);
//}

var staffViewApptCount = 0;

function onGetFacilityProviderList(dataObj){
	//console.log(dataObj);
	prViewArray = [];

	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		if($.isArray(dataObj.response.provider)){
			prViewArray = dataObj.response.provider;
		}else{
			prViewArray.push(dataObj.response.provider);
		}
	}

	if(prViewArray.length>0){

		var pdArray = [];
		for(var p=0;p<prViewArray.length;p++){
			pdArray.push(prViewArray[p].id);
		}
		var staffobj = {};
		staffobj.id=0;
		pdArray.unshift(0);
        // var txtPRDate = $("#txtPRDate").data("kendoDatePicker");
        // if ($("#tab10").css("display").toLowerCase() == "block") {
		var  txtPRDate = $("#txtPRDate2").data("kendoDatePicker");
        // }
		var dt = txtPRDate.value();

		var day = dt.getDate();
		var month = dt.getMonth();
		month = month+1;
		var year = dt.getFullYear();

		var stDate = month+"/"+day+"/"+year;
		stDate = stDate+" 00:00:00";

		var startDate = new Date(stDate);
		var stDateTime = startDate.getTime();

		var etDate = month+"/"+day+"/"+year;
		etDate = etDate+" 23:59:59";

		var endtDate = new Date(etDate);
		var edDateTime = endtDate.getTime();

        // var txtPRFacility = $("#txtPRFacility").data("kendoComboBox");
        // if ($("#tab10").css("display").toLowerCase() == "block") {
		//var txtPRFacility = $("#txtPRFacility2").data("kendoComboBox");
        // }
		var intfacilityid = Number($("#txtPRFacility2 option:selected").val());
		var patientListURL = ipAddress+"/appointment/list/?facility-id="+intfacilityid+"&provider-id="+pdArray.toString()+"&to-date="+edDateTime+"&from-date="+stDateTime+"&is-active=1";//+"&is-active=1&is-deleted=0"

		getAjaxObject(patientListURL,"GET",onProviderPatientListData,onErrorMedication);

	}

}

function onGetFacilityProviderForWeeklyView() {
        var txtStaffWeeklyViewDate = $("#txtStaffWeeklyViewDate").data("kendoDatePicker");
        var selectedDate = txtStaffWeeklyViewDate.value();
        var selDate = new Date(selectedDate);

        day = selDate.getDate();
        var month = selDate.getMonth();
        month = month + 1;
        var year = selDate.getFullYear();

        var stDate = month + "/" + day + "/" + year;
        stDate = stDate + " 00:00:00";
        var startDate = new Date(stDate);

        var etDate = month + "/" + day + "/" + year;
        etDate = etDate + " 23:59:59";
        var endtDate = new Date(etDate);

        day = startDate.getDate() - startDate.getDay();
        var pDate = new Date(startDate);
        pDate.setDate(day);

        endtDate.setDate(day + 6);
        stDateTime = pDate.getTime();
        edDateTime = endtDate.getTime();

		var providerIds = [];
		providerIds = $('#cmbWeeklyStaff').multipleSelect('getSelects');

		/*var patientIds = [];
		patientIds = $('#cmbWeeklyServiceUser').multipleSelect('getSelects');*/

		//var txtStaffWeeklyViewFacility = $("#txtStaffWeeklyViewFacility").data("kendoComboBox");

		var intfacilityid = Number($("#txtStaffWeeklyViewFacility option:selected").val());
        var appointmentListURL = ipAddress + "/appointment/list/?facility-id=" + intfacilityid + "&to-date=" + edDateTime + "&from-date=" + stDateTime + "&is-active=1&provider-id="+ providerIds.join(",")+"&patient-id="+ selPatientId;//+"&is-active=1&is-deleted=0"

        getAjaxObject(appointmentListURL, "GET", onProviderAppointmentList, onErrorMedication);


}

function onGetStaffApptReportView() {
    var txtStaffApptFromDate = $("#txtStaffApptFromDate").data("kendoDatePicker");
    var txtStaffApptToDate = $("#txtStaffApptToDate").data("kendoDatePicker");

    var fDate = txtStaffApptFromDate.value();
    var tDate = txtStaffApptToDate.value();


    var selFDate = new Date(fDate);
    var selTDate = new Date(tDate);
    var stDateTime = getFromDate(selFDate);
    var edDateTime = getToDate(selTDate);

	//var txtStaffApptReportFacility = $("#txtStaffApptReportFacility").data("kendoComboBox");
	var intfacilityid = Number($("#txtStaffApptReportFacility option:selected").val());

    var providerIds = [];

    providerIds = $('#cmbStaff').multipleSelect('getSelects');

   /* console.log("OnClick View");
    console.log(providerIds);*/

    var appointmentListURL = ipAddress + "/appointment/list/?facility-id=" + intfacilityid + "&to-date=" + edDateTime + "&from-date=" + stDateTime + "&is-active=1&provider-id=" + providerIds.join(",");

    getAjaxObject(appointmentListURL, "GET", onStaffApptReportAppointmentList, onErrorMedication);


}

var mailProviderIds = [];
function onStaffApptReportAppointmentList(dataObj) {

    var dtArray = [];

    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if ($.isArray(dataObj.response.appointment)) {
            dtArray = dataObj.response.appointment;
        } else {
            dtArray.push(dataObj.response.appointment);
        }
    }

    if (dataObj.response.appointment && dtArray != null && dtArray.length > 0) {

		dtArray.sort(function (a, b) {
			var p1 = a.composition.provider.lastName + " " + a.composition.provider.firstName;
			var p2 = b.composition.provider.lastName + " " + b.composition.provider.firstName;

			if (p1 < p2) return -1;
			if (p1 > p2) return 1;
			return 0;
		});

        var lookup = {};

        var providerIds = [];

        for (var item, i = 0; item = dtArray[i++];) {
            var providerId = item.providerId;

            if (!(providerId in lookup)) {
                lookup[providerId] = 1;
                providerIds.push(providerId);
            }
        }
		mailProviderIds = providerIds;
        // console.log('mailProviderIds'+ mailProviderIds);
        var staffAppointmentsWeekly = [];

        if (providerIds != null && providerIds.length > 0) {
            for (var i = 0; i < providerIds.length; i++) {
                var providerAppointments = _.where(dtArray, { providerId: parseInt(providerIds[i]) });
                if (providerAppointments != null && providerAppointments.length > 0) {
                    var tmpObj = {};
                    tmpObj.providerId = parseInt(providerIds[i]);

                    providerAppointments.sort(sortByDateAsc);

                    tmpObj.appointments = providerAppointments;

                    staffAppointmentsWeekly.push(tmpObj);


                }
            }

            // console.log(staffAppointmentsWeekly);
        }

        if (staffAppointmentsWeekly != null && staffAppointmentsWeekly.length > 0) {
            var strHTML = "";

            for (var j = 0; j < staffAppointmentsWeekly.length; j++) {
                strHTML = strHTML + '<div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 form-group noPadding">';
                var staffName = "";
                if (staffAppointmentsWeekly[j].appointments[0]) {
                    var tempObj = staffAppointmentsWeekly[j].appointments[0];

                    if (tempObj.composition && tempObj.composition.provider) {
                        staffName = tempObj.composition.provider.lastName + " " + tempObj.composition.provider.firstName;
                    }
                }

                strHTML = strHTML + '<h5>' + staffName + '</h5>';

                var staffAppts = staffAppointmentsWeekly[j].appointments;

                var columns = staffAppts.length > 7 ? 7 : staffAppts.length;
                var rows = Math.ceil(staffAppts.length / columns);

                strHTML = strHTML + '<table class="fixed" style="margin-left: 20px !important;"><tbody>';

                for (var l = 1; l <= columns; l++) {
                    strHTML = strHTML + '<col width="180px" />';
                }

                var counter = 0;

                for (var r = 1; r <= rows; r++) {

                    strHTML = strHTML + '<tr>';
                    for (var c = 1; c <= columns; c++) {

                        var tempAppointment = staffAppts[counter];
                        if (tempAppointment !== null && typeof (tempAppointment) !== "undefined") {


                            strHTML = strHTML + '<td style="font-size:10px;padding:0;background-color: #fff;">';
                            strHTML = strHTML + '<div class="tblSmContent"><div>';



                            //var appDate = new Date(tempAppointment.dateOfAppointment);
                            //var strH = kendo.toString(appDate, "hh");
                            //var strm = kendo.toString(appDate, "mm");
                            //var stT = kendo.toString(appDate, "tt");
                            //var time = strH;
                            //if (strm.length > 0) {
                            //    time = time + "." + strm;
                            //}
                            //time = time + stT;
                            //time = time + " - " + tempAppointment.duration + " min";



                            var time = GetDateTimeEdit(tempAppointment.dateOfAppointment) + " - " + tempAppointment.duration + " min";;

                            var appointmentReason = "";
                            var patient = "";
                            var provider = "";

                            if (tempAppointment.composition && tempAppointment.composition.appointmentReason) {
                                appointmentReason = tempAppointment.composition.appointmentReason.desc;
                            }

                            if (tempAppointment.composition && tempAppointment.composition.provider) {
                                provider = tempAppointment.composition.provider.lastName + " " + tempAppointment.composition.provider.firstName;
                            }

                            if (tempAppointment.composition && tempAppointment.composition.patient) {
                                patient = tempAppointment.composition.patient.lastName + " " + tempAppointment.composition.patient.firstName;
                            }

                            strHTML = strHTML + provider + '<br>' + time + '<br>' + appointmentReason + '<br>' + patient;
                            strHTML = strHTML + '</div></td>';

                            counter = counter + 1;
                        }
                        else {
                            break;
                        }
                    }
                    strHTML = strHTML + '</tr>';
                }

                strHTML = strHTML + '</tbody></table></div>';
            }

            if (strHTML !== "") {
                $("#prsStaffApptReport3").append(strHTML);
                $("#divSendEmail").css("display", "block");
            }
        }
    }
    else{
    	customAlert.info("Info","No appointments details.")
	}
}

function onGetSTFacilityProviderList(dataObj){
    prViewArray = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if($.isArray(dataObj.response.provider)){
            prViewArray = dataObj.response.provider;
        }else{
            prViewArray.push(dataObj.response.provider);
        }
    }
    if(prViewArray.length>0){

        var pdArray = [];
        for(var p=0;p<prViewArray.length;p++){
            pdArray.push(prViewArray[p].id);
        }
        var txtPRDate = $("#txtSTPRDate").data("kendoDatePicker");
        var dt = txtPRDate.value();

        var day = dt.getDate();
        var month = dt.getMonth();
        month = month+1;
        var year = dt.getFullYear();

        var stDate = month+"/"+day+"/"+year;
        stDate = stDate+" 00:00:00";

        var startDate = new Date(stDate);
        var stDateTime = startDate.getTime();

        var etDate = month+"/"+day+"/"+year;
        etDate = etDate+" 23:59:59";

        var endtDate = new Date(etDate);
        var edDateTime = endtDate.getTime();

		var intfacilityid = Number($("#txtStFacility option:selected").val());
        var patientListURL = ipAddress+"/appointment/list/?facility-id="+intfacilityid+"&provider-id="+pdArray.toString()+"&to-date="+edDateTime+"&from-date="+stDateTime+"&is-active=1";//+"&is-active=1&is-deleted=0"

        getAjaxObject(patientListURL,"GET",onProviderSTPatientListData,onErrorMedication);

    }
    else{
    	customAlert.info("Info","No appointment details");
	}
}

function onProviderSTPatientListData(dataObj){
    providerSArray2 = [];
    var pdtArray = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.appointment){
            if($.isArray(dataObj.response.appointment)){
                pdtArray = dataObj.response.appointment;
            }else{
                pdtArray.push(dataObj.response.appointment);
            }
        }
    }
    $("#liReStaffViewAppt").html();
    columnSequenceSort(pdtArray);
    for(var s=0;s<pdtArray.length;s++){
        var appt = "Day Total Count :" + pdtArray.length;
        $("#liReStaffViewAppt").html(appt);
        var item = pdtArray[s];
        if(item){
            if(providerSArray2.length == 0){
                providerSArray2.push(item);
            }else{
                if(!isProviderSExist(item.dateOfAppointment)){
                    providerSArray2.push(item);
                }
            }
        }
    }
    var stDT;
    var etDT;
    if(providerSArray2[0]) {
		if (providerSArray2[0] && providerSArray2[0].dateOfAppointment && providerSArray2[0].dateOfAppointment != null) {
			stDT = new Date(providerSArray2[0].dateOfAppointment);
			etDT = new Date(providerSArray2[providerSArray2.length - 1].dateOfAppointment);
		}

		//console.log(stDT+","+etDT);

		var sth = stDT.getHours();
		var eth = etDT.getHours();

		var prDTView = [];
		for (var x = 0; x < prViewDateArray.length; x++) {
			var xItem = prViewDateArray[x];
			if (xItem) {
				if (xItem.h >= sth && xItem.h <= eth) {
					prDTView.push(xItem);
				}
			}
		}
		staffAppointments = [];
		for (var counter = 0; counter < prViewArray.length; counter++) {
			var providerInfo = prViewArray[counter];

			var providerAppointments = _.where(pdtArray, {providerId: providerInfo.id});

			if (providerAppointments != null && providerAppointments.length > 0) {
				var tempObj = {};
				tempObj.providerId = providerInfo.id;
				tempObj.appointments = [];

				for (var counter2 = 0; counter2 < providerAppointments.length; counter2++) {
					var tempApp = {};
					// tempApp.appointmentId = providerAppointments[counter2].id;
					// tempApp.dateOfAppointment = providerAppointments[counter2].dateOfAppointment;
					// tempApp.duration = providerAppointments[counter2].duration;
					// tempApp.fromDate = tempApp.dateOfAppointment;
					// tempApp.toDate = tempApp.fromDate + (providerAppointments[counter2].duration * 60 * 1000);
					// tempApp.lastName = providerAppointments[counter2].composition.patient.lastName;
					// tempApp.patientId = providerAppointments[counter2].composition.patient.id;
					// tempApp.middleName = providerAppointments[counter2].composition.patient.middleName;
					// tempApp.firstName = providerAppointments[counter2].composition.patient.firstName;
					// tempApp.desc = providerAppointments[counter2].composition.appointmentReason.desc;

					providerAppointments[counter2].appointmentId = providerAppointments[counter2].id;

					providerAppointments[counter2].fromDate = providerAppointments[counter2].dateOfAppointment;
					providerAppointments[counter2].toDate = tempApp.fromDate + (providerAppointments[counter2].duration * 60 * 1000);
					providerAppointments[counter2].lastName = providerAppointments[counter2].composition.patient.lastName;
					providerAppointments[counter2].patientId = providerAppointments[counter2].composition.patient.id;
					providerAppointments[counter2].middleName = providerAppointments[counter2].composition.patient.middleName;
					providerAppointments[counter2].firstName = providerAppointments[counter2].composition.patient.firstName;
					providerAppointments[counter2].desc = providerAppointments[counter2].composition.appointmentReason.desc;
					// tempObj.push(providerAppointments[counter2]);
					tempObj.appointments.push(providerAppointments[counter2]);
				}

				staffAppointments.push(tempObj);
			}
		}

		/*console.log(staffAppointments);*/


		//console.log(prDTView);
		//prViewDateArray
		$("#prschedulerST").text("");
		// $("#prschedulerH").text("");
		var strTable = '<table class="table">';
		strTable = strTable + '<thead class="fillsHeader">';
		strTable = strTable + '<tr>';
		strTable = strTable + '<th class="textAlign whiteColor" style="width:200px;font-size:10px">Staff Name</th>';
		strTable = strTable + '</tr>';
		strTable = strTable + '</thead>';
		strTable = strTable + '<tbody>';
		for (var y1 = 0; y1 < prViewArray.length; y1++) {
			var pItem = prViewArray[y1];
			var obj = getProviderStatusCount(pItem.id, pdtArray);
			var strTitle = pItem.lastName + "," + pItem.firstName + "<br>" + obj.am + "+" + obj.pm + "=" + obj.tot;
			strTable = strTable + '<tr>';
			strTable = strTable + '<td>' + strTitle + '</td>';
			strTable = strTable + '</tr>';
		}

		strTable = strTable + '</tbody>';
		strTable = strTable + '</table>';
		//$("#prschedulerH").append(strTable);

		var strTable = '<div class="tableBlockFixing"><div class="scrolltable suvTableBlock"><div class="table-header"><table class="header" id="tdh">';
		strTable = strTable + '<thead class="fillsHeader">';
		strTable = strTable + '<tr style="height:20px">';
		var strApptTable =
			strTable = strTable + '<th class="textAlign whiteColor headcol fntStyle">Staff Name</th>';

		var val1 = _.groupBy(pdtArray, 'providerId');
		var max1 = _.max(val1, function (o) {
			return o.length
		});

		// var val2 = _.groupBy(unAllocatedAPIResponse, 'id');
		// var max2 = _.max(val2, function(o)
		// {
		//     return o.length
		// });

		var max2 = unAllocatedAPIResponse;


		if ((unAllocatedAPIResponse.length == 0) || (max1.length >= max2.length)) {
			max = max1;
		} else {
			max = max2;
		}

		for (var y = 0; y < max.length; y++) {
			if ((y + 1) == 1) {
				strTable = strTable + '<th class="textAlign whiteColor headcol fntStyle">' + (y + 1) + 'st appt' + '</th>';
			} else if (((y + 1) == 2) || ((y + 1) == 3)) {
				strTable = strTable + '<th class="textAlign whiteColor headcol fntStyle">' + (y + 1) + 'rd appt' + '</th>';
			} else {
				strTable = strTable + '<th class="textAlign whiteColor headcol fntStyle">' + (y + 1) + 'th appt' + '</th>';
			}

			if (y == max.length - 1) {
				if ((y + 1) == 1) {
					strTable = strTable + '<th class="textAlign whiteColor headcol fntStyle">' + (y + 2) + 'st appt' + '</th>';
				} else if (((y + 1) == 2) || ((y + 1) == 3)) {
					strTable = strTable + '<th class="textAlign whiteColor headcol fntStyle">' + (y + 2) + 'rd appt' + '</th>';
				} else {
					strTable = strTable + '<th class="textAlign whiteColor headcol fntStyle">' + (y + 2) + 'th appt' + '</th>';
				}
			}

		}


		strTable = strTable + '</tr>';

		strTable = strTable + '</thead></table></div>';
		strTable = strTable + '<div class="tablebody"><table><tbody>';
		strTable = strTable + '<tr style="height:20px">';
		strTable = strTable + '<td class="headcol fntStyle" style="color:red;">Unallocated</td>';
		var strDataUnallocated = onBindUnAllocatedAppt();
		if (strDataUnallocated) {
			// strSU = strSU + strData;
			// strTable = strTable + '<td style="width:80%;font-size:10px;padding:0"><table style="width:80%;border: 0 #fff;"><tbody><tr style="border: 0 #fff;">' + strData + '</tr></tbody></table></td>';
			// strTable = strTable +'<td style="width:80%;font-size:10px;padding:0">'+strData +'</td>';
			strTable = strTable + strDataUnallocated;

		}
		strTable = strTable + '</tr>';
		for (var y = 0; y < prViewArray.length; y++) {
			var pItem = prViewArray[y];
			var obj = getProviderStatusCount(pItem.id, pdtArray);
			var strTitle = pItem.lastName + "," + pItem.firstName + " " + obj.am + "+" + obj.pm + "=" + obj.tot;
			strTable = strTable + '<tr style="height:20px">';
			// strTable = strTable+'<td class="headcol fntStyle">'+strTitle+'</td>';
			strTable = strTable + '<td class="headcol fntStyle" providerid="' + pItem.id + '">' + strTitle + '</td>';


			var strData = getSUDisplay(item, prViewArray[y], pdtArray);
			if (strData) {
				// strSU = strSU + strData;
				// strTable = strTable + '<td style="width:80%;font-size:10px;padding:0"><table style="width:80%;border: 0 #fff;"><tbody><tr style="border: 0 #fff;">' + strData + '</tr></tbody></table></td>';
				// strTable = strTable +'<td style="width:80%;font-size:10px;padding:0">'+strData +'</td>';
				strTable = strTable + strData;

			} else {
				strTable = strTable + '<td style="font-size:10px;padding:0"><a href="javascript:void(0);" onclick="onClickPasteAppointment(this)"><img class="paste-appointment"></a></td>';
			}

			strTable = strTable + '</tr>';
		}
		strTable = strTable + '</tbody>';
		strTable = strTable + '</table></div></div>';
		$("#prschedulerST").append(strTable);
		$("#divReStaffViewAppt").show();

		initDragAndDrop();
	}
    else{
		customAlert.info("Info",dataObj.response.status.message);
	}

}

function initDragAndDrop() {

	$("#prschedulerST .draggable-item").draggable({
		helper: 'clone'
	});
	$("#prschedulerST td.connected-sortable").droppable({
		accept: '.draggable-item',
		drop: function (e, ui) {
			// customAlert.confirm("Confirm", "Are you sure want to update appointment?", function (response) {
			// 	if (response.button == "Yes") {
			if (IsPreviousDay == 0) {
				var providerId = parseInt($(ui.draggable).attr("providerid"));
				var appointmentId = parseInt($(ui.draggable).attr("appointmentid"));

				var droppableProvider = $(this).parent().children(':first-child');

				var dproviderid = parseInt($(droppableProvider).attr("providerid"));

				if (droppableProvider.text().toLowerCase() === "unallocated" && isNaN(dproviderid)) {
					var msg = "Are you want to un allocate the appointment?";

					displayConfirmSessionErrorPopUp("Info", msg, function (res) {
						unAllocateAppointment(appointmentId, providerId);
					});

				} else {
					var msg = "Are you want to update the appointment?";
					displayConfirmSessionErrorPopUp("Info", msg, function (res) {
						if (typeof dproviderid !== typeof undefined && dproviderid !== false) {
							dproviderid = parseInt(dproviderid);
						}
						if (providerId !== dproviderid) {

							var isExist = false;
							if (staffAppointments != null && staffAppointments.length > 0) {
								var tempStaffAppointments = _.where(staffAppointments, {providerId: providerId});

								if (tempStaffAppointments != null && tempStaffAppointments.length > 0) {
									var tempAppointment = _.where(tempStaffAppointments[0].appointments, {appointmentId: appointmentId});

									var dStaffAppointments = _.where(staffAppointments, {providerId: dproviderid});

									if (dStaffAppointments != null && dStaffAppointments.length > 0) {
										if (dStaffAppointments[0].appointments) {
											var dAppointments = dStaffAppointments[0].appointments;

											if (dAppointments != null && dAppointments.length > 0) {
												for (var m = 0; m < dAppointments.length; m++) {
													if ((tempAppointment[0].fromDate >= dAppointments[m].fromDate && tempAppointment[0].fromDate < dAppointments[m].toDate)) {
														isExist = true;
														break;
													}
												}
											}
										}
									}
									/* else{
                                         isExist = true;
                                     }*/

									if (!isExist) {

										var st = new Date(tempAppointment[0].fromDate);
										var et;
										if (tempAppointment[0].toDate) {
											et = new Date(tempAppointment[0].toDate);
										}
										var strData = '', strData1 = '';
										strData = kendo.toString(st, "h:mm tt") + " - " + kendo.toString(et, "h:mm tt") + "</br>";
										strData = strData + tempAppointment[0].lastName + " " + tempAppointment[0].middleName + " " + tempAppointment[0].firstName + "</br>";
										try {
											strData = strData + tempAppointment[0].desc;
										} catch (ex) {
										}
										strData = '<div class="draggable-item" appointmentid="' + appointmentId + '" providerid="' + dproviderid + '">' + strData + '</div>';
										strData1 = $(strData);


										$(ui.draggable).parent().addClass("connected-sortable");

										/*$(this).append(strData1[0].outerHTML);*/
										$(ui.draggable).remove();

										var response = _.where(providerSArray, {id: appointmentId});
										var reqObj = {};
										if (response && response.length > 0) {
											reqObj = response[0];
										} else {
											reqObj = tempAppointment[0];
										}


										delete reqObj.composition;
										delete reqObj.createdDate;
										delete reqObj.appointmentStartDate;
										delete reqObj.appointmentEndDate;
										delete reqObj.handoverNotes;
										delete reqObj.inTime;
										delete reqObj.isSynced;
										delete reqObj.modifiedDate;
										delete reqObj.notes;
										delete reqObj.outTime;
										delete reqObj.readBy;
										delete reqObj.syncTime;
										// delete reqObj.appointmentId;
										// delete reqObj.fromDate;
										// delete reqObj.toDate;

										reqObj.modifiedBy = Number(sessionStorage.userId);
										reqObj.dateOfAppointment = tempAppointment[0].fromDate;
										reqObj.duration = tempAppointment[0].duration;
										reqObj.patientId = tempAppointment[0].patientId;
										reqObj.providerId = dproviderid;
										// reqObj.id = appointmentId;
										// reqObj.isActive = 1;
										// reqObj.isDeleted = 0;

										var arr = [];
										arr.push(reqObj);

										var dataUrl = ipAddress + "/appointment/update";


										createAjaxObject(dataUrl, arr, "POST", onUpdateAppointment, onError);


										//initDragAndDrop();
									} else {
										customAlert.info("Error", "This slot is occupied, do you want to overlap an appointment?");
									}

								} else {
									var tempUnallocatedAppointments = _.where(unAllocatedAPIResponse, {id: appointmentId});
									if (tempUnallocatedAppointments != null && tempUnallocatedAppointments.length > 0) {
										var dStaffAppointments = _.where(staffAppointments, {providerId: dproviderid});
										if (dStaffAppointments != null && dStaffAppointments.length > 0) {
											var dAppointments;
											if (dStaffAppointments[0].appointments) {
												dAppointments = dStaffAppointments[0].appointments;
											}

											if (dAppointments != null && dAppointments.length > 0) {
												for (var m = 0; m < dAppointments.length; m++) {
													if ((tempUnallocatedAppointments[0].fromDate >= dAppointments[m].fromDate && tempUnallocatedAppointments[0].fromDate < dAppointments[m].toDate)) {
														isExist = true;
														break;
													}
												}
											}
										}
									}

									if (!isExist) {

										var st = new Date(tempUnallocatedAppointments[0].fromDate);
										var et;
										if (tempUnallocatedAppointments[0].toDate) {
											et = new Date(tempUnallocatedAppointments[0].toDate);
										}
										var strData = '', strData1 = '';
										strData = kendo.toString(st, "h:mm tt") + " - " + kendo.toString(et, "h:mm tt") + "</br>";
										strData = strData + tempUnallocatedAppointments[0].lastName + " " + tempUnallocatedAppointments[0].middleName + " " + tempUnallocatedAppointments[0].firstName + "</br>";
										try {
											strData = strData + tempUnallocatedAppointments[0].desc;
										} catch (ex) {
										}
										strData = '<div class="draggable-item" appointmentid="' + appointmentId + '" providerid="' + dproviderid + '">' + strData + '</div>';
										strData1 = $(strData);

										$(ui.draggable).parent().addClass("connected-sortable");

										$(this).append(strData1[0].outerHTML);
										$(ui.draggable).remove();

										var response = _.where(tempUnallocatedAppointments, {id: appointmentId});
										var reqObj = {};
										reqObj = response[0];

										delete reqObj.composition;
										delete reqObj.createdDate;
										delete reqObj.appointmentStartDate;
										delete reqObj.appointmentEndDate;
										delete reqObj.handoverNotes;
										delete reqObj.inTime;
										delete reqObj.isSynced;
										delete reqObj.modifiedDate;
										delete reqObj.notes;
										delete reqObj.outTime;
										delete reqObj.readBy;
										delete reqObj.syncTime;
										// delete reqObj.appointmentId;
										// delete reqObj.fromDate;
										// delete reqObj.toDate;

										reqObj.modifiedBy = Number(sessionStorage.userId);
										reqObj.dateOfAppointment = tempUnallocatedAppointments[0].fromDate;
										reqObj.duration = tempUnallocatedAppointments[0].duration;
										reqObj.patientId = tempUnallocatedAppointments[0].patientId;
										reqObj.providerId = dproviderid;
										// reqObj.id = appointmentId;
										// reqObj.isActive = 1;
										// reqObj.isDeleted = 0;

										var arr = [];
										arr.push(reqObj);

										var dataUrl = ipAddress + "/appointment/update";


										createAjaxObject(dataUrl, arr, "POST", onUpdateAppointment, onError);


										//initDragAndDrop();
									} else {
										customAlert.info("Error", "This slot is occupied, do you want to overlap an appointment?");
									}

								}
							}
						}
					});
				}
				// 	}
				// });
			}
			else{
				customAlert.error("Error","Appointment can not be moved to past days");
			}
		}

	});

}

function onClickCutAppointment(e) {

    var providerId = parseInt($(e).parent().attr("providerid"));
    var appointmentId = parseInt($(e).parent().attr("appointmentid"));

    if (copiedAppointment) {
        copiedAppointment.providerId = providerId;
        copiedAppointment.appointmentId = appointmentId;
        copiedAppointment.copiedDOM = e;

        $('.cut-appointment').parent().parent().css('background-color', 'white');
        $(e).parent().css('background-color', 'lightgray');

        $('.paste-appointment').parent().parent().css('background-color', 'navajowhite');

    }
}

function onClickSUCutAppointment(e) {

	var providerId = parseInt($(e).parent().attr("providerid"));
	var appointmentId = parseInt($(e).parent().attr("appointmentid"));

	if (weeklyCopiedAppointment) {
		weeklyCopiedAppointment.providerId = providerId;
		weeklyCopiedAppointment.appointmentId = appointmentId;
		weeklyCopiedAppointment.copiedDOM = e;

		$('.cut-appointment').parent().parent().css('background-color', 'white');
		$(e).parent().css('background-color', 'lightgray');

		$('.paste-appointment').parent().parent().css('background-color', 'navajowhite');

	}
}


function onClickSUPasteAppointment(e) {
	// if(IsPreviousDay == 0) {
		if (weeklyCopiedAppointment != null && weeklyCopiedAppointment.appointmentId != null && weeklyCopiedAppointment.appointmentId > 0) {

			var providerId = weeklyCopiedAppointment.providerId;
			var appointmentId = weeklyCopiedAppointment.appointmentId;

			var dateTime = parseInt($(e).parent().attr("dateTime"));

			var droppableProvider = $(e).parent().parent().children(':first-child');

			var dproviderid = parseInt($(droppableProvider).attr("providerid"));


				var msg = "Are you want to create the appointment?";
				displayConfirmSessionErrorPopUp("Info", msg, function (res) {
					// if (typeof dproviderid !== typeof undefined && dproviderid !== false) {
					// 	dproviderid = parseInt(dproviderid);
					// }
					// if (providerId !== dproviderid) {

						var isExist = false;
						if (weeklyAppointments != null && weeklyAppointments.length > 0) {
							var tempStaffAppointments = _.where(weeklyAppointments, {id: appointmentId});

							if (tempStaffAppointments != null && tempStaffAppointments.length > 0) {
								// var tempAppointment = _.where(tempStaffAppointments[0].appointments, {appointmentId: appointmentId});
								//
								// var dStaffAppointments = _.where(staffAppointments, {providerId: dproviderid});

								// if (dStaffAppointments != null && dStaffAppointments.length > 0) {
								// 	if (dStaffAppointments[0].appointments) {
								// 		var dAppointments = dStaffAppointments[0].appointments;
								//
								// 		// if (dAppointments != null && dAppointments.length > 0) {
								// 		// 	for (var m = 0; m < dAppointments.length; m++) {
								// 		// 		dAppointments[m].toDate = dAppointments[m].fromDate + (dAppointments[m].duration * 60 * 1000);
								// 		// 		tempAppointment[0].toDate = tempAppointment[0].fromDate + (tempAppointment[0].duration * 60 * 1000);
								// 		// 		if ((tempAppointment[0].fromDate >= dAppointments[m].fromDate && tempAppointment[0].fromDate < dAppointments[m].toDate)) {
								// 		// 			// if ((dAppointments[m].fromDate >= tempAppointment[0].fromDate && dAppointments[m].fromDate < tempAppointment[0].toDate)) {
								// 		// 			isExist = true;
								// 		// 			break;
								// 		// 		}
								// 		// 	}
								// 		// }
								// 	}
								// }

								if (!isExist) {
									if (weeklyCopiedAppointment && weeklyCopiedAppointment.copiedDOM) {
										$(weeklyCopiedAppointment.copiedDOM).next().remove();
										$(weeklyCopiedAppointment.copiedDOM).remove();
									}

									//$(ui.draggable).remove();

									// var response = _.where(tempStaffAppointments[0].appointments, {id: appointmentId});
									var reqObj = {};
									reqObj = tempStaffAppointments[0];

									delete reqObj.composition;
									delete reqObj.createdDate;
									delete reqObj.appointmentStartDate;
									delete reqObj.appointmentEndDate;
									delete reqObj.handoverNotes;
									delete reqObj.inTime;
									delete reqObj.isSynced;
									delete reqObj.modifiedDate;
									delete reqObj.notes;
									delete reqObj.outTime;
									delete reqObj.readBy;
									delete reqObj.syncTime;
									delete reqObj.id;
									delete reqObj.modifiedBy

									// delete reqObj.appointmentId;
									// delete reqObj.fromDate;
									// delete reqObj.toDate;

									reqObj.createdBy = Number(sessionStorage.userId);
									reqObj.dateOfAppointment = dateTime;
									// reqObj.duration = tempAppointment[0].duration;
									// reqObj.patientId = tempAppointment[0].patientId;
									// reqObj.providerId = dproviderid;
									// reqObj.id = appointmentId;
									// reqObj.isActive = 1;
									// reqObj.isDeleted = 0;

									var arr = [];
									arr.push(reqObj);

									// var dataUrl = ipAddress + "/appointment/update";

									var dataUrl = ipAddress + "/appointment/create";


									createAjaxObject(dataUrl, arr, "POST", onUpdateAppointment, onError);

									//console.log("calling api...");

									//initDragAndDrop();
								} else {
									customAlert.info("Error", "This slot is occupied, do you want to overlap an appointment?");
								}
							}
						}
					// }
				});

		}
	// }
	// else {
	// 	customAlert.error("Error","Appointment can not be created to past days");
	// }
}

function onClickPasteAppointment(e) {
	if(IsPreviousDay == 0) {
		if (copiedAppointment != null && copiedAppointment.appointmentId != null && copiedAppointment.appointmentId > 0) {

			var providerId = copiedAppointment.providerId;
			var appointmentId = copiedAppointment.appointmentId;

			var droppableProvider = $(e).parent().parent().children(':first-child');

			var dproviderid = parseInt($(droppableProvider).attr("providerid"));

			if (droppableProvider.text().toLowerCase() === "unallocated" && isNaN(dproviderid)) {
				var msg = "Are you want to unallocate the appointment?";

				displayConfirmSessionErrorPopUp("Info", msg, function (res) {
					unAllocateAppointment(appointmentId, providerId);
				});

			} else {
				var msg = "Are you want to update the appointment?";
				displayConfirmSessionErrorPopUp("Info", msg, function (res) {
					if (typeof dproviderid !== typeof undefined && dproviderid !== false) {
						dproviderid = parseInt(dproviderid);
					}
					if (providerId !== dproviderid) {

						var isExist = false;
						if (staffAppointments != null && staffAppointments.length > 0) {
							var tempStaffAppointments = _.where(staffAppointments, {providerId: providerId});

							if (tempStaffAppointments != null && tempStaffAppointments.length > 0) {
								var tempAppointment = _.where(tempStaffAppointments[0].appointments, {appointmentId: appointmentId});

								var dStaffAppointments = _.where(staffAppointments, {providerId: dproviderid});

								if (dStaffAppointments != null && dStaffAppointments.length > 0) {
									if (dStaffAppointments[0].appointments) {
										var dAppointments = dStaffAppointments[0].appointments;

										if (dAppointments != null && dAppointments.length > 0) {
											for (var m = 0; m < dAppointments.length; m++) {
												dAppointments[m].toDate = dAppointments[m].fromDate + (dAppointments[m].duration * 60 * 1000);
												tempAppointment[0].toDate = tempAppointment[0].fromDate + (tempAppointment[0].duration * 60 * 1000);
												if ((tempAppointment[0].fromDate >= dAppointments[m].fromDate && tempAppointment[0].fromDate < dAppointments[m].toDate)) {
													// if ((dAppointments[m].fromDate >= tempAppointment[0].fromDate && dAppointments[m].fromDate < tempAppointment[0].toDate)) {
													isExist = true;
													break;
												}
											}
										}
									}
								}

								if (!isExist) {


									if (copiedAppointment && copiedAppointment.copiedDOM) {
										$(copiedAppointment.copiedDOM).next().remove();
										$(copiedAppointment.copiedDOM).remove();
									}

									//$(ui.draggable).remove();

									var response = _.where(tempStaffAppointments[0].appointments, {id: appointmentId});
									var reqObj = {};
									reqObj = response[0];

									delete reqObj.composition;
									delete reqObj.createdDate;
									delete reqObj.appointmentStartDate;
									delete reqObj.appointmentEndDate;
									delete reqObj.handoverNotes;
									delete reqObj.inTime;
									delete reqObj.isSynced;
									delete reqObj.modifiedDate;
									delete reqObj.notes;
									delete reqObj.outTime;
									delete reqObj.readBy;
									delete reqObj.syncTime;

									// delete reqObj.appointmentId;
									// delete reqObj.fromDate;
									// delete reqObj.toDate;

									reqObj.modifiedBy = Number(sessionStorage.userId);
									reqObj.dateOfAppointment = tempAppointment[0].fromDate;
									reqObj.duration = tempAppointment[0].duration;
									reqObj.patientId = tempAppointment[0].patientId;
									reqObj.providerId = dproviderid;
									// reqObj.id = appointmentId;
									// reqObj.isActive = 1;
									// reqObj.isDeleted = 0;

									var arr = [];
									arr.push(reqObj);

									var dataUrl = ipAddress + "/appointment/update";


									createAjaxObject(dataUrl, arr, "POST", onUpdateAppointment, onError);

									//console.log("calling api...");

									//initDragAndDrop();
								} else {
									customAlert.info("Error", "This slot is occupied, do you want to overlap an appointment?");
								}

							} else {
								var tempUnallocatedAppointments = _.where(unAllocatedAPIResponse, {id: appointmentId});
								if (tempUnallocatedAppointments != null && tempUnallocatedAppointments.length > 0) {
									var dStaffAppointments = _.where(staffAppointments, {providerId: dproviderid});

									if (dStaffAppointments != null && dStaffAppointments.length > 0) {
										var dAppointments = dStaffAppointments[0].appointments;

										if (dAppointments != null && dAppointments.length > 0) {
											for (var m = 0; m < dAppointments.length; m++) {
												dAppointments[m].toDate = dAppointments[m].fromDate + (dAppointments[m].duration * 60 * 1000);
												tempUnallocatedAppointments[0].toDate = tempUnallocatedAppointments[0].fromDate + (tempUnallocatedAppointments[0].duration * 60 * 1000);
												if ((tempUnallocatedAppointments[0].fromDate >= dAppointments[m].fromDate && tempUnallocatedAppointments[0].fromDate < dAppointments[m].toDate)) {
													isExist = true;
													break;
												}
											}
										}
									}
								}

								if (!isExist) {



									if (copiedAppointment && copiedAppointment.copiedDOM) {
										$(copiedAppointment.copiedDOM).next().remove();
										$(copiedAppointment.copiedDOM).remove();
									}

									var response = _.where(tempUnallocatedAppointments, {id: appointmentId});
									var reqObj = {};
									reqObj = response[0];

									delete reqObj.composition;
									delete reqObj.createdDate;
									delete reqObj.appointmentStartDate;
									delete reqObj.appointmentEndDate;
									delete reqObj.handoverNotes;
									delete reqObj.inTime;
									delete reqObj.isSynced;
									delete reqObj.modifiedDate;
									delete reqObj.notes;
									delete reqObj.outTime;
									delete reqObj.readBy;
									delete reqObj.syncTime;
									// delete reqObj.appointmentId;
									// delete reqObj.fromDate;
									// delete reqObj.toDate;

									reqObj.modifiedBy = Number(sessionStorage.userId);
									reqObj.dateOfAppointment = tempUnallocatedAppointments[0].fromDate;
									reqObj.duration = tempUnallocatedAppointments[0].duration;
									reqObj.patientId = tempUnallocatedAppointments[0].patientId;
									reqObj.providerId = dproviderid;
									// reqObj.id = appointmentId;
									// reqObj.isActive = 1;
									// reqObj.isDeleted = 0;

									var arr = [];
									arr.push(reqObj);

									var dataUrl = ipAddress + "/appointment/update";


									createAjaxObject(dataUrl, arr, "POST", onUpdateAppointment, onError);

									//initDragAndDrop();
								} else {
									customAlert.info("Error", "This slot is occupied, do you want to overlap an appointment?");
								}

							}
						}
					}
				});
			}
		}
	}
	else {
		customAlert.error("Error","Appointment can not be created to past days");
	}
}

function unAllocateAppointment(appointmentId, providerId) {


    if (staffAppointments !== null && staffAppointments.length > 0) {
        var tempStaffAppointments = _.where(staffAppointments, { providerId: providerId });

        if (tempStaffAppointments !== null && tempStaffAppointments.length > 0) {
            var tempAppointment = _.where(tempStaffAppointments[0].appointments, { appointmentId: appointmentId });
        }

        var response = _.where(tempAppointment, { id: appointmentId });
        var reqObj = {};
        reqObj = response[0];

		delete reqObj.providerId;
        delete reqObj.composition;
        delete reqObj.createdDate;
        delete reqObj.appointmentStartDate;
        delete reqObj.appointmentEndDate;
        delete reqObj.handoverNotes;
        delete reqObj.inTime;
        delete reqObj.isSynced;
        delete reqObj.modifiedDate;
        delete reqObj.notes;
        delete reqObj.outTime;
        delete reqObj.readBy;
        delete reqObj.syncTime;
		delete reqObj.timeSlots;
		delete reqObj.readDate;
		delete reqObj.earliestStart;
		delete reqObj.latestStart;
        // delete reqObj.appointmentId;
        // delete reqObj.fromDate;
        // delete reqObj.toDate;

        reqObj.modifiedBy = Number(sessionStorage.userId);
        // reqObj.dateOfAppointment = tempAppointment[0].fromDate;
        // reqObj.duration = tempAppointment[0].duration;
        // reqObj.patientId = tempAppointment[0].patientId;
        reqObj.providerId = 0;
        // reqObj.id = appointmentId;
        // reqObj.isActive = 1;
        // reqObj.isDeleted = 0;

        var arr = [];
        arr.push(reqObj);

        var dataUrl = ipAddress + "/appointment/update";


        createAjaxObject(dataUrl, arr, "POST", onUpdateAppointment, onError);

    }
}

function initDragAndDrop2() {
    $("#prscheduler4 div.draggable-item").draggable({
        helper: 'clone'
    });
    $("#prscheduler4 td.connected-sortable").droppable({
        accept: '.draggable-item',
        drop: function (e, ui) {

        	if(IsPreviousDay == 0) {
				var providerId = parseInt($(ui.draggable).attr("providerid"));
				var appointmentId = parseInt($(ui.draggable).attr("appointmentid"));

				var droppableProvider = $(this).parent().children(':first-child');

				var dproviderid = parseInt($(droppableProvider).attr("providerid"));

				if (typeof dproviderid !== typeof undefined && dproviderid !== false) {
					dproviderid = parseInt(dproviderid);
				}
				if (providerId !== dproviderid) {

					var isExist = false;

					if (appointmentsArr && appointmentsArr.length > 0) {

						var tempAppointment = _.where(appointmentsArr, {id: appointmentId});
						var dStaffAppointments = _.where(appointmentsArr, {providerId: dproviderid});

						if (dStaffAppointments != null && dStaffAppointments.length > 0) {
							for (var m = 0; m < dStaffAppointments.length; m++) {
								var currAppSD = tempAppointment[0].dateOfAppointment;
								var currAppED = tempAppointment[0].dateOfAppointment + (tempAppointment[0].duration * 60 * 1000);

								var dAppSD = dStaffAppointments[m].dateOfAppointment;
								var dAppED = dStaffAppointments[m].dateOfAppointment + (dStaffAppointments[m].duration * 60 * 1000);

								if ((currAppSD >= dAppSD && currAppSD < dAppED)) {
									isExist = true;
									break;
								}

							}
						}
							// else {
						// 	isExist = true;
						// }

						if (!isExist) {
							var ets = tempAppointment[0].dateOfAppointment + (tempAppointment[0].duration * 60 * 1000);
							var st = new Date(tempAppointment[0].dateOfAppointment);
							var et = new Date(ets);
							var dob = new Date(tempAppointment[0].composition.patient.dateOfBirth);
							var strAge = getAge(dob);
							var strData = '', strData1 = '';
							strData = tempAppointment[0].composition.patient.lastName + " " + tempAppointment[0].composition.patient.middleName + " " + tempAppointment[0].composition.patient.firstName + "\n";
							strData = strData + kendo.toString(dob, "MM/dd/yyyy") + " (" + strAge + ")," + tempAppointment[0].composition.patient.gender + "\n";
							strData = strData + kendo.toString(st, "MM/dd/yyyy h:mm:ss tt") + " - " + kendo.toString(et, "MM/dd/yyyy h:mm:ss tt") + "\n";
							try {
								strData = strData + tempAppointment[0].composition.appointmentReason.desc + "\n";
								strData = strData + tempAppointment[0].composition.appointmentType.desc + "\n";
							} catch (ex) {
							}

							strData1 = '<div class="draggable-item" title="' + strData + '" providerid="' + providerId + '" appointmentId="' + appointmentId + '" style="background:#4F7A28;border:1px solid #4F7A28;height:15px;"></div>';

							var $parent = $(ui.draggable).parent();

							var colspan2 = parseInt($parent.prop("colspan"));
							$parent.removeAttr("colspan");
							var $droppableElement = $(this);

							for (var counter = 1; counter <= colspan2 - 1; counter++) {
								$droppableElement.next().remove();
								$("<td class=\"connected-sortable ui-droppable\" style=\"height:15px\"></td>").insertAfter($parent);
							}

							$droppableElement.prop("colspan", colspan2.toString());

							$(this).append(strData1);

							$(ui.draggable).parent().addClass("connected-sortable");

							$(ui.draggable).remove();

							var response = _.where(providerSArray, {id: appointmentId});
							var reqObj = {};
							reqObj = response[0];

							delete reqObj.composition;
							delete reqObj.createdDate;
							delete reqObj.appointmentStartDate;
							delete reqObj.appointmentEndDate;
							delete reqObj.handoverNotes;
							delete reqObj.inTime;
							delete reqObj.isSynced;
							delete reqObj.modifiedDate;
							delete reqObj.notes;
							delete reqObj.outTime;
							delete reqObj.readBy;
							delete reqObj.syncTime;
							// delete reqObj.appointmentId;
							// delete reqObj.fromDate;
							// delete reqObj.toDate;

							reqObj.modifiedBy = Number(sessionStorage.userId);
							reqObj.dateOfAppointment = tempAppointment[0].dateOfAppointment;
							reqObj.duration = tempAppointment[0].duration;
							reqObj.patientId = tempAppointment[0].patientId;
							reqObj.providerId = dproviderid;
							// reqObj.id = appointmentId;
							// reqObj.isActive = 1;
							// reqObj.isDeleted = 0;

							var arr = [];
							arr.push(reqObj);

							var dataUrl = ipAddress + "/appointment/update";


							createAjaxObject(dataUrl, arr, "POST", onUpdateAppointment, onError);


							//initDragAndDrop();
						} else {
							customAlert.info("Error", "This slot is occupied, do you want to overlap an appointment?");
						}
					}

				}
			}
        	else{
				customAlert.error("Error","Appointment can not be moved to past days");
			}



        }
    });
}

function onUpdateAppointment(dataObj) {

    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            var msg = "Appointment updated successfully";
            displaySessionErrorPopUp("Info", msg, function (res) {
                if ($("#tab10").css("display").toLowerCase() == "block") {
                    $("#btnPRView2").trigger("click");
                }
                else {
                    $("#btnSTPRView").trigger("click");
                }
                copiedAppointment = {};
            })
        } else {
            customAlert.error("Error", dataObj.response.status.message);
        }
    } else {
        customAlert.error("Error", dataObj.response.status.message);
    }
    copiedAppointment = {};
}


function onUpdateAppointment(dataObj) {

	if (dataObj && dataObj.response && dataObj.response.status) {
		if (dataObj.response.status.code == "1") {
			var msg = "Appointment created successfully";
			displaySessionErrorPopUp("Info", msg, function (res) {
				// if ($("#tab10").css("display").toLowerCase() == "block") {
				// 	$("#btnPRView2").trigger("click");
				// }
				// else {
				// 	$("#btnSTPRView").trigger("click");
				// }
				$("#btnStaffWeeklyView").trigger("click");
				weeklyAppointments = [];
				weeklyCopiedAppointment = {};
			})
		} else {
			customAlert.error("Error", dataObj.response.status.message);
		}
	} else {
		customAlert.error("Error", dataObj.response.status.message);
	}
	weeklyCopiedAppointment = {};
}

var providerSArray = [], providerSArray2 = [];
function isProviderSExist(ms){
	for(var r=0;r<providerSArray.length;r++){
		var rItem = providerSArray[r];
		if(rItem.dateOfAppointment == ms){
			return true;
		}
	}
	return false;
}
function onProviderPatientListData(dataObj) {

    if (prViewDateArray != null && prViewDateArray.length > 0) {
        for (var x = 0; x < prViewDateArray.length - 1; x++) {
            var IDK = parseInt(prViewDateArray[x].idk)
            prViewDateArray[x].idk = IDK;
        }
    }



	providerSArray = [];
    var pdtArray = [];

	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		if(dataObj.response.appointment){
			if($.isArray(dataObj.response.appointment)){
				pdtArray = dataObj.response.appointment;
			}else{
				pdtArray.push(dataObj.response.appointment);
			}
		}
	}
    $("#liStaffViewAppt").html();
    $("#liStaffViewAppt").hide();
    $("#divStaffViewAppt").hide();
	if(pdtArray.length > 0) {

		var objUnalocated = _.where(pdtArray, {providerId: 0})

		var totalAppt = pdtArray.length - objUnalocated.length;

        staffViewApptCount = totalAppt;
        var appt = "Day Total Count :" + staffViewApptCount;
        $("#liStaffViewAppt").html(appt);

		columnSequenceSort(pdtArray);
		appointmentsArr = pdtArray;
		parentRef.appointmentsArr = appointmentsArr;
		for (var s = 0; s < pdtArray.length; s++) {
			var item = pdtArray[s];
			if (item) {
				if (providerSArray.length == 0) {
					providerSArray.push(item);
				} else {
					if (!isProviderSExist(item.dateOfAppointment)) {
						providerSArray.push(item);prschedulerST
					}
				}
			}
		}
		var stDT;
		var etDT;
		if (providerSArray[0] && providerSArray[0].dateOfAppointment && providerSArray[0].dateOfAppointment != null) {
			stDT = new Date(providerSArray[0].dateOfAppointment);
			etDT = new Date(providerSArray[providerSArray.length - 1].dateOfAppointment);
		}

		//console.log(stDT+","+etDT);

		var sth = stDT.getHours();
		var eth = etDT.getHours();

		var prDTView = [];
		for (var x = 0; x < prViewDateArray.length; x++) {
			var xItem = prViewDateArray[x];
			if (xItem) {
				if (xItem.h >= sth && xItem.h <= eth) {
					prDTView.push(xItem);
				}
			}
		}

		//console.log(prDTView);
		//prViewDateArray
		$("#prscheduler").html("");
		$("#prscheduler4").html("");
		$("#prscheduler").text("");
		$("#prschedulerH").text("");
		var strTable = '<table class="table">';
		strTable = strTable + '<thead class="fillsHeader">';
		strTable = strTable + '<tr>';
		strTable = strTable + '<th class="textAlign whiteColor" style="width:200px;font-size:10px">Staff Name</th>';
		strTable = strTable + '</tr>';
		strTable = strTable + '</thead>';
		strTable = strTable + '<tbody>';
		strTable = strTable + '<tr><td>Unallocated</td></tr>';
		for (var y1 = 0; y1 < prViewArray.length; y1++) {
			var pItem = prViewArray[y1];
			var obj = getProviderStatusCount(pItem.id, pdtArray);
			var strTitle = pItem.lastName + "," + pItem.firstName + "<br>" + obj.am + "+" + obj.pm + "=" + obj.tot;
			strTable = strTable + '<tr>';
			strTable = strTable + '<td>' + strTitle + '</td>';
			strTable = strTable + '</tr>';
		}

		strTable = strTable + '</tbody>';
		strTable = strTable + '</table>';
		//$("#prschedulerH").append(strTable);

		var strTable = '<div class="tableBlockFixing tableverticalScroll"><table class="tablehorVerfix" id="tdh">';
		strTable = strTable + '<thead class="fillsHeader">';
		strTable = strTable + '<tr style="height:20px">';
		var strApptTable =
			strTable = strTable + '<th class="textAlign whiteColor headcol fntStyle" >Staff Name</th>';
		// strTable = strTable+'<th class="textAlign whiteColor headcol fntStyle"></th>';
		for (var i = 0; i < prDTView.length; i++) {
			var item = prDTView[i];
			//var strDT = kendo.toString(new Date(item.dateOfAppointment),"t");
			strTable = strTable + '<th class="textAlign whiteColor fntStyle">' + item.tm + '</th>';
			// strTable = strTable + '<th class="textAlign whiteColor fntStyle"></th>';
		}
		strTable = strTable + '</tr>';
		strTable = strTable + '</thead>';
		strTable = strTable + '<tbody>';

		var tempApp = [];

		var staffobj={};
		staffobj.id=0;
		prViewArray.unshift(0);

		for (var y = 0; y < prViewArray.length; y++) {
			var pItem = prViewArray[y];
			if(pItem != 0) {
				var obj = getProviderStatusCount(pItem.id, pdtArray);
				var strCount = " " + obj.am + "+" + obj.pm + "=" + obj.tot;
				var strTitle = pItem.lastName + "," + pItem.firstName;
				strTable = strTable + '<tr style="height:20px">';
				strTable = strTable + '<th class="headcol fntStyle" providerid="' + pItem.id + '"><span style="color:#004eff ;">' + strTitle + '</span>' + ' ' + strCount + '</th>';
			}
			else{
				var obj = getProviderStatusCount(pItem, pdtArray);
				var strCount = " " + obj.am + "+" + obj.pm + "=" + obj.tot;
				strTable = strTable + '<tr style="height:20px">';
				strTable = strTable + '<th class="headcol fntStyle" providerid="' + pItem.id + '"><span style="color:red;">Unallocated</span>' + ' ' + strCount + '</th>';

			}

			for (var z = 0; z < prDTView.length; z++) {

				var item = prDTView[z];
				var strSU;

				var strData = "";
				if ($("#tab10").css("display").toLowerCase() == "block") {
					var returnObj = getProviderAppDays2(item, prViewArray[y], pdtArray);
					if (returnObj != null && returnObj.strData) {
						strData = returnObj.strData;
						var providerId = returnObj.providerId;
						var obj = _.where(pdtArray, {id: returnObj.appointmentId})
						var suName = obj[0].composition.patient.lastName + " " + obj[0].composition.patient.middleName + " " + obj[0].composition.patient.firstName;
						var colspan = 0;
						colspan = returnObj.duration / 15;
						var foundElements = [];
						if (tempApp != null && tempApp.length > 0) {
							foundElements = $.grep(tempApp, function (e) {
								return (e.providerId === returnObj.providerId && e.appointmentId === returnObj.appointmentId);
							});
						}
						if (foundElements && foundElements.length === 0) {
							if(providerId != 0){
								strTable = strTable + '<td style="height:15px" colspan="' + colspan + '"><div class="draggable-item" title="' + strData + '" providerid="' + returnObj.providerId + '" appointmentId="' + returnObj.appointmentId + '" style="background:#4F7A28;border:1px solid #4F7A28;height:15px;"><span style="color:white;">'+suName+'</span></div></td>';
							}else{
								strTable = strTable + '<td style="height:15px" colspan="' + colspan + '"><div class="draggable-item" title="' + strData + '" providerid="' + returnObj.providerId + '" appointmentId="' + returnObj.appointmentId + '" style="background:red;border:1px solid red;height:15px;"><span style="color:white;">'+suName+'</span></div></td>';
							}

							var tmpObj = {};
							tmpObj.appointmentId = returnObj.appointmentId;
							tmpObj.providerId = returnObj.providerId;
							tempApp.push(tmpObj);
						}
					} else {
						strTable = strTable + '<td class="connected-sortable" style="height:15px"><a href="javascript:void(0)" id="popupStaff' + 1 + '" class="popupClose_staffAppt" onclick="onCreateAppt(' + 1 + ',' + 1 + ',this,' + item.idk + ')"></a></td>';


					}
				} else {
					strData = getProviderAppDays(item, prViewArray[y], pdtArray);
					if (strData) {
						strTable = strTable + '<td style="height:15px"><div class="draggable-item" title="' + strData + '" style="background:#4F7A28;border:1px solid #4F7A28;height:15px;"></div></td>';
					} else {
						strTable = strTable + '<td class="connected-sortable" style="height:15px"></td>';
					}
				}


			}

			strTable = strTable + '</tr>';
		}
		strTable = strTable + '</tbody>';
		strTable = strTable + '</table></div></div>';
		if ($("#tab10").css("display").toLowerCase() == "block") {
			$("#prscheduler4").append(strTable);
            $("#liStaffViewAppt").show();
            $("#divStaffViewAppt").show();
			initDragAndDrop2();
		} else {
			$("#prscheduler").append(strTable);
		}
	}
	else{
		customAlert.info("Info","No appointment details");
	}

	//$("#tdh").CongelarFilaColumna({Columnas:1});
}

function onProviderAppointmentList(dataObj) {
    providerSArray = [];
    var pdtArray = [];

    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.appointment) {
            if ($.isArray(dataObj.response.appointment)) {
                pdtArray = dataObj.response.appointment;
            } else {
                pdtArray.push(dataObj.response.appointment);
            }
        }
    }

    if(pdtArray && pdtArray.length > 0) {
		//pdtArray - Appointment Array
		weeklyAppointments = pdtArray;
		columnSequenceSort(pdtArray);

        $("#liStaffWeekView").html();
        $("#divStaffWeekView").show();

        var arrSum = 0;

        var tottalAppt = 0;

        for (let a = 0; a <= pdtArray.length; a++) {
            if(pdtArray[a] && pdtArray[a].duration) {
                arrSum = arrSum + pdtArray[a].duration;
            }

            if(pdtArray[a] && pdtArray[a].providerId != null && pdtArray[a].providerId != 0) {
                tottalAppt++;
            }

        }
        var houtTotal = arrSum;
        var hrs = Math.floor(houtTotal / 60);
        var min = houtTotal % 60;

        var apptTotal = hrs+":"+min;


        var appt = "Totals Count :"+ tottalAppt + "  Hours :"+ apptTotal;
        $("#liStaffWeekView").html(appt);

		var txtStaffWeeklyViewDate = $("#txtStaffWeeklyViewDate").data("kendoDatePicker");
		var selectedDate = txtStaffWeeklyViewDate.value();

		var arrDatesInWeek = [];

		for (let i = 1; i <= 7; i++) {
			var first = selectedDate.getDate() - (selectedDate.getDay() + 1) + i;
			var day = new Date(selectedDate.setDate(first));

			arrDatesInWeek.push(day);
		}

		var allDaysInWeek = [];
		for (var counter4 = 0; counter4 < arrDatesInWeek.length; counter4++) {
			// var obj = {};
			allDaysInWeek.push(daysOfWeek[arrDatesInWeek[counter4].getDay()] + " " + formatDate(arrDatesInWeek[counter4]));
			// obj.date = daysOfWeek[arrDatesInWeek[counter4].getDay()] + " " + formatDate(arrDatesInWeek[counter4]);
			// var dateTime= arrDatesInWeek[counter4];
			// dateTime.setHours(0, 0, 0, 0);
			// obj.dateTime = dateTime.getTime();
			// allDaysInWeek.push(obj);
		}


	var uniqueAppointmentReasons = [];
	var lookup = {};
	for (var item, i = 0; item = pdtArray[i++];) {
		var tmpAppointmentReason = "";
		if (item.composition && item.composition.appointmentReason) {
			tmpAppointmentReason = item.composition.appointmentReason.desc.trim();
		}

		if (!(tmpAppointmentReason in lookup)) {
			lookup[tmpAppointmentReason] = 1;
			var obj={};
			obj.appointmentReason = tmpAppointmentReason;
			obj.color = getRandomColor();
			uniqueAppointmentReasons.push(obj);
		}
	}



		var staffAppointments = [];

		for (var counter = 0; counter < arrDatesInWeek.length; counter++) {

			var obj = {};

			var tmpDate = arrDatesInWeek[counter];
			tmpDate.setHours(0, 0, 0, 0);


			var tmpAppointments = $.grep(pdtArray, function (e) {
				var apptDate = new Date(e.dateOfAppointment);
				apptDate.setHours(0, 0, 0, 0);
				return tmpDate.getTime() === apptDate.getTime();
			});

			obj.dateOfAppointment = allDaysInWeek[counter];
			obj.dateTime = tmpDate.getTime();
			obj.appointments = [];

			if (tmpAppointments != null && tmpAppointments.length > 0) {
				obj.appointments = tmpAppointments;

			}

			staffAppointments.push(obj);
		}


		// console.log(staffAppointments);

		var strTable = '<div class="tableBlockFixing"><div class="scrolltable staffTableWeekly"><div class="table-header"><table class="header" id="tblStaffWeeklyView">';
		strTable = strTable + '<thead class="fillsHeader">';
		strTable = strTable + '<tr style="height:20px">';

		var arrAppointmentsCount = [];

		for (var i = 0; i < staffAppointments.length; i++) {

			strTable = strTable + '<th class="textAlign whiteColor fntStyle">' + staffAppointments[i].dateOfAppointment + '</th>';
			arrAppointmentsCount.push(staffAppointments[i].appointments.length);
		}
		strTable = strTable + '</tr>';
		strTable = strTable + '</thead></table></div>';
		strTable = strTable + '<div class="tablebody"><table><tbody >';

		var columns = 7;
		var rows = parseInt(Math.max(...arrAppointmentsCount));

		for (var rownumber = 0; rownumber < rows; rownumber++) {

			strTable = strTable + '<tr>';

			for (var columnnumber = 0; columnnumber < columns; columnnumber++) {
				var tempAppointment = staffAppointments[columnnumber].appointments[rownumber];

				if (tempAppointment !== null && typeof (tempAppointment) !== "undefined") {
					var appDate = new Date(tempAppointment.dateOfAppointment);

					var strH = kendo.toString(appDate, "hh");
					var strm = kendo.toString(appDate, "mm");
					var stT = kendo.toString(appDate, "tt");

					var time = strH;
					if (strm.length > 0) {
						time = time + "." + strm;
					}
					time = time + stT;
					time = time + " - " + tempAppointment.duration + " min";

					time = time.toLowerCase();

					var appointmentReason = "";
					var patient = "";
					var provider = "";

					if (tempAppointment.composition && tempAppointment.composition.appointmentReason) {
						appointmentReason = tempAppointment.composition.appointmentReason.desc;
					}

					if (tempAppointment.composition && tempAppointment.composition.patient) {
						patient = tempAppointment.composition.patient.lastName + " " + tempAppointment.composition.patient.firstName + " " + tempAppointment.composition.patient.middleName;
					}

					if (tempAppointment.providerId != 0 && tempAppointment.composition && tempAppointment.composition.provider) {
						provider = tempAppointment.composition.provider.lastName + " " + tempAppointment.composition.provider.firstName + " " + tempAppointment.composition.provider.middleName;
					}
					else{
						provider = "Un Allocated"
					}

					var bgcolor = "";

					if (uniqueAppointmentReasons !== null && uniqueAppointmentReasons.length > 0) {
						if (appointmentReason !== null && appointmentReason !== "") {
							var appointmentReasonColor = $.map(uniqueAppointmentReasons,function(e){
								if (e.appointmentReason.toLowerCase() === appointmentReason.trim().toLowerCase())
									return e.color;
								else
									return null;
							});
							bgcolor = appointmentReasonColor !== null && appointmentReasonColor.length > 0?appointmentReasonColor[0]:"";
						}
					}

					color = "background-color:"+bgcolor+";";
					var fontcolor = "black";
					if(provider.toLowerCase() === "un allocated"){
						fontcolor="red";
					}

					strTable = strTable + '<td style="font-size:10px;padding:0;'+ color + '" ondblclick="onClickAppointment(' + tempAppointment.id + ')"><div class="tblSmContent" appointmentid="' + tempAppointment.id + '" providerid="' + tempAppointment.providerId + '"><a href="javascript:void(0);" class="cut-appointment" onclick="onClickSUCutAppointment(this)"></a><a href="javascript:void(0)" id="popupClose'+ columnnumber +'" class="popupClose_staff" onclick="onClickDeleteAppt('+tempAppointment.id+',' + columnnumber + ')"></a><div style="color:' + fontcolor +'">' + time + '<br>' + provider + '<br>' + appointmentReason + '<br>' + patient + '</div></div></td>';
				} else {
					strTable = strTable + '<td class="connected-sortable" style="font-size:10px;padding:0;"><div dateTime= "'+staffAppointments[columnnumber].dateTime+'"><a href="javascript:void(0);" onclick="onClickSUPasteAppointment(this)"><img class="paste-appointment"></a></div></td>';
				}

			}
			strTable = strTable + '</tr>';
		}

		var footer = ' <div class="col-xs-12 tableHints" style="display:none">' + '<div class="lblHint01"> <span class="sp-morningVisit"></span>Morning Visit</div>' +
			'<div class="lblHint02"> <span class="sp-lunchCare"></span>Lunch Care</div>' +
			'<div class="lblHint02"> <span class="sp-afternoon"></span>Afternoon Visit</div>' +
			'<div class="lblHint02"> <span class="sp-followUp"></span>Followup</div>' +
			'<div class="lblHint02"> <span class="sp-evening"></span>Evening Visit</div>' +
			'<div class="lblHint02"> <span class="sp-nightBrief"></span>Night Brief</div>' +
			'<div class="lblHint02"> <span class="sp-nightVisit"></span>Night Visit</div>' +
			'</div>';

		strTable = strTable + '</tbody>';
		strTable = strTable + '</table></div></div></div>' + footer;


		$("#prsStaffWeeklyView3").html(strTable);



	}
    else{
    	customAlert.info("Info",dataObj.response.status.message);
	}
}
function getProviderStatusCount(pdId,pdArray){
	var pm = 0;
	var am = 0;
	var tot = 0;
	for(var p=0;p<pdArray.length;p++){
		var pItem = pdArray[p];
		if(pItem && pItem.providerId == pdId){
			var ms = pItem.dateOfAppointment;
			var dt = new Date(ms);
			if(dt.getHours()>=12){
				pm = pm+1;
			}else{
				am = am+1;
			}
		}
	}
	tot = am+pm;
	var obj = {};
	obj.am = am;
	obj.pm = pm;
	obj.tot = tot;
	return obj;
}
function getStaff2AppDays1(tItem,pItem){
	var txtPRDate = $("#txt2dDate1").data("kendoDatePicker");
	txtPRDate = new Date(txtPRDate.value());
	for(var a=0;a<pt2dAvlTimeArray.length;a++){
		var item = pt2dAvlTimeArray[a];
		if(item){

			//var ets = item.endDateTime;//dateOfAppointment+(item.duration*60*1000);

			if(pItem.id == item.parentId && txtPRDate.getDay() == item.dayOfWeek){//== tItem.h && st.getMinutes() ==
				/*txtPRDate.setHours(tItem.h);
				var currMS = new Date(txtPRDate);
				currMS.setMinutes(tItem.m);
				currMS = currMS.getTime();
				currMS = currMS+1000;*/
				var hours = Number(tItem.h);
				hours = hours*3600;

				var minutes = Number(tItem.m);
				minutes = minutes*60;

				var currMS = (hours+minutes);
				var stTime = item.startTime;
				stTime = Number(stTime);

				var etTime = item.endTime;
				etTime = Number(etTime);

				if(currMS>=stTime && currMS<=etTime){ //&& tItem.h<=et.getHours()
					var sDate = new Date();//item.startDateTime);
					var eDate = new Date();
					//var dob = new Date(item.composition.patient.dateOfBirth);
					//var strAge = getAge(dob);
					var strData = "";
					sDate.setHours(0,0,0,0);
					sDate.setSeconds(stTime);

					eDate.setHours(0,0,0,0);
					eDate.setSeconds(etTime);
					var st = new Date(sDate);
					var et = new Date(eDate);
					//strData = item.composition.patient.firstName+" "+item.composition.patient.middleName+" "+item.composition.patient.lastName+"\n";
					//strData = strData+kendo.toString(dob,"MM/dd/yyyy")+" ("+strAge+"),"+item.composition.patient.gender+"\n";
					strData = strData+kendo.toString(st,"MM/dd/yyyy h:mm:ss tt")+" - "+kendo.toString(et,"MM/dd/yyyy h:mm:ss tt")+"\n";
					//strData = strData+ item.composition.appointmentReason.desc+"\n";
					//strData = strData+item.composition.appointmentType.desc+"\n";
					return strData;
				}

			}
		}
	}
	return "";
}
function getStaff2AppDays(tItem,pItem){
	var txtPRDate = $("#txt2dDate").data("kendoDatePicker");
	txtPRDate = new Date(txtPRDate.value());
	for(var a=0;a<pt2dAvlTimeArray.length;a++){
		var item = pt2dAvlTimeArray[a];
		if(item){

			//var ets = item.endDateTime;//dateOfAppointment+(item.duration*60*1000);

			if(pItem.id == item.parentId && txtPRDate.getDay() == item.dayOfWeek){//== tItem.h && st.getMinutes() ==
				/*txtPRDate.setHours(tItem.h);
				var currMS = new Date(txtPRDate);
				currMS.setMinutes(tItem.m);
				currMS = currMS.getTime();
				currMS = currMS+1000;*/
				var hours = Number(tItem.h);
				hours = hours*3600;

				var minutes = Number(tItem.m);
				minutes = minutes*60;

				var currMS = (hours+minutes);
				var stTime = item.startTime;
				stTime = Number(stTime);

				var etTime = item.endTime;
				etTime = Number(etTime);

				if(currMS>=stTime && currMS<=etTime){ //&& tItem.h<=et.getHours()
					var sDate = new Date();//item.startDateTime);
					var eDate = new Date();
					//var dob = new Date(item.composition.patient.dateOfBirth);
					//var strAge = getAge(dob);
					var strData = "";
					sDate.setHours(0,0,0,0);
					sDate.setSeconds(stTime);

					eDate.setHours(0,0,0,0);
					eDate.setSeconds(etTime);
					var st = new Date(sDate);
					var et = new Date(eDate);
					//strData = item.composition.patient.firstName+" "+item.composition.patient.middleName+" "+item.composition.patient.lastName+"\n";
					//strData = strData+kendo.toString(dob,"MM/dd/yyyy")+" ("+strAge+"),"+item.composition.patient.gender+"\n";
					strData = strData+kendo.toString(st,"MM/dd/yyyy h:mm:ss tt")+" - "+kendo.toString(et,"MM/dd/yyyy h:mm:ss tt")+"\n";
					//strData = strData+ item.composition.appointmentReason.desc+"\n";
					//strData = strData+item.composition.appointmentType.desc+"\n";
					return strData;
				}

			}
		}
	}
	return "";
}


function getStaffAppDays(tItem,pItem){
	var txtPRDate = $("#txtPRSDate").data("kendoDatePicker");
	txtPRDate = new Date(txtPRDate.value());
	for(var a=0;a<staffBlockData.length;a++){
		var item = staffBlockData[a];
		if(item){

			//var ets = item.endDateTime;//dateOfAppointment+(item.duration*60*1000);

			if(pItem.id == item.parentId && txtPRDate.getDay() == item.dayOfWeek){//== tItem.h && st.getMinutes() ==
				/*txtPRDate.setHours(tItem.h);
				var currMS = new Date(txtPRDate);
				currMS.setMinutes(tItem.m);
				currMS = currMS.getTime();
				currMS = currMS+1000;*/
				var hours = Number(tItem.h);
				hours = hours*3600;

				var minutes = Number(tItem.m);
				minutes = minutes*60;

				var currMS = (hours+minutes);
				var stTime = item.startTime;
				stTime = Number(stTime);

				var etTime = item.endTime;
				etTime = Number(etTime);

				if(currMS>=stTime && currMS<=etTime){ //&& tItem.h<=et.getHours()
					var sDate = new Date();//item.startDateTime);
					var eDate = new Date();
					//var dob = new Date(item.composition.patient.dateOfBirth);
					//var strAge = getAge(dob);
					var strData = "";
					sDate.setHours(0,0,0,0);
					sDate.setSeconds(stTime);

					eDate.setHours(0,0,0,0);
					eDate.setSeconds(etTime);
					var st = new Date(sDate);
					var et = new Date(eDate);
					//strData = item.composition.patient.firstName+" "+item.composition.patient.middleName+" "+item.composition.patient.lastName+"\n";
					//strData = strData+kendo.toString(dob,"MM/dd/yyyy")+" ("+strAge+"),"+item.composition.patient.gender+"\n";
					strData = strData+kendo.toString(st,"MM/dd/yyyy h:mm:ss tt")+" - "+kendo.toString(et,"MM/dd/yyyy h:mm:ss tt")+"\n";
					//strData = strData+ item.composition.appointmentReason.desc+"\n";
					//strData = strData+item.composition.appointmentType.desc+"\n";
					return strData;
				}

			}
		}
	}
	return "";
}
function getProviderAppDays(tItem,pItem,appArray){
    var txtPRDate = $("#txtPRDate").data("kendoDatePicker");
    if ($("#tab10").css("display").toLowerCase() == "block") {
        txtPRDate = $("#txtPRDate2").data("kendoDatePicker");
    }
	txtPRDate = new Date(txtPRDate.value());
	for(var a=0;a<appArray.length;a++){
		var item = appArray[a];
		if(item){

			var ets = item.dateOfAppointment+(item.duration*60*1000);

			if(pItem.id == item.providerId){//== tItem.h && st.getMinutes() ==
				txtPRDate.setHours(tItem.h);
				var currMS = new Date(txtPRDate);
				currMS.setMinutes(tItem.m);
				currMS = currMS.getTime();
				currMS = currMS+1000;
				if(currMS>=item.dateOfAppointment && currMS<=ets){ //&& tItem.h<=et.getHours()
					var st = new Date(item.dateOfAppointment);
					var et = new Date(ets);
					var dob = new Date(item.composition.patient.dateOfBirth);
					var strAge = getAge(dob);
					var strData = "";
					strData = item.composition.patient.lastName+" "+item.composition.patient.middleName+" "+item.composition.patient.firstName+"\n";
					strData = strData+kendo.toString(dob,"MM/dd/yyyy")+" ("+strAge+"),"+item.composition.patient.gender+"\n";
					strData = strData+kendo.toString(st,"MM/dd/yyyy h:mm:ss tt")+" - "+kendo.toString(et,"MM/dd/yyyy h:mm:ss tt")+"\n";
					try{
						strData = strData+ item.composition.appointmentReason.desc+"\n";
						strData = strData+item.composition.appointmentType.desc+"\n";
					}catch(ex){}
					return strData;
				}

			}
		}
	}
	return "";
}


function getProviderAppDays2(tItem, pItem, appArray) {
    var returnObj = {};
    var txtPRDate = $("#txtPRDate").data("kendoDatePicker");
    if ($("#tab10").css("display").toLowerCase() == "block") {
        txtPRDate = $("#txtPRDate2").data("kendoDatePicker");
    }
    txtPRDate = new Date(txtPRDate.value());
    for (var a = 0; a < appArray.length; a++) {
        var item = appArray[a];
        if (item) {

            var ets = item.dateOfAppointment + (item.duration * 60 * 1000);

            if (pItem == 0 && pItem == item.providerId) {//== tItem.h && st.getMinutes() ==
                txtPRDate.setHours(tItem.h);
                var currMS = new Date(txtPRDate);
                currMS.setMinutes(tItem.m);
                currMS = currMS.getTime();
                currMS = currMS + 1000;
                if (currMS >= item.dateOfAppointment && currMS <= ets) { //&& tItem.h<=et.getHours()
                    var st = new Date(item.dateOfAppointment);
                    var et = new Date(ets);
                    var dob = new Date(item.composition.patient.dateOfBirth);
                    var strAge = getAge(dob);
                    var strData = "";
                    strData = item.composition.patient.lastName + " " + item.composition.patient.middleName + " " + item.composition.patient.firstName + "\n";
                    strData = strData + kendo.toString(dob, "dd-MMM-yyyy") + " (" + strAge + ")," + item.composition.patient.gender + "\n";
                    strData = strData + kendo.toString(st, "dd-MMM-yyyy h:mm tt") + " - " + kendo.toString(et, "dd-MMM-yyyy h:mm tt") + "\n";
                    try {
                        strData = strData + item.composition.appointmentReason.desc + "\n";
                        strData = strData + item.composition.appointmentType.desc + "\n";
                    } catch (ex) { }
                    returnObj.strData = strData;
                    returnObj.appointmentId = item.id;
                    returnObj.providerId = item.providerId;
                    returnObj.duration = item.duration;
                    return returnObj;
                }

            }
            else if(pItem.id == item.providerId){
				txtPRDate.setHours(tItem.h);
				var currMS = new Date(txtPRDate);
				currMS.setMinutes(tItem.m);
				currMS = currMS.getTime();
				currMS = currMS + 1000;
				if (currMS >= item.dateOfAppointment && currMS <= ets) { //&& tItem.h<=et.getHours()
					var st = new Date(item.dateOfAppointment);
					var et = new Date(ets);
					var dob = new Date(item.composition.patient.dateOfBirth);
					var strAge = getAge(dob);
					var strData = "";
					strData = item.composition.patient.lastName+ " " + item.composition.patient.middleName + " " + item.composition.patient.firstName  + "\n";
					strData = strData + kendo.toString(dob, "dd-MMM-yyyy") + " (" + strAge + ")," + item.composition.patient.gender + "\n";
					strData = strData + kendo.toString(st, "dd-MMM-yyyy h:mm tt") + " - " + kendo.toString(et, "dd-MMM-yyyy h:mm tt") + "\n";
					try {
						strData = strData + item.composition.appointmentReason.desc + "\n";
						strData = strData + item.composition.appointmentType.desc + "\n";
					} catch (ex) { }
					returnObj.strData = strData;
					returnObj.appointmentId = item.id;
					returnObj.providerId = item.providerId;
					returnObj.duration = item.duration;
					return returnObj;
				}
			}
        }
    }
    return returnObj;
}


function getSUDisplay(tItem,pItem,appArray){
    var strData1 = "";
    var i = 0;
    var max1;
    var txtPRDate = $("#txtPRDate").data("kendoDatePicker");
    txtPRDate = new Date(txtPRDate.value());
    // var val = _.groupBy(appArray, 'providerId');
    // max = _.max(val, function(o)
    // {
    //     return o.length
    // });
    for(var a=0;a<appArray.length;a++){
        var item = appArray[a];
        if(item){

            var ets = item.dateOfAppointment+(item.duration*60*1000);

            if(pItem.id == item.providerId){//== tItem.h && st.getMinutes() ==
                // txtPRDate.setHours(tItem.h);
                // var currMS = new Date(txtPRDate);
                // // currMS.setMinutes(tItem.m);
                // currMS = currMS.getTime();
                // currMS = currMS+1000;
                i = i+1;
                // if(currMS>=item.dateOfAppointment && currMS<=ets){ //&& tItem.h<=et.getHours()
                    var st = new Date(item.dateOfAppointment);
                    var et = new Date(ets);
                    var strData = '';
                	strData = kendo.toString(st,"h:mm tt")+" - "+kendo.toString(et,"h:mm tt")+"</br>";
                    strData = strData+item.composition.patient.lastName+" "+item.composition.patient.middleName+" "+item.composition.patient.firstName+"</br>";
                    try{
                        strData = strData+ item.composition.appointmentReason.desc;
                    }catch(ex){}
                strData = '<td style="font-size:10px;padding:0;">' + '<div class="tblSmContent" appointmentid="' + item.id + '" providerid="' + item.providerId + '"><a href="javascript:void(0);" class="cut-appointment" onclick="onClickCutAppointment(this)"></a><a href="javascript:void(0)" id="popupClose'+ i +'" class="popupClose_staff" onclick="onClickDeleteAppt('+item.id+',' + i + ')"></a><div class="draggable-item" appointmentid="' + item.id + '" providerid="' + item.providerId + '">'+ strData +'</div></div></td>';
                // strData = '<td style="font-size:10px;padding:0;"><div class="draggable-item" appointmentid="' + item.id + '" providerid="' + item.providerId + '">' + strData + '</div></td>';
                strData1 = strData1 + strData;
                // }
                // strData1 = strData;
                // return strData1;
            }
        }
    }
    if(i != max.length){
        var len = max.length-i;
        for(var a=0;a<len;a++) {
            strData1 = strData1 +'<td class="connected-sortable" style="font-size:10px;padding:0;"><a href="javascript:void(0);" onclick="onClickPasteAppointment(this)"><img class="paste-appointment" ></a></td>';
        }
    }
    strData1 = strData1 + '<td class="connected-sortable" style="font-size:10px;padding:0;"><a href="javascript:void(0);" onclick="onClickPasteAppointment(this)"><img class="paste-appointment"></a></td>';

    return strData1;
}
function getProviderStaff2dAppDays1(tItem,pItem,appArray){
	var txtPRDate = $("#txt2dDate1").data("kendoDatePicker");
	txtPRDate = new Date(txtPRDate.value());
	for(var a=0;a<appArray.length;a++){
		var item = appArray[a];
		if(item){

			var ets = item.dateOfAppointment+(item.duration*60*1000);

			if(pItem.id == item.providerId){//== tItem.h && st.getMinutes() ==
				txtPRDate.setHours(tItem.h);
				var currMS = new Date(txtPRDate);
				currMS.setMinutes(tItem.m);
				currMS = currMS.getTime();
				currMS = currMS+1000;
				item.dateOfAppointment = item.dateOfAppointment-(15*60*1000);
				ets = ets+(15*60*1000);
				if(currMS>=item.dateOfAppointment && currMS<=ets){ //&& tItem.h<=et.getHours()
					var strObj = {};
					var st = new Date(item.dateOfAppointment);
					var et = new Date(ets);
					var dob = new Date(item.composition.patient.dateOfBirth);
					var strAge = getAge(dob);
					var strData = "";
					strData = item.composition.patient.firstName+" "+item.composition.patient.middleName+" "+item.composition.patient.lastName+"\n";
					strData = strData+kendo.toString(dob,"MM/dd/yyyy")+" ("+strAge+"),"+item.composition.patient.gender+"\n";
					strData = strData+kendo.toString(st,"h:mm:ss tt")+" - "+kendo.toString(et,"h:mm:ss tt")+"\n";
					try{
						strData = strData+ item.composition.appointmentReason.desc+"\n";
						strData = strData+item.composition.appointmentType.desc+"\n";
					}catch(ex){}
					strObj.id = item.id;
					strObj.name = item.item.composition.patient.firstName+" "+item.composition.patient.middleName+" "+item.composition.patient.lastName;
					strObj.st = kendo.toString(st,"h:mm:ss tt");
					strObj.et = kendo.toString(et,"h:mm:ss tt");
					try{
						strObj.reason = item.composition.appointmentReason.desc;
						strObj.et = item.composition.appointmentType.desc;
					}catch(ex){}
					strObj.strData = strData;
					return strObj;
				}

			}
		}
	}
	return "";
}
function getProviderStaff2dAppDays(tItem,pItem,appArray){
	var txtPRDate = $("#txt2dDate").data("kendoDatePicker");
	txtPRDate = new Date(txtPRDate.value());
	for(var a=0;a<appArray.length;a++){
		var item = appArray[a];
		if(item){

			var ets = item.dateOfAppointment+(item.duration*60*1000);

			if(pItem.id == item.providerId){//== tItem.h && st.getMinutes() ==
				txtPRDate.setHours(tItem.h);
				var currMS = new Date(txtPRDate);
				currMS.setMinutes(tItem.m);
				currMS = currMS.getTime();
				currMS = currMS+1000;
				//item.dateOfAppointment = item.dateOfAppointment-(15*60*1000);
				//ets = ets+(15*60*1000);
				if(currMS>=item.dateOfAppointment && currMS<=ets){ //&& tItem.h<=et.getHours()
					var st = new Date(item.dateOfAppointment);
					var et = new Date(ets);
					var dob = new Date(item.composition.patient.dateOfBirth);
					var strAge = getAge(dob);
					var strData = "";
					strData = item.composition.patient.firstName+" "+item.composition.patient.middleName+" "+item.composition.patient.lastName+"\n";
					strData = strData+kendo.toString(dob,"MM/dd/yyyy")+" ("+strAge+"),"+item.composition.patient.gender+"\n";
					strData = strData+kendo.toString(st,"h:mm:ss tt")+" - "+kendo.toString(et,"h:mm:ss tt")+"\n";
					try{
						strData = strData+ item.composition.appointmentReason.desc+"\n";
						strData = strData+item.composition.appointmentType.desc+"\n";
					}catch(ex){}
					var strObj = {};
					strObj.id = item.id;
					strObj.name = item.composition.patient.firstName+" "+item.composition.patient.middleName+" "+item.composition.patient.lastName;
					strObj.st = kendo.toString(st,"h:mm:ss tt");
					strObj.et = kendo.toString(et,"h:mm:ss tt");
					try{
						strObj.reason = item.composition.appointmentReason.desc;
						strObj.desc = item.composition.appointmentType.desc;
					}catch(ex){}
					strObj.strData = strData;
					return strObj;
				}

			}
		}
	}
	return "";
}
function getProviderStaff2dAppDaysDot(tItem,pItem,appArray){
	var txtPRDate = $("#txt2dDate").data("kendoDatePicker");
	txtPRDate = new Date(txtPRDate.value());
	for(var a=0;a<appArray.length;a++){
		var item = appArray[a];
		if(item){

			var ets = item.dateOfAppointment+(item.duration*60*1000);

			if(pItem.id == item.providerId){//== tItem.h && st.getMinutes() ==
				txtPRDate.setHours(tItem.h);
				var currMS = new Date(txtPRDate);
				currMS.setMinutes(tItem.m);
				currMS = currMS.getTime();
				//currMS = currMS+1000;
				//item.dateOfAppointment = item.dateOfAppointment-(15*60*1000);
				//ets = ets+(15*60*1000);
				//console.log(currMS,item.dateOfAppointment);
				if(currMS >= item.dateOfAppointment){
					var st = new Date(item.dateOfAppointment);
					var et = new Date(ets);
					var dob = new Date(item.composition.patient.dateOfBirth);
					var strAge = getAge(dob);
					var strData = "";
					strData = item.composition.patient.firstName+" "+item.composition.patient.middleName+" "+item.composition.patient.lastName+"\n";
					strData = strData+kendo.toString(dob,"MM/dd/yyyy")+" ("+strAge+"),"+item.composition.patient.gender+"\n";
					strData = strData+kendo.toString(st,"MM/dd/yyyy h:mm:ss tt")+" - "+kendo.toString(et,"MM/dd/yyyy h:mm:ss tt")+"\n";
					try{
						strData = strData+ item.composition.appointmentReason.desc+"\n";
						strData = strData+item.composition.appointmentType.desc+"\n";
					}catch(ex){}
					return strData;
				}

			}
		}
	}
	return "";
}
function getProviderStaffAppDays(tItem,pItem,appArray){
	var txtPRDate = $("#txtPRSDate").data("kendoDatePicker");
	txtPRDate = new Date(txtPRDate.value());
	for(var a=0;a<appArray.length;a++){
		var item = appArray[a];
		if(item){

			var ets = item.dateOfAppointment+(item.duration*60*1000);

			if(pItem.id == item.providerId){//== tItem.h && st.getMinutes() ==
				txtPRDate.setHours(tItem.h);
				var currMS = new Date(txtPRDate);
				currMS.setMinutes(tItem.m);
				currMS = currMS.getTime();
				currMS = currMS+1000;
				if(currMS>=item.dateOfAppointment && currMS<=ets){ //&& tItem.h<=et.getHours()
					var st = new Date(item.dateOfAppointment);
					var et = new Date(ets);
					var dob = new Date(item.composition.patient.dateOfBirth);
					var strAge = getAge(dob);
					var strData = "";
					strData = item.composition.patient.firstName+" "+item.composition.patient.middleName+" "+item.composition.patient.lastName+"\n";
					strData = strData+kendo.toString(dob,"MM/dd/yyyy")+" ("+strAge+"),"+item.composition.patient.gender+"\n";
					strData = strData+kendo.toString(st,"MM/dd/yyyy h:mm:ss tt")+" - "+kendo.toString(et,"MM/dd/yyyy h:mm:ss tt")+"\n";
					try{
						strData = strData+ item.composition.appointmentReason.desc+"\n";
						strData = strData+item.composition.appointmentType.desc+"\n";
					}catch(ex){}
					return strData;
				}

			}
		}
	}
	return "";
}
function onClickCancel(){
	popupClose(false);
}
function popupClose(obj){
	var windowWrapper = new kendoWindowWrapper();
	windowWrapper.closePageWindow(obj);
}




function buildAppointmentsListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;

    var columns = [];
    if (dataSource != null && dataSource.length > 0) {
        var firstObj = dataSource[0];

        for (var key in firstObj) {
            columns.push(key);
        }
    }

    if (columns != null && columns.length > 0) {
        for (var counter = 0; counter < columns.length; counter++) {
            var strGridColumn = '{';
            strGridColumn = strGridColumn + '"title":"' + columns[counter] + '","field":"' + columns[counter] + '"}';

            var jsonGridColumn = JSON.parse(strGridColumn);
            gridColumns.push(jsonGridColumn);

        }
    }

    angularUIgridUnavWrapper.creategrid(dataSource, gridColumns, otoptions);
    adjustHeightUnAvailable();
}

function adjustHeightUnAvailable() {
    var defHeight = 230;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    angularUIgridUnavWrapper.adjustGridHeight(cmpHeight);
}

var selectPatients = null;
function showPatients(time, date) {
	selectPatients = null;
	var patients = null;
    var selectedDate = date.trim().substring(4);
    $("#rdUnAllocate").prop("checked", true);

    flatAppointments.find(function (item, index) {
        if (item.time.trim() === time.trim() && item.date.trim() === date.trim()) {
        	// if(item.patients[index] && item.patients[index].time) {
        	// 	item.patients[index]
            // }
            patients = item;
            // patients.appId= item.idk;
        }
    });

    if (patients !== null && patients.patients.length > 0) {
		selectPatients = patients;
        $("#dgridUnAllocatedAppointmentsList").css("display", "none");
        $("#divPatients").css("display", "block");
        $("#divTab8").css("display", "none");
		bindSelectFieldComboBox();

        buildPatientsListGrid(patients);

        selectedDate = selectedDate.split("/");


        selectedDate = (selectedDate[1] + "/" + selectedDate[0] + "/" + selectedDate[2]);

        var selDate = new Date(selectedDate);
        var day = selDate.getDate();
        var month = selDate.getMonth();
        month = month+1;
        var year = selDate.getFullYear();

        var stDate = month+"/"+day+"/"+year;
        stDate = stDate+" 00:00:00";

        var startDate = new Date(stDate);
        var stDateTime = startDate.getTime();

        var etDate = month+"/"+day+"/"+year;
        etDate = etDate+" 23:59:59";

        // var endtDate = new Date(etDate);
        // var edDateTime = endtDate.getTime();

        var startDate = new Date(stDate);
        var endtDate = new Date(etDate);

        var day = startDate.getDate()-startDate.getDay();
        var pDate = new Date(startDate);
        pDate.setDate(day);

        endtDate.setDate(day+6);
        // staffStDateTime = pDate.getTime()/1000;
        // staffEdDateTime = endtDate.getTime()/1000;
        //
        // staffStDateTime = staffStDateTime - 1;

        staffStDateTime = patients.dateOfAppointmentTime;
		staffEdDateTime	= patients.dateOfAppointmentTime + (patients.duration * 60 * 1000) + (86400 * 1000);

		buildProvidersListGrid([]);
		var urlExtn = '/provider/list?is-active=1&is-deleted=0';
		getAjaxObject(ipAddress+urlExtn,"GET",onAllStaffListData,onErrorMedication);

        // var url = ipAddress+"/homecare/providers/filter-by/appointments/?start-time="+staffStDateTime+"&end-time="+staffEdDateTime+"&type=201";
		var intfacilityid = Number($("#txtAppPTFacility option:selected").val());
		var url = ipAddress+"/appointment/list/?facility-id="+intfacilityid+"&to-date="+staffEdDateTime+"&from-date="+staffStDateTime+"&is-active=1&is-deleted=0";
        getAjaxObject(url, "GET", onStaffListData, onErrorMedication);
    }
    else {
        alert('There are no appointments at this time.');
    }
}



function bindSelectFieldComboBox(){
	$("#cmbSelectField").empty();
	var selOptions = [];
	selOptions.push({
		"Key":1,
		"Value":"Past Staff View History"
	});
	selOptions.push({
		"Key":2,
		"Value":"Gender"
	});
	selOptions.push({
		"Key":3,
		"Value":"Language"
	});
	for (var i = 0; i < selOptions.length; i++) {
		$("#cmbSelectField").append('<option value="' + selOptions[i].Key + '">' + selOptions[i].Value + '</option>');
	}

	$("#cmbSelectField").multipleSelect({
		selectAll: true,
		width: 200,
		dropWidth: 200,
		multipleWidth: 200,
		placeholder: 'Select Field',
		onCheckAll:function(e){
			var arr = $('#cmbSelectField').multipleSelect('getSelects');
			if(arr !== null && arr.length > 0){


					onGetPreviousHistory();

			}
		},
		onClick:function(e){
			var arr = $('#cmbSelectField').multipleSelect('getSelects');
			if(arr !== null && arr.length > 0){

				if (arr.indexOf("2") > -1)
					onSelectServiceUser(2);
				else if (arr.indexOf("3") > -1)
					onSelectServiceUser(3);
				else
					onGetPreviousHistory();

			}
		}
	});

	// $("li input[name=selectAll]").parent().parent().hide();

}

function buildPatientsListGrid(dataSource) {
    $('#ulPatientsList').empty();
    if (dataSource != null && dataSource.patients.length > 0) {
        for (var counter = 0; counter < dataSource.patients.length; counter++) {
            var id = "patient-" + counter;
            var imageServletUrl;
            if(dataSource.patients[counter].photoExt){
                imageServletUrl = ipAddress+"/download/patient/photo/"+dataSource.patients[counter].id+"/?"+Math.round(Math.random()*1000000)+"&access_token="+sessionStorage.access_token+"&tenant="+sessionStorage.tenant;;
            }else if(dataSource.patients[counter].gender.toLowerCase() == "male"){
                imageServletUrl = "../../img/AppImg/HosImages/male_profile.png";
            }else if(dataSource.patients[counter].gender.toLowerCase() == "female"){
                imageServletUrl = "../../img/AppImg/HosImages/profile_female.png";//ipAddress+"/download/patient/photo/"+strPatientId+"/?"+Math.round(Math.random()*1000000);
            }
            // var $item = $('<li id="' + id + '" class="list-group-item"> <br> <img src="'+imageServletUrl +'"> ' + dataSource[counter].id + ' - ' + dataSource[counter].firstName + ' ' + dataSource[counter].lastName + ", "+ kendo.toString(new Date(dataSource[counter].dateOfBirth),"dd/MM/yyyy") + ', Appointment Date: '+dataSource.dateOfAppointment+' </div> </li>');
            // var $item = $('<li id="' + id + '" > <div class="thumbpatientDiv"> <img src="'+imageServletUrl +'" class="docThumb rounded-circle" alt="Cinque Terre"> <h5>' + dataSource[counter].id + ' - ' + dataSource[counter].lastName+ ' ' + dataSource[counter].firstName  + '</h5> <h6>' + ", "+ kendo.toString(new Date(dataSource[counter].dateOfBirth),"dd/MM/yyyy") + '</h6>' + ', Appointment Date: '+dataSource.dateOfAppointment+' <div class="dragPhoto" ondrop="drop(event)" ondragover="allowDrop(event)"></div> </div></li>');
            	var $item = $('<li id="' + id + '" > <div class="thumbpatientDiv"> <img src="'+imageServletUrl +'" class="docThumb rounded-circle" alt="Cinque Terre"> <h5>' + dataSource.patients[counter].id + ' - ' + dataSource.patients[counter].lastName+ ' ' + dataSource.patients[counter].firstName  + '</h5>' +dataSource.patients.dateOfAppointment+' '+ dataSource.time+' ,  <br> '  + dataSource.patients.appointmentReason.desc+'<div><div class="dragPhoto" ondrop="drop(event,'+ counter  +','+dataSource.idk+')" ondragover="allowDrop(event)"></div><a href="javascript:void(0)" id="popupClose'+ counter +'" class="popupClose" style="display:none" onclick="onClickUnassignStaff('+dataSource.idk+',' + counter + ')"></a></div></div></li>')

            var tip = "Name: "+dataSource.patients[counter].lastName+" "+dataSource.patients[counter].firstName+"\n";
            tip = tip+"DOB: "+kendo.toString(new Date(dataSource.patients[counter].dateOfBirth),"dd/MM/yyyy")+"\n";
            tip = tip+"Gender: "+dataSource.patients[counter].gender+"\n";
            //$('.k-event').attr('title', tip);
            $item.attr("title",tip);
            $item.appendTo('#ulPatientsList');
            // $(".docThumb").droppable({
            //     accept: '.imgStaff',
            //     drop: function (e, ui) {
			//
            //         $(this).find('.staff').remove();
            //         var $staffDiv = $('<div class="dragPhoto">' + $(imgStaff).html() + '</div>');
            //     // <div class="dragPhoto"></div>
            //         var text = $(ui.draggable).text();
            //         staffId = text.substr(0, text.indexOf('-')).trim();
            //         $staffDiv.appendTo($(this));
            //     }
            // });

            // 	$item.droppable({
            //     accept: '.imgStaff',
            //     drop: function (e, ui) {
			//
            //         $(this).find('.dragPhoto').remove();
            //         var $staffDiv = $('<div class="dragPhoto">' + $(ui.draggable).html() + '</div>');
            //         var text = $(ui.draggable).text();
            //         staffId = text.substr(0, text.indexOf('-')).trim();
            //         $staffDiv.appendTo($(this));
            //     }
            // });
        }

    } else {
        $('#ulPatientsList').empty();
    }
}

var distCompareProviderIds = [];
var distCurrentProviderIds = [];
function onStaffListData(dataObj) {
	var dataArray = [];
	distCompareProviderIds = [];
	distCurrentProviderIds = [];
	if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
		// if (dataObj.response.providers) {
		//     // if ($.isArray(dataObj.response.providers)) {
		//     //     dataArray = dataObj.response.providers;
		//     // } else {
		//     //     dataArray.push(dataObj.response.providers);
		//     // }
		// }

		if (dataObj.response.appointment) {
			if ($.isArray(dataObj.response.appointment)) {
				dataArray = dataObj.response.appointment;
			} else {
				dataArray.push(dataObj.response.appointment);
			}
		}
	}

	if (dataArray && dataArray.length > 0) {

		var lookup = {};

		var distApptProviderIds = [];


		for (var item, i = 0; item = dataArray[i++];) {
			if (item.providerId != 0) {
				var providerId = item.providerId;

				if (!(providerId in lookup)) {
					lookup[providerId] = 1;
					if (providerId > 0)
						var response = _.where(allDataArray, {id: providerId})
						distCurrentProviderIds.push(response[0]);
						distApptProviderIds.push(providerId);
				}
			}
		}

		distCompareProviderIds = _.difference(allDataArray, distCurrentProviderIds);
		buildProvidersListGrid(distCompareProviderIds);
	} else {

		buildProvidersListGrid(allDataArray);
	}

}
var allDataArray = [];

function onAllStaffListData(dataObj) {
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.provider) {
            if ($.isArray(dataObj.response.provider)) {
                dataArray = dataObj.response.provider;
            } else {
                dataArray.push(dataObj.response.provider);
            }
        }
		allDataArray = dataArray;
    }
    buildProvidersListGrid(dataArray);
}

function buildProvidersListGrid(dataSource) {
    $('#ulStaffList').empty();
    if (dataSource != null && dataSource.length > 0) {
        for (var counter = 0; counter < dataSource.length; counter++) {
            var id = "provider-" + counter;
            if(radioValue == 1) {
                // var $item = $('<li id="' + id + '"class="list-group-item" style="padding: 0"> <br> <img ondragstart="drag(event,' + dataSource[counter].id + ')" draggable="true" class="imgStaff imgStaff' + counter + '" src="' + ipAddress + "/homecare/download/providers/photo/?" + Math.round(Math.random() * 1000000) + "&access_token=" + sessionStorage.access_token + "&tenant=" + sessionStorage.tenant + "&id=" + dataSource[counter].id + '">' + '  ' + dataSource[counter].id + ' - ' + dataSource[counter].lastName + ' ' + dataSource[counter].firstName + '      (' + SecondsTohhmmss(dataSource[counter].startTime) + ' - ' + SecondsTohhmmss(dataSource[counter].endTime) + ')' + '</li>');
				var $item = $('<li id="' + id + '"class="list-group-item" style="padding: 0"> <br> <img ondragstart="drag(event,' + dataSource[counter].id + ')" draggable="true" style="border: 1px solid #ddd;" class="imgStaff imgStaff' + counter + '" src="' + ipAddress + "/homecare/download/providers/photo/?" + Math.round(Math.random() * 1000000) + "&access_token=" + sessionStorage.access_token + "&tenant=" + sessionStorage.tenant + "&id=" + dataSource[counter].id + '">' + '  ' + dataSource[counter].id + ' - ' + dataSource[counter].lastName + ' ' + dataSource[counter].firstName + '</li>');
            }
            else{
                var $item = $('<li id="' + id + '"class="list-group-item" style="padding: 0"> <br> <img ondragstart="drag(event,' + dataSource[counter].id + ')" draggable="true" style="border: 1px solid #ddd;" class="imgStaff imgStaff' + counter + '" src="' + ipAddress + "/homecare/download/providers/photo/?" + Math.round(Math.random() * 1000000) + "&access_token=" + sessionStorage.access_token + "&tenant=" + sessionStorage.tenant + "&id=" + dataSource[counter].id + '">' + '  ' + dataSource[counter].id + ' - ' + dataSource[counter].lastName + ' ' + dataSource[counter].firstName + '</li>');
			}
            // var $drag = $(' <img class="imgStaff" src="'+ipAddress+"/homecare/download/providers/photo/?"+Math.round(Math.random()*1000000)+"&access_token="+sessionStorage.access_token+"&tenant="+sessionStorage.tenant+"&id="+dataSource[counter].id+'">');
			// var dr = "imgStaff" + counter +";
			// $(".imgStaff").draggable({
            //     helper: "clone"
            // });
            $item.appendTo('#ulStaffList');
            // $drag.appendTo('#ulStaffList');
        }
    }
    else {
        $('#ulStaffList').empty();
    }
}


function onClickSubmit() {
    if($("#txtAppPTDate").val() !== ""){
        flatAppointments=[];
        $("#divPatients").css("display", "none");
        $('#dgridUnAllocatedAppointmentsList').css('display', 'none');
        // var selectedDate = GetDateTime("txtAppPTDate");

        var txtAppPTDate = $("#txtAppPTDate").data("kendoDatePicker");
        var selectedDate = txtAppPTDate.value();

        allDatesInWeek = [];
        APIResponse = [];

        for (let i = 1; i <= 7; i++) {
            var first = selectedDate.getDate() - (selectedDate.getDay() + 1) + i;
            var day = new Date(selectedDate.setDate(first));

            allDatesInWeek.push(day);
        }
        var firstDayOfWeek = allDatesInWeek[0];
        var lastDayOfWeek = allDatesInWeek[allDatesInWeek.length - 1];

        // var selectedDate = GetDateTime("txtDate");
        var selDate = new Date(selectedDate);

        var day = selDate.getDate();
        var month = selDate.getMonth();
        month = month+1;
        var year = selDate.getFullYear();

        var stDate = month+"/"+day+"/"+year;
        stDate = stDate+" 00:00:00";

        var startDate = new Date(stDate);
        var stDateTime = startDate.getTime();

        var etDate = month+"/"+day+"/"+year;
        etDate = etDate+" 23:59:59";

        var endtDate = new Date(etDate);
        var edDateTime = endtDate.getTime();

        var startDate = new Date(stDate);
        var endtDate = new Date(etDate);

        var day = startDate.getDate()-startDate.getDay();
        var pDate = new Date(startDate);
        pDate.setDate(day);

        endtDate.setDate(day+6);
        var stDateTime = pDate.getTime();
        var edDateTime = endtDate.getTime();
		//var txtAppPTFacility = $("#txtAppPTFacility").data("kendoComboBox");
		var intfacilityid = Number($("#txtAppPTFacility option:selected").val());
        var patientListURL = ipAddress+"/appointment/list/?facility-id="+intfacilityid+"&provider-id=0&to-date="+edDateTime+"&from-date="+stDateTime+"&is-active=1";;

        getAjaxObject(patientListURL,"GET",onPatientAppListDataUnAvailable,onErrorMedication);

    }
    else{
        alert("Select date");
    }
}


function onPatientAppListDataUnAvailable(dataObj) {
    // console.log(dataObj);
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.appointment) {
            if ($.isArray(dataObj.response.appointment)) {
                APIResponse = dataObj.response.appointment;
            } else {
                APIResponse.push(dataObj.response.appointment);
            }
        }
    }

    if (APIResponse !== null && APIResponse.length > 0) {
        var appointments = APIResponse;
        var updatedAppointments = $.map(appointments, function (element, index) {
            var duration = parseInt(element.duration);
            var appointmentStartDate = new Date(parseInt(element.dateOfAppointment));
            var appointmentEndDate = dateAdd(appointmentStartDate, 'minute', duration);

            var startHour = appointmentStartDate.getHours(), endHour = appointmentEndDate.getHours();
            var timeSlots = [];
            for (var counter = startHour; counter <= endHour; counter++) {

                var hours = counter > 12 ? counter - 12 : counter;
                var am_pm = counter >= 12 ? "PM" : "AM";
                hours = hours < 10 ? "0" + hours : hours;
                time = hours + ":00 " + am_pm;

                timeSlots.push(time);
            }

            element.appointmentStartDate = appointmentStartDate;
            element.appointmentEndDate = appointmentEndDate;
            element.timeSlots = timeSlots;

            return element;
        });


        for (var counter = 0; counter < updatedAppointments.length; counter++) {
            var appointment = updatedAppointments[counter];

            for (var counter2 = 0; counter2 < appointment.timeSlots.length; counter2++) {
                var time = appointment.timeSlots[counter2];
                var date = daysOfWeek[appointment.appointmentStartDate.getDay()] + " " + formatDate(appointment.appointmentStartDate);
                var flatAppointmentObj = {};

                var index = -1;
                flatAppointments.find(function (item, i) {
                    if (item.time === time && item.date === date) {
                        index = i;
                    }
                });

                if (index > -1) {
                    flatAppointments[index]["patients"].push(appointment.composition.patient);
                }
                else {

                    flatAppointmentObj.time = time;
                    flatAppointmentObj.date = date;
                    flatAppointmentObj.idk = appointment.id;
                    flatAppointmentObj.duration = appointment.duration;
                    flatAppointmentObj.dateOfAppointmentTime = appointment.dateOfAppointment;
                    flatAppointmentObj.patients = [];
                    flatAppointmentObj.patients.dateOfAppointment =GetDateEdit(appointment.dateOfAppointment);
                    // flatAppointmentObj.patients.time = time;
                    // appointment.composition.patient.time = time;
                    // appointment.composition.patient.date = date;
                    flatAppointmentObj.patients.push(appointment.composition.patient);
                    flatAppointmentObj.patients.appointmentReason =appointment.composition.appointmentReason;
                    flatAppointments.push(flatAppointmentObj);
                }
            }
        }


        var hoursArray = [];
        hoursArray = $.map(APIResponse, function (e, i) {
            return new Date(e.dateOfAppointment).getHours();
        });

        var minHour = Math.min(...hoursArray);
        var maxHour = Math.max(...hoursArray);

        var allTimeSlots = [];

        for (var counter3 = minHour; counter3 <= maxHour; counter3++) {

            var hours = counter3 > 12 ? counter3 - 12 : counter3;
            var am_pm = counter3 >= 12 ? "PM" : "AM";
            hours = hours < 10 ? "0" + hours : hours;
            time = hours + ":00 " + am_pm;

            allTimeSlots.push(time);
        }

        var allDaysInWeek = [];
        for (var counter4 = 0; counter4 < allDatesInWeek.length; counter4++) {
            allDaysInWeek.push(daysOfWeek[allDatesInWeek[counter4].getDay()] + " " + formatDate(allDatesInWeek[counter4]));
        }

        var dataSource = [];

        for (var counter5 = 0; counter5 < allTimeSlots.length; counter5++) {
            var timeSlot = allTimeSlots[counter5];
            var dataSourceObj = {};
            dataSourceObj.Time = timeSlot;

            for (var counter6 = 0; counter6 < allDaysInWeek.length; counter6++) {
                var tempDate = allDaysInWeek[counter6];
                var noOfAppointments = '';

                flatAppointments.find(function (item, index) {
                    if (item.time === timeSlot && item.date === tempDate) {
                        noOfAppointments = item.patients.length;
                    }
                });

                dataSourceObj[allDaysInWeek[counter6]] = noOfAppointments;
            }

            dataSource.push(dataSourceObj);
        }
        if (dataSource != null && dataSource.length > 0) {
            $('#dgridUnAllocatedAppointmentsList').css('display', 'block');
            buildAppointmentsListGrid(dataSource);
        }

    }
    else{
        customAlert.info("Info","No appointments found in this week")
    }

}

function dateAdd(date, interval, units) {
    var ret = new Date(date); //don't change original date
    var checkRollover = function () { if (ret.getDate() != date.getDate()) ret.setDate(0); };
    switch (interval.toLowerCase()) {
        case 'year': ret.setFullYear(ret.getFullYear() + units); checkRollover(); break;
        case 'quarter': ret.setMonth(ret.getMonth() + 3 * units); checkRollover(); break;
        case 'month': ret.setMonth(ret.getMonth() + units); checkRollover(); break;
        case 'week': ret.setDate(ret.getDate() + 7 * units); break;
        case 'day': ret.setDate(ret.getDate() + units); break;
        case 'hour': ret.setTime(ret.getTime() + units * 3600000); break;
        case 'minute': ret.setTime(ret.getTime() + units * 60000); break;
        case 'second': ret.setTime(ret.getTime() + units * 1000); break;
        default: ret = undefined; break;
    }
    return ret;
}


function formatDate(date) {

    var dd = date.getDate();

    var mm = date.getMonth() + 1;
    var yyyy = date.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }

    if (mm < 10) {
        mm = '0' + mm;
    }

    return dd + '/' + mm + '/' + yyyy;
}

var SecondsTohhmmss = function(totalSeconds) {
    var result = '';
	if(totalSeconds != null) {
        var hours = Math.floor(totalSeconds / 3600);
        var minutes = Math.floor((totalSeconds - (hours * 3600)) / 60);
        var seconds = totalSeconds - (hours * 3600) - (minutes * 60);

        // round seconds
        seconds = Math.round(seconds * 100) / 100

        var am_pm;
       result = (hours < 10 ? "0" + hours : hours);
        am_pm = result >= 12 ? "PM" : "AM";
        result = result > 12 ? result - 12 : result;
        result += ":" + (minutes < 10 ? "0" + minutes : minutes);
        // result += ":" + (seconds  < 10 ? "0" + seconds : seconds);
        result += " " + am_pm;
    }
    return result;
}


function allowDrop(ev) {
    ev.preventDefault();
    // $(".dragPhoto").find('.docThumb').remove();
}

function drag(ev,id) {
    ev.dataTransfer.setData("text", ev.target.id);
    staffId = id;
}

function drop(ev,gid,appId) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    var $staffDiv= "";
    // $staffDiv = $('<img class="docThumb rounded-circle" src="">');
    // $staffDiv.appendTo(ev.target);
    // ev.target.clear();
    var id = "#popupClose"+gid;
    $(id).css("display","block");
    var url = ipAddress + "/homecare/download/providers/photo/?" + Math.round(Math.random() * 1000000) + "&access_token=" + sessionStorage.access_token + "&tenant=" + sessionStorage.tenant + "&id="+staffId;
    $staffDiv = $('<img class="docThumb rounded-circle docThumb'+gid+'" src='+ url+'>');
    $staffDiv.appendTo(ev.target);

    if(staffId != '' ) {
        customAlert.confirm("Confirm", "Are you sure want to attach staff?", function (response) {
            if (response.button == "Yes") {
                onClickStaffSave(appId);
            }
            else{
                $(".dragPhoto").find('.docThumb'+gid).remove();
                var popupId = "#popupClose"+gid;
                $(popupId).css("display","none");
			}
        })
    }
}

function onClickBackToList(){
    $("#divTab8").css("display","block");
    $("#dgridUnAllocatedAppointmentsList").css("display", "none");
    $("#divPatients").css("display", "none");
    // buildAppointmentsListGrid([]);
    $('#ulStaffList').empty();
    $('#ulPatientsList').empty();
    // $("#txtDate").val("");
	$('#btnAppPTSView').click();

}


function onClickStaffSave(appointid) {
    var response = _.where(APIResponse, {id: appointid})
    var appObj = {};
    appObj = response[0];
    appObj.id = appointid;
    delete appObj.providerId;
    delete appObj.composition;
    delete appObj.createdDate;
    delete appObj.appointmentStartDate;
    delete appObj.appointmentEndDate;
    delete appObj.handoverNotes;
    delete appObj.inTime;
    delete appObj.isSynced;
    delete appObj.modifiedDate;
    delete appObj.notes;
    delete appObj.outTime;
    delete appObj.readBy;
    delete appObj.syncTime;
    delete appObj.timeSlots;
    appObj.modifiedBy = Number(sessionStorage.userId);
    appObj.providerId = parseInt(staffId);

    var dataUrl = ipAddress+"/appointment/update";
    var arr = [];
    arr.push(appObj);
    createAjaxObject(dataUrl,arr,"POST",onAvlAppCreate,onErrorMedication);
}

function onAvlAppCreate(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            var msg = "Staff attached successfully";
            displaySessionErrorPopUp("Info", msg, function(res) {
                // onClickCancel();
				// onClickSubmit();
                // $("#divPatients").css("display", "none");

            })
        }else{
            customAlert.error("Error",dataObj.response.status.message);
        }
    }else{
        customAlert.error("Error",dataObj.response.status.message);
    }
}

function onClickUnassignStaff(id,counter) {
    customAlert.confirm("Confirm", "Are you sure want to unattach staff?", function (response) {
        if (response.button == "Yes") {
            $(".dragPhoto").find('.docThumb'+counter).remove();
            var popupId = "#popupClose"+counter;
            $(popupId).css("display","none");
            var response = _.where(APIResponse, {id: id})
            var appObj = {};
            appObj = response[0];
            appObj.id = id;
            delete appObj.providerId;
            delete appObj.composition;
            delete appObj.createdDate;
            delete appObj.appointmentStartDate;
            delete appObj.appointmentEndDate;
            delete appObj.handoverNotes;
            delete appObj.inTime;
            delete appObj.isSynced;
            delete appObj.modifiedDate;
            delete appObj.notes;
            delete appObj.outTime;
            delete appObj.readBy;
            delete appObj.syncTime;
            delete appObj.timeSlots;
            appObj.modifiedBy = Number(sessionStorage.userId);
            appObj.providerId = 0;

            var dataUrl = ipAddress + "/appointment/update";
            var arr = [];
            arr.push(appObj);
            createAjaxObject(dataUrl, arr, "POST", onUnAssignCreate, onErrorMedication);
        }
    });
}

function onUnAssignCreate(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            var msg = "Staff unattached successfully";
            displaySessionErrorPopUp("Info", msg, function(res) {
                // onClickCancel();
                // onClickSubmit();
                // $("#divPatients").css("display", "none");

            })
        }else{
            customAlert.error("Error",dataObj.response.status.message);
        }
    }else{
        customAlert.error("Error",dataObj.response.status.message);
    }
}


function deleteAppointmentSave(){
    //var cmbFacility = $("#cmbFacility").data("kendoComboBox");
    var obj = {};
    obj.createdBy = Number(sessionStorage.userId);
    obj.isActive = 0;
    obj.isDeleted = 1;
    var dsItem = appointmentSaveItem;//e.event;
    saveDate = dsItem.start;
    var sDate = new Date(dsItem.start);
    var eDate = new Date(dsItem.end);
    var diff = eDate.getTime() - sDate.getTime();
    diff = diff/1000;
    diff = diff/60;
    var nSec = sDate.getTime();
    var uSec = getGMTDateFromLocaleDate(nSec);
    obj.dateOfAppointment = nSec;//uSec;//getGMTDateFromLocaleDate(sDate.getTime());//sDate.getTime();//
    obj.duration = diff;
    var providerId = getProviderId(appProviderArr[appCount]);
    obj.providerId = Number(providerId);
    obj.facilityId = Number($("#cmbFacility option:selected").val());
    obj.patientId = dsItem.patientID;//parentRef.patientId;
    obj.appointmentType = dsItem.ownerId;
    obj.notes = dsItem.notes;//$("#taArea").val();
    if(dsItem.id == 0){
        obj.shiftValue = appointmentSaveItem.bill.shiftValue;
        obj.billingRateType = appointmentSaveItem.bill.billingRateTypeCode;
        obj.patientBillingRate = appointmentSaveItem.bill.rate;
        obj.billingRateTypeId = appointmentSaveItem.bill.billingRateTypeId;
        obj.swiftBillingId = appointmentSaveItem.bill.swiftBillingId;
        obj.payoutHourlyRate = appointmentSaveItem.pRate;
        obj.billToName = appointmentSaveItem.bill.billToname;
        obj.billToId = appointmentSaveItem.bill.billToId;
    }else{
        var pBillItem = getPatientBillingDetails(appointmentSaveItem.bill);
        obj.shiftValue = pBillItem.shiftValue;
        obj.billingRateType = pBillItem.billingRateTypeCode;
        obj.patientBillingRate = pBillItem.rate;
        obj.billingRateTypeId = pBillItem.billingRateTypeId;
        obj.swiftBillingId = pBillItem.swiftBillingId;
        obj.payoutHourlyRate = appointmentSaveItem.pRate;
        obj.billToName = pBillItem.billToname;
        obj.billToId = pBillItem.billToId;
    }

    if( dsItem.description1 &&  dsItem.description1.text){
        //$("#taArea").val();;
    }
    x = dsItem.description1;

    var dataUrl = "";
    if(dsItem.id == 0){
        updateFlag = false;
        dataUrl = ipAddress+"/appointment/create";
    }else{
        updateFlag = true;
        obj.id = dsItem.id;
        /*obj.shiftValue = dsItem.shiftValue;
        obj.billingRateType = dsItem.billingRateType;
        obj.patientBillingRate = dsItem.patientBillingRate;
        obj.billingRateTypeId = dsItem.billingRateTypeId;
        obj.swiftBillingId = dsItem.swiftBillingId;*/
        dataUrl = ipAddress+"/appointment/update";
    }
    var arr = [];
    arr.push(obj);
    createAjaxObject(dataUrl,arr,"POST",onDelete,onError);
}


var unAllocatedAppointments = [];
function onGetUnAllocated() {
    if($("#txtSTPRDate").val() !== ""){
        unAllocatedAppointments=[];
		unAllocatedAPIResponse = [];
        var txtSTPRDate = $("#txtSTPRDate").data("kendoDatePicker");
        var selectedDate = txtSTPRDate.value();
		var selectedCurrentDate = txtSTPRDate.value();

        allDatesInWeek = [];

        /*for (let i = 1; i <= 7; i++) {
            var first = selectedDate.getDate() - (selectedDate.getDay() + 1) + i;
            var day = new Date(selectedDate.setDate(first));

            allDatesInWeek.push(day);
        }
        var firstDayOfWeek = allDatesInWeek[0];
        var lastDayOfWeek = allDatesInWeek[allDatesInWeek.length - 1];*/

        // var selectedDate = GetDateTime("txtDate");
        var selDate = new Date(selectedDate);

        var day = selDate.getDate();
        var month = selDate.getMonth();
        month = month+1;
        var year = selDate.getFullYear();

        var stDate = month+"/"+day+"/"+year;
        stDate = stDate+" 00:00:00";

        var startDate = new Date(stDate);
        var stDateTime = startDate.getTime();

        var etDate = month+"/"+day+"/"+year;
        etDate = etDate+" 23:59:59";

        var endtDate = new Date(etDate);
        var edDateTime = endtDate.getTime();

        var startDate = new Date(stDate);
        var endtDate = new Date(etDate);

        var day = startDate.getDate()-startDate.getDay();
        var pDate = new Date(startDate);
        pDate.setDate(day);

       /* endtDate.setDate(day+6);*/
        var stDateTime = pDate.getTime();
        var edDateTime = endtDate.getTime();
        //var txtStFacility = $("#txtStFacility").data("kendoComboBox");

		var selCDate = new Date(selectedCurrentDate);

		var dayC = selCDate.getDate();
		var monthC = selCDate.getMonth();
		monthC = monthC+1;
		var yearC = selCDate.getFullYear();

		var stDateC = monthC+"/"+dayC+"/"+yearC;
		stDateC = stDateC+" 00:00:00";



		var etDateC = monthC+"/"+dayC+"/"+yearC;
		etDateC = etDateC+" 23:59:59";

		var startCDate = new Date(stDateC);
		var endtCDate = new Date(etDateC);

		var intfacilityid = Number($("#txtStFacility option:selected").val());
        var patientListURL = ipAddress+"/appointment/list/?facility-id="+intfacilityid+"&provider-id=0&to-date="+endtCDate.getTime()+"&from-date="+startCDate.getTime()+"&is-active=1";;

        getAjaxObject(patientListURL,"GET",onUnAllocatedAppointments,onErrorMedication);

    }
    else{
        alert("Select date");
    }
}

var unAllocatedAPIResponse = [];
function onUnAllocatedAppointments(dataObj) {
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.appointment) {
            if ($.isArray(dataObj.response.appointment)) {
                unAllocatedAPIResponse = dataObj.response.appointment;
            } else {
                unAllocatedAPIResponse.push(dataObj.response.appointment);
            }
        }

		for (var counter2 = 0; counter2 < unAllocatedAPIResponse.length; counter2++) {
			unAllocatedAPIResponse[counter2].appointmentId = unAllocatedAPIResponse[counter2].id;
			// unAllocatedAPIResponse[counter2].dateOfAppointment = providerAppointments[counter2].dateOfAppointment;
			// unAllocatedAPIResponse[counter2].duration = providerAppointments[counter2].duration;
			unAllocatedAPIResponse[counter2].fromDate = unAllocatedAPIResponse[counter2].dateOfAppointment;
			unAllocatedAPIResponse[counter2].toDate = unAllocatedAPIResponse[counter2].fromDate + (unAllocatedAPIResponse[counter2].duration * 60 * 1000);
			unAllocatedAPIResponse[counter2].lastName = unAllocatedAPIResponse[counter2].composition.patient.lastName;
			unAllocatedAPIResponse[counter2].patientId = unAllocatedAPIResponse[counter2].composition.patient.id;
			unAllocatedAPIResponse[counter2].middleName = unAllocatedAPIResponse[counter2].composition.patient.middleName;
			unAllocatedAPIResponse[counter2].firstName = unAllocatedAPIResponse[counter2].composition.patient.firstName;
			unAllocatedAPIResponse[counter2].desc = unAllocatedAPIResponse[counter2].composition.appointmentReason.desc;
		}
    }
}

function onBindUnAllocatedAppt(){
    var strData1 ="";
    var i =0;
    if (unAllocatedAPIResponse !== null && unAllocatedAPIResponse.length > 0) {
        for (var count = 0; count < unAllocatedAPIResponse.length; count++) {

            var item = unAllocatedAPIResponse[count];
            i=i+1;
            var ets = item.dateOfAppointment+(item.duration*60*1000);
            var st = new Date(item.dateOfAppointment);
            var et = new Date(ets);
            var strData = '';
            strData = kendo.toString(st, "h:mm tt") + " - " + kendo.toString(et, "h:mm tt") + "</br>";
            strData = strData + item.composition.patient.lastName + " " + item.composition.patient.firstName + " " + item.composition.patient.middleName + "</br>";
            strData = strData + item.composition.appointmentReason.desc;
            // strData = '<td style="font-size:10px;padding:0;">'+ strData +'</td>';

            strData = '<td style="font-size:10px;padding:0;">' +'<div style="color: red" class="tblSmContent" appointmentid="' + item.id + '" providerid="' + item.providerId + '"><a href="javascript:void(0);" class="cut-appointment" onclick="onClickCutAppointment(this)"></a><a href="javascript:void(0)" id="popupClose'+ i +'" class="popupClose_staff" onclick="onClickDeleteAppt('+item.id+',' + i + ')"></a><div class="draggable-item" style="margin-top: 18px;color: red" appointmentid="' + item.id + '" providerid="' + item.providerId + '">'+ strData +'</div></div></td>';
            strData1 = strData1 + strData;

        }
    }

    if(i > max.length || i < max.length){
        var len = max.length-i;
        for(var a=0;a<len;a++) {
            strData1 = strData1 +'<td class="connected-sortable" style="font-size:10px;padding:0;"><a href="javascript:void(0);" onclick="onClickPasteAppointment(this)"><img class="paste-appointment" ></td>';
        }
    }

    strData1 = strData1 + '<td class="connected-sortable" style="font-size:10px;padding:0;"><a href="javascript:void(0);" onclick="onClickPasteAppointment(this)"><img class="paste-appointment" ></td>';

    return strData1;
}


function onDeleteAppt(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            var msg = "Appointment deleted successfully";
            displaySessionErrorPopUp("Info", msg, function(res) {
                // onClickCancel();
                // onClickSubmit();
                // $("#divPatients").css("display", "none");
				$("#btnSTPRView").click();

            })
        }else{
            customAlert.error("Error",dataObj.response.status.message);
        }
    }else{
        customAlert.error("Error",dataObj.response.status.message);
    }
}

function onClickDeleteAppt(id,counter) {
	if(IsPreviousDay == 0) {
		customAlert.confirm("Confirm", "Are you sure want to delete appointment?", function (response) {
			if (response.button == "Yes") {
				// $(".dragPhoto").find('.docThumb'+counter).remove();
				// var popupId = "#popupClose"+counter;
				// $(popupId).css("display","none");
				var response = _.where(providerSArray2, {id: id})
				var appObj = {};
				appObj = response[0];
				// appObj.id = id;
				appObj.isActive = 0;
				appObj.isDeleted = 1;

				delete appObj.composition;
				delete appObj.createdDate;
				delete appObj.appointmentStartDate;
				delete appObj.appointmentEndDate;
				delete appObj.handoverNotes;
				delete appObj.inTime;
				delete appObj.isSynced;
				delete appObj.modifiedDate;
				delete appObj.notes;
				delete appObj.outTime;
				delete appObj.readBy;
				delete appObj.syncTime;
				delete appObj.timeSlots;
				appObj.modifiedBy = Number(sessionStorage.userId);

				var dataUrl = ipAddress + "/appointment/update";
				var arr = [];
				arr.push(appObj);
				// console.log(arr);
				createAjaxObject(dataUrl, arr, "POST", onDeleteAppt, onErrorMedication);
			}
		});
	}
	else {
		customAlert.error("Error","Appointment can not be deleted past days");
	}

}



function onCreateAppt(id, counter, e, prViewDateArrayId) {

	if(IsPreviousDay == 0) {

		var droppableProvider = $(e).parent().parent().children(':first-child');

		var dproviderid = parseInt($(droppableProvider).attr("providerid"));

		if (typeof dproviderid !== typeof undefined && dproviderid !== false) {
			dproviderid = parseInt(dproviderid);
		}

		var selObj = {};
		if( isNaN(dproviderid) || dproviderid == null){
			dproviderid = 0;
		}

		selObj.providerId = dproviderid;
		selObj.facilityId = Number($("#txtPRFacility2 option:selected").val());
		selObj.facilityName = $("#txtPRFacility2 option:selected").text();
		if (prViewArray != null && prViewArray.length > 0) {
			var objProvider = _.where(prViewArray, {id: dproviderid});
			if (objProvider != null && objProvider.length > 0) {
				selObj.pLastName = objProvider[0].lastName;
				selObj.pFirstName = objProvider[0].firstName;
			}
		}


		var txtPRDate2 = $("#txtPRDate2").data("kendoDatePicker");
		if (txtPRDate2.value() != "") {
			var selDate = new Date(txtPRDate2.value());
			var index = $(e).parent().index();
			var $appt = $("#prscheduler4 table thead th:eq(" + index + ")");
			// console.log($appt.text());

			var prTempViewDateArray = _.where(prViewDateArray, {idk: parseInt(prViewDateArrayId)});

			if (prTempViewDateArray != null && prTempViewDateArray.length > 0) {
				selDate.setHours(parseInt(prTempViewDateArray[0].h));
				selDate.setMinutes(parseInt(prTempViewDateArray[0].m));
				selObj.dateOfAppointment = selDate.getTime();
			}
		}
		parentRef.selObj = selObj;
		//devModelWindowWrapper.openPageWindow("../../html/masters/createViewAppointment.html", profileLbl, popW, popH, true, onClickViewProviderApp);
		//   parent.setPopupWIndowHeaderColor("0bc56f", "FFF");


		var popW = "60%";
		var popH = 500;

		var profileLbl;
		var devModelWindowWrapper = new kendoWindowWrapper();
		profileLbl = "Search Service User";
		if (sessionStorage.clientTypeId == "2") {
			profileLbl = "Search Service User";
		}
		parentRef.facilityId = $("#txtPRFacility2").val();
		devModelWindowWrapper.openPageWindow("../../html/patients/searchPatient.html", profileLbl, popW, popH, true, onCloseCreateApptPopUp);

		//$("#CreateApptPopUp").modal("show");
	}
	else{
		customAlert.error("Error","Appointment can not be created to past days");
	}
}


function onCloseCreateApptPopUp(evt, returnData) {



    debugger;
    if (returnData && returnData.status == "success") {


        patientId = returnData.selItem.PID;
        patientReturnItem = returnData;

        selectPatientId = returnData.selItem.PID;

        getAjaxObject(ipAddress + "/carehome/patient-billing/?is-active=1&patientId=" + selectPatientId + "&fields=*,shift.*,travelMode.*,billingType.*,billingPeriod.*,billTo.name,billingRateType.code", "GET", getPatientBillingTypes, onError);


        $('#txtSUPopUp').val(returnData.selItem.LN + " " + returnData.selItem.FN + " " + returnData.selItem.MN);

        if (parentRef != null && parentRef.selObj != null) {
            if (parentRef.selObj.facilityName != null && parentRef.selObj.facilityName != "") {
                $("#txtFacilityPopup").val(parentRef.selObj.facilityName);
            }

            if (parentRef.selObj.pLastName != null && parentRef.selObj.pLastName != "" && parentRef.selObj.pFirstName != null && parentRef.selObj.pFirstName != "") {
                $("#txtStaffPopUp").val(parentRef.selObj.pLastName + " " + parentRef.selObj.pFirstName);
            }
        }



        $("#txtStartTimePopUp").kendoDateTimePicker({
            change: startChange,
            format: appointmentdtFMT, interval: 15
        });
        $("#txtEndTimePopUp").kendoDateTimePicker({ format: appointmentdtFMT, interval: 15 });

        var txtStartTimePopUp = $("#txtStartTimePopUp").data("kendoDateTimePicker");
        if (txtStartTimePopUp) {
            txtStartTimePopUp.value(GetDateTimeEdit(parentRef.selObj.dateOfAppointment));
        }
        txtStartTimePopUp.readonly();
        var endappoinment = (parentRef.selObj.dateOfAppointment + (15 * 60000));
        var tDate = new Date(endappoinment);
        var minDate = new Date(tDate.getFullYear(), tDate.getMonth(), tDate.getDate());
        var maxDate = new Date(tDate.getFullYear(), tDate.getMonth(), tDate.getDate(), 23, 59, 0);

        $("#txtEndTimePopUp").kendoDateTimePicker(
            {
                format: appointmentdtFMT,
                interval: 15,
                min: minDate,
                max: maxDate
            })
        var txtEndTimePopUp = $("#txtEndTimePopUp").data("kendoDateTimePicker");
        if (txtEndTimePopUp) {
            txtEndTimePopUp.value(GetDateTimeEdit(endappoinment));
        }

        $("#CreateApptPopUp").modal("show");
    }
}


function getPatientBillingTypes(dataObj) {
    patientBillArray = [];

    var tempCompType = [];
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.patientBilling) {
            if ($.isArray(dataObj.response.patientBilling)) {
                tempCompType = dataObj.response.patientBilling;
            } else {
                tempCompType.push(dataObj.response.patientBilling);
            }
        }
    }
    for (var q = 0; q < tempCompType.length; q++) {
        var qItem = tempCompType[q];
        if (qItem) {
            qItem.value = qItem.billToname;
            qItem.desc = qItem.billToname;
            patientBillArray.push(qItem);
        }
    }
    setDataForSelection(patientBillArray, "cmbBillTypePopUp", onPatientBillingTypeChange, ["desc", "value"], 0, "");

    $("#cmbBillTypePopUp").removeAttr("disabled");
}



function onClickAppointment(appointmentId) {



    parentRef.selectedItems = [
        {
            id: appointmentId
        }
    ];



    var popW = "50%";
    var popH = "60%";
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Edit Appointment";

    devModelWindowWrapper.openPageWindow("../../html/masters/editStaffAppointment.html", profileLbl, popW, popH, true, onCloseEditAppointment);
    parent.setPopupWIndowHeaderColor("0bc56f", "FFF");

}


function onClickSaveAppointmentPopUp() {
    var IsValid = true;
    var errormessage = "";
    if ($("#txtHourlyRatePopUp").val() === "") {
        IsValid = false;
        errormessage = "Please enter hourly rate.";
    }
    /*if (IsValid) {
        if ($("#txtNotesPopUp").val() === "") {
            IsValid = false;
            errormessage = "Please enter Notes.";
        }
    }*/


    if (IsValid) {
        var arr = [];
        var obj = {};

        if (parentRef.selObj != null) {

            var txtEndTimePopUp = $("#txtEndTimePopUp").data("kendoDateTimePicker");

            var appointmentsArr = parentRef.appointmentsArr;

            var isExist = false;

            if (appointmentsArr && appointmentsArr.length > 0) {
				if (parentRef.selObj.providerId != null && parentRef.selObj.providerId != 0) {
					obj.providerId = parentRef.selObj.providerId;

					var dStaffAppointments = _.where(appointmentsArr, {providerId: parentRef.selObj.providerId});

					if (dStaffAppointments != null && dStaffAppointments.length > 0) {
						for (var m = 0; m < dStaffAppointments.length; m++) {
							var newApptSD = parentRef.selObj.dateOfAppointment;
							var eDate = new Date(txtEndTimePopUp.value());
							var newApptED = eDate.getTime();

							var dAppSD = dStaffAppointments[m].dateOfAppointment;
							var dAppED = dStaffAppointments[m].dateOfAppointment + (dStaffAppointments[m].duration * 60 * 1000);


							//if (((dAppSD >= newApptSD && dAppSD < newApptED) || (dAppED >= newApptSD && dAppED < newApptED)) || (newApptSD < dAppSD && newApptED > dAppED)) {
							//    isExist = true;
							//    break;
							//}

							if (((newApptSD >= dAppSD && newApptSD < dAppED) || (newApptED >= dAppSD && newApptED < dAppED)) || (newApptSD < dAppSD && newApptED > dAppED)) {
								isExist = true;
								break;
							}

						}
					}
				}
				else{
					obj.providerId = 0;
				}
			}

            if (!isExist) {
                obj.facilityId = parentRef.selObj.facilityId;
                obj.createdBy = Number(sessionStorage.userId);
                obj.isActive = 1;
                obj.isDeleted = 0;

                var txtStartTimePopUp = $("#txtStartTimePopUp").data("kendoDateTimePicker");
                var sDate = new Date(txtStartTimePopUp.value());
                obj.dateOfAppointment = sDate.getTime();

                txtEndTimePopUp = $("#txtEndTimePopUp").data("kendoDateTimePicker");
                eDate = new Date(txtEndTimePopUp.value());
                if (!(sDate.getTime() > eDate.getTime())) {
                    var diff = eDate.getTime() - sDate.getTime();
                    diff = diff / 1000;
                    diff = diff / 60;
                    obj.duration = Math.round(diff);
                    obj.patientId = selectPatientId;

                    obj.notes = $("#txtNotesPopUp").val();
                    obj.billToId = $("#cmbBillTypePopUp").val();

                    obj.appointmentType = $("#cmbStatusPopUp").val();
                    obj.notes = "";
                    var pBillItem = getPatientBillingDetails(obj.billToId);
                    obj.shiftValue = pBillItem.shiftValue;
                    obj.billingRateType = pBillItem.billingRateTypeCode;
                    obj.patientBillingRate = pBillItem.rate;
                    obj.billingRateTypeId = pBillItem.billingRateTypeId;
                    obj.swiftBillingId = pBillItem.swiftBillingId;
                    obj.payoutHourlyRate = Number($("#txtHourlyRatePopUp").val());
                    obj.billToName = pBillItem.billToname;
                    obj.billToId = pBillItem.billToId;

                    obj.appointmentReason = $("#cmbReasonPopUp").val();


                    arr.push(obj);

                    var dataUrl = ipAddress + "/appointment/create";
                    createAjaxObject(dataUrl, arr, "POST", onCreateAppointment, onError);
                    // console.log("Create Appointment");
                    // console.log(arr);
                }
                else {
                    customAlert.info("Info", "Start date should be less than end date.");
                }
            }
            else {
                customAlert.info("Error", "This slot is occupied, do you want to overlap an appointment?");
            }
        }
    }
    else {
        customAlert.info("Info", errormessage);
    }
}

function onCreateAppointment(dataObj) {

    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            var msg = "Appointment created successfully";
            displaySessionErrorPopUp("Info", msg, function (res) {
                $("#btnClosePopUp").trigger("click");
                $("#btnPRView2").trigger("click");
            })
        } else {
            customAlert.error("Error", dataObj.response.status.message);
        }
    } else {
        customAlert.error("Error", dataObj.response.status.message);
    }

}

function onCloseEditAppointment(dataObj) {
	onClickStaffWeeklyView();
}

function onClickCancelSaveAppt() {
    $("#btnClosePopUp").trigger("click");
}

var sortByDateAsc = function (x, y) {
    // This is a comparison function that will result in dates being sorted in
    // ASCENDING order. As you can see, JavaScript's native comparison operators
    // can be used to compare dates. This was news to me.

    date1 = new Date(x.dateOfAppointment);
    date2 = new Date(y.dateOfAppointment);

    if (date1 > date2) return 1;
    if (date1 < date2) return -1;
    return 0;
};


function getFromDate(fromDate) {


    var day = fromDate.getDate();
    var month = fromDate.getMonth();
    month = month + 1;
    var year = fromDate.getFullYear();

    var strDate = month + "/" + day + "/" + year;
    strDate = strDate + " 00:00:00";

    var date = new Date(strDate);
    var strDateTime = date.getTime();

    return strDateTime;
}

function getToDate(toDate) {


    var day = toDate.getDate();
    var month = toDate.getMonth();
    month = month + 1;
    var year = toDate.getFullYear();

    var strDate = month + "/" + day + "/" + year;
    strDate = strDate + " 23:59:59";

    var date = new Date(strDate);
    var strDateTime = date.getTime();

    return strDateTime;
}


function onClickServiceUser(){
	var popW = 800;
	var popH = 450;

	if(parentRef)
		parentRef.searchZip = true;


	var profileLbl;
	var devModelWindowWrapper = new kendoWindowWrapper();
	profileLbl = "Service User";
	parentRef.facilityId = $("#txtStaffWeeklyViewFacility").val();
	devModelWindowWrapper.openPageWindow("../../html/patients/searchPatient.html", profileLbl, popW, popH, true, onCloseServiceUserAction);
}
var selPatientId;
function onCloseServiceUserAction(evt,returnData){
	if(returnData && returnData.status == "success"){
		selPatientId = returnData.selItem.PID;
		$('#txtServiceUser').val(returnData.selItem.LN + " "+ returnData.selItem.FN);
	}
}

function onGetPreviousHistory() {
	var endDate = new Date();
	var today = new Date();

	var startDate = addMonths(today, -6);

	if (selectPatients.patients[0]) {
		patientId = selectPatients.patients[0].id;
	}

	startDate.setHours(0, 0, 0, 0);
	var stDateTime = startDate.getTime();
	endDate.setHours(23, 59, 59, 0);
	var edDateTime = endDate.getTime();


	var patientListURL = ipAddress + "/appointment/list/?patient-id=" + patientId + "&to-date=" + edDateTime + "&from-date=" + stDateTime + "&is-active=1";
	getAjaxObject(patientListURL, "GET", onApptList, onError);
}

var distProviderIds = [];
var distPastProviderIds = [];
function onApptList(dataObj){
	var dtArray = [];
	distPastProviderIds = [];
	distProviderIds = [];
	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		if(dataObj.response.appointment){
			if($.isArray(dataObj.response.appointment)){
				dtArray = dataObj.response.appointment;
			}else{
				dtArray.push(dataObj.response.appointment);
			}
		}
	}

	if (dtArray !== null && dtArray.length > 0) {
		var lookup = {};

		var distProviderIds = [];

		for (var item, i = 0; item = dtArray[i++];) {
			var providerId = item.providerId;

			if (!(providerId in lookup)) {
				lookup[providerId] = 1;
				if (providerId > 0) {
					distProviderIds.push(providerId);
					var response = _.where(distCurrentProviderIds, {id: providerId});
					if(response && response.length > 0)
						distPastProviderIds.push(response[0]);
				}


			}


		}
	}

		buildProvidersListGrid(distPastProviderIds);


	//
	//
	//
	//
	// 	if(parentRef && parentRef.prViewArray){
	//
	//
	// 		lookup = {};
	//
	// 		var providerIds = [];
	//
	// 		for (var item, i = 0; item = parentRef.prViewArray[i++];) {
	// 			var providerId = item.id;
	//
	// 			if (!(providerId in lookup)) {
	// 				lookup[providerId] = 1;
	// 				if(providerId > 0)
	// 					providerIds.push(providerId);
	// 			}
	// 		}
	//
	// 		var staffWithNoAppts = [];
	// 		$.each(dtArray,function(index,e){
	//
	// 			if(providerIds !==null && providerIds.length > 0){
	// 				if(providerIds.indexOf(e.providerId) === -1){
	// 					if(staffWithNoAppts.findIndex((s) => e.providerId) === -1)
	// 						if(e.providerId > 0)
	// 							staffWithNoAppts.push(e.providerId);
	// 				}
	// 			}
	// 		});
	//
	// 		if(distProviderIds !== null && distProviderIds.length > 0 && staffWithNoAppts !== null && staffWithNoAppts.length > 0){
	// 			commonStaff = distProviderIds.filter(value => staffWithNoAppts.includes(value));
	//
	// 			if (commonStaff !== null && commonStaff.length > 0) {
	// 				var dArray = parentRef.prViewArray.filter(function (u) {
	// 					return commonStaff.indexOf(u.id) > 0;
	// 				});
	//
	// 				if (providerList !== null && providerList.length > 0) {
	// 					var dArr = providerList.filter(function (e) {
	// 						return commonStaff.indexOf(e.id) > -1;
	// 					})
	//
	//
	// 					for (var i = 0; i < dArr.length; i++) {
	// 						if (dArr[i].isActive == 1) {
	// 							var obj = dArr[i];
	// 							obj.idk = dArr[i].id;
	// 						}
	// 					}
	//
	//
	// 				}
	// 			}
	// 		}
	// 	}
	// }
}

function onSelectServiceUser(selectId){
	buildProvidersListGrid([]);
	if(selectId == 2){
		if(selectPatients.patients[0].carerGender != null) {
			var compareArray = _.where(distCompareProviderIds, {gender: selectPatients.patients[0].carerGender});
			buildProvidersListGrid(compareArray);
		}
		else{
			buildProvidersListGrid(distCompareProviderIds);
		}
	}else{
		if(selectPatients.patients[0].carerLanguage != null) {
			var compareArray = _.where(distCompareProviderIds, {language: selectPatients.patients[0].carerLanguage});
			buildProvidersListGrid(compareArray);
		}
		else{
			buildProvidersListGrid(distCompareProviderIds);
		}
	}

}

function addMonths(date, months) {
	var newdate = date;
	newdate.setMonth(newdate.getMonth() + months);
	return newdate;
}

var angularUIgridWrapper;
var parentRef = null;
var recordType = "1";
var dataArray = [];
var ADD = "add";
var UPDATE = "edit";
var VIEW = "view";
var DELETE = "delete";
var operation = "add";
var atID = "";
var ds  = "";
var patientId = "";

var IsFlag = 1;
var relationdataArray = [];

var statusArr = [{Key:'Active',Value:'Active'},{Key:'InActive',Value:'InActive'}];

$(document).ready(function(){
    $("#divID").css("display","none");
    sessionStorage.setItem("IsSearchPanel", "1");
    parentRef = parent.frames['iframe'].window;
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("dgridBillTypeList", dataOptions);
    angularUIgridWrapper.init();
    buildDeviceListGrid([]);
});


$(window).load(function(){
    loading = false;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
    onGetBillType();
    init();
    buttonEvents();
    adjustHeight();

}

function init(){
    $("#btnEdit").prop("disabled", true);
    $("#btnDelete").prop("disabled", true);
    // $("#cmbStatus").kendoComboBox();
    $("#divTypeDetails").css("display","none");
    $("#viewDivBlock").css("display","");
    patientId = parentRef.patientId;//101
    buildDeviceListGrid([]);
    getAjaxObject(ipAddress+"/carehome/patient-billing/?is-active=1&patientId="+ patientId +"&fields=*,shift.*,travelMode.*,billingType.*,billingPeriod.*,billTo.name,billingRateType.code","GET",getPatientBilling,onError);
}

function getActivityTypes(dataObj){
    $('#cmbType')
        .find('option')
        .remove()
        .end();
    var tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.billingTypes){
            if($.isArray(dataObj.response.billingTypes)){
                tempCompType = dataObj.response.billingTypes;
            }else{
                tempCompType.push(dataObj.response.billingTypes);
            }
        }
    }

    if (tempCompType != null && tempCompType.length > 0) {
        tempCompType.sort(function (a, b) {
            var p1 = a.code;
            var p2 = b.code;

            if (p1 < p2) return -1;
            if (p1 > p2) return 1;
            return 0;
        });
    }
    $("#cmbType").append('<option value=""></option>');
    for(var i=0;i<tempCompType.length;i++){
        tempCompType[i].idk = tempCompType[i].id;
        $("#cmbType").append('<option value="'+tempCompType[i].id+'">'+tempCompType[i].code+'</option>');
    }
}

var tempCompTypeList = [];
function getPatientBilling(dataObj){
    console.log(dataObj);

    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.patientBilling){
            if($.isArray(dataObj.response.patientBilling)){
                tempCompTypeList = dataObj.response.patientBilling;
            }else{
                tempCompTypeList.push(dataObj.response.patientBilling);
            }
        }
    }

    for(var i=0;i<tempCompTypeList.length;i++){
        tempCompTypeList[i].idk = tempCompTypeList[i].id;
        if(tempCompTypeList[i].percentageCovered != null) {
            tempCompTypeList[i].percentageCovered = get2D(tempCompTypeList[i].percentageCovered);
        }
        if(tempCompTypeList[i].rate != null){
            tempCompTypeList[i].rate = get2D(tempCompTypeList[i].rate);
        }
        if(tempCompTypeList[i].totalHours != null){
            tempCompTypeList[i].totalHours = get2D(tempCompTypeList[i].totalHours);
        }

        if(tempCompTypeList[i].relationshipId != null) {
            tempCompTypeList[i].relationship = getTypeNameById(tempCompTypeList[i].relationshipId);
        }
        else{
            tempCompTypeList[i].relationship = "";
        }
    }


    buildDeviceListGrid(tempCompTypeList);
}

function getPatientBilling1(dataObj){
    console.log(dataObj);

    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.patientBilling){
            if($.isArray(dataObj.response.patientBilling)){
                tempCompTypeList = dataObj.response.patientBilling;
            }else{
                tempCompTypeList.push(dataObj.response.patientBilling);
            }
            billToDropdown();
        }
    }
}


function onError(errorObj){
    console.log(errorObj);
}
function getFileResourceList(dataObj){
    console.log(dataObj);
    dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.fileResource){
        if($.isArray(dataObj.response.fileResource)){
            dataArray = dataObj.response.fileResource;
        }else{
            dataArray.push(dataObj.response.fileResource);
        }
    }
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        if(!dataArray[i].fileType){
            dataArray[i].fileType = "";
        }
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
        }
    }
    //getDietList();
    buildDeviceListGrid([]);
    setTimeout(function(){
        buildDeviceListGrid(dataArray);
    })
}

function buttonEvents(){

    $("#btnSave").off("click",onClickSave);
    $("#btnSave").on("click",onClickSave);

    $("#btnEdit").off("click",onClickEdit);
    $("#btnEdit").on("click",onClickEdit);

    $("#btnDelete").off("click",onClickDelete);
    $("#btnDelete").on("click",onClickDelete);

    $("#btnAdd").off("click");
    $("#btnAdd").on("click",onClickAdd);

    $("#btnCancel").off("click",onClickCancel);
    $("#btnCancel").on("click",onClickCancel);

    $("#btnReset").off("click",onClickReset);
    $("#btnReset").on("click",onClickReset);

    $(".btnActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('active');
        onClickActive();
    });
    $(".btnInActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('inactive');
        onClickInActive();
    });

    $("#cmbBillTo").change(function() {
        var billToId = $("#cmbBillTo").val();
       var selList = _.filter(billCompType,
            function(e){
                return e.id == Number(billToId);
            });
       if(selList && selList.length > 0){
           if(selList[0].department != null && selList[0].department != ""){
               $("#txtDepartment").val(selList[0].department);

           }
           else{
               $("#txtDepartment").val("");
           }

       }
    });

    allowDecimals("txtTotalAmount");
    allowNumerics("txtSwiftId");
    allowDecimals("txtHourlyRate");
    allowDecimals("txtPercentage");
}

function searchOnLoad(status) {
    buildDeviceListGrid([]);
    if(status == "active") {
        var urlExtn = ipAddress + "/carehome/patient-billing/?is-active=1&patientId="+ patientId +"&fields=*,shift.*,travelMode.*,billingType.*,billingPeriod.*,billTo.name,billingRateType.code";
    }
    else if(status == "inactive") {
        var urlExtn = ipAddress + "/carehome/patient-billing/?is-active=0&patientId="+ patientId +"&fields=*,shift.*,travelMode.*,billingType.*,billingPeriod.*,billTo.name,billingRateType.code";
    }
    getAjaxObject(urlExtn,"GET",getPatientBilling,onError);
}


function onClickAdd(){
    $("#divID").css("display","none");
    $("#billTitle").html("Add Service User Billing");
    $("#txtID").hide();
    parentRef.operation = "add";
    $("#divTypeDetails").css("display","");
    $("#viewDivBlock").css("display","none");
    // onGetBillType();
    operation = ADD;
    onClickReset();
    onGetBillType();

}

function addReportMaster(opr){
    var popW = 500;
    var popH = "43%";
    //$('.listDataWrapper').hide();
    //$('.addOrRemoveWrapper').show();
    $('.alert').remove();
    var profileLbl = "Add Task Type";
    parentRef.operation = opr;
    operation = opr;
    if(opr == "add"){
        $('#btnSave').html('<span><img src="../../img/medication/Save.png" class="providerLink-btn-img" />Save</span>');
        $('.tabContentTitle').text('Add Task Type');
        $('#btnReset').show();
        //var billActName = $("#txtBAN").val();
        $('#btnReset').trigger('click');
        //$("#txtBAN").val(billActName);
    }else{

        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if (selectedItems && selectedItems.length > 0) {
            var obj = selectedItems[0];
            parentRef.id = obj.idk;
            parentRef.displayOrder  = obj.displayOrder;
            parentRef.type = obj.type;
            parentRef.DIO = obj.DIO;
            parentRef.activityGroup = obj.activityGroup;
            parentRef.isActive = obj.isActive;
            operation = UPDATE;
        }

        profileLbl = "Edit Task Type";
        $('.tabContentTitle').text('Edit Task Type');
        $('#btnReset').hide();
        $('#btnSave').html('<span><img src="../../img/medication/Save.png" class="providerLink-btn-img" />Update</span>');
        $('#btnReset').trigger('click');
    }
    var devModelWindowWrapper = new kendoWindowWrapper();
    devModelWindowWrapper.openPageWindow("../../html/masters/createactivityTypeList.html", profileLbl, popW, popH, true, closeaddReportMastertAction);
}
function closeaddReportMastertAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        var opr = returnData.operation;
        if(opr == "add"){
            customAlert.info("info", "Task Type created successfully");
        }else{
            customAlert.info("info", "Task Type updated successfully");
        }
        init();
    }
}

function onClickActive() {
    $(".btnInActive").removeClass("radioButton-active");
    $(".btnActive").addClass("radioButton-active");
}

function onClickInActive() {
    $(".btnActive").removeClass("radioButton-active");
    $(".btnInActive").addClass("radioButton-active");
}

function onClickDelete(){
    customAlert.confirm("Confirm", "Are you sure to delete?",function(response){
        if(response.button == "Yes"){
            var dataObj = {};
            dataObj.createdBy = Number(sessionStorage.userId);
            dataObj.isDeleted = 1;
            dataObj.isActive = 0;
            dataObj.id = atID;
            var dataUrl = ipAddress +"/carehome/patient-billing/?id="+ atID ;
            var method = "DELETE";
            createAjaxObject(dataUrl, dataObj, method, onCreateDelete, onError);
        }
    });
}

function onCreateDelete(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "Service User Billing Contact Deleted Successfully";
            customAlert.error("Info", msg);
            $("#txtAT").val("");
            operation = ADD;
            init();
        }
    }
}
function onClickSave(){

    // var strAbb = $("#txtAbbreviation").val();
    // strAbb = $.trim(strAbb);
    //
    // var strDescription = $("#txtDescription").val();
    // strDescription = $.trim(strDescription);

    var IsChecked = 0;

    if($("#chkGenerateBill").is(':checked')) {
        IsChecked = 1;
    }

    if($("#cmbType").val() != "" && $("#cmbBillTo").val() != "" && $("#txtHourlyRate").val() != "" && $("#cmbRelation").val() != "" && $("#cmbBillPeriod").val() != "" && $("#cmbMode").val != "" && $("#cmbBillShift").val != "" && $("#cmbRateType").val() != "") {
        var dataObj = {};

        dataObj.isDeleted = 0;
        dataObj.isActive = 1;
        dataObj.shiftId = parseInt($("#cmbBillShift").val());
        dataObj.travelModeId = parseInt($("#cmbMode").val());
        dataObj.patientId = patientId;

        dataObj.relationshipId = parseInt($("#cmbRelation").val());
        if( $("#txtPercentage").val() != ""){
            dataObj.percentageCovered = $("#txtPercentage").val();
        }

        dataObj.rate = $("#txtHourlyRate").val();

        if( $("#txtTotalAmount").val() != "") {
            dataObj.totalHours = $("#txtTotalAmount").val();
        }
        if( $("#txtSwiftId").val() != "") {
            dataObj.swiftBillingId = $("#txtSwiftId").val();
        }

        if( $("#txtNotes").val() != "") {
            dataObj.notes = $("#txtNotes").val();
        }

        dataObj.billingTypeId = parseInt($("#cmbType").val());
        dataObj.billToId = parseInt($("#cmbBillTo").val());
        dataObj.billingPeriodId = parseInt($("#cmbBillPeriod").val());
        dataObj.billingRateTypeId = parseInt($("#cmbRateType").val());
        dataObj.generateBillWithTimeSheets = IsChecked;
        var dataUrl = ipAddress + "/carehome/patient-billing/";
        var method = "POST";
        if (operation == UPDATE) {
            var selectedItems = angularUIgridWrapper.getSelectedRows();
            var createdBy;

            if (selectedItems && selectedItems.length > 0) {
                createdBy = selectedItems[0].createdBy;
            }
            method = "PUT";
            dataObj.id = atID;
            dataObj.createdBy = createdBy;
            dataObj.modifiedBy =  Number(sessionStorage.userId);


        }else{
            dataObj.createdBy = Number(sessionStorage.userId);
        }
        createAjaxObject(dataUrl, dataObj, method, onCreate, onError);
    }
    else{
        customAlert.error("Error","Please fill all required fields");
    }

}

function onCreate(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "Service User Billing Contact Created Successfully";
            $("#divID").css("display","none");
            if(operation == UPDATE){
                msg = "Service User Billing Contact Updated Successfully"
            }

            // customAlert.error("info", msg);
            displaySessionErrorPopUp("Info", msg, function(res) {
                // $("#txtAT").val("");
                operation = ADD;
                init();
            });
        }else{
            customAlert.error("Error", dataObj.response.status.message);
        }
    }else{

    }
    onClickReset();
}

function onClickCancel(){
    $("#divID").css("display","none");
    $("#divTypeDetails").css("display","none");
    $("#viewDivBlock").css("display","");
    $("#billTitle").html("View Service User Billing");

    $(".btnActive").trigger("click");
}

function adjustHeight(){
    var defHeight = 230;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    angularUIgridWrapper.adjustGridHeight(cmpHeight);
}

function buildDeviceListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Bill To",
        "field": "billToname"
    });
    gridColumns.push({
        "title": "Type",
        "width": "10%",
        "field": "billingTypeValue"
    });
    gridColumns.push({
        "title": "Relationship",
        "field": "relationship"
    });
    gridColumns.push({
        "title": "Category",
        "field": "shiftValue"
    });
    gridColumns.push({
        "title": "Rate Type",
        "field": "billingRateTypeCode"
    });
    gridColumns.push({
        "title": "Rate",
        "field": "rate"
    });


    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}

function onChange(){
    setTimeout(function() {
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if (selectedItems && selectedItems.length > 0) {
            var obj = selectedItems[0];
            atID = obj.idk;
            $("#btnEdit").prop("disabled", false);
            $("#btnDelete").prop("disabled", false);
        } else {
            $("#btnEdit").prop("disabled", true);
            $("#btnDelete").prop("disabled", true);
        }
    });
}

function onClickEdit(){

    $("#billTitle").html("Edit Service User Billing");
    $("#divID").css("display","");
    parentRef.operation = "edit";
    operation = "edit";
    $("#divTypeDetails").css("display","")
    $("#viewDivBlock").css("display","none")
    var selectedItems = angularUIgridWrapper.getSelectedRows();
    if (selectedItems && selectedItems.length > 0) {
        var obj = selectedItems[0];
        atID = obj.idk;
        $("#txtID").html("ID : " + obj.idk);
        $("#cmbStatus").val(obj.isActive);
        $("#cmbType").val(obj.billingTypeId);
        $("#cmbBillShift").val(obj.shiftId);
        $("#cmbMode").val(obj.travelModeId);
        $("#cmbRelation").val(obj.relationshipId);
        $("#txtPercentage").val(obj.percentageCovered);
        billToDropdown(obj.billToId);
        $("#cmbBillTo").val(obj.billToId);
        $("#txtHourlyRate").val(obj.rate);
        $("#txtTotalAmount").val(obj.totalHours);



        $("#cmbBillPeriod").val(obj.billingPeriodId);
        $("#txtSwiftId").val(obj.swiftBillingId);
        $("#cmbRateType").val(obj.billingRateTypeId);
        $("#txtNotes").val(obj.notes);
        if(obj.generateBillWithTimeSheets == 1){
            $("#chkGenerateBill").prop("checked",true);
        }
        else{
            $("#chkGenerateBill").prop("checked",false);
        }

    }
}

var operation = "add";
var selFileResourceDataItem = null;

function onStatusChange(){
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if(cmbStatus && cmbStatus.selectedIndex<0){
        cmbStatus.select(0);
    }
}

function onComboChange(cmbId) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb && cmb.selectedIndex < 0) {
        cmb.select(0);
    }
}

function onClickReset() {
    if(operation == ADD) {
        operation = ADD;
    }
    else {
        operation = UPDATE;
    }
    $("#txtID").val("");
    $("#txtHourlyRate").val("");
    $("#txtPercentage").val("");
    $("#txtTotalAmount").val("");
    $("#txtswiftid").val("");
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if(cmbStatus){
        cmbStatus.select(0);
        cmbStatus.enable(false);
    }
    $("#chkGenerateBill").prop("checked",false);
    $("#txtNotes").val("");
    $("#txtDepartment").val("");

}

function onGetBillType(){
    patientId = parentRef.patientId;//
    getAjaxObject(ipAddress+"/carehome/patient-billing/?is-active=1&is-deleted=0&patientId="+ patientId +"&fields=*,shift.*,travelMode.*,billingType.*,billingPeriod.*,billTo.name,billingRateType.code","GET",getPatientBilling1,onError);
    getAjaxObject(ipAddress+"/homecare/billing-types/?is-active=1&is-deleted=0&sort=code","GET",getActivityTypes,onError);
    getAjaxObject(ipAddress + "/master/relation/list/", "GET", getRelations, onError);
    getAjaxObject(ipAddress + "/carehome/bill-to/?is-active=1&is-deleted=0", "GET", getBillToData, onError);
    getAjaxObject(ipAddress + "/homecare/billing-periods/?is-active=1&is-deleted=0", "GET", getBillPeriodData, onError);
    getAjaxObject(ipAddress + "/homecare/shifts/?is-active=1&is-deleted=0", "GET", getShifts, onError);
    getAjaxObject(ipAddress + "/homecare/travel-modes/?is-active=1&is-deleted=0", "GET", getTravelModes, onError);
    getAjaxObject(ipAddress + "/homecare/billing-rate-types/?is-active=1&is-deleted=0", "GET", getRateTypes, onError);
}

function onBillTypeChange() {
    onComboChange("cmbType");
}

function onRelationChange() {
    onComboChange("cmbRelation");
}

function onBillToChange() {
    onComboChange("cmbBillTo");
}

function onShifts() {
    onComboChange("cmbBillShift");
}

function onModes() {
    onComboChange("cmbMode");
}

function getRelations(dataObj) {
    $('#cmbRelation')
        .find('option')
        .remove()
        .end();
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            relationdataArray = dataObj.response.codeTable || [];
        }
    }
    if (relationdataArray != null && relationdataArray.length > 0) {
        relationdataArray.sort(function (a, b) {
            var p1 = a.desc;
            var p2 = b.desc;

            if (p1 < p2) return -1;
            if (p1 > p2) return 1;
            return 0;
        });
    }
    $("#cmbRelation").append('<option value=""></option>');
    for(var i=0;i<relationdataArray.length;i++){
        $("#cmbRelation").append('<option value="'+relationdataArray[i].id+'">'+relationdataArray[i].desc+'</option>');
    }
}
var billCompType = [];

function getBillToData(dataObj) {
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.billTo) {
            if ($.isArray(dataObj.response.billTo)) {
                billCompType = dataObj.response.billTo;
            } else {
                billCompType.push(dataObj.response.billTo);
            }
            billToDropdown(0);
        }
    }
}
var billingList = [];
function billToDropdown(billtoid) {
    $('#cmbBillTo')
        .find('option')
        .remove()
        .end();
    var billCompTypeList = [];
    var obj={};

    for (var i = 0; i < tempCompTypeList.length; i++) {

        for (var j = 0; j < billCompType.length; j++) {
            if(billCompType[j].id == tempCompTypeList[i].billToId) {
                billingList.push(billCompType[j]);
            }


        }
    }
    if(operation == "edit"){
        billingList = _.filter(billingList,
            function(e){
                return e.id != billtoid;
            });
    }
    var val = _.difference(billCompType, billingList);


   /* $('#cmbBillTo')
        .find('option')
        .remove();*/
    if (val != null && val.length > 0) {
        val.sort(function (a, b) {
            var p1 = a.name;
            var p2 = b.name;

            if (p1 < p2) return -1;
            if (p1 > p2) return 1;
            return 0;
        });
    }
    $("#cmbBillTo").append('<option value=""></option>');

    for (var i = 0; i < val.length; i++) {
        $("#cmbBillTo").append('<option value="' + val[i].id + '">' + val[i].name + '</option>');
    }
}

function onEditBillTo(){
    $('#cmbBillTo')
        .find('option')
        .remove()
        .end();
    if (billingList != null && billingList.length > 0) {
        billingList.sort(function (a, b) {
            var p1 = a.name;
            var p2 = b.name;

            if (p1 < p2) return -1;
            if (p1 > p2) return 1;
            return 0;
        });
    }
    // $("#cmbBillTo").append('<option value=""></option>');
    for (var i = 0; i < billingList.length; i++) {
        $("#cmbBillTo").append('<option value="' + billingList[i].id + '">' + billingList[i].name + '</option>');
    }
}

    function getBillPeriodData(dataObj){
        $('#cmbBillPeriod')
            .find('option')
            .remove()
            .end();
    var tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.roles){
            if($.isArray(dataObj.response.roles)){
                tempCompType = dataObj.response.roles;
            }else{
                tempCompType.push(dataObj.response.roles);
            }
        }
    }

    if (tempCompType != null && tempCompType.length > 0) {
        tempCompType.sort(function (a, b) {
            var p1 = a.code;
            var p2 = b.code;

            if (p1 < p2) return -1;
            if (p1 > p2) return 1;
            return 0;
        });
    }

    $("#cmbBillPeriod").append('<option value=""></option>');
    for(var i=0;i<tempCompType.length;i++){
        $("#cmbBillPeriod").append('<option value="'+tempCompType[i].id+'">'+tempCompType[i].code+'</option>');
    }
}

function getShifts(dataObj){
    $('#cmbBillShift')
        .find('option')
        .remove()
        .end();
    var tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.shifts){
            if($.isArray(dataObj.response.shifts)){
                tempCompType = dataObj.response.shifts;
            }else{
                tempCompType.push(dataObj.response.shifts);
            }
        }
    }

    tempCompType.sort(function (a, b) {
        var p1 = a.code;
        var p2 = b.code;

        if (p1 < p2) return -1;
        if (p1 > p2) return 1;
        return 0;
    });

    $("#cmbBillShift").append('<option value=""></option>');
    for(var i=0;i<tempCompType.length;i++){
        $("#cmbBillShift").append('<option value="'+tempCompType[i].id+'">'+tempCompType[i].code+'</option>');
    }
}


function getTravelModes(dataObj){
    $('#cmbMode')
        .find('option')
        .remove()
        .end();
    var tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.travelmodes){
            if($.isArray(dataObj.response.travelmodes)){
                tempCompType = dataObj.response.travelmodes;
            }else{
                tempCompType.push(dataObj.response.travelmodes);
            }
        }
    }
    if (tempCompType != null && tempCompType.length > 0) {
        tempCompType.sort(function (a, b) {
            var p1 = a.code;
            var p2 = b.code;

            if (p1 < p2) return -1;
            if (p1 > p2) return 1;
            return 0;
        });
    }
    $("#cmbMode").append('<option value=""></option>');
    for(var i=0;i<tempCompType.length;i++){
        $("#cmbMode").append('<option value="'+tempCompType[i].id+'">'+tempCompType[i].code+'</option>');
    }
}

function getRateTypes(dataObj){
    $('#cmbRateType')
        .find('option')
        .remove()
        .end();
    var tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.billingRateTypes){
            if($.isArray(dataObj.response.billingRateTypes)){
                tempCompType = dataObj.response.billingRateTypes;
            }else{
                tempCompType.push(dataObj.response.billingRateTypes);
            }
        }
    }
    if (tempCompType != null && tempCompType.length > 0) {
        tempCompType.sort(function (a, b) {
            var p1 = a.code;
            var p2 = b.code;

            if (p1 < p2) return -1;
            if (p1 > p2) return 1;
            return 0;
        });
    }

    $("#cmbRateType").append('<option value=""></option>');
    for(var i=0;i<tempCompType.length;i++){
        $("#cmbRateType").append('<option value="'+tempCompType[i].id+'">'+tempCompType[i].code+'</option>');
    }
}

function getTypeNameById(aId){
    for(var i=0;i<relationdataArray.length;i++){
        var item = relationdataArray[i];
        if(item && item.id == aId){
            return relationdataArray[i].desc;
        }
    }
    return 0;
}

function setMaskTextBoxValue(txtId, strVal) {
    var txtHome = $("#" + txtId).data("kendoMaskedTextBox");
    if (txtHome) {
        txtHome.value(strVal);
    }
}

function getMaskTextBoxValue(txtId) {
    var str = "";
    var txtHome = $("#" + txtId).data("kendoMaskedTextBox");
    if (txtHome) {
        str = txtHome._oldValue.trim();
    }
    return str;
}

function allowDecimals(ctrlId){
    $("#"+ctrlId).keypress(function (e) {
        var eVal = e.currentTarget.value;
        if(e.which == 46){
            if(eVal && eVal.indexOf(".") !=-1){
                return false;
            }
        }
        try {
            if (eVal.split(".")[1].toString().length > 1)
                return false;
        } catch (e) {}
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 95 && e.which != 46) {
            return false;
        }
    });
}



function get2D( num ) {
    var value = Number(num);
    var res = value.toString().split('.');
    if(res.length > 1 && (res[1].length > 0 && res[1].length < 3)) {
        value = value.toFixed(2);
    }
    return value;
}


var angularUIgridWrapper = null;
var parentRef = null;
var operation = "";
var selItem = null;
var recordType = "500";
$(document).ready(function(){
	parentRef = parent.frames['iframe'].window;
});


$(window).load(function(){
	if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
	init();
});

function init(){
	var dataOptions = {
	        pagination: false,
	        paginationPageSize: 500,
			changeCallBack: onChange
	    }

		angularUIgridWrapper = new AngularUIGridWrapper("dgridCallList", dataOptions);
	    angularUIgridWrapper.init();

	    var dataArray = [];
	    buildDeviceTypeGrid(dataArray);
	    getRecords();
	    
	buttonEvents();
}
function buttonEvents(){
	$("#btnCancel").off("click",onClickCancel);
	$("#btnCancel").on("click",onClickCancel);
	
    $("#btnSave").off("click");
    $("#btnSave").on("click",onClickCall);
    
    $("#btnPatient").off("click");
    $("#btnPatient").on("click",onClickPatient);
    
    $("#btnNurse").off("click");
    $("#btnNurse").on("click",onClickNurse);
    
    $("#btnDoctor").off("click");
    $("#btnDoctor").on("click",onClickDoctor);
    
}
function onClickPatient(){
	recordType = "500";
	$("#btnPatient").addClass("selectButtonBarClass");
	$("#btnNurse").removeClass("selectButtonBarClass");
	$("#btnDoctor").removeClass("selectButtonBarClass");
	getRecords();
}
function onClickNurse(){
	recordType = "200";
	$("#btnPatient").removeClass("selectButtonBarClass");
	$("#btnNurse").addClass("selectButtonBarClass");
	$("#btnDoctor").removeClass("selectButtonBarClass");
	getRecords();
}
function onClickDoctor(){
	recordType = "100";
	$("#btnPatient").removeClass("selectButtonBarClass");
	$("#btnNurse").removeClass("selectButtonBarClass");
	$("#btnDoctor").addClass("selectButtonBarClass");
	getRecords();
}

function getRecords(){
	buildDeviceTypeGrid([]);
	 getAjaxObject(ipAddress+"/user/list/"+recordType,"GET",onGetUserListData,onError);
}
function onGetUserListData(dataObj){
	var userListArray = [];
	if(dataObj && dataObj.response && dataObj.response.status){
		if(dataObj.response.status.code == "1"){
			if($.isArray(dataObj.response.loginUser)){
				userListArray = dataObj.response.loginUser;
			}else{
				userListArray.push(dataObj.response.loginUser);
			}
			for(var i=0;i<userListArray.length;i++){
				var item = userListArray[i];
				item.idk = item.id;
			}
		}else{
			customAlert.info("Error", dataObj.response.status.message);
		}
	}
	buildDeviceTypeGrid(userListArray);
}
function buildDeviceTypeGrid(dataSource) {
    var gridColumns = [];
	var otoptions = {};
	otoptions.noUnselect = false;
	 gridColumns.push({
	        "title": "User ID",
	        "field": "idk",
	        "width":"25%"
		});
 gridColumns.push({
     "title": "User Name",
     "field": "userName",
     "width":"30%"
	});
 gridColumns.push({
     "title": "Full Name",
     "field": "fullName",
	});
 gridColumns.push({
     "title": "Status",
     "field": "status",
     "width":"20%"
	});
	angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
	adjustHeight();
}
var selectUser = null;
function onChange(){
	setTimeout(function(){
		var selRows = angularUIgridWrapper.getSelectedRows();
		if(selRows && selRows.length>0){
			var selItem = selRows[0];
			selectUser = selItem;
		}
	})
}
$(window).resize(adjustHeight);
function adjustHeight(){
	var defHeight = 150;
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 120;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    try{angularUIgridWrapper.adjustGridHeight(cmpHeight);}catch(e){};

}
function onClickCancel(){
	var obj = {};
	obj.status = "false";
	popupClose(obj);
}
function onClickCall(){
	var obj = {};
	obj.status = "success";
	obj.user = selectUser;
	popupClose(obj);
}
function onClickSearch(){
	var obj = {};
	obj.status = "search";
	popupClose(obj);
}
function onError(errObj){
	console.log(errObj);
	customAlert.error("Error","Unable to create UserMapping");
}
function popupClose(obj){
	var windowWrapper = new kendoWindowWrapper();
	windowWrapper.closePageWindow(obj);
}



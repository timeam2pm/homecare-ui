var parentRef = null;

var patientId = "";

$(document).ready(function(){
});


$(window).load(function(){
	loading = false;
	$(window).resize(adjustHeight);
onLoaded();
	if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
	parentRef = parent.frames['iframe'].window;
	init();
    buttonEvents();
	adjustHeight();
}

function init(){
	patientId = parentRef.patientId;
	//getAjaxObject(ipAddress+"/patient/reports/holter/"+patientId,"GET",onGetHolterReportsData,onError);
}
function onError(errorObj){
	console.log(errorObj);
}
var holterReportDataArray = [];
function onGetHolterReportsData(dataObj){
		$("#divHolterReports").text("");
		var dataArray = [];
		if(dataObj){
			if($.isArray(dataObj)){
				dataArray = dataObj;
			}else{
				dataArray.push(dataObj);
			}
		}
		holterReportDataArray = dataArray;
		var strTable = '<table class="table">';
		strTable = strTable+'<thead class="fillsHeader whiteColor">';
		strTable = strTable+'<tr>';
		//strTable = strTable+'<th>Patient ID</th>';
		strTable = strTable+'<th class="textAlign whiteColor" style="width:220px">Date Generated</th>';
		strTable = strTable+'<th class="textAlign whiteColor">File Name</th>';
		strTable = strTable+'<th class="textAlign whiteColor">File Type</th>';
		strTable = strTable+'</tr>';
		strTable = strTable+'</thead>';
		strTable = strTable+'<tbody>';
		for(var i=0;i<holterReportDataArray.length;i++){
			var dataItem = holterReportDataArray[i];
			if(dataItem){
				//console.log(dataItem);
				var className = getRowColors(i);
				strTable = strTable+'<tr class="'+className+'">';
				//strTable = strTable+'<td>'+dataItem.patientId+'</td>';
				strTable = strTable+'<td class="textAlign">'+kendo.toString(new Date(dataItem.createdDate),"MM-dd-yyyy h:mm:ss")+'</td>';
				strTable = strTable+'<td class="textAlign">'+dataItem.fileName+'</td>';
				var dietId = dataItem.id;
				if(dataItem.fileType == "mp4"){
					strTable = strTable+'<td style="width:200px;padding:0px" class="textAlign"><img id="'+dietId+'" src="../../img/AppImg/HosImages/video.png"  class="cusrsorStyle videoIcon" onClick="onClickGenerate(event)"></td>';
				}else if(dataItem.fileType == "pdf"){
					strTable = strTable+'<td style="width:200px;padding:0px" class="textAlign"><img  id="'+dietId+'" src="../../img/AppImg/HosImages/pdf.png"    class="cusrsorStyle pdfIcon" onClick="onClickHolterReport(event)"></td>';
				}
				strTable = strTable+'</tr>';
			}
		}
		strTable = strTable+'</tbody>';
		strTable = strTable+'</table>';
		$("#divHolterReports").append(strTable);
	}
function buttonEvents(){
	$("#btnCancelDet").off("click",onClickCancel);
	$("#btnCancelDet").on("click",onClickCancel);
	
	$("#lblMedReport").off("click",onClickMedicalReport);
	$("#lblMedReport").on("click",onClickMedicalReport);
	
	$("#lblPtActivity").off("click",onClickPatientActivity);
	$("#lblPtActivity").on("click",onClickPatientActivity);
	
	$("#lblDaikyFReport").off("click",onClickDailyReport);
	$("#lblDaikyFReport").on("click",onClickDailyReport);
	
	$("#lblRepoChart").off("click",onClickRepositionChart);
	$("#lblRepoChart").on("click",onClickRepositionChart);
	
	$("#lblTotApp").off("click",onClickTotApplication);
	$("#lblTotApp").on("click",onClickTotApplication);
	
	$("#lblGeoReport").off("click",onClickGeoReport);
	$("#lblGeoReport").on("click",onClickGeoReport);
	
}

function onClickMedicalReport(){
	openReportPopup("../../html/patients/medicationReport.html","Medication Report");
}
function onClickPatientActivity(){
	openReportPopup("../../html/reports/patientActivity.html","Patient Activity");
}
function onClickDailyReport(){
	openReportPopup("../../html/patients/dailyFluidReport.html","Daily Fluid Report");
}
function onClickRepositionChart(){
	openReportPopup("../../html/patients/repositionReport.html","Reposition Chart");
}
function onClickTotApplication(){
	openReportPopup("../../html/reports/topicalApplication.html","Topical Application");
}
function onClickGeoReport(){
	parentRef.screenType = "patients";
	openReportPopup("../../html/patients/geoReport.html","Patient Report");
}
function openReportPopup(path,title){
	var popW = "90%";
    var popH = "85%";

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = title;
    devModelWindowWrapper.openPageWindow(path, profileLbl, popW, popH, false, closeCallReport);
}

function closeCallReport(evt,returnData){
	
}

function adjustHeight(){
	var defHeight = 60;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    $("#divPtHolter").height(cmpHeight);
	//angularUIgridWrapper.adjustGridHeight(cmpHeight);
}


function onClickPatientCall(obj){
	var popW = "60%";
    var popH = 400;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Patient Call Chart";
    parentRef.selObj = obj;
    devModelWindowWrapper.openPageWindow("../../html/patients/callPatient.html", profileLbl, popW, popH, false, closeCallPatient);
}
function closeCallPatient(evt,returnData){
	if(returnData && returnData.status == "success"){
		customAlert.info("Save","Patient Call Chart Created Successfully");
		init();
	}
}
function onClickCancel(){
	var obj = {};
	var windowWrapper = new kendoWindowWrapper();
	windowWrapper.closePageWindow(obj);
}

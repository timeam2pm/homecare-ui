/// Speech Text

try {
    var SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition;
    var recognition = new SpeechRecognition();
}
catch(e) {
    console.error(e);
    $('.no-browser-support').show();
    $('.app').hide();
}

var notefirstName = $('#txtFN');
var notelasName = $('#txtLN');
var noteContent = '';


var parentRef = null;
var operation = "";
var billCommunitionType = "billing";
var contactCommunitionType = "contact";
var selItem = null;
var ADD = "add";
var UPDATE = "edit";
var VIEW = "view";
var DELETE = "delete";
var patientId = "";
var statusArr = [{ Key: 'Active', Value: 'Active' }, { Key: 'InActive', Value: 'InActive' }];
var dnrArr = [{ Key: '0', Value: 'No' },{ Key: '1', Value: 'Yes' }];
var typesArr = [{ Key: '100', Value: 'Doctor' }, { Key: '200', Value: 'Nurse' }, { Key: '201', Value: 'Caregiver' }];
var booleanArr = [{ Key: '', Value: '' }, { Key: '1', Value: 'Yes' }, { Key: '0', Value: 'No' }];
var glassesArr = [{ Key: '', Value: '' }, { Key: '1', Value: 'Glasses' }, { Key: '0', Value: 'Bi-focal' }, { Key: '0', Value: 'Contacts' }];
var communicationTypeArray = [{ Key: '202', Value: 'GP' }, { Key: '203', Value: 'Pharmacy' }, { Key: '204', Value: 'Nextof Kin' }, { Key: '205', Value: 'Emmergency' }, { Key: '206', Value: 'Carer' }, { Key: '207', Value: 'Occupational Therapist' }, { Key: '208', Value: 'Social Worker' }, { Key: '209', Value: 'Equipment Supplier' }];

var patientInfoObject = null;
var commId = "";
var relationUIGridWrapper = null;
var selectedItems;

var photoExt = "";
var $parentWindow = $(window.parent.document);
var $kwindow = $parentWindow.find('.k-widget.k-window');
var dataTabURL = "";
var $removeItem = "";
var $createItem = "";
var showTab = "patientInfoTab";
var saveorupdate = "";
var IsPostalCodeManual = sessionStorage.IsPostalCodeManual;
var IsPostalFlag = "0";
var cntry = sessionStorage.countryName;
var speechfacilityArry = [];

if (parseInt($kwindow[0].style.width) < 90) {
    // $kwindow.css('width', parseInt($kwindow[0].style.width) + 5 + '%');
    $kwindow.css('width', '78%');
    $kwindow.css('height', '77.5%');
}
$(document).ready(function() {
    parentRef = parent.frames['iframe'].window;
    var pnlHeight = window.innerHeight;
    var imgHeight = pnlHeight - 90;
    $("#divTop").height(imgHeight);
    $('#tabsUL .patient-medicate-tabLink').on('click', function() {
        $(this).closest('li').addClass('active').siblings('li').removeClass('active');
        $('.tab-content').hide();
        showTab = $(this).attr('data-patient-link');
        $('#' + showTab).show();
        $('.alert').remove();
        if (showTab == "illnessTab" || showTab == "allergiesTab" || showTab == "dietTab" || showTab == "billingTab" || showTab == "contactsTab") {
            $('.main-btn-wrapper').hide();
            $('#' + showTab).find('.patient-btn-wrapper').show();
        } else {
            if (showTab == "additionalInfoTab") {
                $('.patient-btn-wrapper').hide();
                $('.additional-btn-wrapper').show();
            } else if (showTab == "patientInfoTab") {
                $('.patient-btn-wrapper').hide();
                $('.main-btn-wrapper').show();
            }
        }
        if (showTab == "additionalInfoTab") {
            dataTabURL = ipAddress + '/homecare/patient-ext/?patientId='+patientId;
            if (!$('#additionalInfoTab').attr('data-processed') == true) {
                $('#additionalInfoTab').attr('data-processed', true);
                getAjaxObject(dataTabURL, "GET", getAdditionalData, onError);
            }
        }
        if (showTab == "illnessTab") {
            dataTabURL = ipAddress + "/homecare/patient-medical-conditions/";
            if ($('.illnessTabWrapper').find('table tr').length == 1) {
                $('.illnessTabWrapper').attr('data-processed', true);
                getAjaxObject(dataTabURL + '?is-active=1&fields=*,medicalConditionType.*&patientId='+patientId, "GET", getIllnessData, onError);
            }
        } else if (showTab == "allergiesTab") {
            dataTabURL = ipAddress + "/homecare/patient-allergies/";
            if ($('.allergiesTabWrapper').find('table tr').length == 1) {
                $('.allergiesTabWrapper').attr('data-processed', true);
                getAjaxObject(dataTabURL + '?is-active=1&fields=*,allergyType.*&patientId='+patientId, "GET", getAllergiesData, onError);
            }
        } else if (showTab == "dietTab") {
            dataTabURL = ipAddress + "/homecare/patient-diets/";
            if ($('.dietTabWrapper').find('table tr').length == 1) {
                $('.dietTabWrapper').attr('data-processed', true);
                getAjaxObject(dataTabURL + '?is-active=1&fields=*,dietType.*&patientId='+patientId, "GET", getDietsData, onError);
            }
        } else if (showTab == "billingTab" || showTab == "contactsTab") {
            dataTabURL = ipAddress + "/homecare/patient-relationships/";
            getCountryIsPostalCode();
            if (showTab == "billingTab") {
                if ($('.billingTabWrapper').find('table tr').length >= 1) {
                    $('.billingTabWrapper').attr('data-processed', true);
                    // getAjaxObject(ipAddress + '/homecare/communication-types/?fields=id,value&id=:bt:201,202', "GET", getcommunications, onError);
                    getAjaxObject(ipAddress + '/homecare/communication-types/?fields=id,value&id=:bt:201,220', "GET", getcommunications, onError);
                    getAjaxObject(ipAddress + '/master/relation/list/', "GET", getRelations, onError);
                    // getAjaxObject(dataTabURL + '?is-active=1&communication-type-id=:in:201&patient-Id='+patientId+"&fields=*", "GET", getPatientBillingData, onError);
                    getAjaxObject(dataTabURL + '?is-active=1&contactType='+ billCommunitionType +'&patient-Id='+patientId+"&fields=*", "GET", getPatientBillingData, onError);
                }
            } else {
                if ($('.contactsTabWrapper').find('table tr').length >= 1) {
                    $('.contactsTabWrapper').attr('data-processed', true);
                    getAjaxObject(ipAddress + '/homecare/communication-types/?fields=id,value&id=:bt:203,220', "GET", getcommunications, onError);
                    getAjaxObject(ipAddress + '/master/relation/list/', "GET", getRelations, onError);
                    // getAjaxObject(dataTabURL + '?is-active=1&communicationTypeId=:bt:202,220&patientId='+patientId, "GET", getPatientContactsData, onError);
                    getAjaxObject(dataTabURL + '?is-active=1&contactType='+ contactCommunitionType +'&patientId='+patientId, "GET", getPatientContactsData, onError);
                }
            }
        }
        return true;
    });
    $('#note-record-btn').on('click', function(e) {
        console.log("Speech");
        recognition.stop();
        if (noteContent.length) {
            noteContent += ' ';
        }
        // Flag = 1;
        // $('#start-record-btn').removeClass("imgBorder");
        $('#note-record-btn').addClass("imgBorder");
        recognition.start();
    });

    // var dataOptions = {
    //     pagination: false,
    //     changeCallBack: onChange
    // }
    /*relationUIGridWrapper = new AngularUIGridWrapper("relationsWrapper", dataOptions);
    relationUIGridWrapper.init();
    buildRelationListGrid([]);
    if (patientId != "") {
        getAjaxObject(ipAddress + '/user/by-patient/' + patientId, "GET", getRelationListData, onError);
    }*/

    getCountryZoneName();
});

function onChange() {
    setTimeout(function() {
        selectedItems = relationUIGridWrapper.getSelectedRows();
        console.log(selectedItems);
    });
}
var relationshipArrayData = [];
var communicationsArrayData = [];
function getRelations(resp) {
    var dataArray = [];
    if(resp && resp.response && resp.response.codeTable){
        if($.isArray(resp.response.codeTable)){
            dataArray = resp.response.codeTable;
        }else{
            dataArray.push(resp.response.codeTable);
        }
    }
    if(dataArray.length) {
        relationshipArrayData = dataArray;
    }
}
function appendRelationshipsData(element) {
    var $elem = "";
    if(showTab == "billingTab") {
        $elem = element.find('[name="billingWrap-relationship"]');
    } else if(showTab == "contactsTab") {
        $elem = element.find('[name="contactsWrap-relationship"]');
    }
    if($elem) {
        $elem.empty();
        $elem.append('<option selected="true" disabled="disabled">Please Select</option>');
        for(var i=0;i<relationshipArrayData.length;i++) {
            $elem.append('<option value="'+relationshipArrayData[i].value+'">'+relationshipArrayData[i].desc+'</option>');
        }
        // $elem.each(function() {
            var selectedValue = $elem.attr('data-attr-selected-relation');
            if(selectedValue && selectedValue != "null") {
                $elem.val(selectedValue);
            }
        // });
    }
}

function getcommunications(resp) {
    console.log(resp);
    var dataArray = [];
    communicationsArrayData = [];
    if(resp && resp.response && resp.response.communicationTypes){
        if($.isArray(resp.response.communicationTypes)){
            dataArray = resp.response.communicationTypes;
        }else{
            dataArray.push(resp.response.communicationTypes);
        }
    }
    if(dataArray.length) {
        communicationsArrayData = dataArray;
    }
}
function appendcommunicationsData(element) {
    //communicationsArrayData = communicationTypeArray;
    // communicationsArrayData = [];
    var $elem = element.find('[name="contactsWrap-type"]');
    $elem.empty();
    $elem.append('<option selected="true" disabled="disabled">Please Select</option>');
    for(var i=0;i<communicationsArrayData.length;i++){
        $elem.append('<option value="'+communicationsArrayData[i].id+'">'+communicationsArrayData[i].value+'</option>');
    }
    // $elem.each(function() {
        var selectedValue = $elem.attr('data-attr-selected-communication_type');
        if(selectedValue && selectedValue != "null") {
            $elem.val(selectedValue);
        }
    // });
}

function billappendcommunicationsData(element) {
    //communicationsArrayData = communicationTypeArray;
    var $elem = element.find('[name="billingWrap-type"]');
    $elem.empty();
    $elem.append('<option selected="true" disabled="disabled">Please Select</option>');
    for(var i=0;i<communicationsArrayData.length;i++){
        $elem.append('<option value="'+communicationsArrayData[i].id+'">'+communicationsArrayData[i].value+'</option>');
    }
    // $elem.each(function() {
        var selectedValue = $elem.attr('data-attr-selected-billcommunication_type');
        if(selectedValue && selectedValue != "null") {
            $elem.val(selectedValue);
        }
    // });
}
function getRelationListData(resp) {
    var dataListArr = [],
        dataArray;
    if (resp && resp.response && resp.response.loginUser) {
        if ($.isArray(resp.response.loginUser)) {
            dataArray = resp.response.loginUser;
        } else {
            dataArray.push(resp.response.loginUser);
        }
    }
    if (dataArray != undefined) {
        for (var i = 0; i < dataArray.length; i++) {
            /*dataArray[i].idDuplicate = dataArray[i].id;
            if(dataArray[i].isActive == 1){*/
            dataListArr.push(dataArray[i]);
            /*}*/
        }
    }
    buildRelationListGrid(dataListArr);
}

function onError(errObj) {
    console.log(errObj);
    customAlert.error("Error", "Error");
}

function buildRelationListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Contact Name",
        "field": "userName"
    });
    gridColumns.push({
        "title": "Relation",
        "field": "fullName"
    });
    gridColumns.push({
        "title": "Home Phone",
        "field": "userTypeId"
    });
    gridColumns.push({
        "title": "Mobile Phone",
        "field": "id"
    });
    relationUIGridWrapper.creategrid(dataSource, gridColumns, otoptions);
    adjustHeight();
}

function adjustHeight() {
    var defHeight = 220; //+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    relationUIGridWrapper.adjustGridHeight(cmpHeight);
}

function getCountryZoneName() {
    /*   $.getJSON('//freegeoip.net/json/?callback=', function(dataObj) {
           var dataUrl = ipAddress + "/user/location";
           console.log(dataObj);
           sessionStorage.country = dataObj.country_code;
           dataObj.createdBy = sessionStorage.userId;
           dataObj.countryCode = dataObj.country_code;
           dataObj.countryName = dataObj.country_name;
           dataObj.regionCode = dataObj.region_code;
           dataObj.regionName = dataObj.region_name;
           dataObj.timeZone = dataObj.time_zone;
           dataObj.metroCode = dataObj.metro_codoe;
           dataObj.zipCode = dataObj.zip_code;
           sessionStorage.countryName = dataObj.country_name;
           sessionStorage.countryName =  getCountryName();
           if (sessionStorage.countryName == "India") {
              // $('.postalCode').text("Postal Code");
               $('.stateLabel').text("State");
               $('.nhsDetail').html('NHS: <span class="mandatoryClass">*</span>');
               $('.weightLabel').html('Weight (Kgs): <span class="mandatoryClass">*</span>');
               $('.zipFourWrapper').hide();
           } else if (sessionStorage.countryName == "United Kingdom") {
               //$('.postalCode').text("Postal Code");
               $('.stateLabel').text("County");
               $('.nhsDetail').html('NHS: <span class="mandatoryClass">*</span>');
               $('.weightLabel').html('Weight (Kgs): <span class="mandatoryClass">*</span>');
               $('.zipFourWrapper').hide();
           } else {
            //$('.postalCode').text("Zip");
               $('.stateLabel').text("State");
            $('.nhsDetail').html('SSN: <span class="mandatoryClass">*</span>');
            $('.weightLabel').html('Weight (lbs): <span class="mandatoryClass">*</span>');
               $('.zipFourWrapper').show();
           }
       });*/
}
/*$(function(){
    $("#dtDOB").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "-150:+150"
        });
});*/

/*function adjustHeight() {
    var defHeight = 45;
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 40;
    }
}*/

$(window).load(function() {
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
    init();
});

function init() {
    if (sessionStorage.clientTypeId == "2") {
        //$("#spnPI").text("Service User Information");
    }
    allowAlphaNumeric("txtExtID1");
    allowAlphaNumeric("txtExtID2");
    allowAlphaNumeric("txtWeight");
    allowDecimals("txtHeight");
    /*allowAlphaNumeric("txtNN");*/
    allowAlphaNumericwithSapce("txtFN");
    allowAlphaNumericwithSapce("txtMN");
    allowAlphaNumericwithSapce("txtLN");
    //allowNumerics("txtSSN");
    allowNumerics("txtAge");
    allowNumerics("txtWeight");

    allowNumerics("txtRoom");
    allowNumerics("txtBed");

    //allowAlphaNumeric("txtAdd1");
    //allowAlphaNumeric("txtAdd2");
     allowPhoneNumber("txtHPhone");
    //allowPhoneNumber("txtWP");
    allowPhoneNumber("txtExtension");
    //allowPhoneNumber("txtCell");
    validateEmail("txtEmail");
    allowPhoneNumber("txtCell");

    /*var cntry = sessionStorage.countryName;
    var nhs = 'NHS :<span class="mandatoryClass">*</span>';
    if(cntry.indexOf("India")>=0){
        $('.postalCode').html('Postal Code : <span class="mandatoryClass">*</span>');
        $('.stateLabel').text("State");
        $('.nhsDetail').html('NHS: <span class="mandatoryClass">*</span>');
        $('.weightLabel').html('Weight (Kgs): <span class="mandatoryClass">*</span>');
        $('.zipFourWrapper').hide();
        $('#txtSSN').unmask().maskSSN('999-999-9999', {maskedChar:'X', maskedCharsLength:6});
        $('#txtHPhone').unmask().maskSSN('(999) 999-999', {maskedChar:'X', maskedCharsLength:3});
        $('#txtCell').unmask().maskSSN('999 999 9999', {maskedChar:'X', maskedCharsLength:3});
    }else if(cntry.indexOf("United Kingdom")>=0){
        $('.postalCode').html('Postal Code : <span class="mandatoryClass">*</span>');
        $('.stateLabel').text("County");
        $('.nhsDetail').html('NHS: <span class="mandatoryClass">*</span>');
        $('.weightLabel').html('Weight (Kgs): <span class="mandatoryClass">*</span>');
        $('.zipFourWrapper').hide();
        $('#txtSSN').unmask().maskSSN('999-999-9999', {maskedChar:'X', maskedCharsLength:6});
        $('#txtHPhone').unmask().maskSSN('99999999999999999999', {maskedChar:'X', maskedCharsLength:3});
        $('#txtCell').unmask().maskSSN('999999999999999', {maskedChar:'X', maskedCharsLength:3});
    }else {
        $('.postalCode').html('Zip : <span class="mandatoryClass">*</span>');
        $('.stateLabel').text("State");
        $('.nhsDetail').html('SSN: <span class="mandatoryClass">*</span>');
        $('.weightLabel').html('Weight (lbs): <span class="mandatoryClass">*</span>');
        $('.zipFourWrapper').show();
        $('#txtSSN').unmask().maskSSN('999-99-9999', {maskedChar:'X', maskedCharsLength:5});
        $('#txtHPhone').unmask().maskSSN('(999) 999-9999', {maskedChar:'X', maskedCharsLength:3});
    }*/
    setHomePhoneMask();
    // $("#lblNHS").html(nhs);

    $("#txtDNR").kendoComboBox();
    $("#cmbReligion").kendoComboBox();
    $("#txtL1").kendoComboBox();
    $("#txtL2").kendoComboBox();
    $("#txtL3").kendoComboBox();
    $("#txtTalk").kendoComboBox();
    $("#txtGlass").kendoComboBox();
    $("#txtHear").kendoComboBox()
    $("#txtT1alk").kendoComboBox();;
    $("#txtDalk").kendoComboBox();
    $("#txtTdalk").kendoComboBox();
    $("#cmbMarital").kendoComboBox();
    setDataForSelection(dnrArr, "txtDNR", function() {
    	onChangeDNR();
    }, ["Value", "Key"], 0, "");
    setDataForSelection(typesArr, "cmbType", onPtChange, ["Value", "Key"], 0, "");
    setDataForSelection(booleanArr, "txtTalk", onTalkChange, ["Value", "Key"], 0, "");
    setDataForSelection(glassesArr, "txtGlass", onGlassChange, ["Value", "Key"], 0, "");
    setDataForSelection(booleanArr, "txtHear", onHearChange, ["Value", "Key"], 0, "");
    setDataForSelection(booleanArr, "txtT1alk", onT1alkChange, ["Value", "Key"], 0, "");
    setDataForSelection(booleanArr, "txtDalk", onDalkChange, ["Value", "Key"], 0, "");
    setDataForSelection(booleanArr, "txtTdalk", onTdalkChange, ["Value", "Key"], 0, "");



    //var text_ssn = $('#txtSSN').val();
    var hide_ssn = '';
    //$("#txtSSN").attr('maxlength', '9');
    if ($('#txtSSN').val().length > 4) {
        //   $('#txtSSN').val($('#txtSSN').val().replace(/^\d{5}/, '*****'));
    }
    $('#txtSSN').on('focus', function() {
        //$('#txtSSN').val(text_ssn);
    });
    $('#txtSSN').on('blur', function() {
        //  text_ssn = $('#txtSSN').val();

        if ($('#txtSSN').val().length > 4) {
            //  $('#txtSSN').val($('#txtSSN').val().replace(/^\d{5}/, '(***)_**_'));
        }

        // hide_ssn = $('#txtSSN').val();
    });
    $('#EyeImg').on('click', function() {

        $('img', this).toggle(1000);
        if ($('#txtSSN').val() == text_ssn) {
            // $('#txtSSN').val(hide_ssn);
        } else {
            //$('#txtSSN').val(text_ssn);
        }
    });


    /* var text_hp = $('#txtHPhone').val();
     var hide_hp = '';
     //$("#txtHPhone").attr('maxlength','10');
     if($('#txtHPhone').val().length > 4){
     
         //$('#txtHPhone').val($('#txtHPhone').val().replace(/^\d{6}/, '******'));
     }*/

    // Listen for Focus on any three fields
    /*$('#txtHPhone').on('focus', function(){
        //$('#txtHPhone').val(text_hp);
    });*/

    // Listen for Blur on any three fields

    /*$('#txtHPhone').on('blur', function(){
        //text_hp = $('#txtHPhone').val();
        
        if($('#txtHPhone').val().length > 4){
            //$('#txtHPhone').val($('#txtHPhone').val().replace(/^\d{6}/, '(***)_***_'));
           
        }
        
        //hide_hp = $('#txtHPhone').val();
    });*/

    // Show/Hide SSN click
    /* $('#EyeImg').on('click', function(){
         if($('#txtHPhone').val() == text_hp){
            // $('#txtHPhone').val(hide_hp);
         }
         else{
             //$('#txtHPhone').val(text_hp);
         }
     });*/



    /*$("#EyeImg").click(function(){
        $('img',this).toggle(1000);
       
    });*/
    /*$("#txtWP").kendoMaskedTextBox({
        mask: "(999) 999-9999"
    });
    $("#txtExtension").kendoMaskedTextBox({
        mask: "(999) 999-9999"
    });
    $("#txtCell").kendoMaskedTextBox({
        mask: "(999) 999-9999"
    });*/

    setHomePhoneMask();
    /*setWorkPhoneMask();
    setExtensionMask();
    setCellPhoneMask();*/
    operation = parentRef.operation;
    patientId = parentRef.patientId;//101
    //selItem = parentRef.selItem;
    $("#cmbPrefix").kendoComboBox();
    $("#cmbStatus").kendoComboBox();
    $("#cmbGender").kendoComboBox();
    $("#cmbCarerGender").kendoComboBox();
    $("#cmbZip1").kendoComboBox();
    $("#cmbSMS").kendoComboBox();
    $("#cmbLang").igCombo();
    $("#cmbEthicity").kendoComboBox();
    $("#cmbRace").kendoComboBox();
    // $("#cmbLan").kendoComboBox();
    //$("#dtDOB").kendoDatePicker({change: onDOBChange});
    var cntry = sessionStorage.countryName;
    if (cntry.indexOf("India") >= 0) {
        $("#dtDOB").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            yearRange: "-200:+200",
            maxDate: '0',
            onSelect: function(e, e1) {
                // var dob = document.getElementById("dtDOB").value;
                var dob = ((e1.selectedMonth + 1) + "-" + e1.selectedDay + "-" + e1.selectedYear);
                var DOB = new Date(dob);
                var today = new Date();
                var age = today.getTime() - DOB.getTime();
                age = Math.floor(age / (1000 * 60 * 60 * 24 * 365.25));
                document.getElementById('txtAge').value = age;
            }
        });
    } else if (cntry.indexOf("United Kingdom") >= 0) {
        $("#dtDOB").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            yearRange: "-200:+200",
            maxDate: '0',
            onSelect: function(e, e1) {
                var dob = ((e1.selectedMonth + 1) + "-" + e1.selectedDay + "-" + e1.selectedYear);
                //var dob = document.getElementById("dtDOB").value;
                var DOB = new Date(dob);
                var today = new Date();
                var age = today.getTime() - DOB.getTime();
                age = Math.floor(age / (1000 * 60 * 60 * 24 * 365.25));
                document.getElementById('txtAge').value = age;
            }
        });
    } else {
        $("#dtDOB").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'mm/dd/yy',
            yearRange: "-200:+200",
            maxDate: '0',
            onSelect: function(e, e1) {
                var dob = ((e1.selectedMonth + 1) + "-" + e1.selectedDay + "-" + e1.selectedYear);
                // var dob = document.getElementById("dtDOB").value;
                var DOB = new Date(dob);
                var today = new Date();
                var age = today.getTime() - DOB.getTime();
                age = Math.floor(age / (1000 * 60 * 60 * 24 * 365.25));
                document.getElementById('txtAge').value = age;
            }
        });
    }

    getAjaxObject(ipAddress + "/facility/list?is-active=1", "GET", getFacilityList, onError);
    getAjaxObject(ipAddress + "/master/marital_status/list?is-active=1", "GET", getMaritalList, onError);
    getAjaxObject(ipAddress + "/master/religion/list?is-active=1", "GET", getReligionList, onError);
    
    buttonEvents();
}

function onChangeDNR(){
	var txtDNR = $("#txtDNR").data("kendoComboBox");
	$("#imgFieldSet").removeClass("imgFieldRedClass");
	$("#imgFieldSet").removeClass("imgFieldGreenClass");
	if(txtDNR.text() == "Yes") {
        $("#imgFieldSet").addClass("imgFieldRedClass");
    }
	// }else{
	// 	$("#imgFieldSet").addClass("imgFieldGreenClass");
	// }
}
function getMaritalList(dataObj) {
    console.log(dataObj);
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbMarital", onChangeMaritalStatus, ["desc", "value"], 0, "");
    }
}

function getReligionList(dataObj) {
    console.log(dataObj);
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbReligion", onChangeReligion, ["desc", "value"], 0, "");
    }
}

function onChangeMaritalStatus() {

}
function onChangeReligion() {

}

function getFacilityList(dataObj) {
    console.log(dataObj);
    var dataArray = [];
    if (dataObj) {
        if ($.isArray(dataObj.response.facility)) {
            dataArray = dataObj.response.facility;
        } else {
            dataArray.push(dataObj.response.facility);
        }
    }
    var tempDataArry = [];
    var obj = {};
    obj.name = "";
    obj.idk = "";
    tempDataArry.push(obj);
    for (var i = 0; i < dataArray.length; i++) {
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if (dataArray[i].isActive == 1) {
            dataArray[i].Status = "Active";
            var obj = dataArray[i];
            obj.idk = dataArray[i].id;
            obj.status = dataArray[i].Status;
            tempDataArry.push(obj);
        }
    }
    //dataArray = tempDataArry;

    speechfacilityArry = tempDataArry;

    setDataForSelection(tempDataArry, "txtFAN", onFacilityChange, ["name", "idk"], 0, "");
    // getZip();
    getPrefix();
    onFacilityChange();
}

function onFacilityChange() {
    var txtFAN = $("#txtFAN").data("kendoComboBox");
    if (txtFAN) {
        var txtFId = txtFAN.value();
        getAjaxObject(ipAddress + "/facility/list?id=" + txtFId, "GET", onGetFacilityInfo, onError);
    }
}
var billActNo = "";

function onGetFacilityInfo(dataObj) {
    billActNo = "";
    $("#txtBAN").val("");
    if (dataObj && dataObj.response && dataObj.response.facility) {
        $("#txtBAN").val(dataObj.response.facility[0].name);
        billActNo = dataObj.response.facility[0].billingAccountId;
    }
}
/*function validateEmail(sEmail) {
    console.log(sEmail);
    var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    if (filter.test(sEmail)) {
    return true;
    }
    else {
    return false;
    }
}
function Validate(event) {
        var regex = new RegExp("^[A-Za-z0-9? ]");
        var key = String.fromCharCode(event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            event.preventDefault();
            return false;
        }
    }  
function Validate1(event) {
        var regex = new RegExp("^[0-9.? ]");
        var key = String.fromCharCode(event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            event.preventDefault();
            return false;
        }
}
function Validate2(event) {
        var regex = new RegExp("^[0-9? ]");
        var key = String.fromCharCode(event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            event.preventDefault();
            return false;
        }
}     
function onlyAlphabets(e, t) {
            try {
                if (window.event) {
                    var charCode = window.event.keyCode;
                }
                else if (e) {
                    var charCode = e.which;
                }
                else { return true; }
                if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))
                    return true;
                else
                    return false;
            }
            catch (err) {
                alert(err.Description);
            }
}*/


/*function setAge(e){
    /*var dtItem = $("#dtDOB").data("datepicker");
    var strDate = "";
    $("#txtAge").val("");
    if(dtItem && dtItem.value()){
        console.log(dtItem.value());
        var strAge = getAge(dtItem.value());
        $("#txtAge").val(strAge);
        strDate = toString(dtItem.value(),"yyyy-MM-dd");
        //strDate = kendo.toString(dtItem.value(),"yyyy-MM-dd");
    }
    var bday = new Date(Date.parse(e.target.value));
      var today = new Date()
      
      document.getElementsById('age')[0].value = today.getFullYear() - bday.getFullYear();
}*/
function onDOBChange() {
    var dtDOB = $("#dtDOB").data("kendoDatePicker");
    if (dtDOB) {
        var dt = dtDOB.value();
        var strAge = getAge(dt);
        $("#txtAge").val(strAge);
    }

}

function onStatusChange() {
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if (cmbStatus && cmbStatus.selectedIndex < 0) {
        cmbStatus.select(0);
    }
}

var ssnNum = "";

function buttonEvents() {
    $("#btnCancel").off("click", onClickCancel);
    $("#btnCancel").on("click", onClickCancel);

    $("#btnCancelAdditional").off("click", onClickCancelAdditional);
    $("#btnCancelAdditional").on("click", onClickCancelAdditional);

    $("#btnSave").off("click", onClickSave);
    $("#btnSave").on("click", onClickSave);

    $("#btnSaveAdditional").off("click", onClickSaveAdditional);
    $("#btnSaveAdditional").on("click", onClickSaveAdditional);

    $("#btnSearch").off("click", onClickSearch);
    $("#btnSearch").on("click", onClickSearch);

    $("#btnReset").off("click", onClickReset);
    $("#btnReset").on("click", onClickReset);

    $("#btnResetAdditional").off("click", onClickResetAdditional);
    $("#btnResetAdditional").on("click", onClickResetAdditional);

    $("#btnZipSearch").off("click");
    $("#btnZipSearch").on("click", onClickZipSearch);

    $("#btnCitySearch").off("click");
    $("#btnCitySearch").on("click", onClickZipSearch);

    $(document).on("click", '.zipSearchImg', onClickZipSearch);
    $(document).on("click", '.citySearchImg', onClickZipSearch);

    $("#btnBrowse").off("click", onClickBrowse);
    $("#btnBrowse").on("click", onClickBrowse);

    $("#btnBrowse").off("mouseover", onClickBrowseOver);
    $("#btnBrowse").on("mouseover", onClickBrowseOver);

    $("#btnBrowse").off("mouseout", onClickBrowseOut);
    $("#btnBrowse").on("mouseout", onClickBrowseOut);

    $("#txtSSN").off("mouseover");
    $("#txtSSN").on("mouseover", onOverSSN);

    $("#txtHPhone").off("mouseover");
    $("#txtHPhone").on("mouseover", onOverHPhone);

    $("#txtCell").off("mouseover");
    $("#txtCell").on("mouseover", onOverCell);

    $("#fileElem").off("change", onSelectionFiles);
    $("#fileElem").on("change", onSelectionFiles);

    $("#fileElem").off("click", onSelectionFiles);
    $("#fileElem").on("click", onSelectionFiles);

    $("#btnStartVideo").off("click", onClickStartVideo);
    $("#btnStartVideo").on("click", onClickStartVideo);

    $("#btnStartVideo").off("mouseover", onClickStartVideoOver);
    $("#btnStartVideo").on("mouseover", onClickStartVideoOver);

    $("#btnStartVideo").off("mouseout", onClickBrowseOut);
    $("#btnStartVideo").on("mouseout", onClickBrowseOut);

    $("#btnTakePhoto").off("click", onClickTakePhoto);
    $("#btnTakePhoto").on("click", onClickTakePhoto);

    $("#btnTakePhoto").off("mouseover", onClickTakePhotoOver);
    $("#btnTakePhoto").on("mouseover", onClickTakePhotoOver);

    $("#btnTakePhoto").off("mouseout", onClickBrowseOut);
    $("#btnTakePhoto").on("mouseout", onClickBrowseOut);

    $("#btnUpload").off("click", onClickUploadPhoto);
    $("#btnUpload").on("click", onClickUploadPhoto);

    $("#btnUpload").off("mouseover", onClickUploadPhotoOver);
    $("#btnUpload").on("mouseover", onClickUploadPhotoOver);

    $("#btnUpload").off("mouseout", onClickBrowseOut);
    $("#btnUpload").on("mouseout", onClickBrowseOut);

    $("#txtWP").off("change");
    $("#txtWP").on("change", onChangeWorkPhone);

    $("#txtCell").off("change");
    $("#txtCell").on("change", onChangeCell);

    $('#btnSaveMedicalCondition').off('click', onSaveMedicalCondition);
    $('#btnSaveMedicalCondition').on('click', onSaveMedicalCondition);

    $('#btnSaveDiet').off('click', onSaveDiet);
    $('#btnSaveDiet').on('click', onSaveDiet);

    $('#btnSaveAllergy').off('click', onSaveAllergy);
    $('#btnSaveAllergy').on('click', onSaveAllergy);

    $('#btnSaveBilling').off('click', onSaveBilling);
    $('#btnSaveBilling').on('click', onSaveBilling);

    $('#btnSaveContacts').off('click', onSaveContacts);
    $('#btnSaveContacts').on('click', onSaveContacts);

    $(".btnActive").off("click");
    $(".btnActive").on("click", onClickActive);

    $(".btnInActive").off("click");
    $(".btnInActive").on("click", onClickInActive);

    function onClickActive() {
        /*$("#btnSave").prop("disabled", true);
        $("#btnDelete").prop("disabled", true);*/
        $('.medicalConditionAlert:visible .alert').remove();
        if (showTab == "illnessTab") {
            $(".illnessTabWrapper .btnInActive").removeClass("selectButtonBarClass");
            $(".illnessTabWrapper .btnActive").addClass("selectButtonBarClass");
            dataTabURL = ipAddress + "/homecare/patient-medical-conditions/";
            $('.illnessTabWrapper').attr('data-processed', true);
            getAjaxObject(dataTabURL + '?is-active=1&fields=*,medicalConditionType.*&patientId='+patientId, "GET", getIllnessData, onError);
          //  getAjaxObject(dataTabURL + '?is-active=1&patientId='+patientId, "GET", getIllnessData, onError);
        } else if (showTab == "allergiesTab") {
            $(".allergiesTabWrapper .btnInActive").removeClass("selectButtonBarClass");
            $(".allergiesTabWrapper .btnActive").addClass("selectButtonBarClass");
            dataTabURL = ipAddress + "/homecare/patient-allergies/";
            $('.allergiesTabWrapper').attr('data-processed', true);
            getAjaxObject(dataTabURL + '?is-active=1&fields=*,allergyType.*&patientId='+patientId, "GET", getAllergiesData, onError);
           // getAjaxObject(dataTabURL + '?is-active=1&patientId='+patientId, "GET", getAllergiesData, onError);
        } else if (showTab == "dietTab") {
            $(".dietTabWrapper .btnInActive").removeClass("selectButtonBarClass");
            $(".dietTabWrapper .btnActive").addClass("selectButtonBarClass");
            dataTabURL = ipAddress + "/homecare/patient-diets/";
            $('.dietTabWrapper').attr('data-processed', true);
            getAjaxObject(dataTabURL + '?is-active=1&fields=*,dietType.*&patientId='+patientId, "GET", getDietsData, onError);
          //  getAjaxObject(dataTabURL + '?is-active=1&patientId='+patientId, "GET", getDietsData, onError);
        } else if (showTab == "billingTab") {
            $(".billingTabWrapper .btnInActive").removeClass("selectButtonBarClass");
            $(".billingTabWrapper .btnActive").addClass("selectButtonBarClass");
            dataTabURL = ipAddress + "/homecare/patient-relationships/";
            $('.billingTabWrapper').attr('data-processed', true);
            getAjaxObject(ipAddress + '/homecare/communication-types/?fields=id,value&id=:bt:201,202', "GET", getcommunications, onError);
            getAjaxObject(dataTabURL + '?is-active=1&is-deleted=0&patient-Id='+patientId+'&contactType='+ billCommunitionType +'&fields=*', "GET", getPatientBillingData, onError);
          //  getAjaxObject(dataTabURL + '?is-active=1&communicationTypeId=201&patientId='+patientId, "GET", getPatientBillingData, onError);
        } else if (showTab == "contactsTab") {
            $(".contactsTabWrapper .btnInActive").removeClass("selectButtonBarClass");
            $(".contactsTabWrapper .btnActive").addClass("selectButtonBarClass");
            dataTabURL = ipAddress + "/homecare/patient-relationships/";
            $('.contactsTabWrapper').attr('data-processed', true);
            // getAjaxObject(dataTabURL + '?is-active=1&communicationTypeId=:bt:202,220&patientId='+patientId, "GET", getPatientContactsData, onError);
            getAjaxObject(dataTabURL + '?is-active=1&is-deleted=0&contactType='+ contactCommunitionType +'&patientId='+patientId, "GET", getPatientContactsData, onError);
        }
    }


    function onClickInActive() {
        /*$("#btnSave").prop("disabled", true);
        $("#btnDelete").prop("disabled", true);*/
        $('.medicalConditionAlert:visible .alert').remove();
        if (showTab == "illnessTab") {
            $(".illnessTabWrapper .btnActive").removeClass("selectButtonBarClass");
            $(".illnessTabWrapper .btnInActive").addClass("selectButtonBarClass");
            dataTabURL = ipAddress + "/homecare/patient-medical-conditions/";
            $('.illnessTabWrapper').attr('data-processed', true);
            getAjaxObject(dataTabURL + '?is-active=1&fields=*,medicalConditionType.*&patientId='+patientId, "GET", getIllnessData, onError);
           // getAjaxObject(dataTabURL + '?is-active=0&patientId='+patientId, "GET", getIllnessData, onError);
        } else if (showTab == "allergiesTab") {
            $(".allergiesTabWrapper .btnActive").removeClass("selectButtonBarClass");
            $(".allergiesTabWrapper .btnInActive").addClass("selectButtonBarClass");
            dataTabURL = ipAddress + "/homecare/patient-allergies/";
            $('.allergiesTabWrapper').attr('data-processed', true);
            getAjaxObject(dataTabURL + '?is-active=1&fields=*,allergyType.*&patientId='+patientId, "GET", getAllergiesData, onError);
           // getAjaxObject(dataTabURL + '?is-active=0&patientId='+patientId, "GET", getAllergiesData, onError);
        } else if (showTab == "dietTab") {
            $(".dietTabWrapper .btnActive").removeClass("selectButtonBarClass");
            $(".dietTabWrapper .btnInActive").addClass("selectButtonBarClass");
            dataTabURL = ipAddress + "/homecare/patient-diets/";
            $('.dietTabWrapper').attr('data-processed', true);
            getAjaxObject(dataTabURL + '?is-active=1&fields=*,dietType.*&patientId='+patientId, "GET", getDietsData, onError);
          //  getAjaxObject(dataTabURL + '?is-active=0&patientId='+patientId, "GET", getDietsData, onError);
        } else if (showTab == "billingTab") {
            $(".billingTabWrapper .btnActive").removeClass("selectButtonBarClass");
            $(".billingTabWrapper .btnInActive").addClass("selectButtonBarClass");
            dataTabURL = ipAddress + "/homecare/patient-relationships/";
            $('.billingTabWrapper').attr('data-processed', true);
            getAjaxObject(ipAddress + '/homecare/communication-types/?fields=id,value&id=:bt:201,202', "GET", getcommunications, onError);
            getAjaxObject(dataTabURL + '?is-active=0&is-deleted=1&contactType='+ billCommunitionType +'&patient-Id='+patientId+"&fields=*", "GET", getPatientBillingData, onError);
            // getAjaxObject(dataTabURL + '?is-active=1&communication-type-id=:in:201&patient-Id='+patientId+"&fields=*", "GET", getPatientBillingData, onError);
           // getAjaxObject(dataTabURL + '?is-active=0&communicationTypeId=201&patientId='+patientId, "GET", getPatientBillingData, onError);
        } else if (showTab == "contactsTab") {
            $(".contactsTabWrapper .btnActive").removeClass("selectButtonBarClass");
            $(".contactsTabWrapper .btnInActive").addClass("selectButtonBarClass");
            dataTabURL = ipAddress + "/homecare/patient-relationships/";
            $('.contactsTabWrapper').attr('data-processed', true);
            getAjaxObject(dataTabURL + '?is-active=0&is-deleted=1&contactType='+ contactCommunitionType +'&patientId='+patientId, "GET", getPatientContactsData, onError);
            // getAjaxObject(dataTabURL + '?is-active=0&communicationTypeId=:bt:202,220&patientId='+patientId, "GET", getPatientContactsData, onError);
        }
    }


    $('body').on('click', '.illnessAddRemoveLink.addIllnessQual', function() {
        var $trLen = $('.illnessTabWrapper table tbody tr').length;
        $('.illnessTabWrapper table tbody').append('<tr class="qualWrapper_li qualificationTabWrapper-li-' + $trLen + '" data-item-new="true"><td class="qual-td-addRemove"><span><a href="#" class="illnessAddRemoveLink addIllnessQual">+</a> <a href="#" class="illnessAddRemoveLink removeIllnessQual">-</a></span></td><td><input type="text" name="illnessWrap-sno" data-attr-id-number="" data-attr-id-patient-number="" value="' + $trLen + '" readonly /></td><td><input type="text" name="illnessWrap-condition" id="qualWrapQualification" readonly value="" /><button class="btn btn-primary medicalCondition-search patient-search-img"><img src="../../img/search-icon-white.png" class="tradeFormWrapper-searchName-img"></button></td><td><input type="text" name="illnessWrap-notes" maxlength="100" value="" /></td><td><input type="text" name="illnessWrap-remarks" maxlength="100" value="" /></td></tr>');
        modifySerialNumber('illnessTabWrapper');
    });
    $('body').on('click', '.illnessAddRemoveLink.removeIllnessQual', function() {
        var removeItemId = parseInt($(this).closest('tr').find('[name="illnessWrap-sno"]').attr('data-attr-id-patient-number'));
        var obj = {
            "id": removeItemId,
            "modifiedBy": sessionStorage.userId,
            "isActive": 0,
            "isDeleted": 0
        };
        $removeItem = $(this);
        createAjaxObject(dataTabURL, obj, "DELETE", onDeleteItem, onError);
    });

    $('body').on('click', '.allergiesAddRemoveLink.addAllergiesQual', function() {
        var $trLen = $('.allergiesTabWrapper table tbody tr').length;
        $('.allergiesTabWrapper table tbody').append('<tr class="qualWrapper_li qualificationTabWrapper-li-' + $trLen + '" data-item-new="true"><td class="qual-td-addRemove"><span><a href="#" class="allergiesAddRemoveLink addAllergiesQual">+</a> <a href="#" class="allergiesAddRemoveLink removeAllergiesQual">-</a></span></td><td><input type="text" name="allergiesWrap-sno" data-attr-id-number="" data-attr-id-patient-number="" value="' + $trLen + '" readonly /></td><td><input type="text" name="allergiesWrap-condition" id="qualWrapQualification" readonly value="" /><button class="btn btn-primary allergy-search patient-search-img"><img src="../../img/search-icon-white.png" class="tradeFormWrapper-searchName-img"></button></td><td><input type="text" name="allergiesWrap-notes" maxlength="100" value="" /></td><td><input type="text" name="allergiesWrap-remarks" maxlength="100" value="" /></td></tr>');
        modifySerialNumber('allergiesTabWrapper');
    });
    $('body').on('click', '.allergiesAddRemoveLink.removeAllergiesQual', function() {
        var removeItemId = parseInt($(this).closest('tr').find('[name="allergiesWrap-sno"]').attr('data-attr-id-patient-number'));
        var obj = {
            "id": removeItemId,
            "modifiedBy": sessionStorage.userId,
            "isActive": 0,
            "isDeleted": 0
        };
        $removeItem = $(this);
        createAjaxObject(dataTabURL, obj, "DELETE", onDeleteItem, onError);
    });

    $('body').on('click', '.dietAddRemoveLink.addDietQual', function() {
        var $trLen = $('.dietTabWrapper table tbody tr').length;
        $('.dietTabWrapper table tbody').append('<tr class="qualWrapper_li qualificationTabWrapper-li-' + $trLen + '" data-item-new="true"><td class="qual-td-addRemove"><span><a href="#" class="dietAddRemoveLink addDietQual">+</a> <a href="#" class="dietAddRemoveLink removeDietQual">-</a></span></td><td><input type="text" name="dietWrap-sno" data-attr-id-number="" data-attr-id-patient-number="" value="' + $trLen + '" readonly /></td><td><input type="text" name="dietWrap-condition" id="qualWrapQualification" readonly value="" /><button class="btn btn-primary diet-search patient-search-img"><img src="../../img/search-icon-white.png" class="tradeFormWrapper-searchName-img"></button></td><td><input type="text" name="dietWrap-notes" maxlength="100" value="" /></td><td><input type="text" name="dietWrap-remarks" maxlength="100" value="" /></td></tr>');
        modifySerialNumber('dietTabWrapper');
    });
    $('body').on('click', '.dietAddRemoveLink.removeDietQual', function() {
        var removeItemId = parseInt($(this).closest('tr').find('[name="dietWrap-sno"]').attr('data-attr-id-patient-number'));
        var obj = {
            "id": removeItemId,
            "modifiedBy": sessionStorage.userId,
            "isActive": 0,
            "isDeleted": 0
        };
        $removeItem = $(this);
        createAjaxObject(dataTabURL, obj, "DELETE", onDeleteItem, onError);
    });
    $('body').on('click', '.billingAddRemoveLink.addBillingQual', function() {
        $('.medicalConditionAlert:visible .alert').remove();
        $('.customAlert .alert').remove();
        var $trLen = $('.billingTabWrapper table tbody tr').length;
        if($trLen != 1){

            var $thisList = $('.qualificationTabWrapper-li-' + ($trLen-1));

            var txtName = $thisList.find('[name="billingWrap-name"]').val();
            txtName = $.trim(txtName);

            var txtAddress1 = $thisList.find('[name="billingWrap-address1"]').val();
            txtAddress1 = $.trim(txtAddress1);

            var txtzipcode = $thisList.find('[name="billingWrap-zipcode"]').val();
            txtzipcode = $.trim(txtzipcode);

            var cmbtype = $thisList.find('[name="billingWrap-type"]').val();
            cmbtype = $.trim(cmbtype);

            var txtrelation = $thisList.find('[name="billingWrap-relationship"]').val();
            txtrelation = $.trim(txtrelation);

            var txtcity = $thisList.find('[name="billingWrap-city"]').val();
            txtcity = $.trim(txtcity);



            if(txtName && txtName != "" && txtAddress1 && txtAddress1 != "" && ((IsPostalFlag == "0" && txtzipcode && txtzipcode != "") || (IsPostalFlag == "1" && txtcity && txtcity != "")) && cmbtype && cmbtype > 0 && txtrelation && txtrelation != ""){
                if(IsPostalFlag == "0"){
                    $('.billingTabWrapper table tbody').append('<tr class="qualWrapper_li qualificationTabWrapper-li-' + $trLen + '" data-item-new="true"><td class="qual-td-addRemove"><span><a href="#" class="billingAddRemoveLink addBillingQual">+</a> <a href="#" class="billingAddRemoveLink removeBillingQual">-</a></span></td><td style="display:none"><input type="text" name="billingWrap-sno" data-attr-id-number="" value="' + $trLen + '" readonly /></td><td><select name="billingWrap-type" style="margin-bottom:5px" class="select-dropdown-width fields-mandatory"><option selected="true" disabled="disabled">Please Select</option></select><select name="billingWrap-relationship" class="select-dropdown-width" style="border-color: #45cff0;"><option selected="true" disabled="disabled">Please Select</option></select></td><td><input type="text" name="billingWrap-name" style="border-color: #45cff0;" maxlength="100" value="" /></td><td><div><input type="text" name="billingWrap-address1" style="border-color: #45cff0;" placeholder="Address 1" maxlength="100" value="" /><input type="text" name="billingWrap-address2" placeholder="Address 2" maxlength="100" value="" /><input type="text" name="billingWrap-city" data-attr-city-id="" placeholder="City" readonly maxlength="100" readonly value="" /></div><div><input type="text" name="billingWrap-state" placeholder="County" maxlength="100" readonly value="" /><input type="text" name="billingWrap-country" placeholder="Country" readonly maxlength="100" value="" /><div class="inline-block"><input type="text" name="billingWrap-zipcode" class="fields-mandatory" placeholder="Postal Code" maxlength="100" disabled value="" /><img class="postalWrapper-img zipSearchImg" src="../../img/Default/search-icon.png" title="Search"></div><input type="text" name="billingWrap-zip4" placeholder="Zip4" readonly style="display: none;" maxlength="100" value="" /></div></td><td><div class="block-elem"><input type="text" name="billingWrap-phone" placeholder="Mobile" maxlength="100" value="" /></div><input type="text" name="billingWrap-home" placeholder="Home Phone" maxlength="100" value="" /></div></div></td><td><div class="block-elem"><input type="text" name="billingWrap-email" placeholder="Email" maxlength="100" value="" /></td><td><input type="text" name="billingWrap-hourlybillrate" placeholder="Hourly Billing Rate" maxlength="100" value="" /></td><td><input type="text" name="billingWrap-notes" maxlength="100" value="" /></td></tr>');
                    modifySerialNumber('billingTabWrapper');
                    appendRelationshipsData($('.qualificationTabWrapper-li-' + $trLen));
                    billappendcommunicationsData($('.qualificationTabWrapper-li-' + $trLen));
                }else{
                    $('.billingTabWrapper table tbody').append('<tr class="qualWrapper_li qualificationTabWrapper-li-' + $trLen + '" data-item-new="true"><td class="qual-td-addRemove"><span><a href="#" class="billingAddRemoveLink addBillingQual">+</a> <a href="#" class="billingAddRemoveLink removeBillingQual">-</a></span></td><td style="display:none"><input type="text" name="billingWrap-sno" data-attr-id-number="" value="' + $trLen + '" readonly /></td><td><select name="billingWrap-type" style="margin-bottom:5px" class="select-dropdown-width fields-mandatory"><option selected="true" disabled="disabled">Please Select</option></select><select name="billingWrap-relationship" class="select-dropdown-width" style="border-color: #45cff0;"><option selected="true" disabled="disabled">Please Select</option></select></td><td><input type="text" name="billingWrap-name" style="border-color: #45cff0;" maxlength="100" value="" /></td><td><div><input type="text" name="billingWrap-address1" style="border-color: #45cff0;" placeholder="Address 1" maxlength="100" value="" /><input type="text" name="billingWrap-address2" placeholder="Address 2" maxlength="100" value="" /><div class="inline-block"><input type="text" name="billingWrap-city" class="fields-mandatory" data-attr-city-id="" placeholder="City" disabled maxlength="100" readonly value="" /><img class="postalWrapper-img citySearchImg" src="../../img/Default/search-icon.png" title="Search"></div><input type="text" name="billingWrap-state" placeholder="County" maxlength="100" readonly value="" /><input type="text" name="billingWrap-country" placeholder="Country" readonly maxlength="100" value="" /><div class="inline-block"><input type="text" name="billingWrap-zipcode" placeholder="Postal Code" maxlength="100" /></div><input type="text" name="billingWrap-zip4" placeholder="Zip4" readonly style="display: none;" maxlength="100" value="" /></div></td><td><div class="block-elem"><input type="text" name="billingWrap-phone" placeholder="Mobile" maxlength="100" value="" /></div><input type="text" name="billingWrap-home" placeholder="Home Phone" maxlength="100" value="" /></div></div></td><td><div class="block-elem"><input type="text" name="billingWrap-email" placeholder="Email" maxlength="100" value="" /></td><td><input type="text" name="billingWrap-hourlybillrate" placeholder="Hourly Billing Rate" maxlength="100" value="" /></td><td><input type="text" name="billingWrap-notes" maxlength="100" value="" /></td></tr>');
                    modifySerialNumber('billingTabWrapper');
                    appendRelationshipsData($('.qualificationTabWrapper-li-' + $trLen));
                    billappendcommunicationsData($('.qualificationTabWrapper-li-' + $trLen));
                }
            }
            else{
                $('.customAlert').append('<div class="alert alert-danger">Please fill all the Required Fields</div>');
            }

        }
        else{
            if(IsPostalFlag == "0") {
                $('.billingTabWrapper table tbody').append('<tr class="qualWrapper_li qualificationTabWrapper-li-' + $trLen + '" data-item-new="true"><td class="qual-td-addRemove"><span><a href="#" class="billingAddRemoveLink addBillingQual">+</a> <a href="#" class="billingAddRemoveLink removeBillingQual">-</a></span></td><td style="display:none"><input type="text" name="billingWrap-sno" data-attr-id-number="" value="' + $trLen + '" readonly /></td><td><select name="billingWrap-type" style="margin-bottom:5px" class="select-dropdown-width fields-mandatory"><option selected="true" disabled="disabled">Please Select</option></select><select name="billingWrap-relationship" class="select-dropdown-width"><option selected="true" disabled="disabled">Please Select</option></select></td><td><input type="text" name="billingWrap-name" style="border-color: #45cff0;" maxlength="100" value="" /></td><td><div><input type="text" name="billingWrap-address1" style="border-color: #45cff0;" placeholder="Address 1" maxlength="100" value="" /><input type="text" name="billingWrap-address2" placeholder="Address 2" maxlength="100" value="" /><input type="text" name="billingWrap-city" data-attr-city-id="" placeholder="City" readonly maxlength="100" readonly value="" /></div><div><input type="text" name="billingWrap-state" placeholder="County" maxlength="100" readonly value="" /><input type="text" name="billingWrap-country" placeholder="Country" readonly maxlength="100" value="" /><div class="inline-block"><input type="text" name="billingWrap-zipcode" class="fields-mandatory" placeholder="Postal Code" maxlength="100" disabled value="" /><img class="postalWrapper-img zipSearchImg" src="../../img/Default/search-icon.png" title="Search"></div><input type="text" name="billingWrap-zip4" placeholder="Zip4" readonly style="display: none;" maxlength="100" value="" /></div></td><td><div class="block-elem"><input type="text" name="billingWrap-phone" placeholder="Mobile" maxlength="100" value="" /></div><input type="text" name="billingWrap-home" placeholder="Home Phone" maxlength="100" value="" /></div></div></td><td><div class="block-elem"><input type="text" name="billingWrap-email" placeholder="Email" maxlength="100" value="" /></td><td><input type="text" name="billingWrap-hourlybillrate" placeholder="Hourly Billing Rate" maxlength="100" value="" /></td><td><input type="text" name="billingWrap-notes" maxlength="100" value="" /></td></tr>');
            } else{
                $('.billingTabWrapper table tbody').append('<tr class="qualWrapper_li qualificationTabWrapper-li-' + $trLen + '" data-item-new="true"><td class="qual-td-addRemove"><span><a href="#" class="billingAddRemoveLink addBillingQual">+</a> <a href="#" class="billingAddRemoveLink removeBillingQual">-</a></span></td><td style="display:none"><input type="text" name="billingWrap-sno" data-attr-id-number="" value="' + $trLen + '" readonly /></td><td><select name="billingWrap-type" style="margin-bottom:5px" class="select-dropdown-width fields-mandatory"><option selected="true" disabled="disabled">Please Select</option></select><select name="billingWrap-relationship" class="select-dropdown-width"><option selected="true" disabled="disabled">Please Select</option></select></td><td><input type="text" name="billingWrap-name" style="border-color: #45cff0;" maxlength="100" value="" /></td><td><div><input type="text" name="billingWrap-address1" style="border-color: #45cff0;" placeholder="Address 1" maxlength="100" value="" /><input type="text" name="billingWrap-address2" placeholder="Address 2" maxlength="100" value="" /><div class="inline-block"><input type="text" name="billingWrap-city" data-attr-city-id="" class="fields-mandatory" disabled placeholder="City" maxlength="100" value="" /><img class="postalWrapper-img citySearchImg" src="../../img/Default/search-icon.png" title="Search"></div><input type="text" name="billingWrap-state" placeholder="County" maxlength="100" readonly value="" /><input type="text" name="billingWrap-country" placeholder="Country" readonly maxlength="100" value="" /><div class="inline-block"><input type="text" name="billingWrap-zipcode" placeholder="Postal Code" maxlength="100"/></div><input type="text" name="billingWrap-zip4" placeholder="Zip4" readonly style="display: none;" maxlength="100" value="" /></div></td><td><div class="block-elem"><input type="text" name="billingWrap-phone" placeholder="Mobile" maxlength="100" value="" /></div><input type="text" name="billingWrap-home" placeholder="Home Phone" maxlength="100" value="" /></div></div></td><td><div class="block-elem"><input type="text" name="billingWrap-email" placeholder="Email" maxlength="100" value="" /></td><td><input type="text" name="billingWrap-hourlybillrate" placeholder="Hourly Billing Rate" maxlength="100" value="" /></td><td><input type="text" name="billingWrap-notes" maxlength="100" value="" /></td></tr>');
            }
            modifySerialNumber('billingTabWrapper');
            appendRelationshipsData($('.qualificationTabWrapper-li-' + $trLen));
            billappendcommunicationsData($('.qualificationTabWrapper-li-' + $trLen));
        }

    });
    $('body').on('click', '.billingAddRemoveLink.removeBillingQual', function() {
        var removeItemId = parseInt($(this).closest('tr').find('[name="billingWrap-sno"]').attr('data-attr-id-number'));
        var obj = {
            "id": removeItemId,
            "modifiedBy": sessionStorage.userId,
            "isActive": 0,
            "isDeleted": 0
        };
        $removeItem = $(this);
        createAjaxObject(dataTabURL, obj, "DELETE", onDeleteItem, onError);
    });
    $('body').on('click', '.contactsAddRemoveLink.addContactsQual', function() {
        $('.medicalConditionAlert:visible .alert').remove();
        $('.customAlert .alert').remove();
        var $trLen = $('.contactsTabWrapper table tbody tr').length;
        if($trLen != 1){

            var $thisList = $('.qualificationTabWrapper-li-' + ($trLen-1));

            var txtName = $thisList.find('[name="contactsWrap-name"]').val();
            txtName = $.trim(txtName);

            var txtAddress1 = $thisList.find('[name="contactsWrap-address1"]').val();
            txtAddress1 = $.trim(txtAddress1);

            var txtzipcode = $thisList.find('[name="contactsWrap-zipcode"]').val();
            txtzipcode = $.trim(txtzipcode);

            var cmbtype = $thisList.find('[name="contactsWrap-type"]').val();
            cmbtype = $.trim(cmbtype);


            var txtrelation = $thisList.find('[name="contactsWrap-relationship"]').val();
            txtrelation = $.trim(txtrelation);

            var txtcity = $thisList.find('[name="contactsWrap-city"]').val();
            txtcity = $.trim(txtcity);

            if(txtName && txtName != "" && cmbtype && cmbtype > 0 && txtrelation && txtrelation != ""){
                if(IsPostalFlag == "1") {
                    $('.contactsTabWrapper table tbody').append('<tr class="qualWrapper_li qualificationTabWrapper-li-' + $trLen + '" data-item-new="true"><td class="qual-td-addRemove"><span><a href="#" class="contactsAddRemoveLink addContactsQual">+</a> <a href="#" class="contactsAddRemoveLink removeContactsQual">-</a></span></td><td style="display:none"><input type="text" name="contactsWrap-sno" data-attr-id-number="" value="' + $trLen + '" readonly /></td><td><select style="margin-bottom:5px" name="contactsWrap-type" class="select-dropdown-width fields-mandatory" value=""><option selected="true" disabled="disabled">Please Select</option></select>&nbsp;<select name="contactsWrap-relationship" class="select-dropdown-width" style="border-color: #45cff0;"><option selected="true" disabled="disabled">Please Select</option></select></td><td><input type="text" name="contactsWrap-name" style="border-color: #45cff0;" maxlength="100" value="" /></td><td><div><input type="text" name="contactsWrap-address1" placeholder="Address 1" maxlength="100" value="" /><input type="text" name="contactsWrap-address2" placeholder="Address 2" maxlength="100" value="" /><div class="inline-block"><input type="text" name="contactsWrap-city" data-attr-city-id="" placeholder="City" disabled maxlength="100" value="" /><img class="postalWrapper-img citySearchImg" src="../../img/Default/search-icon.png" title="Search"></div></div><div><input type="text" name="contactsWrap-state" placeholder="County" maxlength="100" readonly value="" /><input type="text" name="contactsWrap-country" placeholder="Country" readonly maxlength="100" value="" /><div class="inline-block"><input type="text" name="contactsWrap-zipcode" placeholder="Postal Code" maxlength="100" value="" /></div><input type="text" name="contactsWrap-zip4" placeholder="Zip4" readonly style="display: none;" maxlength="100" value="" /></div></td><td><div class="block-elem"><input type="text" name="contactsWrap-phone" placeholder="Mobile" maxlength="100" value="" /></div><div class="block-elem"><input type="text" name="contactsWrap-home" placeholder="Home Phone" maxlength="100" value="" /></div></td><td><div class="block-elem"><input type="text" name="contactsWrap-email" placeholder="Email" maxlength="100" value="" /></div></td><td><input type="text" name="contactsWrap-notes" maxlength="100" value="" /></td></tr>');
                }
                else{
                    $('.contactsTabWrapper table tbody').append('<tr class="qualWrapper_li qualificationTabWrapper-li-' + $trLen + '" data-item-new="true"><td class="qual-td-addRemove"><span><a href="#" class="contactsAddRemoveLink addContactsQual">+</a> <a href="#" class="contactsAddRemoveLink removeContactsQual">-</a></span></td><td style="display:none"><input type="text" name="contactsWrap-sno" data-attr-id-number="" value="' + $trLen + '" readonly /></td><td><select style="margin-bottom:5px" name="contactsWrap-type" class="select-dropdown-width fields-mandatory" value=""><option selected="true" disabled="disabled">Please Select</option></select>&nbsp;<select name="contactsWrap-relationship" class="select-dropdown-width" style="border-color: #45cff0;"><option selected="true" disabled="disabled">Please Select</option></select></td><td><input type="text" name="contactsWrap-name" style="border-color: #45cff0;" maxlength="100" value="" /></td><td><div><input type="text" name="contactsWrap-address1" placeholder="Address 1" maxlength="100" value="" /><input type="text" name="contactsWrap-address2" placeholder="Address 2" maxlength="100" value="" /><input type="text" name="contactsWrap-city" data-attr-city-id="" placeholder="City" readonly maxlength="100" readonly value="" /></div><div><input type="text" name="contactsWrap-state" placeholder="County" maxlength="100" readonly value="" /><input type="text" name="contactsWrap-country" placeholder="Country" readonly maxlength="100" value="" /><div class="inline-block"><input type="text" name="contactsWrap-zipcode" placeholder="Postal Code" maxlength="100" disabled value="" /><img class="postalWrapper-img zipSearchImg" src="../../img/Default/search-icon.png" title="Search"></div><input type="text" name="contactsWrap-zip4" placeholder="Zip4" readonly style="display: none;" maxlength="100" value="" /></div></td><td><div class="block-elem"><input type="text" name="contactsWrap-phone" placeholder="Mobile" maxlength="100" value="" /></div><div class="block-elem"><input type="text" name="contactsWrap-home" placeholder="Home Phone" maxlength="100" value="" /></div></td><td><div class="block-elem"><input type="text" name="contactsWrap-email" placeholder="Email" maxlength="100" value="" /></div></td><td><input type="text" name="contactsWrap-notes" maxlength="100" value="" /></td></tr>');
                }
                modifySerialNumber('contactsTabWrapper');
                appendRelationshipsData($('.qualificationTabWrapper-li-' + $trLen));
                appendcommunicationsData($('.qualificationTabWrapper-li-' + $trLen));
            }
            else{
                $('.customAlert').append('<div class="alert alert-danger">Please fill all the Required Fields</div>');
            }
        }
        else{
            if(IsPostalFlag == "1") {
                $('.contactsTabWrapper table tbody').append('<tr class="qualWrapper_li qualificationTabWrapper-li-' + $trLen + '" data-item-new="true"><td class="qual-td-addRemove"><span><a href="#" class="contactsAddRemoveLink addContactsQual">+</a> <a href="#" class="contactsAddRemoveLink removeContactsQual">-</a></span></td><td style="display:none"><input type="text" name="contactsWrap-sno" data-attr-id-number="" value="' + $trLen + '" readonly /></td><td><select style="margin-bottom:5px" name="contactsWrap-type" class="select-dropdown-width fields-mandatory" value=""><option selected="true" disabled="disabled">Please Select</option></select>&nbsp;<select name="contactsWrap-relationship" class="select-dropdown-width" style="border-color: #45cff0;"><option selected="true" disabled="disabled">Please Select</option></select></td><td><input type="text" name="contactsWrap-name" style="border-color: #45cff0;" maxlength="100" value="" /></td><td><div><input type="text" name="contactsWrap-address1" placeholder="Address 1" maxlength="100" value="" /><input type="text" name="contactsWrap-address2" placeholder="Address 2" maxlength="100" value="" /><div class="inline-block"><input type="text" name="contactsWrap-city" data-attr-city-id="" placeholder="City" maxlength="100" disabled value="" /><img class="postalWrapper-img citySearchImg" src="../../img/Default/search-icon.png" title="Search"></div><div><input type="text" name="contactsWrap-state" placeholder="County" maxlength="100" readonly value="" /><input type="text" name="contactsWrap-country" placeholder="Country" readonly maxlength="100" value="" /><div class="inline-block"><input type="text" name="contactsWrap-zipcode" placeholder="Postal Code" maxlength="100" value="" /></div><input type="text" name="contactsWrap-zip4" placeholder="Zip4" readonly style="display: none;" maxlength="100" value="" /></div></td><td><div class="block-elem"><input type="text" name="contactsWrap-phone" placeholder="Mobile" maxlength="100" value="" /></div><div class="block-elem"><input type="text" name="contactsWrap-home" placeholder="Home Phone" maxlength="100" value="" /></div></td><td><div class="block-elem"><input type="text" name="contactsWrap-email" placeholder="Email" maxlength="100" value="" /></div></td><td><input type="text" name="contactsWrap-notes" maxlength="100" value="" /></td></tr>');
            }
            else{
                $('.contactsTabWrapper table tbody').append('<tr class="qualWrapper_li qualificationTabWrapper-li-' + $trLen + '" data-item-new="true"><td class="qual-td-addRemove"><span><a href="#" class="contactsAddRemoveLink addContactsQual">+</a> <a href="#" class="contactsAddRemoveLink removeContactsQual">-</a></span></td><td style="display:none"><input type="text" name="contactsWrap-sno" data-attr-id-number="" value="' + $trLen + '" readonly /></td><td><select style="margin-bottom:5px" name="contactsWrap-type" class="select-dropdown-width fields-mandatory" value=""><option selected="true" disabled="disabled">Please Select</option></select>&nbsp;<select name="contactsWrap-relationship" class="select-dropdown-width" style="border-color: #45cff0;"><option selected="true" disabled="disabled">Please Select</option></select></td><td><input type="text" name="contactsWrap-name" style="border-color: #45cff0;" maxlength="100" value="" /></td><td><div><input type="text" name="contactsWrap-address1" placeholder="Address 1" maxlength="100" value="" /><input type="text" name="contactsWrap-address2" placeholder="Address 2" maxlength="100" value="" /><input type="text" name="contactsWrap-city" data-attr-city-id="" placeholder="City" readonly maxlength="100" readonly value="" /></div><div><input type="text" name="contactsWrap-state" placeholder="County" maxlength="100" readonly value="" /><input type="text" name="contactsWrap-country" placeholder="Country" readonly maxlength="100" value="" /><div class="inline-block"><input type="text" name="contactsWrap-zipcode" placeholder="Postal Code" maxlength="100" disabled value="" /><img class="postalWrapper-img zipSearchImg" src="../../img/Default/search-icon.png" title="Search"></div><input type="text" name="contactsWrap-zip4" placeholder="Zip4" readonly style="display: none;" maxlength="100" value="" /></div></td><td><div class="block-elem"><input type="text" name="contactsWrap-phone" placeholder="Mobile" maxlength="100" value="" /></div><div class="block-elem"><input type="text" name="contactsWrap-home" placeholder="Home Phone" maxlength="100" value="" /></div></td><td><div class="block-elem"><input type="text" name="contactsWrap-email" placeholder="Email" maxlength="100" value="" /></div></td><td><input type="text" name="contactsWrap-notes" maxlength="100" value="" /></td></tr>');
            }
            modifySerialNumber('contactsTabWrapper');
            appendRelationshipsData($('.qualificationTabWrapper-li-' + $trLen));
            appendcommunicationsData($('.qualificationTabWrapper-li-' + $trLen));
        }
    });
    $('body').on('click', '.contactsAddRemoveLink.removeContactsQual', function() {
        var removeItemId = parseInt($(this).closest('tr').find('[name="contactsWrap-sno"]').attr('data-attr-id-number'));
        var obj = {
            "id": removeItemId,
            "modifiedBy": sessionStorage.userId,
            "isActive": 0,
            "isDeleted": 0
        };
        $removeItem = $(this);
        createAjaxObject(dataTabURL, obj, "DELETE", onDeleteItem, onError);
    });

    $("#imgPhoto").error(function() {
        onshowImage();
    });

    var $medicalConditionTargetElem;
    $(document).on('click', '.medicalCondition-search', function(e) {
        e.preventDefault();
        $medicalConditionTargetElem = $(this);
        var popW = 800;
        var popH = 500;

        var profileLbl;
        var devModelWindowWrapper = new kendoWindowWrapper();
        profileLbl = "Illness List";
        devModelWindowWrapper.openPageWindow("../../html/masters/medicalConditionList.html", profileLbl, popW, popH, true, closeMedicalConditionAction);
    });

    function closeMedicalConditionAction(evt, returnData) {
        if (returnData && returnData.status == "success") {
            var $elem = $medicalConditionTargetElem.closest('tr');
            if (!$elem.attr('data-item-new') == true && !$elem.attr('data-item-edited') == true) {
                $elem.attr("data-item-edited", true);
            }
            $elem.find('input[name="illnessWrap-condition"]').val(returnData.selItem.value);
            $elem.find('input[name="illnessWrap-sno"]').attr('data-attr-id-number', returnData.selItem.idk);
        }
    }

    var $allergyTargetElem;
    $(document).on('click', '.allergy-search', function(e) {
        e.preventDefault();
        $allergyTargetElem = $(this);
        var popW = 800;
        var popH = 500;

        var profileLbl;
        var devModelWindowWrapper = new kendoWindowWrapper();
        profileLbl = "Allergy List";
        devModelWindowWrapper.openPageWindow("../../html/masters/addAllergyList.html", profileLbl, popW, popH, true, closeAllergyAction);
    });

    function closeAllergyAction(evt, returnData) {
        if (returnData && returnData.status == "success") {
            var $elem = $allergyTargetElem.closest('tr');
            if (!$elem.attr('data-item-new') == true && !$elem.attr('data-item-edited') == true) {
                $elem.attr("data-item-edited", true);
            }
            $elem.find('input[name="allergiesWrap-condition"]').val(returnData.selItem.value);
            $elem.find('input[name="allergiesWrap-sno"]').attr('data-attr-id-number', returnData.selItem.idk);
        }
    }

    var $dietTargetElem;
    $(document).on('click', '.diet-search', function(e) {
        e.preventDefault();
        $dietTargetElem = $(this);
        var popW = 800;
        var popH = 500;

        var profileLbl;
        var devModelWindowWrapper = new kendoWindowWrapper();
        profileLbl = "Diet List";
        devModelWindowWrapper.openPageWindow("../../html/masters/addDietList.html", profileLbl, popW, popH, true, closeDietAction);
    });

    function closeDietAction(evt, returnData) {
        if (returnData && returnData.status == "success") {
            var $elem = $dietTargetElem.closest('tr');
            if (!$elem.attr('data-item-new') == true && !$elem.attr('data-item-edited') == true) {
                $elem.attr("data-item-edited", true);
            }
            $elem.find('input[name="dietWrap-condition"]').val(returnData.selItem.value);
            $elem.find('input[name="dietWrap-sno"]').attr('data-attr-id-number', returnData.selItem.idk);
        }
    }
    $(document).on('keyup change', '.qualWrapper_li input, .qualWrapper_li select', function() {
        if (!$(this).closest('tr').attr('data-item-edited') == true && !$(this).closest('tr').attr('data-item-new') == true) {
            $(this).closest('tr').attr('data-item-edited', true);
        }
    });
    //$('#txtSSN1').off('keydown keyup mousedown mouseup');
    $('#txtSSN1').on('keydown keyup mousedown mouseup', function() {
        // var result = showSSNNumver(this.value);
        //$("#txtSSN").val(result); //spits the value into the input
    });
    /*$('#txtHPhone1').off('keydown keyup mousedown mouseup');
    $('#txtHPhone1').on('keydown keyup mousedown mouseup', function() {
        var result = showHomeNumver(this.value);
        $("#txtHPhone").val(result); //spits the value into the input
    });*/
}

function modifySerialNumber(elem) {
    $('.medicalConditionAlert .alert').remove();
    $('.' + elem + ' [data-attr-id-number]').each(function(i) {
        $(this).val(i + 1);
    });
}

function onSaveMedicalCondition(e) {
    e.preventDefault();
    var multipleConditionValues = [];
    var multipleConditionUpdateValues = [];
    $createItem = $('input[name="illnessWrap-condition"]');
    $createItem.each(function() {
        var obj = {
            "patientId": patientId,
            "medicalConditionTypeId": parseInt($(this).closest('tr').find('input').attr('data-attr-id-number')),
            "notes": $(this).closest('tr').find('input[name="illnessWrap-notes"]').val(),
            "remarks": $(this).closest('tr').find('input[name="illnessWrap-remarks"]').val(),
            "isActive": 1,
            "isDeleted": 0
        }
        if ($(this).closest('tr').attr('data-item-edited') == "true") {
            obj.modifiedBy = sessionStorage.userId;
            obj.id = parseInt($(this).closest('tr').find('input').attr('data-attr-id-patient-number'));
            multipleConditionUpdateValues.push(obj);
        } else if ($(this).closest('tr').attr('data-item-new') == "true") {
            obj.createdBy = sessionStorage.userId;
            multipleConditionValues.push(obj);
        }
    });
    if (multipleConditionValues.length) {
        saveorupdate = ADD;
        createAjaxObjectAsync(dataTabURL + 'batch/', multipleConditionValues, "POST", onSuccessCreate, onError);
    }
    if (multipleConditionUpdateValues.length) {
        saveorupdate = UPDATE;
        createAjaxObjectAsync(dataTabURL + 'batch/', multipleConditionUpdateValues, "PUT", onSuccessCreate, onError);
    }
}

function onSaveAllergy(e) {
    e.preventDefault();
    var multipleConditionValues = [];
    var multipleConditionUpdateValues = [];
    $createItem = $('input[name="allergiesWrap-condition"]');
    $createItem.each(function() {
        var obj = {
            "patientId": patientId,
            "allergyTypeId": parseInt($(this).closest('tr').find('input').attr('data-attr-id-number')),
            "notes": $(this).closest('tr').find('input[name="allergiesWrap-notes"]').val(),
            "remarks": $(this).closest('tr').find('input[name="allergiesWrap-remarks"]').val(),
            "isActive": 1,
            "isDeleted": 0
        }
        if ($(this).closest('tr').attr('data-item-edited') == "true") {
            obj.modifiedBy = sessionStorage.userId;
            obj.id = parseInt($(this).closest('tr').find('input').attr('data-attr-id-patient-number'));
            multipleConditionUpdateValues.push(obj);
        } else if ($(this).closest('tr').attr('data-item-new') == "true") {
            obj.createdBy = sessionStorage.userId;
            multipleConditionValues.push(obj);
        }
    });
    if (multipleConditionValues.length) {
        saveorupdate = ADD;
        createAjaxObjectAsync(dataTabURL + 'batch/', multipleConditionValues, "POST", onSuccessCreate, onError);
    }
    if (multipleConditionUpdateValues.length) {
        saveorupdate = UPDATE;
        createAjaxObjectAsync(dataTabURL + 'batch/', multipleConditionUpdateValues, "PUT", onSuccessCreate, onError);
    }
}

function onSaveDiet(e) {
    e.preventDefault();
    var multipleConditionValues = [];
    var multipleConditionUpdateValues = [];
    $createItem = $('input[name="dietWrap-condition"]');
    $createItem.each(function() {
        var obj = {
            "patientId": patientId,
            "dietTypeId": parseInt($(this).closest('tr').find('input').attr('data-attr-id-number')),
            "notes": $(this).closest('tr').find('input[name="dietWrap-notes"]').val(),
            "remarks": $(this).closest('tr').find('input[name="dietWrap-remarks"]').val(),
            "isActive": 1,
            "isDeleted": 0
        };
        if ($(this).closest('tr').attr('data-item-edited') == "true") {
            obj.modifiedBy = sessionStorage.userId;
            obj.id = parseInt($(this).closest('tr').find('input').attr('data-attr-id-patient-number'));
            multipleConditionUpdateValues.push(obj);
        } else if ($(this).closest('tr').attr('data-item-new') == "true") {
            obj.createdBy = sessionStorage.userId;
            multipleConditionValues.push(obj);
        }
    });
    if (multipleConditionValues.length) {
        saveorupdate = ADD;
        createAjaxObjectAsync(dataTabURL + 'batch/', multipleConditionValues, "POST", onSuccessCreate, onError);
    }
    if (multipleConditionUpdateValues.length) {
        saveorupdate = UPDATE;
        createAjaxObjectAsync(dataTabURL + 'batch/', multipleConditionUpdateValues, "PUT", onSuccessCreate, onError);
    }
}

function onSaveBilling(e) {
    $('.medicalConditionAlert:visible .alert').remove();
    $('.customAlert .alert').remove();
    e.preventDefault();
    var multipleConditionValues = [];
    var multipleConditionUpdateValues = [];

    var ind = $('.billingTabWrapper table tbody tr').length;
    //var ind = 1;
    var $thisList = $('.qualificationTabWrapper-li-' + (ind-1));

    var txtName = $thisList.find('[name="billingWrap-name"]').val();
    txtName = $.trim(txtName);

    var txtAddress1 = $thisList.find('[name="billingWrap-address1"]').val();
    txtAddress1 = $.trim(txtAddress1);

    var txtzipcode = $thisList.find('[name="billingWrap-zipcode"]').val();
    txtzipcode = $.trim(txtzipcode);

    var cmbtype = $thisList.find('[name="billingWrap-type"]').val();
    cmbtype = $.trim(cmbtype);

    var txtrelation = $thisList.find('[name="billingWrap-relationship"]').val();
    txtrelation = $.trim(txtrelation);

    var txtcity = $thisList.find('[name="billingWrap-city"]').val();
    txtcity = $.trim(txtcity);

    var txtHouseNumber = "";

    if(IsPostalFlag == "1") {
        txtHouseNumber = txtzipcode;
    }

    if(txtName && txtName != "" && txtAddress1 && txtAddress1 != "" && ((IsPostalFlag == "0" && txtzipcode && txtzipcode != "") || (IsPostalFlag == "1" && txtcity && txtcity != "")) && cmbtype && cmbtype > 0 && txtrelation && txtrelation != "") {
        $createItem = $('input[name="billingWrap-sno"]');
        $createItem.each(function() {
            var obj = {
                "patientId": patientId,
                "name": $(this).closest('tr').find('input[name="billingWrap-name"]').val(),
                "relationship": $(this).closest('tr').find('select[name="billingWrap-relationship"]').val(),
                "notes": $(this).closest('tr').find('input[name="billingWrap-notes"]').val(),
                "remarks": $(this).closest('tr').find('input[name="billingWrap-remarks"]').val(),
                "communicationTypeId": $(this).closest('tr').find('select[name="billingWrap-type"]').val(),
                "createdBy" : $(this).closest('tr').find('input[name="billingWrap-createdBy"]').val(),
                //if($(this).closest('tr').find('input[name="billingWrap-hourlybillrate"]').val() != "")
                //{
                //"ratePerHour": $(this).closest('tr').find('input[name="billingWrap-hourlybillrate"]').val(),
                //}


                "contactType" : billCommunitionType,
                "isActive": 1,
                "isDeleted": 0
            };
            if ($(this).closest('tr').attr('data-item-edited') == "true") {
                obj.modifiedBy = sessionStorage.userId;
                obj.id = parseInt($(this).closest('tr').find('input').attr('data-attr-id-number'));
                var ratePerHour = $(this).closest('tr').find('input[name="billingWrap-hourlybillrate"]').val();
                if(ratePerHour != "")
                {
                    obj.ratePerHour = ratePerHour;
                }
                else{
                    obj.ratePerHour = null;
                }
                multipleConditionUpdateValues.push(obj);
            } else if ($(this).closest('tr').attr('data-item-new') == "true") {
                var ratePerHour = $(this).closest('tr').find('input[name="billingWrap-hourlybillrate"]').val();
                if(ratePerHour != "")
                {
                    obj.ratePerHour = ratePerHour;
                }
                else{
                    obj.ratePerHour = null;
                }
                obj.createdBy = sessionStorage.userId;
                multipleConditionValues.push(obj);
            }
        });
        if (multipleConditionValues.length) {
            saveorupdate = ADD;
            createAjaxObjectAsync(dataTabURL + 'batch/', multipleConditionValues, "POST", onSuccessCreate, onError);
        }
        if (multipleConditionUpdateValues.length) {
            saveorupdate = UPDATE;
            createAjaxObjectAsync(dataTabURL + 'batch/', multipleConditionUpdateValues, "PUT", onSuccessCreate, onError);
        }
    }
    else {
        $('.customAlert').append('<div class="alert alert-danger">Please fill all the Required Fields</div>');
    }
}
function onSaveContacts(e) {
    $('.medicalConditionAlert:visible .alert').remove();
    $('.customAlert .alert').remove();
    e.preventDefault();
    var multipleConditionValues = [];
    var multipleConditionUpdateValues = [];
    var ind =  $('.contactsTabWrapper table tbody tr').length;
    var $thisList = $('.qualificationTabWrapper-li-' + (ind-1) );

    var txtName = $thisList.find('[name="contactsWrap-name"]').val();
    txtName = $.trim(txtName);

    var txtAddress1 = $thisList.find('[name="contactsWrap-address1"]').val();
    txtAddress1 = $.trim(txtAddress1);

    var txtzipcode = $thisList.find('[name="contactsWrap-zipcode"]').val();
    txtzipcode = $.trim(txtzipcode);

    var cmbtype = $thisList.find('[name="contactsWrap-type"]').val();
    cmbtype = $.trim(cmbtype);

    var txtrelation = $thisList.find('[name="contactsWrap-relationship"]').val();
    txtrelation = $.trim(txtrelation);


    var txtcity = $thisList.find('[name="billingWrap-city"]').val();
    txtcity = $.trim(txtcity);

    if(txtName && txtName != "" && cmbtype && cmbtype > 0 && txtrelation && txtrelation != ""){
        $createItem = $('input[name="contactsWrap-sno"]');
        $createItem.each(function() {
            var obj = {
                "patientId": patientId,
                "name": $(this).closest('tr').find('input[name="contactsWrap-name"]').val(),
                "relationship": $(this).closest('tr').find('select[name="contactsWrap-relationship"]').val(),
                "notes": $(this).closest('tr').find('input[name="contactsWrap-notes"]').val(),
                "remarks": $(this).closest('tr').find('input[name="contactsWrap-remarks"]').val(),
                "communicationTypeId": $(this).closest('tr').find('select[name="contactsWrap-type"]').val(),
                "createdBy" : $(this).closest('tr').find('input[name="contactsWrap-createdBy"]').val(),
                "contactType" : contactCommunitionType,
                "isActive": 1,
                "isDeleted": 0
            };
            if ($(this).closest('tr').attr('data-item-edited') == "true") {
                obj.modifiedBy = sessionStorage.userId;
                //obj.createdBy = $(this).closest('tr').find('input').attr('data-attr-createdBy');
                obj.id = parseInt($(this).closest('tr').find('input').attr('data-attr-id-number'));
                multipleConditionUpdateValues.push(obj);
            } else if ($(this).closest('tr').attr('data-item-new') == "true") {
                obj.createdBy = sessionStorage.userId;
                multipleConditionValues.push(obj);
            }
        });
        if (multipleConditionValues.length) {
            saveorupdate = ADD;
            createAjaxObjectAsync(dataTabURL + 'batch/', multipleConditionValues, "POST", onSuccessCreate, onError);
        }
        if (multipleConditionUpdateValues.length) {
            saveorupdate = UPDATE;
            createAjaxObjectAsync(dataTabURL + 'batch/', multipleConditionUpdateValues, "PUT", onSuccessCreate, onError);
        }
    }
    else{

        $('.customAlert').append('<div class="alert alert-danger">Please fill all the Required Fields</div>');
    }
}

function onSuccessCreate(dataObj) {
    console.log(dataObj);
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            if(showTab == "illnessTab" || showTab == "allergiesTab" || showTab == "dietTab") {
                $createItem.each(function() {
                    $(this).closest('tr').removeAttr('data-item-edited data-item-new');
                });
            }
            if (showTab == "illnessTab") {
                if ($('.illnessTabWrapper').find('table tr').length > 1) {
                    $('.illnessAddRemoveLink').addClass('visibility-hidden');
                }
                getAjaxObject(dataTabURL + '?is-active=1&fields=*,medicalConditionType.*&patientId='+patientId, "GET", getIllnessData, onError);
              //  getAjaxObject(dataTabURL + '?is-active=1&patientId='+patientId, "GET", getIllnessData, onError);
                if (saveorupdate == ADD) {
                    $('.medicalConditionAlert:visible').prepend('<div class="alert alert-success">Illness saved successfully</div>');
                } else if (saveorupdate == UPDATE) {
                    $('.medicalConditionAlert:visible').prepend('<div class="alert alert-success">Illness updated successfully</div>');
                }
            }
            if (showTab == "allergiesTab") {
                if ($('.allergiesTabWrapper').find('table tr').length > 1) {
                    $('.allergiesAddRemoveLink').addClass('visibility-hidden');
                }
                getAjaxObject(dataTabURL + '?is-active=1&fields=*,allergyType.*&patientId='+patientId, "GET", getAllergiesData, onError);
                //getAjaxObject(dataTabURL + '?is-active=1&patientId='+patientId, "GET", getAllergiesData, onError);
                if (saveorupdate == ADD) {
                    $('.medicalConditionAlert:visible').prepend('<div class="alert alert-success">Allergies saved successfully</div>');
                } else if (saveorupdate == UPDATE) {
                    $('.medicalConditionAlert:visible').prepend('<div class="alert alert-success">Allergies updated successfully</div>');
                }
            }
            if (showTab == "dietTab") {
                if ($('.dietTabWrapper').find('table tr').length > 1) {
                    $('.dietAddRemoveLink').addClass('visibility-hidden');
                }

                //getAjaxObject(dataTabURL + '?is-active=1&patientId='+patientId, "GET", getDietsData, onError);
                if (saveorupdate == ADD) {
                    $('.medicalConditionAlert:visible').prepend('<div class="alert alert-success">Diets saved successfully</div>');
                } else if (saveorupdate == UPDATE) {
                    $('.medicalConditionAlert:visible').prepend('<div class="alert alert-success">Diets updated successfully</div>');
                }
            }
            if (showTab == "billingTab") {
                if ($('.billingTabWrapper').find('table tr').length > 1) {
                    $('.billingAddRemoveLink').addClass('visibility-hidden');
                }
               // getAjaxObject(dataTabURL + '?is-active=1&communication-type-id=:in:201&patient-Id='+patientId, "GET", getPatientBillingData, onError);
               //   getAjaxObject(dataTabURL + '?is-active=1&communicationTypeId=201&patient-Id='+patientId, "GET", getPatientRelationsData, onError);
                getAjaxObject(dataTabURL + '?is-active=1&contactType='+ billCommunitionType +'&patient-Id='+patientId, "GET", getPatientRelationsData, onError);
                function getPatientRelationsData(dataObj) {
                    if (dataObj && dataObj.response && dataObj.response.status) {
                        var dataArray = dataObj.response.patientRelationships;
                        if (dataObj.response.status.code == "1" && dataArray) {
                            var updateArr = [];
                            var updateCommArr = [];
                            var parentIdArr = [];
                            for (var i = 0; i < dataArray.length; i++) {
                                parentIdArr.push(dataArray[i].id);
                            }
                            $createItem.each(function(i) {
                                if(($(this).closest('tr').find('input[name="billingWrap-address1"]').val() != "") ||
                                    ($(this).closest('tr').find('input[name="billingWrap-city"]').attr('data-attr-city-id').val() != "") ||
                                    ($(this).closest('tr').find('input[name="billingWrap-email"]').val() != "") ||
                                    ($(this).closest('tr').find('input[name="billingWrap-address2"]').val() != "") ||
                                    ($(this).closest('tr').find('input[name="billingWrap-phone"]').val() != "") ||
                                    ($(this).closest('tr').find('input[name="billingWrap-home"]').val() != "") ||
                                    ($(this).closest('tr').find('input[name="billingWrap-zipcode"]').val() != "")
                                  ) {
                                    var obj = {
                                        "defaultCommunication": 1,
                                        "cityId": parseInt($(this).closest('tr').find('input[name="billingWrap-city"]').attr('data-attr-city-id')),
                                        "isActive": 1,
                                        "isDeleted": 0,
                                        "sms": "yes",
                                        "fax": null,
                                        "email": $(this).closest('tr').find('input[name="billingWrap-email"]').val(),
                                        "parentId": dataArray[i] ? dataArray[i].id : null,
                                        "parentTypeId": dataArray[i] ? dataArray[i].communicationTypeId : null,
                                        "address2": $(this).closest('tr').find('input[name="billingWrap-address2"]').val(),
                                        "address1": $(this).closest('tr').find('input[name="billingWrap-address1"]').val(),
                                        "cellPhone": $(this).closest('tr').find('input[name="billingWrap-phone"]').val(),
                                        "homePhone": $(this).closest('tr').find('input[name="billingWrap-home"]').val(),
                                        "createdBy": dataArray[i] ? dataArray[i].createdBy : null,
                                        "houseNumber": IsPostalFlag == "1" ? $(this).closest('tr').find('input[name="billingWrap-zipcode"]').val() : null
                                        //"contactType" : billCommunitionType,
                                        //"id" : dataArray[i].communication ? dataArray[i].communication[0].id : null,
                                    };

                                    if ($(this).closest('tr').attr('data-item-edited') == "true") {
                                        obj.modifiedBy = sessionStorage.userId;
                                        //obj.id = parseInt($(this).closest('tr').find('input').attr('data-attr-id-number'));
                                        obj.id = dataArray[i].communication ? dataArray[i].communication[0].id : null,
                                            updateCommArr.push(obj);

                                    } else if ($(this).closest('tr').attr('data-item-new') == "true") {
                                        obj.createdBy = sessionStorage.userId;
                                        updateArr.push(obj);

                                    }
                                }
                            });

                            if(updateCommArr.length){
                                saveorupdate = UPDATE;
                                createAjaxObjectAsync(ipAddress + '/homecare/communications/batch/', updateCommArr, "PUT", onSuccessCreateAnother, onError);
                            }

                            if(updateArr.length){
                                saveorupdate = ADD;
                                createAjaxObjectAsync(ipAddress + '/homecare/communications/batch/', updateArr, "POST", onSuccessCreateAnother, onError);
                            }
                        } else {
                            customAlert.error("error", dataObj.response.status.message);
                        }
                    }
                }
            }
            if (showTab == "contactsTab") {
                if ($('.contactsTabWrapper').find('table tr').length > 1) {
                    $('.contactsAddRemoveLink').addClass('visibility-hidden');
                }
                //getAjaxObject(dataTabURL + '?is-active=1&communicationTypeId=203', "GET", getPatientRelationsData, onError);
                getAjaxObject(dataTabURL + '?is-active=1&patient-Id='+patientId +'&contactType='+ contactCommunitionType, "GET", getPatientRelationsData, onError);
                function getPatientRelationsData(dataObj) {
                    if (dataObj && dataObj.response && dataObj.response.status) {
                        var dataArray = dataObj.response.patientRelationships;
                        if (dataObj.response.status.code == "1" && dataArray) {
                            var updateArr = [];
                            var updateCommArr = [];
                            var parentIdArr = [];
                            for (var i = 0; i < dataArray.length; i++) {
                                parentIdArr.push(dataArray[i].id);

                            }

                            //var updateArr = [];
                            //var updateCommArr = [];
                            $createItem.each(function(i) {
                                var obj = {
                                    "defaultCommunication": 1,
                                    //"cityId": parseInt($(this).closest('tr').find('input').attr('data-attr-city-id')),
                                    "cityId": parseInt($(this).closest('tr').find('input[name="contactsWrap-city"]').attr('data-attr-city-id')),
                                    "isActive": 1,
                                    "isDeleted": 0,
                                    "sms": "yes",
                                    "fax": null,
                                    "email": $(this).closest('tr').find('input[name="contactsWrap-email"]').val(),
                                    "parentId": dataArray[i] ? dataArray[i].id : null,
                                    //"parentTypeId": parseInt($(this).closest('tr').find('select[name="contactsWrap-type"]').val()),
                                    "parentTypeId": dataArray[i] ? dataArray[i].communicationTypeId : null,
                                    "address2": $(this).closest('tr').find('input[name="contactsWrap-address2"]').val(),
                                    "address1": $(this).closest('tr').find('input[name="contactsWrap-address1"]').val(),
                                    "cellPhone": $(this).closest('tr').find('input[name="contactsWrap-phone"]').val(),
                                    "homePhone": $(this).closest('tr').find('input[name="contactsWrap-home"]').val(),
                                    "createdBy" : dataArray[i] ? dataArray[i].createdBy : null,
                                    "houseNumber" : IsPostalFlag == "1" ? $(this).closest('tr').find('input[name="contactsWrap-zipcode"]').val():null
                                    //"contactType" : contactCommunitionType

                                };
                                if ($(this).closest('tr').attr('data-item-edited') == "true") {
                                    obj.modifiedBy = sessionStorage.userId;
                                    obj.id = dataArray[i].communication ? dataArray[i].communication[0].id : null,
                                        //obj.id = parseInt($(this).closest('tr').find('input').attr('data-attr-id-number'));
                                        updateCommArr.push(obj);
                                    console.log('updateArr', updateArr);
                                } else if ($(this).closest('tr').attr('data-item-new') == "true") {
                                    obj.createdBy = sessionStorage.userId;
                                    updateArr.push(obj);
                                    console.log('updateCommArr', updateCommArr);
                                }
                            });

                            if(updateCommArr.length){
                                saveorupdate = UPDATE;
                                createAjaxObjectAsync(ipAddress + '/homecare/communications/batch/', updateCommArr, "PUT", onSuccessCreateAnother, onError);
                            }

                            if(updateArr.length){
                                saveorupdate = ADD;
                                createAjaxObjectAsync(ipAddress + '/homecare/communications/batch/', updateArr, "POST", onSuccessCreateAnother, onError);
                            }

                        } else {
                            customAlert.error("error", dataObj.response.status.message);
                        }
                    }
                }
            }
        } else {
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}

function onSuccessCreateAnother(dataObj) {
    console.log(dataObj);
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            $createItem.each(function() {
                $(this).closest('tr').removeAttr('data-item-edited data-item-new');
            });
            if (showTab == "billingTab") {
                getAjaxObject(dataTabURL + '?is-active=1&contactType='+ billCommunitionType +'&patient-Id='+patientId+"&fields=*", "GET", getPatientBillingData, onError);
            	// getAjaxObject(dataTabURL + '?is-active=1&communication-type-id=:in:201&patient-Id='+patientId+"&fields=*", "GET", getPatientBillingData, onError);
               // getAjaxObject(dataTabURL + '?is-active=1&communicationTypeId=201&patientId='+patientId, "GET", getPatientBillingData, onError);
                if (saveorupdate == ADD) {
                    $('.medicalConditionAlert:visible').prepend('<div class="alert alert-success">Billing saved successfully</div>');
                } else if (saveorupdate == UPDATE) {
                    $('.medicalConditionAlert:visible').prepend('<div class="alert alert-success">Billing updated successfully</div>');
                }
            }
            if (showTab == "contactsTab") {
                getAjaxObject(dataTabURL+ '?is-active=1&contactType='+ contactCommunitionType +'&patientId='
                    + patientId, "GET", getPatientContactsData, onError);
               // getAjaxObject(dataTabURL + '?is-active=1&communicationTypeId=:bt:202,220&patientId='+patientId, "GET", getPatientContactsData, onError);
                if (saveorupdate == ADD) {
                    $('.medicalConditionAlert:visible').prepend('<div class="alert alert-success">Contacts saved successfully</div>');
                } else if (saveorupdate == UPDATE) {
                    $('.medicalConditionAlert:visible').prepend('<div class="alert alert-success">Contacts updated successfully</div>');
                }
            }
        } else {
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}

function getIllnessData(dataObj) {
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            var dataArray = dataObj.response.patientMedicalConditions || [];
            $('.illnessTabWrapper').find('.qualWrapper_li').remove();
            if (dataArray.length) {
                $('.illnessAddRemoveLink').addClass('visibility-hidden');
            } else if(dataArray.length == 0) {
                $('.medicalConditionAlert:visible').prepend('<div class="alert alert-warning">No Records Found</div>');
            }
            for (var i = 0; i < dataArray.length; i++) {
                var ind = i + 1;
                $('.illnessTabWrapper').find('table tbody').append('<tr class="qualWrapper_li qualificationTabWrapper-li-' + ind + '"><td class="qual-td-addRemove"><span><a href="#" class="illnessAddRemoveLink addIllnessQual">+</a> <a href="#" class="illnessAddRemoveLink removeIllnessQual">-</a></span></td><td><input type="text" name="illnessWrap-sno" data-attr-id-number="' + dataArray[i].medicalConditionTypeId + '" data-attr-id-patient-number="' + dataArray[i].id + '" value="' + ind + '" readonly=""></td><td><input type="text" name="illnessWrap-condition" id="qualWrapQualification" readonly="" value="' + dataArray[i].medicalConditionTypeValue + '"><button class="btn btn-primary medicalCondition-search patient-search-img"><img src="../../img/search-icon-white.png" class="tradeFormWrapper-searchName-img"></button></td><td><input type="text" name="illnessWrap-notes" maxlength="100" value="' + dataArray[i].notes + '"></td><td><input type="text" name="illnessWrap-remarks" maxlength="100" value="' + dataArray[i].remarks + '"></td></tr>');
            }
        } else {
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}

function getAllergiesData(dataObj) {
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            var dataArray = dataObj.response.patientAllergies || [];
            $('.allergiesTabWrapper').find('.qualWrapper_li').remove();
            if (dataArray.length) {
                $('.allergiesAddRemoveLink').addClass('visibility-hidden');
            } else if(dataArray.length == 0) {
                $('.medicalConditionAlert:visible').prepend('<div class="alert alert-warning">No Records Found</div>');
            }
            for (var i = 0; i < dataArray.length; i++) {
                var ind = i + 1;
                $('.allergiesTabWrapper').find('table tbody').append('<tr class="qualWrapper_li qualificationTabWrapper-li-' + ind + '"><td class="qual-td-addRemove"><span><a href="#" class="allergiesAddRemoveLink addAllergiesQual">+</a> <a href="#" class="allergiesAddRemoveLink removeAllergiesQual">-</a></span></td><td><input type="text" name="allergiesWrap-sno" data-attr-id-number="' + dataArray[i].allergyTypeId + '" data-attr-id-patient-number="' + dataArray[i].id + '" value="' + ind + '" readonly=""></td><td><input type="text" name="allergiesWrap-condition" id="qualWrapQualification" readonly="" value="' + dataArray[i].allergyTypeValue + '"><button class="btn btn-primary allergy-search patient-search-img"><img src="../../img/search-icon-white.png" class="tradeFormWrapper-searchName-img"></button></td><td><input type="text" name="allergiesWrap-notes" maxlength="100" value="' + dataArray[i].notes + '"></td><td><input type="text" name="allergiesWrap-remarks" maxlength="100" value="' + dataArray[i].remarks + '"></td></tr>');
            }
        } else {
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}

function getDietsData(dataObj) {
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            var dataArray = dataObj.response.patientDiets || [];
            $('.dietTabWrapper').find('.qualWrapper_li').remove();
            if (dataArray.length) {
                $('.dietAddRemoveLink').addClass('visibility-hidden');
            } else if(dataArray.length == 0) {
                $('.medicalConditionAlert:visible').prepend('<div class="alert alert-warning">No Records Found</div>');
            }
            for (var i = 0; i < dataArray.length; i++) {
                var ind = i + 1;
                $('.dietTabWrapper').find('table tbody').append('<tr class="qualWrapper_li qualificationTabWrapper-li-' + ind + '"><td class="qual-td-addRemove"><span><a href="#" class="dietAddRemoveLink addDietQual">+</a> <a href="#" class="dietAddRemoveLink removeDietQual">-</a></span></td><td><input type="text" name="dietWrap-sno" data-attr-id-number="' + dataArray[i].dietTypeId + '" data-attr-id-patient-number="' + dataArray[i].id + '" value="' + ind + '" readonly=""></td><td><input type="text" name="dietWrap-condition" id="qualWrapQualification" readonly="" value="' + dataArray[i].dietTypeValue + '"><button class="btn btn-primary diet-search patient-search-img"><img src="../../img/search-icon-white.png" class="tradeFormWrapper-searchName-img"></button></td><td><input type="text" name="dietWrap-notes" maxlength="100" value="' + dataArray[i].notes + '"></td><td><input type="text" name="dietWrap-remarks" maxlength="100" value="' + dataArray[i].remarks + '"></td></tr>');
            }
        } else {
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}

function getPatientBillingData(dataObj) {
    console.log(dataObj);
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            var dataArray = dataObj.response.patientRelationships || [];
            $('.billingTabWrapper').find('.qualWrapper_li').remove();
            if (dataArray.length) {
                $('.billingAddRemoveLink').addClass('visibility-hidden');
            } else if(dataArray.length == 0) {
                $('.medicalConditionAlert:visible').prepend('<div class="alert alert-warning">No Records Found</div>');
            }
            for (var i = 0; i < dataArray.length; i++) {
                var ind = i + 1;
                if(IsPostalFlag != "1") {
                    $('.billingTabWrapper table tbody').append('<tr class="qualWrapper_li qualificationTabWrapper-li-' + ind + '"><td class="qual-td-addRemove"><span><a href="#" class="billingAddRemoveLink addBillingQual">+</a> <a href="#" class="billingAddRemoveLink removeBillingQual">-</a></span></td><td style="display:none"><input  type="text" name="billingWrap-sno" data-attr-id-number="' + dataArray[i].id + '" value="' + ind + '" readonly /></td><td style="display:none"><input type="text" name="billingWrap-createdBy" value="' + dataArray[i].createdBy + '" readonly /></td><td><select name="billingWrap-type" class="select-dropdown-width fields-mandatory" data-attr-selected-billcommunication_type="' + dataArray[i].communicationTypeId + '"><option selected="true" disabled="disabled">Please Select</option></select><select name="billingWrap-relationship" class="select-dropdown-width" style="border-color: #45cff0;" data-attr-selected-relation="' + dataArray[i].relationship + '"><option selected="true" disabled="disabled">Please Select</option></select></td><td><input type="text" style="border-color:#45cff0;" name="billingWrap-name" maxlength="100" value="" /></td><td><div><input type="text" name="billingWrap-address1" style="border-color:#45cff0;" placeholder="Address 1" maxlength="100" value="" /><input type="text" name="billingWrap-address2" placeholder="Address 2" maxlength="100" value="" /><input type="text" name="billingWrap-city" data-attr-city-id="" placeholder="City" readonly maxlength="100" readonly value="" /></div><div><input type="text" name="billingWrap-state" placeholder="County" maxlength="100" readonly value="" /><input type="text" name="billingWrap-country" placeholder="Country" readonly maxlength="100" value="" /><div class="inline-block"><input type="text" class="fields-mandatory" name="billingWrap-zipcode" placeholder="Postal Code" maxlength="100" disabled value="" /><img class="postalWrapper-img zipSearchImg" src="../../img/Default/search-icon.png" title="Search"></div><input type="text" name="billingWrap-zip4" placeholder="Zip4" readonly style="display: none;" maxlength="100" value="" /></div></td><td><div class="block-elem"><input type="text" name="billingWrap-phone" placeholder="Mobile" maxlength="100" value="" /></div><div class="block-elem"><input type="text" name="billingWrap-home" placeholder="Home Phone" maxlength="100" value="" /></div></td><td><div class="block-elem"><input type="text" name="billingWrap-email" placeholder="Email" maxlength="100" value="" /></div></td><td><input type="text" name="billingWrap-hourlybillrate" placeholder="Hourly Billing Rate" maxlength="100" value="" /></td><td><input type="text" name="billingWrap-notes" maxlength="100" value="' + dataArray[i].notes + '" /></td></tr>');
                }
                else{
                    $('.billingTabWrapper table tbody').append('<tr class="qualWrapper_li qualificationTabWrapper-li-' + ind + '"><td class="qual-td-addRemove"><span><a href="#" class="billingAddRemoveLink addBillingQual">+</a> <a href="#" class="billingAddRemoveLink removeBillingQual">-</a></span></td><td style="display:none"><input  type="text" name="billingWrap-sno" data-attr-id-number="' + dataArray[i].id + '" value="' + ind + '" readonly /></td><td style="display:none"><input type="text" name="billingWrap-createdBy" value="' + dataArray[i].createdBy + '" readonly /></td><td><select name="billingWrap-type" class="select-dropdown-width fields-mandatory" data-attr-selected-billcommunication_type="' + dataArray[i].communicationTypeId + '"><option selected="true" disabled="disabled">Please Select</option></select><select name="billingWrap-relationship" class="select-dropdown-width" style="border-color: #45cff0;" data-attr-selected-relation="' + dataArray[i].relationship + '"><option selected="true" disabled="disabled">Please Select</option></select></td><td><input type="text" style="border-color:#45cff0;" name="billingWrap-name" maxlength="100" value="" /></td><td><div><input type="text" name="billingWrap-address1" style="border-color:#45cff0;" placeholder="Address 1" maxlength="100" value="" /><input type="text" name="billingWrap-address2" placeholder="Address 2" maxlength="100" value="" /><div class="inline-block"><input type="text" style="border-color:#45cff0;" name="billingWrap-city" data-attr-city-id="" placeholder="City" disabled maxlength="100" value="" /><img class="postalWrapper-img citySearchImg" src="../../img/Default/search-icon.png" title="Search"></div><div><input type="text" name="billingWrap-state" placeholder="County" maxlength="100" readonly value="" /><input type="text" name="billingWrap-country" placeholder="Country" readonly maxlength="100" value="" /><div class="inline-block"><input type="text" name="billingWrap-zipcode" placeholder="Postal Code" maxlength="100" value="" /></div><input type="text" name="billingWrap-zip4" placeholder="Zip4" readonly style="display: none;" maxlength="100" value="" /></div></td><td><div class="block-elem"><input type="text" name="billingWrap-phone" placeholder="Mobile" maxlength="100" value="" /></div><div class="block-elem"><input type="text" name="billingWrap-home" placeholder="Home Phone" maxlength="100" value="" /></div></td><td><div class="block-elem"><input type="text" name="billingWrap-email" placeholder="Email" maxlength="100" value="" /></div></td><td><input type="text" name="billingWrap-hourlybillrate" placeholder="Hourly Billing Rate" maxlength="100" value="" /></td><td><input type="text" name="billingWrap-notes" maxlength="100" value="' + dataArray[i].notes + '" /></td></tr>');
                }
                appendRelationshipsData($('.qualificationTabWrapper-li-' + ind));
                billappendcommunicationsData($('.qualificationTabWrapper-li-' + ind));
                //getAjaxObjectAsync(ipAddress + '/homecare/communications/?parentId='+dataArray[i].id+'&parentTypeId=201&fields=*,address.*', "GET", getCommunicationsListData, onError);
                // getAjaxObjectAsync(ipAddress + '/homecare/communications/?parentId='+dataArray[i].id+'&parentTypeId=401&fields=*,address.*', "GET", getCommunicationsListData, onError);

                //function getCommunicationsListData(resp) {
                //console.log(resp);
                //if(resp && resp.response && resp.response.status) {
                //if(resp.response.status.code == "1") {
                // var communicationsArr = resp.response.communications ? resp.response.communications[0] : [];
                var communicationsArr = dataArray[i].communication ? dataArray[i].communication : [];
                if(communicationsArr.length) {
                    //if(communicationsArr) {
                    var $thisList = $('.qualificationTabWrapper-li-' + ind);
                    $thisList.find('[name="billingWrap-address1"]').val(communicationsArr[0].address1);
                    $thisList.find('[name="billingWrap-address2"]').val(communicationsArr[0].address2);
                    $thisList.find('[name="billingWrap-city"]').val(communicationsArr[0].addressCity).attr('data-attr-city-id', communicationsArr[0].cityId);
                    $thisList.find('[name="billingWrap-state"]').val(communicationsArr[0].addressState);
                    $thisList.find('[name="billingWrap-country"]').val(communicationsArr[0].addressCountry);
                    if(IsPostalFlag != "1") {
                        $thisList.find('[name="billingWrap-zipcode"]').val(communicationsArr[0].addressZip);
                    }else{
                        $thisList.find('[name="billingWrap-zipcode"]').val(communicationsArr[0].houseNumber);
                    }

                    $thisList.find('[name="billingWrap-zip4"]').val(communicationsArr[0].addressZip4);
                    $thisList.find('[name="billingWrap-phone"]').val(communicationsArr[0].cellPhone);
                    $thisList.find('[name="billingWrap-email"]').val(communicationsArr[0].email);
                    $thisList.find('[name="billingWrap-name"]').val(dataArray[i].name);
                    $thisList.find('[name="billingWrap-home"]').val(communicationsArr[0].homePhone);
                    $thisList.find('[name="billingWrap-hourlybillrate"]').val(dataArray[i].ratePerHour);
                    $thisList.find('[name="billingWrap-contactType"]').val(billCommunitionType);
                }
                //}
                // }
                // }
                //}
            }
        } else {
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}

function getPatientContactsData(dataObj) {
    console.log(dataObj);
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            var dataArray = dataObj.response.patientRelationships || [];
            $('.contactsTabWrapper').find('.qualWrapper_li').remove();
            if (dataArray.length) {
                $('.contactsAddRemoveLink').addClass('visibility-hidden');
            } else if(dataArray.length == 0) {
                $('.medicalConditionAlert:visible').prepend('<div class="alert alert-warning">No Records Found</div>');
            }
            for (var i = 0; i < dataArray.length; i++) {
                var ind = i + 1;
                if(IsPostalFlag == "1"){
                    $('.contactsTabWrapper table tbody').append('<tr class="qualWrapper_li qualificationTabWrapper-li-' + ind + '"><td class="qual-td-addRemove"><span><a href="#" class="contactsAddRemoveLink addContactsQual">+</a> <a href="#" class="contactsAddRemoveLink removeContactsQual">-</a></span></td><td style="display:none"><input type="text" name="contactsWrap-sno" data-attr-id-number="' + dataArray[i].id + '" value="' + ind + '" readonly /></td><td style="display:none"><input type="text" name="contactsWrap-createdBy" value="' + dataArray[i].createdBy + '" readonly /></td><td><select style="margin-bottom:5px"  name="contactsWrap-type" class="select-dropdown-width fields-mandatory" value="" data-attr-selected-communication_type="' + dataArray[i].communicationTypeId + '"><option selected="true" disabled="disabled">Please Select</option></select> &nbsp;<select name="contactsWrap-relationship" data-attr-selected-relation="' + dataArray[i].relationship + '" class="select-dropdown-width fields-mandatory" style="border-color: #45cff0;"><option selected="true" disabled="disabled">Please Select</option></select></td><td><input type="text" name="contactsWrap-name" style="border-color:#45cff0;" maxlength="100" value="' + dataArray[i].name + '" /></td><td><div><input type="text" name="contactsWrap-address1" placeholder="Address 1" maxlength="100" value="" /><input type="text" name="contactsWrap-address2" placeholder="Address 2" maxlength="100" value="" /><div class="inline-block"><input type="text" name="contactsWrap-city" data-attr-city-id="" placeholder="City" maxlength="100" disabled value="" /><img class="postalWrapper-img citySearchImg" src="../../img/Default/search-icon.png" title="Search"></div></div><div><input type="text" name="contactsWrap-state" placeholder="State" maxlength="100" readonly value="" /><input type="text" name="contactsWrap-country" placeholder="Country" readonly maxlength="100" value="" /><div class="inline-block"><input type="text" name="contactsWrap-zipcode" placeholder="Postal Code" maxlength="100" value="" /></div><input type="text" name="contactsWrap-zip4" placeholder="Zip4" readonly style="display: none;" maxlength="100" value="" /></div></td><td><div class="block-elem"><input type="text" name="contactsWrap-phone" placeholder="Mobile" maxlength="100" value="" /></div><div class="block-elem"><input type="text" name="contactsWrap-home" placeholder="Home Phone" maxlength="100" value="" /></div></td><td><div class="block-elem"><input type="text" name="contactsWrap-email" placeholder="Email" maxlength="100" value="" /></div></td><td><input type="text" name="contactsWrap-notes" maxlength="100" value="' + dataArray[i].notes + '" /></td></tr>');
                }
                else{
                    $('.contactsTabWrapper table tbody').append('<tr class="qualWrapper_li qualificationTabWrapper-li-' + ind + '"><td class="qual-td-addRemove"><span><a href="#" class="contactsAddRemoveLink addContactsQual">+</a> <a href="#" class="contactsAddRemoveLink removeContactsQual">-</a></span></td><td style="display:none"><input type="text" name="contactsWrap-sno" data-attr-id-number="' + dataArray[i].id + '" value="' + ind + '" readonly /></td><td style="display:none"><input type="text" name="contactsWrap-createdBy" value="' + dataArray[i].createdBy + '" readonly /></td><td><select style="margin-bottom:5px"  name="contactsWrap-type" class="select-dropdown-width fields-mandatory" value="" data-attr-selected-communication_type="' + dataArray[i].communicationTypeId + '"><option selected="true" disabled="disabled">Please Select</option></select> &nbsp;<select name="contactsWrap-relationship" data-attr-selected-relation="' + dataArray[i].relationship + '" class="select-dropdown-width fields-mandatory" style="border-color: #45cff0;"><option selected="true" disabled="disabled">Please Select</option></select></td><td><input type="text" name="contactsWrap-name" style="border-color:#45cff0;" maxlength="100" value="' + dataArray[i].name + '" /></td><td><div><input type="text" name="contactsWrap-address1" placeholder="Address 1" maxlength="100" value="" /><input type="text" name="contactsWrap-address2" placeholder="Address 2" maxlength="100" value="" /><input type="text" name="contactsWrap-city" data-attr-city-id="" placeholder="City" readonly maxlength="100" readonly value="" /></div><div><input type="text" name="contactsWrap-state" placeholder="State" maxlength="100" readonly value="" /><input type="text" name="contactsWrap-country" placeholder="Country" readonly maxlength="100" value="" /><div class="inline-block"><input type="text" name="contactsWrap-zipcode" placeholder="Postal Code" maxlength="100" disabled value="" /><img class="postalWrapper-img zipSearchImg" src="../../img/Default/search-icon.png" title="Search"></div><input type="text" name="contactsWrap-zip4" placeholder="Zip4" readonly style="display: none;" maxlength="100" value="" /></div></td><td><div class="block-elem"><input type="text" name="contactsWrap-phone" placeholder="Mobile" maxlength="100" value="" /></div><div class="block-elem"><input type="text" name="contactsWrap-home" placeholder="Home Phone" maxlength="100" value="" /></div></td><td><div class="block-elem"><input type="text" name="contactsWrap-email" placeholder="Email" maxlength="100" value="" /></div></td><td><input type="text" name="contactsWrap-notes" maxlength="100" value="' + dataArray[i].notes + '" /></td></tr>');
                }

                appendRelationshipsData($('.qualificationTabWrapper-li-' + ind));
                appendcommunicationsData($('.qualificationTabWrapper-li-' + ind));
                //getAjaxObjectAsync(ipAddress + '/homecare/communications/?parentId='+dataArray[i].id+'&parentTypeId='+dataArray[i].communicationTypeId, "GET", getCommunicationsListData, onError);
                //getAjaxObjectAsync(ipAddress + '/homecare/communications/?parentId='+dataArray[i].id+'&parentTypeId='+ dataArray[i].communicationTypeId, "GET", getCommunicationsListData, onError);

                //function getCommunicationsListData(resp) {
                console.log(dataArray);
                // if(resp && resp.response && resp.response.status) {
                //    if(resp.response.status.code == "1") {
                //      var communicationsArr = resp.response.communications ? resp.response.communications[0] : [];
                var communicationsArr = dataArray[i].communication ? dataArray[i].communication : [];
                if(communicationsArr.length) {
                    var $thisList = $('.qualificationTabWrapper-li-' + ind);
                    $thisList.find('[name="contactsWrap-address1"]').val(dataArray[i].communication[0].address1);
                    $thisList.find('[name="contactsWrap-address2"]').val(dataArray[i].communication[0].address2);
                    $thisList.find('[name="contactsWrap-city"]').val(communicationsArr[0].addressCity).attr('data-attr-city-id', communicationsArr[0].cityId);
                    $thisList.find('[name="contactsWrap-state"]').val(communicationsArr[0].addressState);
                    $thisList.find('[name="contactsWrap-country"]').val(communicationsArr[0].addressCountry);
                    if(IsPostalFlag == "1") {
                        $thisList.find('[name="contactsWrap-zipcode"]').val(communicationsArr[0].houseNumber);
                    }
                    else{
                        $thisList.find('[name="contactsWrap-zipcode"]').val(communicationsArr[0].addressZip);
                    }
                    $thisList.find('[name="contactsWrap-zip4"]').val(communicationsArr[0].addressZip4);
                    $thisList.find('[name="contactsWrap-phone"]').val(communicationsArr[0].cellPhone);
                    $thisList.find('[name="contactsWrap-email"]').val(communicationsArr[0].email);
                    $thisList.find('[name="contactsWrap-home"]').val(communicationsArr[0].homePhone);
                    $thisList.find('[name="contactsWrap-contactType"]').val(contactCommunitionType);
                }
                //}
                //}
                //}
            }
        } else {
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}

function onDeleteItem(respObj) {
    console.log(respObj);
    if (respObj && respObj.response && respObj.response.status) {
        if (respObj.response.status.code == "1") {
            var remItemName = $removeItem.closest('.pat-wrapper-elem').attr('data-remove-wrap-name');
            $removeItem.closest('tr').remove();
            modifySerialNumber(remItemName);
            if (showTab == "illnessTab" && $('.illnessTabWrapper').find('table tr').length == 1) {
                $('.illnessAddRemoveLink').removeClass('visibility-hidden');
                $('.medicalConditionAlert:visible').prepend('<div class="alert alert-success">Illness deactivated successfully</div>');
            } else if (showTab == "allergiesTab" && $('.allergiesTabWrapper').find('table tr').length == 1) {
                $('.allergiesAddRemoveLink').removeClass('visibility-hidden');
                $('.medicalConditionAlert:visible').prepend('<div class="alert alert-success">Allergies deactivated successfully</div>');
            } else if (showTab == "dietTab" && $('.dietTabWrapper').find('table tr').length == 1) {
                $('.dietAddRemoveLink').removeClass('visibility-hidden');
                $('.medicalConditionAlert:visible').prepend('<div class="alert alert-success">Diet deactivated successfully</div>');
            } else if (showTab == "billingTab" && $('.billingTabWrapper').find('table tr').length == 1) {
                $('.billingAddRemoveLink').removeClass('visibility-hidden');
                $('.medicalConditionAlert:visible').prepend('<div class="alert alert-success">Billing deactivated successfully</div>');
            } else if (showTab == "contactsTab" && $('.contactsTabWrapper').find('table tr').length == 1) {
                $('.contactsAddRemoveLink').removeClass('visibility-hidden');
                $('.medicalConditionAlert:visible').prepend('<div class="alert alert-success">Contacts deactivated successfully</div>');
            }
        } else {
            customAlert.error("error", respObj.response.status.message);
        }
    }
}




function allowOnlyDecimals(selector) {
    selector.keypress(function(e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 95 && e.which != 46) {
            return false;
        }
    });
}

function onChangeWorkPhone() {
    $("#txtWPExt").attr("disabled", true);
    var txtWP = $("#txtWP").val();
    txtWP = $.trim(txtWP);
    if (txtWP != "") {
        $("#txtWPExt").attr("disabled", false);
    }
}

function onChangeCell() { //cmbSMS
    var txtCell = $("#txtCell").val();
    txtCell = $.trim(txtCell);
    var cmbSMS = $("#cmbSMS").data("kendoComboBox");
    if (txtCell != "") {
        //  $("#txtWPExt").attr("disabled",false);
        cmbSMS.select(1);
        cmbSMS.enable(false);
    } else {
        cmbSMS.enable(true);
    }
}

function onshowImage() {
    var cmbGender = $("#cmbGender").data("kendoComboBox");
    if (cmbGender) {
        if (cmbGender.text() == "Male") {
            $("#imgPhoto").attr("src", "../../img/AppImg/HosImages/male_profile.png");
        } else if (cmbGender.text() == "Female") {
            $("#imgPhoto").attr("src", "../../img/AppImg/HosImages/profile_female.png");
        } else {
            $("#imgPhoto").attr("src", "../../img/AppImg/HosImages/blank.png");
        }
    }
}

function onErrorImage(image) {
    image.onerror = "";
    image.src = "../../img/AppImg/HosImages/patient1.png";
    //$("#imgPhoto").attr("src", "../../img/AppImg/HosImages/patient1.png");
    //imgPhoto.src = "../../img/AppImg/HosImages/patient1.png";
}

function onOverSSN(e) {
    //console.log(e);
    //$("#txtSSN").attr("title",$('[name="txtSSN"]').val());
}

function onOverHPhone() {
    //$("#txtHPhone").attr("title",$('[name="txtHPhone"]').val());
}

function onOverCell() {
    //$("#txtCell").attr("title",$('[name="txtCell"]').val());
}

function removeOverSelection() {
    $("#btnBrowse").removeClass("borderClass");
    $("#btnStartVideo").removeClass("borderClass");
    $("#btnTakePhoto").removeClass("borderClass");
    $("#btnUpload").removeClass("borderClass");
}

function onClickBrowseOver() {
    removeOverSelection();
    $("#btnBrowse").addClass("borderClass");
}

function onClickStartVideoOver() {
    removeOverSelection();
    $("#btnStartVideo").addClass("borderClass");
}

function onClickTakePhotoOver() {
    removeOverSelection();
    $("#btnTakePhoto").addClass("borderClass");
}

function onClickUploadPhotoOver() {
    removeOverSelection();
    $("#btnUpload").addClass("borderClass");
}

function onClickBrowseOut() {
    removeOverSelection();
}

function onClickTakePhoto() {
    removeSelectedButtons();
    $("#btnTakePhoto").addClass("selClass");
    isPhotoFlag = true;
    $("#video").hide();
    $("#canvas").show();
    var canvas = document.getElementById('canvas');
    var context = canvas.getContext('2d');
    var video = document.getElementById('video');
    context.drawImage(video, 0, 0, 200, 150);

    var dataURL = canvas.toDataURL("image/png");

    var reqUrl = ipAddress + "/upload/patient/photo/stream/?patient-id=" + patientId + "&photo-ext=png";
    // var xmlhttp = new XMLHttpRequest();
    //  xmlhttp.open(reqUrl, true);
    // // xmlhttp.open("POST", "/ROOT/upload/patient/photo/?patient-id=101&photo-ext=png", true);
    //  xmlhttp.send(dataURL);
    //  $.ajax({
    //       type: "POST",
    //       url: reqUrl,
    //       data: {
    //           base64: dataURL
    //       }
    //     }).done(function(o) {
    //       console.log('saved');
    //     });
}
var localStream = null;
function onClickStartVideo() {
    removeSelectedButtons();
    $("#btnStartVideo").addClass("selClass");
    isBrowseFlag = false;
    var video = document.getElementById('video');
    if ($('#startCamera-switch').is(':checked')) {
        $("#video").show();
        $("#btnTakePhoto").show();
        //$("#canvas").show();
        $("#imgPhoto").hide();
        // Get access to the camera!
        if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
            // Not adding `{ audio: true }` since we only want video now
            navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
                localStream = stream;
				video.srcObject = stream;
                //video.src = URL.createObjectURL(stream);
                //video.play();
            });
        }
    } else {
        $("#video").hide();
        $("#btnTakePhoto").show();
        $("#imgPhoto").show();
        $("#canvas").hide();
        if(localStream){
        	 // localStream.stop();
            localStream.active = false;
            localStream.src="";
            // video.srcObject = "";
        }
    }
}
var isBrowseFlag = false;
var isPhotoFlag = false;

function removeSelectedButtons() {
    $("#btnBrowse").removeClass("selClass");
    $("#btnStartVideo").removeClass("selClass");
    $("#btnTakePhoto").removeClass("selClass");
    $("#btnUpload").removeClass("selClass");
}

function onClickBrowse(e) {
    removeSelectedButtons();
    $("#btnBrowse").addClass("selClass");
    isBrowseFlag = true;
    $("#video").hide();
    $("#canvas").hide();
    $("#imgPhoto").show();
    console.log(e);
    if (e.currentTarget && e.currentTarget.id) {
        var btnId = e.currentTarget.id;
        var fileTagId = ("fileElem" + btnId);
       $("#fileElem").click();
        // onSelectionFiles();
    }
}

function onClickUploadPhoto() {
    if (patientId != "") {
        removeSelectedButtons();
        $("#btnUpload").addClass("selClass");
        if (isBrowseFlag) {

            //imagePhotoData
            var reqUrl = ipAddress + "/upload/patient/photo/?patient-id=" + patientId + "&photo-ext=jpg&access_token=" + sessionStorage.access_token + "&tenant=" + sessionStorage.tenant;
            var reqUrl = ipAddress + "/upload/patient/photo/stream/?patient-id=" + patientId + "&photo-ext=png&access_token=" + sessionStorage.access_token + "&tenant=" + sessionStorage.tenant
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.open("POST", reqUrl, true);
            //xmlhttp.open("POST", "/ROOT/upload/patient/photo/?patient-id=101&photo-ext=png", true);
            xmlhttp.send(imagePhotoData);
            xmlhttp.onreadystatechange = function() { //Call a function when the state changes.
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    // customAlert.error("Info", "Image uploaded successfully");
                    console.log("Image uploaded successfully");
                }
            }
            /* var formData = new FormData();
             formData.append("desc", fileName);
             formData.append("file", files[0]);

             Loader.showLoader();
             $.ajax({
                 url: reqUrl,
                 type: 'POST',
                 data: formData,
                 processData: false,
                 contentType: false, // "multipart/form-data", 
                 success: function(data, textStatus, jqXHR) {
                     if (typeof data.error === 'undefined') {
                         Loader.hideLoader();
                         submitForm(data);
                     } else {
                         Loader.hideLoader();
                     }
                 },
                 error: function(jqXHR, textStatus, errorThrown) {
                     Loader.hideLoader();
                 }
             });*/
        } else {
            if(isPhotoFlag) {
                var canvas = document.getElementById('canvas');
                //var context = canvas.getContext('2d');
                //var video = document.getElementById('video');
                //context.drawImage(video, 0, 0, 200, 150);

                var dataURL = canvas.toDataURL("image/png");

                var reqUrl = ipAddress + "/upload/patient/photo/stream/?patient-id=" + patientId + "&photo-ext=png&access_token=" + sessionStorage.access_token + "&tenant=" + sessionStorage.tenant;
                var xmlhttp = new XMLHttpRequest();
                xmlhttp.open("POST", reqUrl, true);
                //xmlhttp.open("POST", "/ROOT/upload/patient/photo/?patient-id=101&photo-ext=png", true);
                xmlhttp.send(dataURL);
                xmlhttp.onreadystatechange = function () { //Call a function when the state changes.
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        console.log("Image uploaded successfully");
                    }
                }
            }
        }
    }
}
var files = null;
var fileName = "";
var compressorSettings = {
    toWidth: 70,
    toHeight: 84,
    mimeType: 'image/png',
    mode: 'strict',
    quality: 0.2,
    grayScale: false,
    sepia: false,
    threshold: false,
    vReverse: false,
    hReverse: false,
    speed: 'low'
};

var imagePhotoData = null;

function onSelectionFiles(event) {
    var imageCompressor = new ImageCompressor();
    console.log(event);
    files = event.target.files;
    fileName = "";
    //$('#txtFU').val("");
    if (files) {
        if (files.length > 0) {
            fileName = files[0].name;
            // if (patientId != "") {
                var oFReader = new FileReader();
                oFReader.readAsDataURL(files[0]);
                oFReader.onload = function(oFREvent) {
                    document.getElementById("imgPhoto").src = oFREvent.target.result;
                    if (imageCompressor) {
                        imageCompressor.run(oFREvent.target.result, compressorSettings, function(small) {
                            document.getElementById("imgPhoto").src = small;
                            //isBrowseFlag = false;
                            imagePhotoData = small;
                            return small;
                        });
                    }

                }
            // }
        }
    }
}

function submitForm(dataObj) {
    //console.log(dataObj);
    customAlert.error("Info", "Image uploaded successfully");
}

function showSSNNumver(num) {
    var res = num, //grabs the value
        len = res.length, //grabs the length
        stars;
    if (sessionStorage.countryName == "India" || sessionStorage.countryName == "United Kingdom") {
        stars = len > 0 ? len > 1 ? len > 2 ? len > 3 ? len > 4 ? len > 5 ? 'XXX-XXX-' : 'XXX-XX-' : 'XXX-X' : 'XXX-' : 'XX' : 'X' : ''; //this provides the masking and formatting
    } else {
        stars = len > 0 ? len > 1 ? len > 2 ? len > 3 ? len > 4 ? 'XXX-XX-' : 'XXX-X' : 'XXX-' : 'XX' : 'X' : ''; //this provides the masking and formatting
    }
    var result = stars + res.substring(5); //this is the result
    return result;
}

function showHomeNumver(num) {
    var res = num, //grabs the value
        len = res.length, //grabs the length
        stars = len > 0 ? len > 1 ? len > 2 ? '(XXX)___-___' : '(XX_)___-___' : '(X__)___-___' : '(___)___-___', //this provides the masking and formatting
        result = stars + res.substring(7); //this is the result
    return result;
}
var $selfZipRow = "";
function onClickZipSearch() {
    var popW = 600;
    var popH = 500;

    parentRef.searchZip = true;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search Zip";
    var cntry = sessionStorage.countryName;
    if (cntry.indexOf("India") >= 0) {
        profileLbl = "Search Postal Code";
    } else if (cntry.indexOf("United Kingdom") >= 0) {
        if(IsPostalCodeManual == "1"){
            profileLbl = "Search City";
        }
        else{
            profileLbl = "Search Postal Code";
        }

    } else if (cntry.indexOf("United State") >= 0) {
        profileLbl = "Search Zip";
    } else {
        profileLbl = "Search Zip";
    }
    $selfZipRow = $(this);
    devModelWindowWrapper.openPageWindow("../../html/masters/zipList.html", profileLbl, popW, popH, true, onCloseSearchZipAction);

    if (!$(this).closest('tr').attr('data-item-edited') == true && !$(this).closest('tr').attr('data-item-new') == true) {
        $(this).closest('tr').attr('data-item-edited', true);
    }
}
var zipSelItem = null;

function onCloseSearchZipAction(evt, returnData) {
    console.log(returnData);
    if (returnData && returnData.status == "success") {
        //console.log();
        var selItem = returnData.selItem;
        if (showTab == "billingTab" && selItem) {
            var $elem = $selfZipRow.closest('tr');
            $elem.find('[name="billingWrap-zipcode"]').val("");
            $elem.find('[name="billingWrap-zip4"]').val("");
            $elem.find('[name="billingWrap-city"]').val("");
            $elem.find('[name="billingWrap-state"]').val("");
            $elem.find('[name="billingWrap-country"]').val("");
            zipSelItem = selItem;
            cityId = zipSelItem.idk;
            $elem.find('input').attr('data-attr-city-id', cityId);
            if(IsPostalFlag != "1") {
                if (zipSelItem.zip) {
                    $elem.find('[name="billingWrap-zipcode"]').val(zipSelItem.zip);
                }
            }
            if (zipSelItem.zipFour) {
                $elem.find('[name="billingWrap-zip4"]').val(zipSelItem.zipFour);
            }
            if (zipSelItem.state) {
                $elem.find('[name="billingWrap-state"]').val(zipSelItem.state);
            }
            if (zipSelItem.country) {
                $elem.find('[name="billingWrap-country"]').val(zipSelItem.country);
            }
            if (zipSelItem.city) {
                $elem.find('[name="billingWrap-city"]').val(zipSelItem.city);
            }
            if (!$elem.attr('data-item-new') == true && !$elem.attr('data-item-edited') == true) {
                $elem.attr("data-item-edited", true);
            }
        } else if (showTab == "contactsTab" && selItem) {
            var $elem = $selfZipRow.closest('tr');
            $elem.find('[name="contactsWrap-zipcode"]').val("");
            $elem.find('[name="contactsWrap-zip4"]').val("");
            $elem.find('[name="contactsWrap-city"]').val("");
            $elem.find('[name="contactsWrap-state"]').val("");
            $elem.find('[name="contactsWrap-country"]').val("");
            zipSelItem = selItem;
            cityId = zipSelItem.idk;
            $elem.find('input').attr('data-attr-city-id', cityId);
            if(IsPostalFlag)
            if (zipSelItem.zip) {
                $elem.find('[name="contactsWrap-zipcode"]').val(zipSelItem.zip);
            }
            if (zipSelItem.zipFour) {
                $elem.find('[name="contactsWrap-zip4"]').val(zipSelItem.zipFour);
            }
            if (zipSelItem.state) {
                $elem.find('[name="contactsWrap-state"]').val(zipSelItem.state);
            }
            if (zipSelItem.country) {
                $elem.find('[name="contactsWrap-country"]').val(zipSelItem.country);
            }
            if (zipSelItem.city) {
                $elem.find('[name="contactsWrap-city"]').val(zipSelItem.city);
            }
        } else if (showTab == "patientInfoTab" && selItem) {
            $("#cmbZip").val("");
            $("#txtZip4").val("");
            $("#txtState").val("");
            $("#txtCountry").val("");
            $("#txtCity").val("");
            zipSelItem = selItem;
            cityId = zipSelItem.idk;
            if (cntry.indexOf("United Kingdom") >= 0) {
                if(IsPostalCodeManual != "1") {
                    if (zipSelItem.zip) {
                        $("#cmbZip").val(zipSelItem.zip);
                    }
                }
            }
            else {
                if (zipSelItem.zip) {
                    $("#cmbZip").val(zipSelItem.zip);
                }
            }
            if (zipSelItem.zipFour) {
                $("#txtZip4").val(zipSelItem.zipFour);
            }
            if (zipSelItem.state) {
                $("#txtState").val(zipSelItem.state);
            }
            if (zipSelItem.country) {
                $("#txtCountry").val(zipSelItem.country);
            }
            if (zipSelItem.city) {
                $("#txtCity").val(zipSelItem.city);
            }
        }
    }
}

function getZip() {
    getAjaxObject(ipAddress + "/city/list/", "GET", getZipList, onError);
}

function getZipList(dataObj) {
    //var dArray = getTableListArray(dataObj);
    var dArray = [];
    if (dataObj && dataObj.response && dataObj.response.cityExt) {
        if ($.isArray(dataObj.response.cityext)) {
            dArray = dataObj.response.cityext;
        } else {
            dArray.push(dataObj.response.cityext);
        }
    }
    if (dArray && dArray.length > 0) {
        //setDataForSelection(dArray, "cmbZip", onZipChange, ["zip", "id"], 0, "");
        //onZipChange();
    }
    getPrefix();

}

function onErrorMedication() {

}

function getPrefix() {
    getAjaxObject(ipAddress + "/master/Prefix/list/", "GET", getCodeTableValueList, onError);
}

function onComboChange(cmbId) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb && cmb.selectedIndex < 0) {
        cmb.select(0);
    }
}

function onGethbtPatientSuccess() {

}

function onPrefixChange() {
    onComboChange("cmbPrefix");
}

function onPtChange() {
    onComboChange("cmbStatus");
}

function onTalkChange() {
    // if(txtDNR.text() == "Yes")
    onComboChange("txtTalk");
    var txtTalk = $("#txtTalk").data("kendoComboBox");
    if(txtTalk.text() == "Yes" || txtTalk.text() == "No"){
        $("#txtTalkNotes").attr("readonly", false);
    }
    else{
        $("#txtTalkNotes").attr("readonly", true);
    }
}

function onGlassChange() {
    onComboChange("txtGlass");
    var txtTalk = $("#txtGlass").data("kendoComboBox");
    if(txtTalk.text() == "Glasses" || txtTalk.text() == "Bi-focal" || txtTalk.text() == "Contacts"){
        $("#txtGlassNotes").attr("readonly", false);
    }
    else{
        $("#txtGlassNotes").attr("readonly", true);
    }
}

function onHearChange() {
    onComboChange("txtHear");
    var txtTalk = $("#txtHear").data("kendoComboBox");
    if(txtTalk.text() == "Yes" || txtTalk.text() == "No"){
        $("#txtHearNotes").attr("readonly", false);
    }
    else{
        $("#txtHearNotes").attr("readonly", true);
    }
}

function onT1alkChange() {
    onComboChange("txtT1alk");
    var txtTalk = $("#txtT1alk").data("kendoComboBox");
    if(txtTalk.text() == "Yes" || txtTalk.text() == "No"){
        $("#txtT1alkNotes").attr("readonly", false);
    }
    else{
        $("#txtT1alkNotes").attr("readonly", true);
    }
}

function onDalkChange() {
    onComboChange("txtDalk");
    var txtTalk = $("#txtDalk").data("kendoComboBox");
    if(txtTalk.text() == "Yes" || txtTalk.text() == "No"){
        $("#txtDalkNotes").attr("readonly", false);
    }
    else{
        $("#txtDalkNotes").attr("readonly", true);
    }
}

function onTdalkChange() {
    onComboChange("txtTdalk");
    var txtTalk = $("#txtTdalk").data("kendoComboBox");
    if(txtTalk.text() == "Yes" || txtTalk.text() == "No"){
        $("#txtTdalkNotes").attr("readonly", false);
    }
    else{
        $("#txtTdalkNotes").attr("readonly", true);
    }
}

function onGenderChange() {
    onComboChange("cmbGender");
    if (patientId != "") {
        var imageServletUrl = ipAddress + "/download/patient/photo/" + patientId + "/?" + Math.round(Math.random() * 1000000) + "&access_token=" + sessionStorage.access_token + "&tenant=" + sessionStorage.tenant;
        $("#imgPhoto").attr("src", imageServletUrl);
    } else {
        onshowImage();
    }

}

function oncmbCarerGenderChange() {
    onComboChange("cmbCarerGender");
}

function onSMSChange() {
    onComboChange("cmbSMS");
}

function onLanChange() {
    onComboChange("cmbLang");
}
function onLanChange1() {
    onComboChange("txtL1");
}
function onLanChange2() {
    onComboChange("txtL2");
}
function onLanChange3() {
    onComboChange("txtL3");
}

function onRaceChange() {
    onComboChange("cmbRace");
}

function onEthnicityChange() {
    onComboChange("cmbEthicity");
}

function onZipChange() {
    onComboChange("cmbZip");
    var cmbZip = $("#cmbZip").data("kendoComboBox");
    if (cmbZip) {
        var dataItem = cmbZip.dataItem();
        if (dataItem) {
            $("#txtZip4").val(dataItem.zipFour);
            $("#txtCountry").val(dataItem.country);
            $("#txtState").val(dataItem.state);
            $("#txtCity").val(dataItem.city);
        }
    }
}

function getCodeTableValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbPrefix", onPrefixChange, ["value", "idk"], 0, "");
    }
    getAjaxObject(ipAddress + "/master/Patient_Status/list/", "GET", getPatientStatusValueList, onError);
}

function getPatientStatusValueList(dataObj) {
    var dArray = getTableFirstListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbStatus", onPtChange, ["desc", "idk"], 0, "");
    }
    getAjaxObject(ipAddress + "/master/Gender/list/", "GET", getGenderValueList, onError);
}

function getGenderValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbGender", onGenderChange, ["desc", "idk"], 0, "");
        setDataForSelection(dArray, "cmbCarerGender", oncmbCarerGenderChange, ["desc", "idk"], 0, "");
    }
    getAjaxObject(ipAddress + "/master/SMS/list/", "GET", getSMSValueList, onError);
}

function getSMSValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbSMS", onSMSChange, ["desc", "idk"], 0, "");
    }
    getAjaxObject(ipAddress + "/master/Language/list?is-active=1", "GET", getLanguageValueList, onError);
}

function getLanguageValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    var dtArray = getTableFirstListArray(dataObj);

    if (dtArray && dtArray.length > 0) {
        setMultipleDataForSelection(dtArray, "cmbLang", onLanChange, ["desc", "idk"], 0, "");
    }

    if (dArray && dArray.length > 0) {
        // setDataForSelection(dArray, "cmbLan", onLanChange, ["desc", "idk"], 0, "");
        setDataForSelection(dArray, "txtL1", onLanChange1, ["desc", "idk"], 0, "");
        setDataForSelection(dArray, "txtL2", onLanChange2, ["desc", "idk"], 0, "");
        setDataForSelection(dArray, "txtL3", onLanChange3, ["desc", "idk"], 0, "");
    }
    getAjaxObject(ipAddress + "/master/Race/list/", "GET", getRaceValueList, onError);
}

function getRaceValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbRace", onRaceChange, ["desc", "idk"], 0, "");
    }
    getAjaxObject(ipAddress + "/master/Ethnicity/list/", "GET", getEthnicityValueList, onError);
}

function getEthnicityValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "cmbEthicity", onEthnicityChange, ["desc", "idk"], 0, "");
    }
    getAjaxObject(ipAddress + "/user/list/?user-type-id=200,201", "GET", onGetUserListData, onError);
}

function onGetUserListData(dataObj) {
    //console.log(dataObj);
    var userListArray = [];
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            if ($.isArray(dataObj.response.user)) {
                userListArray = dataObj.response.user;
            } else {
                userListArray.push(dataObj.response.user);
            }
            var tempDataArry = [];
            var obj = {};
            obj.id = "";
            obj.userName = "";
            obj.fullName = "";
            obj.userTypeId = "";
            obj.userNameDuplicate = "";
            obj.idk = "";
            tempDataArry.push(obj);
            for (var i = 0; i < userListArray.length; i++) {
                var item = userListArray[i];
                if (item.userTypeId == 200) {
                    item.userNameDuplicate = item.username + " - nurse";
                } else if (item.userTypeId == 201) {
                    item.userNameDuplicate = item.username + " - care giver";
                }
                item.idk = userListArray[i].id;
                tempDataArry.push(item);
            }
            userListArray = tempDataArry;
            /*for (var i = 0; i < userListArray.length; i++) {
                var item = userListArray[i];
                if (item.userTypeId == 200) {
                    item.userNameDuplicate = item.userName + " - nurse";
                } else if (item.userTypeId == 201) {
                    item.userNameDuplicate = item.userName + " - care giver";
                }
                item.idk = item.id;
            }*/
        } else {
            customAlert.info("Error", dataObj.response.status.message);
        }
    }
    setDataForSelection(userListArray, "cmbType", onEthnicityChange, ["userNameDuplicate", "idk"], 0, "");
    if (operation == UPDATE && patientId != "") {
        getPatientInfo();
        var imageServletUrl = ipAddress + "/download/patient/photo/" + patientId + "/?" + Math.round(Math.random() * 1000000) + "&access_token=" + sessionStorage.access_token + "&tenant=" + sessionStorage.tenant;
        $("#imgPhoto").attr("src", imageServletUrl);
    }
}

function getPatientInfo() {
    getAjaxObject(ipAddress + "/patient/" + patientId, "GET", onGetPatientInfo, onError);
}

function getComboListIndex(cmbId, attr, attrVal) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb) {
        var ds = cmb.dataSource;
        var totalRec = ds.total();
        for (var i = 0; i < totalRec; i++) {
            var dtItem = ds.at(i);
            if (dtItem && dtItem[attr] == attrVal) {
                cmb.select(i);
                return i;
            }
        }
    }
    return -1;
}

function onHomeKeydown(e) {
    //setTimeout(function(){
    var txtHPhone = $("#txtHPhone").data("kendoMaskedTextBox");
    if (txtHPhone) {
        // var strValue = txtHPhone.raw();
        /*if(strValue.length == 0){
            strValue = "";
        }else if(strValue.length == 1){
            strValue = "x";
        }else if(strValue.length == 2){
            strValue = "xx";
        }else if(strValue.length == 3){
            strValue = "xxx";
        }else{
            strValue = strValue.substring(3,strValue.length);
            strValue = "xxx"+strValue;
        }
        txtHPhone.value(strValue);*/
        console.log(strValue)
    }
    //})
}

function setHomePhoneMask() {
    /*  $("#txtHPhone").kendoMaskedTextBox({
          mask: "(999) 999-9999"
      });
      $("#txtCell").kendoMaskedTextBox({
          mask: "9999999999"
      });*/


    var cntry = sessionStorage.countryName;
    var nhs = 'NHS :<span class="mandatoryClass">*</span>';
    if (cntry.indexOf("India") >= 0) {
        $('.postalCode').html('Postal Code : <span class="mandatoryClass">*</span>');
        $('.stateLabel').text("State:");
        $('.nhsDetail').html('NHS: <span class="mandatoryClass"></span>');
        $('.weightLabel').html('Weight (Kgs): <span class="mandatoryClass"></span>');
        $('.zipFourWrapper').hide();
        $('#txtSSN').kendoMaskedTextBox({ mask: '999-999-9999' });
        // $('#txtSSN').unmask().maskSSN('999-999-9999', {maskedChar:'X', maskedCharsLength:6});
        //$('#txtHPhone').unmask().maskSSN('(999) 999-9999', {maskedChar:'X', maskedCharsLength:3});
        //$('#txtCell').unmask().maskSSN('999 999 9999', {maskedChar:'X', maskedCharsLength:3});
        $('#txtHPhone').kendoMaskedTextBox({ mask: '(999) 999-9999' });
        // $('#txtCell').kendoMaskedTextBox({ mask: "999 999 9999" });
        $("#txtWP").kendoMaskedTextBox({ mask: "(999) 999-9999" });
    } else if (cntry.indexOf("United Kingdom") >= 0) {
        if(IsPostalCodeManual == "1"){
            $("#btnCitySearch").css("display","");
            $("#lblCity").html("City" +"<span class='mandatoryClass'>*</span>");
            $("#cmbZip").attr("readonly", false);
            $("#btnZipSearch").css("display","none");
            $(".postalCode").html("Postal Code : ");
            IsPostalFlag = "1";
        }
        else{
            $('.postalCode').html('Postal Code : <span class="mandatoryClass">*</span>');
        }

        $('.stateLabel').text("County:");
        $('.nhsDetail').html('NHS: <span class="mandatoryClass"></span>');
        $('.weightLabel').html('Weight (Kgs): <span class="mandatoryClass"></span>');
        $('.zipFourWrapper').hide();
        $('#txtSSN').kendoMaskedTextBox({ mask: '999-999-9999' });
        //  $('#txtSSN').unmask().maskSSN('999-999-9999', {maskedChar:'X', maskedCharsLength:6});
        //  $('#txtHPhone').unmask().maskSSN('99999999999999999999', {maskedChar:'X', maskedCharsLength:3});
        // $('#txtCell').unmask().maskSSN('999999999999999', {maskedChar:'X', maskedCharsLength:3});
        $('#txtHPhone').kendoMaskedTextBox();
        // $('#txtCell').kendoMaskedTextBox();
        $("#txtWP").kendoMaskedTextBox();
    } else if (cntry.indexOf("United State") >= 0) {
        $('.postalCode').html('Zip : <span class="mandatoryClass">*</span>');
        $('.stateLabel').text("State:");
        $('.nhsDetail').html('SSN: <span class="mandatoryClass"></span>');
        $('.weightLabel').html('Weight (lbs): <span class="mandatoryClass"></span>');
        $('.zipFourWrapper').show();
        $('#txtSSN').kendoMaskedTextBox({ mask: '999-999-9999' });
        //$('#txtSSN').unmask().maskSSN('999-99-9999', {maskedChar:'X', maskedCharsLength:5});
        // $('#txtHPhone').unmask().maskSSN('(999) 999-9999', {maskedChar:'X', maskedCharsLength:3});
        $('#txtHPhone').kendoMaskedTextBox({ mask: "(999) 999-9999" }); //('(999) 999-9999', {maskedChar:'X', maskedCharsLength:3});
        $("#txtWP").kendoMaskedTextBox({ mask: "(999) 999-9999" });
        // $('#txtCell').kendoMaskedTextBox({ mask: "999 999 9999" });
    } else {
        $('.postalCode').html('Zip : <span class="mandatoryClass">*</span>');
        $('.stateLabel').text("State:");
        $('.nhsDetail').html('SSN: <span class="mandatoryClass"></span>');
        $('.weightLabel').html('Weight (lbs): <span class="mandatoryClass"></span>');
        $('.zipFourWrapper').show();
        $('#txtSSN').kendoMaskedTextBox({ mask: '999-999-9999' });
        //$('#txtSSN').unmask().maskSSN('999-99-9999', {maskedChar:'X', maskedCharsLength:5});
        // $('#txtHPhone').unmask().maskSSN('(999) 999-9999', {maskedChar:'X', maskedCharsLength:3});
        $('#txtHPhone').kendoMaskedTextBox({ mask: "(999) 999-9999" }); //('(999) 999-9999', {maskedChar:'X', maskedCharsLength:3});
        $("#txtWP").kendoMaskedTextBox({ mask: "(999) 999-9999" });
        // $('#txtCell').kendoMaskedTextBox({ mask: "999 999 9999" });
    }


    //$("#txtHPhone").off("keyup");
    //$("#txtHPhone").on("keydown",onHomeKeydown);
}
/*function setWorkPhoneMask(){
    $("#txtWP").kendoMaskedTextBox({
        mask: "(999) 999-9999"
    });
}
function setExtensionMask(){
    $("#txtExtension").kendoMaskedTextBox({
        mask: "(999) 999-9999"
    });
}
function setCellPhoneMask(){
    $("#txtCell").kendoMaskedTextBox({
        mask: "(999) 999-9999"
    });
}*/
var cityId = "";

function onGetPatientInfo(dataObj) {
    patientInfoObject = dataObj;
    $("#tabsUL").children().removeClass("disabled");
    if (dataObj && dataObj.response && dataObj.response.patient) {
        if (dataObj.response.patient.photoExt) {
            photoExt = dataObj.response.patient.photoExt;
        }
        $("#txtID").val(dataObj.response.patient.id);
        $("#txtBed").val(dataObj.response.patient.bedNumber);
        $("#txtRoom").val(dataObj.response.patient.roomNumber);

        $("#txtExtID1").val(dataObj.response.patient.externalId1);
        $("#txtExtID2").val(dataObj.response.patient.externalId2);
        getComboListIndex("cmbPrefix", "value", dataObj.response.patient.prefix);
        getComboListIndex("txtFAN", "idk", dataObj.response.patient.facilityId);
        onFacilityChange();
        $("#txtFN").val(dataObj.response.patient.firstName);
        $("#txtLN").val(dataObj.response.patient.lastName);
        $("#txtMN").val(dataObj.response.patient.middleName);
        $("#txtWeight").val(dataObj.response.patient.weight);
        $("#txtHeight").val(dataObj.response.patient.height);
        $("#txtNN").val(dataObj.response.patient.nickname);
        $("#txtNI").val(dataObj.response.patient.ni);
        if (dataObj.response.patient.dateOfBirth) {
            var dt = new Date(dataObj.response.patient.dateOfBirth);
            if (dt) {
                var strDT = "";
                var cntry = sessionStorage.countryName;
                if (cntry.indexOf("India") >= 0) {
                    strDT = kendo.toString(dt, "dd/MM/yyyy");
                } else if (cntry.indexOf("United Kingdom") >= 0) {
                    strDT = kendo.toString(dt, "dd/MM/yyyy");
                } else {
                    strDT = kendo.toString(dt, "MM/dd/yyyy");
                }

                $("#dtDOB").datepicker();
                $("#dtDOB").datepicker("setDate", strDT);
                /*var dtDOB = $("#dtDOB").data("kendoDatePicker");
                if(dtDOB){
                    dtDOB.value(strDT);
                }*/
                /*var currDate = new Date();
                console.log(currDate.getFullYear()-dt.getFullYear());
                var strAge = currDate.getFullYear()-dt.getFullYear();*/
                var strAge = getAge(dt);
                $("#txtAge").val(strAge);
            }
        }

        //$("#txtSSN1").val(dataObj.response.patient.ssn);
        //var result = showSSNNumver(dataObj.response.patient.ssn);
        //$("#txtSSN").val(result);
        setMaskTextBoxValue("txtSSN", dataObj.response.patient.ssn);
        // $("#txtSSN").val(dataObj.response.patient.ssn);
        // $('#txtSSN').unmask().maskSSN('(9999)-(999)-999', {maskedChar:'X', maskedCharsLength:7});

        getComboListIndex("cmbStatus", "desc", dataObj.response.patient.status);
        getComboListIndex("cmbGender", "desc", dataObj.response.patient.gender);
        getComboListIndex("cmbEthicity", "desc", dataObj.response.patient.ethnicity);
        getComboListIndex("cmbRace", "desc", dataObj.response.patient.race);
        getComboListIndex("cmbReligion", "desc", dataObj.response.patient.religion);
        getComboListIndex("txtDNR", "Key", dataObj.response.patient.dnr);
        onChangeDNR();
        getComboListIndex("cmbMarital", "desc", dataObj.response.patient.maritalStatus);
        // getComboListIndex("cmbLan", "desc", dataObj.response.patient.language);
        var languageVal = dataObj.response.patient.language;
        if(languageVal) {
            languageVal = languageVal.split(',');
            getComboListIndex("txtL1", "desc", $.trim(languageVal[0]));
            getComboListIndex("txtL2", "desc", $.trim(languageVal[1]));
            getComboListIndex("txtL3", "desc", $.trim(languageVal[2]));
        }
        getComboListIndex("cmbType", "idk", dataObj.response.patient.nurseId);

        if (dataObj && dataObj.response && dataObj.response.communication) {

            var commArray = [];
            if ($.isArray(dataObj.response.communication)) {
                commArray = dataObj.response.communication;
            } else {
                commArray.push(dataObj.response.communication);
            }
            var comObj = commArray[0];
            commId = comObj.id;
            $("#txtAdd1").val(comObj.address1);
            $("#txtAdd2").val(comObj.address2);
            //  $("#txtSMS").val(comObj.sms);
            getComboListIndex("cmbSMS", "desc", comObj.sms);
            if (cntry.indexOf("United Kingdom") >= 0) {
                if(IsPostalCodeManual == "1"){
                    $("#cmbZip").val(comObj.houseNumber);
                }
                else{
                    getComboListIndex("cmbZip", "idk", comObj.cityId);
                    $("#cmbZip").val(comObj.zip);
                }
            }
            else{
                getComboListIndex("cmbZip", "idk", comObj.cityId);
                $("#cmbZip").val(comObj.zip);
            }

            cityId = comObj.cityId;
            //onZipChange();
            //$("#txtCell").val(comObj.cellPhone);

            //$("#txtWP").val(comObj.workPhone);
            if (comObj.workPhoneExt != "0") {
                $("#txtWPExt").attr("disabled", false);
                $("#txtWPExt").val(comObj.workPhoneExt);

            }
            $("#txtExtension").val(comObj.homePhoneExt);
            setHomePhoneMask();
            // setHomePhoneMask();
            if (comObj.homePhone != "0") {
                setMaskTextBoxValue("txtHPhone", comObj.homePhone);
                //$("#txtHPhone").val(comObj.homePhone);
                // $('#txtHPhone').unmask().maskSSN('(999) 999-999', {maskedChar:'X', maskedCharsLength:6});
            }
            if (comObj.cellPhone != "0") {
                // setMaskTextBoxValue("txtCell", comObj.cellPhone);
                $("#txtCell").val(comObj.cellPhone);
                //$("#txtCell").val(comObj.cellPhone);
                //$('#txtCell').unmask().maskSSN('999999999', {maskedChar:'X', maskedCharsLength:6});
            }
            //setMaskTextBoxValue("txtCell", comObj.cellPhone);
            $("#txtWP").kendoMaskedTextBox({ mask: "(999) 999-9999" });
            if (comObj.workPhone != "0") {
                setMaskTextBoxValue("txtWP", comObj.workPhone);
            }



            /*var txtHome = $("#txtHPhone").data("kendoMaskedTextBox");
            if(txtHome){
                txtHome.value(comObj.homePhone);
            }
            var txtWP = $("#txtWP").data("kendoMaskedTextBox");
            if(txtWP){
                txtWP.value(comObj.workPhone);
            }
            var txtCell = $("#txtCell").data("kendoMaskedTextBox");
            if(txtCell){
                txtCell.value(comObj.cellPhone);
            }*/
            //$("#txtHPhone").val(comObj.homePhone);
            $("#txtEmail").val(comObj.email);

            $("#txtCountry").val(comObj.country);
            // $("#cmbZip").val(comObj.zip);
            $("#txtZip4").val(comObj.zipFour);
            $("#txtState").val(comObj.state);
            $("#txtCity").val(comObj.city);


            /*setWorkPhoneMask();
            setExtensionMask();
            setCellPhoneMask();*/

            if (comObj.defaultCommunication == "1") {
                $("#rdHome").prop("checked", true);
            } else if (comObj.defaultCommunication == "2") {
                $("#rdWork").prop("checked", true);
            } else if (comObj.defaultCommunication == "3") {
                $("#rdCell").prop("checked", true);
            } else if (comObj.defaultCommunication == "4") {
                $("#rdEmail").prop("checked", true);
            }
        }

    }
}

function getTableListArray(dataObj) {
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.codeTable) {
        if ($.isArray(dataObj.response.codeTable)) {
            dataArray = dataObj.response.codeTable;
        } else {
            dataArray.push(dataObj.response.codeTable);
        }
    }
    var tempDataArry = [];
    var obj = {};
    obj.desc = "";
    obj.zip = "";
    obj.value = "";
    obj.idk = "";
    tempDataArry.push(obj);
    for (var i = 0; i < dataArray.length; i++) {
        if (dataArray[i].isActive == 1) {
            var obj = dataArray[i];
            obj.idk = dataArray[i].id;
            obj.status = dataArray[i].Status;
            tempDataArry.push(obj);
        }
    }
    return tempDataArry;
}

function getTableFirstListArray(dataObj) {
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.codeTable) {
        if ($.isArray(dataObj.response.codeTable)) {
            dataArray = dataObj.response.codeTable;
        } else {
            dataArray.push(dataObj.response.codeTable);
        }
    }
    var tempDataArry = [];
    for (var i = 0; i < dataArray.length; i++) {
        if (dataArray[i].isActive == 1) {
            var obj = dataArray[i];
            obj.idk = dataArray[i].id;
            obj.status = dataArray[i].Status;
            tempDataArry.push(obj);
        }
    }
    return tempDataArry;
}

function onClickReset() {
    operation = ADD;
    patientId = "";
    $("#txtID").val("");
    $("#txtExtID1").val("");
    $("#txtExtID2").val("");
    setComboReset("cmbPrefix");
    $("#txtFN").val("");
    $("#txtLN").val("");
    $("#txtMN").val("");
    $("#txtWeight").val("");
    $("#txtHeight").val("");
    $("#txtNN").val("");
    $("#txtBed").val("");
    $("#txtBAN").val("");
    billActNo = "";
    var dtDOB = $("#dtDOB").data("datepicker");
    /*if(dtDOB){
        dtDOB.value("");
    }*/
    $("#txtAge").val("");
    $("#txtSSN").val("");
    setComboReset("txtFAN");
    setComboReset("cmbStatus");
    setComboReset("cmbGender");
    setComboReset("cmbEthicity");
    setComboReset("cmbRace");
    // setComboReset("cmbLan");
    setComboReset("txtL1");
    setComboReset("txtL2");
    setComboReset("txtL3");
    $("#txtAdd1").val("");
    $("#txtAdd2").val("");
    setComboReset("cmbSMS");
    $("#txtCell").val("");
    $("#txtWPExt").val("");
    $("#txtWPExt").attr("disabled", true);
    $("#txtWP").val("");
    $("#txtExtension").val("");

    //resetMaskTextBoxValue("txtHPhone");
    //resetMaskTextBoxValue("txtWP");
    //resetMaskTextBoxValue("txtCell");

    $("#txtHPhone").val("");
    $("#txtEmail").val("");

    $("#rdHome").prop("checked", true);

    $('#cmbLang').closest('.ui-igcombo').find('.ui-igcombo-clearicon').trigger('click');setComboReset("cmbGender");
    setComboReset("cmbCarerGender");

}

function getMaskTextBoxValue(txtId) {
    var str = "";
    var txtHome = $("#" + txtId).data("kendoMaskedTextBox");
    if (txtHome) {
        str = txtHome.raw();
    }
    return str;
}

function setMaskTextBoxValue(txtId, strVal) {
    var txtHome = $("#" + txtId).data("kendoMaskedTextBox");
    if (txtHome) {
        txtHome.value(strVal);
    }
}

function resetMaskTextBoxValue(txtId) {
    var txtHome = $("#" + txtId).data("kendoMaskedTextBox");
    if (txtHome) {
        txtHome.value("");
    }
}

function setComboReset(cmbId) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb) {
        cmb.select(0);
    }
}

function validation() {
    var flag = true;
    var txtID = $("#txtExtID1").val();
    txtID = $.trim(txtID);
    /*if(txtID == ""){
        customAlert.error("Error","Enter patient External ID1");
        flag = false;
        return false;
    }*/
    var txtWeight = $("#txtWeight").val();
    txtWeight = $.trim(txtWeight);
    /* if (txtWeight == "") {
         customAlert.error("Error", "Enter patient weight");
         flag = false;
         return false;
     }*/
    var txtHeight = $("#txtHeight").val();
    txtHeight = $.trim(txtHeight);
    /*if (txtHeight == "") {
        customAlert.error("Error", "Enter patient height");
        flag = false;
        return false;
    }*/
    var cmbPrefix = $("#cmbPrefix").data("kendoComboBox");
    if (cmbPrefix && cmbPrefix.value() == "") {
        customAlert.error("Error", "Select prefix value");
        flag = false;
        return false;
    }
    var cmbType = $("#cmbType").data("kendoComboBox");
    if (cmbType && cmbType.value() == "") {
        customAlert.error("Error", "Select Default Call");
        flag = false;
        return false;
    }
    var cmbZip = $("#cmbZip").val();
    cmbZip = $.trim(cmbZip);
    if (cmbZip == "") {
        customAlert.error("Error", "Enter Postal code");
        flag = false;
        return false;
    }
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if (cmbStatus && cmbStatus.value() == "") {
        customAlert.error("Error", "Select status value");
        flag = false;
        return false;
    }
    var txtFN = $("#txtFN").val();
    txtFN = $.trim(txtFN);
    if (txtFN == "") {
        customAlert.error("Error", "Enter patient first name");
        flag = false;
        return false;
    }
    var txtAge = $("#txtAge").val();
    txtAge = $.trim(txtAge);
    if (txtAge == "") {
        customAlert.error("Error", "Enter patient date of birth");
        flag = false;
        return false;
    }

    var txtSSN = $('[name="txtSSN"]').val(); //$("#txtSSN").val();
    txtSSN = $.trim(txtSSN);
    /*if (txtSSN == "") {
        customAlert.error("Error", "Enter patient SSN");
        flag = false;
        return false;
    }*/
    var cntry = sessionStorage.countryName;
    if (txtSSN != "") {
        var len = txtSSN.length;
        if (cntry.indexOf("India") >= 0 || cntry.indexOf("United Kingdom") >= 0) {
            if (len != 10) {
                customAlert.error("Error", "Enter patient SSN length required");
                flag = false;
                return false;
            }
        } else if (cntry.indexOf("United State") >= 0) {
            if (len != 9) {
                customAlert.error("Error", "Enter patient SSN length required");
                flag = false;
                return false;
            }
        }
    }

    var sEmail = $('#txtEmail').val();
    if (sEmail && !validateEmail(sEmail)) {
        flag = false;
        return false;
        customAlert.error("Error", "your EmailId is invalid,Please enter the valid EmailId");
    }

    var cmbGender = $("#cmbGender").data("kendoComboBox");
    if (cmbGender && cmbGender.value() == "") {
        customAlert.error("Error", "Select gender");
        flag = false;
        return false;
    }
    var txtSSN = $("#txtSSN").val();
    txtSSN = $.trim(txtSSN);
    if (txtSSN == "") {
        customAlert.error("Error", "Enter patient SSN");
        flag = false;
        return false;
    }


    return flag;
}

function getComboDataItem(cmbId) {
    var dItem = null;
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb && cmb.dataItem()) {
        dItem = cmb.dataItem();
        return cmb.text();
    }
    return "";
}

function getComboDataItemValue(cmbId) {
    var dItem = null;
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb && cmb.dataItem()) {
        dItem = cmb.dataItem();
        return cmb.value();
    }
    return "";
}

function onClickSave(e) {
    /*var sEmail = $('#txtEmail').val();
    if (validateEmail(sEmail)) {
        }
else {
customAlert.error("Error","your EmailId is invalid,Please enter the valid EmailId");
//preventDefault();
}*/
    /*$("#txtHPhone").kendoMaskedTextBox({
        mask: "(999) 999-9999"
    });*/
    /*var txtHome = $("#txtHPhone").data("kendoMaskedTextBox");
    if(txtHome){
        $("#txtHPhone").val().replace(/[()\s-_]/g, "");
    }*/
    //if(validation()){
    e.preventDefault();
    $('.alert').remove();
    var strId = $("#txtID").val();
    var strExtId1 = $("#txtExtID1").val();
    var strExtId2 = $("#txtExtID2").val();
    var strPrefix = getComboDataItem("cmbPrefix");
    var strNickName = $("#txtNN").val();
    var strStatus = getComboDataItem("cmbStatus");
    var strFN = $("#txtFN").val();
    var strMN = $("#txtMN").val();
    if(txtMN == ""){
        txtMN = null;
    }
    var strLN = $("#txtLN").val();

    /*var dtItem = $("#dtDOB").data("kendoDatePicker");
    var strDate = "";
    if(dtItem && dtItem.value()){
        strDate = kendo.toString(dtItem.value(),"yyyy-MM-dd");
    }*/
    var strDate = "";
    var strhouseNumber = "";
    var dt = document.getElementById("dtDOB").value;
    var dob = null;
    var mm = 0;
    var dd = 0;
    var yy = 0;
    var cntry = sessionStorage.countryName;
    if (cntry.indexOf("India") >= 0) {
        var dtArray = dt.split("/");
        dob = (dtArray[1] + "/" + dtArray[0] + "/" + dtArray[2]);
    } else if (cntry.indexOf("United Kingdom") >= 0) {
        var dtArray = dt.split("/");
        dob = (dtArray[1] + "/" + dtArray[0] + "/" + dtArray[2]);
        if(IsPostalCodeManual =="1"){
            strhouseNumber = $("#cmbZip").val();
        }
    } else {
        var dtArray = dt.split("/");
        dob = (dtArray[0] + "/" + dtArray[1] + "/" + dtArray[2]);
    }
    if (dob) {
        var DOB = new Date(dob);
        strDate = kendo.toString(DOB, "yyyy-MM-dd");
    }

    if(strDate){
    	var stDate = new Date(strDate);
    	var currDate = new Date();
    	if(currDate.getTime()<stDate.getTime()){
    		var strMsg = "Invalid Date of Birth ";
    		$('.customAlert').append('<div class="alert alert-danger">'+strMsg+'</div>');
    		return false;
    	}
    }
    
    //$('.customAlert').append('<div class="alert alert-danger">Please fill all the Required Fields</div>');
    
    var strSSN = getMaskTextBoxValue("txtSSN"); //$('[name="txtSSN"]').val();//$("#txtSSN").val();
    console.log(strSSN);
    var strGender = getComboDataItem("cmbGender");
    var strAdd1 = $("#txtAdd1").val();
    var strAdd2 = $("#txtAdd2").val();
    var strCity = $("#txtCity").val();
    var strState = $("#txtState").val();
    var strCountry = $("#txtCountry").val();

    //  var strZip = getComboDataItemValue("cmbZip");
    var strZip4 = $("#txtZip4").val();
    //var strHPhone = $("#txtHPhone").val();
    var strHPhone = "";
    /*var txtHome = $("#txtHPhone").data("kendoMaskedTextBox");
    if(txtHome){
        strHPhone = txtHome.raw();
    }*/
    strHPhone = getMaskTextBoxValue("txtHPhone"); //$('[name="txtHPhone"]').val();//getMaskTextBoxValue("txtHPhone");
    var strWp = getMaskTextBoxValue("txtWP");
    var strCell = $("#txtCell").val();//getMaskTextBoxValue("txtCell"); //$('[name="txtCell"]').val();//getMaskTextBoxValue("txtCell"); //$("#txtCell").val();
    var cntry = sessionStorage.countryName;
    if (cntry.indexOf("United Kingdom") >= 0) {
        strHPhone = $("#txtHPhone").val();
       // strWp = $("#txtWP").val();
        strCell = $("#txtCell").val();
    }
    var strExt = $("#txtWPExt").val();
    //var strWp = $("#txtWP").val();
    var strWpExt = $("#txtWPExt").val();
    //var strCell = $("#txtCell").val();

    var strSMS = getComboDataItem("cmbSMS");

    var strEmail = $("#txtEmail").val();
    // var strLan = getComboDataItem("cmbLan");
    var strLan = getComboDataItem("txtL1") + ", " + getComboDataItem("txtL2") + ", " + getComboDataItem("txtL3");
    var strRace = getComboDataItem("cmbRace");
    var strEthinicity = getComboDataItem("cmbEthicity");
    var strNI = $("#txtNI").val();
    var strDnr = getComboDataItemValue("txtDNR");
    if(strDnr == "Y"){
    //	strDnr = "1";
    }
    var strMaritalStatus = getComboDataItem("cmbMarital");
    var strReligion = getComboDataItem("cmbReligion");

    var dataObj = {};
    dataObj.patient = {};
    dataObj.patient.bedNumber = $("#txtBed").val();
    dataObj.patient.roomNumber = $("#txtRoom").val();
    var txtFAN = $("#txtFAN").data("kendoComboBox");
    dataObj.patient.facilityId = txtFAN.value();
   // dataObj.patient.billAcNo = billActNo;
    dataObj.patient.createdBy = Number(sessionStorage.userId);; //"101";
    dataObj.patient.externalId1 = strExtId1;
    dataObj.patient.externalId2 = strExtId2;
    dataObj.patient.height = $("#txtHeight").val();;
    dataObj.patient.weight = $("#txtWeight").val();;
    dataObj.patient.prefix = strPrefix; //$("#txtEmail").val();;
    dataObj.patient.firstName = strFN;
    dataObj.patient.middleName = strMN;
    dataObj.patient.lastName = strLN;
    dataObj.patient.nickname = strNickName;
    dataObj.patient.dateOfBirth = strDate;
    dataObj.patient.ssn = strSSN;
    dataObj.patient.gender = strGender;
    dataObj.patient.ethnicity = strEthinicity;
    dataObj.patient.race = strRace;
    dataObj.patient.language = strLan;
    dataObj.patient.ni = strNI;
    dataObj.patient.dnr = strDnr;
    dataObj.patient.maritalStatus = strMaritalStatus;
    dataObj.patient.religion = strReligion;
    dataObj.patient.isActive = 1;
    dataObj.patient.isDelete = 0;
    dataObj.patient.status = strStatus;
    var cmbType = $("#cmbType").data("kendoComboBox");
    if(cmbType.value() != ""){
        dataObj.patient.nurseId = Number(cmbType.value());
    }

    dataObj.patient.vitals = [];

    var comm = [];
    var comObj = {};
    comObj.createdBy = Number(sessionStorage.userId); //"101";
    comObj.address1 = strAdd1;
    comObj.address2 = strAdd2;
    var cmbZip = $("#cmbZip").data("kendoComboBox");
    comObj.cityId = Number(cityId); //cmbZip.value();
    comObj.homePhone = (strHPhone);
    comObj.homePhoneExt = Number(strExt);
    comObj.workPhone = Number(strWp);
    comObj.workPhoneExt = Number(strWpExt);
    comObj.cellPhone = strCell;
    comObj.sms = strSMS;
    comObj.email = strEmail;
    comObj.houseNumber = strhouseNumber;
    var propVal = $("input:radio[name='Comm']:checked").val();
    comObj.defaultCommunication = Number(propVal);
    if (operation == UPDATE) {
        comObj.modifiedBy = Number(sessionStorage.userId);; //"101";
        comObj.id = commId;
        comObj.isDeleted = 0;
    }
    comObj.isActive = 1;
    comm.push(comObj);
    dataObj.communication = comm;
    console.log(dataObj);

    var txtWeight = $("#txtWeight").val();
    txtWeight = $.trim(txtWeight);
    var txtHeight = $("#txtHeight").val();
    txtHeight = $.trim(txtHeight);
    /*var cmbPrefix = $("#cmbPrefix").data("kendoComboBox");*/
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    var txtFN = $("#txtFN").val();
    txtFN = $.trim(txtFN);
    var txtMN = $("#txtMN").val();
    txtMN = $.trim(txtMN);
    var txtLN = $("#txtLN").val();
    txtLN = $.trim(txtLN);
    var txtNN = $("#txtNN").val();
    txtNN = $.trim(txtNN);
    var txtAge = $("#txtAge").val();
    txtAge = $.trim(txtAge);
    var txtSSN = $("#txtSSN").val();
    txtSSN = $.trim(txtSSN);
    var txtCell = $("#txtCell").val();
    txtCell = $.trim(txtCell);
    var cmbZip = $("#cmbZip").val();
    cmbZip = $.trim(cmbZip);
    /*var sEmail = $('#txtEmail').val();*/
    var cmbSMS = $("#cmbSMS").data("kendoComboBox");
    var cmbGender = $("#cmbGender").data("kendoComboBox");




    
    var txtL1 = $("#txtL1").data("kendoComboBox");

    if (txtL1 && txtL1.value() != "" && cmbStatus && cmbStatus.value() != "" && txtFN != "" && txtLN != "" && txtAge != "" && cmbGender && cmbGender.value() != "" && strAdd1 != "") {
        var cntry = sessionStorage.countryName;
        var flag = true;
        var strErrorMsg = "";
        var txtSSN = strSSN; //$('[name="txtSSN"]').val();//$("#txtSSN").val();
        var strSSNMsg = "";
        try {
            strSSNMsg = $("#lblNHS").text().split(":")[0]
        } catch (ex) {
            console.log(ex);
        }
        txtSSN = $.trim(txtSSN);
        // if (txtSSN == "") {
        //     strErrorMsg = "Enter patient " + strSSNMsg;
        //     // customAlert.error("Error", "Enter patient " + strSSNMsg);
        //     // strErrorMsg = "Enter patient " + strSSNMsg;
        //     flag = false;
        //     // return;
        // }
        if (txtSSN != "") {
            var len = txtSSN.length;
            if (cntry.indexOf("India") >= 0 || cntry.indexOf("United Kingdom") >= 0) {
                if (len != 10) {
                    strErrorMsg = "Enter patient " + strSSNMsg + " length required";
                    // customAlert.error("Error", "Enter patient " + strSSNMsg + " length required");
                    flag = false;
                    // return;
                }
            } else if (cntry.indexOf("United State") >= 0) {
                if (len != 10) {
                    strErrorMsg = "Enter patient " + strSSNMsg + " length required";
                    // customAlert.error("Error", "Enter patient " + strSSNMsg + " length required");
                    flag = false;
                    // return;
                }
            }
        }


        if (cntry.indexOf("United Kingdom") >= 0) {
            if (IsPostalCodeManual == "1") {
                var strCity = $("#txtCity").val();
                if(strCity == ""){
                    strErrorMsg = "Enter city";
                    flag = false;
                }
            }
            else {
                strErrorMsg = "Enter postal code";
                flag = false;
            }
        }
        else{
            if(cmbZip && cmbZip.value == ""){
                strErrorMsg = "Enter postal code";
                flag = false;
            }
        }

        var propVal = $("input:radio[name='Comm']:checked").val();
        if (propVal == "1") {
            // if (strHPhone == "") {
            //     flag = false;
            //     strErrorMsg = "You are selected Home Phone as Deault Communication. Please Enter Home phone";
            //     // customAlert.error("Error", "You are selected Home Phone as Deault Communication. Please Enter Home phone");
            //     // return;
            // }
        }

        if (strHPhone) {
            var len = strHPhone.length;
            if (cntry.indexOf("India") >= 0 || cntry.indexOf("United State") >= 0) {
                if (len != 10) {
                    // customAlert.error("Error", "Enter Homephone length required");
                    strErrorMsg = "Enter Homephone length required";
                    flag = false;
                    // return;
                }
            }
        }

        // if (propVal == "3") {
        //     if (strCell == "") {
        //         flag = false;
        //         customAlert.error("Error", "You are selected Cell Phone  as Deault Communication. Please Enter Cell phone");
        //         return;
        //     }
        // }
        var cmbSMS = $("#cmbSMS").data("kendoComboBox");
        if (cmbSMS.text() == "Yes") {
            if (strCell == "") {
                flag = false;
                strErrorMsg = "Enter cell phone";
                // customAlert.error("Error", "Enter cell phone");
                // return;
            }
        }
        if (strCell) {
            var len = strCell.length;
            if (cntry.indexOf("India") >= 0 || cntry.indexOf("United State") >= 0) {
                if (len != 10) {
                    strErrorMsg = "Enter cell phone length required";
                    // customAlert.error("Error", "Enter cell phone length required");
                    flag = false;
                    // return;
                }
            }
        }

        if (txtFAN && txtFAN.value() == "") {
            strErrorMsg = "Select faciliy name";
            flag = false;
        }

        var sEmail = $('#txtEmail').val();
        if (propVal == "4") {
            // if (sEmail == "") {
            //     // customAlert.error("Error", "You are selected Email as Deault Communication. Please Enter Email Address");
            //
            //     strErrorMsg = "You are selected Email as Deault Communication. Please Enter Email Address";
            //     flag = false;
            //     // return;
            // }
        }

        if (sEmail && !validateEmail(sEmail)) {
            flag = false;
            strErrorMsg = "your EmailId is invalid,Please enter the valid EmailId";
            // customAlert.error("Error", "your EmailId is invalid,Please enter the valid EmailId");
            // return;
        }
        /*if (strExt != "") {
            if (strWp == "") {
                flag = false;
                customAlert.error("Error", "Enter Workphone");
                return;
            } else {
                var len = strWp.length;
                if (cntry.indexOf("India") >= 0 || cntry.indexOf("United State") >= 0) {
                    if (len != 10) {
                        customAlert.error("Error", "Enter work phone length required");
                        flag = false;
                        return;
                    }
                }
            }
        }*/

        if(flag){
            $("btnSave").attr("disabled", true);
            if (operation == ADD && patientId != " ") {
                var dataUrl = ipAddress + "/patient/create";
                createAjaxObject(dataUrl, dataObj, "POST", onCreate, onError);
            } else if (operation == UPDATE) {
                dataObj.patient.modifiedBy = Number(sessionStorage.userId); //"101";
                dataObj.patient.id = patientId;
                dataObj.patient.isDeleted = "0";
                dataObj.patient.photoExt = photoExt;
                var dataUrl = ipAddress + "/patient/update";
                createAjaxObject(dataUrl, dataObj, "POST", onCreate, onError);
            }
        }
        else {
            $("body,html").animate({
                scrollTop: 0
            }, 500);
            $('.customAlert').append('<div class="alert alert-danger">'+ strErrorMsg + '</div>');
        }
    } else {
        $("body,html").animate({
            scrollTop: 0
        }, 500);
        $('.customAlert').append('<div class="alert alert-danger">Please fill all the Required Fields</div>');
    }
    /*}else{
         e.preventDefault();
    }*/
}

function onClickResetAdditional(e) {
    e.preventDefault();
    $('#additionalInfoTab').find('input').val('');
    setComboReset("txtTalk");
    setComboReset("txtGlass");
    setComboReset("txtHear");
    setComboReset("txtT1alk");
    setComboReset("txtDalk");
    setComboReset("txtTdalk");
}

function onClickSaveAdditional(e) {
    e.preventDefault();
    $('.alert').remove();

    var txtTalk = $("#txtGlass").data("kendoComboBox");
    var petsVal = $('#txtTdalk').val() ? parseInt($('#txtTdalk').val()) : '';
    var petsNotesVal = $('#txtTdalkNotes').val();
    var smokerVal = $("#txtT1alk").val() ? parseInt($('#txtT1alk').val()) : '';
    var smokerNotesVal = $('#txtT1alkNotes').val();
    var visionGlassesVal = $("#txtGlass").val() ? parseInt($('#txtGlass').val()) : '';
    var visionGlassesNotesVal = txtTalk.text() + '~'+ $('#txtGlassNotes').val();
    var alcoholVal = $("#txtDalk").val() ? parseInt($('#txtDalk').val()) : '';
    var alcoholNotesVal = $('#txtDalkNotes').val();
    var hearingAidVal = $('#txtHear').val() ? parseInt($('#txtHear').val()) : '';
    var hearingAidNotesVal = $('#txtHearNotes').val();
    var abilityToTalkVal = $('#txtTalk').val() ? parseInt($('#txtTalk').val()) : '';
    var abilityToTalkNotesVal = $('#txtTalkNotes').val();

    var mobilityVal = $('#txtMobility').val();
    var fileLocationOnPropertyVal = $('#fileLocationOnProperty').val();
    var spiritualOrCultureNeedsVal = $('#txtSpiritualNeeds').val();
    var physiologicalNeedsVal = $('#physiologicalNeeds').val();
    var parkigInstructionsVal = $('#parkigInstructions').val();
    var accessToPropertyVal = $('#accessToProperty').val();
    var dolsVal = $('#txtdols').val();
    var sopaOrPovaVal = $('#txtSopaPova').val();
    var cmbCarerGender = $("#cmbCarerGender").data("kendoComboBox");
    var strLan = $("#cmbLang").val();
    var obj = {
        "mobility": mobilityVal,
        "patientId": patientId,
        "carerLanguage": strLan,
        "isActive": 1,
        "fileLocationOnProperty": fileLocationOnPropertyVal,
        "isDeleted": 0,
        "spiritualOrCultureNeeds": spiritualOrCultureNeedsVal,
        "physiologicalNeeds": physiologicalNeedsVal,
        "carerGender": cmbCarerGender.text(),
        "parkigInstructions": parkigInstructionsVal,
        "createdBy": sessionStorage.userId,
        "accessToProperty": accessToPropertyVal,
        "dols": dolsVal,
        "sopaOrPova": sopaOrPovaVal
    };
    if(petsNotesVal.length > 0){
        obj.pets= petsVal;
        obj.petsNotes= petsNotesVal;
    }
    if(smokerNotesVal.length > 0){
        obj.smoker= smokerVal;
        obj.smokerNotes= smokerNotesVal;
    }
    if(visionGlassesNotesVal.length > 0){
        obj.visionGlasses= visionGlassesVal;
        obj.visionGlassesNotes= visionGlassesNotesVal;
    }
    if(alcoholNotesVal.length > 0){
        obj.alcohol= alcoholVal;
        obj.alcoholNotes= alcoholNotesVal;
    }
    if(hearingAidNotesVal.length > 0){
        obj.hearingAid= hearingAidVal;
        obj.hearingAidNotes= hearingAidNotesVal;
    }
    if(abilityToTalkNotesVal.length > 0){
        obj.abilityToTalk= abilityToTalkVal;
        obj.abilityToTalkNotes= abilityToTalkNotesVal;
    }
    if($('#btnSaveAdditional').attr("data-attr-update-additional") == "true") {
        obj.id = patientAdditionalId;
        obj.modifiedBy = sessionStorage.userId;
        obj.modifiedDate = new Date().getTime();
        createAjaxObject(dataTabURL, obj, "PUT", onAdditionalCreate, onError);
    } else {
        createAjaxObject(dataTabURL, obj, "POST", onAdditionalCreate, onError);
    }
}
var patientAdditionalId = "";
function onAdditionalCreate(dataObj) {
    console.log(dataObj);
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            $('body, html').stop().animate({ scrollTop: 0 }, 500);
            if($('#btnSaveAdditional').attr("data-attr-update-additional") == "true") {
                $('#additionalInfoTab').prepend('<div class="alert alert-success">Additional Information is updated successfully</div>');
            } else {
                $('#additionalInfoTab').prepend('<div class="alert alert-success">Additional Information is saved successfully</div>');
            }
            $('#additionalInfoTab').find('input').val('');
            setComboReset("txtTalk");
            setComboReset("txtGlass");
            setComboReset("txtHear");
            setComboReset("txtT1alk");
            setComboReset("txtDalk");
            setComboReset("txtTdalk");
            setComboReset("cmbCarerGender");
            getAjaxObject(dataTabURL, "GET", getAdditionalData, onError);
        } else {
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}

function onCreate(dataObj) {
    console.log(dataObj);
    if (dataObj && dataObj.response && dataObj.response.status) {
        $("btnSave").attr("disabled", false);
        if (dataObj.response.status.code == "1") {
            /**/
        	if(operation == ADD){
                patientId = dataObj.response.patient.id;
                parentRef.patientId = patientId;
                // if(isBrowseFlag) {
                    onClickUploadPhoto();
                // }
        		 var obj = {};
                 obj.status = "success";
                 obj.operation = operation;
                 // popupClose(obj);
                displaySessionErrorPopUp("Info", "Service User Created Successfully", function(res) {
                    popupClose(obj);
                })
        	}else{
                // if(isBrowseFlag) {
                    onClickUploadPhoto();
                // }
        		console.log("updated");
        		 // $('#patientInfoTab').prepend('<div class="alert alert-success">Service User Information is updated successfully</div>');
        		 //customAlert.error("info", "Service User Updated Successfully");
                // customAlert.info("info", "Service User Information is updated successfully");
                // popupClose(obj);
                displaySessionErrorPopUp("Info", "Service User Information is updated successfully", function(res) {
                    popupClose(obj);
                })
        	}
        } else {
            customAlert.error("error", dataObj.response.status.message);
        }
    }
    /**/
}
function getAdditionalData(dataObj) {
    console.log(dataObj);
    if (dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1") {
        if (dataObj.response.patientExt.length > 0) {
            var dataArray = dataObj.response.patientExt[0];
            patientAdditionalId = dataArray.id;
            getComboListIndexBoolean("txtTdalk", "Key", dataArray.pets);
            $('#txtTdalkNotes').val(dataArray.petsNotes);
            getComboListIndexBoolean("txtT1alk", "Key", dataArray.smoker);
            $('#txtT1alkNotes').val(dataArray.smokerNotes);
            // getComboListIndexBoolean("txtGlass", "Key", dataArray.visionGlasses);
            var notes = dataArray.visionGlassesNotes.split('~');
            if (notes && notes.length > 0) {
                $('#txtGlassNotes').val(notes[1]);
                $('#txtGlass').data("kendoComboBox").text(notes[0]);

            }

            getComboListIndexBoolean("txtDalk", "Key", dataArray.alcohol);
            $('#txtDalkNotes').val(dataArray.alcoholNotes);
            getComboListIndexBoolean("txtHear", "Key", dataArray.hearingAid);
            $('#txtHearNotes').val(dataArray.hearingAidNotes);
            getComboListIndexBoolean("txtTalk", "Key", dataArray.abilityToTalk);
            $('#txtTalkNotes').val(dataArray.abilityToTalkNotes);

            $('#txtMobility').val(dataArray.mobility);
            $('#fileLocationOnProperty').val(dataArray.fileLocationOnProperty);
            $('#txtSpiritualNeeds').val(dataArray.spiritualOrCultureNeeds);
            $('#physiologicalNeeds').val(dataArray.physiologicalNeeds);
            $('#parkigInstructions').val(dataArray.parkigInstructions);
            $('#accessToProperty').val(dataArray.accessToProperty);
            $('#txtdols').val(dataArray.dols);
            $('#txtSopaPova').val(dataArray.sopaOrPova);

            getComboListIndex("cmbCarerGender", "desc", dataArray.carerGender);
            getComboListMultipleIndex("cmbLang", "desc", dataArray.carerLanguage);
            $('#btnSaveAdditional').attr("data-attr-update-additional", "true");
        } else {
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}

function getComboListIndexBoolean(cmbId, attr, attrVal) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb) {
        var ds = cmb.dataSource;
        var totalRec = ds.total();
        for (var i = 0; i < totalRec; i++) {
            var dtItem = ds.at(i);
            if (dtItem && parseInt(dtItem[attr]) === attrVal) {
                cmb.select(i);
                return i;
            }
        }
    }
    return -1;
}


function onError(errObj) {
    console.log(errObj);
    customAlert.error("Error", "Error");
}

function onClickCancel() {
    var obj = {};
    obj.status = "false";
    popupClose(obj);
}

function onClickCancelAdditional() {
    var obj = {};
    obj.status = "false";
    popupClose(obj);
}

function onClickSearch() {
    var obj = {};
    obj.status = "search";
    popupClose(obj);
}

function popupClose(obj) {
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}

function getCountryIsPostalCode() {
    if (cntry.indexOf("United Kingdom") >= 0) {
        if (IsPostalCodeManual == "1") {
            IsPostalFlag = "1";
        }
    }
}



/*-----------------------------
 Voice Recognition
 ------------------------------*/

// If false, the recording will stop after a few seconds of silence.
// When true, the silence period is longer (about 15 seconds),
// allowing us to keep recording even when the user pauses.
recognition.continuous = true;

// This block is called every time the Speech APi captures a line.
recognition.onresult = function(event) {

    // event is a SpeechRecognitionEvent object.
    // It holds all the lines we have captured so far.
    // We only need the current one.
    var current = event.resultIndex;

    // Get a transcript of what was said.
    var transcript = event.results[current][0].transcript;

    // Add the current transcript to the contents of our Note.
    // There is a weird bug on mobile, where everything is repeated twice.
    // There is no official solution so far so we have to handle an edge case.
    var mobileRepeatBug = (current == 1 && transcript == event.results[0][0].transcript);

    if(!mobileRepeatBug) {
        if(transcript.includes("first name")){
            $("#txtFN").val("");
            noteContent = transcript.replace("first name","");
            $("#txtFN").val(noteContent);
        }

        if(transcript.includes("last name")){
            $("#txtLN").val("");
            noteContent = transcript.replace("last name","");
            $("#txtLN").val(noteContent);
        }

        if(transcript.includes("address one")){
            $("#txtAdd1").val("");
            noteContent = transcript.replace("address one","");
            $("#txtAdd1").val(noteContent);
        }

        if(transcript.includes("race")){
            noteContent = transcript.replace("race ","");
            noteContent = noteContent.charAt(0).toUpperCase() + noteContent.toLowerCase().slice(1);
            getComboListIndex("cmbRace", "desc", noteContent.trim());
        }

        if(transcript.includes("facility name")){
            noteContent = transcript.replace("facility name ","");
            // noteContent = noteContent.charAt(0).toUpperCase() + noteContent.toLowerCase().slice(1);
            selectFacilityValue(noteContent.trim());
        }

        if(transcript.includes("browse")){
            $("#btnBrowse").click = function(e) {
                alert('el1 event');
                $("#btnBrowse").click(e);
                onClickBrowse(e);
            };

        }

        if(transcript.includes("gender")){
            noteContent = transcript.replace("gender ","").toLowerCase();
            noteContent = noteContent.charAt(0).toUpperCase() + noteContent.toLowerCase().slice(1);
            getComboListIndex("cmbGender", "desc", noteContent.trim());
            onGenderChange();
        }
        if(transcript.includes("preferred")){
            noteContent = transcript.replace("preferred ","");
            noteContent = noteContent.charAt(0).toUpperCase() + noteContent.toLowerCase().slice(1);
            getComboListIndex("txtL1", "desc", noteContent.trim());
        }
        if(transcript.includes("city")){
            noteContent = transcript.replace("city ","");
            onClickZipSearch();
        }
        if(transcript.includes("video start")){
            $("#startCamera-switch").prop("checked", true);
            $("#btnStartVideo").click();
            onClickStartVideo();
            $("#btnTakePhoto").click();
        }

        if(transcript.includes("reset")){
            $("#btnReset").click();
        }

        if(transcript.includes("save")){
            $("#btnSave").click();
            $("#btnCancel").click();
        }

        if(transcript.includes("cancel")){
            $("#btnCancel").click();
        }
    }
};


// document.body.onclick = function(e) {
//     if (e.target.matches('button')) return;
//     recognition.start();
//     console.log('Listening');
// }

recognition.onstart = function() {
    instructions.text('Voice recognition activated. Try speaking into the microphone.');
}

recognition.onspeechend = function() {
    instructions.text('You were quiet for a while so voice recognition turned itself off.');
}

recognition.onerror = function(event) {
    if(event.error == 'no-speech') {

        instructions.text('No speech was detected. Try again.');
    };
}





/*-----------------------------
 App buttons and input
 ------------------------------*/
$('#note-record-btn').on('click', function(e) {
    recognition.stop();
    if (noteContent.length) {
        noteContent += ' ';
    }
    // Flag = 1;
    // $('#start-record-btn').removeClass("imgBorder");
    $('#note-record-btn').addClass("imgBorder");
    recognition.start();
});


// Sync the text inside the text area with the noteContent variable.
notefirstName.on('input', function() {
    noteContent = $(this).val();
})




/*-----------------------------
 Speech Synthesis
 ------------------------------*/

function readOutLoud(message) {
    var speech = new SpeechSynthesisUtterance();

    // Set the text and voice attributes.
    speech.text = message;
    speech.volume = 1;
    speech.rate = 1;
    speech.pitch = 1;

    window.speechSynthesis.speak(speech);
}

function selectFacilityValue(name){
    for(var i=0;i<speechfacilityArry.length;i++) {
        if(speechfacilityArry[i].name.toLowerCase()== name){
            getComboListIndex("txtFAN", "idk", speechfacilityArry[i].idk);
            onFacilityChange();
        }
    }
}
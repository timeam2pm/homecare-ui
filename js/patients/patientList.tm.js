var parentRef = null;
var dietDataArray = [];
var excerDataArr = [];
var conversationsClient = null;
var activeConversation = null;
var previewMedia = null;
var ws = null;
var userCalling = false;
//var invFlag = true;
//var ipAddress = "http://192.168.0.106:8080";
//var ipAddress = "http://stage.timeam.com";
var strPatientId = "";
var ptExtId2 = "";

var angularUIgridWrapper = null;
var rangePicker = null;
var videoClient = null;

var participantArray = [];

var angularPTUIgridWrapper = null;//AngularUIGridWrapper();

$(document).ready(function(){
    themeAPIChange();
    //Loader.showLoader();
    conversationsClient = null;
    parentRef = parent.frames['iframe'].window;
    parentRef.invFlag = true;

    var cntry = sessionStorage.countryName;
    if((cntry.indexOf("India")>=0) || (cntry.indexOf("United Kingdom")>=0)){
        $("#spFacility").text("Location");
    }else if(cntry.indexOf("United State")>=0){
        $("#spFacility").text("Facility");
    }

});

$(window).load(function(){
    showPanelHeight();
    buttonEvents();
    init();
    //adjustVideoHeight();
});

$(window).resize(function(){
    showPanelHeight();
    adjustHeight();
    adjustVideoHeight();
});


function adjustVideoHeight(){
    var pnlHeight = window.innerHeight;
    var imgHeight = pnlHeight-500;
    $("#divPtVideo").height(imgHeight);
    $("#divNurVideo").height(imgHeight);
    $("#divLocVideo").height(imgHeight);
}
function showPanelHeight(){
    var pnlHeight = window.innerHeight;
    console.log(pnlHeight);
    pnlHeight = pnlHeight-100;
    //$("#divLoginBox").css({height:(cmpHeight)+'px'});
    $("#leftPanel").css({height:(pnlHeight+20)+'px'});
    $("#centerPanel").css({height:(pnlHeight)+'px'});
    $("#rightPanel").css({height:(pnlHeight)+'px'});
    //$("#leftPanel").height(pnlHeight+20);
    //$("#centerPanel").height(pnlHeight);
    //$("#rightPanel").height(pnlHeight);
    $("#leftButtonBar").height(pnlHeight);
    $("#divPanel").height((pnlHeight-100));
    $("#divVideoCall").height((pnlHeight-50));
}
function buttonEvents(){
    // $("#divActivityList").off("click");
    // $("#divActivityList").on("click",function() {
    //         if (sessionStorage.IsActivity == 2) {
    //             var msg = "Application is set to Appointment Reasons Tasks. Do you want to open from here?";
    //             displaySessionErrorPopUp("Info", msg, function(res) {
    //                 $("#ptframe").attr("src", "../../html/masters/attachTask.html");
    //             });
    //         }
    //         else {
    //             onClickShowPatientActivities();
    //         }
    //     }

    // );

    $("#divActivityListOld").off("click");
    $("#divActivityListOld").on("click",onClickShowActivityReportListOld);

    $("#divActivityList").off("click");
    $("#divActivityList").on("click",onClickShowActivityReportList);

    $("#divPtDetails1").off("click");
    $("#divPtDetails1").on("click",onClickShowPatientDetails);

    $("#divPatientVisits").off("click");
    $("#divPatientVisits").on("click",onClickShowPatientVisits);


    $("#imgCarePlan").off("click");
    $("#imgCarePlan").on("click",onClickShowCarePlan);

    $("#divPatientMedication").off("click");
    $("#divPatientMedication").on("click",onClickShowPatientMedications);

    $("#divPatientDiet").off("click");
    $("#divPatientDiet").on("click",onClickShowPatientDiet);

    $("#divPatientIllness").off("click");
    $("#divPatientIllness").on("click",onClickShowPatientIllness);

    $("#divPatientPreventiveCare").off("click");
    $("#divPatientPreventiveCare").on("click",onClickShowPatientPreventiveCare);

    $("#divCallHostory").off("click");
    $("#divCallHostory").on("click",onClickShowPatientCallHistory);

    $("#divPatientHolter").off("click");
    $("#divPatientHolter").on("click",onClickShowPatientHolterReports);

    $("#divPatientNotes").off("click");
    $("#divPatientNotes").on("click",onClickShowPatientNotes);

    $("#divPatientVitals").off("click");
    $("#divPatientVitals").on("click",onClickShowPatientVitals);




    $("#divPatientAllergy").off("click");
    $("#divPatientAllergy").on("click",onClickShowPatientAllergy);

    $("#divPatientBilling").off("click");
    $("#divPatientBilling").on("click",onClickShowPatientBilling);

    $("#divPatientDocuments").off("click");
    $("#divPatientDocuments").on("click",onClickShowPatientDocuments);

    $("#addPt").off("click",onClickAddPatient);
    $("#addPt").on("click",onClickAddPatient);

    $("#searchPt").off("click",onClickSearchPatient);
    $("#searchPt").on("click",onClickSearchPatient);

    $("#openCallChart").off("click",onClickPatientChart);
    $("#openCallChart").on("click",onClickPatientChart);

    $("#searchCallChart").off("click",onClickSearchChart);
    $("#searchCallChart").on("click",onClickSearchChart);

    $("#imgNurse").off("click");
    $("#imgNurse").on("click",onClickNurse);

    $("#lblCreate").off("click");
    $("#lblCreate").on("click",onClickCreateInterest1);

    $("#lblEdit").off("click");
    $("#lblEdit").on("click",onClickEditInterest);

    $("#btnCall").off("click");
    $("#btnCall").on("click",onClickCall);

    $("#btnAccept").off("click");
    $("#btnAccept").on("click",onClickAccept);

    $("#btnDecline").off("click");
    $("#btnDecline").on("click",onClickDecline);

    $("#btnDisconnect").off("click");
    $("#btnDisconnect").on("click",onClickDisconnect);

    $("#lblView").off("click");
    $("#lblView").on("click",onClickViewPatientHistory);

    $("#btnClose").off("click");
    $("#btnClose").on("click",onClickCloseDisconnect);
    //$("#imgNurse").trigger("click");

    $("#lblImport").off("click");
    $("#lblImport").on("click",onBrowseClick);

    $("#fileElem").on("change", onSelectionFiles);

    $("#lblPatientEdit").off("click");
    $("#lblPatientEdit").on("click",onClickPatientInfo);

    $("#lblPatientIllness").off("click");
    $("#lblPatientIllness").on("click",onClickAddIllness);

    $("#lblPatientDiet").off("click");
    $("#lblPatientDiet").on("click",onClickAddPatientDiet);

    $("#lblPatientExcersz").off("click");
    $("#lblPatientExcersz").on("click",onClickAddPatientExercise);

    $("#lblAddAppointment").off("click");
    $("#lblAddAppointment").on("click",onClickAppointment);

    $("#lblPatientActivities").off("click");
    $("#lblPatientActivities").on("click",onClickAddPatientActivities);


    $("#lblRefresh").off("click");
    $("#lblRefresh").on("click",onClickVitalsRefresh);

    $("#lblVitalToggle").off("click");
    $("#lblVitalToggle").on("click",onClickGetVitalData);

    $("#divPatientInfo").off("click");
    $("#divPatientInfo").on("click",getPatientInfomation);

    $("#divPatientCallHis").off("click");
    $("#divPatientCallHis").on("click",getPatientCallChartHistory);

    $("#divHolorReport").off("click");
    $("#divHolorReport").on("click",getHolerReports);

    $("#divPreventiveCare").off("click");
    $("#divPreventiveCare").on("click",getInterests);

    $("#divAppointments").off("click");
    $("#divAppointments").on("click",getAppointment);

    $("#divIllness").off("click");
    $("#divIllness").on("click",getIllness);

    $("#divPills").off("click");
    $("#divPills").on("click",getMedicationData);

    $("#btnSearch").off("click");
    $("#btnSearch").on("click",onPatientSearch);

    $("#btnPatientSearch").off("click");
    $("#btnPatientSearch").on("click", onClickAddPatient);

    $("#txtInviteName").keyup(function(event){
        if(event.keyCode == 13){
            $("#btnCall").trigger("click");
        }
    });

    $('#seFacility').on('change', function(e) {
        e.preventDefault();
        buildPatientList([]);
        $("#mySelect").val(1);
       // onPtChange();
        onPatientSearch();
        angularPTUIgridWrapper.refreshGrid();
    });

    allowNumerics("txtPID");
}

function onClickAddPatient() {
    var myIframe = parent.frames["ptframe"];
    if (myIframe && myIframe.contentWindow) {
        myIframe.contentWindow.onClickAddPatient();
    }
}

var callTimer = null;
function callPatientData(){
    var frame = parent.frames["ptframe"];
    if(frame && frame.PtNo){
        clearInterval(callTimer);
        callTimer = null;
        strPatientId = frame.PtNo;
        if(strPatientId){
            patentComplePanelInfo();
        }

    }
    console.log(frame.PtNo);
}

function getCallPatientId(){
    if(callTimer){
        clearInterval(callTimer);
        callTimer = null;
    }
    //callTimer = setInterval(callPatientData,1000);
}
function onPtChange(){
    $("#txtPID").val('');
    patientSearch();
}
function patientSearch(){
    var searchValue = document.getElementById("mySelect").value;
    //console.log(searchValue);
    if(searchValue == 1){
        $("#divPPanel").hide();
    }else{
        $("#divPPanel").show();
    }
    buildPatientList([]);
    if(searchValue == 1){
        var myStatus = $("#myStatus").data("kendoComboBox");
        patientListURL = ipAddress+"/patient/list/";
        if(sessionStorage.userTypeId == "200" || sessionStorage.userTypeId == "100"){
            patientListURL = ipAddress+"/patient/list?status="+myStatus.text();
        }else{

        }
        getAjaxObject(patientListURL,"GET",onPatientListData1,onErrorMedication);
    }
    //onPatientSearch();
}
function allowNumerics(ctrlId){
    $("#"+ctrlId).keypress(function (e) {
        var searchValue = document.getElementById("mySelect").value;
        if(searchValue == 2){
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        }

    });
}
function onPatientSearch(){
    var searchValue = document.getElementById("mySelect").value;
    var myStatus = $("#myStatus").data("kendoComboBox");
    var pId = $("#txtPID").val();
    var facilityId = $("#seFacility").val();
    console.log(searchValue);
    pId = $.trim(pId);
    console.log(pId);
    var patientListURL = "";
    buildPatientList([]);
    if(searchValue == 1){
        patientListURL = ipAddress+"/patient/list/";
        if(sessionStorage.userTypeId == "200" || sessionStorage.userTypeId == "100"){
            patientListURL = ipAddress+"/patient/list?status="+myStatus.text();
        }
        // getAjaxObject(patientListURL,"GET",onPatientListData1,onErrorMedication);

    }else if(searchValue == 2){
        if(pId != ""){
            patientListURL = ipAddress+"/patient/list/?id="+pId+"&status="+myStatus.text();
            /*console.log(patientListURL);
            getAjaxObject(patientListURL,"GET",onPatientListData1,onErrorMedication);*/

        }
    }else if(searchValue == 3){
        if(pId != ""){
            patientListURL = ipAddress+"/patient/list/?first-name="+pId+"&status="+myStatus.text();
            /*getAjaxObject(patientListURL,"GET",onPatientListData1,onErrorMedication);*/
        }
    }else if(searchValue == 4){
        if(pId != ""){
            patientListURL = ipAddress+"/patient/list/?last-name="+pId+"&status="+myStatus.text();
            /*getAjaxObject(patientListURL,"GET",onPatientListData1,onErrorMedication);*/
        }
    }else if(searchValue == 5){
        if(pId != ""){
            var pArray = pId.split(",");
            if(pArray){
                if(pArray.length == 2){
                    var fName = pArray[0];
                    var lName = pArray[1];
                    if(fName && lName){
                        patientListURL = ipAddress+"/patient/list/?first-name="+fName+"&last-name="+lName+"&status="+myStatus.text();
                        /*getAjaxObject(patientListURL,"GET",onPatientListData1,onErrorMedication);*/
                    }else if(fName){
                        patientListURL = ipAddress+"/patient/list/?first-name="+fName+"&status="+myStatus.text();
                        /*getAjaxObject(patientListURL,"GET",onPatientListData1,onErrorMedication);*/
                    }else if(lName){
                        patientListURL = ipAddress+"/patient/list/?last-name="+lName+"&status="+myStatus.text();
                        /*getAjaxObject(patientListURL,"GET",onPatientListData1,onErrorMedication);*/
                    }
                }else if(pArray.length == 1){
                    patientListURL = ipAddress+"/patient/list/?first-name="+pId+"&status="+myStatus.text();
                    /*getAjaxObject(patientListURL,"GET",onPatientListData1,onErrorMedication);*/
                }
            }

        }
    }
        if(facilityId != 0){
            getAjaxObject(patientListURL+"&facility-id="+facilityId,"GET",onPatientListData1,onErrorMedication);
        }else{
            getAjaxObject(patientListURL,"GET",onPatientListData1,onErrorMedication);
        }

}
function getPatientStatus(){
    getAjaxObject(ipAddress + "/master/Patient_Status/list/", "GET", getPatientStatusValueList, onErrorMedication);
}

function getPatientStatusValueList(dataObj) {
    var dArray = getTableFirstListArray(dataObj);
    if (dArray && dArray.length > 0) {
        setDataForSelection(dArray, "myStatus", onPtChange, ["desc", "idk"], 0, "");
    }
}
function getTableFirstListArray(dataObj) {
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.codeTable) {
        if ($.isArray(dataObj.response.codeTable)) {
            dataArray = dataObj.response.codeTable;
        } else {
            dataArray.push(dataObj.response.codeTable);
        }
    }
    var tempDataArry = [];
    for (var i = 0; i < dataArray.length; i++) {
        if (dataArray[i].isActive == 1) {
            var obj = dataArray[i];
            obj.idk = dataArray[i].id;
            obj.status = dataArray[i].Status;
            tempDataArry.push(obj);
        }
    }
    return tempDataArry;
}
function onClickCreateInterest1(){
    parentRef.screenType = "Create";
    onClickCreateInterest();
}
function onBrowseClick(e){
    console.log(e);
    if(e.currentTarget && e.currentTarget.id){
        var btnId = e.currentTarget.id;
        var fileTagId = ("fileElem"+btnId);
        $("#fileElem").click();
    }
}
function onSelectionFiles(event){
    console.log(event);
    var files = event.target.files;
    var fileName = "";
    //$('#tiFileName').prop('value', fileName);
    if(files){
        if(files.length > 0){
            fileName = event.currentTarget.files[0].name;

            //var reqUrl = getRootUrl+"FileIOServlet?serviceName=DCDeviceMetadataConfigFileImportUIService&action=uploadDeviceMetadataConfigFile&sessionId="+getSessionId+"&headerRow=true&Config="+strConfigFile+"&MCK="+mck+"rand="+Math.ceil(Math.random()*100000);

            var reqUrl = ipAddress+"/patient/vital/upload/"+strPatientId;

            var formData = new FormData();
            formData.append("desc", fileName);
            formData.append("file",event.currentTarget.files[0]);

            Loader.showLoader();
            $.ajax({
                url: reqUrl,
                type: 'POST',
                data: formData,
                processData: false,
                contentType:false,// "multipart/form-data",
                success: function(data, textStatus, jqXHR){
                    //console.log('data val: '+data);
                    //	document.body.style.cursor = 'auto';
                    if(typeof data.error === 'undefined'){
                        // Success so call function to process the form
                        Loader.hideLoader();
                        submitForm(data);
                    }else{
                        // Handle errors here
                        Loader.hideLoader();
                        //console.log('ERRORS: ' + data.error);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown){
                    // Handle errors here
                    Loader.hideLoader();
                    //	document.body.style.cursor = 'auto';
                    //console.log('ERRORS: ' + textStatus);
                }
            });
        }
    }
}
function submitForm(dataObj){
    //console.log(dataObj);
    customAlert.error("Info","File imported successfully");
}
function onClickCloseDisconnect(){
    $("#navDisconnect").hide();
}
function buildCallChartGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Call Date",
        "field": "dateOfCall",
        "enableColumnMenu": false,
    });
    gridColumns.push({
        "title": "Begin Time",
        "field": "beginTime",
        "enableColumnMenu": false,
    });
    gridColumns.push({
        "title": "End Time",
        "field": "endTime",
        "enableColumnMenu": false,
    });
    gridColumns.push({
        "title": "Call Duration",
        "field": "callDuration",
        "enableColumnMenu": false,
    });
    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
}
function onClickViewPatientHistory(){
    setTimeout(function(){
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if(selectedItems && selectedItems.length>0){
            var obj = {};
            obj = selectedItems[0];
            onClickPatientCall(obj);
        }
    })
}
function getUserIdByName(inviteTo){
    for(var i=0;i<userList.length;i++){
        var userObj = userList[i];
        if(userObj && userObj.username == inviteTo){
            return userObj.id;
        }
    }
    return "";
}
function onClickCall(){
    $("#btnCall").addClass("btnSelection");
    setTimeout(function(){
        $("#btnCall").removeClass("btnSelection");
    },1000)
    if($.trim($("#txtInviteName").val()) != "" && sessionStorage.uName != $.trim($("#txtInviteName").val())){
        console.log(activeRoom);
        if(!activeRoom){
            if(!userCalling){
                userCalling = true;
                //initTwilio();
                var inviteTo = $("#txtInviteName").val();
                var userId = getUserIdByName(inviteTo);
                var dataObj = {};
                dataObj.senderId = sessionStorage.userId;
                dataObj.senderName = sessionStorage.uName;
                dataObj.receiverId = userId;//getUserIdByName();//"104";
                dataObj.receiverName = inviteTo;
                dataObj.room =  sessionStorage.userId;//userId;//"room1";
                dataObj.status =  "calling";//,data "status": "calling"
                console.log(dataObj);
                sendData(JSON.stringify(dataObj));
            }else{
                initTwilio();
            }

        }else{
            var inviteTo = $("#txtInviteName").val();
            var userId = getUserIdByName(inviteTo);
            var dataObj = {};
            dataObj.senderId = sessionStorage.userId;
            dataObj.senderName = sessionStorage.uName;
            dataObj.receiverId = userId;//getUserIdByName();//"104";
            dataObj.receiverName = inviteTo;
            if(activeRoom.name){
                dataObj.room =  activeRoom.name;//userId;//"room1";
            }else{
                dataObj.room =  sessionStorage.userId;
            }
            //userId;//"room1";
            dataObj.status =  "calling";//,data "status": "calling"
            console.log(dataObj);
            sendData(JSON.stringify(dataObj));
        }
    }


	/*if(inviteTo != ""){
	 if(videoCallVer == "1.0"){
	 if (activeConversation){// && activeConversation.participation && activeConversation.participation.size>0) {
	 // add a participant
	 activeConversation.invite(inviteTo);
	 } else {
	 // create a conversation
	 var options = {};
	 if (previewMedia) {
	 options.localMedia = previewMedia;
	 }
	 conversationsClient.inviteToConversation(inviteTo, options).then(
	 conversationStarted,
	 function (error) {
	 console.log('Unable to create conversation');
	 console.error('Unable to create conversation', error);
	 }
	 );
	 }
	 }else{
	 videoClient.connect({ to: inviteTo}).then(function(room){
	 console.log(room)
	 roomJoined(room);
	 },
	 function(error) {
	 log('Could not connect to Twilio: ' + error.message);
	 });
	 }
	 }*/
}
var activeRoom = null;

var divFilled = false;
function roomJoined(room) {
    divFilled = false;
    activeRoom = room;
    console.log(room);
    if(!isDuplicateParticipant(identity)){
        participantArray.push(identity);
    }

    log("Joined as '" + identity + "'");
//	  document.getElementById('button-join').style.display = 'inline';
    //  document.getElementById('button-leave').style.display = 'inline';

    // Draw local video, if not already previewing
    if (!previewMedia) {
        room.localParticipant.media.attach('#local-media');
    }

    room.participants.forEach(function(participant) {
        log("Already in Room: '" + participant.identity + "'");
        var ptArr = $('#patient-media').find("video");
        if (ptArr.length == 0 && divFilled == false){
            $("#lblPt").text(participant.identity);
            divFilled = true;
            participant.media.attach('#patient-media');
        }else{
            $("#lblNur").text(participant.identity);
            participant.media.attach('#remote-media');
        }
        //  $("#lblParticipant").text(participant.identity);
		/* if(participant.identity == "test1"){
		 $("#lblPt").text(participant.identity);
		 participant.media.attach('#patient-media');
		 }else if(participant.identity == "test2" || participant.identity == "test3"){
		 $("#lblNur").text(participant.identity);
		 participant.media.attach('#remote-media');
		 }*/
    });

    // When a participant joins, draw their video on screen

    room.on('participantConnected', function (participant) {
        log("Joining: '" + participant.identity + "'");
        // participant.media.attach('#remote-media');
        // if(participants.indexOf(identity) == -1){
        if(participant.identity != identity){
            // $("#lblParticipant").text(participant.identity);
            if(!isDuplicateParticipant(identity)){
                participantArray.push(identity);
            }

            //participant.media.attach('#remote-media');
			/*setTimeout(function(){
			 var lbl = "<label class='paticipantStyle'>"+participant.identity+"</label>";
			 $("#remote-media").append(lbl);
			 },1000)*/
			/* if(participant.identity == "test1"){
			 $("#lblPt").text(participant.identity);
			 participant.media.attach('#patient-media');
			 }else if(participant.identity == "test2" || participant.identity == "test3"){
			 $("#lblNur").text(participant.identity);
			 participant.media.attach('#remote-media');
			 }*/
            var ptArr = $('#patient-media').find("video");
            if (ptArr.length == 0){
                $("#lblPt").text(participant.identity);
                participant.media.attach('#patient-media');
            }else{
                $("#lblNur").text(participant.identity);
                participant.media.attach('#remote-media');
            }
        } else{
            //participant.media.detach();
        }
        //  }

    });

    // When a participant disconnects, note in log
    room.on('participantDisconnected', function (participant) {
        log("Participant '" + participant.identity + "' left the room");
        participant.media.detach();
        removeParticipant(participant.identity);
        var str1 = $("#lblPt").text();
        if(str1 == participant.identity){
            $("#lblPt").text("");
        }else{
            $("#lblNur").text("");
        }

		/*if(participant.identity == "test1"){
		 $("#lblPt").text("");
		 }else if(participant.identity == "test2" || participant.identity == "test3"){
		 $("#lblNur").text("");
		 }*/
		/*try{
		 var lblArray = $("#remote-media").find("label");
		 for(var i=0;i<lblArray.length;i++){
		 var lbl = $("#remote-media").find("label")[i];
		 if(lbl && $(lbl).text() == participant.identity){
		 $(lbl).remove();
		 }
		 }*/
        //var lblStr =  $("#remote-media").find("label").text();
        //console.log(lblStr);
        //}catch(ex){}

        //$("#remote-media").text("");
    });

    // When we are disconnected, stop capturing local video
    // Also remove media for all remote participants
    room.on('disconnected', function () {
        log('Left');
        room.localParticipant.media.detach();
        room.participants.forEach(function(participant) {
            log("Participant list '" + participant.identity + "' left the room");
            participant.media.detach();
            var str1 = $("#lblPt").text();
            if(str1 == participant.identity){
                $("#lblPt").text("");
            }else{
                $("#lblNur").text("");
            }
            removeParticipant(participant.identity);
            //var lblStr =  $("#remote-media").find("label").text();
            //console.log(lblStr);
            // }catch(ex){}
            //  $("#remote-media").text("");
        });
        activeRoom = null;
        // removeParticipant(sessionStorage.uName);
        //  document.getElementById('button-join').style.display = 'inline';
        //document.getElementById('button-leave').style.display = 'inline';
    });
}
function log(msg){
    console.log(msg);
}
function adjustHeight(){
    var defHeight = 330;
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 300;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    try{
        angularUIgridWrapper.adjustGridHeight(cmpHeight);
        angularPTUIgridWrapper.adjustGridHeight(cmpHeight+140);
    }catch(e){};
}

var twilioAccessToken = "";
function yourMethod(str){
    alert(str);
}
window.onmessage = function(e){
    if (e.data == 'hello') {
        alert('It works!');
    }
};
function targetFunction(strPtId){
    strPatientId = strPtId;
    patentComplePanelInfo();
}
function init(){
    $("#myStatus").kendoComboBox();
    getPatientStatus();
    //getCallPatientId();
    if(sessionStorage.uName == "nurse"){
        //$("#txtInviteName").val("doctor");
    }
    var dataOptions = {
        pagination: false,
        paginationPageSize: 500,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("dgridCallChart", dataOptions);
    angularUIgridWrapper.init();
    buildCallChartGrid([]);

    var dataOptionsPT = {
        pagination: false,
        paginationPageSize: 500,
        changeCallBack: onPTChange
    }

    angularPTUIgridWrapper = new AngularUIGridWrapper("dgridPatientCountList", dataOptionsPT);
    angularPTUIgridWrapper.init();
    buildPatientList([]);
    getPatientCountList();

    if(participantArray.indexOf(sessionStorage.uName) == -1){
        // getAjaxObject(ipAddress+"/video/token/"+sessionStorage.uName, "GET", onGetAccessCode, onErrorMedication);
    }

    if((sessionStorage.userTypeId == "500" || sessionStorage.userTypeId == "600") && sessionStorage.userId != 101 && sessionStorage.userId != 104 && sessionStorage.userId != 110){
        var patientListURL = ipAddress+"/patient/list/";
        getAjaxObject(patientListURL,"GET",onPatientListData,onErrorMedication);
    }else{
        if($("#divPatInfo",parent.document).is(":visible")){
            var strId = $("#lblptID",parent.document).html();
            strPatientId = strId;
            patentComplePanelInfo();
            console.log(strId);
            //$("#centerPanel").css("visibility","visible");
        }
    }
    getFacilityList();
    //getAjaxObject(ipAddress+"/user/list", "GET", onGetUserList, onErrorMedication);
    // getPatientCallChartHistory();
}
function getPatientCountList(){
    //patientSearch();
    onPatientSearch();
}
function onPatientListData1(dataObj){
    console.log(dataObj);
    var tempArray = [];
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.patient){
            if($.isArray(dataObj.response.patient)){
                tempArray = dataObj.response.patient;
            }else{
                tempArray.push(dataObj.response.patient);
            }
        }
    }else{
        customAlert.error("Error",dataObj.response.status.message.replace('patient','service user'));
    }

    for(var i=0;i<tempArray.length;i++){
        var obj = tempArray[i];
        if(obj){
            var dataItemObj = {};
            dataItemObj.PID = obj.id;
            dataItemObj.FN = obj.firstName;
            dataItemObj.LN = obj.lastName;
            dataItemObj.WD = obj.weight;
            dataItemObj.HD = obj.height;
            if(obj.dnr){
                dataItemObj.dnr = obj.dnr;
            }else{
                dataItemObj.dnr = 0;
            }

            if(obj.middleName){
                dataItemObj.MN = obj.middleName;
            }else{
                dataItemObj.MN =  "";
            }
            dataItemObj.GR = obj.gender;
            if(obj.status){
                dataItemObj.ST = obj.status;
            }else{
                dataItemObj.ST = "ACTIVE";
            }
            dataItemObj.DOB = kendo.toString(new Date(obj.dateOfBirth),"MM/dd/yyyy");
            if(obj.photoExt){
                dataItemObj.photo = ipAddress+"/download/patient/photo/"+obj.id+"/?"+Math.round(Math.random()*1000000)+"&access_token="+sessionStorage.access_token+"&tenant="+sessionStorage.tenant;
            }else if(obj.gender == "Male"){
                dataItemObj.photo = "../../img/AppImg/HosImages/male_profile.png";
            }else if(obj.gender == "Female"){
                dataItemObj.photo = "../../img/AppImg/HosImages/profile_female.png";
            }else{
                dataItemObj.photo = "../../img/AppImg/HosImages/blank.png";
            }
            if (obj.dateOfBirth) {
                var dt = new Date(obj.dateOfBirth);
                dataItemObj.Age = getAge(dt);
            }
            else{
                dataItemObj.Age = "";
            }
             var facilitydata = _.where(facilitydataArray, {idk: obj.facilityId})
            if(facilitydata && facilitydata.length > 0 && facilitydata[0].abbreviation) {
                dataItemObj.abbr = facilitydata[0].abbreviation;
            }
            else{
                dataItemObj.abbr = "";
            }
            var gen = obj.gender;
            dataItemObj.gender = gen.charAt(0);
            dataArray.push(dataItemObj);
        }
    }
    buildPatientList(dataArray);
}

function onGetCreatePatientInfo(dataObj){
    onPatientListData1(dataObj);
    patentComplePanelInfo();


}

function buildPatientList(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    otoptions.rowHeight = 100;
    if(dataSource.length > 0) {
        gridColumns.push({
            "title": "Image",
            "field": "PID",
            "enableColumnMenu": false,
            "width": "20%",
            "cellTemplate": showPtImage()
        });
        gridColumns.push({
            "title": "Patient Details",
            "field": "FN",
            "enableColumnMenu": false,
            "width": "80%",
            "cellTemplate": showPtDetails()
        });
    }
    else{
        gridColumns.push({
            "title": "Image",
            "field": "PID",
            "enableColumnMenu": false,
            "width": "20%"
        });
        gridColumns.push({
            "title": "Patient Details",
            "field": "FN",
            "enableColumnMenu": false,
            "width": "80%",
        });
    }
    gridColumns.push({
        "title": "Image",
        "field": "PID1",
        "enableColumnMenu": false,
        "width": "0%"
        // "cellTemplate":showVideoImage()
    });
    angularPTUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
}
function onPTChange(){
    setTimeout(function(){
        var selectedItems = angularPTUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if(selectedItems && selectedItems.length>0){
            strPatientId = selectedItems[0].PID;
            patentComplePanelInfo();
            // getAjaxObject(ipAddress+"/patient/"+strPatientId,"GET",onGetPatientInfo,onErrorMedication);
        }else{
        }

    });
}
function showPtDetails(){
    var node = '<div style="padding-top:5px"><spn class="pt">{{row.entity.PID}}</spn><spn class="pt"> - {{row.entity.LN}} {{row.entity.FN}} {{row.entity.MN}} </spn><br>';
    node = node+'<span class="avlClass">{{row.entity.gender}} , {{row.entity.Age}} - {{row.entity.abbr}}</span></div>';
    //node = node+'<spn class="pt">Gender: {{row.entity.GR}}</spn><br><spn class="pt">Date Of Birth: {{row.entity.DOB}}</spn></div>';
    return node;
}
function showPtImage(){
    var node;

    // node = '<div *ngIf="{{row.entity.dnr}} > 0; then thenBlock; else elseBlock"> </div> <ng-template #thenBlock>'
    //node = node + '<div ><img src="{{row.entity.photo}}"  onerror="onImgError(event)" style="width:90%;height:100%;border: 1px solid red;" gender="{{row.entity.GR}}"><spn></div>'
    //node = node + '</ng-template> <ng-template #elseBlock>'
        node = '<div ng-show="((row.entity.dnr == \'1\'))"><img src="{{row.entity.photo}}"  onerror="onImgError(event)" style="width:90%;height:100%;border: 1px solid red;" gender="{{row.entity.GR}}"></img></div>';
        node = node + '<div ng-show="((row.entity.dnr == \'0\'))"><img src="{{row.entity.photo}}"  onerror="onImgError(event)" style="width:90%;height:100%;border: 1px solid;" gender="{{row.entity.GR}}"></img></div>';
    //node = node + '</ng-template>';



    // if(row.entity.dnr == 1){
    // node = '<div><img src="{{row.entity.photo}}"  onerror="onImgError(event)" datacity={{row.entity.dnr}} gender="{{row.entity.GR}}"><spn>';//this.src=\'../../img/AppImg/HosImages/patient1.png\'
    // node = '<div><img src="{{row.entity.photo}}"  onerror="onImgError(event)" *ngIf='{{row.entity.dnr}} = 0 ; then style=''width:90%;height:100%;border: 1px solid green;''' gender="{{row.entity.GR}}"><spn>';//this.src=\'../../img/AppImg/HosImages/patient1.png\'
    // node = node+"</div>";
    // }
    // else{
    //    node = '<div><img src="{{row.entity.photo}}" onerror="onImgError(event)" style="width:90%;height:100%;border: 1px solid green;" gender="{{row.entity.GR}}"><spn>';//this.src=\'../../img/AppImg/HosImages/patient1.png\'
    //    node = node+"</div>";
    // }
    return node;
}
function showVideoImage(){
    var node = '<div style="padding-top:5px;text-align:right;padding-top:10px;padding-right:10px"><img src="../../img/login/videocall-color.png" style="width:40%;height:100%;cursor:pointer"><spn>';
    node = node+"</div>";
    return node;
}
function onImgError(e){
    //console.log(e);
}
function onPatientListData(dataObj){
    console.log(dataObj);
    if(dataObj.response && dataObj.response.patient && dataObj.response.patient[0] && dataObj.response.patient[0].id)
        strPatientId = dataObj.response.patient[0].id;
    $("#lblDB",parent.document).hide();
    $("#lblPatient",parent.document).hide();
    $("#lblMasters",parent.document).hide();
    $("#lblSetUp",parent.document).hide();
    $("#pnlPatient",parent.document).css("top","50px");


    $("#addPt").hide();
    $("#searchPt").hide();
    $("#openCallChart").hide();

    $("#lblPatientEdit").hide();
    $("#lblView").hide();
    $("#lblImport").hide();
    $("#lblCreate").hide();
    $("#lblAddAppointment").hide();
    $("#lblPatientIllness").hide();
    $("#lblPatientDiet").hide();
    $("#lblPatientExcersz").hide();
    $("#lblPatientEdit").hide();
    $("#lblPatientEdit").hide();
    $("#lblRefresh").hide();

    var myIframe = parent.frames["videoFrame"];
    if(myIframe && myIframe.contentWindow){
        myIframe.contentWindow.hideNavigationBar();
    }
    patentComplePanelInfo();
}
var userList = [];
function onGetUserList(dataObj){
    console.log(dataObj);
    userList = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if($.isArray(dataObj.response.user)){
            userList = dataObj.response.user;
        }else{
            userList.push(dataObj.response.user);
        }
    }
}
function onClickShowPatientCallHistory(){
    console.log("call history"+strPatientId);
    parentRef.patientId = strPatientId;
    var popW = "60%";
    var popH = "60%";
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Service User Call History";
    devModelWindowWrapper.openPageWindow("../../html/patients/patientCallHistoryList.html", profileLbl, popW, popH, true, closePatientCallHistoryScreen);
}
function closePatientCallHistoryScreen(evt,returnData){

}

function onClickShowPatientDetails(){
    onClickPatientInfo();
}
function onClickShowCarePlan(){
    if(strPatientId){
        var popW = "90%";
        var popH = "80%";
        var profileLbl;
        var devModelWindowWrapper = new kendoWindowWrapper();
        profileLbl = "Service User Care Plan";
        parentRef.pid = strPatientId;
        parentRef.pname = strPName;

        parentRef.fid = facilityId;
        parentRef.fname = "";
        //  showPatientRoster();
        devModelWindowWrapper.openPageWindow("../../html/patients/createPlanRoster.html", profileLbl, popW, popH, false, closeRosterPlan);
    }
}
function closeRosterPlan(evt,returnData){

}

function onClickShowActivityReportListOld(){
    if(strPatientId){
        var popW = "90%";
        var popH = "80%";
        var profileLbl;
        var devModelWindowWrapper = new kendoWindowWrapper();
        profileLbl = "Tasks";
        parentRef.patientId = strPatientId;
        parentRef.pname = strPName;

        parentRef.fid = facilityId;
        parentRef.fname = "";
        //  showPatientRoster();
        devModelWindowWrapper.openPageWindow("../../html/patients/patientActivityReportList-old.html", profileLbl, popW, popH, false, closeActivityReportListOld);
    }
}
function closeActivityReportListOld(evt,returnData){

}

function onClickShowActivityReportList(){
    if(strPatientId){
        var popW = "82%";
        var popH = "78%";

        var profileLbl;
        var devModelWindowWrapper = new kendoWindowWrapper();
        profileLbl = "Service User Appointment Tasks";
        parentRef.patientId = strPatientId;
        parentRef.pname = strPName;

        parentRef.fid = facilityId;
        parentRef.fname = "";
        //  showPatientRoster();
        devModelWindowWrapper.openPageWindow("../../html/patients/patientActivityReportList.html", profileLbl, popW, popH, true, closeActivityReportListOld);
    }
}
function closeActivityReportListNew(evt,returnData){

}

function onClickShowPatientMedications(){
    parentRef.patientId = strPatientId;
    var popW = "85%";
    var popH = "85%";
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Service User Medications";
    devModelWindowWrapper.openPageWindow("../../html/masters/prescription.html", profileLbl, popW, popH, false, closePatientCallHistoryScreen);
    // devModelWindowWrapper.openPageWindow("../../html/patients/patientMedicationList.html", profileLbl, popW, popH, true, closePatientCallHistoryScreen);
}
function onClickShowPatientDiet(){
    parentRef.patientId = strPatientId;
    var popW = "50%";
    var popH = "70%";
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Service User Diet";
    devModelWindowWrapper.openPageWindow("../../html/patients/patientDietList.html", profileLbl, popW, popH, true, closePatientCallHistoryScreen);
}
function onClickShowPatientIllness(){
    parentRef.patientId = strPatientId;
    var popW = "56%";
    var popH = "75%";
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Service User Diagnosis";
    devModelWindowWrapper.openPageWindow("../../html/patients/patientIllnessList.html", profileLbl, popW, popH, true, closePatientCallHistoryScreen);
}

function onClickShowPatientAllergy(){
    parentRef.patientId = strPatientId;
    var popW = "65%";
    var popH = "75%";
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Service User Allergy";
    devModelWindowWrapper.openPageWindow("../../html/patients/patientAllergyList.html", profileLbl, popW, popH, true, closePatientCallHistoryScreen);
}

function onClickShowPatientBilling(){
    parentRef.patientId = strPatientId;
    var popW = "70%";
    var popH = "80%";
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Service User Billing";
    devModelWindowWrapper.openPageWindow("../../html/patients/patientBillingList.html", profileLbl, popW, popH, true, closePatientCallHistoryScreen);

}

function onClickShowPatientDocuments(){
    parentRef.patientId = strPatientId;
    var popW = "56%";
    var popH = "65%";
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Service User images/Documents";
    devModelWindowWrapper.openPageWindow("../../html/patients/patientDocumentList.html", profileLbl, popW, popH, true, closePatientCallHistoryScreen);
}

function onClickShowPatientPreventiveCare(){
    parentRef.patientId = strPatientId;
    var popW = "60%";
    var popH = "60%";
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Preventitive Care";
    devModelWindowWrapper.openPageWindow("../../html/patients/patientPreventiveCareList.html", profileLbl, popW, popH, true, closePatientCallHistoryScreen);
}
function onClickShowPatientVisits(){
    parentRef.patientId = strPatientId;
    var popW = "70%";
    var popH = "80%";
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Service User Appointments";
    devModelWindowWrapper.openPageWindow("../../html/patients/providerAppointmentList.html", profileLbl, popW, popH, true, closePatientCallHistoryScreen);
}
function onClickShowPatientHolterReports(){
    parentRef.patientId = strPatientId;
    var popW = "60%";
    var popH = "60%";
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Service User Holter Reports";
    devModelWindowWrapper.openPageWindow("../../html/patients/patientHolterReportList.html", profileLbl, popW, popH, true, closePatientCallHistoryScreen);
}

function onClickShowPatientNotes(){
    parentRef.patientId = strPatientId;
    var popW = "55%";
    var popH = "65%";
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Service User Notes";
    devModelWindowWrapper.openPageWindow("../../html/patients/patientNotes.html", profileLbl, popW, popH, true, closePatientCallHistoryScreen);
}


function onClickShowPatientVitals(){
    parentRef.patientId = strPatientId;
    var popW = "60%";
    var popH = "60%";
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Service User Vitals";
    devModelWindowWrapper.openPageWindow("../../html/patients/patientVitalsList.html", profileLbl, popW, popH, true, closePatientCallHistoryScreen);
}
function onClickShowPatientActivities(){

    // if (sessionStorage.IsActivity == 2) {
    //     var msg = "Application is set to Appointment Reasons Tasks. Do you want to open from here?";
    //     displaySessionErrorPopUp("Info", msg, function(res) {
    //         $("#ptframe").attr("src", "../../html/masters/attachTask.html");
    //     });
    // }
    // else {
        // onClickShowPatientActivities();
        parentRef.patientId = strPatientId;
        var popW = "70%";
        var popH = "68%";
        var profileLbl;
        var devModelWindowWrapper = new kendoWindowWrapper();
        profileLbl = "Service User Appointment Tasks";
        devModelWindowWrapper.openPageWindow("../../html/patients/patientActivityReportList.html", profileLbl, popW, popH, true, closePatientCallHistoryScreen);
    // }

}
function getPatientCallChartHistory(){
    getAjaxObject(ipAddress+"/patient/calldata/"+strPatientId,"GET",onGetCallData,onErrorMedication);
}
function getHolerReports(){
    getAjaxObject(ipAddress+"/patient/reports/holter/"+strPatientId,"GET",onGetHolterReportsData,onErrorMedication);
}
var holterReportDataArray = [];
function onGetHolterReportsData(dataObj){
    $("#divHolterReports").text("");
    var dataArray = [];
    if(dataObj){
        if($.isArray(dataObj)){
            dataArray = dataObj;
        }else{
            dataArray.push(dataObj);
        }
    }
    holterReportDataArray = dataArray;
    var strTable = '<table class="table">';
    strTable = strTable+'<thead class="fillsHeader whiteColor">';
    strTable = strTable+'<tr>';
    //strTable = strTable+'<th>Patient ID</th>';
    strTable = strTable+'<th class="textAlign whiteColor" style="width:220px">Date Generated</th>';
    strTable = strTable+'<th class="textAlign whiteColor">File Name</th>';
    strTable = strTable+'<th class="textAlign whiteColor">File Type</th>';
    strTable = strTable+'</tr>';
    strTable = strTable+'</thead>';
    strTable = strTable+'<tbody>';
    for(var i=0;i<holterReportDataArray.length;i++){
        var dataItem = holterReportDataArray[i];
        if(dataItem){
            //console.log(dataItem);
            var className = getRowColors(i);
            strTable = strTable+'<tr class="'+className+'">';
            //strTable = strTable+'<td>'+dataItem.patientId+'</td>';
            strTable = strTable+'<td class="textAlign">'+kendo.toString(new Date(dataItem.createdDate),"MM-dd-yyyy h:mm:ss")+'</td>';
            strTable = strTable+'<td class="textAlign">'+dataItem.fileName+'</td>';
            var dietId = dataItem.id;
            if(dataItem.fileType == "mp4"){
                strTable = strTable+'<td style="width:200px;padding:0px" class="textAlign"><img id="'+dietId+'" src="../../img/AppImg/HosImages/video.png"  class="cusrsorStyle videoIcon" onClick="onClickGenerate(event)"></td>';
            }else if(dataItem.fileType == "pdf"){
                strTable = strTable+'<td style="width:200px;padding:0px" class="textAlign"><img  id="'+dietId+'" src="../../img/AppImg/HosImages/pdf.png"    class="cusrsorStyle pdfIcon" onClick="onClickHolterReport(event)"></td>';
            }
            strTable = strTable+'</tr>';
        }
    }
    strTable = strTable+'</tbody>';
    strTable = strTable+'</table>';
    $("#divHolterReports").append(strTable);
}
function onChange(){

}
function onGetCallData(dataObj){
    var dataArray = [];
    if(dataObj){
        if($.isArray(dataObj)){
            dataArray = dataObj;
        }else{
            dataArray.push(dataObj);
        }
    }
    var tempArray = [];
    for(var i=0;i<dataArray.length;i++){
        var dataItem = dataArray[i];
        if(dataItem){
            dataItem.dateOfCall = kendo.toString(new Date(dataItem.dateOfCall),"MM-dd-yyyy");
            tempArray.push(dataItem);
        }
    }
    buildCallChartGrid(dataArray);
}
var identity = "";
function onGetAccessCode(dataObj){
//	console.log(dataObj);
    twilioAccessToken = dataObj.token;
    identity = dataObj.identity;
    parentRef.accessToken = twilioAccessToken;
    //showLocalPreview();
    //initTwilio();
//	webSocketConnect();
}
function webSocketConnect(){
    wsURL = wsURL+sessionStorage.userId;
    ws = new WebSocket(wsURL);
    ws.onmessage = function(data) {
        getUserData(data.data);
    }
}
var receiverName = "";
var receiverId = "";
var senderId = "";
var senderName = "";
var roomName = "";
function getUserData(data){
    console.log(data);
    var obj = JSON.parse(data);
    if(obj){
        receiverName = obj.receiverName;
        receiverId = obj.receiverId;
        roomName = obj.room;
        senderId = obj.senderId;
        senderName = obj.senderName;
        if(obj.status == "calling" || obj.status == "call"){
            if(obj.status == "calling"){
                document.getElementById('audioCall').play();
                setButtonBlink();
                $("#divInCall").show();
                $("#lblName").text( senderName);
            }else{
                if(participantArray.indexOf(receiverName) == -1){
                    getAjaxObject(ipAddress+"/video/token/"+receiverName, "GET", onGetSenderAccessCode, onErrorMedication);
                }else{
                    if(activeRoom){
                        activeRoom.disconnect();
                        activeRoom = null;
                        removeParticipant(sessionStorage.uName);
                        getAjaxObject(ipAddress+"/video/token/"+receiverName, "GET", onGetReceiverAccessCode, onErrorMedication);
                    }
                }
            }
        }
    }
}
var clientInvite = null;
function onClickAccept(){
    if(videoCallVer == "1.0"){
        if(clientInvite){
            $("#navBar").hide();
            $("#divInCall").hide();
            $("#navDisconnect").show();
            document.getElementById('audioCall').pause();
            if(timer){
                clearInterval(timer);
            }
            timer = null
            clientInvite.accept().then(conversationStarted);
        }
    }else{
        $("#navBar").hide();
        $("#divInCall").hide();
        document.getElementById('audioCall').pause();
        // room.participants
        if(participantArray.indexOf(receiverName) == -1){
            getAjaxObject(ipAddress+"/video/token/"+receiverName, "GET", onGetReceiverAccessCode, onErrorMedication);
        }
        var dataObj = {};
        dataObj.senderId = receiverId;
        dataObj.senderName = receiverName;
        dataObj.receiverId = senderId;//getUserIdByName();//"104";
        dataObj.receiverName = senderName;
        if(activeRoom && activeRoom.participants &&  activeRoom.participants.size>0){
            dataObj.room =  activeRoom.name;
        }else{
            dataObj.room =  roomName;
        }
        dataObj.status =  "call";//,data "status": "calling"
        console.log(dataObj);
        sendData(JSON.stringify(dataObj));

		/* if(activeRoom && activeRoom.participants){
		 if(activeRoom.participants.size>1){
		 var dataObj = {};
		 dataObj.senderId = receiverId;
		 dataObj.senderName = receiverName;
		 dataObj.receiverId = senderId;//getUserIdByName();//"104";
		 dataObj.receiverName = senderName;
		 if(activeRoom){
		 dataObj.room =  activeRoom.name;
		 }else{
		 dataObj.room =  roomName;
		 }
		 dataObj.status =  "call";//,data "status": "calling"
		 console.log(dataObj);
		 sendData(JSON.stringify(dataObj));
		 }else{
		 if (activeRoom) {
		 removeParticipant(sessionStorage.uName);
		 activeRoom.disconnect();
		 activeRoom = null;
		 getAjaxObject(ipAddress+"/video/token/"+receiverName, "GET", onGetReceiverAccessCode, onErrorMedication);
		 }
		 }
		 }*/
        //	getAjaxObject(ipAddress+"/video/token/"+senderName, "GET", onGetSenderAccessCode, onErrorMedication);
    }
}
function onClickDecline(){
    $("#navBar").hide();
    $("#divInCall").hide();
    $("#navDisconnect").hide();
    if(clientInvite){
        clientInvite.reject();
    }
    document.getElementById('audioCall').stop();
    if(timer){
        clearInterval(timer);
    }
    timer = null;
}

function onGetReceiverAccessCode(dataObj){
    var token = dataObj.token;
    identity = dataObj.identity;
    videoClient = new Twilio.Video.Client(token);
    videoClient.connect({ to: roomName}).then(roomJoined,
        function(error) {
            log('Could not connect to Twilio: ' + error.message);
        });
}
function onGetSenderAccessCode(dataObj){
    var token = dataObj.token;
    identity = dataObj.identity;
    var client1 = new Twilio.Video.Client(token);
    client1.connect({ to: roomName}).then(roomJoined,
        function(error) {
            log('Could not connect to Twilio: ' + error.message);
        });
}
function sendData(data) {
    //var data = JSON.stringify($("#name").val())
    //var data = $("#name").val();
    ws.send(data);
}
function initTwilio(){
    if(videoCallVer == "1.0"){
        var accessManager = new Twilio.AccessManager(twilioAccessToken);
        // create a Conversations Client and connect to Twilio
        conversationsClient = new Twilio.Conversations.Client(accessManager);
        conversationsClient.listen().then(
            clientConnected,
            function (error) {
                console.log('Could not connect to Twilio: ' + error.message);
            }
        );
    }else{
        //if(sessionStorage.uName == "nurse1"){
        inviteTo =   sessionStorage.userId;//"room1";
        videoClient = new Twilio.Video.Client(twilioAccessToken);
        videoClient.connect({ to: inviteTo}).then(roomJoined,
            function(error) {
                log('Could not connect to Twilio: ' + error.message);
            });
        //}
    }
}

function showLocalPreview(){
    if (!previewMedia) {
        if(videoCallVer == "1.0"){
            previewMedia = new Twilio.Conversations.LocalMedia();
            Twilio.Conversations.getUserMedia().then(
                function (mediaStream) {
                    previewMedia.addStream(mediaStream);
                    previewMedia.attach('#local-media');
                },
                function (error) {
                    console.error('Unable to access local media', error);
                    console.log('Unable to access Camera and Microphone');
                }
            );
        }else{
            previewMedia = new Twilio.Video.LocalMedia();
            Twilio.Video.getUserMedia().then(
                function (mediaStream) {
                    previewMedia.addStream(mediaStream);
                    previewMedia.attach('#local-media');
                    // previewMedia.attach('#patient-media');
                    // previewMedia.attach('#remote-media');
                    $("#lblLogin").text(sessionStorage.uName);
                },
                function (error) {
                    console.error('Unable to access local media', error);
                    log('Unable to access Camera and Microphone');
                });
        }
    }
}
var timer = null;
var count = 1;
function setButtonBlink(){
    timer = setInterval(function(){
        if(count%2 == 0){
            $("#navBar").addClass("buttonBorder");
            $("#btnInviteImage").show();
        }else{
            $("#navBar").removeClass("buttonBorder");
            $("#btnInviteImage").show();
        }
        count++;
    }, 1000);
}
function clientConnected() {
    Loader.hideLoader();
    //$("#btnButton").show();
    //setButtonBlink();

    if(parentRef.invFlag){
        console.log("Connected to Twilio. Listening for incoming Invites as '" + conversationsClient.identity + "'");
        conversationsClient.on('invite', function (invite) {
            $("#navBar").show();
            document.getElementById('audioCall').play();
            setButtonBlink();
            $("#divInCall").show();
            $("#lblName").text( invite.from);
            // $("#audioCall").play();
            clientInvite = invite;
            if(parentRef.invFlag){
                parentRef.from =  invite.from;//sessionStorage.uName;
                console.log('Incoming invite from: ' + invite.from);
                //onClickPatientCall(null);
				/*customAlert.confirm("Confirm", invite.from+" calling. Do you want accept?",function(response){
				 console.log(response);
				 var inviteFrom = invite.from;
				 if(response.button == "Yes"){
				 invite.accept().then(conversationStarted);
				 }
				 });*/
            }
        });
    }

	/*conversation.on('participantConnected', function (participant) {
	 console.log("Participant '" + participant.identity + "' connected");
	 });*/
};
function onclikPatientCallToNurse(){

}
var aConversation = null;
function conversationStarted(conversation) {
    console.log('In an active Conversation');
    activeConversation = conversation;
    //aConversation = conversation;
    // activeConversation = conversation;
    //  parentRef.activeConversion = conversation;
    //parentRef.conversationsClient = conversationsClient;
    //parentRef.screenType = "Patient";
    //onClickPatientCall(null);
    conversation.on('participantConnected', function (participant) {
        console.log("Participant '" + participant.identity + "' connected");
        //parentRef.participant = participant;
        //onClickPatientCall(null);
        $("#navDisconnect").show();
        participant.media.attach('#remote-media');
    });
    // when a participant disconnects, note in log
    conversation.on('participantFailed', function (participant) {
        console.log("Participant failed to connect: '" + participant.identity );
    });
    conversation.on('participantDisconnected', function (participant) {
        // $("#navDisconnect").hide();
        console.log("Participant '" + participant.identity + "' disconnected");
        console.log(conversationsClient);
        // if(sessionStorage.uName != "text3"){
        // activeConversation = null;
        //}
    });
    conversation.on('ended', function (conversation) {
        $("#navDisconnect").hide();
        console.log("Connected to Twilio. Listening for incoming Invites as '" + conversationsClient.identity + "'");
        conversation.localMedia.stop();
        conversation.disconnect();
        activeConversation = null;
    });
    conversation.on('disconnected', function(conversation) {
		/* Release the local camera and microphone if we're no longer using this
		 LocalMedia instance in another Conversation */
        //conversation.localMedia.stop();
    });
    //  onClickPatientCall(null);
    // draw local video, if not already previewing
	/* if (!previewMedia) {
	 conversation.localMedia.attach('#local-media');
	 }
	 // when a participant joins, draw their video on screen
	 conversation.on('participantConnected', function (participant) {
	 console.log("Participant '" + participant.identity + "' connected");
	 participant.media.attach('#remote-media');
	 });
	 // when a participant disconnects, note in log
	 conversation.on('participantDisconnected', function (participant) {
	 console.log("Participant '" + participant.identity + "' disconnected");
	 });
	 // when the conversation ends, stop capturing local video
	 conversation.on('ended', function (conversation) {
	 console.log("Connected to Twilio. Listening for incoming Invites as '" + conversationsClient.identity + "'");
	 conversation.localMedia.stop();
	 conversation.disconnect();
	 activeConversation = null;
	 });*/
};
function onClickDisconnect(){
    $("#btnDisconnect").addClass("btnSelection");
    setTimeout(function(){
        $("#btnDisconnect").removeClass("btnSelection");
    },1000)
    if(videoCallVer == "1.0"){
        if(activeConversation){
            //activeConversation.localMedia.stop();
            activeConversation.disconnect();
            $("#navDisconnect").hide();
            activeConversation = null;
            //aConversation = null;
            setTimeout(function(){
                //initTwilio();
            },1000)
        }
    }else{
        if (activeRoom) {
            userCalling = false;
            removeParticipant(sessionStorage.uName);
            activeRoom.disconnect();
        }
    }
}

function removeParticipant(nm){
    for(var i = participantArray.length - 1; i >= 0; i--) {
        if(participantArray[i] === nm) {
            participantArray.splice(i, 1);
        }
    }
}
function isDuplicateParticipant(nm){
    for(var i = participantArray.length - 1; i >= 0; i--) {
        if(participantArray[i] === nm) {
            return true;
        }
    }
    return false;
}
var imgImage = null;
function onClickGenerate(event){
    console.log(event);
    var dietVideoId = "";
    if(imgImage){
        $(imgImage).removeClass("imgBorder");
    }

    if(event.currentTarget){
        dietVideoId = event.currentTarget.id;
        imgImage = event.currentTarget;
        $(imgImage).addClass("imgBorder");
    }
    var fileType = findFileType(dietDataArray,dietVideoId);
    showPdfVideo(dietVideoId,fileType,"diet");
}
function onClickDietYoutube(event){
    var dietVideoId = "";
    var urlPath = "";
    if(imgImage){
        $(imgImage).removeClass("imgBorder");
    }

    if(event.currentTarget){
        dietVideoId = event.currentTarget.id;
        imgImage = event.currentTarget;
        $(imgImage).addClass("imgBorder");
    }
    for(var i=0;i<dietDataArray.length;i++){
        var item = dietDataArray[i];
        if(item && item.id == event.currentTarget.id){
            urlPath = item.youtubeLink;
            iItem = item;
            break;
        }
    }
    if(urlPath && urlPath.indexOf("www.youtube.com")>=0){
        playYouTubeFile(urlPath);
    }
}
function onClickYouTubeExcersize(event){
    var dietVideoId = "";
    var urlPath = "";
    if(imgImage){
        $(imgImage).removeClass("imgBorder");
    }

    if(event.currentTarget){
        dietVideoId = event.currentTarget.id;
        imgImage = event.currentTarget;
        $(imgImage).addClass("imgBorder");
    }
    for(var i=0;i<excerDataArr.length;i++){
        var item = excerDataArr[i];
        if(item && item.id == event.currentTarget.id){
            urlPath = item.youtubeLink;
            iItem = item;
            break;
        }
    }
    if(urlPath && urlPath.indexOf("www.youtube.com")>=0){
        playYouTubeFile(urlPath);
    }
}
function onClickExcersize(event){
    var dietVideoId = "";
    if(imgImage){
        $(imgImage).removeClass("imgBorder");
    }

    if(event.currentTarget){
        dietVideoId = event.currentTarget.id;
        imgImage = event.currentTarget;
        $(imgImage).addClass("imgBorder");
    }
    var fileType = findFileType(excerDataArr,dietVideoId);
    showPdfVideo(dietVideoId,fileType,"excersize");
}
function onClickMyAudio(event){
    var audioId = "";
    if(imgImage){
        $(imgImage).removeClass("textBorder");
    }

    if(event.currentTarget){
        audioId = event.currentTarget.id;
        imgImage = event.currentTarget;
        $(imgImage).addClass("textBorder");
    }
    console.log(audioId);
    if(audioId && audioId != "null"){
        playAudioFile(audioId);
    }
}

function playAudioFile(aid){
    parentRef.audioId = aid;
    var popW = 600;
    var popH = 200;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Audio";
    devModelWindowWrapper.openPageWindow("../../html/patients/showAudio.html", profileLbl, popW, popH, true, closeVideoScreen);
}
function playPDFFile(pdfId){
    var popW = 1100;
    var popH = 580;
    var profileLbl;
    parentRef.dietId = pdfId;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Document";
    devModelWindowWrapper.openPageWindow("../../html/patients/showPdf.html", profileLbl, popW, popH, true, closeVideoScreen);
}
function onClickHolterReport(event){
    var dietVideoId = "";
    parentRef.sType = "holter";
    if(imgImage){
        $(imgImage).removeClass("textBorder");
    }
    if(event.currentTarget){
        dietVideoId = event.currentTarget.id;
        imgImage = event.currentTarget;
        $(imgImage).addClass("textBorder");
    }

    playPDFFile(dietVideoId);
    //var urlPath = ipAddress+"/patient/report/download/"+dietVideoId;
    //window.open(urlPath, "popupWindow", "width=1000,height=600,scrollbars=yes");
}
function showPdfVideo(fileId,fileType,sType){
    parentRef.dietId = fileId;
    parentRef.sType = sType;
    var urlPath = "";
    if(sType == "diet"){
        urlPath = ipAddress+"/patient/diet/download/"
    }else{
        urlPath = ipAddress+"/patient/exercise/download/"
    }
    var popW =900;
    var popH = 580;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    if(fileType && fileType.toLowerCase() == "pdf"){
        profileLbl = "Document";
        popW = 1100;
        devModelWindowWrapper.openPageWindow("../../html/patients/showPdf.html", profileLbl, popW, popH, true, closeVideoScreen);
        //var reqUrl = urlPath+fileId;
        //window.open(reqUrl, "popupWindow", "width=1000,height=600,scrollbars=yes");
        //$(imgImage).removeClass("imgBorder");
        //$(imgImage).removeClass("textBorder");
    }else{
        profileLbl = "Video";
        devModelWindowWrapper.openPageWindow("../../html/patients/showVideo.html", profileLbl, popW, popH, true, closeVideoScreen);
    }
}
function onClickIllnessVideo(event){
    var urlPath = "";
    var urlText = "";
    var iItem = null;
    if(event.currentTarget){
        for(var i=0;i<illnessArr.length;i++){
            var item = illnessArr[i];
            if(item && item.fileName == event.currentTarget.text){
                urlPath = item.youtubeLink;
                iItem = item;
                break;
            }
        }
        //urlPath = event.currentTarget.href;
    }
    if(urlPath && urlPath.indexOf("www.youtube.com")>=0){
        playYouTubeFile(urlPath);
    }else{
        var iFileType = "";
        iFileType = iItem.fileType;
        iFileType = iFileType.toLowerCase();
        if(iFileType == "mp3"){
            playAudioFile(iItem.id);
        }else{
            showPdfVideo(iItem.id, iFileType, "Diet");
        }
    }
}
function playYouTubeFile(urlPath){
    var popW =900;
    var popH = 580;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Video";
    parentRef.sType = "illness";
    parentRef.illUrlPath = urlPath;
    devModelWindowWrapper.openPageWindow("../../html/patients/showVideo.html", profileLbl, popW, popH, true, closeVideoScreen);
}
function findFileType(arr,fileId){
    var type = "";
    for(var i=0;i<arr.length;i++){
        var dataItem = arr[i];
        if(dataItem && dataItem.id == fileId){
            type =  dataItem.fileType;
            break;
        }
    }
    return type;
}
function closeVideoScreen(evt,returnData){
    //$(imgImage).removeClass("imgBorder");
    //$(imgImage).removeClass("textBorder");
}
function getPatientInfomation(){
    getAjaxObject(ipAddress+"/patient/"+strPatientId,"GET",onGetPatientInfo,onErrorMedication);
}
function getMedicationData(){
    getAjaxObject(ipAddress+"/patient/medications/"+strPatientId,"GET",onGetMedication,onErrorMedication);
}
function getAppointment(){
    getAjaxObject(ipAddress+"/patient/appointments/"+strPatientId,"GET",onGetAppointSuccess,onErrorMedication);
}
function getVitals(){
    getAjaxObject(ipAddress+"/patient/vitals/"+strPatientId,"GET",onGetVitalsSuccess,onErrorMedication);
}
function getExcersize(){
    getAjaxObject(ipAddress+"/patient/exercise/"+strPatientId,"GET",onGetExcersizeSuccess,onErrorMedication);
}
function getDiet(){
    getAjaxObject(ipAddress+"/patient/diet/"+strPatientId,"GET",onGetDietSuccess,onErrorMedication);
}
function getIllness(){
    getAjaxObject(ipAddress+"/patient/illness/"+strPatientId,"GET",onGetIllnessSuccess,onErrorMedication);
}
function getInterests(){
    getAjaxObject(ipAddress+"/patient/preventative-services/"+strPatientId,"GET",onGetInterestsSuccess,onErrorMedication);
}

function getPatientActivies(){
    getAjaxObject(ipAddress+"/patient/activity/list/"+strPatientId,"GET",onGetPatientActivities,onErrorMedication);
}

function onClickGetVitalData(){
    getVitals();
}
function showSSNNumver(num){
    var res = num, //grabs the value
        len = res.length, //grabs the length
        stars = len>0?len>1?len>2?len>3?len>4?'XXX-XX-':'XXX-X':'XXX-':'XX':'X':'', //this provides the masking and formatting
        result = stars+res.substring(5); //this is the result
    return result;
}
var strPName = "";
var facilityId = "";
function onGetPatientInfo(dataObj){
    if(dataObj && dataObj.response && dataObj.response.patient){
        parentRef.patientInfo = dataObj.response.patient;
        facilityId = dataObj.response.patient.facilityId;
        strPName = dataObj.response.patient.firstName+" "+dataObj.response.patient.middleName+" "+dataObj.response.patient.lastName;
        $("#divPatInfo",parent.document).show();
        $("#divIdMain",parent.document).show();
        $("#divTitleName",parent.document).hide();
        //photoExt = dataObj.response.patient.photoExt;
        var imageServletUrl = "";
        if(dataObj.response.patient.photoExt){
            imageServletUrl = ipAddress+"/download/patient/photo/"+strPatientId+"/?"+Math.round(Math.random()*1000000)+"&access_token="+sessionStorage.access_token+"&tenant="+sessionStorage.tenant;;
        }else if(dataObj.response.patient.gender == "Male"){
            imageServletUrl = "../../img/AppImg/HosImages/male_profile.png";
        }else if(dataObj.response.patient.gender == "Female"){
            imageServletUrl = "../../img/AppImg/HosImages/profile_female.png";//ipAddress+"/download/patient/photo/"+strPatientId+"/?"+Math.round(Math.random()*1000000);
        }
        $("#userImg",parent.document).show();
        $("#userImg",parent.document).attr("src",imageServletUrl);

        if(dataObj.response.patient.dnr == 1){
            $("#userImg",parent.document).css("border","3px solid red");
        }
        else{
            $("#userImg",parent.document).css("border","3px solid white");

        }

        //$("#divVideoPanelFrame",parent.document).show();
        //$("#videoFrame",parent.document).attr('src', "../../html/patients/videoCallModule.html") ;
        //strPatientId = selObj.PID;
        $("#lblptID",parent.document).html(dataObj.response.patient.id);
        $("#ptName",parent.document).html(dataObj.response.patient.firstName+" "+dataObj.response.patient.middleName+" "+dataObj.response.patient.lastName);
        $("#lblGender",parent.document).html(dataObj.response.patient.gender);
        var dt = new Date(dataObj.response.patient.dateOfBirth);
        if(dt){
            var strDT = onGetDate(dataObj.response.patient.dateOfBirth);
            var currDate = new Date();
            //console.log(currDate.getFullYear()-dt.getFullYear());
            var strAge = getAge(dt);// currDate.getFullYear()-dt.getFullYear();
            strDT = strDT+"  ("+strAge+" Y)"
            $("#ptDOB",parent.document).html(strDT);
            var str1 = strDT+' , '+dataObj.response.patient.gender;//+" - "+dataObj.response.patient.firstName+" "+dataObj.response.patient.middleName+" "+dataObj.response.patient.lastName;
            $("#pt2").text(str1);
            //$("#ptDOB").val(strDT);
        }
        //$("#ptDOB",parent.document).html(dataObj.response.patient.dateOfBirth);
        $("#lblStatus",parent.document).html(dataObj.response.patient.status);
        $("#lblptID",parent.document).html(dataObj.response.patient.id);
        $("#lblPtHeight",parent.document).html(dataObj.response.patient.height);
        $("#lblPtWeight",parent.document).html(dataObj.response.patient.weight);

        var str = dataObj.response.patient.id+" - "+dataObj.response.patient.firstName+" "+dataObj.response.patient.middleName+" "+dataObj.response.patient.lastName;
        $("#pt1").text(str);
        //var imageServletUrl = ipAddress+"/download/patient/photo/"+strPatientId+"/?"+Math.round(Math.random()*1000000);
        $("#imgPreview").attr("src",imageServletUrl);

        $("#divPatientInfo").show();
    }
	/*$("#txtID").val(dataObj.response.patient.id);
	 $("#txtExtID1").val(dataObj.response.patient.externalId1);
	 $("#txtExtID2").val(dataObj.response.patient.externalId2);
	 ptExtId2 = dataObj.response.patient.externalId2;
	 $("#txtPrefix").val(dataObj.response.patient.prefix);
	 $("#txtStatus").val(dataObj.response.patient.status);
	 $("#txtNN").val(dataObj.response.patient.nickname);
	 $("#txtFN").val(dataObj.response.patient.firstName);
	 $("#txtMN").val(dataObj.response.patient.middleName);
	 $("#txtLN").val(dataObj.response.patient.lastName);
	 var dt = new Date(dataObj.response.patient.dateOfBirth);
	 if(dt){
	 var strDT = kendo.toString(dt,"MM-dd-yyyy");
	 $("#txtDOB").val(strDT);

	 strAge = getAge(dt);
	 $("#txtAge").val(strAge);
	 }
	 var ssn = showSSNNumver(dataObj.response.patient.ssn);
	 $("#txtSSN").val(ssn);
	 $("#txtWeight").val(dataObj.response.patient.weight);
	 $("#txtHeight").val(dataObj.response.patient.height);
	 $("#txtGender").val(dataObj.response.patient.gender);
	 $("#txtEthnicity").val(dataObj.response.patient.ethnicity);
	 $("#txtRace").val(dataObj.response.patient.race);
	 $("#txtLan").val(dataObj.response.patient.language);
	 }
	 if(dataObj && dataObj.response && dataObj.response.communication){
	 var commArray = [];
	 if($.isArray(dataObj.response.communication)){
	 commArray = dataObj.response.communication;
	 }else{
	 commArray.push(dataObj.response.communication);
	 }
	 var comObj = commArray[0];
	 $("#txtAddr1").val(comObj.address1);
	 $("#txtAddr2").val(comObj.address2);
	 $("#txtSMS").val(comObj.sms);
	 $("#txtCell").val(comObj.cellPhone);
	 $("#txtExtension").val(comObj.workPhoneExt);
	 $("#txtWORK").val(comObj.workPhone);
	 $("#txtHExt").val(comObj.homePhoneExt);
	 $("#txtHM").val(comObj.homePhone);

	 $("#txtCity").val(comObj.city);
	 $("#txtState").val(comObj.state);
	 $("#txtZip").val(comObj.zip);
	 $("#txtZip4").val(comObj.zipFour);
	 $("#txtCity").val(comObj.city);
	 }*/
}
function onGetMedication(dataObj){
    //console.log(dataObj);
    $("#divTable").text("");
    var medicArray = [];
    if(dataObj){
        if($.isArray(dataObj)){
            medicArray = dataObj;
        }else{
            medicArray.push(dataObj);
        }
    }
	/*var strTable = '<table data-toggle="table" data-classes="table table-hover table-condensed"  data-row-style="rowColors"  data-striped="true"  data-sort-name="Quality"  data-sort-order="desc"  data-pagination="true"	 data-click-to-select="true">';
	 strTable = strTable+'<thead>';
	 strTable = strTable+'<tr>';
	 strTable = strTable+'<th class="col-xs-1" data-field="Date" data-sortable="true">Date</th>';
	 strTable = strTable+'<th class="col-xs-1" data-field="Name" data-sortable="true">Name</th>';
	 strTable = strTable+'<th class="col-xs-1" data-field="Strength" data-sortable="true">Strength</th>';
	 strTable = strTable+'<th class="col-xs-1" data-field="Form" data-sortable="true">Form</th>';
	 strTable = strTable+'<th class="col-xs-1" data-field="Schedule" data-sortable="true">Schedule</th>';
	 strTable = strTable+'<th class="col-xs-1" data-field="Instructions" data-sortable="true">Instructions</th>';
	 strTable = strTable+'</tr></thead>*/
    var strTable = '<table class="table">';
    strTable = strTable+'<thead class="fillsHeader">';
    strTable = strTable+'<tr>';
    //strTable = strTable+'<th class="textAlign whiteColor">Date</th>';
    strTable = strTable+'<th class="textAlign whiteColor" style="width:210px">Name</th>';
    strTable = strTable+'<th class="textAlign whiteColor">Morning</th>';
    strTable = strTable+'<th class="textAlign whiteColor">Lunch</th>';
    strTable = strTable+'<th class="textAlign whiteColor">Evening</th>';
    strTable = strTable+'<th class="textAlign whiteColor">Night</th>';
    strTable = strTable+'<th class="textAlign whiteColor">Strength</th>';
    strTable = strTable+'<th class="textAlign whiteColor">Form</th>';
    strTable = strTable+'<th class="textAlign whiteColor">Schedule</th>';
    strTable = strTable+'<th class="textAlign whiteColor">Instructions</th>';
    //strTable = strTable+'<th class="textAlign whiteColor">Timing</th>';
    strTable = strTable+'</tr>';
    strTable = strTable+'</thead>';
    strTable = strTable+'<tbody>';

    for(var i=0;i<medicArray.length;i++){
        var dataItem = medicArray[i];
        if(dataItem){
            //console.log(dataItem);
            var className = "success";
            if(i%2 == 0){
                className = "danger";
            }
            if(i%3 == 0){
                className = "info";
            }
            if(i%3 == 0){
                className = "active";
            }
            var strMorning = "";
            var strLunch = "";
            var strEvening = "";
            var strNight = "";
            var timing = dataItem.timing;
            var timiningArray = timing.split(" ");
            //console.log(timiningArray);
            className = getRowColors(i);
            strTable = strTable+'<tr class="'+className+'">';
            //strTable = strTable+'<td>'+kendo.toString(new Date(dataItem.date), "MM/dd/yyyy")+'</td>';
            //strTable = strTable+'<td>'+dataItem.name+'</td>';
            if(dataItem.fileResourceId && dataItem.fileResourceId !="null"){
                strTable = strTable+'<td class="textAlign"><a id="'+dataItem.fileResourceId+'" onClick="onClickMyAudio(event)" style="cursor:pointer" class="hyperLinkStyle">'+dataItem.name+'</a></td>';
            }else{
                strTable = strTable+'<td class="textAlign">'+dataItem.name+'</td>';
            }

            //
            //var strAction = "";
            for(var j=0;j<timiningArray.length;j++){
                var item = timiningArray[j];
                if(item == "1"){
                    strMorning  = '<img  src="../../img/AppImg/HosImages/blue_1.png"  class="cusrsorStyle medicationIcon" title="Morning"></img>&nbsp;&nbsp;&nbsp;';
                }else if(item == "2"){
                    strLunch = '<img  src="../../img/AppImg/HosImages/green_1.png"  class="cusrsorStyle medicationIcon" title="Lunch"></img>&nbsp;&nbsp;&nbsp;';
                }else if(item == "3"){
                    strEvening = '<img  src="../../img/AppImg/HosImages/orange_1.png"  class="cusrsorStyle medicationIcon" title="Evening"></img>&nbsp;&nbsp;&nbsp;';
                }else if(item == "4"){
                    strNight = '<img  src="../../img/AppImg/HosImages/red_1.png"  class="cusrsorStyle medicationIcon" title="Night"></img>';
                }
            }
            strTable = strTable+'<td class="textAlign">'+strMorning+'</td>';
            strTable = strTable+'<td class="textAlign">'+strLunch+'</td>';
            strTable = strTable+'<td class="textAlign">'+strEvening+'</td>';
            strTable = strTable+'<td class="textAlign">'+strNight+'</td>';

            strTable = strTable+'<td class="textAlign">'+dataItem.strength+'</td>';
            strTable = strTable+'<td class="textAlign">'+dataItem.form+'</td>';
            strTable = strTable+'<td class="textAlign">'+dataItem.schedule+'</td>';
            strTable = strTable+'<td class="textAlign">'+dataItem.instructions+'</td>';
            //strTable = strTable+'<td>'+strAction+'</td>';
            strTable = strTable+'</tr>';
        }
    }
    strTable = strTable+'</tbody>';
    strTable = strTable+'</table>';
    $("#divTable").append(strTable);
}
function getRowColors(index) {
    var classes = ['active', 'success', 'info', 'warning', 'danger'];
    // console.log(index);
    if (index % 2 === 0 && index / 2 < classes.length) {
        //console.log(classes[index / 2]);
        return classes[index / 2];
        //console.log(index+','+classes[index / 2]);
		/*  return {
		 classes: classes[index / 2]
		 };*/
    }
    return "";
}
function onGetAppointSuccess(dataObj){
    //console.log(dataObj);
    $("#divAppointment").text("");
    var appointArray = [];
    if(dataObj){
        if($.isArray(dataObj)){
            appointArray = dataObj;
        }else{
            appointArray.push(dataObj);
        }
    }
    var strTable = '<table class="table">';
    strTable = strTable+'<thead class="fillsHeader">';
    strTable = strTable+'<tr>';
    strTable = strTable+'<th class="textAlign whiteColor">Date</th>';
    strTable = strTable+'<th class="textAlign whiteColor">Time</th>';
    strTable = strTable+'<th class="textAlign whiteColor">Doctor</th>';
    strTable = strTable+'<th class="textAlign whiteColor">Reason</th>';
    strTable = strTable+'<th class="textAlign whiteColor">Facility</th>';
    strTable = strTable+'<th class="textAlign whiteColor">Remarks</th>';
    strTable = strTable+'</tr>';
    strTable = strTable+'</thead>';
    strTable = strTable+'<tbody>';
    for(var i=0;i<appointArray.length;i++){
        var dataItem = appointArray[i];
        if(dataItem){
            var className = "success";
			/*if(i%2 == 0){
			 className = "danger";
			 }
			 if(i%3 == 0){
			 className = "info";
			 }
			 if(i%3 == 0){
			 className = "active";
			 }*/
            if(dataItem.status == "Confirmed"){
                className = "confirm";
            }else if(dataItem.status == "Not Confirmed"){
                className = "completed ";
            }else if(dataItem.status == "Completed"){
                className = "notConfirm";
            }
            strTable = strTable+'<tr class="'+className+'">';
            strTable = strTable+'<td>'+kendo.toString(new Date(dataItem.dateOfAppointment), "MM/dd/yyyy")+'</td>';
            strTable = strTable+'<td>'+dataItem.timeOfAppointment+'</td>';
            strTable = strTable+'<td>'+dataItem.doctor+'</td>';
            strTable = strTable+'<td>'+dataItem.reason+'</td>';
            strTable = strTable+'<td>'+dataItem.facility+'</td>';
            strTable = strTable+'<td>'+dataItem.status+'</td>';
            strTable = strTable+'</tr>';
        }
    }
    strTable = strTable+'</tbody>';
    strTable = strTable+'</table>';
    $("#divAppointment").append(strTable);
}
function findVitalByIndex(idx,attr){
    var dataItem = vitalDataArray[idx];
    if(dataItem){
        if(attr == "Date"){
            return dataItem.readingDate;
        }else if(attr == "Time"){
            return dataItem.readingTime;
        }else if(attr == "Value"){
            return dataItem.value;
        }
    }
    return "";
	/*for(var i=0;i<vitalDataArray.length;i++){
	 var dataItem = vitalDataArray[i];
	 if(dataItem){
	 if(attr == "Date"){

	 }
	 }
	 }*/
}
var vitalDataArray = [];
function mycomparator(a,b) {
    return Number(a.readingDate) - Number(b.readingDate);
}
function isVitalDateExist(rDate){

}
function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}
function getVitalHeaders(){
    vitalDataArray.sort(mycomparator);
    var vitalArr = [];
    for(var i=0;i<vitalDataArray.length;i++){
        var item = vitalDataArray[i];
        var rd = Number(item.readingDate);
        var dtOffsetDate = new Date();
        var offset = dtOffsetDate.getTimezoneOffset();
        offset = offset*60*1000;
        rd = rd+Math.abs(offset);
        var dt = new Date(rd);
        var strDT = kendo.toString(dt,"MM-dd-yyyy");
        strDT = strDT+" "+formatAMPM(dt);
        item.date = strDT;
        if(vitalArr.indexOf(strDT)<0){
            vitalArr.push(strDT);
        }
    }
    if(vitalArr.length>0){
        vitalArr = vitalArr.reverse();
    }
    return vitalArr;
}
function getVitalInfo(vName,dt){
    for(var i=0;i<vitalDataArray.length;i++){
        var item = vitalDataArray[i];
        if(item.vital == vName && item.date == dt){
            return item.value;
        }
    }
    return "";
}

function onClickVitalsRefresh(){
    if(ptExtId2 && ptExtId2 != ""){
        getAjaxObject(ipAddress+"/hbt/getPatientData/"+strPatientId,"GET",onGetHBTPatientData,onErrorMedication);
        //getVitals();
    }
}
function onGetHBTPatientData(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        getVitals();
    }
}
function onGetVitalsSuccess(dataObj){
    var vitalsArray = [];
    if(dataObj && dataObj.vitals){
        if($.isArray(dataObj.vitals)){
            vitalsArray = dataObj.vitals;
        }else{
            vitalsArray.push(dataObj.vitals)
        }
    }
    vitalDataArray = vitalsArray;
    var headers = getVitalHeaders();
    console.log(headers);
    $("#divVitals").html("");
    if(headers && headers.length>0){
        var strTable = '<table class="table">';
        strTable = strTable+'<thead class="fillsHeader">';
        strTable = strTable+'<tr>';
        //strTable = strTable+'<th colspan="2" class="vitalsHeader">Patient Information</th>';
        strTable = strTable+'<th class="textAlign whiteColor">Vital</th>';
        strTable = strTable+'<th class="textAlign whiteColor" style="width:60px">Date</th>';

        for(var j=0;j<headers.length;j++){
            strTable = strTable+'<th class="textAlign whiteColor" style="width:90px">'+headers[j]+'</th>';
        }
        strTable = strTable+'</tr>';
        strTable = strTable+'</thead>';
        strTable = strTable+'<tbody>';
        strTable = strTable+'<tr>';
        strTable = strTable+'<td class="textAlign heartRate whiteColor">Heart Rate</td>';
        strTable = strTable+'<td class="textAlign"><img  src="../../img/AppImg/HosImages/heartrate.png"  class="cusrsorStyle vitalImage"></img></td>';

        for(var j=0;j<headers.length;j++){
            strTable = strTable+'<td class="textAlign heartRate whiteColor">'+getVitalInfo('Heart Rate',headers[j])+'</td>';
        }
        strTable = strTable+'</tr>';
        strTable = strTable+'<tr>';
        strTable = strTable+'<td class="textAlign raspiberry whiteColor">Raspiratory</td>';
        strTable = strTable+'<td class="textAlign"><img  src="../../img/AppImg/HosImages/respiratory.png"  class="cusrsorStyle vitalImage"></img></td>';
        for(var j=0;j<headers.length;j++){
            strTable = strTable+'<td class="textAlign raspiberry whiteColor">'+getVitalInfo('Raspiratory',headers[j])+'</td>';
        }
        strTable = strTable+'</tr>';
        strTable = strTable+'<tr>';
        strTable = strTable+'<tr>';
        strTable = strTable+'<td class="textAlign bp whiteColor">Blood Pressure</td>';
        strTable = strTable+'<td class="textAlign"><img  src="../../img/AppImg/HosImages/bp.png"  class="cusrsorStyle vitalImage"></img></td>';
        for(var j=0;j<headers.length;j++){
            strTable = strTable+'<td class="textAlign bp whiteColor">'+getVitalInfo('Blood Pressure',headers[j])+'</td>';
        }
        strTable = strTable+'</tr>';

        strTable = strTable+'<tr>';
        strTable = strTable+'<td class="textAlign temparature whiteColor">Temparature</td>';
        strTable = strTable+'<td class="textAlign"><img  src="../../img/AppImg/HosImages/temparature.png"  class="cusrsorStyle vitalImage"></img></td>';
        for(var j=0;j<headers.length;j++){
            strTable = strTable+'<td class="textAlign temparature whiteColor">'+getVitalInfo('Temparature',headers[j])+'</td>';
        }

        strTable = strTable+'</tr>';

        strTable = strTable+'<tr>';
        strTable = strTable+'<td class="textAlign spo2 whiteColor">SpO2</td>';
        strTable = strTable+'<td class="textAlign"><img  src="../../img/AppImg/HosImages/spo2.png"  class="cusrsorStyle vitalImage"></img></td>';
        for(var j=0;j<headers.length;j++){
            strTable = strTable+'<td class="textAlign spo2 whiteColor">'+getVitalInfo('SpO2',headers[j])+'</td>';
        }
        strTable = strTable+'</tr>';

        strTable = strTable+'<tr>';
        strTable = strTable+'<td class="textAlign systolicbp whiteColor">Systolic BP</td>';
        strTable = strTable+'<td class="textAlign"><img  src="../../img/AppImg/HosImages/systolicbp.png"  class="cusrsorStyle vitalImage"></img></td>';
        for(var j=0;j<headers.length;j++){
            strTable = strTable+'<td class="textAlign systolicbp whiteColor">'+getVitalInfo('Systolic BP',headers[j])+'</td>';
        }
        strTable = strTable+'</tr>';

        strTable = strTable+'<tr>';
        strTable = strTable+'<td class="textAlign diastolicbp whiteColor">Diastolic BP</td>';
        strTable = strTable+'<td class="textAlign"><img  src="../../img/AppImg/HosImages/diastolicbp.png"  class="cusrsorStyle vitalImage"></img></td>';
        for(var j=0;j<headers.length;j++){
            strTable = strTable+'<td class="textAlign diastolicbp whiteColor">'+getVitalInfo('Diastolic BP',headers[j])+'</td>';
        }

        strTable = strTable+'</tr>';

        strTable = strTable+'<tr>';
        strTable = strTable+'<td class="textAlign cardicOut whiteColor">Cardiatic Output</td>';
        strTable = strTable+'<td class="textAlign"><img  src="../../img/AppImg/HosImages/cardiatic.png"  class="cusrsorStyle vitalImage"></img></td>';
        for(var j=0;j<headers.length;j++){
            strTable = strTable+'<td class="textAlign cardicOut whiteColor">'+getVitalInfo('Cardiatic Output',headers[j])+'</td>';
        }
        strTable = strTable+'</tr>';

        strTable = strTable+'<tr>';
        strTable = strTable+'<td class="textAlign cardicIdx whiteColor">Cardiac Index</td>';
        strTable = strTable+'<td class="textAlign"><img  src="../../img/AppImg/HosImages/cardiaticidx.png"  class="cusrsorStyle vitalImage"></img></td>';
        for(var j=0;j<headers.length;j++){
            strTable = strTable+'<td class="textAlign cardicIdx whiteColor">'+getVitalInfo('Cardiac Index',headers[j])+'</td>';
        }
        strTable = strTable+'</tr>';

        strTable = strTable+'<tr>';
        strTable = strTable+'<td class="textAlign systemV whiteColor">Systemic Vascular Resistance</td>';
        strTable = strTable+'<td class="textAlign"><img  src="../../img/AppImg/HosImages/systematicV.png"  class="cusrsorStyle vitalImage"></img></td>';
        for(var j=0;j<headers.length;j++){
            strTable = strTable+'<td class="textAlign systemV whiteColor">'+getVitalInfo('Systemic Vascular Resistance',headers[j])+'</td>';
        }
        strTable = strTable+'</tr>';

        strTable = strTable+'<tr>';
        strTable = strTable+'<td class="textAlign sVolume whiteColor">Stroke Volume</td>';
        strTable = strTable+'<td class="textAlign"><img  src="../../img/AppImg/HosImages/strokeVolume.png"  class="cusrsorStyle vitalImage"></img></td>';
        for(var j=0;j<headers.length;j++){
            strTable = strTable+'<td class="textAlign sVolume whiteColor">'+getVitalInfo('Stroke Volume',headers[j])+'</td>';
        }

        strTable = strTable+'</tr>';

        strTable = strTable+'</tbody>';
        strTable = strTable+'</table>';
        $("#divVitals").html("");
        $("#divVitals").append(strTable);
    }
}
function onGetExcersizeSuccess(dataObj){
    var excerArr = [];
    if(dataObj){
        if($.isArray(dataObj)){
            excerArr = dataObj;
        }else{
            excerArr.push(dataObj);
        }
    }
    excerDataArr = excerArr;
    $("#divExcersize").text("");
    var strTable = '<table class="table">';
    strTable = strTable+'<thead class="fillsHeader">';
    strTable = strTable+'<tr>';
    //strTable = strTable+'<th>Patient ID</th>';
    strTable = strTable+'<th class="textAlign whiteColor">File Name</th>';
    strTable = strTable+'<th class="textAlign whiteColor">File Type</th>';
    strTable = strTable+'</tr>';
    strTable = strTable+'</thead>';
    strTable = strTable+'<tbody>';
    for(var i=0;i<excerArr.length;i++){
        var dataItem = excerArr[i];
        if(dataItem){
            //console.log(dataItem);
            var className = getRowColors(i);
            strTable = strTable+'<tr class="'+className+'">';
            //strTable = strTable+'<td>'+dataItem.patientId+'</td>';
            strTable = strTable+'<td class="textAlign">'+dataItem.fileName+'</td>';
            var dietId = dataItem.id;
            if(dataItem && dataItem.fileType && dataItem.fileType.toLowerCase() == "mp4"){
                strTable = strTable+'<td style="width:200px;padding:0px" class="textAlign"><img  id="'+dietId+'" src="../../img/AppImg/HosImages/video.png"  class="cusrsorStyle videoIcon" onClick="onClickExcersize(event)"></td>';
            }else if(dataItem && dataItem.fileType && dataItem.fileType.toLowerCase() == "pdf"){
                strTable = strTable+'<td style="width:200px;padding:0px" class="textAlign"><img  id="'+dietId+'" src="../../img/AppImg/HosImages/pdf.png"   class="cusrsorStyle pdfIcon" onClick="onClickExcersize(event)"></td>';
            }else{
                strTable = strTable+'<td style="width:200px;padding:0px" class="textAlign"><img  id="'+dietId+'" src="../../img/AppImg/HosImages/video.png"  class="cusrsorStyle videoIcon" onClick="onClickYouTubeExcersize(event)"></td>';
            }

            //
            strTable = strTable+'</tr>';
        }
    }
    strTable = strTable+'</tbody>';
    strTable = strTable+'</table>';
    $("#divExcersize").append(strTable);
}
function onGetDietSuccess(dataObj){
//	console.log(dataObj);
    var dietArr = [];
    if(dataObj){
        if($.isArray(dataObj)){
            dietArr = dataObj;
        }else{
            dietArr.push(dataObj);
        }
    }
    dietDataArray = dietArr;
    $("#divDiet").text("");
    var strTable = '<table class="table">';
    strTable = strTable+'<thead class="fillsHeader whiteColor">';
    strTable = strTable+'<tr>';
    //strTable = strTable+'<th>Patient ID</th>';
    strTable = strTable+'<th class="textAlign whiteColor">File Name</th>';
    strTable = strTable+'<th class="textAlign whiteColor">File Type</th>';
    strTable = strTable+'</tr>';
    strTable = strTable+'</thead>';
    strTable = strTable+'<tbody>';
    for(var i=0;i<dietArr.length;i++){
        var dataItem = dietArr[i];
        if(dataItem){
            //console.log(dataItem);
            var className = getRowColors(i);
            strTable = strTable+'<tr class="'+className+'">';
            //strTable = strTable+'<td>'+dataItem.patientId+'</td>';
            strTable = strTable+'<td class="textAlign">'+dataItem.fileName+'</td>';
            var dietId = dataItem.id;
            if(dataItem.fileType && (dataItem.fileType.toLowerCase() == "mp4" || dataItem.fileType.toLowerCase() == "mov")){
                strTable = strTable+'<td style="width:200px;padding:0px" class="textAlign"><img id="'+dietId+'" src="../../img/AppImg/HosImages/video.png"  class="cusrsorStyle videoIcon" onClick="onClickGenerate(event)"></td>';
            }else if(dataItem.fileType && dataItem.fileType.toLowerCase() == "pdf"){
                strTable = strTable+'<td style="width:200px;padding:0px" class="textAlign"><img  id="'+dietId+'" src="../../img/AppImg/HosImages/pdf.png"   class="cusrsorStyle pdfIcon" onClick="onClickGenerate(event)"></td>';
            }else{
                strTable = strTable+'<td style="width:200px;padding:0px" class="textAlign"><img id="'+dietId+'" src="../../img/AppImg/HosImages/video.png"  class="cusrsorStyle videoIcon" onClick="onClickDietYoutube(event)"></td>';
            }
            strTable = strTable+'</tr>';
        }
    }
    strTable = strTable+'</tbody>';
    strTable = strTable+'</table>';
    $("#divDiet").append(strTable);
}
var illnessArr = [];
function onGetIllnessSuccess(dataObj){
    $("#divaddIllnes").text("");
    illnessArr = [];
    if(dataObj){
        if($.isArray(dataObj)){
            illnessArr = dataObj;
        }else{
            illnessArr.push(dataObj);
        }
    }
    var strTable1 = '<table class="table">';
    strTable1 = strTable1+'<thead class="fillsHeader">';
    strTable1 = strTable1+'<tr>';
    //strTable = strTable+'<th>Patient ID</th>';
    //strTable = strTable+'<th class="textAlign whiteColor">Link</th>';
    //strTable = strTable+'<th>Link</th>';
    strTable1 = strTable1+'<th class="textAlign whiteColor">Description</th>';
    strTable1 = strTable1+'</tr>';
    strTable1 = strTable1+'</thead>';
    strTable1 = strTable1+'<tbody>';
    for(var i=0;i<illnessArr.length;i++){
        var dataItem = illnessArr[i];
        if(dataItem){
            //console.log(dataItem);
            var className = getRowColors(i);
            strTable1 = strTable1+'<tr  class="'+className+'">';
            //strTable = strTable+'<td>'+dataItem.patientId+'</td>';
            //	strTable = strTable+'<td><a target="_blank" href="'+dataItem.link+'">'+dataItem.link+'</a></td>';
            //strTable = strTable+'<td>'+dataItem.link+'</td>';
            if(dataItem.fileName){
                strTable1 = strTable1+'<td class="textAlign"><a onClick="onClickIllnessVideo(event)" style="cursor:pointer" class="hyperLinkStyle">'+dataItem.fileName+'</a></td>';
            }else{
                strTable1 = strTable1+'<td class="textAlign">'+dataItem.fileName+'</td>';
            }

            strTable1 = strTable1+'</tr>';
        }
    }
    strTable1 = strTable1+'</tbody>';
    strTable1 = strTable1+'</table>';
    $("#divaddIllnes").append(strTable1);
}
function myRowFunction(event){
    console.log(event);
    var evtId = "";
    if(event.currentTarget){
        evtId = event.currentTarget.id;
    }
    evtId = Number(evtId)-1;
    var dataObj = interestArr[evtId];
    parentRef.screenType = "Edit";
    if(dataObj){
        parentRef.dataObj = dataObj;
        onClickCreateInterest();
    }
}
var interestArr = [];
function onGetInterestsSuccess(dataObj){
    //console.log(dataObj);
    $("#divInterests").text("");
    interestArr = [];
    if(dataObj){
        if($.isArray(dataObj)){
            interestArr = dataObj;
        }else{
            interestArr.push(dataObj);
        }
    }
    var strTable = '<table class="table">';
    strTable = strTable+'<thead class="fillsHeader">';
    strTable = strTable+'<tr>';
    strTable = strTable+'<th class="textAlign whiteColor">Question</th>';
    strTable = strTable+'<th class="textAlign whiteColor">Answer</th>';
    strTable = strTable+'<th class="textAlign whiteColor">Action</th>';
    strTable = strTable+'</tr>';
    strTable = strTable+'</thead>';
    strTable = strTable+'<tbody>';
    for(var i=0;i<interestArr.length;i++){
        var dataItem = interestArr[i];
        if(dataItem){
            //console.log(dataItem);
            var className = getRowColors(i);
            strTable = strTable+'<tr  class="'+className+'" style="cursor:pointer">';
            //strTable = strTable+'<td>'+dataItem.patientId+'</td>';
            strTable = strTable+'<td>'+dataItem.question+'</td>';
            //strTable = strTable+'<td>'+dataItem.link+'</td>';
            strTable = strTable+'<td>'+dataItem.answer+'</td>';
            strTable = strTable+'<td style="cursor:pointer"><span id="'+dataItem.id+'" onClick="myRowFunction(event)">Edit</span></td>';
            strTable = strTable+'</tr>';
        }
    }
    strTable = strTable+'</tbody>';
    strTable = strTable+'</table>';
    $("#divInterests").append(strTable);
}
var ptactivites = [];
function onGetPatientActivities(dataObj){
    $("#divptActivies").text("");
    ptactivites = [];
    if(dataObj && dataObj.response && dataObj.response.patientActivity){
        if($.isArray(dataObj.response.patientActivity)){
            ptactivites = dataObj.response.patientActivity;
        }else{
            ptactivites.push(dataObj.response.patientActivity);
        }
    }
    var strTable = '<table class="table">';
    strTable = strTable+'<thead class="fillsHeader">';
    strTable = strTable+'<tr>';
    strTable = strTable+'<th class="textAlign whiteColor">Activity</th>';
    strTable = strTable+'<th class="textAlign whiteColor">Charge</th>';
    strTable = strTable+'<th class="textAlign whiteColor">Duration</th>';
    strTable = strTable+'</tr>';
    strTable = strTable+'</thead>';
    strTable = strTable+'<tbody>';

    for(var i=0;i<ptactivites.length;i++){
        var dataItem = ptactivites[i];
        if(dataItem){
            var className = getRowColors(i);
            strTable = strTable+'<tr  class="'+className+'" style="cursor:pointer">';
            //strTable = strTable+'<td>'+dataItem.patientId+'</td>';
            if(dataItem.type == 0){
                strTable = strTable+'<td>ADL</td>';
            }else if(dataItem.type == 1){
                strTable = strTable+'<td>iADL</td>';
            }else{
                strTable = strTable+'<td></td>';
            }
            if(dataItem.charge){
                strTable = strTable+'<td>'+dataItem.charge+'</td>';
            }else{
                strTable = strTable+'<td></td>';
            }
            if(dataItem.duration){
                strTable = strTable+'<td>'+dataItem.duration+'</td>';
            }else{
                strTable = strTable+'<td></td>';
            }

            strTable = strTable+'</tr>';
        }
    }
    strTable = strTable+'</tbody>';
    strTable = strTable+'</table>';
    $("#divptActivies").append(strTable);

}
function onErrorMedication(errobj){
    console.log(errobj);
}
// function getAjaxObject(dataUrl,method,successFunction,errorFunction){
// Loader.showLoader();
// $.ajax({
// type: method,
// url: dataUrl,
// data: null,
// context: this,
// cache: false,
// success: function( data, statusCode, jqXHR ){
// Loader.hideLoader();
// successFunction(data);
// },
// error: function( jqXHR, textStatus, errorThrown ){
// Loader.hideLoader();
// errorFunction(jqXHR);
// },
// contentType: "application/json",
// });

// }
function onClickLeftButton(){
    console.log("left");
    var leftPnlVisible = $("#leftPanel").is(":visible");
    var rightPnlVisible = $("#rightPanel").is(":visible");

    if(leftPnlVisible){
        $("#leftPanel").hide();
        if(!rightPnlVisible){
            $("#centerPanel").removeClass("col-sm-10 col-md-10 col-lg-10 col-xs-12 panelBorderStyle");
            $("#centerPanel").addClass("col-sm-12 col-md-12 col-lg-12 col-xs-12 panelBorderStyle");
        }else{
            $("#centerPanel").removeClass("col-sm-8 col-md-8 col-lg-8 col-xs-12 panelBorderStyle");
            $("#centerPanel").addClass("col-sm-9 col-md-9 col-lg-9 col-xs-12 panelBorderStyle");
        }

    }else{
        $("#leftPanel").show();
        if(!rightPnlVisible){
            $("#centerPanel").removeClass("col-sm-12 col-md-12 col-lg-12 col-xs-12 panelBorderStyle");
            $("#centerPanel").addClass("col-sm-10 col-md-10 col-lg-10 col-xs-10 panelBorderStyle");
        }else{
            $("#centerPanel").removeClass("col-sm-9 col-md-9 col-lg-9 col-xs-12 panelBorderStyle");
            $("#centerPanel").addClass("col-sm-8 col-md-8 col-lg-8 col-xs-12 panelBorderStyle");
        }

    }
}

function onClickRightButton(){
    console.log("right");
    var leftPnlVisible = $("#leftPanel").is(":visible");
    var rightPnlVisible = $("#rightPanel").is(":visible");

    if(rightPnlVisible){
        $("#rightPanel").hide();
        if(!leftPnlVisible){
            $("#centerPanel").removeClass("col-sm-9 col-md-9 col-lg-9 col-xs-12 panelBorderStyle");
            $("#centerPanel").addClass("col-sm-12 col-md-12 col-lg-12 col-xs-12 panelBorderStyle");
        }else{
            $("#centerPanel").removeClass("col-sm-8 col-md-8 col-lg-8 col-xs-12 panelBorderStyle");
            $("#centerPanel").addClass("col-sm-10 col-md-10 col-lg-10 col-xs-12 panelBorderStyle");
        }
    }else{
        $("#rightPanel").show();
        $("#centerPanel").removeClass("col-sm-10 col-md-10 col-lg-10 col-xs-12 panelBorderStyle");
        $("#centerPanel").addClass("col-sm-8 col-md-8 col-lg-8 col-xs-12 panelBorderStyle");
    }
}

var ADD = "add";
var EDIT = "edit";
var operation = "";
function onClickPatientInfo(){
    operation = EDIT;
    parentRef.patientId = strPatientId;
    addPatient();
}
function onClickAddPatient(){
    console.log("Add");
    operation = ADD;
    parentRef.patientId = "";
    addPatient();
    //$("#centerPanel").css("visibility","visible");
    //$(html).show();
}
function addPatient(){
    var popW = "65%";
    var popH = "72%";

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Add Service User";
    if(operation == EDIT){
        profileLbl = "Edit Service User ";
    }

    parentRef.operation = operation;
    // parentRef.selItem = selCountryItem;

    devModelWindowWrapper.openPageWindow("../../html/patients/createPatient.html", profileLbl, popW, popH, true, closeAddPatientAction);
}
function closeAddPatientAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        var opr = returnData.operation;
        if(opr == ADD){
           // var patientListURL = ipAddress+"/patient/list/?id="+parentRef.patientId;
            if(strPatientId != ""){
                var imageServletUrl = ipAddress+"/download/patient/photo/"+strPatientId+"/?"+Math.round(Math.random()*1000000);
                $("#imgPreview").attr("src",imageServletUrl);
            }

            document.getElementById("mySelect").value = 2;
            patientSearch();

            strPatientId = parentRef.patientId;
            $("#txtPID").val(strPatientId);
            onPatientSearch();
            // getAjaxObject(patientListURL,"GET",onGetCreatePatientInfo,onErrorMedication);
        }
    }
    if(strPatientId != ""){
        var imageServletUrl = ipAddress+"/download/patient/photo/"+strPatientId+"/?"+Math.round(Math.random()*1000000);
        $("#imgPreview").attr("src",imageServletUrl);
    }

    getPatientCountList();
    patentComplePanelInfo();

}
function onClickCreateInterest(){
    var popW = "67%";
    var popH = 450;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Create Preventive";
    devModelWindowWrapper.openPageWindow("../../html/patients/createInterest.html", profileLbl, popW, popH, true, closeInterestScreen);
}
function closeInterestScreen(evt,returnData){
    if(returnData && returnData.status == "success"){
        //customAlert.error("Save","Created");
        getInterests();
    }
}
function onClickEditInterest(){
    var popW = "80%";
    var popH = "65%";

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Service User Answers";
    devModelWindowWrapper.openPageWindow("../../html/patients/patientPreventAns.html", profileLbl, popW, popH, true, closePreventCallChart);
}
function closePreventCallChart(evt,returnData){

}
function onClickSearchPatient(){
    var popW = "80%";
    var popH = 600;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search Patient";
    devModelWindowWrapper.openPageWindow("../../html/patients/searchPatient.html", profileLbl, popW, popH, true, closeAddAction);
    showPatientPanel();

}
function closeAddAction(evt,returnData){
    if(returnData && returnData.status == "success"){
//		console.log(returnData);
        var selObj = returnData.selItem;
        if(selObj){
            $("#divPatInfo",parent.document).show();
            $("#divIdMain",parent.document).show();
            $("#divTitleName",parent.document).hide();
            //var htmlStr = "ID:"+selObj.PID+" &nbsp;&nbsp;&nbsp;&nbsp; First Name: "+selObj.FN+" &nbsp;&nbsp;&nbsp;&nbsp; MiddleName: "+selObj.MN+" &nbsp;&nbsp;&nbsp;&nbsp;Last Name: "+selObj.LN+" &nbsp;&nbsp;&nbsp;&nbsp;Date of Birth: "+selObj.DOB
            //$("#spnPtInfo",parent.document).html(htmlStr);
            strPatientId = selObj.PID;
            $("#lblptID",parent.document).html(selObj.PID);
            $("#ptName",parent.document).html(selObj.FN+" "+selObj.MN+" "+selObj.LN);
            $("#lblGender",parent.document).html(selObj.GR);
            $("#ptDOB",parent.document).html(selObj.DOB);
            $("#lblStatus",parent.document).html(selObj.ST);
            $("#lblptID",parent.document).html(selObj.PID);
            $("#lblPtHeight",parent.document).html(selObj.HD);
            $("#lblPtWeight",parent.document).html(selObj.WD);
            $("#rightPanel").css("visibility","visible");
        }

        patentComplePanelInfo();
        getPatientInfomation();
    }else{
        getPatientInfomation();
    }
}

function patentComplePanelInfo(){
    $("#centerPanel").css("visibility","visible");
    if(strPatientId){
        getPatientInfomation();
    }

	/*getMedicationData();
	 getAppointment();
	 getVitals();
	 getExcersize();
	 getDiet();
	 getIllness();
	 getInterests();
	 getHolerReports();
	 getPatientCallChartHistory();
	 getPatientActivies();*/
}
function onClickPatientChart(){
    onClickPatientCall(null);
}
function showPatientPanel(){
	/*if(navigator.userAgent.toLowerCase().indexOf('firefox')==1){
	 $("#pnlPatient").css("top","50px");
	 $("#iframe").css("top","110px");
	 console.log(window.innerHeight);
	 if(window.innerHeight>700 || window.innerHeight>650){
	 $("#pnlPatient").css("top","50px");
	 $("#iframe").css("top","116px");
	 }else if(window.innerHeight>600){
	 $("#pnlPatient").css("top","60px");
	 $("#iframe").css("top","116px");
	 }else{
	 $("#pnlPatient").css("top","50px");
	 $("#iframe").css("top","116px");
	 }
	 }*/
}
function onClickPatientCall(obj){
    Loader.hideLoader();
    var popW = "60%";
    var popH = 400;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Service User Call Chart";
    parentRef.selObj = obj;
    devModelWindowWrapper.openPageWindow("../../html/patients/callPatient.html", profileLbl, popW, popH, false, closeCallPatient);
    showPatientPanel();
}
function closeCallPatient(evt,returnData){
    if(returnData && returnData.status == "success"){
        customAlert.info("Save","Service User Call Chart Created Successfully");
        getPatientCallChartHistory();
    }
}
function onClickSearchChart(){
    var popW = "80%";
    var popH = "65%";

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search Call Chart";
    devModelWindowWrapper.openPageWindow("../../html/patients/searchCallChart.html", profileLbl, popW, popH, true, closeSearchCallChart);
    showPatientPanel();
}
function closeSearchCallChart(evt,returnData){
    if(returnData && returnData.status == "success"){
        var selObj = returnData.selItem;
        if(selObj){
            parentRef.screenType = "search";
            onClickPatientCall(selObj);
        }
    }
}
function onClickNurse(){
	/*var popW = 500;
	 var popH = 650;

	 var profileLbl;
	 var devModelWindowWrapper = new kendoWindowWrapper();
	 profileLbl = "Patient Call";
	 devModelWindowWrapper.openPageWindow("../../../ts/ui/html/patients/callNurse.html", profileLbl, popW, popH, true, closeCallNurse);*/
    onClickPatientCall(null);
    showPatientPanel();
}
function closeCallNurse(evt,returnData){

}
var recType = "1";
function onClickAddIllness(){
    recType = "5";
    showDietTypes("illness","Illness",recType);
}

function onClickAddPatientDiet(){
    recType = "1";
    showDietTypes("Patient Diet"," Patient Diet",recType);
}
function onClickAddPatientExercise(){
    recType = "2";
    showDietTypes("Patient Exercise"," Patient Exercise",recType);
}
function showDietTypes(type,tit,recType){
    var popW = "67%";
    var popH = 450;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = tit;
    parentRef.recordType = recType;
    parentRef.patientId = strPatientId;
    devModelWindowWrapper.openPageWindow("../../html/patients/dietExerciseList.html", profileLbl, popW, popH, true, closeDietExcersizeList);
}
function closeDietExcersizeList(evt,returnData){
    if(returnData && returnData.status == "success"){
        if(recType == "1"){
            customAlert.info("info", "Diet file(s) attached/removed successfully.");
            getAjaxObject(ipAddress+"/patient/diet/"+strPatientId,"GET",onGetDietSuccess,onErrorMedication);
        }else if(recType == "2"){
            customAlert.info("info", "Exercise file(s) attached/removed successfully.");
            getAjaxObject(ipAddress+"/patient/exercise/"+strPatientId,"GET",onGetExcersizeSuccess,onErrorMedication);
        }else if(recType == "5"){
            customAlert.info("info", "illness file(s) attached/removed successfully.");
            getAjaxObject(ipAddress+"/patient/illness/"+strPatientId,"GET",onGetIllnessSuccess,onErrorMedication);
        }
    }else if(returnData && returnData.status == "fail"){
        customAlert.info("error", "Service User file resource(s) error.");
    }
}

function onClickAddPatientActivities(){
    var popW = "67%";
    var popH = 450;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Activities";
    parentRef.patientId = strPatientId;
    devModelWindowWrapper.openPageWindow("../../html/patients/patientActivityList.html", profileLbl, popW, popH, true, closePatientActivitiesList);
}
function closePatientActivitiesList(evt,returnData){
    if(returnData && returnData.status == "success"){
        customAlert.info("info", "Activities attached/removed successfully.");
        getPatientActivies();
    }
}
function onClickAppointment(){
    var popW = "80%";
    var popH = 650;

    var profileLbl = "Appointments"
    parentRef.patientId = strPatientId;
    var devModelWindowWrapper = new kendoWindowWrapper();
    var devUrl = "../../html/patients/createAppointment.html";
    devModelWindowWrapper.openPageWindow(devUrl, profileLbl, popW, popH, true, closeAppointment);
}

function closeAppointment(evt,returnData){
    getAppointment();
}


function onGetDate(DateOfBirth) {
    var dt = new Date(DateOfBirth);
    var strDT = "";
    if (dt) {

        var cntry = sessionStorage.countryName;
        if (cntry.indexOf("India") >= 0) {
            strDT = kendo.toString(dt, "dd/MM/yyyy");
        } else if (cntry.indexOf("United Kingdom") >= 0) {
            strDT = kendo.toString(dt, "dd/MM/yyyy");
        } else {
            strDT = kendo.toString(dt, "MM/dd/yyyy");
        }
    }
    return strDT;
}

function displaySessionErrorPopUp(e, a, s) {
    $.when(kendo.ui.ExtOkCancelDialog.show({
        title: e,
        message: a,
        icon: "k-ext-error"
    })).done(function(e) {
        if (e.button == "OK") {
            $("#ptframe",parent.document).attr("src", "../../html/masters/attachLinkTasks.html");
        }
    })
}

function getFacilityList(){
    getAjaxObject(ipAddress+"/facility/list/?is-active=1&is-deleted=0","GET",getFacilityDataList,onError);
}

var facilitydataArray = [];
function getFacilityDataList(dataObj){
    var dataArray = [];
    if(dataObj){
        if($.isArray(dataObj.response.facility)){
            dataArray = dataObj.response.facility;
        }else{
            dataArray.push(dataObj.response.facility);
        }
    }

    facilitydataArray = dataArray;
    $("#seFacility").append('<option value="' + 0 + '">All</option>');
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
        dataArray[i].Status = "InActive";
        if(dataArray[i].isActive == 1){
            dataArray[i].Status = "Active";
        }


        $("#seFacility").append('<option value="' + dataArray[i].idk + '">' + dataArray[i].name + '</option>');
    }
}
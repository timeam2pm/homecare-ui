var parentRef = null;
var selObj = null;
var previewMedia = null;
var conversationsClient = null;
var activeConversation = null;
var accessToken = "eyJjdHkiOiJ0d2lsaW8tZnBhO3Y9MSIsInR5cCI6IkpXVCIsImFsZyI6IkhTMjU2In0.eyJqdGkiOiJTS2JlZGJjODY2YzhhZjI4ZjI1ZWUxMmJmZmI0ZjliNzNhLTE0NzQ5OTA5NzYiLCJpc3MiOiJTS2JlZGJjODY2YzhhZjI4ZjI1ZWUxMmJmZmI0ZjliNzNhIiwic3ViIjoiQUNiYjg5ZGVmNTMwZTZjNGI1ZTk5ZTY0MTUxOWI0YTg1OCIsImV4cCI6MTQ3NDk5NDYwMywiZ3JhbnRzIjp7InJ0YyI6eyJjb25maWd1cmF0aW9uX3Byb2ZpbGVfc2lkIjoiVlM5OTkyZGRkNTg1ODFmZGM0MDRhMDdlNmJkYThjYmQ4MSJ9LCJpZGVudGl0eSI6Ijg4OCJ9fQ.ajOE6Ew6hhT9rlRextXPZljQt_HEmVKjg13x8dDqAC8";
//var accessToken = "";
$(document).ready(function(){
	parentRef = parent.frames['iframe'].window;
});

$(window).load(function(){
	selObj = parentRef.selObj;
	console.log(selObj);
	init();
	buttonEvents();
});//stage.timeam.com/video/token/111
function init(){
	Loader.showLoader();
	$.ajax({
		  type: "GET",
		  url: "http://stage.timeam.com/video/token/101",
		  data: null,
		  context: this,
			cache: false,
		  success: function( data, statusCode, jqXHR ){
			  console.log(data.token);
			  accessToken = data.token;
			  showLocalPreview();
			  initTwilio();
		  },
		  error: function( jqXHR, textStatus, errorThrown ){
		  },
		  contentType: "application/json",
		});
	if (!navigator.webkitGetUserMedia && !navigator.mozGetUserMedia) {
		  alert('WebRTC is not available in your browser.');
		}
	 // showLocalPreview();
	//  initTwilio();
}
function buttonEvents(){
	$("#btnCancelDet").off("click");
	$("#btnCancelDet").on("click",onClickCancel);
	
	$("#btnCall").off("click");
	$("#btnCall").on("click",onClickCall);
}
function onClickCall(){
	  var inviteTo = "102";//$("#txtPatientName").val();////document.getElementById('invite-to').value;

	    if (activeConversation) {
	      // add a participant
	      activeConversation.invite(inviteTo);
	    } else {
	      // create a conversation
	      var options = {};
	      if (previewMedia) {
	        options.localMedia = previewMedia;
	      }
	      conversationsClient.inviteToConversation(inviteTo, options).then(
	        conversationStarted,
	        function (error) {
	         console.log('Unable to create conversation');
	          console.error('Unable to create conversation', error);
	        }
	      );
	    }
}
function initTwilio(){
	//var accessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImN0eSI6InR3aWxpby1mcGE7dj0xIn0.eyJqdGkiOiJTSzdhZDQwMzUzMTBhYmI3ZGQ3MTU4YTA1ZTI4NDgxYzQ4LTE0NzQ5MDYyOTkiLCJpc3MiOiJTSzdhZDQwMzUzMTBhYmI3ZGQ3MTU4YTA1ZTI4NDgxYzQ4Iiwic3ViIjoiQUMxMTFmOThlNDQ4MmE3NWRhMjU0MDVhYjg2YzgxMmE5NiIsImV4cCI6MTQ3NDkwOTg5OSwiZ3JhbnRzIjp7ImlkZW50aXR5Ijoia3Jpc2huYSIsInJ0YyI6eyJjb25maWd1cmF0aW9uX3Byb2ZpbGVfc2lkIjoiVlMwZDk3Yjg4MTBlYjBhYTEwNjE4MWYzMDQ4MDUwZDM1MyJ9fX0.ymn60qZ_Fa-5yDbiukb1iosFCFmwqm0MVkWlP0uyl7Q";

	// use our AccessToken to generate an AccessManager object
	var accessManager = new Twilio.AccessManager(accessToken);

	// create a Conversations Client and connect to Twilio
	conversationsClient = new Twilio.Conversations.Client(accessManager);
	conversationsClient.listen().then(
	  clientConnected,
	  function (error) {
	   console.log('Could not connect to Twilio: ' + error.message);
	  }
	);
}
function clientConnected() {
	Loader.hideLoader();
	console.log("Connected to Twilio. Listening for incoming Invites as '" + conversationsClient.identity + "'");
	  conversationsClient.on('invite', function (invite) {
		  console.log('Incoming invite from: ' + invite.from);
	    invite.accept().then(conversationStarted);
	  });
	  onClickCall();
	  // bind button to create conversation
	  /*document.getElementById('button-invite').onclick = function () {
	    var inviteTo = document.getElementById('invite-to').value;

	    if (activeConversation) {
	      // add a participant
	      activeConversation.invite(inviteTo);
	    } else {
	      // create a conversation
	      var options = {};
	      if (previewMedia) {
	        options.localMedia = previewMedia;
	      }
	      conversationsClient.inviteToConversation(inviteTo, options).then(
	        conversationStarted,
	        function (error) {
	          log('Unable to create conversation');
	          console.error('Unable to create conversation', error);
	        }
	      );
	    }
	  };*/
	};
	function conversationStarted(conversation) {
		 console.log('In an active Conversation');
		  activeConversation = conversation;
		  // draw local video, if not already previewing
		  if (!previewMedia) {
		    conversation.localMedia.attach('#local-media');
		  }
		  // when a participant joins, draw their video on screen
		  conversation.on('participantConnected', function (participant) {
			  console.log("Participant '" + participant.identity + "' connected");
		    participant.media.attach('#remote-media');
		  });
		  // when a participant disconnects, note in log
		  conversation.on('participantDisconnected', function (participant) {
			  console.log("Participant '" + participant.identity + "' disconnected");
		  });
		  // when the conversation ends, stop capturing local video
		  conversation.on('ended', function (conversation) {
			  console.log("Connected to Twilio. Listening for incoming Invites as '" + conversationsClient.identity + "'");
		    conversation.localMedia.stop();
		    conversation.disconnect();
		    activeConversation = null;
		  });
		};
function showLocalPreview(){
	if (!previewMedia) {
	    previewMedia = new Twilio.Conversations.LocalMedia();
	    Twilio.Conversations.getUserMedia().then(
	      function (mediaStream) {
	        previewMedia.addStream(mediaStream);
	        previewMedia.attach('#local-media');
	      },
	      function (error) {
	        console.error('Unable to access local media', error);
	        log('Unable to access Camera and Microphone');
	      }
	    );
	  };
}

function onClickOK(){
	popupClose(false);
}
function onClickCancel(){
	popupClose(false);
}
function popupClose(st){
	var onCloseData = new Object();
	onCloseData.status = st;
	var windowWrapper = new kendoWindowWrapper();
	windowWrapper.closePageWindow(onCloseData);
}


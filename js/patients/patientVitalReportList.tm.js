var angularUIgridWrapper;
var angularUISelgridWrapper;

var parentRef = null;
var patientId = "";
var patientId = "";
var ptAge  = "";
var ptGender = "";
var ptSD = "";

$(document).ready(function(){
	parentRef = parent.frames['iframe'].window;
	patientId = parentRef.patientId;
	ptAge = parentRef.ptAge;
	ptAge = Number(ptAge);
	ptGender = parentRef.ptGender;
	ptSD = parentRef.ptSD;
	var dataOptions = {
        pagination: false,
        changeCallBack: onChange
	}
	/*angularUIgridWrapper = new AngularUIGridWrapper("dgSelAddPatientVitalRepList", dataOptions);
	angularUIgridWrapper.init();
	buildFileResourceListGrid([]);*/
	
});


$(window).load(function(){
	$(window).resize(adjustHeight);
	onLoaded();
	if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
	init();
    buttonEvents();
	adjustHeight();
}

var recordType = "";
function init(){
	var fmt = getCountryDateFormat();
	startDT = $("#txtStartDate").kendoDatePicker({
        change: startChange,format:fmt
    }).data("kendoDatePicker");
	
    endDT = $("#txtEndDate").kendoDatePicker({
        change: endChange,format:fmt
    }).data("kendoDatePicker");
    
    var dt = new Date();
	    var stDate = new Date();
	    stDate.setDate(stDate.getDate());
	    startDT.value(stDate);
	    
	    var endDate = new Date();
	    endDate.setDate(endDate.getDate());
	    endDT.value(endDate);
	var str1 = "";
	var str2 = "";
	onClickSearch();
	//getAjaxObject(ipAddress+"/homecare/patient-vital-values/?patient-id="+patientId+"&fields=*,provider.*,patient.*,appointment.*,vital.*","GET",getPatientDietList,onError);
}
function startChange() {
    var startDate = startDT.value(),
    endDate = endDT.value();

    if (startDate) {
        startDate = new Date(startDate);
        startDate.setDate(startDate.getDate());
        endDT.min(startDate);
    } else if (endDate) {
        startDT.max(new Date(endDate));
    } else {
        endDate = new Date();
        startDT.max(endDate);
        endDT.min(endDate);
    }
}

function endChange() {
    var endDate = endDT.value(),
    startDate = startDT.value();

    if (endDate) {
        endDate = new Date(endDate);
        endDate.setDate(endDate.getDate());
        startDT.max(endDate);
    } else if (startDate) {
        endDT.min(new Date(startDate));
    } else {
        endDate = new Date();
        startDT.max(endDate);
        endDT.min(endDate);
    }
}
function onError(errorObj){
	console.log(errorObj);
}
var dietArray = [];
var dataArray = [];

function getPatientDietList(dataObj){
	console.log(dataObj);
	 dietArray = [];
	 if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		 if(dataObj && dataObj.response && dataObj.response.patientVitalValues){
				if($.isArray(dataObj.response.patientVitalValues)){
					dietArray = dataObj.response.patientVitalValues;
				}else{
					dietArray.push(dataObj.response.patientVitalValues);
				}
			}
	}
	 if(dietArray.length>0){
		 var txtStartDate = $("#txtStartDate").data("kendoDatePicker");
			var txtEndDate = $("#txtEndDate").data("kendoDatePicker");
				var endDate = new Date(txtEndDate.value());
				var stDate = new Date(txtStartDate.value());
				stDate.setHours(0, 0, 0);
				endDate.setHours(23, 59, 59);
		// var dt = new Date(dietArray[0].dateOfReading);
		// var stDate = new Date(dietArray[0].dateOfReading);
			 var sttDT = kendo.toString(stDate, "yyyy-MM-dd HH:mm:ss");
			 var endDT = kendo.toString(endDate, "yyyy-MM-dd HH:mm:ss");
		 var ptName = "Service User Name<br>";
		 ptName = ptName+dietArray[0].patientLastName+" "+dietArray[0].patientFirstName+" "+dietArray[0].patientMiddleName+"<br>";
		 if(parentRef.ptSD != undefined){
			 ptName = ptName+ptSD+","+dietArray[0].patientGender;
		 }else{
			 ptName = ptName+dietArray[0].patientGender;
		 }

		 
		 var careGiverName = "Care Giver / Staff <br>";
		 careGiverName = careGiverName+dietArray[0].providerLastName+" "+dietArray[0].providerFirstName+" "+dietArray[0].providerMiddleName+"<br>";
		 
		 $("#taNotes").val(dietArray[0].notes);
		 
	 $("#dgSelAddPatientVitalRepList").html("");
		var strTable = '<table class="callPatientTbl patientActivityTbl">';
		strTable = strTable+'<caption class="caption">Vital Report</caption>';
		strTable = strTable+'<thead>';
		strTable = strTable+'<tr>';
		strTable = strTable+'</tr>';
		strTable = strTable+'<tr>';
		strTable = strTable+'<td colspan="3"">Date Time of Reading</td><td colspan="3" class="bgColor caption">'+sttDT+" - "+endDT+'</td>';
		strTable = strTable+'</tr>';
		strTable = strTable+'<tr>';
		strTable = strTable+'<td colspan="3" class="bgColor caption">'+ptName+'</td><td colspan="3" class="bgColor caption">'+careGiverName+'</td>';
		strTable = strTable+'</tr>';
		strTable = strTable+'<tr style="height:20px">';
		strTable = strTable+'</tr>';
		strTable = strTable+'<tr>';
		strTable = strTable+'<th >Name of Vital</th>';
		strTable = strTable+'<th >Result</th>'
		strTable = strTable+'<th style="width:200px" >Unit</th>';
		strTable = strTable+'<th >Flag</th>';
		strTable = strTable+'<th  colspan="2">Observation Date Time</th>';
		strTable = strTable+'</tr>';
		strTable = strTable+'</thead>';
		strTable = strTable+'<tbody>';
		//var vitalDataArray = [];
		for(var i=0;i<dietArray.length;i++){
			var dataItem = dietArray[i];
			if(dataItem){
				//console.log(dataItem);
				var className = "success";
				if(i%2 == 0){
					className = "danger";
				}
				if(i%3 == 0){
					className = "info";
				}
				if(i%3 == 0){
					className = "active";
				}
				//console.log(timiningArray);
				className = getRowColors(i);
				strTable = strTable+'<tr class="'+className+'">';
				var vName = dataItem.vitalAbbreviation+"<br>"+"(Normal: "+dataItem.vitalFemaleRangeFrom+"-"+dataItem.vitalFemaleRangeTo+")";
				
				var appdt = new Date(dataItem.dateOfReading);
				 if(appdt){
					 appdt = kendo.toString(appdt, "yyyy-MM-dd HH:mm:ss");
				 }
				
				strTable = strTable+'<td>'+vName+'</td>';
				strTable = strTable+'<td>'+dataItem.vitalValue+'</td>';
				strTable = strTable+'<td>'+dataItem.vitalUnitOfMeasurement+'</td>';
				strTable = strTable+'<td style="font-weight:bold">'+dataItem.flag+'</td>';
				
				strTable = strTable+'<td>'+appdt+'</td>';
				strTable = strTable+'</tr>';
			}
		}
		strTable = strTable+'</tbody>';
		strTable = strTable+'</table>';
		$("#dgSelAddPatientVitalRepList").append(strTable);
	 }
	//buildFileResourceListGrid(dietArray);
	//getAjaxObject(ipAddress+"/homecare/vitals/","GET",getCountryList,onError);
}
function getRowColors(index) {
    var classes = ['active', 'success', 'info', 'warning', 'danger'];
    if (index % 2 === 0 && index / 2 < classes.length) {
    	return classes[index / 2];
    }
    return "";
}
function getCountryList(dataObj){
	console.log(dataObj);
	dataArray = [];
	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		if($.isArray(dataObj.response.vitals)){
			dataArray = dataObj.response.vitals;
		}else{
			dataArray.push(dataObj.response.vitals);
		}
	}
	var tempDataArray = [];
	for(var i=0;i<dataArray.length;i++){
		if(dataArray[i].id){
			dataArray[i].idk = dataArray[i].id; 
			dataArray[i].Status = "InActive";
			if(dataArray[i].isActive == 1){
				dataArray[i].Status = "Active";
			}
			if(!getFileExist(dataArray[i].id)){
				tempDataArray.push(dataArray[i]);
			}
		}
	}
	buildFileResourceSelListGrid(tempDataArray);
}
function getFileExist(id1){
	var flag = false;
	for(var i=0;i<dietArray.length;i++){
		var dataObj = dietArray[i];
		if(dataObj && dataObj.id == id1){
			flag = true;
			break;
		}
	}
	return flag;
}
function buttonEvents(){
	$("#btnCancelDet").off("click",onClickCancel);
	$("#btnCancelDet").on("click",onClickCancel);
	
	$("#btnSave").off("click");
	$("#btnSave").on("click",onClickSave);
	
	$("#btnSubRight").off("click");
	$("#btnSubRight").on("click",onClickSubRight);
	
	$("#btnSubLeft").off("click");
	$("#btnSubLeft").on("click",onClickSubLeft);
	
	$("#btnSave").off("click");
	$("#btnSave").on("click",onClickPrint);
	
	$("#btnSearch").off("click");
	$("#btnSearch").on("click",onClickSearch);
	
}
var startDT = null;
var endDT = null;

function adjustHeight(){
	var defHeight = 160;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    $("#dgSelAddPatientVitalRepList").height(cmpHeight);
}

var rFrom = "";
var rTo = "";
var child = "";

function buildFileResourceListGrid(dataSource) {
	var gridColumns = [];
	var otoptions = {};
	otoptions.noUnselect = false;
	
	if(ptAge<=10){
		child = "true";
		rFrom = "vitalPediatricRangeFrom";
		rTo = "vitalPediatricRangeTo";
	}else if(ptGender == "Male"){
		rFrom = "vitalMaleRangeFrom";
		rTo = "vitalMaleRangeTo";
	}else{
		rFrom = "vitalFemaleRangeFrom";
		rTo = "vitalFemaleRangeTo";
	}
	
	 gridColumns.push({
	        "title": "Abrevation",
	        "field": "vitalAbbreviation",
		});
	gridColumns.push({
        "title": "UOM",
        "field": "vitalUnitOfMeasurement",
	});
 
	  if(child == "true"){
		    gridColumns.push({
		        "title": "Pediatric Range From",
		        "field": "vitalPediatricRangeFrom",
			});
		    gridColumns.push({
		        "title": "Pediatric Range To",
		        "field": "vitalPediatricRangeTo",
			});
	    }else if(ptGender == "Male"){
	    	gridColumns.push({
	            "title": "Male Range From",
	            "field": "vitalMaleRangeFrom",
	    	});
	        gridColumns.push({
	            "title": "Male Range To",
	            "field": "vitalMaleRangeTo",
	    	});
	    }else{
	    	 gridColumns.push({
	    	        "title": "Female Range From",
	    	        "field": "vitalFemaleRangeFrom",
	    		});
	    	    gridColumns.push({
	    	        "title": "Female Range To",
	    	        "field": "vitalFemaleRangeTo",
	    		});
	    }
  
    gridColumns.push({
        "title": "Value",
        "field": "vitalValue",
	});
   
   angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions); 
	adjustHeight();
	//onIndividualRowClick();
}
var prevSelectedItem =[];
function onChange(){
	
}
function buildFileResourceSelListGrid(dataSource) {
	var gridColumns = [];
	var otoptions = {};
	otoptions.noUnselect = false;
	 gridColumns.push({
	        "title": "Select",
	        "field": "SEL",
	        "cellTemplate": showCheckBoxTemplate(),
	        "width":"15%"
		});
	 gridColumns.push({
	        "title": "Description",
	        "field": "description",
	         "width":"67%"
		});
	    gridColumns.push({
	        "title": "Abrevation",
	        "field": "abbreviation",
		});
   angularUISelgridWrapper.creategrid(dataSource, gridColumns,otoptions); 
	adjustHeight();
	//onIndividualRowClick();
}
var prevSelectedItem =[];
function onChange1(){
	
}

function showCheckBoxTemplate(){
	var node = '<div style="text-align:center;padding-top: 0px;border:1px solid #000"><input type="text" class="check1" id="chkbox" ng-model="row.entity.SEL" style="width:100%;height:20px;margin:0px;"></input></div>';
	return node;
}
function onSelect(evt){
	console.log(evt);
}

function onClickSubRight(){
//	alert("right");
	 var selGridData = angularUISelgridWrapper.getAllRows();
	 var selList = [];
	    for (var i = 0; i < selGridData.length; i++) {
	        var dataRow = selGridData[i].entity;
	        if(dataRow.SEL){
	        	angularUISelgridWrapper.deleteItem(dataRow);
	        	//dataRow.SEL = false;
	        	angularUIgridWrapper.insert(dataRow);
	        }
	    }  
}
function onClickSubLeft(){
	//alert("click");
	 var selGridData = angularUIgridWrapper.getAllRows();
	 var selList = [];
	    for (var i = 0; i < selGridData.length; i++) {
	        var dataRow = selGridData[i].entity;
	        if(dataRow.SEL){
	        	angularUIgridWrapper.deleteItem(dataRow);
	        	angularUISelgridWrapper.insert(dataRow);
	        }
	    }  
}
function onClickSave(){
	var rows = angularUIgridWrapper.getAllRows();
	//console.log(rows);
	var dArray = [];
	for(var i=0;i<rows.length;i++){
		var rowObj = rows[i].entity;
		if(rowObj){
				var obj = {};
				obj.createdBy = Number(sessionStorage.userId);
				obj.isActive = 1;
				obj.isDeleted = 0;
				obj.vitalId = rowObj.idk;
				obj.vitalValue = rowObj.SEL;
				obj.appointmentId = 1;
				obj.dateOfReading = new Date().getTime();
				obj.patientId = patientId;
				dArray.push(obj);
	 }
	}
	
		var dataObj = {};
	dataObj = dArray;
	var dataUrl = ipAddress+"/homecare/patient-vital-values/batch/";
	createAjaxObject(dataUrl,dataObj,"POST",onCreate,onError);
}
function onClickSearch(){
	$("#dgSelAddPatientVitalRepList").html("");
	var txtStartDate = $("#txtStartDate").data("kendoDatePicker");
	var txtEndDate = $("#txtEndDate").data("kendoDatePicker");
	if(txtStartDate.value() && txtEndDate.value()){
		var endDate = new Date(txtEndDate.value());
		var stDate = new Date(txtStartDate.value());
		stDate.setHours(0, 0, 0);
		endDate.setHours(23, 59, 59);
		var edTime = endDate.getTime();
		
		var patientListURL = ipAddress+"/homecare/patient-vital-values/?patient-id="+patientId+"&dateOfReading=:bt:"+stDate.getTime()+","+edTime+"&fields=*,provider.*,patient.*,appointment.*,vital.*";
		
		getAjaxObject(patientListURL,"GET",getPatientDietList,onError);
	}
}
function onCreate(dataObj){
	console.log(dataObj);
	var status = "fail";
	var flag = true;
	if(dataObj){
		if(dataObj && dataObj.response){
			if(dataObj.response.status){
				if(dataObj.response.status.code == "1"){
					status = "success";
					customAlert.info("Info", "Vital values are created successfully");
				}else{
					flag = false;
					customAlert.info("error", dataObj.response.status.message);
				}
			}
		}
	}
	if(flag){
		//setTimeout(function)
		var obj = {};
		obj.status = status;
		//popupClose(obj);
	}
}
function onClickPrint(){
	//window.print();
	 var win = window.open('', '', 'width=1000, height=500, scrollbars=yes, resizable=yes');
	    var doc = win.document.open();
	    var htmlStart =
	        '<!DOCTYPE html>' +
	        '<html>' +
	        '<head>' +
	        '<meta charset="utf-8" />' +
	        '<title>Print grid data</title>' +
	        '<link href="../../css/patient/patientList.css" rel="stylesheet" /> ' +
	        '<link href="../../css/libs/css/bootstrap.min.css" rel="stylesheet" /> ' +
	        '<style>' +
	        'html { font: 11pt sans-serif; }' +
	        '.caption{font-size:18px;font-weight:bold;border:1px solid #333;border-left:0px;border-right:0px;border-top:0px}' +
	        '.bgColor{background:#FFF;}' +
	        '.k-grid-toolbar, .k-grid-pager > .k-link { display: none; }' +
	        '.k-link { text-decoration: none !important; }' +
	        'div.k-grid-header .k-header, div.k-grid-footer { padding-right: 0px !important; background-color: #F5F5F5; font-weight:bold !important;}' +
	        '.k-alt { background-color: #F5F5F5 !important; }' +
	        '.k-print-header { background-color: #D7D7D7 !important; }' +
	        'td { border:1px solid #000 }' +
	        'tr[role="row"] { height:50px; }' +
	        '<style>' +
	        'html { font: 11pt sans-serif; }' +
	        '</style>' +
	        '</head>' +
	        '<body style="overflow-y:scroll !important;overflow-x:scroll !important;">',
	        htmlEnd =
	        '</body>' +
	        '</html>';

	    var strHtml = $("#dgSelAddPatientVitalRepList").html();
	    console.log(strHtml);
	    doc.write(htmlStart+strHtml+htmlEnd);
	    
	    doc.close();
	    setTimeout(function() {
	        win.print();
	    }, 1000);
}
function popupClose(st){
	var onCloseData = new Object();
	onCloseData = st;
	var windowWrapper = new kendoWindowWrapper();
	windowWrapper.closePageWindow(onCloseData);
}
function onClickCancel(){
	var obj = {};
	var windowWrapper = new kendoWindowWrapper();
	windowWrapper.closePageWindow(obj);
}

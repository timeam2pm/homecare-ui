var angularUIgridWrapper = AngularUIGridWrapper();

var rangePicker = null;
$(document).ready(function(){
});

$(window).load(function(){
	rangePicker = new kendoRangePicker();
	rangePicker.CHOOSE_RANGE_LABEL = "";
	rangePicker.createPicker("datePicker",true,"true");
	 var combo = $("#dateRangesdatePicker").data("kendoComboBox");
	 if(combo){
		 combo.select(1);
		 $("#dateRangesdatePicker").trigger("change");
	 }
	onMessagesLoaded();
});

	function onMessagesLoaded() {
		var dataOptions = {
	        pagination: false,
	        paginationPageSize: 500,
			changeCallBack: onChange
	    }

		angularUIgridWrapper = new AngularUIGridWrapper("dgridCallChart", dataOptions);
	    angularUIgridWrapper.init();
	    
	    var dataArray = [];
	    var dataObj = {};
	    
	    dataObj.PID = "101";
	    dataObj.PN = "John Z Doe";
	    dataObj.CD = "09/21/2016";
	    dataObj.BT = "10.50AM";
	    dataObj.ET = "10.55AM";
	    dataObj.CDT = "5mins";
	    
	  //  dataArray.push(dataObj);
	    
	    buildCallChartGrid(dataArray);
	    buttonEvents();
	    var dataUrl = ipAddress+"/patient/calldata/101";
	    getAjaxObject(dataUrl,"GET",onGetCallData,errorFunction);
}
function buttonEvents(){
	$("#btnSave").off("click");
	$("#btnSave").on("click",onClickOK);
	
	$("#btnCancelDet").off("click");
	$("#btnCancelDet").on("click",onClickCancel);
}
$(window).resize(adjustHeight);
function adjustHeight(){
	var defHeight = 300;
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 300;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    try{angularUIgridWrapper.adjustGridHeight(cmpHeight);}catch(e){};

}
function getAjaxObject(dataUrl,method,successFunction,errorFunction){
	Loader.showLoader();
	$.ajax({
		  type: method,
		  url: dataUrl,
		  data: null,
		  context: this,
			cache: false,
		  success: function( data, statusCode, jqXHR ){
			  Loader.hideLoader();
			  successFunction(data);
		  },
		  error: function( jqXHR, textStatus, errorThrown ){
			  Loader.hideLoader();
			  errorFunction(jqXHR);
		  },
		  contentType: "application/json",
		});

}
function onGetCallData(dataObj){
	console.log(dataObj);
	var dataArray = [];
	if(dataObj){
		if($.isArray(dataObj)){
			dataArray = dataObj;
		}else{
			dataArray.push(dataObj);
		}
	}
	var tempArray = [];
	for(var i=0;i<dataArray.length;i++){
		var dataItem = dataArray[i];
		if(dataItem){
			dataItem.dateOfCall = kendo.toString(new Date(dataItem.dateOfCall),"MM-dd-yyyy");
			tempArray.push(dataItem);
		}
	}
	 buildCallChartGrid(dataArray);
}
function errorFunction(errObj){
	console.log(errObj);
}
function buildCallChartGrid(dataSource) {
    var gridColumns = [];
	var otoptions = {};
	otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Patient Id",
        "field": "patientId",
        "enableColumnMenu": false,
    });
    gridColumns.push({
        "title": "Call Date",
        "field": "dateOfCall",
        "enableColumnMenu": false,
    });
	gridColumns.push({
        "title": "Begin Time",
        "field": "beginTime",
        "enableColumnMenu": false,
    });
	gridColumns.push({
        "title": "End Time",
        "field": "endTime",
        "enableColumnMenu": false,
    });
	gridColumns.push({
        "title": "Call Duration",
        "field": "callDuration",
        "enableColumnMenu": false,
    });
	angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
	adjustHeight();
}
function onChange(){
}
function onClickOK(){
	//popupClose(true);
	setTimeout(function(){
		 var selectedItems = angularUIgridWrapper.getSelectedRows();
		 console.log(selectedItems);
		 if(selectedItems && selectedItems.length>0){
			 var obj = {};
			 obj.selItem = selectedItems[0];
			 obj.status = "success";
				var windowWrapper = new kendoWindowWrapper();
				windowWrapper.closePageWindow(obj);
		 }
	})
}
function onClickCancel(){
	popupClose(false);
}
function popupClose(st){
	var onCloseData = new Object();
	onCloseData.status = st;
	var windowWrapper = new kendoWindowWrapper();
	windowWrapper.closePageWindow(onCloseData);
}

function handleGetError(e) {
	var errLbl = getLocaleStringWithDefault(DC_UI_COMM, 'ERROR_MESSAGE', "Error");
	if(e && e.RestData && e.RestData.Description){
		window.top.displayErrorPopUp(errLbl,e.RestData.Description);
	}
}
function handleGetHttpError(e) {
   var errLbl = getLocaleStringWithDefault(DC_UI_COMM, 'ERROR_MESSAGE', "Error");
   var errDesc = getLocaleStringWithDefault(DC_UI_COMM, 'HTTP_ERROR', 'Http Error');
	// window.top.displayErrorPopUp(errLbl,errDesc);
}
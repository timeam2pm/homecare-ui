var parentRef = null;
var selObj = null;
//var ipAddress = "http://192.168.0.106:8080";
//var ipAddress = "http://stage.timeam.com";
$(document).ready(function(){
	parentRef = parent.frames['iframe'].window;
});

$(window).load(function(){
	init();
	buttonEvents();
});
function init(){
	var video = document.getElementById('videoCtrl');
	var source = document.createElement('source');
	var sType = parentRef.sType;
	var extension = "";
	var fileType = "";
	var urlPath = "";
	var videoUrl = "";
	fileType = parentRef.fileType;
	extension =  parentRef.extension;
	if(sType == "diet"){
		$("#videoCtrl").show();
		urlPath = ipAddress+"/file-resource/download/";
		videoUrl = urlPath+parentRef.dietId;
	}else if(sType == "excersize"){
		$("#videoCtrl").show();
		urlPath = ipAddress+"/file-resource/download/";
		videoUrl = urlPath+parentRef.dietId;
	}else if(sType == "illness"){
		urlPath = parentRef.illUrlPath;
		videoUrl = urlPath;
	}

	if(sType == "illness"){
		if(fileType.toLowerCase() == "file"){
			$("#iVideoFrame").hide();
			if(extension.toLowerCase() == "png" || extension.toLowerCase() == "jpeg" || extension.toLowerCase() == "jpg"){
				$("#iframePdf").css("display","none");
				$("#imgPhoto").show();
				$("#imgPhoto").attr('src', videoUrl);
			}
			else if(extension.toLowerCase() == "pdf"){
				$("#iframePdf").show();
				$("#iframePdf").attr('src', videoUrl);
				// $("#iVideoFrame").show();
				// $("#iVideoFrame").attr('src',videoUrl+"&embedded=true") ;
				$("#imgPhoto").css("display","none");
			}
		}
		else{
			$("#iVideoFrame").show();
			videoUrl = videoUrl+"?access_token="+sessionStorage.access_token;
			$("#iVideoFrame").attr('src',videoUrl) ;
		}
	}else{
		source.setAttribute('src', videoUrl);//'http://stage.timeam.com/patient/diet/download/6');
		video.appendChild(source);
		video.play();
	}
	
	
}
/*<video controls="controls" 
    class="video-stream" 
    x-webkit-airplay="allow" 
    data-youtube-id="N9oxmRT2YWw"  
src="http://www.youtube.com/watch?v=OmxT8a9RWbE"
    ></video>*/
function buttonEvents(){
	$("#btnCancelDet").off("click");
	$("#btnCancelDet").on("click",onClickCancel);
}
function onClickOK(){
	popupClose(false);
}
function onClickCancel(){
	popupClose(false);
}
function popupClose(st){
	var onCloseData = new Object();
	onCloseData.status = st;
	var windowWrapper = new kendoWindowWrapper();
	windowWrapper.closePageWindow(onCloseData);
}

function handleGetError(e) {
	var errLbl = getLocaleStringWithDefault(DC_UI_COMM, 'ERROR_MESSAGE', "Error");
	if(e && e.RestData && e.RestData.Description){
		window.top.displayErrorPopUp(errLbl,e.RestData.Description);
	}
}
function handleGetHttpError(e) {
   var errLbl = getLocaleStringWithDefault(DC_UI_COMM, 'ERROR_MESSAGE', "Error");
   var errDesc = getLocaleStringWithDefault(DC_UI_COMM, 'HTTP_ERROR', 'Http Error');
	// window.top.displayErrorPopUp(errLbl,errDesc);
}
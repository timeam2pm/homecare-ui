var parentRef = null;
var selArray = []
$(document).ready(function(){
    parentRef = parent.frames['iframe'].window;
    selArray = parentRef.selArray;
});

var cntry = "";
var dtFMT = "dd/MM/yyyy";

$(window).load(function(){
    init();
    buttonEvents();
});
function init(){
    cntry = sessionStorage.countryName;
    if(cntry.indexOf("India")>=0){
        dtFMT = INDIA_DATE_FMT;
    }else  if(cntry.indexOf("United Kingdom")>=0){
        dtFMT = ENG_DATE_FMT;
    }else  if(cntry.indexOf("United State")>=0){
        dtFMT = US_DATE_FMT;
    }
    $("#txtStartTime").kendoTimePicker({interval: 15,format:dtFMT});
    $("#txtEndTime").kendoTimePicker({interval: 15,format:dtFMT});

    if(selArray && selArray.length == 1){
        var std = $("#txtStartTime").data("kendoTimePicker");
        if(std){
            std.value(selArray[0].ST);
        }
        var etd = $("#txtEndTime").data("kendoTimePicker");
        if(etd){
            etd.value(selArray[0].ET);
        }
    }
}
function buttonEvents(){
    $("#btnSave").off("click");
    $("#btnSave").on("click",onClickSave);

    $("#btnCancelDet").off("click");
    $("#btnCancelDet").on("click",onClickCancel);
}

function onClickSave(){

    var st = $("#txtStartTime").data("kendoTimePicker");
    var et = $("#txtEndTime").data("kendoTimePicker");

    if(st.value() && et.value()){

        var stValue = new Date(st.value());
        var etValue = new Date(et.value());
        var stv = stValue.getTime();
        var etv = etValue.getTime();

        if(etv>stv){
            var arr = [];
            for(var i=0;i<selArray.length;i++){
                var reqObj = {};
                reqObj.id = selArray[i].idk;
                reqObj.createdBy = 101;
                reqObj.modifiedBy = 101;
                reqObj.isActive = 1;
                reqObj.isDeleted = 0;
                reqObj.providerId = parentRef.providerId;;
                reqObj.facilityId = parentRef.facilityId;

                var st = $("#txtStartTime").val();
                var et = $("#txtEndTime").val();

                var sd = selArray[i].SD;

                var std = sd+" "+st;
                var etd = sd+" "+et;

                console.log(std+","+etd);

                var sDate = new Date(std);
                var eDate = new Date(etd);

                reqObj.startDateTime = sDate.getTime();
                reqObj.endDateTime = eDate.getTime();
                arr.push(reqObj);
            }
            var dataUrl = "";
            dataUrl = ipAddress+"/appointment-block/update";
            createAjaxObject(dataUrl,arr,"POST",onCreate,onError);
        }else{
            customAlert.error("Error", "End Date always greater than the Start Date");
        }

    }else{
        customAlert.error("Error", "Enter Proper timings");
    }
}
function onCreate(dataObj){
    if(dataObj.response.status.code == "1"){
        popupClose("success");
    }else{
        if(dataObj.response.status.message){
            customAlert.error("Error", dataObj.response.status.message);
        }else{
            customAlert.error("Error", "Appointment block update failed");
        }

    }
}
function onClickCancel(){
    popupClose("false");
}
function popupClose(st){
    var onCloseData = new Object();
    onCloseData.status = st;
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(onCloseData);
}

function onError(errorObj){
    console.log(errorObj);
}
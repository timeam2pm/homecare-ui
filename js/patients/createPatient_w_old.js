var parentRef = null;
var operation = "";
var selItem = null;
var ADD = "add";
var UPDATE = "edit";
var VIEW = "view";
var DELETE = "delete";
var patientId = "";
var statusArr = [{Key:'Active',Value:'Active'},{Key:'InActive',Value:'InActive'}];
var typesArr = [{Key:'100',Value:'Doctor'},{Key:'200',Value:'Nurse'},{Key:'201',Value:'Caregiver'}];

var patientInfoObject = null;
var commId = "";

$(document).ready(function(){
	parentRef = parent.frames['iframe'].window;
	var pnlHeight = window.innerHeight;
	var imgHeight = pnlHeight-90;
	$("#divTop").height(imgHeight);
	
	
	
	
});
/*$(function(){
	$("#dtDOB").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "-150:+150"
        });
});*/

function adjustHeight(){
	var defHeight = 45;
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 40;
    }
}

$(window).load(function(){
	if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
	init();
});

function init(){
	allowAlphaNumeric("txtExtID1");
	allowAlphaNumeric("txtExtID2");
	allowAlphaNumeric("txtWeight");
	allowDecimals("txtHeight");
	allowAlphabets("txtNN");
	allowAlphabets("txtFN");
	allowAlphabets("txtMN");
	allowAlphabets("txtLN");
	allowNumerics("txtSSN");
	allowNumerics("txtAge");
	//allowAlphaNumeric("txtAdd1");
	//allowAlphaNumeric("txtAdd2");
//	allowPhoneNumber("txtHPhone");
	allowPhoneNumber("txtWP");
	allowPhoneNumber("txtExtension");
	allowPhoneNumber("txtCell");
	validateEmail("txtEmail");
	
	setDataForSelection(typesArr, "cmbType", onPtChange, ["Value", "Key"], 0, "");
	
	
	
	var text_ssn = $('#txtSSN').val();
    var hide_ssn = '';
    $("#txtSSN").attr('maxlength','9');
    if($('#txtSSN').val().length > 4){
     //   $('#txtSSN').val($('#txtSSN').val().replace(/^\d{5}/, '*****'));
    }
    $('#txtSSN').on('focus', function(){
        //$('#txtSSN').val(text_ssn);
    });
    $('#txtSSN').on('blur', function(){
      //  text_ssn = $('#txtSSN').val();
        
        if($('#txtSSN').val().length > 4){
          //  $('#txtSSN').val($('#txtSSN').val().replace(/^\d{5}/, '(***)_**_'));
        }
        
        hide_ssn = $('#txtSSN').val();
    });
    $('#EyeImg').on('click', function(){
    	
    	$('img',this).toggle(1000);
       if($('#txtSSN').val() == text_ssn){
           // $('#txtSSN').val(hide_ssn);
        }
        else{
            //$('#txtSSN').val(text_ssn);
        }
    });
	
    
   /* var text_hp = $('#txtHPhone').val();
    var hide_hp = '';
    //$("#txtHPhone").attr('maxlength','10');
    if($('#txtHPhone').val().length > 4){
    
        //$('#txtHPhone').val($('#txtHPhone').val().replace(/^\d{6}/, '******'));
    }*/
    
    // Listen for Focus on any three fields
    /*$('#txtHPhone').on('focus', function(){
        //$('#txtHPhone').val(text_hp);
    });*/
    
    // Listen for Blur on any three fields
   
    /*$('#txtHPhone').on('blur', function(){
        //text_hp = $('#txtHPhone').val();
        
        if($('#txtHPhone').val().length > 4){
            //$('#txtHPhone').val($('#txtHPhone').val().replace(/^\d{6}/, '(***)_***_'));
           
        }
        
        //hide_hp = $('#txtHPhone').val();
    });*/
    
    // Show/Hide SSN click
   /* $('#EyeImg').on('click', function(){
        if($('#txtHPhone').val() == text_hp){
           // $('#txtHPhone').val(hide_hp);
        }
        else{
            //$('#txtHPhone').val(text_hp);
        }
    });*/
    
  
 
	/*$("#EyeImg").click(function(){
        $('img',this).toggle(1000);
       
    });*/
	/*$("#txtWP").kendoMaskedTextBox({
        mask: "(999) 999-9999"
    });
	$("#txtExtension").kendoMaskedTextBox({
        mask: "(999) 999-9999"
    });
	$("#txtCell").kendoMaskedTextBox({
        mask: "(999) 999-9999"
    });*/
	
	setHomePhoneMask();
	/*setWorkPhoneMask();
	setExtensionMask();
	setCellPhoneMask();*/
	operation = parentRef.operation;
	patientId = parentRef.patientId;
	//selItem = parentRef.selItem;
	$("#cmbPrefix").kendoComboBox();
	$("#cmbStatus").kendoComboBox();
	$("#cmbGender").kendoComboBox();
	$("#cmbZip1").kendoComboBox();
	$("#cmbSMS").kendoComboBox();
	$("#cmbEthicity").kendoComboBox();
	$("#cmbRace").kendoComboBox();
	$("#cmbLan").kendoComboBox();
    //$("#dtDOB").kendoDatePicker({change: onDOBChange});
	$( "#dtDOB" ).datepicker({
		changeMonth: true,
        changeYear: true,
        yearRange: "-200:+200",
        onSelect: function() {
            var dob = document.getElementById("dtDOB").value;
             var DOB = new Date(dob);
             var today = new Date();
             var age = today.getTime() - DOB.getTime();
             age = Math.floor(age / (1000 * 60 * 60 * 24 * 365.25));

             document.getElementById('txtAge').value = age;
         }
        //change: onDOBChange
		});
	
	getZip();
	buttonEvents();
}
/*function validateEmail(sEmail) {
    console.log(sEmail);
    var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    if (filter.test(sEmail)) {
    return true;
    }
    else {
    return false;
    }
}
function Validate(event) {
        var regex = new RegExp("^[A-Za-z0-9? ]");
        var key = String.fromCharCode(event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            event.preventDefault();
            return false;
        }
    }  
function Validate1(event) {
        var regex = new RegExp("^[0-9.? ]");
        var key = String.fromCharCode(event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            event.preventDefault();
            return false;
        }
}
function Validate2(event) {
        var regex = new RegExp("^[0-9? ]");
        var key = String.fromCharCode(event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            event.preventDefault();
            return false;
        }
}     
function onlyAlphabets(e, t) {
            try {
                if (window.event) {
                    var charCode = window.event.keyCode;
                }
                else if (e) {
                    var charCode = e.which;
                }
                else { return true; }
                if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))
                    return true;
                else
                    return false;
            }
            catch (err) {
                alert(err.Description);
            }
}*/


/*function setAge(e){
	/*var dtItem = $("#dtDOB").data("datepicker");
	var strDate = "";
	$("#txtAge").val("");
	if(dtItem && dtItem.value()){
		console.log(dtItem.value());
		var strAge = getAge(dtItem.value());
		$("#txtAge").val(strAge);
		strDate = toString(dtItem.value(),"yyyy-MM-dd");
		//strDate = kendo.toString(dtItem.value(),"yyyy-MM-dd");
	}
	var bday = new Date(Date.parse(e.target.value));
	  var today = new Date()
	  
	  document.getElementsById('age')[0].value = today.getFullYear() - bday.getFullYear();
}*/
function onDOBChange(){
	var dtDOB = $("#dtDOB").data("kendoDatePicker");
	if(dtDOB){
		var dt  = dtDOB.value();
		var strAge = getAge(dt);
		$("#txtAge").val(strAge);
	}
	
}
function onStatusChange(){
	var cmbStatus = $("#cmbStatus").data("kendoComboBox");
	if(cmbStatus && cmbStatus.selectedIndex<0){
		cmbStatus.select(0);
	}
}
function buttonEvents(){
	$("#btnCancel").off("click",onClickCancel);
	$("#btnCancel").on("click",onClickCancel);
	
	$("#btnSave").off("click",onClickSave);
	$("#btnSave").on("click",onClickSave);
	
	$("#btnSearch").off("click",onClickSearch);
	$("#btnSearch").on("click",onClickSearch);
	
	$("#btnReset").off("click",onClickReset);
	$("#btnReset").on("click",onClickReset);

	$("#btnZipSearch").off("click");
	$("#btnZipSearch").on("click",onClickZipSearch);
	
	$("#btnBrowse").off("click",onClickBrowse);
	$("#btnBrowse").on("click",onClickBrowse);
	
	$("#fileElem").off("change", onSelectionFiles);
	$("#fileElem").on("change", onSelectionFiles);
	
	$("#btnStartVideo").off("click",onClickStartVideo);
	$("#btnStartVideo").on("click",onClickStartVideo);
	
	$("#btnTakePhoto").off("click",onClickTakePhoto);
	$("#btnTakePhoto").on("click",onClickTakePhoto);
	
	$("#btnUpload").off("click",onClickUploadPhoto);
	$("#btnUpload").on("click",onClickUploadPhoto);
	
	$('#txtSSN1').off('keydown keyup mousedown mouseup');
	$('#txtSSN1').on('keydown keyup mousedown mouseup', function() {
		var result = showSSNNumver(this.value);
		$("#txtSSN").val(result); //spits the value into the input
   });
/*	$('#txtHPhone1').off('keydown keyup mousedown mouseup');
	$('#txtHPhone1').on('keydown keyup mousedown mouseup', function() {
		var result = showHomeNumver(this.value);
		$("#txtHPhone").val(result); //spits the value into the input
   });*/
}

function onClickTakePhoto(){
	$("#video").hide();
	$("#canvas").show();
	var canvas = document.getElementById('canvas');
	var context = canvas.getContext('2d');
	var video = document.getElementById('video');
	context.drawImage(video, 0, 0, 200, 150);
	
	 var dataURL = canvas.toDataURL("image/png");
	 
	 var reqUrl = ipAddress+"/upload/patient/photo/stream/?patient-id="+patientId+"&photo-ext=png";
	// var xmlhttp = new XMLHttpRequest();
     //xmlhttp.open(reqUrl, true);
     //xmlhttp.open("POST", "/ROOT/upload/patient/photo/?patient-id=101&photo-ext=png", true);
   //  xmlhttp.send(dataURL);
	/* $.ajax({
		  type: "POST",
		  url: reqUrl,
		  data: { 
			  base64: dataURL
		  }
		}).done(function(o) {
		  console.log('saved'); 
		});*/
}
function onClickStartVideo(){
	isBrowseFlag = false;
	$("#video").show();
	$("#btnTakePhoto").show();
	//$("#canvas").show();
	$("#imgPhoto").hide();
	
	var video = document.getElementById('video');

	// Get access to the camera!
	if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
	    // Not adding `{ audio: true }` since we only want video now
	    navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
	        video.src = window.URL.createObjectURL(stream);
	        video.play();
	    });
	}
}
var isBrowseFlag = false;
function onClickBrowse(e){
	isBrowseFlag = true;
	$("#video").hide();
	$("#canvas").hide();
	$("#imgPhoto").show();
	console.log(e);
	if(e.currentTarget && e.currentTarget.id){
		var btnId = e.currentTarget.id;
		var fileTagId = ("fileElem"+btnId);
		$("#fileElem").click();
	}
}

function onClickUploadPhoto(){
	if(patientId != ""){
		if(isBrowseFlag){
			var reqUrl = ipAddress+"/upload/patient/photo/?patient-id="+patientId+"&photo-ext=jpg";
			var formData = new FormData();
			formData.append("desc", fileName);
			formData.append("file",files[0]);
			
			Loader.showLoader();
			 $.ajax({
		            url: reqUrl,
		            type: 'POST',
		            data: formData,
		            processData: false, 
		            contentType:false,// "multipart/form-data", 
		            success: function(data, textStatus, jqXHR){	
		            	if(typeof data.error === 'undefined'){
							Loader.hideLoader();
		            		submitForm(data);
		            	}else{
							Loader.hideLoader();
		            	}
		            },
		            error: function(jqXHR, textStatus, errorThrown){
						Loader.hideLoader();
		            }
		        });
		}else{
			var canvas = document.getElementById('canvas');
			//var context = canvas.getContext('2d');
			//var video = document.getElementById('video');
			//context.drawImage(video, 0, 0, 200, 150);
			
			 var dataURL = canvas.toDataURL("image/png");
			 
			 var reqUrl = ipAddress+"/upload/patient/photo/stream/?patient-id="+patientId+"&photo-ext=png";
			 var xmlhttp = new XMLHttpRequest();
		     xmlhttp.open("POST",reqUrl, true);
		     //xmlhttp.open("POST", "/ROOT/upload/patient/photo/?patient-id=101&photo-ext=png", true);
		     xmlhttp.send(dataURL);
		}
	}
}
var files = null;
var fileName = "";
function onSelectionFiles(event){
	console.log(event);
	files = event.target.files;
	fileName = "";
	//$('#txtFU').val("");
	if(files){
		if(files.length > 0){
			fileName = files[0].name;
			if(patientId != ""){
				 var oFReader = new FileReader();
				    oFReader.readAsDataURL(files[0]);
					oFReader.onload = function(oFREvent) {
					document.getElementById("imgPhoto").src = oFREvent.target.result;
			}
		}
		}
	}
}
function submitForm(dataObj){
	//console.log(dataObj);
	customAlert.error("Info","Image uploaded successfully");
}
function showSSNNumver(num){
	 var res = num, //grabs the value
		 len = res.length, //grabs the length
		 stars = len>0?len>1?len>2?len>3?len>4?'XXX-XX-':'XXX-X':'XXX-':'XX':'X':'', //this provides the masking and formatting
		result = stars+res.substring(5); //this is the result
		 return result;
}
function showHomeNumver(num){
	 var res = num, //grabs the value
		 len = res.length, //grabs the length
		 stars = len>0?len>1?len>2?'(XXX)___-___':'(XX_)___-___':'(X__)___-___':'(___)___-___', //this provides the masking and formatting
		result = stars+res.substring(7); //this is the result
		 return result;
}
function onClickZipSearch(){
	var popW = 600;
    var popH = 500;
    
    parentRef.searchZip = true;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Search Zip";
    devModelWindowWrapper.openPageWindow("../../html/masters/zipList.html", profileLbl, popW, popH, true, onCloseSearchZipAction);
}
var zipSelItem = null;
function onCloseSearchZipAction(evt,returnData){
	console.log(returnData);
	if(returnData && returnData.status == "success"){
		//console.log();
		$("#cmbZip").val("");
		$("#txtZip4").val("");
		$("#txtState").val("");
		$("#txtCountry").val("");
		$("#txtCity").val("");
		var selItem = returnData.selItem;
		if(selItem){
			zipSelItem = selItem;
			if(zipSelItem){
				cityId = zipSelItem.idk;
				if(zipSelItem.zip){
					$("#cmbZip").val(zipSelItem.zip);
				}
				if(zipSelItem.zipFour){
					$("#txtZip4").val(zipSelItem.zipFour);
				}
				if(zipSelItem.state){
					$("#txtState").val(zipSelItem.state);
				}
				if(zipSelItem.country){
					$("#txtCountry").val(zipSelItem.country);
				}
				if(zipSelItem.city){
					$("#txtCity").val(zipSelItem.city);
				}
			}
		}
	}
}
function getZip(){
	getAjaxObject(ipAddress+"/city/list/","GET",getZipList,onError);
}
function getZipList(dataObj){
	//var dArray = getTableListArray(dataObj);
	var dArray = [];
	if(dataObj && dataObj.response && dataObj.response.cityExt){
		if($.isArray(dataObj.response.cityext)){
			dArray = dataObj.response.cityext;
		}else{
			dArray.push(dataObj.response.cityext);
		}
	}
	if(dArray && dArray.length>0){
		//setDataForSelection(dArray, "cmbZip", onZipChange, ["zip", "id"], 0, "");
		//onZipChange();
	}
	getPrefix();
	
}

function onErrorMedication(){
	
}
function getPrefix(){
	getAjaxObject(ipAddress+"/Prefix/list/","GET",getCodeTableValueList,onError);
}
function onComboChange(cmbId){
	var cmb = $("#"+cmbId).data("kendoComboBox");
	if(cmb && cmb.selectedIndex<0){
		cmb.select(0);
	}
}
function onGethbtPatientSuccess(){
	
}
function onPrefixChange(){
	onComboChange("cmbPrefix");
}
function onPtChange(){
	onComboChange("cmbStatus");
}
function onGenderChange(){
	onComboChange("cmbGender");
}
function onSMSChange(){
	onComboChange("cmbSMS");
}
function onLanChange(){
	onComboChange("cmbLan");
}
function onRaceChange(){
	onComboChange("cmbRace");
}
function onEthnicityChange(){
	onComboChange("cmbEthicity");
}
function onZipChange(){
	onComboChange("cmbZip");
	var cmbZip = $("#cmbZip").data("kendoComboBox");
	if(cmbZip){
		var dataItem = cmbZip.dataItem();
		if(dataItem){
			$("#txtZip4").val(dataItem.zipFour);
			$("#txtCountry").val(dataItem.country);
			$("#txtState").val(dataItem.state);
			$("#txtCity").val(dataItem.city);
		}
	}
}
function getCodeTableValueList(dataObj){
	var dArray = getTableListArray(dataObj);
	if(dArray && dArray.length>0){
		setDataForSelection(dArray, "cmbPrefix", onPrefixChange, ["value", "idk"], 0, "");
	}
	getAjaxObject(ipAddress+"/Patient_Status/list/","GET",getPatientStatusValueList,onError);
}

function getPatientStatusValueList(dataObj){
	var dArray = getTableListArray(dataObj);
	if(dArray && dArray.length>0){
		setDataForSelection(dArray, "cmbStatus", onPtChange, ["desc", "idk"], 0, "");
	}
	getAjaxObject(ipAddress+"/Gender/list/","GET",getGenderValueList,onError);
}
function getGenderValueList(dataObj){
	var dArray = getTableListArray(dataObj);
	if(dArray && dArray.length>0){
		setDataForSelection(dArray, "cmbGender", onGenderChange, ["desc", "idk"], 0, "");
	}
	getAjaxObject(ipAddress+"/SMS/list/","GET",getSMSValueList,onError);
}
function getSMSValueList(dataObj){
	var dArray = getTableListArray(dataObj);
	if(dArray && dArray.length>0){
		setDataForSelection(dArray, "cmbSMS", onSMSChange, ["desc", "idk"], 0, "");
	}
	getAjaxObject(ipAddress+"/Language/list/","GET",getLanguageValueList,onError);
}
function getLanguageValueList(dataObj){
	var dArray = getTableListArray(dataObj);
	if(dArray && dArray.length>0){
		setDataForSelection(dArray, "cmbLan", onLanChange, ["desc", "idk"], 0, "");
	}
	getAjaxObject(ipAddress+"/Race/list/","GET",getRaceValueList,onError);
}
function getRaceValueList(dataObj){
	var dArray = getTableListArray(dataObj);
	if(dArray && dArray.length>0){
		setDataForSelection(dArray, "cmbRace", onRaceChange, ["desc", "idk"], 0, "");
	}
	getAjaxObject(ipAddress+"/Ethnicity/list/","GET",getEthnicityValueList,onError);
}
function getEthnicityValueList(dataObj){
	var dArray = getTableListArray(dataObj);
	if(dArray && dArray.length>0){
		setDataForSelection(dArray, "cmbEthicity", onEthnicityChange, ["desc", "idk"], 0, "");
	}
	getAjaxObject(ipAddress+"/user/list/200","GET",onGetUserListData,onError);
}
function onGetUserListData(dataObj){
	console.log(dataObj);
	var userListArray = [];
	if(dataObj && dataObj.response && dataObj.response.status){
		if(dataObj.response.status.code == "1"){
			if($.isArray(dataObj.response.loginUser)){
				userListArray = dataObj.response.loginUser;
			}else{
				userListArray.push(dataObj.response.loginUser);
			}
			for(var i=0;i<userListArray.length;i++){
				var item = userListArray[i];
				item.idk = item.id;
			}
		}else{
			customAlert.info("Error", dataObj.response.status.message);
		}
	}
	setDataForSelection(userListArray, "cmbType", onEthnicityChange, ["userName", "idk"], 0, "");
	if(operation == UPDATE && patientId != ""){
		getPatientInfo();
		var imageServletUrl = ipAddress+"/download/patient/photo/"+patientId+"/?"+Math.round(Math.random()*1000000);
		$("#imgPhoto").attr("src",imageServletUrl);
	}
}
function getPatientInfo(){
	getAjaxObject(ipAddress+"/patient/"+patientId,"GET",onGetPatientInfo,onError);
}
function getComboListIndex(cmbId,attr,attrVal){
	var cmb = $("#"+cmbId).data("kendoComboBox");
	if(cmb){
		var ds = cmb.dataSource;
		var totalRec = ds.total();
		for(var i=0;i<totalRec;i++){
			var dtItem = ds.at(i);
			if(dtItem && dtItem[attr] == attrVal){
				cmb.select(i);
				return i;
			}
		}
	}
	return -1;
}

function onHomeKeydown(e){
	//setTimeout(function(){
		var txtHPhone = $("#txtHPhone").data("kendoMaskedTextBox");
		if(txtHPhone){
			var strValue = txtHPhone.raw();
			/*if(strValue.length == 0){
				strValue = "";
			}else if(strValue.length == 1){
				strValue = "x";
			}else if(strValue.length == 2){
				strValue = "xx";
			}else if(strValue.length == 3){
				strValue = "xxx";
			}else{
				strValue = strValue.substring(3,strValue.length);
				strValue = "xxx"+strValue;
			}
			txtHPhone.value(strValue);*/
			console.log(strValue)
		}
	//})
}
function setHomePhoneMask(){
	$("#txtHPhone").kendoMaskedTextBox({
        mask: "(999) 999-9999"
    });
	$("#txtWP").kendoMaskedTextBox({
        mask: "(999) 999-9999"
    });
	$("#txtCell").kendoMaskedTextBox({
        mask: "999 999 9999"
    });
	//$("#txtHPhone").off("keyup");
	//$("#txtHPhone").on("keydown",onHomeKeydown);
}
/*function setWorkPhoneMask(){
	$("#txtWP").kendoMaskedTextBox({
        mask: "(999) 999-9999"
    });
}
function setExtensionMask(){
	$("#txtExtension").kendoMaskedTextBox({
        mask: "(999) 999-9999"
    });
}
function setCellPhoneMask(){
	$("#txtCell").kendoMaskedTextBox({
        mask: "(999) 999-9999"
    });
}*/
var cityId = "";
function onGetPatientInfo(dataObj){
	patientInfoObject = dataObj;
	if(dataObj && dataObj.response && dataObj.response.patient){
		$("#txtID").val(dataObj.response.patient.id);
		$("#txtExtID1").val(dataObj.response.patient.externalId1);
		$("#txtExtID2").val(dataObj.response.patient.externalId2);
		getComboListIndex("cmbPrefix","value",dataObj.response.patient.prefix);
		$("#txtFN").val(dataObj.response.patient.firstName);
		$("#txtLN").val(dataObj.response.patient.lastName);
		$("#txtMN").val(dataObj.response.patient.middleName);
		$("#txtWeight").val(dataObj.response.patient.weight);
		$("#txtHeight").val(dataObj.response.patient.height);
		$("#txtNN").val(dataObj.response.patient.nickname);
		if(dataObj.response.patient.dateOfBirth){
			var dt = new Date(dataObj.response.patient.dateOfBirth);
			if(dt){
				var strDT = kendo.toString(dt,"MM/dd/yyyy");
				 $( "#dtDOB" ).datepicker();
		            $( "#dtDOB" ).datepicker("setDate", strDT);
				/*var dtDOB = $("#dtDOB").data("kendoDatePicker");
				if(dtDOB){
					dtDOB.value(strDT);
				}*/
				/*var currDate = new Date();
				console.log(currDate.getFullYear()-dt.getFullYear());
				var strAge = currDate.getFullYear()-dt.getFullYear();*/
				var strAge = getAge(dt);
				$("#txtAge").val(strAge);
			}
		}
		
		$("#txtSSN1").val(dataObj.response.patient.ssn);
		var result = showSSNNumver(dataObj.response.patient.ssn);
		$("#txtSSN").val(result); 
		
	//	$("#txtSSN").val(dataObj.response.patient.ssn);
		
		getComboListIndex("cmbStatus","desc",dataObj.response.patient.status);
		getComboListIndex("cmbGender","desc",dataObj.response.patient.gender);
		getComboListIndex("cmbEthicity","desc",dataObj.response.patient.ethnicity);
		getComboListIndex("cmbRace","desc",dataObj.response.patient.race);
		getComboListIndex("cmbLan","desc",dataObj.response.patient.language);
		
		if(dataObj && dataObj.response && dataObj.response.communication){
			
			var commArray = [];
			if($.isArray(dataObj.response.communication)){
				commArray = dataObj.response.communication;
			}else{
				commArray.push(dataObj.response.communication);
			}
			var comObj = commArray[0];
			commId = comObj.id;
			$("#txtAdd1").val(comObj.address1);
			$("#txtAdd2").val(comObj.address2);
		//	$("#txtSMS").val(comObj.sms);
			getComboListIndex("cmbSMS","desc",comObj.sms);
			getComboListIndex("cmbZip","idk",comObj.cityId);
			cityId = comObj.cityId;
			//onZipChange();
			//$("#txtCell").val(comObj.cellPhone);
			$("#txtWPExt").val(comObj.workPhoneExt);
			//$("#txtWP").val(comObj.workPhone);
			$("#txtExtension").val(comObj.homePhoneExt);
			setHomePhoneMask();
			setMaskTextBoxValue("txtHPhone", comObj.homePhone);
			setMaskTextBoxValue("txtWP", comObj.workPhone);
			setMaskTextBoxValue("txtCell", comObj.cellPhone);
			/*var txtHome = $("#txtHPhone").data("kendoMaskedTextBox");
			if(txtHome){
				txtHome.value(comObj.homePhone);
			}
			var txtWP = $("#txtWP").data("kendoMaskedTextBox");
			if(txtWP){
				txtWP.value(comObj.workPhone);
			}
			var txtCell = $("#txtCell").data("kendoMaskedTextBox");
			if(txtCell){
				txtCell.value(comObj.cellPhone);
			}*/
			//$("#txtHPhone").val(comObj.homePhone);
			$("#txtEmail").val(comObj.email);
			
			$("#txtCountry").val(comObj.country);
			$("#cmbZip").val(comObj.zip);
			$("#txtZip4").val(comObj.zipFour);
			$("#txtState").val(comObj.state);
			$("#txtCity").val(comObj.city);
			
			
			/*setWorkPhoneMask();
			setExtensionMask();
			setCellPhoneMask();*/
			
			if(comObj.defaultCommunication == "1"){
				$("#rdHome").prop("checked",true);
			}else if(comObj.defaultCommunication == "2"){
				$("#rdWork").prop("checked",true);
			}else if(comObj.defaultCommunication == "3"){
				$("#rdCell").prop("checked",true);
			}else if(comObj.defaultCommunication == "4"){
				$("#rdEmail").prop("checked",true);
			}
		}
		
	}
}
function getTableListArray(dataObj){
	var dataArray = [];
	if(dataObj && dataObj.response && dataObj.response.codeTable){
		if($.isArray(dataObj.response.codeTable)){
			dataArray = dataObj.response.codeTable;
		}else{
			dataArray.push(dataObj.response.codeTable);
		}
	}
	var tempDataArry = [];
	var obj = {};
	obj.desc = "";
	obj.zip = "";
	obj.value = "";
	obj.idk = "";
	tempDataArry.push(obj);
	for(var i=0;i<dataArray.length;i++){
		if(dataArray[i].isActive == 1){
			var obj = dataArray[i];
			obj.idk = dataArray[i].id;
			obj.status = dataArray[i].Status;
			tempDataArry.push(obj);
		}
	}
	return tempDataArry;
}
function onClickReset(){
	operation = ADD;
	patientId = "";
	$("#txtID").val("");
	$("#txtExtID1").val("");
	$("#txtExtID2").val("");
	setComboReset("cmbPrefix");
	$("#txtFN").val("");
	$("#txtLN").val("");
	$("#txtMN").val("");
	$("#txtWeight").val("");
	$("#txtHeight").val("");
	$("#txtNN").val("");
		var dtDOB = $("#dtDOB").data("datepicker");
		/*if(dtDOB){
			dtDOB.value("");
		}*/
		$("#txtAge").val("");
	$("#txtSSN").val("");
	setComboReset("cmbStatus");
	setComboReset("cmbGender");
	setComboReset("cmbEthicity");
	setComboReset("cmbRace");
	setComboReset("cmbLan");
	$("#txtAdd1").val("");
	$("#txtAdd2").val("");
	setComboReset("cmbSMS");
	$("#txtCell").val("");
	$("#txtWPExt").val("");
	$("#txtWP").val("");
	$("#txtExtension").val("");
		
	resetMaskTextBoxValue("txtHPhone");
	resetMaskTextBoxValue("txtWP");
	resetMaskTextBoxValue("txtCell");
		
		//$("#txtHPhone").val("");
	$("#txtEmail").val("");
		
	$("#rdHome").prop("checked",true);

}

function getMaskTextBoxValue(txtId){
	var str = "";
	var txtHome = $("#"+txtId).data("kendoMaskedTextBox");
	if(txtHome){
		str = txtHome.raw();
	}
	return str;
}
function setMaskTextBoxValue(txtId,strVal){
	var txtHome = $("#"+txtId).data("kendoMaskedTextBox");
	if(txtHome){
		txtHome.value(strVal);
	}
}
function resetMaskTextBoxValue(txtId){
	var txtHome = $("#"+txtId).data("kendoMaskedTextBox");
	if(txtHome){
		txtHome.value("");
	}
}
function setComboReset(cmbId){
	var cmb = $("#"+cmbId).data("kendoComboBox");
	if(cmb){
		cmb.select(0);
	}
}

function validation(){
var flag = true;	
var txtID = $("#txtExtID1").val();
txtID = $.trim(txtID);
/*if(txtID == ""){
	customAlert.error("Error","Enter patient External ID1");
	flag = false;
	return false;
}*/
var txtWeight = $("#txtWeight").val();
txtWeight = $.trim(txtWeight);
if(txtWeight == ""){
	customAlert.error("Error","Enter patient weight");
	flag = false;
	return false;
}
var txtHeight = $("#txtHeight").val();
txtHeight = $.trim(txtHeight);
if(txtHeight == ""){
	customAlert.error("Error","Enter patient height");
	flag = false;
	return false;
}
var cmbPrefix = $("#cmbPrefix").data("kendoComboBox");
if(cmbPrefix && cmbPrefix.value() == ""){
	customAlert.error("Error","Select prefix value");
	flag = false;
	return false;
}
var cmbStatus = $("#cmbStatus").data("kendoComboBox");
if(cmbStatus && cmbStatus.value() == ""){
	customAlert.error("Error","Select status value");
	flag = false;
	return false;
}
var txtFN = $("#txtFN").val();
txtFN = $.trim(txtFN);
if(txtFN == ""){
	customAlert.error("Error","Enter patient first name");
	flag = false;
	return false;
}
var txtAge = $("#txtAge").val();
txtAge = $.trim(txtAge);
if(txtAge == ""){
	customAlert.error("Error","Enter patient date of birth");
	flag = false;
	return false;
}

var txtSSN = $("#txtSSN").val();
txtSSN = $.trim(txtSSN);
if(txtSSN == ""){
	customAlert.error("Error","Enter patient SSN");
	flag = false;
	return false;
}
var sEmail = $('#txtEmail').val();
if (sEmail && !validateEmail(sEmail)) {
	customAlert.error("Error","your EmailId is invalid,Please enter the valid EmailId");
}

var cmbGender = $("#cmbGender").data("kendoComboBox");
if(cmbGender && cmbGender.value() == ""){
	customAlert.error("Error","Select gender");
	flag = false;
	return false;
}
var txtSSN = $("#txtSSN").val();
txtSSN = $.trim(txtSSN);
if(txtSSN == ""){
	customAlert.error("Error","Enter patient SSN");
	flag = false;
	return false;
}


	return flag;
}

function getComboDataItem(cmbId){
	var dItem = null;
	var cmb = $("#"+cmbId).data("kendoComboBox");
	if(cmb && cmb.dataItem()){
		dItem =  cmb.dataItem();
		return cmb.text();
	}
	return "";
}

function getComboDataItemValue(cmbId){
	var dItem = null;
	var cmb = $("#"+cmbId).data("kendoComboBox");
	if(cmb && cmb.dataItem()){
		dItem =  cmb.dataItem();
		return cmb.value();
	}
	return "";
}

function onClickSave(){
	/*var sEmail = $('#txtEmail').val();
	if (validateEmail(sEmail)) {
		}
else {
customAlert.error("Error","your EmailId is invalid,Please enter the valid EmailId");
//preventDefault();
}*/
	/*$("#txtHPhone").kendoMaskedTextBox({
        mask: "(999) 999-9999"
    });*/
	/*var txtHome = $("#txtHPhone").data("kendoMaskedTextBox");
	if(txtHome){
		$("#txtHPhone").val().replace(/[()\s-_]/g, "");
	}*/
	if(validation()){
		var strId = $("#txtID").val();
		var strExtId1 = $("#txtExtID1").val();
		var strExtId2 = $("#txtExtID2").val();
		var strPrefix = getComboDataItem("cmbPrefix");
		var strNickName = $("#txtNN").val();
		var strStatus = getComboDataItem("cmbStatus");
		var strFN = $("#txtFN").val();
		var strMN = $("#txtMN").val();
		var strLN = $("#txtLN").val();
		
		/*var dtItem = $("#dtDOB").data("kendoDatePicker");
		var strDate = "";
		if(dtItem && dtItem.value()){
			strDate = kendo.toString(dtItem.value(),"yyyy-MM-dd");
		}*/
		var strDate = "";
		var dob = document.getElementById("dtDOB").value;
		if(dob){
			var DOB = new Date(dob);
			strDate = kendo.toString(DOB,"yyyy-MM-dd");
		}
        
		
		var strSSN = $("#txtSSN1").val();
		console.log(strSSN);
		var strGender = getComboDataItem("cmbGender");
		var strAdd1 = $("#txtAdd1").val();
		var strAdd2 = $("#txtAdd2").val();
		var strCity = $("#txtCity").val();
		var strState = $("#txtState").val();
		var strCountry = $("#txtCountry").val();
		
	//	var strZip = getComboDataItemValue("cmbZip");
		var strZip4 = $("#txtZip4").val();
		//var strHPhone = $("#txtHPhone").val();
		var strHPhone = "";
		/*var txtHome = $("#txtHPhone").data("kendoMaskedTextBox");
		if(txtHome){
			strHPhone = txtHome.raw();
		}*/
		strHPhone = getMaskTextBoxValue("txtHPhone");
		var strWp = getMaskTextBoxValue("txtWP");
		var strCell = getMaskTextBoxValue("txtCell");//$("#txtCell").val();
		var strExt = $("#txtWPExt").val();
		//var strWp = $("#txtWP").val();
		var strWpExt = $("#txtWPExt").val();
		//var strCell = $("#txtCell").val();
		
		var strSMS = getComboDataItem("cmbSMS");
		
		var strEmail = $("#txtEmail").val();
		var strLan = getComboDataItem("cmbLan");
		var strRace = getComboDataItem("cmbRace");
		var strEthinicity = getComboDataItem("cmbEthicity");
		
		var dataObj = {};
		dataObj.patient = {};
		dataObj.patient.createdBy = Number(sessionStorage.userId);;//"101";
		dataObj.patient.externalId1 = strExtId1;
		dataObj.patient.externalId2 = strExtId2;
		dataObj.patient.height = $("#txtHeight").val();;
		dataObj.patient.weight = $("#txtWeight").val();;
		dataObj.patient.prefix = strPrefix;//$("#txtEmail").val();;
		dataObj.patient.firstName = strFN;
		dataObj.patient.middleName = strMN;
		dataObj.patient.lastName = strLN;
		dataObj.patient.nickname = strNickName;
		dataObj.patient.dateOfBirth = strDate;
		dataObj.patient.ssn = strSSN;
		dataObj.patient.gender = strGender;
		dataObj.patient.ethnicity = strEthinicity;
		dataObj.patient.race = strRace;
		dataObj.patient.language = strLan;
		dataObj.patient.isActive = "1";
		dataObj.patient.status = strStatus;
		var cmbType = $("#cmbType").data("kendoComboBox");
		dataObj.patient.nurseId = Number(cmbType.value());
		dataObj.patient.vitals = [];
		
		var comm = [];
		var comObj = {};
		comObj.createdBy = sessionStorage.userId;//"101";
		comObj.address1 = strAdd1;
		comObj.address2 = strAdd2;
		var cmbZip = $("#cmbZip").data("kendoComboBox");
		comObj.cityId = Number(cityId);//cmbZip.value();
		comObj.homePhone = Number(strHPhone);
		comObj.homePhoneExt = Number(strExt);
		comObj.workPhone = Number(strWp);
		comObj.workPhoneExt = Number(strWpExt);
		comObj.cellPhone = strCell;
		comObj.sms = strSMS;
		comObj.email = strEmail;
		var propVal =  $("input:radio[name='Comm']:checked").val();
		comObj.defaultCommunication = Number(propVal);
		if(operation == UPDATE){
			comObj.modifiedBy = Number(sessionStorage.userId);;//"101";
			comObj.id = commId;
			comObj.isDeleted = 0;
		}
		comObj.isActive = 1;
		comm.push(comObj);
		dataObj.communication = comm;
		console.log(dataObj);
		
				if(operation == ADD){
				var dataUrl = ipAddress+"/patient/create";
				createAjaxObject(dataUrl,dataObj,"POST",onCreate,onError);
			}else if(operation == UPDATE){
				dataObj.patient.modifiedBy = Number(sessionStorage.userId);//"101";
				dataObj.patient.id = patientId;
				dataObj.patient.isDeleted = "0";
				var dataUrl = ipAddress+"/patient/update";
				createAjaxObject(dataUrl,dataObj,"POST",onCreate,onError);
			}
	}
}

function onCreate(dataObj){
	console.log(dataObj);
	if(dataObj && dataObj.response && dataObj.response.status){
		if(dataObj.response.status.code == "1"){
			/**/
			var obj = {};
			obj.status = "success";
			obj.operation = operation;
			popupClose(obj);
			
		}else{
			customAlert.error("error", dataObj.response.status.message);
		}
	}
	/**/
}

function onError(errObj){
	console.log(errObj);
	customAlert.error("Error","Error");
}
function onClickCancel(){
	var obj = {};
	obj.status = "false";
	popupClose(obj);
}
function onClickSearch(){
	var obj = {};
	obj.status = "search";
	popupClose(obj);
}
function popupClose(obj){
	var windowWrapper = new kendoWindowWrapper();
	windowWrapper.closePageWindow(obj);
}



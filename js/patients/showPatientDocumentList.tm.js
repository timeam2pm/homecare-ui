
var parentRef = null;
var selObj = null;
$(document).ready(function(){
    parentRef = parent.frames['iframe'].window;
});

$(window).load(function(){
    init();
    buttonEvents();
});
function init(){
    //var video = document.getElementById('videoCtrl');
    //var source = document.createElement('source');
    var sType = parentRef.sType;
    var urlPath = "";
    var extension = parentRef.category;
    urlPath = ipAddress+"/homecare/download/documents/";
    var videoUrl = urlPath+"?access_token="+sessionStorage.access_token+"&id="+parentRef.documentId+"&tenant=" + sessionStorage.tenant;
    console.log(videoUrl);


    // $("#imgPhoto").attr('src', videoUrl);

    // var URL = "http://docs.google.com/viewer?url=<?=urlencode("+videoUrl+")?>&embedded=true";


    //     $("#iframePdf").attr('src', "https://docs.google.com/gview?url="+videoUrl);


    if(extension.toLowerCase() == "png" || extension.toLowerCase() == "jpeg" || extension.toLowerCase() == "gif"){
        $("#iframePdf").css("display","none");
        $("#imgPhoto").attr('src', videoUrl);
    }
    else if(extension.toLowerCase() == "pdf"){
        $("#iframePdf").attr('src', videoUrl);
        $("#imgPhoto").css("display","none");
    }
}
function buttonEvents(){
    $("#btnCancelDet").off("click");
    $("#btnCancelDet").on("click",onClickCancel);
}
function onClickOK(){
    popupClose(false);
}
function onClickCancel(){
    popupClose(false);
}
function popupClose(st){
    var onCloseData = new Object();
    onCloseData.status = st;
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(onCloseData);
}

function handleGetError(e) {
    var errLbl = getLocaleStringWithDefault(DC_UI_COMM, 'ERROR_MESSAGE', "Error");
    if(e && e.RestData && e.RestData.Description){
        window.top.displayErrorPopUp(errLbl,e.RestData.Description);
    }
}
function handleGetHttpError(e) {
    var errLbl = getLocaleStringWithDefault(DC_UI_COMM, 'ERROR_MESSAGE', "Error");
    var errDesc = getLocaleStringWithDefault(DC_UI_COMM, 'HTTP_ERROR', 'Http Error');
    // window.top.displayErrorPopUp(errLbl,errDesc);
}
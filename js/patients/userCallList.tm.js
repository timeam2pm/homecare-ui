var angularUIgridWrapper = null;
var parentRef = null;
var operation = "";
var selItem = null;
var recordType = ""; // Usr Call List

var statusArr = [{Key:'Video',Value:'Video'},{Key:'Audio',Value:'Audio'}];
var userTpeArray = [{Key:'',Value:''},{Key:'Patient',Value:'500'},{Key:'Relative',Value:'600'},{Key:'Nurse',Value:'200'},{Key:'Doctor',Value:'100'},{Key:'Care Giver',Value:'201'}];

$(document).ready(function(){
	parentRef = parent.frames['iframe'].window;
});


$(window).load(function(){
	if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
	init();
});

function init(){
	$("#cmbCallType").kendoComboBox();
	$("#cmbUserType").kendoComboBox();
	setDataForSelection(statusArr, "cmbCallType", onCallTypeChange, ["Key", "Value"], 0, "");
	setDataForSelection(userTpeArray, "cmbUserType", onUserTypeChange, ["Key", "Value"], 0, "");
	var dataOptions = {
	        pagination: false,
	        paginationPageSize: 500,
			changeCallBack: onChange
	    }

		angularUIgridWrapper = new AngularUIGridWrapper("dgridCallList", dataOptions);
	    angularUIgridWrapper.init();

	    var dataArray = [];
	    buildDeviceTypeGrid(dataArray);
	    getRecords();
	    
	buttonEvents();
}
function onCallTypeChange(){
	var cmbCallType = $("#cmbCallType").data("kendoComboBox");
	if(cmbCallType && cmbCallType.selectedIndex<0){
		cmbCallType.select(0);
	}
}
function buttonEvents(){
	$("#btnCancel").off("click",onClickCancel);
	$("#btnCancel").on("click",onClickCancel);
	
    $("#btnSave").off("click");
    $("#btnSave").on("click",onClickCall);
    
    $("#btnPatient").off("click");
    $("#btnPatient").on("click",onClickPatient);
    
    $("#btnNurse").off("click");
    $("#btnNurse").on("click",onClickNurse);
    
    $("#btnDoctor").off("click");
    $("#btnDoctor").on("click",onClickDoctor);
    
}
function onUserTypeChange(){
	var cmbUserType = $("#cmbUserType").data("kendoComboBox");
	if(cmbUserType && cmbUserType.selectedIndex<0){
		cmbUserType.select(0);
	}
	recordType = cmbUserType.value();
	getRecords();
}
function onClickPatient(){
	recordType = "500";
	$("#btnPatient").addClass("selectButtonBarClass");
	$("#btnNurse").removeClass("selectButtonBarClass");
	$("#btnDoctor").removeClass("selectButtonBarClass");
	getRecords();
}
function onClickNurse(){
	recordType = "200";
	$("#btnPatient").removeClass("selectButtonBarClass");
	$("#btnNurse").addClass("selectButtonBarClass");
	$("#btnDoctor").removeClass("selectButtonBarClass");
	getRecords();
}
function onClickDoctor(){
	recordType = "100";
	$("#btnPatient").removeClass("selectButtonBarClass");
	$("#btnNurse").removeClass("selectButtonBarClass");
	$("#btnDoctor").addClass("selectButtonBarClass");
	getRecords();
}

function getRecords(){
	buildDeviceTypeGrid([]);//https://stage.timeam.com/user/list/?user-type-id=100
	if(recordType != ""){
		 //getAjaxObject(ipAddress+"/user/list/?user-type-id="+recordType,"GET",onGetUserListData,onError);
	}
}
function onGetUserListData(dataObj){
	var userListArray = [];
	if(dataObj && dataObj.response && dataObj.response.status){
		if(dataObj.response.status.code == "1"){
			if($.isArray(dataObj.response.user)){
				userListArray = dataObj.response.user;
			}else{
				userListArray.push(dataObj.response.user);
			}
			for(var i=0;i<userListArray.length;i++){
				var item = userListArray[i];
				item.idk = item.id;
				item.fullName = item.firstName+" "+item.middleName+" "+item.lastName;
				item.status  = "Active";
				if(item.isActive == 0){
					item.status  = "In-Active";
				}
			}
		}else{
			customAlert.info("Error", dataObj.response.status.message);
		}
	}
	buildDeviceTypeGrid(userListArray);
}
function buildDeviceTypeGrid(dataSource) {
    var gridColumns = [];
	var otoptions = {};
	otoptions.noUnselect = false;
	 gridColumns.push({
	        "title": "User ID",
	        "field": "idk",
	        "width":"25%"
		});
 gridColumns.push({
     "title": "User Name",
     "field": "username",
     "width":"30%"
	});
 gridColumns.push({
     "title": "Full Name",
     "field": "fullName",
	});
 gridColumns.push({
     "title": "Status",
     "field": "status",
     "width":"20%"
	});
	angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
	adjustHeight();
}
var selectUser = null;
function onChange(){
	setTimeout(function(){
		var selRows = angularUIgridWrapper.getSelectedRows();
		if(selRows && selRows.length>0){
			var selItem = selRows[0];
			selectUser = selItem;
		}
	})
}
$(window).resize(adjustHeight);
function adjustHeight(){
	var defHeight = 150;
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 120;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    try{angularUIgridWrapper.adjustGridHeight(cmpHeight);}catch(e){};

}
function onClickCancel(){
	var obj = {};
	obj.status = "false";
	popupClose(obj);
}
function onClickCall(){
	var obj = {};
	obj.status = "success";
	obj.user = selectUser;
	var cmbCallType = $("#cmbCallType").data("kendoComboBox");
	if(cmbCallType){
		if(cmbCallType.text() == ""){
			customAlert.error("Error","Please select call type");
			return;
		}
	}
	obj.callType = cmbCallType.text();
	
	popupClose(obj);
}
function onClickSearch(){
	var obj = {};
	obj.status = "search";
	popupClose(obj);
}
function onError(errObj){
	console.log(errObj);
	customAlert.error("Error","Unable to create UserMapping");
}
function popupClose(obj){
	var windowWrapper = new kendoWindowWrapper();
	windowWrapper.closePageWindow(obj);
}



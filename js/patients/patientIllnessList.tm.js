var angularUIgridWrapper;
var parentRef = null;
var dataTabURL = "";
var dataArray = [];
var ADD = "add";
var UPDATE = "edit";
var operation = "add";
var patientId = "";
var atID = "";

$(document).ready(function(){
    sessionStorage.setItem("IsSearchPanel", "1");
    parentRef = parent.frames['iframe'].window;
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("divPatientIllnessGrid", dataOptions);
    angularUIgridWrapper.init();
    buildIllnessListGrid([]);
});


$(window).load(function(){
    loading = false;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onChange(){
    setTimeout(function() {
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        if (selectedItems && selectedItems.length > 0) {
            var obj = selectedItems[0];
            atID = obj.illnessId;
            $("#btnEdit").prop("disabled", false);
            $("#btnDelete").prop("disabled", false);
        } else {
            $("#btnEdit").prop("disabled", true);
            $("#btnDelete").prop("disabled", true);
        }
    });
}

function onLoaded(){
    init();
    buttonEvents();
    adjustHeight();
}

function init(){
    patientId = parentRef.patientId;
    // getExcersizeRecords();
    buildIllnessListGrid([]);
    //getAjaxObject(ipAddress+"/patient/reports/holter/"+patientId,"GET",onGetHolterReportsData,onError);

    dataTabURL = ipAddress + "/homecare/patient-medical-conditions/";
    getAjaxObject(dataTabURL + '?is-active=1&is-deleted=0&fields=*,medicalConditionType.*&patientId='+patientId, "GET", getIllnessData, onError);
}
function getExcersizeRecords(){
    getAjaxObject(ipAddress+"/patient/illness/"+patientId,"GET",onGetVitalsSuccess,onError);
}
function onGetHBTPatientData(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        getVitals();
    }
}
function getVitals(){

}
function getVitalInfo(vName,dt){
    for(var i=0;i<vitalDataArray.length;i++){
        var item = vitalDataArray[i];
        if(item.vital == vName && item.date == dt){
            return item.value;
        }
    }
    return "";
}

function getRowColors(index) {
    var classes = ['active', 'success', 'info', 'warning', 'danger'];
    // console.log(index);
    if (index % 2 === 0 && index / 2 < classes.length) {
        //console.log(classes[index / 2]);
        return classes[index / 2];
        //console.log(index+','+classes[index / 2]);
		/*  return {
		 classes: classes[index / 2]
		 };*/
    }
    return "";
}
var illnessArr = [];
function onGetVitalsSuccess(dataObj){
    $("#divaddIllnes").text("");
    illnessArr = [];
    if(dataObj){
        if($.isArray(dataObj)){
            illnessArr = dataObj;
        }else{
            illnessArr.push(dataObj);
        }
    }
    var strTable1 = '<table class="table">';
    strTable1 = strTable1+'<thead class="fillsHeader">';
    strTable1 = strTable1+'<tr>';
    //strTable = strTable+'<th>Patient ID</th>';
    //strTable = strTable+'<th class="textAlign whiteColor">Link</th>';
    //strTable = strTable+'<th>Link</th>';
    strTable1 = strTable1+'<th class="textAlign whiteColor">Description</th>';
    strTable1 = strTable1+'</tr>';
    strTable1 = strTable1+'</thead>';
    strTable1 = strTable1+'<tbody>';
    for(var i=0;i<illnessArr.length;i++){
        var dataItem = illnessArr[i];
        if(dataItem){
            //console.log(dataItem);
            var className = getRowColors(i);
            strTable1 = strTable1+'<tr  class="'+className+'">';
            //strTable = strTable+'<td>'+dataItem.patientId+'</td>';
            //	strTable = strTable+'<td><a target="_blank" href="'+dataItem.link+'">'+dataItem.link+'</a></td>';
            //strTable = strTable+'<td>'+dataItem.link+'</td>';
            if(dataItem.fileName){
                strTable1 = strTable1+'<td class="textAlign"><a onClick="onClickIllnessVideo(event)" style="cursor:pointer" class="hyperLinkStyle">'+dataItem.fileName+'</a></td>';
            }else{
                strTable1 = strTable1+'<td class="textAlign">'+dataItem.fileName+'</td>';
            }

            strTable1 = strTable1+'</tr>';
        }
    }
    strTable1 = strTable1+'</tbody>';
    strTable1 = strTable1+'</table>';
    $("#divaddIllnes").append(strTable1);
}
function onClickIllnessVideo(event){
    var urlPath = "";
    var urlText = "";
    var iItem = null;
    if(event.currentTarget){
        for(var i=0;i<illnessArr.length;i++){
            var item = illnessArr[i];
            if(item && item.fileName == event.currentTarget.text){
                urlPath = item.youtubeLink;
                iItem = item;
                break;
            }
        }
        //urlPath = event.currentTarget.href;
    }
    if(urlPath && urlPath.indexOf("www.youtube.com")>=0){
        playYouTubeFile(urlPath);
    }else{
        var iFileType = "";
        iFileType = iItem.fileType;
        iFileType = iFileType.toLowerCase();
        if(iFileType == "mp3"){
            playAudioFile(iItem.id);
        }else{
            showPdfVideo(iItem.id, iFileType, "Diet");
        }
    }
}
var imgImage = null;
function onClickGenerate(event){
    console.log(event);
    var dietVideoId = "";
    if(imgImage){
        $(imgImage).removeClass("imgBorder");
    }

    if(event.currentTarget){
        dietVideoId = event.currentTarget.id;
        imgImage = event.currentTarget;
        $(imgImage).addClass("imgBorder");
    }
    var fileType = findFileType(dietDataArray,dietVideoId);
    showPdfVideo(dietVideoId,fileType,"diet");
}
function showPdfVideo(fileId,fileType,sType){
    parentRef.dietId = fileId;
    parentRef.sType = sType;
    var urlPath = "";
    if(sType == "diet"){
        urlPath = ipAddress+"/patient/diet/download/"
    }else{
        urlPath = ipAddress+"/patient/exercise/download/"
    }
    var popW =900;
    var popH = 580;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    if(fileType && fileType.toLowerCase() == "pdf"){
        profileLbl = "Document";
        popW = 1100;
        devModelWindowWrapper.openPageWindow("../../html/patients/showPdf.html", profileLbl, popW, popH, true, closeVideoScreen);
        //var reqUrl = urlPath+fileId;
        //window.open(reqUrl, "popupWindow", "width=1000,height=600,scrollbars=yes");
        //$(imgImage).removeClass("imgBorder");
        //$(imgImage).removeClass("textBorder");
    }else{
        profileLbl = "Video";
        devModelWindowWrapper.openPageWindow("../../html/patients/showVideo.html", profileLbl, popW, popH, true, closeVideoScreen);
    }
}
function onClickDietYoutube(event){
    var dietVideoId, ext = "";
    var urlPath = "";
    if(imgImage){
        $(imgImage).removeClass("imgBorder");
    }

    if(event.currentTarget){
        imgImage = event.currentTarget;
        $(imgImage).addClass("imgBorder");
    }
    for(var i=0;i<dietDataArray.length;i++){
        var item = dietDataArray[i];
        if(item && item.id == event.currentTarget.id){
            urlPath = item.youtubeLink;
            break;
        }
    }
    // if(urlPath && urlPath.indexOf("www.youtube.com")>=0){
        playYouTubeFile(urlPath);
    // }
}
function playYouTubeFile(urlPath){
    var popW =900;
    var popH = 580;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Video";
    parentRef.sType = "illness";
    parentRef.illUrlPath = urlPath;
    devModelWindowWrapper.openPageWindow("../../html/patients/showVideo.html", profileLbl, popW, popH, true, closeVideoScreen);
}
var imgImage = null;
function onClickMyAudio(event){
    var audioId = "";
    if(imgImage){
        $(imgImage).removeClass("textBorder");
    }

    if(event.currentTarget){
        audioId = event.currentTarget.id;
        imgImage = event.currentTarget;
        $(imgImage).addClass("textBorder");
    }
    console.log(audioId);
    if(audioId && audioId != "null"){
        playAudioFile(audioId);
    }
}
function playAudioFile(aid){
    parentRef.audioId = aid;
    var popW = 600;
    var popH = 200;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Audio";
    devModelWindowWrapper.openPageWindow("../../html/patients/showAudio.html", profileLbl, popW, popH, true, closeVideoScreen);
}
function closeVideoScreen(evt,returnData){
    //$(imgImage).removeClass("imgBorder");
    //$(imgImage).removeClass("textBorder");
}
function onError(errorObj){
    console.log(errorObj);
}
var holterReportDataArray = [];
function onGetHolterReportsData(dataObj){
    $("#divTable").text("");
    var dataArray = [];
    if(dataObj){
        if($.isArray(dataObj)){
            dataArray = dataObj;
        }else{
            dataArray.push(dataObj);
        }
    }
    holterReportDataArray = dataArray;
    var strTable = '<table class="table">';
    strTable = strTable+'<thead class="fillsHeader whiteColor">';
    strTable = strTable+'<tr>';
    //strTable = strTable+'<th>Patient ID</th>';
    strTable = strTable+'<th class="textAlign whiteColor" style="width:220px">Date Generated</th>';
    strTable = strTable+'<th class="textAlign whiteColor">File Name</th>';
    strTable = strTable+'<th class="textAlign whiteColor">File Type</th>';
    strTable = strTable+'</tr>';
    strTable = strTable+'</thead>';
    strTable = strTable+'<tbody>';
    for(var i=0;i<holterReportDataArray.length;i++){
        var dataItem = holterReportDataArray[i];
        if(dataItem){
            //console.log(dataItem);
            var className = getRowColors(i);
            strTable = strTable+'<tr class="'+className+'">';
            //strTable = strTable+'<td>'+dataItem.patientId+'</td>';
            strTable = strTable+'<td class="textAlign">'+kendo.toString(new Date(dataItem.createdDate),"MM-dd-yyyy h:mm:ss")+'</td>';
            strTable = strTable+'<td class="textAlign">'+dataItem.fileName+'</td>';
            var dietId = dataItem.id;
            if(dataItem.fileType == "mp4"){
                strTable = strTable+'<td style="width:200px;padding:0px" class="textAlign"><img id="'+dietId+'" src="../../img/AppImg/HosImages/video.png"  class="cusrsorStyle videoIcon" onClick="onClickGenerate(event)"></td>';
            }else if(dataItem.fileType == "pdf"){
                strTable = strTable+'<td style="width:200px;padding:0px" class="textAlign"><img  id="'+dietId+'" src="../../img/AppImg/HosImages/pdf.png"    class="cusrsorStyle pdfIcon" onClick="onClickHolterReport(event)"></td>';
            }
            strTable = strTable+'</tr>';
        }
    }
    strTable = strTable+'</tbody>';
    strTable = strTable+'</table>';
    $("#divHolterReports").append(strTable);
}
function buttonEvents(){
    $("#btnCancel").off("click",onClickCancel);
    $("#btnCancel").on("click",onClickCancel);

    $("#btnAdd").off("click");
    $("#btnAdd").on("click",onClickAdd);

    $("#btnReset").off("click", onClickReset);
    $("#btnReset").on("click", onClickReset);

    $("#btnSave").off("click");
    $("#btnSave").on("click",onClickSave);

    $("#btnEdit").off("click",onClickEdit);
    $("#btnEdit").on("click",onClickEdit);

    $("#btnDelete").off("click",onClickDelete);
    $("#btnDelete").on("click",onClickDelete);

    $("#btnCondition").off("click", OpenIllness);
    $("#btnCondition").on("click", OpenIllness);

    $(".btnActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('active');
        onClickActive();
    });
    $(".btnInActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('inactive');
        onClickInActive();
    });
}

function onClickDelete(){
    customAlert.confirm("Confirm", "Are you sure to delete?",function(response){
        if(response.button == "Yes"){
            var dataObj = {};
            dataObj.modifiedBy = Number(sessionStorage.userId);
            dataObj.isDeleted = 1;
            dataObj.isActive = 0;
            dataObj.id = atID;
            var dataUrl = ipAddress + "/homecare/patient-medical-conditions/";
            var method = "DELETE";
            createAjaxObject(dataUrl, dataObj, method, onCreateDelete, onError);
        }
    });
}

function onCreateDelete(respObj) {
    console.log(respObj);
    if (respObj && respObj.response && respObj.response.status) {
        if (respObj.response.status.code == "1") {
            var msg = "Diagnosis deactivated successfully";
            customAlert.error("Info", msg);
            operation = ADD;
            init();

        } else {
            customAlert.error("error", respObj.response.status.message);
        }
    }
}

function onClickAdd(){
    parentRef.operation = "add";
    addIllnessData("add");
}

function onClickEdit(){
    parentRef.operation = "edit";
    addIllnessData("edit");
}

var recType = "5";
function onClickAddDiet(){
    recType = "5";
    showDietTypes("illness","Illness",recType);
}
function showDietTypes(type,tit,recType){
    var popW = "67%";
    var popH = 450;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = tit;
    parentRef.recordType = recType;
    parentRef.patientId = patientId;
    devModelWindowWrapper.openPageWindow("../../html/patients/dietExerciseList.html", profileLbl, popW, popH, true, closeDietExcersizeList);
}
function closeDietExcersizeList(evt,returnData){
    if(returnData && returnData.status == "success"){
        if(recType == "5"){
            customAlert.info("info", "Excersize file(s) attached/removed successfully.");
        }
    }
    getExcersizeRecords();
}
// function adjustHeight(){
//     var defHeight = 60;//+window.top.getAvailPageHeight();
//     if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
//         defHeight = 80;
//     }
//     var cmpHeight = window.innerHeight - defHeight;
//     cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
//     $("#divPtHolter").height(cmpHeight);
//     angularUIgridWrapper.adjustGridHeight(cmpHeight);
// }

function adjustHeight(){
    var defHeight = 230;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 70;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    angularUIgridWrapper.adjustGridHeight(cmpHeight);
}


function onClickPatientCall(obj){
    var popW = "60%";
    var popH = 400;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Patient Call Chart";
    parentRef.selObj = obj;
    devModelWindowWrapper.openPageWindow("../../html/patients/callPatient.html", profileLbl, popW, popH, false, closeCallPatient);
}
function closeCallPatient(evt,returnData){
    if(returnData && returnData.status == "success"){
        customAlert.info("Save","Patient Call Chart Created Successfully");
        init();
    }
}
function onClickCancel(){
    $("#divTypeDetails").css("display","none");
    $("#viewDivBlock").css("display","");
}


function buildIllnessListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Id",
        "field": "idk",
    });
    gridColumns.push({
        "title": "Condition",
        "field": "medicalConditionTypeValue"
    });
    gridColumns.push({
        "title": "Notes",
        "field": "notes"
    });
    gridColumns.push({
        "title": "Remarks",
        "field": "remarks"
    });
    gridColumns.push({
        "title": "Video / Document",
        "field": "medicalConditionTypeVideoUrl",
        "cellTemplate":showVideo()
    });
    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}

function showVideo(){
    var node = '<div style="margin-top:-4px" ng-show="((row.entity.medicalConditionTypeVideoUrl))" class="textAlign">';
    node += '<img id="{{row.entity.id}}" src="../../img/AppImg/HosImages/video.png"  class="cusrsorStyle" style="width: 29px;" onClick="onClickDietYoutube(event)">';
    node += '</div>';
    node += '<div style="margin-top:-4px" ng-show="((row.entity.medicalConditionTypeVideoUrl ==  \'\'))" class="textAlign">';
    node += '<img id="{{row.entity.id}}" src="../../img/img-icon.png"  class="cusrsorStyle" style="width: 29px;" onClick="onClickDietYoutube(event)">';
    node += '</div>';
    return node;
}
var imgImage = null;
function onClickDietYoutube(event){
    var dietVideoId = "", ext= "";
    var urlPath = "";
    if(imgImage){
        $(imgImage).removeClass("imgBorder");
    }

    if(event.currentTarget){
        imgImage = event.currentTarget;
        $(imgImage).addClass("imgBorder");
    }
    for(var i=0;i<dataArray.length;i++){
        var item = dataArray[i];
        if(item && item.id == event.currentTarget.id){
            urlPath = item.medicalConditionTypeVideoUrl;
            dietVideoId = item.medicalConditionTypeId;
            var fNameSplit = item.medicalConditionTypeCode.split('.');

            if(fNameSplit[1] != undefined && fNameSplit[1] != "") {
                ext = fNameSplit[1];
            }
            break;
        }
    }
    // if(urlPath && urlPath.indexOf("www.youtube.com")>=0){
            playYouTubeFile(urlPath, dietVideoId, ext);
    // }
}

function playYouTubeFile(urlPath, fileId, ext) {
    var popW = 900;
    var popH = 580;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Video";
    parentRef.sType = "illness";
    parentRef.fileType = "Video";
    parentRef.extension = ext;
    if(urlPath == ""){
        var urlExtn =  '/homecare/download/medical-condition-types/?id='+fileId;
        urlPath = ipAddress +urlExtn+"&access_token="+sessionStorage.access_token+"&tenant=" + sessionStorage.tenant;
        parentRef.fileType = "file";
        profileLbl = "Image/Document";
    }
    parentRef.illUrlPath = urlPath;
    devModelWindowWrapper.openPageWindow("../../html/patients/showVideo.html", profileLbl, popW, popH, true, closeVideoScreen);
}

function closeVideoScreen(evt,returnData){
    //$(imgImage).removeClass("imgBorder");
    //$(imgImage).removeClass("textBorder");
}

function getIllnessData(dataObj) {
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            dataArray = dataObj.response.patientMedicalConditions || [];
            if (dataArray.length) {
                for (var i = 0; i < dataArray.length; i++) {
                    var ind = i + 1;
                    dataArray[i].idk = ind;
                    dataArray[i].illnessId = dataArray[i].id;
                }
            }
        } else {
            customAlert.error("error", dataObj.response.status.message);
        }
    }
    buildIllnessListGrid(dataArray);
}

function addIllnessData(opr){
    $('#viewDivBlock').hide();
    $('#divTypeDetails').show()
    $('.alert').remove();
    parentRef.operation = opr;
    operation = opr;
    if(opr == "add"){
        $('#tabContentTitle').text('Add Service User Diagnosis');
        $('#btnReset').show();
        $('#btnReset').trigger('click');

    }else {
        $('#tabContentTitle').text('Edit Service User Diagnosis');
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        if (selectedItems && selectedItems.length > 0) {
            var obj = selectedItems[0];

            $("#txtID").html("ID :" + obj.illnessId);
            IllnessId = obj.medicalConditionTypeId;
            $("#txtCondition").val(obj.medicalConditionTypeValue);
            $("#txtNotes").val(obj.notes);
            $("#txtRemarks").val(obj.remarks);

            $("#cmbStatus").val(obj.isActive);
        }
    }
}

function closeAction(evt,returnData) {
    if (returnData && returnData.status == "success") {
        var opr = returnData.operation;
        if (opr == "add") {
            customAlert.info("info", "Diagnosis saved successfully");
        } else {
            customAlert.info("info", "Diagnosis updated successfully");
        }
        init();
    }
}

function onClickActive() {
    $(".btnInActive").removeClass("radioButton-active");
    $(".btnActive").addClass("radioButton-active");
}

function onClickInActive() {
    $(".btnActive").removeClass("radioButton-active");
    $(".btnInActive").addClass("radioButton-active");
}

function searchOnLoad(status) {
    buildIllnessListGrid([]);
    var urlExtn = ipAddress + "/homecare/patient-medical-conditions/";
    if(status == "active") {
        urlExtn = urlExtn + "?is-active=1&is-deleted=0&fields=*,medicalConditionType.*&patientId="+patientId;
    }
    else if(status == "inactive") {
        urlExtn = urlExtn + "?is-active=0&is-deleted=1&fields=*,medicalConditionType.*&patientId="+patientId;
    }
    getAjaxObject(urlExtn,"GET",getIllnessData,onError);
}

function onClickSave() {
    var strNotes = $("#txtNotes").val();
    strNotes = $.trim(strNotes);

    var strRemarks = $("#txtRemarks").val();
    strRemarks = $.trim(strRemarks);
    var isDelete = 0;
    var dataArray = [];
    var dataObj = {};


    if($("#cmbStatus").val() != 1){
        isDelete = 1;
    }

    dataObj.isDeleted = isDelete;
    dataObj.isActive = $("#cmbStatus").val();
    dataObj.patientId = patientId;
    dataObj.medicalConditionTypeId = IllnessId;
    dataObj.notes = strNotes;
    dataObj.remarks = strRemarks;

    var dataUrl = ipAddress + "/homecare/patient-medical-conditions/";
    // getAjaxObject(dataUrl + '?is-active=1&fields=*,medicalConditionType.*&patientId='+patientId, "GET", getIllnessData, onError);
    var method = "POST";
    if (operation == UPDATE) {
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        if (selectedItems && selectedItems.length > 0) {
            var obj = selectedItems[0];
            method = "PUT";
            dataObj.id = obj.illnessId;
            dataObj.createdBy = obj.createdBy;
            dataObj.modifiedBy = Number(sessionStorage.userId);
        }
    }
    else{
        dataObj.createdBy = Number(sessionStorage.userId);
    }
    dataArray.push(dataObj);
    createAjaxObject(dataUrl + 'batch/', dataArray, method, onCreate, onError);
}


function onCreate(dataObj){
    var msg;
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            if(operation == ADD){
                msg = "Service User Diagnosis created successfully";
            }else{
                msg = "Service User Diagnosis updated successfully";
            }
            displaySessionErrorPopUp("Info", msg, function(res) {
                onClickReset();
                onClickCancel();
                searchOnLoad('active');
            })
        }else{
            customAlert.error("error", dataObj.response.status.message.replace("patient-medical-conditions","service user diagnosis"));
        }
    }
}

function OpenIllness(){
    var popW = 900;
    var popH = 500;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Diagnosis List";
    devModelWindowWrapper.openPageWindow("../../html/masters/medicalConditionList.html", profileLbl, popW, popH, true, closeMedicalConditionAction);
}

function closeMedicalConditionAction(evt, returnData) {
    if (returnData && returnData.status == "success") {
        $("#txtCondition").val(returnData.selItem.value);
        IllnessId = returnData.selItem.idk;
    }
}

function onClickReset() {
    if(operation == ADD) {
        operation = ADD;
    }
    else {
        operation = UPDATE;
    }
    IllnessId = "";
    $("#txtNotes").val("");
    $("#txtCondition").val("");
    $("#txtID").html("");
    $("#txtRemarks").val("");
}
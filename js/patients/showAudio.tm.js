var parentRef = null;
var selObj = null;
//var ipAddress = "http://192.168.0.106:8080";
//var ipAddress = "http://stage.timeam.com";
$(document).ready(function(){
	parentRef = parent.frames['iframe'].window;
});

$(window).load(function(){
	init();
	buttonEvents();
});
function init(){
	console.log(parentRef.dietId)
	var audio = document.getElementById('audioCtrl');
	var source = document.createElement('source');
	var sType = parentRef.sType;
	var urlPath = "";
	var videoUrl = "";
	var audioId = parentRef.audioId;
	var audioUrl = ipAddress+"/file-resource/download/"+audioId;
	
	source.setAttribute('src', audioUrl);
	audio.appendChild(source);
	audio.play();
}
/*<video controls="controls" 
    class="video-stream" 
    x-webkit-airplay="allow" 
    data-youtube-id="N9oxmRT2YWw"  
src="http://www.youtube.com/watch?v=OmxT8a9RWbE"
    ></video>*/
function buttonEvents(){
	$("#btnCancelDet").off("click");
	$("#btnCancelDet").on("click",onClickCancel);
}
function onClickOK(){
	popupClose(false);
}
function onClickCancel(){
	popupClose(false);
}
function popupClose(st){
	var onCloseData = new Object();
	onCloseData.status = st;
	var windowWrapper = new kendoWindowWrapper();
	windowWrapper.closePageWindow(onCloseData);
}

function handleGetError(e) {
	var errLbl = getLocaleStringWithDefault(DC_UI_COMM, 'ERROR_MESSAGE', "Error");
	if(e && e.RestData && e.RestData.Description){
		window.top.displayErrorPopUp(errLbl,e.RestData.Description);
	}
}
function handleGetHttpError(e) {
   var errLbl = getLocaleStringWithDefault(DC_UI_COMM, 'ERROR_MESSAGE', "Error");
   var errDesc = getLocaleStringWithDefault(DC_UI_COMM, 'HTTP_ERROR', 'Http Error');
	// window.top.displayErrorPopUp(errLbl,errDesc);
}
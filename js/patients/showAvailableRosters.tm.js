var angularPTUIgridWrapper = null;
var angularPTUIgridWrapper1 = null;
var facilityArr = [{Key:'Facility1',Value:'Facility1'},{Key:'Facility2',Value:'Facility2'}];

var doctorArr = [{Key:'Doctor1',Value:'Doctor1'},{Key:'Doctor2',Value:'Doctor2'}];

var appTypeArr = [{Key:'Type1',Value:'Type1'},{Key:'Type2',Value:'Type2'}];

var towArr = [{Key:'1',Value:'1'},{Key:'2',Value:'2'},{Key:'3',Value:'3'},{Key:'4',Value:'4'},{Key:'5',Value:'5'},{Key:'6',Value:'6'},{Key:'7',Value:'7'}];
var colors = ['red', 'blue', 'green', 'teal', 'rosybrown', 'tan', 'plum', 'saddlebrown'];

var appTypeArr = [{Key:'Home',Value:'Home'},{Key:'Live',Value:'Live'}];
var appTypeArr1 = [{Key:'Kms',Value:'Kms'},{Key:'Miles',Value:'Miles'}];

var DAY_VIEW = "DayView";
var appTypeArray = [];
var reasonArray = [];

var dArray = [];
var parentRef = null;

var startDT  = null;
var endDT = null;

var pid = "";
var pname = "";

var fid = "";
var fname = "";
var cntry = sessionStorage.countryName;
$(document).ready(function(){
	$(".roadsterContentBlock").css("padding","5px");
	$(".roaster-refineBlock").css("padding","4px 4px 0px 4px");

	if((cntry.indexOf("India")>=0) || (cntry.indexOf("United Kingdom")>=0)){
		$("#lblFacility").text("Location");
	}else if(cntry.indexOf("United State")>=0){
		$("#lblFacility").text("Facility");
	}
});

$(window).load(function(){
	parentRef = parent.frames['iframe'].window;
	parentRef.screenType = "patient";
	pid = parentRef.pid;
	pname = parentRef.pname;
	
	fid = parentRef.fid;
	fname = parentRef.fname;
	
	loaded();
});

function onFacilityChange(){
	
}

function adjustHeight(){
	var defHeight = 230;
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 100;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    try{angularPTUIgridWrapper.adjustGridHeight(cmpHeight);}catch(e){};
    //try{angularPTUIgridWrapper1.adjustGridHeight(cmpHeight);}catch(e){};
    $("#divTb").css({height: (cmpHeight) + 'px' });
    $("#divImage").css({height: cmpHeight + 'px' });
}
function loaded() {
	buttonEvents();
	init();
}

function init(){
	
	if(sessionStorage.clientTypeId == "2"){
		$("#lblPTT").text("Service User :");
	}
	    var dataOptionsPT = {
		        pagination: false,
		        paginationPageSize: 500,
				changeCallBack: onPTChange
		    }
	    
	    angularPTUIgridWrapper = new AngularUIGridWrapper("rGrid1", dataOptionsPT);
	    angularPTUIgridWrapper.init();
		 buildPatientRosterList([]);
	
	    adjustHeight();
	    
	  
	    onClickRPSearch();
	}

function getGenders(){
	 getAjaxObject(ipAddress + "/master/Gender/list?is-active=1", "GET", getGenderValueList, onError);
}
function getTableListArray(dataObj) {
    var dataArray = [];
    if (dataObj && dataObj.response && dataObj.response.codeTable) {
        if ($.isArray(dataObj.response.codeTable)) {
            dataArray = dataObj.response.codeTable;
        } else {
            dataArray.push(dataObj.response.codeTable);
        }
    }
    var tempDataArry = [];
    var obj = {};
    obj.desc = "";
    obj.zip = "";
    obj.value = "";
    obj.idk = "";
   // tempDataArry.push(obj);
    for (var i = 0; i < dataArray.length; i++) {
        if (dataArray[i].isActive == 1) {
            var obj = dataArray[i];
            obj.idk = dataArray[i].id;
            obj.status = dataArray[i].Status;
            tempDataArry.push(obj);
        }
    }
    return tempDataArry;
}
function getGenderValueList(dataObj){
	var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
    	  var obj = {};
    	    obj.desc = "All";
    	    obj.zip = "";
    	    obj.value = "All";
    	    obj.idk = "All";
    	    dArray.unshift(obj);
    	   // tempDataArry.push(obj);
        setDataForSelection(dArray, "cmbGender", function(){}, ["desc", "idk"], 0, "");
    }
}
function getLanguages(){
	 getAjaxObject(ipAddress + "/master/Language/list?is-active=1", "GET", getLanguageValueList, onError);
}
function getLanguageValueList(dataObj) {
    var dArray = getTableListArray(dataObj);
    if (dArray && dArray.length > 0) {
    	var required = $("#cmbLang").kendoMultiSelect({
            itemTemplate: "<div style='width:100px;float:left'>#:data.desc# </div>",
            autoClose: false,
            dataSource: dArray,
            dataTextField: "desc",
            dataValueField: "idk",
          }).data("kendoMultiSelect");
    	//setDataForSelection(dArray, "cmbLang", function(){}, ["desc", "idk"], 0, "");
    }
}
	function onGetPatientInfo(dataObj){
		if (dataObj.response.patient.dateOfBirth) {
	        var dt = new Date(dataObj.response.patient.dateOfBirth);
	        if (dt) {
	        	var strDT = "";
	        	var cntry = sessionStorage.countryName;
	        	if(cntry.indexOf("India")>=0){
	        		strDT = kendo.toString(dt, "dd/MM/yyyy");
	        	}else if(cntry.indexOf("United Kingdom")>=0){
	        		strDT = kendo.toString(dt, "dd/MM/yyyy");
	        	}else{
	        		strDT = kendo.toString(dt, "MM/dd/yyyy");
	        	}
	        }
	        viewpDT = strDT;
	        var strAge = getAge(dt);
	        viewPTAge = strAge;
	        var strName = dataObj.response.patient.id+" - "+dataObj.response.patient.lastName+" "+dataObj.response.patient.firstName+"  "+dataObj.response.patient.middleName;
	        $("#lblName").html(strName);
	        var dob = strDT+" ("+strAge+" Y)"+", "+dataObj.response.patient.gender;
	        $("#lblDOB").html(dob);
	        if (dataObj && dataObj.response && dataObj.response.communication) {
	        	 var commArray = [];
	             if ($.isArray(dataObj.response.communication)) {
	                 commArray = dataObj.response.communication;
	             } else {
	                 commArray.push(dataObj.response.communication);
	             }
	             var comObj = commArray[0];
	             commId = comObj.id;
	             var addr = comObj.address1+","+comObj.address2;//+','+comObj.zip+"\n"+comObj.homePhone+","+comObj.cellPhone;
	             $("#lblAddr").html(addr);
	             var strZip = comObj.city+","+comObj.state+","+comObj.zip;
	             $("#lblZip").html(strZip);
	             $("#lblhp").html("<b>Home Phone :</b> "+comObj.homePhone);
	             $("#lblcell").html("<b>Cell Phone :</b> "+comObj.cellPhone);
	        }
		}
	       // $("#txtAge").val(strAge);
	}

function getFacilityList(dataObj){
	var dataArray = [];
	if(dataObj){
		if($.isArray(dataObj.response.facility)){
			dataArray = dataObj.response.facility;
		}else{
			dataArray.push(dataObj.response.facility);
		}
	}
	var tempDataArry = [];
	for(var i=0;i<dataArray.length;i++){
		dataArray[i].idk = dataArray[i].id; 
		dataArray[i].Status = "InActive";
		if(dataArray[i].isActive == 1){
			dataArray[i].Status = "Active";
		}
		if(parentRef.facilityId == dataArray[i].idk){
			$("#txtRLocation").val(dataArray[i].name);
		}
	}
	// setDataForSelection(dataArray, "txtRFacility", onFacilityChange, ["name", "idk"], 0, "");
	// getComboListIndex("txtRFacility","idk",fid);
	// var txtRFacility = $("#txtRFacility").data("kendoComboBox");
	// if(txtRFacility){
	// 	//txtRFacility.enable(false);
	// }
}
function getComboListIndex(cmbId, attr, attrVal) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb) {
        var ds = cmb.dataSource;
        var totalRec = ds.total();
        for (var i = 0; i < totalRec; i++) {
            var dtItem = ds.at(i);
            if (dtItem && dtItem[attr] == attrVal) {
                cmb.select(i);
                return i;
            }
        }
    }
    return -1;
}
function onFacilityChange(){
	
}
	var dtArray = [];
	function onErrorMedication(errobj){
		console.log(errobj);
	}
	function onError(errorObj){
		console.log(errorObj);
	}

	function onshowImage(){
		  $("#imgPhoto").attr( "src", "../../img/AppImg/HosImages/male_profile.png" );
	}
function buttonEvents(){
	  $("#imgPhoto").error(function() {
		   onshowImage();
       });
       

	  
	$("#btnRClose").off("click");
	$("#btnRClose").on("click",onClickCancel);
	
	$("#btnRClose1").off("click");
	$("#btnRClose1").on("click",onClickCancel);
	
	$("#btnRAdd").off("click",onClickAddRoster);
	$("#btnRAdd").on("click",onClickAddRoster);
	
	$("#btnREdit").off("click",onClickEditRoster);
	$("#btnREdit").on("click",onClickEditRoster);
	
	$("#btnRDel").off("click",onClickDeleteRoster);
	$("#btnRDel").on("click",onClickDeleteRoster);
	
	$("#btnRRoster").off("click",onClickCreateRoster);
	$("#btnRRoster").on("click",onClickCreateRoster);
	
	$("#btnRApp").off("click",onClickCreateRosterAppointment);
	$("#btnRApp").on("click",onClickCreateRosterAppointment);
	
	$("#btnGO").off("click",onClickGO);
	$("#btnGO").on("click",onClickGO);
	
	$("#btnRView").off("click",onClickViewRoster);
	$("#btnRView").on("click",onClickViewRoster);
	
	$("#btnRCopy").off("click",onClickViewCopy);
	$("#btnRCopy").on("click",onClickViewCopy);
	
	//$("#btnRSearch1").off("click",onClickRSearch);
	//$("#btnRSearch1").on("click",onClickRSearch);
	
	//$("#btnRSearch").off("click",onClickRPSearch);
	//$("#btnRSearch").on("click",onClickRPSearch);
	
	
}
//$(window).resize(adjustHeight);

function onClickGO(){
	var cmbGender = $("#cmbGender").data("kendoComboBox");
	 var required = $("#cmbLang").data("kendoMultiSelect");
	 var dkArray = [];
		if(required){
			var arr = required.listView._dataItems
			for(var i=0;i<arr.length;i++){
				var item = arr[i];
				if(item){
					dkArray.push(item.desc);
				}
			}
		}
		if(dkArray.length>0){
			var strDesc = dkArray.join(",");
			var txtRange = $("#txtRadius").val();
			txtRange = Number(txtRange);
			var txtRows = $("#txtRows").val();
			txtRows = Number(txtRows);
			var cmbDist = $("#cmbDist").data("kendoComboBox");
			var cmbUOM = $("#cmbUOM").data("kendoComboBox");
			
			var gender = "";
			var strUrl = "";
			if(cmbGender.text() == "All"){
				strUrl = ipAddress + "/homecare/provider/search/?patient-id="+pid+"&language=:lk:"+strDesc+"&range="+txtRange+"&rows="+txtRows+"&location="+cmbDist.text()+"&unit="+cmbUOM.text();
			}else{
				strUrl = ipAddress + "/homecare/provider/search/?patient-id="+pid+"&gender="+cmbGender.text()+"&language=:lk:"+strDesc+"&range="+txtRange+"&rows="+txtRows+"&location="+cmbDist.text()+"&unit="+cmbUOM.text();
			}
			
			 getAjaxObject(strUrl, "GET", getSearchValues, onError);
		}
		
}



function getSearchValues(dataObj){
	console.log(dataObj);
	var pdArray = [];
	if(dataObj && dataObj.response &&  dataObj.response.status && dataObj.response.status.code == 1){
		if($.isArray(dataObj.response.providers)){
			pdArray = dataObj.response.providers;
		}else{
			pdArray.push(dataObj.response.providers);
		}
	}
	$("#tracking-content").html("");
	var strHeader = "<tr>";
	strHeader = strHeader+'<th style="width:5%">ID</th>';
	strHeader = strHeader+'<th style="width:5%">Abbrevation</th>';
	strHeader = strHeader+'<th style="width:20%">Provider</th>';
	strHeader = strHeader+'<th>Gender</th>';
	strHeader = strHeader+'<th>Distance</th>';
	strHeader = strHeader+'</tr>';
	$("#tracking-content").append(strHeader)
	if(pdArray.length>0){
		for(var p=0;p<pdArray.length;p++){
			var pItem = pdArray[p];
			if(pItem){
				var strName = pItem.firstName+" "+pItem.middleName+" "+pItem.lastName;
				var strAddr = pItem.address1+" "+pItem.address2;
				var strData = "";
				strData = "<tr><td>"+pItem.id+"</td>";
				strData = strData+"<td>"+pItem.abbr+"</td>";
				strData = strData+"<td>"+strName+"<br><br>"+strAddr+"</td>";
				strData = strData+"<td>"+pItem.gender+"</td>";
				strData = strData+"<td>"+pItem.distance+"</td></tr>";
				$("#tracking-content").append(strData);
			}
		}
	}
}
var roseterPID = "";
var rosterPName = "";
var deletedCount = 0;
var selectedRecCount = 0;
function onClickDeleteRoster(){
	 var selectedAllItems = angularPTUIgridWrapper.getAllRows();
	 if(selectedAllItems && selectedAllItems.length>0){

		 customAlert.confirm("Confirm", "Are you sure you want delete?",function(response){
			 if(response.button == "Yes"){

				var selectedItems = $.grep(selectedAllItems,function(e){
						return e.entity.SEL === true;
				});

				if (selectedItems !== null && selectedItems.length > 0) {
					selectedRecCount = selectedItems.length;


					for (var i = 0;i < selectedItems.length; i++){

						var obj = selectedItems[i].entity;
						if(obj.idk && obj.SEL){

							deletedCount = deletedCount + 1;

							var rosterObj = {};
							rosterObj.id = obj.idk;
							rosterObj.modifiedBy = Number(sessionStorage.userId);;
							rosterObj.isActive = 0;
							rosterObj.isDeleted = 1;
							var dataUrl = ipAddress+"/patient/roster/delete";
								createAjaxObject(dataUrl,rosterObj,"POST",onPatientRosterDelete,onError);
						}else{
							angularPTUIgridWrapper.deleteItem(selectedItems[i]);
						}

					}
				}


			 }
		 });
	 }
}
function onPatientRosterDelete(dataObj){
	if (deletedCount === selectedRecCount) {
		onClickRPSearch();
		deletedCount = 0;
	}
}


var allRosterRows = [];
var allRosterRowCount = 0;
var allRosterRowIndex = 0;
function onClickCreateRoster(){
	allRosterRowCount = 0;
	allRosterRowIndex = 0;
	allRosterRows = angularPTUIgridWrapper.getAllRows();
	allRosterRowCount = allRosterRows.length;
	if(allRosterRowCount>0){
		saveRoster();
	}
}
function saveRoster(){
	if(allRosterRowCount>allRosterRowIndex){
		var objRosterEntiry = allRosterRows[allRosterRowIndex];
		var rosterObj = objRosterEntiry.entity;
		rosterObj.createdBy = Number(sessionStorage.userId);
		rosterObj.isActive = 1;
		rosterObj.isDeleted = 0;
		
		rosterObj.patientId = rosterObj.patientK;
		rosterObj.facilityId = rosterObj.facilityK;
		rosterObj.contractId = rosterObj.contractK;
		rosterObj.providerId = rosterObj.providerK;
		rosterObj.weekDay = rosterObj.dowK;
		rosterObj.fromTime = rosterObj.fromTimeK;
		rosterObj.toTime = rosterObj.toTimeK;
		rosterObj.careTypeId = 2;
		rosterObj.duration = rosterObj.duration;
		rosterObj.reason = rosterObj.reason;
		rosterObj.reasonId = rosterObj.reasonK;
		rosterObj.appointmentReason = rosterObj.reason;
		rosterObj.appointmentType = rosterObj.AppType;
		rosterObj.appointmentReasonId = rosterObj.reasonK;
		rosterObj.appointmentTypeId = rosterObj.AppTypeK;
		rosterObj.notes = rosterObj.notes;
		var dataUrl = "";
		if(rosterObj.idk){
			var rObj = {};
			rObj.id = rosterObj.idk;
			rObj.modifiedBy = Number(sessionStorage.userId);
			rObj.isActive = 1;
			rObj.isDeleted = 0;
			rObj.patientId = rosterObj.patientK;
			rObj.contractId = rosterObj.contractK;
			rObj.providerId = rosterObj.providerK;
			rObj.weekDay = rosterObj.dowK;
			rObj.fromTime = rosterObj.fromTimeK;
			rObj.toTime = rosterObj.toTimeK;
			rObj.careTypeId = rosterObj.careTypeId;
			rObj.duration = rosterObj.duration;
			rObj.reason = rosterObj.reason;
			rObj.reasonId = rosterObj.reasonK;
			rObj.notes = rosterObj.notes;
			rObj.facilityId = rosterObj.facilityK;
			rObj.appointmentReason = rosterObj.reason;
			rObj.appointmentType = rosterObj.AppType;
			rObj.appointmentReasonId = rosterObj.reasonK;
			rObj.appointmentTypeId = rosterObj.AppTypeK;
			
			dataUrl = ipAddress+"/patient/roster/update";
			createAjaxObject(dataUrl,rObj,"POST",onPatientRosterCreate,onError);
		}else{
			dataUrl = ipAddress+"/patient/roster/create";
			createAjaxObject(dataUrl,rosterObj,"POST",onPatientRosterCreate,onError);
		}
	}
}
function onPatientRosterCreate(dataObj){
	allRosterRowIndex = allRosterRowIndex+1;
	if(allRosterRowCount == allRosterRowIndex){
		customAlert.error("Info", "Roster created successfully");
		buildPatientRosterList([]);
		onClickRPSearch();
		//getPatientRosterList();
	}else{
		saveRoster();
	}
}

function onClickCarePlan(){
	
}
function onClickCreateRosterAppointment(){
	if(pid){
		var allRosterRows = angularPTUIgridWrapper.getAllRows();
		if(allRosterRows.length>0){
			var popW = "60%";
		    var popH = "60%";
		    var profileLbl;
		    var devModelWindowWrapper = new kendoWindowWrapper();
		    profileLbl = "Create  Appointment";
		    parentRef.pid = pid;
		    parentRef.allRosterRows = allRosterRows;
		    devModelWindowWrapper.openPageWindow("../../html/patients/createRosterAppointment.html", profileLbl, popW, popH, true, closeRosterAppointment);
		}else{
			customAlert.info("error", "Roster is Empty. First Create Roster");
		}
	}
}
function closeRosterAppointment(evt,returnData){
	getPatientRosterAppointments();
}
function getPatientRosterAppointments(){
	var patientRosterURL = ipAddress+"/patient/roster/assignment/list/?patient-id="+pid;
	getAjaxObject(patientRosterURL,"GET",onGetRosterAppList,onErrorMedication);
}
function onGetRosterAppList(dataObj){
	var ptArray = [];
	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		if(dataObj.response.patientRosterAssignment){
			if($.isArray(dataObj.response.patientRosterAssignment)){
				ptArray = dataObj.response.patientRosterAssignment;
			}else{
				ptArray.push(dataObj.response.patientRosterAssignment);
			}
		}
	}
	
	if(ptArray.length>0){
		var pItem = ptArray[0];
		if(pItem){
			var dtFMT = null;
			var cntry = sessionStorage.countryName;
			 if(cntry.indexOf("India")>=0){
				 dtFMT = INDIA_DATE_FMT;
			 }else  if(cntry.indexOf("United Kingdom")>=0){
				 dtFMT = ENG_DATE_FMT;
			 }else  if(cntry.indexOf("United State")>=0){
				 dtFMT = US_DATE_FMT;
			 }
			$("#txtRCR").text(pItem.createdByUser);
			var dt = kendo.toString(new Date(pItem.createdDate),dtFMT);
			$("#txtRCD").text(dt);
			var st = kendo.toString(new Date(pItem.fromDate),dtFMT);
			var ed = kendo.toString(new Date(pItem.toDate),dtFMT);
			$("#txtRCA").text(st);
			$("#txtRCE").text(ed);
		}
	}
}
function onClickEditRoster(){
	 var selectedItems = angularPTUIgridWrapper.getSelectedRows();
	 if(selectedItems && selectedItems.length>0){
		 var popW = "78%";
		    var popH = "80%";
		    var profileLbl;
		    var devModelWindowWrapper = new kendoWindowWrapper();
		    profileLbl = "Edit Roster";
		    parentRef.RS = "edit";
		    parentRef.selRSItem = selectedItems[0];
		    showPatientRoster();
		    parentRef.ds = getDataGridArray();
		    devModelWindowWrapper.openPageWindow("../../html/patients/createRoster.html", profileLbl, popW, popH, true, closePatientRoster);
	 }
}

function showPatientRoster(){
	
	// var txtRFacility = $("#txtRFacility").data("kendoComboBox");
	// parentRef.fid = txtRFacility.value();
	// parentRef.fname = txtRFacility.text();
	
	/*parentRef.pid = txtRPatient.value();
	parentRef.pname = txtRPatient.text();
	
	parentRef.fid = txtRFacility.value();
	parentRef.fname = txtRFacility.text();*/
}
function onClickViewRoster(){
	var popW = "80%";
    var popH = "80%";
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "View Roster";
    parentRef.RS = "create";
    parentRef.selRSItem = null;
    parentRef.ds = getDataGridArray();
    showPatientRoster();
    devModelWindowWrapper.openPageWindow("../../html/patients/viewRoster.html", profileLbl, popW, popH, true, closePatientViewRoster);
    parent.setPopupWIndowHeaderColor("0bc56f","FFF");
}
function closePatientViewRoster(){
	
}
function onClickViewCopy(){
	//alert("copy");
	 var selGridData = angularPTUIgridWrapper.getAllRows();
	 var selList = [];
	    for (var i = 0; i < selGridData.length; i++) {
	        var dataRow = selGridData[i].entity;
	        if(dataRow.SEL){
	        	selList.push(dataRow);
	        }
	    }  
	    if(selList.length>0){
	    	  var flag =  isSameWeekStart(selList);
	   	   console.log(flag);
	   	   if(flag){
	   		   console.log(selList);
		   		 parentRef.selList = selList;
		   		var popW = "60%";
		   	    var popH = "80%";
		   	    var profileLbl;
		   	    var devModelWindowWrapper = new kendoWindowWrapper();
		   	    profileLbl = "Copy Roster";
		   	    parentRef.RS = "copy";
		   	    parentRef.ds = getDataGridArray();
		   	 showPatientRoster();
		   	 var fItem = selList[0];
		   	 if(fItem.doeK != fItem.dowK){
		   		 devModelWindowWrapper.openPageWindow("../../html/patients/copyRoster.html", profileLbl, popW, popH, true, copyPatientRoster);
		   	 }else{
		   		var popW = "40%";
		   	    var popH = "50%";
		   	    devModelWindowWrapper.openPageWindow("../../html/patients/multyCopyRoster.html", profileLbl, popW, popH, true, multyCopyPatientRoster);
		   	 }
		   	   
		   	    parent.setPopupWIndowHeaderColor("0bc56f","FFF");
	   	   }else{
	   		   customAlert.info("error", "Please select same staff,start & end day of the week");
	   	   }
	    }
}
function multyCopyPatientRoster(evr,obj){
	console.log(obj);
	if(obj.status == "true"){
		var popW = "75%";
   	    var popH = "80%";
   	    var profileLbl;
   	    var devModelWindowWrapper = new kendoWindowWrapper();
   	    profileLbl = "Copy Roster";
   	    parentRef.RS = "copy";
		 devModelWindowWrapper.openPageWindow("../../html/patients/copyMultiRoster.html", profileLbl, popW, popH, true, copyPatientRoster);
	}
}
function copyPatientRoster(){
	onClickRPSearch();
}
function isSameWeekStart(list){
	var flag = false;
	if(list.length == 1){
		return true;
	}
	var fItem = list[0];
	for(var i=1;i<list.length;i++){
		var aItem = list[i];
		if(aItem.dowK == fItem.dowK && aItem.doeK == fItem.doeK && (aItem.providerK == fItem.providerK)){
			flag = true;
			//return true;
		}else{
			return false;
		}
	}
	return flag;
}
function onClickAddRoster(){
	//angularPTUIgridWrapper.insert({},0);
	var popW = "75%";
    var popH = "80%";
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Add Roster";
    parentRef.RS = "create";
    parentRef.selRSItem = null;
    parentRef.ds = getDataGridArray();
    showPatientRoster();
    devModelWindowWrapper.openPageWindow("../../html/patients/createRoster.html", profileLbl, popW, popH, true, closePatientRoster);
    parent.setPopupWIndowHeaderColor("0bc56f","FFF");
}

function getDataGridArray(){
	var allRows = angularPTUIgridWrapper.getAllRows();
	var items = [];
	for(var a=0;a<allRows.length;a++){
		var row = allRows[a].entity;
		items.push(row);
	}
	return items;
}
function closePatientRoster(evt,returnData){
	console.log(returnData);
	/*if(returnData && returnData.status == "success"){
		var obj = returnData;
		var st = returnData.rs;
		if(st == "create"){
			var strDOW = obj.dow1;
			var strDOWK = obj.dowK1;
			var dArray = [];
			for(var s=0;s<strDOW.length;s++){
				//var item = strDOW[s];
				obj.dow = strDOW[s];
				obj.dowK = strDOWK[s];
				var rdItem =   JSON.parse(JSON.stringify(obj));
				dArray.push(rdItem);
			}
			
			var dItems = getDataGridArray();
			dArray = dArray.concat(dItems);
			buildPatientRosterList([]);
			setTimeout(function(){
				buildPatientRosterList(dArray);
				angularPTUIgridWrapper.refreshGrid();
			},100);
			
		}else{
			 var selectedItems = angularPTUIgridWrapper.getSelectedRows();
			 var dgItem = selectedItems[0];
			 dgItem.contract = obj.contract;
			 dgItem.contractK = obj.contractK;
			 
			 dgItem.facility = obj.facility;
			 dgItem.facilityK = obj.facilityK;
			 
			 dgItem.provider = obj.provider;
			 dgItem.providerK = obj.providerK;
			 
			 dgItem.fromTime = obj.fromTime;
			 dgItem.fromTimeK = obj.fromTimeK;
			 
			 dgItem.toTime = obj.toTime;
			 dgItem.toTimeK = obj.toTimeK;
			 
			 dgItem.duration = obj.duration;
			 
			 dgItem.AppType = obj.AppType;
			 dgItem.AppTypeK = obj.AppTypeK;
			 
			 dgItem.reason = obj.reason;
			 dgItem.reasonK = obj.reasonK;
			 
			 dgItem.notes = obj.notes;
			 
			 dgItem.patient = obj.patient;
			 dgItem.patientK = obj.patientK;
			 
			 dgItem.dow = obj.dow;
			 dgItem.dowK = obj.dowK;
			 
			 angularPTUIgridWrapper.refreshGrid();
		}
		
		
	}*/
	
	onClickRPSearch();
}
function onRosterCreate(dataObj){
	console.log(dataObj);
}
function onClickRPSearch(){//patient-id
	/*var txtRPatient = $("#txtRPatient").data("kendoComboBox");
	if(txtRPatient){
		roseterPID = txtRPatient.value();
	}*/
	
	 $("#btnREdit").prop("disabled", true);
	 $("#btnRDel").prop("disabled", true);
	 
	if(pid != ""){
		var patientRosterURL = ipAddress+"/patient/roster/list/?patient-id="+pid+"&is-active=1";
	}else{
		var patientRosterURL = ipAddress+"/patient/roster/list/?is-active=1";
	}
	buildPatientRosterList([]);
	getAjaxObject(patientRosterURL,"GET",onGetRosterList,onErrorMedication);
}
function getPatientRosterList(){
    buildPatientRosterList([]);
	var patientRosterURL = ipAddress+"/patient/roster/list/?is-active=1";
	getAjaxObject(patientRosterURL,"GET",onGetRosterList,onErrorMedication);
}
function getWeekDayName(wk){
	var wn = "";
	if(wk == 1){
		return "Monday";
	}else if(wk == 2){
		return "Tuesday";
	}else if(wk == 2){
		return "Tuesday";
	}else if(wk == 3){
		return "Wednesday";
	}else if(wk == 4){
		return "Thursday";
	}else if(wk == 5){
		return "Friday";
	}else if(wk == 6){
		return "Saturday";
	}else if(wk == 0){
		return "Sunday";
	}
	return wn;
}
function onGetRosterList(dataObj){
	// console.log(dataObj);
    buildPatientRosterList([]);
	var rosterArray = [];
	if(dataObj && dataObj.response &&  dataObj.response.status && dataObj.response.status.code == 1){
		if($.isArray(dataObj.response.patientRoster)){
			rosterArray = dataObj.response.patientRoster;
		}else{
			rosterArray.push(dataObj.response.patientRoster);
		}
	}
	for(var i=0;i<rosterArray.length;i++){
		rosterArray[i].idk = rosterArray[i].id;
		rosterArray[i].contractK = rosterArray[i].contractId;
		rosterArray[i].contract = rosterArray[i].contract+"-"+rosterArray[i].contractRelationship;
		
		rosterArray[i].facility = rosterArray[i].facility;
		rosterArray[i].facilityK = rosterArray[i].facilityId;
		
		rosterArray[i].provider = rosterArray[i].provider;
		rosterArray[i].providerK = rosterArray[i].providerId;
		
		rosterArray[i].dow = getWeekDayName(rosterArray[i].weekDayStart);
		rosterArray[i].dowK = rosterArray[i].weekDayStart;
		
		rosterArray[i].doe = getWeekDayName(rosterArray[i].weekDayEnd);
		rosterArray[i].doeK = rosterArray[i].weekDayEnd;
		
		var sdt = new Date();
		sdt.setHours(0, 0, 0, 0);
		sdt.setMinutes(Number(rosterArray[i].fromTime));
		//var sdms = rosterArray[i].fromTime;
		rosterArray[i].fromTimeK = rosterArray[i].fromTime;
		rosterArray[i].fromTime = kendo.toString(sdt,"t");
		
		
		var edt = new Date();
		edt.setHours(0, 0, 0, 0);
		edt.setMinutes(Number(rosterArray[i].toTime));
		
		rosterArray[i].toTimeK = rosterArray[i].toTime;
		rosterArray[i].toTime = kendo.toString(edt,"t");
		
		rosterArray[i].duration = rosterArray[i].duration;
		
		rosterArray[i].AppType = rosterArray[i].appointmentType;
		rosterArray[i].AppTypeK = rosterArray[i].appointmentTypeId;
		
		rosterArray[i].reason = rosterArray[i].appointmentReason;
		rosterArray[i].reasonK = rosterArray[i].appointmentReasonId;
		
		rosterArray[i].dow = rosterArray[i].dow;
		rosterArray[i].dowK = rosterArray[i].dowK;
		
		rosterArray[i].patient = rosterArray[i].patientId;
		rosterArray[i].patientK = rosterArray[i].patientId;
	}

	buildPatientRosterList(rosterArray);
}
function buildPatientRosterList(dataSource){
	var gridColumns = [];
	var otoptions = {};
	otoptions.noUnselect = false;
	otoptions.rowHeight = 100; 
	gridColumns.push({
        "title": "Select",
        "field": "SEL",
        "cellTemplate": showCheckBoxTemplate(),
        "headerCellTemplate": "<input type='checkbox' class='check1' ng-model='TCSELECT' onclick='toggleSelectAll(event);' style='margin: 6px 0 0 12px !important; display: inline-block;'></input>",
        "width":"5%"
	});
    gridColumns.push({
        "title": "Week Id",
        "field": "weekId",
        "width": "2%",
        "cellTemplate": '<div class="ui-grid-cell-contents" >{{row.entity.weekId}}</div>'
    });
	  gridColumns.push({
	        "title": "Start",
	        "field": "dowK",
	        "width": "10%",
	        "cellTemplate": '<div class="ui-grid-cell-contents" >{{row.entity.dow}}</div>'
	    });
	  gridColumns.push({
	        "title": "Start Time",
	        "field": "fromTimeK",
	        "width": "10%",
	        "cellTemplate": '<div class="ui-grid-cell-contents" >{{row.entity.fromTime}}</div>'
	    });
	  gridColumns.push({
	        "title": "End",
	        "field": "doeK",
	        "width": "10%",
	        "cellTemplate": '<div class="ui-grid-cell-contents" >{{row.entity.doe}}</div>'
	    });
	  gridColumns.push({
	        "title": "End Time",
	        "field": "toTime",
	        "width": "8%",
	    });
	  gridColumns.push({
	        "title": "Dur",
	        "field": "duration",
	        "width": "8%",
	    });
	  gridColumns.push({
	        "title": "Billing",
	        "field": "billToName",
	        "width": "10%",
	    });
	  gridColumns.push({
	        "title": "Reason",
	        "field": "reason",
	        "width": "10%",
	    });
	  gridColumns.push({
	        "title": "Staff",
	        "field": "provider",
	        "width": "12%",
	    });
	if((cntry.indexOf("India")>=0) || (cntry.indexOf("United Kingdom")>=0)){
		gridColumns.push({
			"title": "Location",
			"field": "facility",
			"width": "12%",
		});
	}else if(cntry.indexOf("United State")>=0){
		gridColumns.push({
			"title": "Facility",
			"field": "facility",
			"width": "12%",
		});
	}

	   gridColumns.push({
	        "title": "Bill Rate",
	        "field": "patientBillingRate",
	        "width": "8%",
	    });
	   gridColumns.push({
	        "title": "Payout Rate",
	        "field": "payoutHourlyRate",
	        "width": "10%",
	    });
    // gridColumns.push({
    //     "title": "Notes",
    //     "field": "notes",
    //     "width": "20%",
    // });
 
    angularPTUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
}
function showCheckBoxTemplate(){
	var node = '<div style="text-align:center;padding-top: 0;"><input type="checkbox" class="check1" id="chkbox" ng-model="row.entity.SEL" style="width:15px;height:15px;margin:0px;"   onclick="onSelect(event);"></input></div>';
	return node;
}
function onSelect(evt){
	//console.log(evt);
}
function toggleSelectAll(e) {
    var checked = e.currentTarget.checked;
    var rows = angularPTUIgridWrapper.getScope().gridApi.core.getVisibleRows(); 
    for (var i = 0; i < rows.length; i++) {
        rows[i].entity["SEL"] = checked;
    }
}
function buildPatientRosterList1(dataSource){
	var gridColumns = [];
	var otoptions = {};
	otoptions.noUnselect = false;
	otoptions.rowHeight = 100; 
	gridColumns.push({
        "title": "Billing",
        "field": "contract",
        "width": "10%",
    });
	  gridColumns.push({
	        "title": "DOW",
	        "field": "dowK",
	        "width": "10%",
	        "cellTemplate": '<div class="ui-grid-cell-contents" >{{row.entity.dow}}</div>'
	    });
	  gridColumns.push({
	        "title": "Start",
	        "field": "fromTime",
	        "width": "8%",
	    });
	  gridColumns.push({
	        "title": "End",
	        "field": "toTime",
	        "width": "8%",
	    });
	  gridColumns.push({
	        "title": "Duration",
	        "field": "duration",
	        "width": "10%",
	    });
	  gridColumns.push({
	        "title": "Reason",
	        "field": "reason",
	        "width": "10%",
	    });
	  gridColumns.push({
	        "title": "Carrier",
	        "field": "provider",
	        "width": "12%",
	    });
	   gridColumns.push({
	        "title": "Facility",
	        "field": "facility",
	        "width": "12%",
	    });
    gridColumns.push({
        "title": "Notes",
        "field": "notes",
    });
 
    angularPTUIgridWrapper1.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
}
function onPTChange(){
	setTimeout(function(){
		var selectedItems = angularPTUIgridWrapper.getSelectedRows();
		 console.log(selectedItems);
		 if(selectedItems && selectedItems.length>0){
			 $("#btnREdit").prop("disabled", false);
			 $("#btnRDel").prop("disabled", false);
			 //$("#btnRCopy").prop("disabled", false);
		 }else{
			 $("#btnREdit").prop("disabled", true);
			 $("#btnRDel").prop("disabled", true);
			 //$("#btnRCopy").prop("disabled", true);
		 }
	},100)
}
function showOperations(){
	var node = '<div style="text-align:center"><span style="cursor:pointer;font-weight:bold;font-size:15px">Edit</span>&nbsp;&nbsp;<span style="cursor:pointer;font-weight:bold;font-size:15px">Delete</span>';
	node = node+"</div>";
	return node;
}
function onClickCancel(){
	popupClose(false);
}
function popupClose(obj){
	var windowWrapper = new kendoWindowWrapper();
	windowWrapper.closePageWindow(obj);
}


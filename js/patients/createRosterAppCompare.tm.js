var parentRef = null;
var operation = "";
var selItem = null;
var ADD = "add";
var UPDATE = "edit";
var VIEW = "view";
var DELETE = "delete";
var photoExt = "";
var pid = "";
var st = null;
var ed = null;
var days = "";
var leaveArray = null;
$(document).ready(function() {
	$("#kendoWindowContainer2_wnd_title",parent.document).css("top","0px");
    parentRef = parent.frames['iframe'].window;
    var pnlHeight = window.innerHeight;
});
 
$(window).load(function(){
	debugger;
	parentRef = parent.frames['iframe'].window;
	pid = parentRef.pid;
	st = parentRef.st;
	ed = parentRef.ed;
	days = parentRef.days;
	leaveArray = parentRef.leaveArray;
	//init();
	buttonEvents();
	getBillingDetails();
	
});

function buttonEvents(){
	$("#btnSave").off("click");
	$("#btnSave").on("click",onClickOK);
	
	$("#btnCancelDet").off("click");
	$("#btnCancelDet").on("click",onClickCancel);
}
function getBillingDetails(){
	getAjaxObject(ipAddress+"/carehome/patient-billing/?is-active=1&is-deleted=0&patientId="+pid+"&fields=*,shift.*,travelMode.*,billingType.*,billingPeriod.*,billTo.name,billingRateType.code","GET",getBillingDetailList,onError);
	
}
var patientBillDetails = [];
function getBillingDetailList(dataObj){
	patientBillDetails = [];
	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		if($.isArray(dataObj.response.patientBilling)){
			patientBillDetails = dataObj.response.patientBilling;
		}else{
			patientBillDetails.push(dataObj.response.patientBilling);
		}
	}else{
		//customAlert.error("Error",dataObj.response.status.message);
	}
	getAjaxObject(ipAddress+"/master/appointment_type/list/?is-active=1","GET",getAppointmentList,onError);
}
var appTypeArray = [];
function getAppointmentList(dataObj){
	var dArray = getTableListArray(dataObj);
	if(dArray && dArray.length>0){
		appTypeArray = dArray;
	}
	getAjaxObject(ipAddress+"/master/appointment_reason/list/?is-active=1","GET",getReasonList,onError);
}
var reasonArray = [];
function getReasonList(dataObj){
	var dArray = getTableListArray(dataObj);
	if(dArray && dArray.length>0){
		reasonArray = dArray;
	}
	getAjaxObject(ipAddress+"/provider/list/?is-active=1","GET",getProviderList,onError);
}
var providerArray = [];
var facilityId = "";
function getProviderList(dataObj){
	//console.log(dataObj);
	var dataArray = [];
	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		if($.isArray(dataObj.response.provider)){
			dataArray = dataObj.response.provider;
		}else{
			dataArray.push(dataObj.response.provider);
		}
	}else{
		//customAlert.error("Error",dataObj.response.status.message);
	}
	var tempDataArry = [];
	var prIds = [];
	for(var i=0;i<dataArray.length;i++){
		dataArray[i].idk = dataArray[i].id; 
		dataArray[i].Status = "InActive";
		if(dataArray[i].isActive == 1){
			dataArray[i].Status = "Active";
		}
		prIds.push( dataArray[i].id);
	}
	providerArray = dataArray;
	if(providerArray.length>0){
		facilityId = providerArray[0].facilityId;
	}
	var ipUrl = ipAddress+"/homecare/availabilities/?facility-id="+facilityId+"&parent-type-id=201&is-active=1&is-deleted=0&parent-id=:in:"+parentRef.prid;
	getAjaxObject(ipUrl,"GET",getProviderAvlList,onError);
	
}

function onClickShowRosters() {
	var popW = "90%";
	var popH = "60%";

	var profileLbl;
	var devModelWindowWrapper = new kendoWindowWrapper();

	parentRef.apptArr = apptArr;

	profileLbl = "Appointments";
	devModelWindowWrapper.openPageWindow("../../html/patients/showAvailableRosters.html", profileLbl, popW, popH, true, closePtRAddAction);
}

var providerAvlArray = [];
function getProviderAvlList(dataObj){
	console.log(dataObj);
	var dataArray = [];
	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		if($.isArray(dataObj.response.availabilities)){
			providerAvlArray = dataObj.response.availabilities;
		}else{
			providerAvlArray.push(dataObj.response.availabilities);
		}
	}else{
		//customAlert.error("Error",dataObj.response.status.message);
	}
	getPatientList();
}
function getPatientList(){
	// var patientListURL = ipAddress+"/patient/list/"+sessionStorage.userId;
	var patientListURL = ipAddress+"/patient/list/?is-active=1&is-deleted=0";
	// if(sessionStorage.userTypeId == "200" || sessionStorage.userTypeId == "100"){
	// 	patientListURL = ipAddress+"/patient/list/?is-active=1";
	// }else{
	//
	// }
	getAjaxObject(patientListURL,"GET",onPTPatientListData,onError);
}
var patientListArray = [];
function onPTPatientListData(dataObj){
	var tempArray = [];
	var dataArray = [];
	patientListArray = [];
	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		if(dataObj.response.patient){
			if($.isArray(dataObj.response.patient)){
				tempArray = dataObj.response.patient;
			}else{
				tempArray.push(dataObj.response.patient);
			}	
		}
	}else{
		customAlert.error("Error",dataObj.response.status.message);
	}
	patientListArray = tempArray;
	init();
}
function getTableListArray(dataObj){
	var dataArray = [];
	if(dataObj && dataObj.response && dataObj.response.codeTable){
		if($.isArray(dataObj.response.codeTable)){
			dataArray = dataObj.response.codeTable;
		}else{
			dataArray.push(dataObj.response.codeTable);
		}
	}
	var tempDataArry = [];
	var obj = {};
	obj.desc = "";
	obj.value = "";
	//tempDataArry.push(obj);
	for(var i=0;i<dataArray.length;i++){
		if(dataArray[i].isActive == 1){
			var obj = dataArray[i];
			//obj.idk = dataArray[i].id;
			//obj.status = dataArray[i].Status;
			tempDataArry.push(obj);
		}
	}
	return tempDataArry;
}
var appPatientArray = [];
var rosterDatesArray = [];

function getDayAvailable(day){
	for(var i=0;i<providerAvlArray.length;i++){
		var item = providerAvlArray[i];
		if(item && item.dayOfWeek == day){
			return true;
		}
	}
	return false;
}

function init(){
	var defHeight = 50;
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 100;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
	    
    $("#divTable").css({height: ((cmpHeight/2)) + 'px' });
    $("#ptscheduler").css({height: ((cmpHeight/2)) + 'px' });
    
    $("#divTable1").css({height: ((cmpHeight/2)) + 'px' });
    $("#ptscheduler1").css({height: ((cmpHeight/2)) + 'px' });
    var startDateView = kendo.toString(new Date(st),"dd-MMM-yyyy").toUpperCase();
 //   console.log(pid+","+startDateView+","+days);
    
    var patientListURL = "";
    if(parentRef.screenType == "patient"){
    	 patientListURL = ipAddress+"/appointment/by-patient/"+pid+"/"+startDateView+"/"+days+"/?is-active=1";
    }else{
    	 patientListURL = ipAddress+"/appointment/list/?facility-id="+parentRef.fid+"&provider-id="+parentRef.pid+"&to-date="+parentRef.edtime+"&from-date="+parentRef.sttime+"&is-active=1";
    }
   
   
	getAjaxObject(patientListURL,"GET",function(dataObj){
		//console.log(dataObj);
		var dtArray = [];
		var sdt = new Date(st);
		var fmt = getCountryDateFormat();
		var first = kendo.toString(new Date(st),fmt);
		//console.log(first);
		var dtObj = {};
		dtObj.day = getWeekDayName(sdt.getDay());
		dtObj.date = first;
		dtObj.dt = sdt;
		dtArray.push(dtObj);
		for(var i=0;i<days-1;i++){
			var currDate = sdt.getDate();
			currDate = currDate+1;
			var dt = st.setDate(currDate);
			dt = new Date(dt);
			var second = kendo.toString(dt,fmt);
			//console.log(second);
			//dtArray.push(second);
			//if(getDayAvailable(dt.getDay())){
				var dtObj = {};
				dtObj.day = getWeekDayName(dt.getDay());
				dtObj.dt = dt;
				dtObj.date = second;
				dtArray.push(dtObj);
				sdt = new Date(dt);
			//}
		}
		rosterDatesArray = dtArray;
		//console.log(dtArray);
		$("#ptscheduler").html("");
		var strTable = '<table class="table">';
		strTable = strTable+'<thead class="fillsHeader">';
		strTable = strTable+'<tr>';
		for(var d=0;d<dtArray.length;d++){
			var dItem = dtArray[d];
			if(dItem){
				strTable = strTable+'<th class="textAlign whiteColor">'+dItem.date +" ("+dItem.day+") "+'</th>';
			}
		}
		strTable = strTable+'</tr></thead>';
		var ptArray = [];
		if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
			if(dataObj.response.appointment){
				if($.isArray(dataObj.response.appointment)){
					ptArray = dataObj.response.appointment;
				}else{
					ptArray.push(dataObj.response.appointment);
				}
			}
		}
		appPatientArray = ptArray;
		for(var p=0;p<ptArray.length;p++){
			var item = ptArray[p];
			item.ST = "0";
			item.PR = "0";
		}
		var col = columnSequenceSort(ptArray);
		var rowArray = [];
		for(var q=0;q<ptArray.length;q++){
			var item = ptArray[q];
			if(item){
				var ms = item.dateOfAppointment;
				if(rowArray.toString().indexOf(ms) == -1){
					rowArray.push(ms);
				}
			}
		}
		//console.log(rowArray)
		strTable = strTable+'<tbody>';
		for(var r=0;r<rowArray.length;r++){
			strTable = strTable+'<tr>';
			for(var x=0;x<dtArray.length;x++){
				var sData = getPatientDayInfo(rowArray[r],dtArray[x].day,ptArray);
				//console.log(sData);
				strTable = strTable+'<td style="text-align:center">'+sData+'</td>';
			}
			strTable = strTable+'</tr>';
		}
		//strTable = strTable+'</tr>';
		//strTable = strTable+'</thead>';
		strTable = strTable+'</tbody>';
		strTable = strTable+'</table>';
		$("#ptscheduler").html(strTable);
		
		getRosterPatientInfo();
		
	});
}
function getRosterPatientInfo(){
	var patientRosterURL = "";
	 if(parentRef.screenType == "patient"){
		 patientRosterURL = ipAddress+"/patient/roster/list/?patient-id="+pid+"&is-active=1";
	 }else{
		 patientRosterURL = ipAddress+"/patient/roster/list/?provider-id="+parentRef.pid+"&is-active=1";
	 }
	
	getAjaxObject(patientRosterURL,"GET",onGetRosterList,onError);
}
var rosterArray = [];
var rosterAppArray = [];
var strLeave = "";
function onGetRosterList(dataObj){
	//console.log(dataObj);
	rosterArray = [];
	if(dataObj && dataObj.response &&  dataObj.response.status && dataObj.response.status.code == 1){
		if($.isArray(dataObj.response.patientRoster)){
			rosterArray = dataObj.response.patientRoster;
		}else{
			rosterArray.push(dataObj.response.patientRoster);
		}
	}
	rosterArray = parentRef.rArray;
	//console.log(rosterDatesArray);
	for(var j=0;j<rosterDatesArray.length;j++){
		var dItem = rosterDatesArray[j];
		if(dItem){
			for(var r=0;r<rosterArray.length;r++){
				var rItem = rosterArray[r];
				if(rItem){
					var weekDay = getWeekDayName(rItem.weekDayStart);//getRosterDay(rItem.weekDay);
					if(weekDay == dItem.day){
						var dt = new Date(dItem.dt);
						if(dt){
							dt.setHours(0,0,0);
							dt.setMinutes(Number(rItem.fromTime));
							var ft = new Date(dt);
							rItem.dateOfAppointment = ft.getTime();
							rItem.ST = "0";
							rItem.PR = "0";
							var rdItem =   JSON.parse(JSON.stringify(rItem));
							rosterAppArray.push(rdItem);
						}
					}
				}
			}
		}
	}
	
	//console.log(rosterAppArray);
	$("#ptscheduler1").text("");
	var strTable = '<table class="table">';
	strTable = strTable+'<thead class="fillsHeader">';
	
	strTable = strTable+'<tr>';
	for(var d=0;d<rosterDatesArray.length;d++){
		var dItem = rosterDatesArray[d];
		if(dItem){
			strTable = strTable+'<th class="textAlign whiteColor">'+dItem.date +" ("+dItem.day+") "+'</th>';
		}
	}
	var col = columnSequenceSort(rosterAppArray);
	var rowArray = [];
	var rowProviderArray = [];
	for(var p=0;p<rosterAppArray.length;p++){
		var item = rosterAppArray[p];
		if(item){
			var ms = item.dateOfAppointment;
			var dt = new Date(ms);
			if(rowArray.length == 0){
				rowArray.push(ms);
				rowProviderArray.push(item);
			}else{
				if(!isDateTimeExist(ms,rowArray,rowProviderArray,item)){
					rowArray.push(ms);
				}
			}
		}
	}
	//console.log(rowArray);
	patientAppExist = false;
	strLeave = "";
	for(var r=0;r<rowArray.length;r++){
		strTable = strTable+'<tr>';
		for(var x=0;x<rosterDatesArray.length;x++){
			strLeave = "";
			var bgColor = getPatientBGColor(rowArray);
			var trData = getRosterPatientDayInfo(rowArray[r],rosterDatesArray[x].dt,rosterAppArray);
			if(patientAppExist && trData){
				strTable = strTable+'<td style="text-align:center;background:#ef6251">'+trData+'</td>';
			}else if(strLeave == "leave"){
				strTable = strTable+'<td style="text-align:center;background:#FFFF00">'+trData+'</td>';
			}else{
				strTable = strTable+'<td style="text-align:center">'+trData+'</td>';
			}
			
		}
		strTable = strTable+'</tr>';
	}
	strTable = strTable+'</tr>';
	strTable = strTable+'</thead>';
	strTable = strTable+'<tbody>';
	strTable = strTable+'</tbody>';
	strTable = strTable+'</table>';
	$("#ptscheduler1").append(strTable);
	//console.log(rosterArray);
	//console.log(rosterDatesArray);
}
var patientAppExist = false;
function getPatientBGColor(){
	return "#ef6251";
}
function isDateTimeExist(ms,dtArray,rowPrArray,item){
	var dt = new Date(ms);
	for(var i=0;i<dtArray.length;i++){
		var edt = new Date(dtArray[i]);
		if(edt.getHours() == dt.getHours() && edt.getMinutes() == dt.getMinutes()){
			for(var r=0;r<rowPrArray.length;r++){
				var rItem = rowPrArray[r];
				if(rItem.providerId == item.providerId){
					return true;
				}
			}
			return false;
		}
	}
	return false;
}
function getWeekDayName(wk){
	var wn = "";
	if(wk == 1){
		return "Monday";
	}else if(wk == 2){
		return "Tuesday";
	}else if(wk == 2){
		return "Tuesday";
	}else if(wk == 3){
		return "Wednesday";
	}else if(wk == 4){
		return "Thursday";
	}else if(wk == 5){
		return "Friday";
	}else if(wk == 6){
		return "Saturday";
	}else if(wk == 0){
		return "Sunday";
	}
	return wn;
}
function getRosterDay(wk){
	for(var w=0;w<rosterDatesArray.length;w++){
		var wItem = rosterDatesArray[w];
		if(wItem){
			var dt = new Date(wItem.dt);
			if(dt.getDay() == wk){
				return dt;
			}
		}
	}
	return null;
}
function getRosterPatientDayInfo(ms,pDay,ptArray){
	var strData = "";
	for(var p=0;p<parentRef.apptArr.length;p++) {
		var Array = _.where(ptArray, {id: Number(parentRef.apptArr[p].rosterUpdateId)})
		// Array = _.where(Array, {ST: "0"})
		//console.log(ptArray[p]);
		// var item = ptArray[p];
		for (var a = 0; a < Array.length; a++){
			 var item = Array[a];
		if (item && item.ST == "0") {
			var dt = new Date(parentRef.apptArr[p].dateOfAppointment);
			//var days = getWeekDayName(dt.getDay());
			// if(days == pDay){
			if (dt.getDate() == pDay.getDate()) {
				// && ms == item.dateOfAppointment
				item.ST = "1";
				//strData = "1";
				var st1 = "";
				var ms = parentRef.apptArr[p].dateOfAppointment;
				var d1 = new Date(ms);
				var est = parentRef.apptArr[p].dateOfAppointment;
				var st = kendo.toString(new Date(est), "g");
				var eet = est + (item.duration * 60 * 1000);
				var et = kendo.toString(new Date(eet), "g");
				if (d1.getHours() > 12) {
					//pm++;
					st1 = "Evening Visit";
				} else {
					//am++;
					st1 = "Morning Visit";
				}
				var appIdk = item.id;
				var est = parentRef.apptArr[p].dateOfAppointment;
				var st = kendo.toString(new Date(est), "t");
				var eet = est + (item.duration * 60 * 1000);
				var et = kendo.toString(new Date(eet), "t");

				var appTYpe = item.appointmentType;
				var appTYpeV = getAppTypeValue(appTYpe, appTypeArray);
				var note = item.notes;
				var desc = item.appointmentReason;
				var descV = item.reason;//getAppTypeValue(desc, reasonArray);
				patientAppExist = isPatientAppExist(pDay, item.providerId, st, et);
				item.pt = patientAppExist;
				var provider = "";
				if (parentRef.screenType == "patient") {
					provider = getProviderNames1(ms, pDay, ptArray, item.providerId);//getProviderNameisPatientAppExist(item.providerId);
				} else {
					provider = getPatientName(item.patientId);
				}

				strData = item.provider + "<br>";
				strData = strData + st + " - " + item.duration + "<br>";
				strData = strData + "" + desc + "<br>";
				strLeave = getPatientLeaveType(item.providerId, est, eet);
				strData = strData + "<b>" + strLeave + "</b><br>";
				//strData = strData+""+st1+"<br>";

				return strData;
			}
		}
	}
	}
	return "";
}

function getPatientLeaveType(pid,st,et){
	for(var p=0;p<leaveArray.length;p++){
		var pItem = leaveArray[p];
		if(pItem && pItem.parentId == pid){
			if((st>=pItem.acceptedFromDate && st<=pItem.acceptedToDate) || (et>=pItem.acceptedFromDate && et<=pItem.acceptedToDate)){
				return "leave";
			}else{
				
			}
		}
	}
	return "";
}
function getPatientName(pid){
	var pName = "";
	for(var k=0;k<patientListArray.length;k++){
		var pItem = patientListArray[k];
		if(pItem && pItem.id == pid){
			return pItem.lastName+" "+pItem.firstName;
		}
	}
	return "";
}
function isPatientAppExist(d1,providerId,pst,pet){
	for(var j=0;j<appPatientArray.length;j++){
		var jItem = appPatientArray[j];
		var est = jItem.dateOfAppointment;
		var st = kendo.toString(new Date(est), "t");
		var eet = est+(jItem.duration*60*1000);
		var et = kendo.toString(new Date(eet), "t");
		var pDay = new Date(est).getDay();
		var ppDay = getWeekDayName(pDay);

		var d1Time = d1.getTime();
		var d1Day = new Date(d1Time).getDay();
		var p1Day = getWeekDayName(d1Day);
		if(ppDay == p1Day && st == pst && et == pet &&  jItem.providerId == providerId && !jItem.pday1){
			jItem.pday1 = "1";
			return true;
		}
	}
	return false;
}

function getPatientDayInfo(ms,pDay,ptArray){
	var strData = "";
	for(var p=0;p<ptArray.length;p++){
		//console.log(ptArray[p]);
		var item = ptArray[p];
		if(item && item.ST == "0"){
			var dt = new Date(item.dateOfAppointment);
			var days = getWeekDayName(dt.getDay());
			//console.log(ms);
			if(days == pDay){//&& ms == item.dateOfAppointment
				item.ST = "1";
				//strData = "1";
				var st1 = "";
				var ms = item.dateOfAppointment;
				var d1 = new Date(ms);
				var est = item.dateOfAppointment;
				var st = kendo.toString(new Date(est), "g");
				var eet = est+(item.duration*60*1000);
				var et = kendo.toString(new Date(eet), "g");
				if(d1.getHours()>12){
					//pm++;
					st1 = "Evening Visit";
				}else{
					//am++;
					st1 = "Moring Visit";
				}
				var appIdk = item.id;
				var est = item.dateOfAppointment;
				var st = kendo.toString(new Date(est), "t");
				var eet = est+(item.duration*60*1000);
				var et = kendo.toString(new Date(eet), "t");
				
				var appTYpe = item.appointmentType;
				var appTYpeV = getAppTypeValue(appTYpe, appTypeArray);
				var note = item.notes;
				var desc = item.appointmentReason;
				var descV = getAppTypeValue(desc, reasonArray);
				var provider = "";
				if(parentRef.screenType == "patient"){
					 provider = getProviderName(item.providerId);//getProviderNames(ms,pDay,ptArray,item.providerId);//
				 }else{
					 provider = getPatientName(item.patientId);
				 }
				//var provider = getProviderNames(ms,pDay,ptArray,item.providerId);//getProviderName(item.providerId);
				strData = provider+"<br>";
				strData = strData+st+" - "+item.duration+"<br>";
				//strData = strData+""+item.duration+"<br>";
				//strData = strData+""+appTYpeV+"<br>";
				strData = strData+""+descV+"<br>";
				//strData = strData+""+st1+"<br>";
				
				return strData;
			}
		}
	}
	return "";
}
function getAppTypeValue(strV,arr){
	for(var i=0;i<arr.length;i++){
		var item = arr[i];
		if(item && item["value"] == strV){
			return item["desc"];
		}
	}
	return "";
}
function getProviderNames1(ms,pDay,ptArray,prID){
	var pNames = "";
	for(var p=0;p<ptArray.length;p++){
		var item = ptArray[p];
		if(item && item.PR == "0"){
			var ms = item.dateOfAppointment;
			var d1 = new Date(ms);
			if(getWeekDayName(d1.getDay()) == pDay ){//&& prID == item.providerId
				var provider = getProviderName( item.providerId);
				pNames = provider;//pNames+" "+provider+",";
				item.PR = "1";
				return pNames;
			}
		}
	}
	if(pNames.length>0){
		//pNames = pNames.substring(0, pNames.length-1);
	}
	return pNames;
}
function getProviderNames(ms,pDay,ptArray,prID){
	var pNames = "";
	for(var p=0;p<ptArray.length;p++){
		var item = ptArray[p];
		if(item && item.PR == "0"){
			var ms = item.dateOfAppointment;
			var d1 = new Date(ms);
			if(ms == ms && getWeekDayName(d1.getDay()) == pDay ){//&& prID == item.providerId
				var provider = getProviderName( item.providerId);
				pNames = pNames+" "+provider+",";
				item.PR = "1";
			}
		}
	}
	if(pNames.length>0){
		pNames = pNames.substring(0, pNames.length-1);
	}
	return pNames;
}
function getProviderName(pk){
	var idx = "";
	for(var p=0;p<providerArray.length;p++){
		var item = providerArray[p];
		if(item && item.idk == pk){
			return item.lastName+","+item.firstName;
		}
	}
	return "";
}
function columnSequenceSort(arrToSort){
	 arrToSort.sort(function (item1,item2) {
			//console.log(a,b);
			var seq1 = Number(item1.dateOfAppointment);
			var seq2 = Number(item2.dateOfAppointment);
			return (seq1-seq2);
           //return a[strObjParamToSortBy] > b[strObjParamToSortBy];
       });
}

function onError(errObj) {
    console.log(errObj);
    customAlert.error("Error", "Error");
}

function getPatientBillingDetails(pBid){
	for(var p=0;p<patientBillDetails.length;p++){
		var pItem = patientBillDetails[p];
		if(pItem && pItem.id == pBid){
			return pItem;
		}
	}
	return null;
}//patientBillingId
function onClickOK(){
	//console.log(rosterArray);
	var arr = [];

	patientAppExist = false;
	for(var r=0;r<rosterAppArray.length;r++){
		var rItem = rosterAppArray[r];
		if(rItem.pt == true){
			patientAppExist = true;
		}
		if(rItem && rItem.pt == false){
			//console.log(rItem);
			var obj = {};
			rItem.appointmentType = "Con";//rItem.appointmentTypeId;
			rItem.appointmentReason = rItem.appointmentReasonId;
			rItem.payoutHourlyRate = rItem.payoutHourlyRate;
			rItem.billToName = rItem.billToName;
			//rdItem.weekId = getWeekIdByDate(rItem.dateOfAppointment);
			var pItem = getPatientBillingDetails(rItem.patientBillingId);
			if(pItem){
				rItem.patientBillingRate = pItem.rate;
				rItem.shiftValue = pItem.shiftValue;
				rItem.billingRateType = pItem.billingRateTypeCode;
				//rItem.patientBillingRate = pItem.patientBillingRate;
				rItem.billingRateTypeCode = pItem.billingRateTypeCode;
				rItem.billingRateTypeId = pItem.billingRateTypeId;
				rItem.swiftBillingId = pItem.swiftBillingId;
				rItem.createdBy = Number(sessionStorage.userId);
			}
			var Array = _.where(parentRef.apptArr, {rosterUpdateId: Number(rItem.id)});
			var st="";
			var et="";
			if(Array){
				st= Array[0].dateOfAppointment;
				rItem.dateOfAppointment =st;
				et= Array[0].dateOfAppointment+(rItem.duration*60*1000);
			}
			var pid = rItem.providerId;
			if(rItem.providerId == null) {
				rItem.providerId = 0;
			}


			delete rItem.id ;
			delete rItem.modifiedBy ;
			delete rItem.modifiedDate ;

			var pL = getPatientLeaveType(pid,st,et);
			if(pL != ""){
				rItem.providerId = 0;
			}

			// var weekid = getWeekIdByDate(rItem.dateOfAppointment);
			//
			// if(parentRef.apptArr !==  null && parentRef.apptArr.length > 0){
			// 	var index = parentRef.apptArr.findIndex((e) => e.weekNumber === weekid && e.dayOfWeek === rItem.weekDayStart);
			//
			// 	if(index > -1){
			// 		rItem.dateOfAppointment = parentRef.apptArr[index].dateOfAppointment;
			// 	}
			// }


			arr.push(rItem);
			//obj.dateOfAppointment = nSec;
		}
	}
	// console.log(arr);
	successReponse = [];
	apptLenghth;
	responseLength = 0;
	apptLenghth = arr;
	if(arr.length>0) {
		if (arr.length > 0 && arr.length > 1) {
			for (var r = 0; r < arr.length; r++) {
				var req = [];
				req.push(arr[r]);
				var dataUrl = ipAddress + "/appointment/create";
				createAjaxObject(dataUrl, req, "POST", onCreate, onError);
			}
			/*if(patientAppExist){
                customAlert.confirm("Confirm", "Are you sure you want override service user appointments?",function(response){
                    if(response.button == "Yes"){
                        var dataUrl = ipAddress+"/appointment/update";
                        createAjaxObject(dataUrl,arr,"POST",onCreate,onError);
                    }
                });
            }else{
                var dataUrl = ipAddress+"/appointment/create";
                createAjaxObject(dataUrl,arr,"POST",onCreate,onError);
            }*/

		} else {
			var dataUrl = ipAddress + "/appointment/create";
			createAjaxObject(dataUrl, arr, "POST", onCreate, onError);
		}
	}
	else{
		customAlert.info("Info","Appointments are duplicate can not create");
	}


}

function getWeekId(dateOfAppointment){
	var weekId = 0;
	if(parentRef.apptArr !== null && parentRef.length > 0){

		var apptDate = new Date(dateOfAppointment);

		apptDate.setHours(0,0,0,0);

		var tmpWeek = parentRef.apptArr.filter(function(item){
			return item.dateOfAppointment === apptDate.getTime();
		});

		if(tmpWeek !== null && tmpWeek.length > 0){
			weekId = tmpWeek[0].weekNumber;
		}
		else{

		}
	}
	return weekId;
}

function getWeekIdByDate(dateOfAppointment) {
	var weekId = 0;


	var apptDate = new Date(dateOfAppointment);
	weekId = apptDate.getWeekNo();

	return weekId;
}


var successReponse = [];
var apptLenghth;
var responseLength = 0;
function savePatientRosterAssignment(){
	console.log(parentRef.dtobj);
	var obj = {};
	 if(parentRef.screenType == "provider"){
		 obj.providerId = parentRef.pid;
	 }else{
		 obj.patientId = pid;
	 }
	
	var rED = new Date(parentRef.dtobj.ed);
	var rST = new Date(parentRef.dtobj.st);
	obj.toDate = rED.getTime();
	obj.fromDate = rST.getTime();
	obj.isActive = 1;
	obj.isDeleted = 0;
	obj.createdBy = Number(sessionStorage.userId);
	
	dataUrl = ipAddress+"/patient/roster/assignment/create";
	createAjaxObject(dataUrl,obj,"POST",onPatientRosterCreate,onError);
}
function onPatientRosterCreate(dataObj){
	if(dataObj && dataObj.response &&  dataObj.response.status && dataObj.response.status.code == 1){
		/*customAlert.error("Info","Appointments created succeesfully");*/
		/*setTimeout(function(){
			popupClose({});
		},2000)*/
		if(apptLenghth.length > 1) {
			$("#lblSU").text("Service User : " + getPatientName(pid));
			var strTable = '<thead class="fillsHeader">';
			strTable = strTable + +'<tr style="height:20px">';

			strTable = strTable + '<th></th><th>Week Number</th> <th>Week</th> <th>Date & Time</th><th>Staff</th><th>Status</th></tr></thead><tbody>';
			for (var r = 0; r < successReponse.length; r++) {
				var msg;
				if (successReponse[r].code == 1) {
					msg = "<span style='color: green;'>Success</span>";
				} else {
					msg = "<span style='color: red;'>+successReponse[r].msg+</span>";
				}
				strTable = strTable + '<tr><td>' + (r + 1) + '</td><td>' + apptLenghth[r].weekId + '</td><td>' + successReponse[r].week + '</td><td>' + successReponse[r].dateOfAppointment + '</td><td>' + getProviderName(successReponse[r].providerId) + '</td><td>' + msg + '</td></tr>';

			}
			strTable = strTable + "</tbody>";
			$("#tblStaffWeeklyView").html(strTable);
			$("#appointmentStatus").show();
			setTimeout(function () {
				onClickCancel();
			}, 5000);
		}
		else{
			$("#appointmentStatus").hide();
			var msg = "Appointments created successfully";
			displaySessionErrorPopUp("Info", msg, function(res) {
				onClickCancel();
			});
		}
	}else{
		customAlert.error("Error",dataObj.response.status.message);
	}
}
function onCreate(dataObj){
	debugger;
	//customAlert.error("Info","Appointments created succeesfully");
	if(dataObj && dataObj.response &&  dataObj.response.status){
		if(apptLenghth.length > 1) {
			responseLength = responseLength + 1;
			var obj = {};
			obj.msg = dataObj.response.status.message;

			var date = new Date(GetDateTimeEditDay(dataObj.response.appointment[0].dateOfAppointment));
			var day = date.getDay();
			var dow = getWeekDayName(day);
			obj.dateOfAppointment = kendo.toString(new Date(dataObj.response.appointment[0].dateOfAppointment), "dd-MM-yyyy hh:mm tt");
			obj.week = dow;
			obj.weekid = dataObj.response.appointment[0].weekId;
			obj.code =  dataObj.response.status.code;
			successReponse.push(obj);
			if(responseLength == apptLenghth.length){
				savePatientRosterAssignment();
			}

		}
		else{
			savePatientRosterAssignment();
		}

		setTimeout(function(){
			
		},1000)
	}else{
		customAlert.error("Error",dataObj.response.status.message);
	}
	
}
function onClickCancel() {
    var obj = {};
    obj.status = "false";
    popupClose(obj);
}

function onClickSearch() {
    var obj = {};
    obj.status = "search";
    popupClose(obj);
}

function popupClose(obj) {
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}

function closePopup(id) {
	$('#' + id).hide();
}

Date.prototype.getWeekNo = function() {
	var firstDay = new Date(this.getFullYear(), this.getMonth(), 1).getDay();
    return Math.ceil((this.getDate() + firstDay)/7);
};

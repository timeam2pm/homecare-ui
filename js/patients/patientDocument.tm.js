var angularUIgridWrapper;
var parentRef = null;
var recordType = "1";
var dataArray = [];
var ADD = "add";
var UPDATE = "edit";
var VIEW = "view";
var DELETE = "delete";
var operation = "add";
var atID = "";
var ds  = "";
var patientId = "";
var appointmentdtFMT = "dd/MM/yyyy hh:mm tt";

var IsFlag = 0;
var relationdataArray = [];

var statusArr = [{Key:'Active',Value:'Active'},{Key:'InActive',Value:'InActive'}];

var fileTypeArray = [{Key:'jpg',Value:'jpg'},{Key:'jpeg',Value:'jpeg'},{Key:'png',Value:'png'},{Key:'gif',Value:'gif'},{Key:'doc',Value:'doc'},{Key:'pdf',Value:'pdf'}];

$(document).ready(function(){
    parentRef = parent.frames['iframe'].window;
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("dgridDocumentTypeList", dataOptions);
    angularUIgridWrapper.init();
    buildDeviceListGrid([]);

});


$(window).load(function(){
    loading = false;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
    onGetDocumentType();
    init();
    buttonEvents();
    adjustHeight();
}

function init(){
    $("#btnEdit").prop("disabled", true);
    $("#btnDelete").prop("disabled", true);
    $("#divTypeDetails").css("display","none");
    $(".topContainerPopup").css("display","");

    $("#txtFDate").kendoDateTimePicker({ format: appointmentdtFMT, interval: 15, value:new Date() });
    bindFileTypes();
    patientId = parentRef.patientId;//101
    buildDeviceListGrid([]);
    getAjaxObject(ipAddress+"/homecare/documents/?is-active=1&parentId="+patientId+"&fields=*,documentType.*&parent-type-id=500","GET",getPatientDocuments,onError);
}

function getDocumentTypes(dataObj){
    var tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.documentTypes){
            if($.isArray(dataObj.response.documentTypes)){
                tempCompType = dataObj.response.documentTypes;
            }else{
                tempCompType.push(dataObj.response.documentTypes);
            }
        }
    }

    for(var i=0;i<tempCompType.length;i++){
        tempCompType[i].idk = tempCompType[i].id;
        $("#cmbDocumentType").append('<option value="'+tempCompType[i].id+'">'+tempCompType[i].value+'</option>');
    }
}

function getPatientDocuments(dataObj){
    console.log(dataObj);
    var tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.documents){
            if($.isArray(dataObj.response.documents)){
                tempCompType = dataObj.response.documents;
            }else{
                tempCompType.push(dataObj.response.documents);
            }
        }
    }

    for(var i=0;i<tempCompType.length;i++){
        var val;
        tempCompType[i].idk = tempCompType[i].id;
        if(tempCompType[i].Name != '') {
            val = tempCompType[i].Name.split('.');
            tempCompType[i].fileName = val[0];
            if(val[1] != undefined && val[1] != "") {
                tempCompType[i].category = val[1];
            }
            else{
                tempCompType[i].category = " ";
            }

        }

        if(tempCompType[i].modifiedDate && tempCompType[i].modifiedDate != null){
            tempCompType[i].lastModified = GetDateEdit(tempCompType[i].modifiedDate);
        }
        else{
            tempCompType[i].lastModified = GetDateEdit(tempCompType[i].createdDate);
        }

        if(tempCompType[i].documentDate != null) {
            tempCompType[i].documentDateTime = GetDateTimeEdit(tempCompType[i].documentDate);
        }else{
            tempCompType[i].documentDateTime = "";
        }
    }
    buildDeviceListGrid(tempCompType);
}


function onError(errorObj){
    console.log(errorObj);
}

function buttonEvents(){

    $("#btnSave").off("click",onClickSave);
    $("#btnSave").on("click",onClickSave);

    $("#btnEdit").off("click",onClickEdit);
    $("#btnEdit").on("click",onClickEdit);

    $("#btnDelete").off("click",onClickDelete);
    $("#btnDelete").on("click",onClickDelete);

    $("#btnAdd").off("click");
    $("#btnAdd").on("click",onClickAdd);

    $("#btnCancel").off("click",onClickCancel);
    $("#btnCancel").on("click",onClickCancel);

    $("#btnReset").off("click",onClickReset);
    $("#btnReset").on("click",onClickReset);

    $("#fileElem").on("change", onSelectionFiles);

    $("#btnBrowse").off("click", onBrowseClick);
    $("#btnBrowse").on("click", onBrowseClick);

    $(".btnActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('active');
        onClickActive();
    });
    $(".btnInActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('inactive');
        onClickInActive();
    });
}

function onBrowseClick(e){
    // console.log(e);
    if(e.currentTarget && e.currentTarget.id){
        var btnId = e.currentTarget.id;
        var fileTagId = ("fileElem"+btnId);
        $("#fileElem").click();
    }
}

function searchOnLoad(status) {
    buildDeviceListGrid([]);
    if(status == "active") {
        var urlExtn = ipAddress + "/homecare/documents/?is-active=1&parentId="+patientId+"&fields=*,documentType.*&parent-type-id=500";
    }
    else if(status == "inactive") {
        var urlExtn = ipAddress + "/homecare/documents/?is-active=0&parentId="+patientId+"&fields=*,documentType.*&parent-type-id=500";
    }
    getAjaxObject(urlExtn,"GET",getPatientDocuments,onError);
}


function onSelectionFiles(event){
    console.log(event);
    files = event.target.files;
    fileName = "";
    var size = files[0].size;
    var type = files[0].type;
    var FUtype = $("#cmbCategory").val();
    if(type.toLowerCase() ==  "application/pdf"){
        type = "pdf";
    }
    if(type.toLowerCase() == "application/vnd.openxmlformats-officedocument.wordprocessingml.document"){
        type = "doc";
    }
    if(type.toLowerCase() == "image/png"){
        type = "png";
    }
    if(type.toLowerCase() == "image/gif"){
        type = "gif";
    }
    if(type.toLowerCase() == "image/jpeg"){
        type = "jpeg";
    }
    if(type.toLowerCase() == "image/jpg"){
        type = "jpg";
    }

    $('#txtFU').val("");
    var maxSize = 10485760;
    if(files){
        if(type != undefined && type != ""){
            if(type.toLowerCase() == FUtype.toLowerCase()) {
                if (files.length > 0) {
                    if (size != undefined && size < maxSize) {
                        fileName = files[0].name;
                        IsFlag = 1;
                        $('#txtFU').val(fileName);
                    }
                    else {
                        customAlert.error("Error", "File size should be less than 10MB");
                    }
                }
            }
            else{
                buttonEvents();
                customAlert.error("Error", "File extension is mismatched");
            }
        }
    }


}

function onClickAdd(){
    $("#billTitle").html("Add Service User Document");
    parentRef.operation = "add";
    $("#viewDivBlock").css("display","none");
    $("#divTypeDetails").css("display","");
    onClickReset();
}

function onClickActive() {
    $(".btnInActive").removeClass("radioButton-active");
    $(".btnActive").addClass("radioButton-active");
}

function onClickInActive() {
    $(".btnActive").removeClass("radioButton-active");
    $(".btnInActive").addClass("radioButton-active");
}

function onClickDelete(){
    customAlert.confirm("Confirm", "Are you sure to delete?",function(response){
        if(response.button == "Yes"){
            var dataObj = {};
            dataObj.createdBy = Number(sessionStorage.userId);
            dataObj.isDeleted = 1;
            dataObj.isActive = 0;
            dataObj.id = atID;
            var dataUrl = ipAddress +"/homecare/documents/?id="+ atID ;
            var method = "DELETE";
            createAjaxObject(dataUrl, dataObj, method, onCreateDelete, onError);
        }
    });
}

function onCreateDelete(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "Service User Document deleted successfully";
            customAlert.error("Info", msg);
            $("#txtName").val("");
            operation = ADD;
            init();
        }
    }
}
function onClickSave(){
    if($("#txtName").val() != "") {

        var txtFDate = $("#txtFDate").data("kendoDateTimePicker");
        var fDate = new Date(txtFDate.value());

        var dataObj = {};
        dataObj.createdBy = Number(sessionStorage.userId);
        dataObj.isDeleted = 0;
        dataObj.isActive = 1;
        dataObj.documentDate = fDate.getTime();
        dataObj.parentTypeId = 500;
        dataObj.documentTypeId = parseInt($("#cmbDocumentType").val());
        dataObj.parentId = patientId;
        // dataObj.patientId = patientId;
        dataObj.Name = $("#txtName").val()+'.'+ $("#cmbCategory").val();
        var dataUrl = ipAddress + "/homecare/documents/";
        var method = "POST";
        if (operation == UPDATE) {
            method = "PUT";
            dataObj.id = atID;
        }
        createAjaxObject(dataUrl, dataObj, method, onSaveFileResource, onError);
    }
    else{
        customAlert.error("Error","Please Fill All required fields");
    }
}

function onSuccess(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var dataObj = {};
            dataObj.createdBy = Number(sessionStorage.userId);
            dataObj.isDeleted = 0;
            dataObj.isActive = 1;
            dataObj.parentTypeId = 500;
            dataObj.documentTypeId = parseInt($("#cmbDocumentType").val());
            dataObj.parentId = patientId;
            // dataObj.patientId = patientId;
            dataObj.Name = $("#txtName").val();
            var dataUrl = ipAddress + "/homecare/upload/documents/&id="+atID;
            var method = "POST";

            createAjaxObject(dataUrl, dataObj, method, onCreate, onError);

        }else{
            customAlert.error("Error", dataObj.response.status.message);
        }
    }else{
        customAlert.error("Error", dataObj.response.status.message);
    }
}

function zipCreation(file){
    var zip = new JSZip();
    zip.file("Hello.txt", "Hello World\n");
    var img = zip.folder("images");
    img.file("smile.gif", imgData, {base64: true});
    zip.generateAsync({type:"blob"})
        .then(function(content) {
            // see FileSaver.js
            saveAs(content, "example.zip");
        });
}

function onSaveFileResource(dataObj){
    if(IsFlag == 1) {
        var documentId;
        if (dataObj && dataObj.response && dataObj.response.documents) {
            documentId = dataObj.response.documents.id;
        }
        else {
            documentId = atID;
        }

        var tagFileId = ("fileElem");
        var file = document.getElementById(tagFileId);
        var fileName = $("#txtFU").val();
        if (fileName != "") {
            if (file && file.files[0] && file.files[0].name) {
                fileName = file.files[0].name;
            }
        }

        var reqUrl = "";

        // var strDataObj = JSON.stringify(dataObj);
        var formData = new FormData();
        if (operation != UPDATE) {
            formData.append("file", file.files[0]);
        } else {
            formData.append("file", file.files[0]);
        }
        reqUrl = ipAddress + "/homecare/upload/documents/";

        reqUrl = reqUrl + "?access_token=" + sessionStorage.access_token + "&id=" + documentId;

        Loader.showLoader();
        $.ajax({
            url: reqUrl,
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,// "multipart/form-data",
            // contentType: "application/json",
            headers: {
                tenant: sessionStorage.tenant
            },
            success: function (data, textStatus, jqXHR) {
                if (typeof data.error === 'undefined') {
                    Loader.hideLoader();
                    onCreate(data);
                } else {
                    Loader.hideLoader();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                Loader.hideLoader();
            }
        });
    }
    else{
        onCreate(dataObj);
    }

}


function onCreate(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "Service User Document Created Successfully";
            if(operation == UPDATE){
                msg = "Service User Document Updated Successfully"
            }
            displaySessionErrorPopUp("Info", msg, function(res) {
                // $("#txtAT").val("");
                operation = ADD;
                init();
                onClickCancel();
            })
        }else{
            customAlert.error("Error", dataObj.response.status.message);
        }
    }
    onClickReset();
}

function onClickCancel(){
    $("#divTypeDetails").css("display","none");
    parentRef.operation = "add";
    $("#viewDivBlock").css("display","");
}

function adjustHeight(){
    var defHeight = 230;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    angularUIgridWrapper.adjustGridHeight(cmpHeight);
}

function buildDeviceListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "File Name",
        "field": "fileName"
    });
    gridColumns.push({
        "title": "File Type",
        "field": "category"
    });
    gridColumns.push({
        "title": "Document Type",
        "field": "documentTypeValue"
    });

    gridColumns.push({
        "title": "Document Date Time",
        "field": "documentDateTime"
    });

    gridColumns.push({
        "title": "Modified Date",
        "field": "lastModified"
    });
    gridColumns.push({
        "title": "View",
        "field": "View",
        "width": "10%",
        "cellTemplate": showVideoTemplate(),
    });

    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}

function showVideoTemplate() {
    var node = '<div style="text-align:center;">';
    node = node + '<img  src="../../img/AppImg/HosImages/pdf.png" class="videoIcon cusrsorStyle" onClick="onClickVideo()"></img>';
    // node = node + '<img  ng-hide="((row.entity.fileType ==\'\' || row.entity.fileType ==\'mp3\' || row.entity.fileType == \'MP3\' || row.entity.fileType == \'MP4\' || row.entity.fileType == \'mp4\'))" src="../../img/AppImg/HosImages/pdf.png" class="videoIcon cusrsorStyle" onClick="onClickVideo()"></img>';
    node = node + "</div>";
    return node;
}

function onClickVideo() {
    var selGridRow = angular.element($(event.currentTarget).parent()).scope();
    console.log(selGridRow);

    if (selGridRow && selGridRow.row && selGridRow.row.entity) {
        /*if(recordType == "1" || recordType == "2"){
         parentRef.sType = "diet";
         parentRef.dietId = selGridRow.row.entity.idk;
         }*/
        var popW = 900;
        var popH = 580;
        var profileLbl;
        var devModelWindowWrapper = new kendoWindowWrapper();
        // if (selGridRow.row.entity.fileType && selGridRow.row.entity.fileType.toLowerCase() == "pdf") {
        profileLbl = "Document";
        popW = 1100;
        parentRef.sType = "Document";
        parentRef.documentId = selGridRow.row.entity.idk;
        parentRef.category = selGridRow.row.entity.category;
        devModelWindowWrapper.openPageWindow("../../html/patients/showPatientDocumentList.html", profileLbl, popW, popH, true, closeVideoScreen);
        // } else if (selGridRow.row.entity.fileType && selGridRow.row.entity.fileType.toLowerCase() == "mp4") {
        //     profileLbl = "Video";
        //     parentRef.sType = "diet";
        //     parentRef.dietId = selGridRow.row.entity.idk;
        //     devModelWindowWrapper.openPageWindow("../../html/patients/showVideo.html", profileLbl, popW, popH, true, closeVideoScreen);
        // } else if (selGridRow.row.entity.fileType && selGridRow.row.entity.fileType.toLowerCase() == "mp3") {
        //     parentRef.audioId = selGridRow.row.entity.idk;
        //     var popW = 600;
        //     var popH = 200;
        //     var profileLbl;
        //     var devModelWindowWrapper = new kendoWindowWrapper();
        //     profileLbl = "Audio";
        //     devModelWindowWrapper.openPageWindow("../../html/patients/showAudio.html", profileLbl, popW, popH, true, closeVideoScreen);
        // }
    }

    //selGridRow.row.entity
}

function closeVideoScreen(evt, returnData) {

}

function onChange(){
    setTimeout(function() {
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if (selectedItems && selectedItems.length > 0) {
            var obj = selectedItems[0];
            atID = obj.idk;
            $("#btnEdit").prop("disabled", false);
            $("#btnDelete").prop("disabled", false);
        } else {
            $("#btnEdit").prop("disabled", true);
            $("#btnDelete").prop("disabled", true);
        }
    });
}

function onClickEdit(){
    $("#billTitle").html("Edit Service User Documents");
    $("#viewDivBlock").css("display","none");
    parentRef.operation = "edit";
    operation = "edit";
    $("#divTypeDetails").css("display","")
    var selectedItems = angularUIgridWrapper.getSelectedRows();
    if (selectedItems && selectedItems.length > 0) {
        var obj = selectedItems[0];
        atID = obj.idk;
        var txtFDate = $("#txtFDate").data("kendoDateTimePicker");
        if (txtFDate) {
            txtFDate.value(GetDateTimeEdit(obj.documentDate));
        }


        $("#txtID").html("ID : "+obj.idk);
        $("#cmbDocumentType").val(obj.documentTypeId);
        $("#txtName").val(obj.fileName);
        $("#cmbCategory").val(obj.category);
        $('#txtFU').val(obj.Name);
    }
}

var operation = "add";
var selFileResourceDataItem = null;

function onStatusChange(){
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if(cmbStatus && cmbStatus.selectedIndex<0){
        cmbStatus.select(0);
    }
}

function onComboChange(cmbId) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb && cmb.selectedIndex < 0) {
        cmb.select(0);
    }
}

function onClickReset() {
    if(operation == ADD) {
        operation = ADD;
    }
    else {
        operation = UPDATE;
    }
    $("#txtID").html("");
    $("#txtName").val("");
    $("#cmbCategory").val("");
    $("#txtFU").val("");
    $("#cmbDocumentType").val("");
}

function onGetDocumentType(){
    getAjaxObject(ipAddress+"/homecare/document-types/?is-active=1&is-deleted=0","GET",getDocumentTypes,onError);
}

function bindFileTypes(){
    for(var i=0;i<fileTypeArray.length;i++){
        $("#cmbCategory").append('<option value="'+fileTypeArray[i].Key+'">'+fileTypeArray[i].Value+'</option>');
    }
}
var angularPTUIgridWrapper = null;
var angularPTUIgridWrapper1 = null;
var parentRef = null;
var operation = "";
var selItem = null;
var ADD = "add";
var UPDATE = "edit";
var VIEW = "view";
var DELETE = "delete";
var photoExt = "";
var pid = "";
var allRosterRows = null;
var masterRosterArr = [];
var selWeeks = [];
var rostersAvail = [];
$(document).ready(function() {
    parentRef = parent.frames['iframe'].window;

    var pnlHeight = window.innerHeight;
});

var cntry = "";
var dtFMT = "dd/MM/yyyy";
$(window).load(function(){
	parentRef = parent.frames['iframe'].window;
	pid = parentRef.pid;
	 allRosterRows = parentRef.allRosterRows;
	init();
	buttonEvents();
});
function adjustHeight(){
	var defHeight = 205;
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 100;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    try{angularPTUIgridWrapper.adjustGridHeight(cmpHeight);
    angularPTUIgridWrapper1.adjustGridHeight(cmpHeight);
    }catch(e){};
}
function buttonEvents(){


	$("#ancShowRosters").off("click");
	$("#ancShowRosters").on("click",onClickShowRosters);

	$("#btnSave").off("click");
	$("#btnSave").on("click",onClickOK);

	$("#btnContinue").off("click");
	$("#btnContinue").on("click",onClickContinue);


	$("#btnCancelDet").off("click");
	$("#btnCancelDet").on("click",onClickCancel);

	$("#btnUpdateWeekNo").off("click",onClickUpdateWeekNo);
	$("#btnUpdateWeekNo").on("click",onClickUpdateWeekNo);
}


function onClickShowRosters() {
	var popW = "60%";
	var popH = "60%";

	var profileLbl;
	var devModelWindowWrapper = new kendoWindowWrapper();

	//parentRef.apptArr = apptArr;

	profileLbl = "Available";
	devModelWindowWrapper.openPageWindow("../../html/patients/showAvailableRosters.html", profileLbl, popW, popH, true, closePtRAddAction);
}
var startDT  = null;
var endDT = null;
var prId = [];
function init(){
	//$("#txtRSD").kendoDatePicker();
	//$("#txtRED").kendoDatePicker();
	 var dataOptionsPT = {
		        pagination: false,
		        paginationPageSize: 500,
				changeCallBack: onPTChange
		    }

	    angularPTUIgridWrapper = new AngularUIGridWrapper("rosterAppGrid", dataOptionsPT);
	    angularPTUIgridWrapper.init();
		 buildPatientRosterList([]);

		 angularPTUIgridWrapper1 = new AngularUIGridWrapper("rosterLAppGrid", dataOptionsPT);
		    angularPTUIgridWrapper1.init();
			 buildPatientRosterList1([]);

	    adjustHeight();
	cntry = sessionStorage.countryName;
	console.log(allRosterRows );
	 if(cntry.indexOf("India")>=0){
		 dtFMT = INDIA_DATE_FMT;
	 }else  if(cntry.indexOf("United Kingdom")>=0){
		 dtFMT = ENG_DATE_FMT;
	 }else  if(cntry.indexOf("United State")>=0){
		 dtFMT = US_DATE_FMT;
	 }
	 startDT = $("#txtRSD").kendoDatePicker({
         change: startChange,format:dtFMT
     }).data("kendoDatePicker");

     endDT = $("#txtRED").kendoDatePicker({
         change: endChange,format:dtFMT
     }).data("kendoDatePicker");

     startDT.min(new Date());
     endDT.min(new Date());

     $("#cmbCarer").kendoComboBox();
     var providers = [];
     prId = [];
     for(var i=0;i<allRosterRows.length;i++){
    	 var rItem = allRosterRows[i];
    	 if(rItem && rItem.entity){
    		 var rEntity = rItem.entity;
    		 if(rEntity){
    			 if(providers.length == 0){
    				 providers.push(rEntity.provider);
    				 prId.push(rEntity.providerId);
    			 }else if(providers.indexOf(rEntity.provider) == -1){
    				 providers.push(rEntity.provider);
    				 prId.push(rEntity.providerId);
    			 }
    		 }
    	 }
     }

     var carerArray = [];
     if(providers.length>0){
    	 for(var j=0;j<providers.length;j++){
    		 var item = providers[j];
    		 var obj = {};
    		 obj.Key = item;
    		 if(item == null){
				 obj.Value = "";
			 }else{
				 obj.Value = item;
			 }

    		 carerArray.push(obj);
    	 }
     }

	 setDataForSelection(carerArray, "cmbCarer", function(){}, ["Value", "Key"], 0, "");


	if(parentRef.pid){
		var patientRosterURL = ipAddress+"/patient/roster/list/?patient-id="+parentRef.pid+"&is-active=1";
	}

	getAjaxObject(patientRosterURL,"GET",onGetRosterList2,onError);
}


function onGetRosterList2(dataObj){
	var rosterArray = [];
	if(dataObj && dataObj.response &&  dataObj.response.status && dataObj.response.status.code == 1){
		if($.isArray(dataObj.response.patientRoster)){
			rosterArray = dataObj.response.patientRoster;
		}else{
			rosterArray.push(dataObj.response.patientRoster);
		}
	}

	masterRosterArr = rosterArray;


}

function getProviderNameById(pid){
	 for(var i=0;i<allRosterRows.length;i++){
    	 var rItem = allRosterRows[i];
    	 if(rItem && rItem.entity){
    		 var rEntity = rItem.entity;
    		 if(rEntity && rEntity.providerId == pid){
    			 return rEntity.provider;
    		 }
    	 }
	 }
	 return "";
}
function startChange() {
    var startDate = startDT.value(),
    endDate = endDT.value();

    if (startDate) {
        startDate = new Date(startDate);
        startDate.setDate(startDate.getDate());
        endDT.min(startDate);
    } else if (endDate) {
        startDT.max(new Date(endDate));
    } else {
        endDate = new Date();
        startDT.max(endDate);
        endDT.min(endDate);
    }
}

function endChange() {
    var endDate = endDT.value(),
    startDate = startDT.value();

    if (endDate) {
        endDate = new Date(endDate);
        endDate.setDate(endDate.getDate());
        startDT.max(endDate);
    } else if (startDate) {
        endDT.min(new Date(startDate));
    } else {
        endDate = new Date();
        startDT.max(endDate);
        endDT.min(endDate);
    }
}

function allowNumerics(ctrlId){
	$("#"+ctrlId).keypress(function (e) {
			 if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
					return false;
				}
   });
}
function onError(errObj) {
    console.log(errObj);
    customAlert.error("Error", "Error");
}
var isContinue = false;
function onClickOK(){
	var startDT = $("#txtRSD").data("kendoDatePicker");
	var endDT = $("#txtRED").data("kendoDatePicker");

	if(endDT.value() && startDT.value()) {
		isContinue = true;
		var patientRosterURL = "";
		if (parentRef.screenType == "patient") {
			patientRosterURL = ipAddress + "/patient/roster/list/?patient-id=" + pid + "&is-active=1";
		} else {
			patientRosterURL = ipAddress + "/patient/roster/list/?provider-id=" + parentRef.pid + "&is-active=1";
		}

		getAjaxObject(patientRosterURL, "GET", onGetRosterList, onErrorMedication);
	}
	else{
		customAlert.error("Error", "Please select date");
	}

}
var rosterArray = [];
function onGetRosterList(dataObj){
	//console.log(dataObj);
	rosterArray = [];
	if(dataObj && dataObj.response &&  dataObj.response.status && dataObj.response.status.code == 1){
		if($.isArray(dataObj.response.patientRoster)){
			rosterArray = dataObj.response.patientRoster;
		}else{
			rosterArray.push(dataObj.response.patientRoster);
		}
	}
	//console.log(rosterArray);

	var startDT = $("#txtRSD").data("kendoDatePicker");
	var endDT = $("#txtRED").data("kendoDatePicker");

	if(endDT.value() && startDT.value()){
		var ed = new Date(endDT.value());
		var st = new Date(startDT.value());

		st.setHours(0, 0, 0);
		ed.setHours(23, 59,59);

		patientListURL = ipAddress+"/appointment-block/list/?provider-id="+prId.toString()+"&from-date="+st.getTime()+"&to-date="+ed.getTime()+"&is-active=1";
		buildPatientRosterList([]);
		buildPatientRosterList1([]);
		 getAjaxObject(patientListURL,"GET",onPatientBlockDays,onErrorMedication);

	}
}
var rosterIdArr = [];
function onPatientBlockDays(dataObj){
	//console.log(dataObj);
	var dArray = [];
	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		if(dataObj.response.appointmentBlock){
			if($.isArray(dataObj.response.appointmentBlock)){
				dArray = dataObj.response.appointmentBlock;
			}else{
				dArray.push(dataObj.response.appointmentBlock);
			}
		}
	}

	var startDT = $("#txtRSD").data("kendoDatePicker");
	var endDT = $("#txtRED").data("kendoDatePicker");

	var ed = new Date(endDT.value());
	var st = new Date(startDT.value());

	st.setHours(0, 0, 0);
	ed.setHours(23, 59,59);

	var patientListURL = ipAddress+"/homecare/vacations/?parentId=:in:"+prId.toString()+"&parentTypeId=201&acceptedFromDate=:bt:"+st.getTime()+","+ed.getTime()+"&is-active=1";
	 getAjaxObject(patientListURL,"GET",onPatientLeaveDays,onErrorMedication);

	if(dArray.length == 0){
		$("#divBlockDays").hide();
		$("#divNavBar").hide();
		//onClickContinue();
	}else{
		$("#divBlockDays").show();
		$("#divNavBar").show();
		//customAlert.error("Error","Block Days");
		//console.log(dArray);
		var arr = [];
		for(var d=0;d<dArray.length;d++){
			var dItem = dArray[d];
			if(dItem){
				var obj = {};
				var st = dItem.startDateTime;
				obj.ProviderId = dItem.providerId;
				obj.idk = dItem.id;
				if(st){
					st = new Date(st);
					obj.dw = st.getDay();
					obj.startDate = dItem.startDateTime;
					obj.endDate = dItem.endDateTime;
					obj.Day = getWeekDayName(st.getDay());
					obj.SD =  kendo.toString(st, "dd/MM/yyyy");
					obj.ST =  kendo.toString(st, "hh:mm tt");
				}
				var et = dItem.endDateTime;
				if(et){
					et = new Date(et);
					obj.ED =  kendo.toString(et, "dd/MM/yyyy");
					obj.ET =  kendo.toString(et, "hh:mm tt");
				}
				if(dItem.composition && dItem.composition.provider){
					obj.provider = dItem.composition.provider.firstName +" "+dItem.composition.provider.middleName+" "+dItem.composition.provider.lastName;
				}
				arr.push(obj);
			}
		}
		rosterIdArr = [];
		for(var a=0;a<arr.length;a++){
			var dItem = arr[a];
			var st = dItem.startDate;
			var et = dItem.endDate;
			var dw = dItem.dw;
			var ProviderId = dItem.ProviderId;
			var flag = isRosterDayAvailable(dItem.idk,dw,st,et,ProviderId);
			//arr[a].status = flag.toString();
			console.log(dw,flag);
		}
		for(var b=0;b<arr.length;b++){
			var dItem = arr[b];
			arr[b].status = isExistBId(dItem.idk);
		}
		console.log(rosterIdArr);
		buildPatientRosterList(arr);
	}
}

var leaveArray = [];
function onPatientLeaveDays1(dataObj){
	var dArray = [];
	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		if(dataObj.response.vacations){
			if($.isArray(dataObj.response.vacations)){
				leaveArray = dataObj.response.vacations;
			}else{
				leaveArray.push(dataObj.response.vacations);
			}
		}
	}
	if(leaveArray.length == 0){
		$("#divBlockDays").hide();
		$("#divNavBar").hide();
		onClickContinue();
	}else{
		$("#divLeaveDays").show();
		$("#divNavBar").show();
		var arr = [];
		for(var d=0;d<leaveArray.length;d++){
			var dItem = leaveArray[d];
			if(dItem){
				var obj = {};
				var st = dItem.acceptedFromDate;
				obj.ProviderId = dItem.parentId;
				obj.provider = getProviderNameById(obj.ProviderId);
				obj.idk = dItem.id;
				if(st){
					st = new Date(st);
					obj.SD =  kendo.toString(st, "dd/MM/yyyy hh:mm tt");
				}
				var et = dItem.acceptedToDate;
				if(et){
					et = new Date(et);
					obj.ED =  kendo.toString(et, "dd/MM/yyyy hh:mm tt");
				}
				if(dItem.composition && dItem.composition.provider){
					obj.provider = dItem.composition.provider.firstName +" "+dItem.composition.provider.middleName+" "+dItem.composition.provider.lastName;
				}
				arr.push(obj);
			}
		}
	}
	buildPatientRosterList1(arr);
}
function onPatientLeaveDays(dataObj){
	//console.log(dataObj);
	var dArray = [];
	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		if(dataObj.response.vacations){
			if($.isArray(dataObj.response.vacations)){
				leaveArray = dataObj.response.vacations;
			}else{
				leaveArray.push(dataObj.response.vacations);
			}
		}
	}
	if(leaveArray.length == 0){
		$("#divBlockDays").hide();
		$("#divNavBar").hide();
		//onClickContinue();

		var startDT = $("#txtRSD").data("kendoDatePicker");
		var endDT = $("#txtRED").data("kendoDatePicker");

		var ed = new Date(endDT.value());
		var st = new Date(startDT.value());

		st.setHours(0, 0, 0);
		ed.setHours(23, 59,59);

		var patientListURL = ipAddress+"/homecare/vacations/?parentId=:in:"+prId.toString()+"&parentTypeId=201&acceptedToDate=:bt:"+st.getTime()+","+ed.getTime()+"&is-active=1";
		 getAjaxObject(patientListURL,"GET",onPatientLeaveDays1,onErrorMedication);

	}else{
		$("#divLeaveDays").show();
		$("#divNavBar").show();
		var arr = [];
		for(var d=0;d<leaveArray.length;d++){
			var dItem = leaveArray[d];
			if(dItem){
				var obj = {};
				var st = dItem.acceptedFromDate;
				obj.ProviderId = dItem.parentId;
				obj.provider = getProviderNameById(obj.ProviderId);
				obj.idk = dItem.id;
				if(st){
					st = new Date(st);
					obj.SD =  kendo.toString(st, "dd/MM/yyyy hh:mm tt");
				}
				var et = dItem.acceptedToDate;
				if(et){
					et = new Date(et);
					obj.ED =  kendo.toString(et, "dd/MM/yyyy hh:mm tt");
				}
				if(dItem.composition && dItem.composition.provider){
					obj.provider = dItem.composition.provider.firstName +" "+dItem.composition.provider.middleName+" "+dItem.composition.provider.lastName;
				}
				arr.push(obj);
			}
		}
	}
	buildPatientRosterList1(arr);
}

function isExistBId(bId){
	for(var b=0;b<rosterIdArr.length;b++){
		var bItem = rosterIdArr[b];
		if(bItem && bItem.b == bId){
			return "true";
		}
	}
	return "false";
}
function isExistRId(rId){
	for(var b=0;b<rosterIdArr.length;b++){
		var bItem = rosterIdArr[b];
		if(bItem && bItem.r == rId){
			return true;
		}
	}
	return false;
}
function isRosterDayAvailable(idk,dw,bst,bed,pId){
	for(var r=0;r<rosterArray.length;r++){
		var rItem = rosterArray[r];
		if(rItem && rItem.providerId == pId && rItem.weekDay == dw){
			var sdt = new Date(bst);
			sdt.setHours(0, 0, 0,0);
			//console.log(new Date(sdt));
			var rst = sdt.setMinutes(rItem.fromTime);
			sdt.setHours(0, 0, 0,0);
			var ret = sdt.setMinutes(rItem.toTime);
			console.log(new Date(bst),new Date(bed),new Date(rst),new Date(ret));
			if(rst>bst){
				if(rst>bed){
					return true;
				}
			}else if(rst<bst){
				if(ret<bst){
					return true;
				}
			}
			if(rosterIdArr.indexOf(rItem.id) == -1){
				rosterIdArr.push({r:rItem.id,b:idk});
				return false;
			}
		}
	}
	return false;
}

var rosterSt;
function onClickContinue(){
	var startDT = $("#txtRSD").data("kendoDatePicker");
	var endDT = $("#txtRED").data("kendoDatePicker");
	if(endDT.value() && startDT.value() && isContinue){
		var rosterCopyArray =  [];//JSON.parse(JSON.stringify(rosterArray));
		for(var r=0;r<rosterArray.length;r++){
			var flag = isExistRId(rosterArray[r].id);
			if(!flag){
				rosterCopyArray.push(rosterArray[r]);
			}
		}
	//	console.log(rosterCopyArray,rosterArray);
		var ed = new Date(endDT.value());
		var st = new Date(startDT.value());
		rosterSt = new Date(startDT.value());

		st.setHours(0, 0, 0);
		ed.setHours(23, 59,59);
		var timeDiff = Math.abs(ed.getTime() - st.getTime());
		var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

		parentRef.st = rosterSt;
		parentRef.ed = ed;

		parentRef.sttime = st.getTime();
		parentRef.edtime = ed.getTime();

		parentRef.days = diffDays;

		var aobj = {};
		aobj.st = st;
		aobj.ed = ed;
		parentRef.dtobj = JSON.parse(JSON.stringify(aobj));
		parentRef.prid = prId.toString();
		parentRef.rArray = rosterCopyArray;
		parentRef.leaveArray = leaveArray;


		// var popW = "60%";
		// var popH = "50%";

		// var profileLbl;
		// var devModelWindowWrapper = new kendoWindowWrapper();
		// profileLbl = "Change Week Number";
		// devModelWindowWrapper.openPageWindow("../../html/patients/changeWeekNumber.html", profileLbl, popW, popH, true, closePtRAddAction);



			//profileLbl = "Appointments";
		    //devModelWindowWrapper.openPageWindow("../../html/patients/createRosterAppCompare.html", profileLbl, popW, popH, true, closePtRAddAction);
			//parent.setPopupWIndowHeaderColor1("0bc56f","FFF");

		var weeks = [];

		var startdate = st;
		var enddate = ed;
		startdate.setHours(0, 0, 0, 0);
		enddate.setHours(0, 0, 0, 0);
		var daysOfYear = [];
		var obj = {};
		for (var d = startdate; d <= enddate; d.setDate(d.getDate() + 1)) {
			//daysOfYear.push(new Date(d));
			var currweekno = d.getWeekNo();
			var currday = d.getDay();
			var currdate = d.getDate();
			var currmonth = d.getMonth();
			var curryear = d.getFullYear();
			var index = weeks.findIndex((e) => e.weekid === currweekno && e.month === currmonth);

			if (index > -1) {
				weeks[index].dates[currday].dateno = currdate.toString();
			}
			else {
				obj = {};
				obj.weekid = currweekno;
				obj.month = currmonth;
				obj.year = curryear;
				obj.dates = [{ dow: "Sun", dateno: "" }, { dow: "Mon", dateno: "" }, { dow: "Tue", dateno: "" }, { dow: "Wed", dateno: "" }, { dow: "Thu", dateno: "" }, { dow: "Fri", dateno: "" }, { dow: "Sat", dateno: "" }];

				obj.dates[currday].dateno = currdate.toString();

				weeks.push(obj);
			}
		}
		selWeeks = weeks;
		bindWeeks(weeks);
		showAvailableRosters(weeks);
	}
}

function bindWeeks(weeks){
	$("#divTableWeeks").empty();
	if(weeks !== null && weeks.length > 0){
		var strHTML = '';
		for(var c = 0 ; c < weeks.length ; c++){
			var monthDate = getCounMonthName(weeks[c].month)+' - '+weeks[c].year;
			//strHTML = strHTML + '<div class="col-xs-12 rosterappointmentTableInner"> <div class="col-xs-6"> <div class="col-xs-6 input-block"> <div class="col-xs-12"> <label class="">Week Number</label> <select class="form-control" id="cmbWeek'+weeks[c].month+''+weeks[c].weekid+'"> <option value="1">1</option> <option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option> </select> </div> </div> <div class="zui-wrapper"> <div>';
			strHTML = strHTML +'<div class="col-xs-12 rosterappointmentTableInner"> <div class="col-xs-12"> <div class="col-xs-6"><span style="border: none;margin-right: 10px;font-weight: bold;">'+monthDate+'</span> <label style="font-size: 12px;font-weight: 300;margin-right: 5px">Week Number</label><select id="cmbWeek'+weeks[c].month+''+weeks[c].weekid+'" onchange="onChangeWeekNumber('+weeks[c].month+','+weeks[c].weekid+',this)"> <option value="1">1</option> <option value="2">2</option> <option value="3">3</option> <option value="4">4</option> <option value="5">5</option> </select> </div> </div> <div class="col-xs-12"> <div class="col-xs-6"> <div class="zui-wrapper"> <div>'
			strHTML = strHTML + '<table class="fixed-first-column">';
			strHTML = strHTML + '<tbody>';
			strHTML = strHTML + '<tr>';
			if(weeks[c].dates){
				weeks[c].dates.forEach(function (arrayItem) {
					strHTML = strHTML + '<th>'+arrayItem.dow+'</th>';
				});
			}
			strHTML = strHTML + '</tr>';
			strHTML = strHTML + '<tr>';
			if(weeks[c].dates){
				weeks[c].dates.forEach(function (arrayItem) {
					strHTML = strHTML + '<td>'+arrayItem.dateno+'</td>';
				});
			}
			strHTML = strHTML + '</tr>';
			strHTML = strHTML + '</tbody>';
			strHTML = strHTML + '</table>';
			strHTML = strHTML + '</div></div></div>';
			strHTML = strHTML + '<div class="col-xs-6">';
			strHTML = strHTML + '<span id="spn'+weeks[c].month+''+weeks[c].weekid+'"></span>';
			strHTML = strHTML + '<ul id="ul'+weeks[c].month+''+weeks[c].weekid+'" style="display:none"></ul>';
			strHTML = strHTML + '</div>';
			strHTML = strHTML + '</div></div>';

		}
		$("#divTableWeeks").html(strHTML);
		$("#divWeeks").show();

	}
}

function onChangeWeekNumber(month,weekid,e){
	console.log("sss");
	var selectedweekid = parseInt( $(e).val());
	if(selWeeks !== null && selWeeks.length > 0){
		$('#ul' + idnumber).empty();

		var idnumber = month + '' + weekid;
		var index = selWeeks.findIndex((w) => w.month === month && w.weekid === weekid);
		var dowArr = [];
		var objweek={};
		if(index > -1){
			 objweek = selWeeks[index];
			$.each(objweek.dates,function(i,e){
				if(e.dateno && e.dateno !== ""){
					dowArr.push(i);
				}
			});
		}


		var tmpRosters =  $.map(masterRosterArr,function(item){
			if (item.weekId === selectedweekid && dowArr.indexOf(item.weekDayStart) > -1) {
				var tmpobj = {};

				var sdt = new Date();
				sdt.setHours(0, 0, 0, 0);
				sdt.setMinutes(Number(item.fromTime));
				var fromtime = kendo.toString(sdt, "t");

				var dt = new Date(objweek.year, objweek.month, parseInt(objweek.dates[item.weekDayStart].dateno));
				dt.setHours(0, 0, 0, 0);
				dt.setMinutes(Number(item.fromTime));

				tmpobj.dow = objweek.dates[item.weekDayStart].dow;
				tmpobj.apptdate = new Date(objweek.year, objweek.month, parseInt(objweek.dates[item.weekDayStart].dateno));
				tmpobj.duration = item.duration;
				tmpobj.fromtime = fromtime;
				tmpobj.timestamp = dt.getTime();
				tmpobj.rosterUpdateId = Number(item.id);

				return tmpobj;
			}

		});

		if(tmpRosters !== null && tmpRosters.length > 0){
			$('#ul'+idnumber).empty();
			for (var k = 0; k < tmpRosters.length; k++) {
				$('#ul' + idnumber).append('<li rosterid="'+tmpRosters[k].rosterUpdateId+'" doa="'+tmpRosters[k].timestamp+'">' + tmpRosters[k].dow + ' ' + formatDate(tmpRosters[k].apptdate) + ' ' + tmpRosters[k].fromtime + ' (' + tmpRosters[k].duration + 'min)</li>');
			}

			$('#spn'+idnumber).hide();
			$('#ul'+idnumber).show();

		}
		else{
			$('#spn'+idnumber).text('No care plan records exist for selected week.');
			$('#ul'+idnumber).empty();
			$('#ul'+idnumber).hide();
			$('#spn'+idnumber).show();
		}


	}
}

function getPatientLeaveType(pid,st,et){
	for(var p=0;p<leaveArray.length;p++){
		var pItem = leaveArray[p];
		if(pItem && pItem.parentId == pid){
			if((st>=pItem.acceptedFromDate && st<=pItem.acceptedToDate) || (et>=pItem.acceptedFromDate && et<=pItem.acceptedToDate)){
				return "leave";
			}else{

			}
		}
	}
	return "";
}

function showAvailableRosters(weeks){
	if(weeks !== null && weeks.length > 0 && masterRosterArr !== null && masterRosterArr.length > 0){
		for(var j = 0; j < weeks.length; j++){

			var idnumber = weeks[j].month + '' + weeks[j].weekid;
			$('#ul' + idnumber).empty();
			$('#cmbWeek' + idnumber).val(weeks[j].weekid);

			var dowArr = [];
			$.each(weeks[j].dates,function(i,e){
				if(e.dateno && e.dateno !== ""){
					dowArr.push(i);
				}
			});

			// var tmpRosters =  masterRosterArr.filter(function(item){
			// 	return item.weekId === weeks[j].weekid && dowArr.indexOf(item.weekDayStart) > 0;
			// });

			var tmpRosters =  $.map(masterRosterArr,function(item){
				if (item.weekId === weeks[j].weekid && dowArr.indexOf(item.weekDayStart) > -1) {
					var tmpobj = {};

					var sdt = new Date();
					sdt.setHours(0, 0, 0, 0);
					sdt.setMinutes(Number(item.fromTime));
					var fromtime = kendo.toString(sdt, "t");

					var dt = new Date(weeks[j].year, weeks[j].month, parseInt(weeks[j].dates[item.weekDayStart].dateno));
					dt.setHours(0, 0, 0, 0);
					dt.setMinutes(Number(item.fromTime));

					tmpobj.dow = weeks[j].dates[item.weekDayStart].dow;
					tmpobj.apptdate = new Date(weeks[j].year, weeks[j].month, parseInt(weeks[j].dates[item.weekDayStart].dateno));
					let aptdate = tmpobj.apptdate.setMinutes(Number(item.fromTime));
					tmpobj.duration = item.duration;
					tmpobj.fromtime = fromtime;
					tmpobj.timestamp = dt.getTime();
					tmpobj.rosterUpdateId = item.id;
					tmpobj.providerId = item.providerId;
					tmpobj.dateOfAppointment=aptdate;
					return tmpobj;
				}

			});

			if(tmpRosters !== null && tmpRosters.length > 0){

				for (var k = 0; k < tmpRosters.length; k++) {
					let st = tmpRosters[k].dateOfAppointment;
					let et = st+(tmpRosters[k].duration*60*1000);

					var pL = getPatientLeaveType(tmpRosters[k].providerId,st,et);

					$('#ul' + idnumber).append('<li rosterid="' + tmpRosters[k].rosterUpdateId + '" doa="' + tmpRosters[k].timestamp + '">' + tmpRosters[k].dow + ' ' + formatDate(tmpRosters[k].apptdate) + ' ' + tmpRosters[k].fromtime + ' (' + tmpRosters[k].duration + 'min)<span style="color:red">' + (pL != "" ? '(' + pL + ')' : "") + '</span></li>');
				}

				$('#spn'+idnumber).hide();
				$('#ul'+idnumber).show();
			}
			else{
				$('#spn'+idnumber).text('No care plan records exist for above selected week.');
				$('#ul'+idnumber).empty();
				$('#ul'+idnumber).hide();
				$('#spn'+idnumber).show();
			}

		}
	}
}

var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
function formatDate(apptdate) {

	var formatted_date = apptdate.getDate() + "-" + months[apptdate.getMonth()] + "-" + apptdate.getFullYear();
	return formatted_date;
}

function getWeekDayName(wk){
	var wn = "";
	if(wk == 1){
		return "Monday";
	}else if(wk == 2){
		return "Tuesday";
	}else if(wk == 2){
		return "Tuesday";
	}else if(wk == 3){
		return "Wednesday";
	}else if(wk == 4){
		return "Thursday";
	}else if(wk == 5){
		return "Friday";
	}else if(wk == 6){
		return "Saturday";
	}else if(wk == 0){
		return "Sunday";
	}
	return wn;
}
function buildPatientRosterList(dataSource){
	var gridColumns = [];
	var otoptions = {};
	otoptions.noUnselect = false;
	gridColumns.push({
        "title": "Staff",
        "field": "provider",
        "cellTemplate": onProviderTemplate(),
    });
	gridColumns.push({
        "title": "Day",
        "field": "Day",
        "cellTemplate":onDayTemplate(),
    });
	  gridColumns.push({
	        "title": "Start Date",
	        "field": "SD",
	        "cellTemplate":onSDTemplate(),
	    });
	  gridColumns.push({
	        "title": "End Date",
	        "field": "ED",
	        "cellTemplate":onEDTemplate(),
	    });
	  gridColumns.push({
	        "title": "Start Time",
	        "field": "ST",
	        "cellTemplate":onSTemplate(),
	    });
	  gridColumns.push({
	        "title": "End Time",
	        "field": "ET",
	        "cellTemplate":onETemplate(),
	    });
    angularPTUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
}
function buildPatientRosterList1(dataSource){
	var gridColumns = [];
	var otoptions = {};
	otoptions.noUnselect = false;
	gridColumns.push({
        "title": "Staff",
        "field": "provider",
    });
	  gridColumns.push({
	        "title": "Start Date",
	        "field": "SD",
	    });
	  gridColumns.push({
	        "title": "End Date",
	        "field": "ED",
	    });
    angularPTUIgridWrapper1.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
}
function onProviderTemplate(){
	var node = '<div>';
    	node += '<div ng-show="(row.entity.status == \'false\')" style="background-color:white"><label>{{row.entity.provider}}';
    	node += '</label></div>';
    	node += '<div ng-show="(row.entity.status == \'true\')" style="background-color:red"><label>{{row.entity.provider}}';
    	node += '</label></div>';
    node += '</div>';
    return node;
}
function onDayTemplate(){
	var node = '<div>';
    	node += '<div ng-show="(row.entity.status == \'false\')" style="background-color:white"><label>{{row.entity.Day}}';
    	node += '</label></div>';
    	node += '<div ng-show="(row.entity.status == \'true\')" style="background-color:red"><label>{{row.entity.Day}}';
    	node += '</label></div>';
    node += '</div>';
    return node;
}
function onSDTemplate(){
	var node = '<div>';
    	node += '<div ng-show="(row.entity.status == \'false\')" style="background-color:white"><label>{{row.entity.SD}}';
    	node += '</label></div>';
    	node += '<div ng-show="(row.entity.status == \'true\')" style="background-color:red"><label>{{row.entity.SD}}';
    	node += '</label></div>';
    node += '</div>';
    return node;
}
function onEDTemplate(){
	var node = '<div>';
    	node += '<div ng-show="(row.entity.status == \'false\')" style="background-color:white"><label>{{row.entity.ED}}';
    	node += '</label></div>';
    	node += '<div ng-show="(row.entity.status == \'true\')" style="background-color:red"><label>{{row.entity.ED}}';
    	node += '</label></div>';
    node += '</div>';
    return node;
}
function onSTemplate(){
	var node = '<div>';
    	node += '<div ng-show="(row.entity.status == \'false\')" style="background-color:white"><label>{{row.entity.ST}}';
    	node += '</label></div>';
    	node += '<div ng-show="(row.entity.status == \'true\')" style="background-color:red"><label>{{row.entity.ST}}';
    	node += '</label></div>';
    node += '</div>';
    return node;
}
function onETemplate(){
	var node = '<div>';
    	node += '<div ng-show="(row.entity.status == \'false\')" style="background-color:white"><label>{{row.entity.ET}}';
    	node += '</label></div>';
    	node += '<div ng-show="(row.entity.status == \'true\')" style="background-color:red"><label>{{row.entity.ET}}';
    	node += '</label></div>';
    node += '</div>';
    return node;
}
function onPTChange(){

}
function onErrorMedication(err){

}
function closePtRAddAction(evt,returnData){

}

function onClickCancel() {
    var obj = {};
    obj.status = "false";
    popupClose(obj);
}

function onClickSearch() {
    var obj = {};
    obj.status = "search";
    popupClose(obj);
}

function popupClose(obj) {
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}

Date.prototype.getWeekNo = function() {
	var firstDay = new Date(this.getFullYear(), this.getMonth(), 1).getDay();
    return Math.ceil((this.getDate() + firstDay)/7);
};

function onClickUpdateWeekNo() {
	customAlert.confirm("Confirm", "Are you sure want to update week numbers?",function(response){
		if(response.button == "Yes"){
			var apptArr = [];
			var $divrosterappts = $('#divTableWeeks').find('div.rosterappointmentTableInner');

			if ($divrosterappts !== null && $divrosterappts.length > 0) {
				for(var c = 0; c < $divrosterappts.length ; c++){
					var $selbox = $($divrosterappts[c]).find('select');
					var $li = $($divrosterappts[c]).find('ul li');

					if($li && $li.length > 0){

						for(var d = 0 ; d < $li.length ; d++){


							var reviseddate = new Date(parseInt($($li[d]).attr("doa")));
							var obj = {};
							obj.apptDate = formatDate(reviseddate);
							obj.dateOfAppointment = reviseddate.getTime();
							obj.weekNumber = reviseddate.getWeekNo();
							obj.dayOfWeek = reviseddate.getDay();
							obj.rosterUpdateId = Number($($li[d]).attr("rosterId"));

							apptArr.push(obj);
						}

					}
				}

				var obj = {};
				popupClose(obj);


				var popW = "90%";
				var popH = "80%";

				var profileLbl;
				var devModelWindowWrapper = new kendoWindowWrapper();

				parentRef.apptArr = apptArr;
				var startDT = $("#txtRSD").data("kendoDatePicker");
				parentRef.st = new Date(startDT.value());

				profileLbl = "Appointments";
				devModelWindowWrapper.openPageWindow("../../html/patients/createRosterAppCompare.html", profileLbl, popW, popH, true, closePtRAddAction);
			}
		}
	});
}


function closePtRAddAction(evt,returnData){

}
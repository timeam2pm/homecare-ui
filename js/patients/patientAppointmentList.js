var parentRef = null;

var patientId = "";

$(document).ready(function(){
});


$(window).load(function(){
	loading = false;
	$(window).resize(adjustHeight);
onLoaded();
	if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
	parentRef = parent.frames['iframe'].window;
	init();
    buttonEvents();
	adjustHeight();
}

function init(){
	patientId = parentRef.patientId;
	getVitals();
	//getAjaxObject(ipAddress+"/hbt/getPatientData/"+patientId,"GET",onGetHBTPatientData,onError);
	//getAjaxObject(ipAddress+"/patient/reports/holter/"+patientId,"GET",onGetHolterReportsData,onError);
}
function onGetHBTPatientData(dataObj){
	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		
	}
}
function getVitals(){
	getAjaxObject(ipAddress+"/patient/appointments/"+patientId,"GET",onGetVitalsSuccess,onError);
}
function getVitalInfo(vName,dt){
	for(var i=0;i<vitalDataArray.length;i++){
		var item = vitalDataArray[i];
		if(item.vital == vName && item.date == dt){
			return item.value;
		}
	}
	return "";
}
function mycomparator(a,b) {
	  return Number(a.readingDate) - Number(b.readingDate);
	}
function formatAMPM(date) {
	  var hours = date.getHours();
	  var minutes = date.getMinutes();
	  var ampm = hours >= 12 ? 'PM' : 'AM';
	  hours = hours % 12;
	  hours = hours ? hours : 12; // the hour '0' should be '12'
	  minutes = minutes < 10 ? '0'+minutes : minutes;
	  var strTime = hours + ':' + minutes + ' ' + ampm;
	  return strTime;
	}
var vitalDataArray = [];
function onGetVitalsSuccess(dataObj){
	$("#divAppointment").text("");
	var appointArray = [];
	if(dataObj){
		if($.isArray(dataObj)){
			appointArray = dataObj;
		}else{
			appointArray.push(dataObj);
		}
	}
	var strTable = '<table class="table">';
	strTable = strTable+'<thead class="fillsHeader">';
	strTable = strTable+'<tr>';
	strTable = strTable+'<th class="textAlign whiteColor">Date</th>';
	strTable = strTable+'<th class="textAlign whiteColor">Time</th>';
	strTable = strTable+'<th class="textAlign whiteColor">Doctor</th>';
	strTable = strTable+'<th class="textAlign whiteColor">Reason</th>';
	strTable = strTable+'<th class="textAlign whiteColor">Facility</th>';
	strTable = strTable+'<th class="textAlign whiteColor">Remarks</th>';
	strTable = strTable+'</tr>';
	strTable = strTable+'</thead>';
	strTable = strTable+'<tbody>';
	for(var i=0;i<appointArray.length;i++){
		var dataItem = appointArray[i];
		if(dataItem){
			var className = "success";
			/*if(i%2 == 0){
				className = "danger";
			}
			if(i%3 == 0){
				className = "info";
			}
			if(i%3 == 0){
				className = "active";
			}*/
			if(dataItem.status == "Confirmed"){
				className = "confirm";
			}else if(dataItem.status == "Not Confirmed"){
				className = "completed ";
			}else if(dataItem.status == "Completed"){
				className = "notConfirm";
			}
			strTable = strTable+'<tr class="'+className+'">';
			strTable = strTable+'<td>'+kendo.toString(new Date(dataItem.dateOfAppointment), "MM/dd/yyyy")+'</td>';
			strTable = strTable+'<td>'+dataItem.timeOfAppointment+'</td>';
			strTable = strTable+'<td>'+dataItem.doctor+'</td>';
			strTable = strTable+'<td>'+dataItem.reason+'</td>';
			strTable = strTable+'<td>'+dataItem.facility+'</td>';
			strTable = strTable+'<td>'+dataItem.status+'</td>';
			strTable = strTable+'</tr>';
		}
	}
	strTable = strTable+'</tbody>';
	strTable = strTable+'</table>';
	$("#divAppointment").append(strTable);
}
function onError(errorObj){
	//console.log(errorObj);
}
function getVitalHeaders(){
	vitalDataArray.sort(mycomparator);
	var vitalArr = [];
	for(var i=0;i<vitalDataArray.length;i++){
		var item = vitalDataArray[i];
		var rd = Number(item.readingDate);
		var dtOffsetDate = new Date();
		var offset = dtOffsetDate.getTimezoneOffset();
		offset = offset*60*1000;
		rd = rd+Math.abs(offset);
		var dt = new Date(rd);
		var strDT = kendo.toString(dt,"MM-dd-yyyy");
		strDT = strDT+" "+formatAMPM(dt);
		item.date = strDT;
		if(vitalArr.indexOf(strDT)<0){
			vitalArr.push(strDT);
		}
	}
	if(vitalArr.length>0){
		vitalArr = vitalArr.reverse();
	}
	return vitalArr;
}
var holterReportDataArray = [];
function onGetHolterReportsData(dataObj){
		$("#divVitals").text("");
		var dataArray = [];
		if(dataObj){
			if($.isArray(dataObj)){
				dataArray = dataObj;
			}else{
				dataArray.push(dataObj);
			}
		}
		holterReportDataArray = dataArray;
		var strTable = '<table class="table">';
		strTable = strTable+'<thead class="fillsHeader whiteColor">';
		strTable = strTable+'<tr>';
		//strTable = strTable+'<th>Patient ID</th>';
		strTable = strTable+'<th class="textAlign whiteColor" style="width:220px">Date Generated</th>';
		strTable = strTable+'<th class="textAlign whiteColor">File Name</th>';
		strTable = strTable+'<th class="textAlign whiteColor">File Type</th>';
		strTable = strTable+'</tr>';
		strTable = strTable+'</thead>';
		strTable = strTable+'<tbody>';
		for(var i=0;i<holterReportDataArray.length;i++){
			var dataItem = holterReportDataArray[i];
			if(dataItem){
				//console.log(dataItem);
				var className = getRowColors(i);
				strTable = strTable+'<tr class="'+className+'">';
				//strTable = strTable+'<td>'+dataItem.patientId+'</td>';
				strTable = strTable+'<td class="textAlign">'+kendo.toString(new Date(dataItem.createdDate),"MM-dd-yyyy h:mm:ss")+'</td>';
				strTable = strTable+'<td class="textAlign">'+dataItem.fileName+'</td>';
				var dietId = dataItem.id;
				if(dataItem.fileType == "mp4"){
					strTable = strTable+'<td style="width:200px;padding:0px" class="textAlign"><img id="'+dietId+'" src="../../img/AppImg/HosImages/video.png"  class="cusrsorStyle videoIcon" onClick="onClickGenerate(event)"></td>';
				}else if(dataItem.fileType == "pdf"){
					strTable = strTable+'<td style="width:200px;padding:0px" class="textAlign"><img  id="'+dietId+'" src="../../img/AppImg/HosImages/pdf.png"    class="cusrsorStyle pdfIcon" onClick="onClickHolterReport(event)"></td>';
				}
				strTable = strTable+'</tr>';
			}
		}
		strTable = strTable+'</tbody>';
		strTable = strTable+'</table>';
		$("#divVitals").append(strTable);
	}
function buttonEvents(){
	$("#btnCancelDet").off("click",onClickCancel);
	$("#btnCancelDet").on("click",onClickCancel);
	
	$("#btnAdd").off("click",onClickAdd);
	$("#btnAdd").on("click",onClickAdd);
}

function adjustHeight(){
	var defHeight = 60;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    $("#divPtHolter").height(cmpHeight);
	//angularUIgridWrapper.adjustGridHeight(cmpHeight);
}

function onClickAdd(){
	var popW = "80%";
    var popH = 650;

    var profileLbl = "Appointments"
    	parentRef.patientId = patientId;
    var devModelWindowWrapper = new kendoWindowWrapper();
 //   var devUrl = "../../html/patients/patientAppointments.html";
    var devUrl = "../../html/patients/createAppointment.html";
    devModelWindowWrapper.openPageWindow(devUrl, profileLbl, popW, popH, true, closeAppointment);
}

function closeAppointment(evt,returnData){
	if(returnData && returnData.status == "success"){
		var opr = returnData.operation;
		if(opr == "add"){
			customAlert.info("info", "Appontment Created Successfully");
		}else{
			customAlert.info("info", "Appontment Updated Successfully");
		}
	}
}

function onClickPatientCall(obj){
	var popW = "60%";
    var popH = 400;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Patient Call Chart";
    parentRef.selObj = obj;
    devModelWindowWrapper.openPageWindow("../../html/patients/callPatient.html", profileLbl, popW, popH, false, closeCallPatient);
}
function closeCallPatient(evt,returnData){
	if(returnData && returnData.status == "success"){
		customAlert.info("Save","Patient Call Chart Created Successfully");
		init();
	}
}
function onClickCancel(){
	var obj = {};
	var windowWrapper = new kendoWindowWrapper();
	windowWrapper.closePageWindow(obj);
}

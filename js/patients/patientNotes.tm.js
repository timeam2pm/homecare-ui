var angularUIgridWrapper;
var parentRef = null;
var recordType = "1";
var dataArray = [];
var ADD = "add";
var UPDATE = "edit";
var VIEW = "view";
var DELETE = "delete";
var operation = "add";
var atID = "";
var ds  = "";
var patientId = "";

var IsFlag = 1;

var statusArr = [{Key:'Active',Value:'Active'},{Key:'InActive',Value:'InActive'}];
var cntry = sessionStorage.countryName;

$(document).ready(function(){
    sessionStorage.setItem("IsSearchPanel", "1");
    parentRef = parent.frames['iframe'].window;
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("dgridPatientNotesList", dataOptions);
    angularUIgridWrapper.init();
    buildDeviceListGrid([]);


});


$(window).load(function(){
    loading = false;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
    init();
    buttonEvents();
    adjustHeight();
    onGetBillType();
}

function init(){
    $("#btnEdit").prop("disabled", true);
    $("#btnDelete").prop("disabled", true);
    $("#divTypeDetails").css("display","none");
    $("#viewDivBlock").css("display","");

    if(cntry.indexOf("India")>=0){
        $("#txtDate").datepick({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yyyy',
            yearRange: "-200:+200"
        });
    }else if(cntry.indexOf("United Kingdom")>=0){
        $("#txtDate").datepick({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yyyy',
            yearRange: "-200:+200"
        });
    }else{
        $("#txtDate").datepick({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'mm/dd/yyyy',
            yearRange: "-200:+200"
        });
    }

    patientId = parentRef.patientId;//101
    buildDeviceListGrid([]);
    getAjaxObject(ipAddress+"/homecare/patient-notes/?is-active=1&is-deleted=0&patientId="+ patientId + "&fields=*,noteType.*,noteStatus.*","GET",getPatientBilling,onError);
}

function getActivityTypes(dataObj){
    console.log(dataObj);
    var tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.noteStatus){
            if($.isArray(dataObj.response.noteStatus)){
                tempCompType = dataObj.response.noteStatus;
            }else{
                tempCompType.push(dataObj.response.noteStatus);
            }
        }
    }

    for(var i=0;i<tempCompType.length;i++){
        tempCompType[i].idk = tempCompType[i].id;
        $("#cmbNoteStatusId").append('<option value="'+tempCompType[i].id+'">'+tempCompType[i].code+'</option>');
    }
}

function getPatientBilling(dataObj){
    console.log(dataObj);
    var tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.patientNotes){
            if($.isArray(dataObj.response.patientNotes)){
                tempCompType = dataObj.response.patientNotes;
            }else{
                tempCompType.push(dataObj.response.patientNotes);
            }
        }
    }

    for(var i=0;i<tempCompType.length;i++){
        tempCompType[i].idk = tempCompType[i].id;
        tempCompType[i].notesDate = kendo.toString(new Date(tempCompType[i].date), "dd-MMM-yyyy");
        if(tempCompType[i].notes.length > 30){
            tempCompType[i].gridNotes = tempCompType[i].notes.substring(0,30)+".....";
        }
        else{
            tempCompType[i].gridNotes = tempCompType[i].notes;
        }

    }
    buildDeviceListGrid(tempCompType);
}


function onError(errorObj){
    console.log(errorObj);
}

function buttonEvents(){

    $("#btnSave").off("click",onClickSave);
    $("#btnSave").on("click",onClickSave);

    $("#btnEdit").off("click",onClickEdit);
    $("#btnEdit").on("click",onClickEdit);

    $("#btnDelete").off("click",onClickDelete);
    $("#btnDelete").on("click",onClickDelete);

    $("#btnAdd").off("click");
    $("#btnAdd").on("click",onClickAdd);

    $("#btnCancel").off("click",onClickCancel);
    $("#btnCancel").on("click",onClickCancel);

    $("#btnReset").off("click",onClickReset);
    $("#btnReset").on("click",onClickReset);

    $(".btnActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('active');
        onClickActive();
    });
    $(".btnInActive").on("click", function(e) {
        e.preventDefault();
        $('.alert').remove();
        searchOnLoad('inactive');
        onClickInActive();
    });

    allowNumerics("txtTotalAmount");
    allowNumerics("txtSwiftId");
    allowDecimals("txtHourlyRate");
    allowDecimals("txtPercentage");
}

function searchOnLoad(status) {
    buildDeviceListGrid([]);
    var urlExtn;
    if(status == "active") {
        urlExtn = ipAddress + "/homecare/patient-notes/?is-active=1&is-deleted=0&patientId="+ patientId + "&fields=*,noteType.*,noteStatus.*";
        getAjaxObject(urlExtn,"GET",getPatientBilling,onError);
    }
    else if(status == "inactive") {
        urlExtn = ipAddress + "/homecare/patient-notes/?is-active=0&is-deleted=1&patientId="+ patientId + "&fields=*,noteType.*,noteStatus.*";
    }
    getAjaxObject(urlExtn,"GET",getPatientBilling,onError);
}


function onClickAdd(){
    $("#txtID").html("");
    $("#billTitle").html("Add Service User Note");
    $("#viewDivBlock").hide();
    parentRef.operation = "add";
    $("#divTypeDetails").css("display","");
    onClickReset();
    operation = ADD;
}

function onClickActive() {
    $(".btnInActive").removeClass("radioButton-active");
    $(".btnActive").addClass("radioButton-active");
}

function onClickInActive() {
    $(".btnActive").removeClass("radioButton-active");
    $(".btnInActive").addClass("radioButton-active");
}

function onClickDelete(){
    customAlert.confirm("Confirm", "Are you sure to delete?",function(response){
        if(response.button == "Yes"){
            var dataObj = {};
            dataObj.createdBy = Number(sessionStorage.userId);
            dataObj.isDeleted = 1;
            dataObj.isActive = 0;
            dataObj.id = atID;
            var dataUrl = ipAddress +"/homecare/patient-notes/?id="+ atID ;
            var method = "DELETE";
            createAjaxObject(dataUrl, dataObj, method, onCreateDelete, onError);
        }
    });
}

function onCreateDelete(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "Service User Note deleted successfully";
            customAlert.error("Info", msg);
            $("#txtAT").val("");
            operation = ADD;
            init();
        }
    }
}
function onClickSave(){
    var dataObj = {};

    dataObj.isDeleted = 0;
    dataObj.isActive = 1;
    dataObj.notes = $("#txtNotes").val();
    dataObj.followupNotes = $("#txtFollowupNotes").val();
    dataObj.patientId = patientId;
    dataObj.noteStatusId = parseInt($("#cmbNoteStatusId").val());
    dataObj.noteTypeId = parseInt($("#cmbNoteTypeId").val());
    dataObj.date = GetDateTime("txtDate");
    var dataUrl = ipAddress +"/homecare/patient-notes/";
    var method = "POST";
    if(operation == UPDATE){
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        /*if (selectedItems && selectedItems.length > 0) {
            dataObj.createdBy = selectedItems[0].createdBy;
        }*/
        method = "PUT";
        dataObj.id = atID;
        dataObj.modifiedBy = Number(sessionStorage.userId);
    }
    else{
        dataObj.createdBy = Number(sessionStorage.userId);
    }
    createAjaxObject(dataUrl, dataObj, method, onCreate, onError);
}

function onCreate(dataObj){
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            var msg = "Service User Notes created successfully";
            if(operation == UPDATE){
                msg = "Service User Notes updated successfully"
            }
            displaySessionErrorPopUp("Info", msg, function(res) {
                // $("#txtAT").val("");
                operation = ADD;
                init();
            })
        }else{
            customAlert.error("Error", dataObj.response.status.message.replace("patient","service user"));
        }
    }
    onClickReset();
}

function onClickCancel(){
    $("#divTypeDetails").css("display","none");
    $("#viewDivBlock").show();
}

function adjustHeight(){
    var defHeight = 230;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    angularUIgridWrapper.adjustGridHeight(cmpHeight);
}

function buildDeviceListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Date",
        "field": "notesDate"
    });
    gridColumns.push({
        "title": "Type",
        "field": "noteTypeCode"
    });
    gridColumns.push({
        "title": "Notes Status",
        "field": "noteStatusCode"
    });
    gridColumns.push({
        "title": "Notes",
        "field": "notes"
    });
    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}

function onChange(){
    setTimeout(function() {
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if (selectedItems && selectedItems.length > 0) {
            var obj = selectedItems[0];
            atID = obj.idk;
            $("#btnEdit").prop("disabled", false);
            $("#btnDelete").prop("disabled", false);
        } else {
            $("#btnEdit").prop("disabled", true);
            $("#btnDelete").prop("disabled", true);
        }
    });
}

function onClickEdit(){
    $("#billTitle").html("Edit Service User Note");
    $("#divId").css("display","");
    parentRef.operation = "edit";
    operation = "edit";
    $("#divTypeDetails").css("display","")
    $("#viewDivBlock").css("display","none")
    var selectedItems = angularUIgridWrapper.getSelectedRows();
    if (selectedItems && selectedItems.length > 0) {
        var obj = selectedItems[0];
        atID = obj.idk;
        $("#txtID").html("ID :"+ obj.idk);
        // $("#cmbStatus").val(obj.isActive);
        $("#txtNotes").val(obj.notes);
        $("#txtFollowupNotes").val(obj.followupNotes);
        $("#cmbNoteStatusId").val(obj.noteStatusId);
        $("#cmbNoteTypeId").val(obj.noteTypeId);
        $("#cmbStatus").val(obj.isActive);
        // if (obj.date) {
        //     var dt = new Date(obj.date);
        //     if (dt) {
        //         var strDT = "";
        //
        //         if (cntry.indexOf("India") >= 0) {
        //             strDT = kendo.toString(dt, "dd/MM/yyyy");
        //         } else if (cntry.indexOf("United Kingdom") >= 0) {
        //             strDT = kendo.toString(dt, "dd/MM/yyyy");
        //         } else {
        //             strDT = kendo.toString(dt, "MM/dd/yyyy");
        //         }
        //
        //         $("#txtDate").datepick();
        //         $("#txtDate").datepick("setDate", strDT);
        //     }
        // }
        $("#txtDate").datepick();
        $("#txtDate").datepick("setDate", GetDateEdit(obj.date));

        //$("#txtDate").val(obj.date);
        // $("#txtDate").datepick("setDate", GetDateEdit(obj.date));

    }
}

var operation = "add";
var selFileResourceDataItem = null;

function onStatusChange(){
    var cmbStatus = $("#cmbStatus").data("kendoComboBox");
    if(cmbStatus && cmbStatus.selectedIndex<0){
        cmbStatus.select(0);
    }
}

function onComboChange(cmbId) {
    var cmb = $("#" + cmbId).data("kendoComboBox");
    if (cmb && cmb.selectedIndex < 0) {
        cmb.select(0);
    }
}

function onClickReset() {
    if(operation == ADD) {
        operation = ADD;
    }
    else {
        operation = UPDATE;
    }
    $("#txtID").val("");
    $("#txtNotes").val("");
    $("#txtFollowupNotes").val("");
    $("#txtDate").val("");
}

function onGetBillType(){
    getAjaxObject(ipAddress+"/homecare/note-status/?is-active=1&is-deleted=0","GET",getActivityTypes,onError);
    getAjaxObject(ipAddress+"/homecare/note-types/?is-active=1&is-deleted=0","GET",getNoteTypeData,onError);

    //getAjaxObject(ipAddress + "/carehome/bill-to/?is-active=1", "GET", getBillToData, onError);
    // getAjaxObject(ipAddress + "/homecare/billing-periods/?is-active=1", "GET", getBillPeriodData, onError);
    // getAjaxObject(ipAddress + "/homecare/shifts/?is-active=1", "GET", getShifts, onError);
    // getAjaxObject(ipAddress + "/homecare/travel-modes/?is-active=1", "GET", getTravelModes, onError);
}

function onBillTypeChange() {
    onComboChange("cmbType");
}

function onRelationChange() {
    onComboChange("cmbRelation");
}

function onBillToChange() {
    onComboChange("cmbBillTo");
}

function onShifts() {
    onComboChange("cmbBillShift");
}

function onModes() {
    onComboChange("cmbMode");
}

function getRelations(dataObj) {
    var dataArray
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            dataArray = dataObj.response.codeTable || [];
        }
    }
    for(var i=0;i<dataArray.length;i++){
        $("#cmbRelation").append('<option value="'+dataArray[i].id+'">'+dataArray[i].desc+'</option>');
    }
}

function getNoteTypeData(dataObj){
    var tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.noteTypes){
            if($.isArray(dataObj.response.noteTypes)){
                tempCompType = dataObj.response.noteTypes;
            }else{
                tempCompType.push(dataObj.response.noteTypes);
            }
        }
    }
    for(var i=0;i<tempCompType.length;i++){
        $("#cmbNoteTypeId").append('<option value="'+tempCompType[i].id+'">'+tempCompType[i].code+'</option>');
    }
}

function getBillPeriodData(dataObj){
    var tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.roles){
            if($.isArray(dataObj.response.roles)){
                tempCompType = dataObj.response.roles;
            }else{
                tempCompType.push(dataObj.response.roles);
            }
        }
    }
    for(var i=0;i<tempCompType.length;i++){
        $("#cmbBillPeriod").append('<option value="'+tempCompType[i].id+'">'+tempCompType[i].code+'</option>');
    }
}

function getShifts(dataObj){
    var tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.shifts){
            if($.isArray(dataObj.response.shifts)){
                tempCompType = dataObj.response.shifts;
            }else{
                tempCompType.push(dataObj.response.shifts);
            }
        }
    }

    for(var i=0;i<tempCompType.length;i++){
        $("#cmbBillShift").append('<option value="'+tempCompType[i].id+'">'+tempCompType[i].code+'</option>');
    }
}


function getTravelModes(dataObj){
    var tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.travelmodes){
            if($.isArray(dataObj.response.travelmodes)){
                tempCompType = dataObj.response.travelmodes;
            }else{
                tempCompType.push(dataObj.response.travelmodes);
            }
        }
    }
    for(var i=0;i<tempCompType.length;i++){
        $("#cmbMode").append('<option value="'+tempCompType[i].id+'">'+tempCompType[i].code+'</option>');
    }
}


// function GetDateEdit(objDate){
//     var dt = new Date(objDate);
//     var strDT = "";
//     if (dt) {
//         var cntry = sessionStorage.countryName;
//         if (cntry.indexOf("India") >= 0) {
//             strDT = kendo.toString(dt, "dd/MM/yyyy");
//         } else if (cntry.indexOf("United Kingdom") >= 0) {
//             strDT = kendo.toString(dt, "dd/MM/yyyy");
//         } else {
//             strDT = kendo.toString(dt, "MM/dd/yyyy");
//         }
//     }
//     return strDT;
// }
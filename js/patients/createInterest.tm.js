var parentRef = null;
var selObj = null;
var sType = "";
var dataObj = null;
var compTypes = [{text: 'None',value: '0'},{text: 'Text Box',value: '2'},{text: 'Radio Button',value: '4'},{text: 'Hyper Text',value: '7'},{text: 'Calender',value: '8'}];	

$(document).ready(function(){
	parentRef = parent.frames['iframe'].window;
});

$(window).load(function(){
	init();
	buttonEvents();
});
function init(){
	sType = parentRef.screenType;
	
	setDataForSelection(compTypes, "cmbCType", onCompTypeChange, ["text", "value"], 0, "");
	onCompTypeChange();
	//$("#txtCalander").kendoDatePicker({ value:new Date() });
	if(sType != "Create"){
		console.log(parentRef.dataObj)
		// $("#btnSave").text("Update");
		$("#btnDelete").show();
		
		dataObj = parentRef.dataObj;
		if(dataObj){
			$("#taQuestion").val(dataObj.question);
			var cmbCType = $("#cmbCType").data("kendoComboBox");
			if(dataObj.controlType == "0"){
				cmbCType.select(0);
			}else if(dataObj.controlType == "2"){
				$("#divText").show();
				$("#taText").val(dataObj.answer);
				cmbCType.select(1);
			}else if(dataObj.controlType == "4"){
				$("#divRadio").show();
				cmbCType.select(2);
				if(dataObj.answer == "Yes"){
					$("#rdYes").attr("checked",true);
				}else{
					$("#rdNo").attr("checked",true);
				}
			}else if(dataObj.controlType == "7"){
				$("#divText").show();
				$("#txtText").val(dataObj.answer);
				cmbCType.select(3);
			}else if(dataObj.controlType == "8"){
				$("#divCalander").show();
				var dt = new Date(dataObj.answer);
				var dtfmt = kendo.toString(dt,"MM/dd/yyyy");
				$("#txtCalander").val(dtfmt);
				cmbCType.select(4);
			}
			cmbCType.enable(false);
		}
		/*var cmbCType = $("#cmbCType").data("kendoComboBox");
		cmbCType.select(3);
		onCompTypeChange();
		onClickPreview();*/
	}
}
function onCompTypeChange(){
	var cmbCType = $("#cmbCType").data("kendoComboBox");
	if(cmbCType && cmbCType.selectedIndex<1){
		cmbCType.select(0);
	}
	
	$("#divRadio").hide();
	$("#divCheck").hide();
	$("#divText").hide();
	$("#divCalander").hide();
	$("#divLink").hide();
	//$("#lblAns").hide();
	
	if(cmbCType){
		if(cmbCType.selectedIndex == "0"){
		}else if(cmbCType.value() == "2"){
			$("#divText").show();
		}else if(cmbCType.value() == "4"){
			$("#divRadio").show();
		}else if(cmbCType.value() == "7"){
			$("#divText").show();
		}else if(cmbCType.value() == "8"){
			$("#divCalander").show();
		}
	}
}
function buttonEvents(){
	$("#btnSave").off("click");
	$("#btnSave").on("click",onClickSave);
	
	$("#btnCancelDet").off("click");
	$("#btnCancelDet").on("click",onClickCancel);
	
	$("#btnPreview").off("click");
	$("#btnPreview").on("click",onClickPreview);
	
	$("#btnDelete").off("click");
	$("#btnDelete").on("click",onClickDelete);
}
function onClickPreview(){
	var cmbCType = $("#cmbCType").data("kendoComboBox");
	$("#divTable").show();
	$("#divNone").hide();
	$("#divLabel").hide();
	$("#divTextBox").hide();
	$("#divButtonRd").hide();
	$("#divPrevRadio").hide();
	$("#divPrevCheck").hide();
	if(cmbCType.selectedIndex == 0){
		$("#taNoneQue").val($("#taQuestion").val());
		$("#divNone").show();
	}else if(cmbCType.selectedIndex == 1){
		$("#lblLblAns").text($("#taQuestion").val());
		$("#taLblQue").val($("#taText").val());
		$("#divLabel").show();
	}else if(cmbCType.selectedIndex == 2){
		$("#lblTbxAns").text($("#taQuestion").val());
		$("#taTbxQue").val($("#taText").val());
		$("#divTextBox").show();
	}else if(cmbCType.selectedIndex == 3){
		$("#taBulletRd").text($("#taQuestion").val());
		$("#divButtonRd").show();
	}else if(cmbCType.selectedIndex == 4){
		$("#lblRadioQue").text($("#taQuestion").val());
		var txtVal = $('input:radio[name="Answer"]:checked').val();
		$("#taRadioQue").val(txtVal);
		$("#divPrevRadio").show();
	}else if(cmbCType.selectedIndex == 5){
		$("#divPrevCheck").show();
		$("#lblCheckQue").text($("#taQuestion").val());
		$("#taCheckQue").val($("#txtText").val())
	}else if(cmbCType.selectedIndex == 6){
		$("#divPrevCheck").show();
		$("#lblCheckQue").text($("#taQuestion").val());
		/*var txtCalander = $("#txtCalander").data("kendoDatePicker");
		if(txtCalander){
			$("#taCheckQue").val(txtCalander.value());
		}*/
	}
	
	/*var dataObj = getDataObject();
	$("#divTable").text("");
	var strTable = '<table>';
	strTable = strTable+'<thead>';
	strTable = strTable+'<tr>';
	strTable = strTable+'<th>Question</th>';
	strTable = strTable+'<th>Answer</th>';
	strTable = strTable+'</tr>';
	strTable = strTable+'</thead>';
	strTable = strTable+'<tbody>';
	strTable = strTable+'<tr class="info">';
	strTable = strTable+'<td class="textAlign">'+dataObj.question+'</td>';
	strTable = strTable+'<td class="textAlign">'+dataObj.answer+'</td>';
	strTable = strTable+'</tr>';
	strTable = strTable+'</tbody>';
	strTable = strTable+'</table>';
	$("#divTable").append(strTable);*/
}
function validations(){
	var flag = true;
	var strQue = $("#taQuestion").val();
	strQue = $.trim(strQue);
	if(strQue == ""){
		customAlert.error("Error","Please enter question");
		flag = false;
	}
	return flag;
}
function onClickSave(){
	if(validations()){
		var cmbCType = $("#cmbCType").data("kendoComboBox");
		var reqObj = {};
		reqObj.question = $("#taQuestion").val();
		reqObj.patientId = parentRef.patientId;
        reqObj.isActive = 1;
		reqObj.isDeleted = 0;
		// reqObj.id = 0;
		if(cmbCType.value() == 0){
			reqObj.answer = "";
			reqObj.controlType = 0;
		}else if(cmbCType.value() == 2){
			reqObj.answer = $("#taText").val();
			reqObj.controlType = 2;
		}else if(cmbCType.value() == 4){
			reqObj.answer = $('input:radio[name="Answer"]:checked').val();;
			reqObj.controlType = 4;
		}else if(cmbCType.value() == 7){
			reqObj.answer = $("#txtText").val();
			reqObj.controlType = 7;
		}else if(cmbCType.value() == 8){
			reqObj.answer = $("#txtCalander").val();
			reqObj.controlType = 8;
		}
		if(sType != "Create"){
			reqObj.id = dataObj.id;
		}
		console.log(reqObj);
		var dataUrl = ipAddress+"/patient/preventative-services/create";
		 getAjaxObject(dataUrl,"POST",reqObj,onSucessInterest,onErrorFunction);
	}
	//popupClose(false);
}
function onClickDelete(){
	customAlert.confirm("Confirm", "Are you sure you want delete?",function(response){
		if(response.button == "Yes"){
			if(sType != "Create"){
				dataObj["deleted"]= "1";
				console.log(dataObj);
				var dataUrl = ipAddress+"/patient/preventative-services/delete/"+dataObj.id;
				 getAjaxObject(dataUrl,"GET",dataObj,onDeleteInterest,onErrorFunction);
			}
		}
	});
}
function getDataObject(){
	var strQue = $("#taQuestion").val();
	strQue = $.trim(strQue);
	var strAns = "";
	var cmbCType = $("#cmbCType").data("kendoComboBox");
	if(cmbCType.selectedIndex == 0){
		strAns = "";
	}if(cmbCType.selectedIndex == 1){
		strAns = $('input:radio[name="Answer"]').val();
	}else if(cmbCType.selectedIndex == 2){
		strAns = $('#txtText').val();
	}else if(cmbCType.selectedIndex == 3){
		var txtCalander = $("#txtCalander").data("kendoDatePicker");
		if(txtCalander){
			strAns = txtCalander.value();
			strAns = kendo.toString(new Date(strAns), "MM/dd/yyyy")
		}
	}
	 
	 console.log(strQue+','+strAns);
	 var dataObj = {};
	 dataObj.question = strQue; 
	 dataObj.answer = strAns; 
	 dataObj.controlType = "1"; 
	 return dataObj;
}
function onDeleteInterest(dataObj){
	console.log(dataObj);
	var msg = "";
	msg = "Deleted Successfully";
	customAlert.error("Info",msg);
	setTimeout(function(){
		var onCloseData = getDataObject();
		onCloseData.status = "success";
		var windowWrapper = new kendoWindowWrapper();
		windowWrapper.closePageWindow(onCloseData);
	},1000);
}
function onSucessInterest(dataObj){
	console.log(dataObj);
	var msg = "";
	if(sType != "Create"){
		msg = "Updated Successfully";
	}else{
		msg = "Created Successfully";
	}
	customAlert.error("Info",msg);
	setTimeout(function(){
		var onCloseData = getDataObject();
		onCloseData.status = "success";
		var windowWrapper = new kendoWindowWrapper();
		windowWrapper.closePageWindow(onCloseData);
	},1000);
	
}
function onErrorFunction(data){
	console.log(data);
}
function getAjaxObject(dataUrl,method,dataObj,successFunction,errorFunction){
	Loader.showLoader();
	$.ajax({
		  type: method,
		  url: dataUrl,
		  data: JSON.stringify(dataObj),
		  context: this,
			cache: false,
		  success: function( data, statusCode, jqXHR ){
			  Loader.hideLoader();
			  successFunction(data);
		  },
		  error: function( jqXHR, textStatus, errorThrown ){
			  Loader.hideLoader();
			  errorFunction(jqXHR);
		  },
		  contentType: "application/json",
		});

}
function onClickCancel(){
	popupClose(false);
}
function popupClose(st){
	var onCloseData = new Object();
	onCloseData.status = st;
	var windowWrapper = new kendoWindowWrapper();
	windowWrapper.closePageWindow(onCloseData);
}

function handleGetError(e) {
	var errLbl = getLocaleStringWithDefault(DC_UI_COMM, 'ERROR_MESSAGE', "Error");
	if(e && e.RestData && e.RestData.Description){
		window.top.displayErrorPopUp(errLbl,e.RestData.Description);
	}
}
function handleGetHttpError(e) {
   var errLbl = getLocaleStringWithDefault(DC_UI_COMM, 'ERROR_MESSAGE', "Error");
   var errDesc = getLocaleStringWithDefault(DC_UI_COMM, 'HTTP_ERROR', 'Http Error');
	// window.top.displayErrorPopUp(errLbl,errDesc);
}
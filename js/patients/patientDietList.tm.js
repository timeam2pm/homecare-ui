var parentRef = null;

var patientId = "";
var recordType = "1";

var dataTabURL = "";
var dataArray = [];
var ADD = "add";
var UPDATE = "edit";

var atID = "";
var showTab = "dietInfoTab";

$(document).ready(function(){
    parentRef = parent.frames['iframe'].window;
    patientId = parentRef.patientId;
    var pnlHeight = window.innerHeight;
    var imgHeight = pnlHeight - 90;
    dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("divPatientDietGrid", dataOptions);
    angularUIgridWrapper.init();


    $("#divTop").height(imgHeight);
    $('#tabsUL .data-patient-link').on('click', function() {
        $(this).closest('li').addClass('active').siblings('li').removeClass('active');
        $('.tab-content').hide();
        showTab = $(this).attr('data-patient-link');
        // $('#' + showTab).show();
        $('.alert').remove();
        if (showTab == "dietInfoTab") {
            showTab = "dietInfoTab";
            recordType = "1";
            $('#' + showTab).show();
             $('.main-btn-wrapper').hide();
            $('#' + showTab).find('.patient-btn-wrapper').show();
        }
        else {
            // showTab = "dietInfoTab";
            recordType = "2";
            $('#' + showTab).show();
            $('.main-btn-wrapper').hide();
            $('#' + showTab).find('.patient-btn-wrapper').show();
		}

        if (recordType == "1") {
            buildDietListGrid([]);
            $("#kendoWindowContainer_wnd_title",parent.document).html("Service User Diet");

            dataTabURL = ipAddress + "/homecare/patient-diets/";
			getAjaxObject(dataTabURL + '?is-active=1&is-deleted=0&fields=*,dietType.*&patientId='+patientId, "GET", getDietsData, onError);
        }
        if (recordType == "2") {
            buildDietListGrid([]);
            $("#kendoWindowContainer_wnd_title",parent.document).html("Service User Exercise");
            // recordType = "2";
            dataTabURL = ipAddress + "/homecare/patient-exercises/";
			getAjaxObject(dataTabURL + '?is-active=1&is-deleted=0&fields=*,exerciseType.*&patientId='+patientId, "GET", getExerciseData, onError);
        }
        return true;
    });


});


$(window).load(function(){
	loading = false;
	$(window).resize(adjustHeight);
	onLoaded();
	if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
	parentRef = parent.frames['iframe'].window;
	init();
    buttonEvents();
	adjustHeight();
}

function init(){
	patientId = parentRef.patientId;
    onClickActive();
	// getDietRecords();
	//getAjaxObject(ipAddress+"/patient/reports/holter/"+patientId,"GET",onGetHolterReportsData,onError);
    buildDietListGrid([]);
    parentRef = parent.frames['iframe'].window;
    if (recordType == "1") {
    	// recordType = "1";
        dataTabURL = ipAddress + "/homecare/patient-diets/";
        getAjaxObject(dataTabURL + '?is-active=1&is-deleted=0&fields=*,dietType.*&patientId=' + patientId, "GET", getDietsData, onError);
    }
    else{
        // recordType = "2";
        dataTabURL = ipAddress + "/homecare/patient-exercises/";
        getAjaxObject(dataTabURL + '?is-active=1&fields=*,exerciseType.*&patientId='+patientId, "GET", getExerciseData, onError);
    }
}
function getDietRecords(){
	recordType  = "1";
	getAjaxObject(ipAddress+"/patient/diet/"+patientId,"GET",onGetVitalsSuccess,onError);
}
function getExcersizeRecords(){
	recordType  = "2";
	getAjaxObject(ipAddress+"/patient/exercise/"+patientId,"GET",onGetVitalsSuccess,onError);
}
function onGetHBTPatientData(dataObj){
	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		getVitals();
	}
}
function getVitals(){
	
}

function getVitalInfo(vName,dt){
	for(var i=0;i<vitalDataArray.length;i++){
		var item = vitalDataArray[i];
		if(item.vital == vName && item.date == dt){
			return item.value;
		}
	}
	return "";
}

function getRowColors(index) {
    var classes = ['active', 'success', 'info', 'warning', 'danger'];
   // console.log(index);
    if (index % 2 === 0 && index / 2 < classes.length) {
    	//console.log(classes[index / 2]);
    	return classes[index / 2];
    	//console.log(index+','+classes[index / 2]);
      /*  return {
            classes: classes[index / 2]
        };*/
    }
    return "";
}
var dietDataArray = [];
function onGetVitalsSuccess(dataObj){
	var dietArr = [];
	if(dataObj){
		if($.isArray(dataObj)){
			dietArr = dataObj;
		}else{
			dietArr.push(dataObj);
		}
	}
	dietDataArray = dietArr;
	$("#divDiet").text("");
	var strTable = '<table class="table">';
	strTable = strTable+'<thead class="fillsHeader whiteColor">';
	strTable = strTable+'<tr>';
	//strTable = strTable+'<th>Patient ID</th>';
	strTable = strTable+'<th class="textAlign whiteColor">File Name</th>';
	strTable = strTable+'<th class="textAlign whiteColor">File Type</th>';
	strTable = strTable+'</tr>';
	strTable = strTable+'</thead>';
	strTable = strTable+'<tbody>';
	for(var i=0;i<dietArr.length;i++){
		var dataItem = dietArr[i];
		if(dataItem){
			//console.log(dataItem);
			var className = getRowColors(i);
			strTable = strTable+'<tr class="'+className+'">';
			//strTable = strTable+'<td>'+dataItem.patientId+'</td>';
			strTable = strTable+'<td class="textAlign">'+dataItem.fileName+'</td>';
			var dietId = dataItem.id;
			if(dataItem.fileType && (dataItem.fileType.toLowerCase() == "mp4" || dataItem.fileType.toLowerCase() == "mov")){
				strTable = strTable+'<td style="width:200px;padding:0px"><img id="'+dietId+'" src="../../img/AppImg/HosImages/video.png"  class="cusrsorStyle videoIcon" onClick="onClickGenerate(event)"></td>';
			}else if(dataItem.fileType && dataItem.fileType.toLowerCase() == "pdf"){
				strTable = strTable+'<td style="width:200px;padding:0px"><img  id="'+dietId+'" src="../../img/AppImg/HosImages/pdf.png"   class="cusrsorStyle pdfIcon" onClick="onClickGenerate(event)"></td>';
			}else{
				strTable = strTable+'<td style="width:200px;padding:0px"><img id="'+dietId+'" src="../../img/AppImg/HosImages/video.png"  class="cusrsorStyle videoIcon" onClick="onClickDietYoutube(event)"></td>';
			}
			strTable = strTable+'</tr>';
		}
	}
	strTable = strTable+'</tbody>';
	strTable = strTable+'</table>';
	$("#divDiet").append(strTable);
}
var imgImage = null;
function onClickGenerate(event){
	console.log(event);
	var dietVideoId = "";
	if(imgImage){
		$(imgImage).removeClass("imgBorder");
	}
	
	if(event.currentTarget){
		dietVideoId = event.currentTarget.id;
		imgImage = event.currentTarget;
		$(imgImage).addClass("imgBorder");
	}
	var fileType = findFileType(dietDataArray,dietVideoId);
	showPdfVideo(dietVideoId,fileType,"diet");
}
function showPdfVideo(fileId,fileType,sType){
	parentRef.dietId = fileId;
	parentRef.sType = sType;
	var urlPath = "";
	if(sType == "diet"){
		urlPath = ipAddress+"/patient/diet/download/"
	}else{
		urlPath = ipAddress+"/patient/exercise/download/"
	}
	var popW =900;
    var popH = 580;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
	if(fileType && fileType.toLowerCase() == "pdf"){
		 profileLbl = "Document";
		 popW = 1100;
		 devModelWindowWrapper.openPageWindow("../../html/patients/showPdf.html", profileLbl, popW, popH, true, closeVideoScreen);
			//var reqUrl = urlPath+fileId;
			//window.open(reqUrl, "popupWindow", "width=1000,height=600,scrollbars=yes");
			//$(imgImage).removeClass("imgBorder");
			//$(imgImage).removeClass("textBorder");
	}else{
	    profileLbl = "Video";
	    devModelWindowWrapper.openPageWindow("../../html/patients/showVideo.html", profileLbl, popW, popH, true, closeVideoScreen);
	}
}
function onClickDietYoutube(event){
	var dietVideoId = "";
	var urlPath = "";
	if(imgImage){
		$(imgImage).removeClass("imgBorder");
	}
	
	if(event.currentTarget){
		dietVideoId = event.currentTarget.id;
		imgImage = event.currentTarget;
		$(imgImage).addClass("imgBorder");
	}
	for(var i=0;i<dietDataArray.length;i++){
		var item = dietDataArray[i];
		if(item && item.id == event.currentTarget.id){
			urlPath = item.youtubeLink;
			iItem = item;
			break;
		}
	}
	if(urlPath && urlPath.indexOf("www.youtube.com")>=0){
		playYouTubeFile(urlPath);
	}
}
function playYouTubeFile(urlPath, fileId, ext) {
    var popW = 900;
    var popH = 580;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Video";
    parentRef.sType = "illness";
    parentRef.fileType = "Video";
    parentRef.extension = ext;
    if(urlPath == ""){
        var urlExtn =  '/homecare/download/diet-types/?id='+fileId;
        urlPath = ipAddress +urlExtn+"&access_token="+sessionStorage.access_token+"&tenant=" + sessionStorage.tenant;
        profileLbl = "Image/Document";
        parentRef.fileType = "File";
    }
    parentRef.illUrlPath = urlPath;
    devModelWindowWrapper.openPageWindow("../../html/patients/showVideo.html", profileLbl, popW, popH, true, closeVideoScreen);
}

var imgImage = null;
function onClickMyAudio(event){
	var audioId = "";
	if(imgImage){
		$(imgImage).removeClass("textBorder");
	}
	
	if(event.currentTarget){
		audioId = event.currentTarget.id;
		imgImage = event.currentTarget;
		$(imgImage).addClass("textBorder");
	}
	console.log(audioId);
	if(audioId && audioId != "null"){
		playAudioFile(audioId);
	}
}
function playAudioFile(aid){
	parentRef.audioId = aid;
	var popW = 600;
    var popH = 200;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Audio";
    devModelWindowWrapper.openPageWindow("../../html/patients/showAudio.html", profileLbl, popW, popH, true, closeVideoScreen);
}
function closeVideoScreen(evt,returnData){
	//$(imgImage).removeClass("imgBorder");
	//$(imgImage).removeClass("textBorder");
}
function onError(errorObj){
	console.log(errorObj);
}
var holterReportDataArray = [];
function onGetHolterReportsData(dataObj){
		$("#divTable").text("");
		var dataArray = [];
		if(dataObj){
			if($.isArray(dataObj)){
				dataArray = dataObj;
			}else{
				dataArray.push(dataObj);
			}
		}
		holterReportDataArray = dataArray;
		var strTable = '<table class="table">';
		strTable = strTable+'<thead class="fillsHeader whiteColor">';
		strTable = strTable+'<tr>';
		//strTable = strTable+'<th>Patient ID</th>';
		strTable = strTable+'<th class="textAlign whiteColor" style="width:220px">Date Generated</th>';
		strTable = strTable+'<th class="textAlign whiteColor">File Name</th>';
		strTable = strTable+'<th class="textAlign whiteColor">File Type</th>';
		strTable = strTable+'</tr>';
		strTable = strTable+'</thead>';
		strTable = strTable+'<tbody>';
		for(var i=0;i<holterReportDataArray.length;i++){
			var dataItem = holterReportDataArray[i];
			if(dataItem){
				//console.log(dataItem);
				var className = getRowColors(i);
				strTable = strTable+'<tr class="'+className+'">';
				//strTable = strTable+'<td>'+dataItem.patientId+'</td>';
				strTable = strTable+'<td class="textAlign">'+kendo.toString(new Date(dataItem.createdDate),"MM-dd-yyyy h:mm:ss")+'</td>';
				strTable = strTable+'<td class="textAlign">'+dataItem.fileName+'</td>';
				var dietId = dataItem.id;
				if(dataItem.fileType == "mp4"){
					strTable = strTable+'<td style="width:200px;padding:0px" class="textAlign"><img id="'+dietId+'" src="../../img/AppImg/HosImages/video.png"  class="cusrsorStyle videoIcon" onClick="onClickGenerate(event)"></td>';
				}else if(dataItem.fileType == "pdf"){
					strTable = strTable+'<td style="width:200px;padding:0px" class="textAlign"><img  id="'+dietId+'" src="../../img/AppImg/HosImages/pdf.png"    class="cusrsorStyle pdfIcon" onClick="onClickHolterReport(event)"></td>';
				}
				strTable = strTable+'</tr>';
			}
		}
		strTable = strTable+'</tbody>';
		strTable = strTable+'</table>';
		$("#divHolterReports").append(strTable);
	}
function buttonEvents(){
	$("#btnCancelDet").off("click",onClickCancel);
	$("#btnCancelDet").on("click",onClickCancel);
	
	$("#lblDiet").off("click");
	$("#lblDiet").on("click",onClickDiet);
	
	$("#lblExcersize").off("click");
	$("#lblExcersize").on("click",onClickExcersize);
	
	$("#btnAdd").off("click");
	$("#btnAdd").on("click",onClickAddDiet);

    $("#btnDietAdd").off("click");
    $("#btnDietAdd").on("click",onClickAdd);

    $("#btnEdit").off("click",onClickEdit);
    $("#btnEdit").on("click",onClickEdit);

    $("#btnDelete").off("click",onClickDelete);
    $("#btnDelete").on("click",onClickDelete);

    $("#btnCancel").off("click", onClickCancel);
    $("#btnCancel").on("click", onClickCancel);

    $(".btnActive").on("click", function(e) {
        e.preventDefault();
        searchOnLoad('active');
        onClickActive();
    });
    $(".btnInActive").on("click", function(e) {
        e.preventDefault();
        searchOnLoad('inactive');
        onClickInActive();
    });

    $("#btnCondition").off("click", OpenIllness);
    $("#btnCondition").on("click", OpenIllness);

    $("#btnSave").off("click", onClickSave);
    $("#btnSave").on("click", onClickSave);

    $("#btnReset").off("click", onClickReset);
    $("#btnReset").on("click", onClickReset);

}


function onClickSave() {
    var dataUrl;
    var dataArray = [];
    var dataObj = {};

    if(recordType == "1") {
        var strNotes = $("#txtNotes").val();
        strNotes = $.trim(strNotes);

        var strRemarks = $("#txtRemarks").val();
        strRemarks = $.trim(strRemarks);

        var isActive = 0;
        var isDelete = 0;


        if($("#cmbStatus").val() != 1){
            isDelete = 1;
        }
        dataObj.createdBy = Number(sessionStorage.userId);
        dataObj.isDeleted = isDelete;
        dataObj.isActive = $("#cmbStatus").val();
        dataObj.patientId = patientId;
        dataObj.dietTypeId = SUdietId;
        dataObj.notes = strNotes;
        dataObj.remarks = strRemarks;

        dataUrl = ipAddress + "/homecare/patient-diets/";
    }
    else {
        var strNotes = $("#txtNotes").val();
        strNotes = $.trim(strNotes);

        var strRemarks = $("#txtRemarks").val();
        strRemarks = $.trim(strRemarks);

        var isActive = 0;
        var isDelete = 0;


        if($("#cmbStatus").val() != 1){
            isDelete = 1;
        }

        dataObj.createdBy = Number(sessionStorage.userId);
        dataObj.isDeleted = isDelete;
        dataObj.isActive = $("#cmbStatus").val();
        dataObj.patientId = patientId;
        dataObj.exerciseTypeId = SUexerciseId;
        dataObj.notes = strNotes;
        dataObj.remarks = strRemarks;

        dataUrl = ipAddress + "/homecare/patient-exercises/";
    }

    var method = "POST";
    if (operation == UPDATE) {
        method = "PUT";
        dataObj.id = atID;
    }
    dataArray.push(dataObj);
    createAjaxObject(dataUrl + 'batch/', dataArray, method, onCreate, onError);
}

function onCreate(dataObj){
    console.log(dataObj);
    if(dataObj && dataObj.response && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            if(recordType == "1") {
                if (operation == ADD) {
                    customAlert.info("info", "Service User Diet created successfully");
                } else {
                    customAlert.info("info", "Service User Diet updated successfully");
                }
            }
            else{
                if (operation == ADD) {
                    customAlert.info("info", "Service User Exercise created successfully");
                } else {
                    customAlert.info("info", "Service User Exercise updated successfully");
                }
            }
            $(".btnActive").click();
        }else{
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}

function onClickReset() {
    if(operation == ADD) {
        operation = ADD;
    }
    else {
        operation = UPDATE;
    }
    SUdietId= "";
    SUexerciseId = "";
    $("#txtNotes").val("");
    $("#txtCondition").val("");
    $("#txtID").html("");
    $("#txtRemarks").val("");
}


function onClickDelete(){
	var url;
    customAlert.confirm("Confirm", "Are you sure to delete?",function(response){
        if(response.button == "Yes"){
            if(recordType == "1") {
                url = 	ipAddress + "/homecare/patient-diets/";
            }
            else {
                url = 	ipAddress + "/homecare/patient-exercises/";
            }
            var dataObj = {};
            dataObj.modifiedBy = Number(sessionStorage.userId);
            dataObj.isDeleted = 1;
            dataObj.isActive = 0;
            dataObj.id = atID;
            var dataUrl = url;
            var method = "DELETE";
            createAjaxObject(dataUrl, dataObj, method, onCreateDelete, onError);
        }
    });
}

function onCreateDelete(respObj) {
    console.log(respObj);
    var msg;
    if (respObj && respObj.response && respObj.response.status) {
        if (respObj.response.status.code == "1") {
        	if(recordType == "1") {
                msg = "Diet deactivated successfully";
            }
            else {
                msg = "Exercise deactivated successfully";
			}
            customAlert.error("Info", msg);
            operation = ADD;
            init();

        } else {
            customAlert.error("error", respObj.response.status.message);
        }
    }
}

function onClickDiet(){
	removeSelection();
	$("#divLblDiet").addClass("selection");
	$("#lblDiet").addClass("selectionLbl");
	getDietRecords();
}
function onClickExcersize(){
	removeSelection();
	$("#divLblExcersize").addClass("selection");
	$("#lblExcersize").addClass("selectionLbl");
	getExcersizeRecords();
}
function removeSelection(){
	$("#divLblDiet").removeClass("selection");
	$("#lblDiet").removeClass("selectionLbl");
	
	$("#divLblExcersize").removeClass("selection");
	$("#lblExcersize").removeClass("selectionLbl");
}
function onClickAddDiet(){
	if(recordType == "1"){
		showDietTypes("Patient Diet"," Patient Diet",recordType);
	}else{
		showDietTypes("Patient Exercise"," Patient Exercise",recordType);
	}
}
function showDietTypes(type,tit,recType){
	var popW = "67%";
    var popH = 450;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = tit;
    parentRef.recordType = recType;
    parentRef.patientId = patientId;
    devModelWindowWrapper.openPageWindow("../../html/patients/dietExerciseList.html", profileLbl, popW, popH, true, closeDietExcersizeList);
}
function closeDietExcersizeList(evt,returnData){
	if(returnData && returnData.status == "success"){
		if(recordType == "1"){
			customAlert.info("info", "Diet file(s) attached/removed successfully.");
		}else if(recordType == "2"){
			customAlert.info("info", "Exercise file(s) attached/removed successfully.");
		}
	}
	if(recordType == "1"){
		getDietRecords();
	}else{
		getExcersizeRecords();
	}
}
function adjustHeight(){
	var defHeight = 60;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    $("#divPtHolter").height(cmpHeight);
	//angularUIgridWrapper.adjustGridHeight(cmpHeight);
}


function onClickCancel(){
	$("#dietInfoTab").show();
    $("#divTypeDetails").hide();
    $("#lidietInfoTab").show();
    $("#liexerciseInfoTab").show();
}


function buildDietListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "ID",
        "field": "idk",
    });
    gridColumns.push({
        "title": "Condition",
        "field": "dietTypeValue"
    });
    gridColumns.push({
        "title": "Notes",
        "field": "notes"
    });
    gridColumns.push({
        "title": "Remarks",
        "field": "remarks"
    });
    gridColumns.push({
        "title": "Video / Document",
        "field": "dietTypeVideoUrl",
        "cellTemplate":showVideo()
    });
    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
}

function onChange(){
    setTimeout(function() {
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if (selectedItems && selectedItems.length > 0) {
            var obj = selectedItems[0];
            if(recordType == "1") {
                atID = obj.dietId;
            }
            else {
                atID = obj.exerciseId;
			}
            $("#btnEdit").prop("disabled", false);
            $("#btnDelete").prop("disabled", false);
        } else {
            $("#btnEdit").prop("disabled", true);
            $("#btnDelete").prop("disabled", true);
        }
    });
}

function showVideo() {
    var node = '<div style="width:200px;padding:0px" ng-show="((row.entity.dietTypeVideoUrl))">';
    node += '<img id="{{row.entity.id}}" src="../../img/AppImg/HosImages/video.png"  class="cusrsorStyle" style="width: 23px;margin-left: 60px;" onClick="onClickDietYoutube(event)">';
    node += '</div>';
    node += '<div style="margin-top:-4px" ng-show="((row.entity.dietTypeVideoUrl ==  \'\'))" class="textAlign">';
    node += '<img id="{{row.entity.id}}" src="../../img/img-icon.png"  class="cusrsorStyle" style="width: 23px;margin-left: 60px;" onClick="onClickDietYoutube(event)">';
    node += '</div>';
    return node;
}

var imgImage = null;
function onClickDietYoutube(event){
    var dietVideoId = "", ext= "";
    var urlPath = "";
    if(imgImage){
        $(imgImage).removeClass("imgBorder");
    }

    if(event.currentTarget){
        imgImage = event.currentTarget;
        $(imgImage).addClass("imgBorder");
    }
    for(var i=0;i<dataArray.length;i++){
        var item = dataArray[i];
        if(item && item.id == event.currentTarget.id){
            urlPath = item.dietTypeVideoUrl;
            dietVideoId = item.dietTypeId;
            var fNameSplit = item.dietTypeCode.split('.');

            if(fNameSplit[1] != undefined && fNameSplit[1] != "") {
                ext = fNameSplit[1];
            }
            break;
        }
    }
    // if(urlPath && urlPath.indexOf("www.youtube.com")>=0){
    playYouTubeFile(urlPath, dietVideoId, ext);
    // }
}
// function playYouTubeFile(urlPath){
//     var popW =900;
//     var popH = 580;
//     var profileLbl;
//     var devModelWindowWrapper = new kendoWindowWrapper();
//     profileLbl = "Video";
//     parentRef.sType = "illness";
//     parentRef.illUrlPath = urlPath;
//     devModelWindowWrapper.openPageWindow("../../html/patients/showVideo.html", profileLbl, popW, popH, true, closeVideoScreen);
// }

function closeVideoScreen(evt,returnData){
    //$(imgImage).removeClass("imgBorder");
    //$(imgImage).removeClass("textBorder");
}

function getDietsData(dataObj) {
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            dataArray = dataObj.response.patientDiets || [];
            if (dataArray.length) {
                for (var i = 0; i < dataArray.length; i++) {
                    var ind = i + 1;
                    dataArray[i].idk = ind;
                    dataArray[i].dietId = dataArray[i].id;
                    dataArray[i].dietTypeId = dataArray[i].dietTypeId;
                    dataArray[i].dietTypeCode = dataArray[i].dietTypeCode;
                }
            }
        } else {
            customAlert.error("error", dataObj.response.status.message);
        }
    }
    buildDietListGrid(dataArray);
}

function getExerciseData(dataObj) {
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            dataArray = dataObj.response.patientExercises || [];
            if (dataArray.length) {
                for (var i = 0; i < dataArray.length; i++) {
                    var ind = i + 1;
                    dataArray[i].idk = ind;
                    dataArray[i].exerciseId = dataArray[i].id;
                    dataArray[i].dietTypeId = dataArray[i].exerciseTypeId;
                    dataArray[i].dietTypeValue = dataArray[i].exerciseTypeValue;
                    dataArray[i].dietTypeVideoUrl = dataArray[i].exerciseTypeVideoUrl;
                    dataArray[i].dietTypeCode = dataArray[i].exerciseTypeCode;


                }
            }
        } else {
            customAlert.error("error", dataObj.response.status.message);
        }
    }
    buildDietListGrid(dataArray);
}

function addDietData(opr){
    parentRef.operation = opr;
    parentRef.recordType = recordType;
    operation = opr;
    if(opr == "add"){
        $('.tabContentTitle').text('Add Service User Diet');
        $('#btnReset').trigger('click');
    }else{

        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if (selectedItems && selectedItems.length > 0) {
            var obj = selectedItems[0];
            parentRef.id = obj.dietId;
            parentRef.dietTypeId  = obj.dietTypeId;
            parentRef.dietTypeValue = obj.dietTypeValue;
            parentRef.notes = obj.notes
            parentRef.remarks = obj.remarks;
            parentRef.isActive = obj.isActive;
            operation = UPDATE;
            onClickRPSearch(obj);
        }
        $('.tabContentTitle').text('Edit Service User Diet');
        $("#divTypeDetails").show();
    }
}

function addExerciseData(opr){
    parentRef.recordType = recordType;
    operation = opr;
    if(opr == "add"){
        $('.tabContentTitle').text('Add Service User Exercise');
        $('#btnReset').trigger('click');
    }else{

        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if (selectedItems && selectedItems.length > 0) {
            var obj = selectedItems[0];
            parentRef.id = obj.exerciseId;
            parentRef.exerciseTypeId  = obj.exerciseTypeId;
            parentRef.exerciseTypeValue = obj.exerciseTypeValue;
            parentRef.notes = obj.notes
            parentRef.remarks = obj.remarks;
            parentRef.isActive = obj.isActive;
            operation = UPDATE;
            onClickRPSearch(obj);
        }
        profileLbl = "Edit Service User Exercise";
        $('.tabContentTitle').text('Edit Service User Exercise');
    }
    // var devModelWindowWrapper = new kendoWindowWrapper();
    // devModelWindowWrapper.openPageWindow("../../html/patients/createDiet.html", profileLbl, popW, popH, true, closeAction);
}

function closeAction(evt,returnData) {
    if (returnData && returnData.status == "success") {
        var opr = returnData.operation;
        if(recordType == "1") {
            if (opr == "add") {
                customAlert.info("info", "Service User Diet saved successfully");
            } else {
                customAlert.info("info", "Service User Diet updated successfully");
            }
        }
        else{
            if (opr == "add") {
                customAlert.info("info", "Service User Exercise saved successfully");
            } else {
                customAlert.info("info", "Service User Exercise updated successfully");
            }
		}
        init();
    }
}

function onClickActive() {
    $(".btnInActive").removeClass("radioButton-active");
    $(".btnActive").addClass("radioButton-active");
}

function onClickInActive() {
    $(".btnActive").removeClass("radioButton-active");
    $(".btnInActive").addClass("radioButton-active");
}

function searchOnLoad(status) {
    buildDietListGrid([]);
    if(recordType == "1") {
        var urlExtn = ipAddress + "/homecare/patient-diets/";
        if (status == "active") {
            urlExtn = urlExtn + "?is-active=1&is-deleted=0&fields=*,dietType.*&patientId=" + patientId;
        }
        else if (status == "inactive") {
            urlExtn = urlExtn + "?is-active=0&is-deleted=1&fields=*,dietType.*&patientId=" + patientId;
        }
        getAjaxObject(urlExtn, "GET", getDietsData, onError);
    }
    else{
        var urlExtn = ipAddress + "/homecare/patient-exercises/";
        if (status == "active") {
            urlExtn = urlExtn + "?is-active=1&is-deleted=0&fields=*,exerciseType.*&patientId=" + patientId;
        }
        else if (status == "inactive") {
            urlExtn = urlExtn + "?is-active=0&is-deleted=1&fields=*,exerciseType.*&patientId=" + patientId;
        }
        getAjaxObject(urlExtn, "GET", getExerciseData, onError);
	}
}

function onClickAdd(){
    parentRef.operation = "add";
    if(recordType=="1"){
        $("#liexerciseInfoTab").hide();
        addDietData("add");
	}else{
        $("#lidietInfoTab").hide();
        addExerciseData("add");
	}
	$("#divTypeDetails").show();
    $("#dietInfoTab").hide();

}

function onClickEdit(){
    $("#divTypeDetails").show();
    $("#dietInfoTab").hide();
    parentRef.operation = "edit";
    if(recordType=="1"){
        $("#liexerciseInfoTab").hide();
        addDietData("edit");
    }else{
        $("#lidietInfoTab").hide();
        addExerciseData("edit");
    }
}

function OpenIllness(){
    var popW = 800;
    var popH = 500;
    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    if(recordType == "1") {
        profileLbl = "Diet List";
    }
    else{
        profileLbl = "Exercise List";
    }
    devModelWindowWrapper.openPageWindow("../../html/masters/addDietList.html", profileLbl, popW, popH, true, closeDietAction);
}
var SUdietId, SUexerciseId
function closeDietAction(evt, returnData) {
    if (returnData && returnData.status == "success") {
        $("#txtCondition").val(returnData.selItem.value);
        if(recordType == "1"){
            SUdietId = returnData.selItem.idk;
        }
        else {
            SUexerciseId = returnData.selItem.idk;
        }
    }
}


function onClickRPSearch(selItem){
    if(recordType == "1") {
        if (selItem.id != "") {
            $("#txtID").html("ID: "+selItem.idk);
            SUdietId = selItem.dietTypeId;
            $("#txtCondition").val(selItem.dietTypeValue);
            $("#txtNotes").val(selItem.notes);
            $("#txtRemarks").val(selItem.remarks);
            $("#cmbStatus").val(selItem.isActive)

        }
    }
    else {
        if (selItem.id != "") {
            $("#txtID").html("ID: "+selItem.idk);
            SUexerciseId = selItem.exerciseTypeId;
            $("#txtCondition").val(selItem.exerciseTypeValue);
            $("#txtNotes").val(selItem.notes);
            $("#txtRemarks").val(selItem.remarks);
            $("#cmbStatus").val(selItem.isActive)
        }
    }
}
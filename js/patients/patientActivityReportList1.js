var parentRef = null;
var angularUIgridWrapper1;

var patientId = "";

var activityid = "1";

var ADL = "1";
var IADL = "2";
var CARE = "3";
var FLUID = "4";
var FOOD = "5";

var radioValue = "1";

var typeArr = [{Key:'ADL',Value:'1'},{Key:'iADL',Value:'2'},{Key:'Care',Value:'3'},{Key:'Fluid',Value:'4'},{Key:'Food',Value:'5'}];
var compType = [{Key:'Text',Value:'1'},{Key:'Text Area',Value:'2'},{Key:'Boolean',Value:'3'},{Key:'Checkbox',Value:'4'},{Key:'Radio Button',Value:'5'}];

$(document).ready(function(){
    parentRef = parent.frames['iframe'].window;
    patientId = parentRef.patientId;
    sessionStorage.setItem("IsSearchPanel", "1");

	initTaskGroups();

	getAjaxObject(ipAddress+"/activity-types/list?is-active=1&is-deleted=0","GET",getActivityGroup,onError);
});


function initTaskGroups(){
	var dataOptionsTaskGroup = {
        pagination: false,
        changeCallBack: onChangeAppointmentReason
    }
    angularUIgridWrapper1 = new AngularUIGridWrapper("dgridActivTypeList", dataOptionsTaskGroup);
    angularUIgridWrapper1.init();
    buildStateListGrid([]);
}

$(window).load(function(){

	$(window).resize(adjustHeight);
	onLoaded();
	if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function getActivityNameById(aId){
	for(var i=0;i<typeArr.length;i++){
		var item = typeArr[i];
		if(item && item.Value == aId){
			return item.Key;
		}
	}
	return "";
}

function getGroupNameById(aId){
	for(var i=0;i<actGroup.length;i++){
		var item = actGroup[i];
		if(item && item.id == aId){
			return item.type;
		}
	}
	return "";
}


function getTypeNameById(aId){
	for(var i=0;i<compType.length;i++){
		var item = compType[i];
		if(item && item.Value == aId){
			return item.Key;
		}
	}
	return "";
}
function onLoaded(){
	parentRef = parent.frames['iframe'].window;
	//loadMenus();
    // getAjaxObject(ipAddress+"/patient/activity/type/list/?patient-id="+patientId,"GET",getTaskTypes,onError);

    var url = ipAddress+"/master/appointment_reason/list/?is-active=1&is-deleted=0";
    getAjaxObject(url,"GET",handleGetTemplateList,onError);
	init();
    buttonEvents();
	adjustHeight();
}

function loadMenus(){
	var strMenu = "";
	$("#setupPlanMenu").html("");
	for(var i=0;i<typeArr.length;i++){
		var item = typeArr[i];
		var strItem = item.Key;
		var strVal = item.Value;
		if(i == 0){
			strMenu = strMenu+'<a href="#" class="list-group-item sub-item paddingLeftStyle subMenuStyle selectedMenu " id='+strVal+' name='+strVal+'>'+strItem+'</a>';
            activityid = strVal;
		}else{
			strMenu = strMenu+'<a href="#" class="list-group-item sub-item paddingLeftStyle subMenuStyle " id='+strVal+' name='+strVal+'>'+strItem+'</a>';
		}
		
	}
	//console.log(strItem);
	$("#setupPlanMenu").append(strMenu);
	for(var j=0;j<typeArr.length;j++){
		var item = typeArr[j];
		var strItem = item.Key;
		var strVal = item.Value;
		$("#"+strVal).off("click");
		$("#"+strVal).on("click",onClickItem);
		
		$("#"+strVal).off("mouseover");
		$("#"+strVal).on("mouuseover",onClickMouseOver);
		
		$("#"+strVal).off("mouseout");
		$("#"+strVal).on("mouseout",onClickMouseOut);
	}
}
function onClickMouseOver(evt){
	var strId = evt.currentTarget.id;
	$("#"+strId).addClass("selectedMenu");
}
function onClickMouseOut(evt){
	var strId = evt.currentTarget.id;
	//$("#"+strId).removeClass("selectedMenu");
}
function onClickItem(evt){
	//console.log(evt);
	removeSelections();
	var strId = evt.currentTarget.id;
	$("#"+strId).addClass("selectedMenu");
	var strId = evt.currentTarget.name;
	var strActivity = getActivityNameById(strId);
	$("#lblActivity").text(strActivity);
	activityid = strId;
	showGrid();
	//init();
}
function removeSelections(){
	for(var i=0;i<typeArr.length;i++){
		var item = typeArr[i];
		var strItem = item.Key;
		var strVal = item.Value;
		$("#"+strVal).removeClass("selectedMenu");
	}
}
function onClickAddPatientActivities(){
	var popW = "75%";
    var popH = "75%";

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Tasks";
    parentRef.patientId = patientId;
    devModelWindowWrapper.openPageWindow("../../html/patients/patientActivityList.html", profileLbl, popW, popH, true, closePatientActivitiesList);
}
function closePatientActivitiesList(evt,returnData){
	if(returnData && returnData.status == "success"){
		//customAlert.info("info", "Tasks attached/removed successfully.");
		initTaskGroups();
		onLoaded();
		//showGrid();
	}
}
function init(){
	patientId = parentRef.patientId;
	getAjaxObject(ipAddress+"/master/type/list/?name=component","GET",getComponentTypes,onError);
}
function getComponentTypes(dataObj){
	var tempCompType = [];
	compType = [];
	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		if(dataObj.response.typeMaster){
			if($.isArray(dataObj.response.typeMaster)){
				tempCompType = dataObj.response.typeMaster;
			}else{
				tempCompType.push(dataObj.response.typeMaster);
			}
		}
	}
	for(var i=0;i<tempCompType.length;i++){
		var obj = {};
		obj.Key = tempCompType[i].type;
		obj.Value = tempCompType[i].id;
		compType.push(obj);
	}

    getAjaxObject(ipAddress+"/patient/activity/type/list/?patient-id="+patientId,"GET",getActivityTypes,onError);
}

function getActivityTypes(dataObj){
	var tempCompType = [];
	typeArr = [];
	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		if(dataObj.response.arrayList){
			if($.isArray(dataObj.response.arrayList)){
				tempCompType = dataObj.response.arrayList;
			}else{
				tempCompType.push(dataObj.response.arrayList);
			}
		}
	}
    tempCompType.sort(function(a, b) {
        var nameA = a.type.toUpperCase(); // ignore upper and lowercase
        var nameB = b.type.toUpperCase(); // ignore upper and lowercase
        if (nameA < nameB) {
            return -1;
        }
        if (nameA > nameB) {
            return 1;
        }
        // names must be equal
        return 0;
    });


	for(var i=0;i<tempCompType.length;i++) {
		var obj = {};
		obj.Key = tempCompType[i].type;
		obj.Value = tempCompType[i].id;
		typeArr.push(obj);
		if (radioValue == 2) {
			buildStateListGrid([]);
    		buildStateListGrid(typeArr);
		}


	}
	loadMenus();
	showGrid();
}
function showGrid(){
	$("#divptActivies").text("");
	if(radioValue == 2) {
		getAjaxObject(ipAddress + "/patient/activity/list/" + patientId + "/?is-active=1", "GET", onGetHolterReportsData, onError);
	}
	else{
		var cmbAppointmentReason = $("#cmbAppointmentReason").data("kendoComboBox");
		var appointmentReasonId = Number(cmbAppointmentReason.value());
		getAjaxObject(ipAddress + "/homecare/appointment-reason-activities/?is-active=1&patient-id="+patientId+"&appointment-reason-id="+appointmentReasonId, "GET", onGetHolterReportsData, onError);
	}
}

function onError(errorObj){
	//console.log(errorObj);
}
var holterReportDataArray = [];
function onGetHolterReportsData(dataObj){
	debugger;
	$("#divptActivies").text("");
	ptactivites = [];

	if(radioValue == 2){
		if (dataObj && dataObj.response && dataObj.response.patientActivity) {
			if ($.isArray(dataObj.response.patientActivity)) {
				ptactivites = dataObj.response.patientActivity;
			} else {
				ptactivites.push(dataObj.response.patientActivity);
			}
		}
	}
	else{
		if (dataObj && dataObj.response && dataObj.response.appointmentReasonActivities) {
			if ($.isArray(dataObj.response.appointmentReasonActivities)) {
				ptactivites = dataObj.response.appointmentReasonActivities;
			} else {
				ptactivites.push(dataObj.response.appointmentReasonActivities);
			}
		}
	}

	/*if (radioValue == 1) {*/
		for(var i=0;i<ptactivites.length;i++){
			var obj = {};
			if(radioValue == 2) {
				ptactivites[i].Key = getActivityNameById(ptactivites[i].activityId);
			}
			else{
				ptactivites[i].Key = getGroupNameById(ptactivites[i].activityTypeId);
			}
		}

		buildStateListGrid([]);
		buildStateListGrid(ptactivites);
	/*}*/


	var strTable = '<table class="table">';
	strTable = strTable+'<thead class="fillsHeader">';
	strTable = strTable+'<tr>';
	strTable = strTable+'<th class="textAlign whiteColor">Task</th>';
	//strTable = strTable+'<th class="textAlign whiteColor">Activity Type</th>';
	//strTable = strTable+'<th class="textAlign whiteColor">Charge</th>';
	//strTable = strTable+'<th class="textAlign whiteColor">Duration</th>';
	strTable = strTable+'<th class="textAlign whiteColor">Required</th>';
	strTable = strTable+'</tr>';
	strTable = strTable+'</thead>';
	strTable = strTable+'<tbody>';
    var cmbAppointmentReason = $("#cmbAppointmentReason").data("kendoComboBox");
    var appointmentReasonId = Number(cmbAppointmentReason.value());

	for(var i=0;i<ptactivites.length;i++){
		var dataItem = ptactivites[i];
		if(((dataItem && dataItem.activity && dataItem.activity.activityTypeId == activityid && dataItem.activity.isDeleted == 0) || radioValue == "1" )&& ((radioValue == "1" && dataItem.appointmentReasonId == appointmentReasonId)|| radioValue == "2")){
			var className = getRowColors(i);
			if(radioValue == 2) {
				if (dataItem.activity.type == 0) {
					className = "info";
				} else {
					className = "danger";
				}
			}
			else{
				className = "danger";
			}
			strTable = strTable+'<tr  class="'+className+'" style="cursor:pointer">';
			//strTable = strTable+'<td>'+dataItem.patientId+'</td>';
			if(radioValue == 2) {
				if (dataItem.activity.activity) {
					strTable = strTable + '<td class="textAlign">' + dataItem.activity.activity + '</td>';
				} else {
					strTable = strTable + '<td></td>';
				}
				if(dataItem.required){
					strTable = strTable+'<td class="textAlign">True</td>';
				}else{
					strTable = strTable+'<td class="textAlign">False</td>';
				}
			}
			else{
				if (dataItem.Key) {
					strTable = strTable + '<td class="textAlign">' + getActivityNameById(dataItem.activityId)+ '</td>';
				} else {
					strTable = strTable + '<td></td>';
				}
				if(dataItem.required){
					strTable = strTable+'<td class="textAlign">True</td>';
				}else{
					strTable = strTable+'<td class="textAlign">False</td>';
				}
			}
			/*var act = getActivityNameById(dataItem.activity.activityTypeId);
			//strTable = strTable+'<td>'+act+'</td>';
			
			var cType = getTypeNameById(dataItem.activity.componentId);*/
			//strTable = strTable+'<td>'+cType+'</td>';
			/*if(dataItem.activity.charge){
				//strTable = strTable+'<td>'+dataItem.activity.charge+'</td>';
			}else{
				//strTable = strTable+'<td></td>';
			}
			if(dataItem.activity.duration){
				//strTable = strTable+'<td>'+dataItem.activity.duration+'</td>';
			}else{
				//strTable = strTable+'<td></td>';
			}*/

			
			strTable = strTable+'</tr>';
		}
	}
	strTable = strTable+'</tbody>';
	strTable = strTable+'</table>';
	$("#divptActivies").append(strTable);

	}
function getRowColors(index) {
    var classes = ['active', 'success', 'info', 'warning', 'danger'];
   // console.log(index);
    if (index % 2 === 0 && index / 2 < classes.length) {
    	//console.log(classes[index / 2]);
    	return classes[index / 2];
    	//console.log(index+','+classes[index / 2]);
      /*  return {
            classes: classes[index / 2]
        };*/
    }
    return "";
}
function buttonEvents(){
	$("#btnCancelDet").off("click",onClickCancel);
	$("#btnCancelDet").on("click",onClickCancel);
	
	$("#btnADL").off("click",onClickAddADL);
	$("#btnADL").on("click",onClickAddADL);
	
	$("#btniADL").off("click",onClickAddiADL);
	$("#btniADL").on("click",onClickAddiADL);
	
	$("#btnCare").off("click",onClickAddCare);
	$("#btnCare").on("click",onClickAddCare);
	
	$("#btnFluid").off("click",onClickAddFluid);
	$("#btnFluid").on("click",onClickAddFluid);
	
	$("#btnFood").off("click",onClickAddFood);
	$("#btnFood").on("click",onClickAddFood);

	$("#btnSave").off("click",onClickAddPatientActivities);
	$("#btnSave").on("click",onClickAddPatientActivities);

    $("input[name=Comm]").on( "change", function() {
        radioValue = $(this).val();
        if(radioValue == "1"){
            $("#divActivity").show();
            $("#dgridActivTypeListgrid").css("height","229px");
		}else{
            $("#divActivity").hide();
            $("#dgridActivTypeListgrid").css("height","282px");
		}
	} );

	$("input[name=TaskType]").on( "change", function() {
        radioValue = $(this).val();
        if(radioValue == "1"){
            $("#divAppointmentReason").show();
		}else{
            $("#divAppointmentReason").hide();
		}
	} );


}

function adjustHeight(){
	var defHeight = 50;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    $("#divPtHolter").height(cmpHeight);
    $("#divButttons").height(cmpHeight);
	//angularUIgridWrapper.adjustGridHeight(cmpHeight);
}


function onClickAddADL(){
	$("#btnADL").addClass("selectButtonBarClass");
	$("#btniADL").removeClass("selectButtonBarClass");
	$("#btnCare").removeClass("selectButtonBarClass");
	$("#btnFluid").removeClass("selectButtonBarClass");
	$("#btnFood").removeClass("selectButtonBarClass");
	activityid = ADL;
	init();
}
function onClickAddiADL(){
	$("#btnADL").removeClass("selectButtonBarClass");
	$("#btniADL").addClass("selectButtonBarClass");
	$("#btnCare").removeClass("selectButtonBarClass");
	$("#btnFluid").removeClass("selectButtonBarClass");
	$("#btnFood").removeClass("selectButtonBarClass");
	activityid = IADL;
	init();
}
function onClickAddCare(){
	$("#btnADL").removeClass("selectButtonBarClass");
	$("#btniADL").removeClass("selectButtonBarClass");
	$("#btnCare").addClass("selectButtonBarClass");
	$("#btnFluid").removeClass("selectButtonBarClass");
	$("#btnFood").removeClass("selectButtonBarClass");
	activityid = CARE;
	init();
}
function onClickAddFluid(){
	$("#btnADL").removeClass("selectButtonBarClass");
	$("#btniADL").removeClass("selectButtonBarClass");
	$("#btnCare").removeClass("selectButtonBarClass");
	$("#btnFluid").addClass("selectButtonBarClass");
	$("#btnFood").removeClass("selectButtonBarClass");
	activityid = FLUID;
	init();
}
function onClickAddFood(){
	$("#btnADL").removeClass("selectButtonBarClass");
	$("#btniADL").removeClass("selectButtonBarClass");
	$("#btnCare").removeClass("selectButtonBarClass");
	$("#btnFluid").removeClass("selectButtonBarClass");
	$("#btnFood").addClass("selectButtonBarClass");
	activityid = FOOD;
	init();
}
function onClickPatientCall(obj){
	var popW = "70%";
    var popH = 400;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    profileLbl = "Patient Call Chart";
    parentRef.selObj = obj;
    devModelWindowWrapper.openPageWindow("../../html/patients/callPatient.html", profileLbl, popW, popH, false, closeCallPatient);
}
function closeCallPatient(evt,returnData){
	if(returnData && returnData.status == "success"){
		customAlert.info("Save","Patient Call Chart Created Successfully");
		showGrid();
	}
}
function onClickCancel(){
	var obj = {};
	var windowWrapper = new kendoWindowWrapper();
	windowWrapper.closePageWindow(obj);
}


function buildStateListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Group",
        "field": "Key",
    });
    angularUIgridWrapper1.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
}

function getTaskTypes(dataObj){
    var types = [];
    if(dataObj && dataObj.response && dataObj.response.status ){
        if(dataObj.response.status.code == "1"){
            if(dataObj.response.activityTypes){
                if($.isArray(dataObj.response.activityTypes)){
                    types = dataObj.response.activityTypes;
                }else{
                    types.push(dataObj.response.activityTypes);
                }
            }
            for(var i=0;i<types.length;i++){
                types[i].idk = types[i].id;
            }
        }
    }
    buildStateListGrid(types);
}


function handleGetTemplateList(dataObj){
    var vacation = [];
    if(dataObj && dataObj.response && dataObj.response.codeTable ){
        if(dataObj.response.status.code == "1"){
            if(dataObj.response.codeTable){
                if($.isArray(dataObj.response.codeTable)){
                    vacation = dataObj.response.codeTable;
                }else{
                    vacation.push(dataObj.response.codeTable);
                }
            }
        }
    }
    for(var j=0;j<vacation.length;j++){
        vacation[j].idk = vacation[j].id;
    }
    setDataForSelection(vacation, "cmbAppointmentReason", onTemplateChange, ["desc", "idk"], 0, "");
}

function onTemplateChange() {
	showGrid();
}

var actComponents;
function getStateList(dataObj){
    var dataArray = [];
    if(dataObj && dataObj.response && dataObj.response.activity){
        if($.isArray(dataObj.response.activity)){
            dataArray = dataObj.response.activity;
        }else{
            dataArray.push(dataObj.response.activity);
        }
    }
    var tempDataArry = [];
    for(var i=0;i<dataArray.length;i++){
        dataArray[i].idk = dataArray[i].id;
    }
    actComponents = dataArray;
    buildFileResourceSelListGrid(dataArray);
}

function onChangeAppointmentReason(){
    setTimeout(function(){
        var selectedItems = angularUIgridWrapper1.getSelectedRows();
        if(selectedItems && selectedItems.length>0){
            activityid = selectedItems[0].Value;
            showGrid();
        }else{
            // $("#btnSave").prop("disabled", true);
            $("#btnDelete").prop("disabled", true);
        }
    },100)

}
// function buildStateListGrid(dataSource) {
//     var gridColumns = [];
//     var otoptions = {};
//     otoptions.noUnselect = false;
//     gridColumns.push({
//         "title": "Group",
//         "field": "type",
//     });
//     angularUIgridWrapper1.creategrid(dataSource, gridColumns,otoptions);
//     adjustHeight();
// }


var actGroup;
function getActivityGroup(dataObj){
	var dataArray = [];
	if(dataObj && dataObj.response && dataObj.response.activity){
		if($.isArray(dataObj.response.activity)){
			dataArray = dataObj.response.activity;
		}else{
			dataArray.push(dataObj.response.activity);
		}
	}
	var tempDataArry = [];
	for(var i=0;i<dataArray.length;i++) {
		dataArray[i].idk = dataArray[i].id;
	}
	actGroup= dataArray;
}

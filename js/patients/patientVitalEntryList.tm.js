var angularUIgridWrapper;
var angularUISelgridWrapper;

var parentRef = null;
var patientId = "";
var ptAge  = "";
var ptGender = "";

$(document).ready(function(){
	parentRef = parent.frames['iframe'].window;
	patientId = parentRef.patientId;
	ptAge = parentRef.ptAge;
	ptAge = Number(ptAge);
	ptGender = parentRef.ptGender;
	var dataOptions = {
        pagination: false,
        changeCallBack: onChange
	}
	angularUIgridWrapper = new AngularUIGridWrapper("dgSelAddPatientVitalList", dataOptions);
	angularUIgridWrapper.init();
	buildFileResourceListGrid([]);
	
});


$(window).load(function(){
	$(window).resize(adjustHeight);
	onLoaded();
	if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
	init();
    buttonEvents();
	adjustHeight();
}

var recordType = "";
function init(){
	var str1 = "";
	var str2 = "";
	getAjaxObject(ipAddress+"/homecare/patient-vitals/?patient-id="+patientId+"&is-active=1&is-deleted=0&fields=*,vital.*","GET",getPatientDietList,onError);
}
function onError(errorObj){
	console.log(errorObj);
}
var dietArray = [];
var dataArray = [];

function getPatientDietList(dataObj){
	console.log(dataObj);
	 dietArray = [];
	 if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		 if(dataObj && dataObj.response && dataObj.response.patientVitals){
				if($.isArray(dataObj.response.patientVitals)){
					dietArray = dataObj.response.patientVitals;
				}else{
					dietArray.push(dataObj.response.patientVitals);
				}
			}
	}
	buildFileResourceListGrid(dietArray);
	//getAjaxObject(ipAddress+"/homecare/vitals/","GET",getCountryList,onError);
}
function getCountryList(dataObj){
	console.log(dataObj);
	dataArray = [];
	if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
		if($.isArray(dataObj.response.vitals)){
			dataArray = dataObj.response.vitals;
		}else{
			dataArray.push(dataObj.response.vitals);
		}
	}
	var tempDataArray = [];
	for(var i=0;i<dataArray.length;i++){
		if(dataArray[i].id){
			dataArray[i].idk = dataArray[i].id; 
			dataArray[i].Status = "InActive";
			if(dataArray[i].isActive == 1){
				dataArray[i].Status = "Active";
			}
			if(!getFileExist(dataArray[i].id)){
				tempDataArray.push(dataArray[i]);
			}
		}
	}
	buildFileResourceSelListGrid(tempDataArray);
}
function getFileExist(id1){
	var flag = false;
	for(var i=0;i<dietArray.length;i++){
		var dataObj = dietArray[i];
		if(dataObj && dataObj.id == id1){
			flag = true;
			break;
		}
	}
	return flag;
}
function buttonEvents(){
	$("#btnCancelDet").off("click",onClickCancel);
	$("#btnCancelDet").on("click",onClickCancel);
	
	$("#btnSave").off("click");
	$("#btnSave").on("click",onClickSave);
	
	$("#btnSubRight").off("click");
	$("#btnSubRight").on("click",onClickSubRight);
	
	$("#btnSubLeft").off("click");
	$("#btnSubLeft").on("click",onClickSubLeft);
	
}
function adjustHeight(){
	var defHeight = 200;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    if(angularUIgridWrapper){
    	angularUIgridWrapper.adjustGridHeight(cmpHeight);
    }
	if(angularUISelgridWrapper){
		angularUISelgridWrapper.adjustGridHeight(cmpHeight);
	}
}

var rFrom = "";
var rTo = "";
var child = "";
function buildFileResourceListGrid(dataSource) {
	var gridColumns = [];
	var otoptions = {};
	otoptions.noUnselect = false;
	
	if(ptAge<=10){
		child = "true";
		rFrom = "vitalPediatricRangeFrom";
		rTo = "vitalPediatricRangeTo";
	}else if(ptGender == "Male"){
		rFrom = "vitalMaleRangeFrom";
		rTo = "vitalMaleRangeTo";
	}else{
		rFrom = "vitalFemaleRangeFrom";
		rTo = "vitalFemaleRangeTo";
	}
	 gridColumns.push({
	        "title": "Abbreviation",
	        "field": "vitalAbbreviation",
		});
	
	 if(child == "true"){
		 gridColumns.push({
		        "title": "Range From",
		        "field": "vitalPediatricRangeFrom",
			});
		    gridColumns.push({
		        "title": "Range To",
		        "field": "vitalPediatricRangeTo",
			});
	 }else if(ptGender == "Male"){
		 gridColumns.push({
		        "title": "Range From",
		        "field": "vitalMaleRangeFrom",
			});
		    gridColumns.push({
		        "title": "Range To",
		        "field": "vitalMaleRangeTo",
			});
	 }else{
		 gridColumns.push({
		        "title": "Range From",
		        "field": "vitalFemaleRangeFrom",
			});
		    gridColumns.push({
		        "title": "Range To",
		        "field": "vitalFemaleRangeTo",
			});
	 }
	 gridColumns.push({
	        "title": "Flag",
	        "field": "Flag",
	        "cellTemplate": showColorTemplate(),
	       
		});
	   
    gridColumns.push({
        "title": "Value",
        "field": "SEL",
        "cellTemplate": showCheckBoxTemplate(),
	});
    gridColumns.push({
        "title": "UOM",
        "field": "vitalUnitOfMeasurement",
	});
 // "cellClass": rowCellClass
   angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions); 
	adjustHeight();
	//onIndividualRowClick();
}
var prevSelectedItem =[];
function onChange(){
	
}
function buildFileResourceSelListGrid(dataSource) {
	var gridColumns = [];
	var otoptions = {};
	otoptions.noUnselect = false;
	 gridColumns.push({
	        "title": "Select",
	        "field": "SEL",
	        "cellTemplate": showCheckBoxTemplate(),
	        "width":"15%"
		});
	 gridColumns.push({
	        "title": "Description",
	        "field": "description",
	         "width":"67%"
		});
	    gridColumns.push({
	        "title": "Abrevation",
	        "field": "abbreviation",
		});
   angularUISelgridWrapper.creategrid(dataSource, gridColumns,otoptions); 
	adjustHeight();
	//onIndividualRowClick();
}
var prevSelectedItem =[];
function onChange1(){
	
}

function showCheckBoxTemplate(){
	var node = '<div style="text-align:center;padding-top: 0px;border:1px solid #000"><input type="text" class="check1" id="chkbox" ng-model="row.entity.SEL" style="width:100%;height:20px;margin:0px;font-weight:bold" onchange="onTextValueChange(event)"></input></div>';
	return node;
}
function showColorTemplate(){
	var node = '<div style="text-align:center;padding-top: 0px;background:{{row.entity.red}}"><input type="text"   ng-model="row.entity.Flag" style="color:{{row.entity.red}};width:100%;height:20px;margin:0px;font-weight:bold" readonly></input></div>';
	return node;
}
function rowCellClass(grid, row, col, rowRenderIndex, colRenderIndex){
	var field = "";
	if(row && col && col.field){
		var fieldVal = row.entity["Flag"];
		if(fieldVal == "Normal"){
			return "normalColor";
		}else if(fieldVal == "High"){
			return "highColor";
		}else{
			return "lowColor";
		}
	}
   }
function onTextValueChange(event){
	try{
		var selRow = angular.element($(event.currentTarget).parent()).scope();
		var eVal = event.currentTarget.value;
		eVal = Number(eVal);
		var rFromVal = selRow.row.entity[rFrom];
		var rToVal = selRow.row.entity[rTo];
		if(eVal>=rFromVal && eVal<=rToVal){
			selRow.row.entity.Flag = "Normal";
			selRow.row.entity.red = "green";
		}else if(eVal>rToVal){
			selRow.row.entity.Flag = "High";
			selRow.row.entity.red = "red";
		}else{
			selRow.row.entity.Flag = "Low";
			selRow.row.entity.red = "blue";
		}
		
		angularUIgridWrapper.refreshGrid();
	}catch(ex){}
	
}
function onSelect(evt){
	console.log(evt);
}

function onClickSubRight(){
//	alert("right");
	 var selGridData = angularUISelgridWrapper.getAllRows();
	 var selList = [];
	    for (var i = 0; i < selGridData.length; i++) {
	        var dataRow = selGridData[i].entity;
	        if(dataRow.SEL){
	        	angularUISelgridWrapper.deleteItem(dataRow);
	        	//dataRow.SEL = false;
	        	angularUIgridWrapper.insert(dataRow);
	        }
	    }  
}
function onClickSubLeft(){
	//alert("click");
	 var selGridData = angularUIgridWrapper.getAllRows();
	 var selList = [];
	    for (var i = 0; i < selGridData.length; i++) {
	        var dataRow = selGridData[i].entity;
	        if(dataRow.SEL){
	        	angularUIgridWrapper.deleteItem(dataRow);
	        	angularUISelgridWrapper.insert(dataRow);
	        }
	    }  
}
function onClickSave(){
	var rows = angularUIgridWrapper.getAllRows();
	//console.log(rows);
	var dArray = [];
	for(var i=0;i<rows.length;i++){
		var rowObj = rows[i].entity;
		if(rowObj && rowObj.SEL){
				var obj = {};
				obj.createdBy = Number(sessionStorage.userId);
				obj.isActive = 1;
				obj.isDeleted = 0;
				obj.vitalId = rowObj.vitalId;
				obj.vitalValue = rowObj.SEL;
				obj.flag = rowObj.Flag;
				obj.appointmentId = 1;
				obj.dateOfReading = new Date().getTime();
				obj.patientId = patientId;
				if(i == 0){
					obj.notes = $("#taNotes").val();
				}
				dArray.push(obj);
	 }
	}
	
		var dataObj = {};
	dataObj = dArray;
	var dataUrl = ipAddress+"/homecare/patient-vital-values/batch/";
	createAjaxObject(dataUrl,dataObj,"POST",onCreate,onError);
}
function onCreate(dataObj){
	console.log(dataObj);
	var status = "fail";
	var flag = true;
	if(dataObj){
		if(dataObj && dataObj.response){
			if(dataObj.response.status){
				if(dataObj.response.status.code == "1"){
					status = "success";
					customAlert.info("Info", "Vital values are created successfully");
				}else{
					flag = false;
					customAlert.info("error", dataObj.response.status.message);
				}
			}
		}
	}
	if(flag){
		//setTimeout(function)
		var obj = {};
		obj.status = status;
		//popupClose(obj);
	}
}
function popupClose(st){
	var onCloseData = new Object();
	onCloseData = st;
	var windowWrapper = new kendoWindowWrapper();
	windowWrapper.closePageWindow(onCloseData);
}
function onClickCancel(){
	var obj = {};
	var windowWrapper = new kendoWindowWrapper();
	windowWrapper.closePageWindow(obj);
}

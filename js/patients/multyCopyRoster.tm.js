var parentRef = null;
var operation = "";
var selItem = null;
var ADD = "add";
var UPDATE = "edit";
var VIEW = "view";
var DELETE = "delete";
var patientId = "";
var rs = "";
var selRSItem = null;

var towArr = [{Key:'0',Value:'Sunday'},{Key:'1',Value:'Monday'},{Key:'2',Value:'Tuesday'},{Key:'3',Value:'Wednesday'},{Key:'4',Value:'Thursday'},{Key:'5',Value:'Friday'},{Key:'6',Value:'Saturday'}];
var towArr2 = [{Key:'0',Value:'Sun'},{Key:'1',Value:'Mon'},{Key:'2',Value:'Tue'},{Key:'3',Value:'Wed'},{Key:'4',Value:'Thu'},{Key:'5',Value:'Fri'},{Key:'6',Value:'Sat'}];

var photoExt = "";
var pid = "";
var pname = "";

var fid = "";
var fname = "";
var selList = [];
var prsViewArray = [];

$(document).ready(function() {
    parentRef = parent.frames['iframe'].window;
    var pnlHeight = window.innerHeight;
});
 
$(window).load(function(){
	parentRef = parent.frames['iframe'].window;
	selList = parentRef.selList;
	/*if(selList.length > 0) {
		for(var s=0;s<selList.length;s++) {
			$("#cmbWeekId  option[value='"+ selList[s].weekId+"']").remove();
		}
	}*/
	init();
	buttonEvents();
});

function buttonEvents(){
	//$("#chkOverride").off("click");
	//$("#chkOverride").on("click",onChangeOverride);
	
	$("#btnSave").off("click");
	$("#btnSave").on("click",onClickOK);
	
	$("#btnCancelDet").off("click");
	$("#btnCancelDet").on("click",onClickCancel);

	$("#btnSelectAll").off("click");
	$("#btnSelectAll").on("click",onClickSelectAll);

	$("#btnUnselectAll").off("click");
	$("#btnUnselectAll").on("click",onClickUnselectAll);

	$('input[type=radio][name=rbnCopyType]').change(function() {
		if (this.value == '1') {
			$("#divAdd").hide();
			$("#divWeek").show();
			$("#divMulWeek").hide();
		}
		else if (this.value == '2') {
			$("#divAdd").show();
			$("#divWeek").hide();
			$("#divMulWeek").show();
			$("#cmbMulWeekId").multipleSelect({
				selectAll: true,
				width: 200,
				dropWidth: 200,
				multipleWidth: 200,
				placeholder: 'Select Week'

			});
			$("#cmbRDow").empty();
			var selDays = [];
			for(var s=0;s<towArr2.length;s++) {
				var sItem = towArr2[s];
				selDays.push(sItem);
			}

			for (var i = 0; i < selDays.length; i++) {
				$("#cmbRDow").append('<option value="' + selDays[i].Key + '">' + selDays[i].Value + '</option>');
			}

			$("#cmbRDow").multipleSelect({
				selectAll: true,
				width: 200,
				dropWidth: 200,
				multipleWidth: 200,
				placeholder: 'Select DOW',
				selectAllText: 'All'

			});
		}
	});

	$("#btnAdd").off("click");
	$("#btnAdd").on("click",onClickAdd);

	$("#cmbWeekId").off("change");
	$("#cmbWeekId").on("change",onChangeWeek);


	$("#divTableWeeks").on("click","a.closeweek",function(){
		$(this).parent().parent().remove();
	});


	bindDowComboBox(1);

}

function onClickRemoveWeek(){
}

function onChangeWeek(){
	$('#cmbRDow').multipleSelect('uncheckAll');

	var selWeekId = parseInt($("#cmbWeekId").val());
	bindDowComboBox(selWeekId);

}


function bindDowComboBox(weekno){
	$("#cmbRDow").empty();

	var weekDayStart = -1;

	if(selList !== null && selList.length > 0){
		var index = selList.findIndex((e) => e.weekId === weekno);

		if(index > -1){
			weekDayStart = selList[index].weekDayStart;
		}
	}

	var selDays = [];
	for(var s=0;s<towArr2.length;s++){
		var sItem = towArr2[s];
		if(sItem && parseInt(sItem.Key) !== weekDayStart) {
			selDays.push(sItem);
		}
	}



	for (var i = 0; i < selDays.length; i++) {
		$("#cmbRDow").append('<option value="' + selDays[i].Key + '">' + selDays[i].Value + '</option>');
	}

	$("#cmbRDow").multipleSelect({
		selectAll: true,
		width: 200,
		dropWidth: 200,
		multipleWidth: 200,
		placeholder: 'Select DOW',
		selectAllText: 'All'

	});

}

function onClickSelectAll(){
	var txtRDow = $("#txtRDow").data("kendoMultiSelect");

	var values = $.map(txtRDow._data(), function(dataItem) {
		return dataItem.Key;
	});

	txtRDow.value(values);
}

function onClickAdd(){
	//$("#divTableWeeks").empty();
	var selWeekId;
	var mulSelWeeks;
	var copyTypeValue = parseInt($("input[name='rbnCopyType']:checked").val());
	if(copyTypeValue == 1){
		selWeekId = $("#cmbWeekId").val();
	}
	else{
		mulSelWeeks = $('#cmbMulWeekId').multipleSelect('getSelects');
	}


	var arr = $('#cmbRDow').multipleSelect('getSelects');

	for(var w = 0; w < mulSelWeeks.length ; w++) {

		var weekDayStart = -1;

		if(selList !== null && selList.length > 0){
			var index = selList.findIndex((e) => e.weekId === Number(mulSelWeeks[w]));

			if(index > -1){
				weekDayStart = selList[index].weekDayStart;
			}
		}

		// var selDays = [];
		// for(var s=0;s<towArr2.length;s++){
		// 	var sItem = towArr2[s];
		// 	if(sItem && parseInt(sItem.Key) !== weekDayStart) {
		// 		selDays.push(sItem);
		// 	}
		// }


		if (arr !== null && arr.length > 0) {

			var $week = $('#divTableWeeks').find('div[weekno=' + Number(mulSelWeeks[w]) + ']');

			var strHTML = '';

			if ($week && $week.length > 0) {
				var $table = $($week).find('table tbody');
				$table.empty();

				strHTML = strHTML + '<tr>';

				for (var i = 0; i < towArr2.length; i++) {
					var sItem = towArr2[i];
					if(sItem && parseInt(sItem.Key) !== weekDayStart) {

						var index = arr.findIndex((e) => e === towArr2[i].Key);

						if (index > -1) {
							strHTML = strHTML + '<td class="circle">' + towArr2[i].Value + '</td>';
						} else {
							strHTML = strHTML + '<td>' + towArr2[i].Value + '</td>';
						}
					}

				}

				strHTML = strHTML + '</tr>';
				$table.append(strHTML);

			} else {


				strHTML = strHTML + '<div class="col-xs-6 divweekdays" weekno="' + Number(mulSelWeeks[w]) + '"><div class="col-xs-6"> <a href="javascript:void(0)" class="closeweek" style="float:right;"><img src="../../img/remove.png" style="width:15px;height:15px" alt="remove"></a> </div><div class="col-xs-12"> <div class="col-xs-6 input-block"> <div class="col-xs-12">';
				strHTML = strHTML + '<label class="">Week Number <span>' + Number(mulSelWeeks[w]) + '</span></label>';
				strHTML = strHTML + '</div> </div> </div> <div class="col-xs-12"> <div class="col-xs-6"> <div class="zui-wrapper"> <div> <table class="table copyappt"> <tbody> <tr>';

				for (var i = 0; i < towArr2.length; i++) {
					var sItem = towArr2[i];
					if (sItem && parseInt(sItem.Key) !== weekDayStart) {

						var index = arr.findIndex((e) => e === towArr2[i].Key);

						if (index > -1) {
							strHTML = strHTML + '<td class="circle">' + towArr2[i].Value + '</td>';
						} else {
							strHTML = strHTML + '<td>' + towArr2[i].Value + '</td>';
						}

					}
				}

				strHTML = strHTML + '</tr> </tbody> </table> </div> </div> </div></div> </div>';


				$("#divTableWeeks").append(strHTML);
			}
		} else {
			customAlert.error("Error", "Please select atleast one day");
		}

	}

}

function onClickUnselectAll(){
	var txtRDow = $("#txtRDow").data("kendoMultiSelect");
	txtRDow.value([]);
}

function init(){
	//if(selList[0].dowK == selList[0].doeK){
	var selDays = [];
	for(var s=0;s<towArr2.length;s++){
		var sItem = towArr2[s];
		if(sItem && sItem.Key != selList[0].dowK) {
			selDays.push(sItem);
		}
	}

	for (var i = 0; i < selDays.length; i++) {
		$("#cmbRDow").append('<option value="' + selDays[i].Key + '">' + selDays[i].Value + '</option>');
	}

	$("#cmbRDow").multipleSelect({
		selectAll: true,
		width: 200,
		dropWidth: 200,
		multipleWidth: 200,
		placeholder: 'Select DOW',
		selectAllText: 'All'

	});
	
	$('input[type=radio][name=rbnCopyType][value=1]').prop("checked", true);

}
var apptLenghth = 0;
var responseLength = 0;
function onClickOK(){

	customAlert.confirm("Confirm", "Are you sure want to create care plan records as per selection?",function(response){
		if(response.button == "Yes"){

			var copyTypeValue = parseInt($("input[name='rbnCopyType']:checked").val());
			if (copyTypeValue === 1) {
				var selArray = [];

				var arr = $('#cmbRDow').multipleSelect('getSelects');;
				if(arr && arr.length>0){
					for(var j=0;j<arr.length;j++){
						selArray.push(arr[j]);
					}
					parentRef.selArray = selArray;
					parentRef.selWeekId = $("#cmbWeekId").val();
					var obj = {};
					  obj.status = "true";
					   popupClose(obj);
				}


				if(selArray.length == 0){
					customAlert.error("Error", "Please select start days");
				}
			}
			else{
					var $divweeks = $("#divTableWeeks").find("div.divweekdays");


					var dtArr = [];

					if(selList !== null && selList.length > 0 && $divweeks !== null && $divweeks.length > 0){
						for(var c = 0; c < selList.length ; c++){

							var obj = selList[c];
							obj.fromTime = obj.fromTimeK;
							obj.toTime = obj.toTimeK;
							obj.contract = obj.contractK;
							obj.reasonId = obj.reasonK;

							delete obj.id;
							delete obj.gender;
							delete obj.language;
							delete obj.patientBillingRate;
							delete obj.provider;
							delete obj.patient;
							delete obj.fromTimeK;
							delete obj.toTimeK;
							delete obj.modifiedBy;
							delete obj.appointmentType;
							delete obj.billToName;
							delete obj.appointmentTypeId;
							delete obj.createdDate;
							delete obj.contractId;
							delete obj.modifiedDate;
							delete obj.facility;
							delete obj.age;
							delete obj.idk;
							delete obj.contractK;
							delete obj.facilityK;
							delete obj.providerK;
							delete obj.dow;
							delete obj.dowK;
							delete obj.doe;
							delete obj.doeK;
							delete obj.AppType;
							delete obj.AppTypeK;
							delete obj.reasonK;
							delete obj.AppTypeK;
							delete obj.patientK;
							delete obj.SEL;

							for(var i = 0 ; i < $divweeks.length; i++){

								var weekid = Number($($divweeks[i]).find("span").text());
								var $tdArr = $($divweeks[i]).find("td.circle");

								if($tdArr !== null && $tdArr.length > 0){

									for(var j = 0 ; j < $tdArr.length ; j++){

										var dow = $($tdArr[j]).text();
										var index = towArr2.findIndex((e) => e.Value === dow);

										obj.weekId = weekid;
										obj.weekDayStart = index;
										obj.weekDayEnd = index;
										obj.careTypeId = 2;

										dtArr.push(obj);


									}
								}
							}

						}
					}


				apptLenghth = dtArr;
				if(dtArr.length > 0 ){
					for(var r=0;r<dtArr.length;r++) {
						var req = {};
						req= dtArr[r];
						var dataUrl = ipAddress+"/patient/roster/create";
						createAjaxObject(dataUrl,req,"POST",onCreate,onError);

					}
				}
			}
		}
	});


}

function onCloseRoster(){
	var windowWrapper = new kendoWindowWrapper();
	windowWrapper.closePageWindow(obj);
}

function onCreate(dataObj){
	debugger;
	//customAlert.error("Info","Appointments created succeesfully");
	if(dataObj && dataObj.response &&  dataObj.response.status){
		if(apptLenghth.length > 0) {
			responseLength = responseLength + 1;
			// var obj = {};
			// obj.msg = dataObj.response.status.message;
			//
			// var date = new Date(GetDateTimeEditDay(dataObj.response.appointment[0].dateOfAppointment));
			// var day = date.getDay();
			// var dow = getWeekDayName(day);
			// obj.dateOfAppointment = kendo.toString(new Date(dataObj.response.appointment[0].dateOfAppointment), "dd-MM-yyyy hh:mm tt");
			// obj.week = dow;
			// obj.weekid = dataObj.response.appointment[0].weekId;
			// obj.code =  dataObj.response.status.code;
			// successReponse.push(obj);
			if(responseLength == apptLenghth.length){
				customAlert.error("Info", "Care Plan created successfully");
				setTimeout(function(){
					onClickCancel();
				},1000);
			}
		}

	}else{
		customAlert.error("Error",dataObj.response.status.message);
	}

}

function onClickCancel() {
    var obj = {};
    obj.status = "false";
    popupClose(obj);
}


function popupClose(obj) {
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}
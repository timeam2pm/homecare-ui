var angularUIgridWrapper = null;
var parentRef = null;

var relationdataArray = [];
$(document).ready(function(){
    sessionStorage.setItem("IsSearchPanel", "1");
    var dataOptions = {
        pagination: false,
        changeCallBack: onChange
    }
    angularUIgridWrapper = new AngularUIGridWrapper("dgridBillingList", dataOptions);
    angularUIgridWrapper.init();
    buildDeviceListGrid([]);
});


$(window).load(function(){
    loading = false;
    parentRef = parent.frames['iframe'].window;
    $(window).resize(adjustHeight);
    onLoaded();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
    if(parentRef && parentRef.searchZip == true){
        $("#btnSave").text("OK");
    }
    init();
    buttonEvents();
    adjustHeight();
}

function init(){
    buildDeviceListGrid([]);
    getAjaxObject(ipAddress + "/master/relation/list/", "GET", getRelations, onError);
    getAjaxObject(ipAddress+"/carehome/patient-billing/?is-active=1&patientId="+ parentRef.patientId +"&fields=*,shift.*,travelMode.*,billingType.*,billingPeriod.*,billTo.name,billingRateType.code","GET",getPatientBilling,onError);
}
function onError(errorObj){
    console.log(errorObj);
}
function getPatientBilling(dataObj){
    console.log(dataObj);
    var tempCompType = [];
    if(dataObj && dataObj.response && dataObj.response.status && dataObj.response.status.code == "1"){
        if(dataObj.response.patientBilling){
            if($.isArray(dataObj.response.patientBilling)){
                tempCompType = dataObj.response.patientBilling;
            }else{
                tempCompType.push(dataObj.response.patientBilling);
            }
        }
    }

    for(var i=0;i<tempCompType.length;i++){
        tempCompType[i].idk = tempCompType[i].id;
        if(tempCompType[i].relationshipId != null) {
            tempCompType[i].relationship = getTypeNameById(tempCompType[i].relationshipId);
        }
        else{
            tempCompType[i].relationship = "";
        }
    }
    buildDeviceListGrid(tempCompType);
}
function buttonEvents(){

    $("#btnSave").off("click",onClickOK);
    $("#btnSave").on("click",onClickOK);

    $("#btnCancel").off("click",onClickCancel);
    $("#btnCancel").on("click",onClickCancel);

    $("#btnDelete").off("click",onClickDelete);
    $("#btnDelete").on("click",onClickDelete);

    $("#btnAdd").off("click");
    $("#btnAdd").on("click",onClickAdd);

    $("#btnActive").off("click");
    $("#btnActive").on("click",onClickActive);

    $("#btnInActive").off("click");
    $("#btnInActive").on("click",onClickInActive);
}

function adjustHeight(){
    var defHeight = 220;//+window.top.getAvailPageHeight();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        defHeight = 80;
    }
    var cmpHeight = window.innerHeight - defHeight;
    cmpHeight = (cmpHeight < 200) ? 200 : cmpHeight;
    angularUIgridWrapper.adjustGridHeight(cmpHeight);
}

var isActive = 1;
function onClickActive(){
    isActive = 1;
    $("#btnInActive").removeClass("selectButtonBarClass");
    $("#btnActive").addClass("selectButtonBarClass");
    init();
}
function onClickInActive(){
    isActive = 0;
    $("#btnActive").removeClass("selectButtonBarClass");
    $("#btnInActive").addClass("selectButtonBarClass");
    init();
}


function buildDeviceListGrid(dataSource) {
    var gridColumns = [];
    var otoptions = {};
    otoptions.noUnselect = false;
    gridColumns.push({
        "title": "Bill To",
        "field": "billToname"
    });
    gridColumns.push({
        "title": "Type",
        "field": "billingTypeValue"
    });
    gridColumns.push({
        "title": "Relationship",
        "field": "relationship"
    });
    gridColumns.push({
        "title": "Category",
        "field": "shiftValue"
    });
    gridColumns.push({
        "title": "Rate Type",
        "field": "billingRateTypeCode"
    });
    gridColumns.push({
        "title": "Rate",
        "field": "rate"
    });

    angularUIgridWrapper.creategrid(dataSource, gridColumns,otoptions);
    adjustHeight();
    //onIndividualRowClick();
}

var prevSelectedItem =[];
function onChange(){
    setTimeout(function(){
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if(selectedItems && selectedItems.length>0){
            $("#btnSave").prop("disabled", false);
            $("#btnDelete").prop("disabled", false);
        }else{
            $("#btnSave").prop("disabled", true);
            $("#btnDelete").prop("disabled", true);
        }
    });
}

function onClickOK(){
    setTimeout(function(){
        var selectedItems = angularUIgridWrapper.getSelectedRows();
        console.log(selectedItems);
        if(selectedItems && selectedItems.length>0){
            var obj = {};
            obj.selItem = selectedItems[0];
            if($("#btnSave").text() ==  "OK"){
                obj.status = "success";
                var windowWrapper = new kendoWindowWrapper();
                windowWrapper.closePageWindow(obj);
            }else{
                operation = "update";
                parentRef.operation = operation;
                parentRef.selItem = selectedItems[0];
                addZip();
            }

        }
    })
}
function onClickDelete(){
    customAlert.confirm("Confirm", "Are you sure you want delete?",function(response){
        if(response.button == "Yes"){
            var selectedItems = angularUIgridWrapper.getSelectedRows();
            console.log(selectedItems);
            if(selectedItems && selectedItems.length>0){
                var selItem = selectedItems[0];
                if(selItem){
                    var dataUrl = ipAddress+"/city/delete/";
                    var reqObj = {};
                    reqObj.id = selItem.idk;
                    reqObj.isDeleted = "0";
                    reqObj.isActive = "0";
                    reqObj.modifiedBy = sessionStorage.userId;//"101";
                    createAjaxObject(dataUrl,reqObj,"POST",onDeleteCountryt,onError);
                }
            }
        }
    });
}
function onDeleteCountryt(dataObj){
    if(dataObj && dataObj.response.status){
        if(dataObj.response.status.code == "1"){
            customAlert.info("info", "Deleted Successfully");
            init();
        }else{
            customAlert.error("error", dataObj.response.status.message);
        }
    }
}
function onClickAdd(){

    operation = "add";
    parentRef.operation = operation;
    parentRef.selItem = null;
    addZip();

}
var operation = "";

function addZip(){
    var popW = 600;
    var popH = 500;

    var profileLbl;
    var devModelWindowWrapper = new kendoWindowWrapper();
    if(operation == "add"){
        profileLbl = "Add Zip";
    }else{
        profileLbl = "Edit Zip";
    }
    var cntry = sessionStorage.countryName;
    if(cntry.indexOf("India")>=0){
        if(operation == "add"){
            profileLbl = "Add Postal Code";
        }else{
            profileLbl = "Edit Postal Code";
        }
    }else if(cntry.indexOf("United Kingdom")>=0){
        if(operation == "add"){
            profileLbl = "Add Postal Code";
        }else{
            profileLbl = "Edit Postal Code";
        }
    }else if(cntry.indexOf("United State")>=0){
        if(operation == "add"){
            profileLbl = "Add Zip";
        }else{
            profileLbl = "Edit Zip";
        }
    }else{
        if(operation == "add"){
            profileLbl = "Add Zip";
        }else{
            profileLbl = "Edit Zip";
        }
    }
    devModelWindowWrapper.openPageWindow("../../html/masters/createZip.html", profileLbl, popW, popH, true, closeAddZipAction);
}
function closeAddZipAction(evt,returnData){
    if(returnData && returnData.status == "success"){
        if(returnData.operation == "add"){
            customAlert.info("Info","Created Successfully.")
        }else if(returnData.operation == "update"){
            customAlert.info("Info","Updated Successfully.")
        }
    }
    init();
}
function onClickCancel(){
    var obj = {};
    var windowWrapper = new kendoWindowWrapper();
    windowWrapper.closePageWindow(obj);
}

function getTypeNameById(aId){
    for(var i=0;i<relationdataArray.length;i++){
        var item = relationdataArray[i];
        if(item && item.id == aId){
            return relationdataArray[i].desc;
        }
    }
    return 0;
}

function getRelations(dataObj) {
    if (dataObj && dataObj.response && dataObj.response.status) {
        if (dataObj.response.status.code == "1") {
            relationdataArray = dataObj.response.codeTable || [];
        }
    }
}
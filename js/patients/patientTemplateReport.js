var parentRef = null;
var recordType = "1";
var dataArray = [];
var selItem = null;
var resultDataObj;
$(document).ready(function(){
	parentRef = parent.frames['iframe'].window;
});

$(window).load(function(){
	loading = false;
	//$(window).resize(adjustHeight);
	onLoaded();
	if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $("html").attr("style", "padding-bottom:1px !important; width:100% !important;");
    }
});

function onLoaded(){
	init()
}
function init(){
	console.log(parentRef);
	//resultDataObj = parentRef;
	var patientId = sessionStorage.patientId;
	getAjaxObject(ipAddress + "/patient/"+patientId+"/", "GET", onGetPatientInfo, onError);
}

function onError(errorObj){
	console.log(errorObj);
}

function buttonEvents(){
	$("#btnCancelDet").off("click",onClickCancel);
	$("#btnCancelDet").on("click",onClickCancel);
}

function onClickCancel(){
	var obj = {};
	var windowWrapper = new kendoWindowWrapper();
	windowWrapper.closePageWindow(obj);
}
function onGetPatientInfo(dataObj) {
    patientInfoObject = dataObj.response;
    
    var dob = dformat(new Date(patientInfoObject.patient.dateOfBirth).getTime(), 'dd-MM-yyyy');
    console.log(patientInfoObject);
    var dt = new Date(patientInfoObject.patient.modifiedDate);
	dt = kendo.toString(dt, "dd-MM-yyyy h:mm:tt");
    var modifiedDate = dt;
    $('#first').text(patientInfoObject.patient.firstName);
    $('#middle').text(patientInfoObject.patient.middleName);
    $('#last').text(patientInfoObject.patient.lastName);
    $('#dateOfBirth').text(dob);
    $('#gender').text(patientInfoObject.patient.gender);
    $('#add1').text(patientInfoObject.communication.address1);
    $('#add2').text(patientInfoObject.communication.address2);
    $('#city').text(patientInfoObject.communication.city);
    $('#state').text(patientInfoObject.communication.state);
    $('#country').text(patientInfoObject.communication.country);
    $('#zip').text(patientInfoObject.communication.zip);
    $('#hPhone').text(patientInfoObject.communication.homePhone);
    $('#cPhone').text(patientInfoObject.communication.cellPhone);
    $('#rDateTime').text(modifiedDate);
    setFormData(parentRef.dataObj);
    
}
function setFormData(resultDataObj){
	var strHtml = "";
	selItem = parentRef.selItem;
	$('#templateName').text(selItem.name);
	$("#divPanel").html("");
	miniTemplates = resultDataObj.response.templateValues[0].componentValues;
	$("#txtNotes").text(resultDataObj.response.templateValues[0].notes);
	console.log(miniTemplates);
	var eles = '';
	var miniTemplatesArray = selItem.miniTemplates;
	
	for(var l=0;l<miniTemplatesArray.length;l++){
	var miniTemplateById = _.where(miniTemplates,{miniTemplateId:parseInt(miniTemplatesArray[l].id)});
	if(miniTemplateById.length)
	{
		miniTemplateById = _.sortBy(miniTemplateById, 'miniComponentDisplayOrder');
		strHtml = strHtml+'<div class="panel-heading" style="background-color: #F4F4F4 !important;">'+miniTemplatesArray[l].name+'</div>';
	}
	for(var m=0;m<miniTemplateById.length;m++){
	
		var mItem = miniTemplateById[m];
		if(mItem){
			var mItemHref = mItem.miniComponentName;
			var mValue = mItem.value || '';
			var mItemId = "m"+mItem.id;
			var pBodyItem = "p"+mItem.id;
			var strhref = "#mini"+mItem.id;
			
			if(mItem.miniComponentType && mItem.miniComponentType.toLowerCase().indexOf('date')>-1&&mValue.length>0)
				{
					var dob = dformat(new Date(parseInt(mValue)).getTime(), 'dd-MM-yyyy');
					mValue = dob;
				}
			/*strHtml = strHtml+'<div class="panel panel-default">';
			strHtml = strHtml+'<div class="panel-heading">';
			strHtml = strHtml+'<a href="'+strhref+'" class="pnlTitle" id="'+mItemId+'" idk="'+mItem.id+'">'+mItem.miniComponentName+'</a></div>';
			strHtml = strHtml+'<div id="mini'+mItem.id+'"><div class="panel-body" id="'+pBodyItem+'"style="padding-bottom:0px;padding:10px"></div></div></div>';*/
			//componenets = miniTemplateById[m].componenets;
			//for(var n=0;n<componenets.length;n++){
				//componenets = _.sortBy(componenets, 'miniComponentDisplayOrder');
				//console.log(componenets[n]);
				//if(!componenets[n].isDeleted)
			if(mValue.indexOf('~')>-1)
				mValue = mValue.split('~');
			if(mItem.miniComponentName && mItem.miniComponentName.toLowerCase() != "hline"){
                if(mItem.miniComponentName.length >= 120) {
                    strHtml += '<div class="row"><div class="col-xs-6 leftDiv leftDivReport-height"><label>' + mItem.miniComponentName + '</label></div><div class="col-xs-6 rightDiv leftDivReport-height"><label>' + mValue + '</label></div></div>';
                }
                else{
                    strHtml += '<div class="row"><div class="col-xs-6 leftDiv"><label>' + mItem.miniComponentName + '</label></div><div class="col-xs-6 rightDiv"><label>' + mValue + '</label></div></div>';
				}
			}

			//}
			//strHtml = strHtml+eles;
		}
	}
	}
	$("#divPanel").html(strHtml);
	if(!resultDataObj.response.templateValues[0].notes.length)
		$('.notes').css('display','none');
	console.log(miniTemplatesArray);
	/*for(var n=0;n<miniTemplatesArray.length;n++){
		componenets = miniTemplatesArray[m];
		console.log(miniTemplatesArray);
		var eles = '<div class="col-xs-6"><label>'+componenets[n].label+'</label></div><div class="col-xs-6"></div>'
	}*/
	
}

function dformat(time, format) {
            var t = new Date(time);
            var tf = function (i) { return (i < 10 ? '0' : '') + i };
            return format.replace(/yyyy|MM|dd|HH|mm|ss/g, function (a) {
                switch (a) {
                    case 'yyyy':
                        return tf(t.getFullYear());
                        break;
                    case 'MM':
                        return tf(t.getMonth() + 1);
                        break;
                    case 'mm':
                        return tf(t.getMinutes());
                        break;
                    case 'dd':
                        return tf(t.getDate());
                        break;
                    case 'HH':
                        return tf(t.getHours());
                        break;
                    case 'ss':
                        return tf(t.getSeconds());
                        break;
                }
            })
        }
function printReport(){
	$('#report-print').html($('.panel').html());
	$('#report-print').printMe({ "path": ["../../css/external/css/bootstrap.min.css","../../js/external/angular/ui-grid-angular.css","../../css/external/css/timeam.css","../../css/external/css/customStyles.css","../../css/patient/patient-report.css"]});
}